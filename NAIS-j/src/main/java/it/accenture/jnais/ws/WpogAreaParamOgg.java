package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WpogTab;

/**Original name: WPOG-AREA-PARAM-OGG<br>
 * Variable: WPOG-AREA-PARAM-OGG from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WpogAreaParamOgg {

    //==== PROPERTIES ====
    //Original name: WPOG-ELE-PARAM-OGG-MAX
    private short wpogEleParamOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPOG-TAB
    private WpogTab wpogTab = new WpogTab();

    //==== METHODS ====
    public void setWpogAreaParamOggFormatted(String data) {
        byte[] buffer = new byte[Len.WPOG_AREA_PARAM_OGG];
        MarshalByte.writeString(buffer, 1, data, Len.WPOG_AREA_PARAM_OGG);
        setWpogAreaParamOggBytes(buffer, 1);
    }

    public void setWpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        wpogEleParamOggMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wpogTab.setWpogTabBytes(buffer, position);
    }

    public byte[] getWpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpogEleParamOggMax);
        position += Types.SHORT_SIZE;
        wpogTab.getWpogTabBytes(buffer, position);
        return buffer;
    }

    public void setWpogEleParamOggMax(short wpogEleParamOggMax) {
        this.wpogEleParamOggMax = wpogEleParamOggMax;
    }

    public short getWpogEleParamOggMax() {
        return this.wpogEleParamOggMax;
    }

    public WpogTab getWpogTab() {
        return wpogTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_ELE_PARAM_OGG_MAX = 2;
        public static final int WPOG_AREA_PARAM_OGG = WPOG_ELE_PARAM_OGG_MAX + WpogTab.Len.WPOG_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
