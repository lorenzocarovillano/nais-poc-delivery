package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDEQ-RISP-PC<br>
 * Variable: WDEQ-RISP-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqRispPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqRispPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_RISP_PC;
    }

    public void setWdeqRispPc(AfDecimal wdeqRispPc) {
        writeDecimalAsPacked(Pos.WDEQ_RISP_PC, wdeqRispPc.copy());
    }

    public void setWdeqRispPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_RISP_PC, Pos.WDEQ_RISP_PC);
    }

    /**Original name: WDEQ-RISP-PC<br>*/
    public AfDecimal getWdeqRispPc() {
        return readPackedAsDecimal(Pos.WDEQ_RISP_PC, Len.Int.WDEQ_RISP_PC, Len.Fract.WDEQ_RISP_PC);
    }

    public byte[] getWdeqRispPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_RISP_PC, Pos.WDEQ_RISP_PC);
        return buffer;
    }

    public void initWdeqRispPcSpaces() {
        fill(Pos.WDEQ_RISP_PC, Len.WDEQ_RISP_PC, Types.SPACE_CHAR);
    }

    public void setWdeqRispPcNull(String wdeqRispPcNull) {
        writeString(Pos.WDEQ_RISP_PC_NULL, wdeqRispPcNull, Len.WDEQ_RISP_PC_NULL);
    }

    /**Original name: WDEQ-RISP-PC-NULL<br>*/
    public String getWdeqRispPcNull() {
        return readString(Pos.WDEQ_RISP_PC_NULL, Len.WDEQ_RISP_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_PC = 1;
        public static final int WDEQ_RISP_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_PC = 4;
        public static final int WDEQ_RISP_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
