package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-BOLLO-DETT-C<br>
 * Variable: LQU-IMPB-BOLLO-DETT-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_BOLLO_DETT_C;
    }

    public void setLquImpbBolloDettC(AfDecimal lquImpbBolloDettC) {
        writeDecimalAsPacked(Pos.LQU_IMPB_BOLLO_DETT_C, lquImpbBolloDettC.copy());
    }

    public void setLquImpbBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_BOLLO_DETT_C, Pos.LQU_IMPB_BOLLO_DETT_C);
    }

    /**Original name: LQU-IMPB-BOLLO-DETT-C<br>*/
    public AfDecimal getLquImpbBolloDettC() {
        return readPackedAsDecimal(Pos.LQU_IMPB_BOLLO_DETT_C, Len.Int.LQU_IMPB_BOLLO_DETT_C, Len.Fract.LQU_IMPB_BOLLO_DETT_C);
    }

    public byte[] getLquImpbBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_BOLLO_DETT_C, Pos.LQU_IMPB_BOLLO_DETT_C);
        return buffer;
    }

    public void setLquImpbBolloDettCNull(String lquImpbBolloDettCNull) {
        writeString(Pos.LQU_IMPB_BOLLO_DETT_C_NULL, lquImpbBolloDettCNull, Len.LQU_IMPB_BOLLO_DETT_C_NULL);
    }

    /**Original name: LQU-IMPB-BOLLO-DETT-C-NULL<br>*/
    public String getLquImpbBolloDettCNull() {
        return readString(Pos.LQU_IMPB_BOLLO_DETT_C_NULL, Len.LQU_IMPB_BOLLO_DETT_C_NULL);
    }

    public String getLquImpbBolloDettCNullFormatted() {
        return Functions.padBlanks(getLquImpbBolloDettCNull(), Len.LQU_IMPB_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_BOLLO_DETT_C = 1;
        public static final int LQU_IMPB_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_BOLLO_DETT_C = 8;
        public static final int LQU_IMPB_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
