package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-OLD-TS-TEC<br>
 * Variable: TGA-OLD-TS-TEC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaOldTsTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaOldTsTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_OLD_TS_TEC;
    }

    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        writeDecimalAsPacked(Pos.TGA_OLD_TS_TEC, tgaOldTsTec.copy());
    }

    public void setTgaOldTsTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_OLD_TS_TEC, Pos.TGA_OLD_TS_TEC);
    }

    /**Original name: TGA-OLD-TS-TEC<br>*/
    public AfDecimal getTgaOldTsTec() {
        return readPackedAsDecimal(Pos.TGA_OLD_TS_TEC, Len.Int.TGA_OLD_TS_TEC, Len.Fract.TGA_OLD_TS_TEC);
    }

    public byte[] getTgaOldTsTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_OLD_TS_TEC, Pos.TGA_OLD_TS_TEC);
        return buffer;
    }

    public void setTgaOldTsTecNull(String tgaOldTsTecNull) {
        writeString(Pos.TGA_OLD_TS_TEC_NULL, tgaOldTsTecNull, Len.TGA_OLD_TS_TEC_NULL);
    }

    /**Original name: TGA-OLD-TS-TEC-NULL<br>*/
    public String getTgaOldTsTecNull() {
        return readString(Pos.TGA_OLD_TS_TEC_NULL, Len.TGA_OLD_TS_TEC_NULL);
    }

    public String getTgaOldTsTecNullFormatted() {
        return Functions.padBlanks(getTgaOldTsTecNull(), Len.TGA_OLD_TS_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_OLD_TS_TEC = 1;
        public static final int TGA_OLD_TS_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_OLD_TS_TEC = 8;
        public static final int TGA_OLD_TS_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_OLD_TS_TEC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_OLD_TS_TEC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
