package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-DT-SCAD-ADES-DFLT<br>
 * Variable: DAD-DT-SCAD-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadDtScadAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadDtScadAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_DT_SCAD_ADES_DFLT;
    }

    public void setDadDtScadAdesDflt(int dadDtScadAdesDflt) {
        writeIntAsPacked(Pos.DAD_DT_SCAD_ADES_DFLT, dadDtScadAdesDflt, Len.Int.DAD_DT_SCAD_ADES_DFLT);
    }

    public void setDadDtScadAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_DT_SCAD_ADES_DFLT, Pos.DAD_DT_SCAD_ADES_DFLT);
    }

    /**Original name: DAD-DT-SCAD-ADES-DFLT<br>*/
    public int getDadDtScadAdesDflt() {
        return readPackedAsInt(Pos.DAD_DT_SCAD_ADES_DFLT, Len.Int.DAD_DT_SCAD_ADES_DFLT);
    }

    public byte[] getDadDtScadAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_DT_SCAD_ADES_DFLT, Pos.DAD_DT_SCAD_ADES_DFLT);
        return buffer;
    }

    public void setDadDtScadAdesDfltNull(String dadDtScadAdesDfltNull) {
        writeString(Pos.DAD_DT_SCAD_ADES_DFLT_NULL, dadDtScadAdesDfltNull, Len.DAD_DT_SCAD_ADES_DFLT_NULL);
    }

    /**Original name: DAD-DT-SCAD-ADES-DFLT-NULL<br>*/
    public String getDadDtScadAdesDfltNull() {
        return readString(Pos.DAD_DT_SCAD_ADES_DFLT_NULL, Len.DAD_DT_SCAD_ADES_DFLT_NULL);
    }

    public String getDadDtScadAdesDfltNullFormatted() {
        return Functions.padBlanks(getDadDtScadAdesDfltNull(), Len.DAD_DT_SCAD_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_DT_SCAD_ADES_DFLT = 1;
        public static final int DAD_DT_SCAD_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_DT_SCAD_ADES_DFLT = 5;
        public static final int DAD_DT_SCAD_ADES_DFLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_DT_SCAD_ADES_DFLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
