package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-BNS<br>
 * Variable: WTGA-IMP-BNS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_BNS;
    }

    public void setWtgaImpBns(AfDecimal wtgaImpBns) {
        writeDecimalAsPacked(Pos.WTGA_IMP_BNS, wtgaImpBns.copy());
    }

    public void setWtgaImpBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_BNS, Pos.WTGA_IMP_BNS);
    }

    /**Original name: WTGA-IMP-BNS<br>*/
    public AfDecimal getWtgaImpBns() {
        return readPackedAsDecimal(Pos.WTGA_IMP_BNS, Len.Int.WTGA_IMP_BNS, Len.Fract.WTGA_IMP_BNS);
    }

    public byte[] getWtgaImpBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_BNS, Pos.WTGA_IMP_BNS);
        return buffer;
    }

    public void initWtgaImpBnsSpaces() {
        fill(Pos.WTGA_IMP_BNS, Len.WTGA_IMP_BNS, Types.SPACE_CHAR);
    }

    public void setWtgaImpBnsNull(String wtgaImpBnsNull) {
        writeString(Pos.WTGA_IMP_BNS_NULL, wtgaImpBnsNull, Len.WTGA_IMP_BNS_NULL);
    }

    /**Original name: WTGA-IMP-BNS-NULL<br>*/
    public String getWtgaImpBnsNull() {
        return readString(Pos.WTGA_IMP_BNS_NULL, Len.WTGA_IMP_BNS_NULL);
    }

    public String getWtgaImpBnsNullFormatted() {
        return Functions.padBlanks(getWtgaImpBnsNull(), Len.WTGA_IMP_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_BNS = 1;
        public static final int WTGA_IMP_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_BNS = 8;
        public static final int WTGA_IMP_BNS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_BNS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
