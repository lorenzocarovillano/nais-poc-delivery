package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.AmmbFunzFunzLccs0023;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Lccc0007;
import it.accenture.jnais.copy.Ldbv2441;
import it.accenture.jnais.copy.Ldbvg781;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.ws.enums.WsFgAnnVag;
import it.accenture.jnais.ws.enums.WsFgDefaultGravita;
import it.accenture.jnais.ws.enums.WsFgMoviAnn;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpAnnullo;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.occurs.WmovTabMovi;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0023<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0023Data {

    //==== PROPERTIES ====
    public static final int WMOV_TAB_MOVI_MAXOCCURS = 30;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0023";
    //Original name: PGM-IDBSPOL0
    private String pgmIdbspol0 = "IDBSPOL0";
    //Original name: PGM-IDBSADE0
    private String pgmIdbsade0 = "IDBSADE0";
    //Original name: PGM-IDBSTGA0
    private String pgmIdbstga0 = "IDBSTGA0";
    //Original name: WK-ELE-MOV-MAX
    private short wkEleMovMax = ((short)30);
    //Original name: LCCC0007
    private Lccc0007 lccc0007 = new Lccc0007();
    /**Original name: WS-FG-DEFAULT-GRAVITA<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private WsFgDefaultGravita wsFgDefaultGravita = new WsFgDefaultGravita();
    //Original name: WS-FG-MOVI-ANN
    private WsFgMoviAnn wsFgMoviAnn = new WsFgMoviAnn();
    //Original name: WS-FG-ANN-VAG
    private WsFgAnnVag wsFgAnnVag = new WsFgAnnVag();
    //Original name: IX-TAB-MOV
    private short ixTabMov = DefaultValues.BIN_SHORT_VAL;
    /**Original name: WS-TP-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     TIPOLOGICHE DI PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-TP-ANNULLO<br>
	 * <pre>*****************************************************************
	 *     TP_ANNULLO (Tipo Annullo)
	 * *****************************************************************</pre>*/
    private WsTpAnnullo wsTpAnnullo = new WsTpAnnullo();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    //Original name: WK-VARIABILI
    private WkVariabiliLccs0023 wkVariabili = new WkVariabiliLccs0023();
    //Original name: WMOV-ELE-MOVI-MAX
    private short wmovEleMoviMax = ((short)0);
    //Original name: WMOV-TAB-MOVI
    private WmovTabMovi[] wmovTabMovi = new WmovTabMovi[WMOV_TAB_MOVI_MAXOCCURS];
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: AMMB-FUNZ-FUNZ
    private AmmbFunzFunzLccs0023 ammbFunzFunz = new AmmbFunzFunzLccs0023();
    //Original name: LDBV2441
    private Ldbv2441 ldbv2441 = new Ldbv2441();
    //Original name: LDBVG781
    private Ldbvg781 ldbvg781 = new Ldbvg781();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();

    //==== CONSTRUCTORS ====
    public Lccs0023Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wmovTabMoviIdx = 1; wmovTabMoviIdx <= WMOV_TAB_MOVI_MAXOCCURS; wmovTabMoviIdx++) {
            wmovTabMovi[wmovTabMoviIdx - 1] = new WmovTabMovi();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getPgmIdbspol0() {
        return this.pgmIdbspol0;
    }

    public String getPgmIdbspol0Formatted() {
        return Functions.padBlanks(getPgmIdbspol0(), Len.PGM_IDBSPOL0);
    }

    public String getPgmIdbsade0() {
        return this.pgmIdbsade0;
    }

    public String getPgmIdbsade0Formatted() {
        return Functions.padBlanks(getPgmIdbsade0(), Len.PGM_IDBSADE0);
    }

    public String getPgmIdbstga0() {
        return this.pgmIdbstga0;
    }

    public String getPgmIdbstga0Formatted() {
        return Functions.padBlanks(getPgmIdbstga0(), Len.PGM_IDBSTGA0);
    }

    public short getWkEleMovMax() {
        return this.wkEleMovMax;
    }

    public void setIxTabMov(short ixTabMov) {
        this.ixTabMov = ixTabMov;
    }

    public short getIxTabMov() {
        return this.ixTabMov;
    }

    public void setWmovEleMoviMax(short wmovEleMoviMax) {
        this.wmovEleMoviMax = wmovEleMoviMax;
    }

    public short getWmovEleMoviMax() {
        return this.wmovEleMoviMax;
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public AmmbFunzFunzLccs0023 getAmmbFunzFunz() {
        return ammbFunzFunz;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Lccc0007 getLccc0007() {
        return lccc0007;
    }

    public Ldbv2441 getLdbv2441() {
        return ldbv2441;
    }

    public Ldbvg781 getLdbvg781() {
        return ldbvg781;
    }

    public Movi getMovi() {
        return movi;
    }

    public Poli getPoli() {
        return poli;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public WkVariabiliLccs0023 getWkVariabili() {
        return wkVariabili;
    }

    public WmovTabMovi getWmovTabMovi(int idx) {
        return wmovTabMovi[idx - 1];
    }

    public WsFgAnnVag getWsFgAnnVag() {
        return wsFgAnnVag;
    }

    public WsFgDefaultGravita getWsFgDefaultGravita() {
        return wsFgDefaultGravita;
    }

    public WsFgMoviAnn getWsFgMoviAnn() {
        return wsFgMoviAnn;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpAnnullo getWsTpAnnullo() {
        return wsTpAnnullo;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PGM_IDBSPOL0 = 8;
        public static final int PGM_IDBSADE0 = 8;
        public static final int PGM_IDBSTGA0 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
