package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FLAG-RATALO<br>
 * Variable: WK-FLAG-RATALO from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFlagRatalo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFlagRatalo(char wkFlagRatalo) {
        this.value = wkFlagRatalo;
    }

    public char getWkFlagRatalo() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
