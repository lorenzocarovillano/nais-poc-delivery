package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: SPG-VAL-IMP<br>
 * Variable: SPG-VAL-IMP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SpgValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SpgValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SPG_VAL_IMP;
    }

    public void setSpgValImp(AfDecimal spgValImp) {
        writeDecimalAsPacked(Pos.SPG_VAL_IMP, spgValImp.copy());
    }

    public void setSpgValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SPG_VAL_IMP, Pos.SPG_VAL_IMP);
    }

    /**Original name: SPG-VAL-IMP<br>*/
    public AfDecimal getSpgValImp() {
        return readPackedAsDecimal(Pos.SPG_VAL_IMP, Len.Int.SPG_VAL_IMP, Len.Fract.SPG_VAL_IMP);
    }

    public byte[] getSpgValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SPG_VAL_IMP, Pos.SPG_VAL_IMP);
        return buffer;
    }

    public void setSpgValImpNull(String spgValImpNull) {
        writeString(Pos.SPG_VAL_IMP_NULL, spgValImpNull, Len.SPG_VAL_IMP_NULL);
    }

    /**Original name: SPG-VAL-IMP-NULL<br>*/
    public String getSpgValImpNull() {
        return readString(Pos.SPG_VAL_IMP_NULL, Len.SPG_VAL_IMP_NULL);
    }

    public String getSpgValImpNullFormatted() {
        return Functions.padBlanks(getSpgValImpNull(), Len.SPG_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SPG_VAL_IMP = 1;
        public static final int SPG_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SPG_VAL_IMP = 8;
        public static final int SPG_VAL_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SPG_VAL_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int SPG_VAL_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
