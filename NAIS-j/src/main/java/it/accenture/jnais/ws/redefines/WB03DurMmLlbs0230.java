package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-MM<br>
 * Variable: W-B03-DUR-MM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurMmLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurMmLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_MM;
    }

    public void setwB03DurMm(int wB03DurMm) {
        writeIntAsPacked(Pos.W_B03_DUR_MM, wB03DurMm, Len.Int.W_B03_DUR_MM);
    }

    /**Original name: W-B03-DUR-MM<br>*/
    public int getwB03DurMm() {
        return readPackedAsInt(Pos.W_B03_DUR_MM, Len.Int.W_B03_DUR_MM);
    }

    public byte[] getwB03DurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_MM, Pos.W_B03_DUR_MM);
        return buffer;
    }

    public void setwB03DurMmNull(String wB03DurMmNull) {
        writeString(Pos.W_B03_DUR_MM_NULL, wB03DurMmNull, Len.W_B03_DUR_MM_NULL);
    }

    /**Original name: W-B03-DUR-MM-NULL<br>*/
    public String getwB03DurMmNull() {
        return readString(Pos.W_B03_DUR_MM_NULL, Len.W_B03_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_MM = 1;
        public static final int W_B03_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_MM = 3;
        public static final int W_B03_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
