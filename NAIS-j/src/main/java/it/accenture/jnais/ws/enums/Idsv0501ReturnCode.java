package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0501-RETURN-CODE<br>
 * Variable: IDSV0501-RETURN-CODE from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0501ReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IDSV0501_RETURN_CODE);
    public static final String SUCCESSFUL_RC = "00";
    public static final String GENERIC_ERROR = "KO";

    //==== METHODS ====
    public void setIdsv0501ReturnCode(String idsv0501ReturnCode) {
        this.value = Functions.subString(idsv0501ReturnCode, Len.IDSV0501_RETURN_CODE);
    }

    public String getIdsv0501ReturnCode() {
        return this.value;
    }

    public boolean isIdsv0501SuccessfulRc() {
        return value.equals(SUCCESSFUL_RC);
    }

    public void setIdsv0501SuccessfulRc() {
        value = SUCCESSFUL_RC;
    }

    public void setIdsv0501GenericError() {
        value = GENERIC_ERROR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSV0501_RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
