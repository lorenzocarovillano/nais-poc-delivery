package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-NASC-CLI<br>
 * Variable: A25-DT-NASC-CLI from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25DtNascCli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25DtNascCli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT_NASC_CLI;
    }

    public void setA25DtNascCli(int a25DtNascCli) {
        writeIntAsPacked(Pos.A25_DT_NASC_CLI, a25DtNascCli, Len.Int.A25_DT_NASC_CLI);
    }

    public void setA25DtNascCliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT_NASC_CLI, Pos.A25_DT_NASC_CLI);
    }

    /**Original name: A25-DT-NASC-CLI<br>*/
    public int getA25DtNascCli() {
        return readPackedAsInt(Pos.A25_DT_NASC_CLI, Len.Int.A25_DT_NASC_CLI);
    }

    public byte[] getA25DtNascCliAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT_NASC_CLI, Pos.A25_DT_NASC_CLI);
        return buffer;
    }

    public void setA25DtNascCliNull(String a25DtNascCliNull) {
        writeString(Pos.A25_DT_NASC_CLI_NULL, a25DtNascCliNull, Len.A25_DT_NASC_CLI_NULL);
    }

    /**Original name: A25-DT-NASC-CLI-NULL<br>*/
    public String getA25DtNascCliNull() {
        return readString(Pos.A25_DT_NASC_CLI_NULL, Len.A25_DT_NASC_CLI_NULL);
    }

    public String getA25DtNascCliNullFormatted() {
        return Functions.padBlanks(getA25DtNascCliNull(), Len.A25_DT_NASC_CLI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT_NASC_CLI = 1;
        public static final int A25_DT_NASC_CLI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT_NASC_CLI = 5;
        public static final int A25_DT_NASC_CLI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT_NASC_CLI = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
