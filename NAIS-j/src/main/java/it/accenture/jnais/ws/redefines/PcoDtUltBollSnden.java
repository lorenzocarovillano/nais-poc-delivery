package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-SNDEN<br>
 * Variable: PCO-DT-ULT-BOLL-SNDEN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollSnden extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollSnden() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_SNDEN;
    }

    public void setPcoDtUltBollSnden(int pcoDtUltBollSnden) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_SNDEN, pcoDtUltBollSnden, Len.Int.PCO_DT_ULT_BOLL_SNDEN);
    }

    public void setPcoDtUltBollSndenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SNDEN, Pos.PCO_DT_ULT_BOLL_SNDEN);
    }

    /**Original name: PCO-DT-ULT-BOLL-SNDEN<br>*/
    public int getPcoDtUltBollSnden() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_SNDEN, Len.Int.PCO_DT_ULT_BOLL_SNDEN);
    }

    public byte[] getPcoDtUltBollSndenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SNDEN, Pos.PCO_DT_ULT_BOLL_SNDEN);
        return buffer;
    }

    public void initPcoDtUltBollSndenHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_SNDEN, Len.PCO_DT_ULT_BOLL_SNDEN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollSndenNull(String pcoDtUltBollSndenNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_SNDEN_NULL, pcoDtUltBollSndenNull, Len.PCO_DT_ULT_BOLL_SNDEN_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-SNDEN-NULL<br>*/
    public String getPcoDtUltBollSndenNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_SNDEN_NULL, Len.PCO_DT_ULT_BOLL_SNDEN_NULL);
    }

    public String getPcoDtUltBollSndenNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollSndenNull(), Len.PCO_DT_ULT_BOLL_SNDEN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SNDEN = 1;
        public static final int PCO_DT_ULT_BOLL_SNDEN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SNDEN = 5;
        public static final int PCO_DT_ULT_BOLL_SNDEN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_SNDEN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
