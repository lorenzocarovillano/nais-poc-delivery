package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-TFR-STRC<br>
 * Variable: L3421-IMP-TFR-STRC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_TFR_STRC;
    }

    public void setL3421ImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_TFR_STRC, Pos.L3421_IMP_TFR_STRC);
    }

    /**Original name: L3421-IMP-TFR-STRC<br>*/
    public AfDecimal getL3421ImpTfrStrc() {
        return readPackedAsDecimal(Pos.L3421_IMP_TFR_STRC, Len.Int.L3421_IMP_TFR_STRC, Len.Fract.L3421_IMP_TFR_STRC);
    }

    public byte[] getL3421ImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_TFR_STRC, Pos.L3421_IMP_TFR_STRC);
        return buffer;
    }

    /**Original name: L3421-IMP-TFR-STRC-NULL<br>*/
    public String getL3421ImpTfrStrcNull() {
        return readString(Pos.L3421_IMP_TFR_STRC_NULL, Len.L3421_IMP_TFR_STRC_NULL);
    }

    public String getL3421ImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getL3421ImpTfrStrcNull(), Len.L3421_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_TFR_STRC = 1;
        public static final int L3421_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_TFR_STRC = 8;
        public static final int L3421_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
