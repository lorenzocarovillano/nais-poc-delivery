package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-RENDTO-LRD-END2006<br>
 * Variable: P61-RENDTO-LRD-END2006 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61RendtoLrdEnd2006 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61RendtoLrdEnd2006() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_RENDTO_LRD_END2006;
    }

    public void setP61RendtoLrdEnd2006(AfDecimal p61RendtoLrdEnd2006) {
        writeDecimalAsPacked(Pos.P61_RENDTO_LRD_END2006, p61RendtoLrdEnd2006.copy());
    }

    public void setP61RendtoLrdEnd2006FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_RENDTO_LRD_END2006, Pos.P61_RENDTO_LRD_END2006);
    }

    /**Original name: P61-RENDTO-LRD-END2006<br>*/
    public AfDecimal getP61RendtoLrdEnd2006() {
        return readPackedAsDecimal(Pos.P61_RENDTO_LRD_END2006, Len.Int.P61_RENDTO_LRD_END2006, Len.Fract.P61_RENDTO_LRD_END2006);
    }

    public byte[] getP61RendtoLrdEnd2006AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_RENDTO_LRD_END2006, Pos.P61_RENDTO_LRD_END2006);
        return buffer;
    }

    public void setP61RendtoLrdEnd2006Null(String p61RendtoLrdEnd2006Null) {
        writeString(Pos.P61_RENDTO_LRD_END2006_NULL, p61RendtoLrdEnd2006Null, Len.P61_RENDTO_LRD_END2006_NULL);
    }

    /**Original name: P61-RENDTO-LRD-END2006-NULL<br>*/
    public String getP61RendtoLrdEnd2006Null() {
        return readString(Pos.P61_RENDTO_LRD_END2006_NULL, Len.P61_RENDTO_LRD_END2006_NULL);
    }

    public String getP61RendtoLrdEnd2006NullFormatted() {
        return Functions.padBlanks(getP61RendtoLrdEnd2006Null(), Len.P61_RENDTO_LRD_END2006_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_RENDTO_LRD_END2006 = 1;
        public static final int P61_RENDTO_LRD_END2006_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_RENDTO_LRD_END2006 = 8;
        public static final int P61_RENDTO_LRD_END2006_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_RENDTO_LRD_END2006 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_RENDTO_LRD_END2006 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
