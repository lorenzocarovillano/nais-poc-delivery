package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-SCAD-TRCH<br>
 * Variable: B03-DT-SCAD-TRCH from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtScadTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtScadTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_SCAD_TRCH;
    }

    public void setB03DtScadTrch(int b03DtScadTrch) {
        writeIntAsPacked(Pos.B03_DT_SCAD_TRCH, b03DtScadTrch, Len.Int.B03_DT_SCAD_TRCH);
    }

    public void setB03DtScadTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_SCAD_TRCH, Pos.B03_DT_SCAD_TRCH);
    }

    /**Original name: B03-DT-SCAD-TRCH<br>*/
    public int getB03DtScadTrch() {
        return readPackedAsInt(Pos.B03_DT_SCAD_TRCH, Len.Int.B03_DT_SCAD_TRCH);
    }

    public byte[] getB03DtScadTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_SCAD_TRCH, Pos.B03_DT_SCAD_TRCH);
        return buffer;
    }

    public void setB03DtScadTrchNull(String b03DtScadTrchNull) {
        writeString(Pos.B03_DT_SCAD_TRCH_NULL, b03DtScadTrchNull, Len.B03_DT_SCAD_TRCH_NULL);
    }

    /**Original name: B03-DT-SCAD-TRCH-NULL<br>*/
    public String getB03DtScadTrchNull() {
        return readString(Pos.B03_DT_SCAD_TRCH_NULL, Len.B03_DT_SCAD_TRCH_NULL);
    }

    public String getB03DtScadTrchNullFormatted() {
        return Functions.padBlanks(getB03DtScadTrchNull(), Len.B03_DT_SCAD_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_SCAD_TRCH = 1;
        public static final int B03_DT_SCAD_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_SCAD_TRCH = 5;
        public static final int B03_DT_SCAD_TRCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_SCAD_TRCH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
