package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-SOST-EFFLQ<br>
 * Variable: WDFL-ACCPRE-SOST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreSostEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreSostEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_SOST_EFFLQ;
    }

    public void setWdflAccpreSostEfflq(AfDecimal wdflAccpreSostEfflq) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_SOST_EFFLQ, wdflAccpreSostEfflq.copy());
    }

    public void setWdflAccpreSostEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_SOST_EFFLQ, Pos.WDFL_ACCPRE_SOST_EFFLQ);
    }

    /**Original name: WDFL-ACCPRE-SOST-EFFLQ<br>*/
    public AfDecimal getWdflAccpreSostEfflq() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_SOST_EFFLQ, Len.Int.WDFL_ACCPRE_SOST_EFFLQ, Len.Fract.WDFL_ACCPRE_SOST_EFFLQ);
    }

    public byte[] getWdflAccpreSostEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_SOST_EFFLQ, Pos.WDFL_ACCPRE_SOST_EFFLQ);
        return buffer;
    }

    public void setWdflAccpreSostEfflqNull(String wdflAccpreSostEfflqNull) {
        writeString(Pos.WDFL_ACCPRE_SOST_EFFLQ_NULL, wdflAccpreSostEfflqNull, Len.WDFL_ACCPRE_SOST_EFFLQ_NULL);
    }

    /**Original name: WDFL-ACCPRE-SOST-EFFLQ-NULL<br>*/
    public String getWdflAccpreSostEfflqNull() {
        return readString(Pos.WDFL_ACCPRE_SOST_EFFLQ_NULL, Len.WDFL_ACCPRE_SOST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_SOST_EFFLQ = 1;
        public static final int WDFL_ACCPRE_SOST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_SOST_EFFLQ = 8;
        public static final int WDFL_ACCPRE_SOST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_SOST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_SOST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
