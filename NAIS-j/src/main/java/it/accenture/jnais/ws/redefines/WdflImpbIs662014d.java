package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-662014D<br>
 * Variable: WDFL-IMPB-IS-662014D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIs662014d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIs662014d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS662014D;
    }

    public void setWdflImpbIs662014d(AfDecimal wdflImpbIs662014d) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS662014D, wdflImpbIs662014d.copy());
    }

    public void setWdflImpbIs662014dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS662014D, Pos.WDFL_IMPB_IS662014D);
    }

    /**Original name: WDFL-IMPB-IS-662014D<br>*/
    public AfDecimal getWdflImpbIs662014d() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS662014D, Len.Int.WDFL_IMPB_IS662014D, Len.Fract.WDFL_IMPB_IS662014D);
    }

    public byte[] getWdflImpbIs662014dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS662014D, Pos.WDFL_IMPB_IS662014D);
        return buffer;
    }

    public void setWdflImpbIs662014dNull(String wdflImpbIs662014dNull) {
        writeString(Pos.WDFL_IMPB_IS662014D_NULL, wdflImpbIs662014dNull, Len.WDFL_IMPB_IS662014D_NULL);
    }

    /**Original name: WDFL-IMPB-IS-662014D-NULL<br>*/
    public String getWdflImpbIs662014dNull() {
        return readString(Pos.WDFL_IMPB_IS662014D_NULL, Len.WDFL_IMPB_IS662014D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS662014D = 1;
        public static final int WDFL_IMPB_IS662014D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS662014D = 8;
        public static final int WDFL_IMPB_IS662014D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS662014D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS662014D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
