package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-662014L<br>
 * Variable: WDFL-IMPB-IS-662014L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIs662014l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIs662014l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS662014L;
    }

    public void setWdflImpbIs662014l(AfDecimal wdflImpbIs662014l) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS662014L, wdflImpbIs662014l.copy());
    }

    public void setWdflImpbIs662014lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS662014L, Pos.WDFL_IMPB_IS662014L);
    }

    /**Original name: WDFL-IMPB-IS-662014L<br>*/
    public AfDecimal getWdflImpbIs662014l() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS662014L, Len.Int.WDFL_IMPB_IS662014L, Len.Fract.WDFL_IMPB_IS662014L);
    }

    public byte[] getWdflImpbIs662014lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS662014L, Pos.WDFL_IMPB_IS662014L);
        return buffer;
    }

    public void setWdflImpbIs662014lNull(String wdflImpbIs662014lNull) {
        writeString(Pos.WDFL_IMPB_IS662014L_NULL, wdflImpbIs662014lNull, Len.WDFL_IMPB_IS662014L_NULL);
    }

    /**Original name: WDFL-IMPB-IS-662014L-NULL<br>*/
    public String getWdflImpbIs662014lNull() {
        return readString(Pos.WDFL_IMPB_IS662014L_NULL, Len.WDFL_IMPB_IS662014L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS662014L = 1;
        public static final int WDFL_IMPB_IS662014L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS662014L = 8;
        public static final int WDFL_IMPB_IS662014L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS662014L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS662014L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
