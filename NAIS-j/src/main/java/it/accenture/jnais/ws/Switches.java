package it.accenture.jnais.ws;

import it.accenture.jnais.ws.enums.FineFetch;
import it.accenture.jnais.ws.enums.FlGarRamoIii;
import it.accenture.jnais.ws.enums.Sw6002;
import it.accenture.jnais.ws.enums.Sw6003;
import it.accenture.jnais.ws.enums.Sw6009;
import it.accenture.jnais.ws.enums.Sw6101;
import it.accenture.jnais.ws.enums.SwCurVas;
import it.accenture.jnais.ws.enums.SwDfa;
import it.accenture.jnais.ws.enums.SwDir;
import it.accenture.jnais.ws.enums.SwDtc1;
import it.accenture.jnais.ws.enums.SwDtc2;
import it.accenture.jnais.ws.enums.SwFineDisin;
import it.accenture.jnais.ws.enums.SwFineGrz;
import it.accenture.jnais.ws.enums.SwFineMovi;
import it.accenture.jnais.ws.enums.SwFineTga;
import it.accenture.jnais.ws.enums.SwGarIns;
import it.accenture.jnais.ws.enums.SwPog;
import it.accenture.jnais.ws.enums.SwRamoRich;
import it.accenture.jnais.ws.enums.SwRiscatt;
import it.accenture.jnais.ws.enums.SwSaveTranche;
import it.accenture.jnais.ws.enums.SwScartaGar;
import it.accenture.jnais.ws.enums.SwScartaTga;
import it.accenture.jnais.ws.enums.SwStAdes;
import it.accenture.jnais.ws.enums.SwStb;
import it.accenture.jnais.ws.enums.SwStGar;
import it.accenture.jnais.ws.enums.SwTab;
import it.accenture.jnais.ws.enums.SwTranche;
import it.accenture.jnais.ws.enums.SwTrovato;
import it.accenture.jnais.ws.enums.SwValAst;
import it.accenture.jnais.ws.enums.SwZeroOpzioni;

/**Original name: SWITCHES<br>
 * Variable: SWITCHES from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Switches {

    //==== PROPERTIES ====
    //Original name: SW-SCARTA-GAR
    private SwScartaGar swScartaGar = new SwScartaGar();
    //Original name: SW-SCARTA-TGA
    private SwScartaTga swScartaTga = new SwScartaTga();
    //Original name: SW-FINE-GRZ
    private SwFineGrz swFineGrz = new SwFineGrz();
    //Original name: SW-FINE-TGA
    private SwFineTga swFineTga = new SwFineTga();
    //Original name: SW-RAMO-RICH
    private SwRamoRich swRamoRich = new SwRamoRich();
    //Original name: SW-TROVATO
    private SwTrovato swTrovato = new SwTrovato();
    //Original name: SW-6101
    private Sw6101 sw6101 = new Sw6101();
    //Original name: SW-6002
    private Sw6002 sw6002 = new Sw6002();
    //Original name: SW-6003
    private Sw6003 sw6003 = new Sw6003();
    //Original name: SW-6009
    private Sw6009 sw6009 = new Sw6009();
    //Original name: SW-TAB
    private SwTab swTab = new SwTab();
    //Original name: SW-DFA
    private SwDfa swDfa = new SwDfa();
    //Original name: SW-RISCATT
    private SwRiscatt swRiscatt = new SwRiscatt();
    //Original name: SW-DTC1
    private SwDtc1 swDtc1 = new SwDtc1();
    /**Original name: SW-DTC2<br>
	 * <pre>       &#x1c;262 -->  data esito titolo</pre>*/
    private SwDtc2 swDtc2 = new SwDtc2();
    /**Original name: SW-STB<br>
	 * <pre>       &#x1c;262 -->  data esito titolo</pre>*/
    private SwStb swStb = new SwStb();
    //Original name: SW-TRANCHE
    private SwTranche swTranche = new SwTranche();
    //Original name: SW-ST-ADES
    private SwStAdes swStAdes = new SwStAdes();
    //Original name: SW-ST-GAR
    private SwStGar swStGar = new SwStGar();
    //Original name: SW-POG
    private SwPog swPog = new SwPog();
    //Original name: FL-GAR-RAMO-III
    private FlGarRamoIii flGarRamoIii = new FlGarRamoIii();
    //Original name: SW-FINE-DISIN
    private SwFineDisin swFineDisin = new SwFineDisin();
    //Original name: SW-FINE-MOVI
    private SwFineMovi swFineMovi = new SwFineMovi();
    //Original name: SW-SAVE-TRANCHE
    private SwSaveTranche swSaveTranche = new SwSaveTranche();
    //Original name: SW-GAR-INS
    private SwGarIns swGarIns = new SwGarIns();
    //Original name: SW-VAL-AST
    private SwValAst swValAst = new SwValAst();
    //Original name: SW-CUR-VAS
    private SwCurVas swCurVas = new SwCurVas();
    //Original name: SW-ZERO-OPZIONI
    private SwZeroOpzioni swZeroOpzioni = new SwZeroOpzioni();
    /**Original name: SW-DIR<br>
	 * <pre>       &#x1c;261 -->  Costi di emissione</pre>*/
    private SwDir swDir = new SwDir();
    /**Original name: FINE-FETCH<br>
	 * <pre>       &#x1c;261 -->  Costi di emissione
	 *  FETCH RAPP-ANA</pre>*/
    private FineFetch fineFetch = new FineFetch();

    //==== METHODS ====
    public FineFetch getFineFetch() {
        return fineFetch;
    }

    public FlGarRamoIii getFlGarRamoIii() {
        return flGarRamoIii;
    }

    public Sw6002 getSw6002() {
        return sw6002;
    }

    public Sw6003 getSw6003() {
        return sw6003;
    }

    public Sw6009 getSw6009() {
        return sw6009;
    }

    public Sw6101 getSw6101() {
        return sw6101;
    }

    public SwCurVas getSwCurVas() {
        return swCurVas;
    }

    public SwDfa getSwDfa() {
        return swDfa;
    }

    public SwDir getSwDir() {
        return swDir;
    }

    public SwDtc1 getSwDtc1() {
        return swDtc1;
    }

    public SwDtc2 getSwDtc2() {
        return swDtc2;
    }

    public SwFineDisin getSwFineDisin() {
        return swFineDisin;
    }

    public SwFineGrz getSwFineGrz() {
        return swFineGrz;
    }

    public SwFineMovi getSwFineMovi() {
        return swFineMovi;
    }

    public SwFineTga getSwFineTga() {
        return swFineTga;
    }

    public SwGarIns getSwGarIns() {
        return swGarIns;
    }

    public SwPog getSwPog() {
        return swPog;
    }

    public SwRamoRich getSwRamoRich() {
        return swRamoRich;
    }

    public SwRiscatt getSwRiscatt() {
        return swRiscatt;
    }

    public SwSaveTranche getSwSaveTranche() {
        return swSaveTranche;
    }

    public SwScartaGar getSwScartaGar() {
        return swScartaGar;
    }

    public SwScartaTga getSwScartaTga() {
        return swScartaTga;
    }

    public SwStAdes getSwStAdes() {
        return swStAdes;
    }

    public SwStGar getSwStGar() {
        return swStGar;
    }

    public SwStb getSwStb() {
        return swStb;
    }

    public SwTab getSwTab() {
        return swTab;
    }

    public SwTranche getSwTranche() {
        return swTranche;
    }

    public SwTrovato getSwTrovato() {
        return swTrovato;
    }

    public SwValAst getSwValAst() {
        return swValAst;
    }

    public SwZeroOpzioni getSwZeroOpzioni() {
        return swZeroOpzioni;
    }
}
