package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNDE-DAL2007-CALC<br>
 * Variable: WDFL-CNDE-DAL2007-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCndeDal2007Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCndeDal2007Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNDE_DAL2007_CALC;
    }

    public void setWdflCndeDal2007Calc(AfDecimal wdflCndeDal2007Calc) {
        writeDecimalAsPacked(Pos.WDFL_CNDE_DAL2007_CALC, wdflCndeDal2007Calc.copy());
    }

    public void setWdflCndeDal2007CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNDE_DAL2007_CALC, Pos.WDFL_CNDE_DAL2007_CALC);
    }

    /**Original name: WDFL-CNDE-DAL2007-CALC<br>*/
    public AfDecimal getWdflCndeDal2007Calc() {
        return readPackedAsDecimal(Pos.WDFL_CNDE_DAL2007_CALC, Len.Int.WDFL_CNDE_DAL2007_CALC, Len.Fract.WDFL_CNDE_DAL2007_CALC);
    }

    public byte[] getWdflCndeDal2007CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNDE_DAL2007_CALC, Pos.WDFL_CNDE_DAL2007_CALC);
        return buffer;
    }

    public void setWdflCndeDal2007CalcNull(String wdflCndeDal2007CalcNull) {
        writeString(Pos.WDFL_CNDE_DAL2007_CALC_NULL, wdflCndeDal2007CalcNull, Len.WDFL_CNDE_DAL2007_CALC_NULL);
    }

    /**Original name: WDFL-CNDE-DAL2007-CALC-NULL<br>*/
    public String getWdflCndeDal2007CalcNull() {
        return readString(Pos.WDFL_CNDE_DAL2007_CALC_NULL, Len.WDFL_CNDE_DAL2007_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_DAL2007_CALC = 1;
        public static final int WDFL_CNDE_DAL2007_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_DAL2007_CALC = 8;
        public static final int WDFL_CNDE_DAL2007_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_DAL2007_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_DAL2007_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
