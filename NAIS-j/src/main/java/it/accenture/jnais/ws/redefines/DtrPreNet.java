package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PRE-NET<br>
 * Variable: DTR-PRE-NET from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PRE_NET;
    }

    public void setDtrPreNet(AfDecimal dtrPreNet) {
        writeDecimalAsPacked(Pos.DTR_PRE_NET, dtrPreNet.copy());
    }

    public void setDtrPreNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PRE_NET, Pos.DTR_PRE_NET);
    }

    /**Original name: DTR-PRE-NET<br>*/
    public AfDecimal getDtrPreNet() {
        return readPackedAsDecimal(Pos.DTR_PRE_NET, Len.Int.DTR_PRE_NET, Len.Fract.DTR_PRE_NET);
    }

    public byte[] getDtrPreNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PRE_NET, Pos.DTR_PRE_NET);
        return buffer;
    }

    public void setDtrPreNetNull(String dtrPreNetNull) {
        writeString(Pos.DTR_PRE_NET_NULL, dtrPreNetNull, Len.DTR_PRE_NET_NULL);
    }

    /**Original name: DTR-PRE-NET-NULL<br>*/
    public String getDtrPreNetNull() {
        return readString(Pos.DTR_PRE_NET_NULL, Len.DTR_PRE_NET_NULL);
    }

    public String getDtrPreNetNullFormatted() {
        return Functions.padBlanks(getDtrPreNetNull(), Len.DTR_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PRE_NET = 1;
        public static final int DTR_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PRE_NET = 8;
        public static final int DTR_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
