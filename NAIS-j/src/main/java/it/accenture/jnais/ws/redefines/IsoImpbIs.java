package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-IMPB-IS<br>
 * Variable: ISO-IMPB-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoImpbIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoImpbIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_IMPB_IS;
    }

    public void setIsoImpbIs(AfDecimal isoImpbIs) {
        writeDecimalAsPacked(Pos.ISO_IMPB_IS, isoImpbIs.copy());
    }

    public void setIsoImpbIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_IMPB_IS, Pos.ISO_IMPB_IS);
    }

    /**Original name: ISO-IMPB-IS<br>*/
    public AfDecimal getIsoImpbIs() {
        return readPackedAsDecimal(Pos.ISO_IMPB_IS, Len.Int.ISO_IMPB_IS, Len.Fract.ISO_IMPB_IS);
    }

    public byte[] getIsoImpbIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_IMPB_IS, Pos.ISO_IMPB_IS);
        return buffer;
    }

    public void setIsoImpbIsNull(String isoImpbIsNull) {
        writeString(Pos.ISO_IMPB_IS_NULL, isoImpbIsNull, Len.ISO_IMPB_IS_NULL);
    }

    /**Original name: ISO-IMPB-IS-NULL<br>*/
    public String getIsoImpbIsNull() {
        return readString(Pos.ISO_IMPB_IS_NULL, Len.ISO_IMPB_IS_NULL);
    }

    public String getIsoImpbIsNullFormatted() {
        return Functions.padBlanks(getIsoImpbIsNull(), Len.ISO_IMPB_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_IMPB_IS = 1;
        public static final int ISO_IMPB_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_IMPB_IS = 8;
        public static final int ISO_IMPB_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_IMPB_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_IMPB_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
