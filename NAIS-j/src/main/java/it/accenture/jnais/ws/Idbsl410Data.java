package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSL410<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsl410Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-L41-DESC-AGG
    private short indL41DescAgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: L41-DT-QTZ-DB
    private String l41DtQtzDb = DefaultValues.stringVal(Len.L41_DT_QTZ_DB);

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndL41DescAgg(short indL41DescAgg) {
        this.indL41DescAgg = indL41DescAgg;
    }

    public short getIndL41DescAgg() {
        return this.indL41DescAgg;
    }

    public void setL41DtQtzDb(String l41DtQtzDb) {
        this.l41DtQtzDb = Functions.subString(l41DtQtzDb, Len.L41_DT_QTZ_DB);
    }

    public String getL41DtQtzDb() {
        return this.l41DtQtzDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L41_DT_QTZ_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
