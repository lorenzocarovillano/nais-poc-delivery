package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ID-MOVI-CHIU<br>
 * Variable: TGA-ID-MOVI-CHIU from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ID_MOVI_CHIU;
    }

    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        writeIntAsPacked(Pos.TGA_ID_MOVI_CHIU, tgaIdMoviChiu, Len.Int.TGA_ID_MOVI_CHIU);
    }

    public void setTgaIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ID_MOVI_CHIU, Pos.TGA_ID_MOVI_CHIU);
    }

    /**Original name: TGA-ID-MOVI-CHIU<br>*/
    public int getTgaIdMoviChiu() {
        return readPackedAsInt(Pos.TGA_ID_MOVI_CHIU, Len.Int.TGA_ID_MOVI_CHIU);
    }

    public byte[] getTgaIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ID_MOVI_CHIU, Pos.TGA_ID_MOVI_CHIU);
        return buffer;
    }

    public void setTgaIdMoviChiuNull(String tgaIdMoviChiuNull) {
        writeString(Pos.TGA_ID_MOVI_CHIU_NULL, tgaIdMoviChiuNull, Len.TGA_ID_MOVI_CHIU_NULL);
    }

    /**Original name: TGA-ID-MOVI-CHIU-NULL<br>*/
    public String getTgaIdMoviChiuNull() {
        return readString(Pos.TGA_ID_MOVI_CHIU_NULL, Len.TGA_ID_MOVI_CHIU_NULL);
    }

    public String getTgaIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getTgaIdMoviChiuNull(), Len.TGA_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ID_MOVI_CHIU = 1;
        public static final int TGA_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ID_MOVI_CHIU = 5;
        public static final int TGA_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
