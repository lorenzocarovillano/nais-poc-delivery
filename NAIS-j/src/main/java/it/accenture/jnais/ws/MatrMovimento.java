package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: MATR-MOVIMENTO<br>
 * Variable: MATR-MOVIMENTO from copybook IDBVMMO1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class MatrMovimento extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: MMO-ID-MATR-MOVIMENTO
    private int idMatrMovimento = DefaultValues.BIN_INT_VAL;
    //Original name: MMO-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: MMO-TP-MOVI-PTF
    private int tpMoviPtf = DefaultValues.INT_VAL;
    //Original name: MMO-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: MMO-TP-FRM-ASSVA
    private String tpFrmAssva = DefaultValues.stringVal(Len.TP_FRM_ASSVA);
    //Original name: MMO-TP-MOVI-ACT
    private String tpMoviAct = DefaultValues.stringVal(Len.TP_MOVI_ACT);
    //Original name: MMO-AMMISSIBILITA-MOVI
    private char ammissibilitaMovi = DefaultValues.CHAR_VAL;
    //Original name: MMO-SERVIZIO-CONTROLLO
    private String servizioControllo = DefaultValues.stringVal(Len.SERVIZIO_CONTROLLO);
    //Original name: MMO-COD-PROCESSO-WF
    private String codProcessoWf = DefaultValues.stringVal(Len.COD_PROCESSO_WF);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MATR_MOVIMENTO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMatrMovimentoBytes(buf);
    }

    public String getMatrMovimentoFormatted() {
        return MarshalByteExt.bufferToStr(getMatrMovimentoBytes());
    }

    public void setMatrMovimentoBytes(byte[] buffer) {
        setMatrMovimentoBytes(buffer, 1);
    }

    public byte[] getMatrMovimentoBytes() {
        byte[] buffer = new byte[Len.MATR_MOVIMENTO];
        return getMatrMovimentoBytes(buffer, 1);
    }

    public void setMatrMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        idMatrMovimento = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpMoviPtf = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI_PTF, 0);
        position += Len.TP_MOVI_PTF;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpFrmAssva = MarshalByte.readString(buffer, position, Len.TP_FRM_ASSVA);
        position += Len.TP_FRM_ASSVA;
        tpMoviAct = MarshalByte.readString(buffer, position, Len.TP_MOVI_ACT);
        position += Len.TP_MOVI_ACT;
        ammissibilitaMovi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        servizioControllo = MarshalByte.readString(buffer, position, Len.SERVIZIO_CONTROLLO);
        position += Len.SERVIZIO_CONTROLLO;
        codProcessoWf = MarshalByte.readString(buffer, position, Len.COD_PROCESSO_WF);
    }

    public byte[] getMatrMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, idMatrMovimento);
        position += Types.INT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, tpMoviPtf, Len.Int.TP_MOVI_PTF, 0);
        position += Len.TP_MOVI_PTF;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpFrmAssva, Len.TP_FRM_ASSVA);
        position += Len.TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, tpMoviAct, Len.TP_MOVI_ACT);
        position += Len.TP_MOVI_ACT;
        MarshalByte.writeChar(buffer, position, ammissibilitaMovi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, servizioControllo, Len.SERVIZIO_CONTROLLO);
        position += Len.SERVIZIO_CONTROLLO;
        MarshalByte.writeString(buffer, position, codProcessoWf, Len.COD_PROCESSO_WF);
        return buffer;
    }

    public void setIdMatrMovimento(int idMatrMovimento) {
        this.idMatrMovimento = idMatrMovimento;
    }

    public int getIdMatrMovimento() {
        return this.idMatrMovimento;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpMoviPtf(int tpMoviPtf) {
        this.tpMoviPtf = tpMoviPtf;
    }

    public int getTpMoviPtf() {
        return this.tpMoviPtf;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setTpFrmAssva(String tpFrmAssva) {
        this.tpFrmAssva = Functions.subString(tpFrmAssva, Len.TP_FRM_ASSVA);
    }

    public String getTpFrmAssva() {
        return this.tpFrmAssva;
    }

    public void setTpMoviAct(String tpMoviAct) {
        this.tpMoviAct = Functions.subString(tpMoviAct, Len.TP_MOVI_ACT);
    }

    public String getTpMoviAct() {
        return this.tpMoviAct;
    }

    public void setAmmissibilitaMovi(char ammissibilitaMovi) {
        this.ammissibilitaMovi = ammissibilitaMovi;
    }

    public char getAmmissibilitaMovi() {
        return this.ammissibilitaMovi;
    }

    public void setServizioControllo(String servizioControllo) {
        this.servizioControllo = Functions.subString(servizioControllo, Len.SERVIZIO_CONTROLLO);
    }

    public String getServizioControllo() {
        return this.servizioControllo;
    }

    public void setCodProcessoWf(String codProcessoWf) {
        this.codProcessoWf = Functions.subString(codProcessoWf, Len.COD_PROCESSO_WF);
    }

    public String getCodProcessoWf() {
        return this.codProcessoWf;
    }

    @Override
    public byte[] serialize() {
        return getMatrMovimentoBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_MATR_MOVIMENTO = 4;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_MOVI_PTF = 3;
        public static final int TP_OGG = 2;
        public static final int TP_FRM_ASSVA = 2;
        public static final int TP_MOVI_ACT = 5;
        public static final int AMMISSIBILITA_MOVI = 1;
        public static final int SERVIZIO_CONTROLLO = 8;
        public static final int COD_PROCESSO_WF = 3;
        public static final int MATR_MOVIMENTO = ID_MATR_MOVIMENTO + COD_COMP_ANIA + TP_MOVI_PTF + TP_OGG + TP_FRM_ASSVA + TP_MOVI_ACT + AMMISSIBILITA_MOVI + SERVIZIO_CONTROLLO + COD_PROCESSO_WF;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int TP_MOVI_PTF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
