package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E12-ID-MOVI-CHIU<br>
 * Variable: E12-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E12IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E12IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E12_ID_MOVI_CHIU;
    }

    public void setE12IdMoviChiu(int e12IdMoviChiu) {
        writeIntAsPacked(Pos.E12_ID_MOVI_CHIU, e12IdMoviChiu, Len.Int.E12_ID_MOVI_CHIU);
    }

    public void setE12IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E12_ID_MOVI_CHIU, Pos.E12_ID_MOVI_CHIU);
    }

    /**Original name: E12-ID-MOVI-CHIU<br>*/
    public int getE12IdMoviChiu() {
        return readPackedAsInt(Pos.E12_ID_MOVI_CHIU, Len.Int.E12_ID_MOVI_CHIU);
    }

    public byte[] getE12IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E12_ID_MOVI_CHIU, Pos.E12_ID_MOVI_CHIU);
        return buffer;
    }

    public void setE12IdMoviChiuNull(String e12IdMoviChiuNull) {
        writeString(Pos.E12_ID_MOVI_CHIU_NULL, e12IdMoviChiuNull, Len.E12_ID_MOVI_CHIU_NULL);
    }

    /**Original name: E12-ID-MOVI-CHIU-NULL<br>*/
    public String getE12IdMoviChiuNull() {
        return readString(Pos.E12_ID_MOVI_CHIU_NULL, Len.E12_ID_MOVI_CHIU_NULL);
    }

    public String getE12IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getE12IdMoviChiuNull(), Len.E12_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E12_ID_MOVI_CHIU = 1;
        public static final int E12_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E12_ID_MOVI_CHIU = 5;
        public static final int E12_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E12_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
