package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P67Amm1aRat;
import it.accenture.jnais.ws.redefines.P67AmmRatEnd;
import it.accenture.jnais.ws.redefines.P67CptFin;
import it.accenture.jnais.ws.redefines.P67Dt1oUtlzCRev;
import it.accenture.jnais.ws.redefines.P67DtEndFinanz;
import it.accenture.jnais.ws.redefines.P67DtErogFinanz;
import it.accenture.jnais.ws.redefines.P67DtEstFinanz;
import it.accenture.jnais.ws.redefines.P67DtManCop;
import it.accenture.jnais.ws.redefines.P67DtScad1aRat;
import it.accenture.jnais.ws.redefines.P67DtScadCop;
import it.accenture.jnais.ws.redefines.P67DtStipulaFinanz;
import it.accenture.jnais.ws.redefines.P67DurMmFinanz;
import it.accenture.jnais.ws.redefines.P67GgDelMmScadRat;
import it.accenture.jnais.ws.redefines.P67IdMoviChiu;
import it.accenture.jnais.ws.redefines.P67ImpAssto;
import it.accenture.jnais.ws.redefines.P67ImpCanoneAntic;
import it.accenture.jnais.ws.redefines.P67ImpDebRes;
import it.accenture.jnais.ws.redefines.P67ImpFinRevolving;
import it.accenture.jnais.ws.redefines.P67ImpRatFinanz;
import it.accenture.jnais.ws.redefines.P67ImpRatRevolving;
import it.accenture.jnais.ws.redefines.P67ImpUtilCRev;
import it.accenture.jnais.ws.redefines.P67MmPreamm;
import it.accenture.jnais.ws.redefines.P67NumTstFin;
import it.accenture.jnais.ws.redefines.P67PerRatFinanz;
import it.accenture.jnais.ws.redefines.P67PreVers;
import it.accenture.jnais.ws.redefines.P67TsCreRatFinanz;
import it.accenture.jnais.ws.redefines.P67TsFinanz;
import it.accenture.jnais.ws.redefines.P67ValRiscBene;
import it.accenture.jnais.ws.redefines.P67ValRiscEndLeas;

/**Original name: EST-POLI-CPI-PR<br>
 * Variable: EST-POLI-CPI-PR from copybook IDBVP671<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class EstPoliCpiPrIdbsp670 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P67-ID-EST-POLI-CPI-PR
    private int p67IdEstPoliCpiPr = DefaultValues.INT_VAL;
    //Original name: P67-ID-MOVI-CRZ
    private int p67IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P67-ID-MOVI-CHIU
    private P67IdMoviChiu p67IdMoviChiu = new P67IdMoviChiu();
    //Original name: P67-DT-INI-EFF
    private int p67DtIniEff = DefaultValues.INT_VAL;
    //Original name: P67-DT-END-EFF
    private int p67DtEndEff = DefaultValues.INT_VAL;
    //Original name: P67-COD-COMP-ANIA
    private int p67CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P67-IB-OGG
    private String p67IbOgg = DefaultValues.stringVal(Len.P67_IB_OGG);
    //Original name: P67-DS-RIGA
    private long p67DsRiga = DefaultValues.LONG_VAL;
    //Original name: P67-DS-OPER-SQL
    private char p67DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P67-DS-VER
    private int p67DsVer = DefaultValues.INT_VAL;
    //Original name: P67-DS-TS-INI-CPTZ
    private long p67DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: P67-DS-TS-END-CPTZ
    private long p67DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: P67-DS-UTENTE
    private String p67DsUtente = DefaultValues.stringVal(Len.P67_DS_UTENTE);
    //Original name: P67-DS-STATO-ELAB
    private char p67DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P67-COD-PROD-ESTNO
    private String p67CodProdEstno = DefaultValues.stringVal(Len.P67_COD_PROD_ESTNO);
    //Original name: P67-CPT-FIN
    private P67CptFin p67CptFin = new P67CptFin();
    //Original name: P67-NUM-TST-FIN
    private P67NumTstFin p67NumTstFin = new P67NumTstFin();
    //Original name: P67-TS-FINANZ
    private P67TsFinanz p67TsFinanz = new P67TsFinanz();
    //Original name: P67-DUR-MM-FINANZ
    private P67DurMmFinanz p67DurMmFinanz = new P67DurMmFinanz();
    //Original name: P67-DT-END-FINANZ
    private P67DtEndFinanz p67DtEndFinanz = new P67DtEndFinanz();
    //Original name: P67-AMM-1A-RAT
    private P67Amm1aRat p67Amm1aRat = new P67Amm1aRat();
    //Original name: P67-VAL-RISC-BENE
    private P67ValRiscBene p67ValRiscBene = new P67ValRiscBene();
    //Original name: P67-AMM-RAT-END
    private P67AmmRatEnd p67AmmRatEnd = new P67AmmRatEnd();
    //Original name: P67-TS-CRE-RAT-FINANZ
    private P67TsCreRatFinanz p67TsCreRatFinanz = new P67TsCreRatFinanz();
    //Original name: P67-IMP-FIN-REVOLVING
    private P67ImpFinRevolving p67ImpFinRevolving = new P67ImpFinRevolving();
    //Original name: P67-IMP-UTIL-C-REV
    private P67ImpUtilCRev p67ImpUtilCRev = new P67ImpUtilCRev();
    //Original name: P67-IMP-RAT-REVOLVING
    private P67ImpRatRevolving p67ImpRatRevolving = new P67ImpRatRevolving();
    //Original name: P67-DT-1O-UTLZ-C-REV
    private P67Dt1oUtlzCRev p67Dt1oUtlzCRev = new P67Dt1oUtlzCRev();
    //Original name: P67-IMP-ASSTO
    private P67ImpAssto p67ImpAssto = new P67ImpAssto();
    //Original name: P67-PRE-VERS
    private P67PreVers p67PreVers = new P67PreVers();
    //Original name: P67-DT-SCAD-COP
    private P67DtScadCop p67DtScadCop = new P67DtScadCop();
    //Original name: P67-GG-DEL-MM-SCAD-RAT
    private P67GgDelMmScadRat p67GgDelMmScadRat = new P67GgDelMmScadRat();
    //Original name: P67-FL-PRE-FIN
    private char p67FlPreFin = DefaultValues.CHAR_VAL;
    //Original name: P67-DT-SCAD-1A-RAT
    private P67DtScad1aRat p67DtScad1aRat = new P67DtScad1aRat();
    //Original name: P67-DT-EROG-FINANZ
    private P67DtErogFinanz p67DtErogFinanz = new P67DtErogFinanz();
    //Original name: P67-DT-STIPULA-FINANZ
    private P67DtStipulaFinanz p67DtStipulaFinanz = new P67DtStipulaFinanz();
    //Original name: P67-MM-PREAMM
    private P67MmPreamm p67MmPreamm = new P67MmPreamm();
    //Original name: P67-IMP-DEB-RES
    private P67ImpDebRes p67ImpDebRes = new P67ImpDebRes();
    //Original name: P67-IMP-RAT-FINANZ
    private P67ImpRatFinanz p67ImpRatFinanz = new P67ImpRatFinanz();
    //Original name: P67-IMP-CANONE-ANTIC
    private P67ImpCanoneAntic p67ImpCanoneAntic = new P67ImpCanoneAntic();
    //Original name: P67-PER-RAT-FINANZ
    private P67PerRatFinanz p67PerRatFinanz = new P67PerRatFinanz();
    //Original name: P67-TP-MOD-PAG-RAT
    private String p67TpModPagRat = DefaultValues.stringVal(Len.P67_TP_MOD_PAG_RAT);
    //Original name: P67-TP-FINANZ-ER
    private String p67TpFinanzEr = DefaultValues.stringVal(Len.P67_TP_FINANZ_ER);
    //Original name: P67-CAT-FINANZ-ER
    private String p67CatFinanzEr = DefaultValues.stringVal(Len.P67_CAT_FINANZ_ER);
    //Original name: P67-VAL-RISC-END-LEAS
    private P67ValRiscEndLeas p67ValRiscEndLeas = new P67ValRiscEndLeas();
    //Original name: P67-DT-EST-FINANZ
    private P67DtEstFinanz p67DtEstFinanz = new P67DtEstFinanz();
    //Original name: P67-DT-MAN-COP
    private P67DtManCop p67DtManCop = new P67DtManCop();
    //Original name: P67-NUM-FINANZ
    private String p67NumFinanz = DefaultValues.stringVal(Len.P67_NUM_FINANZ);
    //Original name: P67-TP-MOD-ACQS
    private String p67TpModAcqs = DefaultValues.stringVal(Len.P67_TP_MOD_ACQS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.EST_POLI_CPI_PR;
    }

    @Override
    public void deserialize(byte[] buf) {
        setEstPoliCpiPrBytes(buf);
    }

    public void setEstPoliCpiPrFormatted(String data) {
        byte[] buffer = new byte[Len.EST_POLI_CPI_PR];
        MarshalByte.writeString(buffer, 1, data, Len.EST_POLI_CPI_PR);
        setEstPoliCpiPrBytes(buffer, 1);
    }

    public String getEstPoliCpiPrFormatted() {
        return MarshalByteExt.bufferToStr(getEstPoliCpiPrBytes());
    }

    public void setEstPoliCpiPrBytes(byte[] buffer) {
        setEstPoliCpiPrBytes(buffer, 1);
    }

    public byte[] getEstPoliCpiPrBytes() {
        byte[] buffer = new byte[Len.EST_POLI_CPI_PR];
        return getEstPoliCpiPrBytes(buffer, 1);
    }

    public void setEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        p67IdEstPoliCpiPr = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P67_ID_EST_POLI_CPI_PR, 0);
        position += Len.P67_ID_EST_POLI_CPI_PR;
        p67IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P67_ID_MOVI_CRZ, 0);
        position += Len.P67_ID_MOVI_CRZ;
        p67IdMoviChiu.setP67IdMoviChiuFromBuffer(buffer, position);
        position += P67IdMoviChiu.Len.P67_ID_MOVI_CHIU;
        p67DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P67_DT_INI_EFF, 0);
        position += Len.P67_DT_INI_EFF;
        p67DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P67_DT_END_EFF, 0);
        position += Len.P67_DT_END_EFF;
        p67CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P67_COD_COMP_ANIA, 0);
        position += Len.P67_COD_COMP_ANIA;
        p67IbOgg = MarshalByte.readString(buffer, position, Len.P67_IB_OGG);
        position += Len.P67_IB_OGG;
        p67DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P67_DS_RIGA, 0);
        position += Len.P67_DS_RIGA;
        p67DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p67DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P67_DS_VER, 0);
        position += Len.P67_DS_VER;
        p67DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P67_DS_TS_INI_CPTZ, 0);
        position += Len.P67_DS_TS_INI_CPTZ;
        p67DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P67_DS_TS_END_CPTZ, 0);
        position += Len.P67_DS_TS_END_CPTZ;
        p67DsUtente = MarshalByte.readString(buffer, position, Len.P67_DS_UTENTE);
        position += Len.P67_DS_UTENTE;
        p67DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p67CodProdEstno = MarshalByte.readString(buffer, position, Len.P67_COD_PROD_ESTNO);
        position += Len.P67_COD_PROD_ESTNO;
        p67CptFin.setP67CptFinFromBuffer(buffer, position);
        position += P67CptFin.Len.P67_CPT_FIN;
        p67NumTstFin.setP67NumTstFinFromBuffer(buffer, position);
        position += P67NumTstFin.Len.P67_NUM_TST_FIN;
        p67TsFinanz.setP67TsFinanzFromBuffer(buffer, position);
        position += P67TsFinanz.Len.P67_TS_FINANZ;
        p67DurMmFinanz.setP67DurMmFinanzFromBuffer(buffer, position);
        position += P67DurMmFinanz.Len.P67_DUR_MM_FINANZ;
        p67DtEndFinanz.setP67DtEndFinanzFromBuffer(buffer, position);
        position += P67DtEndFinanz.Len.P67_DT_END_FINANZ;
        p67Amm1aRat.setP67Amm1aRatFromBuffer(buffer, position);
        position += P67Amm1aRat.Len.P67_AMM1A_RAT;
        p67ValRiscBene.setP67ValRiscBeneFromBuffer(buffer, position);
        position += P67ValRiscBene.Len.P67_VAL_RISC_BENE;
        p67AmmRatEnd.setP67AmmRatEndFromBuffer(buffer, position);
        position += P67AmmRatEnd.Len.P67_AMM_RAT_END;
        p67TsCreRatFinanz.setP67TsCreRatFinanzFromBuffer(buffer, position);
        position += P67TsCreRatFinanz.Len.P67_TS_CRE_RAT_FINANZ;
        p67ImpFinRevolving.setP67ImpFinRevolvingFromBuffer(buffer, position);
        position += P67ImpFinRevolving.Len.P67_IMP_FIN_REVOLVING;
        p67ImpUtilCRev.setP67ImpUtilCRevFromBuffer(buffer, position);
        position += P67ImpUtilCRev.Len.P67_IMP_UTIL_C_REV;
        p67ImpRatRevolving.setP67ImpRatRevolvingFromBuffer(buffer, position);
        position += P67ImpRatRevolving.Len.P67_IMP_RAT_REVOLVING;
        p67Dt1oUtlzCRev.setP67Dt1oUtlzCRevFromBuffer(buffer, position);
        position += P67Dt1oUtlzCRev.Len.P67_DT1O_UTLZ_C_REV;
        p67ImpAssto.setP67ImpAsstoFromBuffer(buffer, position);
        position += P67ImpAssto.Len.P67_IMP_ASSTO;
        p67PreVers.setP67PreVersFromBuffer(buffer, position);
        position += P67PreVers.Len.P67_PRE_VERS;
        p67DtScadCop.setP67DtScadCopFromBuffer(buffer, position);
        position += P67DtScadCop.Len.P67_DT_SCAD_COP;
        p67GgDelMmScadRat.setP67GgDelMmScadRatFromBuffer(buffer, position);
        position += P67GgDelMmScadRat.Len.P67_GG_DEL_MM_SCAD_RAT;
        p67FlPreFin = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p67DtScad1aRat.setP67DtScad1aRatFromBuffer(buffer, position);
        position += P67DtScad1aRat.Len.P67_DT_SCAD1A_RAT;
        p67DtErogFinanz.setP67DtErogFinanzFromBuffer(buffer, position);
        position += P67DtErogFinanz.Len.P67_DT_EROG_FINANZ;
        p67DtStipulaFinanz.setP67DtStipulaFinanzFromBuffer(buffer, position);
        position += P67DtStipulaFinanz.Len.P67_DT_STIPULA_FINANZ;
        p67MmPreamm.setP67MmPreammFromBuffer(buffer, position);
        position += P67MmPreamm.Len.P67_MM_PREAMM;
        p67ImpDebRes.setP67ImpDebResFromBuffer(buffer, position);
        position += P67ImpDebRes.Len.P67_IMP_DEB_RES;
        p67ImpRatFinanz.setP67ImpRatFinanzFromBuffer(buffer, position);
        position += P67ImpRatFinanz.Len.P67_IMP_RAT_FINANZ;
        p67ImpCanoneAntic.setP67ImpCanoneAnticFromBuffer(buffer, position);
        position += P67ImpCanoneAntic.Len.P67_IMP_CANONE_ANTIC;
        p67PerRatFinanz.setP67PerRatFinanzFromBuffer(buffer, position);
        position += P67PerRatFinanz.Len.P67_PER_RAT_FINANZ;
        p67TpModPagRat = MarshalByte.readString(buffer, position, Len.P67_TP_MOD_PAG_RAT);
        position += Len.P67_TP_MOD_PAG_RAT;
        p67TpFinanzEr = MarshalByte.readString(buffer, position, Len.P67_TP_FINANZ_ER);
        position += Len.P67_TP_FINANZ_ER;
        p67CatFinanzEr = MarshalByte.readString(buffer, position, Len.P67_CAT_FINANZ_ER);
        position += Len.P67_CAT_FINANZ_ER;
        p67ValRiscEndLeas.setP67ValRiscEndLeasFromBuffer(buffer, position);
        position += P67ValRiscEndLeas.Len.P67_VAL_RISC_END_LEAS;
        p67DtEstFinanz.setP67DtEstFinanzFromBuffer(buffer, position);
        position += P67DtEstFinanz.Len.P67_DT_EST_FINANZ;
        p67DtManCop.setP67DtManCopFromBuffer(buffer, position);
        position += P67DtManCop.Len.P67_DT_MAN_COP;
        p67NumFinanz = MarshalByte.readString(buffer, position, Len.P67_NUM_FINANZ);
        position += Len.P67_NUM_FINANZ;
        p67TpModAcqs = MarshalByte.readString(buffer, position, Len.P67_TP_MOD_ACQS);
    }

    public byte[] getEstPoliCpiPrBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p67IdEstPoliCpiPr, Len.Int.P67_ID_EST_POLI_CPI_PR, 0);
        position += Len.P67_ID_EST_POLI_CPI_PR;
        MarshalByte.writeIntAsPacked(buffer, position, p67IdMoviCrz, Len.Int.P67_ID_MOVI_CRZ, 0);
        position += Len.P67_ID_MOVI_CRZ;
        p67IdMoviChiu.getP67IdMoviChiuAsBuffer(buffer, position);
        position += P67IdMoviChiu.Len.P67_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, p67DtIniEff, Len.Int.P67_DT_INI_EFF, 0);
        position += Len.P67_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p67DtEndEff, Len.Int.P67_DT_END_EFF, 0);
        position += Len.P67_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p67CodCompAnia, Len.Int.P67_COD_COMP_ANIA, 0);
        position += Len.P67_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, p67IbOgg, Len.P67_IB_OGG);
        position += Len.P67_IB_OGG;
        MarshalByte.writeLongAsPacked(buffer, position, p67DsRiga, Len.Int.P67_DS_RIGA, 0);
        position += Len.P67_DS_RIGA;
        MarshalByte.writeChar(buffer, position, p67DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p67DsVer, Len.Int.P67_DS_VER, 0);
        position += Len.P67_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p67DsTsIniCptz, Len.Int.P67_DS_TS_INI_CPTZ, 0);
        position += Len.P67_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, p67DsTsEndCptz, Len.Int.P67_DS_TS_END_CPTZ, 0);
        position += Len.P67_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, p67DsUtente, Len.P67_DS_UTENTE);
        position += Len.P67_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p67DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p67CodProdEstno, Len.P67_COD_PROD_ESTNO);
        position += Len.P67_COD_PROD_ESTNO;
        p67CptFin.getP67CptFinAsBuffer(buffer, position);
        position += P67CptFin.Len.P67_CPT_FIN;
        p67NumTstFin.getP67NumTstFinAsBuffer(buffer, position);
        position += P67NumTstFin.Len.P67_NUM_TST_FIN;
        p67TsFinanz.getP67TsFinanzAsBuffer(buffer, position);
        position += P67TsFinanz.Len.P67_TS_FINANZ;
        p67DurMmFinanz.getP67DurMmFinanzAsBuffer(buffer, position);
        position += P67DurMmFinanz.Len.P67_DUR_MM_FINANZ;
        p67DtEndFinanz.getP67DtEndFinanzAsBuffer(buffer, position);
        position += P67DtEndFinanz.Len.P67_DT_END_FINANZ;
        p67Amm1aRat.getP67Amm1aRatAsBuffer(buffer, position);
        position += P67Amm1aRat.Len.P67_AMM1A_RAT;
        p67ValRiscBene.getP67ValRiscBeneAsBuffer(buffer, position);
        position += P67ValRiscBene.Len.P67_VAL_RISC_BENE;
        p67AmmRatEnd.getP67AmmRatEndAsBuffer(buffer, position);
        position += P67AmmRatEnd.Len.P67_AMM_RAT_END;
        p67TsCreRatFinanz.getP67TsCreRatFinanzAsBuffer(buffer, position);
        position += P67TsCreRatFinanz.Len.P67_TS_CRE_RAT_FINANZ;
        p67ImpFinRevolving.getP67ImpFinRevolvingAsBuffer(buffer, position);
        position += P67ImpFinRevolving.Len.P67_IMP_FIN_REVOLVING;
        p67ImpUtilCRev.getP67ImpUtilCRevAsBuffer(buffer, position);
        position += P67ImpUtilCRev.Len.P67_IMP_UTIL_C_REV;
        p67ImpRatRevolving.getP67ImpRatRevolvingAsBuffer(buffer, position);
        position += P67ImpRatRevolving.Len.P67_IMP_RAT_REVOLVING;
        p67Dt1oUtlzCRev.getP67Dt1oUtlzCRevAsBuffer(buffer, position);
        position += P67Dt1oUtlzCRev.Len.P67_DT1O_UTLZ_C_REV;
        p67ImpAssto.getP67ImpAsstoAsBuffer(buffer, position);
        position += P67ImpAssto.Len.P67_IMP_ASSTO;
        p67PreVers.getP67PreVersAsBuffer(buffer, position);
        position += P67PreVers.Len.P67_PRE_VERS;
        p67DtScadCop.getP67DtScadCopAsBuffer(buffer, position);
        position += P67DtScadCop.Len.P67_DT_SCAD_COP;
        p67GgDelMmScadRat.getP67GgDelMmScadRatAsBuffer(buffer, position);
        position += P67GgDelMmScadRat.Len.P67_GG_DEL_MM_SCAD_RAT;
        MarshalByte.writeChar(buffer, position, p67FlPreFin);
        position += Types.CHAR_SIZE;
        p67DtScad1aRat.getP67DtScad1aRatAsBuffer(buffer, position);
        position += P67DtScad1aRat.Len.P67_DT_SCAD1A_RAT;
        p67DtErogFinanz.getP67DtErogFinanzAsBuffer(buffer, position);
        position += P67DtErogFinanz.Len.P67_DT_EROG_FINANZ;
        p67DtStipulaFinanz.getP67DtStipulaFinanzAsBuffer(buffer, position);
        position += P67DtStipulaFinanz.Len.P67_DT_STIPULA_FINANZ;
        p67MmPreamm.getP67MmPreammAsBuffer(buffer, position);
        position += P67MmPreamm.Len.P67_MM_PREAMM;
        p67ImpDebRes.getP67ImpDebResAsBuffer(buffer, position);
        position += P67ImpDebRes.Len.P67_IMP_DEB_RES;
        p67ImpRatFinanz.getP67ImpRatFinanzAsBuffer(buffer, position);
        position += P67ImpRatFinanz.Len.P67_IMP_RAT_FINANZ;
        p67ImpCanoneAntic.getP67ImpCanoneAnticAsBuffer(buffer, position);
        position += P67ImpCanoneAntic.Len.P67_IMP_CANONE_ANTIC;
        p67PerRatFinanz.getP67PerRatFinanzAsBuffer(buffer, position);
        position += P67PerRatFinanz.Len.P67_PER_RAT_FINANZ;
        MarshalByte.writeString(buffer, position, p67TpModPagRat, Len.P67_TP_MOD_PAG_RAT);
        position += Len.P67_TP_MOD_PAG_RAT;
        MarshalByte.writeString(buffer, position, p67TpFinanzEr, Len.P67_TP_FINANZ_ER);
        position += Len.P67_TP_FINANZ_ER;
        MarshalByte.writeString(buffer, position, p67CatFinanzEr, Len.P67_CAT_FINANZ_ER);
        position += Len.P67_CAT_FINANZ_ER;
        p67ValRiscEndLeas.getP67ValRiscEndLeasAsBuffer(buffer, position);
        position += P67ValRiscEndLeas.Len.P67_VAL_RISC_END_LEAS;
        p67DtEstFinanz.getP67DtEstFinanzAsBuffer(buffer, position);
        position += P67DtEstFinanz.Len.P67_DT_EST_FINANZ;
        p67DtManCop.getP67DtManCopAsBuffer(buffer, position);
        position += P67DtManCop.Len.P67_DT_MAN_COP;
        MarshalByte.writeString(buffer, position, p67NumFinanz, Len.P67_NUM_FINANZ);
        position += Len.P67_NUM_FINANZ;
        MarshalByte.writeString(buffer, position, p67TpModAcqs, Len.P67_TP_MOD_ACQS);
        return buffer;
    }

    public void setP67IdEstPoliCpiPr(int p67IdEstPoliCpiPr) {
        this.p67IdEstPoliCpiPr = p67IdEstPoliCpiPr;
    }

    public int getP67IdEstPoliCpiPr() {
        return this.p67IdEstPoliCpiPr;
    }

    public void setP67IdMoviCrz(int p67IdMoviCrz) {
        this.p67IdMoviCrz = p67IdMoviCrz;
    }

    public int getP67IdMoviCrz() {
        return this.p67IdMoviCrz;
    }

    public void setP67DtIniEff(int p67DtIniEff) {
        this.p67DtIniEff = p67DtIniEff;
    }

    public int getP67DtIniEff() {
        return this.p67DtIniEff;
    }

    public void setP67DtEndEff(int p67DtEndEff) {
        this.p67DtEndEff = p67DtEndEff;
    }

    public int getP67DtEndEff() {
        return this.p67DtEndEff;
    }

    public void setP67CodCompAnia(int p67CodCompAnia) {
        this.p67CodCompAnia = p67CodCompAnia;
    }

    public int getP67CodCompAnia() {
        return this.p67CodCompAnia;
    }

    public void setP67IbOgg(String p67IbOgg) {
        this.p67IbOgg = Functions.subString(p67IbOgg, Len.P67_IB_OGG);
    }

    public String getP67IbOgg() {
        return this.p67IbOgg;
    }

    public void setP67DsRiga(long p67DsRiga) {
        this.p67DsRiga = p67DsRiga;
    }

    public long getP67DsRiga() {
        return this.p67DsRiga;
    }

    public void setP67DsOperSql(char p67DsOperSql) {
        this.p67DsOperSql = p67DsOperSql;
    }

    public void setP67DsOperSqlFormatted(String p67DsOperSql) {
        setP67DsOperSql(Functions.charAt(p67DsOperSql, Types.CHAR_SIZE));
    }

    public char getP67DsOperSql() {
        return this.p67DsOperSql;
    }

    public void setP67DsVer(int p67DsVer) {
        this.p67DsVer = p67DsVer;
    }

    public int getP67DsVer() {
        return this.p67DsVer;
    }

    public void setP67DsTsIniCptz(long p67DsTsIniCptz) {
        this.p67DsTsIniCptz = p67DsTsIniCptz;
    }

    public long getP67DsTsIniCptz() {
        return this.p67DsTsIniCptz;
    }

    public void setP67DsTsEndCptz(long p67DsTsEndCptz) {
        this.p67DsTsEndCptz = p67DsTsEndCptz;
    }

    public long getP67DsTsEndCptz() {
        return this.p67DsTsEndCptz;
    }

    public void setP67DsUtente(String p67DsUtente) {
        this.p67DsUtente = Functions.subString(p67DsUtente, Len.P67_DS_UTENTE);
    }

    public String getP67DsUtente() {
        return this.p67DsUtente;
    }

    public void setP67DsStatoElab(char p67DsStatoElab) {
        this.p67DsStatoElab = p67DsStatoElab;
    }

    public void setP67DsStatoElabFormatted(String p67DsStatoElab) {
        setP67DsStatoElab(Functions.charAt(p67DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP67DsStatoElab() {
        return this.p67DsStatoElab;
    }

    public void setP67CodProdEstno(String p67CodProdEstno) {
        this.p67CodProdEstno = Functions.subString(p67CodProdEstno, Len.P67_COD_PROD_ESTNO);
    }

    public String getP67CodProdEstno() {
        return this.p67CodProdEstno;
    }

    public void setP67FlPreFin(char p67FlPreFin) {
        this.p67FlPreFin = p67FlPreFin;
    }

    public char getP67FlPreFin() {
        return this.p67FlPreFin;
    }

    public void setP67TpModPagRat(String p67TpModPagRat) {
        this.p67TpModPagRat = Functions.subString(p67TpModPagRat, Len.P67_TP_MOD_PAG_RAT);
    }

    public String getP67TpModPagRat() {
        return this.p67TpModPagRat;
    }

    public String getP67TpModPagRatFormatted() {
        return Functions.padBlanks(getP67TpModPagRat(), Len.P67_TP_MOD_PAG_RAT);
    }

    public void setP67TpFinanzEr(String p67TpFinanzEr) {
        this.p67TpFinanzEr = Functions.subString(p67TpFinanzEr, Len.P67_TP_FINANZ_ER);
    }

    public String getP67TpFinanzEr() {
        return this.p67TpFinanzEr;
    }

    public String getP67TpFinanzErFormatted() {
        return Functions.padBlanks(getP67TpFinanzEr(), Len.P67_TP_FINANZ_ER);
    }

    public void setP67CatFinanzEr(String p67CatFinanzEr) {
        this.p67CatFinanzEr = Functions.subString(p67CatFinanzEr, Len.P67_CAT_FINANZ_ER);
    }

    public String getP67CatFinanzEr() {
        return this.p67CatFinanzEr;
    }

    public String getP67CatFinanzErFormatted() {
        return Functions.padBlanks(getP67CatFinanzEr(), Len.P67_CAT_FINANZ_ER);
    }

    public void setP67NumFinanz(String p67NumFinanz) {
        this.p67NumFinanz = Functions.subString(p67NumFinanz, Len.P67_NUM_FINANZ);
    }

    public String getP67NumFinanz() {
        return this.p67NumFinanz;
    }

    public void setP67TpModAcqs(String p67TpModAcqs) {
        this.p67TpModAcqs = Functions.subString(p67TpModAcqs, Len.P67_TP_MOD_ACQS);
    }

    public String getP67TpModAcqs() {
        return this.p67TpModAcqs;
    }

    public String getP67TpModAcqsFormatted() {
        return Functions.padBlanks(getP67TpModAcqs(), Len.P67_TP_MOD_ACQS);
    }

    public P67Amm1aRat getP67Amm1aRat() {
        return p67Amm1aRat;
    }

    public P67AmmRatEnd getP67AmmRatEnd() {
        return p67AmmRatEnd;
    }

    public P67CptFin getP67CptFin() {
        return p67CptFin;
    }

    public P67Dt1oUtlzCRev getP67Dt1oUtlzCRev() {
        return p67Dt1oUtlzCRev;
    }

    public P67DtEndFinanz getP67DtEndFinanz() {
        return p67DtEndFinanz;
    }

    public P67DtErogFinanz getP67DtErogFinanz() {
        return p67DtErogFinanz;
    }

    public P67DtEstFinanz getP67DtEstFinanz() {
        return p67DtEstFinanz;
    }

    public P67DtManCop getP67DtManCop() {
        return p67DtManCop;
    }

    public P67DtScad1aRat getP67DtScad1aRat() {
        return p67DtScad1aRat;
    }

    public P67DtScadCop getP67DtScadCop() {
        return p67DtScadCop;
    }

    public P67DtStipulaFinanz getP67DtStipulaFinanz() {
        return p67DtStipulaFinanz;
    }

    public P67DurMmFinanz getP67DurMmFinanz() {
        return p67DurMmFinanz;
    }

    public P67GgDelMmScadRat getP67GgDelMmScadRat() {
        return p67GgDelMmScadRat;
    }

    public P67IdMoviChiu getP67IdMoviChiu() {
        return p67IdMoviChiu;
    }

    public P67ImpAssto getP67ImpAssto() {
        return p67ImpAssto;
    }

    public P67ImpCanoneAntic getP67ImpCanoneAntic() {
        return p67ImpCanoneAntic;
    }

    public P67ImpDebRes getP67ImpDebRes() {
        return p67ImpDebRes;
    }

    public P67ImpFinRevolving getP67ImpFinRevolving() {
        return p67ImpFinRevolving;
    }

    public P67ImpRatFinanz getP67ImpRatFinanz() {
        return p67ImpRatFinanz;
    }

    public P67ImpRatRevolving getP67ImpRatRevolving() {
        return p67ImpRatRevolving;
    }

    public P67ImpUtilCRev getP67ImpUtilCRev() {
        return p67ImpUtilCRev;
    }

    public P67MmPreamm getP67MmPreamm() {
        return p67MmPreamm;
    }

    public P67NumTstFin getP67NumTstFin() {
        return p67NumTstFin;
    }

    public P67PerRatFinanz getP67PerRatFinanz() {
        return p67PerRatFinanz;
    }

    public P67PreVers getP67PreVers() {
        return p67PreVers;
    }

    public P67TsCreRatFinanz getP67TsCreRatFinanz() {
        return p67TsCreRatFinanz;
    }

    public P67TsFinanz getP67TsFinanz() {
        return p67TsFinanz;
    }

    public P67ValRiscBene getP67ValRiscBene() {
        return p67ValRiscBene;
    }

    public P67ValRiscEndLeas getP67ValRiscEndLeas() {
        return p67ValRiscEndLeas;
    }

    @Override
    public byte[] serialize() {
        return getEstPoliCpiPrBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_ID_EST_POLI_CPI_PR = 5;
        public static final int P67_ID_MOVI_CRZ = 5;
        public static final int P67_DT_INI_EFF = 5;
        public static final int P67_DT_END_EFF = 5;
        public static final int P67_COD_COMP_ANIA = 3;
        public static final int P67_IB_OGG = 40;
        public static final int P67_DS_RIGA = 6;
        public static final int P67_DS_OPER_SQL = 1;
        public static final int P67_DS_VER = 5;
        public static final int P67_DS_TS_INI_CPTZ = 10;
        public static final int P67_DS_TS_END_CPTZ = 10;
        public static final int P67_DS_UTENTE = 20;
        public static final int P67_DS_STATO_ELAB = 1;
        public static final int P67_COD_PROD_ESTNO = 20;
        public static final int P67_FL_PRE_FIN = 1;
        public static final int P67_TP_MOD_PAG_RAT = 2;
        public static final int P67_TP_FINANZ_ER = 2;
        public static final int P67_CAT_FINANZ_ER = 20;
        public static final int P67_NUM_FINANZ = 40;
        public static final int P67_TP_MOD_ACQS = 4;
        public static final int EST_POLI_CPI_PR = P67_ID_EST_POLI_CPI_PR + P67_ID_MOVI_CRZ + P67IdMoviChiu.Len.P67_ID_MOVI_CHIU + P67_DT_INI_EFF + P67_DT_END_EFF + P67_COD_COMP_ANIA + P67_IB_OGG + P67_DS_RIGA + P67_DS_OPER_SQL + P67_DS_VER + P67_DS_TS_INI_CPTZ + P67_DS_TS_END_CPTZ + P67_DS_UTENTE + P67_DS_STATO_ELAB + P67_COD_PROD_ESTNO + P67CptFin.Len.P67_CPT_FIN + P67NumTstFin.Len.P67_NUM_TST_FIN + P67TsFinanz.Len.P67_TS_FINANZ + P67DurMmFinanz.Len.P67_DUR_MM_FINANZ + P67DtEndFinanz.Len.P67_DT_END_FINANZ + P67Amm1aRat.Len.P67_AMM1A_RAT + P67ValRiscBene.Len.P67_VAL_RISC_BENE + P67AmmRatEnd.Len.P67_AMM_RAT_END + P67TsCreRatFinanz.Len.P67_TS_CRE_RAT_FINANZ + P67ImpFinRevolving.Len.P67_IMP_FIN_REVOLVING + P67ImpUtilCRev.Len.P67_IMP_UTIL_C_REV + P67ImpRatRevolving.Len.P67_IMP_RAT_REVOLVING + P67Dt1oUtlzCRev.Len.P67_DT1O_UTLZ_C_REV + P67ImpAssto.Len.P67_IMP_ASSTO + P67PreVers.Len.P67_PRE_VERS + P67DtScadCop.Len.P67_DT_SCAD_COP + P67GgDelMmScadRat.Len.P67_GG_DEL_MM_SCAD_RAT + P67_FL_PRE_FIN + P67DtScad1aRat.Len.P67_DT_SCAD1A_RAT + P67DtErogFinanz.Len.P67_DT_EROG_FINANZ + P67DtStipulaFinanz.Len.P67_DT_STIPULA_FINANZ + P67MmPreamm.Len.P67_MM_PREAMM + P67ImpDebRes.Len.P67_IMP_DEB_RES + P67ImpRatFinanz.Len.P67_IMP_RAT_FINANZ + P67ImpCanoneAntic.Len.P67_IMP_CANONE_ANTIC + P67PerRatFinanz.Len.P67_PER_RAT_FINANZ + P67_TP_MOD_PAG_RAT + P67_TP_FINANZ_ER + P67_CAT_FINANZ_ER + P67ValRiscEndLeas.Len.P67_VAL_RISC_END_LEAS + P67DtEstFinanz.Len.P67_DT_EST_FINANZ + P67DtManCop.Len.P67_DT_MAN_COP + P67_NUM_FINANZ + P67_TP_MOD_ACQS;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_ID_EST_POLI_CPI_PR = 9;
            public static final int P67_ID_MOVI_CRZ = 9;
            public static final int P67_DT_INI_EFF = 8;
            public static final int P67_DT_END_EFF = 8;
            public static final int P67_COD_COMP_ANIA = 5;
            public static final int P67_DS_RIGA = 10;
            public static final int P67_DS_VER = 9;
            public static final int P67_DS_TS_INI_CPTZ = 18;
            public static final int P67_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
