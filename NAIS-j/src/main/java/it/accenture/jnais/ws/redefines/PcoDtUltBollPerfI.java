package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-PERF-I<br>
 * Variable: PCO-DT-ULT-BOLL-PERF-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollPerfI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollPerfI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_PERF_I;
    }

    public void setPcoDtUltBollPerfI(int pcoDtUltBollPerfI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_PERF_I, pcoDtUltBollPerfI, Len.Int.PCO_DT_ULT_BOLL_PERF_I);
    }

    public void setPcoDtUltBollPerfIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PERF_I, Pos.PCO_DT_ULT_BOLL_PERF_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-PERF-I<br>*/
    public int getPcoDtUltBollPerfI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_PERF_I, Len.Int.PCO_DT_ULT_BOLL_PERF_I);
    }

    public byte[] getPcoDtUltBollPerfIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PERF_I, Pos.PCO_DT_ULT_BOLL_PERF_I);
        return buffer;
    }

    public void initPcoDtUltBollPerfIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_PERF_I, Len.PCO_DT_ULT_BOLL_PERF_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollPerfINull(String pcoDtUltBollPerfINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_PERF_I_NULL, pcoDtUltBollPerfINull, Len.PCO_DT_ULT_BOLL_PERF_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-PERF-I-NULL<br>*/
    public String getPcoDtUltBollPerfINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_PERF_I_NULL, Len.PCO_DT_ULT_BOLL_PERF_I_NULL);
    }

    public String getPcoDtUltBollPerfINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollPerfINull(), Len.PCO_DT_ULT_BOLL_PERF_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PERF_I = 1;
        public static final int PCO_DT_ULT_BOLL_PERF_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PERF_I = 5;
        public static final int PCO_DT_ULT_BOLL_PERF_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_PERF_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
