package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WA-TAFESTEX<br>
 * Variable: WA-TAFESTEX from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WaTafestex extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int FESTEX_MAXOCCURS = 20;

    //==== CONSTRUCTORS ====
    public WaTafestex() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WA_TAFESTEX;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "0101", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "0106", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "0425", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "0501", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "0602", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "0815", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "1101", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "1208", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "1225", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "1226", Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
        position += Len.FLR1;
        writeString(position, LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FLR1), Len.FLR1);
    }

    /**Original name: WA-FESTEX<br>*/
    public String getFestex(int festexIdx) {
        int position = Pos.waFestex(festexIdx - 1);
        return readString(position, Len.FESTEX);
    }

    public String getFestexFormatted(int festexIdx) {
        return Functions.padBlanks(getFestex(festexIdx), Len.FESTEX);
    }

    public byte[] getFestexAsBuffer(int festexIdx, byte[] buffer, int offset) {
        int position = Pos.waFestex(festexIdx - 1);
        getBytes(buffer, offset, Len.FESTEX, position);
        return buffer;
    }

    public byte[] getFestexAsBuffer(int festexIdx) {
        byte[] buffer = new byte[Len.FESTEX];
        return getFestexAsBuffer(festexIdx, buffer, 1);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WA_TAFESTEX = 1;
        public static final int FLR1 = WA_TAFESTEX;
        public static final int FLR2 = FLR1 + Len.FLR1;
        public static final int FLR3 = FLR2 + Len.FLR1;
        public static final int FLR4 = FLR3 + Len.FLR1;
        public static final int FLR5 = FLR4 + Len.FLR1;
        public static final int FLR6 = FLR5 + Len.FLR1;
        public static final int FLR7 = FLR6 + Len.FLR1;
        public static final int FLR8 = FLR7 + Len.FLR1;
        public static final int FLR9 = FLR8 + Len.FLR1;
        public static final int FLR10 = FLR9 + Len.FLR1;
        public static final int FLR11 = FLR10 + Len.FLR1;
        public static final int FLR12 = FLR11 + Len.FLR1;
        public static final int FLR13 = FLR12 + Len.FLR1;
        public static final int FLR14 = FLR13 + Len.FLR1;
        public static final int FLR15 = FLR14 + Len.FLR1;
        public static final int FLR16 = FLR15 + Len.FLR1;
        public static final int FLR17 = FLR16 + Len.FLR1;
        public static final int FLR18 = FLR17 + Len.FLR1;
        public static final int FLR19 = FLR18 + Len.FLR1;
        public static final int FLR20 = FLR19 + Len.FLR1;
        public static final int FILLER_WS7 = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int waFestex(int idx) {
            return FILLER_WS7 + idx * Len.FESTEX;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 4;
        public static final int FESTEX = 4;
        public static final int WA_TAFESTEX = 20 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
