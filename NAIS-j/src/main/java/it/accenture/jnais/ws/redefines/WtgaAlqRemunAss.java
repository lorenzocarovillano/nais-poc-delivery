package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ALQ-REMUN-ASS<br>
 * Variable: WTGA-ALQ-REMUN-ASS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAlqRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAlqRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ALQ_REMUN_ASS;
    }

    public void setWtgaAlqRemunAss(AfDecimal wtgaAlqRemunAss) {
        writeDecimalAsPacked(Pos.WTGA_ALQ_REMUN_ASS, wtgaAlqRemunAss.copy());
    }

    public void setWtgaAlqRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ALQ_REMUN_ASS, Pos.WTGA_ALQ_REMUN_ASS);
    }

    /**Original name: WTGA-ALQ-REMUN-ASS<br>*/
    public AfDecimal getWtgaAlqRemunAss() {
        return readPackedAsDecimal(Pos.WTGA_ALQ_REMUN_ASS, Len.Int.WTGA_ALQ_REMUN_ASS, Len.Fract.WTGA_ALQ_REMUN_ASS);
    }

    public byte[] getWtgaAlqRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ALQ_REMUN_ASS, Pos.WTGA_ALQ_REMUN_ASS);
        return buffer;
    }

    public void initWtgaAlqRemunAssSpaces() {
        fill(Pos.WTGA_ALQ_REMUN_ASS, Len.WTGA_ALQ_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWtgaAlqRemunAssNull(String wtgaAlqRemunAssNull) {
        writeString(Pos.WTGA_ALQ_REMUN_ASS_NULL, wtgaAlqRemunAssNull, Len.WTGA_ALQ_REMUN_ASS_NULL);
    }

    /**Original name: WTGA-ALQ-REMUN-ASS-NULL<br>*/
    public String getWtgaAlqRemunAssNull() {
        return readString(Pos.WTGA_ALQ_REMUN_ASS_NULL, Len.WTGA_ALQ_REMUN_ASS_NULL);
    }

    public String getWtgaAlqRemunAssNullFormatted() {
        return Functions.padBlanks(getWtgaAlqRemunAssNull(), Len.WTGA_ALQ_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_REMUN_ASS = 1;
        public static final int WTGA_ALQ_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_REMUN_ASS = 4;
        public static final int WTGA_ALQ_REMUN_ASS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
