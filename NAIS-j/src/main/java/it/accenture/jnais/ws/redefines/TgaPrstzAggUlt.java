package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-AGG-ULT<br>
 * Variable: TGA-PRSTZ-AGG-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzAggUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzAggUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_AGG_ULT;
    }

    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_AGG_ULT, tgaPrstzAggUlt.copy());
    }

    public void setTgaPrstzAggUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_AGG_ULT, Pos.TGA_PRSTZ_AGG_ULT);
    }

    /**Original name: TGA-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getTgaPrstzAggUlt() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_AGG_ULT, Len.Int.TGA_PRSTZ_AGG_ULT, Len.Fract.TGA_PRSTZ_AGG_ULT);
    }

    public byte[] getTgaPrstzAggUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_AGG_ULT, Pos.TGA_PRSTZ_AGG_ULT);
        return buffer;
    }

    public void setTgaPrstzAggUltNull(String tgaPrstzAggUltNull) {
        writeString(Pos.TGA_PRSTZ_AGG_ULT_NULL, tgaPrstzAggUltNull, Len.TGA_PRSTZ_AGG_ULT_NULL);
    }

    /**Original name: TGA-PRSTZ-AGG-ULT-NULL<br>*/
    public String getTgaPrstzAggUltNull() {
        return readString(Pos.TGA_PRSTZ_AGG_ULT_NULL, Len.TGA_PRSTZ_AGG_ULT_NULL);
    }

    public String getTgaPrstzAggUltNullFormatted() {
        return Functions.padBlanks(getTgaPrstzAggUltNull(), Len.TGA_PRSTZ_AGG_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_AGG_ULT = 1;
        public static final int TGA_PRSTZ_AGG_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_AGG_ULT = 8;
        public static final int TGA_PRSTZ_AGG_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_AGG_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_AGG_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
