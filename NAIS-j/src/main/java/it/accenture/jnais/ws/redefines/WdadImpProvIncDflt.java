package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-IMP-PROV-INC-DFLT<br>
 * Variable: WDAD-IMP-PROV-INC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadImpProvIncDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadImpProvIncDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_IMP_PROV_INC_DFLT;
    }

    public void setWdadImpProvIncDflt(AfDecimal wdadImpProvIncDflt) {
        writeDecimalAsPacked(Pos.WDAD_IMP_PROV_INC_DFLT, wdadImpProvIncDflt.copy());
    }

    public void setWdadImpProvIncDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_IMP_PROV_INC_DFLT, Pos.WDAD_IMP_PROV_INC_DFLT);
    }

    /**Original name: WDAD-IMP-PROV-INC-DFLT<br>*/
    public AfDecimal getWdadImpProvIncDflt() {
        return readPackedAsDecimal(Pos.WDAD_IMP_PROV_INC_DFLT, Len.Int.WDAD_IMP_PROV_INC_DFLT, Len.Fract.WDAD_IMP_PROV_INC_DFLT);
    }

    public byte[] getWdadImpProvIncDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_IMP_PROV_INC_DFLT, Pos.WDAD_IMP_PROV_INC_DFLT);
        return buffer;
    }

    public void setWdadImpProvIncDfltNull(String wdadImpProvIncDfltNull) {
        writeString(Pos.WDAD_IMP_PROV_INC_DFLT_NULL, wdadImpProvIncDfltNull, Len.WDAD_IMP_PROV_INC_DFLT_NULL);
    }

    /**Original name: WDAD-IMP-PROV-INC-DFLT-NULL<br>*/
    public String getWdadImpProvIncDfltNull() {
        return readString(Pos.WDAD_IMP_PROV_INC_DFLT_NULL, Len.WDAD_IMP_PROV_INC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_PROV_INC_DFLT = 1;
        public static final int WDAD_IMP_PROV_INC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_PROV_INC_DFLT = 8;
        public static final int WDAD_IMP_PROV_INC_DFLT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_PROV_INC_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_PROV_INC_DFLT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
