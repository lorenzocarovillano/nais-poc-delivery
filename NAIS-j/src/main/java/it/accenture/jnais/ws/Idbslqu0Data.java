package it.accenture.jnais.ws;

import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndLiq;
import it.accenture.jnais.copy.TrchDiGarDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSLQU0<br>
 * Generated as a class for rule WS.<br>*/
public class Idbslqu0Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-BUFFER-TABLE
    private String wsBufferTable = "";
    //Original name: WS-ID-MOVI-CRZ
    private int wsIdMoviCrz = 0;
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-LIQ
    private IndLiq indLiq = new IndLiq();
    //Original name: LIQ-DB
    private TrchDiGarDb liqDb = new TrchDiGarDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsBufferTable(String wsBufferTable) {
        this.wsBufferTable = Functions.subString(wsBufferTable, Len.WS_BUFFER_TABLE);
    }

    public String getWsBufferTable() {
        return this.wsBufferTable;
    }

    public String getWsBufferTableFormatted() {
        return Functions.padBlanks(getWsBufferTable(), Len.WS_BUFFER_TABLE);
    }

    public void setWsIdMoviCrz(int wsIdMoviCrz) {
        this.wsIdMoviCrz = wsIdMoviCrz;
    }

    public int getWsIdMoviCrz() {
        return this.wsIdMoviCrz;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndLiq getIndLiq() {
        return indLiq;
    }

    public TrchDiGarDb getLiqDb() {
        return liqDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WS_BUFFER_TABLE = 2000;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
