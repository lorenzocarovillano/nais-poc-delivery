package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-PC-RIP-PAT-SET-IM<br>
 * Variable: P56-PC-RIP-PAT-SET-IM from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56PcRipPatSetIm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56PcRipPatSetIm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_PC_RIP_PAT_SET_IM;
    }

    public void setP56PcRipPatSetIm(AfDecimal p56PcRipPatSetIm) {
        writeDecimalAsPacked(Pos.P56_PC_RIP_PAT_SET_IM, p56PcRipPatSetIm.copy());
    }

    public void setP56PcRipPatSetImFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_PC_RIP_PAT_SET_IM, Pos.P56_PC_RIP_PAT_SET_IM);
    }

    /**Original name: P56-PC-RIP-PAT-SET-IM<br>*/
    public AfDecimal getP56PcRipPatSetIm() {
        return readPackedAsDecimal(Pos.P56_PC_RIP_PAT_SET_IM, Len.Int.P56_PC_RIP_PAT_SET_IM, Len.Fract.P56_PC_RIP_PAT_SET_IM);
    }

    public byte[] getP56PcRipPatSetImAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_PC_RIP_PAT_SET_IM, Pos.P56_PC_RIP_PAT_SET_IM);
        return buffer;
    }

    public void setP56PcRipPatSetImNull(String p56PcRipPatSetImNull) {
        writeString(Pos.P56_PC_RIP_PAT_SET_IM_NULL, p56PcRipPatSetImNull, Len.P56_PC_RIP_PAT_SET_IM_NULL);
    }

    /**Original name: P56-PC-RIP-PAT-SET-IM-NULL<br>*/
    public String getP56PcRipPatSetImNull() {
        return readString(Pos.P56_PC_RIP_PAT_SET_IM_NULL, Len.P56_PC_RIP_PAT_SET_IM_NULL);
    }

    public String getP56PcRipPatSetImNullFormatted() {
        return Functions.padBlanks(getP56PcRipPatSetImNull(), Len.P56_PC_RIP_PAT_SET_IM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_PC_RIP_PAT_SET_IM = 1;
        public static final int P56_PC_RIP_PAT_SET_IM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_PC_RIP_PAT_SET_IM = 4;
        public static final int P56_PC_RIP_PAT_SET_IM_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_PC_RIP_PAT_SET_IM = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_PC_RIP_PAT_SET_IM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
