package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-ULT<br>
 * Variable: TGA-PRSTZ-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_ULT;
    }

    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_ULT, tgaPrstzUlt.copy());
    }

    public void setTgaPrstzUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_ULT, Pos.TGA_PRSTZ_ULT);
    }

    /**Original name: TGA-PRSTZ-ULT<br>*/
    public AfDecimal getTgaPrstzUlt() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_ULT, Len.Int.TGA_PRSTZ_ULT, Len.Fract.TGA_PRSTZ_ULT);
    }

    public byte[] getTgaPrstzUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_ULT, Pos.TGA_PRSTZ_ULT);
        return buffer;
    }

    public void setTgaPrstzUltNull(String tgaPrstzUltNull) {
        writeString(Pos.TGA_PRSTZ_ULT_NULL, tgaPrstzUltNull, Len.TGA_PRSTZ_ULT_NULL);
    }

    /**Original name: TGA-PRSTZ-ULT-NULL<br>*/
    public String getTgaPrstzUltNull() {
        return readString(Pos.TGA_PRSTZ_ULT_NULL, Len.TGA_PRSTZ_ULT_NULL);
    }

    public String getTgaPrstzUltNullFormatted() {
        return Functions.padBlanks(getTgaPrstzUltNull(), Len.TGA_PRSTZ_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_ULT = 1;
        public static final int TGA_PRSTZ_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_ULT = 8;
        public static final int TGA_PRSTZ_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
