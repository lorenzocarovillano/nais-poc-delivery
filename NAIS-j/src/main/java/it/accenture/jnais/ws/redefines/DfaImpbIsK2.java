package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-IS-K2<br>
 * Variable: DFA-IMPB-IS-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbIsK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbIsK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_IS_K2;
    }

    public void setDfaImpbIsK2(AfDecimal dfaImpbIsK2) {
        writeDecimalAsPacked(Pos.DFA_IMPB_IS_K2, dfaImpbIsK2.copy());
    }

    public void setDfaImpbIsK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_IS_K2, Pos.DFA_IMPB_IS_K2);
    }

    /**Original name: DFA-IMPB-IS-K2<br>*/
    public AfDecimal getDfaImpbIsK2() {
        return readPackedAsDecimal(Pos.DFA_IMPB_IS_K2, Len.Int.DFA_IMPB_IS_K2, Len.Fract.DFA_IMPB_IS_K2);
    }

    public byte[] getDfaImpbIsK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_IS_K2, Pos.DFA_IMPB_IS_K2);
        return buffer;
    }

    public void setDfaImpbIsK2Null(String dfaImpbIsK2Null) {
        writeString(Pos.DFA_IMPB_IS_K2_NULL, dfaImpbIsK2Null, Len.DFA_IMPB_IS_K2_NULL);
    }

    /**Original name: DFA-IMPB-IS-K2-NULL<br>*/
    public String getDfaImpbIsK2Null() {
        return readString(Pos.DFA_IMPB_IS_K2_NULL, Len.DFA_IMPB_IS_K2_NULL);
    }

    public String getDfaImpbIsK2NullFormatted() {
        return Functions.padBlanks(getDfaImpbIsK2Null(), Len.DFA_IMPB_IS_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K2 = 1;
        public static final int DFA_IMPB_IS_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K2 = 8;
        public static final int DFA_IMPB_IS_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
