package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-COS-TUNNEL-USCITA<br>
 * Variable: S089-COS-TUNNEL-USCITA from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089CosTunnelUscita extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089CosTunnelUscita() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_COS_TUNNEL_USCITA;
    }

    public void setWlquCosTunnelUscita(AfDecimal wlquCosTunnelUscita) {
        writeDecimalAsPacked(Pos.S089_COS_TUNNEL_USCITA, wlquCosTunnelUscita.copy());
    }

    public void setWlquCosTunnelUscitaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_COS_TUNNEL_USCITA, Pos.S089_COS_TUNNEL_USCITA);
    }

    /**Original name: WLQU-COS-TUNNEL-USCITA<br>*/
    public AfDecimal getWlquCosTunnelUscita() {
        return readPackedAsDecimal(Pos.S089_COS_TUNNEL_USCITA, Len.Int.WLQU_COS_TUNNEL_USCITA, Len.Fract.WLQU_COS_TUNNEL_USCITA);
    }

    public byte[] getWlquCosTunnelUscitaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_COS_TUNNEL_USCITA, Pos.S089_COS_TUNNEL_USCITA);
        return buffer;
    }

    public void initWlquCosTunnelUscitaSpaces() {
        fill(Pos.S089_COS_TUNNEL_USCITA, Len.S089_COS_TUNNEL_USCITA, Types.SPACE_CHAR);
    }

    public void setWlquCosTunnelUscitaNull(String wlquCosTunnelUscitaNull) {
        writeString(Pos.S089_COS_TUNNEL_USCITA_NULL, wlquCosTunnelUscitaNull, Len.WLQU_COS_TUNNEL_USCITA_NULL);
    }

    /**Original name: WLQU-COS-TUNNEL-USCITA-NULL<br>*/
    public String getWlquCosTunnelUscitaNull() {
        return readString(Pos.S089_COS_TUNNEL_USCITA_NULL, Len.WLQU_COS_TUNNEL_USCITA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_COS_TUNNEL_USCITA = 1;
        public static final int S089_COS_TUNNEL_USCITA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_COS_TUNNEL_USCITA = 8;
        public static final int WLQU_COS_TUNNEL_USCITA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_COS_TUNNEL_USCITA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_COS_TUNNEL_USCITA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
