package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-ALTRI-SOPR<br>
 * Variable: WPAG-RATA-ALTRI-SOPR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataAltriSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataAltriSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_ALTRI_SOPR;
    }

    public void setWpagRataAltriSopr(AfDecimal wpagRataAltriSopr) {
        writeDecimalAsPacked(Pos.WPAG_RATA_ALTRI_SOPR, wpagRataAltriSopr.copy());
    }

    public void setWpagRataAltriSoprFormatted(String wpagRataAltriSopr) {
        setWpagRataAltriSopr(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_ALTRI_SOPR + Len.Fract.WPAG_RATA_ALTRI_SOPR, Len.Fract.WPAG_RATA_ALTRI_SOPR, wpagRataAltriSopr));
    }

    public void setWpagRataAltriSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_ALTRI_SOPR, Pos.WPAG_RATA_ALTRI_SOPR);
    }

    /**Original name: WPAG-RATA-ALTRI-SOPR<br>*/
    public AfDecimal getWpagRataAltriSopr() {
        return readPackedAsDecimal(Pos.WPAG_RATA_ALTRI_SOPR, Len.Int.WPAG_RATA_ALTRI_SOPR, Len.Fract.WPAG_RATA_ALTRI_SOPR);
    }

    public byte[] getWpagRataAltriSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_ALTRI_SOPR, Pos.WPAG_RATA_ALTRI_SOPR);
        return buffer;
    }

    public void initWpagRataAltriSoprSpaces() {
        fill(Pos.WPAG_RATA_ALTRI_SOPR, Len.WPAG_RATA_ALTRI_SOPR, Types.SPACE_CHAR);
    }

    public void setWpagRataAltriSoprNull(String wpagRataAltriSoprNull) {
        writeString(Pos.WPAG_RATA_ALTRI_SOPR_NULL, wpagRataAltriSoprNull, Len.WPAG_RATA_ALTRI_SOPR_NULL);
    }

    /**Original name: WPAG-RATA-ALTRI-SOPR-NULL<br>*/
    public String getWpagRataAltriSoprNull() {
        return readString(Pos.WPAG_RATA_ALTRI_SOPR_NULL, Len.WPAG_RATA_ALTRI_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ALTRI_SOPR = 1;
        public static final int WPAG_RATA_ALTRI_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ALTRI_SOPR = 8;
        public static final int WPAG_RATA_ALTRI_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ALTRI_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ALTRI_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
