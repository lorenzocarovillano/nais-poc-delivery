package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-IMP-CAR-INC-TDR<br>
 * Variable: WPAG-IMP-CAR-INC-TDR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarIncTdr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarIncTdr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_INC_TDR;
    }

    public void setWpagImpCarIncTdr(AfDecimal wpagImpCarIncTdr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_INC_TDR, wpagImpCarIncTdr.copy());
    }

    public void setWpagImpCarIncTdrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_INC_TDR, Pos.WPAG_IMP_CAR_INC_TDR);
    }

    /**Original name: WPAG-IMP-CAR-INC-TDR<br>*/
    public AfDecimal getWpagImpCarIncTdr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_INC_TDR, Len.Int.WPAG_IMP_CAR_INC_TDR, Len.Fract.WPAG_IMP_CAR_INC_TDR);
    }

    public byte[] getWpagImpCarIncTdrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_INC_TDR, Pos.WPAG_IMP_CAR_INC_TDR);
        return buffer;
    }

    public void initWpagImpCarIncTdrSpaces() {
        fill(Pos.WPAG_IMP_CAR_INC_TDR, Len.WPAG_IMP_CAR_INC_TDR, Types.SPACE_CHAR);
    }

    public void setWpagImpCarIncTdrNull(String wpagImpCarIncTdrNull) {
        writeString(Pos.WPAG_IMP_CAR_INC_TDR_NULL, wpagImpCarIncTdrNull, Len.WPAG_IMP_CAR_INC_TDR_NULL);
    }

    /**Original name: WPAG-IMP-CAR-INC-TDR-NULL<br>*/
    public String getWpagImpCarIncTdrNull() {
        return readString(Pos.WPAG_IMP_CAR_INC_TDR_NULL, Len.WPAG_IMP_CAR_INC_TDR_NULL);
    }

    public String getWpagImpCarIncTdrNullFormatted() {
        return Functions.padBlanks(getWpagImpCarIncTdrNull(), Len.WPAG_IMP_CAR_INC_TDR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_INC_TDR = 1;
        public static final int WPAG_IMP_CAR_INC_TDR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_INC_TDR = 8;
        public static final int WPAG_IMP_CAR_INC_TDR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_INC_TDR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_INC_TDR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
