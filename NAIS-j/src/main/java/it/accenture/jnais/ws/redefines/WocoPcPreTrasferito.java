package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-PC-PRE-TRASFERITO<br>
 * Variable: WOCO-PC-PRE-TRASFERITO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoPcPreTrasferito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoPcPreTrasferito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_PC_PRE_TRASFERITO;
    }

    public void setWocoPcPreTrasferito(AfDecimal wocoPcPreTrasferito) {
        writeDecimalAsPacked(Pos.WOCO_PC_PRE_TRASFERITO, wocoPcPreTrasferito.copy());
    }

    public void setWocoPcPreTrasferitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_PC_PRE_TRASFERITO, Pos.WOCO_PC_PRE_TRASFERITO);
    }

    /**Original name: WOCO-PC-PRE-TRASFERITO<br>*/
    public AfDecimal getWocoPcPreTrasferito() {
        return readPackedAsDecimal(Pos.WOCO_PC_PRE_TRASFERITO, Len.Int.WOCO_PC_PRE_TRASFERITO, Len.Fract.WOCO_PC_PRE_TRASFERITO);
    }

    public byte[] getWocoPcPreTrasferitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_PC_PRE_TRASFERITO, Pos.WOCO_PC_PRE_TRASFERITO);
        return buffer;
    }

    public void initWocoPcPreTrasferitoSpaces() {
        fill(Pos.WOCO_PC_PRE_TRASFERITO, Len.WOCO_PC_PRE_TRASFERITO, Types.SPACE_CHAR);
    }

    public void setWocoPcPreTrasferitoNull(String wocoPcPreTrasferitoNull) {
        writeString(Pos.WOCO_PC_PRE_TRASFERITO_NULL, wocoPcPreTrasferitoNull, Len.WOCO_PC_PRE_TRASFERITO_NULL);
    }

    /**Original name: WOCO-PC-PRE-TRASFERITO-NULL<br>*/
    public String getWocoPcPreTrasferitoNull() {
        return readString(Pos.WOCO_PC_PRE_TRASFERITO_NULL, Len.WOCO_PC_PRE_TRASFERITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_PC_PRE_TRASFERITO = 1;
        public static final int WOCO_PC_PRE_TRASFERITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_PC_PRE_TRASFERITO = 4;
        public static final int WOCO_PC_PRE_TRASFERITO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_PC_PRE_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_PC_PRE_TRASFERITO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
