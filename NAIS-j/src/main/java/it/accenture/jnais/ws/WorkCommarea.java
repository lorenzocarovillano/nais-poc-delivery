package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;

/**Original name: WORK-COMMAREA<br>
 * Variable: WORK-COMMAREA from program LOAS0310<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WorkCommarea extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_MOV_MAXOCCURS = 50;
    //Original name: VPMO-ELE-PARAM-MOV-MAX
    private short eleParamMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: VPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] tabParamMov = new WpmoTabParamMov[TAB_PARAM_MOV_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WorkCommarea() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WORK_COMMAREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWorkCommareaBytes(buf);
    }

    public void init() {
        for (int tabParamMovIdx = 1; tabParamMovIdx <= TAB_PARAM_MOV_MAXOCCURS; tabParamMovIdx++) {
            tabParamMov[tabParamMovIdx - 1] = new WpmoTabParamMov();
        }
    }

    public String getWorkCommareaFormatted() {
        return getAreaParamMoviFormatted();
    }

    public void setWorkCommareaBytes(byte[] buffer) {
        setWorkCommareaBytes(buffer, 1);
    }

    public byte[] getWorkCommareaBytes() {
        byte[] buffer = new byte[Len.WORK_COMMAREA];
        return getWorkCommareaBytes(buffer, 1);
    }

    public void setWorkCommareaBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaParamMoviBytes(buffer, position);
    }

    public byte[] getWorkCommareaBytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaParamMoviBytes(buffer, position);
        return buffer;
    }

    public String getAreaParamMoviFormatted() {
        return MarshalByteExt.bufferToStr(getAreaParamMoviBytes());
    }

    public void setAreaParamMoviBytes(byte[] buffer) {
        setAreaParamMoviBytes(buffer, 1);
    }

    /**Original name: VPMO-AREA-PARAM-MOVI<br>
	 * <pre> -- AREA APPOGGIO PARAMETRO MOVIMENTO</pre>*/
    public byte[] getAreaParamMoviBytes() {
        byte[] buffer = new byte[Len.AREA_PARAM_MOVI];
        return getAreaParamMoviBytes(buffer, 1);
    }

    public void setAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        eleParamMovMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                tabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public byte[] getAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleParamMovMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PARAM_MOV_MAXOCCURS; idx++) {
            tabParamMov[idx - 1].getWpmoTabParamMovBytes(buffer, position);
            position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        }
        return buffer;
    }

    public void setEleParamMovMax(short eleParamMovMax) {
        this.eleParamMovMax = eleParamMovMax;
    }

    public short getEleParamMovMax() {
        return this.eleParamMovMax;
    }

    public WpmoTabParamMov getTabParamMov(int idx) {
        return tabParamMov[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWorkCommareaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_PARAM_MOV_MAX = 2;
        public static final int AREA_PARAM_MOVI = ELE_PARAM_MOV_MAX + WorkCommarea.TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        public static final int WORK_COMMAREA = AREA_PARAM_MOVI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
