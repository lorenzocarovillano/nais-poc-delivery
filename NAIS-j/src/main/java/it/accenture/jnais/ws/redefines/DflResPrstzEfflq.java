package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RES-PRSTZ-EFFLQ<br>
 * Variable: DFL-RES-PRSTZ-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflResPrstzEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflResPrstzEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RES_PRSTZ_EFFLQ;
    }

    public void setDflResPrstzEfflq(AfDecimal dflResPrstzEfflq) {
        writeDecimalAsPacked(Pos.DFL_RES_PRSTZ_EFFLQ, dflResPrstzEfflq.copy());
    }

    public void setDflResPrstzEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RES_PRSTZ_EFFLQ, Pos.DFL_RES_PRSTZ_EFFLQ);
    }

    /**Original name: DFL-RES-PRSTZ-EFFLQ<br>*/
    public AfDecimal getDflResPrstzEfflq() {
        return readPackedAsDecimal(Pos.DFL_RES_PRSTZ_EFFLQ, Len.Int.DFL_RES_PRSTZ_EFFLQ, Len.Fract.DFL_RES_PRSTZ_EFFLQ);
    }

    public byte[] getDflResPrstzEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RES_PRSTZ_EFFLQ, Pos.DFL_RES_PRSTZ_EFFLQ);
        return buffer;
    }

    public void setDflResPrstzEfflqNull(String dflResPrstzEfflqNull) {
        writeString(Pos.DFL_RES_PRSTZ_EFFLQ_NULL, dflResPrstzEfflqNull, Len.DFL_RES_PRSTZ_EFFLQ_NULL);
    }

    /**Original name: DFL-RES-PRSTZ-EFFLQ-NULL<br>*/
    public String getDflResPrstzEfflqNull() {
        return readString(Pos.DFL_RES_PRSTZ_EFFLQ_NULL, Len.DFL_RES_PRSTZ_EFFLQ_NULL);
    }

    public String getDflResPrstzEfflqNullFormatted() {
        return Functions.padBlanks(getDflResPrstzEfflqNull(), Len.DFL_RES_PRSTZ_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRSTZ_EFFLQ = 1;
        public static final int DFL_RES_PRSTZ_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRSTZ_EFFLQ = 8;
        public static final int DFL_RES_PRSTZ_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRSTZ_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRSTZ_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
