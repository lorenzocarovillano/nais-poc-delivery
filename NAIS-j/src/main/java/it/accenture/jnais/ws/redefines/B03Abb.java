package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ABB<br>
 * Variable: B03-ABB from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Abb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Abb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ABB;
    }

    public void setB03Abb(AfDecimal b03Abb) {
        writeDecimalAsPacked(Pos.B03_ABB, b03Abb.copy());
    }

    public void setB03AbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ABB, Pos.B03_ABB);
    }

    /**Original name: B03-ABB<br>*/
    public AfDecimal getB03Abb() {
        return readPackedAsDecimal(Pos.B03_ABB, Len.Int.B03_ABB, Len.Fract.B03_ABB);
    }

    public byte[] getB03AbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ABB, Pos.B03_ABB);
        return buffer;
    }

    public void setB03AbbNull(String b03AbbNull) {
        writeString(Pos.B03_ABB_NULL, b03AbbNull, Len.B03_ABB_NULL);
    }

    /**Original name: B03-ABB-NULL<br>*/
    public String getB03AbbNull() {
        return readString(Pos.B03_ABB_NULL, Len.B03_ABB_NULL);
    }

    public String getB03AbbNullFormatted() {
        return Functions.padBlanks(getB03AbbNull(), Len.B03_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ABB = 1;
        public static final int B03_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ABB = 8;
        public static final int B03_ABB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ABB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ABB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
