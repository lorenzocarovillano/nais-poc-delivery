package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-ID-3O-ASSTO<br>
 * Variable: L3401-ID-3O-ASSTO from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401Id3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401Id3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_ID3O_ASSTO;
    }

    public void setL3401Id3oAssto(int l3401Id3oAssto) {
        writeIntAsPacked(Pos.L3401_ID3O_ASSTO, l3401Id3oAssto, Len.Int.L3401_ID3O_ASSTO);
    }

    public void setL3401Id3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_ID3O_ASSTO, Pos.L3401_ID3O_ASSTO);
    }

    /**Original name: L3401-ID-3O-ASSTO<br>*/
    public int getL3401Id3oAssto() {
        return readPackedAsInt(Pos.L3401_ID3O_ASSTO, Len.Int.L3401_ID3O_ASSTO);
    }

    public byte[] getL3401Id3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_ID3O_ASSTO, Pos.L3401_ID3O_ASSTO);
        return buffer;
    }

    /**Original name: L3401-ID-3O-ASSTO-NULL<br>*/
    public String getL3401Id3oAsstoNull() {
        return readString(Pos.L3401_ID3O_ASSTO_NULL, Len.L3401_ID3O_ASSTO_NULL);
    }

    public String getL3401Id3oAsstoNullFormatted() {
        return Functions.padBlanks(getL3401Id3oAsstoNull(), Len.L3401_ID3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_ID3O_ASSTO = 1;
        public static final int L3401_ID3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ID3O_ASSTO = 5;
        public static final int L3401_ID3O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ID3O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
