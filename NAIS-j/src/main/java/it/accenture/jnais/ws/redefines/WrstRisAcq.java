package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-ACQ<br>
 * Variable: WRST-RIS-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_ACQ;
    }

    public void setWrstRisAcq(AfDecimal wrstRisAcq) {
        writeDecimalAsPacked(Pos.WRST_RIS_ACQ, wrstRisAcq.copy());
    }

    public void setWrstRisAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_ACQ, Pos.WRST_RIS_ACQ);
    }

    /**Original name: WRST-RIS-ACQ<br>*/
    public AfDecimal getWrstRisAcq() {
        return readPackedAsDecimal(Pos.WRST_RIS_ACQ, Len.Int.WRST_RIS_ACQ, Len.Fract.WRST_RIS_ACQ);
    }

    public byte[] getWrstRisAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_ACQ, Pos.WRST_RIS_ACQ);
        return buffer;
    }

    public void initWrstRisAcqSpaces() {
        fill(Pos.WRST_RIS_ACQ, Len.WRST_RIS_ACQ, Types.SPACE_CHAR);
    }

    public void setWrstRisAcqNull(String wrstRisAcqNull) {
        writeString(Pos.WRST_RIS_ACQ_NULL, wrstRisAcqNull, Len.WRST_RIS_ACQ_NULL);
    }

    /**Original name: WRST-RIS-ACQ-NULL<br>*/
    public String getWrstRisAcqNull() {
        return readString(Pos.WRST_RIS_ACQ_NULL, Len.WRST_RIS_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_ACQ = 1;
        public static final int WRST_RIS_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_ACQ = 8;
        public static final int WRST_RIS_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
