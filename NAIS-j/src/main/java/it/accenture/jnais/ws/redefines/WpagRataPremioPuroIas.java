package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-PREMIO-PURO-IAS<br>
 * Variable: WPAG-RATA-PREMIO-PURO-IAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataPremioPuroIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataPremioPuroIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_PREMIO_PURO_IAS;
    }

    public void setWpagRataPremioPuroIas(AfDecimal wpagRataPremioPuroIas) {
        writeDecimalAsPacked(Pos.WPAG_RATA_PREMIO_PURO_IAS, wpagRataPremioPuroIas.copy());
    }

    public void setWpagRataPremioPuroIasFormatted(String wpagRataPremioPuroIas) {
        setWpagRataPremioPuroIas(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_PREMIO_PURO_IAS + Len.Fract.WPAG_RATA_PREMIO_PURO_IAS, Len.Fract.WPAG_RATA_PREMIO_PURO_IAS, wpagRataPremioPuroIas));
    }

    public void setWpagRataPremioPuroIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_PREMIO_PURO_IAS, Pos.WPAG_RATA_PREMIO_PURO_IAS);
    }

    /**Original name: WPAG-RATA-PREMIO-PURO-IAS<br>*/
    public AfDecimal getWpagRataPremioPuroIas() {
        return readPackedAsDecimal(Pos.WPAG_RATA_PREMIO_PURO_IAS, Len.Int.WPAG_RATA_PREMIO_PURO_IAS, Len.Fract.WPAG_RATA_PREMIO_PURO_IAS);
    }

    public byte[] getWpagRataPremioPuroIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_PREMIO_PURO_IAS, Pos.WPAG_RATA_PREMIO_PURO_IAS);
        return buffer;
    }

    public void initWpagRataPremioPuroIasSpaces() {
        fill(Pos.WPAG_RATA_PREMIO_PURO_IAS, Len.WPAG_RATA_PREMIO_PURO_IAS, Types.SPACE_CHAR);
    }

    public void setWpagRataPremioPuroIasNull(String wpagRataPremioPuroIasNull) {
        writeString(Pos.WPAG_RATA_PREMIO_PURO_IAS_NULL, wpagRataPremioPuroIasNull, Len.WPAG_RATA_PREMIO_PURO_IAS_NULL);
    }

    /**Original name: WPAG-RATA-PREMIO-PURO-IAS-NULL<br>*/
    public String getWpagRataPremioPuroIasNull() {
        return readString(Pos.WPAG_RATA_PREMIO_PURO_IAS_NULL, Len.WPAG_RATA_PREMIO_PURO_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_PURO_IAS = 1;
        public static final int WPAG_RATA_PREMIO_PURO_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_PREMIO_PURO_IAS = 8;
        public static final int WPAG_RATA_PREMIO_PURO_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_PURO_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_PREMIO_PURO_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
