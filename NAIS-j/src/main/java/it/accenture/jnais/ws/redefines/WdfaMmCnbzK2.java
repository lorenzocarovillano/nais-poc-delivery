package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-MM-CNBZ-K2<br>
 * Variable: WDFA-MM-CNBZ-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMmCnbzK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMmCnbzK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MM_CNBZ_K2;
    }

    public void setWdfaMmCnbzK2(short wdfaMmCnbzK2) {
        writeShortAsPacked(Pos.WDFA_MM_CNBZ_K2, wdfaMmCnbzK2, Len.Int.WDFA_MM_CNBZ_K2);
    }

    public void setWdfaMmCnbzK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MM_CNBZ_K2, Pos.WDFA_MM_CNBZ_K2);
    }

    /**Original name: WDFA-MM-CNBZ-K2<br>*/
    public short getWdfaMmCnbzK2() {
        return readPackedAsShort(Pos.WDFA_MM_CNBZ_K2, Len.Int.WDFA_MM_CNBZ_K2);
    }

    public byte[] getWdfaMmCnbzK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MM_CNBZ_K2, Pos.WDFA_MM_CNBZ_K2);
        return buffer;
    }

    public void setWdfaMmCnbzK2Null(String wdfaMmCnbzK2Null) {
        writeString(Pos.WDFA_MM_CNBZ_K2_NULL, wdfaMmCnbzK2Null, Len.WDFA_MM_CNBZ_K2_NULL);
    }

    /**Original name: WDFA-MM-CNBZ-K2-NULL<br>*/
    public String getWdfaMmCnbzK2Null() {
        return readString(Pos.WDFA_MM_CNBZ_K2_NULL, Len.WDFA_MM_CNBZ_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MM_CNBZ_K2 = 1;
        public static final int WDFA_MM_CNBZ_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MM_CNBZ_K2 = 3;
        public static final int WDFA_MM_CNBZ_K2_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MM_CNBZ_K2 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
