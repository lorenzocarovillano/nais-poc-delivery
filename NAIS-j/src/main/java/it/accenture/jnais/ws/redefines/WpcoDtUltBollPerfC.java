package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-PERF-C<br>
 * Variable: WPCO-DT-ULT-BOLL-PERF-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollPerfC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollPerfC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_PERF_C;
    }

    public void setWpcoDtUltBollPerfC(int wpcoDtUltBollPerfC) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_PERF_C, wpcoDtUltBollPerfC, Len.Int.WPCO_DT_ULT_BOLL_PERF_C);
    }

    public void setDpcoDtUltBollPerfCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PERF_C, Pos.WPCO_DT_ULT_BOLL_PERF_C);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PERF-C<br>*/
    public int getWpcoDtUltBollPerfC() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_PERF_C, Len.Int.WPCO_DT_ULT_BOLL_PERF_C);
    }

    public byte[] getWpcoDtUltBollPerfCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PERF_C, Pos.WPCO_DT_ULT_BOLL_PERF_C);
        return buffer;
    }

    public void setWpcoDtUltBollPerfCNull(String wpcoDtUltBollPerfCNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_PERF_C_NULL, wpcoDtUltBollPerfCNull, Len.WPCO_DT_ULT_BOLL_PERF_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PERF-C-NULL<br>*/
    public String getWpcoDtUltBollPerfCNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_PERF_C_NULL, Len.WPCO_DT_ULT_BOLL_PERF_C_NULL);
    }

    public String getWpcoDtUltBollPerfCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollPerfCNull(), Len.WPCO_DT_ULT_BOLL_PERF_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PERF_C = 1;
        public static final int WPCO_DT_ULT_BOLL_PERF_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PERF_C = 5;
        public static final int WPCO_DT_ULT_BOLL_PERF_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_PERF_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
