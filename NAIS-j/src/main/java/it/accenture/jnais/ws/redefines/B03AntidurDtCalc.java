package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ANTIDUR-DT-CALC<br>
 * Variable: B03-ANTIDUR-DT-CALC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AntidurDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AntidurDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ANTIDUR_DT_CALC;
    }

    public void setB03AntidurDtCalc(AfDecimal b03AntidurDtCalc) {
        writeDecimalAsPacked(Pos.B03_ANTIDUR_DT_CALC, b03AntidurDtCalc.copy());
    }

    public void setB03AntidurDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ANTIDUR_DT_CALC, Pos.B03_ANTIDUR_DT_CALC);
    }

    /**Original name: B03-ANTIDUR-DT-CALC<br>*/
    public AfDecimal getB03AntidurDtCalc() {
        return readPackedAsDecimal(Pos.B03_ANTIDUR_DT_CALC, Len.Int.B03_ANTIDUR_DT_CALC, Len.Fract.B03_ANTIDUR_DT_CALC);
    }

    public byte[] getB03AntidurDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ANTIDUR_DT_CALC, Pos.B03_ANTIDUR_DT_CALC);
        return buffer;
    }

    public void setB03AntidurDtCalcNull(String b03AntidurDtCalcNull) {
        writeString(Pos.B03_ANTIDUR_DT_CALC_NULL, b03AntidurDtCalcNull, Len.B03_ANTIDUR_DT_CALC_NULL);
    }

    /**Original name: B03-ANTIDUR-DT-CALC-NULL<br>*/
    public String getB03AntidurDtCalcNull() {
        return readString(Pos.B03_ANTIDUR_DT_CALC_NULL, Len.B03_ANTIDUR_DT_CALC_NULL);
    }

    public String getB03AntidurDtCalcNullFormatted() {
        return Functions.padBlanks(getB03AntidurDtCalcNull(), Len.B03_ANTIDUR_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ANTIDUR_DT_CALC = 1;
        public static final int B03_ANTIDUR_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ANTIDUR_DT_CALC = 6;
        public static final int B03_ANTIDUR_DT_CALC_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ANTIDUR_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ANTIDUR_DT_CALC = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
