package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-ALQ-PRVR<br>
 * Variable: WDFA-ALQ-PRVR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAlqPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAlqPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ALQ_PRVR;
    }

    public void setWdfaAlqPrvr(AfDecimal wdfaAlqPrvr) {
        writeDecimalAsPacked(Pos.WDFA_ALQ_PRVR, wdfaAlqPrvr.copy());
    }

    public void setWdfaAlqPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ALQ_PRVR, Pos.WDFA_ALQ_PRVR);
    }

    /**Original name: WDFA-ALQ-PRVR<br>*/
    public AfDecimal getWdfaAlqPrvr() {
        return readPackedAsDecimal(Pos.WDFA_ALQ_PRVR, Len.Int.WDFA_ALQ_PRVR, Len.Fract.WDFA_ALQ_PRVR);
    }

    public byte[] getWdfaAlqPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ALQ_PRVR, Pos.WDFA_ALQ_PRVR);
        return buffer;
    }

    public void setWdfaAlqPrvrNull(String wdfaAlqPrvrNull) {
        writeString(Pos.WDFA_ALQ_PRVR_NULL, wdfaAlqPrvrNull, Len.WDFA_ALQ_PRVR_NULL);
    }

    /**Original name: WDFA-ALQ-PRVR-NULL<br>*/
    public String getWdfaAlqPrvrNull() {
        return readString(Pos.WDFA_ALQ_PRVR_NULL, Len.WDFA_ALQ_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ALQ_PRVR = 1;
        public static final int WDFA_ALQ_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ALQ_PRVR = 4;
        public static final int WDFA_ALQ_PRVR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_ALQ_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ALQ_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
