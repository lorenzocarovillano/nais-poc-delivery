package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-C-SUBRSH-T<br>
 * Variable: WB03-C-SUBRSH-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CSubrshT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CSubrshT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_C_SUBRSH_T;
    }

    public void setWb03CSubrshTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_C_SUBRSH_T, Pos.WB03_C_SUBRSH_T);
    }

    /**Original name: WB03-C-SUBRSH-T<br>*/
    public AfDecimal getWb03CSubrshT() {
        return readPackedAsDecimal(Pos.WB03_C_SUBRSH_T, Len.Int.WB03_C_SUBRSH_T, Len.Fract.WB03_C_SUBRSH_T);
    }

    public byte[] getWb03CSubrshTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_C_SUBRSH_T, Pos.WB03_C_SUBRSH_T);
        return buffer;
    }

    public void setWb03CSubrshTNull(String wb03CSubrshTNull) {
        writeString(Pos.WB03_C_SUBRSH_T_NULL, wb03CSubrshTNull, Len.WB03_C_SUBRSH_T_NULL);
    }

    /**Original name: WB03-C-SUBRSH-T-NULL<br>*/
    public String getWb03CSubrshTNull() {
        return readString(Pos.WB03_C_SUBRSH_T_NULL, Len.WB03_C_SUBRSH_T_NULL);
    }

    public String getWb03CSubrshTNullFormatted() {
        return Functions.padBlanks(getWb03CSubrshTNull(), Len.WB03_C_SUBRSH_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_C_SUBRSH_T = 1;
        public static final int WB03_C_SUBRSH_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_C_SUBRSH_T = 8;
        public static final int WB03_C_SUBRSH_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_C_SUBRSH_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_C_SUBRSH_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
