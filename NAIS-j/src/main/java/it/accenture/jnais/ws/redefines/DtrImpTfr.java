package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-IMP-TFR<br>
 * Variable: DTR-IMP-TFR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_IMP_TFR;
    }

    public void setDtrImpTfr(AfDecimal dtrImpTfr) {
        writeDecimalAsPacked(Pos.DTR_IMP_TFR, dtrImpTfr.copy());
    }

    public void setDtrImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_IMP_TFR, Pos.DTR_IMP_TFR);
    }

    /**Original name: DTR-IMP-TFR<br>*/
    public AfDecimal getDtrImpTfr() {
        return readPackedAsDecimal(Pos.DTR_IMP_TFR, Len.Int.DTR_IMP_TFR, Len.Fract.DTR_IMP_TFR);
    }

    public byte[] getDtrImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_IMP_TFR, Pos.DTR_IMP_TFR);
        return buffer;
    }

    public void setDtrImpTfrNull(String dtrImpTfrNull) {
        writeString(Pos.DTR_IMP_TFR_NULL, dtrImpTfrNull, Len.DTR_IMP_TFR_NULL);
    }

    /**Original name: DTR-IMP-TFR-NULL<br>*/
    public String getDtrImpTfrNull() {
        return readString(Pos.DTR_IMP_TFR_NULL, Len.DTR_IMP_TFR_NULL);
    }

    public String getDtrImpTfrNullFormatted() {
        return Functions.padBlanks(getDtrImpTfrNull(), Len.DTR_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_IMP_TFR = 1;
        public static final int DTR_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_IMP_TFR = 8;
        public static final int DTR_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
