package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-CNBT-ANTIRAC<br>
 * Variable: DTR-CNBT-ANTIRAC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_CNBT_ANTIRAC;
    }

    public void setDtrCnbtAntirac(AfDecimal dtrCnbtAntirac) {
        writeDecimalAsPacked(Pos.DTR_CNBT_ANTIRAC, dtrCnbtAntirac.copy());
    }

    public void setDtrCnbtAntiracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_CNBT_ANTIRAC, Pos.DTR_CNBT_ANTIRAC);
    }

    /**Original name: DTR-CNBT-ANTIRAC<br>*/
    public AfDecimal getDtrCnbtAntirac() {
        return readPackedAsDecimal(Pos.DTR_CNBT_ANTIRAC, Len.Int.DTR_CNBT_ANTIRAC, Len.Fract.DTR_CNBT_ANTIRAC);
    }

    public byte[] getDtrCnbtAntiracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_CNBT_ANTIRAC, Pos.DTR_CNBT_ANTIRAC);
        return buffer;
    }

    public void setDtrCnbtAntiracNull(String dtrCnbtAntiracNull) {
        writeString(Pos.DTR_CNBT_ANTIRAC_NULL, dtrCnbtAntiracNull, Len.DTR_CNBT_ANTIRAC_NULL);
    }

    /**Original name: DTR-CNBT-ANTIRAC-NULL<br>*/
    public String getDtrCnbtAntiracNull() {
        return readString(Pos.DTR_CNBT_ANTIRAC_NULL, Len.DTR_CNBT_ANTIRAC_NULL);
    }

    public String getDtrCnbtAntiracNullFormatted() {
        return Functions.padBlanks(getDtrCnbtAntiracNull(), Len.DTR_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_CNBT_ANTIRAC = 1;
        public static final int DTR_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_CNBT_ANTIRAC = 8;
        public static final int DTR_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
