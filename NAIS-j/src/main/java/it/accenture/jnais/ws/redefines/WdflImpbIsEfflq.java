package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-EFFLQ<br>
 * Variable: WDFL-IMPB-IS-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIsEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIsEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS_EFFLQ;
    }

    public void setWdflImpbIsEfflq(AfDecimal wdflImpbIsEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS_EFFLQ, wdflImpbIsEfflq.copy());
    }

    public void setWdflImpbIsEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS_EFFLQ, Pos.WDFL_IMPB_IS_EFFLQ);
    }

    /**Original name: WDFL-IMPB-IS-EFFLQ<br>*/
    public AfDecimal getWdflImpbIsEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS_EFFLQ, Len.Int.WDFL_IMPB_IS_EFFLQ, Len.Fract.WDFL_IMPB_IS_EFFLQ);
    }

    public byte[] getWdflImpbIsEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS_EFFLQ, Pos.WDFL_IMPB_IS_EFFLQ);
        return buffer;
    }

    public void setWdflImpbIsEfflqNull(String wdflImpbIsEfflqNull) {
        writeString(Pos.WDFL_IMPB_IS_EFFLQ_NULL, wdflImpbIsEfflqNull, Len.WDFL_IMPB_IS_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPB-IS-EFFLQ-NULL<br>*/
    public String getWdflImpbIsEfflqNull() {
        return readString(Pos.WDFL_IMPB_IS_EFFLQ_NULL, Len.WDFL_IMPB_IS_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS_EFFLQ = 1;
        public static final int WDFL_IMPB_IS_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS_EFFLQ = 8;
        public static final int WDFL_IMPB_IS_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
