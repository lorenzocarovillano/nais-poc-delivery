package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-ID-OGG<br>
 * Variable: PCA-ID-OGG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_ID_OGG;
    }

    public void setPcaIdOgg(int pcaIdOgg) {
        writeIntAsPacked(Pos.PCA_ID_OGG, pcaIdOgg, Len.Int.PCA_ID_OGG);
    }

    public void setPcaIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_ID_OGG, Pos.PCA_ID_OGG);
    }

    /**Original name: PCA-ID-OGG<br>*/
    public int getPcaIdOgg() {
        return readPackedAsInt(Pos.PCA_ID_OGG, Len.Int.PCA_ID_OGG);
    }

    public byte[] getPcaIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_ID_OGG, Pos.PCA_ID_OGG);
        return buffer;
    }

    public void setPcaIdOggNull(String pcaIdOggNull) {
        writeString(Pos.PCA_ID_OGG_NULL, pcaIdOggNull, Len.PCA_ID_OGG_NULL);
    }

    /**Original name: PCA-ID-OGG-NULL<br>*/
    public String getPcaIdOggNull() {
        return readString(Pos.PCA_ID_OGG_NULL, Len.PCA_ID_OGG_NULL);
    }

    public String getPcaIdOggNullFormatted() {
        return Functions.padBlanks(getPcaIdOggNull(), Len.PCA_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_ID_OGG = 1;
        public static final int PCA_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_ID_OGG = 5;
        public static final int PCA_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
