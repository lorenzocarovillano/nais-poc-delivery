package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: C214-SALTA-VALORIZZAZIONE<br>
 * Variable: C214-SALTA-VALORIZZAZIONE from copybook IVVC0213<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class C214SaltaValorizzazione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setSaltaValorizzazione(char saltaValorizzazione) {
        this.value = saltaValorizzazione;
    }

    public char getSaltaValorizzazione() {
        return this.value;
    }

    public void setIvvc0213SaltaSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SALTA_VALORIZZAZIONE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
