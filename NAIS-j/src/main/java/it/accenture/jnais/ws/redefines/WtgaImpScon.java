package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-SCON<br>
 * Variable: WTGA-IMP-SCON from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_SCON;
    }

    public void setWtgaImpScon(AfDecimal wtgaImpScon) {
        writeDecimalAsPacked(Pos.WTGA_IMP_SCON, wtgaImpScon.copy());
    }

    public void setWtgaImpSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_SCON, Pos.WTGA_IMP_SCON);
    }

    /**Original name: WTGA-IMP-SCON<br>*/
    public AfDecimal getWtgaImpScon() {
        return readPackedAsDecimal(Pos.WTGA_IMP_SCON, Len.Int.WTGA_IMP_SCON, Len.Fract.WTGA_IMP_SCON);
    }

    public byte[] getWtgaImpSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_SCON, Pos.WTGA_IMP_SCON);
        return buffer;
    }

    public void initWtgaImpSconSpaces() {
        fill(Pos.WTGA_IMP_SCON, Len.WTGA_IMP_SCON, Types.SPACE_CHAR);
    }

    public void setWtgaImpSconNull(String wtgaImpSconNull) {
        writeString(Pos.WTGA_IMP_SCON_NULL, wtgaImpSconNull, Len.WTGA_IMP_SCON_NULL);
    }

    /**Original name: WTGA-IMP-SCON-NULL<br>*/
    public String getWtgaImpSconNull() {
        return readString(Pos.WTGA_IMP_SCON_NULL, Len.WTGA_IMP_SCON_NULL);
    }

    public String getWtgaImpSconNullFormatted() {
        return Functions.padBlanks(getWtgaImpSconNull(), Len.WTGA_IMP_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SCON = 1;
        public static final int WTGA_IMP_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SCON = 8;
        public static final int WTGA_IMP_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
