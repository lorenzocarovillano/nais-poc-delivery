package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-SOVRAM<br>
 * Variable: WPAG-SOVRAM from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagSovram extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagSovram() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_SOVRAM;
    }

    public void setWpagSovramFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_SOVRAM, Pos.WPAG_SOVRAM);
    }

    public byte[] getWpagSovramAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_SOVRAM, Pos.WPAG_SOVRAM);
        return buffer;
    }

    public void initWpagSovramSpaces() {
        fill(Pos.WPAG_SOVRAM, Len.WPAG_SOVRAM, Types.SPACE_CHAR);
    }

    public void setWpagSovramNull(String wpagSovramNull) {
        writeString(Pos.WPAG_SOVRAM_NULL, wpagSovramNull, Len.WPAG_SOVRAM_NULL);
    }

    /**Original name: WPAG-SOVRAM-NULL<br>*/
    public String getWpagSovramNull() {
        return readString(Pos.WPAG_SOVRAM_NULL, Len.WPAG_SOVRAM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_SOVRAM = 1;
        public static final int WPAG_SOVRAM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_SOVRAM = 4;
        public static final int WPAG_SOVRAM_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
