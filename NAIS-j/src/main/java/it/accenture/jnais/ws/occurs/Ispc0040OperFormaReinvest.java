package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0040-OPER-FORMA-REINVEST<br>
 * Variables: ISPC0040-OPER-FORMA-REINVEST from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040OperFormaReinvest {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-OPER-REINV
    private String codOperReinv = DefaultValues.stringVal(Len.COD_OPER_REINV);
    //Original name: ISPC0040-COD-FORMA-REINV
    private String codFormaReinv = DefaultValues.stringVal(Len.COD_FORMA_REINV);
    //Original name: ISPC0040-PERC-REINV
    private AfDecimal percReinv = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);

    //==== METHODS ====
    public void setOperFormaReinvestBytes(byte[] buffer, int offset) {
        int position = offset;
        codOperReinv = MarshalByte.readFixedString(buffer, position, Len.COD_OPER_REINV);
        position += Len.COD_OPER_REINV;
        codFormaReinv = MarshalByte.readFixedString(buffer, position, Len.COD_FORMA_REINV);
        position += Len.COD_FORMA_REINV;
        percReinv.assign(MarshalByte.readDecimal(buffer, position, Len.Int.PERC_REINV, Len.Fract.PERC_REINV, SignType.NO_SIGN));
    }

    public byte[] getOperFormaReinvestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codOperReinv, Len.COD_OPER_REINV);
        position += Len.COD_OPER_REINV;
        MarshalByte.writeString(buffer, position, codFormaReinv, Len.COD_FORMA_REINV);
        position += Len.COD_FORMA_REINV;
        MarshalByte.writeDecimal(buffer, position, percReinv.copy(), SignType.NO_SIGN);
        return buffer;
    }

    public void initOperFormaReinvestSpaces() {
        codOperReinv = "";
        codFormaReinv = "";
        percReinv.setNaN();
    }

    public void setIspc0040CodOperReinvFormatted(String ispc0040CodOperReinv) {
        this.codOperReinv = Trunc.toUnsignedNumeric(ispc0040CodOperReinv, Len.COD_OPER_REINV);
    }

    public short getIspc0040CodOperReinv() {
        return NumericDisplay.asShort(this.codOperReinv);
    }

    public void setIspc0040CodFormaReinvFormatted(String ispc0040CodFormaReinv) {
        this.codFormaReinv = Trunc.toUnsignedNumeric(ispc0040CodFormaReinv, Len.COD_FORMA_REINV);
    }

    public short getIspc0040CodFormaReinv() {
        return NumericDisplay.asShort(this.codFormaReinv);
    }

    public void setPercReinv(AfDecimal percReinv) {
        this.percReinv.assign(percReinv);
    }

    public AfDecimal getPercReinv() {
        return this.percReinv.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_OPER_REINV = 4;
        public static final int COD_FORMA_REINV = 4;
        public static final int PERC_REINV = 14;
        public static final int OPER_FORMA_REINVEST = COD_OPER_REINV + COD_FORMA_REINV + PERC_REINV;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PERC_REINV = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PERC_REINV = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
