package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-PC-C-SUBRSH-MARSOL<br>
 * Variable: WPCO-PC-C-SUBRSH-MARSOL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoPcCSubrshMarsol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoPcCSubrshMarsol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_PC_C_SUBRSH_MARSOL;
    }

    public void setWpcoPcCSubrshMarsol(AfDecimal wpcoPcCSubrshMarsol) {
        writeDecimalAsPacked(Pos.WPCO_PC_C_SUBRSH_MARSOL, wpcoPcCSubrshMarsol.copy());
    }

    public void setDpcoPcCSubrshMarsolFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_PC_C_SUBRSH_MARSOL, Pos.WPCO_PC_C_SUBRSH_MARSOL);
    }

    /**Original name: WPCO-PC-C-SUBRSH-MARSOL<br>*/
    public AfDecimal getWpcoPcCSubrshMarsol() {
        return readPackedAsDecimal(Pos.WPCO_PC_C_SUBRSH_MARSOL, Len.Int.WPCO_PC_C_SUBRSH_MARSOL, Len.Fract.WPCO_PC_C_SUBRSH_MARSOL);
    }

    public byte[] getWpcoPcCSubrshMarsolAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_PC_C_SUBRSH_MARSOL, Pos.WPCO_PC_C_SUBRSH_MARSOL);
        return buffer;
    }

    public void setWpcoPcCSubrshMarsolNull(String wpcoPcCSubrshMarsolNull) {
        writeString(Pos.WPCO_PC_C_SUBRSH_MARSOL_NULL, wpcoPcCSubrshMarsolNull, Len.WPCO_PC_C_SUBRSH_MARSOL_NULL);
    }

    /**Original name: WPCO-PC-C-SUBRSH-MARSOL-NULL<br>*/
    public String getWpcoPcCSubrshMarsolNull() {
        return readString(Pos.WPCO_PC_C_SUBRSH_MARSOL_NULL, Len.WPCO_PC_C_SUBRSH_MARSOL_NULL);
    }

    public String getWpcoPcCSubrshMarsolNullFormatted() {
        return Functions.padBlanks(getWpcoPcCSubrshMarsolNull(), Len.WPCO_PC_C_SUBRSH_MARSOL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_PC_C_SUBRSH_MARSOL = 1;
        public static final int WPCO_PC_C_SUBRSH_MARSOL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_PC_C_SUBRSH_MARSOL = 4;
        public static final int WPCO_PC_C_SUBRSH_MARSOL_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_PC_C_SUBRSH_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_PC_C_SUBRSH_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
