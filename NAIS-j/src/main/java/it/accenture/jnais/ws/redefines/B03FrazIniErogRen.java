package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-FRAZ-INI-EROG-REN<br>
 * Variable: B03-FRAZ-INI-EROG-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03FrazIniErogRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03FrazIniErogRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_FRAZ_INI_EROG_REN;
    }

    public void setB03FrazIniErogRen(int b03FrazIniErogRen) {
        writeIntAsPacked(Pos.B03_FRAZ_INI_EROG_REN, b03FrazIniErogRen, Len.Int.B03_FRAZ_INI_EROG_REN);
    }

    public void setB03FrazIniErogRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_FRAZ_INI_EROG_REN, Pos.B03_FRAZ_INI_EROG_REN);
    }

    /**Original name: B03-FRAZ-INI-EROG-REN<br>*/
    public int getB03FrazIniErogRen() {
        return readPackedAsInt(Pos.B03_FRAZ_INI_EROG_REN, Len.Int.B03_FRAZ_INI_EROG_REN);
    }

    public byte[] getB03FrazIniErogRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_FRAZ_INI_EROG_REN, Pos.B03_FRAZ_INI_EROG_REN);
        return buffer;
    }

    public void setB03FrazIniErogRenNull(String b03FrazIniErogRenNull) {
        writeString(Pos.B03_FRAZ_INI_EROG_REN_NULL, b03FrazIniErogRenNull, Len.B03_FRAZ_INI_EROG_REN_NULL);
    }

    /**Original name: B03-FRAZ-INI-EROG-REN-NULL<br>*/
    public String getB03FrazIniErogRenNull() {
        return readString(Pos.B03_FRAZ_INI_EROG_REN_NULL, Len.B03_FRAZ_INI_EROG_REN_NULL);
    }

    public String getB03FrazIniErogRenNullFormatted() {
        return Functions.padBlanks(getB03FrazIniErogRenNull(), Len.B03_FRAZ_INI_EROG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_FRAZ_INI_EROG_REN = 1;
        public static final int B03_FRAZ_INI_EROG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_FRAZ_INI_EROG_REN = 3;
        public static final int B03_FRAZ_INI_EROG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_FRAZ_INI_EROG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
