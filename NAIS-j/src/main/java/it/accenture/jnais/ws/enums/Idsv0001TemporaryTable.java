package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0001-TEMPORARY-TABLE<br>
 * Variable: IDSV0001-TEMPORARY-TABLE from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001TemporaryTable {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NO_TEMP_TABLE = '0';
    public static final char STATIC_TEMP_TABLE = '1';
    public static final char SESSION_TEMP_TABLE = '2';

    //==== METHODS ====
    public void setTemporaryTable(char temporaryTable) {
        this.value = temporaryTable;
    }

    public char getTemporaryTable() {
        return this.value;
    }

    public void setIdsv0001NoTempTable() {
        value = NO_TEMP_TABLE;
    }

    public void setIdsv0001StaticTempTable() {
        value = STATIC_TEMP_TABLE;
    }

    public void setIdsv0001SessionTempTable() {
        value = SESSION_TEMP_TABLE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TEMPORARY_TABLE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
