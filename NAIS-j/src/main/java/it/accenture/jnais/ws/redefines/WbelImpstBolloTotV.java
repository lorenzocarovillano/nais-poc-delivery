package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-BOLLO-TOT-V<br>
 * Variable: WBEL-IMPST-BOLLO-TOT-V from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstBolloTotV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstBolloTotV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_BOLLO_TOT_V;
    }

    public void setWbelImpstBolloTotV(AfDecimal wbelImpstBolloTotV) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_BOLLO_TOT_V, wbelImpstBolloTotV.copy());
    }

    public void setWbelImpstBolloTotVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_BOLLO_TOT_V, Pos.WBEL_IMPST_BOLLO_TOT_V);
    }

    /**Original name: WBEL-IMPST-BOLLO-TOT-V<br>*/
    public AfDecimal getWbelImpstBolloTotV() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_BOLLO_TOT_V, Len.Int.WBEL_IMPST_BOLLO_TOT_V, Len.Fract.WBEL_IMPST_BOLLO_TOT_V);
    }

    public byte[] getWbelImpstBolloTotVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_BOLLO_TOT_V, Pos.WBEL_IMPST_BOLLO_TOT_V);
        return buffer;
    }

    public void initWbelImpstBolloTotVSpaces() {
        fill(Pos.WBEL_IMPST_BOLLO_TOT_V, Len.WBEL_IMPST_BOLLO_TOT_V, Types.SPACE_CHAR);
    }

    public void setWbelImpstBolloTotVNull(String wbelImpstBolloTotVNull) {
        writeString(Pos.WBEL_IMPST_BOLLO_TOT_V_NULL, wbelImpstBolloTotVNull, Len.WBEL_IMPST_BOLLO_TOT_V_NULL);
    }

    /**Original name: WBEL-IMPST-BOLLO-TOT-V-NULL<br>*/
    public String getWbelImpstBolloTotVNull() {
        return readString(Pos.WBEL_IMPST_BOLLO_TOT_V_NULL, Len.WBEL_IMPST_BOLLO_TOT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_BOLLO_TOT_V = 1;
        public static final int WBEL_IMPST_BOLLO_TOT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_BOLLO_TOT_V = 8;
        public static final int WBEL_IMPST_BOLLO_TOT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_BOLLO_TOT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_BOLLO_TOT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
