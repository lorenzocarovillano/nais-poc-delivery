package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-BATCH-EXECUTOR-STD<br>
 * Variable: FLAG-BATCH-EXECUTOR-STD from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagBatchExecutorStd {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagBatchExecutorStd(char flagBatchExecutorStd) {
        this.value = flagBatchExecutorStd;
    }

    public char getFlagBatchExecutorStd() {
        return this.value;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
