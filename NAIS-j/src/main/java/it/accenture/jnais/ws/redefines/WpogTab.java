package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPOG-TAB<br>
 * Variable: WPOG-TAB from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_OGG_MAXOCCURS = 200;
    public static final char WPOG_ST_ADD = 'A';
    public static final char WPOG_ST_MOD = 'M';
    public static final char WPOG_ST_INV = 'I';
    public static final char WPOG_ST_DEL = 'D';
    public static final char WPOG_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WpogTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_TAB;
    }

    public String getWpogTabFormatted() {
        return readFixedString(Pos.WPOG_TAB, Len.WPOG_TAB);
    }

    public void setWpogTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_TAB, Pos.WPOG_TAB);
    }

    public byte[] getWpogTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_TAB, Pos.WPOG_TAB);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wpogStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WPOG-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA PARAM_OGG
	 *    ALIAS POG
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wpogStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wpogIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WPOG-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wpogIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdParamOgg(int idParamOggIdx, int idParamOgg) {
        int position = Pos.wpogIdParamOgg(idParamOggIdx - 1);
        writeIntAsPacked(position, idParamOgg, Len.Int.ID_PARAM_OGG);
    }

    /**Original name: WPOG-ID-PARAM-OGG<br>*/
    public int getIdParamOgg(int idParamOggIdx) {
        int position = Pos.wpogIdParamOgg(idParamOggIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PARAM_OGG);
    }

    public void setIdOgg(int idOggIdx, int idOgg) {
        int position = Pos.wpogIdOgg(idOggIdx - 1);
        writeIntAsPacked(position, idOgg, Len.Int.ID_OGG);
    }

    /**Original name: WPOG-ID-OGG<br>*/
    public int getIdOgg(int idOggIdx) {
        int position = Pos.wpogIdOgg(idOggIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_OGG);
    }

    public void setTpOgg(int tpOggIdx, String tpOgg) {
        int position = Pos.wpogTpOgg(tpOggIdx - 1);
        writeString(position, tpOgg, Len.TP_OGG);
    }

    /**Original name: WPOG-TP-OGG<br>*/
    public String getTpOgg(int tpOggIdx) {
        int position = Pos.wpogTpOgg(tpOggIdx - 1);
        return readString(position, Len.TP_OGG);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wpogIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WPOG-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wpogIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wpogIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WPOG-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wpogIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wpogDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WPOG-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wpogDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wpogDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WPOG-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wpogDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wpogCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WPOG-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wpogCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodParam(int codParamIdx, String codParam) {
        int position = Pos.wpogCodParam(codParamIdx - 1);
        writeString(position, codParam, Len.COD_PARAM);
    }

    /**Original name: WPOG-COD-PARAM<br>*/
    public String getCodParam(int codParamIdx) {
        int position = Pos.wpogCodParam(codParamIdx - 1);
        return readString(position, Len.COD_PARAM);
    }

    public void setTpParam(int tpParamIdx, char tpParam) {
        int position = Pos.wpogTpParam(tpParamIdx - 1);
        writeChar(position, tpParam);
    }

    /**Original name: WPOG-TP-PARAM<br>*/
    public char getTpParam(int tpParamIdx) {
        int position = Pos.wpogTpParam(tpParamIdx - 1);
        return readChar(position);
    }

    public void setTpD(int tpDIdx, String tpD) {
        int position = Pos.wpogTpD(tpDIdx - 1);
        writeString(position, tpD, Len.TP_D);
    }

    /**Original name: WPOG-TP-D<br>*/
    public String getTpD(int tpDIdx) {
        int position = Pos.wpogTpD(tpDIdx - 1);
        return readString(position, Len.TP_D);
    }

    public void setValImp(int valImpIdx, AfDecimal valImp) {
        int position = Pos.wpogValImp(valImpIdx - 1);
        writeDecimalAsPacked(position, valImp.copy());
    }

    /**Original name: WPOG-VAL-IMP<br>*/
    public AfDecimal getValImp(int valImpIdx) {
        int position = Pos.wpogValImp(valImpIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_IMP, Len.Fract.VAL_IMP);
    }

    public void setValDt(int valDtIdx, int valDt) {
        int position = Pos.wpogValDt(valDtIdx - 1);
        writeIntAsPacked(position, valDt, Len.Int.VAL_DT);
    }

    /**Original name: WPOG-VAL-DT<br>*/
    public int getValDt(int valDtIdx) {
        int position = Pos.wpogValDt(valDtIdx - 1);
        return readPackedAsInt(position, Len.Int.VAL_DT);
    }

    public void setValTs(int valTsIdx, AfDecimal valTs) {
        int position = Pos.wpogValTs(valTsIdx - 1);
        writeDecimalAsPacked(position, valTs.copy());
    }

    /**Original name: WPOG-VAL-TS<br>*/
    public AfDecimal getValTs(int valTsIdx) {
        int position = Pos.wpogValTs(valTsIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_TS, Len.Fract.VAL_TS);
    }

    public void setValTxt(int valTxtIdx, String valTxt) {
        int position = Pos.wpogValTxt(valTxtIdx - 1);
        writeString(position, valTxt, Len.VAL_TXT);
    }

    /**Original name: WPOG-VAL-TXT<br>*/
    public String getValTxt(int valTxtIdx) {
        int position = Pos.wpogValTxt(valTxtIdx - 1);
        return readString(position, Len.VAL_TXT);
    }

    public void setValFl(int valFlIdx, char valFl) {
        int position = Pos.wpogValFl(valFlIdx - 1);
        writeChar(position, valFl);
    }

    /**Original name: WPOG-VAL-FL<br>*/
    public char getValFl(int valFlIdx) {
        int position = Pos.wpogValFl(valFlIdx - 1);
        return readChar(position);
    }

    public void setValNum(int valNumIdx, long valNum) {
        int position = Pos.wpogValNum(valNumIdx - 1);
        writeLongAsPacked(position, valNum, Len.Int.VAL_NUM);
    }

    /**Original name: WPOG-VAL-NUM<br>*/
    public long getValNum(int valNumIdx) {
        int position = Pos.wpogValNum(valNumIdx - 1);
        return readPackedAsLong(position, Len.Int.VAL_NUM);
    }

    public void setValPc(int valPcIdx, AfDecimal valPc) {
        int position = Pos.wpogValPc(valPcIdx - 1);
        writeDecimalAsPacked(position, valPc.copy());
    }

    /**Original name: WPOG-VAL-PC<br>*/
    public AfDecimal getValPc(int valPcIdx) {
        int position = Pos.wpogValPc(valPcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_PC, Len.Fract.VAL_PC);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wpogDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WPOG-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wpogDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wpogDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WPOG-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wpogDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wpogDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WPOG-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wpogDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wpogDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WPOG-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wpogDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wpogDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WPOG-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wpogDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wpogDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WPOG-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wpogDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wpogDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WPOG-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wpogDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WPOG-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_TAB = 1;
        public static final int WPOG_TAB_R = 1;
        public static final int FLR1 = WPOG_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wpogTabParamOgg(int idx) {
            return WPOG_TAB + idx * Len.TAB_PARAM_OGG;
        }

        public static int wpogStatus(int idx) {
            return wpogTabParamOgg(idx);
        }

        public static int wpogIdPtf(int idx) {
            return wpogStatus(idx) + Len.STATUS;
        }

        public static int wpogDati(int idx) {
            return wpogIdPtf(idx) + Len.ID_PTF;
        }

        public static int wpogIdParamOgg(int idx) {
            return wpogDati(idx);
        }

        public static int wpogIdOgg(int idx) {
            return wpogIdParamOgg(idx) + Len.ID_PARAM_OGG;
        }

        public static int wpogTpOgg(int idx) {
            return wpogIdOgg(idx) + Len.ID_OGG;
        }

        public static int wpogIdMoviCrz(int idx) {
            return wpogTpOgg(idx) + Len.TP_OGG;
        }

        public static int wpogIdMoviChiu(int idx) {
            return wpogIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wpogIdMoviChiuNull(int idx) {
            return wpogIdMoviChiu(idx);
        }

        public static int wpogDtIniEff(int idx) {
            return wpogIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wpogDtEndEff(int idx) {
            return wpogDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wpogCodCompAnia(int idx) {
            return wpogDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wpogCodParam(int idx) {
            return wpogCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wpogCodParamNull(int idx) {
            return wpogCodParam(idx);
        }

        public static int wpogTpParam(int idx) {
            return wpogCodParam(idx) + Len.COD_PARAM;
        }

        public static int wpogTpParamNull(int idx) {
            return wpogTpParam(idx);
        }

        public static int wpogTpD(int idx) {
            return wpogTpParam(idx) + Len.TP_PARAM;
        }

        public static int wpogTpDNull(int idx) {
            return wpogTpD(idx);
        }

        public static int wpogValImp(int idx) {
            return wpogTpD(idx) + Len.TP_D;
        }

        public static int wpogValImpNull(int idx) {
            return wpogValImp(idx);
        }

        public static int wpogValDt(int idx) {
            return wpogValImp(idx) + Len.VAL_IMP;
        }

        public static int wpogValDtNull(int idx) {
            return wpogValDt(idx);
        }

        public static int wpogValTs(int idx) {
            return wpogValDt(idx) + Len.VAL_DT;
        }

        public static int wpogValTsNull(int idx) {
            return wpogValTs(idx);
        }

        public static int wpogValTxt(int idx) {
            return wpogValTs(idx) + Len.VAL_TS;
        }

        public static int wpogValFl(int idx) {
            return wpogValTxt(idx) + Len.VAL_TXT;
        }

        public static int wpogValFlNull(int idx) {
            return wpogValFl(idx);
        }

        public static int wpogValNum(int idx) {
            return wpogValFl(idx) + Len.VAL_FL;
        }

        public static int wpogValNumNull(int idx) {
            return wpogValNum(idx);
        }

        public static int wpogValPc(int idx) {
            return wpogValNum(idx) + Len.VAL_NUM;
        }

        public static int wpogValPcNull(int idx) {
            return wpogValPc(idx);
        }

        public static int wpogDsRiga(int idx) {
            return wpogValPc(idx) + Len.VAL_PC;
        }

        public static int wpogDsOperSql(int idx) {
            return wpogDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wpogDsVer(int idx) {
            return wpogDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wpogDsTsIniCptz(int idx) {
            return wpogDsVer(idx) + Len.DS_VER;
        }

        public static int wpogDsTsEndCptz(int idx) {
            return wpogDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wpogDsUtente(int idx) {
            return wpogDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wpogDsStatoElab(int idx) {
            return wpogDsUtente(idx) + Len.DS_UTENTE;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_PARAM_OGG = 5;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_PARAM = 20;
        public static final int TP_PARAM = 1;
        public static final int TP_D = 2;
        public static final int VAL_IMP = 8;
        public static final int VAL_DT = 5;
        public static final int VAL_TS = 8;
        public static final int VAL_TXT = 100;
        public static final int VAL_FL = 1;
        public static final int VAL_NUM = 8;
        public static final int VAL_PC = 8;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DATI = ID_PARAM_OGG + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_PARAM + TP_PARAM + TP_D + VAL_IMP + VAL_DT + VAL_TS + VAL_TXT + VAL_FL + VAL_NUM + VAL_PC + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;
        public static final int TAB_PARAM_OGG = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 255;
        public static final int WPOG_TAB = WpogTab.TAB_PARAM_OGG_MAXOCCURS * TAB_PARAM_OGG;
        public static final int RESTO_TAB = 50745;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_PARAM_OGG = 9;
            public static final int ID_OGG = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int VAL_IMP = 12;
            public static final int VAL_DT = 8;
            public static final int VAL_TS = 5;
            public static final int VAL_NUM = 14;
            public static final int VAL_PC = 5;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 3;
            public static final int VAL_TS = 9;
            public static final int VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
