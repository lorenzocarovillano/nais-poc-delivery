package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;

/**Original name: ISPC0211-TAB-VAL-P<br>
 * Variable: ISPC0211-TAB-VAL-P from program ISPS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ispc0211TabValP extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int AREA_VARIABILI_P_MAXOCCURS = 100;
    public static final int TAB_SCHEDA_P_MAXOCCURS = 3;

    //==== CONSTRUCTORS ====
    public Ispc0211TabValP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISPC0211_TAB_VAL_P;
    }

    public String getIspc0211TabValPFormatted() {
        return readFixedString(Pos.ISPC0211_TAB_VAL_P, Len.ISPC0211_TAB_VAL_P);
    }

    public void setIspc0211TabValPBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISPC0211_TAB_VAL_P, Pos.ISPC0211_TAB_VAL_P);
    }

    public byte[] getIspc0211TabValPBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISPC0211_TAB_VAL_P, Pos.ISPC0211_TAB_VAL_P);
        return buffer;
    }

    public void setIspc0211TipoLivelloP(int ispc0211TipoLivelloPIdx, char ispc0211TipoLivelloP) {
        int position = Pos.ispc0211TipoLivelloP(ispc0211TipoLivelloPIdx - 1);
        writeChar(position, ispc0211TipoLivelloP);
    }

    /**Original name: ISPC0211-TIPO-LIVELLO-P<br>*/
    public char getIspc0211TipoLivelloP(int ispc0211TipoLivelloPIdx) {
        int position = Pos.ispc0211TipoLivelloP(ispc0211TipoLivelloPIdx - 1);
        return readChar(position);
    }

    public void setIspc0211CodiceLivelloP(int ispc0211CodiceLivelloPIdx, String ispc0211CodiceLivelloP) {
        int position = Pos.ispc0211CodiceLivelloP(ispc0211CodiceLivelloPIdx - 1);
        writeString(position, ispc0211CodiceLivelloP, Len.CODICE_LIVELLO_P);
    }

    /**Original name: ISPC0211-CODICE-LIVELLO-P<br>*/
    public String getIspc0211CodiceLivelloP(int ispc0211CodiceLivelloPIdx) {
        int position = Pos.ispc0211CodiceLivelloP(ispc0211CodiceLivelloPIdx - 1);
        return readString(position, Len.CODICE_LIVELLO_P);
    }

    public void setIspc0211IdentLivelloPFormatted(int ispc0211IdentLivelloPIdx, String ispc0211IdentLivelloP) {
        int position = Pos.ispc0211IdentLivelloP(ispc0211IdentLivelloPIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211IdentLivelloP, Len.IDENT_LIVELLO_P), Len.IDENT_LIVELLO_P);
    }

    /**Original name: ISPC0211-IDENT-LIVELLO-P<br>*/
    public int getIspc0211IdentLivelloP(int ispc0211IdentLivelloPIdx) {
        int position = Pos.ispc0211IdentLivelloP(ispc0211IdentLivelloPIdx - 1);
        return readNumDispUnsignedInt(position, Len.IDENT_LIVELLO_P);
    }

    public void setIspc0211DtInizProd(int ispc0211DtInizProdIdx, String ispc0211DtInizProd) {
        int position = Pos.ispc0211DtInizProd(ispc0211DtInizProdIdx - 1);
        writeString(position, ispc0211DtInizProd, Len.DT_INIZ_PROD);
    }

    /**Original name: ISPC0211-DT-INIZ-PROD<br>*/
    public String getIspc0211DtInizProd(int ispc0211DtInizProdIdx) {
        int position = Pos.ispc0211DtInizProd(ispc0211DtInizProdIdx - 1);
        return readString(position, Len.DT_INIZ_PROD);
    }

    public void setIspc0211CodRgmFiscFormatted(int ispc0211CodRgmFiscIdx, String ispc0211CodRgmFisc) {
        int position = Pos.ispc0211CodRgmFisc(ispc0211CodRgmFiscIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211CodRgmFisc, Len.COD_RGM_FISC), Len.COD_RGM_FISC);
    }

    /**Original name: ISPC0211-COD-RGM-FISC<br>*/
    public short getIspc0211CodRgmFisc(int ispc0211CodRgmFiscIdx) {
        int position = Pos.ispc0211CodRgmFisc(ispc0211CodRgmFiscIdx - 1);
        return readNumDispUnsignedShort(position, Len.COD_RGM_FISC);
    }

    public void setIspc0211FlgLiqP(int ispc0211FlgLiqPIdx, String ispc0211FlgLiqP) {
        int position = Pos.ispc0211FlgLiqP(ispc0211FlgLiqPIdx - 1);
        writeString(position, ispc0211FlgLiqP, Len.FLG_LIQ_P);
    }

    /**Original name: ISPC0211-FLG-LIQ-P<br>*/
    public String getIspc0211FlgLiqP(int ispc0211FlgLiqPIdx) {
        int position = Pos.ispc0211FlgLiqP(ispc0211FlgLiqPIdx - 1);
        return readString(position, Len.FLG_LIQ_P);
    }

    public void setIspc0211CodiceOpzioneP(int ispc0211CodiceOpzionePIdx, String ispc0211CodiceOpzioneP) {
        int position = Pos.ispc0211CodiceOpzioneP(ispc0211CodiceOpzionePIdx - 1);
        writeString(position, ispc0211CodiceOpzioneP, Len.CODICE_OPZIONE_P);
    }

    /**Original name: ISPC0211-CODICE-OPZIONE-P<br>*/
    public String getIspc0211CodiceOpzioneP(int ispc0211CodiceOpzionePIdx) {
        int position = Pos.ispc0211CodiceOpzioneP(ispc0211CodiceOpzionePIdx - 1);
        return readString(position, Len.CODICE_OPZIONE_P);
    }

    public void setIspc0211NumComponMaxEleP(int ispc0211NumComponMaxElePIdx, short ispc0211NumComponMaxEleP) {
        int position = Pos.ispc0211NumComponMaxEleP(ispc0211NumComponMaxElePIdx - 1);
        writeShort(position, ispc0211NumComponMaxEleP, Len.Int.ISPC0211_NUM_COMPON_MAX_ELE_P, SignType.NO_SIGN);
    }

    public void setIspc0211NumComponMaxElePFormatted(int ispc0211NumComponMaxElePIdx, String ispc0211NumComponMaxEleP) {
        int position = Pos.ispc0211NumComponMaxEleP(ispc0211NumComponMaxElePIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ispc0211NumComponMaxEleP, Len.NUM_COMPON_MAX_ELE_P), Len.NUM_COMPON_MAX_ELE_P);
    }

    /**Original name: ISPC0211-NUM-COMPON-MAX-ELE-P<br>*/
    public short getIspc0211NumComponMaxEleP(int ispc0211NumComponMaxElePIdx) {
        int position = Pos.ispc0211NumComponMaxEleP(ispc0211NumComponMaxElePIdx - 1);
        return readNumDispUnsignedShort(position, Len.NUM_COMPON_MAX_ELE_P);
    }

    public void setIspc0211CodiceVariabileP(int ispc0211CodiceVariabilePIdx1, int ispc0211CodiceVariabilePIdx2, String ispc0211CodiceVariabileP) {
        int position = Pos.ispc0211CodiceVariabileP(ispc0211CodiceVariabilePIdx1 - 1, ispc0211CodiceVariabilePIdx2 - 1);
        writeString(position, ispc0211CodiceVariabileP, Len.CODICE_VARIABILE_P);
    }

    /**Original name: ISPC0211-CODICE-VARIABILE-P<br>*/
    public String getIspc0211CodiceVariabileP(int ispc0211CodiceVariabilePIdx1, int ispc0211CodiceVariabilePIdx2) {
        int position = Pos.ispc0211CodiceVariabileP(ispc0211CodiceVariabilePIdx1 - 1, ispc0211CodiceVariabilePIdx2 - 1);
        return readString(position, Len.CODICE_VARIABILE_P);
    }

    public void setIspc0211TipoDatoP(int ispc0211TipoDatoPIdx1, int ispc0211TipoDatoPIdx2, char ispc0211TipoDatoP) {
        int position = Pos.ispc0211TipoDatoP(ispc0211TipoDatoPIdx1 - 1, ispc0211TipoDatoPIdx2 - 1);
        writeChar(position, ispc0211TipoDatoP);
    }

    /**Original name: ISPC0211-TIPO-DATO-P<br>*/
    public char getIspc0211TipoDatoP(int ispc0211TipoDatoPIdx1, int ispc0211TipoDatoPIdx2) {
        int position = Pos.ispc0211TipoDatoP(ispc0211TipoDatoPIdx1 - 1, ispc0211TipoDatoPIdx2 - 1);
        return readChar(position);
    }

    public void setIspc0211ValGenericoP(int ispc0211ValGenericoPIdx1, int ispc0211ValGenericoPIdx2, String ispc0211ValGenericoP) {
        int position = Pos.ispc0211ValGenericoP(ispc0211ValGenericoPIdx1 - 1, ispc0211ValGenericoPIdx2 - 1);
        writeString(position, ispc0211ValGenericoP, Len.VAL_GENERICO_P);
    }

    /**Original name: ISPC0211-VAL-GENERICO-P<br>*/
    public String getIspc0211ValGenericoP(int ispc0211ValGenericoPIdx1, int ispc0211ValGenericoPIdx2) {
        int position = Pos.ispc0211ValGenericoP(ispc0211ValGenericoPIdx1 - 1, ispc0211ValGenericoPIdx2 - 1);
        return readString(position, Len.VAL_GENERICO_P);
    }

    public String getIspc0211TabSchedaRPFormatted() {
        return readFixedString(Pos.ISPC0211_TAB_SCHEDA_R_P, Len.ISPC0211_TAB_SCHEDA_R_P);
    }

    public void setIspc0211RestoTabSchedaP(String ispc0211RestoTabSchedaP) {
        writeString(Pos.RESTO_TAB_SCHEDA_P, ispc0211RestoTabSchedaP, Len.ISPC0211_RESTO_TAB_SCHEDA_P);
    }

    /**Original name: ISPC0211-RESTO-TAB-SCHEDA-P<br>*/
    public String getIspc0211RestoTabSchedaP() {
        return readString(Pos.RESTO_TAB_SCHEDA_P, Len.ISPC0211_RESTO_TAB_SCHEDA_P);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISPC0211_TAB_VAL_P = 1;
        public static final int ISPC0211_TAB_SCHEDA_R_P = 1;
        public static final int FLR1 = ISPC0211_TAB_SCHEDA_R_P;
        public static final int RESTO_TAB_SCHEDA_P = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ispc0211TabSchedaP(int idx) {
            return ISPC0211_TAB_VAL_P + idx * Len.TAB_SCHEDA_P;
        }

        public static int ispc0211TipoLivelloP(int idx) {
            return ispc0211TabSchedaP(idx);
        }

        public static int ispc0211CodiceLivelloP(int idx) {
            return ispc0211TipoLivelloP(idx) + Len.TIPO_LIVELLO_P;
        }

        public static int ispc0211IdentLivelloP(int idx) {
            return ispc0211CodiceLivelloP(idx) + Len.CODICE_LIVELLO_P;
        }

        public static int ispc0211DtInizProd(int idx) {
            return ispc0211IdentLivelloP(idx) + Len.IDENT_LIVELLO_P;
        }

        public static int ispc0211CodRgmFisc(int idx) {
            return ispc0211DtInizProd(idx) + Len.DT_INIZ_PROD;
        }

        public static int ispc0211FlgLiqP(int idx) {
            return ispc0211CodRgmFisc(idx) + Len.COD_RGM_FISC;
        }

        public static int ispc0211CodiceOpzioneP(int idx) {
            return ispc0211FlgLiqP(idx) + Len.FLG_LIQ_P;
        }

        public static int ispc0211NumComponMaxEleP(int idx) {
            return ispc0211CodiceOpzioneP(idx) + Len.CODICE_OPZIONE_P;
        }

        public static int ispc0211AreaVariabiliP(int idx1, int idx2) {
            return ispc0211NumComponMaxEleP(idx1) + Len.NUM_COMPON_MAX_ELE_P + idx2 * Len.AREA_VARIABILI_P;
        }

        public static int ispc0211CodiceVariabileP(int idx1, int idx2) {
            return ispc0211AreaVariabiliP(idx1, idx2);
        }

        public static int ispc0211TipoDatoP(int idx1, int idx2) {
            return ispc0211CodiceVariabileP(idx1, idx2) + Len.CODICE_VARIABILE_P;
        }

        public static int ispc0211ValGenericoP(int idx1, int idx2) {
            return ispc0211TipoDatoP(idx1, idx2) + Len.TIPO_DATO_P;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_LIVELLO_P = 1;
        public static final int CODICE_LIVELLO_P = 12;
        public static final int IDENT_LIVELLO_P = 9;
        public static final int DT_INIZ_PROD = 8;
        public static final int COD_RGM_FISC = 2;
        public static final int FLG_LIQ_P = 2;
        public static final int CODICE_OPZIONE_P = 2;
        public static final int NUM_COMPON_MAX_ELE_P = 3;
        public static final int CODICE_VARIABILE_P = 12;
        public static final int TIPO_DATO_P = 1;
        public static final int VAL_GENERICO_P = 60;
        public static final int AREA_VARIABILI_P = CODICE_VARIABILE_P + TIPO_DATO_P + VAL_GENERICO_P;
        public static final int TAB_SCHEDA_P = TIPO_LIVELLO_P + CODICE_LIVELLO_P + IDENT_LIVELLO_P + DT_INIZ_PROD + COD_RGM_FISC + FLG_LIQ_P + CODICE_OPZIONE_P + NUM_COMPON_MAX_ELE_P + Ispc0211TabValP.AREA_VARIABILI_P_MAXOCCURS * AREA_VARIABILI_P;
        public static final int FLR1 = 7339;
        public static final int ISPC0211_TAB_VAL_P = Ispc0211TabValP.TAB_SCHEDA_P_MAXOCCURS * TAB_SCHEDA_P;
        public static final int ISPC0211_RESTO_TAB_SCHEDA_P = 14678;
        public static final int ISPC0211_TAB_SCHEDA_R_P = ISPC0211_RESTO_TAB_SCHEDA_P + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISPC0211_NUM_COMPON_MAX_ELE_P = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
