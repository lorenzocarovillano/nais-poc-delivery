package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FINE-CICLO<br>
 * Variable: FINE-CICLO from program LVVS2780<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FineCiclo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFineCiclo(char fineCiclo) {
        this.value = fineCiclo;
    }

    public char getFineCiclo() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
