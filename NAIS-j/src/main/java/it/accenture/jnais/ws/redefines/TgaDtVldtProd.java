package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DT-VLDT-PROD<br>
 * Variable: TGA-DT-VLDT-PROD from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDtVldtProd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDtVldtProd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DT_VLDT_PROD;
    }

    public void setTgaDtVldtProd(int tgaDtVldtProd) {
        writeIntAsPacked(Pos.TGA_DT_VLDT_PROD, tgaDtVldtProd, Len.Int.TGA_DT_VLDT_PROD);
    }

    public void setTgaDtVldtProdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DT_VLDT_PROD, Pos.TGA_DT_VLDT_PROD);
    }

    /**Original name: TGA-DT-VLDT-PROD<br>*/
    public int getTgaDtVldtProd() {
        return readPackedAsInt(Pos.TGA_DT_VLDT_PROD, Len.Int.TGA_DT_VLDT_PROD);
    }

    public byte[] getTgaDtVldtProdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DT_VLDT_PROD, Pos.TGA_DT_VLDT_PROD);
        return buffer;
    }

    public void setTgaDtVldtProdNull(String tgaDtVldtProdNull) {
        writeString(Pos.TGA_DT_VLDT_PROD_NULL, tgaDtVldtProdNull, Len.TGA_DT_VLDT_PROD_NULL);
    }

    /**Original name: TGA-DT-VLDT-PROD-NULL<br>*/
    public String getTgaDtVldtProdNull() {
        return readString(Pos.TGA_DT_VLDT_PROD_NULL, Len.TGA_DT_VLDT_PROD_NULL);
    }

    public String getTgaDtVldtProdNullFormatted() {
        return Functions.padBlanks(getTgaDtVldtProdNull(), Len.TGA_DT_VLDT_PROD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DT_VLDT_PROD = 1;
        public static final int TGA_DT_VLDT_PROD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DT_VLDT_PROD = 5;
        public static final int TGA_DT_VLDT_PROD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DT_VLDT_PROD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
