package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-AMM-RAT-END<br>
 * Variable: WP67-AMM-RAT-END from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67AmmRatEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67AmmRatEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_AMM_RAT_END;
    }

    public void setWp67AmmRatEnd(AfDecimal wp67AmmRatEnd) {
        writeDecimalAsPacked(Pos.WP67_AMM_RAT_END, wp67AmmRatEnd.copy());
    }

    public void setWp67AmmRatEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_AMM_RAT_END, Pos.WP67_AMM_RAT_END);
    }

    /**Original name: WP67-AMM-RAT-END<br>*/
    public AfDecimal getWp67AmmRatEnd() {
        return readPackedAsDecimal(Pos.WP67_AMM_RAT_END, Len.Int.WP67_AMM_RAT_END, Len.Fract.WP67_AMM_RAT_END);
    }

    public byte[] getWp67AmmRatEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_AMM_RAT_END, Pos.WP67_AMM_RAT_END);
        return buffer;
    }

    public void setWp67AmmRatEndNull(String wp67AmmRatEndNull) {
        writeString(Pos.WP67_AMM_RAT_END_NULL, wp67AmmRatEndNull, Len.WP67_AMM_RAT_END_NULL);
    }

    /**Original name: WP67-AMM-RAT-END-NULL<br>*/
    public String getWp67AmmRatEndNull() {
        return readString(Pos.WP67_AMM_RAT_END_NULL, Len.WP67_AMM_RAT_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_AMM_RAT_END = 1;
        public static final int WP67_AMM_RAT_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_AMM_RAT_END = 8;
        public static final int WP67_AMM_RAT_END_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_AMM_RAT_END = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_AMM_RAT_END = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
