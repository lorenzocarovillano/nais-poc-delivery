package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-TOT<br>
 * Variable: RST-RIS-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_TOT;
    }

    public void setRstRisTot(AfDecimal rstRisTot) {
        writeDecimalAsPacked(Pos.RST_RIS_TOT, rstRisTot.copy());
    }

    public void setRstRisTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_TOT, Pos.RST_RIS_TOT);
    }

    /**Original name: RST-RIS-TOT<br>*/
    public AfDecimal getRstRisTot() {
        return readPackedAsDecimal(Pos.RST_RIS_TOT, Len.Int.RST_RIS_TOT, Len.Fract.RST_RIS_TOT);
    }

    public byte[] getRstRisTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_TOT, Pos.RST_RIS_TOT);
        return buffer;
    }

    public void setRstRisTotNull(String rstRisTotNull) {
        writeString(Pos.RST_RIS_TOT_NULL, rstRisTotNull, Len.RST_RIS_TOT_NULL);
    }

    /**Original name: RST-RIS-TOT-NULL<br>*/
    public String getRstRisTotNull() {
        return readString(Pos.RST_RIS_TOT_NULL, Len.RST_RIS_TOT_NULL);
    }

    public String getRstRisTotNullFormatted() {
        return Functions.padBlanks(getRstRisTotNull(), Len.RST_RIS_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_TOT = 1;
        public static final int RST_RIS_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_TOT = 8;
        public static final int RST_RIS_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
