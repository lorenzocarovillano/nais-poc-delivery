package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Ivvv0212CtrlAutOper;
import it.accenture.jnais.ws.redefines.WskdTabValP;
import it.accenture.jnais.ws.redefines.WskdTabValT;

/**Original name: WSKD-AREA-SCHEDA<br>
 * Variable: WSKD-AREA-SCHEDA from program LOAS0800<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WskdAreaScheda extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int WSKD_CTRL_AUT_OPER_MAXOCCURS = 20;
    /**Original name: WSKD-DEE<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT VALORIZZATORE VARIABILI
	 *    - area skeda per servizi di prodotto
	 *    - area variabili autonomia operativa
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    AREA OUTPUT PRODOTTO SCHEDE 'P' 'Q' 'O'
	 * ----------------------------------------------------------------*</pre>*/
    private String wskdDee = DefaultValues.stringVal(Len.WSKD_DEE);
    //Original name: WSKD-ELE-LIVELLO-MAX-P
    private short wskdEleLivelloMaxP = DefaultValues.SHORT_VAL;
    //Original name: WSKD-TAB-VAL-P
    private WskdTabValP wskdTabValP = new WskdTabValP();
    /**Original name: WSKD-ELE-LIVELLO-MAX-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA OUTPUT TRANCHE SCHEDE 'G' 'H'
	 * ----------------------------------------------------------------*</pre>*/
    private short wskdEleLivelloMaxT = DefaultValues.SHORT_VAL;
    //Original name: WSKD-TAB-VAL-T
    private WskdTabValT wskdTabValT = new WskdTabValT();
    //Original name: WSKD-ELE-CTRL-AUT-OPER-MAX
    private short wskdEleCtrlAutOperMax = DefaultValues.SHORT_VAL;
    //Original name: WSKD-CTRL-AUT-OPER
    private Ivvv0212CtrlAutOper[] wskdCtrlAutOper = new Ivvv0212CtrlAutOper[WSKD_CTRL_AUT_OPER_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WskdAreaScheda() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSKD_AREA_SCHEDA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWskdAreaSchedaBytes(buf);
    }

    public void init() {
        for (int wskdCtrlAutOperIdx = 1; wskdCtrlAutOperIdx <= WSKD_CTRL_AUT_OPER_MAXOCCURS; wskdCtrlAutOperIdx++) {
            wskdCtrlAutOper[wskdCtrlAutOperIdx - 1] = new Ivvv0212CtrlAutOper();
        }
    }

    public void setWskdAreaSchedaFormatted(String data) {
        byte[] buffer = new byte[Len.WSKD_AREA_SCHEDA];
        MarshalByte.writeString(buffer, 1, data, Len.WSKD_AREA_SCHEDA);
        setWskdAreaSchedaBytes(buffer, 1);
    }

    public String getWskdAreaSchedaFormatted() {
        return MarshalByteExt.bufferToStr(getWskdAreaSchedaBytes());
    }

    public void setWskdAreaSchedaBytes(byte[] buffer) {
        setWskdAreaSchedaBytes(buffer, 1);
    }

    public byte[] getWskdAreaSchedaBytes() {
        byte[] buffer = new byte[Len.WSKD_AREA_SCHEDA];
        return getWskdAreaSchedaBytes(buffer, 1);
    }

    public void setWskdAreaSchedaBytes(byte[] buffer, int offset) {
        int position = offset;
        wskdDee = MarshalByte.readString(buffer, position, Len.WSKD_DEE);
        position += Len.WSKD_DEE;
        wskdEleLivelloMaxP = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WSKD_ELE_LIVELLO_MAX_P, 0);
        position += Len.WSKD_ELE_LIVELLO_MAX_P;
        wskdTabValP.setWskdTabValPBytes(buffer, position);
        position += WskdTabValP.Len.WSKD_TAB_VAL_P;
        wskdEleLivelloMaxT = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WSKD_ELE_LIVELLO_MAX_T, 0);
        position += Len.WSKD_ELE_LIVELLO_MAX_T;
        wskdTabValT.setWskdTabValTBytes(buffer, position);
        position += WskdTabValT.Len.WSKD_TAB_VAL_T;
        setWskdVarAutOperBytes(buffer, position);
    }

    public byte[] getWskdAreaSchedaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wskdDee, Len.WSKD_DEE);
        position += Len.WSKD_DEE;
        MarshalByte.writeShortAsPacked(buffer, position, wskdEleLivelloMaxP, Len.Int.WSKD_ELE_LIVELLO_MAX_P, 0);
        position += Len.WSKD_ELE_LIVELLO_MAX_P;
        wskdTabValP.getWskdTabValPBytes(buffer, position);
        position += WskdTabValP.Len.WSKD_TAB_VAL_P;
        MarshalByte.writeShortAsPacked(buffer, position, wskdEleLivelloMaxT, Len.Int.WSKD_ELE_LIVELLO_MAX_T, 0);
        position += Len.WSKD_ELE_LIVELLO_MAX_T;
        wskdTabValT.getWskdTabValTBytes(buffer, position);
        position += WskdTabValT.Len.WSKD_TAB_VAL_T;
        getWskdVarAutOperBytes(buffer, position);
        return buffer;
    }

    public void setWskdDee(String wskdDee) {
        this.wskdDee = Functions.subString(wskdDee, Len.WSKD_DEE);
    }

    public String getWskdDee() {
        return this.wskdDee;
    }

    public void setWskdEleLivelloMaxP(short wskdEleLivelloMaxP) {
        this.wskdEleLivelloMaxP = wskdEleLivelloMaxP;
    }

    public short getWskdEleLivelloMaxP() {
        return this.wskdEleLivelloMaxP;
    }

    public void setWskdEleLivelloMaxT(short wskdEleLivelloMaxT) {
        this.wskdEleLivelloMaxT = wskdEleLivelloMaxT;
    }

    public short getWskdEleLivelloMaxT() {
        return this.wskdEleLivelloMaxT;
    }

    public void setWskdVarAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        wskdEleCtrlAutOperMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.WSKD_ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.WSKD_ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= WSKD_CTRL_AUT_OPER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wskdCtrlAutOper[idx - 1].setCtrlAutOperBytes(buffer, position);
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
            else {
                wskdCtrlAutOper[idx - 1].initCtrlAutOperSpaces();
                position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
            }
        }
    }

    public byte[] getWskdVarAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShortAsPacked(buffer, position, wskdEleCtrlAutOperMax, Len.Int.WSKD_ELE_CTRL_AUT_OPER_MAX, 0);
        position += Len.WSKD_ELE_CTRL_AUT_OPER_MAX;
        for (int idx = 1; idx <= WSKD_CTRL_AUT_OPER_MAXOCCURS; idx++) {
            wskdCtrlAutOper[idx - 1].getCtrlAutOperBytes(buffer, position);
            position += Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
        }
        return buffer;
    }

    public void setWskdEleCtrlAutOperMax(short wskdEleCtrlAutOperMax) {
        this.wskdEleCtrlAutOperMax = wskdEleCtrlAutOperMax;
    }

    public short getWskdEleCtrlAutOperMax() {
        return this.wskdEleCtrlAutOperMax;
    }

    public Ivvv0212CtrlAutOper getWskdCtrlAutOper(int idx) {
        return wskdCtrlAutOper[idx - 1];
    }

    public WskdTabValP getWskdTabValP() {
        return wskdTabValP;
    }

    public WskdTabValT getWskdTabValT() {
        return wskdTabValT;
    }

    @Override
    public byte[] serialize() {
        return getWskdAreaSchedaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WSKD_DEE = 8;
        public static final int WSKD_ELE_LIVELLO_MAX_P = 3;
        public static final int WSKD_ELE_LIVELLO_MAX_T = 3;
        public static final int WSKD_ELE_CTRL_AUT_OPER_MAX = 3;
        public static final int WSKD_VAR_AUT_OPER = WSKD_ELE_CTRL_AUT_OPER_MAX + WskdAreaScheda.WSKD_CTRL_AUT_OPER_MAXOCCURS * Ivvv0212CtrlAutOper.Len.CTRL_AUT_OPER;
        public static final int WSKD_AREA_SCHEDA = WSKD_DEE + WSKD_ELE_LIVELLO_MAX_P + WskdTabValP.Len.WSKD_TAB_VAL_P + WSKD_ELE_LIVELLO_MAX_T + WskdTabValT.Len.WSKD_TAB_VAL_T + WSKD_VAR_AUT_OPER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSKD_ELE_LIVELLO_MAX_P = 4;
            public static final int WSKD_ELE_LIVELLO_MAX_T = 4;
            public static final int WSKD_ELE_CTRL_AUT_OPER_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
