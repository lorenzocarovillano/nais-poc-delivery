package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-SOST-CALC<br>
 * Variable: DFL-IMPST-SOST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstSostCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstSostCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_SOST_CALC;
    }

    public void setDflImpstSostCalc(AfDecimal dflImpstSostCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPST_SOST_CALC, dflImpstSostCalc.copy());
    }

    public void setDflImpstSostCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_SOST_CALC, Pos.DFL_IMPST_SOST_CALC);
    }

    /**Original name: DFL-IMPST-SOST-CALC<br>*/
    public AfDecimal getDflImpstSostCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPST_SOST_CALC, Len.Int.DFL_IMPST_SOST_CALC, Len.Fract.DFL_IMPST_SOST_CALC);
    }

    public byte[] getDflImpstSostCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_SOST_CALC, Pos.DFL_IMPST_SOST_CALC);
        return buffer;
    }

    public void setDflImpstSostCalcNull(String dflImpstSostCalcNull) {
        writeString(Pos.DFL_IMPST_SOST_CALC_NULL, dflImpstSostCalcNull, Len.DFL_IMPST_SOST_CALC_NULL);
    }

    /**Original name: DFL-IMPST-SOST-CALC-NULL<br>*/
    public String getDflImpstSostCalcNull() {
        return readString(Pos.DFL_IMPST_SOST_CALC_NULL, Len.DFL_IMPST_SOST_CALC_NULL);
    }

    public String getDflImpstSostCalcNullFormatted() {
        return Functions.padBlanks(getDflImpstSostCalcNull(), Len.DFL_IMPST_SOST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_SOST_CALC = 1;
        public static final int DFL_IMPST_SOST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_SOST_CALC = 8;
        public static final int DFL_IMPST_SOST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_SOST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_SOST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
