package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL23-DT-NOTIFICA-BLOCCO<br>
 * Variable: WL23-DT-NOTIFICA-BLOCCO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23DtNotificaBlocco extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23DtNotificaBlocco() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_DT_NOTIFICA_BLOCCO;
    }

    public void setWl23DtNotificaBlocco(int wl23DtNotificaBlocco) {
        writeIntAsPacked(Pos.WL23_DT_NOTIFICA_BLOCCO, wl23DtNotificaBlocco, Len.Int.WL23_DT_NOTIFICA_BLOCCO);
    }

    public void setWl23DtNotificaBloccoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_DT_NOTIFICA_BLOCCO, Pos.WL23_DT_NOTIFICA_BLOCCO);
    }

    /**Original name: WL23-DT-NOTIFICA-BLOCCO<br>*/
    public int getWl23DtNotificaBlocco() {
        return readPackedAsInt(Pos.WL23_DT_NOTIFICA_BLOCCO, Len.Int.WL23_DT_NOTIFICA_BLOCCO);
    }

    public byte[] getWl23DtNotificaBloccoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_DT_NOTIFICA_BLOCCO, Pos.WL23_DT_NOTIFICA_BLOCCO);
        return buffer;
    }

    public void initWl23DtNotificaBloccoSpaces() {
        fill(Pos.WL23_DT_NOTIFICA_BLOCCO, Len.WL23_DT_NOTIFICA_BLOCCO, Types.SPACE_CHAR);
    }

    public void setWl23DtNotificaBloccoNull(String wl23DtNotificaBloccoNull) {
        writeString(Pos.WL23_DT_NOTIFICA_BLOCCO_NULL, wl23DtNotificaBloccoNull, Len.WL23_DT_NOTIFICA_BLOCCO_NULL);
    }

    /**Original name: WL23-DT-NOTIFICA-BLOCCO-NULL<br>*/
    public String getWl23DtNotificaBloccoNull() {
        return readString(Pos.WL23_DT_NOTIFICA_BLOCCO_NULL, Len.WL23_DT_NOTIFICA_BLOCCO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_DT_NOTIFICA_BLOCCO = 1;
        public static final int WL23_DT_NOTIFICA_BLOCCO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_DT_NOTIFICA_BLOCCO = 5;
        public static final int WL23_DT_NOTIFICA_BLOCCO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_DT_NOTIFICA_BLOCCO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
