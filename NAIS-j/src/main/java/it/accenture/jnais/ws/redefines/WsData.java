package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-DATA<br>
 * Variable: WS-DATA from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsData extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsData() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_DATA;
    }

    public void setWsData(int wsData) {
        writeInt(Pos.WS_DATA, wsData, Len.Int.WS_DATA, SignType.NO_SIGN);
    }

    public void setWsDataFormatted(String wsData) {
        writeString(Pos.WS_DATA, Trunc.toUnsignedNumeric(wsData, Len.WS_DATA), Len.WS_DATA);
    }

    /**Original name: WS-DATA<br>
	 * <pre>--------------</pre>*/
    public int getWsData() {
        return readNumDispUnsignedInt(Pos.WS_DATA, Len.WS_DATA);
    }

    public String getWsDataFormatted() {
        return readFixedString(Pos.WS_DATA, Len.WS_DATA);
    }

    public void setDataGg(short dataGg) {
        writeShort(Pos.DATA_GG, dataGg, Len.Int.DATA_GG, SignType.NO_SIGN);
    }

    public void setDataGgFormatted(String dataGg) {
        writeString(Pos.DATA_GG, Trunc.toUnsignedNumeric(dataGg, Len.DATA_GG), Len.DATA_GG);
    }

    /**Original name: WS-DATA-GG<br>*/
    public short getDataGg() {
        return readNumDispUnsignedShort(Pos.DATA_GG, Len.DATA_GG);
    }

    public String getDataGgFormatted() {
        return readFixedString(Pos.DATA_GG, Len.DATA_GG);
    }

    public void setDataMm(short dataMm) {
        writeShort(Pos.DATA_MM, dataMm, Len.Int.DATA_MM, SignType.NO_SIGN);
    }

    public void setDataMmFormatted(String dataMm) {
        writeString(Pos.DATA_MM, Trunc.toUnsignedNumeric(dataMm, Len.DATA_MM), Len.DATA_MM);
    }

    /**Original name: WS-DATA-MM<br>*/
    public short getDataMm() {
        return readNumDispUnsignedShort(Pos.DATA_MM, Len.DATA_MM);
    }

    public String getDataMmFormatted() {
        return readFixedString(Pos.DATA_MM, Len.DATA_MM);
    }

    public void setDataSsFormatted(String dataSs) {
        writeString(Pos.DATA_SS, Trunc.toUnsignedNumeric(dataSs, Len.DATA_SS), Len.DATA_SS);
    }

    /**Original name: WS-DATA-SS<br>*/
    public short getDataSs() {
        return readNumDispUnsignedShort(Pos.DATA_SS, Len.DATA_SS);
    }

    public String getDataSsFormatted() {
        return readFixedString(Pos.DATA_SS, Len.DATA_SS);
    }

    public void setDataAaFormatted(String dataAa) {
        writeString(Pos.DATA_AA, Trunc.toUnsignedNumeric(dataAa, Len.DATA_AA), Len.DATA_AA);
    }

    /**Original name: WS-DATA-AA<br>*/
    public short getDataAa() {
        return readNumDispUnsignedShort(Pos.DATA_AA, Len.DATA_AA);
    }

    public String getDataAaFormatted() {
        return readFixedString(Pos.DATA_AA, Len.DATA_AA);
    }

    public void setDataSsaa(short dataSsaa) {
        writeShort(Pos.DATA_SSAA, dataSsaa, Len.Int.DATA_SSAA, SignType.NO_SIGN);
    }

    /**Original name: WS-DATA-SSAA<br>*/
    public short getDataSsaa() {
        return readNumDispUnsignedShort(Pos.DATA_SSAA, Len.DATA_SSAA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_DATA = 1;
        public static final int FILLER_WS_DATA_X = 1;
        public static final int DATA_GG = FILLER_WS_DATA_X;
        public static final int DATA_MM = DATA_GG + Len.DATA_GG;
        public static final int DATA_SS = DATA_MM + Len.DATA_MM;
        public static final int DATA_AA = DATA_SS + Len.DATA_SS;
        public static final int FILLER_WS_DATA_X1 = 1;
        public static final int FLR1 = FILLER_WS_DATA_X1;
        public static final int DATA_SSAA = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_GG = 2;
        public static final int DATA_MM = 2;
        public static final int DATA_SS = 2;
        public static final int FLR1 = 4;
        public static final int WS_DATA = 8;
        public static final int DATA_AA = 2;
        public static final int DATA_SSAA = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_DATA = 8;
            public static final int DATA_GG = 2;
            public static final int DATA_MM = 2;
            public static final int DATA_SSAA = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
