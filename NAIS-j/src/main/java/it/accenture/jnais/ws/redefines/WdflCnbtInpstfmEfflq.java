package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNBT-INPSTFM-EFFLQ<br>
 * Variable: WDFL-CNBT-INPSTFM-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCnbtInpstfmEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCnbtInpstfmEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNBT_INPSTFM_EFFLQ;
    }

    public void setWdflCnbtInpstfmEfflq(AfDecimal wdflCnbtInpstfmEfflq) {
        writeDecimalAsPacked(Pos.WDFL_CNBT_INPSTFM_EFFLQ, wdflCnbtInpstfmEfflq.copy());
    }

    public void setWdflCnbtInpstfmEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNBT_INPSTFM_EFFLQ, Pos.WDFL_CNBT_INPSTFM_EFFLQ);
    }

    /**Original name: WDFL-CNBT-INPSTFM-EFFLQ<br>*/
    public AfDecimal getWdflCnbtInpstfmEfflq() {
        return readPackedAsDecimal(Pos.WDFL_CNBT_INPSTFM_EFFLQ, Len.Int.WDFL_CNBT_INPSTFM_EFFLQ, Len.Fract.WDFL_CNBT_INPSTFM_EFFLQ);
    }

    public byte[] getWdflCnbtInpstfmEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNBT_INPSTFM_EFFLQ, Pos.WDFL_CNBT_INPSTFM_EFFLQ);
        return buffer;
    }

    public void setWdflCnbtInpstfmEfflqNull(String wdflCnbtInpstfmEfflqNull) {
        writeString(Pos.WDFL_CNBT_INPSTFM_EFFLQ_NULL, wdflCnbtInpstfmEfflqNull, Len.WDFL_CNBT_INPSTFM_EFFLQ_NULL);
    }

    /**Original name: WDFL-CNBT-INPSTFM-EFFLQ-NULL<br>*/
    public String getWdflCnbtInpstfmEfflqNull() {
        return readString(Pos.WDFL_CNBT_INPSTFM_EFFLQ_NULL, Len.WDFL_CNBT_INPSTFM_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNBT_INPSTFM_EFFLQ = 1;
        public static final int WDFL_CNBT_INPSTFM_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNBT_INPSTFM_EFFLQ = 8;
        public static final int WDFL_CNBT_INPSTFM_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNBT_INPSTFM_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNBT_INPSTFM_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
