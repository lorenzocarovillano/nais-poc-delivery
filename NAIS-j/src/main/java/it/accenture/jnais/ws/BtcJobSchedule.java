package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.BjsDtEnd;
import it.accenture.jnais.ws.redefines.BjsDtStart;
import it.accenture.jnais.ws.redefines.BjsExecutionsCount;
import it.accenture.jnais.ws.redefines.BjsTpMovi;

/**Original name: BTC-JOB-SCHEDULE<br>
 * Variable: BTC-JOB-SCHEDULE from copybook IDBVBJS1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcJobSchedule extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: BJS-ID-BATCH
    private int bjsIdBatch = DefaultValues.BIN_INT_VAL;
    //Original name: BJS-ID-JOB
    private int bjsIdJob = DefaultValues.BIN_INT_VAL;
    //Original name: BJS-COD-ELAB-STATE
    private char bjsCodElabState = DefaultValues.CHAR_VAL;
    //Original name: BJS-FLAG-WARNINGS
    private char bjsFlagWarnings = DefaultValues.CHAR_VAL;
    //Original name: BJS-DT-START
    private BjsDtStart bjsDtStart = new BjsDtStart();
    //Original name: BJS-DT-END
    private BjsDtEnd bjsDtEnd = new BjsDtEnd();
    //Original name: BJS-USER-START
    private String bjsUserStart = DefaultValues.stringVal(Len.BJS_USER_START);
    //Original name: BJS-FLAG-ALWAYS-EXE
    private char bjsFlagAlwaysExe = DefaultValues.CHAR_VAL;
    //Original name: BJS-EXECUTIONS-COUNT
    private BjsExecutionsCount bjsExecutionsCount = new BjsExecutionsCount();
    //Original name: BJS-DATA-CONTENT-TYPE
    private String bjsDataContentType = DefaultValues.stringVal(Len.BJS_DATA_CONTENT_TYPE);
    //Original name: BJS-COD-MACROFUNCT
    private String bjsCodMacrofunct = DefaultValues.stringVal(Len.BJS_COD_MACROFUNCT);
    //Original name: BJS-TP-MOVI
    private BjsTpMovi bjsTpMovi = new BjsTpMovi();
    //Original name: BJS-IB-OGG
    private String bjsIbOgg = DefaultValues.stringVal(Len.BJS_IB_OGG);
    //Original name: BJS-DATA-LEN
    private short bjsDataLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BJS-DATA
    private String bjsData = DefaultValues.stringVal(Len.BJS_DATA);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_JOB_SCHEDULE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcJobScheduleBytes(buf);
    }

    public String getBtcJobScheduleFormatted() {
        return MarshalByteExt.bufferToStr(getBtcJobScheduleBytes());
    }

    public void setBtcJobScheduleBytes(byte[] buffer) {
        setBtcJobScheduleBytes(buffer, 1);
    }

    public byte[] getBtcJobScheduleBytes() {
        byte[] buffer = new byte[Len.BTC_JOB_SCHEDULE];
        return getBtcJobScheduleBytes(buffer, 1);
    }

    public void setBtcJobScheduleBytes(byte[] buffer, int offset) {
        int position = offset;
        bjsIdBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bjsIdJob = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        bjsCodElabState = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bjsFlagWarnings = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bjsDtStart.setBjsDtStartFromBuffer(buffer, position);
        position += BjsDtStart.Len.BJS_DT_START;
        bjsDtEnd.setBjsDtEndFromBuffer(buffer, position);
        position += BjsDtEnd.Len.BJS_DT_END;
        bjsUserStart = MarshalByte.readString(buffer, position, Len.BJS_USER_START);
        position += Len.BJS_USER_START;
        bjsFlagAlwaysExe = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        bjsExecutionsCount.setBjsExecutionsCountFromBuffer(buffer, position);
        position += Types.INT_SIZE;
        bjsDataContentType = MarshalByte.readString(buffer, position, Len.BJS_DATA_CONTENT_TYPE);
        position += Len.BJS_DATA_CONTENT_TYPE;
        bjsCodMacrofunct = MarshalByte.readString(buffer, position, Len.BJS_COD_MACROFUNCT);
        position += Len.BJS_COD_MACROFUNCT;
        bjsTpMovi.setBjsTpMoviFromBuffer(buffer, position);
        position += BjsTpMovi.Len.BJS_TP_MOVI;
        bjsIbOgg = MarshalByte.readString(buffer, position, Len.BJS_IB_OGG);
        position += Len.BJS_IB_OGG;
        setBjsDataVcharBytes(buffer, position);
    }

    public byte[] getBtcJobScheduleBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, bjsIdBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, bjsIdJob);
        position += Types.INT_SIZE;
        MarshalByte.writeChar(buffer, position, bjsCodElabState);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, bjsFlagWarnings);
        position += Types.CHAR_SIZE;
        bjsDtStart.getBjsDtStartAsBuffer(buffer, position);
        position += BjsDtStart.Len.BJS_DT_START;
        bjsDtEnd.getBjsDtEndAsBuffer(buffer, position);
        position += BjsDtEnd.Len.BJS_DT_END;
        MarshalByte.writeString(buffer, position, bjsUserStart, Len.BJS_USER_START);
        position += Len.BJS_USER_START;
        MarshalByte.writeChar(buffer, position, bjsFlagAlwaysExe);
        position += Types.CHAR_SIZE;
        bjsExecutionsCount.getBjsExecutionsCountAsBuffer(buffer, position);
        position += Types.INT_SIZE;
        MarshalByte.writeString(buffer, position, bjsDataContentType, Len.BJS_DATA_CONTENT_TYPE);
        position += Len.BJS_DATA_CONTENT_TYPE;
        MarshalByte.writeString(buffer, position, bjsCodMacrofunct, Len.BJS_COD_MACROFUNCT);
        position += Len.BJS_COD_MACROFUNCT;
        bjsTpMovi.getBjsTpMoviAsBuffer(buffer, position);
        position += BjsTpMovi.Len.BJS_TP_MOVI;
        MarshalByte.writeString(buffer, position, bjsIbOgg, Len.BJS_IB_OGG);
        position += Len.BJS_IB_OGG;
        getBjsDataVcharBytes(buffer, position);
        return buffer;
    }

    public void setBjsIdBatch(int bjsIdBatch) {
        this.bjsIdBatch = bjsIdBatch;
    }

    public int getBjsIdBatch() {
        return this.bjsIdBatch;
    }

    public void setBjsIdJob(int bjsIdJob) {
        this.bjsIdJob = bjsIdJob;
    }

    public int getBjsIdJob() {
        return this.bjsIdJob;
    }

    public void setBjsCodElabState(char bjsCodElabState) {
        this.bjsCodElabState = bjsCodElabState;
    }

    public char getBjsCodElabState() {
        return this.bjsCodElabState;
    }

    public void setBjsFlagWarnings(char bjsFlagWarnings) {
        this.bjsFlagWarnings = bjsFlagWarnings;
    }

    public char getBjsFlagWarnings() {
        return this.bjsFlagWarnings;
    }

    public void setBjsUserStart(String bjsUserStart) {
        this.bjsUserStart = Functions.subString(bjsUserStart, Len.BJS_USER_START);
    }

    public String getBjsUserStart() {
        return this.bjsUserStart;
    }

    public void setBjsFlagAlwaysExe(char bjsFlagAlwaysExe) {
        this.bjsFlagAlwaysExe = bjsFlagAlwaysExe;
    }

    public char getBjsFlagAlwaysExe() {
        return this.bjsFlagAlwaysExe;
    }

    public void setBjsDataContentType(String bjsDataContentType) {
        this.bjsDataContentType = Functions.subString(bjsDataContentType, Len.BJS_DATA_CONTENT_TYPE);
    }

    public String getBjsDataContentType() {
        return this.bjsDataContentType;
    }

    public void setBjsCodMacrofunct(String bjsCodMacrofunct) {
        this.bjsCodMacrofunct = Functions.subString(bjsCodMacrofunct, Len.BJS_COD_MACROFUNCT);
    }

    public String getBjsCodMacrofunct() {
        return this.bjsCodMacrofunct;
    }

    public String getBjsCodMacrofunctFormatted() {
        return Functions.padBlanks(getBjsCodMacrofunct(), Len.BJS_COD_MACROFUNCT);
    }

    public void setBjsIbOgg(String bjsIbOgg) {
        this.bjsIbOgg = Functions.subString(bjsIbOgg, Len.BJS_IB_OGG);
    }

    public void setBjsIbOggSubstring(String replacement, int start, int length) {
        bjsIbOgg = Functions.setSubstring(bjsIbOgg, replacement, start, length);
    }

    public String getBjsIbOgg() {
        return this.bjsIbOgg;
    }

    public String getBjsIbOggFormatted() {
        return Functions.padBlanks(getBjsIbOgg(), Len.BJS_IB_OGG);
    }

    public void setBjsDataVcharFormatted(String data) {
        byte[] buffer = new byte[Len.BJS_DATA_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.BJS_DATA_VCHAR);
        setBjsDataVcharBytes(buffer, 1);
    }

    public String getBjsDataVcharFormatted() {
        return MarshalByteExt.bufferToStr(getBjsDataVcharBytes());
    }

    /**Original name: BJS-DATA-VCHAR<br>*/
    public byte[] getBjsDataVcharBytes() {
        byte[] buffer = new byte[Len.BJS_DATA_VCHAR];
        return getBjsDataVcharBytes(buffer, 1);
    }

    public void setBjsDataVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        bjsDataLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        bjsData = MarshalByte.readString(buffer, position, Len.BJS_DATA);
    }

    public byte[] getBjsDataVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, bjsDataLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, bjsData, Len.BJS_DATA);
        return buffer;
    }

    public void setBjsDataLen(short bjsDataLen) {
        this.bjsDataLen = bjsDataLen;
    }

    public short getBjsDataLen() {
        return this.bjsDataLen;
    }

    public void setBjsData(String bjsData) {
        this.bjsData = Functions.subString(bjsData, Len.BJS_DATA);
    }

    public String getBjsData() {
        return this.bjsData;
    }

    public String getBjsDataFormatted() {
        return Functions.padBlanks(getBjsData(), Len.BJS_DATA);
    }

    public BjsDtEnd getBjsDtEnd() {
        return bjsDtEnd;
    }

    public BjsDtStart getBjsDtStart() {
        return bjsDtStart;
    }

    public BjsExecutionsCount getBjsExecutionsCount() {
        return bjsExecutionsCount;
    }

    public BjsTpMovi getBjsTpMovi() {
        return bjsTpMovi;
    }

    @Override
    public byte[] serialize() {
        return getBtcJobScheduleBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BJS_ID_BATCH = 4;
        public static final int BJS_ID_JOB = 4;
        public static final int BJS_COD_ELAB_STATE = 1;
        public static final int BJS_FLAG_WARNINGS = 1;
        public static final int BJS_USER_START = 30;
        public static final int BJS_FLAG_ALWAYS_EXE = 1;
        public static final int BJS_DATA_CONTENT_TYPE = 30;
        public static final int BJS_COD_MACROFUNCT = 2;
        public static final int BJS_IB_OGG = 100;
        public static final int BJS_DATA_LEN = 2;
        public static final int BJS_DATA = 32000;
        public static final int BJS_DATA_VCHAR = BJS_DATA_LEN + BJS_DATA;
        public static final int BTC_JOB_SCHEDULE = BJS_ID_BATCH + BJS_ID_JOB + BJS_COD_ELAB_STATE + BJS_FLAG_WARNINGS + BjsDtStart.Len.BJS_DT_START + BjsDtEnd.Len.BJS_DT_END + BJS_USER_START + BJS_FLAG_ALWAYS_EXE + BjsExecutionsCount.Len.BJS_EXECUTIONS_COUNT + BJS_DATA_CONTENT_TYPE + BJS_COD_MACROFUNCT + BjsTpMovi.Len.BJS_TP_MOVI + BJS_IB_OGG + BJS_DATA_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
