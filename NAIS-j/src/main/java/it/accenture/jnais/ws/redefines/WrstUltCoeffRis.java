package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-ULT-COEFF-RIS<br>
 * Variable: WRST-ULT-COEFF-RIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstUltCoeffRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstUltCoeffRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_ULT_COEFF_RIS;
    }

    public void setWrstUltCoeffRis(AfDecimal wrstUltCoeffRis) {
        writeDecimalAsPacked(Pos.WRST_ULT_COEFF_RIS, wrstUltCoeffRis.copy());
    }

    public void setWrstUltCoeffRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_ULT_COEFF_RIS, Pos.WRST_ULT_COEFF_RIS);
    }

    /**Original name: WRST-ULT-COEFF-RIS<br>*/
    public AfDecimal getWrstUltCoeffRis() {
        return readPackedAsDecimal(Pos.WRST_ULT_COEFF_RIS, Len.Int.WRST_ULT_COEFF_RIS, Len.Fract.WRST_ULT_COEFF_RIS);
    }

    public byte[] getWrstUltCoeffRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_ULT_COEFF_RIS, Pos.WRST_ULT_COEFF_RIS);
        return buffer;
    }

    public void initWrstUltCoeffRisSpaces() {
        fill(Pos.WRST_ULT_COEFF_RIS, Len.WRST_ULT_COEFF_RIS, Types.SPACE_CHAR);
    }

    public void setWrstUltCoeffRisNull(String wrstUltCoeffRisNull) {
        writeString(Pos.WRST_ULT_COEFF_RIS_NULL, wrstUltCoeffRisNull, Len.WRST_ULT_COEFF_RIS_NULL);
    }

    /**Original name: WRST-ULT-COEFF-RIS-NULL<br>*/
    public String getWrstUltCoeffRisNull() {
        return readString(Pos.WRST_ULT_COEFF_RIS_NULL, Len.WRST_ULT_COEFF_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_ULT_COEFF_RIS = 1;
        public static final int WRST_ULT_COEFF_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_ULT_COEFF_RIS = 8;
        public static final int WRST_ULT_COEFF_RIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_ULT_COEFF_RIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_ULT_COEFF_RIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
