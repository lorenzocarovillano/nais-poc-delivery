package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-INI-NET<br>
 * Variable: TGA-PRE-INI-NET from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreIniNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreIniNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_INI_NET;
    }

    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        writeDecimalAsPacked(Pos.TGA_PRE_INI_NET, tgaPreIniNet.copy());
    }

    public void setTgaPreIniNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_INI_NET, Pos.TGA_PRE_INI_NET);
    }

    /**Original name: TGA-PRE-INI-NET<br>*/
    public AfDecimal getTgaPreIniNet() {
        return readPackedAsDecimal(Pos.TGA_PRE_INI_NET, Len.Int.TGA_PRE_INI_NET, Len.Fract.TGA_PRE_INI_NET);
    }

    public byte[] getTgaPreIniNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_INI_NET, Pos.TGA_PRE_INI_NET);
        return buffer;
    }

    public void setTgaPreIniNetNull(String tgaPreIniNetNull) {
        writeString(Pos.TGA_PRE_INI_NET_NULL, tgaPreIniNetNull, Len.TGA_PRE_INI_NET_NULL);
    }

    /**Original name: TGA-PRE-INI-NET-NULL<br>*/
    public String getTgaPreIniNetNull() {
        return readString(Pos.TGA_PRE_INI_NET_NULL, Len.TGA_PRE_INI_NET_NULL);
    }

    public String getTgaPreIniNetNullFormatted() {
        return Functions.padBlanks(getTgaPreIniNetNull(), Len.TGA_PRE_INI_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_INI_NET = 1;
        public static final int TGA_PRE_INI_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_INI_NET = 8;
        public static final int TGA_PRE_INI_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_INI_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_INI_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
