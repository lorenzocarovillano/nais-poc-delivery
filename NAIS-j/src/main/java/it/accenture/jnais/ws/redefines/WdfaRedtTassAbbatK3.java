package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-REDT-TASS-ABBAT-K3<br>
 * Variable: WDFA-REDT-TASS-ABBAT-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaRedtTassAbbatK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaRedtTassAbbatK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_REDT_TASS_ABBAT_K3;
    }

    public void setWdfaRedtTassAbbatK3(AfDecimal wdfaRedtTassAbbatK3) {
        writeDecimalAsPacked(Pos.WDFA_REDT_TASS_ABBAT_K3, wdfaRedtTassAbbatK3.copy());
    }

    public void setWdfaRedtTassAbbatK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_REDT_TASS_ABBAT_K3, Pos.WDFA_REDT_TASS_ABBAT_K3);
    }

    /**Original name: WDFA-REDT-TASS-ABBAT-K3<br>*/
    public AfDecimal getWdfaRedtTassAbbatK3() {
        return readPackedAsDecimal(Pos.WDFA_REDT_TASS_ABBAT_K3, Len.Int.WDFA_REDT_TASS_ABBAT_K3, Len.Fract.WDFA_REDT_TASS_ABBAT_K3);
    }

    public byte[] getWdfaRedtTassAbbatK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_REDT_TASS_ABBAT_K3, Pos.WDFA_REDT_TASS_ABBAT_K3);
        return buffer;
    }

    public void setWdfaRedtTassAbbatK3Null(String wdfaRedtTassAbbatK3Null) {
        writeString(Pos.WDFA_REDT_TASS_ABBAT_K3_NULL, wdfaRedtTassAbbatK3Null, Len.WDFA_REDT_TASS_ABBAT_K3_NULL);
    }

    /**Original name: WDFA-REDT-TASS-ABBAT-K3-NULL<br>*/
    public String getWdfaRedtTassAbbatK3Null() {
        return readString(Pos.WDFA_REDT_TASS_ABBAT_K3_NULL, Len.WDFA_REDT_TASS_ABBAT_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_REDT_TASS_ABBAT_K3 = 1;
        public static final int WDFA_REDT_TASS_ABBAT_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_REDT_TASS_ABBAT_K3 = 8;
        public static final int WDFA_REDT_TASS_ABBAT_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_REDT_TASS_ABBAT_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_REDT_TASS_ABBAT_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
