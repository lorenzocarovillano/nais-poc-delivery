package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0501-SEGNO-POSTIV<br>
 * Variable: IDSV0501-SEGNO-POSTIV from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0501SegnoPostiv {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIdsv0501SegnoPostiv(char idsv0501SegnoPostiv) {
        this.value = idsv0501SegnoPostiv;
    }

    public char getIdsv0501SegnoPostiv() {
        return this.value;
    }

    public boolean isIdsv0501SegnoPostivSi() {
        return value == SI;
    }

    public void setIdsv0501SegnoPostivNo() {
        value = NO;
    }
}
