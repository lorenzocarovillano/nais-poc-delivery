package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-BOLLO-TOT-VD<br>
 * Variable: DFL-IMPST-BOLLO-TOT-VD from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstBolloTotVd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstBolloTotVd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_BOLLO_TOT_VD;
    }

    public void setDflImpstBolloTotVd(AfDecimal dflImpstBolloTotVd) {
        writeDecimalAsPacked(Pos.DFL_IMPST_BOLLO_TOT_VD, dflImpstBolloTotVd.copy());
    }

    public void setDflImpstBolloTotVdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_BOLLO_TOT_VD, Pos.DFL_IMPST_BOLLO_TOT_VD);
    }

    /**Original name: DFL-IMPST-BOLLO-TOT-VD<br>*/
    public AfDecimal getDflImpstBolloTotVd() {
        return readPackedAsDecimal(Pos.DFL_IMPST_BOLLO_TOT_VD, Len.Int.DFL_IMPST_BOLLO_TOT_VD, Len.Fract.DFL_IMPST_BOLLO_TOT_VD);
    }

    public byte[] getDflImpstBolloTotVdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_BOLLO_TOT_VD, Pos.DFL_IMPST_BOLLO_TOT_VD);
        return buffer;
    }

    public void setDflImpstBolloTotVdNull(String dflImpstBolloTotVdNull) {
        writeString(Pos.DFL_IMPST_BOLLO_TOT_VD_NULL, dflImpstBolloTotVdNull, Len.DFL_IMPST_BOLLO_TOT_VD_NULL);
    }

    /**Original name: DFL-IMPST-BOLLO-TOT-VD-NULL<br>*/
    public String getDflImpstBolloTotVdNull() {
        return readString(Pos.DFL_IMPST_BOLLO_TOT_VD_NULL, Len.DFL_IMPST_BOLLO_TOT_VD_NULL);
    }

    public String getDflImpstBolloTotVdNullFormatted() {
        return Functions.padBlanks(getDflImpstBolloTotVdNull(), Len.DFL_IMPST_BOLLO_TOT_VD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_TOT_VD = 1;
        public static final int DFL_IMPST_BOLLO_TOT_VD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_TOT_VD = 8;
        public static final int DFL_IMPST_BOLLO_TOT_VD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_TOT_VD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_TOT_VD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
