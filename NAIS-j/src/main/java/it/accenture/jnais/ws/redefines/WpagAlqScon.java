package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-ALQ-SCON<br>
 * Variable: WPAG-ALQ-SCON from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAlqScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAlqScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_ALQ_SCON;
    }

    public void setWpagAlqScon(AfDecimal wpagAlqScon) {
        writeDecimalAsPacked(Pos.WPAG_ALQ_SCON, wpagAlqScon.copy());
    }

    public void setWpagAlqSconFormatted(String wpagAlqScon) {
        setWpagAlqScon(PicParser.display(new PicParams("S9(3)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_ALQ_SCON + Len.Fract.WPAG_ALQ_SCON, Len.Fract.WPAG_ALQ_SCON, wpagAlqScon));
    }

    public void setWpagAlqSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_ALQ_SCON, Pos.WPAG_ALQ_SCON);
    }

    /**Original name: WPAG-ALQ-SCON<br>*/
    public AfDecimal getWpagAlqScon() {
        return readPackedAsDecimal(Pos.WPAG_ALQ_SCON, Len.Int.WPAG_ALQ_SCON, Len.Fract.WPAG_ALQ_SCON);
    }

    public byte[] getWpagAlqSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_ALQ_SCON, Pos.WPAG_ALQ_SCON);
        return buffer;
    }

    public void initWpagAlqSconSpaces() {
        fill(Pos.WPAG_ALQ_SCON, Len.WPAG_ALQ_SCON, Types.SPACE_CHAR);
    }

    public void setWpagAlqSconNull(String wpagAlqSconNull) {
        writeString(Pos.WPAG_ALQ_SCON_NULL, wpagAlqSconNull, Len.WPAG_ALQ_SCON_NULL);
    }

    /**Original name: WPAG-ALQ-SCON-NULL<br>*/
    public String getWpagAlqSconNull() {
        return readString(Pos.WPAG_ALQ_SCON_NULL, Len.WPAG_ALQ_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_ALQ_SCON = 1;
        public static final int WPAG_ALQ_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ALQ_SCON = 4;
        public static final int WPAG_ALQ_SCON_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
