package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-BUFFER-DATI<br>
 * Variable: FLAG-BUFFER-DATI from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagBufferDati {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagBufferDati(char flagBufferDati) {
        this.value = flagBufferDati;
    }

    public char getFlagBufferDati() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
