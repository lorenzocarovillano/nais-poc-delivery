package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-RIS-MAT<br>
 * Variable: TGA-RIS-MAT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_RIS_MAT;
    }

    public void setTgaRisMat(AfDecimal tgaRisMat) {
        writeDecimalAsPacked(Pos.TGA_RIS_MAT, tgaRisMat.copy());
    }

    public void setTgaRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_RIS_MAT, Pos.TGA_RIS_MAT);
    }

    /**Original name: TGA-RIS-MAT<br>*/
    public AfDecimal getTgaRisMat() {
        return readPackedAsDecimal(Pos.TGA_RIS_MAT, Len.Int.TGA_RIS_MAT, Len.Fract.TGA_RIS_MAT);
    }

    public byte[] getTgaRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_RIS_MAT, Pos.TGA_RIS_MAT);
        return buffer;
    }

    public void setTgaRisMatNull(String tgaRisMatNull) {
        writeString(Pos.TGA_RIS_MAT_NULL, tgaRisMatNull, Len.TGA_RIS_MAT_NULL);
    }

    /**Original name: TGA-RIS-MAT-NULL<br>*/
    public String getTgaRisMatNull() {
        return readString(Pos.TGA_RIS_MAT_NULL, Len.TGA_RIS_MAT_NULL);
    }

    public String getTgaRisMatNullFormatted() {
        return Functions.padBlanks(getTgaRisMatNull(), Len.TGA_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_RIS_MAT = 1;
        public static final int TGA_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_RIS_MAT = 8;
        public static final int TGA_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
