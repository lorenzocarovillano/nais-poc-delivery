package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-AA-PAG-PRE-UNI<br>
 * Variable: WGRZ-AA-PAG-PRE-UNI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzAaPagPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzAaPagPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_AA_PAG_PRE_UNI;
    }

    public void setWgrzAaPagPreUni(int wgrzAaPagPreUni) {
        writeIntAsPacked(Pos.WGRZ_AA_PAG_PRE_UNI, wgrzAaPagPreUni, Len.Int.WGRZ_AA_PAG_PRE_UNI);
    }

    public void setWgrzAaPagPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_AA_PAG_PRE_UNI, Pos.WGRZ_AA_PAG_PRE_UNI);
    }

    /**Original name: WGRZ-AA-PAG-PRE-UNI<br>*/
    public int getWgrzAaPagPreUni() {
        return readPackedAsInt(Pos.WGRZ_AA_PAG_PRE_UNI, Len.Int.WGRZ_AA_PAG_PRE_UNI);
    }

    public byte[] getWgrzAaPagPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_AA_PAG_PRE_UNI, Pos.WGRZ_AA_PAG_PRE_UNI);
        return buffer;
    }

    public void initWgrzAaPagPreUniSpaces() {
        fill(Pos.WGRZ_AA_PAG_PRE_UNI, Len.WGRZ_AA_PAG_PRE_UNI, Types.SPACE_CHAR);
    }

    public void setWgrzAaPagPreUniNull(String wgrzAaPagPreUniNull) {
        writeString(Pos.WGRZ_AA_PAG_PRE_UNI_NULL, wgrzAaPagPreUniNull, Len.WGRZ_AA_PAG_PRE_UNI_NULL);
    }

    /**Original name: WGRZ-AA-PAG-PRE-UNI-NULL<br>*/
    public String getWgrzAaPagPreUniNull() {
        return readString(Pos.WGRZ_AA_PAG_PRE_UNI_NULL, Len.WGRZ_AA_PAG_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_AA_PAG_PRE_UNI = 1;
        public static final int WGRZ_AA_PAG_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_AA_PAG_PRE_UNI = 3;
        public static final int WGRZ_AA_PAG_PRE_UNI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_AA_PAG_PRE_UNI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
