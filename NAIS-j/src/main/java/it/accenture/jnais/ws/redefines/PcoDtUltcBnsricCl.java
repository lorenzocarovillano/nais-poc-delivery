package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-BNSRIC-CL<br>
 * Variable: PCO-DT-ULTC-BNSRIC-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcBnsricCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcBnsricCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_BNSRIC_CL;
    }

    public void setPcoDtUltcBnsricCl(int pcoDtUltcBnsricCl) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_BNSRIC_CL, pcoDtUltcBnsricCl, Len.Int.PCO_DT_ULTC_BNSRIC_CL);
    }

    public void setPcoDtUltcBnsricClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_BNSRIC_CL, Pos.PCO_DT_ULTC_BNSRIC_CL);
    }

    /**Original name: PCO-DT-ULTC-BNSRIC-CL<br>*/
    public int getPcoDtUltcBnsricCl() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_BNSRIC_CL, Len.Int.PCO_DT_ULTC_BNSRIC_CL);
    }

    public byte[] getPcoDtUltcBnsricClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_BNSRIC_CL, Pos.PCO_DT_ULTC_BNSRIC_CL);
        return buffer;
    }

    public void initPcoDtUltcBnsricClHighValues() {
        fill(Pos.PCO_DT_ULTC_BNSRIC_CL, Len.PCO_DT_ULTC_BNSRIC_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcBnsricClNull(String pcoDtUltcBnsricClNull) {
        writeString(Pos.PCO_DT_ULTC_BNSRIC_CL_NULL, pcoDtUltcBnsricClNull, Len.PCO_DT_ULTC_BNSRIC_CL_NULL);
    }

    /**Original name: PCO-DT-ULTC-BNSRIC-CL-NULL<br>*/
    public String getPcoDtUltcBnsricClNull() {
        return readString(Pos.PCO_DT_ULTC_BNSRIC_CL_NULL, Len.PCO_DT_ULTC_BNSRIC_CL_NULL);
    }

    public String getPcoDtUltcBnsricClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcBnsricClNull(), Len.PCO_DT_ULTC_BNSRIC_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSRIC_CL = 1;
        public static final int PCO_DT_ULTC_BNSRIC_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSRIC_CL = 5;
        public static final int PCO_DT_ULTC_BNSRIC_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_BNSRIC_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
