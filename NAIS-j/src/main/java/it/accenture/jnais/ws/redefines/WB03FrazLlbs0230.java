package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-FRAZ<br>
 * Variable: W-B03-FRAZ from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03FrazLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03FrazLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_FRAZ;
    }

    public void setwB03Fraz(int wB03Fraz) {
        writeIntAsPacked(Pos.W_B03_FRAZ, wB03Fraz, Len.Int.W_B03_FRAZ);
    }

    /**Original name: W-B03-FRAZ<br>*/
    public int getwB03Fraz() {
        return readPackedAsInt(Pos.W_B03_FRAZ, Len.Int.W_B03_FRAZ);
    }

    public byte[] getwB03FrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_FRAZ, Pos.W_B03_FRAZ);
        return buffer;
    }

    public void setwB03FrazNull(String wB03FrazNull) {
        writeString(Pos.W_B03_FRAZ_NULL, wB03FrazNull, Len.W_B03_FRAZ_NULL);
    }

    /**Original name: W-B03-FRAZ-NULL<br>*/
    public String getwB03FrazNull() {
        return readString(Pos.W_B03_FRAZ_NULL, Len.W_B03_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_FRAZ = 1;
        public static final int W_B03_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_FRAZ = 3;
        public static final int W_B03_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
