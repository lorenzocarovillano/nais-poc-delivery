package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SPE-AGE<br>
 * Variable: WTIT-TOT-SPE-AGE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SPE_AGE;
    }

    public void setWtitTotSpeAge(AfDecimal wtitTotSpeAge) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SPE_AGE, wtitTotSpeAge.copy());
    }

    public void setWtitTotSpeAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SPE_AGE, Pos.WTIT_TOT_SPE_AGE);
    }

    /**Original name: WTIT-TOT-SPE-AGE<br>*/
    public AfDecimal getWtitTotSpeAge() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SPE_AGE, Len.Int.WTIT_TOT_SPE_AGE, Len.Fract.WTIT_TOT_SPE_AGE);
    }

    public byte[] getWtitTotSpeAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SPE_AGE, Pos.WTIT_TOT_SPE_AGE);
        return buffer;
    }

    public void initWtitTotSpeAgeSpaces() {
        fill(Pos.WTIT_TOT_SPE_AGE, Len.WTIT_TOT_SPE_AGE, Types.SPACE_CHAR);
    }

    public void setWtitTotSpeAgeNull(String wtitTotSpeAgeNull) {
        writeString(Pos.WTIT_TOT_SPE_AGE_NULL, wtitTotSpeAgeNull, Len.WTIT_TOT_SPE_AGE_NULL);
    }

    /**Original name: WTIT-TOT-SPE-AGE-NULL<br>*/
    public String getWtitTotSpeAgeNull() {
        return readString(Pos.WTIT_TOT_SPE_AGE_NULL, Len.WTIT_TOT_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SPE_AGE = 1;
        public static final int WTIT_TOT_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SPE_AGE = 8;
        public static final int WTIT_TOT_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
