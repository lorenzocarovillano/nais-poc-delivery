package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PRE-PP-IAS<br>
 * Variable: TDR-TOT-PRE-PP-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PRE_PP_IAS;
    }

    public void setTdrTotPrePpIas(AfDecimal tdrTotPrePpIas) {
        writeDecimalAsPacked(Pos.TDR_TOT_PRE_PP_IAS, tdrTotPrePpIas.copy());
    }

    public void setTdrTotPrePpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PRE_PP_IAS, Pos.TDR_TOT_PRE_PP_IAS);
    }

    /**Original name: TDR-TOT-PRE-PP-IAS<br>*/
    public AfDecimal getTdrTotPrePpIas() {
        return readPackedAsDecimal(Pos.TDR_TOT_PRE_PP_IAS, Len.Int.TDR_TOT_PRE_PP_IAS, Len.Fract.TDR_TOT_PRE_PP_IAS);
    }

    public byte[] getTdrTotPrePpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PRE_PP_IAS, Pos.TDR_TOT_PRE_PP_IAS);
        return buffer;
    }

    public void setTdrTotPrePpIasNull(String tdrTotPrePpIasNull) {
        writeString(Pos.TDR_TOT_PRE_PP_IAS_NULL, tdrTotPrePpIasNull, Len.TDR_TOT_PRE_PP_IAS_NULL);
    }

    /**Original name: TDR-TOT-PRE-PP-IAS-NULL<br>*/
    public String getTdrTotPrePpIasNull() {
        return readString(Pos.TDR_TOT_PRE_PP_IAS_NULL, Len.TDR_TOT_PRE_PP_IAS_NULL);
    }

    public String getTdrTotPrePpIasNullFormatted() {
        return Functions.padBlanks(getTdrTotPrePpIasNull(), Len.TDR_TOT_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_PP_IAS = 1;
        public static final int TDR_TOT_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_PP_IAS = 8;
        public static final int TDR_TOT_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
