package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PROV-ACQ-1AA<br>
 * Variable: WDTR-PROV-ACQ-1AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PROV_ACQ1AA;
    }

    public void setWdtrProvAcq1aa(AfDecimal wdtrProvAcq1aa) {
        writeDecimalAsPacked(Pos.WDTR_PROV_ACQ1AA, wdtrProvAcq1aa.copy());
    }

    /**Original name: WDTR-PROV-ACQ-1AA<br>*/
    public AfDecimal getWdtrProvAcq1aa() {
        return readPackedAsDecimal(Pos.WDTR_PROV_ACQ1AA, Len.Int.WDTR_PROV_ACQ1AA, Len.Fract.WDTR_PROV_ACQ1AA);
    }

    public void setWdtrProvAcq1aaNull(String wdtrProvAcq1aaNull) {
        writeString(Pos.WDTR_PROV_ACQ1AA_NULL, wdtrProvAcq1aaNull, Len.WDTR_PROV_ACQ1AA_NULL);
    }

    /**Original name: WDTR-PROV-ACQ-1AA-NULL<br>*/
    public String getWdtrProvAcq1aaNull() {
        return readString(Pos.WDTR_PROV_ACQ1AA_NULL, Len.WDTR_PROV_ACQ1AA_NULL);
    }

    public String getWdtrProvAcq1aaNullFormatted() {
        return Functions.padBlanks(getWdtrProvAcq1aaNull(), Len.WDTR_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_ACQ1AA = 1;
        public static final int WDTR_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_ACQ1AA = 8;
        public static final int WDTR_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
