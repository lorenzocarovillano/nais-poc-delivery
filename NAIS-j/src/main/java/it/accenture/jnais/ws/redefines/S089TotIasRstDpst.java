package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IAS-RST-DPST<br>
 * Variable: S089-TOT-IAS-RST-DPST from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotIasRstDpst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotIasRstDpst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IAS_RST_DPST;
    }

    public void setWlquTotIasRstDpst(AfDecimal wlquTotIasRstDpst) {
        writeDecimalAsPacked(Pos.S089_TOT_IAS_RST_DPST, wlquTotIasRstDpst.copy());
    }

    public void setWlquTotIasRstDpstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IAS_RST_DPST, Pos.S089_TOT_IAS_RST_DPST);
    }

    /**Original name: WLQU-TOT-IAS-RST-DPST<br>*/
    public AfDecimal getWlquTotIasRstDpst() {
        return readPackedAsDecimal(Pos.S089_TOT_IAS_RST_DPST, Len.Int.WLQU_TOT_IAS_RST_DPST, Len.Fract.WLQU_TOT_IAS_RST_DPST);
    }

    public byte[] getWlquTotIasRstDpstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IAS_RST_DPST, Pos.S089_TOT_IAS_RST_DPST);
        return buffer;
    }

    public void initWlquTotIasRstDpstSpaces() {
        fill(Pos.S089_TOT_IAS_RST_DPST, Len.S089_TOT_IAS_RST_DPST, Types.SPACE_CHAR);
    }

    public void setWlquTotIasRstDpstNull(String wlquTotIasRstDpstNull) {
        writeString(Pos.S089_TOT_IAS_RST_DPST_NULL, wlquTotIasRstDpstNull, Len.WLQU_TOT_IAS_RST_DPST_NULL);
    }

    /**Original name: WLQU-TOT-IAS-RST-DPST-NULL<br>*/
    public String getWlquTotIasRstDpstNull() {
        return readString(Pos.S089_TOT_IAS_RST_DPST_NULL, Len.WLQU_TOT_IAS_RST_DPST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_RST_DPST = 1;
        public static final int S089_TOT_IAS_RST_DPST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IAS_RST_DPST = 8;
        public static final int WLQU_TOT_IAS_RST_DPST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_RST_DPST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IAS_RST_DPST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
