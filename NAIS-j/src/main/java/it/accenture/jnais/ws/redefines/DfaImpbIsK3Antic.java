package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-IS-K3-ANTIC<br>
 * Variable: DFA-IMPB-IS-K3-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbIsK3Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbIsK3Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_IS_K3_ANTIC;
    }

    public void setDfaImpbIsK3Antic(AfDecimal dfaImpbIsK3Antic) {
        writeDecimalAsPacked(Pos.DFA_IMPB_IS_K3_ANTIC, dfaImpbIsK3Antic.copy());
    }

    public void setDfaImpbIsK3AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_IS_K3_ANTIC, Pos.DFA_IMPB_IS_K3_ANTIC);
    }

    /**Original name: DFA-IMPB-IS-K3-ANTIC<br>*/
    public AfDecimal getDfaImpbIsK3Antic() {
        return readPackedAsDecimal(Pos.DFA_IMPB_IS_K3_ANTIC, Len.Int.DFA_IMPB_IS_K3_ANTIC, Len.Fract.DFA_IMPB_IS_K3_ANTIC);
    }

    public byte[] getDfaImpbIsK3AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_IS_K3_ANTIC, Pos.DFA_IMPB_IS_K3_ANTIC);
        return buffer;
    }

    public void setDfaImpbIsK3AnticNull(String dfaImpbIsK3AnticNull) {
        writeString(Pos.DFA_IMPB_IS_K3_ANTIC_NULL, dfaImpbIsK3AnticNull, Len.DFA_IMPB_IS_K3_ANTIC_NULL);
    }

    /**Original name: DFA-IMPB-IS-K3-ANTIC-NULL<br>*/
    public String getDfaImpbIsK3AnticNull() {
        return readString(Pos.DFA_IMPB_IS_K3_ANTIC_NULL, Len.DFA_IMPB_IS_K3_ANTIC_NULL);
    }

    public String getDfaImpbIsK3AnticNullFormatted() {
        return Functions.padBlanks(getDfaImpbIsK3AnticNull(), Len.DFA_IMPB_IS_K3_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K3_ANTIC = 1;
        public static final int DFA_IMPB_IS_K3_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K3_ANTIC = 8;
        public static final int DFA_IMPB_IS_K3_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K3_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K3_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
