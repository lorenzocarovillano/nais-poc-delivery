package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEL-ID-MOVI-CHIU<br>
 * Variable: WBEL-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_ID_MOVI_CHIU;
    }

    public void setWbelIdMoviChiu(int wbelIdMoviChiu) {
        writeIntAsPacked(Pos.WBEL_ID_MOVI_CHIU, wbelIdMoviChiu, Len.Int.WBEL_ID_MOVI_CHIU);
    }

    public void setWbelIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_ID_MOVI_CHIU, Pos.WBEL_ID_MOVI_CHIU);
    }

    /**Original name: WBEL-ID-MOVI-CHIU<br>*/
    public int getWbelIdMoviChiu() {
        return readPackedAsInt(Pos.WBEL_ID_MOVI_CHIU, Len.Int.WBEL_ID_MOVI_CHIU);
    }

    public byte[] getWbelIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_ID_MOVI_CHIU, Pos.WBEL_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWbelIdMoviChiuSpaces() {
        fill(Pos.WBEL_ID_MOVI_CHIU, Len.WBEL_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWbelIdMoviChiuNull(String wbelIdMoviChiuNull) {
        writeString(Pos.WBEL_ID_MOVI_CHIU_NULL, wbelIdMoviChiuNull, Len.WBEL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WBEL-ID-MOVI-CHIU-NULL<br>*/
    public String getWbelIdMoviChiuNull() {
        return readString(Pos.WBEL_ID_MOVI_CHIU_NULL, Len.WBEL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_ID_MOVI_CHIU = 1;
        public static final int WBEL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_ID_MOVI_CHIU = 5;
        public static final int WBEL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
