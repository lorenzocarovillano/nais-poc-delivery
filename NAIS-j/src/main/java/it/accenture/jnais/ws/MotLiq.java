package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P86IdMoviChiu;

/**Original name: MOT-LIQ<br>
 * Variable: MOT-LIQ from copybook IDBVP861<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class MotLiq extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P86-ID-MOT-LIQ
    private int p86IdMotLiq = DefaultValues.INT_VAL;
    //Original name: P86-ID-LIQ
    private int p86IdLiq = DefaultValues.INT_VAL;
    //Original name: P86-ID-MOVI-CRZ
    private int p86IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P86-ID-MOVI-CHIU
    private P86IdMoviChiu p86IdMoviChiu = new P86IdMoviChiu();
    //Original name: P86-DT-INI-EFF
    private int p86DtIniEff = DefaultValues.INT_VAL;
    //Original name: P86-DT-END-EFF
    private int p86DtEndEff = DefaultValues.INT_VAL;
    //Original name: P86-COD-COMP-ANIA
    private int p86CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P86-TP-LIQ
    private String p86TpLiq = DefaultValues.stringVal(Len.P86_TP_LIQ);
    //Original name: P86-TP-MOT-LIQ
    private String p86TpMotLiq = DefaultValues.stringVal(Len.P86_TP_MOT_LIQ);
    //Original name: P86-DS-RIGA
    private long p86DsRiga = DefaultValues.LONG_VAL;
    //Original name: P86-DS-OPER-SQL
    private char p86DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P86-DS-VER
    private int p86DsVer = DefaultValues.INT_VAL;
    //Original name: P86-DS-TS-INI-CPTZ
    private long p86DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: P86-DS-TS-END-CPTZ
    private long p86DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: P86-DS-UTENTE
    private String p86DsUtente = DefaultValues.stringVal(Len.P86_DS_UTENTE);
    //Original name: P86-DS-STATO-ELAB
    private char p86DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P86-DESC-LIB-MOT-LIQ-LEN
    private short p86DescLibMotLiqLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P86-DESC-LIB-MOT-LIQ
    private String p86DescLibMotLiq = DefaultValues.stringVal(Len.P86_DESC_LIB_MOT_LIQ);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOT_LIQ;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMotLiqBytes(buf);
    }

    public void setMotLiqFormatted(String data) {
        byte[] buffer = new byte[Len.MOT_LIQ];
        MarshalByte.writeString(buffer, 1, data, Len.MOT_LIQ);
        setMotLiqBytes(buffer, 1);
    }

    public String getMotLiqFormatted() {
        return MarshalByteExt.bufferToStr(getMotLiqBytes());
    }

    public void setMotLiqBytes(byte[] buffer) {
        setMotLiqBytes(buffer, 1);
    }

    public byte[] getMotLiqBytes() {
        byte[] buffer = new byte[Len.MOT_LIQ];
        return getMotLiqBytes(buffer, 1);
    }

    public void setMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        p86IdMotLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_ID_MOT_LIQ, 0);
        position += Len.P86_ID_MOT_LIQ;
        p86IdLiq = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_ID_LIQ, 0);
        position += Len.P86_ID_LIQ;
        p86IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_ID_MOVI_CRZ, 0);
        position += Len.P86_ID_MOVI_CRZ;
        p86IdMoviChiu.setP86IdMoviChiuFromBuffer(buffer, position);
        position += P86IdMoviChiu.Len.P86_ID_MOVI_CHIU;
        p86DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_DT_INI_EFF, 0);
        position += Len.P86_DT_INI_EFF;
        p86DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_DT_END_EFF, 0);
        position += Len.P86_DT_END_EFF;
        p86CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_COD_COMP_ANIA, 0);
        position += Len.P86_COD_COMP_ANIA;
        p86TpLiq = MarshalByte.readString(buffer, position, Len.P86_TP_LIQ);
        position += Len.P86_TP_LIQ;
        p86TpMotLiq = MarshalByte.readString(buffer, position, Len.P86_TP_MOT_LIQ);
        position += Len.P86_TP_MOT_LIQ;
        p86DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P86_DS_RIGA, 0);
        position += Len.P86_DS_RIGA;
        p86DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p86DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P86_DS_VER, 0);
        position += Len.P86_DS_VER;
        p86DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P86_DS_TS_INI_CPTZ, 0);
        position += Len.P86_DS_TS_INI_CPTZ;
        p86DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P86_DS_TS_END_CPTZ, 0);
        position += Len.P86_DS_TS_END_CPTZ;
        p86DsUtente = MarshalByte.readString(buffer, position, Len.P86_DS_UTENTE);
        position += Len.P86_DS_UTENTE;
        p86DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setP86DescLibMotLiqVcharBytes(buffer, position);
    }

    public byte[] getMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p86IdMotLiq, Len.Int.P86_ID_MOT_LIQ, 0);
        position += Len.P86_ID_MOT_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, p86IdLiq, Len.Int.P86_ID_LIQ, 0);
        position += Len.P86_ID_LIQ;
        MarshalByte.writeIntAsPacked(buffer, position, p86IdMoviCrz, Len.Int.P86_ID_MOVI_CRZ, 0);
        position += Len.P86_ID_MOVI_CRZ;
        p86IdMoviChiu.getP86IdMoviChiuAsBuffer(buffer, position);
        position += P86IdMoviChiu.Len.P86_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, p86DtIniEff, Len.Int.P86_DT_INI_EFF, 0);
        position += Len.P86_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p86DtEndEff, Len.Int.P86_DT_END_EFF, 0);
        position += Len.P86_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p86CodCompAnia, Len.Int.P86_COD_COMP_ANIA, 0);
        position += Len.P86_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, p86TpLiq, Len.P86_TP_LIQ);
        position += Len.P86_TP_LIQ;
        MarshalByte.writeString(buffer, position, p86TpMotLiq, Len.P86_TP_MOT_LIQ);
        position += Len.P86_TP_MOT_LIQ;
        MarshalByte.writeLongAsPacked(buffer, position, p86DsRiga, Len.Int.P86_DS_RIGA, 0);
        position += Len.P86_DS_RIGA;
        MarshalByte.writeChar(buffer, position, p86DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p86DsVer, Len.Int.P86_DS_VER, 0);
        position += Len.P86_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p86DsTsIniCptz, Len.Int.P86_DS_TS_INI_CPTZ, 0);
        position += Len.P86_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, p86DsTsEndCptz, Len.Int.P86_DS_TS_END_CPTZ, 0);
        position += Len.P86_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, p86DsUtente, Len.P86_DS_UTENTE);
        position += Len.P86_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p86DsStatoElab);
        position += Types.CHAR_SIZE;
        getP86DescLibMotLiqVcharBytes(buffer, position);
        return buffer;
    }

    public void setP86IdMotLiq(int p86IdMotLiq) {
        this.p86IdMotLiq = p86IdMotLiq;
    }

    public int getP86IdMotLiq() {
        return this.p86IdMotLiq;
    }

    public void setP86IdLiq(int p86IdLiq) {
        this.p86IdLiq = p86IdLiq;
    }

    public int getP86IdLiq() {
        return this.p86IdLiq;
    }

    public void setP86IdMoviCrz(int p86IdMoviCrz) {
        this.p86IdMoviCrz = p86IdMoviCrz;
    }

    public int getP86IdMoviCrz() {
        return this.p86IdMoviCrz;
    }

    public void setP86DtIniEff(int p86DtIniEff) {
        this.p86DtIniEff = p86DtIniEff;
    }

    public int getP86DtIniEff() {
        return this.p86DtIniEff;
    }

    public void setP86DtEndEff(int p86DtEndEff) {
        this.p86DtEndEff = p86DtEndEff;
    }

    public int getP86DtEndEff() {
        return this.p86DtEndEff;
    }

    public void setP86CodCompAnia(int p86CodCompAnia) {
        this.p86CodCompAnia = p86CodCompAnia;
    }

    public int getP86CodCompAnia() {
        return this.p86CodCompAnia;
    }

    public void setP86TpLiq(String p86TpLiq) {
        this.p86TpLiq = Functions.subString(p86TpLiq, Len.P86_TP_LIQ);
    }

    public String getP86TpLiq() {
        return this.p86TpLiq;
    }

    public void setP86TpMotLiq(String p86TpMotLiq) {
        this.p86TpMotLiq = Functions.subString(p86TpMotLiq, Len.P86_TP_MOT_LIQ);
    }

    public String getP86TpMotLiq() {
        return this.p86TpMotLiq;
    }

    public void setP86DsRiga(long p86DsRiga) {
        this.p86DsRiga = p86DsRiga;
    }

    public long getP86DsRiga() {
        return this.p86DsRiga;
    }

    public void setP86DsOperSql(char p86DsOperSql) {
        this.p86DsOperSql = p86DsOperSql;
    }

    public void setP86DsOperSqlFormatted(String p86DsOperSql) {
        setP86DsOperSql(Functions.charAt(p86DsOperSql, Types.CHAR_SIZE));
    }

    public char getP86DsOperSql() {
        return this.p86DsOperSql;
    }

    public void setP86DsVer(int p86DsVer) {
        this.p86DsVer = p86DsVer;
    }

    public int getP86DsVer() {
        return this.p86DsVer;
    }

    public void setP86DsTsIniCptz(long p86DsTsIniCptz) {
        this.p86DsTsIniCptz = p86DsTsIniCptz;
    }

    public long getP86DsTsIniCptz() {
        return this.p86DsTsIniCptz;
    }

    public void setP86DsTsEndCptz(long p86DsTsEndCptz) {
        this.p86DsTsEndCptz = p86DsTsEndCptz;
    }

    public long getP86DsTsEndCptz() {
        return this.p86DsTsEndCptz;
    }

    public void setP86DsUtente(String p86DsUtente) {
        this.p86DsUtente = Functions.subString(p86DsUtente, Len.P86_DS_UTENTE);
    }

    public String getP86DsUtente() {
        return this.p86DsUtente;
    }

    public void setP86DsStatoElab(char p86DsStatoElab) {
        this.p86DsStatoElab = p86DsStatoElab;
    }

    public void setP86DsStatoElabFormatted(String p86DsStatoElab) {
        setP86DsStatoElab(Functions.charAt(p86DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP86DsStatoElab() {
        return this.p86DsStatoElab;
    }

    public void setP86DescLibMotLiqVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P86_DESC_LIB_MOT_LIQ_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P86_DESC_LIB_MOT_LIQ_VCHAR);
        setP86DescLibMotLiqVcharBytes(buffer, 1);
    }

    public String getP86DescLibMotLiqVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP86DescLibMotLiqVcharBytes());
    }

    /**Original name: P86-DESC-LIB-MOT-LIQ-VCHAR<br>*/
    public byte[] getP86DescLibMotLiqVcharBytes() {
        byte[] buffer = new byte[Len.P86_DESC_LIB_MOT_LIQ_VCHAR];
        return getP86DescLibMotLiqVcharBytes(buffer, 1);
    }

    public void setP86DescLibMotLiqVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p86DescLibMotLiqLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p86DescLibMotLiq = MarshalByte.readString(buffer, position, Len.P86_DESC_LIB_MOT_LIQ);
    }

    public byte[] getP86DescLibMotLiqVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p86DescLibMotLiqLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p86DescLibMotLiq, Len.P86_DESC_LIB_MOT_LIQ);
        return buffer;
    }

    public void setP86DescLibMotLiqLen(short p86DescLibMotLiqLen) {
        this.p86DescLibMotLiqLen = p86DescLibMotLiqLen;
    }

    public short getP86DescLibMotLiqLen() {
        return this.p86DescLibMotLiqLen;
    }

    public void setP86DescLibMotLiq(String p86DescLibMotLiq) {
        this.p86DescLibMotLiq = Functions.subString(p86DescLibMotLiq, Len.P86_DESC_LIB_MOT_LIQ);
    }

    public String getP86DescLibMotLiq() {
        return this.p86DescLibMotLiq;
    }

    public P86IdMoviChiu getP86IdMoviChiu() {
        return p86IdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getMotLiqBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P86_ID_MOT_LIQ = 5;
        public static final int P86_ID_LIQ = 5;
        public static final int P86_ID_MOVI_CRZ = 5;
        public static final int P86_DT_INI_EFF = 5;
        public static final int P86_DT_END_EFF = 5;
        public static final int P86_COD_COMP_ANIA = 3;
        public static final int P86_TP_LIQ = 2;
        public static final int P86_TP_MOT_LIQ = 2;
        public static final int P86_DS_RIGA = 6;
        public static final int P86_DS_OPER_SQL = 1;
        public static final int P86_DS_VER = 5;
        public static final int P86_DS_TS_INI_CPTZ = 10;
        public static final int P86_DS_TS_END_CPTZ = 10;
        public static final int P86_DS_UTENTE = 20;
        public static final int P86_DS_STATO_ELAB = 1;
        public static final int P86_DESC_LIB_MOT_LIQ_LEN = 2;
        public static final int P86_DESC_LIB_MOT_LIQ = 250;
        public static final int P86_DESC_LIB_MOT_LIQ_VCHAR = P86_DESC_LIB_MOT_LIQ_LEN + P86_DESC_LIB_MOT_LIQ;
        public static final int MOT_LIQ = P86_ID_MOT_LIQ + P86_ID_LIQ + P86_ID_MOVI_CRZ + P86IdMoviChiu.Len.P86_ID_MOVI_CHIU + P86_DT_INI_EFF + P86_DT_END_EFF + P86_COD_COMP_ANIA + P86_TP_LIQ + P86_TP_MOT_LIQ + P86_DS_RIGA + P86_DS_OPER_SQL + P86_DS_VER + P86_DS_TS_INI_CPTZ + P86_DS_TS_END_CPTZ + P86_DS_UTENTE + P86_DS_STATO_ELAB + P86_DESC_LIB_MOT_LIQ_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P86_ID_MOT_LIQ = 9;
            public static final int P86_ID_LIQ = 9;
            public static final int P86_ID_MOVI_CRZ = 9;
            public static final int P86_DT_INI_EFF = 8;
            public static final int P86_DT_END_EFF = 8;
            public static final int P86_COD_COMP_ANIA = 5;
            public static final int P86_DS_RIGA = 10;
            public static final int P86_DS_VER = 9;
            public static final int P86_DS_TS_INI_CPTZ = 18;
            public static final int P86_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
