package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-AA-STAB<br>
 * Variable: GRZ-AA-STAB from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzAaStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzAaStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_AA_STAB;
    }

    public void setGrzAaStab(int grzAaStab) {
        writeIntAsPacked(Pos.GRZ_AA_STAB, grzAaStab, Len.Int.GRZ_AA_STAB);
    }

    public void setGrzAaStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_AA_STAB, Pos.GRZ_AA_STAB);
    }

    /**Original name: GRZ-AA-STAB<br>*/
    public int getGrzAaStab() {
        return readPackedAsInt(Pos.GRZ_AA_STAB, Len.Int.GRZ_AA_STAB);
    }

    public byte[] getGrzAaStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_AA_STAB, Pos.GRZ_AA_STAB);
        return buffer;
    }

    public void setGrzAaStabNull(String grzAaStabNull) {
        writeString(Pos.GRZ_AA_STAB_NULL, grzAaStabNull, Len.GRZ_AA_STAB_NULL);
    }

    /**Original name: GRZ-AA-STAB-NULL<br>*/
    public String getGrzAaStabNull() {
        return readString(Pos.GRZ_AA_STAB_NULL, Len.GRZ_AA_STAB_NULL);
    }

    public String getGrzAaStabNullFormatted() {
        return Functions.padBlanks(getGrzAaStabNull(), Len.GRZ_AA_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_AA_STAB = 1;
        public static final int GRZ_AA_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_AA_STAB = 3;
        public static final int GRZ_AA_STAB_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_AA_STAB = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
