package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-NUM-RAT-PAG-PRE<br>
 * Variable: WPMO-NUM-RAT-PAG-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoNumRatPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoNumRatPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_NUM_RAT_PAG_PRE;
    }

    public void setWpmoNumRatPagPre(int wpmoNumRatPagPre) {
        writeIntAsPacked(Pos.WPMO_NUM_RAT_PAG_PRE, wpmoNumRatPagPre, Len.Int.WPMO_NUM_RAT_PAG_PRE);
    }

    public void setWpmoNumRatPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_NUM_RAT_PAG_PRE, Pos.WPMO_NUM_RAT_PAG_PRE);
    }

    /**Original name: WPMO-NUM-RAT-PAG-PRE<br>*/
    public int getWpmoNumRatPagPre() {
        return readPackedAsInt(Pos.WPMO_NUM_RAT_PAG_PRE, Len.Int.WPMO_NUM_RAT_PAG_PRE);
    }

    public byte[] getWpmoNumRatPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_NUM_RAT_PAG_PRE, Pos.WPMO_NUM_RAT_PAG_PRE);
        return buffer;
    }

    public void initWpmoNumRatPagPreSpaces() {
        fill(Pos.WPMO_NUM_RAT_PAG_PRE, Len.WPMO_NUM_RAT_PAG_PRE, Types.SPACE_CHAR);
    }

    public void setWpmoNumRatPagPreNull(String wpmoNumRatPagPreNull) {
        writeString(Pos.WPMO_NUM_RAT_PAG_PRE_NULL, wpmoNumRatPagPreNull, Len.WPMO_NUM_RAT_PAG_PRE_NULL);
    }

    /**Original name: WPMO-NUM-RAT-PAG-PRE-NULL<br>*/
    public String getWpmoNumRatPagPreNull() {
        return readString(Pos.WPMO_NUM_RAT_PAG_PRE_NULL, Len.WPMO_NUM_RAT_PAG_PRE_NULL);
    }

    public String getWpmoNumRatPagPreNullFormatted() {
        return Functions.padBlanks(getWpmoNumRatPagPreNull(), Len.WPMO_NUM_RAT_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_NUM_RAT_PAG_PRE = 1;
        public static final int WPMO_NUM_RAT_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_NUM_RAT_PAG_PRE = 3;
        public static final int WPMO_NUM_RAT_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_NUM_RAT_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
