package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-SOST-K3-ANTI<br>
 * Variable: DFA-IMPST-SOST-K3-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstSostK3Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstSostK3Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_SOST_K3_ANTI;
    }

    public void setDfaImpstSostK3Anti(AfDecimal dfaImpstSostK3Anti) {
        writeDecimalAsPacked(Pos.DFA_IMPST_SOST_K3_ANTI, dfaImpstSostK3Anti.copy());
    }

    public void setDfaImpstSostK3AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_SOST_K3_ANTI, Pos.DFA_IMPST_SOST_K3_ANTI);
    }

    /**Original name: DFA-IMPST-SOST-K3-ANTI<br>*/
    public AfDecimal getDfaImpstSostK3Anti() {
        return readPackedAsDecimal(Pos.DFA_IMPST_SOST_K3_ANTI, Len.Int.DFA_IMPST_SOST_K3_ANTI, Len.Fract.DFA_IMPST_SOST_K3_ANTI);
    }

    public byte[] getDfaImpstSostK3AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_SOST_K3_ANTI, Pos.DFA_IMPST_SOST_K3_ANTI);
        return buffer;
    }

    public void setDfaImpstSostK3AntiNull(String dfaImpstSostK3AntiNull) {
        writeString(Pos.DFA_IMPST_SOST_K3_ANTI_NULL, dfaImpstSostK3AntiNull, Len.DFA_IMPST_SOST_K3_ANTI_NULL);
    }

    /**Original name: DFA-IMPST-SOST-K3-ANTI-NULL<br>*/
    public String getDfaImpstSostK3AntiNull() {
        return readString(Pos.DFA_IMPST_SOST_K3_ANTI_NULL, Len.DFA_IMPST_SOST_K3_ANTI_NULL);
    }

    public String getDfaImpstSostK3AntiNullFormatted() {
        return Functions.padBlanks(getDfaImpstSostK3AntiNull(), Len.DFA_IMPST_SOST_K3_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K3_ANTI = 1;
        public static final int DFA_IMPST_SOST_K3_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K3_ANTI = 8;
        public static final int DFA_IMPST_SOST_K3_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K3_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K3_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
