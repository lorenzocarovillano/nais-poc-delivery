package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-OVER-COMM<br>
 * Variable: B03-OVER-COMM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03OverComm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03OverComm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_OVER_COMM;
    }

    public void setB03OverComm(AfDecimal b03OverComm) {
        writeDecimalAsPacked(Pos.B03_OVER_COMM, b03OverComm.copy());
    }

    public void setB03OverCommFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_OVER_COMM, Pos.B03_OVER_COMM);
    }

    /**Original name: B03-OVER-COMM<br>*/
    public AfDecimal getB03OverComm() {
        return readPackedAsDecimal(Pos.B03_OVER_COMM, Len.Int.B03_OVER_COMM, Len.Fract.B03_OVER_COMM);
    }

    public byte[] getB03OverCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_OVER_COMM, Pos.B03_OVER_COMM);
        return buffer;
    }

    public void setB03OverCommNull(String b03OverCommNull) {
        writeString(Pos.B03_OVER_COMM_NULL, b03OverCommNull, Len.B03_OVER_COMM_NULL);
    }

    /**Original name: B03-OVER-COMM-NULL<br>*/
    public String getB03OverCommNull() {
        return readString(Pos.B03_OVER_COMM_NULL, Len.B03_OVER_COMM_NULL);
    }

    public String getB03OverCommNullFormatted() {
        return Functions.padBlanks(getB03OverCommNull(), Len.B03_OVER_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_OVER_COMM = 1;
        public static final int B03_OVER_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_OVER_COMM = 8;
        public static final int B03_OVER_COMM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_OVER_COMM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_OVER_COMM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
