package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-NUM-GG-RITARDO-PAG<br>
 * Variable: DTC-NUM-GG-RITARDO-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcNumGgRitardoPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcNumGgRitardoPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_NUM_GG_RITARDO_PAG;
    }

    public void setDtcNumGgRitardoPag(int dtcNumGgRitardoPag) {
        writeIntAsPacked(Pos.DTC_NUM_GG_RITARDO_PAG, dtcNumGgRitardoPag, Len.Int.DTC_NUM_GG_RITARDO_PAG);
    }

    public void setDtcNumGgRitardoPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_NUM_GG_RITARDO_PAG, Pos.DTC_NUM_GG_RITARDO_PAG);
    }

    /**Original name: DTC-NUM-GG-RITARDO-PAG<br>*/
    public int getDtcNumGgRitardoPag() {
        return readPackedAsInt(Pos.DTC_NUM_GG_RITARDO_PAG, Len.Int.DTC_NUM_GG_RITARDO_PAG);
    }

    public byte[] getDtcNumGgRitardoPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_NUM_GG_RITARDO_PAG, Pos.DTC_NUM_GG_RITARDO_PAG);
        return buffer;
    }

    public void setDtcNumGgRitardoPagNull(String dtcNumGgRitardoPagNull) {
        writeString(Pos.DTC_NUM_GG_RITARDO_PAG_NULL, dtcNumGgRitardoPagNull, Len.DTC_NUM_GG_RITARDO_PAG_NULL);
    }

    /**Original name: DTC-NUM-GG-RITARDO-PAG-NULL<br>*/
    public String getDtcNumGgRitardoPagNull() {
        return readString(Pos.DTC_NUM_GG_RITARDO_PAG_NULL, Len.DTC_NUM_GG_RITARDO_PAG_NULL);
    }

    public String getDtcNumGgRitardoPagNullFormatted() {
        return Functions.padBlanks(getDtcNumGgRitardoPagNull(), Len.DTC_NUM_GG_RITARDO_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_NUM_GG_RITARDO_PAG = 1;
        public static final int DTC_NUM_GG_RITARDO_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_NUM_GG_RITARDO_PAG = 3;
        public static final int DTC_NUM_GG_RITARDO_PAG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_NUM_GG_RITARDO_PAG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
