package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-COMMIS-INTER<br>
 * Variable: DTR-COMMIS-INTER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_COMMIS_INTER;
    }

    public void setDtrCommisInter(AfDecimal dtrCommisInter) {
        writeDecimalAsPacked(Pos.DTR_COMMIS_INTER, dtrCommisInter.copy());
    }

    public void setDtrCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_COMMIS_INTER, Pos.DTR_COMMIS_INTER);
    }

    /**Original name: DTR-COMMIS-INTER<br>*/
    public AfDecimal getDtrCommisInter() {
        return readPackedAsDecimal(Pos.DTR_COMMIS_INTER, Len.Int.DTR_COMMIS_INTER, Len.Fract.DTR_COMMIS_INTER);
    }

    public byte[] getDtrCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_COMMIS_INTER, Pos.DTR_COMMIS_INTER);
        return buffer;
    }

    public void setDtrCommisInterNull(String dtrCommisInterNull) {
        writeString(Pos.DTR_COMMIS_INTER_NULL, dtrCommisInterNull, Len.DTR_COMMIS_INTER_NULL);
    }

    /**Original name: DTR-COMMIS-INTER-NULL<br>*/
    public String getDtrCommisInterNull() {
        return readString(Pos.DTR_COMMIS_INTER_NULL, Len.DTR_COMMIS_INTER_NULL);
    }

    public String getDtrCommisInterNullFormatted() {
        return Functions.padBlanks(getDtrCommisInterNull(), Len.DTR_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_COMMIS_INTER = 1;
        public static final int DTR_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_COMMIS_INTER = 8;
        public static final int DTR_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
