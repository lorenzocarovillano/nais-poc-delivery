package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-ACQ-EXP<br>
 * Variable: DTC-ACQ-EXP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_ACQ_EXP;
    }

    public void setDtcAcqExp(AfDecimal dtcAcqExp) {
        writeDecimalAsPacked(Pos.DTC_ACQ_EXP, dtcAcqExp.copy());
    }

    public void setDtcAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_ACQ_EXP, Pos.DTC_ACQ_EXP);
    }

    /**Original name: DTC-ACQ-EXP<br>*/
    public AfDecimal getDtcAcqExp() {
        return readPackedAsDecimal(Pos.DTC_ACQ_EXP, Len.Int.DTC_ACQ_EXP, Len.Fract.DTC_ACQ_EXP);
    }

    public byte[] getDtcAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_ACQ_EXP, Pos.DTC_ACQ_EXP);
        return buffer;
    }

    public void setDtcAcqExpNull(String dtcAcqExpNull) {
        writeString(Pos.DTC_ACQ_EXP_NULL, dtcAcqExpNull, Len.DTC_ACQ_EXP_NULL);
    }

    /**Original name: DTC-ACQ-EXP-NULL<br>*/
    public String getDtcAcqExpNull() {
        return readString(Pos.DTC_ACQ_EXP_NULL, Len.DTC_ACQ_EXP_NULL);
    }

    public String getDtcAcqExpNullFormatted() {
        return Functions.padBlanks(getDtcAcqExpNull(), Len.DTC_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_ACQ_EXP = 1;
        public static final int DTC_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_ACQ_EXP = 8;
        public static final int DTC_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
