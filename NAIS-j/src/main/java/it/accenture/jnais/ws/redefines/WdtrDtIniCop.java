package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-DT-INI-COP<br>
 * Variable: WDTR-DT-INI-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_DT_INI_COP;
    }

    public void setWdtrDtIniCop(int wdtrDtIniCop) {
        writeIntAsPacked(Pos.WDTR_DT_INI_COP, wdtrDtIniCop, Len.Int.WDTR_DT_INI_COP);
    }

    /**Original name: WDTR-DT-INI-COP<br>*/
    public int getWdtrDtIniCop() {
        return readPackedAsInt(Pos.WDTR_DT_INI_COP, Len.Int.WDTR_DT_INI_COP);
    }

    public void setWdtrDtIniCopNull(String wdtrDtIniCopNull) {
        writeString(Pos.WDTR_DT_INI_COP_NULL, wdtrDtIniCopNull, Len.WDTR_DT_INI_COP_NULL);
    }

    /**Original name: WDTR-DT-INI-COP-NULL<br>*/
    public String getWdtrDtIniCopNull() {
        return readString(Pos.WDTR_DT_INI_COP_NULL, Len.WDTR_DT_INI_COP_NULL);
    }

    public String getWdtrDtIniCopNullFormatted() {
        return Functions.padBlanks(getWdtrDtIniCopNull(), Len.WDTR_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_DT_INI_COP = 1;
        public static final int WDTR_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_DT_INI_COP = 5;
        public static final int WDTR_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
