package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-ANZ-CNBTVA-CARAZI<br>
 * Variable: WDFA-ANZ-CNBTVA-CARAZI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaAnzCnbtvaCarazi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaAnzCnbtvaCarazi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ANZ_CNBTVA_CARAZI;
    }

    public void setWdfaAnzCnbtvaCarazi(short wdfaAnzCnbtvaCarazi) {
        writeShortAsPacked(Pos.WDFA_ANZ_CNBTVA_CARAZI, wdfaAnzCnbtvaCarazi, Len.Int.WDFA_ANZ_CNBTVA_CARAZI);
    }

    public void setWdfaAnzCnbtvaCaraziFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ANZ_CNBTVA_CARAZI, Pos.WDFA_ANZ_CNBTVA_CARAZI);
    }

    /**Original name: WDFA-ANZ-CNBTVA-CARAZI<br>*/
    public short getWdfaAnzCnbtvaCarazi() {
        return readPackedAsShort(Pos.WDFA_ANZ_CNBTVA_CARAZI, Len.Int.WDFA_ANZ_CNBTVA_CARAZI);
    }

    public byte[] getWdfaAnzCnbtvaCaraziAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ANZ_CNBTVA_CARAZI, Pos.WDFA_ANZ_CNBTVA_CARAZI);
        return buffer;
    }

    public void setWdfaAnzCnbtvaCaraziNull(String wdfaAnzCnbtvaCaraziNull) {
        writeString(Pos.WDFA_ANZ_CNBTVA_CARAZI_NULL, wdfaAnzCnbtvaCaraziNull, Len.WDFA_ANZ_CNBTVA_CARAZI_NULL);
    }

    /**Original name: WDFA-ANZ-CNBTVA-CARAZI-NULL<br>*/
    public String getWdfaAnzCnbtvaCaraziNull() {
        return readString(Pos.WDFA_ANZ_CNBTVA_CARAZI_NULL, Len.WDFA_ANZ_CNBTVA_CARAZI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ANZ_CNBTVA_CARAZI = 1;
        public static final int WDFA_ANZ_CNBTVA_CARAZI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ANZ_CNBTVA_CARAZI = 3;
        public static final int WDFA_ANZ_CNBTVA_CARAZI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ANZ_CNBTVA_CARAZI = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
