package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-VOLO-TGA<br>
 * Variable: WPAG-IMP-VOLO-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpVoloTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpVoloTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_VOLO_TGA;
    }

    public void setWpagImpVoloTga(AfDecimal wpagImpVoloTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_VOLO_TGA, wpagImpVoloTga.copy());
    }

    public void setWpagImpVoloTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_VOLO_TGA, Pos.WPAG_IMP_VOLO_TGA);
    }

    /**Original name: WPAG-IMP-VOLO-TGA<br>*/
    public AfDecimal getWpagImpVoloTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_VOLO_TGA, Len.Int.WPAG_IMP_VOLO_TGA, Len.Fract.WPAG_IMP_VOLO_TGA);
    }

    public byte[] getWpagImpVoloTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_VOLO_TGA, Pos.WPAG_IMP_VOLO_TGA);
        return buffer;
    }

    public void initWpagImpVoloTgaSpaces() {
        fill(Pos.WPAG_IMP_VOLO_TGA, Len.WPAG_IMP_VOLO_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpVoloTgaNull(String wpagImpVoloTgaNull) {
        writeString(Pos.WPAG_IMP_VOLO_TGA_NULL, wpagImpVoloTgaNull, Len.WPAG_IMP_VOLO_TGA_NULL);
    }

    /**Original name: WPAG-IMP-VOLO-TGA-NULL<br>*/
    public String getWpagImpVoloTgaNull() {
        return readString(Pos.WPAG_IMP_VOLO_TGA_NULL, Len.WPAG_IMP_VOLO_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_VOLO_TGA = 1;
        public static final int WPAG_IMP_VOLO_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_VOLO_TGA = 8;
        public static final int WPAG_IMP_VOLO_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_VOLO_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_VOLO_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
