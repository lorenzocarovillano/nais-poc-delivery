package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COEFF-RIS-2-T<br>
 * Variable: B03-COEFF-RIS-2-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CoeffRis2T extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CoeffRis2T() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COEFF_RIS2_T;
    }

    public void setB03CoeffRis2T(AfDecimal b03CoeffRis2T) {
        writeDecimalAsPacked(Pos.B03_COEFF_RIS2_T, b03CoeffRis2T.copy());
    }

    public void setB03CoeffRis2TFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COEFF_RIS2_T, Pos.B03_COEFF_RIS2_T);
    }

    /**Original name: B03-COEFF-RIS-2-T<br>*/
    public AfDecimal getB03CoeffRis2T() {
        return readPackedAsDecimal(Pos.B03_COEFF_RIS2_T, Len.Int.B03_COEFF_RIS2_T, Len.Fract.B03_COEFF_RIS2_T);
    }

    public byte[] getB03CoeffRis2TAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COEFF_RIS2_T, Pos.B03_COEFF_RIS2_T);
        return buffer;
    }

    public void setB03CoeffRis2TNull(String b03CoeffRis2TNull) {
        writeString(Pos.B03_COEFF_RIS2_T_NULL, b03CoeffRis2TNull, Len.B03_COEFF_RIS2_T_NULL);
    }

    /**Original name: B03-COEFF-RIS-2-T-NULL<br>*/
    public String getB03CoeffRis2TNull() {
        return readString(Pos.B03_COEFF_RIS2_T_NULL, Len.B03_COEFF_RIS2_T_NULL);
    }

    public String getB03CoeffRis2TNullFormatted() {
        return Functions.padBlanks(getB03CoeffRis2TNull(), Len.B03_COEFF_RIS2_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COEFF_RIS2_T = 1;
        public static final int B03_COEFF_RIS2_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COEFF_RIS2_T = 8;
        public static final int B03_COEFF_RIS2_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_COEFF_RIS2_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COEFF_RIS2_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
