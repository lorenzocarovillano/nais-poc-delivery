package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-VIS-END2000<br>
 * Variable: WTGA-VIS-END2000 from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaVisEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaVisEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_VIS_END2000;
    }

    public void setWtgaVisEnd2000(AfDecimal wtgaVisEnd2000) {
        writeDecimalAsPacked(Pos.WTGA_VIS_END2000, wtgaVisEnd2000.copy());
    }

    public void setWtgaVisEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_VIS_END2000, Pos.WTGA_VIS_END2000);
    }

    /**Original name: WTGA-VIS-END2000<br>*/
    public AfDecimal getWtgaVisEnd2000() {
        return readPackedAsDecimal(Pos.WTGA_VIS_END2000, Len.Int.WTGA_VIS_END2000, Len.Fract.WTGA_VIS_END2000);
    }

    public byte[] getWtgaVisEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_VIS_END2000, Pos.WTGA_VIS_END2000);
        return buffer;
    }

    public void initWtgaVisEnd2000Spaces() {
        fill(Pos.WTGA_VIS_END2000, Len.WTGA_VIS_END2000, Types.SPACE_CHAR);
    }

    public void setWtgaVisEnd2000Null(String wtgaVisEnd2000Null) {
        writeString(Pos.WTGA_VIS_END2000_NULL, wtgaVisEnd2000Null, Len.WTGA_VIS_END2000_NULL);
    }

    /**Original name: WTGA-VIS-END2000-NULL<br>*/
    public String getWtgaVisEnd2000Null() {
        return readString(Pos.WTGA_VIS_END2000_NULL, Len.WTGA_VIS_END2000_NULL);
    }

    public String getWtgaVisEnd2000NullFormatted() {
        return Functions.padBlanks(getWtgaVisEnd2000Null(), Len.WTGA_VIS_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_VIS_END2000 = 1;
        public static final int WTGA_VIS_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_VIS_END2000 = 8;
        public static final int WTGA_VIS_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_VIS_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_VIS_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
