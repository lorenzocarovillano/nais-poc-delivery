package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-SCON<br>
 * Variable: L3421-IMP-SCON from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_SCON;
    }

    public void setL3421ImpSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_SCON, Pos.L3421_IMP_SCON);
    }

    /**Original name: L3421-IMP-SCON<br>*/
    public AfDecimal getL3421ImpScon() {
        return readPackedAsDecimal(Pos.L3421_IMP_SCON, Len.Int.L3421_IMP_SCON, Len.Fract.L3421_IMP_SCON);
    }

    public byte[] getL3421ImpSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_SCON, Pos.L3421_IMP_SCON);
        return buffer;
    }

    /**Original name: L3421-IMP-SCON-NULL<br>*/
    public String getL3421ImpSconNull() {
        return readString(Pos.L3421_IMP_SCON_NULL, Len.L3421_IMP_SCON_NULL);
    }

    public String getL3421ImpSconNullFormatted() {
        return Functions.padBlanks(getL3421ImpSconNull(), Len.L3421_IMP_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SCON = 1;
        public static final int L3421_IMP_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SCON = 8;
        public static final int L3421_IMP_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
