package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.ws.enums.SwPgmTrovato;
import it.accenture.jnais.ws.redefines.Iabv0901TabCallPgm;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDSS0010<br>
 * Generated as a class for rule WS.<br>*/
public class Idss0010Data {

    //==== PROPERTIES ====
    //Original name: WS-NOME-PROG
    private String wsNomeProg = DefaultValues.stringVal(Len.WS_NOME_PROG);
    /**Original name: IABV0901-TAB-CALL-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY DECODIFICA TABELLA/NOME PROGRAMMA
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *                        A T T E N Z I O N E
	 *     SE DOVETE AGGIUNGERE UNA NUOVA TABELLA RISPETTATE L'ORDINE
	 *     DELLA CHIAVE NOME-TABELLA(20 BYTE) + NOME PROGRAMMA (8 BYTE)
	 *     ESESMPIO SE DOVETE INSERIRE:
	 *     - LA TABELLA "PROVA" + PROGRAMMA "IDBSPRO0"
	 *      BISOGNEREBBE INSERIRE LA RIGA TRA LE TABELLE
	 *     -  PROPOSTE-MEDASS E PROV-DI-GAR
	 * *****************************************************************</pre>*/
    private Iabv0901TabCallPgm iabv0901TabCallPgm = new Iabv0901TabCallPgm();
    //Original name: IABV0901-INDEX
    private int iabv0901Index = 1;
    /**Original name: SW-PGM-TROVATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     SWITCH
	 * ----------------------------------------------------------------*</pre>*/
    private SwPgmTrovato swPgmTrovato = new SwPgmTrovato();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: IDSV0003
    private Idsv0003 idsv0003 = new Idsv0003();

    //==== METHODS ====
    public void setWsNomeProg(String wsNomeProg) {
        this.wsNomeProg = Functions.subString(wsNomeProg, Len.WS_NOME_PROG);
    }

    public String getWsNomeProg() {
        return this.wsNomeProg;
    }

    public void setIabv0901Index(int iabv0901Index) {
        this.iabv0901Index = iabv0901Index;
    }

    public int getIabv0901Index() {
        return this.iabv0901Index;
    }

    public Iabv0901TabCallPgm getIabv0901TabCallPgm() {
        return iabv0901TabCallPgm;
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public SwPgmTrovato getSwPgmTrovato() {
        return swPgmTrovato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_NOME_PROG = 8;
        public static final int WS_TS_SYSTEM_DB = 26;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
