package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.OdeCodLivAutApprt;
import it.accenture.jnais.ws.redefines.OdeIdMoviChiu;

/**Original name: OGG-DEROGA<br>
 * Variable: OGG-DEROGA from copybook IDBVODE1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class OggDerogaIdbsode0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: ODE-ID-OGG-DEROGA
    private int odeIdOggDeroga = DefaultValues.INT_VAL;
    //Original name: ODE-ID-MOVI-CRZ
    private int odeIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: ODE-ID-MOVI-CHIU
    private OdeIdMoviChiu odeIdMoviChiu = new OdeIdMoviChiu();
    //Original name: ODE-DT-INI-EFF
    private int odeDtIniEff = DefaultValues.INT_VAL;
    //Original name: ODE-DT-END-EFF
    private int odeDtEndEff = DefaultValues.INT_VAL;
    //Original name: ODE-COD-COMP-ANIA
    private int odeCodCompAnia = DefaultValues.INT_VAL;
    //Original name: ODE-ID-OGG
    private int odeIdOgg = DefaultValues.INT_VAL;
    //Original name: ODE-TP-OGG
    private String odeTpOgg = DefaultValues.stringVal(Len.ODE_TP_OGG);
    //Original name: ODE-IB-OGG
    private String odeIbOgg = DefaultValues.stringVal(Len.ODE_IB_OGG);
    //Original name: ODE-TP-DEROGA
    private String odeTpDeroga = DefaultValues.stringVal(Len.ODE_TP_DEROGA);
    //Original name: ODE-COD-GR-AUT-APPRT
    private long odeCodGrAutApprt = DefaultValues.LONG_VAL;
    //Original name: ODE-COD-LIV-AUT-APPRT
    private OdeCodLivAutApprt odeCodLivAutApprt = new OdeCodLivAutApprt();
    //Original name: ODE-COD-GR-AUT-SUP
    private long odeCodGrAutSup = DefaultValues.LONG_VAL;
    //Original name: ODE-COD-LIV-AUT-SUP
    private int odeCodLivAutSup = DefaultValues.INT_VAL;
    //Original name: ODE-DS-RIGA
    private long odeDsRiga = DefaultValues.LONG_VAL;
    //Original name: ODE-DS-OPER-SQL
    private char odeDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: ODE-DS-VER
    private int odeDsVer = DefaultValues.INT_VAL;
    //Original name: ODE-DS-TS-INI-CPTZ
    private long odeDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: ODE-DS-TS-END-CPTZ
    private long odeDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: ODE-DS-UTENTE
    private String odeDsUtente = DefaultValues.stringVal(Len.ODE_DS_UTENTE);
    //Original name: ODE-DS-STATO-ELAB
    private char odeDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OGG_DEROGA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setOggDerogaBytes(buf);
    }

    public void setOggDerogaFormatted(String data) {
        byte[] buffer = new byte[Len.OGG_DEROGA];
        MarshalByte.writeString(buffer, 1, data, Len.OGG_DEROGA);
        setOggDerogaBytes(buffer, 1);
    }

    public String getOggDerogaFormatted() {
        return MarshalByteExt.bufferToStr(getOggDerogaBytes());
    }

    public void setOggDerogaBytes(byte[] buffer) {
        setOggDerogaBytes(buffer, 1);
    }

    public byte[] getOggDerogaBytes() {
        byte[] buffer = new byte[Len.OGG_DEROGA];
        return getOggDerogaBytes(buffer, 1);
    }

    public void setOggDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        odeIdOggDeroga = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_ID_OGG_DEROGA, 0);
        position += Len.ODE_ID_OGG_DEROGA;
        odeIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_ID_MOVI_CRZ, 0);
        position += Len.ODE_ID_MOVI_CRZ;
        odeIdMoviChiu.setOdeIdMoviChiuFromBuffer(buffer, position);
        position += OdeIdMoviChiu.Len.ODE_ID_MOVI_CHIU;
        odeDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_DT_INI_EFF, 0);
        position += Len.ODE_DT_INI_EFF;
        odeDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_DT_END_EFF, 0);
        position += Len.ODE_DT_END_EFF;
        odeCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_COD_COMP_ANIA, 0);
        position += Len.ODE_COD_COMP_ANIA;
        odeIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_ID_OGG, 0);
        position += Len.ODE_ID_OGG;
        odeTpOgg = MarshalByte.readString(buffer, position, Len.ODE_TP_OGG);
        position += Len.ODE_TP_OGG;
        odeIbOgg = MarshalByte.readString(buffer, position, Len.ODE_IB_OGG);
        position += Len.ODE_IB_OGG;
        odeTpDeroga = MarshalByte.readString(buffer, position, Len.ODE_TP_DEROGA);
        position += Len.ODE_TP_DEROGA;
        odeCodGrAutApprt = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ODE_COD_GR_AUT_APPRT, 0);
        position += Len.ODE_COD_GR_AUT_APPRT;
        odeCodLivAutApprt.setOdeCodLivAutApprtFromBuffer(buffer, position);
        position += OdeCodLivAutApprt.Len.ODE_COD_LIV_AUT_APPRT;
        odeCodGrAutSup = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ODE_COD_GR_AUT_SUP, 0);
        position += Len.ODE_COD_GR_AUT_SUP;
        odeCodLivAutSup = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_COD_LIV_AUT_SUP, 0);
        position += Len.ODE_COD_LIV_AUT_SUP;
        odeDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ODE_DS_RIGA, 0);
        position += Len.ODE_DS_RIGA;
        odeDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        odeDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ODE_DS_VER, 0);
        position += Len.ODE_DS_VER;
        odeDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ODE_DS_TS_INI_CPTZ, 0);
        position += Len.ODE_DS_TS_INI_CPTZ;
        odeDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ODE_DS_TS_END_CPTZ, 0);
        position += Len.ODE_DS_TS_END_CPTZ;
        odeDsUtente = MarshalByte.readString(buffer, position, Len.ODE_DS_UTENTE);
        position += Len.ODE_DS_UTENTE;
        odeDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getOggDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, odeIdOggDeroga, Len.Int.ODE_ID_OGG_DEROGA, 0);
        position += Len.ODE_ID_OGG_DEROGA;
        MarshalByte.writeIntAsPacked(buffer, position, odeIdMoviCrz, Len.Int.ODE_ID_MOVI_CRZ, 0);
        position += Len.ODE_ID_MOVI_CRZ;
        odeIdMoviChiu.getOdeIdMoviChiuAsBuffer(buffer, position);
        position += OdeIdMoviChiu.Len.ODE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, odeDtIniEff, Len.Int.ODE_DT_INI_EFF, 0);
        position += Len.ODE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, odeDtEndEff, Len.Int.ODE_DT_END_EFF, 0);
        position += Len.ODE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, odeCodCompAnia, Len.Int.ODE_COD_COMP_ANIA, 0);
        position += Len.ODE_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, odeIdOgg, Len.Int.ODE_ID_OGG, 0);
        position += Len.ODE_ID_OGG;
        MarshalByte.writeString(buffer, position, odeTpOgg, Len.ODE_TP_OGG);
        position += Len.ODE_TP_OGG;
        MarshalByte.writeString(buffer, position, odeIbOgg, Len.ODE_IB_OGG);
        position += Len.ODE_IB_OGG;
        MarshalByte.writeString(buffer, position, odeTpDeroga, Len.ODE_TP_DEROGA);
        position += Len.ODE_TP_DEROGA;
        MarshalByte.writeLongAsPacked(buffer, position, odeCodGrAutApprt, Len.Int.ODE_COD_GR_AUT_APPRT, 0);
        position += Len.ODE_COD_GR_AUT_APPRT;
        odeCodLivAutApprt.getOdeCodLivAutApprtAsBuffer(buffer, position);
        position += OdeCodLivAutApprt.Len.ODE_COD_LIV_AUT_APPRT;
        MarshalByte.writeLongAsPacked(buffer, position, odeCodGrAutSup, Len.Int.ODE_COD_GR_AUT_SUP, 0);
        position += Len.ODE_COD_GR_AUT_SUP;
        MarshalByte.writeIntAsPacked(buffer, position, odeCodLivAutSup, Len.Int.ODE_COD_LIV_AUT_SUP, 0);
        position += Len.ODE_COD_LIV_AUT_SUP;
        MarshalByte.writeLongAsPacked(buffer, position, odeDsRiga, Len.Int.ODE_DS_RIGA, 0);
        position += Len.ODE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, odeDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, odeDsVer, Len.Int.ODE_DS_VER, 0);
        position += Len.ODE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, odeDsTsIniCptz, Len.Int.ODE_DS_TS_INI_CPTZ, 0);
        position += Len.ODE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, odeDsTsEndCptz, Len.Int.ODE_DS_TS_END_CPTZ, 0);
        position += Len.ODE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, odeDsUtente, Len.ODE_DS_UTENTE);
        position += Len.ODE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, odeDsStatoElab);
        return buffer;
    }

    public void setOdeIdOggDeroga(int odeIdOggDeroga) {
        this.odeIdOggDeroga = odeIdOggDeroga;
    }

    public int getOdeIdOggDeroga() {
        return this.odeIdOggDeroga;
    }

    public void setOdeIdMoviCrz(int odeIdMoviCrz) {
        this.odeIdMoviCrz = odeIdMoviCrz;
    }

    public int getOdeIdMoviCrz() {
        return this.odeIdMoviCrz;
    }

    public void setOdeDtIniEff(int odeDtIniEff) {
        this.odeDtIniEff = odeDtIniEff;
    }

    public int getOdeDtIniEff() {
        return this.odeDtIniEff;
    }

    public void setOdeDtEndEff(int odeDtEndEff) {
        this.odeDtEndEff = odeDtEndEff;
    }

    public int getOdeDtEndEff() {
        return this.odeDtEndEff;
    }

    public void setOdeCodCompAnia(int odeCodCompAnia) {
        this.odeCodCompAnia = odeCodCompAnia;
    }

    public int getOdeCodCompAnia() {
        return this.odeCodCompAnia;
    }

    public void setOdeIdOgg(int odeIdOgg) {
        this.odeIdOgg = odeIdOgg;
    }

    public int getOdeIdOgg() {
        return this.odeIdOgg;
    }

    public void setOdeTpOgg(String odeTpOgg) {
        this.odeTpOgg = Functions.subString(odeTpOgg, Len.ODE_TP_OGG);
    }

    public String getOdeTpOgg() {
        return this.odeTpOgg;
    }

    public void setOdeIbOgg(String odeIbOgg) {
        this.odeIbOgg = Functions.subString(odeIbOgg, Len.ODE_IB_OGG);
    }

    public String getOdeIbOgg() {
        return this.odeIbOgg;
    }

    public void setOdeTpDeroga(String odeTpDeroga) {
        this.odeTpDeroga = Functions.subString(odeTpDeroga, Len.ODE_TP_DEROGA);
    }

    public String getOdeTpDeroga() {
        return this.odeTpDeroga;
    }

    public void setOdeCodGrAutApprt(long odeCodGrAutApprt) {
        this.odeCodGrAutApprt = odeCodGrAutApprt;
    }

    public long getOdeCodGrAutApprt() {
        return this.odeCodGrAutApprt;
    }

    public void setOdeCodGrAutSup(long odeCodGrAutSup) {
        this.odeCodGrAutSup = odeCodGrAutSup;
    }

    public long getOdeCodGrAutSup() {
        return this.odeCodGrAutSup;
    }

    public void setOdeCodLivAutSup(int odeCodLivAutSup) {
        this.odeCodLivAutSup = odeCodLivAutSup;
    }

    public int getOdeCodLivAutSup() {
        return this.odeCodLivAutSup;
    }

    public void setOdeDsRiga(long odeDsRiga) {
        this.odeDsRiga = odeDsRiga;
    }

    public long getOdeDsRiga() {
        return this.odeDsRiga;
    }

    public void setOdeDsOperSql(char odeDsOperSql) {
        this.odeDsOperSql = odeDsOperSql;
    }

    public void setOdeDsOperSqlFormatted(String odeDsOperSql) {
        setOdeDsOperSql(Functions.charAt(odeDsOperSql, Types.CHAR_SIZE));
    }

    public char getOdeDsOperSql() {
        return this.odeDsOperSql;
    }

    public void setOdeDsVer(int odeDsVer) {
        this.odeDsVer = odeDsVer;
    }

    public int getOdeDsVer() {
        return this.odeDsVer;
    }

    public void setOdeDsTsIniCptz(long odeDsTsIniCptz) {
        this.odeDsTsIniCptz = odeDsTsIniCptz;
    }

    public long getOdeDsTsIniCptz() {
        return this.odeDsTsIniCptz;
    }

    public void setOdeDsTsEndCptz(long odeDsTsEndCptz) {
        this.odeDsTsEndCptz = odeDsTsEndCptz;
    }

    public long getOdeDsTsEndCptz() {
        return this.odeDsTsEndCptz;
    }

    public void setOdeDsUtente(String odeDsUtente) {
        this.odeDsUtente = Functions.subString(odeDsUtente, Len.ODE_DS_UTENTE);
    }

    public String getOdeDsUtente() {
        return this.odeDsUtente;
    }

    public void setOdeDsStatoElab(char odeDsStatoElab) {
        this.odeDsStatoElab = odeDsStatoElab;
    }

    public void setOdeDsStatoElabFormatted(String odeDsStatoElab) {
        setOdeDsStatoElab(Functions.charAt(odeDsStatoElab, Types.CHAR_SIZE));
    }

    public char getOdeDsStatoElab() {
        return this.odeDsStatoElab;
    }

    public OdeCodLivAutApprt getOdeCodLivAutApprt() {
        return odeCodLivAutApprt;
    }

    public OdeIdMoviChiu getOdeIdMoviChiu() {
        return odeIdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getOggDerogaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ODE_ID_OGG_DEROGA = 5;
        public static final int ODE_ID_MOVI_CRZ = 5;
        public static final int ODE_DT_INI_EFF = 5;
        public static final int ODE_DT_END_EFF = 5;
        public static final int ODE_COD_COMP_ANIA = 3;
        public static final int ODE_ID_OGG = 5;
        public static final int ODE_TP_OGG = 2;
        public static final int ODE_IB_OGG = 40;
        public static final int ODE_TP_DEROGA = 2;
        public static final int ODE_COD_GR_AUT_APPRT = 6;
        public static final int ODE_COD_GR_AUT_SUP = 6;
        public static final int ODE_COD_LIV_AUT_SUP = 3;
        public static final int ODE_DS_RIGA = 6;
        public static final int ODE_DS_OPER_SQL = 1;
        public static final int ODE_DS_VER = 5;
        public static final int ODE_DS_TS_INI_CPTZ = 10;
        public static final int ODE_DS_TS_END_CPTZ = 10;
        public static final int ODE_DS_UTENTE = 20;
        public static final int ODE_DS_STATO_ELAB = 1;
        public static final int OGG_DEROGA = ODE_ID_OGG_DEROGA + ODE_ID_MOVI_CRZ + OdeIdMoviChiu.Len.ODE_ID_MOVI_CHIU + ODE_DT_INI_EFF + ODE_DT_END_EFF + ODE_COD_COMP_ANIA + ODE_ID_OGG + ODE_TP_OGG + ODE_IB_OGG + ODE_TP_DEROGA + ODE_COD_GR_AUT_APPRT + OdeCodLivAutApprt.Len.ODE_COD_LIV_AUT_APPRT + ODE_COD_GR_AUT_SUP + ODE_COD_LIV_AUT_SUP + ODE_DS_RIGA + ODE_DS_OPER_SQL + ODE_DS_VER + ODE_DS_TS_INI_CPTZ + ODE_DS_TS_END_CPTZ + ODE_DS_UTENTE + ODE_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ODE_ID_OGG_DEROGA = 9;
            public static final int ODE_ID_MOVI_CRZ = 9;
            public static final int ODE_DT_INI_EFF = 8;
            public static final int ODE_DT_END_EFF = 8;
            public static final int ODE_COD_COMP_ANIA = 5;
            public static final int ODE_ID_OGG = 9;
            public static final int ODE_COD_GR_AUT_APPRT = 10;
            public static final int ODE_COD_GR_AUT_SUP = 10;
            public static final int ODE_COD_LIV_AUT_SUP = 5;
            public static final int ODE_DS_RIGA = 10;
            public static final int ODE_DS_VER = 9;
            public static final int ODE_DS_TS_INI_CPTZ = 18;
            public static final int ODE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
