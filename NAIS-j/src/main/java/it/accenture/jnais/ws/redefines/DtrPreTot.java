package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PRE-TOT<br>
 * Variable: DTR-PRE-TOT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PRE_TOT;
    }

    public void setDtrPreTot(AfDecimal dtrPreTot) {
        writeDecimalAsPacked(Pos.DTR_PRE_TOT, dtrPreTot.copy());
    }

    public void setDtrPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PRE_TOT, Pos.DTR_PRE_TOT);
    }

    /**Original name: DTR-PRE-TOT<br>*/
    public AfDecimal getDtrPreTot() {
        return readPackedAsDecimal(Pos.DTR_PRE_TOT, Len.Int.DTR_PRE_TOT, Len.Fract.DTR_PRE_TOT);
    }

    public byte[] getDtrPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PRE_TOT, Pos.DTR_PRE_TOT);
        return buffer;
    }

    public void setDtrPreTotNull(String dtrPreTotNull) {
        writeString(Pos.DTR_PRE_TOT_NULL, dtrPreTotNull, Len.DTR_PRE_TOT_NULL);
    }

    /**Original name: DTR-PRE-TOT-NULL<br>*/
    public String getDtrPreTotNull() {
        return readString(Pos.DTR_PRE_TOT_NULL, Len.DTR_PRE_TOT_NULL);
    }

    public String getDtrPreTotNullFormatted() {
        return Functions.padBlanks(getDtrPreTotNull(), Len.DTR_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PRE_TOT = 1;
        public static final int DTR_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PRE_TOT = 8;
        public static final int DTR_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
