package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ESTR-ASS-MAG70A<br>
 * Variable: PCO-DT-ESTR-ASS-MAG70A from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtEstrAssMag70a extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtEstrAssMag70a() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ESTR_ASS_MAG70A;
    }

    public void setPcoDtEstrAssMag70a(int pcoDtEstrAssMag70a) {
        writeIntAsPacked(Pos.PCO_DT_ESTR_ASS_MAG70A, pcoDtEstrAssMag70a, Len.Int.PCO_DT_ESTR_ASS_MAG70A);
    }

    public void setPcoDtEstrAssMag70aFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ESTR_ASS_MAG70A, Pos.PCO_DT_ESTR_ASS_MAG70A);
    }

    /**Original name: PCO-DT-ESTR-ASS-MAG70A<br>*/
    public int getPcoDtEstrAssMag70a() {
        return readPackedAsInt(Pos.PCO_DT_ESTR_ASS_MAG70A, Len.Int.PCO_DT_ESTR_ASS_MAG70A);
    }

    public byte[] getPcoDtEstrAssMag70aAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ESTR_ASS_MAG70A, Pos.PCO_DT_ESTR_ASS_MAG70A);
        return buffer;
    }

    public void initPcoDtEstrAssMag70aHighValues() {
        fill(Pos.PCO_DT_ESTR_ASS_MAG70A, Len.PCO_DT_ESTR_ASS_MAG70A, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtEstrAssMag70aNull(String pcoDtEstrAssMag70aNull) {
        writeString(Pos.PCO_DT_ESTR_ASS_MAG70A_NULL, pcoDtEstrAssMag70aNull, Len.PCO_DT_ESTR_ASS_MAG70A_NULL);
    }

    /**Original name: PCO-DT-ESTR-ASS-MAG70A-NULL<br>*/
    public String getPcoDtEstrAssMag70aNull() {
        return readString(Pos.PCO_DT_ESTR_ASS_MAG70A_NULL, Len.PCO_DT_ESTR_ASS_MAG70A_NULL);
    }

    public String getPcoDtEstrAssMag70aNullFormatted() {
        return Functions.padBlanks(getPcoDtEstrAssMag70aNull(), Len.PCO_DT_ESTR_ASS_MAG70A_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ESTR_ASS_MAG70A = 1;
        public static final int PCO_DT_ESTR_ASS_MAG70A_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ESTR_ASS_MAG70A = 5;
        public static final int PCO_DT_ESTR_ASS_MAG70A_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ESTR_ASS_MAG70A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
