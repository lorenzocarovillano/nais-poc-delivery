package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-DT-INI-COP<br>
 * Variable: DTC-DT-INI-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_DT_INI_COP;
    }

    public void setDtcDtIniCop(int dtcDtIniCop) {
        writeIntAsPacked(Pos.DTC_DT_INI_COP, dtcDtIniCop, Len.Int.DTC_DT_INI_COP);
    }

    public void setDtcDtIniCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_DT_INI_COP, Pos.DTC_DT_INI_COP);
    }

    /**Original name: DTC-DT-INI-COP<br>*/
    public int getDtcDtIniCop() {
        return readPackedAsInt(Pos.DTC_DT_INI_COP, Len.Int.DTC_DT_INI_COP);
    }

    public byte[] getDtcDtIniCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_DT_INI_COP, Pos.DTC_DT_INI_COP);
        return buffer;
    }

    public void setDtcDtIniCopNull(String dtcDtIniCopNull) {
        writeString(Pos.DTC_DT_INI_COP_NULL, dtcDtIniCopNull, Len.DTC_DT_INI_COP_NULL);
    }

    /**Original name: DTC-DT-INI-COP-NULL<br>*/
    public String getDtcDtIniCopNull() {
        return readString(Pos.DTC_DT_INI_COP_NULL, Len.DTC_DT_INI_COP_NULL);
    }

    public String getDtcDtIniCopNullFormatted() {
        return Functions.padBlanks(getDtcDtIniCopNull(), Len.DTC_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_DT_INI_COP = 1;
        public static final int DTC_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_DT_INI_COP = 5;
        public static final int DTC_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
