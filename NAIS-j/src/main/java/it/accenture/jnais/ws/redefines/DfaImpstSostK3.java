package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-SOST-K3<br>
 * Variable: DFA-IMPST-SOST-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstSostK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstSostK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_SOST_K3;
    }

    public void setDfaImpstSostK3(AfDecimal dfaImpstSostK3) {
        writeDecimalAsPacked(Pos.DFA_IMPST_SOST_K3, dfaImpstSostK3.copy());
    }

    public void setDfaImpstSostK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_SOST_K3, Pos.DFA_IMPST_SOST_K3);
    }

    /**Original name: DFA-IMPST-SOST-K3<br>*/
    public AfDecimal getDfaImpstSostK3() {
        return readPackedAsDecimal(Pos.DFA_IMPST_SOST_K3, Len.Int.DFA_IMPST_SOST_K3, Len.Fract.DFA_IMPST_SOST_K3);
    }

    public byte[] getDfaImpstSostK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_SOST_K3, Pos.DFA_IMPST_SOST_K3);
        return buffer;
    }

    public void setDfaImpstSostK3Null(String dfaImpstSostK3Null) {
        writeString(Pos.DFA_IMPST_SOST_K3_NULL, dfaImpstSostK3Null, Len.DFA_IMPST_SOST_K3_NULL);
    }

    /**Original name: DFA-IMPST-SOST-K3-NULL<br>*/
    public String getDfaImpstSostK3Null() {
        return readString(Pos.DFA_IMPST_SOST_K3_NULL, Len.DFA_IMPST_SOST_K3_NULL);
    }

    public String getDfaImpstSostK3NullFormatted() {
        return Functions.padBlanks(getDfaImpstSostK3Null(), Len.DFA_IMPST_SOST_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K3 = 1;
        public static final int DFA_IMPST_SOST_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K3 = 8;
        public static final int DFA_IMPST_SOST_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
