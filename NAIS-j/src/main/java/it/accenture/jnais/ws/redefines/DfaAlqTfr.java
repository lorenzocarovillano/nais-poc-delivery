package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ALQ-TFR<br>
 * Variable: DFA-ALQ-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAlqTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAlqTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ALQ_TFR;
    }

    public void setDfaAlqTfr(AfDecimal dfaAlqTfr) {
        writeDecimalAsPacked(Pos.DFA_ALQ_TFR, dfaAlqTfr.copy());
    }

    public void setDfaAlqTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ALQ_TFR, Pos.DFA_ALQ_TFR);
    }

    /**Original name: DFA-ALQ-TFR<br>*/
    public AfDecimal getDfaAlqTfr() {
        return readPackedAsDecimal(Pos.DFA_ALQ_TFR, Len.Int.DFA_ALQ_TFR, Len.Fract.DFA_ALQ_TFR);
    }

    public byte[] getDfaAlqTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ALQ_TFR, Pos.DFA_ALQ_TFR);
        return buffer;
    }

    public void setDfaAlqTfrNull(String dfaAlqTfrNull) {
        writeString(Pos.DFA_ALQ_TFR_NULL, dfaAlqTfrNull, Len.DFA_ALQ_TFR_NULL);
    }

    /**Original name: DFA-ALQ-TFR-NULL<br>*/
    public String getDfaAlqTfrNull() {
        return readString(Pos.DFA_ALQ_TFR_NULL, Len.DFA_ALQ_TFR_NULL);
    }

    public String getDfaAlqTfrNullFormatted() {
        return Functions.padBlanks(getDfaAlqTfrNull(), Len.DFA_ALQ_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ALQ_TFR = 1;
        public static final int DFA_ALQ_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ALQ_TFR = 4;
        public static final int DFA_ALQ_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ALQ_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_ALQ_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
