package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-CNBT-INPSTFM<br>
 * Variable: LQU-CNBT-INPSTFM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquCnbtInpstfm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquCnbtInpstfm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_CNBT_INPSTFM;
    }

    public void setLquCnbtInpstfm(AfDecimal lquCnbtInpstfm) {
        writeDecimalAsPacked(Pos.LQU_CNBT_INPSTFM, lquCnbtInpstfm.copy());
    }

    public void setLquCnbtInpstfmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_CNBT_INPSTFM, Pos.LQU_CNBT_INPSTFM);
    }

    /**Original name: LQU-CNBT-INPSTFM<br>*/
    public AfDecimal getLquCnbtInpstfm() {
        return readPackedAsDecimal(Pos.LQU_CNBT_INPSTFM, Len.Int.LQU_CNBT_INPSTFM, Len.Fract.LQU_CNBT_INPSTFM);
    }

    public byte[] getLquCnbtInpstfmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_CNBT_INPSTFM, Pos.LQU_CNBT_INPSTFM);
        return buffer;
    }

    public void setLquCnbtInpstfmNull(String lquCnbtInpstfmNull) {
        writeString(Pos.LQU_CNBT_INPSTFM_NULL, lquCnbtInpstfmNull, Len.LQU_CNBT_INPSTFM_NULL);
    }

    /**Original name: LQU-CNBT-INPSTFM-NULL<br>*/
    public String getLquCnbtInpstfmNull() {
        return readString(Pos.LQU_CNBT_INPSTFM_NULL, Len.LQU_CNBT_INPSTFM_NULL);
    }

    public String getLquCnbtInpstfmNullFormatted() {
        return Functions.padBlanks(getLquCnbtInpstfmNull(), Len.LQU_CNBT_INPSTFM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_CNBT_INPSTFM = 1;
        public static final int LQU_CNBT_INPSTFM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_CNBT_INPSTFM = 8;
        public static final int LQU_CNBT_INPSTFM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_CNBT_INPSTFM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_CNBT_INPSTFM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
