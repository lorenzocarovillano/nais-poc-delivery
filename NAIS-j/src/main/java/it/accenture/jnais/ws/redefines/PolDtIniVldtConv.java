package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DT-INI-VLDT-CONV<br>
 * Variable: POL-DT-INI-VLDT-CONV from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDtIniVldtConv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDtIniVldtConv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DT_INI_VLDT_CONV;
    }

    public void setPolDtIniVldtConv(int polDtIniVldtConv) {
        writeIntAsPacked(Pos.POL_DT_INI_VLDT_CONV, polDtIniVldtConv, Len.Int.POL_DT_INI_VLDT_CONV);
    }

    public void setPolDtIniVldtConvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DT_INI_VLDT_CONV, Pos.POL_DT_INI_VLDT_CONV);
    }

    /**Original name: POL-DT-INI-VLDT-CONV<br>*/
    public int getPolDtIniVldtConv() {
        return readPackedAsInt(Pos.POL_DT_INI_VLDT_CONV, Len.Int.POL_DT_INI_VLDT_CONV);
    }

    public byte[] getPolDtIniVldtConvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DT_INI_VLDT_CONV, Pos.POL_DT_INI_VLDT_CONV);
        return buffer;
    }

    public void setPolDtIniVldtConvNull(String polDtIniVldtConvNull) {
        writeString(Pos.POL_DT_INI_VLDT_CONV_NULL, polDtIniVldtConvNull, Len.POL_DT_INI_VLDT_CONV_NULL);
    }

    /**Original name: POL-DT-INI-VLDT-CONV-NULL<br>*/
    public String getPolDtIniVldtConvNull() {
        return readString(Pos.POL_DT_INI_VLDT_CONV_NULL, Len.POL_DT_INI_VLDT_CONV_NULL);
    }

    public String getPolDtIniVldtConvNullFormatted() {
        return Functions.padBlanks(getPolDtIniVldtConvNull(), Len.POL_DT_INI_VLDT_CONV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DT_INI_VLDT_CONV = 1;
        public static final int POL_DT_INI_VLDT_CONV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DT_INI_VLDT_CONV = 5;
        public static final int POL_DT_INI_VLDT_CONV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DT_INI_VLDT_CONV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
