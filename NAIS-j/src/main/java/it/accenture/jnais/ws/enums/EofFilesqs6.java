package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EOF-FILESQS6<br>
 * Variable: EOF-FILESQS6 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EofFilesqs6 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setEofFilesqs6(char eofFilesqs6) {
        this.value = eofFilesqs6;
    }

    public char getEofFilesqs6() {
        return this.value;
    }

    public void setFilesqs6EofSi() {
        value = SI;
    }

    public void setFilesqs6EofNo() {
        value = NO;
    }
}
