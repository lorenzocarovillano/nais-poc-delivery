package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.IndLogErrore;
import it.accenture.jnais.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0120<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0120Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-TIMESTAMP
    private WsTimestamp wsTimestamp = new WsTimestamp();
    //Original name: IND-LOG-ERRORE
    private IndLogErrore indLogErrore = new IndLogErrore();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public IndLogErrore getIndLogErrore() {
        return indLogErrore;
    }

    public WsTimestamp getWsTimestamp() {
        return wsTimestamp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
