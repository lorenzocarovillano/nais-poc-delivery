package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0221-TAB-RISPOSTA<br>
 * Variables: IVVC0221-TAB-RISPOSTA from copybook IVVC0221<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0221TabRisposta {

    //==== PROPERTIES ====
    //Original name: IVVC0221-VAL-RISPOSTA
    private String valRisposta = DefaultValues.stringVal(Len.VAL_RISPOSTA);
    //Original name: IVVC0221-FILLER-RIS
    private char fillerRis = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTabRispostaBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaRispBytes(buffer, position);
    }

    public void initTabRispostaSpaces() {
        initAreaRispSpaces();
    }

    public void setAreaRispBytes(byte[] buffer, int offset) {
        int position = offset;
        valRisposta = MarshalByte.readString(buffer, position, Len.VAL_RISPOSTA);
        position += Len.VAL_RISPOSTA;
        fillerRis = MarshalByte.readChar(buffer, position);
    }

    public void initAreaRispSpaces() {
        valRisposta = "";
        fillerRis = Types.SPACE_CHAR;
    }

    public void setValRisposta(String valRisposta) {
        this.valRisposta = Functions.subString(valRisposta, Len.VAL_RISPOSTA);
    }

    public String getValRisposta() {
        return this.valRisposta;
    }

    public void setFillerRis(char fillerRis) {
        this.fillerRis = fillerRis;
    }

    public char getFillerRis() {
        return this.fillerRis;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int VAL_RISPOSTA = 3;
        public static final int FILLER_RIS = 1;
        public static final int AREA_RISP = VAL_RISPOSTA + FILLER_RIS;
        public static final int TAB_RISPOSTA = AREA_RISP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
