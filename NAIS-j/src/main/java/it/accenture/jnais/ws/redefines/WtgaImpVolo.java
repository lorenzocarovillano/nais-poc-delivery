package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-VOLO<br>
 * Variable: WTGA-IMP-VOLO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_VOLO;
    }

    public void setWtgaImpVolo(AfDecimal wtgaImpVolo) {
        writeDecimalAsPacked(Pos.WTGA_IMP_VOLO, wtgaImpVolo.copy());
    }

    public void setWtgaImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_VOLO, Pos.WTGA_IMP_VOLO);
    }

    /**Original name: WTGA-IMP-VOLO<br>*/
    public AfDecimal getWtgaImpVolo() {
        return readPackedAsDecimal(Pos.WTGA_IMP_VOLO, Len.Int.WTGA_IMP_VOLO, Len.Fract.WTGA_IMP_VOLO);
    }

    public byte[] getWtgaImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_VOLO, Pos.WTGA_IMP_VOLO);
        return buffer;
    }

    public void initWtgaImpVoloSpaces() {
        fill(Pos.WTGA_IMP_VOLO, Len.WTGA_IMP_VOLO, Types.SPACE_CHAR);
    }

    public void setWtgaImpVoloNull(String wtgaImpVoloNull) {
        writeString(Pos.WTGA_IMP_VOLO_NULL, wtgaImpVoloNull, Len.WTGA_IMP_VOLO_NULL);
    }

    /**Original name: WTGA-IMP-VOLO-NULL<br>*/
    public String getWtgaImpVoloNull() {
        return readString(Pos.WTGA_IMP_VOLO_NULL, Len.WTGA_IMP_VOLO_NULL);
    }

    public String getWtgaImpVoloNullFormatted() {
        return Functions.padBlanks(getWtgaImpVoloNull(), Len.WTGA_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_VOLO = 1;
        public static final int WTGA_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_VOLO = 8;
        public static final int WTGA_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
