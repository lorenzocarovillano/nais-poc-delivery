package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-RSP-IN<br>
 * Variable: PCO-DT-ULT-BOLL-RSP-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollRspIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollRspIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_RSP_IN;
    }

    public void setPcoDtUltBollRspIn(int pcoDtUltBollRspIn) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_RSP_IN, pcoDtUltBollRspIn, Len.Int.PCO_DT_ULT_BOLL_RSP_IN);
    }

    public void setPcoDtUltBollRspInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RSP_IN, Pos.PCO_DT_ULT_BOLL_RSP_IN);
    }

    /**Original name: PCO-DT-ULT-BOLL-RSP-IN<br>*/
    public int getPcoDtUltBollRspIn() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_RSP_IN, Len.Int.PCO_DT_ULT_BOLL_RSP_IN);
    }

    public byte[] getPcoDtUltBollRspInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_RSP_IN, Pos.PCO_DT_ULT_BOLL_RSP_IN);
        return buffer;
    }

    public void initPcoDtUltBollRspInHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_RSP_IN, Len.PCO_DT_ULT_BOLL_RSP_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollRspInNull(String pcoDtUltBollRspInNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_RSP_IN_NULL, pcoDtUltBollRspInNull, Len.PCO_DT_ULT_BOLL_RSP_IN_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-RSP-IN-NULL<br>*/
    public String getPcoDtUltBollRspInNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_RSP_IN_NULL, Len.PCO_DT_ULT_BOLL_RSP_IN_NULL);
    }

    public String getPcoDtUltBollRspInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollRspInNull(), Len.PCO_DT_ULT_BOLL_RSP_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RSP_IN = 1;
        public static final int PCO_DT_ULT_BOLL_RSP_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_RSP_IN = 5;
        public static final int PCO_DT_ULT_BOLL_RSP_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_RSP_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
