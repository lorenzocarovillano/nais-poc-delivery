package it.accenture.jnais.ws;

import it.accenture.jnais.ws.redefines.WkAnnoRifX;
import it.accenture.jnais.ws.redefines.WkDtRicorAX;
import it.accenture.jnais.ws.redefines.WkDtRicorDaX;

/**Original name: DATE-WORK<br>
 * Variable: DATE-WORK from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DateWork {

    //==== PROPERTIES ====
    //Original name: WK-DT-RICOR-DA-X
    private WkDtRicorDaX wkDtRicorDaX = new WkDtRicorDaX();
    //Original name: WK-DT-RICOR-A-X
    private WkDtRicorAX wkDtRicorAX = new WkDtRicorAX();
    //Original name: WK-ANNO-RIF-X
    private WkAnnoRifX wkAnnoRifX = new WkAnnoRifX();
    //Original name: WK-INI-RIC
    private WkIniRic wkIniRic = new WkIniRic();
    //Original name: WK-FIN-RIC
    private WkFinRic wkFinRic = new WkFinRic();

    //==== METHODS ====
    public WkAnnoRifX getWkAnnoRifX() {
        return wkAnnoRifX;
    }

    public WkDtRicorAX getWkDtRicorAX() {
        return wkDtRicorAX;
    }

    public WkDtRicorDaX getWkDtRicorDaX() {
        return wkDtRicorDaX;
    }

    public WkFinRic getWkFinRic() {
        return wkFinRic;
    }

    public WkIniRic getWkIniRic() {
        return wkIniRic;
    }
}
