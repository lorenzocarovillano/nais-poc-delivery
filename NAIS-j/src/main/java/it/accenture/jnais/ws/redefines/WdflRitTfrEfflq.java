package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-TFR-EFFLQ<br>
 * Variable: WDFL-RIT-TFR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitTfrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitTfrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_TFR_EFFLQ;
    }

    public void setWdflRitTfrEfflq(AfDecimal wdflRitTfrEfflq) {
        writeDecimalAsPacked(Pos.WDFL_RIT_TFR_EFFLQ, wdflRitTfrEfflq.copy());
    }

    public void setWdflRitTfrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_TFR_EFFLQ, Pos.WDFL_RIT_TFR_EFFLQ);
    }

    /**Original name: WDFL-RIT-TFR-EFFLQ<br>*/
    public AfDecimal getWdflRitTfrEfflq() {
        return readPackedAsDecimal(Pos.WDFL_RIT_TFR_EFFLQ, Len.Int.WDFL_RIT_TFR_EFFLQ, Len.Fract.WDFL_RIT_TFR_EFFLQ);
    }

    public byte[] getWdflRitTfrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_TFR_EFFLQ, Pos.WDFL_RIT_TFR_EFFLQ);
        return buffer;
    }

    public void setWdflRitTfrEfflqNull(String wdflRitTfrEfflqNull) {
        writeString(Pos.WDFL_RIT_TFR_EFFLQ_NULL, wdflRitTfrEfflqNull, Len.WDFL_RIT_TFR_EFFLQ_NULL);
    }

    /**Original name: WDFL-RIT-TFR-EFFLQ-NULL<br>*/
    public String getWdflRitTfrEfflqNull() {
        return readString(Pos.WDFL_RIT_TFR_EFFLQ_NULL, Len.WDFL_RIT_TFR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_TFR_EFFLQ = 1;
        public static final int WDFL_RIT_TFR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_TFR_EFFLQ = 8;
        public static final int WDFL_RIT_TFR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_TFR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_TFR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
