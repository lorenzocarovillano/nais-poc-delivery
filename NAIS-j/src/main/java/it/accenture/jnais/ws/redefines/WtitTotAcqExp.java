package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-ACQ-EXP<br>
 * Variable: WTIT-TOT-ACQ-EXP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_ACQ_EXP;
    }

    public void setWtitTotAcqExp(AfDecimal wtitTotAcqExp) {
        writeDecimalAsPacked(Pos.WTIT_TOT_ACQ_EXP, wtitTotAcqExp.copy());
    }

    public void setWtitTotAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_ACQ_EXP, Pos.WTIT_TOT_ACQ_EXP);
    }

    /**Original name: WTIT-TOT-ACQ-EXP<br>*/
    public AfDecimal getWtitTotAcqExp() {
        return readPackedAsDecimal(Pos.WTIT_TOT_ACQ_EXP, Len.Int.WTIT_TOT_ACQ_EXP, Len.Fract.WTIT_TOT_ACQ_EXP);
    }

    public byte[] getWtitTotAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_ACQ_EXP, Pos.WTIT_TOT_ACQ_EXP);
        return buffer;
    }

    public void initWtitTotAcqExpSpaces() {
        fill(Pos.WTIT_TOT_ACQ_EXP, Len.WTIT_TOT_ACQ_EXP, Types.SPACE_CHAR);
    }

    public void setWtitTotAcqExpNull(String wtitTotAcqExpNull) {
        writeString(Pos.WTIT_TOT_ACQ_EXP_NULL, wtitTotAcqExpNull, Len.WTIT_TOT_ACQ_EXP_NULL);
    }

    /**Original name: WTIT-TOT-ACQ-EXP-NULL<br>*/
    public String getWtitTotAcqExpNull() {
        return readString(Pos.WTIT_TOT_ACQ_EXP_NULL, Len.WTIT_TOT_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_ACQ_EXP = 1;
        public static final int WTIT_TOT_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_ACQ_EXP = 8;
        public static final int WTIT_TOT_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
