package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-IMP-REN-ASS<br>
 * Variable: WPAG-CONT-IMP-REN-ASS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContImpRenAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContImpRenAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_IMP_REN_ASS;
    }

    public void setWpagContImpRenAss(AfDecimal wpagContImpRenAss) {
        writeDecimalAsPacked(Pos.WPAG_CONT_IMP_REN_ASS, wpagContImpRenAss.copy());
    }

    public void setWpagContImpRenAssFormatted(String wpagContImpRenAss) {
        setWpagContImpRenAss(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_IMP_REN_ASS + Len.Fract.WPAG_CONT_IMP_REN_ASS, Len.Fract.WPAG_CONT_IMP_REN_ASS, wpagContImpRenAss));
    }

    public void setWpagContImpRenAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_IMP_REN_ASS, Pos.WPAG_CONT_IMP_REN_ASS);
    }

    /**Original name: WPAG-CONT-IMP-REN-ASS<br>*/
    public AfDecimal getWpagContImpRenAss() {
        return readPackedAsDecimal(Pos.WPAG_CONT_IMP_REN_ASS, Len.Int.WPAG_CONT_IMP_REN_ASS, Len.Fract.WPAG_CONT_IMP_REN_ASS);
    }

    public byte[] getWpagContImpRenAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_IMP_REN_ASS, Pos.WPAG_CONT_IMP_REN_ASS);
        return buffer;
    }

    public void initWpagContImpRenAssSpaces() {
        fill(Pos.WPAG_CONT_IMP_REN_ASS, Len.WPAG_CONT_IMP_REN_ASS, Types.SPACE_CHAR);
    }

    public void setWpagContImpRenAssNull(String wpagContImpRenAssNull) {
        writeString(Pos.WPAG_CONT_IMP_REN_ASS_NULL, wpagContImpRenAssNull, Len.WPAG_CONT_IMP_REN_ASS_NULL);
    }

    /**Original name: WPAG-CONT-IMP-REN-ASS-NULL<br>*/
    public String getWpagContImpRenAssNull() {
        return readString(Pos.WPAG_CONT_IMP_REN_ASS_NULL, Len.WPAG_CONT_IMP_REN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_IMP_REN_ASS = 1;
        public static final int WPAG_CONT_IMP_REN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_IMP_REN_ASS = 8;
        public static final int WPAG_CONT_IMP_REN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_IMP_REN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_IMP_REN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
