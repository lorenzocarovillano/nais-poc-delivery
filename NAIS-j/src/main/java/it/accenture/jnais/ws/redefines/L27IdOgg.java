package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L27-ID-OGG<br>
 * Variable: L27-ID-OGG from program IDBSL270<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L27IdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L27IdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L27_ID_OGG;
    }

    public void setL27IdOgg(int l27IdOgg) {
        writeIntAsPacked(Pos.L27_ID_OGG, l27IdOgg, Len.Int.L27_ID_OGG);
    }

    public void setL27IdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L27_ID_OGG, Pos.L27_ID_OGG);
    }

    /**Original name: L27-ID-OGG<br>*/
    public int getL27IdOgg() {
        return readPackedAsInt(Pos.L27_ID_OGG, Len.Int.L27_ID_OGG);
    }

    public byte[] getL27IdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L27_ID_OGG, Pos.L27_ID_OGG);
        return buffer;
    }

    public void setL27IdOggNull(String l27IdOggNull) {
        writeString(Pos.L27_ID_OGG_NULL, l27IdOggNull, Len.L27_ID_OGG_NULL);
    }

    /**Original name: L27-ID-OGG-NULL<br>*/
    public String getL27IdOggNull() {
        return readString(Pos.L27_ID_OGG_NULL, Len.L27_ID_OGG_NULL);
    }

    public String getL27IdOggNullFormatted() {
        return Functions.padBlanks(getL27IdOggNull(), Len.L27_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L27_ID_OGG = 1;
        public static final int L27_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L27_ID_OGG = 5;
        public static final int L27_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L27_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
