package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-IRPEF-CALC<br>
 * Variable: WDFL-RIT-IRPEF-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitIrpefCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitIrpefCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_IRPEF_CALC;
    }

    public void setWdflRitIrpefCalc(AfDecimal wdflRitIrpefCalc) {
        writeDecimalAsPacked(Pos.WDFL_RIT_IRPEF_CALC, wdflRitIrpefCalc.copy());
    }

    public void setWdflRitIrpefCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_IRPEF_CALC, Pos.WDFL_RIT_IRPEF_CALC);
    }

    /**Original name: WDFL-RIT-IRPEF-CALC<br>*/
    public AfDecimal getWdflRitIrpefCalc() {
        return readPackedAsDecimal(Pos.WDFL_RIT_IRPEF_CALC, Len.Int.WDFL_RIT_IRPEF_CALC, Len.Fract.WDFL_RIT_IRPEF_CALC);
    }

    public byte[] getWdflRitIrpefCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_IRPEF_CALC, Pos.WDFL_RIT_IRPEF_CALC);
        return buffer;
    }

    public void setWdflRitIrpefCalcNull(String wdflRitIrpefCalcNull) {
        writeString(Pos.WDFL_RIT_IRPEF_CALC_NULL, wdflRitIrpefCalcNull, Len.WDFL_RIT_IRPEF_CALC_NULL);
    }

    /**Original name: WDFL-RIT-IRPEF-CALC-NULL<br>*/
    public String getWdflRitIrpefCalcNull() {
        return readString(Pos.WDFL_RIT_IRPEF_CALC_NULL, Len.WDFL_RIT_IRPEF_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_IRPEF_CALC = 1;
        public static final int WDFL_RIT_IRPEF_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_IRPEF_CALC = 8;
        public static final int WDFL_RIT_IRPEF_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_IRPEF_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_IRPEF_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
