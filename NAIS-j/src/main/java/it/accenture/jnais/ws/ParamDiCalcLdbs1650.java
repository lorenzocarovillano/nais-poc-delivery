package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.PcaIdMoviChiu;
import it.accenture.jnais.ws.redefines.PcaIdOgg;
import it.accenture.jnais.ws.redefines.PcaValDt;
import it.accenture.jnais.ws.redefines.PcaValImp;
import it.accenture.jnais.ws.redefines.PcaValNum;
import it.accenture.jnais.ws.redefines.PcaValPc;
import it.accenture.jnais.ws.redefines.PcaValTs;

/**Original name: PARAM-DI-CALC<br>
 * Variable: PARAM-DI-CALC from copybook IDBVPCA1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ParamDiCalcLdbs1650 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: PCA-ID-PARAM-DI-CALC
    private int pcaIdParamDiCalc = DefaultValues.INT_VAL;
    //Original name: PCA-ID-OGG
    private PcaIdOgg pcaIdOgg = new PcaIdOgg();
    //Original name: PCA-TP-OGG
    private String pcaTpOgg = DefaultValues.stringVal(Len.PCA_TP_OGG);
    //Original name: PCA-ID-MOVI-CRZ
    private int pcaIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: PCA-ID-MOVI-CHIU
    private PcaIdMoviChiu pcaIdMoviChiu = new PcaIdMoviChiu();
    //Original name: PCA-DT-INI-EFF
    private int pcaDtIniEff = DefaultValues.INT_VAL;
    //Original name: PCA-DT-END-EFF
    private int pcaDtEndEff = DefaultValues.INT_VAL;
    //Original name: PCA-COD-COMP-ANIA
    private int pcaCodCompAnia = DefaultValues.INT_VAL;
    //Original name: PCA-COD-PARAM
    private String pcaCodParam = DefaultValues.stringVal(Len.PCA_COD_PARAM);
    //Original name: PCA-TP-D
    private String pcaTpD = DefaultValues.stringVal(Len.PCA_TP_D);
    //Original name: PCA-VAL-DT
    private PcaValDt pcaValDt = new PcaValDt();
    //Original name: PCA-VAL-IMP
    private PcaValImp pcaValImp = new PcaValImp();
    //Original name: PCA-VAL-TS
    private PcaValTs pcaValTs = new PcaValTs();
    //Original name: PCA-VAL-NUM
    private PcaValNum pcaValNum = new PcaValNum();
    //Original name: PCA-VAL-PC
    private PcaValPc pcaValPc = new PcaValPc();
    //Original name: PCA-VAL-TXT-LEN
    private short pcaValTxtLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: PCA-VAL-TXT
    private String pcaValTxt = DefaultValues.stringVal(Len.PCA_VAL_TXT);
    //Original name: PCA-VAL-FL
    private char pcaValFl = DefaultValues.CHAR_VAL;
    //Original name: PCA-DS-RIGA
    private long pcaDsRiga = DefaultValues.LONG_VAL;
    //Original name: PCA-DS-OPER-SQL
    private char pcaDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: PCA-DS-VER
    private int pcaDsVer = DefaultValues.INT_VAL;
    //Original name: PCA-DS-TS-INI-CPTZ
    private long pcaDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: PCA-DS-TS-END-CPTZ
    private long pcaDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: PCA-DS-UTENTE
    private String pcaDsUtente = DefaultValues.stringVal(Len.PCA_DS_UTENTE);
    //Original name: PCA-DS-STATO-ELAB
    private char pcaDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PARAM_DI_CALC;
    }

    @Override
    public void deserialize(byte[] buf) {
        setParamDiCalcBytes(buf);
    }

    public void setParamDiCalcFormatted(String data) {
        byte[] buffer = new byte[Len.PARAM_DI_CALC];
        MarshalByte.writeString(buffer, 1, data, Len.PARAM_DI_CALC);
        setParamDiCalcBytes(buffer, 1);
    }

    public String getParamDiCalcFormatted() {
        return MarshalByteExt.bufferToStr(getParamDiCalcBytes());
    }

    public void setParamDiCalcBytes(byte[] buffer) {
        setParamDiCalcBytes(buffer, 1);
    }

    public byte[] getParamDiCalcBytes() {
        byte[] buffer = new byte[Len.PARAM_DI_CALC];
        return getParamDiCalcBytes(buffer, 1);
    }

    public void setParamDiCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        pcaIdParamDiCalc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCA_ID_PARAM_DI_CALC, 0);
        position += Len.PCA_ID_PARAM_DI_CALC;
        pcaIdOgg.setPcaIdOggFromBuffer(buffer, position);
        position += PcaIdOgg.Len.PCA_ID_OGG;
        pcaTpOgg = MarshalByte.readString(buffer, position, Len.PCA_TP_OGG);
        position += Len.PCA_TP_OGG;
        pcaIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCA_ID_MOVI_CRZ, 0);
        position += Len.PCA_ID_MOVI_CRZ;
        pcaIdMoviChiu.setPcaIdMoviChiuFromBuffer(buffer, position);
        position += PcaIdMoviChiu.Len.PCA_ID_MOVI_CHIU;
        pcaDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCA_DT_INI_EFF, 0);
        position += Len.PCA_DT_INI_EFF;
        pcaDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCA_DT_END_EFF, 0);
        position += Len.PCA_DT_END_EFF;
        pcaCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCA_COD_COMP_ANIA, 0);
        position += Len.PCA_COD_COMP_ANIA;
        pcaCodParam = MarshalByte.readString(buffer, position, Len.PCA_COD_PARAM);
        position += Len.PCA_COD_PARAM;
        pcaTpD = MarshalByte.readString(buffer, position, Len.PCA_TP_D);
        position += Len.PCA_TP_D;
        pcaValDt.setPcaValDtFromBuffer(buffer, position);
        position += PcaValDt.Len.PCA_VAL_DT;
        pcaValImp.setPcaValImpFromBuffer(buffer, position);
        position += PcaValImp.Len.PCA_VAL_IMP;
        pcaValTs.setPcaValTsFromBuffer(buffer, position);
        position += PcaValTs.Len.PCA_VAL_TS;
        pcaValNum.setPcaValNumFromBuffer(buffer, position);
        position += PcaValNum.Len.PCA_VAL_NUM;
        pcaValPc.setPcaValPcFromBuffer(buffer, position);
        position += PcaValPc.Len.PCA_VAL_PC;
        setPcaValTxtVcharBytes(buffer, position);
        position += Len.PCA_VAL_TXT_VCHAR;
        pcaValFl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcaDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PCA_DS_RIGA, 0);
        position += Len.PCA_DS_RIGA;
        pcaDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        pcaDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PCA_DS_VER, 0);
        position += Len.PCA_DS_VER;
        pcaDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PCA_DS_TS_INI_CPTZ, 0);
        position += Len.PCA_DS_TS_INI_CPTZ;
        pcaDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PCA_DS_TS_END_CPTZ, 0);
        position += Len.PCA_DS_TS_END_CPTZ;
        pcaDsUtente = MarshalByte.readString(buffer, position, Len.PCA_DS_UTENTE);
        position += Len.PCA_DS_UTENTE;
        pcaDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getParamDiCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, pcaIdParamDiCalc, Len.Int.PCA_ID_PARAM_DI_CALC, 0);
        position += Len.PCA_ID_PARAM_DI_CALC;
        pcaIdOgg.getPcaIdOggAsBuffer(buffer, position);
        position += PcaIdOgg.Len.PCA_ID_OGG;
        MarshalByte.writeString(buffer, position, pcaTpOgg, Len.PCA_TP_OGG);
        position += Len.PCA_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, pcaIdMoviCrz, Len.Int.PCA_ID_MOVI_CRZ, 0);
        position += Len.PCA_ID_MOVI_CRZ;
        pcaIdMoviChiu.getPcaIdMoviChiuAsBuffer(buffer, position);
        position += PcaIdMoviChiu.Len.PCA_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, pcaDtIniEff, Len.Int.PCA_DT_INI_EFF, 0);
        position += Len.PCA_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pcaDtEndEff, Len.Int.PCA_DT_END_EFF, 0);
        position += Len.PCA_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, pcaCodCompAnia, Len.Int.PCA_COD_COMP_ANIA, 0);
        position += Len.PCA_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, pcaCodParam, Len.PCA_COD_PARAM);
        position += Len.PCA_COD_PARAM;
        MarshalByte.writeString(buffer, position, pcaTpD, Len.PCA_TP_D);
        position += Len.PCA_TP_D;
        pcaValDt.getPcaValDtAsBuffer(buffer, position);
        position += PcaValDt.Len.PCA_VAL_DT;
        pcaValImp.getPcaValImpAsBuffer(buffer, position);
        position += PcaValImp.Len.PCA_VAL_IMP;
        pcaValTs.getPcaValTsAsBuffer(buffer, position);
        position += PcaValTs.Len.PCA_VAL_TS;
        pcaValNum.getPcaValNumAsBuffer(buffer, position);
        position += PcaValNum.Len.PCA_VAL_NUM;
        pcaValPc.getPcaValPcAsBuffer(buffer, position);
        position += PcaValPc.Len.PCA_VAL_PC;
        getPcaValTxtVcharBytes(buffer, position);
        position += Len.PCA_VAL_TXT_VCHAR;
        MarshalByte.writeChar(buffer, position, pcaValFl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, pcaDsRiga, Len.Int.PCA_DS_RIGA, 0);
        position += Len.PCA_DS_RIGA;
        MarshalByte.writeChar(buffer, position, pcaDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, pcaDsVer, Len.Int.PCA_DS_VER, 0);
        position += Len.PCA_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, pcaDsTsIniCptz, Len.Int.PCA_DS_TS_INI_CPTZ, 0);
        position += Len.PCA_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, pcaDsTsEndCptz, Len.Int.PCA_DS_TS_END_CPTZ, 0);
        position += Len.PCA_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, pcaDsUtente, Len.PCA_DS_UTENTE);
        position += Len.PCA_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, pcaDsStatoElab);
        return buffer;
    }

    public void setPcaIdParamDiCalc(int pcaIdParamDiCalc) {
        this.pcaIdParamDiCalc = pcaIdParamDiCalc;
    }

    public int getPcaIdParamDiCalc() {
        return this.pcaIdParamDiCalc;
    }

    public void setPcaTpOgg(String pcaTpOgg) {
        this.pcaTpOgg = Functions.subString(pcaTpOgg, Len.PCA_TP_OGG);
    }

    public String getPcaTpOgg() {
        return this.pcaTpOgg;
    }

    public void setPcaIdMoviCrz(int pcaIdMoviCrz) {
        this.pcaIdMoviCrz = pcaIdMoviCrz;
    }

    public int getPcaIdMoviCrz() {
        return this.pcaIdMoviCrz;
    }

    public void setPcaDtIniEff(int pcaDtIniEff) {
        this.pcaDtIniEff = pcaDtIniEff;
    }

    public int getPcaDtIniEff() {
        return this.pcaDtIniEff;
    }

    public void setPcaDtEndEff(int pcaDtEndEff) {
        this.pcaDtEndEff = pcaDtEndEff;
    }

    public int getPcaDtEndEff() {
        return this.pcaDtEndEff;
    }

    public void setPcaCodCompAnia(int pcaCodCompAnia) {
        this.pcaCodCompAnia = pcaCodCompAnia;
    }

    public int getPcaCodCompAnia() {
        return this.pcaCodCompAnia;
    }

    public void setPcaCodParam(String pcaCodParam) {
        this.pcaCodParam = Functions.subString(pcaCodParam, Len.PCA_COD_PARAM);
    }

    public String getPcaCodParam() {
        return this.pcaCodParam;
    }

    public String getPcaCodParamFormatted() {
        return Functions.padBlanks(getPcaCodParam(), Len.PCA_COD_PARAM);
    }

    public void setPcaTpD(String pcaTpD) {
        this.pcaTpD = Functions.subString(pcaTpD, Len.PCA_TP_D);
    }

    public String getPcaTpD() {
        return this.pcaTpD;
    }

    public String getPcaTpDFormatted() {
        return Functions.padBlanks(getPcaTpD(), Len.PCA_TP_D);
    }

    public void setPcaValTxtVcharFormatted(String data) {
        byte[] buffer = new byte[Len.PCA_VAL_TXT_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.PCA_VAL_TXT_VCHAR);
        setPcaValTxtVcharBytes(buffer, 1);
    }

    public String getPcaValTxtVcharFormatted() {
        return MarshalByteExt.bufferToStr(getPcaValTxtVcharBytes());
    }

    /**Original name: PCA-VAL-TXT-VCHAR<br>*/
    public byte[] getPcaValTxtVcharBytes() {
        byte[] buffer = new byte[Len.PCA_VAL_TXT_VCHAR];
        return getPcaValTxtVcharBytes(buffer, 1);
    }

    public void setPcaValTxtVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        pcaValTxtLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        pcaValTxt = MarshalByte.readString(buffer, position, Len.PCA_VAL_TXT);
    }

    public byte[] getPcaValTxtVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, pcaValTxtLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, pcaValTxt, Len.PCA_VAL_TXT);
        return buffer;
    }

    public void setPcaValTxtLen(short pcaValTxtLen) {
        this.pcaValTxtLen = pcaValTxtLen;
    }

    public short getPcaValTxtLen() {
        return this.pcaValTxtLen;
    }

    public void setPcaValTxt(String pcaValTxt) {
        this.pcaValTxt = Functions.subString(pcaValTxt, Len.PCA_VAL_TXT);
    }

    public String getPcaValTxt() {
        return this.pcaValTxt;
    }

    public void setPcaValFl(char pcaValFl) {
        this.pcaValFl = pcaValFl;
    }

    public char getPcaValFl() {
        return this.pcaValFl;
    }

    public void setPcaDsRiga(long pcaDsRiga) {
        this.pcaDsRiga = pcaDsRiga;
    }

    public long getPcaDsRiga() {
        return this.pcaDsRiga;
    }

    public void setPcaDsOperSql(char pcaDsOperSql) {
        this.pcaDsOperSql = pcaDsOperSql;
    }

    public void setPcaDsOperSqlFormatted(String pcaDsOperSql) {
        setPcaDsOperSql(Functions.charAt(pcaDsOperSql, Types.CHAR_SIZE));
    }

    public char getPcaDsOperSql() {
        return this.pcaDsOperSql;
    }

    public void setPcaDsVer(int pcaDsVer) {
        this.pcaDsVer = pcaDsVer;
    }

    public int getPcaDsVer() {
        return this.pcaDsVer;
    }

    public void setPcaDsTsIniCptz(long pcaDsTsIniCptz) {
        this.pcaDsTsIniCptz = pcaDsTsIniCptz;
    }

    public long getPcaDsTsIniCptz() {
        return this.pcaDsTsIniCptz;
    }

    public void setPcaDsTsEndCptz(long pcaDsTsEndCptz) {
        this.pcaDsTsEndCptz = pcaDsTsEndCptz;
    }

    public long getPcaDsTsEndCptz() {
        return this.pcaDsTsEndCptz;
    }

    public void setPcaDsUtente(String pcaDsUtente) {
        this.pcaDsUtente = Functions.subString(pcaDsUtente, Len.PCA_DS_UTENTE);
    }

    public String getPcaDsUtente() {
        return this.pcaDsUtente;
    }

    public void setPcaDsStatoElab(char pcaDsStatoElab) {
        this.pcaDsStatoElab = pcaDsStatoElab;
    }

    public void setPcaDsStatoElabFormatted(String pcaDsStatoElab) {
        setPcaDsStatoElab(Functions.charAt(pcaDsStatoElab, Types.CHAR_SIZE));
    }

    public char getPcaDsStatoElab() {
        return this.pcaDsStatoElab;
    }

    public PcaIdMoviChiu getPcaIdMoviChiu() {
        return pcaIdMoviChiu;
    }

    public PcaIdOgg getPcaIdOgg() {
        return pcaIdOgg;
    }

    public PcaValDt getPcaValDt() {
        return pcaValDt;
    }

    public PcaValImp getPcaValImp() {
        return pcaValImp;
    }

    public PcaValNum getPcaValNum() {
        return pcaValNum;
    }

    public PcaValPc getPcaValPc() {
        return pcaValPc;
    }

    public PcaValTs getPcaValTs() {
        return pcaValTs;
    }

    @Override
    public byte[] serialize() {
        return getParamDiCalcBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_ID_PARAM_DI_CALC = 5;
        public static final int PCA_TP_OGG = 2;
        public static final int PCA_ID_MOVI_CRZ = 5;
        public static final int PCA_DT_INI_EFF = 5;
        public static final int PCA_DT_END_EFF = 5;
        public static final int PCA_COD_COMP_ANIA = 3;
        public static final int PCA_COD_PARAM = 20;
        public static final int PCA_TP_D = 2;
        public static final int PCA_VAL_TXT_LEN = 2;
        public static final int PCA_VAL_TXT = 100;
        public static final int PCA_VAL_TXT_VCHAR = PCA_VAL_TXT_LEN + PCA_VAL_TXT;
        public static final int PCA_VAL_FL = 1;
        public static final int PCA_DS_RIGA = 6;
        public static final int PCA_DS_OPER_SQL = 1;
        public static final int PCA_DS_VER = 5;
        public static final int PCA_DS_TS_INI_CPTZ = 10;
        public static final int PCA_DS_TS_END_CPTZ = 10;
        public static final int PCA_DS_UTENTE = 20;
        public static final int PCA_DS_STATO_ELAB = 1;
        public static final int PARAM_DI_CALC = PCA_ID_PARAM_DI_CALC + PcaIdOgg.Len.PCA_ID_OGG + PCA_TP_OGG + PCA_ID_MOVI_CRZ + PcaIdMoviChiu.Len.PCA_ID_MOVI_CHIU + PCA_DT_INI_EFF + PCA_DT_END_EFF + PCA_COD_COMP_ANIA + PCA_COD_PARAM + PCA_TP_D + PcaValDt.Len.PCA_VAL_DT + PcaValImp.Len.PCA_VAL_IMP + PcaValTs.Len.PCA_VAL_TS + PcaValNum.Len.PCA_VAL_NUM + PcaValPc.Len.PCA_VAL_PC + PCA_VAL_TXT_VCHAR + PCA_VAL_FL + PCA_DS_RIGA + PCA_DS_OPER_SQL + PCA_DS_VER + PCA_DS_TS_INI_CPTZ + PCA_DS_TS_END_CPTZ + PCA_DS_UTENTE + PCA_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_ID_PARAM_DI_CALC = 9;
            public static final int PCA_ID_MOVI_CRZ = 9;
            public static final int PCA_DT_INI_EFF = 8;
            public static final int PCA_DT_END_EFF = 8;
            public static final int PCA_COD_COMP_ANIA = 5;
            public static final int PCA_DS_RIGA = 10;
            public static final int PCA_DS_VER = 9;
            public static final int PCA_DS_TS_INI_CPTZ = 18;
            public static final int PCA_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
