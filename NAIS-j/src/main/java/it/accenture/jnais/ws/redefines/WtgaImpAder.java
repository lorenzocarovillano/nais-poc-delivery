package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-ADER<br>
 * Variable: WTGA-IMP-ADER from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_ADER;
    }

    public void setWtgaImpAder(AfDecimal wtgaImpAder) {
        writeDecimalAsPacked(Pos.WTGA_IMP_ADER, wtgaImpAder.copy());
    }

    public void setWtgaImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_ADER, Pos.WTGA_IMP_ADER);
    }

    /**Original name: WTGA-IMP-ADER<br>*/
    public AfDecimal getWtgaImpAder() {
        return readPackedAsDecimal(Pos.WTGA_IMP_ADER, Len.Int.WTGA_IMP_ADER, Len.Fract.WTGA_IMP_ADER);
    }

    public byte[] getWtgaImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_ADER, Pos.WTGA_IMP_ADER);
        return buffer;
    }

    public void initWtgaImpAderSpaces() {
        fill(Pos.WTGA_IMP_ADER, Len.WTGA_IMP_ADER, Types.SPACE_CHAR);
    }

    public void setWtgaImpAderNull(String wtgaImpAderNull) {
        writeString(Pos.WTGA_IMP_ADER_NULL, wtgaImpAderNull, Len.WTGA_IMP_ADER_NULL);
    }

    /**Original name: WTGA-IMP-ADER-NULL<br>*/
    public String getWtgaImpAderNull() {
        return readString(Pos.WTGA_IMP_ADER_NULL, Len.WTGA_IMP_ADER_NULL);
    }

    public String getWtgaImpAderNullFormatted() {
        return Functions.padBlanks(getWtgaImpAderNull(), Len.WTGA_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_ADER = 1;
        public static final int WTGA_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_ADER = 8;
        public static final int WTGA_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
