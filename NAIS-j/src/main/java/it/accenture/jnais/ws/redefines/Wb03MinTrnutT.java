package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-MIN-TRNUT-T<br>
 * Variable: WB03-MIN-TRNUT-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03MinTrnutT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03MinTrnutT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_MIN_TRNUT_T;
    }

    public void setWb03MinTrnutTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_MIN_TRNUT_T, Pos.WB03_MIN_TRNUT_T);
    }

    /**Original name: WB03-MIN-TRNUT-T<br>*/
    public AfDecimal getWb03MinTrnutT() {
        return readPackedAsDecimal(Pos.WB03_MIN_TRNUT_T, Len.Int.WB03_MIN_TRNUT_T, Len.Fract.WB03_MIN_TRNUT_T);
    }

    public byte[] getWb03MinTrnutTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_MIN_TRNUT_T, Pos.WB03_MIN_TRNUT_T);
        return buffer;
    }

    public void setWb03MinTrnutTNull(String wb03MinTrnutTNull) {
        writeString(Pos.WB03_MIN_TRNUT_T_NULL, wb03MinTrnutTNull, Len.WB03_MIN_TRNUT_T_NULL);
    }

    /**Original name: WB03-MIN-TRNUT-T-NULL<br>*/
    public String getWb03MinTrnutTNull() {
        return readString(Pos.WB03_MIN_TRNUT_T_NULL, Len.WB03_MIN_TRNUT_T_NULL);
    }

    public String getWb03MinTrnutTNullFormatted() {
        return Functions.padBlanks(getWb03MinTrnutTNull(), Len.WB03_MIN_TRNUT_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_MIN_TRNUT_T = 1;
        public static final int WB03_MIN_TRNUT_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_MIN_TRNUT_T = 8;
        public static final int WB03_MIN_TRNUT_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_MIN_TRNUT_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_MIN_TRNUT_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
