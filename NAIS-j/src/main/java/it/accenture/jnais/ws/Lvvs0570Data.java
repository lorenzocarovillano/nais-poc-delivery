package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0570<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0570Data {

    //==== PROPERTIES ====
    public static final int DRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    //Original name: AREA-CALL-IWFS0050
    private AreaCallIwfs0050 areaCallIwfs0050 = new AreaCallIwfs0050();
    //Original name: DRAN-ELE-RAN-MAX
    private short dranEleRanMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] dranTabRappAnag = new WranTabRappAnag[DRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0570Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dranTabRappAnagIdx = 1; dranTabRappAnagIdx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; dranTabRappAnagIdx++) {
            dranTabRappAnag[dranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public void setDranAreaRappAnaFormatted(String data) {
        byte[] buffer = new byte[Len.DRAN_AREA_RAPP_ANA];
        MarshalByte.writeString(buffer, 1, data, Len.DRAN_AREA_RAPP_ANA);
        setDranAreaRappAnaBytes(buffer, 1);
    }

    public void setDranAreaRappAnaBytes(byte[] buffer, int offset) {
        int position = offset;
        dranEleRanMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                dranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public void setDranEleRanMax(short dranEleRanMax) {
        this.dranEleRanMax = dranEleRanMax;
    }

    public short getDranEleRanMax() {
        return this.dranEleRanMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public AreaCallIwfs0050 getAreaCallIwfs0050() {
        return areaCallIwfs0050;
    }

    public WranTabRappAnag getDranTabRappAnag(int idx) {
        return dranTabRappAnag[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DRAN_ELE_RAN_MAX = 2;
        public static final int DRAN_AREA_RAPP_ANA = DRAN_ELE_RAN_MAX + Lvvs0570Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
