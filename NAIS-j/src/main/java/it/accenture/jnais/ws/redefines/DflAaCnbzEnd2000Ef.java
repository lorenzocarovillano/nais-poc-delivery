package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-AA-CNBZ-END2000-EF<br>
 * Variable: DFL-AA-CNBZ-END2000-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAaCnbzEnd2000Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAaCnbzEnd2000Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_AA_CNBZ_END2000_EF;
    }

    public void setDflAaCnbzEnd2000Ef(int dflAaCnbzEnd2000Ef) {
        writeIntAsPacked(Pos.DFL_AA_CNBZ_END2000_EF, dflAaCnbzEnd2000Ef, Len.Int.DFL_AA_CNBZ_END2000_EF);
    }

    public void setDflAaCnbzEnd2000EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_AA_CNBZ_END2000_EF, Pos.DFL_AA_CNBZ_END2000_EF);
    }

    /**Original name: DFL-AA-CNBZ-END2000-EF<br>*/
    public int getDflAaCnbzEnd2000Ef() {
        return readPackedAsInt(Pos.DFL_AA_CNBZ_END2000_EF, Len.Int.DFL_AA_CNBZ_END2000_EF);
    }

    public byte[] getDflAaCnbzEnd2000EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_AA_CNBZ_END2000_EF, Pos.DFL_AA_CNBZ_END2000_EF);
        return buffer;
    }

    public void setDflAaCnbzEnd2000EfNull(String dflAaCnbzEnd2000EfNull) {
        writeString(Pos.DFL_AA_CNBZ_END2000_EF_NULL, dflAaCnbzEnd2000EfNull, Len.DFL_AA_CNBZ_END2000_EF_NULL);
    }

    /**Original name: DFL-AA-CNBZ-END2000-EF-NULL<br>*/
    public String getDflAaCnbzEnd2000EfNull() {
        return readString(Pos.DFL_AA_CNBZ_END2000_EF_NULL, Len.DFL_AA_CNBZ_END2000_EF_NULL);
    }

    public String getDflAaCnbzEnd2000EfNullFormatted() {
        return Functions.padBlanks(getDflAaCnbzEnd2000EfNull(), Len.DFL_AA_CNBZ_END2000_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_AA_CNBZ_END2000_EF = 1;
        public static final int DFL_AA_CNBZ_END2000_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_AA_CNBZ_END2000_EF = 3;
        public static final int DFL_AA_CNBZ_END2000_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_AA_CNBZ_END2000_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
