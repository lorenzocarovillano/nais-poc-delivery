package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-DIR<br>
 * Variable: WDTC-DIR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_DIR;
    }

    public void setWdtcDir(AfDecimal wdtcDir) {
        writeDecimalAsPacked(Pos.WDTC_DIR, wdtcDir.copy());
    }

    public void setWdtcDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_DIR, Pos.WDTC_DIR);
    }

    /**Original name: WDTC-DIR<br>*/
    public AfDecimal getWdtcDir() {
        return readPackedAsDecimal(Pos.WDTC_DIR, Len.Int.WDTC_DIR, Len.Fract.WDTC_DIR);
    }

    public byte[] getWdtcDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_DIR, Pos.WDTC_DIR);
        return buffer;
    }

    public void initWdtcDirSpaces() {
        fill(Pos.WDTC_DIR, Len.WDTC_DIR, Types.SPACE_CHAR);
    }

    public void setWdtcDirNull(String wdtcDirNull) {
        writeString(Pos.WDTC_DIR_NULL, wdtcDirNull, Len.WDTC_DIR_NULL);
    }

    /**Original name: WDTC-DIR-NULL<br>*/
    public String getWdtcDirNull() {
        return readString(Pos.WDTC_DIR_NULL, Len.WDTC_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_DIR = 1;
        public static final int WDTC_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_DIR = 8;
        public static final int WDTC_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
