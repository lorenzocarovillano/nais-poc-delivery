package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-COS-AT<br>
 * Variable: WPCO-DT-ULT-ELAB-COS-AT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabCosAt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabCosAt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_COS_AT;
    }

    public void setWpcoDtUltElabCosAt(int wpcoDtUltElabCosAt) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_COS_AT, wpcoDtUltElabCosAt, Len.Int.WPCO_DT_ULT_ELAB_COS_AT);
    }

    public void setDpcoDtUltElabCosAtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_COS_AT, Pos.WPCO_DT_ULT_ELAB_COS_AT);
    }

    /**Original name: WPCO-DT-ULT-ELAB-COS-AT<br>*/
    public int getWpcoDtUltElabCosAt() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_COS_AT, Len.Int.WPCO_DT_ULT_ELAB_COS_AT);
    }

    public byte[] getWpcoDtUltElabCosAtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_COS_AT, Pos.WPCO_DT_ULT_ELAB_COS_AT);
        return buffer;
    }

    public void setWpcoDtUltElabCosAtNull(String wpcoDtUltElabCosAtNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_COS_AT_NULL, wpcoDtUltElabCosAtNull, Len.WPCO_DT_ULT_ELAB_COS_AT_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-COS-AT-NULL<br>*/
    public String getWpcoDtUltElabCosAtNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_COS_AT_NULL, Len.WPCO_DT_ULT_ELAB_COS_AT_NULL);
    }

    public String getWpcoDtUltElabCosAtNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabCosAtNull(), Len.WPCO_DT_ULT_ELAB_COS_AT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_COS_AT = 1;
        public static final int WPCO_DT_ULT_ELAB_COS_AT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_COS_AT = 5;
        public static final int WPCO_DT_ULT_ELAB_COS_AT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_COS_AT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
