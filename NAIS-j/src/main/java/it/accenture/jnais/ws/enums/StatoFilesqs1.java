package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: STATO-FILESQS1<br>
 * Variable: STATO-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class StatoFilesqs1 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char APERTO = 'A';
    public static final char CHIUSO = 'C';

    //==== METHODS ====
    public void setStatoFilesqs1(char statoFilesqs1) {
        this.value = statoFilesqs1;
    }

    public char getStatoFilesqs1() {
        return this.value;
    }

    public boolean isAperto() {
        return value == APERTO;
    }

    public void setAperto() {
        value = APERTO;
    }

    public void setChiuso() {
        value = CHIUSO;
    }
}
