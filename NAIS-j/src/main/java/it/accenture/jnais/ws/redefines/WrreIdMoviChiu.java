package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-ID-MOVI-CHIU<br>
 * Variable: WRRE-ID-MOVI-CHIU from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_ID_MOVI_CHIU;
    }

    public void setWrreIdMoviChiu(int wrreIdMoviChiu) {
        writeIntAsPacked(Pos.WRRE_ID_MOVI_CHIU, wrreIdMoviChiu, Len.Int.WRRE_ID_MOVI_CHIU);
    }

    public void setWrreIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_ID_MOVI_CHIU, Pos.WRRE_ID_MOVI_CHIU);
    }

    /**Original name: WRRE-ID-MOVI-CHIU<br>*/
    public int getWrreIdMoviChiu() {
        return readPackedAsInt(Pos.WRRE_ID_MOVI_CHIU, Len.Int.WRRE_ID_MOVI_CHIU);
    }

    public byte[] getWrreIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_ID_MOVI_CHIU, Pos.WRRE_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWrreIdMoviChiuSpaces() {
        fill(Pos.WRRE_ID_MOVI_CHIU, Len.WRRE_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWrreIdMoviChiuNull(String wrreIdMoviChiuNull) {
        writeString(Pos.WRRE_ID_MOVI_CHIU_NULL, wrreIdMoviChiuNull, Len.WRRE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WRRE-ID-MOVI-CHIU-NULL<br>*/
    public String getWrreIdMoviChiuNull() {
        return readString(Pos.WRRE_ID_MOVI_CHIU_NULL, Len.WRRE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_ID_MOVI_CHIU = 1;
        public static final int WRRE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_ID_MOVI_CHIU = 5;
        public static final int WRRE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
