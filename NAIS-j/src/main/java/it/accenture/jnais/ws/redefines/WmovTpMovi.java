package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WMOV-TP-MOVI<br>
 * Variable: WMOV-TP-MOVI from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmovTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmovTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMOV_TP_MOVI;
    }

    public void setWmovTpMovi(int wmovTpMovi) {
        writeIntAsPacked(Pos.WMOV_TP_MOVI, wmovTpMovi, Len.Int.WMOV_TP_MOVI);
    }

    public void setWmovTpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMOV_TP_MOVI, Pos.WMOV_TP_MOVI);
    }

    /**Original name: WMOV-TP-MOVI<br>*/
    public int getWmovTpMovi() {
        return readPackedAsInt(Pos.WMOV_TP_MOVI, Len.Int.WMOV_TP_MOVI);
    }

    public byte[] getWmovTpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMOV_TP_MOVI, Pos.WMOV_TP_MOVI);
        return buffer;
    }

    public void setWmovTpMoviNull(String wmovTpMoviNull) {
        writeString(Pos.WMOV_TP_MOVI_NULL, wmovTpMoviNull, Len.WMOV_TP_MOVI_NULL);
    }

    /**Original name: WMOV-TP-MOVI-NULL<br>*/
    public String getWmovTpMoviNull() {
        return readString(Pos.WMOV_TP_MOVI_NULL, Len.WMOV_TP_MOVI_NULL);
    }

    public String getWmovTpMoviNullFormatted() {
        return Functions.padBlanks(getWmovTpMoviNull(), Len.WMOV_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMOV_TP_MOVI = 1;
        public static final int WMOV_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_TP_MOVI = 3;
        public static final int WMOV_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMOV_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
