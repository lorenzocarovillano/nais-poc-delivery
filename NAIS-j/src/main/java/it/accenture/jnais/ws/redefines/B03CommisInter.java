package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COMMIS-INTER<br>
 * Variable: B03-COMMIS-INTER from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COMMIS_INTER;
    }

    public void setB03CommisInter(AfDecimal b03CommisInter) {
        writeDecimalAsPacked(Pos.B03_COMMIS_INTER, b03CommisInter.copy());
    }

    public void setB03CommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COMMIS_INTER, Pos.B03_COMMIS_INTER);
    }

    /**Original name: B03-COMMIS-INTER<br>*/
    public AfDecimal getB03CommisInter() {
        return readPackedAsDecimal(Pos.B03_COMMIS_INTER, Len.Int.B03_COMMIS_INTER, Len.Fract.B03_COMMIS_INTER);
    }

    public byte[] getB03CommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COMMIS_INTER, Pos.B03_COMMIS_INTER);
        return buffer;
    }

    public void setB03CommisInterNull(String b03CommisInterNull) {
        writeString(Pos.B03_COMMIS_INTER_NULL, b03CommisInterNull, Len.B03_COMMIS_INTER_NULL);
    }

    /**Original name: B03-COMMIS-INTER-NULL<br>*/
    public String getB03CommisInterNull() {
        return readString(Pos.B03_COMMIS_INTER_NULL, Len.B03_COMMIS_INTER_NULL);
    }

    public String getB03CommisInterNullFormatted() {
        return Functions.padBlanks(getB03CommisInterNull(), Len.B03_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COMMIS_INTER = 1;
        public static final int B03_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COMMIS_INTER = 8;
        public static final int B03_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
