package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-INTR-PREST-EFFLQ<br>
 * Variable: WDFL-INTR-PREST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIntrPrestEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIntrPrestEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_INTR_PREST_EFFLQ;
    }

    public void setWdflIntrPrestEfflq(AfDecimal wdflIntrPrestEfflq) {
        writeDecimalAsPacked(Pos.WDFL_INTR_PREST_EFFLQ, wdflIntrPrestEfflq.copy());
    }

    public void setWdflIntrPrestEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_INTR_PREST_EFFLQ, Pos.WDFL_INTR_PREST_EFFLQ);
    }

    /**Original name: WDFL-INTR-PREST-EFFLQ<br>*/
    public AfDecimal getWdflIntrPrestEfflq() {
        return readPackedAsDecimal(Pos.WDFL_INTR_PREST_EFFLQ, Len.Int.WDFL_INTR_PREST_EFFLQ, Len.Fract.WDFL_INTR_PREST_EFFLQ);
    }

    public byte[] getWdflIntrPrestEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_INTR_PREST_EFFLQ, Pos.WDFL_INTR_PREST_EFFLQ);
        return buffer;
    }

    public void setWdflIntrPrestEfflqNull(String wdflIntrPrestEfflqNull) {
        writeString(Pos.WDFL_INTR_PREST_EFFLQ_NULL, wdflIntrPrestEfflqNull, Len.WDFL_INTR_PREST_EFFLQ_NULL);
    }

    /**Original name: WDFL-INTR-PREST-EFFLQ-NULL<br>*/
    public String getWdflIntrPrestEfflqNull() {
        return readString(Pos.WDFL_INTR_PREST_EFFLQ_NULL, Len.WDFL_INTR_PREST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_INTR_PREST_EFFLQ = 1;
        public static final int WDFL_INTR_PREST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_INTR_PREST_EFFLQ = 8;
        public static final int WDFL_INTR_PREST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_INTR_PREST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_INTR_PREST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
