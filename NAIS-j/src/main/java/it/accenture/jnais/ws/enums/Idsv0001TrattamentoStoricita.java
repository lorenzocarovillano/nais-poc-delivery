package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0001-TRATTAMENTO-STORICITA<br>
 * Variable: IDSV0001-TRATTAMENTO-STORICITA from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001TrattamentoStoricita {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TRATTAMENTO_STORICITA);
    public static final String EFFETTO = "EFF";
    public static final String COMPETENZA = "CPZ";

    //==== METHODS ====
    public void setTrattamentoStoricita(String trattamentoStoricita) {
        this.value = Functions.subString(trattamentoStoricita, Len.TRATTAMENTO_STORICITA);
    }

    public String getTrattamentoStoricita() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TRATTAMENTO_STORICITA = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
