package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-COMMIS-INTER<br>
 * Variable: WPVT-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_COMMIS_INTER;
    }

    public void setWpvtCommisInter(AfDecimal wpvtCommisInter) {
        writeDecimalAsPacked(Pos.WPVT_COMMIS_INTER, wpvtCommisInter.copy());
    }

    public void setWpvtCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_COMMIS_INTER, Pos.WPVT_COMMIS_INTER);
    }

    /**Original name: WPVT-COMMIS-INTER<br>*/
    public AfDecimal getWpvtCommisInter() {
        return readPackedAsDecimal(Pos.WPVT_COMMIS_INTER, Len.Int.WPVT_COMMIS_INTER, Len.Fract.WPVT_COMMIS_INTER);
    }

    public byte[] getWpvtCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_COMMIS_INTER, Pos.WPVT_COMMIS_INTER);
        return buffer;
    }

    public void initWpvtCommisInterSpaces() {
        fill(Pos.WPVT_COMMIS_INTER, Len.WPVT_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWpvtCommisInterNull(String wpvtCommisInterNull) {
        writeString(Pos.WPVT_COMMIS_INTER_NULL, wpvtCommisInterNull, Len.WPVT_COMMIS_INTER_NULL);
    }

    /**Original name: WPVT-COMMIS-INTER-NULL<br>*/
    public String getWpvtCommisInterNull() {
        return readString(Pos.WPVT_COMMIS_INTER_NULL, Len.WPVT_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_COMMIS_INTER = 1;
        public static final int WPVT_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_COMMIS_INTER = 8;
        public static final int WPVT_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
