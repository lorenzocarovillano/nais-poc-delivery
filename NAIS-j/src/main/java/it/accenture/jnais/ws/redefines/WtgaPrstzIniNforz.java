package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-INI-NFORZ<br>
 * Variable: WTGA-PRSTZ-INI-NFORZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzIniNforz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzIniNforz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_INI_NFORZ;
    }

    public void setWtgaPrstzIniNforz(AfDecimal wtgaPrstzIniNforz) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_INI_NFORZ, wtgaPrstzIniNforz.copy());
    }

    public void setWtgaPrstzIniNforzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_INI_NFORZ, Pos.WTGA_PRSTZ_INI_NFORZ);
    }

    /**Original name: WTGA-PRSTZ-INI-NFORZ<br>*/
    public AfDecimal getWtgaPrstzIniNforz() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_INI_NFORZ, Len.Int.WTGA_PRSTZ_INI_NFORZ, Len.Fract.WTGA_PRSTZ_INI_NFORZ);
    }

    public byte[] getWtgaPrstzIniNforzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_INI_NFORZ, Pos.WTGA_PRSTZ_INI_NFORZ);
        return buffer;
    }

    public void initWtgaPrstzIniNforzSpaces() {
        fill(Pos.WTGA_PRSTZ_INI_NFORZ, Len.WTGA_PRSTZ_INI_NFORZ, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzIniNforzNull(String wtgaPrstzIniNforzNull) {
        writeString(Pos.WTGA_PRSTZ_INI_NFORZ_NULL, wtgaPrstzIniNforzNull, Len.WTGA_PRSTZ_INI_NFORZ_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-NFORZ-NULL<br>*/
    public String getWtgaPrstzIniNforzNull() {
        return readString(Pos.WTGA_PRSTZ_INI_NFORZ_NULL, Len.WTGA_PRSTZ_INI_NFORZ_NULL);
    }

    public String getWtgaPrstzIniNforzNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzIniNforzNull(), Len.WTGA_PRSTZ_INI_NFORZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI_NFORZ = 1;
        public static final int WTGA_PRSTZ_INI_NFORZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI_NFORZ = 8;
        public static final int WTGA_PRSTZ_INI_NFORZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI_NFORZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI_NFORZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
