package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-RIT-ACC-EFFLQ<br>
 * Variable: WDFL-IMPB-RIT-ACC-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbRitAccEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbRitAccEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_RIT_ACC_EFFLQ;
    }

    public void setWdflImpbRitAccEfflq(AfDecimal wdflImpbRitAccEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_RIT_ACC_EFFLQ, wdflImpbRitAccEfflq.copy());
    }

    public void setWdflImpbRitAccEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_RIT_ACC_EFFLQ, Pos.WDFL_IMPB_RIT_ACC_EFFLQ);
    }

    /**Original name: WDFL-IMPB-RIT-ACC-EFFLQ<br>*/
    public AfDecimal getWdflImpbRitAccEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_RIT_ACC_EFFLQ, Len.Int.WDFL_IMPB_RIT_ACC_EFFLQ, Len.Fract.WDFL_IMPB_RIT_ACC_EFFLQ);
    }

    public byte[] getWdflImpbRitAccEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_RIT_ACC_EFFLQ, Pos.WDFL_IMPB_RIT_ACC_EFFLQ);
        return buffer;
    }

    public void setWdflImpbRitAccEfflqNull(String wdflImpbRitAccEfflqNull) {
        writeString(Pos.WDFL_IMPB_RIT_ACC_EFFLQ_NULL, wdflImpbRitAccEfflqNull, Len.WDFL_IMPB_RIT_ACC_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPB-RIT-ACC-EFFLQ-NULL<br>*/
    public String getWdflImpbRitAccEfflqNull() {
        return readString(Pos.WDFL_IMPB_RIT_ACC_EFFLQ_NULL, Len.WDFL_IMPB_RIT_ACC_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_RIT_ACC_EFFLQ = 1;
        public static final int WDFL_IMPB_RIT_ACC_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_RIT_ACC_EFFLQ = 8;
        public static final int WDFL_IMPB_RIT_ACC_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_RIT_ACC_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_RIT_ACC_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
