package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvlqu1Lvvs0009;
import it.accenture.jnais.copy.S089Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0009<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0009Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0009";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DLQU-ELE-LIQ-MAX
    private short dlquEleLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVLQU1
    private Lccvlqu1Lvvs0009 lccvlqu1 = new Lccvlqu1Lvvs0009();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDlquAreaLiquidazioneFormatted(String data) {
        byte[] buffer = new byte[Len.DLQU_AREA_LIQUIDAZIONE];
        MarshalByte.writeString(buffer, 1, data, Len.DLQU_AREA_LIQUIDAZIONE);
        setDlquAreaLiquidazioneBytes(buffer, 1);
    }

    public void setDlquAreaLiquidazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        dlquEleLiqMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDlquTabLiqBytes(buffer, position);
    }

    public void setDlquEleLiqMax(short dlquEleLiqMax) {
        this.dlquEleLiqMax = dlquEleLiqMax;
    }

    public short getDlquEleLiqMax() {
        return this.dlquEleLiqMax;
    }

    public void setDlquTabLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvlqu1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvlqu1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvlqu1Lvvs0009.Len.Int.ID_PTF, 0));
        position += Lccvlqu1Lvvs0009.Len.ID_PTF;
        lccvlqu1.getDati().setWlquDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvlqu1Lvvs0009 getLccvlqu1() {
        return lccvlqu1;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DLQU_ELE_LIQ_MAX = 2;
        public static final int DLQU_TAB_LIQ = WpolStatus.Len.STATUS + Lccvlqu1Lvvs0009.Len.ID_PTF + S089Dati.Len.WLQU_DATI;
        public static final int DLQU_AREA_LIQUIDAZIONE = DLQU_ELE_LIQ_MAX + DLQU_TAB_LIQ;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
