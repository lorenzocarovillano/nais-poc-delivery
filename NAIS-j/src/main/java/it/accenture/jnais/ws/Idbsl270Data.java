package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvl272;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSL270<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsl270Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IDBVL272
    private Idbvl272 idbvl272 = new Idbvl272();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvl272 getIdbvl272() {
        return idbvl272;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
