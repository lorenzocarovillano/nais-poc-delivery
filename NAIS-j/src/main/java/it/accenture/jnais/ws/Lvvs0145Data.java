package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.S089TabLiq;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0145<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0145Data {

    //==== PROPERTIES ====
    public static final int DLQU_TAB_LIQ_MAXOCCURS = 30;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0145";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: AREA-LDBV2271
    private AreaLdbv2271 areaLdbv2271 = new AreaLdbv2271();
    //Original name: DLQU-ELE-LIQ-MAX
    private short dlquEleLiqMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DLQU-TAB-LIQ
    private S089TabLiq[] dlquTabLiq = new S089TabLiq[DLQU_TAB_LIQ_MAXOCCURS];
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0145Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dlquTabLiqIdx = 1; dlquTabLiqIdx <= DLQU_TAB_LIQ_MAXOCCURS; dlquTabLiqIdx++) {
            dlquTabLiq[dlquTabLiqIdx - 1] = new S089TabLiq();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setDlquEleLiqMax(short dlquEleLiqMax) {
        this.dlquEleLiqMax = dlquEleLiqMax;
    }

    public short getDlquEleLiqMax() {
        return this.dlquEleLiqMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public AreaLdbv2271 getAreaLdbv2271() {
        return areaLdbv2271;
    }

    public S089TabLiq getDlquTabLiq(int idx) {
        return dlquTabLiq[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
