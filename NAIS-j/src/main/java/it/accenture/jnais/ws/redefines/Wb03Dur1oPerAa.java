package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-1O-PER-AA<br>
 * Variable: WB03-DUR-1O-PER-AA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Dur1oPerAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Dur1oPerAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR1O_PER_AA;
    }

    public void setWb03Dur1oPerAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR1O_PER_AA, Pos.WB03_DUR1O_PER_AA);
    }

    /**Original name: WB03-DUR-1O-PER-AA<br>*/
    public int getWb03Dur1oPerAa() {
        return readPackedAsInt(Pos.WB03_DUR1O_PER_AA, Len.Int.WB03_DUR1O_PER_AA);
    }

    public byte[] getWb03Dur1oPerAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR1O_PER_AA, Pos.WB03_DUR1O_PER_AA);
        return buffer;
    }

    public void setWb03Dur1oPerAaNull(String wb03Dur1oPerAaNull) {
        writeString(Pos.WB03_DUR1O_PER_AA_NULL, wb03Dur1oPerAaNull, Len.WB03_DUR1O_PER_AA_NULL);
    }

    /**Original name: WB03-DUR-1O-PER-AA-NULL<br>*/
    public String getWb03Dur1oPerAaNull() {
        return readString(Pos.WB03_DUR1O_PER_AA_NULL, Len.WB03_DUR1O_PER_AA_NULL);
    }

    public String getWb03Dur1oPerAaNullFormatted() {
        return Functions.padBlanks(getWb03Dur1oPerAaNull(), Len.WB03_DUR1O_PER_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR1O_PER_AA = 1;
        public static final int WB03_DUR1O_PER_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR1O_PER_AA = 3;
        public static final int WB03_DUR1O_PER_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR1O_PER_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
