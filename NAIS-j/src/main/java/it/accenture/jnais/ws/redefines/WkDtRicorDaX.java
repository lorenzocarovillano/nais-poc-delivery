package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-DT-RICOR-DA-X<br>
 * Variable: WK-DT-RICOR-DA-X from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDtRicorDaX extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDtRicorDaX() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DT_RICOR_DA_X;
    }

    public void setWkDtRicorDaX(String wkDtRicorDaX) {
        writeString(Pos.WK_DT_RICOR_DA_X, wkDtRicorDaX, Len.WK_DT_RICOR_DA_X);
    }

    /**Original name: WK-DT-RICOR-DA-X<br>*/
    public String getWkDtRicorDaX() {
        return readString(Pos.WK_DT_RICOR_DA_X, Len.WK_DT_RICOR_DA_X);
    }

    public String getWkDtRicorDaFormatted() {
        return readFixedString(Pos.WK_DT_RICOR_DA, Len.WK_DT_RICOR_DA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DT_RICOR_DA_X = 1;
        public static final int WK_DT_RICOR_DA = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DT_RICOR_DA_X = 8;
        public static final int WK_DT_RICOR_DA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
