package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-LIQMEF<br>
 * Variable: WPCO-DT-ULT-ELAB-LIQMEF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabLiqmef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabLiqmef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_LIQMEF;
    }

    public void setWpcoDtUltElabLiqmef(int wpcoDtUltElabLiqmef) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_LIQMEF, wpcoDtUltElabLiqmef, Len.Int.WPCO_DT_ULT_ELAB_LIQMEF);
    }

    public void setDpcoDtUltElabLiqmefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_LIQMEF, Pos.WPCO_DT_ULT_ELAB_LIQMEF);
    }

    /**Original name: WPCO-DT-ULT-ELAB-LIQMEF<br>*/
    public int getWpcoDtUltElabLiqmef() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_LIQMEF, Len.Int.WPCO_DT_ULT_ELAB_LIQMEF);
    }

    public byte[] getWpcoDtUltElabLiqmefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_LIQMEF, Pos.WPCO_DT_ULT_ELAB_LIQMEF);
        return buffer;
    }

    public void setWpcoDtUltElabLiqmefNull(String wpcoDtUltElabLiqmefNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_LIQMEF_NULL, wpcoDtUltElabLiqmefNull, Len.WPCO_DT_ULT_ELAB_LIQMEF_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-LIQMEF-NULL<br>*/
    public String getWpcoDtUltElabLiqmefNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_LIQMEF_NULL, Len.WPCO_DT_ULT_ELAB_LIQMEF_NULL);
    }

    public String getWpcoDtUltElabLiqmefNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabLiqmefNull(), Len.WPCO_DT_ULT_ELAB_LIQMEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_LIQMEF = 1;
        public static final int WPCO_DT_ULT_ELAB_LIQMEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_LIQMEF = 5;
        public static final int WPCO_DT_ULT_ELAB_LIQMEF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_LIQMEF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
