package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-VAL-AST<br>
 * Variable: SW-VAL-AST from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwValAst {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_VAL_AST = 'S';
    public static final char NO_VAL_AST = 'N';

    //==== METHODS ====
    public void setSwValAst(char swValAst) {
        this.value = swValAst;
    }

    public char getSwValAst() {
        return this.value;
    }

    public boolean isSiValAst() {
        return value == SI_VAL_AST;
    }

    public void setSiValAst() {
        value = SI_VAL_AST;
    }

    public void setNoValAst() {
        value = NO_VAL_AST;
    }
}
