package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-INI-STAB<br>
 * Variable: WTGA-PRSTZ-INI-STAB from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzIniStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzIniStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_INI_STAB;
    }

    public void setWtgaPrstzIniStab(AfDecimal wtgaPrstzIniStab) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_INI_STAB, wtgaPrstzIniStab.copy());
    }

    public void setWtgaPrstzIniStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_INI_STAB, Pos.WTGA_PRSTZ_INI_STAB);
    }

    /**Original name: WTGA-PRSTZ-INI-STAB<br>*/
    public AfDecimal getWtgaPrstzIniStab() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_INI_STAB, Len.Int.WTGA_PRSTZ_INI_STAB, Len.Fract.WTGA_PRSTZ_INI_STAB);
    }

    public byte[] getWtgaPrstzIniStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_INI_STAB, Pos.WTGA_PRSTZ_INI_STAB);
        return buffer;
    }

    public void initWtgaPrstzIniStabSpaces() {
        fill(Pos.WTGA_PRSTZ_INI_STAB, Len.WTGA_PRSTZ_INI_STAB, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzIniStabNull(String wtgaPrstzIniStabNull) {
        writeString(Pos.WTGA_PRSTZ_INI_STAB_NULL, wtgaPrstzIniStabNull, Len.WTGA_PRSTZ_INI_STAB_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-STAB-NULL<br>*/
    public String getWtgaPrstzIniStabNull() {
        return readString(Pos.WTGA_PRSTZ_INI_STAB_NULL, Len.WTGA_PRSTZ_INI_STAB_NULL);
    }

    public String getWtgaPrstzIniStabNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzIniStabNull(), Len.WTGA_PRSTZ_INI_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI_STAB = 1;
        public static final int WTGA_PRSTZ_INI_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI_STAB = 8;
        public static final int WTGA_PRSTZ_INI_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
