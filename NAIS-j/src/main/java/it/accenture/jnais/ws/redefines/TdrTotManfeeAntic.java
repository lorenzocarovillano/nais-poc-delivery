package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-MANFEE-ANTIC<br>
 * Variable: TDR-TOT-MANFEE-ANTIC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_MANFEE_ANTIC;
    }

    public void setTdrTotManfeeAntic(AfDecimal tdrTotManfeeAntic) {
        writeDecimalAsPacked(Pos.TDR_TOT_MANFEE_ANTIC, tdrTotManfeeAntic.copy());
    }

    public void setTdrTotManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_MANFEE_ANTIC, Pos.TDR_TOT_MANFEE_ANTIC);
    }

    /**Original name: TDR-TOT-MANFEE-ANTIC<br>*/
    public AfDecimal getTdrTotManfeeAntic() {
        return readPackedAsDecimal(Pos.TDR_TOT_MANFEE_ANTIC, Len.Int.TDR_TOT_MANFEE_ANTIC, Len.Fract.TDR_TOT_MANFEE_ANTIC);
    }

    public byte[] getTdrTotManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_MANFEE_ANTIC, Pos.TDR_TOT_MANFEE_ANTIC);
        return buffer;
    }

    public void setTdrTotManfeeAnticNull(String tdrTotManfeeAnticNull) {
        writeString(Pos.TDR_TOT_MANFEE_ANTIC_NULL, tdrTotManfeeAnticNull, Len.TDR_TOT_MANFEE_ANTIC_NULL);
    }

    /**Original name: TDR-TOT-MANFEE-ANTIC-NULL<br>*/
    public String getTdrTotManfeeAnticNull() {
        return readString(Pos.TDR_TOT_MANFEE_ANTIC_NULL, Len.TDR_TOT_MANFEE_ANTIC_NULL);
    }

    public String getTdrTotManfeeAnticNullFormatted() {
        return Functions.padBlanks(getTdrTotManfeeAnticNull(), Len.TDR_TOT_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_MANFEE_ANTIC = 1;
        public static final int TDR_TOT_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_MANFEE_ANTIC = 8;
        public static final int TDR_TOT_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
