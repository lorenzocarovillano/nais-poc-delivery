package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccve151;
import it.accenture.jnais.copy.We15Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WE15-TAB-E15<br>
 * Variables: WE15-TAB-E15 from copybook LCCVE15A<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class We15TabE15 {

    //==== PROPERTIES ====
    //Original name: LCCVE151
    private Lccve151 lccve151 = new Lccve151();

    //==== METHODS ====
    public void setWe15TabE15Bytes(byte[] buffer, int offset) {
        int position = offset;
        lccve151.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccve151.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccve151.Len.Int.ID_PTF, 0));
        position += Lccve151.Len.ID_PTF;
        lccve151.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWe15TabE15Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccve151.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccve151.getIdPtf(), Lccve151.Len.Int.ID_PTF, 0);
        position += Lccve151.Len.ID_PTF;
        lccve151.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWe15TabE15Spaces() {
        lccve151.initLccve151Spaces();
    }

    public Lccve151 getLccve151() {
        return lccve151;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WE15_TAB_E15 = WpolStatus.Len.STATUS + Lccve151.Len.ID_PTF + We15Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
