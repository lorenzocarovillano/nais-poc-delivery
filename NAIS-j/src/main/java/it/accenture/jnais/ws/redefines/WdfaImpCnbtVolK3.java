package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-CNBT-VOL-K3<br>
 * Variable: WDFA-IMP-CNBT-VOL-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpCnbtVolK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpCnbtVolK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_CNBT_VOL_K3;
    }

    public void setWdfaImpCnbtVolK3(AfDecimal wdfaImpCnbtVolK3) {
        writeDecimalAsPacked(Pos.WDFA_IMP_CNBT_VOL_K3, wdfaImpCnbtVolK3.copy());
    }

    public void setWdfaImpCnbtVolK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_CNBT_VOL_K3, Pos.WDFA_IMP_CNBT_VOL_K3);
    }

    /**Original name: WDFA-IMP-CNBT-VOL-K3<br>*/
    public AfDecimal getWdfaImpCnbtVolK3() {
        return readPackedAsDecimal(Pos.WDFA_IMP_CNBT_VOL_K3, Len.Int.WDFA_IMP_CNBT_VOL_K3, Len.Fract.WDFA_IMP_CNBT_VOL_K3);
    }

    public byte[] getWdfaImpCnbtVolK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_CNBT_VOL_K3, Pos.WDFA_IMP_CNBT_VOL_K3);
        return buffer;
    }

    public void setWdfaImpCnbtVolK3Null(String wdfaImpCnbtVolK3Null) {
        writeString(Pos.WDFA_IMP_CNBT_VOL_K3_NULL, wdfaImpCnbtVolK3Null, Len.WDFA_IMP_CNBT_VOL_K3_NULL);
    }

    /**Original name: WDFA-IMP-CNBT-VOL-K3-NULL<br>*/
    public String getWdfaImpCnbtVolK3Null() {
        return readString(Pos.WDFA_IMP_CNBT_VOL_K3_NULL, Len.WDFA_IMP_CNBT_VOL_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_VOL_K3 = 1;
        public static final int WDFA_IMP_CNBT_VOL_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_VOL_K3 = 8;
        public static final int WDFA_IMP_CNBT_VOL_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_VOL_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_VOL_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
