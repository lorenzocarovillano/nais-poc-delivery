package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-IRPEF<br>
 * Variable: LQU-IMPB-IRPEF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbIrpef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbIrpef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_IRPEF;
    }

    public void setLquImpbIrpef(AfDecimal lquImpbIrpef) {
        writeDecimalAsPacked(Pos.LQU_IMPB_IRPEF, lquImpbIrpef.copy());
    }

    public void setLquImpbIrpefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_IRPEF, Pos.LQU_IMPB_IRPEF);
    }

    /**Original name: LQU-IMPB-IRPEF<br>*/
    public AfDecimal getLquImpbIrpef() {
        return readPackedAsDecimal(Pos.LQU_IMPB_IRPEF, Len.Int.LQU_IMPB_IRPEF, Len.Fract.LQU_IMPB_IRPEF);
    }

    public byte[] getLquImpbIrpefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_IRPEF, Pos.LQU_IMPB_IRPEF);
        return buffer;
    }

    public void setLquImpbIrpefNull(String lquImpbIrpefNull) {
        writeString(Pos.LQU_IMPB_IRPEF_NULL, lquImpbIrpefNull, Len.LQU_IMPB_IRPEF_NULL);
    }

    /**Original name: LQU-IMPB-IRPEF-NULL<br>*/
    public String getLquImpbIrpefNull() {
        return readString(Pos.LQU_IMPB_IRPEF_NULL, Len.LQU_IMPB_IRPEF_NULL);
    }

    public String getLquImpbIrpefNullFormatted() {
        return Functions.padBlanks(getLquImpbIrpefNull(), Len.LQU_IMPB_IRPEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IRPEF = 1;
        public static final int LQU_IMPB_IRPEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_IRPEF = 8;
        public static final int LQU_IMPB_IRPEF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IRPEF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_IRPEF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
