package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-SOPR-PROFES<br>
 * Variable: WPAG-CONT-SOPR-PROFES from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContSoprProfes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContSoprProfes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_SOPR_PROFES;
    }

    public void setWpagContSoprProfes(AfDecimal wpagContSoprProfes) {
        writeDecimalAsPacked(Pos.WPAG_CONT_SOPR_PROFES, wpagContSoprProfes.copy());
    }

    public void setWpagContSoprProfesFormatted(String wpagContSoprProfes) {
        setWpagContSoprProfes(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_SOPR_PROFES + Len.Fract.WPAG_CONT_SOPR_PROFES, Len.Fract.WPAG_CONT_SOPR_PROFES, wpagContSoprProfes));
    }

    public void setWpagContSoprProfesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_SOPR_PROFES, Pos.WPAG_CONT_SOPR_PROFES);
    }

    /**Original name: WPAG-CONT-SOPR-PROFES<br>*/
    public AfDecimal getWpagContSoprProfes() {
        return readPackedAsDecimal(Pos.WPAG_CONT_SOPR_PROFES, Len.Int.WPAG_CONT_SOPR_PROFES, Len.Fract.WPAG_CONT_SOPR_PROFES);
    }

    public byte[] getWpagContSoprProfesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_SOPR_PROFES, Pos.WPAG_CONT_SOPR_PROFES);
        return buffer;
    }

    public void initWpagContSoprProfesSpaces() {
        fill(Pos.WPAG_CONT_SOPR_PROFES, Len.WPAG_CONT_SOPR_PROFES, Types.SPACE_CHAR);
    }

    public void setWpagContSoprProfesNull(String wpagContSoprProfesNull) {
        writeString(Pos.WPAG_CONT_SOPR_PROFES_NULL, wpagContSoprProfesNull, Len.WPAG_CONT_SOPR_PROFES_NULL);
    }

    /**Original name: WPAG-CONT-SOPR-PROFES-NULL<br>*/
    public String getWpagContSoprProfesNull() {
        return readString(Pos.WPAG_CONT_SOPR_PROFES_NULL, Len.WPAG_CONT_SOPR_PROFES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SOPR_PROFES = 1;
        public static final int WPAG_CONT_SOPR_PROFES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SOPR_PROFES = 8;
        public static final int WPAG_CONT_SOPR_PROFES_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SOPR_PROFES = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SOPR_PROFES = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
