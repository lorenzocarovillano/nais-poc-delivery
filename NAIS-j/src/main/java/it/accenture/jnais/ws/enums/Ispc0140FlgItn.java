package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0140-FLG-ITN<br>
 * Variable: ISPC0140-FLG-ITN from copybook ISPC0140<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0140FlgItn {

    //==== PROPERTIES ====
    public String value = DefaultValues.stringVal(Len.FLG_ITN);
    public static final String POS2 = "0";
    public static final String NEG = "1";

    //==== METHODS ====
    public void setFlgItn(short flgItn) {
        this.value = NumericDisplay.asString(flgItn, Len.FLG_ITN);
    }

    public void setIspc0140FlgItnFormatted(String ispc0140FlgItn) {
        this.value = Trunc.toUnsignedNumeric(ispc0140FlgItn, Len.FLG_ITN);
    }

    public short getFlgItn() {
        return NumericDisplay.asShort(this.value);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLG_ITN = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
