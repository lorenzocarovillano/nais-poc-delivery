package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-AA-REN-CER<br>
 * Variable: W-B03-AA-REN-CER from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AaRenCerLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AaRenCerLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_AA_REN_CER;
    }

    public void setwB03AaRenCer(int wB03AaRenCer) {
        writeIntAsPacked(Pos.W_B03_AA_REN_CER, wB03AaRenCer, Len.Int.W_B03_AA_REN_CER);
    }

    /**Original name: W-B03-AA-REN-CER<br>*/
    public int getwB03AaRenCer() {
        return readPackedAsInt(Pos.W_B03_AA_REN_CER, Len.Int.W_B03_AA_REN_CER);
    }

    public byte[] getwB03AaRenCerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_AA_REN_CER, Pos.W_B03_AA_REN_CER);
        return buffer;
    }

    public void setwB03AaRenCerNull(String wB03AaRenCerNull) {
        writeString(Pos.W_B03_AA_REN_CER_NULL, wB03AaRenCerNull, Len.W_B03_AA_REN_CER_NULL);
    }

    /**Original name: W-B03-AA-REN-CER-NULL<br>*/
    public String getwB03AaRenCerNull() {
        return readString(Pos.W_B03_AA_REN_CER_NULL, Len.W_B03_AA_REN_CER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_AA_REN_CER = 1;
        public static final int W_B03_AA_REN_CER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_AA_REN_CER = 3;
        public static final int W_B03_AA_REN_CER_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_AA_REN_CER = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
