package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-TP-ACQS-CNTRT<br>
 * Variable: WRRE-TP-ACQS-CNTRT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreTpAcqsCntrt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreTpAcqsCntrt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_TP_ACQS_CNTRT;
    }

    public void setWrreTpAcqsCntrt(int wrreTpAcqsCntrt) {
        writeIntAsPacked(Pos.WRRE_TP_ACQS_CNTRT, wrreTpAcqsCntrt, Len.Int.WRRE_TP_ACQS_CNTRT);
    }

    public void setWrreTpAcqsCntrtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_TP_ACQS_CNTRT, Pos.WRRE_TP_ACQS_CNTRT);
    }

    /**Original name: WRRE-TP-ACQS-CNTRT<br>*/
    public int getWrreTpAcqsCntrt() {
        return readPackedAsInt(Pos.WRRE_TP_ACQS_CNTRT, Len.Int.WRRE_TP_ACQS_CNTRT);
    }

    public byte[] getWrreTpAcqsCntrtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_TP_ACQS_CNTRT, Pos.WRRE_TP_ACQS_CNTRT);
        return buffer;
    }

    public void initWrreTpAcqsCntrtSpaces() {
        fill(Pos.WRRE_TP_ACQS_CNTRT, Len.WRRE_TP_ACQS_CNTRT, Types.SPACE_CHAR);
    }

    public void setWrreTpAcqsCntrtNull(String wrreTpAcqsCntrtNull) {
        writeString(Pos.WRRE_TP_ACQS_CNTRT_NULL, wrreTpAcqsCntrtNull, Len.WRRE_TP_ACQS_CNTRT_NULL);
    }

    /**Original name: WRRE-TP-ACQS-CNTRT-NULL<br>*/
    public String getWrreTpAcqsCntrtNull() {
        return readString(Pos.WRRE_TP_ACQS_CNTRT_NULL, Len.WRRE_TP_ACQS_CNTRT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_TP_ACQS_CNTRT = 1;
        public static final int WRRE_TP_ACQS_CNTRT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_TP_ACQS_CNTRT = 3;
        public static final int WRRE_TP_ACQS_CNTRT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_TP_ACQS_CNTRT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
