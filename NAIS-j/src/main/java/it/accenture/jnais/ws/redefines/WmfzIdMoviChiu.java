package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WMFZ-ID-MOVI-CHIU<br>
 * Variable: WMFZ-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_ID_MOVI_CHIU;
    }

    public void setWmfzIdMoviChiu(int wmfzIdMoviChiu) {
        writeIntAsPacked(Pos.WMFZ_ID_MOVI_CHIU, wmfzIdMoviChiu, Len.Int.WMFZ_ID_MOVI_CHIU);
    }

    public void setWmfzIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_ID_MOVI_CHIU, Pos.WMFZ_ID_MOVI_CHIU);
    }

    /**Original name: WMFZ-ID-MOVI-CHIU<br>*/
    public int getWmfzIdMoviChiu() {
        return readPackedAsInt(Pos.WMFZ_ID_MOVI_CHIU, Len.Int.WMFZ_ID_MOVI_CHIU);
    }

    public byte[] getWmfzIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_ID_MOVI_CHIU, Pos.WMFZ_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWmfzIdMoviChiuSpaces() {
        fill(Pos.WMFZ_ID_MOVI_CHIU, Len.WMFZ_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWmfzIdMoviChiuNull(String wmfzIdMoviChiuNull) {
        writeString(Pos.WMFZ_ID_MOVI_CHIU_NULL, wmfzIdMoviChiuNull, Len.WMFZ_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WMFZ-ID-MOVI-CHIU-NULL<br>*/
    public String getWmfzIdMoviChiuNull() {
        return readString(Pos.WMFZ_ID_MOVI_CHIU_NULL, Len.WMFZ_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_MOVI_CHIU = 1;
        public static final int WMFZ_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_MOVI_CHIU = 5;
        public static final int WMFZ_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
