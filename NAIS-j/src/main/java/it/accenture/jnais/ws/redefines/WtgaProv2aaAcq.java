package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PROV-2AA-ACQ<br>
 * Variable: WTGA-PROV-2AA-ACQ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaProv2aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaProv2aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PROV2AA_ACQ;
    }

    public void setWtgaProv2aaAcq(AfDecimal wtgaProv2aaAcq) {
        writeDecimalAsPacked(Pos.WTGA_PROV2AA_ACQ, wtgaProv2aaAcq.copy());
    }

    public void setWtgaProv2aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PROV2AA_ACQ, Pos.WTGA_PROV2AA_ACQ);
    }

    /**Original name: WTGA-PROV-2AA-ACQ<br>*/
    public AfDecimal getWtgaProv2aaAcq() {
        return readPackedAsDecimal(Pos.WTGA_PROV2AA_ACQ, Len.Int.WTGA_PROV2AA_ACQ, Len.Fract.WTGA_PROV2AA_ACQ);
    }

    public byte[] getWtgaProv2aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PROV2AA_ACQ, Pos.WTGA_PROV2AA_ACQ);
        return buffer;
    }

    public void initWtgaProv2aaAcqSpaces() {
        fill(Pos.WTGA_PROV2AA_ACQ, Len.WTGA_PROV2AA_ACQ, Types.SPACE_CHAR);
    }

    public void setWtgaProv2aaAcqNull(String wtgaProv2aaAcqNull) {
        writeString(Pos.WTGA_PROV2AA_ACQ_NULL, wtgaProv2aaAcqNull, Len.WTGA_PROV2AA_ACQ_NULL);
    }

    /**Original name: WTGA-PROV-2AA-ACQ-NULL<br>*/
    public String getWtgaProv2aaAcqNull() {
        return readString(Pos.WTGA_PROV2AA_ACQ_NULL, Len.WTGA_PROV2AA_ACQ_NULL);
    }

    public String getWtgaProv2aaAcqNullFormatted() {
        return Functions.padBlanks(getWtgaProv2aaAcqNull(), Len.WTGA_PROV2AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PROV2AA_ACQ = 1;
        public static final int WTGA_PROV2AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PROV2AA_ACQ = 8;
        public static final int WTGA_PROV2AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PROV2AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PROV2AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
