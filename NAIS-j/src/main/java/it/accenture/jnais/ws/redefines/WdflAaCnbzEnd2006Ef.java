package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-AA-CNBZ-END2006-EF<br>
 * Variable: WDFL-AA-CNBZ-END2006-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAaCnbzEnd2006Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAaCnbzEnd2006Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_AA_CNBZ_END2006_EF;
    }

    public void setWdflAaCnbzEnd2006Ef(int wdflAaCnbzEnd2006Ef) {
        writeIntAsPacked(Pos.WDFL_AA_CNBZ_END2006_EF, wdflAaCnbzEnd2006Ef, Len.Int.WDFL_AA_CNBZ_END2006_EF);
    }

    public void setWdflAaCnbzEnd2006EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_AA_CNBZ_END2006_EF, Pos.WDFL_AA_CNBZ_END2006_EF);
    }

    /**Original name: WDFL-AA-CNBZ-END2006-EF<br>*/
    public int getWdflAaCnbzEnd2006Ef() {
        return readPackedAsInt(Pos.WDFL_AA_CNBZ_END2006_EF, Len.Int.WDFL_AA_CNBZ_END2006_EF);
    }

    public byte[] getWdflAaCnbzEnd2006EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_AA_CNBZ_END2006_EF, Pos.WDFL_AA_CNBZ_END2006_EF);
        return buffer;
    }

    public void setWdflAaCnbzEnd2006EfNull(String wdflAaCnbzEnd2006EfNull) {
        writeString(Pos.WDFL_AA_CNBZ_END2006_EF_NULL, wdflAaCnbzEnd2006EfNull, Len.WDFL_AA_CNBZ_END2006_EF_NULL);
    }

    /**Original name: WDFL-AA-CNBZ-END2006-EF-NULL<br>*/
    public String getWdflAaCnbzEnd2006EfNull() {
        return readString(Pos.WDFL_AA_CNBZ_END2006_EF_NULL, Len.WDFL_AA_CNBZ_END2006_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_AA_CNBZ_END2006_EF = 1;
        public static final int WDFL_AA_CNBZ_END2006_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_AA_CNBZ_END2006_EF = 3;
        public static final int WDFL_AA_CNBZ_END2006_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_AA_CNBZ_END2006_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
