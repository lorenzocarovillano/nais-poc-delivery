package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IS-1382011C<br>
 * Variable: WDFL-IS-1382011C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIs1382011c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIs1382011c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IS1382011C;
    }

    public void setWdflIs1382011c(AfDecimal wdflIs1382011c) {
        writeDecimalAsPacked(Pos.WDFL_IS1382011C, wdflIs1382011c.copy());
    }

    public void setWdflIs1382011cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IS1382011C, Pos.WDFL_IS1382011C);
    }

    /**Original name: WDFL-IS-1382011C<br>*/
    public AfDecimal getWdflIs1382011c() {
        return readPackedAsDecimal(Pos.WDFL_IS1382011C, Len.Int.WDFL_IS1382011C, Len.Fract.WDFL_IS1382011C);
    }

    public byte[] getWdflIs1382011cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IS1382011C, Pos.WDFL_IS1382011C);
        return buffer;
    }

    public void setWdflIs1382011cNull(String wdflIs1382011cNull) {
        writeString(Pos.WDFL_IS1382011C_NULL, wdflIs1382011cNull, Len.WDFL_IS1382011C_NULL);
    }

    /**Original name: WDFL-IS-1382011C-NULL<br>*/
    public String getWdflIs1382011cNull() {
        return readString(Pos.WDFL_IS1382011C_NULL, Len.WDFL_IS1382011C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IS1382011C = 1;
        public static final int WDFL_IS1382011C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IS1382011C = 8;
        public static final int WDFL_IS1382011C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IS1382011C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IS1382011C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
