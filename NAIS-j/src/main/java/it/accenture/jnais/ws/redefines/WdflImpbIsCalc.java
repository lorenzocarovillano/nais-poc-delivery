package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-CALC<br>
 * Variable: WDFL-IMPB-IS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIsCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIsCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS_CALC;
    }

    public void setWdflImpbIsCalc(AfDecimal wdflImpbIsCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS_CALC, wdflImpbIsCalc.copy());
    }

    public void setWdflImpbIsCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS_CALC, Pos.WDFL_IMPB_IS_CALC);
    }

    /**Original name: WDFL-IMPB-IS-CALC<br>*/
    public AfDecimal getWdflImpbIsCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS_CALC, Len.Int.WDFL_IMPB_IS_CALC, Len.Fract.WDFL_IMPB_IS_CALC);
    }

    public byte[] getWdflImpbIsCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS_CALC, Pos.WDFL_IMPB_IS_CALC);
        return buffer;
    }

    public void setWdflImpbIsCalcNull(String wdflImpbIsCalcNull) {
        writeString(Pos.WDFL_IMPB_IS_CALC_NULL, wdflImpbIsCalcNull, Len.WDFL_IMPB_IS_CALC_NULL);
    }

    /**Original name: WDFL-IMPB-IS-CALC-NULL<br>*/
    public String getWdflImpbIsCalcNull() {
        return readString(Pos.WDFL_IMPB_IS_CALC_NULL, Len.WDFL_IMPB_IS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS_CALC = 1;
        public static final int WDFL_IMPB_IS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS_CALC = 8;
        public static final int WDFL_IMPB_IS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
