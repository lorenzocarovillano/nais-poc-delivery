package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvgrz1;
import it.accenture.jnais.copy.WgrzDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WGRZ-TAB-GAR<br>
 * Variables: WGRZ-TAB-GAR from copybook LCCVGRZB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WgrzTabGar {

    //==== PROPERTIES ====
    //Original name: LCCVGRZ1
    private Lccvgrz1 lccvgrz1 = new Lccvgrz1();

    //==== METHODS ====
    public void setWgarTabGarBytes(byte[] buffer) {
        setTabGarBytes(buffer, 1);
    }

    public byte[] getTabGarBytes() {
        byte[] buffer = new byte[Len.TAB_GAR];
        return getTabGarBytes(buffer, 1);
    }

    public void setTabGarBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvgrz1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvgrz1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvgrz1.Len.Int.ID_PTF, 0));
        position += Lccvgrz1.Len.ID_PTF;
        lccvgrz1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getTabGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvgrz1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvgrz1.getIdPtf(), Lccvgrz1.Len.Int.ID_PTF, 0);
        position += Lccvgrz1.Len.ID_PTF;
        lccvgrz1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initTabGarSpaces() {
        lccvgrz1.initLccvgrz1Spaces();
    }

    public Lccvgrz1 getLccvgrz1() {
        return lccvgrz1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TAB_GAR = WpolStatus.Len.STATUS + Lccvgrz1.Len.ID_PTF + WgrzDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
