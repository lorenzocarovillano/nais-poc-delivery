package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IS-1382011D<br>
 * Variable: WDFL-IS-1382011D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIs1382011d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIs1382011d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IS1382011D;
    }

    public void setWdflIs1382011d(AfDecimal wdflIs1382011d) {
        writeDecimalAsPacked(Pos.WDFL_IS1382011D, wdflIs1382011d.copy());
    }

    public void setWdflIs1382011dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IS1382011D, Pos.WDFL_IS1382011D);
    }

    /**Original name: WDFL-IS-1382011D<br>*/
    public AfDecimal getWdflIs1382011d() {
        return readPackedAsDecimal(Pos.WDFL_IS1382011D, Len.Int.WDFL_IS1382011D, Len.Fract.WDFL_IS1382011D);
    }

    public byte[] getWdflIs1382011dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IS1382011D, Pos.WDFL_IS1382011D);
        return buffer;
    }

    public void setWdflIs1382011dNull(String wdflIs1382011dNull) {
        writeString(Pos.WDFL_IS1382011D_NULL, wdflIs1382011dNull, Len.WDFL_IS1382011D_NULL);
    }

    /**Original name: WDFL-IS-1382011D-NULL<br>*/
    public String getWdflIs1382011dNull() {
        return readString(Pos.WDFL_IS1382011D_NULL, Len.WDFL_IS1382011D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IS1382011D = 1;
        public static final int WDFL_IS1382011D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IS1382011D = 8;
        public static final int WDFL_IS1382011D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IS1382011D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IS1382011D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
