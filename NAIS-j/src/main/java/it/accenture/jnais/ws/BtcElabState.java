package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: BTC-ELAB-STATE<br>
 * Variable: BTC-ELAB-STATE from copybook IDBVBES1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcElabState extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: BES-COD-ELAB-STATE
    private char codElabState = DefaultValues.CHAR_VAL;
    //Original name: BES-DES
    private String des = DefaultValues.stringVal(Len.DES);
    //Original name: BES-FLAG-TO-EXECUTE
    private char flagToExecute = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_ELAB_STATE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcElabStateBytes(buf);
    }

    public String getBtcElabStateFormatted() {
        return MarshalByteExt.bufferToStr(getBtcElabStateBytes());
    }

    public void setBtcElabStateBytes(byte[] buffer) {
        setBtcElabStateBytes(buffer, 1);
    }

    public byte[] getBtcElabStateBytes() {
        byte[] buffer = new byte[Len.BTC_ELAB_STATE];
        return getBtcElabStateBytes(buffer, 1);
    }

    public void setBtcElabStateBytes(byte[] buffer, int offset) {
        int position = offset;
        codElabState = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        des = MarshalByte.readString(buffer, position, Len.DES);
        position += Len.DES;
        flagToExecute = MarshalByte.readChar(buffer, position);
    }

    public byte[] getBtcElabStateBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, codElabState);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, des, Len.DES);
        position += Len.DES;
        MarshalByte.writeChar(buffer, position, flagToExecute);
        return buffer;
    }

    public void setCodElabState(char codElabState) {
        this.codElabState = codElabState;
    }

    public char getCodElabState() {
        return this.codElabState;
    }

    public void setDes(String des) {
        this.des = Functions.subString(des, Len.DES);
    }

    public String getDes() {
        return this.des;
    }

    public void setFlagToExecute(char flagToExecute) {
        this.flagToExecute = flagToExecute;
    }

    public char getFlagToExecute() {
        return this.flagToExecute;
    }

    @Override
    public byte[] serialize() {
        return getBtcElabStateBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ELAB_STATE = 1;
        public static final int DES = 50;
        public static final int FLAG_TO_EXECUTE = 1;
        public static final int BTC_ELAB_STATE = COD_ELAB_STATE + DES + FLAG_TO_EXECUTE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
