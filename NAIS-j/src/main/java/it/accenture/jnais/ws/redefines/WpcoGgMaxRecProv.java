package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-GG-MAX-REC-PROV<br>
 * Variable: WPCO-GG-MAX-REC-PROV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoGgMaxRecProv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoGgMaxRecProv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_GG_MAX_REC_PROV;
    }

    public void setWpcoGgMaxRecProv(short wpcoGgMaxRecProv) {
        writeShortAsPacked(Pos.WPCO_GG_MAX_REC_PROV, wpcoGgMaxRecProv, Len.Int.WPCO_GG_MAX_REC_PROV);
    }

    public void setDpcoGgMaxRecProvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_GG_MAX_REC_PROV, Pos.WPCO_GG_MAX_REC_PROV);
    }

    /**Original name: WPCO-GG-MAX-REC-PROV<br>*/
    public short getWpcoGgMaxRecProv() {
        return readPackedAsShort(Pos.WPCO_GG_MAX_REC_PROV, Len.Int.WPCO_GG_MAX_REC_PROV);
    }

    public byte[] getWpcoGgMaxRecProvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_GG_MAX_REC_PROV, Pos.WPCO_GG_MAX_REC_PROV);
        return buffer;
    }

    public void setWpcoGgMaxRecProvNull(String wpcoGgMaxRecProvNull) {
        writeString(Pos.WPCO_GG_MAX_REC_PROV_NULL, wpcoGgMaxRecProvNull, Len.WPCO_GG_MAX_REC_PROV_NULL);
    }

    /**Original name: WPCO-GG-MAX-REC-PROV-NULL<br>*/
    public String getWpcoGgMaxRecProvNull() {
        return readString(Pos.WPCO_GG_MAX_REC_PROV_NULL, Len.WPCO_GG_MAX_REC_PROV_NULL);
    }

    public String getWpcoGgMaxRecProvNullFormatted() {
        return Functions.padBlanks(getWpcoGgMaxRecProvNull(), Len.WPCO_GG_MAX_REC_PROV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_GG_MAX_REC_PROV = 1;
        public static final int WPCO_GG_MAX_REC_PROV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_GG_MAX_REC_PROV = 3;
        public static final int WPCO_GG_MAX_REC_PROV_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_GG_MAX_REC_PROV = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
