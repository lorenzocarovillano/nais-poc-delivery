package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-FG<br>
 * Variables: WS-FG from program LOAS0870<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsFg {

    //==== PROPERTIES ====
    //Original name: WS-FLAG-INC-TGA
    private char wsFlagIncTga = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWsFlagIncTga(char wsFlagIncTga) {
        this.wsFlagIncTga = wsFlagIncTga;
    }

    public void setWsFlagIncTgaFormatted(String wsFlagIncTga) {
        setWsFlagIncTga(Functions.charAt(wsFlagIncTga, Types.CHAR_SIZE));
    }

    public char getWsFlagIncTga() {
        return this.wsFlagIncTga;
    }
}
