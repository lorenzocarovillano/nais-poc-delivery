package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-STAB<br>
 * Variable: WTGA-PRE-STAB from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_STAB;
    }

    public void setWtgaPreStab(AfDecimal wtgaPreStab) {
        writeDecimalAsPacked(Pos.WTGA_PRE_STAB, wtgaPreStab.copy());
    }

    public void setWtgaPreStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_STAB, Pos.WTGA_PRE_STAB);
    }

    /**Original name: WTGA-PRE-STAB<br>*/
    public AfDecimal getWtgaPreStab() {
        return readPackedAsDecimal(Pos.WTGA_PRE_STAB, Len.Int.WTGA_PRE_STAB, Len.Fract.WTGA_PRE_STAB);
    }

    public byte[] getWtgaPreStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_STAB, Pos.WTGA_PRE_STAB);
        return buffer;
    }

    public void initWtgaPreStabSpaces() {
        fill(Pos.WTGA_PRE_STAB, Len.WTGA_PRE_STAB, Types.SPACE_CHAR);
    }

    public void setWtgaPreStabNull(String wtgaPreStabNull) {
        writeString(Pos.WTGA_PRE_STAB_NULL, wtgaPreStabNull, Len.WTGA_PRE_STAB_NULL);
    }

    /**Original name: WTGA-PRE-STAB-NULL<br>*/
    public String getWtgaPreStabNull() {
        return readString(Pos.WTGA_PRE_STAB_NULL, Len.WTGA_PRE_STAB_NULL);
    }

    public String getWtgaPreStabNullFormatted() {
        return Functions.padBlanks(getWtgaPreStabNull(), Len.WTGA_PRE_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_STAB = 1;
        public static final int WTGA_PRE_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_STAB = 8;
        public static final int WTGA_PRE_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
