package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-RIDZ-TFR<br>
 * Variable: WDFA-RIDZ-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaRidzTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaRidzTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_RIDZ_TFR;
    }

    public void setWdfaRidzTfr(AfDecimal wdfaRidzTfr) {
        writeDecimalAsPacked(Pos.WDFA_RIDZ_TFR, wdfaRidzTfr.copy());
    }

    public void setWdfaRidzTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_RIDZ_TFR, Pos.WDFA_RIDZ_TFR);
    }

    /**Original name: WDFA-RIDZ-TFR<br>*/
    public AfDecimal getWdfaRidzTfr() {
        return readPackedAsDecimal(Pos.WDFA_RIDZ_TFR, Len.Int.WDFA_RIDZ_TFR, Len.Fract.WDFA_RIDZ_TFR);
    }

    public byte[] getWdfaRidzTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_RIDZ_TFR, Pos.WDFA_RIDZ_TFR);
        return buffer;
    }

    public void setWdfaRidzTfrNull(String wdfaRidzTfrNull) {
        writeString(Pos.WDFA_RIDZ_TFR_NULL, wdfaRidzTfrNull, Len.WDFA_RIDZ_TFR_NULL);
    }

    /**Original name: WDFA-RIDZ-TFR-NULL<br>*/
    public String getWdfaRidzTfrNull() {
        return readString(Pos.WDFA_RIDZ_TFR_NULL, Len.WDFA_RIDZ_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_RIDZ_TFR = 1;
        public static final int WDFA_RIDZ_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_RIDZ_TFR = 8;
        public static final int WDFA_RIDZ_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_RIDZ_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_RIDZ_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
