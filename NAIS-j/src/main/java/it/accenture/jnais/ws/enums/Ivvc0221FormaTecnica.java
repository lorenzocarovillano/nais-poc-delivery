package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0221-FORMA-TECNICA<br>
 * Variable: IVVC0221-FORMA-TECNICA from copybook IVVC0221<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0221FormaTecnica {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FORMA_TECNICA);
    public static final String INDIVIDUALE = "IN";
    public static final String COLLETTIVE = "CO";

    //==== METHODS ====
    public void setFormaTecnica(String formaTecnica) {
        this.value = Functions.subString(formaTecnica, Len.FORMA_TECNICA);
    }

    public String getFormaTecnica() {
        return this.value;
    }

    public boolean isIndividuale() {
        return value.equals(INDIVIDUALE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORMA_TECNICA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
