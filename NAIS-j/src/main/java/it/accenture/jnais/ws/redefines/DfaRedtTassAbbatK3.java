package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-REDT-TASS-ABBAT-K3<br>
 * Variable: DFA-REDT-TASS-ABBAT-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaRedtTassAbbatK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaRedtTassAbbatK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_REDT_TASS_ABBAT_K3;
    }

    public void setDfaRedtTassAbbatK3(AfDecimal dfaRedtTassAbbatK3) {
        writeDecimalAsPacked(Pos.DFA_REDT_TASS_ABBAT_K3, dfaRedtTassAbbatK3.copy());
    }

    public void setDfaRedtTassAbbatK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_REDT_TASS_ABBAT_K3, Pos.DFA_REDT_TASS_ABBAT_K3);
    }

    /**Original name: DFA-REDT-TASS-ABBAT-K3<br>*/
    public AfDecimal getDfaRedtTassAbbatK3() {
        return readPackedAsDecimal(Pos.DFA_REDT_TASS_ABBAT_K3, Len.Int.DFA_REDT_TASS_ABBAT_K3, Len.Fract.DFA_REDT_TASS_ABBAT_K3);
    }

    public byte[] getDfaRedtTassAbbatK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_REDT_TASS_ABBAT_K3, Pos.DFA_REDT_TASS_ABBAT_K3);
        return buffer;
    }

    public void setDfaRedtTassAbbatK3Null(String dfaRedtTassAbbatK3Null) {
        writeString(Pos.DFA_REDT_TASS_ABBAT_K3_NULL, dfaRedtTassAbbatK3Null, Len.DFA_REDT_TASS_ABBAT_K3_NULL);
    }

    /**Original name: DFA-REDT-TASS-ABBAT-K3-NULL<br>*/
    public String getDfaRedtTassAbbatK3Null() {
        return readString(Pos.DFA_REDT_TASS_ABBAT_K3_NULL, Len.DFA_REDT_TASS_ABBAT_K3_NULL);
    }

    public String getDfaRedtTassAbbatK3NullFormatted() {
        return Functions.padBlanks(getDfaRedtTassAbbatK3Null(), Len.DFA_REDT_TASS_ABBAT_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_REDT_TASS_ABBAT_K3 = 1;
        public static final int DFA_REDT_TASS_ABBAT_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_REDT_TASS_ABBAT_K3 = 8;
        public static final int DFA_REDT_TASS_ABBAT_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_REDT_TASS_ABBAT_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_REDT_TASS_ABBAT_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
