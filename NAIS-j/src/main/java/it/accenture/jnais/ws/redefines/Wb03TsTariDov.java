package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-TARI-DOV<br>
 * Variable: WB03-TS-TARI-DOV from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsTariDov extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsTariDov() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_TARI_DOV;
    }

    public void setWb03TsTariDovFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_TARI_DOV, Pos.WB03_TS_TARI_DOV);
    }

    /**Original name: WB03-TS-TARI-DOV<br>*/
    public AfDecimal getWb03TsTariDov() {
        return readPackedAsDecimal(Pos.WB03_TS_TARI_DOV, Len.Int.WB03_TS_TARI_DOV, Len.Fract.WB03_TS_TARI_DOV);
    }

    public byte[] getWb03TsTariDovAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_TARI_DOV, Pos.WB03_TS_TARI_DOV);
        return buffer;
    }

    public void setWb03TsTariDovNull(String wb03TsTariDovNull) {
        writeString(Pos.WB03_TS_TARI_DOV_NULL, wb03TsTariDovNull, Len.WB03_TS_TARI_DOV_NULL);
    }

    /**Original name: WB03-TS-TARI-DOV-NULL<br>*/
    public String getWb03TsTariDovNull() {
        return readString(Pos.WB03_TS_TARI_DOV_NULL, Len.WB03_TS_TARI_DOV_NULL);
    }

    public String getWb03TsTariDovNullFormatted() {
        return Functions.padBlanks(getWb03TsTariDovNull(), Len.WB03_TS_TARI_DOV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_TARI_DOV = 1;
        public static final int WB03_TS_TARI_DOV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_TARI_DOV = 8;
        public static final int WB03_TS_TARI_DOV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_TARI_DOV = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_TARI_DOV = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
