package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-VAL-RISC-INI-VINPG<br>
 * Variable: L23-VAL-RISC-INI-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23ValRiscIniVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23ValRiscIniVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_VAL_RISC_INI_VINPG;
    }

    public void setL23ValRiscIniVinpg(AfDecimal l23ValRiscIniVinpg) {
        writeDecimalAsPacked(Pos.L23_VAL_RISC_INI_VINPG, l23ValRiscIniVinpg.copy());
    }

    public void setL23ValRiscIniVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_VAL_RISC_INI_VINPG, Pos.L23_VAL_RISC_INI_VINPG);
    }

    /**Original name: L23-VAL-RISC-INI-VINPG<br>*/
    public AfDecimal getL23ValRiscIniVinpg() {
        return readPackedAsDecimal(Pos.L23_VAL_RISC_INI_VINPG, Len.Int.L23_VAL_RISC_INI_VINPG, Len.Fract.L23_VAL_RISC_INI_VINPG);
    }

    public byte[] getL23ValRiscIniVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_VAL_RISC_INI_VINPG, Pos.L23_VAL_RISC_INI_VINPG);
        return buffer;
    }

    public void setL23ValRiscIniVinpgNull(String l23ValRiscIniVinpgNull) {
        writeString(Pos.L23_VAL_RISC_INI_VINPG_NULL, l23ValRiscIniVinpgNull, Len.L23_VAL_RISC_INI_VINPG_NULL);
    }

    /**Original name: L23-VAL-RISC-INI-VINPG-NULL<br>*/
    public String getL23ValRiscIniVinpgNull() {
        return readString(Pos.L23_VAL_RISC_INI_VINPG_NULL, Len.L23_VAL_RISC_INI_VINPG_NULL);
    }

    public String getL23ValRiscIniVinpgNullFormatted() {
        return Functions.padBlanks(getL23ValRiscIniVinpgNull(), Len.L23_VAL_RISC_INI_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_VAL_RISC_INI_VINPG = 1;
        public static final int L23_VAL_RISC_INI_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_VAL_RISC_INI_VINPG = 8;
        public static final int L23_VAL_RISC_INI_VINPG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_VAL_RISC_INI_VINPG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L23_VAL_RISC_INI_VINPG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
