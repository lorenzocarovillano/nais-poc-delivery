package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMPB-PROV-RICOR<br>
 * Variable: WTGA-IMPB-PROV-RICOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpbProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpbProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMPB_PROV_RICOR;
    }

    public void setWtgaImpbProvRicor(AfDecimal wtgaImpbProvRicor) {
        writeDecimalAsPacked(Pos.WTGA_IMPB_PROV_RICOR, wtgaImpbProvRicor.copy());
    }

    public void setWtgaImpbProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMPB_PROV_RICOR, Pos.WTGA_IMPB_PROV_RICOR);
    }

    /**Original name: WTGA-IMPB-PROV-RICOR<br>*/
    public AfDecimal getWtgaImpbProvRicor() {
        return readPackedAsDecimal(Pos.WTGA_IMPB_PROV_RICOR, Len.Int.WTGA_IMPB_PROV_RICOR, Len.Fract.WTGA_IMPB_PROV_RICOR);
    }

    public byte[] getWtgaImpbProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMPB_PROV_RICOR, Pos.WTGA_IMPB_PROV_RICOR);
        return buffer;
    }

    public void initWtgaImpbProvRicorSpaces() {
        fill(Pos.WTGA_IMPB_PROV_RICOR, Len.WTGA_IMPB_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWtgaImpbProvRicorNull(String wtgaImpbProvRicorNull) {
        writeString(Pos.WTGA_IMPB_PROV_RICOR_NULL, wtgaImpbProvRicorNull, Len.WTGA_IMPB_PROV_RICOR_NULL);
    }

    /**Original name: WTGA-IMPB-PROV-RICOR-NULL<br>*/
    public String getWtgaImpbProvRicorNull() {
        return readString(Pos.WTGA_IMPB_PROV_RICOR_NULL, Len.WTGA_IMPB_PROV_RICOR_NULL);
    }

    public String getWtgaImpbProvRicorNullFormatted() {
        return Functions.padBlanks(getWtgaImpbProvRicorNull(), Len.WTGA_IMPB_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_PROV_RICOR = 1;
        public static final int WTGA_IMPB_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_PROV_RICOR = 8;
        public static final int WTGA_IMPB_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
