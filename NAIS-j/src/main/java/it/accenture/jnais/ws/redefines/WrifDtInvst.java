package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRIF-DT-INVST<br>
 * Variable: WRIF-DT-INVST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifDtInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifDtInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_DT_INVST;
    }

    public void setWrifDtInvst(int wrifDtInvst) {
        writeIntAsPacked(Pos.WRIF_DT_INVST, wrifDtInvst, Len.Int.WRIF_DT_INVST);
    }

    public void setWrifDtInvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_DT_INVST, Pos.WRIF_DT_INVST);
    }

    /**Original name: WRIF-DT-INVST<br>*/
    public int getWrifDtInvst() {
        return readPackedAsInt(Pos.WRIF_DT_INVST, Len.Int.WRIF_DT_INVST);
    }

    public byte[] getWrifDtInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_DT_INVST, Pos.WRIF_DT_INVST);
        return buffer;
    }

    public void initWrifDtInvstSpaces() {
        fill(Pos.WRIF_DT_INVST, Len.WRIF_DT_INVST, Types.SPACE_CHAR);
    }

    public void setWrifDtInvstNull(String wrifDtInvstNull) {
        writeString(Pos.WRIF_DT_INVST_NULL, wrifDtInvstNull, Len.WRIF_DT_INVST_NULL);
    }

    /**Original name: WRIF-DT-INVST-NULL<br>*/
    public String getWrifDtInvstNull() {
        return readString(Pos.WRIF_DT_INVST_NULL, Len.WRIF_DT_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_DT_INVST = 1;
        public static final int WRIF_DT_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_DT_INVST = 5;
        public static final int WRIF_DT_INVST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_DT_INVST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
