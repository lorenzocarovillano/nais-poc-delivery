package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-ID-JOB-X<br>
 * Variable: WK-ID-JOB-X from program IABS0060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkIdJobX extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkIdJobX() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_ID_JOB_X;
    }

    public void setWkIdJobX(String wkIdJobX) {
        writeString(Pos.WK_ID_JOB_X, wkIdJobX, Len.WK_ID_JOB_X);
    }

    /**Original name: WK-ID-JOB-X<br>*/
    public String getWkIdJobX() {
        return readString(Pos.WK_ID_JOB_X, Len.WK_ID_JOB_X);
    }

    /**Original name: WK-ID-JOB-N<br>*/
    public int getWkIdJobN() {
        return readNumDispUnsignedInt(Pos.WK_ID_JOB_N, Len.WK_ID_JOB_N);
    }

    public String getWkIdJobNFormatted() {
        return readFixedString(Pos.WK_ID_JOB_N, Len.WK_ID_JOB_N);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_ID_JOB_X = 1;
        public static final int WK_ID_JOB_N = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ID_JOB_X = 9;
        public static final int WK_ID_JOB_N = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
