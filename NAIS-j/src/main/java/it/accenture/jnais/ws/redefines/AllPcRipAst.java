package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ALL-PC-RIP-AST<br>
 * Variable: ALL-PC-RIP-AST from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AllPcRipAst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AllPcRipAst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ALL_PC_RIP_AST;
    }

    public void setAllPcRipAst(AfDecimal allPcRipAst) {
        writeDecimalAsPacked(Pos.ALL_PC_RIP_AST, allPcRipAst.copy());
    }

    public void setAllPcRipAstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ALL_PC_RIP_AST, Pos.ALL_PC_RIP_AST);
    }

    /**Original name: ALL-PC-RIP-AST<br>*/
    public AfDecimal getAllPcRipAst() {
        return readPackedAsDecimal(Pos.ALL_PC_RIP_AST, Len.Int.ALL_PC_RIP_AST, Len.Fract.ALL_PC_RIP_AST);
    }

    public byte[] getAllPcRipAstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ALL_PC_RIP_AST, Pos.ALL_PC_RIP_AST);
        return buffer;
    }

    public void setAllPcRipAstNull(String allPcRipAstNull) {
        writeString(Pos.ALL_PC_RIP_AST_NULL, allPcRipAstNull, Len.ALL_PC_RIP_AST_NULL);
    }

    /**Original name: ALL-PC-RIP-AST-NULL<br>*/
    public String getAllPcRipAstNull() {
        return readString(Pos.ALL_PC_RIP_AST_NULL, Len.ALL_PC_RIP_AST_NULL);
    }

    public String getAllPcRipAstNullFormatted() {
        return Functions.padBlanks(getAllPcRipAstNull(), Len.ALL_PC_RIP_AST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ALL_PC_RIP_AST = 1;
        public static final int ALL_PC_RIP_AST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ALL_PC_RIP_AST = 4;
        public static final int ALL_PC_RIP_AST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int ALL_PC_RIP_AST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int ALL_PC_RIP_AST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
