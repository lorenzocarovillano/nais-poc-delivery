package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-SCAD-1A-RAT<br>
 * Variable: WP67-DT-SCAD-1A-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtScad1aRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtScad1aRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_SCAD1A_RAT;
    }

    public void setWp67DtScad1aRat(int wp67DtScad1aRat) {
        writeIntAsPacked(Pos.WP67_DT_SCAD1A_RAT, wp67DtScad1aRat, Len.Int.WP67_DT_SCAD1A_RAT);
    }

    public void setWp67DtScad1aRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_SCAD1A_RAT, Pos.WP67_DT_SCAD1A_RAT);
    }

    /**Original name: WP67-DT-SCAD-1A-RAT<br>*/
    public int getWp67DtScad1aRat() {
        return readPackedAsInt(Pos.WP67_DT_SCAD1A_RAT, Len.Int.WP67_DT_SCAD1A_RAT);
    }

    public byte[] getWp67DtScad1aRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_SCAD1A_RAT, Pos.WP67_DT_SCAD1A_RAT);
        return buffer;
    }

    public void setWp67DtScad1aRatNull(String wp67DtScad1aRatNull) {
        writeString(Pos.WP67_DT_SCAD1A_RAT_NULL, wp67DtScad1aRatNull, Len.WP67_DT_SCAD1A_RAT_NULL);
    }

    /**Original name: WP67-DT-SCAD-1A-RAT-NULL<br>*/
    public String getWp67DtScad1aRatNull() {
        return readString(Pos.WP67_DT_SCAD1A_RAT_NULL, Len.WP67_DT_SCAD1A_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_SCAD1A_RAT = 1;
        public static final int WP67_DT_SCAD1A_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_SCAD1A_RAT = 5;
        public static final int WP67_DT_SCAD1A_RAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_SCAD1A_RAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
