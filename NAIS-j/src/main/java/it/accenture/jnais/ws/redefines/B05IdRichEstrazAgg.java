package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B05-ID-RICH-ESTRAZ-AGG<br>
 * Variable: B05-ID-RICH-ESTRAZ-AGG from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B05IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B05IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B05_ID_RICH_ESTRAZ_AGG;
    }

    public void setB05IdRichEstrazAgg(int b05IdRichEstrazAgg) {
        writeIntAsPacked(Pos.B05_ID_RICH_ESTRAZ_AGG, b05IdRichEstrazAgg, Len.Int.B05_ID_RICH_ESTRAZ_AGG);
    }

    public void setB05IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B05_ID_RICH_ESTRAZ_AGG, Pos.B05_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: B05-ID-RICH-ESTRAZ-AGG<br>*/
    public int getB05IdRichEstrazAgg() {
        return readPackedAsInt(Pos.B05_ID_RICH_ESTRAZ_AGG, Len.Int.B05_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getB05IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B05_ID_RICH_ESTRAZ_AGG, Pos.B05_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setB05IdRichEstrazAggNull(String b05IdRichEstrazAggNull) {
        writeString(Pos.B05_ID_RICH_ESTRAZ_AGG_NULL, b05IdRichEstrazAggNull, Len.B05_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: B05-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getB05IdRichEstrazAggNull() {
        return readString(Pos.B05_ID_RICH_ESTRAZ_AGG_NULL, Len.B05_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getB05IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getB05IdRichEstrazAggNull(), Len.B05_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B05_ID_RICH_ESTRAZ_AGG = 1;
        public static final int B05_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B05_ID_RICH_ESTRAZ_AGG = 5;
        public static final int B05_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B05_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
