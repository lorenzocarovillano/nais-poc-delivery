package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-SD-I<br>
 * Variable: PCO-DT-ULT-BOLL-SD-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollSdI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollSdI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_SD_I;
    }

    public void setPcoDtUltBollSdI(int pcoDtUltBollSdI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_SD_I, pcoDtUltBollSdI, Len.Int.PCO_DT_ULT_BOLL_SD_I);
    }

    public void setPcoDtUltBollSdIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SD_I, Pos.PCO_DT_ULT_BOLL_SD_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-SD-I<br>*/
    public int getPcoDtUltBollSdI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_SD_I, Len.Int.PCO_DT_ULT_BOLL_SD_I);
    }

    public byte[] getPcoDtUltBollSdIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SD_I, Pos.PCO_DT_ULT_BOLL_SD_I);
        return buffer;
    }

    public void initPcoDtUltBollSdIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_SD_I, Len.PCO_DT_ULT_BOLL_SD_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollSdINull(String pcoDtUltBollSdINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_SD_I_NULL, pcoDtUltBollSdINull, Len.PCO_DT_ULT_BOLL_SD_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-SD-I-NULL<br>*/
    public String getPcoDtUltBollSdINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_SD_I_NULL, Len.PCO_DT_ULT_BOLL_SD_I_NULL);
    }

    public String getPcoDtUltBollSdINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollSdINull(), Len.PCO_DT_ULT_BOLL_SD_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SD_I = 1;
        public static final int PCO_DT_ULT_BOLL_SD_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SD_I = 5;
        public static final int PCO_DT_ULT_BOLL_SD_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_SD_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
