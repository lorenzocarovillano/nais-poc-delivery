package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-SOPR<br>
 * Variable: RST-RIS-SOPR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_SOPR;
    }

    public void setRstRisSopr(AfDecimal rstRisSopr) {
        writeDecimalAsPacked(Pos.RST_RIS_SOPR, rstRisSopr.copy());
    }

    public void setRstRisSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_SOPR, Pos.RST_RIS_SOPR);
    }

    /**Original name: RST-RIS-SOPR<br>*/
    public AfDecimal getRstRisSopr() {
        return readPackedAsDecimal(Pos.RST_RIS_SOPR, Len.Int.RST_RIS_SOPR, Len.Fract.RST_RIS_SOPR);
    }

    public byte[] getRstRisSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_SOPR, Pos.RST_RIS_SOPR);
        return buffer;
    }

    public void setRstRisSoprNull(String rstRisSoprNull) {
        writeString(Pos.RST_RIS_SOPR_NULL, rstRisSoprNull, Len.RST_RIS_SOPR_NULL);
    }

    /**Original name: RST-RIS-SOPR-NULL<br>*/
    public String getRstRisSoprNull() {
        return readString(Pos.RST_RIS_SOPR_NULL, Len.RST_RIS_SOPR_NULL);
    }

    public String getRstRisSoprNullFormatted() {
        return Functions.padBlanks(getRstRisSoprNull(), Len.RST_RIS_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_SOPR = 1;
        public static final int RST_RIS_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_SOPR = 8;
        public static final int RST_RIS_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
