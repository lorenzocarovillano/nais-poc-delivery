package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.WtgaTabLoas0310;

/**Original name: WTGA-AREA-TRANCHE<br>
 * Variable: WTGA-AREA-TRANCHE from program LOAS0310<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WtgaAreaTrancheLoas0310 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WTGA-ELE-TRAN-MAX
    private short wtgaEleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB
    private WtgaTabLoas0310 wtgaTab = new WtgaTabLoas0310();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_AREA_TRANCHE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWtgaAreaTrancheBytes(buf);
    }

    public String getWtgaAreaTrancheFormatted() {
        return MarshalByteExt.bufferToStr(getWtgaAreaTrancheBytes());
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer) {
        setWtgaAreaTrancheBytes(buffer, 1);
    }

    public byte[] getWtgaAreaTrancheBytes() {
        byte[] buffer = new byte[Len.WTGA_AREA_TRANCHE];
        return getWtgaAreaTrancheBytes(buffer, 1);
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        wtgaEleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wtgaTab.setWtgaTabBytes(buffer, position);
    }

    public byte[] getWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wtgaEleTranMax);
        position += Types.SHORT_SIZE;
        wtgaTab.getWtgaTabBytes(buffer, position);
        return buffer;
    }

    public void setWtgaEleTranMax(short wtgaEleTranMax) {
        this.wtgaEleTranMax = wtgaEleTranMax;
    }

    public short getWtgaEleTranMax() {
        return this.wtgaEleTranMax;
    }

    public WtgaTabLoas0310 getWtgaTab() {
        return wtgaTab;
    }

    @Override
    public byte[] serialize() {
        return getWtgaAreaTrancheBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_TRANCHE = WTGA_ELE_TRAN_MAX + WtgaTabLoas0310.Len.WTGA_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
