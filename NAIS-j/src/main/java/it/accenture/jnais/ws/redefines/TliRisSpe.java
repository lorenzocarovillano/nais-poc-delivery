package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-RIS-SPE<br>
 * Variable: TLI-RIS-SPE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliRisSpe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliRisSpe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_RIS_SPE;
    }

    public void setTliRisSpe(AfDecimal tliRisSpe) {
        writeDecimalAsPacked(Pos.TLI_RIS_SPE, tliRisSpe.copy());
    }

    public void setTliRisSpeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_RIS_SPE, Pos.TLI_RIS_SPE);
    }

    /**Original name: TLI-RIS-SPE<br>*/
    public AfDecimal getTliRisSpe() {
        return readPackedAsDecimal(Pos.TLI_RIS_SPE, Len.Int.TLI_RIS_SPE, Len.Fract.TLI_RIS_SPE);
    }

    public byte[] getTliRisSpeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_RIS_SPE, Pos.TLI_RIS_SPE);
        return buffer;
    }

    public void setTliRisSpeNull(String tliRisSpeNull) {
        writeString(Pos.TLI_RIS_SPE_NULL, tliRisSpeNull, Len.TLI_RIS_SPE_NULL);
    }

    /**Original name: TLI-RIS-SPE-NULL<br>*/
    public String getTliRisSpeNull() {
        return readString(Pos.TLI_RIS_SPE_NULL, Len.TLI_RIS_SPE_NULL);
    }

    public String getTliRisSpeNullFormatted() {
        return Functions.padBlanks(getTliRisSpeNull(), Len.TLI_RIS_SPE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_RIS_SPE = 1;
        public static final int TLI_RIS_SPE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_RIS_SPE = 8;
        public static final int TLI_RIS_SPE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_RIS_SPE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_RIS_SPE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
