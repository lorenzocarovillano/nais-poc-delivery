package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-COD-CAN<br>
 * Variable: P56-COD-CAN from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56CodCan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56CodCan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_COD_CAN;
    }

    public void setP56CodCan(int p56CodCan) {
        writeIntAsPacked(Pos.P56_COD_CAN, p56CodCan, Len.Int.P56_COD_CAN);
    }

    public void setP56CodCanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_COD_CAN, Pos.P56_COD_CAN);
    }

    /**Original name: P56-COD-CAN<br>*/
    public int getP56CodCan() {
        return readPackedAsInt(Pos.P56_COD_CAN, Len.Int.P56_COD_CAN);
    }

    public byte[] getP56CodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_COD_CAN, Pos.P56_COD_CAN);
        return buffer;
    }

    public void setP56CodCanNull(String p56CodCanNull) {
        writeString(Pos.P56_COD_CAN_NULL, p56CodCanNull, Len.P56_COD_CAN_NULL);
    }

    /**Original name: P56-COD-CAN-NULL<br>*/
    public String getP56CodCanNull() {
        return readString(Pos.P56_COD_CAN_NULL, Len.P56_COD_CAN_NULL);
    }

    public String getP56CodCanNullFormatted() {
        return Functions.padBlanks(getP56CodCanNull(), Len.P56_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_COD_CAN = 1;
        public static final int P56_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_COD_CAN = 3;
        public static final int P56_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
