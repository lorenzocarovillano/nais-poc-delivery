package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-INTR-RIAT<br>
 * Variable: WTDR-TOT-INTR-RIAT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_INTR_RIAT;
    }

    public void setWtdrTotIntrRiat(AfDecimal wtdrTotIntrRiat) {
        writeDecimalAsPacked(Pos.WTDR_TOT_INTR_RIAT, wtdrTotIntrRiat.copy());
    }

    /**Original name: WTDR-TOT-INTR-RIAT<br>*/
    public AfDecimal getWtdrTotIntrRiat() {
        return readPackedAsDecimal(Pos.WTDR_TOT_INTR_RIAT, Len.Int.WTDR_TOT_INTR_RIAT, Len.Fract.WTDR_TOT_INTR_RIAT);
    }

    public void setWtdrTotIntrRiatNull(String wtdrTotIntrRiatNull) {
        writeString(Pos.WTDR_TOT_INTR_RIAT_NULL, wtdrTotIntrRiatNull, Len.WTDR_TOT_INTR_RIAT_NULL);
    }

    /**Original name: WTDR-TOT-INTR-RIAT-NULL<br>*/
    public String getWtdrTotIntrRiatNull() {
        return readString(Pos.WTDR_TOT_INTR_RIAT_NULL, Len.WTDR_TOT_INTR_RIAT_NULL);
    }

    public String getWtdrTotIntrRiatNullFormatted() {
        return Functions.padBlanks(getWtdrTotIntrRiatNull(), Len.WTDR_TOT_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_RIAT = 1;
        public static final int WTDR_TOT_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_RIAT = 8;
        public static final int WTDR_TOT_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
