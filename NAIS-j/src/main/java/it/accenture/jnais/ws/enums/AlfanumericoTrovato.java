package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ALFANUMERICO-TROVATO<br>
 * Variable: ALFANUMERICO-TROVATO from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class AlfanumericoTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setAlfanumericoTrovato(char alfanumericoTrovato) {
        this.value = alfanumericoTrovato;
    }

    public char getAlfanumericoTrovato() {
        return this.value;
    }

    public boolean isAlfanumericoTrovatoSi() {
        return value == SI;
    }

    public void setAlfanumericoTrovatoSi() {
        value = SI;
    }

    public void setAlfanumericoTrovatoNo() {
        value = NO;
    }
}
