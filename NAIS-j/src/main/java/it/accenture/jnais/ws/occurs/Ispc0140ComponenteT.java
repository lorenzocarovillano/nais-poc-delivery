package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;
import it.accenture.jnais.ws.redefines.Ispc0140CompValGenericoT;

/**Original name: ISPC0140-COMPONENTE-T<br>
 * Variables: ISPC0140-COMPONENTE-T from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140ComponenteT {

    //==== PROPERTIES ====
    //Original name: ISPC0140-CODICE-COMPONENTE-T
    private String ispc0140CodiceComponenteT = DefaultValues.stringVal(Len.ISPC0140_CODICE_COMPONENTE_T);
    //Original name: ISPC0140-DESCR-COMPONENTE-T
    private String ispc0140DescrComponenteT = DefaultValues.stringVal(Len.ISPC0140_DESCR_COMPONENTE_T);
    //Original name: ISPC0140-COMP-TIPO-DATO-T
    private char ispc0140CompTipoDatoT = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-COMP-VAL-GENERICO-T
    private Ispc0140CompValGenericoT ispc0140CompValGenericoT = new Ispc0140CompValGenericoT();
    //Original name: ISPC0140-TRATTAMENTO-PROVV-T
    private Lccc006TrattDati ispc0140TrattamentoProvvT = new Lccc006TrattDati();

    //==== METHODS ====
    public void setComponenteTBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0140CodiceComponenteT = MarshalByte.readString(buffer, position, Len.ISPC0140_CODICE_COMPONENTE_T);
        position += Len.ISPC0140_CODICE_COMPONENTE_T;
        ispc0140DescrComponenteT = MarshalByte.readString(buffer, position, Len.ISPC0140_DESCR_COMPONENTE_T);
        position += Len.ISPC0140_DESCR_COMPONENTE_T;
        ispc0140CompTipoDatoT = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ispc0140CompValGenericoT.setIspc0140CompValGenericoTFromBuffer(buffer, position);
        position += Ispc0140CompValGenericoT.Len.ISPC0140_COMP_VAL_GENERICO_T;
        ispc0140TrattamentoProvvT.setFlagModCalcPreP(MarshalByte.readChar(buffer, position));
    }

    public byte[] getComponenteTBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ispc0140CodiceComponenteT, Len.ISPC0140_CODICE_COMPONENTE_T);
        position += Len.ISPC0140_CODICE_COMPONENTE_T;
        MarshalByte.writeString(buffer, position, ispc0140DescrComponenteT, Len.ISPC0140_DESCR_COMPONENTE_T);
        position += Len.ISPC0140_DESCR_COMPONENTE_T;
        MarshalByte.writeChar(buffer, position, ispc0140CompTipoDatoT);
        position += Types.CHAR_SIZE;
        ispc0140CompValGenericoT.getIspc0140CompValGenericoTAsBuffer(buffer, position);
        position += Ispc0140CompValGenericoT.Len.ISPC0140_COMP_VAL_GENERICO_T;
        MarshalByte.writeChar(buffer, position, ispc0140TrattamentoProvvT.getFlagModCalcPreP());
        return buffer;
    }

    public void initComponenteTSpaces() {
        ispc0140CodiceComponenteT = "";
        ispc0140DescrComponenteT = "";
        ispc0140CompTipoDatoT = Types.SPACE_CHAR;
        ispc0140CompValGenericoT.initIspc0140CompValGenericoTSpaces();
        ispc0140TrattamentoProvvT.setFlagModCalcPreP(Types.SPACE_CHAR);
    }

    public void setIspc0140CodiceComponenteT(String ispc0140CodiceComponenteT) {
        this.ispc0140CodiceComponenteT = Functions.subString(ispc0140CodiceComponenteT, Len.ISPC0140_CODICE_COMPONENTE_T);
    }

    public String getIspc0140CodiceComponenteT() {
        return this.ispc0140CodiceComponenteT;
    }

    public void setIspc0140DescrComponenteT(String ispc0140DescrComponenteT) {
        this.ispc0140DescrComponenteT = Functions.subString(ispc0140DescrComponenteT, Len.ISPC0140_DESCR_COMPONENTE_T);
    }

    public String getIspc0140DescrComponenteT() {
        return this.ispc0140DescrComponenteT;
    }

    public void setIspc0140CompTipoDatoT(char ispc0140CompTipoDatoT) {
        this.ispc0140CompTipoDatoT = ispc0140CompTipoDatoT;
    }

    public char getIspc0140CompTipoDatoT() {
        return this.ispc0140CompTipoDatoT;
    }

    public Ispc0140CompValGenericoT getIspc0140CompValGenericoT() {
        return ispc0140CompValGenericoT;
    }

    public Lccc006TrattDati getIspc0140TrattamentoProvvT() {
        return ispc0140TrattamentoProvvT;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ISPC0140_CODICE_COMPONENTE_T = 12;
        public static final int ISPC0140_DESCR_COMPONENTE_T = 30;
        public static final int ISPC0140_COMP_TIPO_DATO_T = 1;
        public static final int COMPONENTE_T = ISPC0140_CODICE_COMPONENTE_T + ISPC0140_DESCR_COMPONENTE_T + ISPC0140_COMP_TIPO_DATO_T + Ispc0140CompValGenericoT.Len.ISPC0140_COMP_VAL_GENERICO_T + Lccc006TrattDati.Len.FLAG_MOD_CALC_PRE_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
