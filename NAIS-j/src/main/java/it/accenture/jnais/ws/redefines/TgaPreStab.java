package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-STAB<br>
 * Variable: TGA-PRE-STAB from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_STAB;
    }

    public void setTgaPreStab(AfDecimal tgaPreStab) {
        writeDecimalAsPacked(Pos.TGA_PRE_STAB, tgaPreStab.copy());
    }

    public void setTgaPreStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_STAB, Pos.TGA_PRE_STAB);
    }

    /**Original name: TGA-PRE-STAB<br>*/
    public AfDecimal getTgaPreStab() {
        return readPackedAsDecimal(Pos.TGA_PRE_STAB, Len.Int.TGA_PRE_STAB, Len.Fract.TGA_PRE_STAB);
    }

    public byte[] getTgaPreStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_STAB, Pos.TGA_PRE_STAB);
        return buffer;
    }

    public void setTgaPreStabNull(String tgaPreStabNull) {
        writeString(Pos.TGA_PRE_STAB_NULL, tgaPreStabNull, Len.TGA_PRE_STAB_NULL);
    }

    /**Original name: TGA-PRE-STAB-NULL<br>*/
    public String getTgaPreStabNull() {
        return readString(Pos.TGA_PRE_STAB_NULL, Len.TGA_PRE_STAB_NULL);
    }

    public String getTgaPreStabNullFormatted() {
        return Functions.padBlanks(getTgaPreStabNull(), Len.TGA_PRE_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_STAB = 1;
        public static final int TGA_PRE_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_STAB = 8;
        public static final int TGA_PRE_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
