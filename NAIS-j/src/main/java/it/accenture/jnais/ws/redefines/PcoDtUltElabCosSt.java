package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-COS-ST<br>
 * Variable: PCO-DT-ULT-ELAB-COS-ST from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabCosSt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabCosSt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_COS_ST;
    }

    public void setPcoDtUltElabCosSt(int pcoDtUltElabCosSt) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_COS_ST, pcoDtUltElabCosSt, Len.Int.PCO_DT_ULT_ELAB_COS_ST);
    }

    public void setPcoDtUltElabCosStFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_COS_ST, Pos.PCO_DT_ULT_ELAB_COS_ST);
    }

    /**Original name: PCO-DT-ULT-ELAB-COS-ST<br>*/
    public int getPcoDtUltElabCosSt() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_COS_ST, Len.Int.PCO_DT_ULT_ELAB_COS_ST);
    }

    public byte[] getPcoDtUltElabCosStAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_COS_ST, Pos.PCO_DT_ULT_ELAB_COS_ST);
        return buffer;
    }

    public void initPcoDtUltElabCosStHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_COS_ST, Len.PCO_DT_ULT_ELAB_COS_ST, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabCosStNull(String pcoDtUltElabCosStNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_COS_ST_NULL, pcoDtUltElabCosStNull, Len.PCO_DT_ULT_ELAB_COS_ST_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-COS-ST-NULL<br>*/
    public String getPcoDtUltElabCosStNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_COS_ST_NULL, Len.PCO_DT_ULT_ELAB_COS_ST_NULL);
    }

    public String getPcoDtUltElabCosStNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabCosStNull(), Len.PCO_DT_ULT_ELAB_COS_ST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_COS_ST = 1;
        public static final int PCO_DT_ULT_ELAB_COS_ST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_COS_ST = 5;
        public static final int PCO_DT_ULT_ELAB_COS_ST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_COS_ST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
