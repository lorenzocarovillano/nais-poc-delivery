package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS-ALL<br>
 * Variable: FLAG-OPEN-FILESQS-ALL from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqsAll {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagOpenFilesqsAll(char flagOpenFilesqsAll) {
        this.value = flagOpenFilesqsAll;
    }

    public char getFlagOpenFilesqsAll() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }
}
