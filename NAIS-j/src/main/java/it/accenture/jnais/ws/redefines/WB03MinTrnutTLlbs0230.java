package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-MIN-TRNUT-T<br>
 * Variable: W-B03-MIN-TRNUT-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03MinTrnutTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03MinTrnutTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_MIN_TRNUT_T;
    }

    public void setwB03MinTrnutT(AfDecimal wB03MinTrnutT) {
        writeDecimalAsPacked(Pos.W_B03_MIN_TRNUT_T, wB03MinTrnutT.copy());
    }

    /**Original name: W-B03-MIN-TRNUT-T<br>*/
    public AfDecimal getwB03MinTrnutT() {
        return readPackedAsDecimal(Pos.W_B03_MIN_TRNUT_T, Len.Int.W_B03_MIN_TRNUT_T, Len.Fract.W_B03_MIN_TRNUT_T);
    }

    public byte[] getwB03MinTrnutTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_MIN_TRNUT_T, Pos.W_B03_MIN_TRNUT_T);
        return buffer;
    }

    public void setwB03MinTrnutTNull(String wB03MinTrnutTNull) {
        writeString(Pos.W_B03_MIN_TRNUT_T_NULL, wB03MinTrnutTNull, Len.W_B03_MIN_TRNUT_T_NULL);
    }

    /**Original name: W-B03-MIN-TRNUT-T-NULL<br>*/
    public String getwB03MinTrnutTNull() {
        return readString(Pos.W_B03_MIN_TRNUT_T_NULL, Len.W_B03_MIN_TRNUT_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_MIN_TRNUT_T = 1;
        public static final int W_B03_MIN_TRNUT_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_MIN_TRNUT_T = 8;
        public static final int W_B03_MIN_TRNUT_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_MIN_TRNUT_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_MIN_TRNUT_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
