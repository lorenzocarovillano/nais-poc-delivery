package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-DOV-INI<br>
 * Variable: W-B03-PRE-DOV-INI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PreDovIniLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PreDovIniLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_DOV_INI;
    }

    public void setwB03PreDovIni(AfDecimal wB03PreDovIni) {
        writeDecimalAsPacked(Pos.W_B03_PRE_DOV_INI, wB03PreDovIni.copy());
    }

    /**Original name: W-B03-PRE-DOV-INI<br>*/
    public AfDecimal getwB03PreDovIni() {
        return readPackedAsDecimal(Pos.W_B03_PRE_DOV_INI, Len.Int.W_B03_PRE_DOV_INI, Len.Fract.W_B03_PRE_DOV_INI);
    }

    public byte[] getwB03PreDovIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_DOV_INI, Pos.W_B03_PRE_DOV_INI);
        return buffer;
    }

    public void setwB03PreDovIniNull(String wB03PreDovIniNull) {
        writeString(Pos.W_B03_PRE_DOV_INI_NULL, wB03PreDovIniNull, Len.W_B03_PRE_DOV_INI_NULL);
    }

    /**Original name: W-B03-PRE-DOV-INI-NULL<br>*/
    public String getwB03PreDovIniNull() {
        return readString(Pos.W_B03_PRE_DOV_INI_NULL, Len.W_B03_PRE_DOV_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_DOV_INI = 1;
        public static final int W_B03_PRE_DOV_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_DOV_INI = 8;
        public static final int W_B03_PRE_DOV_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_DOV_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_DOV_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
