package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-DA-RIMB-EFF<br>
 * Variable: DFL-IMPST-DA-RIMB-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstDaRimbEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstDaRimbEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_DA_RIMB_EFF;
    }

    public void setDflImpstDaRimbEff(AfDecimal dflImpstDaRimbEff) {
        writeDecimalAsPacked(Pos.DFL_IMPST_DA_RIMB_EFF, dflImpstDaRimbEff.copy());
    }

    public void setDflImpstDaRimbEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_DA_RIMB_EFF, Pos.DFL_IMPST_DA_RIMB_EFF);
    }

    /**Original name: DFL-IMPST-DA-RIMB-EFF<br>*/
    public AfDecimal getDflImpstDaRimbEff() {
        return readPackedAsDecimal(Pos.DFL_IMPST_DA_RIMB_EFF, Len.Int.DFL_IMPST_DA_RIMB_EFF, Len.Fract.DFL_IMPST_DA_RIMB_EFF);
    }

    public byte[] getDflImpstDaRimbEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_DA_RIMB_EFF, Pos.DFL_IMPST_DA_RIMB_EFF);
        return buffer;
    }

    public void setDflImpstDaRimbEffNull(String dflImpstDaRimbEffNull) {
        writeString(Pos.DFL_IMPST_DA_RIMB_EFF_NULL, dflImpstDaRimbEffNull, Len.DFL_IMPST_DA_RIMB_EFF_NULL);
    }

    /**Original name: DFL-IMPST-DA-RIMB-EFF-NULL<br>*/
    public String getDflImpstDaRimbEffNull() {
        return readString(Pos.DFL_IMPST_DA_RIMB_EFF_NULL, Len.DFL_IMPST_DA_RIMB_EFF_NULL);
    }

    public String getDflImpstDaRimbEffNullFormatted() {
        return Functions.padBlanks(getDflImpstDaRimbEffNull(), Len.DFL_IMPST_DA_RIMB_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_DA_RIMB_EFF = 1;
        public static final int DFL_IMPST_DA_RIMB_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_DA_RIMB_EFF = 8;
        public static final int DFL_IMPST_DA_RIMB_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_DA_RIMB_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_DA_RIMB_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
