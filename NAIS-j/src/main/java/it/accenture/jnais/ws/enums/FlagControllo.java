package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-CONTROLLO<br>
 * Variable: FLAG-CONTROLLO from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagControllo {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setFlagControllo(char flagControllo) {
        this.value = flagControllo;
    }

    public char getFlagControllo() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }
}
