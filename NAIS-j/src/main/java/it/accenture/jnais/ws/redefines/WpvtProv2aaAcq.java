package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-PROV-2AA-ACQ<br>
 * Variable: WPVT-PROV-2AA-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtProv2aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtProv2aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_PROV2AA_ACQ;
    }

    public void setWpvtProv2aaAcq(AfDecimal wpvtProv2aaAcq) {
        writeDecimalAsPacked(Pos.WPVT_PROV2AA_ACQ, wpvtProv2aaAcq.copy());
    }

    public void setWpvtProv2aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_PROV2AA_ACQ, Pos.WPVT_PROV2AA_ACQ);
    }

    /**Original name: WPVT-PROV-2AA-ACQ<br>*/
    public AfDecimal getWpvtProv2aaAcq() {
        return readPackedAsDecimal(Pos.WPVT_PROV2AA_ACQ, Len.Int.WPVT_PROV2AA_ACQ, Len.Fract.WPVT_PROV2AA_ACQ);
    }

    public byte[] getWpvtProv2aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_PROV2AA_ACQ, Pos.WPVT_PROV2AA_ACQ);
        return buffer;
    }

    public void initWpvtProv2aaAcqSpaces() {
        fill(Pos.WPVT_PROV2AA_ACQ, Len.WPVT_PROV2AA_ACQ, Types.SPACE_CHAR);
    }

    public void setWpvtProv2aaAcqNull(String wpvtProv2aaAcqNull) {
        writeString(Pos.WPVT_PROV2AA_ACQ_NULL, wpvtProv2aaAcqNull, Len.WPVT_PROV2AA_ACQ_NULL);
    }

    /**Original name: WPVT-PROV-2AA-ACQ-NULL<br>*/
    public String getWpvtProv2aaAcqNull() {
        return readString(Pos.WPVT_PROV2AA_ACQ_NULL, Len.WPVT_PROV2AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_PROV2AA_ACQ = 1;
        public static final int WPVT_PROV2AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_PROV2AA_ACQ = 8;
        public static final int WPVT_PROV2AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_PROV2AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_PROV2AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
