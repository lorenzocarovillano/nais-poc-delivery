package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-CNBT-INPSTFM<br>
 * Variable: S089-CNBT-INPSTFM from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089CnbtInpstfm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089CnbtInpstfm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_CNBT_INPSTFM;
    }

    public void setWlquCnbtInpstfm(AfDecimal wlquCnbtInpstfm) {
        writeDecimalAsPacked(Pos.S089_CNBT_INPSTFM, wlquCnbtInpstfm.copy());
    }

    public void setWlquCnbtInpstfmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_CNBT_INPSTFM, Pos.S089_CNBT_INPSTFM);
    }

    /**Original name: WLQU-CNBT-INPSTFM<br>*/
    public AfDecimal getWlquCnbtInpstfm() {
        return readPackedAsDecimal(Pos.S089_CNBT_INPSTFM, Len.Int.WLQU_CNBT_INPSTFM, Len.Fract.WLQU_CNBT_INPSTFM);
    }

    public byte[] getWlquCnbtInpstfmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_CNBT_INPSTFM, Pos.S089_CNBT_INPSTFM);
        return buffer;
    }

    public void initWlquCnbtInpstfmSpaces() {
        fill(Pos.S089_CNBT_INPSTFM, Len.S089_CNBT_INPSTFM, Types.SPACE_CHAR);
    }

    public void setWlquCnbtInpstfmNull(String wlquCnbtInpstfmNull) {
        writeString(Pos.S089_CNBT_INPSTFM_NULL, wlquCnbtInpstfmNull, Len.WLQU_CNBT_INPSTFM_NULL);
    }

    /**Original name: WLQU-CNBT-INPSTFM-NULL<br>*/
    public String getWlquCnbtInpstfmNull() {
        return readString(Pos.S089_CNBT_INPSTFM_NULL, Len.WLQU_CNBT_INPSTFM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_CNBT_INPSTFM = 1;
        public static final int S089_CNBT_INPSTFM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_CNBT_INPSTFM = 8;
        public static final int WLQU_CNBT_INPSTFM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_CNBT_INPSTFM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_CNBT_INPSTFM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
