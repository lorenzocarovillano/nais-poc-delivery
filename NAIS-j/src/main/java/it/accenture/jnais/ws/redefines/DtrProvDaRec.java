package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PROV-DA-REC<br>
 * Variable: DTR-PROV-DA-REC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PROV_DA_REC;
    }

    public void setDtrProvDaRec(AfDecimal dtrProvDaRec) {
        writeDecimalAsPacked(Pos.DTR_PROV_DA_REC, dtrProvDaRec.copy());
    }

    public void setDtrProvDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PROV_DA_REC, Pos.DTR_PROV_DA_REC);
    }

    /**Original name: DTR-PROV-DA-REC<br>*/
    public AfDecimal getDtrProvDaRec() {
        return readPackedAsDecimal(Pos.DTR_PROV_DA_REC, Len.Int.DTR_PROV_DA_REC, Len.Fract.DTR_PROV_DA_REC);
    }

    public byte[] getDtrProvDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PROV_DA_REC, Pos.DTR_PROV_DA_REC);
        return buffer;
    }

    public void setDtrProvDaRecNull(String dtrProvDaRecNull) {
        writeString(Pos.DTR_PROV_DA_REC_NULL, dtrProvDaRecNull, Len.DTR_PROV_DA_REC_NULL);
    }

    /**Original name: DTR-PROV-DA-REC-NULL<br>*/
    public String getDtrProvDaRecNull() {
        return readString(Pos.DTR_PROV_DA_REC_NULL, Len.DTR_PROV_DA_REC_NULL);
    }

    public String getDtrProvDaRecNullFormatted() {
        return Functions.padBlanks(getDtrProvDaRecNull(), Len.DTR_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PROV_DA_REC = 1;
        public static final int DTR_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PROV_DA_REC = 8;
        public static final int DTR_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
