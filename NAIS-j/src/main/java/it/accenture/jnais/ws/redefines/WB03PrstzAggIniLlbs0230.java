package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRSTZ-AGG-INI<br>
 * Variable: W-B03-PRSTZ-AGG-INI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PrstzAggIniLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PrstzAggIniLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRSTZ_AGG_INI;
    }

    public void setwB03PrstzAggIni(AfDecimal wB03PrstzAggIni) {
        writeDecimalAsPacked(Pos.W_B03_PRSTZ_AGG_INI, wB03PrstzAggIni.copy());
    }

    /**Original name: W-B03-PRSTZ-AGG-INI<br>*/
    public AfDecimal getwB03PrstzAggIni() {
        return readPackedAsDecimal(Pos.W_B03_PRSTZ_AGG_INI, Len.Int.W_B03_PRSTZ_AGG_INI, Len.Fract.W_B03_PRSTZ_AGG_INI);
    }

    public byte[] getwB03PrstzAggIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRSTZ_AGG_INI, Pos.W_B03_PRSTZ_AGG_INI);
        return buffer;
    }

    public void setwB03PrstzAggIniNull(String wB03PrstzAggIniNull) {
        writeString(Pos.W_B03_PRSTZ_AGG_INI_NULL, wB03PrstzAggIniNull, Len.W_B03_PRSTZ_AGG_INI_NULL);
    }

    /**Original name: W-B03-PRSTZ-AGG-INI-NULL<br>*/
    public String getwB03PrstzAggIniNull() {
        return readString(Pos.W_B03_PRSTZ_AGG_INI_NULL, Len.W_B03_PRSTZ_AGG_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRSTZ_AGG_INI = 1;
        public static final int W_B03_PRSTZ_AGG_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRSTZ_AGG_INI = 8;
        public static final int W_B03_PRSTZ_AGG_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRSTZ_AGG_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRSTZ_AGG_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
