package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-MOVI-NEG<br>
 * Variable: WPAG-IMP-MOVI-NEG from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpMoviNeg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpMoviNeg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_MOVI_NEG;
    }

    public void setWpagImpMoviNeg(AfDecimal wpagImpMoviNeg) {
        writeDecimalAsPacked(Pos.WPAG_IMP_MOVI_NEG, wpagImpMoviNeg.copy());
    }

    public void setWpagImpMoviNegFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_MOVI_NEG, Pos.WPAG_IMP_MOVI_NEG);
    }

    /**Original name: WPAG-IMP-MOVI-NEG<br>*/
    public AfDecimal getWpagImpMoviNeg() {
        return readPackedAsDecimal(Pos.WPAG_IMP_MOVI_NEG, Len.Int.WPAG_IMP_MOVI_NEG, Len.Fract.WPAG_IMP_MOVI_NEG);
    }

    public byte[] getWpagImpMoviNegAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_MOVI_NEG, Pos.WPAG_IMP_MOVI_NEG);
        return buffer;
    }

    public void initWpagImpMoviNegSpaces() {
        fill(Pos.WPAG_IMP_MOVI_NEG, Len.WPAG_IMP_MOVI_NEG, Types.SPACE_CHAR);
    }

    public void setWpagImpMoviNegNull(String wpagImpMoviNegNull) {
        writeString(Pos.WPAG_IMP_MOVI_NEG_NULL, wpagImpMoviNegNull, Len.WPAG_IMP_MOVI_NEG_NULL);
    }

    /**Original name: WPAG-IMP-MOVI-NEG-NULL<br>*/
    public String getWpagImpMoviNegNull() {
        return readString(Pos.WPAG_IMP_MOVI_NEG_NULL, Len.WPAG_IMP_MOVI_NEG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_MOVI_NEG = 1;
        public static final int WPAG_IMP_MOVI_NEG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_MOVI_NEG = 8;
        public static final int WPAG_IMP_MOVI_NEG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_MOVI_NEG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_MOVI_NEG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
