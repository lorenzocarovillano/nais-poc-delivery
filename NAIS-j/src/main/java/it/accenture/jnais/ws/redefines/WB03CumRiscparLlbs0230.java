package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CUM-RISCPAR<br>
 * Variable: W-B03-CUM-RISCPAR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CumRiscparLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CumRiscparLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CUM_RISCPAR;
    }

    public void setwB03CumRiscpar(AfDecimal wB03CumRiscpar) {
        writeDecimalAsPacked(Pos.W_B03_CUM_RISCPAR, wB03CumRiscpar.copy());
    }

    /**Original name: W-B03-CUM-RISCPAR<br>*/
    public AfDecimal getwB03CumRiscpar() {
        return readPackedAsDecimal(Pos.W_B03_CUM_RISCPAR, Len.Int.W_B03_CUM_RISCPAR, Len.Fract.W_B03_CUM_RISCPAR);
    }

    public byte[] getwB03CumRiscparAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CUM_RISCPAR, Pos.W_B03_CUM_RISCPAR);
        return buffer;
    }

    public void setwB03CumRiscparNull(String wB03CumRiscparNull) {
        writeString(Pos.W_B03_CUM_RISCPAR_NULL, wB03CumRiscparNull, Len.W_B03_CUM_RISCPAR_NULL);
    }

    /**Original name: W-B03-CUM-RISCPAR-NULL<br>*/
    public String getwB03CumRiscparNull() {
        return readString(Pos.W_B03_CUM_RISCPAR_NULL, Len.W_B03_CUM_RISCPAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CUM_RISCPAR = 1;
        public static final int W_B03_CUM_RISCPAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CUM_RISCPAR = 8;
        public static final int W_B03_CUM_RISCPAR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CUM_RISCPAR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CUM_RISCPAR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
