package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-LETTURA-P58<br>
 * Variable: FLAG-FINE-LETTURA-P58 from program LVVS2720<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineLetturaP58 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagFineLetturaP58(char flagFineLetturaP58) {
        this.value = flagFineLetturaP58;
    }

    public char getFlagFineLetturaP58() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
