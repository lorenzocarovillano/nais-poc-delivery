package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-TRASFE<br>
 * Variable: TDR-IMP-TRASFE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_TRASFE;
    }

    public void setTdrImpTrasfe(AfDecimal tdrImpTrasfe) {
        writeDecimalAsPacked(Pos.TDR_IMP_TRASFE, tdrImpTrasfe.copy());
    }

    public void setTdrImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_TRASFE, Pos.TDR_IMP_TRASFE);
    }

    /**Original name: TDR-IMP-TRASFE<br>*/
    public AfDecimal getTdrImpTrasfe() {
        return readPackedAsDecimal(Pos.TDR_IMP_TRASFE, Len.Int.TDR_IMP_TRASFE, Len.Fract.TDR_IMP_TRASFE);
    }

    public byte[] getTdrImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_TRASFE, Pos.TDR_IMP_TRASFE);
        return buffer;
    }

    public void setTdrImpTrasfeNull(String tdrImpTrasfeNull) {
        writeString(Pos.TDR_IMP_TRASFE_NULL, tdrImpTrasfeNull, Len.TDR_IMP_TRASFE_NULL);
    }

    /**Original name: TDR-IMP-TRASFE-NULL<br>*/
    public String getTdrImpTrasfeNull() {
        return readString(Pos.TDR_IMP_TRASFE_NULL, Len.TDR_IMP_TRASFE_NULL);
    }

    public String getTdrImpTrasfeNullFormatted() {
        return Functions.padBlanks(getTdrImpTrasfeNull(), Len.TDR_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_TRASFE = 1;
        public static final int TDR_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_TRASFE = 8;
        public static final int TDR_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
