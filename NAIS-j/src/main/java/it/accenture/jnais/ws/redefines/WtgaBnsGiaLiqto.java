package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-BNS-GIA-LIQTO<br>
 * Variable: WTGA-BNS-GIA-LIQTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaBnsGiaLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaBnsGiaLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_BNS_GIA_LIQTO;
    }

    public void setWtgaBnsGiaLiqto(AfDecimal wtgaBnsGiaLiqto) {
        writeDecimalAsPacked(Pos.WTGA_BNS_GIA_LIQTO, wtgaBnsGiaLiqto.copy());
    }

    public void setWtgaBnsGiaLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_BNS_GIA_LIQTO, Pos.WTGA_BNS_GIA_LIQTO);
    }

    /**Original name: WTGA-BNS-GIA-LIQTO<br>*/
    public AfDecimal getWtgaBnsGiaLiqto() {
        return readPackedAsDecimal(Pos.WTGA_BNS_GIA_LIQTO, Len.Int.WTGA_BNS_GIA_LIQTO, Len.Fract.WTGA_BNS_GIA_LIQTO);
    }

    public byte[] getWtgaBnsGiaLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_BNS_GIA_LIQTO, Pos.WTGA_BNS_GIA_LIQTO);
        return buffer;
    }

    public void initWtgaBnsGiaLiqtoSpaces() {
        fill(Pos.WTGA_BNS_GIA_LIQTO, Len.WTGA_BNS_GIA_LIQTO, Types.SPACE_CHAR);
    }

    public void setWtgaBnsGiaLiqtoNull(String wtgaBnsGiaLiqtoNull) {
        writeString(Pos.WTGA_BNS_GIA_LIQTO_NULL, wtgaBnsGiaLiqtoNull, Len.WTGA_BNS_GIA_LIQTO_NULL);
    }

    /**Original name: WTGA-BNS-GIA-LIQTO-NULL<br>*/
    public String getWtgaBnsGiaLiqtoNull() {
        return readString(Pos.WTGA_BNS_GIA_LIQTO_NULL, Len.WTGA_BNS_GIA_LIQTO_NULL);
    }

    public String getWtgaBnsGiaLiqtoNullFormatted() {
        return Functions.padBlanks(getWtgaBnsGiaLiqtoNull(), Len.WTGA_BNS_GIA_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_BNS_GIA_LIQTO = 1;
        public static final int WTGA_BNS_GIA_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_BNS_GIA_LIQTO = 8;
        public static final int WTGA_BNS_GIA_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_BNS_GIA_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_BNS_GIA_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
