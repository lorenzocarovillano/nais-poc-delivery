package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SOPR-PROF<br>
 * Variable: WTDR-TOT-SOPR-PROF from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SOPR_PROF;
    }

    public void setWtdrTotSoprProf(AfDecimal wtdrTotSoprProf) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SOPR_PROF, wtdrTotSoprProf.copy());
    }

    /**Original name: WTDR-TOT-SOPR-PROF<br>*/
    public AfDecimal getWtdrTotSoprProf() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SOPR_PROF, Len.Int.WTDR_TOT_SOPR_PROF, Len.Fract.WTDR_TOT_SOPR_PROF);
    }

    public void setWtdrTotSoprProfNull(String wtdrTotSoprProfNull) {
        writeString(Pos.WTDR_TOT_SOPR_PROF_NULL, wtdrTotSoprProfNull, Len.WTDR_TOT_SOPR_PROF_NULL);
    }

    /**Original name: WTDR-TOT-SOPR-PROF-NULL<br>*/
    public String getWtdrTotSoprProfNull() {
        return readString(Pos.WTDR_TOT_SOPR_PROF_NULL, Len.WTDR_TOT_SOPR_PROF_NULL);
    }

    public String getWtdrTotSoprProfNullFormatted() {
        return Functions.padBlanks(getWtdrTotSoprProfNull(), Len.WTDR_TOT_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_PROF = 1;
        public static final int WTDR_TOT_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_PROF = 8;
        public static final int WTDR_TOT_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
