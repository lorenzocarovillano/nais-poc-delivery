package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-PA-ESP-MSC-1<br>
 * Variable: P56-IMP-PA-ESP-MSC-1 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpPaEspMsc1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpPaEspMsc1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_PA_ESP_MSC1;
    }

    public void setP56ImpPaEspMsc1(AfDecimal p56ImpPaEspMsc1) {
        writeDecimalAsPacked(Pos.P56_IMP_PA_ESP_MSC1, p56ImpPaEspMsc1.copy());
    }

    public void setP56ImpPaEspMsc1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC1, Pos.P56_IMP_PA_ESP_MSC1);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-1<br>*/
    public AfDecimal getP56ImpPaEspMsc1() {
        return readPackedAsDecimal(Pos.P56_IMP_PA_ESP_MSC1, Len.Int.P56_IMP_PA_ESP_MSC1, Len.Fract.P56_IMP_PA_ESP_MSC1);
    }

    public byte[] getP56ImpPaEspMsc1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC1, Pos.P56_IMP_PA_ESP_MSC1);
        return buffer;
    }

    public void setP56ImpPaEspMsc1Null(String p56ImpPaEspMsc1Null) {
        writeString(Pos.P56_IMP_PA_ESP_MSC1_NULL, p56ImpPaEspMsc1Null, Len.P56_IMP_PA_ESP_MSC1_NULL);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-1-NULL<br>*/
    public String getP56ImpPaEspMsc1Null() {
        return readString(Pos.P56_IMP_PA_ESP_MSC1_NULL, Len.P56_IMP_PA_ESP_MSC1_NULL);
    }

    public String getP56ImpPaEspMsc1NullFormatted() {
        return Functions.padBlanks(getP56ImpPaEspMsc1Null(), Len.P56_IMP_PA_ESP_MSC1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC1 = 1;
        public static final int P56_IMP_PA_ESP_MSC1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC1 = 8;
        public static final int P56_IMP_PA_ESP_MSC1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
