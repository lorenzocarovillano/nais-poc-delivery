package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PRE-SOLO-RSH<br>
 * Variable: TIT-TOT-PRE-SOLO-RSH from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PRE_SOLO_RSH;
    }

    public void setTitTotPreSoloRsh(AfDecimal titTotPreSoloRsh) {
        writeDecimalAsPacked(Pos.TIT_TOT_PRE_SOLO_RSH, titTotPreSoloRsh.copy());
    }

    public void setTitTotPreSoloRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PRE_SOLO_RSH, Pos.TIT_TOT_PRE_SOLO_RSH);
    }

    /**Original name: TIT-TOT-PRE-SOLO-RSH<br>*/
    public AfDecimal getTitTotPreSoloRsh() {
        return readPackedAsDecimal(Pos.TIT_TOT_PRE_SOLO_RSH, Len.Int.TIT_TOT_PRE_SOLO_RSH, Len.Fract.TIT_TOT_PRE_SOLO_RSH);
    }

    public byte[] getTitTotPreSoloRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PRE_SOLO_RSH, Pos.TIT_TOT_PRE_SOLO_RSH);
        return buffer;
    }

    public void setTitTotPreSoloRshNull(String titTotPreSoloRshNull) {
        writeString(Pos.TIT_TOT_PRE_SOLO_RSH_NULL, titTotPreSoloRshNull, Len.TIT_TOT_PRE_SOLO_RSH_NULL);
    }

    /**Original name: TIT-TOT-PRE-SOLO-RSH-NULL<br>*/
    public String getTitTotPreSoloRshNull() {
        return readString(Pos.TIT_TOT_PRE_SOLO_RSH_NULL, Len.TIT_TOT_PRE_SOLO_RSH_NULL);
    }

    public String getTitTotPreSoloRshNullFormatted() {
        return Functions.padBlanks(getTitTotPreSoloRshNull(), Len.TIT_TOT_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_SOLO_RSH = 1;
        public static final int TIT_TOT_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PRE_SOLO_RSH = 8;
        public static final int TIT_TOT_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
