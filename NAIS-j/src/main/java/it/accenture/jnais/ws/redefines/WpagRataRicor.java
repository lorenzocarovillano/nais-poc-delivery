package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-RICOR<br>
 * Variable: WPAG-RATA-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_RICOR;
    }

    public void setWpagRataRicor(AfDecimal wpagRataRicor) {
        writeDecimalAsPacked(Pos.WPAG_RATA_RICOR, wpagRataRicor.copy());
    }

    public void setWpagRataRicorFormatted(String wpagRataRicor) {
        setWpagRataRicor(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_RICOR + Len.Fract.WPAG_RATA_RICOR, Len.Fract.WPAG_RATA_RICOR, wpagRataRicor));
    }

    public void setWpagRataRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_RICOR, Pos.WPAG_RATA_RICOR);
    }

    /**Original name: WPAG-RATA-RICOR<br>*/
    public AfDecimal getWpagRataRicor() {
        return readPackedAsDecimal(Pos.WPAG_RATA_RICOR, Len.Int.WPAG_RATA_RICOR, Len.Fract.WPAG_RATA_RICOR);
    }

    public byte[] getWpagRataRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_RICOR, Pos.WPAG_RATA_RICOR);
        return buffer;
    }

    public void initWpagRataRicorSpaces() {
        fill(Pos.WPAG_RATA_RICOR, Len.WPAG_RATA_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagRataRicorNull(String wpagRataRicorNull) {
        writeString(Pos.WPAG_RATA_RICOR_NULL, wpagRataRicorNull, Len.WPAG_RATA_RICOR_NULL);
    }

    /**Original name: WPAG-RATA-RICOR-NULL<br>*/
    public String getWpagRataRicorNull() {
        return readString(Pos.WPAG_RATA_RICOR_NULL, Len.WPAG_RATA_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_RICOR = 1;
        public static final int WPAG_RATA_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_RICOR = 8;
        public static final int WPAG_RATA_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
