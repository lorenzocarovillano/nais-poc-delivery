package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-ADER<br>
 * Variable: WADE-IMP-ADER from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_ADER;
    }

    public void setWadeImpAder(AfDecimal wadeImpAder) {
        writeDecimalAsPacked(Pos.WADE_IMP_ADER, wadeImpAder.copy());
    }

    public void setWadeImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_ADER, Pos.WADE_IMP_ADER);
    }

    /**Original name: WADE-IMP-ADER<br>*/
    public AfDecimal getWadeImpAder() {
        return readPackedAsDecimal(Pos.WADE_IMP_ADER, Len.Int.WADE_IMP_ADER, Len.Fract.WADE_IMP_ADER);
    }

    public byte[] getWadeImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_ADER, Pos.WADE_IMP_ADER);
        return buffer;
    }

    public void initWadeImpAderSpaces() {
        fill(Pos.WADE_IMP_ADER, Len.WADE_IMP_ADER, Types.SPACE_CHAR);
    }

    public void setWadeImpAderNull(String wadeImpAderNull) {
        writeString(Pos.WADE_IMP_ADER_NULL, wadeImpAderNull, Len.WADE_IMP_ADER_NULL);
    }

    /**Original name: WADE-IMP-ADER-NULL<br>*/
    public String getWadeImpAderNull() {
        return readString(Pos.WADE_IMP_ADER_NULL, Len.WADE_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_ADER = 1;
        public static final int WADE_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_ADER = 8;
        public static final int WADE_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
