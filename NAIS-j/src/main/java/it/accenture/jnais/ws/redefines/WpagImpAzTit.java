package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-AZ-TIT<br>
 * Variable: WPAG-IMP-AZ-TIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAzTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAzTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_AZ_TIT;
    }

    public void setWpagImpAzTit(AfDecimal wpagImpAzTit) {
        writeDecimalAsPacked(Pos.WPAG_IMP_AZ_TIT, wpagImpAzTit.copy());
    }

    public void setWpagImpAzTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_AZ_TIT, Pos.WPAG_IMP_AZ_TIT);
    }

    /**Original name: WPAG-IMP-AZ-TIT<br>*/
    public AfDecimal getWpagImpAzTit() {
        return readPackedAsDecimal(Pos.WPAG_IMP_AZ_TIT, Len.Int.WPAG_IMP_AZ_TIT, Len.Fract.WPAG_IMP_AZ_TIT);
    }

    public byte[] getWpagImpAzTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_AZ_TIT, Pos.WPAG_IMP_AZ_TIT);
        return buffer;
    }

    public void initWpagImpAzTitSpaces() {
        fill(Pos.WPAG_IMP_AZ_TIT, Len.WPAG_IMP_AZ_TIT, Types.SPACE_CHAR);
    }

    public void setWpagImpAzTitNull(String wpagImpAzTitNull) {
        writeString(Pos.WPAG_IMP_AZ_TIT_NULL, wpagImpAzTitNull, Len.WPAG_IMP_AZ_TIT_NULL);
    }

    /**Original name: WPAG-IMP-AZ-TIT-NULL<br>*/
    public String getWpagImpAzTitNull() {
        return readString(Pos.WPAG_IMP_AZ_TIT_NULL, Len.WPAG_IMP_AZ_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_AZ_TIT = 1;
        public static final int WPAG_IMP_AZ_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_AZ_TIT = 8;
        public static final int WPAG_IMP_AZ_TIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_AZ_TIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_AZ_TIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
