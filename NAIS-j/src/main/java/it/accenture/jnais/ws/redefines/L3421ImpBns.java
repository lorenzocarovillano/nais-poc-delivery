package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-BNS<br>
 * Variable: L3421-IMP-BNS from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_BNS;
    }

    public void setL3421ImpBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_BNS, Pos.L3421_IMP_BNS);
    }

    /**Original name: L3421-IMP-BNS<br>*/
    public AfDecimal getL3421ImpBns() {
        return readPackedAsDecimal(Pos.L3421_IMP_BNS, Len.Int.L3421_IMP_BNS, Len.Fract.L3421_IMP_BNS);
    }

    public byte[] getL3421ImpBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_BNS, Pos.L3421_IMP_BNS);
        return buffer;
    }

    /**Original name: L3421-IMP-BNS-NULL<br>*/
    public String getL3421ImpBnsNull() {
        return readString(Pos.L3421_IMP_BNS_NULL, Len.L3421_IMP_BNS_NULL);
    }

    public String getL3421ImpBnsNullFormatted() {
        return Functions.padBlanks(getL3421ImpBnsNull(), Len.L3421_IMP_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_BNS = 1;
        public static final int L3421_IMP_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_BNS = 8;
        public static final int L3421_IMP_BNS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_BNS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
