package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.WkDtRicorTrancheLvvs0135;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LVVS0135<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabiliLvvs0135 {

    //==== PROPERTIES ====
    //Original name: WK-DT-RICOR-TRANCHE
    private WkDtRicorTrancheLvvs0135 wkDtRicorTranche = new WkDtRicorTrancheLvvs0135();
    //Original name: WK-ANNO
    private String wkAnno = DefaultValues.stringVal(Len.WK_ANNO);
    //Original name: WK-DATA-APPO-AA
    private String wkDataAppoAa = DefaultValues.stringVal(Len.WK_DATA_APPO_AA);
    //Original name: WK-DATA-APPO-MM
    private String wkDataAppoMm = DefaultValues.stringVal(Len.WK_DATA_APPO_MM);
    //Original name: WK-DATA-APPO-GG
    private String wkDataAppoGg = DefaultValues.stringVal(Len.WK_DATA_APPO_GG);

    //==== METHODS ====
    public void setWkAnno(short wkAnno) {
        this.wkAnno = NumericDisplay.asString(wkAnno, Len.WK_ANNO);
    }

    public void setWkAnnoFormatted(String wkAnno) {
        this.wkAnno = Trunc.toUnsignedNumeric(wkAnno, Len.WK_ANNO);
    }

    public short getWkAnno() {
        return NumericDisplay.asShort(this.wkAnno);
    }

    public String getWkAnnoFormatted() {
        return this.wkAnno;
    }

    public void setWkDataAppoFormatted(String data) {
        byte[] buffer = new byte[Len.WK_DATA_APPO];
        MarshalByte.writeString(buffer, 1, data, Len.WK_DATA_APPO);
        setWkDataAppoBytes(buffer, 1);
    }

    public void setWkDataAppoBytes(byte[] buffer, int offset) {
        int position = offset;
        wkDataAppoAa = MarshalByte.readFixedString(buffer, position, Len.WK_DATA_APPO_AA);
        position += Len.WK_DATA_APPO_AA;
        wkDataAppoMm = MarshalByte.readFixedString(buffer, position, Len.WK_DATA_APPO_MM);
        position += Len.WK_DATA_APPO_MM;
        wkDataAppoGg = MarshalByte.readFixedString(buffer, position, Len.WK_DATA_APPO_GG);
    }

    public void setWkDataAppoAaFormatted(String wkDataAppoAa) {
        this.wkDataAppoAa = Trunc.toUnsignedNumeric(wkDataAppoAa, Len.WK_DATA_APPO_AA);
    }

    public short getWkDataAppoAa() {
        return NumericDisplay.asShort(this.wkDataAppoAa);
    }

    public String getWkDataAppoAaFormatted() {
        return this.wkDataAppoAa;
    }

    public void setWkDataAppoMmFormatted(String wkDataAppoMm) {
        this.wkDataAppoMm = Trunc.toUnsignedNumeric(wkDataAppoMm, Len.WK_DATA_APPO_MM);
    }

    public short getWkDataAppoMm() {
        return NumericDisplay.asShort(this.wkDataAppoMm);
    }

    public String getWkDataAppoMmFormatted() {
        return this.wkDataAppoMm;
    }

    public void setWkDataAppoGgFormatted(String wkDataAppoGg) {
        this.wkDataAppoGg = Trunc.toUnsignedNumeric(wkDataAppoGg, Len.WK_DATA_APPO_GG);
    }

    public short getWkDataAppoGg() {
        return NumericDisplay.asShort(this.wkDataAppoGg);
    }

    public String getWkDataAppoGgFormatted() {
        return this.wkDataAppoGg;
    }

    public WkDtRicorTrancheLvvs0135 getWkDtRicorTranche() {
        return wkDtRicorTranche;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ANNO = 4;
        public static final int WK_DATA_APPO_AA = 4;
        public static final int WK_DATA_APPO_MM = 2;
        public static final int WK_DATA_APPO_GG = 2;
        public static final int WK_DATA_APPO = WK_DATA_APPO_AA + WK_DATA_APPO_MM + WK_DATA_APPO_GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
