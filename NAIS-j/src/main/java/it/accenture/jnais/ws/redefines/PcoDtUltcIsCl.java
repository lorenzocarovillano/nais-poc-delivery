package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-IS-CL<br>
 * Variable: PCO-DT-ULTC-IS-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcIsCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcIsCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_IS_CL;
    }

    public void setPcoDtUltcIsCl(int pcoDtUltcIsCl) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_IS_CL, pcoDtUltcIsCl, Len.Int.PCO_DT_ULTC_IS_CL);
    }

    public void setPcoDtUltcIsClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_IS_CL, Pos.PCO_DT_ULTC_IS_CL);
    }

    /**Original name: PCO-DT-ULTC-IS-CL<br>*/
    public int getPcoDtUltcIsCl() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_IS_CL, Len.Int.PCO_DT_ULTC_IS_CL);
    }

    public byte[] getPcoDtUltcIsClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_IS_CL, Pos.PCO_DT_ULTC_IS_CL);
        return buffer;
    }

    public void initPcoDtUltcIsClHighValues() {
        fill(Pos.PCO_DT_ULTC_IS_CL, Len.PCO_DT_ULTC_IS_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcIsClNull(String pcoDtUltcIsClNull) {
        writeString(Pos.PCO_DT_ULTC_IS_CL_NULL, pcoDtUltcIsClNull, Len.PCO_DT_ULTC_IS_CL_NULL);
    }

    /**Original name: PCO-DT-ULTC-IS-CL-NULL<br>*/
    public String getPcoDtUltcIsClNull() {
        return readString(Pos.PCO_DT_ULTC_IS_CL_NULL, Len.PCO_DT_ULTC_IS_CL_NULL);
    }

    public String getPcoDtUltcIsClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcIsClNull(), Len.PCO_DT_ULTC_IS_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_IS_CL = 1;
        public static final int PCO_DT_ULTC_IS_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_IS_CL = 5;
        public static final int PCO_DT_ULTC_IS_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_IS_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
