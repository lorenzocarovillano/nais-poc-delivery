package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-COS-RUN-ASSVA-IDC<br>
 * Variable: RDF-COS-RUN-ASSVA-IDC from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfCosRunAssvaIdc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfCosRunAssvaIdc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_COS_RUN_ASSVA_IDC;
    }

    public void setRdfCosRunAssvaIdc(AfDecimal rdfCosRunAssvaIdc) {
        writeDecimalAsPacked(Pos.RDF_COS_RUN_ASSVA_IDC, rdfCosRunAssvaIdc.copy());
    }

    public void setRdfCosRunAssvaIdcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_COS_RUN_ASSVA_IDC, Pos.RDF_COS_RUN_ASSVA_IDC);
    }

    /**Original name: RDF-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getRdfCosRunAssvaIdc() {
        return readPackedAsDecimal(Pos.RDF_COS_RUN_ASSVA_IDC, Len.Int.RDF_COS_RUN_ASSVA_IDC, Len.Fract.RDF_COS_RUN_ASSVA_IDC);
    }

    public byte[] getRdfCosRunAssvaIdcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_COS_RUN_ASSVA_IDC, Pos.RDF_COS_RUN_ASSVA_IDC);
        return buffer;
    }

    public void setRdfCosRunAssvaIdcNull(String rdfCosRunAssvaIdcNull) {
        writeString(Pos.RDF_COS_RUN_ASSVA_IDC_NULL, rdfCosRunAssvaIdcNull, Len.RDF_COS_RUN_ASSVA_IDC_NULL);
    }

    /**Original name: RDF-COS-RUN-ASSVA-IDC-NULL<br>*/
    public String getRdfCosRunAssvaIdcNull() {
        return readString(Pos.RDF_COS_RUN_ASSVA_IDC_NULL, Len.RDF_COS_RUN_ASSVA_IDC_NULL);
    }

    public String getRdfCosRunAssvaIdcNullFormatted() {
        return Functions.padBlanks(getRdfCosRunAssvaIdcNull(), Len.RDF_COS_RUN_ASSVA_IDC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_COS_RUN_ASSVA_IDC = 1;
        public static final int RDF_COS_RUN_ASSVA_IDC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_COS_RUN_ASSVA_IDC = 8;
        public static final int RDF_COS_RUN_ASSVA_IDC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
