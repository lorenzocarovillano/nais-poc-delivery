package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Lccc0024IdBatch;
import it.accenture.jnais.ws.redefines.Lccc0024IdJob;
import it.accenture.jnais.ws.redefines.Lccc0024TpMoviSosp;

/**Original name: LCCC0024-DATI-MOV-SOSPESI<br>
 * Variables: LCCC0024-DATI-MOV-SOSPESI from copybook LCCC0024<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0024DatiMovSospesi {

    //==== PROPERTIES ====
    //Original name: LCCC0024-ID-OGG-SOSP
    private int lccc0024IdOggSosp = DefaultValues.INT_VAL;
    //Original name: LCCC0024-TP-OGG-SOSP
    private String lccc0024TpOggSosp = DefaultValues.stringVal(Len.LCCC0024_TP_OGG_SOSP);
    //Original name: LCCC0024-ID-MOVI-SOSP
    private int lccc0024IdMoviSosp = DefaultValues.INT_VAL;
    //Original name: LCCC0024-TP-MOVI-SOSP
    private Lccc0024TpMoviSosp lccc0024TpMoviSosp = new Lccc0024TpMoviSosp();
    //Original name: LCCC0024-TP-FRM-ASSVA
    private String lccc0024TpFrmAssva = DefaultValues.stringVal(Len.LCCC0024_TP_FRM_ASSVA);
    /**Original name: LCCC0024-ID-BATCH<br>
	 * <pre>          07 LCCC0024-PER-ELAB-DA         PIC S9(08)V COMP-3.
	 *           07 LCCC0024-PER-ELAB-A          PIC S9(08)V COMP-3.</pre>*/
    private Lccc0024IdBatch lccc0024IdBatch = new Lccc0024IdBatch();
    //Original name: LCCC0024-ID-JOB
    private Lccc0024IdJob lccc0024IdJob = new Lccc0024IdJob();
    //Original name: LCCC0024-STEP-ELAB
    private char lccc0024StepElab = DefaultValues.CHAR_VAL;
    //Original name: LCCC0024-D-INPUT-MOVI-SOSP
    private String lccc0024DInputMoviSosp = DefaultValues.stringVal(Len.LCCC0024_D_INPUT_MOVI_SOSP);

    //==== METHODS ====
    public void setLccc0024DatiMovSospesiBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0024IdOggSosp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0024_ID_OGG_SOSP, 0);
        position += Len.LCCC0024_ID_OGG_SOSP;
        lccc0024TpOggSosp = MarshalByte.readString(buffer, position, Len.LCCC0024_TP_OGG_SOSP);
        position += Len.LCCC0024_TP_OGG_SOSP;
        lccc0024IdMoviSosp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0024_ID_MOVI_SOSP, 0);
        position += Len.LCCC0024_ID_MOVI_SOSP;
        lccc0024TpMoviSosp.setLccc0024TpMoviSospFromBuffer(buffer, position);
        position += Lccc0024TpMoviSosp.Len.LCCC0024_TP_MOVI_SOSP;
        lccc0024TpFrmAssva = MarshalByte.readString(buffer, position, Len.LCCC0024_TP_FRM_ASSVA);
        position += Len.LCCC0024_TP_FRM_ASSVA;
        lccc0024IdBatch.setLccc0024IdBatchFromBuffer(buffer, position);
        position += Lccc0024IdBatch.Len.LCCC0024_ID_BATCH;
        lccc0024IdJob.setLccc0024IdJobFromBuffer(buffer, position);
        position += Lccc0024IdJob.Len.LCCC0024_ID_JOB;
        lccc0024StepElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        lccc0024DInputMoviSosp = MarshalByte.readString(buffer, position, Len.LCCC0024_D_INPUT_MOVI_SOSP);
    }

    public byte[] getLccc0024DatiMovSospesiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0024IdOggSosp, Len.Int.LCCC0024_ID_OGG_SOSP, 0);
        position += Len.LCCC0024_ID_OGG_SOSP;
        MarshalByte.writeString(buffer, position, lccc0024TpOggSosp, Len.LCCC0024_TP_OGG_SOSP);
        position += Len.LCCC0024_TP_OGG_SOSP;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0024IdMoviSosp, Len.Int.LCCC0024_ID_MOVI_SOSP, 0);
        position += Len.LCCC0024_ID_MOVI_SOSP;
        lccc0024TpMoviSosp.getLccc0024TpMoviSospAsBuffer(buffer, position);
        position += Lccc0024TpMoviSosp.Len.LCCC0024_TP_MOVI_SOSP;
        MarshalByte.writeString(buffer, position, lccc0024TpFrmAssva, Len.LCCC0024_TP_FRM_ASSVA);
        position += Len.LCCC0024_TP_FRM_ASSVA;
        lccc0024IdBatch.getLccc0024IdBatchAsBuffer(buffer, position);
        position += Lccc0024IdBatch.Len.LCCC0024_ID_BATCH;
        lccc0024IdJob.getLccc0024IdJobAsBuffer(buffer, position);
        position += Lccc0024IdJob.Len.LCCC0024_ID_JOB;
        MarshalByte.writeChar(buffer, position, lccc0024StepElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, lccc0024DInputMoviSosp, Len.LCCC0024_D_INPUT_MOVI_SOSP);
        return buffer;
    }

    public void initLccc0024DatiMovSospesiSpaces() {
        lccc0024IdOggSosp = Types.INVALID_INT_VAL;
        lccc0024TpOggSosp = "";
        lccc0024IdMoviSosp = Types.INVALID_INT_VAL;
        lccc0024TpMoviSosp.initLccc0024TpMoviSospSpaces();
        lccc0024TpFrmAssva = "";
        lccc0024IdBatch.initLccc0024IdBatchSpaces();
        lccc0024IdJob.initLccc0024IdJobSpaces();
        lccc0024StepElab = Types.SPACE_CHAR;
        lccc0024DInputMoviSosp = "";
    }

    public void setLccc0024IdOggSosp(int lccc0024IdOggSosp) {
        this.lccc0024IdOggSosp = lccc0024IdOggSosp;
    }

    public int getLccc0024IdOggSosp() {
        return this.lccc0024IdOggSosp;
    }

    public void setLccc0024TpOggSosp(String lccc0024TpOggSosp) {
        this.lccc0024TpOggSosp = Functions.subString(lccc0024TpOggSosp, Len.LCCC0024_TP_OGG_SOSP);
    }

    public String getLccc0024TpOggSosp() {
        return this.lccc0024TpOggSosp;
    }

    public void setLccc0024IdMoviSosp(int lccc0024IdMoviSosp) {
        this.lccc0024IdMoviSosp = lccc0024IdMoviSosp;
    }

    public int getLccc0024IdMoviSosp() {
        return this.lccc0024IdMoviSosp;
    }

    public void setLccc0024TpFrmAssva(String lccc0024TpFrmAssva) {
        this.lccc0024TpFrmAssva = Functions.subString(lccc0024TpFrmAssva, Len.LCCC0024_TP_FRM_ASSVA);
    }

    public String getLccc0024TpFrmAssva() {
        return this.lccc0024TpFrmAssva;
    }

    public void setLccc0024StepElab(char lccc0024StepElab) {
        this.lccc0024StepElab = lccc0024StepElab;
    }

    public char getLccc0024StepElab() {
        return this.lccc0024StepElab;
    }

    public void setLccc0024DInputMoviSosp(String lccc0024DInputMoviSosp) {
        this.lccc0024DInputMoviSosp = Functions.subString(lccc0024DInputMoviSosp, Len.LCCC0024_D_INPUT_MOVI_SOSP);
    }

    public String getLccc0024DInputMoviSosp() {
        return this.lccc0024DInputMoviSosp;
    }

    public Lccc0024IdBatch getLccc0024IdBatch() {
        return lccc0024IdBatch;
    }

    public Lccc0024IdJob getLccc0024IdJob() {
        return lccc0024IdJob;
    }

    public Lccc0024TpMoviSosp getLccc0024TpMoviSosp() {
        return lccc0024TpMoviSosp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0024_ID_OGG_SOSP = 5;
        public static final int LCCC0024_TP_OGG_SOSP = 2;
        public static final int LCCC0024_ID_MOVI_SOSP = 5;
        public static final int LCCC0024_TP_FRM_ASSVA = 2;
        public static final int LCCC0024_STEP_ELAB = 1;
        public static final int LCCC0024_D_INPUT_MOVI_SOSP = 300;
        public static final int LCCC0024_DATI_MOV_SOSPESI = LCCC0024_ID_OGG_SOSP + LCCC0024_TP_OGG_SOSP + LCCC0024_ID_MOVI_SOSP + Lccc0024TpMoviSosp.Len.LCCC0024_TP_MOVI_SOSP + LCCC0024_TP_FRM_ASSVA + Lccc0024IdBatch.Len.LCCC0024_ID_BATCH + Lccc0024IdJob.Len.LCCC0024_ID_JOB + LCCC0024_STEP_ELAB + LCCC0024_D_INPUT_MOVI_SOSP;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0024_ID_OGG_SOSP = 9;
            public static final int LCCC0024_ID_MOVI_SOSP = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
