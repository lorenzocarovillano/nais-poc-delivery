package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-TARI-INI<br>
 * Variable: WTGA-PRE-TARI-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreTariIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreTariIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_TARI_INI;
    }

    public void setWtgaPreTariIni(AfDecimal wtgaPreTariIni) {
        writeDecimalAsPacked(Pos.WTGA_PRE_TARI_INI, wtgaPreTariIni.copy());
    }

    public void setWtgaPreTariIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_TARI_INI, Pos.WTGA_PRE_TARI_INI);
    }

    /**Original name: WTGA-PRE-TARI-INI<br>*/
    public AfDecimal getWtgaPreTariIni() {
        return readPackedAsDecimal(Pos.WTGA_PRE_TARI_INI, Len.Int.WTGA_PRE_TARI_INI, Len.Fract.WTGA_PRE_TARI_INI);
    }

    public byte[] getWtgaPreTariIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_TARI_INI, Pos.WTGA_PRE_TARI_INI);
        return buffer;
    }

    public void initWtgaPreTariIniSpaces() {
        fill(Pos.WTGA_PRE_TARI_INI, Len.WTGA_PRE_TARI_INI, Types.SPACE_CHAR);
    }

    public void setWtgaPreTariIniNull(String wtgaPreTariIniNull) {
        writeString(Pos.WTGA_PRE_TARI_INI_NULL, wtgaPreTariIniNull, Len.WTGA_PRE_TARI_INI_NULL);
    }

    /**Original name: WTGA-PRE-TARI-INI-NULL<br>*/
    public String getWtgaPreTariIniNull() {
        return readString(Pos.WTGA_PRE_TARI_INI_NULL, Len.WTGA_PRE_TARI_INI_NULL);
    }

    public String getWtgaPreTariIniNullFormatted() {
        return Functions.padBlanks(getWtgaPreTariIniNull(), Len.WTGA_PRE_TARI_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_TARI_INI = 1;
        public static final int WTGA_PRE_TARI_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_TARI_INI = 8;
        public static final int WTGA_PRE_TARI_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_TARI_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_TARI_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
