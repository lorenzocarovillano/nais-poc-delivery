package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-PREMIO-TOT<br>
 * Variable: WPAG-CONT-PREMIO-TOT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContPremioTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContPremioTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_PREMIO_TOT;
    }

    public void setWpagContPremioTot(AfDecimal wpagContPremioTot) {
        writeDecimalAsPacked(Pos.WPAG_CONT_PREMIO_TOT, wpagContPremioTot.copy());
    }

    public void setWpagContPremioTotFormatted(String wpagContPremioTot) {
        setWpagContPremioTot(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_PREMIO_TOT + Len.Fract.WPAG_CONT_PREMIO_TOT, Len.Fract.WPAG_CONT_PREMIO_TOT, wpagContPremioTot));
    }

    public void setWpagContPremioTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_PREMIO_TOT, Pos.WPAG_CONT_PREMIO_TOT);
    }

    /**Original name: WPAG-CONT-PREMIO-TOT<br>*/
    public AfDecimal getWpagContPremioTot() {
        return readPackedAsDecimal(Pos.WPAG_CONT_PREMIO_TOT, Len.Int.WPAG_CONT_PREMIO_TOT, Len.Fract.WPAG_CONT_PREMIO_TOT);
    }

    public byte[] getWpagContPremioTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_PREMIO_TOT, Pos.WPAG_CONT_PREMIO_TOT);
        return buffer;
    }

    public void initWpagContPremioTotSpaces() {
        fill(Pos.WPAG_CONT_PREMIO_TOT, Len.WPAG_CONT_PREMIO_TOT, Types.SPACE_CHAR);
    }

    public void setWpagContPremioTotNull(String wpagContPremioTotNull) {
        writeString(Pos.WPAG_CONT_PREMIO_TOT_NULL, wpagContPremioTotNull, Len.WPAG_CONT_PREMIO_TOT_NULL);
    }

    /**Original name: WPAG-CONT-PREMIO-TOT-NULL<br>*/
    public String getWpagContPremioTotNull() {
        return readString(Pos.WPAG_CONT_PREMIO_TOT_NULL, Len.WPAG_CONT_PREMIO_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_TOT = 1;
        public static final int WPAG_CONT_PREMIO_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_TOT = 8;
        public static final int WPAG_CONT_PREMIO_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
