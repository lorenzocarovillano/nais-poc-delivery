package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-DT-INI-PER<br>
 * Variable: ISO-DT-INI-PER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoDtIniPer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoDtIniPer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_DT_INI_PER;
    }

    public void setIsoDtIniPer(int isoDtIniPer) {
        writeIntAsPacked(Pos.ISO_DT_INI_PER, isoDtIniPer, Len.Int.ISO_DT_INI_PER);
    }

    public void setIsoDtIniPerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_DT_INI_PER, Pos.ISO_DT_INI_PER);
    }

    /**Original name: ISO-DT-INI-PER<br>*/
    public int getIsoDtIniPer() {
        return readPackedAsInt(Pos.ISO_DT_INI_PER, Len.Int.ISO_DT_INI_PER);
    }

    public byte[] getIsoDtIniPerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_DT_INI_PER, Pos.ISO_DT_INI_PER);
        return buffer;
    }

    public void setIsoDtIniPerNull(String isoDtIniPerNull) {
        writeString(Pos.ISO_DT_INI_PER_NULL, isoDtIniPerNull, Len.ISO_DT_INI_PER_NULL);
    }

    /**Original name: ISO-DT-INI-PER-NULL<br>*/
    public String getIsoDtIniPerNull() {
        return readString(Pos.ISO_DT_INI_PER_NULL, Len.ISO_DT_INI_PER_NULL);
    }

    public String getIsoDtIniPerNullFormatted() {
        return Functions.padBlanks(getIsoDtIniPerNull(), Len.ISO_DT_INI_PER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_DT_INI_PER = 1;
        public static final int ISO_DT_INI_PER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_DT_INI_PER = 5;
        public static final int ISO_DT_INI_PER_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_DT_INI_PER = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
