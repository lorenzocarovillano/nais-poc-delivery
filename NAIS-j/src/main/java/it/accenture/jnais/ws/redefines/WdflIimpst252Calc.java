package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IIMPST-252-CALC<br>
 * Variable: WDFL-IIMPST-252-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIimpst252Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIimpst252Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IIMPST252_CALC;
    }

    public void setWdflIimpst252Calc(AfDecimal wdflIimpst252Calc) {
        writeDecimalAsPacked(Pos.WDFL_IIMPST252_CALC, wdflIimpst252Calc.copy());
    }

    public void setWdflIimpst252CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IIMPST252_CALC, Pos.WDFL_IIMPST252_CALC);
    }

    /**Original name: WDFL-IIMPST-252-CALC<br>*/
    public AfDecimal getWdflIimpst252Calc() {
        return readPackedAsDecimal(Pos.WDFL_IIMPST252_CALC, Len.Int.WDFL_IIMPST252_CALC, Len.Fract.WDFL_IIMPST252_CALC);
    }

    public byte[] getWdflIimpst252CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IIMPST252_CALC, Pos.WDFL_IIMPST252_CALC);
        return buffer;
    }

    public void setWdflIimpst252CalcNull(String wdflIimpst252CalcNull) {
        writeString(Pos.WDFL_IIMPST252_CALC_NULL, wdflIimpst252CalcNull, Len.WDFL_IIMPST252_CALC_NULL);
    }

    /**Original name: WDFL-IIMPST-252-CALC-NULL<br>*/
    public String getWdflIimpst252CalcNull() {
        return readString(Pos.WDFL_IIMPST252_CALC_NULL, Len.WDFL_IIMPST252_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST252_CALC = 1;
        public static final int WDFL_IIMPST252_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST252_CALC = 8;
        public static final int WDFL_IIMPST252_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST252_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST252_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
