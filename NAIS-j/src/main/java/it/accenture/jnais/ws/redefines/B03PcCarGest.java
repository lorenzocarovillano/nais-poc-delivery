package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PC-CAR-GEST<br>
 * Variable: B03-PC-CAR-GEST from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PcCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PcCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PC_CAR_GEST;
    }

    public void setB03PcCarGest(AfDecimal b03PcCarGest) {
        writeDecimalAsPacked(Pos.B03_PC_CAR_GEST, b03PcCarGest.copy());
    }

    public void setB03PcCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PC_CAR_GEST, Pos.B03_PC_CAR_GEST);
    }

    /**Original name: B03-PC-CAR-GEST<br>*/
    public AfDecimal getB03PcCarGest() {
        return readPackedAsDecimal(Pos.B03_PC_CAR_GEST, Len.Int.B03_PC_CAR_GEST, Len.Fract.B03_PC_CAR_GEST);
    }

    public byte[] getB03PcCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PC_CAR_GEST, Pos.B03_PC_CAR_GEST);
        return buffer;
    }

    public void setB03PcCarGestNull(String b03PcCarGestNull) {
        writeString(Pos.B03_PC_CAR_GEST_NULL, b03PcCarGestNull, Len.B03_PC_CAR_GEST_NULL);
    }

    /**Original name: B03-PC-CAR-GEST-NULL<br>*/
    public String getB03PcCarGestNull() {
        return readString(Pos.B03_PC_CAR_GEST_NULL, Len.B03_PC_CAR_GEST_NULL);
    }

    public String getB03PcCarGestNullFormatted() {
        return Functions.padBlanks(getB03PcCarGestNull(), Len.B03_PC_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PC_CAR_GEST = 1;
        public static final int B03_PC_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PC_CAR_GEST = 4;
        public static final int B03_PC_CAR_GEST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PC_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PC_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
