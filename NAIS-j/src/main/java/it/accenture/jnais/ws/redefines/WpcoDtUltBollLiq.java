package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-LIQ<br>
 * Variable: WPCO-DT-ULT-BOLL-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_LIQ;
    }

    public void setWpcoDtUltBollLiq(int wpcoDtUltBollLiq) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_LIQ, wpcoDtUltBollLiq, Len.Int.WPCO_DT_ULT_BOLL_LIQ);
    }

    public void setDpcoDtUltBollLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_LIQ, Pos.WPCO_DT_ULT_BOLL_LIQ);
    }

    /**Original name: WPCO-DT-ULT-BOLL-LIQ<br>*/
    public int getWpcoDtUltBollLiq() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_LIQ, Len.Int.WPCO_DT_ULT_BOLL_LIQ);
    }

    public byte[] getWpcoDtUltBollLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_LIQ, Pos.WPCO_DT_ULT_BOLL_LIQ);
        return buffer;
    }

    public void setWpcoDtUltBollLiqNull(String wpcoDtUltBollLiqNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_LIQ_NULL, wpcoDtUltBollLiqNull, Len.WPCO_DT_ULT_BOLL_LIQ_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-LIQ-NULL<br>*/
    public String getWpcoDtUltBollLiqNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_LIQ_NULL, Len.WPCO_DT_ULT_BOLL_LIQ_NULL);
    }

    public String getWpcoDtUltBollLiqNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollLiqNull(), Len.WPCO_DT_ULT_BOLL_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_LIQ = 1;
        public static final int WPCO_DT_ULT_BOLL_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_LIQ = 5;
        public static final int WPCO_DT_ULT_BOLL_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_LIQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
