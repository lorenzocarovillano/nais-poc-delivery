package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WE12-CUM-PRE-ATT<br>
 * Variable: WE12-CUM-PRE-ATT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We12CumPreAtt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We12CumPreAtt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE12_CUM_PRE_ATT;
    }

    public void setWe12CumPreAtt(AfDecimal we12CumPreAtt) {
        writeDecimalAsPacked(Pos.WE12_CUM_PRE_ATT, we12CumPreAtt.copy());
    }

    /**Original name: WE12-CUM-PRE-ATT<br>*/
    public AfDecimal getWe12CumPreAtt() {
        return readPackedAsDecimal(Pos.WE12_CUM_PRE_ATT, Len.Int.WE12_CUM_PRE_ATT, Len.Fract.WE12_CUM_PRE_ATT);
    }

    public byte[] getWe12CumPreAttAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE12_CUM_PRE_ATT, Pos.WE12_CUM_PRE_ATT);
        return buffer;
    }

    public void setWe12CumPreAttNull(String we12CumPreAttNull) {
        writeString(Pos.WE12_CUM_PRE_ATT_NULL, we12CumPreAttNull, Len.WE12_CUM_PRE_ATT_NULL);
    }

    /**Original name: WE12-CUM-PRE-ATT-NULL<br>*/
    public String getWe12CumPreAttNull() {
        return readString(Pos.WE12_CUM_PRE_ATT_NULL, Len.WE12_CUM_PRE_ATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE12_CUM_PRE_ATT = 1;
        public static final int WE12_CUM_PRE_ATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE12_CUM_PRE_ATT = 8;
        public static final int WE12_CUM_PRE_ATT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WE12_CUM_PRE_ATT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WE12_CUM_PRE_ATT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
