package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idso0011FlagCodaTs;
import it.accenture.jnais.ws.enums.Idso0011ReturnCode;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.occurs.Idso0011ElementiBuffer;

/**Original name: OUT-IDSS0010<br>
 * Variable: OUT-IDSS0010 from program IDSS0010<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class OutIdss0010 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int ELEMENTI_BUFFER_MAXOCCURS = 30;
    //Original name: IDSO0011-MAX
    private String max = DefaultValues.stringVal(Len.MAX);
    //Original name: IDSO0011-ELEMENTI-BUFFER
    private Idso0011ElementiBuffer[] elementiBuffer = new Idso0011ElementiBuffer[ELEMENTI_BUFFER_MAXOCCURS];
    //Original name: IDSO0011-FLAG-CODA-TS
    private Idso0011FlagCodaTs flagCodaTs = new Idso0011FlagCodaTs();
    /**Original name: IDSO0011-RETURN-CODE<br>
	 * <pre> --  CAMPI ERRORI
	 *       COPY IDSV0006 REPLACING ==IDSO0011== BY ==IDSO0011==.</pre>*/
    private Idso0011ReturnCode returnCode = new Idso0011ReturnCode();
    /**Original name: IDSO0011-SQLCODE-SIGNED<br>
	 * <pre>   fine COPY IDSV0004</pre>*/
    private Idso0011SqlcodeSigned sqlcodeSigned = new Idso0011SqlcodeSigned();
    //Original name: IDSO0011-SQLCODE
    private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
    //Original name: IDSO0011-CAMPI-ESITO
    private Idsv0003CampiEsito campiEsito = new Idsv0003CampiEsito();

    //==== CONSTRUCTORS ====
    public OutIdss0010() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OUT_IDSS0010;
    }

    @Override
    public void deserialize(byte[] buf) {
        setOutIdss0010Bytes(buf);
    }

    public void init() {
        for (int elementiBufferIdx = 1; elementiBufferIdx <= ELEMENTI_BUFFER_MAXOCCURS; elementiBufferIdx++) {
            elementiBuffer[elementiBufferIdx - 1] = new Idso0011ElementiBuffer();
        }
    }

    public void setOutIdss0010Bytes(byte[] buffer) {
        setOutIdss0010Bytes(buffer, 1);
    }

    public byte[] getOutIdss0010Bytes() {
        byte[] buffer = new byte[Len.OUT_IDSS0010];
        return getOutIdss0010Bytes(buffer, 1);
    }

    public void setOutIdss0010Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaBytes(buffer, position);
    }

    public byte[] getOutIdss0010Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaBytes(buffer, position);
        return buffer;
    }

    public void setAreaBytes(byte[] buffer, int offset) {
        int position = offset;
        max = MarshalByte.readFixedString(buffer, position, Len.MAX);
        position += Len.MAX;
        setComposizioneBufferBytes(buffer, position);
        position += Len.COMPOSIZIONE_BUFFER;
        flagCodaTs.setFlagCodaTs(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Idso0011ReturnCode.Len.RETURN_CODE));
        position += Idso0011ReturnCode.Len.RETURN_CODE;
        sqlcodeSigned.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        sqlcode = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE), Len.SQLCODE);
        position += Len.SQLCODE;
        campiEsito.setCampiEsitoBytes(buffer, position);
    }

    public byte[] getAreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, max, Len.MAX);
        position += Len.MAX;
        getComposizioneBufferBytes(buffer, position);
        position += Len.COMPOSIZIONE_BUFFER;
        MarshalByte.writeChar(buffer, position, flagCodaTs.getFlagCodaTs());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Idso0011ReturnCode.Len.RETURN_CODE);
        position += Idso0011ReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcodeSigned.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
        position += Len.SQLCODE;
        campiEsito.getCampiEsitoBytes(buffer, position);
        return buffer;
    }

    public void setComposizioneBufferBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ELEMENTI_BUFFER_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                elementiBuffer[idx - 1].setElementiBufferBytes(buffer, position);
                position += Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
            }
            else {
                elementiBuffer[idx - 1].initElementiBufferSpaces();
                position += Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
            }
        }
    }

    public byte[] getComposizioneBufferBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ELEMENTI_BUFFER_MAXOCCURS; idx++) {
            elementiBuffer[idx - 1].getElementiBufferBytes(buffer, position);
            position += Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
        }
        return buffer;
    }

    public void setSqlcode(long sqlcode) {
        this.sqlcode = PicFormatter.display("--(7)9").format(sqlcode).toString();
    }

    public long getSqlcode() {
        return PicParser.display("--(7)9").parseLong(this.sqlcode);
    }

    public Idsv0003CampiEsito getCampiEsito() {
        return campiEsito;
    }

    public Idso0011ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idso0011SqlcodeSigned getSqlcodeSigned() {
        return sqlcodeSigned;
    }

    @Override
    public byte[] serialize() {
        return getOutIdss0010Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MAX = 2;
        public static final int COMPOSIZIONE_BUFFER = OutIdss0010.ELEMENTI_BUFFER_MAXOCCURS * Idso0011ElementiBuffer.Len.ELEMENTI_BUFFER;
        public static final int SQLCODE = 9;
        public static final int AREA = MAX + COMPOSIZIONE_BUFFER + Idso0011FlagCodaTs.Len.FLAG_CODA_TS + Idso0011ReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + SQLCODE + Idsv0003CampiEsito.Len.CAMPI_ESITO;
        public static final int OUT_IDSS0010 = AREA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
