package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PC-RETR<br>
 * Variable: WTGA-PC-RETR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPcRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPcRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PC_RETR;
    }

    public void setWtgaPcRetr(AfDecimal wtgaPcRetr) {
        writeDecimalAsPacked(Pos.WTGA_PC_RETR, wtgaPcRetr.copy());
    }

    public void setWtgaPcRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PC_RETR, Pos.WTGA_PC_RETR);
    }

    /**Original name: WTGA-PC-RETR<br>*/
    public AfDecimal getWtgaPcRetr() {
        return readPackedAsDecimal(Pos.WTGA_PC_RETR, Len.Int.WTGA_PC_RETR, Len.Fract.WTGA_PC_RETR);
    }

    public byte[] getWtgaPcRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PC_RETR, Pos.WTGA_PC_RETR);
        return buffer;
    }

    public void initWtgaPcRetrSpaces() {
        fill(Pos.WTGA_PC_RETR, Len.WTGA_PC_RETR, Types.SPACE_CHAR);
    }

    public void setWtgaPcRetrNull(String wtgaPcRetrNull) {
        writeString(Pos.WTGA_PC_RETR_NULL, wtgaPcRetrNull, Len.WTGA_PC_RETR_NULL);
    }

    /**Original name: WTGA-PC-RETR-NULL<br>*/
    public String getWtgaPcRetrNull() {
        return readString(Pos.WTGA_PC_RETR_NULL, Len.WTGA_PC_RETR_NULL);
    }

    public String getWtgaPcRetrNullFormatted() {
        return Functions.padBlanks(getWtgaPcRetrNull(), Len.WTGA_PC_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PC_RETR = 1;
        public static final int WTGA_PC_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PC_RETR = 4;
        public static final int WTGA_PC_RETR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
