package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0001-GRAVITA-ERR<br>
 * Variable: ISPC0001-GRAVITA-ERR from copybook IEAV9903<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0001GravitaErr {

    //==== PROPERTIES ====
    public String value = DefaultValues.stringVal(Len.GRAVITA_ERR);
    public static final String WARNING_ACT = "92";
    public static final String BLOCCANTE = "93";
    public static final String NON_BLOCC = "94";
    public static final String WARNING_UTE = "95";
    public static final String OPER_NON_CONS = "00";

    //==== METHODS ====
    public void setIspc0001GravitaErr(short ispc0001GravitaErr) {
        this.value = NumericDisplay.asString(ispc0001GravitaErr, Len.GRAVITA_ERR);
    }

    public short getIspc0001GravitaErr() {
        return NumericDisplay.asShort(this.value);
    }

    public String getIspc0001GravitaErrFormatted() {
        return this.value;
    }

    public String getIspc0001GravitaErrAsString() {
        return getIspc0001GravitaErrFormatted();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GRAVITA_ERR = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
