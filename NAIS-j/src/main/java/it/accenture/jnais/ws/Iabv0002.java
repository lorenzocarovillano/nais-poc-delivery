package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Iabv0009GestGuideService;
import it.accenture.jnais.ws.enums.Iabv0002TipoOrderBy;
import it.accenture.jnais.ws.redefines.Iabv0002StateGlobal;

/**Original name: IABV0002<br>
 * Variable: IABV0002 from copybook IABV0002<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Iabv0002 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IABV0002-STATE-CURRENT
    private char iabv0002StateCurrent = DefaultValues.CHAR_VAL;
    //Original name: IABV0002-STATE-GLOBAL
    private Iabv0002StateGlobal iabv0002StateGlobal = new Iabv0002StateGlobal();
    //Original name: IABV0002-FLAG-ALWAYS-EXE
    private char iabv0002FlagAlwaysExe = DefaultValues.CHAR_VAL;
    //Original name: IABV0002-TIPO-ORDER-BY
    private Iabv0002TipoOrderBy iabv0002TipoOrderBy = new Iabv0002TipoOrderBy();
    //Original name: IABV0009-GEST-GUIDE-SERVICE
    private Iabv0009GestGuideService iabv0009GestGuideService = new Iabv0009GestGuideService();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IABV0002;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIabv0002Bytes(buf);
    }

    public String getIabv0002Formatted() {
        return MarshalByteExt.bufferToStr(getIabv0002Bytes());
    }

    public void setIabv0002Bytes(byte[] buffer) {
        setIabv0002Bytes(buffer, 1);
    }

    public byte[] getIabv0002Bytes() {
        byte[] buffer = new byte[Len.IABV0002];
        return getIabv0002Bytes(buffer, 1);
    }

    public void setIabv0002Bytes(byte[] buffer, int offset) {
        int position = offset;
        setIabv0002StateBytes(buffer, position);
        position += Len.IABV0002_STATE;
        iabv0002FlagAlwaysExe = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        iabv0002TipoOrderBy.setIabv0002TipoOrderBy(MarshalByte.readString(buffer, position, Iabv0002TipoOrderBy.Len.IABV0002_TIPO_ORDER_BY));
        position += Iabv0002TipoOrderBy.Len.IABV0002_TIPO_ORDER_BY;
        iabv0009GestGuideService.setIabv0009GestGuideServiceBytes(buffer, position);
    }

    public byte[] getIabv0002Bytes(byte[] buffer, int offset) {
        int position = offset;
        getIabv0002StateBytes(buffer, position);
        position += Len.IABV0002_STATE;
        MarshalByte.writeChar(buffer, position, iabv0002FlagAlwaysExe);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, iabv0002TipoOrderBy.getIabv0002TipoOrderBy(), Iabv0002TipoOrderBy.Len.IABV0002_TIPO_ORDER_BY);
        position += Iabv0002TipoOrderBy.Len.IABV0002_TIPO_ORDER_BY;
        iabv0009GestGuideService.getIabv0009GestGuideServiceBytes(buffer, position);
        return buffer;
    }

    public void setIabv0002StateBytes(byte[] buffer, int offset) {
        int position = offset;
        iabv0002StateCurrent = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        iabv0002StateGlobal.setIabv0002StateGlobalBytes(buffer, position);
    }

    public byte[] getIabv0002StateBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, iabv0002StateCurrent);
        position += Types.CHAR_SIZE;
        iabv0002StateGlobal.getIabv0002StateGlobalBytes(buffer, position);
        return buffer;
    }

    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        this.iabv0002StateCurrent = iabv0002StateCurrent;
    }

    public char getIabv0002StateCurrent() {
        return this.iabv0002StateCurrent;
    }

    public void setIabv0002FlagAlwaysExe(char iabv0002FlagAlwaysExe) {
        this.iabv0002FlagAlwaysExe = iabv0002FlagAlwaysExe;
    }

    public char getIabv0002FlagAlwaysExe() {
        return this.iabv0002FlagAlwaysExe;
    }

    public Iabv0002StateGlobal getIabv0002StateGlobal() {
        return iabv0002StateGlobal;
    }

    public Iabv0002TipoOrderBy getIabv0002TipoOrderBy() {
        return iabv0002TipoOrderBy;
    }

    public Iabv0009GestGuideService getIabv0009GestGuideService() {
        return iabv0009GestGuideService;
    }

    @Override
    public byte[] serialize() {
        return getIabv0002Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IABV0002_STATE_CURRENT = 1;
        public static final int IABV0002_STATE = IABV0002_STATE_CURRENT + Iabv0002StateGlobal.Len.IABV0002_STATE_GLOBAL;
        public static final int IABV0002_FLAG_ALWAYS_EXE = 1;
        public static final int IABV0002 = IABV0002_STATE + IABV0002_FLAG_ALWAYS_EXE + Iabv0002TipoOrderBy.Len.IABV0002_TIPO_ORDER_BY + Iabv0009GestGuideService.Len.IABV0009_GEST_GUIDE_SERVICE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
