package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-IMP-ADER<br>
 * Variable: WDAD-IMP-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_IMP_ADER;
    }

    public void setWdadImpAder(AfDecimal wdadImpAder) {
        writeDecimalAsPacked(Pos.WDAD_IMP_ADER, wdadImpAder.copy());
    }

    public void setWdadImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_IMP_ADER, Pos.WDAD_IMP_ADER);
    }

    /**Original name: WDAD-IMP-ADER<br>*/
    public AfDecimal getWdadImpAder() {
        return readPackedAsDecimal(Pos.WDAD_IMP_ADER, Len.Int.WDAD_IMP_ADER, Len.Fract.WDAD_IMP_ADER);
    }

    public byte[] getWdadImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_IMP_ADER, Pos.WDAD_IMP_ADER);
        return buffer;
    }

    public void setWdadImpAderNull(String wdadImpAderNull) {
        writeString(Pos.WDAD_IMP_ADER_NULL, wdadImpAderNull, Len.WDAD_IMP_ADER_NULL);
    }

    /**Original name: WDAD-IMP-ADER-NULL<br>*/
    public String getWdadImpAderNull() {
        return readString(Pos.WDAD_IMP_ADER_NULL, Len.WDAD_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_ADER = 1;
        public static final int WDAD_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_IMP_ADER = 8;
        public static final int WDAD_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
