package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PRE-SOLO-RSH<br>
 * Variable: TDR-TOT-PRE-SOLO-RSH from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PRE_SOLO_RSH;
    }

    public void setTdrTotPreSoloRsh(AfDecimal tdrTotPreSoloRsh) {
        writeDecimalAsPacked(Pos.TDR_TOT_PRE_SOLO_RSH, tdrTotPreSoloRsh.copy());
    }

    public void setTdrTotPreSoloRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PRE_SOLO_RSH, Pos.TDR_TOT_PRE_SOLO_RSH);
    }

    /**Original name: TDR-TOT-PRE-SOLO-RSH<br>*/
    public AfDecimal getTdrTotPreSoloRsh() {
        return readPackedAsDecimal(Pos.TDR_TOT_PRE_SOLO_RSH, Len.Int.TDR_TOT_PRE_SOLO_RSH, Len.Fract.TDR_TOT_PRE_SOLO_RSH);
    }

    public byte[] getTdrTotPreSoloRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PRE_SOLO_RSH, Pos.TDR_TOT_PRE_SOLO_RSH);
        return buffer;
    }

    public void setTdrTotPreSoloRshNull(String tdrTotPreSoloRshNull) {
        writeString(Pos.TDR_TOT_PRE_SOLO_RSH_NULL, tdrTotPreSoloRshNull, Len.TDR_TOT_PRE_SOLO_RSH_NULL);
    }

    /**Original name: TDR-TOT-PRE-SOLO-RSH-NULL<br>*/
    public String getTdrTotPreSoloRshNull() {
        return readString(Pos.TDR_TOT_PRE_SOLO_RSH_NULL, Len.TDR_TOT_PRE_SOLO_RSH_NULL);
    }

    public String getTdrTotPreSoloRshNullFormatted() {
        return Functions.padBlanks(getTdrTotPreSoloRshNull(), Len.TDR_TOT_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_SOLO_RSH = 1;
        public static final int TDR_TOT_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_SOLO_RSH = 8;
        public static final int TDR_TOT_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
