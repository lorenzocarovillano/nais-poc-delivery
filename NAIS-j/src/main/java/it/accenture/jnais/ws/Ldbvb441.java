package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LDBVB441<br>
 * Variable: LDBVB441 from copybook LDBVB441<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbvb441 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBVB441-TP-TIT-01
    private String tpTit01 = DefaultValues.stringVal(Len.TP_TIT01);
    //Original name: LDBVB441-TP-TIT-02
    private String tpTit02 = DefaultValues.stringVal(Len.TP_TIT02);
    //Original name: LDBVB441-TP-STAT-TIT-1
    private String tpStatTit1 = DefaultValues.stringVal(Len.TP_STAT_TIT1);
    //Original name: LDBVB441-TP-STAT-TIT-2
    private String tpStatTit2 = DefaultValues.stringVal(Len.TP_STAT_TIT2);
    //Original name: LDBVB441-TP-STAT-TIT-3
    private String tpStatTit3 = DefaultValues.stringVal(Len.TP_STAT_TIT3);
    //Original name: LDBVB441-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBVB441-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBVB441-DT-MAX
    private String dtMax = DefaultValues.stringVal(Len.DT_MAX);
    //Original name: LDBVB441-DT-MAX-DB
    private String dtMaxDb = DefaultValues.stringVal(Len.DT_MAX_DB);
    //Original name: LDBVB441-DT-MAX-IND
    private short dtMaxInd = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVB441;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbvb441Bytes(buf);
    }

    public String getLdbvb441Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvb441Bytes());
    }

    public void setLdbvb441Bytes(byte[] buffer) {
        setLdbvb441Bytes(buffer, 1);
    }

    public byte[] getLdbvb441Bytes() {
        byte[] buffer = new byte[Len.LDBVB441];
        return getLdbvb441Bytes(buffer, 1);
    }

    public void setLdbvb441Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpTit01 = MarshalByte.readString(buffer, position, Len.TP_TIT01);
        position += Len.TP_TIT01;
        tpTit02 = MarshalByte.readString(buffer, position, Len.TP_TIT02);
        position += Len.TP_TIT02;
        tpStatTit1 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT1);
        position += Len.TP_STAT_TIT1;
        tpStatTit2 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT2);
        position += Len.TP_STAT_TIT2;
        tpStatTit3 = MarshalByte.readString(buffer, position, Len.TP_STAT_TIT3);
        position += Len.TP_STAT_TIT3;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        dtMax = MarshalByte.readFixedString(buffer, position, Len.DT_MAX);
        position += Len.DT_MAX;
        dtMaxDb = MarshalByte.readString(buffer, position, Len.DT_MAX_DB);
        position += Len.DT_MAX_DB;
        dtMaxInd = MarshalByte.readBinaryShort(buffer, position);
    }

    public byte[] getLdbvb441Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpTit01, Len.TP_TIT01);
        position += Len.TP_TIT01;
        MarshalByte.writeString(buffer, position, tpTit02, Len.TP_TIT02);
        position += Len.TP_TIT02;
        MarshalByte.writeString(buffer, position, tpStatTit1, Len.TP_STAT_TIT1);
        position += Len.TP_STAT_TIT1;
        MarshalByte.writeString(buffer, position, tpStatTit2, Len.TP_STAT_TIT2);
        position += Len.TP_STAT_TIT2;
        MarshalByte.writeString(buffer, position, tpStatTit3, Len.TP_STAT_TIT3);
        position += Len.TP_STAT_TIT3;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, dtMax, Len.DT_MAX);
        position += Len.DT_MAX;
        MarshalByte.writeString(buffer, position, dtMaxDb, Len.DT_MAX_DB);
        position += Len.DT_MAX_DB;
        MarshalByte.writeBinaryShort(buffer, position, dtMaxInd);
        return buffer;
    }

    public void setTpTit01(String tpTit01) {
        this.tpTit01 = Functions.subString(tpTit01, Len.TP_TIT01);
    }

    public String getTpTit01() {
        return this.tpTit01;
    }

    public void setTpTit02(String tpTit02) {
        this.tpTit02 = Functions.subString(tpTit02, Len.TP_TIT02);
    }

    public String getTpTit02() {
        return this.tpTit02;
    }

    public void setTpStatTit1(String tpStatTit1) {
        this.tpStatTit1 = Functions.subString(tpStatTit1, Len.TP_STAT_TIT1);
    }

    public String getTpStatTit1() {
        return this.tpStatTit1;
    }

    public void setTpStatTit2(String tpStatTit2) {
        this.tpStatTit2 = Functions.subString(tpStatTit2, Len.TP_STAT_TIT2);
    }

    public String getTpStatTit2() {
        return this.tpStatTit2;
    }

    public void setTpStatTit3(String tpStatTit3) {
        this.tpStatTit3 = Functions.subString(tpStatTit3, Len.TP_STAT_TIT3);
    }

    public String getTpStatTit3() {
        return this.tpStatTit3;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setDtMaxFormatted(String dtMax) {
        this.dtMax = Trunc.toUnsignedNumeric(dtMax, Len.DT_MAX);
    }

    public int getDtMax() {
        return NumericDisplay.asInt(this.dtMax);
    }

    public void setDtMaxDb(String dtMaxDb) {
        this.dtMaxDb = Functions.subString(dtMaxDb, Len.DT_MAX_DB);
    }

    public String getDtMaxDb() {
        return this.dtMaxDb;
    }

    public void setDtMaxInd(short dtMaxInd) {
        this.dtMaxInd = dtMaxInd;
    }

    public short getDtMaxInd() {
        return this.dtMaxInd;
    }

    @Override
    public byte[] serialize() {
        return getLdbvb441Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_TIT01 = 2;
        public static final int TP_TIT02 = 2;
        public static final int TP_STAT_TIT1 = 2;
        public static final int TP_STAT_TIT2 = 2;
        public static final int TP_STAT_TIT3 = 2;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int DT_MAX = 8;
        public static final int DT_MAX_DB = 10;
        public static final int DT_MAX_IND = 2;
        public static final int LDBVB441 = TP_TIT01 + TP_TIT02 + TP_STAT_TIT1 + TP_STAT_TIT2 + TP_STAT_TIT3 + ID_OGG + TP_OGG + DT_MAX + DT_MAX_DB + DT_MAX_IND;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
