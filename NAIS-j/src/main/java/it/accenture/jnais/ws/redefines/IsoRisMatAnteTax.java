package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-RIS-MAT-ANTE-TAX<br>
 * Variable: ISO-RIS-MAT-ANTE-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoRisMatAnteTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoRisMatAnteTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_RIS_MAT_ANTE_TAX;
    }

    public void setIsoRisMatAnteTax(AfDecimal isoRisMatAnteTax) {
        writeDecimalAsPacked(Pos.ISO_RIS_MAT_ANTE_TAX, isoRisMatAnteTax.copy());
    }

    public void setIsoRisMatAnteTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_RIS_MAT_ANTE_TAX, Pos.ISO_RIS_MAT_ANTE_TAX);
    }

    /**Original name: ISO-RIS-MAT-ANTE-TAX<br>*/
    public AfDecimal getIsoRisMatAnteTax() {
        return readPackedAsDecimal(Pos.ISO_RIS_MAT_ANTE_TAX, Len.Int.ISO_RIS_MAT_ANTE_TAX, Len.Fract.ISO_RIS_MAT_ANTE_TAX);
    }

    public byte[] getIsoRisMatAnteTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_RIS_MAT_ANTE_TAX, Pos.ISO_RIS_MAT_ANTE_TAX);
        return buffer;
    }

    public void setIsoRisMatAnteTaxNull(String isoRisMatAnteTaxNull) {
        writeString(Pos.ISO_RIS_MAT_ANTE_TAX_NULL, isoRisMatAnteTaxNull, Len.ISO_RIS_MAT_ANTE_TAX_NULL);
    }

    /**Original name: ISO-RIS-MAT-ANTE-TAX-NULL<br>*/
    public String getIsoRisMatAnteTaxNull() {
        return readString(Pos.ISO_RIS_MAT_ANTE_TAX_NULL, Len.ISO_RIS_MAT_ANTE_TAX_NULL);
    }

    public String getIsoRisMatAnteTaxNullFormatted() {
        return Functions.padBlanks(getIsoRisMatAnteTaxNull(), Len.ISO_RIS_MAT_ANTE_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_RIS_MAT_ANTE_TAX = 1;
        public static final int ISO_RIS_MAT_ANTE_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_RIS_MAT_ANTE_TAX = 8;
        public static final int ISO_RIS_MAT_ANTE_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_RIS_MAT_ANTE_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_RIS_MAT_ANTE_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
