package it.accenture.jnais.ws;

import it.accenture.jnais.copy.AdesDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDettTitCont;
import it.accenture.jnais.copy.Ldbv5681;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSF510<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsf510Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-POLI
    private IndDettTitCont indPoli = new IndDettTitCont();
    //Original name: POLI-DB
    private AdesDb poliDb = new AdesDb();
    //Original name: LDBV5681
    private Ldbv5681 ldbv5681 = new Ldbv5681();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDettTitCont getIndPoli() {
        return indPoli;
    }

    public Ldbv5681 getLdbv5681() {
        return ldbv5681;
    }

    public AdesDb getPoliDb() {
        return poliDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
