package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-TP-OGG-MBS<br>
 * Variable: WCOM-TP-OGG-MBS from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomTpOggMbs {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WCOM_TP_OGG_MBS);
    public static final String POLIZZA_MBS = "PO";
    public static final String ADESIONE_MBS = "AD";

    //==== METHODS ====
    public void setWcomTpOggMbs(String wcomTpOggMbs) {
        this.value = Functions.subString(wcomTpOggMbs, Len.WCOM_TP_OGG_MBS);
    }

    public String getWcomTpOggMbs() {
        return this.value;
    }

    public boolean isPolizzaMbs() {
        return value.equals(POLIZZA_MBS);
    }

    public void setWcomPolizzaMbs() {
        value = POLIZZA_MBS;
    }

    public boolean isAdesioneMbs() {
        return value.equals(ADESIONE_MBS);
    }

    public void setWcomAdesioneMbs() {
        value = ADESIONE_MBS;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_TP_OGG_MBS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
