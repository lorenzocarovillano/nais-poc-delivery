package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-INI-NEWFIS<br>
 * Variable: TGA-PRSTZ-INI-NEWFIS from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzIniNewfis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzIniNewfis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_INI_NEWFIS;
    }

    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_INI_NEWFIS, tgaPrstzIniNewfis.copy());
    }

    public void setTgaPrstzIniNewfisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_INI_NEWFIS, Pos.TGA_PRSTZ_INI_NEWFIS);
    }

    /**Original name: TGA-PRSTZ-INI-NEWFIS<br>*/
    public AfDecimal getTgaPrstzIniNewfis() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_INI_NEWFIS, Len.Int.TGA_PRSTZ_INI_NEWFIS, Len.Fract.TGA_PRSTZ_INI_NEWFIS);
    }

    public byte[] getTgaPrstzIniNewfisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_INI_NEWFIS, Pos.TGA_PRSTZ_INI_NEWFIS);
        return buffer;
    }

    public void setTgaPrstzIniNewfisNull(String tgaPrstzIniNewfisNull) {
        writeString(Pos.TGA_PRSTZ_INI_NEWFIS_NULL, tgaPrstzIniNewfisNull, Len.TGA_PRSTZ_INI_NEWFIS_NULL);
    }

    /**Original name: TGA-PRSTZ-INI-NEWFIS-NULL<br>*/
    public String getTgaPrstzIniNewfisNull() {
        return readString(Pos.TGA_PRSTZ_INI_NEWFIS_NULL, Len.TGA_PRSTZ_INI_NEWFIS_NULL);
    }

    public String getTgaPrstzIniNewfisNullFormatted() {
        return Functions.padBlanks(getTgaPrstzIniNewfisNull(), Len.TGA_PRSTZ_INI_NEWFIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI_NEWFIS = 1;
        public static final int TGA_PRSTZ_INI_NEWFIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI_NEWFIS = 8;
        public static final int TGA_PRSTZ_INI_NEWFIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI_NEWFIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI_NEWFIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
