package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FL-GRAV-PRESENTE<br>
 * Variable: WK-FL-GRAV-PRESENTE from program LCCS0022<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFlGravPresente {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFlGravPresente(char wkFlGravPresente) {
        this.value = wkFlGravPresente;
    }

    public char getWkFlGravPresente() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
