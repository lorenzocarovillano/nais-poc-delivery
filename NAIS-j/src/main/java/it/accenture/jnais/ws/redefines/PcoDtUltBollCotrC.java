package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-COTR-C<br>
 * Variable: PCO-DT-ULT-BOLL-COTR-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollCotrC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollCotrC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_COTR_C;
    }

    public void setPcoDtUltBollCotrC(int pcoDtUltBollCotrC) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_COTR_C, pcoDtUltBollCotrC, Len.Int.PCO_DT_ULT_BOLL_COTR_C);
    }

    public void setPcoDtUltBollCotrCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_COTR_C, Pos.PCO_DT_ULT_BOLL_COTR_C);
    }

    /**Original name: PCO-DT-ULT-BOLL-COTR-C<br>*/
    public int getPcoDtUltBollCotrC() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_COTR_C, Len.Int.PCO_DT_ULT_BOLL_COTR_C);
    }

    public byte[] getPcoDtUltBollCotrCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_COTR_C, Pos.PCO_DT_ULT_BOLL_COTR_C);
        return buffer;
    }

    public void initPcoDtUltBollCotrCHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_COTR_C, Len.PCO_DT_ULT_BOLL_COTR_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollCotrCNull(String pcoDtUltBollCotrCNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_COTR_C_NULL, pcoDtUltBollCotrCNull, Len.PCO_DT_ULT_BOLL_COTR_C_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-COTR-C-NULL<br>*/
    public String getPcoDtUltBollCotrCNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_COTR_C_NULL, Len.PCO_DT_ULT_BOLL_COTR_C_NULL);
    }

    public String getPcoDtUltBollCotrCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollCotrCNull(), Len.PCO_DT_ULT_BOLL_COTR_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_COTR_C = 1;
        public static final int PCO_DT_ULT_BOLL_COTR_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_COTR_C = 5;
        public static final int PCO_DT_ULT_BOLL_COTR_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_COTR_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
