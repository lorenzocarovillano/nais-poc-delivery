package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSD-PRECISIONE-DATO<br>
 * Variable: CSD-PRECISIONE-DATO from program IDSS0020<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CsdPrecisioneDato extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public CsdPrecisioneDato() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CSD_PRECISIONE_DATO;
    }

    public void setCsdPrecisioneDato(short csdPrecisioneDato) {
        writeShortAsPacked(Pos.CSD_PRECISIONE_DATO, csdPrecisioneDato, Len.Int.CSD_PRECISIONE_DATO);
    }

    /**Original name: CSD-PRECISIONE-DATO<br>*/
    public short getCsdPrecisioneDato() {
        return readPackedAsShort(Pos.CSD_PRECISIONE_DATO, Len.Int.CSD_PRECISIONE_DATO);
    }

    public void initCsdPrecisioneDatoSpaces() {
        fill(Pos.CSD_PRECISIONE_DATO, Len.CSD_PRECISIONE_DATO, Types.SPACE_CHAR);
    }

    public void setCsdPrecisioneDatoNull(String csdPrecisioneDatoNull) {
        writeString(Pos.CSD_PRECISIONE_DATO_NULL, csdPrecisioneDatoNull, Len.CSD_PRECISIONE_DATO_NULL);
    }

    /**Original name: CSD-PRECISIONE-DATO-NULL<br>*/
    public String getCsdPrecisioneDatoNull() {
        return readString(Pos.CSD_PRECISIONE_DATO_NULL, Len.CSD_PRECISIONE_DATO_NULL);
    }

    public String getCsdPrecisioneDatoNullFormatted() {
        return Functions.padBlanks(getCsdPrecisioneDatoNull(), Len.CSD_PRECISIONE_DATO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CSD_PRECISIONE_DATO = 1;
        public static final int CSD_PRECISIONE_DATO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CSD_PRECISIONE_DATO = 2;
        public static final int CSD_PRECISIONE_DATO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CSD_PRECISIONE_DATO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
