package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ADDRESS-TROVATO<br>
 * Variable: ADDRESS-TROVATO from copybook IABC0010<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class AddressTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char VAL_VAR_SI = 'S';
    public static final char VAL_VAR_NO = 'N';
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setAddressTrovato(char addressTrovato) {
        this.value = addressTrovato;
    }

    public char getAddressTrovato() {
        return this.value;
    }

    public boolean isValVarSi() {
        return value == VAL_VAR_SI;
    }

    public void setValVarSi() {
        value = VAL_VAR_SI;
    }

    public boolean isDatoInCacheNo() {
        return value == VAL_VAR_NO;
    }

    public void setValVarNo() {
        value = VAL_VAR_NO;
    }
}
