package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-RIS-ZIL<br>
 * Variable: OCO-RIS-ZIL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoRisZil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoRisZil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_RIS_ZIL;
    }

    public void setOcoRisZil(AfDecimal ocoRisZil) {
        writeDecimalAsPacked(Pos.OCO_RIS_ZIL, ocoRisZil.copy());
    }

    public void setOcoRisZilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_RIS_ZIL, Pos.OCO_RIS_ZIL);
    }

    /**Original name: OCO-RIS-ZIL<br>*/
    public AfDecimal getOcoRisZil() {
        return readPackedAsDecimal(Pos.OCO_RIS_ZIL, Len.Int.OCO_RIS_ZIL, Len.Fract.OCO_RIS_ZIL);
    }

    public byte[] getOcoRisZilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_RIS_ZIL, Pos.OCO_RIS_ZIL);
        return buffer;
    }

    public void setOcoRisZilNull(String ocoRisZilNull) {
        writeString(Pos.OCO_RIS_ZIL_NULL, ocoRisZilNull, Len.OCO_RIS_ZIL_NULL);
    }

    /**Original name: OCO-RIS-ZIL-NULL<br>*/
    public String getOcoRisZilNull() {
        return readString(Pos.OCO_RIS_ZIL_NULL, Len.OCO_RIS_ZIL_NULL);
    }

    public String getOcoRisZilNullFormatted() {
        return Functions.padBlanks(getOcoRisZilNull(), Len.OCO_RIS_ZIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_RIS_ZIL = 1;
        public static final int OCO_RIS_ZIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_RIS_ZIL = 8;
        public static final int OCO_RIS_ZIL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_RIS_ZIL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_RIS_ZIL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
