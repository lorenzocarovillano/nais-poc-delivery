package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DIR-1O-VERS<br>
 * Variable: POL-DIR-1O-VERS from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDir1oVers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDir1oVers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DIR1O_VERS;
    }

    public void setPolDir1oVers(AfDecimal polDir1oVers) {
        writeDecimalAsPacked(Pos.POL_DIR1O_VERS, polDir1oVers.copy());
    }

    public void setPolDir1oVersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DIR1O_VERS, Pos.POL_DIR1O_VERS);
    }

    /**Original name: POL-DIR-1O-VERS<br>*/
    public AfDecimal getPolDir1oVers() {
        return readPackedAsDecimal(Pos.POL_DIR1O_VERS, Len.Int.POL_DIR1O_VERS, Len.Fract.POL_DIR1O_VERS);
    }

    public byte[] getPolDir1oVersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DIR1O_VERS, Pos.POL_DIR1O_VERS);
        return buffer;
    }

    public void setPolDir1oVersNull(String polDir1oVersNull) {
        writeString(Pos.POL_DIR1O_VERS_NULL, polDir1oVersNull, Len.POL_DIR1O_VERS_NULL);
    }

    /**Original name: POL-DIR-1O-VERS-NULL<br>*/
    public String getPolDir1oVersNull() {
        return readString(Pos.POL_DIR1O_VERS_NULL, Len.POL_DIR1O_VERS_NULL);
    }

    public String getPolDir1oVersNullFormatted() {
        return Functions.padBlanks(getPolDir1oVersNull(), Len.POL_DIR1O_VERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DIR1O_VERS = 1;
        public static final int POL_DIR1O_VERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DIR1O_VERS = 8;
        public static final int POL_DIR1O_VERS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DIR1O_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POL_DIR1O_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
