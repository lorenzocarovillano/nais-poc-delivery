package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-SCAD-COP<br>
 * Variable: WP67-DT-SCAD-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtScadCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtScadCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_SCAD_COP;
    }

    public void setWp67DtScadCop(int wp67DtScadCop) {
        writeIntAsPacked(Pos.WP67_DT_SCAD_COP, wp67DtScadCop, Len.Int.WP67_DT_SCAD_COP);
    }

    public void setWp67DtScadCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_SCAD_COP, Pos.WP67_DT_SCAD_COP);
    }

    /**Original name: WP67-DT-SCAD-COP<br>*/
    public int getWp67DtScadCop() {
        return readPackedAsInt(Pos.WP67_DT_SCAD_COP, Len.Int.WP67_DT_SCAD_COP);
    }

    public byte[] getWp67DtScadCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_SCAD_COP, Pos.WP67_DT_SCAD_COP);
        return buffer;
    }

    public void setWp67DtScadCopNull(String wp67DtScadCopNull) {
        writeString(Pos.WP67_DT_SCAD_COP_NULL, wp67DtScadCopNull, Len.WP67_DT_SCAD_COP_NULL);
    }

    /**Original name: WP67-DT-SCAD-COP-NULL<br>*/
    public String getWp67DtScadCopNull() {
        return readString(Pos.WP67_DT_SCAD_COP_NULL, Len.WP67_DT_SCAD_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_SCAD_COP = 1;
        public static final int WP67_DT_SCAD_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_SCAD_COP = 5;
        public static final int WP67_DT_SCAD_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_SCAD_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
