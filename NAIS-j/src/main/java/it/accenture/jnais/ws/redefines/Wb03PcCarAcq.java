package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PC-CAR-ACQ<br>
 * Variable: WB03-PC-CAR-ACQ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PcCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PcCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PC_CAR_ACQ;
    }

    public void setWb03PcCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PC_CAR_ACQ, Pos.WB03_PC_CAR_ACQ);
    }

    /**Original name: WB03-PC-CAR-ACQ<br>*/
    public AfDecimal getWb03PcCarAcq() {
        return readPackedAsDecimal(Pos.WB03_PC_CAR_ACQ, Len.Int.WB03_PC_CAR_ACQ, Len.Fract.WB03_PC_CAR_ACQ);
    }

    public byte[] getWb03PcCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PC_CAR_ACQ, Pos.WB03_PC_CAR_ACQ);
        return buffer;
    }

    public void setWb03PcCarAcqNull(String wb03PcCarAcqNull) {
        writeString(Pos.WB03_PC_CAR_ACQ_NULL, wb03PcCarAcqNull, Len.WB03_PC_CAR_ACQ_NULL);
    }

    /**Original name: WB03-PC-CAR-ACQ-NULL<br>*/
    public String getWb03PcCarAcqNull() {
        return readString(Pos.WB03_PC_CAR_ACQ_NULL, Len.WB03_PC_CAR_ACQ_NULL);
    }

    public String getWb03PcCarAcqNullFormatted() {
        return Functions.padBlanks(getWb03PcCarAcqNull(), Len.WB03_PC_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PC_CAR_ACQ = 1;
        public static final int WB03_PC_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PC_CAR_ACQ = 4;
        public static final int WB03_PC_CAR_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
