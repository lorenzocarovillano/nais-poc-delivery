package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LOAS0310<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLoas0310 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PMO
    private short tabPmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-PMO-2
    private short tabPmo2 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-BLC
    private short tabBlc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-MOV
    private short tabMov = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ADE
    private short tabAde = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-GRZ
    private short tabGrz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short tabTga = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-POG
    private short tabPog = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RRE
    private short tabRre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short tabRan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-GRZ
    private short grz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ERR
    private short tabErr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AREA-SCHEDA
    private short areaScheda = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-VAR
    private short tabVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-X-RIV-INC
    private short xRivInc = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-VPMO
    private short vpmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-W670
    private short w670 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TGA-W870
    private short tgaW870 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TIT-W870
    private short titW870 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-WK-VAR
    private short wkVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-PREC
    private short tabPrec = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-WK-0040
    private short wk0040 = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setTabPmo(short tabPmo) {
        this.tabPmo = tabPmo;
    }

    public short getTabPmo() {
        return this.tabPmo;
    }

    public void setTabPmo2(short tabPmo2) {
        this.tabPmo2 = tabPmo2;
    }

    public short getTabPmo2() {
        return this.tabPmo2;
    }

    public void setTabBlc(short tabBlc) {
        this.tabBlc = tabBlc;
    }

    public short getTabBlc() {
        return this.tabBlc;
    }

    public void setTabMov(short tabMov) {
        this.tabMov = tabMov;
    }

    public short getTabMov() {
        return this.tabMov;
    }

    public void setTabAde(short tabAde) {
        this.tabAde = tabAde;
    }

    public short getTabAde() {
        return this.tabAde;
    }

    public void setTabGrz(short tabGrz) {
        this.tabGrz = tabGrz;
    }

    public short getTabGrz() {
        return this.tabGrz;
    }

    public void setTabTga(short tabTga) {
        this.tabTga = tabTga;
    }

    public short getTabTga() {
        return this.tabTga;
    }

    public void setTabPog(short tabPog) {
        this.tabPog = tabPog;
    }

    public short getTabPog() {
        return this.tabPog;
    }

    public void setTabRre(short tabRre) {
        this.tabRre = tabRre;
    }

    public short getTabRre() {
        return this.tabRre;
    }

    public void setTabRan(short tabRan) {
        this.tabRan = tabRan;
    }

    public short getTabRan() {
        return this.tabRan;
    }

    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }

    public void setTabErr(short tabErr) {
        this.tabErr = tabErr;
    }

    public short getTabErr() {
        return this.tabErr;
    }

    public void setAreaScheda(short areaScheda) {
        this.areaScheda = areaScheda;
    }

    public short getAreaScheda() {
        return this.areaScheda;
    }

    public void setTabVar(short tabVar) {
        this.tabVar = tabVar;
    }

    public short getTabVar() {
        return this.tabVar;
    }

    public void setxRivInc(short xRivInc) {
        this.xRivInc = xRivInc;
    }

    public short getxRivInc() {
        return this.xRivInc;
    }

    public void setVpmo(short vpmo) {
        this.vpmo = vpmo;
    }

    public short getVpmo() {
        return this.vpmo;
    }

    public void setW670(short w670) {
        this.w670 = w670;
    }

    public short getW670() {
        return this.w670;
    }

    public void setTgaW870(short tgaW870) {
        this.tgaW870 = tgaW870;
    }

    public short getTgaW870() {
        return this.tgaW870;
    }

    public void setTitW870(short titW870) {
        this.titW870 = titW870;
    }

    public short getTitW870() {
        return this.titW870;
    }

    public void setWkVar(short wkVar) {
        this.wkVar = wkVar;
    }

    public short getWkVar() {
        return this.wkVar;
    }

    public void setTabPrec(short tabPrec) {
        this.tabPrec = tabPrec;
    }

    public short getTabPrec() {
        return this.tabPrec;
    }

    public void setWk0040(short wk0040) {
        this.wk0040 = wk0040;
    }

    public short getWk0040() {
        return this.wk0040;
    }
}
