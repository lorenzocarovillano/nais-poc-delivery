package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-IRPEF-K2-ANT<br>
 * Variable: WDFA-IMPST-IRPEF-K2-ANT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstIrpefK2Ant extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstIrpefK2Ant() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_IRPEF_K2_ANT;
    }

    public void setWdfaImpstIrpefK2Ant(AfDecimal wdfaImpstIrpefK2Ant) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_IRPEF_K2_ANT, wdfaImpstIrpefK2Ant.copy());
    }

    public void setWdfaImpstIrpefK2AntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_IRPEF_K2_ANT, Pos.WDFA_IMPST_IRPEF_K2_ANT);
    }

    /**Original name: WDFA-IMPST-IRPEF-K2-ANT<br>*/
    public AfDecimal getWdfaImpstIrpefK2Ant() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_IRPEF_K2_ANT, Len.Int.WDFA_IMPST_IRPEF_K2_ANT, Len.Fract.WDFA_IMPST_IRPEF_K2_ANT);
    }

    public byte[] getWdfaImpstIrpefK2AntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_IRPEF_K2_ANT, Pos.WDFA_IMPST_IRPEF_K2_ANT);
        return buffer;
    }

    public void setWdfaImpstIrpefK2AntNull(String wdfaImpstIrpefK2AntNull) {
        writeString(Pos.WDFA_IMPST_IRPEF_K2_ANT_NULL, wdfaImpstIrpefK2AntNull, Len.WDFA_IMPST_IRPEF_K2_ANT_NULL);
    }

    /**Original name: WDFA-IMPST-IRPEF-K2-ANT-NULL<br>*/
    public String getWdfaImpstIrpefK2AntNull() {
        return readString(Pos.WDFA_IMPST_IRPEF_K2_ANT_NULL, Len.WDFA_IMPST_IRPEF_K2_ANT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_IRPEF_K2_ANT = 1;
        public static final int WDFA_IMPST_IRPEF_K2_ANT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_IRPEF_K2_ANT = 8;
        public static final int WDFA_IMPST_IRPEF_K2_ANT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_IRPEF_K2_ANT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_IRPEF_K2_ANT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
