package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CPT-RIASTO-ECC<br>
 * Variable: B03-CPT-RIASTO-ECC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CptRiastoEcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CptRiastoEcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CPT_RIASTO_ECC;
    }

    public void setB03CptRiastoEcc(AfDecimal b03CptRiastoEcc) {
        writeDecimalAsPacked(Pos.B03_CPT_RIASTO_ECC, b03CptRiastoEcc.copy());
    }

    public void setB03CptRiastoEccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CPT_RIASTO_ECC, Pos.B03_CPT_RIASTO_ECC);
    }

    /**Original name: B03-CPT-RIASTO-ECC<br>*/
    public AfDecimal getB03CptRiastoEcc() {
        return readPackedAsDecimal(Pos.B03_CPT_RIASTO_ECC, Len.Int.B03_CPT_RIASTO_ECC, Len.Fract.B03_CPT_RIASTO_ECC);
    }

    public byte[] getB03CptRiastoEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CPT_RIASTO_ECC, Pos.B03_CPT_RIASTO_ECC);
        return buffer;
    }

    public void setB03CptRiastoEccNull(String b03CptRiastoEccNull) {
        writeString(Pos.B03_CPT_RIASTO_ECC_NULL, b03CptRiastoEccNull, Len.B03_CPT_RIASTO_ECC_NULL);
    }

    /**Original name: B03-CPT-RIASTO-ECC-NULL<br>*/
    public String getB03CptRiastoEccNull() {
        return readString(Pos.B03_CPT_RIASTO_ECC_NULL, Len.B03_CPT_RIASTO_ECC_NULL);
    }

    public String getB03CptRiastoEccNullFormatted() {
        return Functions.padBlanks(getB03CptRiastoEccNull(), Len.B03_CPT_RIASTO_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CPT_RIASTO_ECC = 1;
        public static final int B03_CPT_RIASTO_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CPT_RIASTO_ECC = 8;
        public static final int B03_CPT_RIASTO_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CPT_RIASTO_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CPT_RIASTO_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
