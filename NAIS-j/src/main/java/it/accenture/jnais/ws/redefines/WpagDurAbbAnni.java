package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-DUR-ABB-ANNI<br>
 * Variable: WPAG-DUR-ABB-ANNI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagDurAbbAnni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagDurAbbAnni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_DUR_ABB_ANNI;
    }

    public void setWpagDurAbbAnni(int wpagDurAbbAnni) {
        writeIntAsPacked(Pos.WPAG_DUR_ABB_ANNI, wpagDurAbbAnni, Len.Int.WPAG_DUR_ABB_ANNI);
    }

    public void setWpagDurAbbAnniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_DUR_ABB_ANNI, Pos.WPAG_DUR_ABB_ANNI);
    }

    /**Original name: WPAG-DUR-ABB-ANNI<br>*/
    public int getWpagDurAbbAnni() {
        return readPackedAsInt(Pos.WPAG_DUR_ABB_ANNI, Len.Int.WPAG_DUR_ABB_ANNI);
    }

    public byte[] getWpagDurAbbAnniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_DUR_ABB_ANNI, Pos.WPAG_DUR_ABB_ANNI);
        return buffer;
    }

    public void initWpagDurAbbAnniSpaces() {
        fill(Pos.WPAG_DUR_ABB_ANNI, Len.WPAG_DUR_ABB_ANNI, Types.SPACE_CHAR);
    }

    public void setWpagDurAbbAnniNull(String wpagDurAbbAnniNull) {
        writeString(Pos.WPAG_DUR_ABB_ANNI_NULL, wpagDurAbbAnniNull, Len.WPAG_DUR_ABB_ANNI_NULL);
    }

    /**Original name: WPAG-DUR-ABB-ANNI-NULL<br>*/
    public String getWpagDurAbbAnniNull() {
        return readString(Pos.WPAG_DUR_ABB_ANNI_NULL, Len.WPAG_DUR_ABB_ANNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_DUR_ABB_ANNI = 1;
        public static final int WPAG_DUR_ABB_ANNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DUR_ABB_ANNI = 4;
        public static final int WPAG_DUR_ABB_ANNI_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_DUR_ABB_ANNI = 6;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
