package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.GarDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGar;
import it.accenture.jnais.copy.Ldbv1351;
import it.accenture.jnais.ws.enums.FlagCausale;
import it.accenture.jnais.ws.enums.FlagStato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1350<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1350Data {

    //==== PROPERTIES ====
    /**Original name: DESCRIZ-ERR-DB2<br>
	 * <pre>------------------------------------------------------------
	 *  COMODO
	 * ------------------------------------------------------------</pre>*/
    private String descrizErrDb2 = "";
    //Original name: WK-ID-ADES-DA
    private int wkIdAdesDa = DefaultValues.INT_VAL;
    //Original name: WK-ID-ADES-A
    private int wkIdAdesA = DefaultValues.INT_VAL;
    //Original name: WK-ID-GAR-DA
    private int wkIdGarDa = DefaultValues.INT_VAL;
    //Original name: WK-ID-GAR-A
    private int wkIdGarA = DefaultValues.INT_VAL;
    /**Original name: FLAG-STATO<br>
	 * <pre>------------------------------------------------------------
	 *  FLAGS
	 * ------------------------------------------------------------</pre>*/
    private FlagStato flagStato = new FlagStato();
    //Original name: FLAG-CAUSALE
    private FlagCausale flagCausale = new FlagCausale();
    /**Original name: NEGAZIONE<br>
	 * <pre>------------------------------------------------------------
	 *  COSTANTI
	 * ------------------------------------------------------------</pre>*/
    private String negazione = "NOT";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: LDBV1351
    private Ldbv1351 ldbv1351 = new Ldbv1351();
    //Original name: IND-GAR
    private IndGar indGar = new IndGar();
    //Original name: GAR-DB
    private GarDb garDb = new GarDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWkIdAdesDa(int wkIdAdesDa) {
        this.wkIdAdesDa = wkIdAdesDa;
    }

    public int getWkIdAdesDa() {
        return this.wkIdAdesDa;
    }

    public void setWkIdAdesA(int wkIdAdesA) {
        this.wkIdAdesA = wkIdAdesA;
    }

    public int getWkIdAdesA() {
        return this.wkIdAdesA;
    }

    public void setWkIdGarDa(int wkIdGarDa) {
        this.wkIdGarDa = wkIdGarDa;
    }

    public int getWkIdGarDa() {
        return this.wkIdGarDa;
    }

    public void setWkIdGarA(int wkIdGarA) {
        this.wkIdGarA = wkIdGarA;
    }

    public int getWkIdGarA() {
        return this.wkIdGarA;
    }

    public String getNegazione() {
        return this.negazione;
    }

    public FlagCausale getFlagCausale() {
        return flagCausale;
    }

    public FlagStato getFlagStato() {
        return flagStato;
    }

    public GarDb getGarDb() {
        return garDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGar getIndGar() {
        return indGar;
    }

    public Ldbv1351 getLdbv1351() {
        return ldbv1351;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
