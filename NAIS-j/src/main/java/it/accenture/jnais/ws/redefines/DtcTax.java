package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-TAX<br>
 * Variable: DTC-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_TAX;
    }

    public void setDtcTax(AfDecimal dtcTax) {
        writeDecimalAsPacked(Pos.DTC_TAX, dtcTax.copy());
    }

    public void setDtcTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_TAX, Pos.DTC_TAX);
    }

    /**Original name: DTC-TAX<br>*/
    public AfDecimal getDtcTax() {
        return readPackedAsDecimal(Pos.DTC_TAX, Len.Int.DTC_TAX, Len.Fract.DTC_TAX);
    }

    public byte[] getDtcTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_TAX, Pos.DTC_TAX);
        return buffer;
    }

    public void setDtcTaxNull(String dtcTaxNull) {
        writeString(Pos.DTC_TAX_NULL, dtcTaxNull, Len.DTC_TAX_NULL);
    }

    /**Original name: DTC-TAX-NULL<br>*/
    public String getDtcTaxNull() {
        return readString(Pos.DTC_TAX_NULL, Len.DTC_TAX_NULL);
    }

    public String getDtcTaxNullFormatted() {
        return Functions.padBlanks(getDtcTaxNull(), Len.DTC_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_TAX = 1;
        public static final int DTC_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_TAX = 8;
        public static final int DTC_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
