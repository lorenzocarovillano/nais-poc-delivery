package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-REMUN-ASS<br>
 * Variable: WDTC-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_REMUN_ASS;
    }

    public void setWdtcRemunAss(AfDecimal wdtcRemunAss) {
        writeDecimalAsPacked(Pos.WDTC_REMUN_ASS, wdtcRemunAss.copy());
    }

    public void setWdtcRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_REMUN_ASS, Pos.WDTC_REMUN_ASS);
    }

    /**Original name: WDTC-REMUN-ASS<br>*/
    public AfDecimal getWdtcRemunAss() {
        return readPackedAsDecimal(Pos.WDTC_REMUN_ASS, Len.Int.WDTC_REMUN_ASS, Len.Fract.WDTC_REMUN_ASS);
    }

    public byte[] getWdtcRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_REMUN_ASS, Pos.WDTC_REMUN_ASS);
        return buffer;
    }

    public void initWdtcRemunAssSpaces() {
        fill(Pos.WDTC_REMUN_ASS, Len.WDTC_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWdtcRemunAssNull(String wdtcRemunAssNull) {
        writeString(Pos.WDTC_REMUN_ASS_NULL, wdtcRemunAssNull, Len.WDTC_REMUN_ASS_NULL);
    }

    /**Original name: WDTC-REMUN-ASS-NULL<br>*/
    public String getWdtcRemunAssNull() {
        return readString(Pos.WDTC_REMUN_ASS_NULL, Len.WDTC_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_REMUN_ASS = 1;
        public static final int WDTC_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_REMUN_ASS = 8;
        public static final int WDTC_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
