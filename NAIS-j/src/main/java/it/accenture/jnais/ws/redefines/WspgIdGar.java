package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSPG-ID-GAR<br>
 * Variable: WSPG-ID-GAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WspgIdGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WspgIdGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSPG_ID_GAR;
    }

    public void setWspgIdGar(int wspgIdGar) {
        writeIntAsPacked(Pos.WSPG_ID_GAR, wspgIdGar, Len.Int.WSPG_ID_GAR);
    }

    public void setWspgIdGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSPG_ID_GAR, Pos.WSPG_ID_GAR);
    }

    /**Original name: WSPG-ID-GAR<br>*/
    public int getWspgIdGar() {
        return readPackedAsInt(Pos.WSPG_ID_GAR, Len.Int.WSPG_ID_GAR);
    }

    public byte[] getWspgIdGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSPG_ID_GAR, Pos.WSPG_ID_GAR);
        return buffer;
    }

    public void initWspgIdGarSpaces() {
        fill(Pos.WSPG_ID_GAR, Len.WSPG_ID_GAR, Types.SPACE_CHAR);
    }

    public void setWspgIdGarNull(String wspgIdGarNull) {
        writeString(Pos.WSPG_ID_GAR_NULL, wspgIdGarNull, Len.WSPG_ID_GAR_NULL);
    }

    /**Original name: WSPG-ID-GAR-NULL<br>*/
    public String getWspgIdGarNull() {
        return readString(Pos.WSPG_ID_GAR_NULL, Len.WSPG_ID_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSPG_ID_GAR = 1;
        public static final int WSPG_ID_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_ID_GAR = 5;
        public static final int WSPG_ID_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WSPG_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
