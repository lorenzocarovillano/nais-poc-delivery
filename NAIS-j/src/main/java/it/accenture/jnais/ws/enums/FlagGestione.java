package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-GESTIONE<br>
 * Variable: FLAG-GESTIONE from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagGestione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_GESTIONE);
    public static final String PRODOTTO = "PR";
    public static final String GARANZIE = "GA";

    //==== METHODS ====
    public void setFlagGestione(String flagGestione) {
        this.value = Functions.subString(flagGestione, Len.FLAG_GESTIONE);
    }

    public String getFlagGestione() {
        return this.value;
    }

    public boolean isProdotto() {
        return value.equals(PRODOTTO);
    }

    public void setProdotto() {
        value = PRODOTTO;
    }

    public boolean isGaranzie() {
        return value.equals(GARANZIE);
    }

    public void setGaranzie() {
        value = GARANZIE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_GESTIONE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
