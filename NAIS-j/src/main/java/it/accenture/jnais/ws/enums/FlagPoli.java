package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-POLI<br>
 * Variable: FLAG-POLI from program LVVS2760<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagPoli {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagPoli(char flagPoli) {
        this.value = flagPoli;
    }

    public char getFlagPoli() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
