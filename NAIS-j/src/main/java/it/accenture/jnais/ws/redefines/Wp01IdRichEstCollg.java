package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-ID-RICH-EST-COLLG<br>
 * Variable: WP01-ID-RICH-EST-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01IdRichEstCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01IdRichEstCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_ID_RICH_EST_COLLG;
    }

    public void setWp01IdRichEstCollg(int wp01IdRichEstCollg) {
        writeIntAsPacked(Pos.WP01_ID_RICH_EST_COLLG, wp01IdRichEstCollg, Len.Int.WP01_ID_RICH_EST_COLLG);
    }

    public void setWp01IdRichEstCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_ID_RICH_EST_COLLG, Pos.WP01_ID_RICH_EST_COLLG);
    }

    /**Original name: WP01-ID-RICH-EST-COLLG<br>*/
    public int getWp01IdRichEstCollg() {
        return readPackedAsInt(Pos.WP01_ID_RICH_EST_COLLG, Len.Int.WP01_ID_RICH_EST_COLLG);
    }

    public byte[] getWp01IdRichEstCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_ID_RICH_EST_COLLG, Pos.WP01_ID_RICH_EST_COLLG);
        return buffer;
    }

    public void setWp01IdRichEstCollgNull(String wp01IdRichEstCollgNull) {
        writeString(Pos.WP01_ID_RICH_EST_COLLG_NULL, wp01IdRichEstCollgNull, Len.WP01_ID_RICH_EST_COLLG_NULL);
    }

    /**Original name: WP01-ID-RICH-EST-COLLG-NULL<br>*/
    public String getWp01IdRichEstCollgNull() {
        return readString(Pos.WP01_ID_RICH_EST_COLLG_NULL, Len.WP01_ID_RICH_EST_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_ID_RICH_EST_COLLG = 1;
        public static final int WP01_ID_RICH_EST_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_ID_RICH_EST_COLLG = 5;
        public static final int WP01_ID_RICH_EST_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_ID_RICH_EST_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
