package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-INTR-TECN<br>
 * Variable: WB03-INTR-TECN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03IntrTecn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03IntrTecn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_INTR_TECN;
    }

    public void setWb03IntrTecnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_INTR_TECN, Pos.WB03_INTR_TECN);
    }

    /**Original name: WB03-INTR-TECN<br>*/
    public AfDecimal getWb03IntrTecn() {
        return readPackedAsDecimal(Pos.WB03_INTR_TECN, Len.Int.WB03_INTR_TECN, Len.Fract.WB03_INTR_TECN);
    }

    public byte[] getWb03IntrTecnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_INTR_TECN, Pos.WB03_INTR_TECN);
        return buffer;
    }

    public void setWb03IntrTecnNull(String wb03IntrTecnNull) {
        writeString(Pos.WB03_INTR_TECN_NULL, wb03IntrTecnNull, Len.WB03_INTR_TECN_NULL);
    }

    /**Original name: WB03-INTR-TECN-NULL<br>*/
    public String getWb03IntrTecnNull() {
        return readString(Pos.WB03_INTR_TECN_NULL, Len.WB03_INTR_TECN_NULL);
    }

    public String getWb03IntrTecnNullFormatted() {
        return Functions.padBlanks(getWb03IntrTecnNull(), Len.WB03_INTR_TECN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_INTR_TECN = 1;
        public static final int WB03_INTR_TECN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_INTR_TECN = 4;
        public static final int WB03_INTR_TECN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_INTR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_INTR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
