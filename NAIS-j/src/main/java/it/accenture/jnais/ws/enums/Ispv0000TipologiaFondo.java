package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPV0000-TIPOLOGIA-FONDO<br>
 * Variable: ISPV0000-TIPOLOGIA-FONDO from copybook ISPV0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispv0000TipologiaFondo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char UNIT_OICR = '0';
    public static final char UNIT_ASSIC = '1';
    public static final char INDEX2 = '2';
    public static final char GEST_SPECIALE = '3';
    public static final char SPEC_PROVVISTA = '4';
    public static final char INDICE = '5';

    //==== METHODS ====
    public void setWkTpFondo(char wkTpFondo) {
        this.value = wkTpFondo;
    }

    public char getWkTpFondo() {
        return this.value;
    }

    public boolean isUnitOicr() {
        return value == UNIT_OICR;
    }

    public boolean isUnitAssic() {
        return value == UNIT_ASSIC;
    }

    public boolean isIndex2() {
        return value == INDEX2;
    }
}
