package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB04-VAL-PC<br>
 * Variable: WB04-VAL-PC from program LLBS0266<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb04ValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb04ValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB04_VAL_PC;
    }

    public void setWb04ValPc(AfDecimal wb04ValPc) {
        writeDecimalAsPacked(Pos.WB04_VAL_PC, wb04ValPc.copy());
    }

    public void setWb04ValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB04_VAL_PC, Pos.WB04_VAL_PC);
    }

    /**Original name: WB04-VAL-PC<br>*/
    public AfDecimal getWb04ValPc() {
        return readPackedAsDecimal(Pos.WB04_VAL_PC, Len.Int.WB04_VAL_PC, Len.Fract.WB04_VAL_PC);
    }

    public byte[] getWb04ValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB04_VAL_PC, Pos.WB04_VAL_PC);
        return buffer;
    }

    public void setWb04ValPcNull(String wb04ValPcNull) {
        writeString(Pos.WB04_VAL_PC_NULL, wb04ValPcNull, Len.WB04_VAL_PC_NULL);
    }

    /**Original name: WB04-VAL-PC-NULL<br>*/
    public String getWb04ValPcNull() {
        return readString(Pos.WB04_VAL_PC_NULL, Len.WB04_VAL_PC_NULL);
    }

    public String getWb04ValPcNullFormatted() {
        return Functions.padBlanks(getWb04ValPcNull(), Len.WB04_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB04_VAL_PC = 1;
        public static final int WB04_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB04_VAL_PC = 8;
        public static final int WB04_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB04_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB04_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
