package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L19-VAL-QUO-ACQ<br>
 * Variable: L19-VAL-QUO-ACQ from program IDBSL190<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L19ValQuoAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L19ValQuoAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L19_VAL_QUO_ACQ;
    }

    public void setL19ValQuoAcq(AfDecimal l19ValQuoAcq) {
        writeDecimalAsPacked(Pos.L19_VAL_QUO_ACQ, l19ValQuoAcq.copy());
    }

    public void setL19ValQuoAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L19_VAL_QUO_ACQ, Pos.L19_VAL_QUO_ACQ);
    }

    /**Original name: L19-VAL-QUO-ACQ<br>*/
    public AfDecimal getL19ValQuoAcq() {
        return readPackedAsDecimal(Pos.L19_VAL_QUO_ACQ, Len.Int.L19_VAL_QUO_ACQ, Len.Fract.L19_VAL_QUO_ACQ);
    }

    public byte[] getL19ValQuoAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L19_VAL_QUO_ACQ, Pos.L19_VAL_QUO_ACQ);
        return buffer;
    }

    public void setL19ValQuoAcqNull(String l19ValQuoAcqNull) {
        writeString(Pos.L19_VAL_QUO_ACQ_NULL, l19ValQuoAcqNull, Len.L19_VAL_QUO_ACQ_NULL);
    }

    /**Original name: L19-VAL-QUO-ACQ-NULL<br>*/
    public String getL19ValQuoAcqNull() {
        return readString(Pos.L19_VAL_QUO_ACQ_NULL, Len.L19_VAL_QUO_ACQ_NULL);
    }

    public String getL19ValQuoAcqNullFormatted() {
        return Functions.padBlanks(getL19ValQuoAcqNull(), Len.L19_VAL_QUO_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L19_VAL_QUO_ACQ = 1;
        public static final int L19_VAL_QUO_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L19_VAL_QUO_ACQ = 7;
        public static final int L19_VAL_QUO_ACQ_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L19_VAL_QUO_ACQ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L19_VAL_QUO_ACQ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
