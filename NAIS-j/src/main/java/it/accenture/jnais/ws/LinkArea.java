package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.LinkReturnCode;

/**Original name: LINK-AREA<br>
 * Variable: LINK-AREA from program LCCS0090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LinkArea extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LINK-CONNECT
    private char connect = 'S';
    //Original name: LINK-NOME-TABELLA
    private String nomeTabella = DefaultValues.stringVal(Len.NOME_TABELLA);
    //Original name: LINK-SEQ-TABELLA
    private int seqTabella = DefaultValues.INT_VAL;
    //Original name: LINK-RETURN-CODE
    private LinkReturnCode returnCode = new LinkReturnCode();
    //Original name: LINK-SQLCODE
    private Idso0011SqlcodeSigned sqlcode = new Idso0011SqlcodeSigned();
    //Original name: LINK-DESCRIZ-ERR-DB2
    private String descrizErrDb2 = DefaultValues.stringVal(Len.DESCRIZ_ERR_DB2);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LINK_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLinkAreaBytes(buf);
    }

    public String getAreaIoLccs0090Formatted() {
        return MarshalByteExt.bufferToStr(getLinkAreaBytes());
    }

    public void setLinkAreaBytes(byte[] buffer) {
        setLinkAreaBytes(buffer, 1);
    }

    public byte[] getLinkAreaBytes() {
        byte[] buffer = new byte[Len.LINK_AREA];
        return getLinkAreaBytes(buffer, 1);
    }

    public void setLinkAreaBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getLinkAreaBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void initLccc0090Spaces() {
        initDatiInputSpaces();
        initDatiOutputSpaces();
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        connect = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        nomeTabella = MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, connect);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
        return buffer;
    }

    public void initDatiInputSpaces() {
        connect = Types.SPACE_CHAR;
        nomeTabella = "";
    }

    public void setConnect(char connect) {
        this.connect = connect;
    }

    public char getConnect() {
        return this.connect;
    }

    public void setNomeTabella(String nomeTabella) {
        this.nomeTabella = Functions.subString(nomeTabella, Len.NOME_TABELLA);
    }

    public String getNomeTabella() {
        return this.nomeTabella;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        seqTabella = MarshalByte.readPackedAsInt(buffer, position, Len.Int.SEQ_TABELLA, 0);
        position += Len.SEQ_TABELLA;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, LinkReturnCode.Len.RETURN_CODE));
        position += LinkReturnCode.Len.RETURN_CODE;
        sqlcode.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        descrizErrDb2 = MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR_DB2);
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, seqTabella, Len.Int.SEQ_TABELLA, 0);
        position += Len.SEQ_TABELLA;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), LinkReturnCode.Len.RETURN_CODE);
        position += LinkReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcode.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        MarshalByte.writeString(buffer, position, descrizErrDb2, Len.DESCRIZ_ERR_DB2);
        return buffer;
    }

    public void initDatiOutputSpaces() {
        seqTabella = Types.INVALID_INT_VAL;
        returnCode.setReturnCode("");
        sqlcode.setSqlcodeSigned(Types.INVALID_INT_VAL);
        descrizErrDb2 = "";
    }

    public void setSeqTabella(int seqTabella) {
        this.seqTabella = seqTabella;
    }

    public int getSeqTabella() {
        return this.seqTabella;
    }

    public void setDescrizErrDb2(String descrizErrDb2) {
        this.descrizErrDb2 = Functions.subString(descrizErrDb2, Len.DESCRIZ_ERR_DB2);
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public String getDescrizErrDb2Formatted() {
        return Functions.padBlanks(getDescrizErrDb2(), Len.DESCRIZ_ERR_DB2);
    }

    public LinkReturnCode getReturnCode() {
        return returnCode;
    }

    public Idso0011SqlcodeSigned getSqlcode() {
        return sqlcode;
    }

    @Override
    public byte[] serialize() {
        return getLinkAreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CONNECT = 1;
        public static final int NOME_TABELLA = 20;
        public static final int DATI_INPUT = CONNECT + NOME_TABELLA;
        public static final int SEQ_TABELLA = 5;
        public static final int DESCRIZ_ERR_DB2 = 300;
        public static final int DATI_OUTPUT = SEQ_TABELLA + LinkReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + DESCRIZ_ERR_DB2;
        public static final int LINK_AREA = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SEQ_TABELLA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
