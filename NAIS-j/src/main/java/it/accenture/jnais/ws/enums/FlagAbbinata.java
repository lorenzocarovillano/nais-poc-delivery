package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-ABBINATA<br>
 * Variable: FLAG-ABBINATA from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAbbinata {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagAbbinata(char flagAbbinata) {
        this.value = flagAbbinata;
    }

    public char getFlagAbbinata() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
