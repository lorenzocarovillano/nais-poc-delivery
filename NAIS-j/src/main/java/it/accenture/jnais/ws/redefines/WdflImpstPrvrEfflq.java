package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-PRVR-EFFLQ<br>
 * Variable: WDFL-IMPST-PRVR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstPrvrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstPrvrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_PRVR_EFFLQ;
    }

    public void setWdflImpstPrvrEfflq(AfDecimal wdflImpstPrvrEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_PRVR_EFFLQ, wdflImpstPrvrEfflq.copy());
    }

    public void setWdflImpstPrvrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_PRVR_EFFLQ, Pos.WDFL_IMPST_PRVR_EFFLQ);
    }

    /**Original name: WDFL-IMPST-PRVR-EFFLQ<br>*/
    public AfDecimal getWdflImpstPrvrEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_PRVR_EFFLQ, Len.Int.WDFL_IMPST_PRVR_EFFLQ, Len.Fract.WDFL_IMPST_PRVR_EFFLQ);
    }

    public byte[] getWdflImpstPrvrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_PRVR_EFFLQ, Pos.WDFL_IMPST_PRVR_EFFLQ);
        return buffer;
    }

    public void setWdflImpstPrvrEfflqNull(String wdflImpstPrvrEfflqNull) {
        writeString(Pos.WDFL_IMPST_PRVR_EFFLQ_NULL, wdflImpstPrvrEfflqNull, Len.WDFL_IMPST_PRVR_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPST-PRVR-EFFLQ-NULL<br>*/
    public String getWdflImpstPrvrEfflqNull() {
        return readString(Pos.WDFL_IMPST_PRVR_EFFLQ_NULL, Len.WDFL_IMPST_PRVR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_PRVR_EFFLQ = 1;
        public static final int WDFL_IMPST_PRVR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_PRVR_EFFLQ = 8;
        public static final int WDFL_IMPST_PRVR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_PRVR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_PRVR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
