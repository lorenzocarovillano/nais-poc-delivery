package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-CAR-IAS<br>
 * Variable: DTR-CAR-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_CAR_IAS;
    }

    public void setDtrCarIas(AfDecimal dtrCarIas) {
        writeDecimalAsPacked(Pos.DTR_CAR_IAS, dtrCarIas.copy());
    }

    public void setDtrCarIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_CAR_IAS, Pos.DTR_CAR_IAS);
    }

    /**Original name: DTR-CAR-IAS<br>*/
    public AfDecimal getDtrCarIas() {
        return readPackedAsDecimal(Pos.DTR_CAR_IAS, Len.Int.DTR_CAR_IAS, Len.Fract.DTR_CAR_IAS);
    }

    public byte[] getDtrCarIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_CAR_IAS, Pos.DTR_CAR_IAS);
        return buffer;
    }

    public void setDtrCarIasNull(String dtrCarIasNull) {
        writeString(Pos.DTR_CAR_IAS_NULL, dtrCarIasNull, Len.DTR_CAR_IAS_NULL);
    }

    /**Original name: DTR-CAR-IAS-NULL<br>*/
    public String getDtrCarIasNull() {
        return readString(Pos.DTR_CAR_IAS_NULL, Len.DTR_CAR_IAS_NULL);
    }

    public String getDtrCarIasNullFormatted() {
        return Functions.padBlanks(getDtrCarIasNull(), Len.DTR_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_CAR_IAS = 1;
        public static final int DTR_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_CAR_IAS = 8;
        public static final int DTR_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
