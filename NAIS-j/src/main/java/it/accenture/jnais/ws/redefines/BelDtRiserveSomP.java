package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-DT-RISERVE-SOM-P<br>
 * Variable: BEL-DT-RISERVE-SOM-P from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelDtRiserveSomP extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelDtRiserveSomP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_DT_RISERVE_SOM_P;
    }

    public void setBelDtRiserveSomP(int belDtRiserveSomP) {
        writeIntAsPacked(Pos.BEL_DT_RISERVE_SOM_P, belDtRiserveSomP, Len.Int.BEL_DT_RISERVE_SOM_P);
    }

    public void setBelDtRiserveSomPFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_DT_RISERVE_SOM_P, Pos.BEL_DT_RISERVE_SOM_P);
    }

    /**Original name: BEL-DT-RISERVE-SOM-P<br>*/
    public int getBelDtRiserveSomP() {
        return readPackedAsInt(Pos.BEL_DT_RISERVE_SOM_P, Len.Int.BEL_DT_RISERVE_SOM_P);
    }

    public byte[] getBelDtRiserveSomPAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_DT_RISERVE_SOM_P, Pos.BEL_DT_RISERVE_SOM_P);
        return buffer;
    }

    public void setBelDtRiserveSomPNull(String belDtRiserveSomPNull) {
        writeString(Pos.BEL_DT_RISERVE_SOM_P_NULL, belDtRiserveSomPNull, Len.BEL_DT_RISERVE_SOM_P_NULL);
    }

    /**Original name: BEL-DT-RISERVE-SOM-P-NULL<br>*/
    public String getBelDtRiserveSomPNull() {
        return readString(Pos.BEL_DT_RISERVE_SOM_P_NULL, Len.BEL_DT_RISERVE_SOM_P_NULL);
    }

    public String getBelDtRiserveSomPNullFormatted() {
        return Functions.padBlanks(getBelDtRiserveSomPNull(), Len.BEL_DT_RISERVE_SOM_P_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_DT_RISERVE_SOM_P = 1;
        public static final int BEL_DT_RISERVE_SOM_P_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_DT_RISERVE_SOM_P = 5;
        public static final int BEL_DT_RISERVE_SOM_P_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_DT_RISERVE_SOM_P = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
