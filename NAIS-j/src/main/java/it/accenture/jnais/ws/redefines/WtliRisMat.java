package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-RIS-MAT<br>
 * Variable: WTLI-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_RIS_MAT;
    }

    public void setWtliRisMat(AfDecimal wtliRisMat) {
        writeDecimalAsPacked(Pos.WTLI_RIS_MAT, wtliRisMat.copy());
    }

    public void setWtliRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_RIS_MAT, Pos.WTLI_RIS_MAT);
    }

    /**Original name: WTLI-RIS-MAT<br>*/
    public AfDecimal getWtliRisMat() {
        return readPackedAsDecimal(Pos.WTLI_RIS_MAT, Len.Int.WTLI_RIS_MAT, Len.Fract.WTLI_RIS_MAT);
    }

    public byte[] getWtliRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_RIS_MAT, Pos.WTLI_RIS_MAT);
        return buffer;
    }

    public void initWtliRisMatSpaces() {
        fill(Pos.WTLI_RIS_MAT, Len.WTLI_RIS_MAT, Types.SPACE_CHAR);
    }

    public void setWtliRisMatNull(String wtliRisMatNull) {
        writeString(Pos.WTLI_RIS_MAT_NULL, wtliRisMatNull, Len.WTLI_RIS_MAT_NULL);
    }

    /**Original name: WTLI-RIS-MAT-NULL<br>*/
    public String getWtliRisMatNull() {
        return readString(Pos.WTLI_RIS_MAT_NULL, Len.WTLI_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_RIS_MAT = 1;
        public static final int WTLI_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_RIS_MAT = 8;
        public static final int WTLI_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
