package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-FRAZ-MM<br>
 * Variable: WK-FRAZ-MM from program LVES0245<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFrazMm {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FRAZ_MM);
    public static final String ANN = "00012";
    public static final String MEN = "00001";
    public static final String BIM = "00006";
    public static final String TRI = "00004";
    public static final String QUA = "00003";
    public static final String SEM = "00002";

    //==== METHODS ====
    public void setFrazMm(int frazMm) {
        this.value = NumericDisplay.asString(frazMm, Len.FRAZ_MM);
    }

    public void setFrazMmFormatted(String frazMm) {
        this.value = Trunc.toUnsignedNumeric(frazMm, Len.FRAZ_MM);
    }

    public int getFrazMm() {
        return NumericDisplay.asInt(this.value);
    }

    public String getFrazMmFormatted() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FRAZ_MM = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
