package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-C-SUBRSH-T<br>
 * Variable: B03-C-SUBRSH-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CSubrshT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CSubrshT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_C_SUBRSH_T;
    }

    public void setB03CSubrshT(AfDecimal b03CSubrshT) {
        writeDecimalAsPacked(Pos.B03_C_SUBRSH_T, b03CSubrshT.copy());
    }

    public void setB03CSubrshTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_C_SUBRSH_T, Pos.B03_C_SUBRSH_T);
    }

    /**Original name: B03-C-SUBRSH-T<br>*/
    public AfDecimal getB03CSubrshT() {
        return readPackedAsDecimal(Pos.B03_C_SUBRSH_T, Len.Int.B03_C_SUBRSH_T, Len.Fract.B03_C_SUBRSH_T);
    }

    public byte[] getB03CSubrshTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_C_SUBRSH_T, Pos.B03_C_SUBRSH_T);
        return buffer;
    }

    public void setB03CSubrshTNull(String b03CSubrshTNull) {
        writeString(Pos.B03_C_SUBRSH_T_NULL, b03CSubrshTNull, Len.B03_C_SUBRSH_T_NULL);
    }

    /**Original name: B03-C-SUBRSH-T-NULL<br>*/
    public String getB03CSubrshTNull() {
        return readString(Pos.B03_C_SUBRSH_T_NULL, Len.B03_C_SUBRSH_T_NULL);
    }

    public String getB03CSubrshTNullFormatted() {
        return Functions.padBlanks(getB03CSubrshTNull(), Len.B03_C_SUBRSH_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_C_SUBRSH_T = 1;
        public static final int B03_C_SUBRSH_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_C_SUBRSH_T = 8;
        public static final int B03_C_SUBRSH_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_C_SUBRSH_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_C_SUBRSH_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
