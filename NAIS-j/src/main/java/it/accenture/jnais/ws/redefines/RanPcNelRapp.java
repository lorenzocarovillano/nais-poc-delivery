package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RAN-PC-NEL-RAPP<br>
 * Variable: RAN-PC-NEL-RAPP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RanPcNelRapp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RanPcNelRapp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAN_PC_NEL_RAPP;
    }

    public void setRanPcNelRapp(AfDecimal ranPcNelRapp) {
        writeDecimalAsPacked(Pos.RAN_PC_NEL_RAPP, ranPcNelRapp.copy());
    }

    public void setRanPcNelRappFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RAN_PC_NEL_RAPP, Pos.RAN_PC_NEL_RAPP);
    }

    /**Original name: RAN-PC-NEL-RAPP<br>*/
    public AfDecimal getRanPcNelRapp() {
        return readPackedAsDecimal(Pos.RAN_PC_NEL_RAPP, Len.Int.RAN_PC_NEL_RAPP, Len.Fract.RAN_PC_NEL_RAPP);
    }

    public byte[] getRanPcNelRappAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RAN_PC_NEL_RAPP, Pos.RAN_PC_NEL_RAPP);
        return buffer;
    }

    public void setRanPcNelRappNull(String ranPcNelRappNull) {
        writeString(Pos.RAN_PC_NEL_RAPP_NULL, ranPcNelRappNull, Len.RAN_PC_NEL_RAPP_NULL);
    }

    /**Original name: RAN-PC-NEL-RAPP-NULL<br>*/
    public String getRanPcNelRappNull() {
        return readString(Pos.RAN_PC_NEL_RAPP_NULL, Len.RAN_PC_NEL_RAPP_NULL);
    }

    public String getRanPcNelRappNullFormatted() {
        return Functions.padBlanks(getRanPcNelRappNull(), Len.RAN_PC_NEL_RAPP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RAN_PC_NEL_RAPP = 1;
        public static final int RAN_PC_NEL_RAPP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_PC_NEL_RAPP = 4;
        public static final int RAN_PC_NEL_RAPP_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int RAN_PC_NEL_RAPP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_PC_NEL_RAPP = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
