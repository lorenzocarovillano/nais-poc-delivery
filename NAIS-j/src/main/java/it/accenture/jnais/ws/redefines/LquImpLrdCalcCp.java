package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-LRD-CALC-CP<br>
 * Variable: LQU-IMP-LRD-CALC-CP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpLrdCalcCp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpLrdCalcCp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_LRD_CALC_CP;
    }

    public void setLquImpLrdCalcCp(AfDecimal lquImpLrdCalcCp) {
        writeDecimalAsPacked(Pos.LQU_IMP_LRD_CALC_CP, lquImpLrdCalcCp.copy());
    }

    public void setLquImpLrdCalcCpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_LRD_CALC_CP, Pos.LQU_IMP_LRD_CALC_CP);
    }

    /**Original name: LQU-IMP-LRD-CALC-CP<br>*/
    public AfDecimal getLquImpLrdCalcCp() {
        return readPackedAsDecimal(Pos.LQU_IMP_LRD_CALC_CP, Len.Int.LQU_IMP_LRD_CALC_CP, Len.Fract.LQU_IMP_LRD_CALC_CP);
    }

    public byte[] getLquImpLrdCalcCpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_LRD_CALC_CP, Pos.LQU_IMP_LRD_CALC_CP);
        return buffer;
    }

    public void setLquImpLrdCalcCpNull(String lquImpLrdCalcCpNull) {
        writeString(Pos.LQU_IMP_LRD_CALC_CP_NULL, lquImpLrdCalcCpNull, Len.LQU_IMP_LRD_CALC_CP_NULL);
    }

    /**Original name: LQU-IMP-LRD-CALC-CP-NULL<br>*/
    public String getLquImpLrdCalcCpNull() {
        return readString(Pos.LQU_IMP_LRD_CALC_CP_NULL, Len.LQU_IMP_LRD_CALC_CP_NULL);
    }

    public String getLquImpLrdCalcCpNullFormatted() {
        return Functions.padBlanks(getLquImpLrdCalcCpNull(), Len.LQU_IMP_LRD_CALC_CP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_LRD_CALC_CP = 1;
        public static final int LQU_IMP_LRD_CALC_CP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_LRD_CALC_CP = 8;
        public static final int LQU_IMP_LRD_CALC_CP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_LRD_CALC_CP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_LRD_CALC_CP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
