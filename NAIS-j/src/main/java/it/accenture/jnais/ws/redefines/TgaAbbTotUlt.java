package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ABB-TOT-ULT<br>
 * Variable: TGA-ABB-TOT-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAbbTotUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAbbTotUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ABB_TOT_ULT;
    }

    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        writeDecimalAsPacked(Pos.TGA_ABB_TOT_ULT, tgaAbbTotUlt.copy());
    }

    public void setTgaAbbTotUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ABB_TOT_ULT, Pos.TGA_ABB_TOT_ULT);
    }

    /**Original name: TGA-ABB-TOT-ULT<br>*/
    public AfDecimal getTgaAbbTotUlt() {
        return readPackedAsDecimal(Pos.TGA_ABB_TOT_ULT, Len.Int.TGA_ABB_TOT_ULT, Len.Fract.TGA_ABB_TOT_ULT);
    }

    public byte[] getTgaAbbTotUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ABB_TOT_ULT, Pos.TGA_ABB_TOT_ULT);
        return buffer;
    }

    public void setTgaAbbTotUltNull(String tgaAbbTotUltNull) {
        writeString(Pos.TGA_ABB_TOT_ULT_NULL, tgaAbbTotUltNull, Len.TGA_ABB_TOT_ULT_NULL);
    }

    /**Original name: TGA-ABB-TOT-ULT-NULL<br>*/
    public String getTgaAbbTotUltNull() {
        return readString(Pos.TGA_ABB_TOT_ULT_NULL, Len.TGA_ABB_TOT_ULT_NULL);
    }

    public String getTgaAbbTotUltNullFormatted() {
        return Functions.padBlanks(getTgaAbbTotUltNull(), Len.TGA_ABB_TOT_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ABB_TOT_ULT = 1;
        public static final int TGA_ABB_TOT_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ABB_TOT_ULT = 8;
        public static final int TGA_ABB_TOT_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ABB_TOT_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ABB_TOT_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
