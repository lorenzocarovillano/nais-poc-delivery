package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPOL-DUR-MM<br>
 * Variable: WPOL-DUR-MM from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DUR_MM;
    }

    public void setWpolDurMm(int wpolDurMm) {
        writeIntAsPacked(Pos.WPOL_DUR_MM, wpolDurMm, Len.Int.WPOL_DUR_MM);
    }

    public void setWpolDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DUR_MM, Pos.WPOL_DUR_MM);
    }

    /**Original name: WPOL-DUR-MM<br>*/
    public int getWpolDurMm() {
        return readPackedAsInt(Pos.WPOL_DUR_MM, Len.Int.WPOL_DUR_MM);
    }

    public byte[] getWpolDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DUR_MM, Pos.WPOL_DUR_MM);
        return buffer;
    }

    public void setWpolDurMmNull(String wpolDurMmNull) {
        writeString(Pos.WPOL_DUR_MM_NULL, wpolDurMmNull, Len.WPOL_DUR_MM_NULL);
    }

    /**Original name: WPOL-DUR-MM-NULL<br>*/
    public String getWpolDurMmNull() {
        return readString(Pos.WPOL_DUR_MM_NULL, Len.WPOL_DUR_MM_NULL);
    }

    public String getWpolDurMmNullFormatted() {
        return Functions.padBlanks(getWpolDurMmNull(), Len.WPOL_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DUR_MM = 1;
        public static final int WPOL_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DUR_MM = 3;
        public static final int WPOL_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
