package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L19-DT-RILEVAZIONE-NAV<br>
 * Variable: L19-DT-RILEVAZIONE-NAV from program IDBSL190<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L19DtRilevazioneNav extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L19DtRilevazioneNav() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L19_DT_RILEVAZIONE_NAV;
    }

    public void setL19DtRilevazioneNav(int l19DtRilevazioneNav) {
        writeIntAsPacked(Pos.L19_DT_RILEVAZIONE_NAV, l19DtRilevazioneNav, Len.Int.L19_DT_RILEVAZIONE_NAV);
    }

    public void setL19DtRilevazioneNavFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L19_DT_RILEVAZIONE_NAV, Pos.L19_DT_RILEVAZIONE_NAV);
    }

    /**Original name: L19-DT-RILEVAZIONE-NAV<br>*/
    public int getL19DtRilevazioneNav() {
        return readPackedAsInt(Pos.L19_DT_RILEVAZIONE_NAV, Len.Int.L19_DT_RILEVAZIONE_NAV);
    }

    public byte[] getL19DtRilevazioneNavAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L19_DT_RILEVAZIONE_NAV, Pos.L19_DT_RILEVAZIONE_NAV);
        return buffer;
    }

    public void setL19DtRilevazioneNavNull(String l19DtRilevazioneNavNull) {
        writeString(Pos.L19_DT_RILEVAZIONE_NAV_NULL, l19DtRilevazioneNavNull, Len.L19_DT_RILEVAZIONE_NAV_NULL);
    }

    /**Original name: L19-DT-RILEVAZIONE-NAV-NULL<br>*/
    public String getL19DtRilevazioneNavNull() {
        return readString(Pos.L19_DT_RILEVAZIONE_NAV_NULL, Len.L19_DT_RILEVAZIONE_NAV_NULL);
    }

    public String getL19DtRilevazioneNavNullFormatted() {
        return Functions.padBlanks(getL19DtRilevazioneNavNull(), Len.L19_DT_RILEVAZIONE_NAV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L19_DT_RILEVAZIONE_NAV = 1;
        public static final int L19_DT_RILEVAZIONE_NAV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L19_DT_RILEVAZIONE_NAV = 5;
        public static final int L19_DT_RILEVAZIONE_NAV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L19_DT_RILEVAZIONE_NAV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
