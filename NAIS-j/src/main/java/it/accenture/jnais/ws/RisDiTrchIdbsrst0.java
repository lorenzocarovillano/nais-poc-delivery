package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.RstDtCalc;
import it.accenture.jnais.ws.redefines.RstDtElab;
import it.accenture.jnais.ws.redefines.RstFrazPrePp;
import it.accenture.jnais.ws.redefines.RstIdMoviChiu;
import it.accenture.jnais.ws.redefines.RstIncrXRival;
import it.accenture.jnais.ws.redefines.RstRisAbb;
import it.accenture.jnais.ws.redefines.RstRisAcq;
import it.accenture.jnais.ws.redefines.RstRisBila;
import it.accenture.jnais.ws.redefines.RstRisBnsfdt;
import it.accenture.jnais.ws.redefines.RstRisBnsric;
import it.accenture.jnais.ws.redefines.RstRisComponAssva;
import it.accenture.jnais.ws.redefines.RstRisCosAmmtz;
import it.accenture.jnais.ws.redefines.RstRisFaivl;
import it.accenture.jnais.ws.redefines.RstRisGarCasoMor;
import it.accenture.jnais.ws.redefines.RstRisIntegBasTec;
import it.accenture.jnais.ws.redefines.RstRisIntegDecrTs;
import it.accenture.jnais.ws.redefines.RstRisMat;
import it.accenture.jnais.ws.redefines.RstRisMatEff;
import it.accenture.jnais.ws.redefines.RstRisMinGarto;
import it.accenture.jnais.ws.redefines.RstRisMoviNonInves;
import it.accenture.jnais.ws.redefines.RstRisPrestFaivl;
import it.accenture.jnais.ws.redefines.RstRisRistorniCap;
import it.accenture.jnais.ws.redefines.RstRisRshDflt;
import it.accenture.jnais.ws.redefines.RstRisSopr;
import it.accenture.jnais.ws.redefines.RstRisSpe;
import it.accenture.jnais.ws.redefines.RstRisSpeFaivl;
import it.accenture.jnais.ws.redefines.RstRisTot;
import it.accenture.jnais.ws.redefines.RstRisTrmBns;
import it.accenture.jnais.ws.redefines.RstRisUti;
import it.accenture.jnais.ws.redefines.RstRisZil;
import it.accenture.jnais.ws.redefines.RstRptoPre;
import it.accenture.jnais.ws.redefines.RstUltCoeffAggRis;
import it.accenture.jnais.ws.redefines.RstUltCoeffRis;
import it.accenture.jnais.ws.redefines.RstUltRm;

/**Original name: RIS-DI-TRCH<br>
 * Variable: RIS-DI-TRCH from copybook IDBVRST1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class RisDiTrchIdbsrst0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: RST-ID-RIS-DI-TRCH
    private int rstIdRisDiTrch = DefaultValues.INT_VAL;
    //Original name: RST-ID-MOVI-CRZ
    private int rstIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: RST-ID-MOVI-CHIU
    private RstIdMoviChiu rstIdMoviChiu = new RstIdMoviChiu();
    //Original name: RST-DT-INI-EFF
    private int rstDtIniEff = DefaultValues.INT_VAL;
    //Original name: RST-DT-END-EFF
    private int rstDtEndEff = DefaultValues.INT_VAL;
    //Original name: RST-COD-COMP-ANIA
    private int rstCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RST-TP-CALC-RIS
    private String rstTpCalcRis = DefaultValues.stringVal(Len.RST_TP_CALC_RIS);
    //Original name: RST-ULT-RM
    private RstUltRm rstUltRm = new RstUltRm();
    //Original name: RST-DT-CALC
    private RstDtCalc rstDtCalc = new RstDtCalc();
    //Original name: RST-DT-ELAB
    private RstDtElab rstDtElab = new RstDtElab();
    //Original name: RST-RIS-BILA
    private RstRisBila rstRisBila = new RstRisBila();
    //Original name: RST-RIS-MAT
    private RstRisMat rstRisMat = new RstRisMat();
    //Original name: RST-INCR-X-RIVAL
    private RstIncrXRival rstIncrXRival = new RstIncrXRival();
    //Original name: RST-RPTO-PRE
    private RstRptoPre rstRptoPre = new RstRptoPre();
    //Original name: RST-FRAZ-PRE-PP
    private RstFrazPrePp rstFrazPrePp = new RstFrazPrePp();
    //Original name: RST-RIS-TOT
    private RstRisTot rstRisTot = new RstRisTot();
    //Original name: RST-RIS-SPE
    private RstRisSpe rstRisSpe = new RstRisSpe();
    //Original name: RST-RIS-ABB
    private RstRisAbb rstRisAbb = new RstRisAbb();
    //Original name: RST-RIS-BNSFDT
    private RstRisBnsfdt rstRisBnsfdt = new RstRisBnsfdt();
    //Original name: RST-RIS-SOPR
    private RstRisSopr rstRisSopr = new RstRisSopr();
    //Original name: RST-RIS-INTEG-BAS-TEC
    private RstRisIntegBasTec rstRisIntegBasTec = new RstRisIntegBasTec();
    //Original name: RST-RIS-INTEG-DECR-TS
    private RstRisIntegDecrTs rstRisIntegDecrTs = new RstRisIntegDecrTs();
    //Original name: RST-RIS-GAR-CASO-MOR
    private RstRisGarCasoMor rstRisGarCasoMor = new RstRisGarCasoMor();
    //Original name: RST-RIS-ZIL
    private RstRisZil rstRisZil = new RstRisZil();
    //Original name: RST-RIS-FAIVL
    private RstRisFaivl rstRisFaivl = new RstRisFaivl();
    //Original name: RST-RIS-COS-AMMTZ
    private RstRisCosAmmtz rstRisCosAmmtz = new RstRisCosAmmtz();
    //Original name: RST-RIS-SPE-FAIVL
    private RstRisSpeFaivl rstRisSpeFaivl = new RstRisSpeFaivl();
    //Original name: RST-RIS-PREST-FAIVL
    private RstRisPrestFaivl rstRisPrestFaivl = new RstRisPrestFaivl();
    //Original name: RST-RIS-COMPON-ASSVA
    private RstRisComponAssva rstRisComponAssva = new RstRisComponAssva();
    //Original name: RST-ULT-COEFF-RIS
    private RstUltCoeffRis rstUltCoeffRis = new RstUltCoeffRis();
    //Original name: RST-ULT-COEFF-AGG-RIS
    private RstUltCoeffAggRis rstUltCoeffAggRis = new RstUltCoeffAggRis();
    //Original name: RST-RIS-ACQ
    private RstRisAcq rstRisAcq = new RstRisAcq();
    //Original name: RST-RIS-UTI
    private RstRisUti rstRisUti = new RstRisUti();
    //Original name: RST-RIS-MAT-EFF
    private RstRisMatEff rstRisMatEff = new RstRisMatEff();
    //Original name: RST-RIS-RISTORNI-CAP
    private RstRisRistorniCap rstRisRistorniCap = new RstRisRistorniCap();
    //Original name: RST-RIS-TRM-BNS
    private RstRisTrmBns rstRisTrmBns = new RstRisTrmBns();
    //Original name: RST-RIS-BNSRIC
    private RstRisBnsric rstRisBnsric = new RstRisBnsric();
    //Original name: RST-DS-RIGA
    private long rstDsRiga = DefaultValues.LONG_VAL;
    //Original name: RST-DS-OPER-SQL
    private char rstDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RST-DS-VER
    private int rstDsVer = DefaultValues.INT_VAL;
    //Original name: RST-DS-TS-INI-CPTZ
    private long rstDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: RST-DS-TS-END-CPTZ
    private long rstDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: RST-DS-UTENTE
    private String rstDsUtente = DefaultValues.stringVal(Len.RST_DS_UTENTE);
    //Original name: RST-DS-STATO-ELAB
    private char rstDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RST-ID-TRCH-DI-GAR
    private int rstIdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: RST-COD-FND
    private String rstCodFnd = DefaultValues.stringVal(Len.RST_COD_FND);
    //Original name: RST-ID-POLI
    private int rstIdPoli = DefaultValues.INT_VAL;
    //Original name: RST-ID-ADES
    private int rstIdAdes = DefaultValues.INT_VAL;
    //Original name: RST-ID-GAR
    private int rstIdGar = DefaultValues.INT_VAL;
    //Original name: RST-RIS-MIN-GARTO
    private RstRisMinGarto rstRisMinGarto = new RstRisMinGarto();
    //Original name: RST-RIS-RSH-DFLT
    private RstRisRshDflt rstRisRshDflt = new RstRisRshDflt();
    //Original name: RST-RIS-MOVI-NON-INVES
    private RstRisMoviNonInves rstRisMoviNonInves = new RstRisMoviNonInves();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIS_DI_TRCH;
    }

    @Override
    public void deserialize(byte[] buf) {
        setRisDiTrchBytes(buf);
    }

    public void setRisDiTrchFormatted(String data) {
        byte[] buffer = new byte[Len.RIS_DI_TRCH];
        MarshalByte.writeString(buffer, 1, data, Len.RIS_DI_TRCH);
        setRisDiTrchBytes(buffer, 1);
    }

    public String getRisDiTrchFormatted() {
        return MarshalByteExt.bufferToStr(getRisDiTrchBytes());
    }

    public void setRisDiTrchBytes(byte[] buffer) {
        setRisDiTrchBytes(buffer, 1);
    }

    public byte[] getRisDiTrchBytes() {
        byte[] buffer = new byte[Len.RIS_DI_TRCH];
        return getRisDiTrchBytes(buffer, 1);
    }

    public void setRisDiTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        rstIdRisDiTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_ID_RIS_DI_TRCH, 0);
        position += Len.RST_ID_RIS_DI_TRCH;
        rstIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_ID_MOVI_CRZ, 0);
        position += Len.RST_ID_MOVI_CRZ;
        rstIdMoviChiu.setRstIdMoviChiuFromBuffer(buffer, position);
        position += RstIdMoviChiu.Len.RST_ID_MOVI_CHIU;
        rstDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_DT_INI_EFF, 0);
        position += Len.RST_DT_INI_EFF;
        rstDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_DT_END_EFF, 0);
        position += Len.RST_DT_END_EFF;
        rstCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_COD_COMP_ANIA, 0);
        position += Len.RST_COD_COMP_ANIA;
        rstTpCalcRis = MarshalByte.readString(buffer, position, Len.RST_TP_CALC_RIS);
        position += Len.RST_TP_CALC_RIS;
        rstUltRm.setRstUltRmFromBuffer(buffer, position);
        position += RstUltRm.Len.RST_ULT_RM;
        rstDtCalc.setRstDtCalcFromBuffer(buffer, position);
        position += RstDtCalc.Len.RST_DT_CALC;
        rstDtElab.setRstDtElabFromBuffer(buffer, position);
        position += RstDtElab.Len.RST_DT_ELAB;
        rstRisBila.setRstRisBilaFromBuffer(buffer, position);
        position += RstRisBila.Len.RST_RIS_BILA;
        rstRisMat.setRstRisMatFromBuffer(buffer, position);
        position += RstRisMat.Len.RST_RIS_MAT;
        rstIncrXRival.setRstIncrXRivalFromBuffer(buffer, position);
        position += RstIncrXRival.Len.RST_INCR_X_RIVAL;
        rstRptoPre.setRstRptoPreFromBuffer(buffer, position);
        position += RstRptoPre.Len.RST_RPTO_PRE;
        rstFrazPrePp.setRstFrazPrePpFromBuffer(buffer, position);
        position += RstFrazPrePp.Len.RST_FRAZ_PRE_PP;
        rstRisTot.setRstRisTotFromBuffer(buffer, position);
        position += RstRisTot.Len.RST_RIS_TOT;
        rstRisSpe.setRstRisSpeFromBuffer(buffer, position);
        position += RstRisSpe.Len.RST_RIS_SPE;
        rstRisAbb.setRstRisAbbFromBuffer(buffer, position);
        position += RstRisAbb.Len.RST_RIS_ABB;
        rstRisBnsfdt.setRstRisBnsfdtFromBuffer(buffer, position);
        position += RstRisBnsfdt.Len.RST_RIS_BNSFDT;
        rstRisSopr.setRstRisSoprFromBuffer(buffer, position);
        position += RstRisSopr.Len.RST_RIS_SOPR;
        rstRisIntegBasTec.setRstRisIntegBasTecFromBuffer(buffer, position);
        position += RstRisIntegBasTec.Len.RST_RIS_INTEG_BAS_TEC;
        rstRisIntegDecrTs.setRstRisIntegDecrTsFromBuffer(buffer, position);
        position += RstRisIntegDecrTs.Len.RST_RIS_INTEG_DECR_TS;
        rstRisGarCasoMor.setRstRisGarCasoMorFromBuffer(buffer, position);
        position += RstRisGarCasoMor.Len.RST_RIS_GAR_CASO_MOR;
        rstRisZil.setRstRisZilFromBuffer(buffer, position);
        position += RstRisZil.Len.RST_RIS_ZIL;
        rstRisFaivl.setRstRisFaivlFromBuffer(buffer, position);
        position += RstRisFaivl.Len.RST_RIS_FAIVL;
        rstRisCosAmmtz.setRstRisCosAmmtzFromBuffer(buffer, position);
        position += RstRisCosAmmtz.Len.RST_RIS_COS_AMMTZ;
        rstRisSpeFaivl.setRstRisSpeFaivlFromBuffer(buffer, position);
        position += RstRisSpeFaivl.Len.RST_RIS_SPE_FAIVL;
        rstRisPrestFaivl.setRstRisPrestFaivlFromBuffer(buffer, position);
        position += RstRisPrestFaivl.Len.RST_RIS_PREST_FAIVL;
        rstRisComponAssva.setRstRisComponAssvaFromBuffer(buffer, position);
        position += RstRisComponAssva.Len.RST_RIS_COMPON_ASSVA;
        rstUltCoeffRis.setRstUltCoeffRisFromBuffer(buffer, position);
        position += RstUltCoeffRis.Len.RST_ULT_COEFF_RIS;
        rstUltCoeffAggRis.setRstUltCoeffAggRisFromBuffer(buffer, position);
        position += RstUltCoeffAggRis.Len.RST_ULT_COEFF_AGG_RIS;
        rstRisAcq.setRstRisAcqFromBuffer(buffer, position);
        position += RstRisAcq.Len.RST_RIS_ACQ;
        rstRisUti.setRstRisUtiFromBuffer(buffer, position);
        position += RstRisUti.Len.RST_RIS_UTI;
        rstRisMatEff.setRstRisMatEffFromBuffer(buffer, position);
        position += RstRisMatEff.Len.RST_RIS_MAT_EFF;
        rstRisRistorniCap.setRstRisRistorniCapFromBuffer(buffer, position);
        position += RstRisRistorniCap.Len.RST_RIS_RISTORNI_CAP;
        rstRisTrmBns.setRstRisTrmBnsFromBuffer(buffer, position);
        position += RstRisTrmBns.Len.RST_RIS_TRM_BNS;
        rstRisBnsric.setRstRisBnsricFromBuffer(buffer, position);
        position += RstRisBnsric.Len.RST_RIS_BNSRIC;
        rstDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RST_DS_RIGA, 0);
        position += Len.RST_DS_RIGA;
        rstDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rstDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_DS_VER, 0);
        position += Len.RST_DS_VER;
        rstDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RST_DS_TS_INI_CPTZ, 0);
        position += Len.RST_DS_TS_INI_CPTZ;
        rstDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RST_DS_TS_END_CPTZ, 0);
        position += Len.RST_DS_TS_END_CPTZ;
        rstDsUtente = MarshalByte.readString(buffer, position, Len.RST_DS_UTENTE);
        position += Len.RST_DS_UTENTE;
        rstDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        rstIdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_ID_TRCH_DI_GAR, 0);
        position += Len.RST_ID_TRCH_DI_GAR;
        rstCodFnd = MarshalByte.readString(buffer, position, Len.RST_COD_FND);
        position += Len.RST_COD_FND;
        rstIdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_ID_POLI, 0);
        position += Len.RST_ID_POLI;
        rstIdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_ID_ADES, 0);
        position += Len.RST_ID_ADES;
        rstIdGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RST_ID_GAR, 0);
        position += Len.RST_ID_GAR;
        rstRisMinGarto.setRstRisMinGartoFromBuffer(buffer, position);
        position += RstRisMinGarto.Len.RST_RIS_MIN_GARTO;
        rstRisRshDflt.setRstRisRshDfltFromBuffer(buffer, position);
        position += RstRisRshDflt.Len.RST_RIS_RSH_DFLT;
        rstRisMoviNonInves.setRstRisMoviNonInvesFromBuffer(buffer, position);
    }

    public byte[] getRisDiTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, rstIdRisDiTrch, Len.Int.RST_ID_RIS_DI_TRCH, 0);
        position += Len.RST_ID_RIS_DI_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, rstIdMoviCrz, Len.Int.RST_ID_MOVI_CRZ, 0);
        position += Len.RST_ID_MOVI_CRZ;
        rstIdMoviChiu.getRstIdMoviChiuAsBuffer(buffer, position);
        position += RstIdMoviChiu.Len.RST_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, rstDtIniEff, Len.Int.RST_DT_INI_EFF, 0);
        position += Len.RST_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rstDtEndEff, Len.Int.RST_DT_END_EFF, 0);
        position += Len.RST_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, rstCodCompAnia, Len.Int.RST_COD_COMP_ANIA, 0);
        position += Len.RST_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, rstTpCalcRis, Len.RST_TP_CALC_RIS);
        position += Len.RST_TP_CALC_RIS;
        rstUltRm.getRstUltRmAsBuffer(buffer, position);
        position += RstUltRm.Len.RST_ULT_RM;
        rstDtCalc.getRstDtCalcAsBuffer(buffer, position);
        position += RstDtCalc.Len.RST_DT_CALC;
        rstDtElab.getRstDtElabAsBuffer(buffer, position);
        position += RstDtElab.Len.RST_DT_ELAB;
        rstRisBila.getRstRisBilaAsBuffer(buffer, position);
        position += RstRisBila.Len.RST_RIS_BILA;
        rstRisMat.getRstRisMatAsBuffer(buffer, position);
        position += RstRisMat.Len.RST_RIS_MAT;
        rstIncrXRival.getRstIncrXRivalAsBuffer(buffer, position);
        position += RstIncrXRival.Len.RST_INCR_X_RIVAL;
        rstRptoPre.getRstRptoPreAsBuffer(buffer, position);
        position += RstRptoPre.Len.RST_RPTO_PRE;
        rstFrazPrePp.getRstFrazPrePpAsBuffer(buffer, position);
        position += RstFrazPrePp.Len.RST_FRAZ_PRE_PP;
        rstRisTot.getRstRisTotAsBuffer(buffer, position);
        position += RstRisTot.Len.RST_RIS_TOT;
        rstRisSpe.getRstRisSpeAsBuffer(buffer, position);
        position += RstRisSpe.Len.RST_RIS_SPE;
        rstRisAbb.getRstRisAbbAsBuffer(buffer, position);
        position += RstRisAbb.Len.RST_RIS_ABB;
        rstRisBnsfdt.getRstRisBnsfdtAsBuffer(buffer, position);
        position += RstRisBnsfdt.Len.RST_RIS_BNSFDT;
        rstRisSopr.getRstRisSoprAsBuffer(buffer, position);
        position += RstRisSopr.Len.RST_RIS_SOPR;
        rstRisIntegBasTec.getRstRisIntegBasTecAsBuffer(buffer, position);
        position += RstRisIntegBasTec.Len.RST_RIS_INTEG_BAS_TEC;
        rstRisIntegDecrTs.getRstRisIntegDecrTsAsBuffer(buffer, position);
        position += RstRisIntegDecrTs.Len.RST_RIS_INTEG_DECR_TS;
        rstRisGarCasoMor.getRstRisGarCasoMorAsBuffer(buffer, position);
        position += RstRisGarCasoMor.Len.RST_RIS_GAR_CASO_MOR;
        rstRisZil.getRstRisZilAsBuffer(buffer, position);
        position += RstRisZil.Len.RST_RIS_ZIL;
        rstRisFaivl.getRstRisFaivlAsBuffer(buffer, position);
        position += RstRisFaivl.Len.RST_RIS_FAIVL;
        rstRisCosAmmtz.getRstRisCosAmmtzAsBuffer(buffer, position);
        position += RstRisCosAmmtz.Len.RST_RIS_COS_AMMTZ;
        rstRisSpeFaivl.getRstRisSpeFaivlAsBuffer(buffer, position);
        position += RstRisSpeFaivl.Len.RST_RIS_SPE_FAIVL;
        rstRisPrestFaivl.getRstRisPrestFaivlAsBuffer(buffer, position);
        position += RstRisPrestFaivl.Len.RST_RIS_PREST_FAIVL;
        rstRisComponAssva.getRstRisComponAssvaAsBuffer(buffer, position);
        position += RstRisComponAssva.Len.RST_RIS_COMPON_ASSVA;
        rstUltCoeffRis.getRstUltCoeffRisAsBuffer(buffer, position);
        position += RstUltCoeffRis.Len.RST_ULT_COEFF_RIS;
        rstUltCoeffAggRis.getRstUltCoeffAggRisAsBuffer(buffer, position);
        position += RstUltCoeffAggRis.Len.RST_ULT_COEFF_AGG_RIS;
        rstRisAcq.getRstRisAcqAsBuffer(buffer, position);
        position += RstRisAcq.Len.RST_RIS_ACQ;
        rstRisUti.getRstRisUtiAsBuffer(buffer, position);
        position += RstRisUti.Len.RST_RIS_UTI;
        rstRisMatEff.getRstRisMatEffAsBuffer(buffer, position);
        position += RstRisMatEff.Len.RST_RIS_MAT_EFF;
        rstRisRistorniCap.getRstRisRistorniCapAsBuffer(buffer, position);
        position += RstRisRistorniCap.Len.RST_RIS_RISTORNI_CAP;
        rstRisTrmBns.getRstRisTrmBnsAsBuffer(buffer, position);
        position += RstRisTrmBns.Len.RST_RIS_TRM_BNS;
        rstRisBnsric.getRstRisBnsricAsBuffer(buffer, position);
        position += RstRisBnsric.Len.RST_RIS_BNSRIC;
        MarshalByte.writeLongAsPacked(buffer, position, rstDsRiga, Len.Int.RST_DS_RIGA, 0);
        position += Len.RST_DS_RIGA;
        MarshalByte.writeChar(buffer, position, rstDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, rstDsVer, Len.Int.RST_DS_VER, 0);
        position += Len.RST_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, rstDsTsIniCptz, Len.Int.RST_DS_TS_INI_CPTZ, 0);
        position += Len.RST_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, rstDsTsEndCptz, Len.Int.RST_DS_TS_END_CPTZ, 0);
        position += Len.RST_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, rstDsUtente, Len.RST_DS_UTENTE);
        position += Len.RST_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, rstDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, rstIdTrchDiGar, Len.Int.RST_ID_TRCH_DI_GAR, 0);
        position += Len.RST_ID_TRCH_DI_GAR;
        MarshalByte.writeString(buffer, position, rstCodFnd, Len.RST_COD_FND);
        position += Len.RST_COD_FND;
        MarshalByte.writeIntAsPacked(buffer, position, rstIdPoli, Len.Int.RST_ID_POLI, 0);
        position += Len.RST_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, rstIdAdes, Len.Int.RST_ID_ADES, 0);
        position += Len.RST_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, rstIdGar, Len.Int.RST_ID_GAR, 0);
        position += Len.RST_ID_GAR;
        rstRisMinGarto.getRstRisMinGartoAsBuffer(buffer, position);
        position += RstRisMinGarto.Len.RST_RIS_MIN_GARTO;
        rstRisRshDflt.getRstRisRshDfltAsBuffer(buffer, position);
        position += RstRisRshDflt.Len.RST_RIS_RSH_DFLT;
        rstRisMoviNonInves.getRstRisMoviNonInvesAsBuffer(buffer, position);
        return buffer;
    }

    public void setRstIdRisDiTrch(int rstIdRisDiTrch) {
        this.rstIdRisDiTrch = rstIdRisDiTrch;
    }

    public int getRstIdRisDiTrch() {
        return this.rstIdRisDiTrch;
    }

    public void setRstIdMoviCrz(int rstIdMoviCrz) {
        this.rstIdMoviCrz = rstIdMoviCrz;
    }

    public int getRstIdMoviCrz() {
        return this.rstIdMoviCrz;
    }

    public void setRstDtIniEff(int rstDtIniEff) {
        this.rstDtIniEff = rstDtIniEff;
    }

    public int getRstDtIniEff() {
        return this.rstDtIniEff;
    }

    public void setRstDtEndEff(int rstDtEndEff) {
        this.rstDtEndEff = rstDtEndEff;
    }

    public int getRstDtEndEff() {
        return this.rstDtEndEff;
    }

    public void setRstCodCompAnia(int rstCodCompAnia) {
        this.rstCodCompAnia = rstCodCompAnia;
    }

    public int getRstCodCompAnia() {
        return this.rstCodCompAnia;
    }

    public void setRstTpCalcRis(String rstTpCalcRis) {
        this.rstTpCalcRis = Functions.subString(rstTpCalcRis, Len.RST_TP_CALC_RIS);
    }

    public String getRstTpCalcRis() {
        return this.rstTpCalcRis;
    }

    public String getRstTpCalcRisFormatted() {
        return Functions.padBlanks(getRstTpCalcRis(), Len.RST_TP_CALC_RIS);
    }

    public void setRstDsRiga(long rstDsRiga) {
        this.rstDsRiga = rstDsRiga;
    }

    public long getRstDsRiga() {
        return this.rstDsRiga;
    }

    public void setRstDsOperSql(char rstDsOperSql) {
        this.rstDsOperSql = rstDsOperSql;
    }

    public void setRstDsOperSqlFormatted(String rstDsOperSql) {
        setRstDsOperSql(Functions.charAt(rstDsOperSql, Types.CHAR_SIZE));
    }

    public char getRstDsOperSql() {
        return this.rstDsOperSql;
    }

    public void setRstDsVer(int rstDsVer) {
        this.rstDsVer = rstDsVer;
    }

    public int getRstDsVer() {
        return this.rstDsVer;
    }

    public void setRstDsTsIniCptz(long rstDsTsIniCptz) {
        this.rstDsTsIniCptz = rstDsTsIniCptz;
    }

    public long getRstDsTsIniCptz() {
        return this.rstDsTsIniCptz;
    }

    public void setRstDsTsEndCptz(long rstDsTsEndCptz) {
        this.rstDsTsEndCptz = rstDsTsEndCptz;
    }

    public long getRstDsTsEndCptz() {
        return this.rstDsTsEndCptz;
    }

    public void setRstDsUtente(String rstDsUtente) {
        this.rstDsUtente = Functions.subString(rstDsUtente, Len.RST_DS_UTENTE);
    }

    public String getRstDsUtente() {
        return this.rstDsUtente;
    }

    public void setRstDsStatoElab(char rstDsStatoElab) {
        this.rstDsStatoElab = rstDsStatoElab;
    }

    public void setRstDsStatoElabFormatted(String rstDsStatoElab) {
        setRstDsStatoElab(Functions.charAt(rstDsStatoElab, Types.CHAR_SIZE));
    }

    public char getRstDsStatoElab() {
        return this.rstDsStatoElab;
    }

    public void setRstIdTrchDiGar(int rstIdTrchDiGar) {
        this.rstIdTrchDiGar = rstIdTrchDiGar;
    }

    public int getRstIdTrchDiGar() {
        return this.rstIdTrchDiGar;
    }

    public void setRstCodFnd(String rstCodFnd) {
        this.rstCodFnd = Functions.subString(rstCodFnd, Len.RST_COD_FND);
    }

    public String getRstCodFnd() {
        return this.rstCodFnd;
    }

    public String getRstCodFndFormatted() {
        return Functions.padBlanks(getRstCodFnd(), Len.RST_COD_FND);
    }

    public void setRstIdPoli(int rstIdPoli) {
        this.rstIdPoli = rstIdPoli;
    }

    public int getRstIdPoli() {
        return this.rstIdPoli;
    }

    public void setRstIdAdes(int rstIdAdes) {
        this.rstIdAdes = rstIdAdes;
    }

    public int getRstIdAdes() {
        return this.rstIdAdes;
    }

    public void setRstIdGar(int rstIdGar) {
        this.rstIdGar = rstIdGar;
    }

    public int getRstIdGar() {
        return this.rstIdGar;
    }

    public RstDtCalc getRstDtCalc() {
        return rstDtCalc;
    }

    public RstDtElab getRstDtElab() {
        return rstDtElab;
    }

    public RstFrazPrePp getRstFrazPrePp() {
        return rstFrazPrePp;
    }

    public RstIdMoviChiu getRstIdMoviChiu() {
        return rstIdMoviChiu;
    }

    public RstIncrXRival getRstIncrXRival() {
        return rstIncrXRival;
    }

    public RstRisAbb getRstRisAbb() {
        return rstRisAbb;
    }

    public RstRisAcq getRstRisAcq() {
        return rstRisAcq;
    }

    public RstRisBila getRstRisBila() {
        return rstRisBila;
    }

    public RstRisBnsfdt getRstRisBnsfdt() {
        return rstRisBnsfdt;
    }

    public RstRisBnsric getRstRisBnsric() {
        return rstRisBnsric;
    }

    public RstRisComponAssva getRstRisComponAssva() {
        return rstRisComponAssva;
    }

    public RstRisCosAmmtz getRstRisCosAmmtz() {
        return rstRisCosAmmtz;
    }

    public RstRisFaivl getRstRisFaivl() {
        return rstRisFaivl;
    }

    public RstRisGarCasoMor getRstRisGarCasoMor() {
        return rstRisGarCasoMor;
    }

    public RstRisIntegBasTec getRstRisIntegBasTec() {
        return rstRisIntegBasTec;
    }

    public RstRisIntegDecrTs getRstRisIntegDecrTs() {
        return rstRisIntegDecrTs;
    }

    public RstRisMat getRstRisMat() {
        return rstRisMat;
    }

    public RstRisMatEff getRstRisMatEff() {
        return rstRisMatEff;
    }

    public RstRisMinGarto getRstRisMinGarto() {
        return rstRisMinGarto;
    }

    public RstRisMoviNonInves getRstRisMoviNonInves() {
        return rstRisMoviNonInves;
    }

    public RstRisPrestFaivl getRstRisPrestFaivl() {
        return rstRisPrestFaivl;
    }

    public RstRisRistorniCap getRstRisRistorniCap() {
        return rstRisRistorniCap;
    }

    public RstRisRshDflt getRstRisRshDflt() {
        return rstRisRshDflt;
    }

    public RstRisSopr getRstRisSopr() {
        return rstRisSopr;
    }

    public RstRisSpe getRstRisSpe() {
        return rstRisSpe;
    }

    public RstRisSpeFaivl getRstRisSpeFaivl() {
        return rstRisSpeFaivl;
    }

    public RstRisTot getRstRisTot() {
        return rstRisTot;
    }

    public RstRisTrmBns getRstRisTrmBns() {
        return rstRisTrmBns;
    }

    public RstRisUti getRstRisUti() {
        return rstRisUti;
    }

    public RstRisZil getRstRisZil() {
        return rstRisZil;
    }

    public RstRptoPre getRstRptoPre() {
        return rstRptoPre;
    }

    public RstUltCoeffAggRis getRstUltCoeffAggRis() {
        return rstUltCoeffAggRis;
    }

    public RstUltCoeffRis getRstUltCoeffRis() {
        return rstUltCoeffRis;
    }

    public RstUltRm getRstUltRm() {
        return rstUltRm;
    }

    @Override
    public byte[] serialize() {
        return getRisDiTrchBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_ID_RIS_DI_TRCH = 5;
        public static final int RST_ID_MOVI_CRZ = 5;
        public static final int RST_DT_INI_EFF = 5;
        public static final int RST_DT_END_EFF = 5;
        public static final int RST_COD_COMP_ANIA = 3;
        public static final int RST_TP_CALC_RIS = 2;
        public static final int RST_DS_RIGA = 6;
        public static final int RST_DS_OPER_SQL = 1;
        public static final int RST_DS_VER = 5;
        public static final int RST_DS_TS_INI_CPTZ = 10;
        public static final int RST_DS_TS_END_CPTZ = 10;
        public static final int RST_DS_UTENTE = 20;
        public static final int RST_DS_STATO_ELAB = 1;
        public static final int RST_ID_TRCH_DI_GAR = 5;
        public static final int RST_COD_FND = 12;
        public static final int RST_ID_POLI = 5;
        public static final int RST_ID_ADES = 5;
        public static final int RST_ID_GAR = 5;
        public static final int RIS_DI_TRCH = RST_ID_RIS_DI_TRCH + RST_ID_MOVI_CRZ + RstIdMoviChiu.Len.RST_ID_MOVI_CHIU + RST_DT_INI_EFF + RST_DT_END_EFF + RST_COD_COMP_ANIA + RST_TP_CALC_RIS + RstUltRm.Len.RST_ULT_RM + RstDtCalc.Len.RST_DT_CALC + RstDtElab.Len.RST_DT_ELAB + RstRisBila.Len.RST_RIS_BILA + RstRisMat.Len.RST_RIS_MAT + RstIncrXRival.Len.RST_INCR_X_RIVAL + RstRptoPre.Len.RST_RPTO_PRE + RstFrazPrePp.Len.RST_FRAZ_PRE_PP + RstRisTot.Len.RST_RIS_TOT + RstRisSpe.Len.RST_RIS_SPE + RstRisAbb.Len.RST_RIS_ABB + RstRisBnsfdt.Len.RST_RIS_BNSFDT + RstRisSopr.Len.RST_RIS_SOPR + RstRisIntegBasTec.Len.RST_RIS_INTEG_BAS_TEC + RstRisIntegDecrTs.Len.RST_RIS_INTEG_DECR_TS + RstRisGarCasoMor.Len.RST_RIS_GAR_CASO_MOR + RstRisZil.Len.RST_RIS_ZIL + RstRisFaivl.Len.RST_RIS_FAIVL + RstRisCosAmmtz.Len.RST_RIS_COS_AMMTZ + RstRisSpeFaivl.Len.RST_RIS_SPE_FAIVL + RstRisPrestFaivl.Len.RST_RIS_PREST_FAIVL + RstRisComponAssva.Len.RST_RIS_COMPON_ASSVA + RstUltCoeffRis.Len.RST_ULT_COEFF_RIS + RstUltCoeffAggRis.Len.RST_ULT_COEFF_AGG_RIS + RstRisAcq.Len.RST_RIS_ACQ + RstRisUti.Len.RST_RIS_UTI + RstRisMatEff.Len.RST_RIS_MAT_EFF + RstRisRistorniCap.Len.RST_RIS_RISTORNI_CAP + RstRisTrmBns.Len.RST_RIS_TRM_BNS + RstRisBnsric.Len.RST_RIS_BNSRIC + RST_DS_RIGA + RST_DS_OPER_SQL + RST_DS_VER + RST_DS_TS_INI_CPTZ + RST_DS_TS_END_CPTZ + RST_DS_UTENTE + RST_DS_STATO_ELAB + RST_ID_TRCH_DI_GAR + RST_COD_FND + RST_ID_POLI + RST_ID_ADES + RST_ID_GAR + RstRisMinGarto.Len.RST_RIS_MIN_GARTO + RstRisRshDflt.Len.RST_RIS_RSH_DFLT + RstRisMoviNonInves.Len.RST_RIS_MOVI_NON_INVES;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_ID_RIS_DI_TRCH = 9;
            public static final int RST_ID_MOVI_CRZ = 9;
            public static final int RST_DT_INI_EFF = 8;
            public static final int RST_DT_END_EFF = 8;
            public static final int RST_COD_COMP_ANIA = 5;
            public static final int RST_DS_RIGA = 10;
            public static final int RST_DS_VER = 9;
            public static final int RST_DS_TS_INI_CPTZ = 18;
            public static final int RST_DS_TS_END_CPTZ = 18;
            public static final int RST_ID_TRCH_DI_GAR = 9;
            public static final int RST_ID_POLI = 9;
            public static final int RST_ID_ADES = 9;
            public static final int RST_ID_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
