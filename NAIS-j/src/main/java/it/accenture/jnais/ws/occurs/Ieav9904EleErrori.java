package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: IEAV9904-ELE-ERRORI<br>
 * Variables: IEAV9904-ELE-ERRORI from copybook IEAV9904<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ieav9904EleErrori implements ICopyable<Ieav9904EleErrori> {

    //==== PROPERTIES ====
    //Original name: IEAV9904-DESC-ERRORE
    private String descErrore = DefaultValues.stringVal(Len.DESC_ERRORE);
    //Original name: IEAV9904-COD-ERRORE
    private int codErrore = DefaultValues.INT_VAL;
    //Original name: IEAV9904-LIV-GRAVITA-BE
    private String livGravitaBe = DefaultValues.stringVal(Len.LIV_GRAVITA_BE);
    //Original name: IEAV9904-TIPO-TRATT-FE
    private char tipoTrattFe = DefaultValues.CHAR_VAL;

    //==== CONSTRUCTORS ====
    public Ieav9904EleErrori() {
    }

    public Ieav9904EleErrori(Ieav9904EleErrori eleErrori) {
        this();
        this.descErrore = eleErrori.descErrore;
        this.codErrore = eleErrori.codErrore;
        this.livGravitaBe = eleErrori.livGravitaBe;
        this.tipoTrattFe = eleErrori.tipoTrattFe;
    }

    //==== METHODS ====
    public void setDescErrore(String descErrore) {
        this.descErrore = Functions.subString(descErrore, Len.DESC_ERRORE);
    }

    public String getDescErrore() {
        return this.descErrore;
    }

    public void setCodErrore(int codErrore) {
        this.codErrore = codErrore;
    }

    public int getCodErrore() {
        return this.codErrore;
    }

    public void setLivGravitaBe(short livGravitaBe) {
        this.livGravitaBe = NumericDisplay.asString(livGravitaBe, Len.LIV_GRAVITA_BE);
    }

    public short getLivGravitaBe() {
        return NumericDisplay.asShort(this.livGravitaBe);
    }

    public String getLivGravitaBeFormatted() {
        return this.livGravitaBe;
    }

    public void setTipoTrattFe(char tipoTrattFe) {
        this.tipoTrattFe = tipoTrattFe;
    }

    public char getTipoTrattFe() {
        return this.tipoTrattFe;
    }

    public Ieav9904EleErrori copy() {
        return new Ieav9904EleErrori(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE = 200;
        public static final int COD_ERRORE = 6;
        public static final int LIV_GRAVITA_BE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
