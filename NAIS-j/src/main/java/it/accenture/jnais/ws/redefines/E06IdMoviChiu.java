package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E06-ID-MOVI-CHIU<br>
 * Variable: E06-ID-MOVI-CHIU from program IDBSE060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E06IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E06IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E06_ID_MOVI_CHIU;
    }

    public void setE06IdMoviChiu(int e06IdMoviChiu) {
        writeIntAsPacked(Pos.E06_ID_MOVI_CHIU, e06IdMoviChiu, Len.Int.E06_ID_MOVI_CHIU);
    }

    public void setE06IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E06_ID_MOVI_CHIU, Pos.E06_ID_MOVI_CHIU);
    }

    /**Original name: E06-ID-MOVI-CHIU<br>*/
    public int getE06IdMoviChiu() {
        return readPackedAsInt(Pos.E06_ID_MOVI_CHIU, Len.Int.E06_ID_MOVI_CHIU);
    }

    public byte[] getE06IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E06_ID_MOVI_CHIU, Pos.E06_ID_MOVI_CHIU);
        return buffer;
    }

    public void setE06IdMoviChiuNull(String e06IdMoviChiuNull) {
        writeString(Pos.E06_ID_MOVI_CHIU_NULL, e06IdMoviChiuNull, Len.E06_ID_MOVI_CHIU_NULL);
    }

    /**Original name: E06-ID-MOVI-CHIU-NULL<br>*/
    public String getE06IdMoviChiuNull() {
        return readString(Pos.E06_ID_MOVI_CHIU_NULL, Len.E06_ID_MOVI_CHIU_NULL);
    }

    public String getE06IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getE06IdMoviChiuNull(), Len.E06_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E06_ID_MOVI_CHIU = 1;
        public static final int E06_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E06_ID_MOVI_CHIU = 5;
        public static final int E06_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E06_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
