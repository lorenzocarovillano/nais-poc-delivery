package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvque1;
import it.accenture.jnais.copy.WqueDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WQUE-TAB-QUEST<br>
 * Variables: WQUE-TAB-QUEST from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WqueTabQuest {

    //==== PROPERTIES ====
    //Original name: LCCVQUE1
    private Lccvque1 lccvque1 = new Lccvque1();

    //==== METHODS ====
    public void setVqueTabQuestBytes(byte[] buffer) {
        setWqueTabQuestBytes(buffer, 1);
    }

    public byte[] getTabQuestBytes() {
        byte[] buffer = new byte[Len.WQUE_TAB_QUEST];
        return getWqueTabQuestBytes(buffer, 1);
    }

    public void setWqueTabQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvque1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvque1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvque1.Len.Int.ID_PTF, 0));
        position += Lccvque1.Len.ID_PTF;
        lccvque1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWqueTabQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvque1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvque1.getIdPtf(), Lccvque1.Len.Int.ID_PTF, 0);
        position += Lccvque1.Len.ID_PTF;
        lccvque1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWqueTabQuestSpaces() {
        lccvque1.initLccvque1Spaces();
    }

    public Lccvque1 getLccvque1() {
        return lccvque1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WQUE_TAB_QUEST = WpolStatus.Len.STATUS + Lccvque1.Len.ID_PTF + WqueDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
