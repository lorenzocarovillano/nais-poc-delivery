package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRL-DT-SIN-1O-ASSTO<br>
 * Variable: WGRL-DT-SIN-1O-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrlDtSin1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrlDtSin1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRL_DT_SIN1O_ASSTO;
    }

    public void setWgrlDtSin1oAssto(int wgrlDtSin1oAssto) {
        writeIntAsPacked(Pos.WGRL_DT_SIN1O_ASSTO, wgrlDtSin1oAssto, Len.Int.WGRL_DT_SIN1O_ASSTO);
    }

    public void setWgrlDtSin1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRL_DT_SIN1O_ASSTO, Pos.WGRL_DT_SIN1O_ASSTO);
    }

    /**Original name: WGRL-DT-SIN-1O-ASSTO<br>*/
    public int getWgrlDtSin1oAssto() {
        return readPackedAsInt(Pos.WGRL_DT_SIN1O_ASSTO, Len.Int.WGRL_DT_SIN1O_ASSTO);
    }

    public byte[] getWgrlDtSin1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRL_DT_SIN1O_ASSTO, Pos.WGRL_DT_SIN1O_ASSTO);
        return buffer;
    }

    public void initWgrlDtSin1oAsstoSpaces() {
        fill(Pos.WGRL_DT_SIN1O_ASSTO, Len.WGRL_DT_SIN1O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrlDtSin1oAsstoNull(String wgrlDtSin1oAsstoNull) {
        writeString(Pos.WGRL_DT_SIN1O_ASSTO_NULL, wgrlDtSin1oAsstoNull, Len.WGRL_DT_SIN1O_ASSTO_NULL);
    }

    /**Original name: WGRL-DT-SIN-1O-ASSTO-NULL<br>*/
    public String getWgrlDtSin1oAsstoNull() {
        return readString(Pos.WGRL_DT_SIN1O_ASSTO_NULL, Len.WGRL_DT_SIN1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRL_DT_SIN1O_ASSTO = 1;
        public static final int WGRL_DT_SIN1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRL_DT_SIN1O_ASSTO = 5;
        public static final int WGRL_DT_SIN1O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRL_DT_SIN1O_ASSTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
