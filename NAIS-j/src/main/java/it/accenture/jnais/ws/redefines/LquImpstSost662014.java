package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-SOST-662014<br>
 * Variable: LQU-IMPST-SOST-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstSost662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstSost662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_SOST662014;
    }

    public void setLquImpstSost662014(AfDecimal lquImpstSost662014) {
        writeDecimalAsPacked(Pos.LQU_IMPST_SOST662014, lquImpstSost662014.copy());
    }

    public void setLquImpstSost662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_SOST662014, Pos.LQU_IMPST_SOST662014);
    }

    /**Original name: LQU-IMPST-SOST-662014<br>*/
    public AfDecimal getLquImpstSost662014() {
        return readPackedAsDecimal(Pos.LQU_IMPST_SOST662014, Len.Int.LQU_IMPST_SOST662014, Len.Fract.LQU_IMPST_SOST662014);
    }

    public byte[] getLquImpstSost662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_SOST662014, Pos.LQU_IMPST_SOST662014);
        return buffer;
    }

    public void setLquImpstSost662014Null(String lquImpstSost662014Null) {
        writeString(Pos.LQU_IMPST_SOST662014_NULL, lquImpstSost662014Null, Len.LQU_IMPST_SOST662014_NULL);
    }

    /**Original name: LQU-IMPST-SOST-662014-NULL<br>*/
    public String getLquImpstSost662014Null() {
        return readString(Pos.LQU_IMPST_SOST662014_NULL, Len.LQU_IMPST_SOST662014_NULL);
    }

    public String getLquImpstSost662014NullFormatted() {
        return Functions.padBlanks(getLquImpstSost662014Null(), Len.LQU_IMPST_SOST662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_SOST662014 = 1;
        public static final int LQU_IMPST_SOST662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_SOST662014 = 8;
        public static final int LQU_IMPST_SOST662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_SOST662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_SOST662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
