package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ESTR-ASS-MIN70A<br>
 * Variable: PCO-DT-ESTR-ASS-MIN70A from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtEstrAssMin70a extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtEstrAssMin70a() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ESTR_ASS_MIN70A;
    }

    public void setPcoDtEstrAssMin70a(int pcoDtEstrAssMin70a) {
        writeIntAsPacked(Pos.PCO_DT_ESTR_ASS_MIN70A, pcoDtEstrAssMin70a, Len.Int.PCO_DT_ESTR_ASS_MIN70A);
    }

    public void setPcoDtEstrAssMin70aFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ESTR_ASS_MIN70A, Pos.PCO_DT_ESTR_ASS_MIN70A);
    }

    /**Original name: PCO-DT-ESTR-ASS-MIN70A<br>*/
    public int getPcoDtEstrAssMin70a() {
        return readPackedAsInt(Pos.PCO_DT_ESTR_ASS_MIN70A, Len.Int.PCO_DT_ESTR_ASS_MIN70A);
    }

    public byte[] getPcoDtEstrAssMin70aAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ESTR_ASS_MIN70A, Pos.PCO_DT_ESTR_ASS_MIN70A);
        return buffer;
    }

    public void initPcoDtEstrAssMin70aHighValues() {
        fill(Pos.PCO_DT_ESTR_ASS_MIN70A, Len.PCO_DT_ESTR_ASS_MIN70A, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtEstrAssMin70aNull(String pcoDtEstrAssMin70aNull) {
        writeString(Pos.PCO_DT_ESTR_ASS_MIN70A_NULL, pcoDtEstrAssMin70aNull, Len.PCO_DT_ESTR_ASS_MIN70A_NULL);
    }

    /**Original name: PCO-DT-ESTR-ASS-MIN70A-NULL<br>*/
    public String getPcoDtEstrAssMin70aNull() {
        return readString(Pos.PCO_DT_ESTR_ASS_MIN70A_NULL, Len.PCO_DT_ESTR_ASS_MIN70A_NULL);
    }

    public String getPcoDtEstrAssMin70aNullFormatted() {
        return Functions.padBlanks(getPcoDtEstrAssMin70aNull(), Len.PCO_DT_ESTR_ASS_MIN70A_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ESTR_ASS_MIN70A = 1;
        public static final int PCO_DT_ESTR_ASS_MIN70A_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ESTR_ASS_MIN70A = 5;
        public static final int PCO_DT_ESTR_ASS_MIN70A_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ESTR_ASS_MIN70A = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
