package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-IMP-ASS-SOCIALE<br>
 * Variable: PCO-IMP-ASS-SOCIALE from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoImpAssSociale extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoImpAssSociale() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_IMP_ASS_SOCIALE;
    }

    public void setPcoImpAssSociale(AfDecimal pcoImpAssSociale) {
        writeDecimalAsPacked(Pos.PCO_IMP_ASS_SOCIALE, pcoImpAssSociale.copy());
    }

    public void setPcoImpAssSocialeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_IMP_ASS_SOCIALE, Pos.PCO_IMP_ASS_SOCIALE);
    }

    /**Original name: PCO-IMP-ASS-SOCIALE<br>*/
    public AfDecimal getPcoImpAssSociale() {
        return readPackedAsDecimal(Pos.PCO_IMP_ASS_SOCIALE, Len.Int.PCO_IMP_ASS_SOCIALE, Len.Fract.PCO_IMP_ASS_SOCIALE);
    }

    public byte[] getPcoImpAssSocialeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_IMP_ASS_SOCIALE, Pos.PCO_IMP_ASS_SOCIALE);
        return buffer;
    }

    public void initPcoImpAssSocialeHighValues() {
        fill(Pos.PCO_IMP_ASS_SOCIALE, Len.PCO_IMP_ASS_SOCIALE, Types.HIGH_CHAR_VAL);
    }

    public void setPcoImpAssSocialeNull(String pcoImpAssSocialeNull) {
        writeString(Pos.PCO_IMP_ASS_SOCIALE_NULL, pcoImpAssSocialeNull, Len.PCO_IMP_ASS_SOCIALE_NULL);
    }

    /**Original name: PCO-IMP-ASS-SOCIALE-NULL<br>*/
    public String getPcoImpAssSocialeNull() {
        return readString(Pos.PCO_IMP_ASS_SOCIALE_NULL, Len.PCO_IMP_ASS_SOCIALE_NULL);
    }

    public String getPcoImpAssSocialeNullFormatted() {
        return Functions.padBlanks(getPcoImpAssSocialeNull(), Len.PCO_IMP_ASS_SOCIALE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_IMP_ASS_SOCIALE = 1;
        public static final int PCO_IMP_ASS_SOCIALE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_IMP_ASS_SOCIALE = 8;
        public static final int PCO_IMP_ASS_SOCIALE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_IMP_ASS_SOCIALE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_IMP_ASS_SOCIALE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
