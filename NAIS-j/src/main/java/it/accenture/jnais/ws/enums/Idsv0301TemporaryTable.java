package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0301-TEMPORARY-TABLE<br>
 * Variable: IDSV0301-TEMPORARY-TABLE from copybook IDSV0301<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0301TemporaryTable {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NO_TEMP_TABLE = '0';
    public static final char STATIC_TEMP_TABLE = '1';
    public static final char SESSION_TEMP_TABLE = '2';

    //==== METHODS ====
    public void setTemporaryTable(char temporaryTable) {
        this.value = temporaryTable;
    }

    public char getTemporaryTable() {
        return this.value;
    }

    public void setStaticTempTable() {
        value = STATIC_TEMP_TABLE;
    }

    public boolean isSessionTempTable() {
        return value == SESSION_TEMP_TABLE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TEMPORARY_TABLE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
