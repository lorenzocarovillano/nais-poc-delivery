package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndCompStrDato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS5470<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs5470Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-RICH-DIS-FND
    private IndCompStrDato indRichDisFnd = new IndCompStrDato();
    //Original name: RICH-DIS-FND-DB
    private DettTitContDb richDisFndDb = new DettTitContDb();
    //Original name: LDBV5471-ID-TRCH-DI-GAR
    private int ldbv5471IdTrchDiGar = DefaultValues.INT_VAL;

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setLdbv5471Formatted(String data) {
        byte[] buffer = new byte[Len.LDBV5471];
        MarshalByte.writeString(buffer, 1, data, Len.LDBV5471);
        setLdbv5471Bytes(buffer, 1);
    }

    public String getLdbv5471Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5471Bytes());
    }

    /**Original name: LDBV5471<br>*/
    public byte[] getLdbv5471Bytes() {
        byte[] buffer = new byte[Len.LDBV5471];
        return getLdbv5471Bytes(buffer, 1);
    }

    public void setLdbv5471Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv5471IdTrchDiGar = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBV5471_ID_TRCH_DI_GAR, 0);
    }

    public byte[] getLdbv5471Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv5471IdTrchDiGar, Len.Int.LDBV5471_ID_TRCH_DI_GAR, 0);
        return buffer;
    }

    public void setLdbv5471IdTrchDiGar(int ldbv5471IdTrchDiGar) {
        this.ldbv5471IdTrchDiGar = ldbv5471IdTrchDiGar;
    }

    public int getLdbv5471IdTrchDiGar() {
        return this.ldbv5471IdTrchDiGar;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndCompStrDato getIndRichDisFnd() {
        return indRichDisFnd;
    }

    public DettTitContDb getRichDisFndDb() {
        return richDisFndDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int LDBV5471_ID_TRCH_DI_GAR = 5;
        public static final int LDBV5471 = LDBV5471_ID_TRCH_DI_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV5471_ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
