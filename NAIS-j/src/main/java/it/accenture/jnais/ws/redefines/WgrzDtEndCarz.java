package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-DT-END-CARZ<br>
 * Variable: WGRZ-DT-END-CARZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDtEndCarz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDtEndCarz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DT_END_CARZ;
    }

    public void setWgrzDtEndCarz(int wgrzDtEndCarz) {
        writeIntAsPacked(Pos.WGRZ_DT_END_CARZ, wgrzDtEndCarz, Len.Int.WGRZ_DT_END_CARZ);
    }

    public void setWgrzDtEndCarzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DT_END_CARZ, Pos.WGRZ_DT_END_CARZ);
    }

    /**Original name: WGRZ-DT-END-CARZ<br>*/
    public int getWgrzDtEndCarz() {
        return readPackedAsInt(Pos.WGRZ_DT_END_CARZ, Len.Int.WGRZ_DT_END_CARZ);
    }

    public byte[] getWgrzDtEndCarzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DT_END_CARZ, Pos.WGRZ_DT_END_CARZ);
        return buffer;
    }

    public void initWgrzDtEndCarzSpaces() {
        fill(Pos.WGRZ_DT_END_CARZ, Len.WGRZ_DT_END_CARZ, Types.SPACE_CHAR);
    }

    public void setWgrzDtEndCarzNull(String wgrzDtEndCarzNull) {
        writeString(Pos.WGRZ_DT_END_CARZ_NULL, wgrzDtEndCarzNull, Len.WGRZ_DT_END_CARZ_NULL);
    }

    /**Original name: WGRZ-DT-END-CARZ-NULL<br>*/
    public String getWgrzDtEndCarzNull() {
        return readString(Pos.WGRZ_DT_END_CARZ_NULL, Len.WGRZ_DT_END_CARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_END_CARZ = 1;
        public static final int WGRZ_DT_END_CARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_END_CARZ = 5;
        public static final int WGRZ_DT_END_CARZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DT_END_CARZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
