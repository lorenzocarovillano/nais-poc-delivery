package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-IPT<br>
 * Variable: WBEL-IMPST-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_IPT;
    }

    public void setWbelImpstIpt(AfDecimal wbelImpstIpt) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_IPT, wbelImpstIpt.copy());
    }

    public void setWbelImpstIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_IPT, Pos.WBEL_IMPST_IPT);
    }

    /**Original name: WBEL-IMPST-IPT<br>*/
    public AfDecimal getWbelImpstIpt() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_IPT, Len.Int.WBEL_IMPST_IPT, Len.Fract.WBEL_IMPST_IPT);
    }

    public byte[] getWbelImpstIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_IPT, Pos.WBEL_IMPST_IPT);
        return buffer;
    }

    public void initWbelImpstIptSpaces() {
        fill(Pos.WBEL_IMPST_IPT, Len.WBEL_IMPST_IPT, Types.SPACE_CHAR);
    }

    public void setWbelImpstIptNull(String wbelImpstIptNull) {
        writeString(Pos.WBEL_IMPST_IPT_NULL, wbelImpstIptNull, Len.WBEL_IMPST_IPT_NULL);
    }

    /**Original name: WBEL-IMPST-IPT-NULL<br>*/
    public String getWbelImpstIptNull() {
        return readString(Pos.WBEL_IMPST_IPT_NULL, Len.WBEL_IMPST_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_IPT = 1;
        public static final int WBEL_IMPST_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_IPT = 8;
        public static final int WBEL_IMPST_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
