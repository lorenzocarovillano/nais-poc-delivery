package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IAS-ONER-PRVNT-FIN<br>
 * Variable: WTLI-IAS-ONER-PRVNT-FIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliIasOnerPrvntFin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliIasOnerPrvntFin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IAS_ONER_PRVNT_FIN;
    }

    public void setWtliIasOnerPrvntFin(AfDecimal wtliIasOnerPrvntFin) {
        writeDecimalAsPacked(Pos.WTLI_IAS_ONER_PRVNT_FIN, wtliIasOnerPrvntFin.copy());
    }

    public void setWtliIasOnerPrvntFinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IAS_ONER_PRVNT_FIN, Pos.WTLI_IAS_ONER_PRVNT_FIN);
    }

    /**Original name: WTLI-IAS-ONER-PRVNT-FIN<br>*/
    public AfDecimal getWtliIasOnerPrvntFin() {
        return readPackedAsDecimal(Pos.WTLI_IAS_ONER_PRVNT_FIN, Len.Int.WTLI_IAS_ONER_PRVNT_FIN, Len.Fract.WTLI_IAS_ONER_PRVNT_FIN);
    }

    public byte[] getWtliIasOnerPrvntFinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IAS_ONER_PRVNT_FIN, Pos.WTLI_IAS_ONER_PRVNT_FIN);
        return buffer;
    }

    public void initWtliIasOnerPrvntFinSpaces() {
        fill(Pos.WTLI_IAS_ONER_PRVNT_FIN, Len.WTLI_IAS_ONER_PRVNT_FIN, Types.SPACE_CHAR);
    }

    public void setWtliIasOnerPrvntFinNull(String wtliIasOnerPrvntFinNull) {
        writeString(Pos.WTLI_IAS_ONER_PRVNT_FIN_NULL, wtliIasOnerPrvntFinNull, Len.WTLI_IAS_ONER_PRVNT_FIN_NULL);
    }

    /**Original name: WTLI-IAS-ONER-PRVNT-FIN-NULL<br>*/
    public String getWtliIasOnerPrvntFinNull() {
        return readString(Pos.WTLI_IAS_ONER_PRVNT_FIN_NULL, Len.WTLI_IAS_ONER_PRVNT_FIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_ONER_PRVNT_FIN = 1;
        public static final int WTLI_IAS_ONER_PRVNT_FIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_ONER_PRVNT_FIN = 8;
        public static final int WTLI_IAS_ONER_PRVNT_FIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_ONER_PRVNT_FIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_ONER_PRVNT_FIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
