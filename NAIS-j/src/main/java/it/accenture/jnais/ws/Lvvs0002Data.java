package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0002;
import it.accenture.jnais.ws.enums.WsTpFrmAssva;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0002<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0002Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0002";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "LDBS5140";
    //Original name: WK-PGM-CALL
    private String wkPgmCall = DefaultValues.stringVal(Len.WK_PGM_CALL);
    //Original name: LDBV5141
    private Ldbv5141 ldbv5141 = new Ldbv5141();
    /**Original name: WS-TP-FRM-ASSVA<br>
	 * <pre>*****************************************************************
	 *     TP_FRM_ASSVA (Forma Assicurativa)
	 * *****************************************************************</pre>*/
    private WsTpFrmAssva wsTpFrmAssva = new WsTpFrmAssva();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0002 wsMovimento = new WsMovimentoLvvs0002();
    //Original name: DPOL-ELE-POLI-MAX
    private short dpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: WS-TRCH-POS
    private WsTrchPos wsTrchPos = new WsTrchPos();
    //Original name: WS-TRCH-NEG
    private WsTrchNeg wsTrchNeg = new WsTrchNeg();
    //Original name: WS-CUM-PRE-ATT
    private AfDecimal wsCumPreAtt = new AfDecimal("0", 15, 3);
    //Original name: WK-ID-MOVI-COMUN
    private int wkIdMoviComun = DefaultValues.INT_VAL;
    //Original name: WK-DT-INI-EFF-TEMP
    private int wkDtIniEffTemp = DefaultValues.INT_VAL;
    //Original name: WK-DT-CPTZ-TEMP
    private long wkDtCptzTemp = DefaultValues.LONG_VAL;
    //Original name: WK-DATA-EFF-PREC
    private int wkDataEffPrec = DefaultValues.INT_VAL;
    //Original name: WK-DATA-CPTZ-PREC
    private long wkDataCptzPrec = DefaultValues.LONG_VAL;
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: FLAG-CUR-MOV
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkPgmCall(String wkPgmCall) {
        this.wkPgmCall = Functions.subString(wkPgmCall, Len.WK_PGM_CALL);
    }

    public String getWkPgmCall() {
        return this.wkPgmCall;
    }

    public void setDpolAreaPolizzaFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POLIZZA];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLIZZA);
        setDpolAreaPolizzaBytes(buffer, 1);
    }

    public void setDpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPoliBytes(buffer, position);
    }

    public void setDpolElePoliMax(short dpolElePoliMax) {
        this.dpolElePoliMax = dpolElePoliMax;
    }

    public short getDpolElePoliMax() {
        return this.dpolElePoliMax;
    }

    public void setDpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setWsCumPreAtt(AfDecimal wsCumPreAtt) {
        this.wsCumPreAtt.assign(wsCumPreAtt);
    }

    public AfDecimal getWsCumPreAtt() {
        return this.wsCumPreAtt.copy();
    }

    public void setWkIdMoviComun(int wkIdMoviComun) {
        this.wkIdMoviComun = wkIdMoviComun;
    }

    public int getWkIdMoviComun() {
        return this.wkIdMoviComun;
    }

    public void setWkDtIniEffTemp(int wkDtIniEffTemp) {
        this.wkDtIniEffTemp = wkDtIniEffTemp;
    }

    public int getWkDtIniEffTemp() {
        return this.wkDtIniEffTemp;
    }

    public void setWkDtCptzTemp(long wkDtCptzTemp) {
        this.wkDtCptzTemp = wkDtCptzTemp;
    }

    public long getWkDtCptzTemp() {
        return this.wkDtCptzTemp;
    }

    public void setWkDataEffPrec(int wkDataEffPrec) {
        this.wkDataEffPrec = wkDataEffPrec;
    }

    public int getWkDataEffPrec() {
        return this.wkDataEffPrec;
    }

    public void setWkDataCptzPrec(long wkDataCptzPrec) {
        this.wkDataCptzPrec = wkDataCptzPrec;
    }

    public long getWkDataCptzPrec() {
        return this.wkDataCptzPrec;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public Ldbv5141 getLdbv5141() {
        return ldbv5141;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WsMovimentoLvvs0002 getWsMovimento() {
        return wsMovimento;
    }

    public WsTpFrmAssva getWsTpFrmAssva() {
        return wsTpFrmAssva;
    }

    public WsTrchNeg getWsTrchNeg() {
        return wsTrchNeg;
    }

    public WsTrchPos getWsTrchPos() {
        return wsTrchPos;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_PGM_CALL = 8;
        public static final int DPOL_ELE_POLI_MAX = 2;
        public static final int DPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POLIZZA = DPOL_ELE_POLI_MAX + DPOL_TAB_POLI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
