package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-RIT-ACC-DFZ<br>
 * Variable: DFL-IMPB-RIT-ACC-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbRitAccDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbRitAccDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_RIT_ACC_DFZ;
    }

    public void setDflImpbRitAccDfz(AfDecimal dflImpbRitAccDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPB_RIT_ACC_DFZ, dflImpbRitAccDfz.copy());
    }

    public void setDflImpbRitAccDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_RIT_ACC_DFZ, Pos.DFL_IMPB_RIT_ACC_DFZ);
    }

    /**Original name: DFL-IMPB-RIT-ACC-DFZ<br>*/
    public AfDecimal getDflImpbRitAccDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPB_RIT_ACC_DFZ, Len.Int.DFL_IMPB_RIT_ACC_DFZ, Len.Fract.DFL_IMPB_RIT_ACC_DFZ);
    }

    public byte[] getDflImpbRitAccDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_RIT_ACC_DFZ, Pos.DFL_IMPB_RIT_ACC_DFZ);
        return buffer;
    }

    public void setDflImpbRitAccDfzNull(String dflImpbRitAccDfzNull) {
        writeString(Pos.DFL_IMPB_RIT_ACC_DFZ_NULL, dflImpbRitAccDfzNull, Len.DFL_IMPB_RIT_ACC_DFZ_NULL);
    }

    /**Original name: DFL-IMPB-RIT-ACC-DFZ-NULL<br>*/
    public String getDflImpbRitAccDfzNull() {
        return readString(Pos.DFL_IMPB_RIT_ACC_DFZ_NULL, Len.DFL_IMPB_RIT_ACC_DFZ_NULL);
    }

    public String getDflImpbRitAccDfzNullFormatted() {
        return Functions.padBlanks(getDflImpbRitAccDfzNull(), Len.DFL_IMPB_RIT_ACC_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_RIT_ACC_DFZ = 1;
        public static final int DFL_IMPB_RIT_ACC_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_RIT_ACC_DFZ = 8;
        public static final int DFL_IMPB_RIT_ACC_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_RIT_ACC_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_RIT_ACC_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
