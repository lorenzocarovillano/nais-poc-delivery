package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-INTR-PREST<br>
 * Variable: WTIT-TOT-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_INTR_PREST;
    }

    public void setWtitTotIntrPrest(AfDecimal wtitTotIntrPrest) {
        writeDecimalAsPacked(Pos.WTIT_TOT_INTR_PREST, wtitTotIntrPrest.copy());
    }

    public void setWtitTotIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_INTR_PREST, Pos.WTIT_TOT_INTR_PREST);
    }

    /**Original name: WTIT-TOT-INTR-PREST<br>*/
    public AfDecimal getWtitTotIntrPrest() {
        return readPackedAsDecimal(Pos.WTIT_TOT_INTR_PREST, Len.Int.WTIT_TOT_INTR_PREST, Len.Fract.WTIT_TOT_INTR_PREST);
    }

    public byte[] getWtitTotIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_INTR_PREST, Pos.WTIT_TOT_INTR_PREST);
        return buffer;
    }

    public void initWtitTotIntrPrestSpaces() {
        fill(Pos.WTIT_TOT_INTR_PREST, Len.WTIT_TOT_INTR_PREST, Types.SPACE_CHAR);
    }

    public void setWtitTotIntrPrestNull(String wtitTotIntrPrestNull) {
        writeString(Pos.WTIT_TOT_INTR_PREST_NULL, wtitTotIntrPrestNull, Len.WTIT_TOT_INTR_PREST_NULL);
    }

    /**Original name: WTIT-TOT-INTR-PREST-NULL<br>*/
    public String getWtitTotIntrPrestNull() {
        return readString(Pos.WTIT_TOT_INTR_PREST_NULL, Len.WTIT_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_PREST = 1;
        public static final int WTIT_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_PREST = 8;
        public static final int WTIT_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
