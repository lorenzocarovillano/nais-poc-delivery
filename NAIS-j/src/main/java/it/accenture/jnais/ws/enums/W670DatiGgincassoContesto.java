package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: W670-DATI-GGINCASSO-CONTESTO<br>
 * Variable: W670-DATI-GGINCASSO-CONTESTO from copybook LOAC0670<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class W670DatiGgincassoContesto {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.DATI_GGINCASSO_CONTESTO);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setDatiGgincassoContesto(String datiGgincassoContesto) {
        this.value = Functions.subString(datiGgincassoContesto, Len.DATI_GGINCASSO_CONTESTO);
    }

    public String getDatiGgincassoContesto() {
        return this.value;
    }

    public void setW670GgcontestoContNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATI_GGINCASSO_CONTESTO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
