package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-ETA-SCAD-MASC-DFLT<br>
 * Variable: DAD-ETA-SCAD-MASC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadEtaScadMascDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadEtaScadMascDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_ETA_SCAD_MASC_DFLT;
    }

    public void setDadEtaScadMascDflt(int dadEtaScadMascDflt) {
        writeIntAsPacked(Pos.DAD_ETA_SCAD_MASC_DFLT, dadEtaScadMascDflt, Len.Int.DAD_ETA_SCAD_MASC_DFLT);
    }

    public void setDadEtaScadMascDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_ETA_SCAD_MASC_DFLT, Pos.DAD_ETA_SCAD_MASC_DFLT);
    }

    /**Original name: DAD-ETA-SCAD-MASC-DFLT<br>*/
    public int getDadEtaScadMascDflt() {
        return readPackedAsInt(Pos.DAD_ETA_SCAD_MASC_DFLT, Len.Int.DAD_ETA_SCAD_MASC_DFLT);
    }

    public byte[] getDadEtaScadMascDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_ETA_SCAD_MASC_DFLT, Pos.DAD_ETA_SCAD_MASC_DFLT);
        return buffer;
    }

    public void setDadEtaScadMascDfltNull(String dadEtaScadMascDfltNull) {
        writeString(Pos.DAD_ETA_SCAD_MASC_DFLT_NULL, dadEtaScadMascDfltNull, Len.DAD_ETA_SCAD_MASC_DFLT_NULL);
    }

    /**Original name: DAD-ETA-SCAD-MASC-DFLT-NULL<br>*/
    public String getDadEtaScadMascDfltNull() {
        return readString(Pos.DAD_ETA_SCAD_MASC_DFLT_NULL, Len.DAD_ETA_SCAD_MASC_DFLT_NULL);
    }

    public String getDadEtaScadMascDfltNullFormatted() {
        return Functions.padBlanks(getDadEtaScadMascDfltNull(), Len.DAD_ETA_SCAD_MASC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_ETA_SCAD_MASC_DFLT = 1;
        public static final int DAD_ETA_SCAD_MASC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_ETA_SCAD_MASC_DFLT = 3;
        public static final int DAD_ETA_SCAD_MASC_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_ETA_SCAD_MASC_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
