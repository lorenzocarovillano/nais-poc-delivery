package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0140-CODICE-TP-PREMIO-P<br>
 * Variable: ISPC0140-CODICE-TP-PREMIO-P from copybook ISPC0140<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0140CodiceTpPremioP {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.CODICE_TP_PREMIO_P);
    public static final String ANNUO_P = "ANNUO";
    public static final String RATA_P = "RATA";
    public static final String FIRMA_P = "FIRMA";

    //==== METHODS ====
    public void setCodiceTpPremioP(String codiceTpPremioP) {
        this.value = Functions.subString(codiceTpPremioP, Len.CODICE_TP_PREMIO_P);
    }

    public String getCodiceTpPremioP() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_TP_PREMIO_P = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
