package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-IMP-RAT-MANFEE<br>
 * Variable: WPMO-IMP-RAT-MANFEE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoImpRatManfee extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoImpRatManfee() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_IMP_RAT_MANFEE;
    }

    public void setWpmoImpRatManfee(AfDecimal wpmoImpRatManfee) {
        writeDecimalAsPacked(Pos.WPMO_IMP_RAT_MANFEE, wpmoImpRatManfee.copy());
    }

    public void setWpmoImpRatManfeeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_IMP_RAT_MANFEE, Pos.WPMO_IMP_RAT_MANFEE);
    }

    /**Original name: WPMO-IMP-RAT-MANFEE<br>*/
    public AfDecimal getWpmoImpRatManfee() {
        return readPackedAsDecimal(Pos.WPMO_IMP_RAT_MANFEE, Len.Int.WPMO_IMP_RAT_MANFEE, Len.Fract.WPMO_IMP_RAT_MANFEE);
    }

    public byte[] getWpmoImpRatManfeeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_IMP_RAT_MANFEE, Pos.WPMO_IMP_RAT_MANFEE);
        return buffer;
    }

    public void initWpmoImpRatManfeeSpaces() {
        fill(Pos.WPMO_IMP_RAT_MANFEE, Len.WPMO_IMP_RAT_MANFEE, Types.SPACE_CHAR);
    }

    public void setWpmoImpRatManfeeNull(String wpmoImpRatManfeeNull) {
        writeString(Pos.WPMO_IMP_RAT_MANFEE_NULL, wpmoImpRatManfeeNull, Len.WPMO_IMP_RAT_MANFEE_NULL);
    }

    /**Original name: WPMO-IMP-RAT-MANFEE-NULL<br>*/
    public String getWpmoImpRatManfeeNull() {
        return readString(Pos.WPMO_IMP_RAT_MANFEE_NULL, Len.WPMO_IMP_RAT_MANFEE_NULL);
    }

    public String getWpmoImpRatManfeeNullFormatted() {
        return Functions.padBlanks(getWpmoImpRatManfeeNull(), Len.WPMO_IMP_RAT_MANFEE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_RAT_MANFEE = 1;
        public static final int WPMO_IMP_RAT_MANFEE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_RAT_MANFEE = 8;
        public static final int WPMO_IMP_RAT_MANFEE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_RAT_MANFEE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_RAT_MANFEE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
