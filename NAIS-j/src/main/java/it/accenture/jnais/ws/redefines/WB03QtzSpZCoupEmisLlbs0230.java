package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-QTZ-SP-Z-COUP-EMIS<br>
 * Variable: W-B03-QTZ-SP-Z-COUP-EMIS from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03QtzSpZCoupEmisLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03QtzSpZCoupEmisLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_QTZ_SP_Z_COUP_EMIS;
    }

    public void setwB03QtzSpZCoupEmis(AfDecimal wB03QtzSpZCoupEmis) {
        writeDecimalAsPacked(Pos.W_B03_QTZ_SP_Z_COUP_EMIS, wB03QtzSpZCoupEmis.copy());
    }

    /**Original name: W-B03-QTZ-SP-Z-COUP-EMIS<br>*/
    public AfDecimal getwB03QtzSpZCoupEmis() {
        return readPackedAsDecimal(Pos.W_B03_QTZ_SP_Z_COUP_EMIS, Len.Int.W_B03_QTZ_SP_Z_COUP_EMIS, Len.Fract.W_B03_QTZ_SP_Z_COUP_EMIS);
    }

    public byte[] getwB03QtzSpZCoupEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_QTZ_SP_Z_COUP_EMIS, Pos.W_B03_QTZ_SP_Z_COUP_EMIS);
        return buffer;
    }

    public void setwB03QtzSpZCoupEmisNull(String wB03QtzSpZCoupEmisNull) {
        writeString(Pos.W_B03_QTZ_SP_Z_COUP_EMIS_NULL, wB03QtzSpZCoupEmisNull, Len.W_B03_QTZ_SP_Z_COUP_EMIS_NULL);
    }

    /**Original name: W-B03-QTZ-SP-Z-COUP-EMIS-NULL<br>*/
    public String getwB03QtzSpZCoupEmisNull() {
        return readString(Pos.W_B03_QTZ_SP_Z_COUP_EMIS_NULL, Len.W_B03_QTZ_SP_Z_COUP_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_SP_Z_COUP_EMIS = 1;
        public static final int W_B03_QTZ_SP_Z_COUP_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_SP_Z_COUP_EMIS = 7;
        public static final int W_B03_QTZ_SP_Z_COUP_EMIS_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_SP_Z_COUP_EMIS = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_SP_Z_COUP_EMIS = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
