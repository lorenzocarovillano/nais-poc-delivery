package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-RENDTO-SPPR<br>
 * Variable: WB03-TS-RENDTO-SPPR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsRendtoSppr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsRendtoSppr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_RENDTO_SPPR;
    }

    public void setWb03TsRendtoSpprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_RENDTO_SPPR, Pos.WB03_TS_RENDTO_SPPR);
    }

    /**Original name: WB03-TS-RENDTO-SPPR<br>*/
    public AfDecimal getWb03TsRendtoSppr() {
        return readPackedAsDecimal(Pos.WB03_TS_RENDTO_SPPR, Len.Int.WB03_TS_RENDTO_SPPR, Len.Fract.WB03_TS_RENDTO_SPPR);
    }

    public byte[] getWb03TsRendtoSpprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_RENDTO_SPPR, Pos.WB03_TS_RENDTO_SPPR);
        return buffer;
    }

    public void setWb03TsRendtoSpprNull(String wb03TsRendtoSpprNull) {
        writeString(Pos.WB03_TS_RENDTO_SPPR_NULL, wb03TsRendtoSpprNull, Len.WB03_TS_RENDTO_SPPR_NULL);
    }

    /**Original name: WB03-TS-RENDTO-SPPR-NULL<br>*/
    public String getWb03TsRendtoSpprNull() {
        return readString(Pos.WB03_TS_RENDTO_SPPR_NULL, Len.WB03_TS_RENDTO_SPPR_NULL);
    }

    public String getWb03TsRendtoSpprNullFormatted() {
        return Functions.padBlanks(getWb03TsRendtoSpprNull(), Len.WB03_TS_RENDTO_SPPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_RENDTO_SPPR = 1;
        public static final int WB03_TS_RENDTO_SPPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_RENDTO_SPPR = 8;
        public static final int WB03_TS_RENDTO_SPPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_RENDTO_SPPR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_RENDTO_SPPR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
