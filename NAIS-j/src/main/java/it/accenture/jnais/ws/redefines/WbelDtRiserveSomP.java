package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEL-DT-RISERVE-SOM-P<br>
 * Variable: WBEL-DT-RISERVE-SOM-P from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelDtRiserveSomP extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelDtRiserveSomP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_DT_RISERVE_SOM_P;
    }

    public void setWbelDtRiserveSomP(int wbelDtRiserveSomP) {
        writeIntAsPacked(Pos.WBEL_DT_RISERVE_SOM_P, wbelDtRiserveSomP, Len.Int.WBEL_DT_RISERVE_SOM_P);
    }

    public void setWbelDtRiserveSomPFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_DT_RISERVE_SOM_P, Pos.WBEL_DT_RISERVE_SOM_P);
    }

    /**Original name: WBEL-DT-RISERVE-SOM-P<br>*/
    public int getWbelDtRiserveSomP() {
        return readPackedAsInt(Pos.WBEL_DT_RISERVE_SOM_P, Len.Int.WBEL_DT_RISERVE_SOM_P);
    }

    public byte[] getWbelDtRiserveSomPAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_DT_RISERVE_SOM_P, Pos.WBEL_DT_RISERVE_SOM_P);
        return buffer;
    }

    public void initWbelDtRiserveSomPSpaces() {
        fill(Pos.WBEL_DT_RISERVE_SOM_P, Len.WBEL_DT_RISERVE_SOM_P, Types.SPACE_CHAR);
    }

    public void setWbelDtRiserveSomPNull(String wbelDtRiserveSomPNull) {
        writeString(Pos.WBEL_DT_RISERVE_SOM_P_NULL, wbelDtRiserveSomPNull, Len.WBEL_DT_RISERVE_SOM_P_NULL);
    }

    /**Original name: WBEL-DT-RISERVE-SOM-P-NULL<br>*/
    public String getWbelDtRiserveSomPNull() {
        return readString(Pos.WBEL_DT_RISERVE_SOM_P_NULL, Len.WBEL_DT_RISERVE_SOM_P_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_DT_RISERVE_SOM_P = 1;
        public static final int WBEL_DT_RISERVE_SOM_P_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_DT_RISERVE_SOM_P = 5;
        public static final int WBEL_DT_RISERVE_SOM_P_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_DT_RISERVE_SOM_P = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
