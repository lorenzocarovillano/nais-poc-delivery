package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-TFR<br>
 * Variable: TGA-IMP-TFR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_TFR;
    }

    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        writeDecimalAsPacked(Pos.TGA_IMP_TFR, tgaImpTfr.copy());
    }

    public void setTgaImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_TFR, Pos.TGA_IMP_TFR);
    }

    /**Original name: TGA-IMP-TFR<br>*/
    public AfDecimal getTgaImpTfr() {
        return readPackedAsDecimal(Pos.TGA_IMP_TFR, Len.Int.TGA_IMP_TFR, Len.Fract.TGA_IMP_TFR);
    }

    public byte[] getTgaImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_TFR, Pos.TGA_IMP_TFR);
        return buffer;
    }

    public void setTgaImpTfrNull(String tgaImpTfrNull) {
        writeString(Pos.TGA_IMP_TFR_NULL, tgaImpTfrNull, Len.TGA_IMP_TFR_NULL);
    }

    /**Original name: TGA-IMP-TFR-NULL<br>*/
    public String getTgaImpTfrNull() {
        return readString(Pos.TGA_IMP_TFR_NULL, Len.TGA_IMP_TFR_NULL);
    }

    public String getTgaImpTfrNullFormatted() {
        return Functions.padBlanks(getTgaImpTfrNull(), Len.TGA_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_TFR = 1;
        public static final int TGA_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_TFR = 8;
        public static final int TGA_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
