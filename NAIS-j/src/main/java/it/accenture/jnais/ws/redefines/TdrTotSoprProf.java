package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SOPR-PROF<br>
 * Variable: TDR-TOT-SOPR-PROF from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SOPR_PROF;
    }

    public void setTdrTotSoprProf(AfDecimal tdrTotSoprProf) {
        writeDecimalAsPacked(Pos.TDR_TOT_SOPR_PROF, tdrTotSoprProf.copy());
    }

    public void setTdrTotSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SOPR_PROF, Pos.TDR_TOT_SOPR_PROF);
    }

    /**Original name: TDR-TOT-SOPR-PROF<br>*/
    public AfDecimal getTdrTotSoprProf() {
        return readPackedAsDecimal(Pos.TDR_TOT_SOPR_PROF, Len.Int.TDR_TOT_SOPR_PROF, Len.Fract.TDR_TOT_SOPR_PROF);
    }

    public byte[] getTdrTotSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SOPR_PROF, Pos.TDR_TOT_SOPR_PROF);
        return buffer;
    }

    public void setTdrTotSoprProfNull(String tdrTotSoprProfNull) {
        writeString(Pos.TDR_TOT_SOPR_PROF_NULL, tdrTotSoprProfNull, Len.TDR_TOT_SOPR_PROF_NULL);
    }

    /**Original name: TDR-TOT-SOPR-PROF-NULL<br>*/
    public String getTdrTotSoprProfNull() {
        return readString(Pos.TDR_TOT_SOPR_PROF_NULL, Len.TDR_TOT_SOPR_PROF_NULL);
    }

    public String getTdrTotSoprProfNullFormatted() {
        return Functions.padBlanks(getTdrTotSoprProfNull(), Len.TDR_TOT_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_PROF = 1;
        public static final int TDR_TOT_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_PROF = 8;
        public static final int TDR_TOT_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
