package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-IMP-BNS-DA-SCO-TOT<br>
 * Variable: WPMO-IMP-BNS-DA-SCO-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoImpBnsDaScoTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoImpBnsDaScoTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_IMP_BNS_DA_SCO_TOT;
    }

    public void setWpmoImpBnsDaScoTot(AfDecimal wpmoImpBnsDaScoTot) {
        writeDecimalAsPacked(Pos.WPMO_IMP_BNS_DA_SCO_TOT, wpmoImpBnsDaScoTot.copy());
    }

    public void setWpmoImpBnsDaScoTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_IMP_BNS_DA_SCO_TOT, Pos.WPMO_IMP_BNS_DA_SCO_TOT);
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO-TOT<br>*/
    public AfDecimal getWpmoImpBnsDaScoTot() {
        return readPackedAsDecimal(Pos.WPMO_IMP_BNS_DA_SCO_TOT, Len.Int.WPMO_IMP_BNS_DA_SCO_TOT, Len.Fract.WPMO_IMP_BNS_DA_SCO_TOT);
    }

    public byte[] getWpmoImpBnsDaScoTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_IMP_BNS_DA_SCO_TOT, Pos.WPMO_IMP_BNS_DA_SCO_TOT);
        return buffer;
    }

    public void initWpmoImpBnsDaScoTotSpaces() {
        fill(Pos.WPMO_IMP_BNS_DA_SCO_TOT, Len.WPMO_IMP_BNS_DA_SCO_TOT, Types.SPACE_CHAR);
    }

    public void setWpmoImpBnsDaScoTotNull(String wpmoImpBnsDaScoTotNull) {
        writeString(Pos.WPMO_IMP_BNS_DA_SCO_TOT_NULL, wpmoImpBnsDaScoTotNull, Len.WPMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO-TOT-NULL<br>*/
    public String getWpmoImpBnsDaScoTotNull() {
        return readString(Pos.WPMO_IMP_BNS_DA_SCO_TOT_NULL, Len.WPMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    public String getWpmoImpBnsDaScoTotNullFormatted() {
        return Functions.padBlanks(getWpmoImpBnsDaScoTotNull(), Len.WPMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_BNS_DA_SCO_TOT = 1;
        public static final int WPMO_IMP_BNS_DA_SCO_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_BNS_DA_SCO_TOT = 8;
        public static final int WPMO_IMP_BNS_DA_SCO_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_BNS_DA_SCO_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_BNS_DA_SCO_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
