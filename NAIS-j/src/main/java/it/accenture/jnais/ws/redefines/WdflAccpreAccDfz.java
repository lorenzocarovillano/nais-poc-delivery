package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-ACC-DFZ<br>
 * Variable: WDFL-ACCPRE-ACC-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreAccDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreAccDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_ACC_DFZ;
    }

    public void setWdflAccpreAccDfz(AfDecimal wdflAccpreAccDfz) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_ACC_DFZ, wdflAccpreAccDfz.copy());
    }

    public void setWdflAccpreAccDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_ACC_DFZ, Pos.WDFL_ACCPRE_ACC_DFZ);
    }

    /**Original name: WDFL-ACCPRE-ACC-DFZ<br>*/
    public AfDecimal getWdflAccpreAccDfz() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_ACC_DFZ, Len.Int.WDFL_ACCPRE_ACC_DFZ, Len.Fract.WDFL_ACCPRE_ACC_DFZ);
    }

    public byte[] getWdflAccpreAccDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_ACC_DFZ, Pos.WDFL_ACCPRE_ACC_DFZ);
        return buffer;
    }

    public void setWdflAccpreAccDfzNull(String wdflAccpreAccDfzNull) {
        writeString(Pos.WDFL_ACCPRE_ACC_DFZ_NULL, wdflAccpreAccDfzNull, Len.WDFL_ACCPRE_ACC_DFZ_NULL);
    }

    /**Original name: WDFL-ACCPRE-ACC-DFZ-NULL<br>*/
    public String getWdflAccpreAccDfzNull() {
        return readString(Pos.WDFL_ACCPRE_ACC_DFZ_NULL, Len.WDFL_ACCPRE_ACC_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_ACC_DFZ = 1;
        public static final int WDFL_ACCPRE_ACC_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_ACC_DFZ = 8;
        public static final int WDFL_ACCPRE_ACC_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_ACC_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_ACC_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
