package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-TROVATO-DTC<br>
 * Variable: SW-TROVATO-DTC from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwTrovatoDtc {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setSwTrovatoDtc(char swTrovatoDtc) {
        this.value = swTrovatoDtc;
    }

    public char getSwTrovatoDtc() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
