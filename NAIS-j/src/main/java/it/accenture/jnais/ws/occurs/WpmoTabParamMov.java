package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvpmo1;
import it.accenture.jnais.copy.WpmoDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WPMO-TAB-PARAM-MOV<br>
 * Variables: WPMO-TAB-PARAM-MOV from copybook LCCVPMOB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpmoTabParamMov {

    //==== PROPERTIES ====
    //Original name: LCCVPMO1
    private Lccvpmo1 lccvpmo1 = new Lccvpmo1();

    //==== METHODS ====
    public void setTabParamMovBytes(byte[] buffer) {
        setWpmoTabParamMovBytes(buffer, 1);
    }

    public byte[] getWpmoTabParamMovBytes() {
        byte[] buffer = new byte[Len.WPMO_TAB_PARAM_MOV];
        return getWpmoTabParamMovBytes(buffer, 1);
    }

    public void setWpmoTabParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpmo1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpmo1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpmo1.Len.Int.ID_PTF, 0));
        position += Lccvpmo1.Len.ID_PTF;
        lccvpmo1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpmoTabParamMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpmo1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpmo1.getIdPtf(), Lccvpmo1.Len.Int.ID_PTF, 0);
        position += Lccvpmo1.Len.ID_PTF;
        lccvpmo1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWpmoTabParamMovSpaces() {
        lccvpmo1.initLccvpmo1Spaces();
    }

    public Lccvpmo1 getLccvpmo1() {
        return lccvpmo1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_TAB_PARAM_MOV = WpolStatus.Len.STATUS + Lccvpmo1.Len.ID_PTF + WpmoDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
