package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-PRVR-DFZ<br>
 * Variable: DFL-IMPST-PRVR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstPrvrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstPrvrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_PRVR_DFZ;
    }

    public void setDflImpstPrvrDfz(AfDecimal dflImpstPrvrDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPST_PRVR_DFZ, dflImpstPrvrDfz.copy());
    }

    public void setDflImpstPrvrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_PRVR_DFZ, Pos.DFL_IMPST_PRVR_DFZ);
    }

    /**Original name: DFL-IMPST-PRVR-DFZ<br>*/
    public AfDecimal getDflImpstPrvrDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPST_PRVR_DFZ, Len.Int.DFL_IMPST_PRVR_DFZ, Len.Fract.DFL_IMPST_PRVR_DFZ);
    }

    public byte[] getDflImpstPrvrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_PRVR_DFZ, Pos.DFL_IMPST_PRVR_DFZ);
        return buffer;
    }

    public void setDflImpstPrvrDfzNull(String dflImpstPrvrDfzNull) {
        writeString(Pos.DFL_IMPST_PRVR_DFZ_NULL, dflImpstPrvrDfzNull, Len.DFL_IMPST_PRVR_DFZ_NULL);
    }

    /**Original name: DFL-IMPST-PRVR-DFZ-NULL<br>*/
    public String getDflImpstPrvrDfzNull() {
        return readString(Pos.DFL_IMPST_PRVR_DFZ_NULL, Len.DFL_IMPST_PRVR_DFZ_NULL);
    }

    public String getDflImpstPrvrDfzNullFormatted() {
        return Functions.padBlanks(getDflImpstPrvrDfzNull(), Len.DFL_IMPST_PRVR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_PRVR_DFZ = 1;
        public static final int DFL_IMPST_PRVR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_PRVR_DFZ = 8;
        public static final int DFL_IMPST_PRVR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_PRVR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_PRVR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
