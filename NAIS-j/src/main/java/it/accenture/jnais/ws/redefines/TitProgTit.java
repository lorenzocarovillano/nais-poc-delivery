package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-PROG-TIT<br>
 * Variable: TIT-PROG-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitProgTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitProgTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_PROG_TIT;
    }

    public void setTitProgTit(int titProgTit) {
        writeIntAsPacked(Pos.TIT_PROG_TIT, titProgTit, Len.Int.TIT_PROG_TIT);
    }

    public void setTitProgTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_PROG_TIT, Pos.TIT_PROG_TIT);
    }

    /**Original name: TIT-PROG-TIT<br>*/
    public int getTitProgTit() {
        return readPackedAsInt(Pos.TIT_PROG_TIT, Len.Int.TIT_PROG_TIT);
    }

    public byte[] getTitProgTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_PROG_TIT, Pos.TIT_PROG_TIT);
        return buffer;
    }

    public void setTitProgTitNull(String titProgTitNull) {
        writeString(Pos.TIT_PROG_TIT_NULL, titProgTitNull, Len.TIT_PROG_TIT_NULL);
    }

    /**Original name: TIT-PROG-TIT-NULL<br>*/
    public String getTitProgTitNull() {
        return readString(Pos.TIT_PROG_TIT_NULL, Len.TIT_PROG_TIT_NULL);
    }

    public String getTitProgTitNullFormatted() {
        return Functions.padBlanks(getTitProgTitNull(), Len.TIT_PROG_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_PROG_TIT = 1;
        public static final int TIT_PROG_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_PROG_TIT = 3;
        public static final int TIT_PROG_TIT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_PROG_TIT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
