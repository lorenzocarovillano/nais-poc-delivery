package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MOV-ID-OGG<br>
 * Variable: MOV-ID-OGG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MovIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MovIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOV_ID_OGG;
    }

    public void setMovIdOgg(int movIdOgg) {
        writeIntAsPacked(Pos.MOV_ID_OGG, movIdOgg, Len.Int.MOV_ID_OGG);
    }

    public void setMovIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MOV_ID_OGG, Pos.MOV_ID_OGG);
    }

    /**Original name: MOV-ID-OGG<br>*/
    public int getMovIdOgg() {
        return readPackedAsInt(Pos.MOV_ID_OGG, Len.Int.MOV_ID_OGG);
    }

    public byte[] getMovIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MOV_ID_OGG, Pos.MOV_ID_OGG);
        return buffer;
    }

    public void setMovIdOggNull(String movIdOggNull) {
        writeString(Pos.MOV_ID_OGG_NULL, movIdOggNull, Len.MOV_ID_OGG_NULL);
    }

    /**Original name: MOV-ID-OGG-NULL<br>*/
    public String getMovIdOggNull() {
        return readString(Pos.MOV_ID_OGG_NULL, Len.MOV_ID_OGG_NULL);
    }

    public String getMovIdOggNullFormatted() {
        return Functions.padBlanks(getMovIdOggNull(), Len.MOV_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MOV_ID_OGG = 1;
        public static final int MOV_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_ID_OGG = 5;
        public static final int MOV_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
