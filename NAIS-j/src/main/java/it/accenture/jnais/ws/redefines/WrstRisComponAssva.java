package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-COMPON-ASSVA<br>
 * Variable: WRST-RIS-COMPON-ASSVA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisComponAssva extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisComponAssva() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_COMPON_ASSVA;
    }

    public void setWrstRisComponAssva(AfDecimal wrstRisComponAssva) {
        writeDecimalAsPacked(Pos.WRST_RIS_COMPON_ASSVA, wrstRisComponAssva.copy());
    }

    public void setWrstRisComponAssvaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_COMPON_ASSVA, Pos.WRST_RIS_COMPON_ASSVA);
    }

    /**Original name: WRST-RIS-COMPON-ASSVA<br>*/
    public AfDecimal getWrstRisComponAssva() {
        return readPackedAsDecimal(Pos.WRST_RIS_COMPON_ASSVA, Len.Int.WRST_RIS_COMPON_ASSVA, Len.Fract.WRST_RIS_COMPON_ASSVA);
    }

    public byte[] getWrstRisComponAssvaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_COMPON_ASSVA, Pos.WRST_RIS_COMPON_ASSVA);
        return buffer;
    }

    public void initWrstRisComponAssvaSpaces() {
        fill(Pos.WRST_RIS_COMPON_ASSVA, Len.WRST_RIS_COMPON_ASSVA, Types.SPACE_CHAR);
    }

    public void setWrstRisComponAssvaNull(String wrstRisComponAssvaNull) {
        writeString(Pos.WRST_RIS_COMPON_ASSVA_NULL, wrstRisComponAssvaNull, Len.WRST_RIS_COMPON_ASSVA_NULL);
    }

    /**Original name: WRST-RIS-COMPON-ASSVA-NULL<br>*/
    public String getWrstRisComponAssvaNull() {
        return readString(Pos.WRST_RIS_COMPON_ASSVA_NULL, Len.WRST_RIS_COMPON_ASSVA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_COMPON_ASSVA = 1;
        public static final int WRST_RIS_COMPON_ASSVA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_COMPON_ASSVA = 8;
        public static final int WRST_RIS_COMPON_ASSVA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_COMPON_ASSVA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_COMPON_ASSVA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
