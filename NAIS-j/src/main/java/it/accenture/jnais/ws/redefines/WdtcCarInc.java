package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-CAR-INC<br>
 * Variable: WDTC-CAR-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_CAR_INC;
    }

    public void setWdtcCarInc(AfDecimal wdtcCarInc) {
        writeDecimalAsPacked(Pos.WDTC_CAR_INC, wdtcCarInc.copy());
    }

    public void setWdtcCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_CAR_INC, Pos.WDTC_CAR_INC);
    }

    /**Original name: WDTC-CAR-INC<br>*/
    public AfDecimal getWdtcCarInc() {
        return readPackedAsDecimal(Pos.WDTC_CAR_INC, Len.Int.WDTC_CAR_INC, Len.Fract.WDTC_CAR_INC);
    }

    public byte[] getWdtcCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_CAR_INC, Pos.WDTC_CAR_INC);
        return buffer;
    }

    public void initWdtcCarIncSpaces() {
        fill(Pos.WDTC_CAR_INC, Len.WDTC_CAR_INC, Types.SPACE_CHAR);
    }

    public void setWdtcCarIncNull(String wdtcCarIncNull) {
        writeString(Pos.WDTC_CAR_INC_NULL, wdtcCarIncNull, Len.WDTC_CAR_INC_NULL);
    }

    /**Original name: WDTC-CAR-INC-NULL<br>*/
    public String getWdtcCarIncNull() {
        return readString(Pos.WDTC_CAR_INC_NULL, Len.WDTC_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_INC = 1;
        public static final int WDTC_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_INC = 8;
        public static final int WDTC_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
