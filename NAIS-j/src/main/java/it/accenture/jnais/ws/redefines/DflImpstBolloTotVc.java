package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-BOLLO-TOT-VC<br>
 * Variable: DFL-IMPST-BOLLO-TOT-VC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstBolloTotVc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstBolloTotVc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_BOLLO_TOT_VC;
    }

    public void setDflImpstBolloTotVc(AfDecimal dflImpstBolloTotVc) {
        writeDecimalAsPacked(Pos.DFL_IMPST_BOLLO_TOT_VC, dflImpstBolloTotVc.copy());
    }

    public void setDflImpstBolloTotVcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_BOLLO_TOT_VC, Pos.DFL_IMPST_BOLLO_TOT_VC);
    }

    /**Original name: DFL-IMPST-BOLLO-TOT-VC<br>*/
    public AfDecimal getDflImpstBolloTotVc() {
        return readPackedAsDecimal(Pos.DFL_IMPST_BOLLO_TOT_VC, Len.Int.DFL_IMPST_BOLLO_TOT_VC, Len.Fract.DFL_IMPST_BOLLO_TOT_VC);
    }

    public byte[] getDflImpstBolloTotVcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_BOLLO_TOT_VC, Pos.DFL_IMPST_BOLLO_TOT_VC);
        return buffer;
    }

    public void setDflImpstBolloTotVcNull(String dflImpstBolloTotVcNull) {
        writeString(Pos.DFL_IMPST_BOLLO_TOT_VC_NULL, dflImpstBolloTotVcNull, Len.DFL_IMPST_BOLLO_TOT_VC_NULL);
    }

    /**Original name: DFL-IMPST-BOLLO-TOT-VC-NULL<br>*/
    public String getDflImpstBolloTotVcNull() {
        return readString(Pos.DFL_IMPST_BOLLO_TOT_VC_NULL, Len.DFL_IMPST_BOLLO_TOT_VC_NULL);
    }

    public String getDflImpstBolloTotVcNullFormatted() {
        return Functions.padBlanks(getDflImpstBolloTotVcNull(), Len.DFL_IMPST_BOLLO_TOT_VC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_TOT_VC = 1;
        public static final int DFL_IMPST_BOLLO_TOT_VC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_TOT_VC = 8;
        public static final int DFL_IMPST_BOLLO_TOT_VC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_TOT_VC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_TOT_VC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
