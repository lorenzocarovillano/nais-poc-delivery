package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-SPE-FAIVL<br>
 * Variable: WRST-RIS-SPE-FAIVL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisSpeFaivl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisSpeFaivl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_SPE_FAIVL;
    }

    public void setWrstRisSpeFaivl(AfDecimal wrstRisSpeFaivl) {
        writeDecimalAsPacked(Pos.WRST_RIS_SPE_FAIVL, wrstRisSpeFaivl.copy());
    }

    public void setWrstRisSpeFaivlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_SPE_FAIVL, Pos.WRST_RIS_SPE_FAIVL);
    }

    /**Original name: WRST-RIS-SPE-FAIVL<br>*/
    public AfDecimal getWrstRisSpeFaivl() {
        return readPackedAsDecimal(Pos.WRST_RIS_SPE_FAIVL, Len.Int.WRST_RIS_SPE_FAIVL, Len.Fract.WRST_RIS_SPE_FAIVL);
    }

    public byte[] getWrstRisSpeFaivlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_SPE_FAIVL, Pos.WRST_RIS_SPE_FAIVL);
        return buffer;
    }

    public void initWrstRisSpeFaivlSpaces() {
        fill(Pos.WRST_RIS_SPE_FAIVL, Len.WRST_RIS_SPE_FAIVL, Types.SPACE_CHAR);
    }

    public void setWrstRisSpeFaivlNull(String wrstRisSpeFaivlNull) {
        writeString(Pos.WRST_RIS_SPE_FAIVL_NULL, wrstRisSpeFaivlNull, Len.WRST_RIS_SPE_FAIVL_NULL);
    }

    /**Original name: WRST-RIS-SPE-FAIVL-NULL<br>*/
    public String getWrstRisSpeFaivlNull() {
        return readString(Pos.WRST_RIS_SPE_FAIVL_NULL, Len.WRST_RIS_SPE_FAIVL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_SPE_FAIVL = 1;
        public static final int WRST_RIS_SPE_FAIVL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_SPE_FAIVL = 8;
        public static final int WRST_RIS_SPE_FAIVL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_SPE_FAIVL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_SPE_FAIVL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
