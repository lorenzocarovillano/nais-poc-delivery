package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-TP-GAR<br>
 * Variable: GRZ-TP-GAR from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzTpGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzTpGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_TP_GAR;
    }

    public void setGrzTpGar(short grzTpGar) {
        writeShortAsPacked(Pos.GRZ_TP_GAR, grzTpGar, Len.Int.GRZ_TP_GAR);
    }

    public void setGrzTpGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_TP_GAR, Pos.GRZ_TP_GAR);
    }

    /**Original name: GRZ-TP-GAR<br>*/
    public short getGrzTpGar() {
        return readPackedAsShort(Pos.GRZ_TP_GAR, Len.Int.GRZ_TP_GAR);
    }

    public byte[] getGrzTpGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_TP_GAR, Pos.GRZ_TP_GAR);
        return buffer;
    }

    public void setGrzTpGarNull(String grzTpGarNull) {
        writeString(Pos.GRZ_TP_GAR_NULL, grzTpGarNull, Len.GRZ_TP_GAR_NULL);
    }

    /**Original name: GRZ-TP-GAR-NULL<br>*/
    public String getGrzTpGarNull() {
        return readString(Pos.GRZ_TP_GAR_NULL, Len.GRZ_TP_GAR_NULL);
    }

    public String getGrzTpGarNullFormatted() {
        return Functions.padBlanks(getGrzTpGarNull(), Len.GRZ_TP_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_TP_GAR = 1;
        public static final int GRZ_TP_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_TP_GAR = 2;
        public static final int GRZ_TP_GAR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_TP_GAR = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
