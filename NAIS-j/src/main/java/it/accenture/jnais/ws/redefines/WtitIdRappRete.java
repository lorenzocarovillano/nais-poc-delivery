package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-ID-RAPP-RETE<br>
 * Variable: WTIT-ID-RAPP-RETE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitIdRappRete extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitIdRappRete() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_ID_RAPP_RETE;
    }

    public void setWtitIdRappRete(int wtitIdRappRete) {
        writeIntAsPacked(Pos.WTIT_ID_RAPP_RETE, wtitIdRappRete, Len.Int.WTIT_ID_RAPP_RETE);
    }

    public void setWtitIdRappReteFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_ID_RAPP_RETE, Pos.WTIT_ID_RAPP_RETE);
    }

    /**Original name: WTIT-ID-RAPP-RETE<br>*/
    public int getWtitIdRappRete() {
        return readPackedAsInt(Pos.WTIT_ID_RAPP_RETE, Len.Int.WTIT_ID_RAPP_RETE);
    }

    public byte[] getWtitIdRappReteAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_ID_RAPP_RETE, Pos.WTIT_ID_RAPP_RETE);
        return buffer;
    }

    public void initWtitIdRappReteSpaces() {
        fill(Pos.WTIT_ID_RAPP_RETE, Len.WTIT_ID_RAPP_RETE, Types.SPACE_CHAR);
    }

    public void setWtitIdRappReteNull(String wtitIdRappReteNull) {
        writeString(Pos.WTIT_ID_RAPP_RETE_NULL, wtitIdRappReteNull, Len.WTIT_ID_RAPP_RETE_NULL);
    }

    /**Original name: WTIT-ID-RAPP-RETE-NULL<br>*/
    public String getWtitIdRappReteNull() {
        return readString(Pos.WTIT_ID_RAPP_RETE_NULL, Len.WTIT_ID_RAPP_RETE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_ID_RAPP_RETE = 1;
        public static final int WTIT_ID_RAPP_RETE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_ID_RAPP_RETE = 5;
        public static final int WTIT_ID_RAPP_RETE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_ID_RAPP_RETE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
