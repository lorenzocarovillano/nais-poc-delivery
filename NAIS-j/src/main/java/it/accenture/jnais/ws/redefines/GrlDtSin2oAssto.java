package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRL-DT-SIN-2O-ASSTO<br>
 * Variable: GRL-DT-SIN-2O-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrlDtSin2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrlDtSin2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRL_DT_SIN2O_ASSTO;
    }

    public void setGrlDtSin2oAssto(int grlDtSin2oAssto) {
        writeIntAsPacked(Pos.GRL_DT_SIN2O_ASSTO, grlDtSin2oAssto, Len.Int.GRL_DT_SIN2O_ASSTO);
    }

    public void setGrlDtSin2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRL_DT_SIN2O_ASSTO, Pos.GRL_DT_SIN2O_ASSTO);
    }

    /**Original name: GRL-DT-SIN-2O-ASSTO<br>*/
    public int getGrlDtSin2oAssto() {
        return readPackedAsInt(Pos.GRL_DT_SIN2O_ASSTO, Len.Int.GRL_DT_SIN2O_ASSTO);
    }

    public byte[] getGrlDtSin2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRL_DT_SIN2O_ASSTO, Pos.GRL_DT_SIN2O_ASSTO);
        return buffer;
    }

    public void setGrlDtSin2oAsstoNull(String grlDtSin2oAsstoNull) {
        writeString(Pos.GRL_DT_SIN2O_ASSTO_NULL, grlDtSin2oAsstoNull, Len.GRL_DT_SIN2O_ASSTO_NULL);
    }

    /**Original name: GRL-DT-SIN-2O-ASSTO-NULL<br>*/
    public String getGrlDtSin2oAsstoNull() {
        return readString(Pos.GRL_DT_SIN2O_ASSTO_NULL, Len.GRL_DT_SIN2O_ASSTO_NULL);
    }

    public String getGrlDtSin2oAsstoNullFormatted() {
        return Functions.padBlanks(getGrlDtSin2oAsstoNull(), Len.GRL_DT_SIN2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRL_DT_SIN2O_ASSTO = 1;
        public static final int GRL_DT_SIN2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRL_DT_SIN2O_ASSTO = 5;
        public static final int GRL_DT_SIN2O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRL_DT_SIN2O_ASSTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
