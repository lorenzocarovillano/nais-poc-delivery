package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-CAR-GEST<br>
 * Variable: WDTC-CAR-GEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_CAR_GEST;
    }

    public void setWdtcCarGest(AfDecimal wdtcCarGest) {
        writeDecimalAsPacked(Pos.WDTC_CAR_GEST, wdtcCarGest.copy());
    }

    public void setWdtcCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_CAR_GEST, Pos.WDTC_CAR_GEST);
    }

    /**Original name: WDTC-CAR-GEST<br>*/
    public AfDecimal getWdtcCarGest() {
        return readPackedAsDecimal(Pos.WDTC_CAR_GEST, Len.Int.WDTC_CAR_GEST, Len.Fract.WDTC_CAR_GEST);
    }

    public byte[] getWdtcCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_CAR_GEST, Pos.WDTC_CAR_GEST);
        return buffer;
    }

    public void initWdtcCarGestSpaces() {
        fill(Pos.WDTC_CAR_GEST, Len.WDTC_CAR_GEST, Types.SPACE_CHAR);
    }

    public void setWdtcCarGestNull(String wdtcCarGestNull) {
        writeString(Pos.WDTC_CAR_GEST_NULL, wdtcCarGestNull, Len.WDTC_CAR_GEST_NULL);
    }

    /**Original name: WDTC-CAR-GEST-NULL<br>*/
    public String getWdtcCarGestNull() {
        return readString(Pos.WDTC_CAR_GEST_NULL, Len.WDTC_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_GEST = 1;
        public static final int WDTC_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_GEST = 8;
        public static final int WDTC_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
