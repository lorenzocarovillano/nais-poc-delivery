package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-DT-DECOR-DFLT<br>
 * Variable: DAD-DT-DECOR-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadDtDecorDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadDtDecorDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_DT_DECOR_DFLT;
    }

    public void setDadDtDecorDflt(int dadDtDecorDflt) {
        writeIntAsPacked(Pos.DAD_DT_DECOR_DFLT, dadDtDecorDflt, Len.Int.DAD_DT_DECOR_DFLT);
    }

    public void setDadDtDecorDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_DT_DECOR_DFLT, Pos.DAD_DT_DECOR_DFLT);
    }

    /**Original name: DAD-DT-DECOR-DFLT<br>*/
    public int getDadDtDecorDflt() {
        return readPackedAsInt(Pos.DAD_DT_DECOR_DFLT, Len.Int.DAD_DT_DECOR_DFLT);
    }

    public byte[] getDadDtDecorDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_DT_DECOR_DFLT, Pos.DAD_DT_DECOR_DFLT);
        return buffer;
    }

    public void setDadDtDecorDfltNull(String dadDtDecorDfltNull) {
        writeString(Pos.DAD_DT_DECOR_DFLT_NULL, dadDtDecorDfltNull, Len.DAD_DT_DECOR_DFLT_NULL);
    }

    /**Original name: DAD-DT-DECOR-DFLT-NULL<br>*/
    public String getDadDtDecorDfltNull() {
        return readString(Pos.DAD_DT_DECOR_DFLT_NULL, Len.DAD_DT_DECOR_DFLT_NULL);
    }

    public String getDadDtDecorDfltNullFormatted() {
        return Functions.padBlanks(getDadDtDecorDfltNull(), Len.DAD_DT_DECOR_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_DT_DECOR_DFLT = 1;
        public static final int DAD_DT_DECOR_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_DT_DECOR_DFLT = 5;
        public static final int DAD_DT_DECOR_DFLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_DT_DECOR_DFLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
