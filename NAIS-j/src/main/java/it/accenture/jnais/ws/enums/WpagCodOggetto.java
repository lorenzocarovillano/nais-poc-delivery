package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-COD-OGGETTO<br>
 * Variable: WPAG-COD-OGGETTO from copybook IDSV0161<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpagCodOggetto {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.COD_OGGETTO);
    public static final String NUM_PROP = "NUMERO-PROPOSTA";
    public static final String NUM_POLI = "NUMERO-POLIZZA";
    public static final String IDEN_ESTNO = "IDENTIFICATIVO-ESTERNO";
    public static final String NUM_RICH = "NUMERO-RICHIESTA";
    public static final String NUM_PREV = "NUMERO-PREVENTIVO";
    public static final String NUM_LIQ = "NUMERO-LIQUIDAZIONE";
    public static final String NUM_QUIET = "NUMERO-QUIETANZA";
    public static final String IDEN_DEROG = "IDENTIFICATIVO-DEROGA";
    public static final String NUM_REND = "NUMERO-RENDITA";
    public static final String NUM_RICH_EST = "NUMERO-RICHIESTA-EST";
    public static final String NUM_RICH_EST_CNL03 = "NUM-RICH-EST-CNL-03";
    public static final String NUM_RICH_EST_CNL04 = "NUM-RICH-EST-CNL-04";
    public static final String NUM_COM = "NUMERO-COMUNICAZIONE";
    public static final String NUM_POLI_C03 = "NUMERO-POLIZZA-CNL-03";
    public static final String NUM_POLI_C04 = "NUMERO-POLIZZA-CNL-04";
    public static final String NUM_POLI_CPI = "NUMERO-POLIZZA-CPI";
    public static final String VER_POLI_CPI = "VERIFI-POLIZZA-CPI";
    public static final String COD_AUTORIZ = "CODICE-AUTORIZZAZIONE";
    public static final String FNZ_EXT_ACCOUNT_ID = "FNZ-EXT-ACCOUNT-ID";
    public static final String FNZ_EXT_ORDER_ID = "FNZ-EXT-ORDER-ID";
    public static final String NUM_IB_RICH_EST = "NUM-IB-RICH-EST";
    public static final String NUM_RICH_EST_C7A = "NUM-RICH-EST-C7A";
    public static final String NUM_ADES_COLL = "NUM-ADES-COLL";
    public static final String NUM_TRCH_COLL = "NUM-TRCH-COLL";

    //==== METHODS ====
    public void setCodOggetto(String codOggetto) {
        this.value = Functions.subString(codOggetto, Len.COD_OGGETTO);
    }

    public String getCodOggetto() {
        return this.value;
    }

    public void setC161NumProp() {
        value = NUM_PROP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_OGGETTO = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
