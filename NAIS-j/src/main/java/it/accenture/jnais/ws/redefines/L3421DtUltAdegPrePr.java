package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-DT-ULT-ADEG-PRE-PR<br>
 * Variable: L3421-DT-ULT-ADEG-PRE-PR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421DtUltAdegPrePr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421DtUltAdegPrePr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_DT_ULT_ADEG_PRE_PR;
    }

    public void setL3421DtUltAdegPrePrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_DT_ULT_ADEG_PRE_PR, Pos.L3421_DT_ULT_ADEG_PRE_PR);
    }

    /**Original name: L3421-DT-ULT-ADEG-PRE-PR<br>*/
    public int getL3421DtUltAdegPrePr() {
        return readPackedAsInt(Pos.L3421_DT_ULT_ADEG_PRE_PR, Len.Int.L3421_DT_ULT_ADEG_PRE_PR);
    }

    public byte[] getL3421DtUltAdegPrePrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_DT_ULT_ADEG_PRE_PR, Pos.L3421_DT_ULT_ADEG_PRE_PR);
        return buffer;
    }

    /**Original name: L3421-DT-ULT-ADEG-PRE-PR-NULL<br>*/
    public String getL3421DtUltAdegPrePrNull() {
        return readString(Pos.L3421_DT_ULT_ADEG_PRE_PR_NULL, Len.L3421_DT_ULT_ADEG_PRE_PR_NULL);
    }

    public String getL3421DtUltAdegPrePrNullFormatted() {
        return Functions.padBlanks(getL3421DtUltAdegPrePrNull(), Len.L3421_DT_ULT_ADEG_PRE_PR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_DT_ULT_ADEG_PRE_PR = 1;
        public static final int L3421_DT_ULT_ADEG_PRE_PR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_DT_ULT_ADEG_PRE_PR = 5;
        public static final int L3421_DT_ULT_ADEG_PRE_PR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_DT_ULT_ADEG_PRE_PR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
