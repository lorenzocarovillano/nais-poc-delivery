package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IABV0006-FLAG-SIMULAZIONE<br>
 * Variable: IABV0006-FLAG-SIMULAZIONE from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006FlagSimulazione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char INFR = 'I';
    public static final char APPL = 'A';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagSimulazione(char flagSimulazione) {
        this.value = flagSimulazione;
    }

    public char getFlagSimulazione() {
        return this.value;
    }

    public boolean isNo() {
        return value == NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_SIMULAZIONE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
