package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-DATI-PACCHETTO<br>
 * Variables: ISPC0040-DATI-PACCHETTO from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040DatiPacchetto {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-PACCHETTO
    private String codPacchetto = DefaultValues.stringVal(Len.COD_PACCHETTO);
    //Original name: ISPC0040-DESC-PACCHETTO
    private String descPacchetto = DefaultValues.stringVal(Len.DESC_PACCHETTO);

    //==== METHODS ====
    public void setDatiPacchettoBytes(byte[] buffer, int offset) {
        int position = offset;
        codPacchetto = MarshalByte.readString(buffer, position, Len.COD_PACCHETTO);
        position += Len.COD_PACCHETTO;
        descPacchetto = MarshalByte.readString(buffer, position, Len.DESC_PACCHETTO);
    }

    public byte[] getDatiPacchettoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codPacchetto, Len.COD_PACCHETTO);
        position += Len.COD_PACCHETTO;
        MarshalByte.writeString(buffer, position, descPacchetto, Len.DESC_PACCHETTO);
        return buffer;
    }

    public void initDatiPacchettoSpaces() {
        codPacchetto = "";
        descPacchetto = "";
    }

    public void setCodPacchetto(String codPacchetto) {
        this.codPacchetto = Functions.subString(codPacchetto, Len.COD_PACCHETTO);
    }

    public String getCodPacchetto() {
        return this.codPacchetto;
    }

    public void setDescPacchetto(String descPacchetto) {
        this.descPacchetto = Functions.subString(descPacchetto, Len.DESC_PACCHETTO);
    }

    public String getDescPacchetto() {
        return this.descPacchetto;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_PACCHETTO = 40;
        public static final int DESC_PACCHETTO = 40;
        public static final int DATI_PACCHETTO = COD_PACCHETTO + DESC_PACCHETTO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
