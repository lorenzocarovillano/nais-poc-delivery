package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-RIV-IND<br>
 * Variable: WPCO-DT-ULT-EC-RIV-IND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcRivInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcRivInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_RIV_IND;
    }

    public void setWpcoDtUltEcRivInd(int wpcoDtUltEcRivInd) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_RIV_IND, wpcoDtUltEcRivInd, Len.Int.WPCO_DT_ULT_EC_RIV_IND);
    }

    public void setDpcoDtUltEcRivIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_RIV_IND, Pos.WPCO_DT_ULT_EC_RIV_IND);
    }

    /**Original name: WPCO-DT-ULT-EC-RIV-IND<br>*/
    public int getWpcoDtUltEcRivInd() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_RIV_IND, Len.Int.WPCO_DT_ULT_EC_RIV_IND);
    }

    public byte[] getWpcoDtUltEcRivIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_RIV_IND, Pos.WPCO_DT_ULT_EC_RIV_IND);
        return buffer;
    }

    public void setWpcoDtUltEcRivIndNull(String wpcoDtUltEcRivIndNull) {
        writeString(Pos.WPCO_DT_ULT_EC_RIV_IND_NULL, wpcoDtUltEcRivIndNull, Len.WPCO_DT_ULT_EC_RIV_IND_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-RIV-IND-NULL<br>*/
    public String getWpcoDtUltEcRivIndNull() {
        return readString(Pos.WPCO_DT_ULT_EC_RIV_IND_NULL, Len.WPCO_DT_ULT_EC_RIV_IND_NULL);
    }

    public String getWpcoDtUltEcRivIndNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcRivIndNull(), Len.WPCO_DT_ULT_EC_RIV_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_RIV_IND = 1;
        public static final int WPCO_DT_ULT_EC_RIV_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_RIV_IND = 5;
        public static final int WPCO_DT_ULT_EC_RIV_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_RIV_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
