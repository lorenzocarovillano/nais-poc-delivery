package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PROV-DA-REC<br>
 * Variable: WDTC-PROV-DA-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PROV_DA_REC;
    }

    public void setWdtcProvDaRec(AfDecimal wdtcProvDaRec) {
        writeDecimalAsPacked(Pos.WDTC_PROV_DA_REC, wdtcProvDaRec.copy());
    }

    public void setWdtcProvDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PROV_DA_REC, Pos.WDTC_PROV_DA_REC);
    }

    /**Original name: WDTC-PROV-DA-REC<br>*/
    public AfDecimal getWdtcProvDaRec() {
        return readPackedAsDecimal(Pos.WDTC_PROV_DA_REC, Len.Int.WDTC_PROV_DA_REC, Len.Fract.WDTC_PROV_DA_REC);
    }

    public byte[] getWdtcProvDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PROV_DA_REC, Pos.WDTC_PROV_DA_REC);
        return buffer;
    }

    public void initWdtcProvDaRecSpaces() {
        fill(Pos.WDTC_PROV_DA_REC, Len.WDTC_PROV_DA_REC, Types.SPACE_CHAR);
    }

    public void setWdtcProvDaRecNull(String wdtcProvDaRecNull) {
        writeString(Pos.WDTC_PROV_DA_REC_NULL, wdtcProvDaRecNull, Len.WDTC_PROV_DA_REC_NULL);
    }

    /**Original name: WDTC-PROV-DA-REC-NULL<br>*/
    public String getWdtcProvDaRecNull() {
        return readString(Pos.WDTC_PROV_DA_REC_NULL, Len.WDTC_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_DA_REC = 1;
        public static final int WDTC_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_DA_REC = 8;
        public static final int WDTC_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
