package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: S089-DT-DEN<br>
 * Variable: S089-DT-DEN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtDen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089DtDen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_DT_DEN;
    }

    public void setWlquDtDen(int wlquDtDen) {
        writeIntAsPacked(Pos.S089_DT_DEN, wlquDtDen, Len.Int.WLQU_DT_DEN);
    }

    public void setWlquDtDenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_DT_DEN, Pos.S089_DT_DEN);
    }

    /**Original name: WLQU-DT-DEN<br>*/
    public int getWlquDtDen() {
        return readPackedAsInt(Pos.S089_DT_DEN, Len.Int.WLQU_DT_DEN);
    }

    public String getDlquDtDenFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWlquDtDen()).toString();
    }

    public byte[] getWlquDtDenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_DT_DEN, Pos.S089_DT_DEN);
        return buffer;
    }

    public void initWlquDtDenSpaces() {
        fill(Pos.S089_DT_DEN, Len.S089_DT_DEN, Types.SPACE_CHAR);
    }

    public void setWlquDtDenNull(String wlquDtDenNull) {
        writeString(Pos.S089_DT_DEN_NULL, wlquDtDenNull, Len.WLQU_DT_DEN_NULL);
    }

    /**Original name: WLQU-DT-DEN-NULL<br>*/
    public String getWlquDtDenNull() {
        return readString(Pos.S089_DT_DEN_NULL, Len.WLQU_DT_DEN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_DT_DEN = 1;
        public static final int S089_DT_DEN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_DT_DEN = 5;
        public static final int WLQU_DT_DEN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_DT_DEN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
