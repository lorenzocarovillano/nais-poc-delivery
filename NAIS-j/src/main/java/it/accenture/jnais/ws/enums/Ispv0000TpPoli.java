package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPV0000-TP-POLI<br>
 * Variable: ISPV0000-TP-POLI from copybook ISPV0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispv0000TpPoli {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TP_POLI);
    public static final String COLL_TFR = "01";
    public static final String COLL_PREV = "02";
    public static final String COLL_GRUPPO = "03";
    public static final String COLL_TFM = "04";
    public static final String COLL_FP_APERTO = "05";
    public static final String COLL_FP_CHIUSO = "06";
    public static final String COLL_FP_COMP = "07";
    public static final String IND_FIP_PIP = "08";
    public static final String IND_NO_FIP_PIP = "09";

    //==== METHODS ====
    public void setTpPoli(String tpPoli) {
        this.value = Functions.subString(tpPoli, Len.TP_POLI);
    }

    public String getTpPoli() {
        return this.value;
    }

    public boolean isIspv0000IndFipPip() {
        return value.equals(IND_FIP_PIP);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_POLI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
