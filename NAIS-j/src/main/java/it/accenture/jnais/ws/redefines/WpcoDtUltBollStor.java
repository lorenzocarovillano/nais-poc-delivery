package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-STOR<br>
 * Variable: WPCO-DT-ULT-BOLL-STOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollStor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollStor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_STOR;
    }

    public void setWpcoDtUltBollStor(int wpcoDtUltBollStor) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_STOR, wpcoDtUltBollStor, Len.Int.WPCO_DT_ULT_BOLL_STOR);
    }

    public void setDpcoDtUltBollStorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_STOR, Pos.WPCO_DT_ULT_BOLL_STOR);
    }

    /**Original name: WPCO-DT-ULT-BOLL-STOR<br>*/
    public int getWpcoDtUltBollStor() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_STOR, Len.Int.WPCO_DT_ULT_BOLL_STOR);
    }

    public byte[] getWpcoDtUltBollStorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_STOR, Pos.WPCO_DT_ULT_BOLL_STOR);
        return buffer;
    }

    public void setWpcoDtUltBollStorNull(String wpcoDtUltBollStorNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_STOR_NULL, wpcoDtUltBollStorNull, Len.WPCO_DT_ULT_BOLL_STOR_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-STOR-NULL<br>*/
    public String getWpcoDtUltBollStorNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_STOR_NULL, Len.WPCO_DT_ULT_BOLL_STOR_NULL);
    }

    public String getWpcoDtUltBollStorNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollStorNull(), Len.WPCO_DT_ULT_BOLL_STOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_STOR = 1;
        public static final int WPCO_DT_ULT_BOLL_STOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_STOR = 5;
        public static final int WPCO_DT_ULT_BOLL_STOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_STOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
