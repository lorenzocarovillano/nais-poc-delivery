package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-ACC-CALC<br>
 * Variable: WDFL-ACCPRE-ACC-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreAccCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreAccCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_ACC_CALC;
    }

    public void setWdflAccpreAccCalc(AfDecimal wdflAccpreAccCalc) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_ACC_CALC, wdflAccpreAccCalc.copy());
    }

    public void setWdflAccpreAccCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_ACC_CALC, Pos.WDFL_ACCPRE_ACC_CALC);
    }

    /**Original name: WDFL-ACCPRE-ACC-CALC<br>*/
    public AfDecimal getWdflAccpreAccCalc() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_ACC_CALC, Len.Int.WDFL_ACCPRE_ACC_CALC, Len.Fract.WDFL_ACCPRE_ACC_CALC);
    }

    public byte[] getWdflAccpreAccCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_ACC_CALC, Pos.WDFL_ACCPRE_ACC_CALC);
        return buffer;
    }

    public void setWdflAccpreAccCalcNull(String wdflAccpreAccCalcNull) {
        writeString(Pos.WDFL_ACCPRE_ACC_CALC_NULL, wdflAccpreAccCalcNull, Len.WDFL_ACCPRE_ACC_CALC_NULL);
    }

    /**Original name: WDFL-ACCPRE-ACC-CALC-NULL<br>*/
    public String getWdflAccpreAccCalcNull() {
        return readString(Pos.WDFL_ACCPRE_ACC_CALC_NULL, Len.WDFL_ACCPRE_ACC_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_ACC_CALC = 1;
        public static final int WDFL_ACCPRE_ACC_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_ACC_CALC = 8;
        public static final int WDFL_ACCPRE_ACC_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_ACC_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_ACC_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
