package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COD-SUBAGE<br>
 * Variable: WB03-COD-SUBAGE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CodSubage extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CodSubage() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COD_SUBAGE;
    }

    public void setWb03CodSubage(int wb03CodSubage) {
        writeIntAsPacked(Pos.WB03_COD_SUBAGE, wb03CodSubage, Len.Int.WB03_COD_SUBAGE);
    }

    public void setWb03CodSubageFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COD_SUBAGE, Pos.WB03_COD_SUBAGE);
    }

    /**Original name: WB03-COD-SUBAGE<br>*/
    public int getWb03CodSubage() {
        return readPackedAsInt(Pos.WB03_COD_SUBAGE, Len.Int.WB03_COD_SUBAGE);
    }

    public byte[] getWb03CodSubageAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COD_SUBAGE, Pos.WB03_COD_SUBAGE);
        return buffer;
    }

    public void setWb03CodSubageNull(String wb03CodSubageNull) {
        writeString(Pos.WB03_COD_SUBAGE_NULL, wb03CodSubageNull, Len.WB03_COD_SUBAGE_NULL);
    }

    /**Original name: WB03-COD-SUBAGE-NULL<br>*/
    public String getWb03CodSubageNull() {
        return readString(Pos.WB03_COD_SUBAGE_NULL, Len.WB03_COD_SUBAGE_NULL);
    }

    public String getWb03CodSubageNullFormatted() {
        return Functions.padBlanks(getWb03CodSubageNull(), Len.WB03_COD_SUBAGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COD_SUBAGE = 1;
        public static final int WB03_COD_SUBAGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COD_SUBAGE = 3;
        public static final int WB03_COD_SUBAGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COD_SUBAGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
