package it.accenture.jnais.ws.enums;

/**Original name: FLAG-TIPO-PGM<br>
 * Variable: FLAG-TIPO-PGM from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTipoPgm {

    //==== PROPERTIES ====
    private char value = 'A';
    public static final char ARCHITECTURE = 'A';
    public static final char BUS_SERVICE = 'B';

    //==== METHODS ====
    public void setFlagTipoPgm(char flagTipoPgm) {
        this.value = flagTipoPgm;
    }

    public char getFlagTipoPgm() {
        return this.value;
    }

    public void setArchitecture() {
        value = ARCHITECTURE;
    }

    public boolean isBusService() {
        return value == BUS_SERVICE;
    }

    public void setBusService() {
        value = BUS_SERVICE;
    }
}
