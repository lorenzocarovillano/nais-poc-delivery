package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AAAAMMGG<br>
 * Variable: AAAAMMGG from program LCCS0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Aaaammgg {

    //==== PROPERTIES ====
    //Original name: AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);
    //Original name: MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setAaaammggBytes(byte[] buffer) {
        setAaaammggBytes(buffer, 1);
    }

    public void setAaaammggBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaa = MarshalByte.readFixedString(buffer, position, Len.AAAA);
        position += Len.AAAA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public void setMm(short mm) {
        this.mm = NumericDisplay.asString(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public void setGg(short gg) {
        this.gg = NumericDisplay.asString(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA = 4;
        public static final int MM = 2;
        public static final int GG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
