package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-TS-RIVAL-INDICIZ<br>
 * Variable: TGA-TS-RIVAL-INDICIZ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaTsRivalIndiciz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaTsRivalIndiciz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_TS_RIVAL_INDICIZ;
    }

    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        writeDecimalAsPacked(Pos.TGA_TS_RIVAL_INDICIZ, tgaTsRivalIndiciz.copy());
    }

    public void setTgaTsRivalIndicizFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_TS_RIVAL_INDICIZ, Pos.TGA_TS_RIVAL_INDICIZ);
    }

    /**Original name: TGA-TS-RIVAL-INDICIZ<br>*/
    public AfDecimal getTgaTsRivalIndiciz() {
        return readPackedAsDecimal(Pos.TGA_TS_RIVAL_INDICIZ, Len.Int.TGA_TS_RIVAL_INDICIZ, Len.Fract.TGA_TS_RIVAL_INDICIZ);
    }

    public byte[] getTgaTsRivalIndicizAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_TS_RIVAL_INDICIZ, Pos.TGA_TS_RIVAL_INDICIZ);
        return buffer;
    }

    public void setTgaTsRivalIndicizNull(String tgaTsRivalIndicizNull) {
        writeString(Pos.TGA_TS_RIVAL_INDICIZ_NULL, tgaTsRivalIndicizNull, Len.TGA_TS_RIVAL_INDICIZ_NULL);
    }

    /**Original name: TGA-TS-RIVAL-INDICIZ-NULL<br>*/
    public String getTgaTsRivalIndicizNull() {
        return readString(Pos.TGA_TS_RIVAL_INDICIZ_NULL, Len.TGA_TS_RIVAL_INDICIZ_NULL);
    }

    public String getTgaTsRivalIndicizNullFormatted() {
        return Functions.padBlanks(getTgaTsRivalIndicizNull(), Len.TGA_TS_RIVAL_INDICIZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_TS_RIVAL_INDICIZ = 1;
        public static final int TGA_TS_RIVAL_INDICIZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_TS_RIVAL_INDICIZ = 8;
        public static final int TGA_TS_RIVAL_INDICIZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_TS_RIVAL_INDICIZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_TS_RIVAL_INDICIZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
