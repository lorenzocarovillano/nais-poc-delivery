package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E15-ID-MOVI-CHIU<br>
 * Variable: E15-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E15IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E15IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E15_ID_MOVI_CHIU;
    }

    public void setE15IdMoviChiu(int e15IdMoviChiu) {
        writeIntAsPacked(Pos.E15_ID_MOVI_CHIU, e15IdMoviChiu, Len.Int.E15_ID_MOVI_CHIU);
    }

    public void setE15IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E15_ID_MOVI_CHIU, Pos.E15_ID_MOVI_CHIU);
    }

    /**Original name: E15-ID-MOVI-CHIU<br>*/
    public int getE15IdMoviChiu() {
        return readPackedAsInt(Pos.E15_ID_MOVI_CHIU, Len.Int.E15_ID_MOVI_CHIU);
    }

    public byte[] getE15IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E15_ID_MOVI_CHIU, Pos.E15_ID_MOVI_CHIU);
        return buffer;
    }

    public void setE15IdMoviChiuNull(String e15IdMoviChiuNull) {
        writeString(Pos.E15_ID_MOVI_CHIU_NULL, e15IdMoviChiuNull, Len.E15_ID_MOVI_CHIU_NULL);
    }

    /**Original name: E15-ID-MOVI-CHIU-NULL<br>*/
    public String getE15IdMoviChiuNull() {
        return readString(Pos.E15_ID_MOVI_CHIU_NULL, Len.E15_ID_MOVI_CHIU_NULL);
    }

    public String getE15IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getE15IdMoviChiuNull(), Len.E15_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E15_ID_MOVI_CHIU = 1;
        public static final int E15_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E15_ID_MOVI_CHIU = 5;
        public static final int E15_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E15_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
