package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-IRPEF<br>
 * Variable: S089-IMPB-IRPEF from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbIrpef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbIrpef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_IRPEF;
    }

    public void setWlquImpbIrpef(AfDecimal wlquImpbIrpef) {
        writeDecimalAsPacked(Pos.S089_IMPB_IRPEF, wlquImpbIrpef.copy());
    }

    public void setWlquImpbIrpefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_IRPEF, Pos.S089_IMPB_IRPEF);
    }

    /**Original name: WLQU-IMPB-IRPEF<br>*/
    public AfDecimal getWlquImpbIrpef() {
        return readPackedAsDecimal(Pos.S089_IMPB_IRPEF, Len.Int.WLQU_IMPB_IRPEF, Len.Fract.WLQU_IMPB_IRPEF);
    }

    public byte[] getWlquImpbIrpefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_IRPEF, Pos.S089_IMPB_IRPEF);
        return buffer;
    }

    public void initWlquImpbIrpefSpaces() {
        fill(Pos.S089_IMPB_IRPEF, Len.S089_IMPB_IRPEF, Types.SPACE_CHAR);
    }

    public void setWlquImpbIrpefNull(String wlquImpbIrpefNull) {
        writeString(Pos.S089_IMPB_IRPEF_NULL, wlquImpbIrpefNull, Len.WLQU_IMPB_IRPEF_NULL);
    }

    /**Original name: WLQU-IMPB-IRPEF-NULL<br>*/
    public String getWlquImpbIrpefNull() {
        return readString(Pos.S089_IMPB_IRPEF_NULL, Len.WLQU_IMPB_IRPEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_IRPEF = 1;
        public static final int S089_IMPB_IRPEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_IRPEF = 8;
        public static final int WLQU_IMPB_IRPEF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_IRPEF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_IRPEF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
