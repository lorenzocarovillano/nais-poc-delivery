package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-ADER-TGA<br>
 * Variable: WPAG-IMP-ADER-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAderTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAderTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ADER_TGA;
    }

    public void setWpagImpAderTga(AfDecimal wpagImpAderTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ADER_TGA, wpagImpAderTga.copy());
    }

    public void setWpagImpAderTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ADER_TGA, Pos.WPAG_IMP_ADER_TGA);
    }

    /**Original name: WPAG-IMP-ADER-TGA<br>*/
    public AfDecimal getWpagImpAderTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ADER_TGA, Len.Int.WPAG_IMP_ADER_TGA, Len.Fract.WPAG_IMP_ADER_TGA);
    }

    public byte[] getWpagImpAderTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ADER_TGA, Pos.WPAG_IMP_ADER_TGA);
        return buffer;
    }

    public void initWpagImpAderTgaSpaces() {
        fill(Pos.WPAG_IMP_ADER_TGA, Len.WPAG_IMP_ADER_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpAderTgaNull(String wpagImpAderTgaNull) {
        writeString(Pos.WPAG_IMP_ADER_TGA_NULL, wpagImpAderTgaNull, Len.WPAG_IMP_ADER_TGA_NULL);
    }

    /**Original name: WPAG-IMP-ADER-TGA-NULL<br>*/
    public String getWpagImpAderTgaNull() {
        return readString(Pos.WPAG_IMP_ADER_TGA_NULL, Len.WPAG_IMP_ADER_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ADER_TGA = 1;
        public static final int WPAG_IMP_ADER_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ADER_TGA = 8;
        public static final int WPAG_IMP_ADER_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ADER_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ADER_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
