package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-DT-INI-COP<br>
 * Variable: WDTC-DT-INI-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_DT_INI_COP;
    }

    public void setWdtcDtIniCop(int wdtcDtIniCop) {
        writeIntAsPacked(Pos.WDTC_DT_INI_COP, wdtcDtIniCop, Len.Int.WDTC_DT_INI_COP);
    }

    public void setWdtcDtIniCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_DT_INI_COP, Pos.WDTC_DT_INI_COP);
    }

    /**Original name: WDTC-DT-INI-COP<br>*/
    public int getWdtcDtIniCop() {
        return readPackedAsInt(Pos.WDTC_DT_INI_COP, Len.Int.WDTC_DT_INI_COP);
    }

    public byte[] getWdtcDtIniCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_DT_INI_COP, Pos.WDTC_DT_INI_COP);
        return buffer;
    }

    public void initWdtcDtIniCopSpaces() {
        fill(Pos.WDTC_DT_INI_COP, Len.WDTC_DT_INI_COP, Types.SPACE_CHAR);
    }

    public void setWdtcDtIniCopNull(String wdtcDtIniCopNull) {
        writeString(Pos.WDTC_DT_INI_COP_NULL, wdtcDtIniCopNull, Len.WDTC_DT_INI_COP_NULL);
    }

    /**Original name: WDTC-DT-INI-COP-NULL<br>*/
    public String getWdtcDtIniCopNull() {
        return readString(Pos.WDTC_DT_INI_COP_NULL, Len.WDTC_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_DT_INI_COP = 1;
        public static final int WDTC_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_DT_INI_COP = 5;
        public static final int WDTC_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
