package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IINT-PREST-DFZ<br>
 * Variable: DFL-IINT-PREST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIintPrestDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIintPrestDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IINT_PREST_DFZ;
    }

    public void setDflIintPrestDfz(AfDecimal dflIintPrestDfz) {
        writeDecimalAsPacked(Pos.DFL_IINT_PREST_DFZ, dflIintPrestDfz.copy());
    }

    public void setDflIintPrestDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IINT_PREST_DFZ, Pos.DFL_IINT_PREST_DFZ);
    }

    /**Original name: DFL-IINT-PREST-DFZ<br>*/
    public AfDecimal getDflIintPrestDfz() {
        return readPackedAsDecimal(Pos.DFL_IINT_PREST_DFZ, Len.Int.DFL_IINT_PREST_DFZ, Len.Fract.DFL_IINT_PREST_DFZ);
    }

    public byte[] getDflIintPrestDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IINT_PREST_DFZ, Pos.DFL_IINT_PREST_DFZ);
        return buffer;
    }

    public void setDflIintPrestDfzNull(String dflIintPrestDfzNull) {
        writeString(Pos.DFL_IINT_PREST_DFZ_NULL, dflIintPrestDfzNull, Len.DFL_IINT_PREST_DFZ_NULL);
    }

    /**Original name: DFL-IINT-PREST-DFZ-NULL<br>*/
    public String getDflIintPrestDfzNull() {
        return readString(Pos.DFL_IINT_PREST_DFZ_NULL, Len.DFL_IINT_PREST_DFZ_NULL);
    }

    public String getDflIintPrestDfzNullFormatted() {
        return Functions.padBlanks(getDflIintPrestDfzNull(), Len.DFL_IINT_PREST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IINT_PREST_DFZ = 1;
        public static final int DFL_IINT_PREST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IINT_PREST_DFZ = 8;
        public static final int DFL_IINT_PREST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IINT_PREST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IINT_PREST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
