package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-SD-I<br>
 * Variable: WPCO-DT-ULT-BOLL-SD-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollSdI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollSdI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_SD_I;
    }

    public void setWpcoDtUltBollSdI(int wpcoDtUltBollSdI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_SD_I, wpcoDtUltBollSdI, Len.Int.WPCO_DT_ULT_BOLL_SD_I);
    }

    public void setDpcoDtUltBollSdIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SD_I, Pos.WPCO_DT_ULT_BOLL_SD_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SD-I<br>*/
    public int getWpcoDtUltBollSdI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_SD_I, Len.Int.WPCO_DT_ULT_BOLL_SD_I);
    }

    public byte[] getWpcoDtUltBollSdIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_SD_I, Pos.WPCO_DT_ULT_BOLL_SD_I);
        return buffer;
    }

    public void setWpcoDtUltBollSdINull(String wpcoDtUltBollSdINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_SD_I_NULL, wpcoDtUltBollSdINull, Len.WPCO_DT_ULT_BOLL_SD_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-SD-I-NULL<br>*/
    public String getWpcoDtUltBollSdINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_SD_I_NULL, Len.WPCO_DT_ULT_BOLL_SD_I_NULL);
    }

    public String getWpcoDtUltBollSdINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollSdINull(), Len.WPCO_DT_ULT_BOLL_SD_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SD_I = 1;
        public static final int WPCO_DT_ULT_BOLL_SD_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_SD_I = 5;
        public static final int WPCO_DT_ULT_BOLL_SD_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_SD_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
