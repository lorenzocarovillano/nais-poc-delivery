package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL19-TABELLA<br>
 * Variable: WL19-TABELLA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl19TabellaLlbs0230 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_FND_MAXOCCURS = 250;
    public static final char WL19_ST_ADD = 'A';
    public static final char WL19_ST_MOD = 'M';
    public static final char WL19_ST_INV = 'I';
    public static final char WL19_ST_DEL = 'D';
    public static final char WL19_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public Wl19TabellaLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_TABELLA;
    }

    public String getWl19TabellaFormatted() {
        return readFixedString(Pos.WL19_TABELLA, Len.WL19_TABELLA);
    }

    public void setWl19TabellaBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL19_TABELLA, Pos.WL19_TABELLA);
    }

    public byte[] getWl19TabellaBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL19_TABELLA, Pos.WL19_TABELLA);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wl19Status(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WL19-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA QUOTZ_FND_UNIT
	 *    ALIAS L19
	 *    ULTIMO AGG. 24 APR 2015
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wl19Status(statusIdx - 1);
        return readChar(position);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wl19CodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WL19-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wl19CodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodFnd(int codFndIdx, String codFnd) {
        int position = Pos.wl19CodFnd(codFndIdx - 1);
        writeString(position, codFnd, Len.COD_FND);
    }

    /**Original name: WL19-COD-FND<br>*/
    public String getCodFnd(int codFndIdx) {
        int position = Pos.wl19CodFnd(codFndIdx - 1);
        return readString(position, Len.COD_FND);
    }

    public void setDtQtz(int dtQtzIdx, int dtQtz) {
        int position = Pos.wl19DtQtz(dtQtzIdx - 1);
        writeIntAsPacked(position, dtQtz, Len.Int.DT_QTZ);
    }

    /**Original name: WL19-DT-QTZ<br>*/
    public int getDtQtz(int dtQtzIdx) {
        int position = Pos.wl19DtQtz(dtQtzIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_QTZ);
    }

    public void setValQuo(int valQuoIdx, AfDecimal valQuo) {
        int position = Pos.wl19ValQuo(valQuoIdx - 1);
        writeDecimalAsPacked(position, valQuo.copy());
    }

    /**Original name: WL19-VAL-QUO<br>*/
    public AfDecimal getValQuo(int valQuoIdx) {
        int position = Pos.wl19ValQuo(valQuoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_QUO, Len.Fract.VAL_QUO);
    }

    public void setValQuoManfee(int valQuoManfeeIdx, AfDecimal valQuoManfee) {
        int position = Pos.wl19ValQuoManfee(valQuoManfeeIdx - 1);
        writeDecimalAsPacked(position, valQuoManfee.copy());
    }

    /**Original name: WL19-VAL-QUO-MANFEE<br>*/
    public AfDecimal getValQuoManfee(int valQuoManfeeIdx) {
        int position = Pos.wl19ValQuoManfee(valQuoManfeeIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_QUO_MANFEE, Len.Fract.VAL_QUO_MANFEE);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wl19DsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WL19-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wl19DsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wl19DsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WL19-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wl19DsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsCptz(int dsTsCptzIdx, long dsTsCptz) {
        int position = Pos.wl19DsTsCptz(dsTsCptzIdx - 1);
        writeLongAsPacked(position, dsTsCptz, Len.Int.DS_TS_CPTZ);
    }

    /**Original name: WL19-DS-TS-CPTZ<br>*/
    public long getDsTsCptz(int dsTsCptzIdx) {
        int position = Pos.wl19DsTsCptz(dsTsCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wl19DsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WL19-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wl19DsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wl19DsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WL19-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wl19DsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setTpFnd(int tpFndIdx, char tpFnd) {
        int position = Pos.wl19TpFnd(tpFndIdx - 1);
        writeChar(position, tpFnd);
    }

    /**Original name: WL19-TP-FND<br>*/
    public char getTpFnd(int tpFndIdx) {
        int position = Pos.wl19TpFnd(tpFndIdx - 1);
        return readChar(position);
    }

    public void setDtRilevazioneNav(int dtRilevazioneNavIdx, int dtRilevazioneNav) {
        int position = Pos.wl19DtRilevazioneNav(dtRilevazioneNavIdx - 1);
        writeIntAsPacked(position, dtRilevazioneNav, Len.Int.DT_RILEVAZIONE_NAV);
    }

    /**Original name: WL19-DT-RILEVAZIONE-NAV<br>*/
    public int getDtRilevazioneNav(int dtRilevazioneNavIdx) {
        int position = Pos.wl19DtRilevazioneNav(dtRilevazioneNavIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_RILEVAZIONE_NAV);
    }

    public void setValQuoAcq(int valQuoAcqIdx, AfDecimal valQuoAcq) {
        int position = Pos.wl19ValQuoAcq(valQuoAcqIdx - 1);
        writeDecimalAsPacked(position, valQuoAcq.copy());
    }

    /**Original name: WL19-VAL-QUO-ACQ<br>*/
    public AfDecimal getValQuoAcq(int valQuoAcqIdx) {
        int position = Pos.wl19ValQuoAcq(valQuoAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_QUO_ACQ, Len.Fract.VAL_QUO_ACQ);
    }

    public void setFlNoNav(int flNoNavIdx, char flNoNav) {
        int position = Pos.wl19FlNoNav(flNoNavIdx - 1);
        writeChar(position, flNoNav);
    }

    /**Original name: WL19-FL-NO-NAV<br>*/
    public char getFlNoNav(int flNoNavIdx) {
        int position = Pos.wl19FlNoNav(flNoNavIdx - 1);
        return readChar(position);
    }

    public void setRestoTabella(String restoTabella) {
        writeString(Pos.RESTO_TABELLA, restoTabella, Len.RESTO_TABELLA);
    }

    /**Original name: WL19-RESTO-TABELLA<br>*/
    public String getRestoTabella() {
        return readString(Pos.RESTO_TABELLA, Len.RESTO_TABELLA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL19_TABELLA = 1;
        public static final int WL19_TABELLA_R = 1;
        public static final int FLR1 = WL19_TABELLA_R;
        public static final int RESTO_TABELLA = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wl19TabFnd(int idx) {
            return WL19_TABELLA + idx * Len.TAB_FND;
        }

        public static int wl19Status(int idx) {
            return wl19TabFnd(idx);
        }

        public static int wl19Dati(int idx) {
            return wl19Status(idx) + Len.STATUS;
        }

        public static int wl19CodCompAnia(int idx) {
            return wl19Dati(idx);
        }

        public static int wl19CodFnd(int idx) {
            return wl19CodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wl19DtQtz(int idx) {
            return wl19CodFnd(idx) + Len.COD_FND;
        }

        public static int wl19ValQuo(int idx) {
            return wl19DtQtz(idx) + Len.DT_QTZ;
        }

        public static int wl19ValQuoNull(int idx) {
            return wl19ValQuo(idx);
        }

        public static int wl19ValQuoManfee(int idx) {
            return wl19ValQuo(idx) + Len.VAL_QUO;
        }

        public static int wl19ValQuoManfeeNull(int idx) {
            return wl19ValQuoManfee(idx);
        }

        public static int wl19DsOperSql(int idx) {
            return wl19ValQuoManfee(idx) + Len.VAL_QUO_MANFEE;
        }

        public static int wl19DsVer(int idx) {
            return wl19DsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wl19DsTsCptz(int idx) {
            return wl19DsVer(idx) + Len.DS_VER;
        }

        public static int wl19DsUtente(int idx) {
            return wl19DsTsCptz(idx) + Len.DS_TS_CPTZ;
        }

        public static int wl19DsStatoElab(int idx) {
            return wl19DsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wl19TpFnd(int idx) {
            return wl19DsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wl19DtRilevazioneNav(int idx) {
            return wl19TpFnd(idx) + Len.TP_FND;
        }

        public static int wl19DtRilevazioneNavNull(int idx) {
            return wl19DtRilevazioneNav(idx);
        }

        public static int wl19ValQuoAcq(int idx) {
            return wl19DtRilevazioneNav(idx) + Len.DT_RILEVAZIONE_NAV;
        }

        public static int wl19ValQuoAcqNull(int idx) {
            return wl19ValQuoAcq(idx);
        }

        public static int wl19FlNoNav(int idx) {
            return wl19ValQuoAcq(idx) + Len.VAL_QUO_ACQ;
        }

        public static int wl19FlNoNavNull(int idx) {
            return wl19FlNoNav(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_FND = 12;
        public static final int DT_QTZ = 5;
        public static final int VAL_QUO = 7;
        public static final int VAL_QUO_MANFEE = 7;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int TP_FND = 1;
        public static final int DT_RILEVAZIONE_NAV = 5;
        public static final int VAL_QUO_ACQ = 7;
        public static final int FL_NO_NAV = 1;
        public static final int DATI = COD_COMP_ANIA + COD_FND + DT_QTZ + VAL_QUO + VAL_QUO_MANFEE + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB + TP_FND + DT_RILEVAZIONE_NAV + VAL_QUO_ACQ + FL_NO_NAV;
        public static final int TAB_FND = STATUS + DATI;
        public static final int FLR1 = 86;
        public static final int WL19_TABELLA = Wl19TabellaLlbs0230.TAB_FND_MAXOCCURS * TAB_FND;
        public static final int RESTO_TABELLA = 21414;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_QTZ = 8;
            public static final int VAL_QUO = 7;
            public static final int VAL_QUO_MANFEE = 7;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;
            public static final int DT_RILEVAZIONE_NAV = 8;
            public static final int VAL_QUO_ACQ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_QUO = 5;
            public static final int VAL_QUO_MANFEE = 5;
            public static final int VAL_QUO_ACQ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
