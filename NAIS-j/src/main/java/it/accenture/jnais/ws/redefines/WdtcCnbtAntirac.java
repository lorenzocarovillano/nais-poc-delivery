package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-CNBT-ANTIRAC<br>
 * Variable: WDTC-CNBT-ANTIRAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_CNBT_ANTIRAC;
    }

    public void setWdtcCnbtAntirac(AfDecimal wdtcCnbtAntirac) {
        writeDecimalAsPacked(Pos.WDTC_CNBT_ANTIRAC, wdtcCnbtAntirac.copy());
    }

    public void setWdtcCnbtAntiracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_CNBT_ANTIRAC, Pos.WDTC_CNBT_ANTIRAC);
    }

    /**Original name: WDTC-CNBT-ANTIRAC<br>*/
    public AfDecimal getWdtcCnbtAntirac() {
        return readPackedAsDecimal(Pos.WDTC_CNBT_ANTIRAC, Len.Int.WDTC_CNBT_ANTIRAC, Len.Fract.WDTC_CNBT_ANTIRAC);
    }

    public byte[] getWdtcCnbtAntiracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_CNBT_ANTIRAC, Pos.WDTC_CNBT_ANTIRAC);
        return buffer;
    }

    public void initWdtcCnbtAntiracSpaces() {
        fill(Pos.WDTC_CNBT_ANTIRAC, Len.WDTC_CNBT_ANTIRAC, Types.SPACE_CHAR);
    }

    public void setWdtcCnbtAntiracNull(String wdtcCnbtAntiracNull) {
        writeString(Pos.WDTC_CNBT_ANTIRAC_NULL, wdtcCnbtAntiracNull, Len.WDTC_CNBT_ANTIRAC_NULL);
    }

    /**Original name: WDTC-CNBT-ANTIRAC-NULL<br>*/
    public String getWdtcCnbtAntiracNull() {
        return readString(Pos.WDTC_CNBT_ANTIRAC_NULL, Len.WDTC_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_CNBT_ANTIRAC = 1;
        public static final int WDTC_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_CNBT_ANTIRAC = 8;
        public static final int WDTC_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
