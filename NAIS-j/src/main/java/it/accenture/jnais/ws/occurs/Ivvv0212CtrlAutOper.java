package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVV0212-CTRL-AUT-OPER<br>
 * Variables: IVVV0212-CTRL-AUT-OPER from copybook IVVV0212<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvv0212CtrlAutOper {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_MAXOCCURS = 20;
    //Original name: IVVV0212-COD-ERRORE
    private int codErrore = DefaultValues.BIN_INT_VAL;
    //Original name: IVVV0212-COD-LIV-AUT
    private int codLivAut = DefaultValues.INT_VAL;
    //Original name: IVVV0212-TP-MOT-DEROGA
    private String tpMotDeroga = DefaultValues.stringVal(Len.TP_MOT_DEROGA);
    //Original name: IVVV0212-MOD-VERIFICA
    private String modVerifica = DefaultValues.stringVal(Len.MOD_VERIFICA);
    //Original name: IVVV0212-CODICE-CONDIZIONE
    private String codiceCondizione = DefaultValues.stringVal(Len.CODICE_CONDIZIONE);
    //Original name: IVVV0212-PROGRESS-CONDITION
    private short progressCondition = DefaultValues.SHORT_VAL;
    //Original name: IVVV0212-RISULTATO-CONDIZIONE
    private char risultatoCondizione = DefaultValues.CHAR_VAL;
    //Original name: IVVV0212-ELE-PARAM-MAX
    private short eleParamMax = DefaultValues.SHORT_VAL;
    //Original name: IVVV0212-TAB-PARAM
    private Ivvv0212TabParam[] tabParam = new Ivvv0212TabParam[TAB_PARAM_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvv0212CtrlAutOper() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabParamIdx = 1; tabParamIdx <= TAB_PARAM_MAXOCCURS; tabParamIdx++) {
            tabParam[tabParamIdx - 1] = new Ivvv0212TabParam();
        }
    }

    public void setCtrlAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        codErrore = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        codLivAut = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_LIV_AUT, 0);
        position += Len.COD_LIV_AUT;
        tpMotDeroga = MarshalByte.readString(buffer, position, Len.TP_MOT_DEROGA);
        position += Len.TP_MOT_DEROGA;
        modVerifica = MarshalByte.readString(buffer, position, Len.MOD_VERIFICA);
        position += Len.MOD_VERIFICA;
        codiceCondizione = MarshalByte.readString(buffer, position, Len.CODICE_CONDIZIONE);
        position += Len.CODICE_CONDIZIONE;
        progressCondition = MarshalByte.readPackedAsShort(buffer, position, Len.Int.PROGRESS_CONDITION, 0);
        position += Len.PROGRESS_CONDITION;
        risultatoCondizione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        eleParamMax = MarshalByte.readPackedAsShort(buffer, position, Len.Int.ELE_PARAM_MAX, 0);
        position += Len.ELE_PARAM_MAX;
        for (int idx = 1; idx <= TAB_PARAM_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabParam[idx - 1].setTabParamBytes(buffer, position);
                position += Ivvv0212TabParam.Len.TAB_PARAM;
            }
            else {
                tabParam[idx - 1].initTabParamSpaces();
                position += Ivvv0212TabParam.Len.TAB_PARAM;
            }
        }
    }

    public byte[] getCtrlAutOperBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, codErrore);
        position += Types.INT_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, codLivAut, Len.Int.COD_LIV_AUT, 0);
        position += Len.COD_LIV_AUT;
        MarshalByte.writeString(buffer, position, tpMotDeroga, Len.TP_MOT_DEROGA);
        position += Len.TP_MOT_DEROGA;
        MarshalByte.writeString(buffer, position, modVerifica, Len.MOD_VERIFICA);
        position += Len.MOD_VERIFICA;
        MarshalByte.writeString(buffer, position, codiceCondizione, Len.CODICE_CONDIZIONE);
        position += Len.CODICE_CONDIZIONE;
        MarshalByte.writeShortAsPacked(buffer, position, progressCondition, Len.Int.PROGRESS_CONDITION, 0);
        position += Len.PROGRESS_CONDITION;
        MarshalByte.writeChar(buffer, position, risultatoCondizione);
        position += Types.CHAR_SIZE;
        MarshalByte.writeShortAsPacked(buffer, position, eleParamMax, Len.Int.ELE_PARAM_MAX, 0);
        position += Len.ELE_PARAM_MAX;
        for (int idx = 1; idx <= TAB_PARAM_MAXOCCURS; idx++) {
            tabParam[idx - 1].getTabParamBytes(buffer, position);
            position += Ivvv0212TabParam.Len.TAB_PARAM;
        }
        return buffer;
    }

    public void initCtrlAutOperSpaces() {
        codErrore = Types.INVALID_INT_VAL;
        codLivAut = Types.INVALID_INT_VAL;
        tpMotDeroga = "";
        modVerifica = "";
        codiceCondizione = "";
        progressCondition = Types.INVALID_SHORT_VAL;
        risultatoCondizione = Types.SPACE_CHAR;
        eleParamMax = Types.INVALID_SHORT_VAL;
        for (int idx = 1; idx <= TAB_PARAM_MAXOCCURS; idx++) {
            tabParam[idx - 1].initTabParamSpaces();
        }
    }

    public void setCodErrore(int codErrore) {
        this.codErrore = codErrore;
    }

    public int getCodErrore() {
        return this.codErrore;
    }

    public void setCodLivAut(int codLivAut) {
        this.codLivAut = codLivAut;
    }

    public int getCodLivAut() {
        return this.codLivAut;
    }

    public void setTpMotDeroga(String tpMotDeroga) {
        this.tpMotDeroga = Functions.subString(tpMotDeroga, Len.TP_MOT_DEROGA);
    }

    public String getTpMotDeroga() {
        return this.tpMotDeroga;
    }

    public void setModVerifica(String modVerifica) {
        this.modVerifica = Functions.subString(modVerifica, Len.MOD_VERIFICA);
    }

    public String getModVerifica() {
        return this.modVerifica;
    }

    public void setCodiceCondizione(String codiceCondizione) {
        this.codiceCondizione = Functions.subString(codiceCondizione, Len.CODICE_CONDIZIONE);
    }

    public String getCodiceCondizione() {
        return this.codiceCondizione;
    }

    public void setProgressCondition(short progressCondition) {
        this.progressCondition = progressCondition;
    }

    public short getProgressCondition() {
        return this.progressCondition;
    }

    public void setRisultatoCondizione(char risultatoCondizione) {
        this.risultatoCondizione = risultatoCondizione;
    }

    public char getRisultatoCondizione() {
        return this.risultatoCondizione;
    }

    public void setEleParamMax(short eleParamMax) {
        this.eleParamMax = eleParamMax;
    }

    public short getEleParamMax() {
        return this.eleParamMax;
    }

    public Ivvv0212TabParam getTabParam(int idx) {
        return tabParam[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ERRORE = 4;
        public static final int COD_LIV_AUT = 3;
        public static final int TP_MOT_DEROGA = 2;
        public static final int MOD_VERIFICA = 8;
        public static final int CODICE_CONDIZIONE = 50;
        public static final int PROGRESS_CONDITION = 2;
        public static final int RISULTATO_CONDIZIONE = 1;
        public static final int ELE_PARAM_MAX = 3;
        public static final int CTRL_AUT_OPER = COD_ERRORE + COD_LIV_AUT + TP_MOT_DEROGA + MOD_VERIFICA + CODICE_CONDIZIONE + PROGRESS_CONDITION + RISULTATO_CONDIZIONE + ELE_PARAM_MAX + Ivvv0212CtrlAutOper.TAB_PARAM_MAXOCCURS * Ivvv0212TabParam.Len.TAB_PARAM;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int COD_LIV_AUT = 5;
            public static final int PROGRESS_CONDITION = 3;
            public static final int ELE_PARAM_MAX = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
