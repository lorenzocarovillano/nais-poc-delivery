package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-INC-ULT-PRE<br>
 * Variable: WB03-DT-INC-ULT-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtIncUltPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtIncUltPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_INC_ULT_PRE;
    }

    public void setWb03DtIncUltPre(int wb03DtIncUltPre) {
        writeIntAsPacked(Pos.WB03_DT_INC_ULT_PRE, wb03DtIncUltPre, Len.Int.WB03_DT_INC_ULT_PRE);
    }

    public void setWb03DtIncUltPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_INC_ULT_PRE, Pos.WB03_DT_INC_ULT_PRE);
    }

    /**Original name: WB03-DT-INC-ULT-PRE<br>*/
    public int getWb03DtIncUltPre() {
        return readPackedAsInt(Pos.WB03_DT_INC_ULT_PRE, Len.Int.WB03_DT_INC_ULT_PRE);
    }

    public byte[] getWb03DtIncUltPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_INC_ULT_PRE, Pos.WB03_DT_INC_ULT_PRE);
        return buffer;
    }

    public void setWb03DtIncUltPreNull(String wb03DtIncUltPreNull) {
        writeString(Pos.WB03_DT_INC_ULT_PRE_NULL, wb03DtIncUltPreNull, Len.WB03_DT_INC_ULT_PRE_NULL);
    }

    /**Original name: WB03-DT-INC-ULT-PRE-NULL<br>*/
    public String getWb03DtIncUltPreNull() {
        return readString(Pos.WB03_DT_INC_ULT_PRE_NULL, Len.WB03_DT_INC_ULT_PRE_NULL);
    }

    public String getWb03DtIncUltPreNullFormatted() {
        return Functions.padBlanks(getWb03DtIncUltPreNull(), Len.WB03_DT_INC_ULT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_INC_ULT_PRE = 1;
        public static final int WB03_DT_INC_ULT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_INC_ULT_PRE = 5;
        public static final int WB03_DT_INC_ULT_PRE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_INC_ULT_PRE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
