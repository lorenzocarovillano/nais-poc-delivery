package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.PreDtConcsPrest;
import it.accenture.jnais.ws.redefines.PreDtDecorPrest;
import it.accenture.jnais.ws.redefines.PreDtRimb;
import it.accenture.jnais.ws.redefines.PreFrazPagIntr;
import it.accenture.jnais.ws.redefines.PreIdMoviChiu;
import it.accenture.jnais.ws.redefines.PreImpPrest;
import it.accenture.jnais.ws.redefines.PreImpPrestLiqto;
import it.accenture.jnais.ws.redefines.PreImpRimb;
import it.accenture.jnais.ws.redefines.PreIntrPrest;
import it.accenture.jnais.ws.redefines.PrePrestResEff;
import it.accenture.jnais.ws.redefines.PreRimbEff;
import it.accenture.jnais.ws.redefines.PreSdoIntr;
import it.accenture.jnais.ws.redefines.PreSpePrest;

/**Original name: PREST<br>
 * Variable: PREST from copybook IDBVPRE1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class PrestIdbspre0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: PRE-ID-PREST
    private int preIdPrest = DefaultValues.INT_VAL;
    //Original name: PRE-ID-OGG
    private int preIdOgg = DefaultValues.INT_VAL;
    //Original name: PRE-TP-OGG
    private String preTpOgg = DefaultValues.stringVal(Len.PRE_TP_OGG);
    //Original name: PRE-ID-MOVI-CRZ
    private int preIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: PRE-ID-MOVI-CHIU
    private PreIdMoviChiu preIdMoviChiu = new PreIdMoviChiu();
    //Original name: PRE-DT-INI-EFF
    private int preDtIniEff = DefaultValues.INT_VAL;
    //Original name: PRE-DT-END-EFF
    private int preDtEndEff = DefaultValues.INT_VAL;
    //Original name: PRE-COD-COMP-ANIA
    private int preCodCompAnia = DefaultValues.INT_VAL;
    //Original name: PRE-DT-CONCS-PREST
    private PreDtConcsPrest preDtConcsPrest = new PreDtConcsPrest();
    //Original name: PRE-DT-DECOR-PREST
    private PreDtDecorPrest preDtDecorPrest = new PreDtDecorPrest();
    //Original name: PRE-IMP-PREST
    private PreImpPrest preImpPrest = new PreImpPrest();
    //Original name: PRE-INTR-PREST
    private PreIntrPrest preIntrPrest = new PreIntrPrest();
    //Original name: PRE-TP-PREST
    private String preTpPrest = DefaultValues.stringVal(Len.PRE_TP_PREST);
    //Original name: PRE-FRAZ-PAG-INTR
    private PreFrazPagIntr preFrazPagIntr = new PreFrazPagIntr();
    //Original name: PRE-DT-RIMB
    private PreDtRimb preDtRimb = new PreDtRimb();
    //Original name: PRE-IMP-RIMB
    private PreImpRimb preImpRimb = new PreImpRimb();
    //Original name: PRE-COD-DVS
    private String preCodDvs = DefaultValues.stringVal(Len.PRE_COD_DVS);
    //Original name: PRE-DT-RICH-PREST
    private int preDtRichPrest = DefaultValues.INT_VAL;
    //Original name: PRE-MOD-INTR-PREST
    private String preModIntrPrest = DefaultValues.stringVal(Len.PRE_MOD_INTR_PREST);
    //Original name: PRE-SPE-PREST
    private PreSpePrest preSpePrest = new PreSpePrest();
    //Original name: PRE-IMP-PREST-LIQTO
    private PreImpPrestLiqto preImpPrestLiqto = new PreImpPrestLiqto();
    //Original name: PRE-SDO-INTR
    private PreSdoIntr preSdoIntr = new PreSdoIntr();
    //Original name: PRE-RIMB-EFF
    private PreRimbEff preRimbEff = new PreRimbEff();
    //Original name: PRE-PREST-RES-EFF
    private PrePrestResEff prePrestResEff = new PrePrestResEff();
    //Original name: PRE-DS-RIGA
    private long preDsRiga = DefaultValues.LONG_VAL;
    //Original name: PRE-DS-OPER-SQL
    private char preDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: PRE-DS-VER
    private int preDsVer = DefaultValues.INT_VAL;
    //Original name: PRE-DS-TS-INI-CPTZ
    private long preDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: PRE-DS-TS-END-CPTZ
    private long preDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: PRE-DS-UTENTE
    private String preDsUtente = DefaultValues.stringVal(Len.PRE_DS_UTENTE);
    //Original name: PRE-DS-STATO-ELAB
    private char preDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PREST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setPrestBytes(buf);
    }

    public void setPrestFormatted(String data) {
        byte[] buffer = new byte[Len.PREST];
        MarshalByte.writeString(buffer, 1, data, Len.PREST);
        setPrestBytes(buffer, 1);
    }

    public String getPrestFormatted() {
        return MarshalByteExt.bufferToStr(getPrestBytes());
    }

    public void setPrestBytes(byte[] buffer) {
        setPrestBytes(buffer, 1);
    }

    public byte[] getPrestBytes() {
        byte[] buffer = new byte[Len.PREST];
        return getPrestBytes(buffer, 1);
    }

    public void setPrestBytes(byte[] buffer, int offset) {
        int position = offset;
        preIdPrest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_ID_PREST, 0);
        position += Len.PRE_ID_PREST;
        preIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_ID_OGG, 0);
        position += Len.PRE_ID_OGG;
        preTpOgg = MarshalByte.readString(buffer, position, Len.PRE_TP_OGG);
        position += Len.PRE_TP_OGG;
        preIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_ID_MOVI_CRZ, 0);
        position += Len.PRE_ID_MOVI_CRZ;
        preIdMoviChiu.setPreIdMoviChiuFromBuffer(buffer, position);
        position += PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU;
        preDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_DT_INI_EFF, 0);
        position += Len.PRE_DT_INI_EFF;
        preDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_DT_END_EFF, 0);
        position += Len.PRE_DT_END_EFF;
        preCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_COD_COMP_ANIA, 0);
        position += Len.PRE_COD_COMP_ANIA;
        preDtConcsPrest.setPreDtConcsPrestFromBuffer(buffer, position);
        position += PreDtConcsPrest.Len.PRE_DT_CONCS_PREST;
        preDtDecorPrest.setPreDtDecorPrestFromBuffer(buffer, position);
        position += PreDtDecorPrest.Len.PRE_DT_DECOR_PREST;
        preImpPrest.setPreImpPrestFromBuffer(buffer, position);
        position += PreImpPrest.Len.PRE_IMP_PREST;
        preIntrPrest.setPreIntrPrestFromBuffer(buffer, position);
        position += PreIntrPrest.Len.PRE_INTR_PREST;
        preTpPrest = MarshalByte.readString(buffer, position, Len.PRE_TP_PREST);
        position += Len.PRE_TP_PREST;
        preFrazPagIntr.setPreFrazPagIntrFromBuffer(buffer, position);
        position += PreFrazPagIntr.Len.PRE_FRAZ_PAG_INTR;
        preDtRimb.setPreDtRimbFromBuffer(buffer, position);
        position += PreDtRimb.Len.PRE_DT_RIMB;
        preImpRimb.setPreImpRimbFromBuffer(buffer, position);
        position += PreImpRimb.Len.PRE_IMP_RIMB;
        preCodDvs = MarshalByte.readString(buffer, position, Len.PRE_COD_DVS);
        position += Len.PRE_COD_DVS;
        preDtRichPrest = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_DT_RICH_PREST, 0);
        position += Len.PRE_DT_RICH_PREST;
        preModIntrPrest = MarshalByte.readString(buffer, position, Len.PRE_MOD_INTR_PREST);
        position += Len.PRE_MOD_INTR_PREST;
        preSpePrest.setPreSpePrestFromBuffer(buffer, position);
        position += PreSpePrest.Len.PRE_SPE_PREST;
        preImpPrestLiqto.setPreImpPrestLiqtoFromBuffer(buffer, position);
        position += PreImpPrestLiqto.Len.PRE_IMP_PREST_LIQTO;
        preSdoIntr.setPreSdoIntrFromBuffer(buffer, position);
        position += PreSdoIntr.Len.PRE_SDO_INTR;
        preRimbEff.setPreRimbEffFromBuffer(buffer, position);
        position += PreRimbEff.Len.PRE_RIMB_EFF;
        prePrestResEff.setPrePrestResEffFromBuffer(buffer, position);
        position += PrePrestResEff.Len.PRE_PREST_RES_EFF;
        preDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PRE_DS_RIGA, 0);
        position += Len.PRE_DS_RIGA;
        preDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        preDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.PRE_DS_VER, 0);
        position += Len.PRE_DS_VER;
        preDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PRE_DS_TS_INI_CPTZ, 0);
        position += Len.PRE_DS_TS_INI_CPTZ;
        preDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.PRE_DS_TS_END_CPTZ, 0);
        position += Len.PRE_DS_TS_END_CPTZ;
        preDsUtente = MarshalByte.readString(buffer, position, Len.PRE_DS_UTENTE);
        position += Len.PRE_DS_UTENTE;
        preDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getPrestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, preIdPrest, Len.Int.PRE_ID_PREST, 0);
        position += Len.PRE_ID_PREST;
        MarshalByte.writeIntAsPacked(buffer, position, preIdOgg, Len.Int.PRE_ID_OGG, 0);
        position += Len.PRE_ID_OGG;
        MarshalByte.writeString(buffer, position, preTpOgg, Len.PRE_TP_OGG);
        position += Len.PRE_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, preIdMoviCrz, Len.Int.PRE_ID_MOVI_CRZ, 0);
        position += Len.PRE_ID_MOVI_CRZ;
        preIdMoviChiu.getPreIdMoviChiuAsBuffer(buffer, position);
        position += PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, preDtIniEff, Len.Int.PRE_DT_INI_EFF, 0);
        position += Len.PRE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, preDtEndEff, Len.Int.PRE_DT_END_EFF, 0);
        position += Len.PRE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, preCodCompAnia, Len.Int.PRE_COD_COMP_ANIA, 0);
        position += Len.PRE_COD_COMP_ANIA;
        preDtConcsPrest.getPreDtConcsPrestAsBuffer(buffer, position);
        position += PreDtConcsPrest.Len.PRE_DT_CONCS_PREST;
        preDtDecorPrest.getPreDtDecorPrestAsBuffer(buffer, position);
        position += PreDtDecorPrest.Len.PRE_DT_DECOR_PREST;
        preImpPrest.getPreImpPrestAsBuffer(buffer, position);
        position += PreImpPrest.Len.PRE_IMP_PREST;
        preIntrPrest.getPreIntrPrestAsBuffer(buffer, position);
        position += PreIntrPrest.Len.PRE_INTR_PREST;
        MarshalByte.writeString(buffer, position, preTpPrest, Len.PRE_TP_PREST);
        position += Len.PRE_TP_PREST;
        preFrazPagIntr.getPreFrazPagIntrAsBuffer(buffer, position);
        position += PreFrazPagIntr.Len.PRE_FRAZ_PAG_INTR;
        preDtRimb.getPreDtRimbAsBuffer(buffer, position);
        position += PreDtRimb.Len.PRE_DT_RIMB;
        preImpRimb.getPreImpRimbAsBuffer(buffer, position);
        position += PreImpRimb.Len.PRE_IMP_RIMB;
        MarshalByte.writeString(buffer, position, preCodDvs, Len.PRE_COD_DVS);
        position += Len.PRE_COD_DVS;
        MarshalByte.writeIntAsPacked(buffer, position, preDtRichPrest, Len.Int.PRE_DT_RICH_PREST, 0);
        position += Len.PRE_DT_RICH_PREST;
        MarshalByte.writeString(buffer, position, preModIntrPrest, Len.PRE_MOD_INTR_PREST);
        position += Len.PRE_MOD_INTR_PREST;
        preSpePrest.getPreSpePrestAsBuffer(buffer, position);
        position += PreSpePrest.Len.PRE_SPE_PREST;
        preImpPrestLiqto.getPreImpPrestLiqtoAsBuffer(buffer, position);
        position += PreImpPrestLiqto.Len.PRE_IMP_PREST_LIQTO;
        preSdoIntr.getPreSdoIntrAsBuffer(buffer, position);
        position += PreSdoIntr.Len.PRE_SDO_INTR;
        preRimbEff.getPreRimbEffAsBuffer(buffer, position);
        position += PreRimbEff.Len.PRE_RIMB_EFF;
        prePrestResEff.getPrePrestResEffAsBuffer(buffer, position);
        position += PrePrestResEff.Len.PRE_PREST_RES_EFF;
        MarshalByte.writeLongAsPacked(buffer, position, preDsRiga, Len.Int.PRE_DS_RIGA, 0);
        position += Len.PRE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, preDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, preDsVer, Len.Int.PRE_DS_VER, 0);
        position += Len.PRE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, preDsTsIniCptz, Len.Int.PRE_DS_TS_INI_CPTZ, 0);
        position += Len.PRE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, preDsTsEndCptz, Len.Int.PRE_DS_TS_END_CPTZ, 0);
        position += Len.PRE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, preDsUtente, Len.PRE_DS_UTENTE);
        position += Len.PRE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, preDsStatoElab);
        return buffer;
    }

    public void setPreIdPrest(int preIdPrest) {
        this.preIdPrest = preIdPrest;
    }

    public int getPreIdPrest() {
        return this.preIdPrest;
    }

    public void setPreIdOgg(int preIdOgg) {
        this.preIdOgg = preIdOgg;
    }

    public int getPreIdOgg() {
        return this.preIdOgg;
    }

    public void setPreTpOgg(String preTpOgg) {
        this.preTpOgg = Functions.subString(preTpOgg, Len.PRE_TP_OGG);
    }

    public String getPreTpOgg() {
        return this.preTpOgg;
    }

    public void setPreIdMoviCrz(int preIdMoviCrz) {
        this.preIdMoviCrz = preIdMoviCrz;
    }

    public int getPreIdMoviCrz() {
        return this.preIdMoviCrz;
    }

    public void setPreDtIniEff(int preDtIniEff) {
        this.preDtIniEff = preDtIniEff;
    }

    public int getPreDtIniEff() {
        return this.preDtIniEff;
    }

    public void setPreDtEndEff(int preDtEndEff) {
        this.preDtEndEff = preDtEndEff;
    }

    public int getPreDtEndEff() {
        return this.preDtEndEff;
    }

    public void setPreCodCompAnia(int preCodCompAnia) {
        this.preCodCompAnia = preCodCompAnia;
    }

    public int getPreCodCompAnia() {
        return this.preCodCompAnia;
    }

    public void setPreTpPrest(String preTpPrest) {
        this.preTpPrest = Functions.subString(preTpPrest, Len.PRE_TP_PREST);
    }

    public String getPreTpPrest() {
        return this.preTpPrest;
    }

    public String getPreTpPrestFormatted() {
        return Functions.padBlanks(getPreTpPrest(), Len.PRE_TP_PREST);
    }

    public void setPreCodDvs(String preCodDvs) {
        this.preCodDvs = Functions.subString(preCodDvs, Len.PRE_COD_DVS);
    }

    public String getPreCodDvs() {
        return this.preCodDvs;
    }

    public String getPreCodDvsFormatted() {
        return Functions.padBlanks(getPreCodDvs(), Len.PRE_COD_DVS);
    }

    public void setPreDtRichPrest(int preDtRichPrest) {
        this.preDtRichPrest = preDtRichPrest;
    }

    public int getPreDtRichPrest() {
        return this.preDtRichPrest;
    }

    public String getPreDtRichPrestFormatted() {
        return PicFormatter.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).format(getPreDtRichPrest()).toString();
    }

    public void setPreModIntrPrest(String preModIntrPrest) {
        this.preModIntrPrest = Functions.subString(preModIntrPrest, Len.PRE_MOD_INTR_PREST);
    }

    public String getPreModIntrPrest() {
        return this.preModIntrPrest;
    }

    public String getPreModIntrPrestFormatted() {
        return Functions.padBlanks(getPreModIntrPrest(), Len.PRE_MOD_INTR_PREST);
    }

    public void setPreDsRiga(long preDsRiga) {
        this.preDsRiga = preDsRiga;
    }

    public long getPreDsRiga() {
        return this.preDsRiga;
    }

    public void setPreDsOperSql(char preDsOperSql) {
        this.preDsOperSql = preDsOperSql;
    }

    public void setPreDsOperSqlFormatted(String preDsOperSql) {
        setPreDsOperSql(Functions.charAt(preDsOperSql, Types.CHAR_SIZE));
    }

    public char getPreDsOperSql() {
        return this.preDsOperSql;
    }

    public void setPreDsVer(int preDsVer) {
        this.preDsVer = preDsVer;
    }

    public int getPreDsVer() {
        return this.preDsVer;
    }

    public void setPreDsTsIniCptz(long preDsTsIniCptz) {
        this.preDsTsIniCptz = preDsTsIniCptz;
    }

    public long getPreDsTsIniCptz() {
        return this.preDsTsIniCptz;
    }

    public void setPreDsTsEndCptz(long preDsTsEndCptz) {
        this.preDsTsEndCptz = preDsTsEndCptz;
    }

    public long getPreDsTsEndCptz() {
        return this.preDsTsEndCptz;
    }

    public void setPreDsUtente(String preDsUtente) {
        this.preDsUtente = Functions.subString(preDsUtente, Len.PRE_DS_UTENTE);
    }

    public String getPreDsUtente() {
        return this.preDsUtente;
    }

    public void setPreDsStatoElab(char preDsStatoElab) {
        this.preDsStatoElab = preDsStatoElab;
    }

    public void setPreDsStatoElabFormatted(String preDsStatoElab) {
        setPreDsStatoElab(Functions.charAt(preDsStatoElab, Types.CHAR_SIZE));
    }

    public char getPreDsStatoElab() {
        return this.preDsStatoElab;
    }

    public PreDtConcsPrest getPreDtConcsPrest() {
        return preDtConcsPrest;
    }

    public PreDtDecorPrest getPreDtDecorPrest() {
        return preDtDecorPrest;
    }

    public PreDtRimb getPreDtRimb() {
        return preDtRimb;
    }

    public PreFrazPagIntr getPreFrazPagIntr() {
        return preFrazPagIntr;
    }

    public PreIdMoviChiu getPreIdMoviChiu() {
        return preIdMoviChiu;
    }

    public PreImpPrest getPreImpPrest() {
        return preImpPrest;
    }

    public PreImpPrestLiqto getPreImpPrestLiqto() {
        return preImpPrestLiqto;
    }

    public PreImpRimb getPreImpRimb() {
        return preImpRimb;
    }

    public PreIntrPrest getPreIntrPrest() {
        return preIntrPrest;
    }

    public PrePrestResEff getPrePrestResEff() {
        return prePrestResEff;
    }

    public PreRimbEff getPreRimbEff() {
        return preRimbEff;
    }

    public PreSdoIntr getPreSdoIntr() {
        return preSdoIntr;
    }

    public PreSpePrest getPreSpePrest() {
        return preSpePrest;
    }

    @Override
    public byte[] serialize() {
        return getPrestBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_ID_PREST = 5;
        public static final int PRE_ID_OGG = 5;
        public static final int PRE_TP_OGG = 2;
        public static final int PRE_ID_MOVI_CRZ = 5;
        public static final int PRE_DT_INI_EFF = 5;
        public static final int PRE_DT_END_EFF = 5;
        public static final int PRE_COD_COMP_ANIA = 3;
        public static final int PRE_TP_PREST = 2;
        public static final int PRE_COD_DVS = 20;
        public static final int PRE_DT_RICH_PREST = 5;
        public static final int PRE_MOD_INTR_PREST = 2;
        public static final int PRE_DS_RIGA = 6;
        public static final int PRE_DS_OPER_SQL = 1;
        public static final int PRE_DS_VER = 5;
        public static final int PRE_DS_TS_INI_CPTZ = 10;
        public static final int PRE_DS_TS_END_CPTZ = 10;
        public static final int PRE_DS_UTENTE = 20;
        public static final int PRE_DS_STATO_ELAB = 1;
        public static final int PREST = PRE_ID_PREST + PRE_ID_OGG + PRE_TP_OGG + PRE_ID_MOVI_CRZ + PreIdMoviChiu.Len.PRE_ID_MOVI_CHIU + PRE_DT_INI_EFF + PRE_DT_END_EFF + PRE_COD_COMP_ANIA + PreDtConcsPrest.Len.PRE_DT_CONCS_PREST + PreDtDecorPrest.Len.PRE_DT_DECOR_PREST + PreImpPrest.Len.PRE_IMP_PREST + PreIntrPrest.Len.PRE_INTR_PREST + PRE_TP_PREST + PreFrazPagIntr.Len.PRE_FRAZ_PAG_INTR + PreDtRimb.Len.PRE_DT_RIMB + PreImpRimb.Len.PRE_IMP_RIMB + PRE_COD_DVS + PRE_DT_RICH_PREST + PRE_MOD_INTR_PREST + PreSpePrest.Len.PRE_SPE_PREST + PreImpPrestLiqto.Len.PRE_IMP_PREST_LIQTO + PreSdoIntr.Len.PRE_SDO_INTR + PreRimbEff.Len.PRE_RIMB_EFF + PrePrestResEff.Len.PRE_PREST_RES_EFF + PRE_DS_RIGA + PRE_DS_OPER_SQL + PRE_DS_VER + PRE_DS_TS_INI_CPTZ + PRE_DS_TS_END_CPTZ + PRE_DS_UTENTE + PRE_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_ID_PREST = 9;
            public static final int PRE_ID_OGG = 9;
            public static final int PRE_ID_MOVI_CRZ = 9;
            public static final int PRE_DT_INI_EFF = 8;
            public static final int PRE_DT_END_EFF = 8;
            public static final int PRE_COD_COMP_ANIA = 5;
            public static final int PRE_DT_RICH_PREST = 8;
            public static final int PRE_DS_RIGA = 10;
            public static final int PRE_DS_VER = 9;
            public static final int PRE_DS_TS_INI_CPTZ = 18;
            public static final int PRE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
