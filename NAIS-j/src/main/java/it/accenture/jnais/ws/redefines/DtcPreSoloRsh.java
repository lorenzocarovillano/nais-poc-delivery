package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PRE-SOLO-RSH<br>
 * Variable: DTC-PRE-SOLO-RSH from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PRE_SOLO_RSH;
    }

    public void setDtcPreSoloRsh(AfDecimal dtcPreSoloRsh) {
        writeDecimalAsPacked(Pos.DTC_PRE_SOLO_RSH, dtcPreSoloRsh.copy());
    }

    public void setDtcPreSoloRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PRE_SOLO_RSH, Pos.DTC_PRE_SOLO_RSH);
    }

    /**Original name: DTC-PRE-SOLO-RSH<br>*/
    public AfDecimal getDtcPreSoloRsh() {
        return readPackedAsDecimal(Pos.DTC_PRE_SOLO_RSH, Len.Int.DTC_PRE_SOLO_RSH, Len.Fract.DTC_PRE_SOLO_RSH);
    }

    public byte[] getDtcPreSoloRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PRE_SOLO_RSH, Pos.DTC_PRE_SOLO_RSH);
        return buffer;
    }

    public void setDtcPreSoloRshNull(String dtcPreSoloRshNull) {
        writeString(Pos.DTC_PRE_SOLO_RSH_NULL, dtcPreSoloRshNull, Len.DTC_PRE_SOLO_RSH_NULL);
    }

    /**Original name: DTC-PRE-SOLO-RSH-NULL<br>*/
    public String getDtcPreSoloRshNull() {
        return readString(Pos.DTC_PRE_SOLO_RSH_NULL, Len.DTC_PRE_SOLO_RSH_NULL);
    }

    public String getDtcPreSoloRshNullFormatted() {
        return Functions.padBlanks(getDtcPreSoloRshNull(), Len.DTC_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PRE_SOLO_RSH = 1;
        public static final int DTC_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PRE_SOLO_RSH = 8;
        public static final int DTC_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
