package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-MOVI-ORIG<br>
 * Variable: WS-MOVI-ORIG from program LVVS2800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMoviOrig {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_MOVI_ORIG);
    public static final String RISTOT_IND = "05010";
    public static final String RISPAR_POLIND = "05006";
    public static final String SCAPOL = "05026";
    public static final String SININD = "05018";
    public static final String RECIND = "05002";
    public static final String TAKE_PROFIT = "02319";
    public static final String RISTOT_INCAPIENZA = "02325";
    public static final String RPP_REDDITO_PROGR = "02317";
    public static final String RPP_BENEFICIO_CONTR = "02323";
    public static final String RISTOT_INC_DA_RP = "02333";

    //==== METHODS ====
    public void setWsMoviOrigFormatted(String wsMoviOrig) {
        this.value = Trunc.toUnsignedNumeric(wsMoviOrig, Len.WS_MOVI_ORIG);
    }

    public int getWsMoviOrig() {
        return NumericDisplay.asInt(this.value);
    }

    public String getWsMoviOrigFormatted() {
        return this.value;
    }

    public boolean isRistotInd() {
        return getWsMoviOrigFormatted().equals(RISTOT_IND);
    }

    public boolean isRisparPolind() {
        return getWsMoviOrigFormatted().equals(RISPAR_POLIND);
    }

    public boolean isScapol() {
        return getWsMoviOrigFormatted().equals(SCAPOL);
    }

    public boolean isSinind() {
        return getWsMoviOrigFormatted().equals(SININD);
    }

    public boolean isRecind() {
        return getWsMoviOrigFormatted().equals(RECIND);
    }

    public boolean isTakeProfit() {
        return getWsMoviOrigFormatted().equals(TAKE_PROFIT);
    }

    public boolean isRistotIncapienza() {
        return getWsMoviOrigFormatted().equals(RISTOT_INCAPIENZA);
    }

    public boolean isRppRedditoProgr() {
        return getWsMoviOrigFormatted().equals(RPP_REDDITO_PROGR);
    }

    public boolean isRppBeneficioContr() {
        return getWsMoviOrigFormatted().equals(RPP_BENEFICIO_CONTR);
    }

    public boolean isRistotIncDaRp() {
        return getWsMoviOrigFormatted().equals(RISTOT_INC_DA_RP);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVI_ORIG = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
