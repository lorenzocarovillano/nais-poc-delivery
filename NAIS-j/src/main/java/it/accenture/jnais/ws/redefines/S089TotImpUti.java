package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-UTI<br>
 * Variable: S089-TOT-IMP-UTI from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_UTI;
    }

    public void setWlquTotImpUti(AfDecimal wlquTotImpUti) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_UTI, wlquTotImpUti.copy());
    }

    public void setWlquTotImpUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_UTI, Pos.S089_TOT_IMP_UTI);
    }

    /**Original name: WLQU-TOT-IMP-UTI<br>*/
    public AfDecimal getWlquTotImpUti() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_UTI, Len.Int.WLQU_TOT_IMP_UTI, Len.Fract.WLQU_TOT_IMP_UTI);
    }

    public byte[] getWlquTotImpUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_UTI, Pos.S089_TOT_IMP_UTI);
        return buffer;
    }

    public void initWlquTotImpUtiSpaces() {
        fill(Pos.S089_TOT_IMP_UTI, Len.S089_TOT_IMP_UTI, Types.SPACE_CHAR);
    }

    public void setWlquTotImpUtiNull(String wlquTotImpUtiNull) {
        writeString(Pos.S089_TOT_IMP_UTI_NULL, wlquTotImpUtiNull, Len.WLQU_TOT_IMP_UTI_NULL);
    }

    /**Original name: WLQU-TOT-IMP-UTI-NULL<br>*/
    public String getWlquTotImpUtiNull() {
        return readString(Pos.S089_TOT_IMP_UTI_NULL, Len.WLQU_TOT_IMP_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_UTI = 1;
        public static final int S089_TOT_IMP_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_UTI = 8;
        public static final int WLQU_TOT_IMP_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
