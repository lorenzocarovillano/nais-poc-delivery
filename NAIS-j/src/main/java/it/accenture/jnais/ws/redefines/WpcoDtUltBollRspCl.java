package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-RSP-CL<br>
 * Variable: WPCO-DT-ULT-BOLL-RSP-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollRspCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollRspCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_RSP_CL;
    }

    public void setWpcoDtUltBollRspCl(int wpcoDtUltBollRspCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_RSP_CL, wpcoDtUltBollRspCl, Len.Int.WPCO_DT_ULT_BOLL_RSP_CL);
    }

    public void setDpcoDtUltBollRspClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RSP_CL, Pos.WPCO_DT_ULT_BOLL_RSP_CL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RSP-CL<br>*/
    public int getWpcoDtUltBollRspCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_RSP_CL, Len.Int.WPCO_DT_ULT_BOLL_RSP_CL);
    }

    public byte[] getWpcoDtUltBollRspClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RSP_CL, Pos.WPCO_DT_ULT_BOLL_RSP_CL);
        return buffer;
    }

    public void setWpcoDtUltBollRspClNull(String wpcoDtUltBollRspClNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_RSP_CL_NULL, wpcoDtUltBollRspClNull, Len.WPCO_DT_ULT_BOLL_RSP_CL_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RSP-CL-NULL<br>*/
    public String getWpcoDtUltBollRspClNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_RSP_CL_NULL, Len.WPCO_DT_ULT_BOLL_RSP_CL_NULL);
    }

    public String getWpcoDtUltBollRspClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollRspClNull(), Len.WPCO_DT_ULT_BOLL_RSP_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RSP_CL = 1;
        public static final int WPCO_DT_ULT_BOLL_RSP_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RSP_CL = 5;
        public static final int WPCO_DT_ULT_BOLL_RSP_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_RSP_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
