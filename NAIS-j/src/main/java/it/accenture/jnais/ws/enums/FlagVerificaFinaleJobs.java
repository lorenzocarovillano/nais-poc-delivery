package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-VERIFICA-FINALE-JOBS<br>
 * Variable: FLAG-VERIFICA-FINALE-JOBS from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagVerificaFinaleJobs {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagVerificaFinaleJobs(char flagVerificaFinaleJobs) {
        this.value = flagVerificaFinaleJobs;
    }

    public char getFlagVerificaFinaleJobs() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
