package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-PC-REINVST-RILIEVI<br>
 * Variable: OCO-PC-REINVST-RILIEVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoPcReinvstRilievi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoPcReinvstRilievi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_PC_REINVST_RILIEVI;
    }

    public void setOcoPcReinvstRilievi(AfDecimal ocoPcReinvstRilievi) {
        writeDecimalAsPacked(Pos.OCO_PC_REINVST_RILIEVI, ocoPcReinvstRilievi.copy());
    }

    public void setOcoPcReinvstRilieviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_PC_REINVST_RILIEVI, Pos.OCO_PC_REINVST_RILIEVI);
    }

    /**Original name: OCO-PC-REINVST-RILIEVI<br>*/
    public AfDecimal getOcoPcReinvstRilievi() {
        return readPackedAsDecimal(Pos.OCO_PC_REINVST_RILIEVI, Len.Int.OCO_PC_REINVST_RILIEVI, Len.Fract.OCO_PC_REINVST_RILIEVI);
    }

    public byte[] getOcoPcReinvstRilieviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_PC_REINVST_RILIEVI, Pos.OCO_PC_REINVST_RILIEVI);
        return buffer;
    }

    public void setOcoPcReinvstRilieviNull(String ocoPcReinvstRilieviNull) {
        writeString(Pos.OCO_PC_REINVST_RILIEVI_NULL, ocoPcReinvstRilieviNull, Len.OCO_PC_REINVST_RILIEVI_NULL);
    }

    /**Original name: OCO-PC-REINVST-RILIEVI-NULL<br>*/
    public String getOcoPcReinvstRilieviNull() {
        return readString(Pos.OCO_PC_REINVST_RILIEVI_NULL, Len.OCO_PC_REINVST_RILIEVI_NULL);
    }

    public String getOcoPcReinvstRilieviNullFormatted() {
        return Functions.padBlanks(getOcoPcReinvstRilieviNull(), Len.OCO_PC_REINVST_RILIEVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_PC_REINVST_RILIEVI = 1;
        public static final int OCO_PC_REINVST_RILIEVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_PC_REINVST_RILIEVI = 4;
        public static final int OCO_PC_REINVST_RILIEVI_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_PC_REINVST_RILIEVI = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_PC_REINVST_RILIEVI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
