package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvmov1;
import it.accenture.jnais.copy.Lccvpol1;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.WadeTabAdes;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;

/**Original name: AREA-MAIN<br>
 * Variable: AREA-MAIN from program LOAM0170<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaMainLoam0170 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int WPMO_TAB_PARAM_MOV_MAXOCCURS = 50;
    public static final int WADE_TAB_ADE_MAXOCCURS = 1;
    //Original name: WPMO-ELE-PMO-MAX
    private short wpmoElePmoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] wpmoTabParamMov = new WpmoTabParamMov[WPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: WPOL-ELE-POL-MAX
    private short wpolElePolMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1 lccvpol1 = new Lccvpol1();
    //Original name: WADE-ELE-ADE-MAX
    private short wadeEleAdeMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WADE-TAB-ADE
    private WadeTabAdes[] wadeTabAde = new WadeTabAdes[WADE_TAB_ADE_MAXOCCURS];
    //Original name: WMOV-ELE-MOVI-MAX
    private short wmovEleMoviMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVMOV1
    private Lccvmov1 lccvmov1 = new Lccvmov1();

    //==== CONSTRUCTORS ====
    public AreaMainLoam0170() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_MAIN;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaMainBytes(buf);
    }

    public void init() {
        for (int wpmoTabParamMovIdx = 1; wpmoTabParamMovIdx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; wpmoTabParamMovIdx++) {
            wpmoTabParamMov[wpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
        for (int wadeTabAdeIdx = 1; wadeTabAdeIdx <= WADE_TAB_ADE_MAXOCCURS; wadeTabAdeIdx++) {
            wadeTabAde[wadeTabAdeIdx - 1] = new WadeTabAdes();
        }
    }

    public String getAreaMainFormatted() {
        return MarshalByteExt.bufferToStr(getAreaMainBytes());
    }

    public void setAreaMainBytes(byte[] buffer) {
        setAreaMainBytes(buffer, 1);
    }

    public byte[] getAreaMainBytes() {
        byte[] buffer = new byte[Len.AREA_MAIN];
        return getAreaMainBytes(buffer, 1);
    }

    public void setAreaMainBytes(byte[] buffer, int offset) {
        int position = offset;
        setWpmoAreaParamMoviBytes(buffer, position);
        position += Len.WPMO_AREA_PARAM_MOVI;
        setWpolAreaPolizzaBytes(buffer, position);
        position += Len.WPOL_AREA_POLIZZA;
        setWadeAreaAdesioneBytes(buffer, position);
        position += Len.WADE_AREA_ADESIONE;
        setWmovAreaMovimentoBytes(buffer, position);
    }

    public byte[] getAreaMainBytes(byte[] buffer, int offset) {
        int position = offset;
        getWpmoAreaParamMoviBytes(buffer, position);
        position += Len.WPMO_AREA_PARAM_MOVI;
        getWpolAreaPolizzaBytes(buffer, position);
        position += Len.WPOL_AREA_POLIZZA;
        getWadeAreaAdesioneBytes(buffer, position);
        position += Len.WADE_AREA_ADESIONE;
        getWmovAreaMovimentoBytes(buffer, position);
        return buffer;
    }

    public void setWpmoAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        wpmoElePmoMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpmoTabParamMov[idx - 1].setWpmoTabParamMovBytes(buffer, position);
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
            else {
                wpmoTabParamMov[idx - 1].initWpmoTabParamMovSpaces();
                position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
            }
        }
    }

    public byte[] getWpmoAreaParamMoviBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpmoElePmoMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPMO_TAB_PARAM_MOV_MAXOCCURS; idx++) {
            wpmoTabParamMov[idx - 1].getWpmoTabParamMovBytes(buffer, position);
            position += WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        }
        return buffer;
    }

    public void setWpmoElePmoMax(short wpmoElePmoMax) {
        this.wpmoElePmoMax = wpmoElePmoMax;
    }

    public short getWpmoElePmoMax() {
        return this.wpmoElePmoMax;
    }

    public void setWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        wpolElePolMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWpolTabPolBytes(buffer, position);
    }

    public byte[] getWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wpolElePolMax);
        position += Types.SHORT_SIZE;
        getWpolTabPolBytes(buffer, position);
        return buffer;
    }

    public void setWpolElePolMax(short wpolElePolMax) {
        this.wpolElePolMax = wpolElePolMax;
    }

    public short getWpolElePolMax() {
        return this.wpolElePolMax;
    }

    public void setWpolTabPolBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1.Len.Int.ID_PTF, 0));
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpolTabPolBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpol1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpol1.getIdPtf(), Lccvpol1.Len.Int.ID_PTF, 0);
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wadeEleAdeMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WADE_TAB_ADE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wadeTabAde[idx - 1].setWadeTabAdesBytes(buffer, position);
                position += WadeTabAdes.Len.WADE_TAB_ADES;
            }
            else {
                wadeTabAde[idx - 1].initWadeTabAdesSpaces();
                position += WadeTabAdes.Len.WADE_TAB_ADES;
            }
        }
    }

    public byte[] getWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wadeEleAdeMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WADE_TAB_ADE_MAXOCCURS; idx++) {
            wadeTabAde[idx - 1].getWadeTabAdesBytes(buffer, position);
            position += WadeTabAdes.Len.WADE_TAB_ADES;
        }
        return buffer;
    }

    public void setWadeEleAdeMax(short wadeEleAdeMax) {
        this.wadeEleAdeMax = wadeEleAdeMax;
    }

    public short getWadeEleAdeMax() {
        return this.wadeEleAdeMax;
    }

    public void setWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        wmovEleMoviMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWmovTabMovBytes(buffer, position);
    }

    public byte[] getWmovAreaMovimentoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wmovEleMoviMax);
        position += Types.SHORT_SIZE;
        getWmovTabMovBytes(buffer, position);
        return buffer;
    }

    public void setWmovEleMoviMax(short wmovEleMoviMax) {
        this.wmovEleMoviMax = wmovEleMoviMax;
    }

    public short getWmovEleMoviMax() {
        return this.wmovEleMoviMax;
    }

    public void setWmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmov1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmov1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmov1.Len.Int.ID_PTF, 0));
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWmovTabMovBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvmov1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvmov1.getIdPtf(), Lccvmov1.Len.Int.ID_PTF, 0);
        position += Lccvmov1.Len.ID_PTF;
        lccvmov1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvmov1 getLccvmov1() {
        return lccvmov1;
    }

    public WpmoTabParamMov getWpmoTabParamMov(int idx) {
        return wpmoTabParamMov[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaMainBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ELE_PMO_MAX = 2;
        public static final int WPMO_AREA_PARAM_MOVI = WPMO_ELE_PMO_MAX + AreaMainLoam0170.WPMO_TAB_PARAM_MOV_MAXOCCURS * WpmoTabParamMov.Len.WPMO_TAB_PARAM_MOV;
        public static final int WPOL_ELE_POL_MAX = 2;
        public static final int WPOL_TAB_POL = WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int WPOL_AREA_POLIZZA = WPOL_ELE_POL_MAX + WPOL_TAB_POL;
        public static final int WADE_ELE_ADE_MAX = 2;
        public static final int WADE_AREA_ADESIONE = WADE_ELE_ADE_MAX + AreaMainLoam0170.WADE_TAB_ADE_MAXOCCURS * WadeTabAdes.Len.WADE_TAB_ADES;
        public static final int WMOV_ELE_MOVI_MAX = 2;
        public static final int WMOV_TAB_MOV = WpolStatus.Len.STATUS + Lccvmov1.Len.ID_PTF + WmovDati.Len.DATI;
        public static final int WMOV_AREA_MOVIMENTO = WMOV_ELE_MOVI_MAX + WMOV_TAB_MOV;
        public static final int AREA_MAIN = WPMO_AREA_PARAM_MOVI + WPOL_AREA_POLIZZA + WADE_AREA_ADESIONE + WMOV_AREA_MOVIMENTO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
