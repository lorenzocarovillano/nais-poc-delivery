package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-RSH-DFLT<br>
 * Variable: RST-RIS-RSH-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisRshDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisRshDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_RSH_DFLT;
    }

    public void setRstRisRshDflt(AfDecimal rstRisRshDflt) {
        writeDecimalAsPacked(Pos.RST_RIS_RSH_DFLT, rstRisRshDflt.copy());
    }

    public void setRstRisRshDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_RSH_DFLT, Pos.RST_RIS_RSH_DFLT);
    }

    /**Original name: RST-RIS-RSH-DFLT<br>*/
    public AfDecimal getRstRisRshDflt() {
        return readPackedAsDecimal(Pos.RST_RIS_RSH_DFLT, Len.Int.RST_RIS_RSH_DFLT, Len.Fract.RST_RIS_RSH_DFLT);
    }

    public byte[] getRstRisRshDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_RSH_DFLT, Pos.RST_RIS_RSH_DFLT);
        return buffer;
    }

    public void setRstRisRshDfltNull(String rstRisRshDfltNull) {
        writeString(Pos.RST_RIS_RSH_DFLT_NULL, rstRisRshDfltNull, Len.RST_RIS_RSH_DFLT_NULL);
    }

    /**Original name: RST-RIS-RSH-DFLT-NULL<br>*/
    public String getRstRisRshDfltNull() {
        return readString(Pos.RST_RIS_RSH_DFLT_NULL, Len.RST_RIS_RSH_DFLT_NULL);
    }

    public String getRstRisRshDfltNullFormatted() {
        return Functions.padBlanks(getRstRisRshDfltNull(), Len.RST_RIS_RSH_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_RSH_DFLT = 1;
        public static final int RST_RIS_RSH_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_RSH_DFLT = 8;
        public static final int RST_RIS_RSH_DFLT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_RSH_DFLT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_RSH_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
