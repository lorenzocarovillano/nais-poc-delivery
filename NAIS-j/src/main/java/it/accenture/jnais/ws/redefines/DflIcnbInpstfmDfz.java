package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ICNB-INPSTFM-DFZ<br>
 * Variable: DFL-ICNB-INPSTFM-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIcnbInpstfmDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIcnbInpstfmDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ICNB_INPSTFM_DFZ;
    }

    public void setDflIcnbInpstfmDfz(AfDecimal dflIcnbInpstfmDfz) {
        writeDecimalAsPacked(Pos.DFL_ICNB_INPSTFM_DFZ, dflIcnbInpstfmDfz.copy());
    }

    public void setDflIcnbInpstfmDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ICNB_INPSTFM_DFZ, Pos.DFL_ICNB_INPSTFM_DFZ);
    }

    /**Original name: DFL-ICNB-INPSTFM-DFZ<br>*/
    public AfDecimal getDflIcnbInpstfmDfz() {
        return readPackedAsDecimal(Pos.DFL_ICNB_INPSTFM_DFZ, Len.Int.DFL_ICNB_INPSTFM_DFZ, Len.Fract.DFL_ICNB_INPSTFM_DFZ);
    }

    public byte[] getDflIcnbInpstfmDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ICNB_INPSTFM_DFZ, Pos.DFL_ICNB_INPSTFM_DFZ);
        return buffer;
    }

    public void setDflIcnbInpstfmDfzNull(String dflIcnbInpstfmDfzNull) {
        writeString(Pos.DFL_ICNB_INPSTFM_DFZ_NULL, dflIcnbInpstfmDfzNull, Len.DFL_ICNB_INPSTFM_DFZ_NULL);
    }

    /**Original name: DFL-ICNB-INPSTFM-DFZ-NULL<br>*/
    public String getDflIcnbInpstfmDfzNull() {
        return readString(Pos.DFL_ICNB_INPSTFM_DFZ_NULL, Len.DFL_ICNB_INPSTFM_DFZ_NULL);
    }

    public String getDflIcnbInpstfmDfzNullFormatted() {
        return Functions.padBlanks(getDflIcnbInpstfmDfzNull(), Len.DFL_ICNB_INPSTFM_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ICNB_INPSTFM_DFZ = 1;
        public static final int DFL_ICNB_INPSTFM_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ICNB_INPSTFM_DFZ = 8;
        public static final int DFL_ICNB_INPSTFM_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ICNB_INPSTFM_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ICNB_INPSTFM_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
