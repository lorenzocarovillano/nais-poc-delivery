package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-INVRIO-INI<br>
 * Variable: TGA-PRE-INVRIO-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreInvrioIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreInvrioIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_INVRIO_INI;
    }

    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        writeDecimalAsPacked(Pos.TGA_PRE_INVRIO_INI, tgaPreInvrioIni.copy());
    }

    public void setTgaPreInvrioIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_INVRIO_INI, Pos.TGA_PRE_INVRIO_INI);
    }

    /**Original name: TGA-PRE-INVRIO-INI<br>*/
    public AfDecimal getTgaPreInvrioIni() {
        return readPackedAsDecimal(Pos.TGA_PRE_INVRIO_INI, Len.Int.TGA_PRE_INVRIO_INI, Len.Fract.TGA_PRE_INVRIO_INI);
    }

    public byte[] getTgaPreInvrioIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_INVRIO_INI, Pos.TGA_PRE_INVRIO_INI);
        return buffer;
    }

    public void setTgaPreInvrioIniNull(String tgaPreInvrioIniNull) {
        writeString(Pos.TGA_PRE_INVRIO_INI_NULL, tgaPreInvrioIniNull, Len.TGA_PRE_INVRIO_INI_NULL);
    }

    /**Original name: TGA-PRE-INVRIO-INI-NULL<br>*/
    public String getTgaPreInvrioIniNull() {
        return readString(Pos.TGA_PRE_INVRIO_INI_NULL, Len.TGA_PRE_INVRIO_INI_NULL);
    }

    public String getTgaPreInvrioIniNullFormatted() {
        return Functions.padBlanks(getTgaPreInvrioIniNull(), Len.TGA_PRE_INVRIO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_INVRIO_INI = 1;
        public static final int TGA_PRE_INVRIO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_INVRIO_INI = 8;
        public static final int TGA_PRE_INVRIO_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_INVRIO_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_INVRIO_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
