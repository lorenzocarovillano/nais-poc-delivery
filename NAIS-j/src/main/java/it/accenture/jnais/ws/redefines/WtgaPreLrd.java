package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-LRD<br>
 * Variable: WTGA-PRE-LRD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_LRD;
    }

    public void setWtgaPreLrd(AfDecimal wtgaPreLrd) {
        writeDecimalAsPacked(Pos.WTGA_PRE_LRD, wtgaPreLrd.copy());
    }

    public void setWtgaPreLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_LRD, Pos.WTGA_PRE_LRD);
    }

    /**Original name: WTGA-PRE-LRD<br>*/
    public AfDecimal getWtgaPreLrd() {
        return readPackedAsDecimal(Pos.WTGA_PRE_LRD, Len.Int.WTGA_PRE_LRD, Len.Fract.WTGA_PRE_LRD);
    }

    public byte[] getWtgaPreLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_LRD, Pos.WTGA_PRE_LRD);
        return buffer;
    }

    public void initWtgaPreLrdSpaces() {
        fill(Pos.WTGA_PRE_LRD, Len.WTGA_PRE_LRD, Types.SPACE_CHAR);
    }

    public void setWtgaPreLrdNull(String wtgaPreLrdNull) {
        writeString(Pos.WTGA_PRE_LRD_NULL, wtgaPreLrdNull, Len.WTGA_PRE_LRD_NULL);
    }

    /**Original name: WTGA-PRE-LRD-NULL<br>*/
    public String getWtgaPreLrdNull() {
        return readString(Pos.WTGA_PRE_LRD_NULL, Len.WTGA_PRE_LRD_NULL);
    }

    public String getWtgaPreLrdNullFormatted() {
        return Functions.padBlanks(getWtgaPreLrdNull(), Len.WTGA_PRE_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_LRD = 1;
        public static final int WTGA_PRE_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_LRD = 8;
        public static final int WTGA_PRE_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
