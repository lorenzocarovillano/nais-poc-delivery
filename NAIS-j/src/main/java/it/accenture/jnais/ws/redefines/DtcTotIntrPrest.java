package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-TOT-INTR-PREST<br>
 * Variable: DTC-TOT-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_TOT_INTR_PREST;
    }

    public void setDtcTotIntrPrest(AfDecimal dtcTotIntrPrest) {
        writeDecimalAsPacked(Pos.DTC_TOT_INTR_PREST, dtcTotIntrPrest.copy());
    }

    public void setDtcTotIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_TOT_INTR_PREST, Pos.DTC_TOT_INTR_PREST);
    }

    /**Original name: DTC-TOT-INTR-PREST<br>*/
    public AfDecimal getDtcTotIntrPrest() {
        return readPackedAsDecimal(Pos.DTC_TOT_INTR_PREST, Len.Int.DTC_TOT_INTR_PREST, Len.Fract.DTC_TOT_INTR_PREST);
    }

    public byte[] getDtcTotIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_TOT_INTR_PREST, Pos.DTC_TOT_INTR_PREST);
        return buffer;
    }

    public void setDtcTotIntrPrestNull(String dtcTotIntrPrestNull) {
        writeString(Pos.DTC_TOT_INTR_PREST_NULL, dtcTotIntrPrestNull, Len.DTC_TOT_INTR_PREST_NULL);
    }

    /**Original name: DTC-TOT-INTR-PREST-NULL<br>*/
    public String getDtcTotIntrPrestNull() {
        return readString(Pos.DTC_TOT_INTR_PREST_NULL, Len.DTC_TOT_INTR_PREST_NULL);
    }

    public String getDtcTotIntrPrestNullFormatted() {
        return Functions.padBlanks(getDtcTotIntrPrestNull(), Len.DTC_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_TOT_INTR_PREST = 1;
        public static final int DTC_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_TOT_INTR_PREST = 8;
        public static final int DTC_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
