package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-REWRITE-FILESQS1<br>
 * Variable: FLAG-REWRITE-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRewriteFilesqs1 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRewriteFilesqs1(char flagRewriteFilesqs1) {
        this.value = flagRewriteFilesqs1;
    }

    public char getFlagRewriteFilesqs1() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setNo() {
        value = NO;
    }
}
