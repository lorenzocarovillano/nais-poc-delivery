package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-TFR-STRC<br>
 * Variable: TIT-IMP-TFR-STRC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_TFR_STRC;
    }

    public void setTitImpTfrStrc(AfDecimal titImpTfrStrc) {
        writeDecimalAsPacked(Pos.TIT_IMP_TFR_STRC, titImpTfrStrc.copy());
    }

    public void setTitImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_TFR_STRC, Pos.TIT_IMP_TFR_STRC);
    }

    /**Original name: TIT-IMP-TFR-STRC<br>*/
    public AfDecimal getTitImpTfrStrc() {
        return readPackedAsDecimal(Pos.TIT_IMP_TFR_STRC, Len.Int.TIT_IMP_TFR_STRC, Len.Fract.TIT_IMP_TFR_STRC);
    }

    public byte[] getTitImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_TFR_STRC, Pos.TIT_IMP_TFR_STRC);
        return buffer;
    }

    public void setTitImpTfrStrcNull(String titImpTfrStrcNull) {
        writeString(Pos.TIT_IMP_TFR_STRC_NULL, titImpTfrStrcNull, Len.TIT_IMP_TFR_STRC_NULL);
    }

    /**Original name: TIT-IMP-TFR-STRC-NULL<br>*/
    public String getTitImpTfrStrcNull() {
        return readString(Pos.TIT_IMP_TFR_STRC_NULL, Len.TIT_IMP_TFR_STRC_NULL);
    }

    public String getTitImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getTitImpTfrStrcNull(), Len.TIT_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_TFR_STRC = 1;
        public static final int TIT_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_TFR_STRC = 8;
        public static final int TIT_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
