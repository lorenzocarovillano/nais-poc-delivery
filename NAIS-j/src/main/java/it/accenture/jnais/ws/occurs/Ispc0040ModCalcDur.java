package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ispc0040CodiceMod;

/**Original name: ISPC0040-MOD-CALC-DUR<br>
 * Variables: ISPC0040-MOD-CALC-DUR from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040ModCalcDur {

    //==== PROPERTIES ====
    //Original name: ISPC0040-CODICE-MOD
    private Ispc0040CodiceMod codiceMod = new Ispc0040CodiceMod();
    //Original name: ISPC0040-DESCR-MOD
    private String descrMod = DefaultValues.stringVal(Len.DESCR_MOD);

    //==== METHODS ====
    public void setModCalcDurBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceMod.setCodiceMod(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        descrMod = MarshalByte.readString(buffer, position, Len.DESCR_MOD);
    }

    public byte[] getModCalcDurBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, codiceMod.getCodiceMod());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, descrMod, Len.DESCR_MOD);
        return buffer;
    }

    public void initModCalcDurSpaces() {
        codiceMod.setCodiceMod(Types.SPACE_CHAR);
        descrMod = "";
    }

    public void setDescrMod(String descrMod) {
        this.descrMod = Functions.subString(descrMod, Len.DESCR_MOD);
    }

    public String getDescrMod() {
        return this.descrMod;
    }

    public Ispc0040CodiceMod getCodiceMod() {
        return codiceMod;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCR_MOD = 20;
        public static final int MOD_CALC_DUR = Ispc0040CodiceMod.Len.CODICE_MOD + DESCR_MOD;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
