package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-DUR-MM-ADES-DFLT<br>
 * Variable: DCO-DUR-MM-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoDurMmAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoDurMmAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_DUR_MM_ADES_DFLT;
    }

    public void setDcoDurMmAdesDflt(int dcoDurMmAdesDflt) {
        writeIntAsPacked(Pos.DCO_DUR_MM_ADES_DFLT, dcoDurMmAdesDflt, Len.Int.DCO_DUR_MM_ADES_DFLT);
    }

    public void setDcoDurMmAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_DUR_MM_ADES_DFLT, Pos.DCO_DUR_MM_ADES_DFLT);
    }

    /**Original name: DCO-DUR-MM-ADES-DFLT<br>*/
    public int getDcoDurMmAdesDflt() {
        return readPackedAsInt(Pos.DCO_DUR_MM_ADES_DFLT, Len.Int.DCO_DUR_MM_ADES_DFLT);
    }

    public byte[] getDcoDurMmAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_DUR_MM_ADES_DFLT, Pos.DCO_DUR_MM_ADES_DFLT);
        return buffer;
    }

    public void setDcoDurMmAdesDfltNull(String dcoDurMmAdesDfltNull) {
        writeString(Pos.DCO_DUR_MM_ADES_DFLT_NULL, dcoDurMmAdesDfltNull, Len.DCO_DUR_MM_ADES_DFLT_NULL);
    }

    /**Original name: DCO-DUR-MM-ADES-DFLT-NULL<br>*/
    public String getDcoDurMmAdesDfltNull() {
        return readString(Pos.DCO_DUR_MM_ADES_DFLT_NULL, Len.DCO_DUR_MM_ADES_DFLT_NULL);
    }

    public String getDcoDurMmAdesDfltNullFormatted() {
        return Functions.padBlanks(getDcoDurMmAdesDfltNull(), Len.DCO_DUR_MM_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_DUR_MM_ADES_DFLT = 1;
        public static final int DCO_DUR_MM_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_DUR_MM_ADES_DFLT = 3;
        public static final int DCO_DUR_MM_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_DUR_MM_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
