package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PROV-DA-REC<br>
 * Variable: WTDR-TOT-PROV-DA-REC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PROV_DA_REC;
    }

    public void setWtdrTotProvDaRec(AfDecimal wtdrTotProvDaRec) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PROV_DA_REC, wtdrTotProvDaRec.copy());
    }

    /**Original name: WTDR-TOT-PROV-DA-REC<br>*/
    public AfDecimal getWtdrTotProvDaRec() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PROV_DA_REC, Len.Int.WTDR_TOT_PROV_DA_REC, Len.Fract.WTDR_TOT_PROV_DA_REC);
    }

    public void setWtdrTotProvDaRecNull(String wtdrTotProvDaRecNull) {
        writeString(Pos.WTDR_TOT_PROV_DA_REC_NULL, wtdrTotProvDaRecNull, Len.WTDR_TOT_PROV_DA_REC_NULL);
    }

    /**Original name: WTDR-TOT-PROV-DA-REC-NULL<br>*/
    public String getWtdrTotProvDaRecNull() {
        return readString(Pos.WTDR_TOT_PROV_DA_REC_NULL, Len.WTDR_TOT_PROV_DA_REC_NULL);
    }

    public String getWtdrTotProvDaRecNullFormatted() {
        return Functions.padBlanks(getWtdrTotProvDaRecNull(), Len.WTDR_TOT_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_DA_REC = 1;
        public static final int WTDR_TOT_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_DA_REC = 8;
        public static final int WTDR_TOT_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
