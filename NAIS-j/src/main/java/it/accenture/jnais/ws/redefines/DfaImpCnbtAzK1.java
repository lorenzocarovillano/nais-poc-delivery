package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-AZ-K1<br>
 * Variable: DFA-IMP-CNBT-AZ-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtAzK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtAzK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_AZ_K1;
    }

    public void setDfaImpCnbtAzK1(AfDecimal dfaImpCnbtAzK1) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_AZ_K1, dfaImpCnbtAzK1.copy());
    }

    public void setDfaImpCnbtAzK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_AZ_K1, Pos.DFA_IMP_CNBT_AZ_K1);
    }

    /**Original name: DFA-IMP-CNBT-AZ-K1<br>*/
    public AfDecimal getDfaImpCnbtAzK1() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_AZ_K1, Len.Int.DFA_IMP_CNBT_AZ_K1, Len.Fract.DFA_IMP_CNBT_AZ_K1);
    }

    public byte[] getDfaImpCnbtAzK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_AZ_K1, Pos.DFA_IMP_CNBT_AZ_K1);
        return buffer;
    }

    public void setDfaImpCnbtAzK1Null(String dfaImpCnbtAzK1Null) {
        writeString(Pos.DFA_IMP_CNBT_AZ_K1_NULL, dfaImpCnbtAzK1Null, Len.DFA_IMP_CNBT_AZ_K1_NULL);
    }

    /**Original name: DFA-IMP-CNBT-AZ-K1-NULL<br>*/
    public String getDfaImpCnbtAzK1Null() {
        return readString(Pos.DFA_IMP_CNBT_AZ_K1_NULL, Len.DFA_IMP_CNBT_AZ_K1_NULL);
    }

    public String getDfaImpCnbtAzK1NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtAzK1Null(), Len.DFA_IMP_CNBT_AZ_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_AZ_K1 = 1;
        public static final int DFA_IMP_CNBT_AZ_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_AZ_K1 = 8;
        public static final int DFA_IMP_CNBT_AZ_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_AZ_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_AZ_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
