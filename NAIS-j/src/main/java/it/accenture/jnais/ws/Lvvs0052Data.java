package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvade1Lvvs0010;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;
import it.accenture.jnais.ws.occurs.WgrzTabGar;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0052<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0052Data {

    //==== PROPERTIES ====
    public static final int DGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0052";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-TP-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkTpOgg = "";
    //Original name: LDBV1701
    private Ldbv1701 ldbv1701 = new Ldbv1701();
    //Original name: DPOL-ELE-POL-MAX
    private short dpolElePolMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> dtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DGRZ-TAB-GAR
    private WgrzTabGar[] dgrzTabGar = new WgrzTabGar[DGRZ_TAB_GAR_MAXOCCURS];
    //Original name: DADE-ELE-ADE-MAX
    private short dadeEleAdeMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1Lvvs0010 lccvade1 = new Lccvade1Lvvs0010();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0052Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dgrzTabGarIdx = 1; dgrzTabGarIdx <= DGRZ_TAB_GAR_MAXOCCURS; dgrzTabGarIdx++) {
            dgrzTabGar[dgrzTabGarIdx - 1] = new WgrzTabGar();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkTpOgg(String wkTpOgg) {
        this.wkTpOgg = Functions.subString(wkTpOgg, Len.WK_TP_OGG);
    }

    public String getWkTpOgg() {
        return this.wkTpOgg;
    }

    public void setDpolAreaPolFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POL];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POL);
        setDpolAreaPolBytes(buffer, 1);
    }

    public void setDpolAreaPolBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePolMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPolBytes(buffer, position);
    }

    public void setDpolElePolMax(short dpolElePolMax) {
        this.dpolElePolMax = dpolElePolMax;
    }

    public short getDpolElePolMax() {
        return this.dpolElePolMax;
    }

    public void setDpolTabPolBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_dtgaTabTran = new W1tgaTabTran();
                temp_dtgaTabTran.initW1tgaTabTranSpaces();
                getDtgaTabTranObj().fill(temp_dtgaTabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (DTGA_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDgrzAreaGraFormatted(String data) {
        byte[] buffer = new byte[Len.DGRZ_AREA_GRA];
        MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GRA);
        setDgrzAreaGraBytes(buffer, 1);
    }

    public void setDgrzAreaGraBytes(byte[] buffer, int offset) {
        int position = offset;
        dgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                dgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setDadeAreaAdesFormatted(String data) {
        byte[] buffer = new byte[Len.DADE_AREA_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.DADE_AREA_ADES);
        setDadeAreaAdesBytes(buffer, 1);
    }

    public void setDadeAreaAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dadeEleAdeMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDadeTabAdeBytes(buffer, position);
    }

    public void setDadeEleAdeMax(short dadeEleAdeMax) {
        this.dadeEleAdeMax = dadeEleAdeMax;
    }

    public short getDadeEleAdeMax() {
        return this.dadeEleAdeMax;
    }

    public void setDadeTabAdeBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1Lvvs0010.Len.Int.ID_PTF, 0));
        position += Lccvade1Lvvs0010.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public LazyArrayCopy<W1tgaTabTran> getDtgaTabTranObj() {
        return dtgaTabTran;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv1701 getLdbv1701() {
        return ldbv1701;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TP_OGG = 2;
        public static final int DPOL_ELE_POL_MAX = 2;
        public static final int DPOL_TAB_POL = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POL = DPOL_ELE_POL_MAX + DPOL_TAB_POL;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + Lvvs0052Data.DTGA_TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;
        public static final int DGRZ_ELE_GAR_MAX = 2;
        public static final int DGRZ_AREA_GRA = DGRZ_ELE_GAR_MAX + Lvvs0052Data.DGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int DADE_ELE_ADE_MAX = 2;
        public static final int DADE_TAB_ADE = WpolStatus.Len.STATUS + Lccvade1Lvvs0010.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int DADE_AREA_ADES = DADE_ELE_ADE_MAX + DADE_TAB_ADE;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
