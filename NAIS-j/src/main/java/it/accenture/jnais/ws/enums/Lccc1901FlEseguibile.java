package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCC1901-FL-ESEGUIBILE<br>
 * Variable: LCCC1901-FL-ESEGUIBILE from copybook LCCC1901<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc1901FlEseguibile {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlEseguibile(char flEseguibile) {
        this.value = flEseguibile;
    }

    public char getFlEseguibile() {
        return this.value;
    }

    public boolean isLccc1901FlEseguibileSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_ESEGUIBILE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
