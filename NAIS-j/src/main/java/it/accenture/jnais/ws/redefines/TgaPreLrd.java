package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-LRD<br>
 * Variable: TGA-PRE-LRD from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_LRD;
    }

    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        writeDecimalAsPacked(Pos.TGA_PRE_LRD, tgaPreLrd.copy());
    }

    public void setTgaPreLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_LRD, Pos.TGA_PRE_LRD);
    }

    /**Original name: TGA-PRE-LRD<br>*/
    public AfDecimal getTgaPreLrd() {
        return readPackedAsDecimal(Pos.TGA_PRE_LRD, Len.Int.TGA_PRE_LRD, Len.Fract.TGA_PRE_LRD);
    }

    public byte[] getTgaPreLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_LRD, Pos.TGA_PRE_LRD);
        return buffer;
    }

    public void setTgaPreLrdNull(String tgaPreLrdNull) {
        writeString(Pos.TGA_PRE_LRD_NULL, tgaPreLrdNull, Len.TGA_PRE_LRD_NULL);
    }

    /**Original name: TGA-PRE-LRD-NULL<br>*/
    public String getTgaPreLrdNull() {
        return readString(Pos.TGA_PRE_LRD_NULL, Len.TGA_PRE_LRD_NULL);
    }

    public String getTgaPreLrdNullFormatted() {
        return Functions.padBlanks(getTgaPreLrdNull(), Len.TGA_PRE_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_LRD = 1;
        public static final int TGA_PRE_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_LRD = 8;
        public static final int TGA_PRE_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
