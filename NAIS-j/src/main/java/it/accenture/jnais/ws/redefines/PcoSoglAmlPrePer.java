package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-SOGL-AML-PRE-PER<br>
 * Variable: PCO-SOGL-AML-PRE-PER from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoSoglAmlPrePer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoSoglAmlPrePer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_SOGL_AML_PRE_PER;
    }

    public void setPcoSoglAmlPrePer(AfDecimal pcoSoglAmlPrePer) {
        writeDecimalAsPacked(Pos.PCO_SOGL_AML_PRE_PER, pcoSoglAmlPrePer.copy());
    }

    public void setPcoSoglAmlPrePerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_SOGL_AML_PRE_PER, Pos.PCO_SOGL_AML_PRE_PER);
    }

    /**Original name: PCO-SOGL-AML-PRE-PER<br>*/
    public AfDecimal getPcoSoglAmlPrePer() {
        return readPackedAsDecimal(Pos.PCO_SOGL_AML_PRE_PER, Len.Int.PCO_SOGL_AML_PRE_PER, Len.Fract.PCO_SOGL_AML_PRE_PER);
    }

    public byte[] getPcoSoglAmlPrePerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_SOGL_AML_PRE_PER, Pos.PCO_SOGL_AML_PRE_PER);
        return buffer;
    }

    public void initPcoSoglAmlPrePerHighValues() {
        fill(Pos.PCO_SOGL_AML_PRE_PER, Len.PCO_SOGL_AML_PRE_PER, Types.HIGH_CHAR_VAL);
    }

    public void setPcoSoglAmlPrePerNull(String pcoSoglAmlPrePerNull) {
        writeString(Pos.PCO_SOGL_AML_PRE_PER_NULL, pcoSoglAmlPrePerNull, Len.PCO_SOGL_AML_PRE_PER_NULL);
    }

    /**Original name: PCO-SOGL-AML-PRE-PER-NULL<br>*/
    public String getPcoSoglAmlPrePerNull() {
        return readString(Pos.PCO_SOGL_AML_PRE_PER_NULL, Len.PCO_SOGL_AML_PRE_PER_NULL);
    }

    public String getPcoSoglAmlPrePerNullFormatted() {
        return Functions.padBlanks(getPcoSoglAmlPrePerNull(), Len.PCO_SOGL_AML_PRE_PER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_SOGL_AML_PRE_PER = 1;
        public static final int PCO_SOGL_AML_PRE_PER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_SOGL_AML_PRE_PER = 8;
        public static final int PCO_SOGL_AML_PRE_PER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_SOGL_AML_PRE_PER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_SOGL_AML_PRE_PER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
