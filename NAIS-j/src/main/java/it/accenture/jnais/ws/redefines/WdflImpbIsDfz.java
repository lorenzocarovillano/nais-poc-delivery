package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-DFZ<br>
 * Variable: WDFL-IMPB-IS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIsDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIsDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS_DFZ;
    }

    public void setWdflImpbIsDfz(AfDecimal wdflImpbIsDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS_DFZ, wdflImpbIsDfz.copy());
    }

    public void setWdflImpbIsDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS_DFZ, Pos.WDFL_IMPB_IS_DFZ);
    }

    /**Original name: WDFL-IMPB-IS-DFZ<br>*/
    public AfDecimal getWdflImpbIsDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS_DFZ, Len.Int.WDFL_IMPB_IS_DFZ, Len.Fract.WDFL_IMPB_IS_DFZ);
    }

    public byte[] getWdflImpbIsDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS_DFZ, Pos.WDFL_IMPB_IS_DFZ);
        return buffer;
    }

    public void setWdflImpbIsDfzNull(String wdflImpbIsDfzNull) {
        writeString(Pos.WDFL_IMPB_IS_DFZ_NULL, wdflImpbIsDfzNull, Len.WDFL_IMPB_IS_DFZ_NULL);
    }

    /**Original name: WDFL-IMPB-IS-DFZ-NULL<br>*/
    public String getWdflImpbIsDfzNull() {
        return readString(Pos.WDFL_IMPB_IS_DFZ_NULL, Len.WDFL_IMPB_IS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS_DFZ = 1;
        public static final int WDFL_IMPB_IS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS_DFZ = 8;
        public static final int WDFL_IMPB_IS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
