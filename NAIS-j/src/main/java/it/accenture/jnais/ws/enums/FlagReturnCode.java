package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FLAG-RETURN-CODE<br>
 * Variable: FLAG-RETURN-CODE from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagReturnCode {

    //==== PROPERTIES ====
    private String value = "00";
    public static final String RC00 = "00";
    public static final String RC04 = "04";
    public static final String RC08 = "08";
    public static final String RC12 = "12";

    //==== METHODS ====
    public void setFlagReturnCode(short flagReturnCode) {
        this.value = NumericDisplay.asString(flagReturnCode, Len.FLAG_RETURN_CODE);
    }

    public void setFlagReturnCodeFormatted(String flagReturnCode) {
        this.value = Trunc.toUnsignedNumeric(flagReturnCode, Len.FLAG_RETURN_CODE);
    }

    public short getFlagReturnCode() {
        return NumericDisplay.asShort(this.value);
    }

    public String getFlagReturnCodeFormatted() {
        return this.value;
    }

    public void setRc00() {
        setFlagReturnCodeFormatted(RC00);
    }

    public boolean isRc04() {
        return getFlagReturnCodeFormatted().equals(RC04);
    }

    public void setRc04() {
        setFlagReturnCodeFormatted(RC04);
    }

    public boolean isRc08() {
        return getFlagReturnCodeFormatted().equals(RC08);
    }

    public void setRc08() {
        setFlagReturnCodeFormatted(RC08);
    }

    public void setRc12() {
        setFlagReturnCodeFormatted(RC12);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
