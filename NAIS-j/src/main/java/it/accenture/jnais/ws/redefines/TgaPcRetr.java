package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PC-RETR<br>
 * Variable: TGA-PC-RETR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPcRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPcRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PC_RETR;
    }

    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        writeDecimalAsPacked(Pos.TGA_PC_RETR, tgaPcRetr.copy());
    }

    public void setTgaPcRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PC_RETR, Pos.TGA_PC_RETR);
    }

    /**Original name: TGA-PC-RETR<br>*/
    public AfDecimal getTgaPcRetr() {
        return readPackedAsDecimal(Pos.TGA_PC_RETR, Len.Int.TGA_PC_RETR, Len.Fract.TGA_PC_RETR);
    }

    public byte[] getTgaPcRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PC_RETR, Pos.TGA_PC_RETR);
        return buffer;
    }

    public void setTgaPcRetrNull(String tgaPcRetrNull) {
        writeString(Pos.TGA_PC_RETR_NULL, tgaPcRetrNull, Len.TGA_PC_RETR_NULL);
    }

    /**Original name: TGA-PC-RETR-NULL<br>*/
    public String getTgaPcRetrNull() {
        return readString(Pos.TGA_PC_RETR_NULL, Len.TGA_PC_RETR_NULL);
    }

    public String getTgaPcRetrNullFormatted() {
        return Functions.padBlanks(getTgaPcRetrNull(), Len.TGA_PC_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PC_RETR = 1;
        public static final int TGA_PC_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PC_RETR = 4;
        public static final int TGA_PC_RETR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
