package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPOG-ID-MOVI-CHIU<br>
 * Variable: WPOG-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpogIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpogIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_ID_MOVI_CHIU;
    }

    public void setWpogIdMoviChiu(int wpogIdMoviChiu) {
        writeIntAsPacked(Pos.WPOG_ID_MOVI_CHIU, wpogIdMoviChiu, Len.Int.WPOG_ID_MOVI_CHIU);
    }

    public void setWpogIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOG_ID_MOVI_CHIU, Pos.WPOG_ID_MOVI_CHIU);
    }

    /**Original name: WPOG-ID-MOVI-CHIU<br>*/
    public int getWpogIdMoviChiu() {
        return readPackedAsInt(Pos.WPOG_ID_MOVI_CHIU, Len.Int.WPOG_ID_MOVI_CHIU);
    }

    public byte[] getWpogIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOG_ID_MOVI_CHIU, Pos.WPOG_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWpogIdMoviChiuSpaces() {
        fill(Pos.WPOG_ID_MOVI_CHIU, Len.WPOG_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWpogIdMoviChiuNull(String wpogIdMoviChiuNull) {
        writeString(Pos.WPOG_ID_MOVI_CHIU_NULL, wpogIdMoviChiuNull, Len.WPOG_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WPOG-ID-MOVI-CHIU-NULL<br>*/
    public String getWpogIdMoviChiuNull() {
        return readString(Pos.WPOG_ID_MOVI_CHIU_NULL, Len.WPOG_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOG_ID_MOVI_CHIU = 1;
        public static final int WPOG_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_ID_MOVI_CHIU = 5;
        public static final int WPOG_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOG_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
