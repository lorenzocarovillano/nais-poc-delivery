package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FINE-LOOP<br>
 * Variable: WK-FINE-LOOP from program IABS0130<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFineLoop {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFineLoop(char wkFineLoop) {
        this.value = wkFineLoop;
    }

    public char getWkFineLoop() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }
}
