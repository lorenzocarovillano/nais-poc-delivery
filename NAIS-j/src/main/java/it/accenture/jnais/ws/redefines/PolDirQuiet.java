package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DIR-QUIET<br>
 * Variable: POL-DIR-QUIET from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDirQuiet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDirQuiet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DIR_QUIET;
    }

    public void setPolDirQuiet(AfDecimal polDirQuiet) {
        writeDecimalAsPacked(Pos.POL_DIR_QUIET, polDirQuiet.copy());
    }

    public void setPolDirQuietFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DIR_QUIET, Pos.POL_DIR_QUIET);
    }

    /**Original name: POL-DIR-QUIET<br>*/
    public AfDecimal getPolDirQuiet() {
        return readPackedAsDecimal(Pos.POL_DIR_QUIET, Len.Int.POL_DIR_QUIET, Len.Fract.POL_DIR_QUIET);
    }

    public byte[] getPolDirQuietAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DIR_QUIET, Pos.POL_DIR_QUIET);
        return buffer;
    }

    public void setPolDirQuietNull(String polDirQuietNull) {
        writeString(Pos.POL_DIR_QUIET_NULL, polDirQuietNull, Len.POL_DIR_QUIET_NULL);
    }

    /**Original name: POL-DIR-QUIET-NULL<br>*/
    public String getPolDirQuietNull() {
        return readString(Pos.POL_DIR_QUIET_NULL, Len.POL_DIR_QUIET_NULL);
    }

    public String getPolDirQuietNullFormatted() {
        return Functions.padBlanks(getPolDirQuietNull(), Len.POL_DIR_QUIET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DIR_QUIET = 1;
        public static final int POL_DIR_QUIET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DIR_QUIET = 8;
        public static final int POL_DIR_QUIET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DIR_QUIET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POL_DIR_QUIET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
