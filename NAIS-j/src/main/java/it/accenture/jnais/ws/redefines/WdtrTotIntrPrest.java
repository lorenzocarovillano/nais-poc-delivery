package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-TOT-INTR-PREST<br>
 * Variable: WDTR-TOT-INTR-PREST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_TOT_INTR_PREST;
    }

    public void setWdtrTotIntrPrest(AfDecimal wdtrTotIntrPrest) {
        writeDecimalAsPacked(Pos.WDTR_TOT_INTR_PREST, wdtrTotIntrPrest.copy());
    }

    /**Original name: WDTR-TOT-INTR-PREST<br>*/
    public AfDecimal getWdtrTotIntrPrest() {
        return readPackedAsDecimal(Pos.WDTR_TOT_INTR_PREST, Len.Int.WDTR_TOT_INTR_PREST, Len.Fract.WDTR_TOT_INTR_PREST);
    }

    public void setWdtrTotIntrPrestNull(String wdtrTotIntrPrestNull) {
        writeString(Pos.WDTR_TOT_INTR_PREST_NULL, wdtrTotIntrPrestNull, Len.WDTR_TOT_INTR_PREST_NULL);
    }

    /**Original name: WDTR-TOT-INTR-PREST-NULL<br>*/
    public String getWdtrTotIntrPrestNull() {
        return readString(Pos.WDTR_TOT_INTR_PREST_NULL, Len.WDTR_TOT_INTR_PREST_NULL);
    }

    public String getWdtrTotIntrPrestNullFormatted() {
        return Functions.padBlanks(getWdtrTotIntrPrestNull(), Len.WDTR_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_TOT_INTR_PREST = 1;
        public static final int WDTR_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_TOT_INTR_PREST = 8;
        public static final int WDTR_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
