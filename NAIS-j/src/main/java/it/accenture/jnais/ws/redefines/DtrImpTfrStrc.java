package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-IMP-TFR-STRC<br>
 * Variable: DTR-IMP-TFR-STRC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_IMP_TFR_STRC;
    }

    public void setDtrImpTfrStrc(AfDecimal dtrImpTfrStrc) {
        writeDecimalAsPacked(Pos.DTR_IMP_TFR_STRC, dtrImpTfrStrc.copy());
    }

    public void setDtrImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_IMP_TFR_STRC, Pos.DTR_IMP_TFR_STRC);
    }

    /**Original name: DTR-IMP-TFR-STRC<br>*/
    public AfDecimal getDtrImpTfrStrc() {
        return readPackedAsDecimal(Pos.DTR_IMP_TFR_STRC, Len.Int.DTR_IMP_TFR_STRC, Len.Fract.DTR_IMP_TFR_STRC);
    }

    public byte[] getDtrImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_IMP_TFR_STRC, Pos.DTR_IMP_TFR_STRC);
        return buffer;
    }

    public void setDtrImpTfrStrcNull(String dtrImpTfrStrcNull) {
        writeString(Pos.DTR_IMP_TFR_STRC_NULL, dtrImpTfrStrcNull, Len.DTR_IMP_TFR_STRC_NULL);
    }

    /**Original name: DTR-IMP-TFR-STRC-NULL<br>*/
    public String getDtrImpTfrStrcNull() {
        return readString(Pos.DTR_IMP_TFR_STRC_NULL, Len.DTR_IMP_TFR_STRC_NULL);
    }

    public String getDtrImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getDtrImpTfrStrcNull(), Len.DTR_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_IMP_TFR_STRC = 1;
        public static final int DTR_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_IMP_TFR_STRC = 8;
        public static final int DTR_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
