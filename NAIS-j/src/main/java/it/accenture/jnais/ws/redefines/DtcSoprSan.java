package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SOPR-SAN<br>
 * Variable: DTC-SOPR-SAN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_SOPR_SAN;
    }

    public void setDtcSoprSan(AfDecimal dtcSoprSan) {
        writeDecimalAsPacked(Pos.DTC_SOPR_SAN, dtcSoprSan.copy());
    }

    public void setDtcSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_SOPR_SAN, Pos.DTC_SOPR_SAN);
    }

    /**Original name: DTC-SOPR-SAN<br>*/
    public AfDecimal getDtcSoprSan() {
        return readPackedAsDecimal(Pos.DTC_SOPR_SAN, Len.Int.DTC_SOPR_SAN, Len.Fract.DTC_SOPR_SAN);
    }

    public byte[] getDtcSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_SOPR_SAN, Pos.DTC_SOPR_SAN);
        return buffer;
    }

    public void setDtcSoprSanNull(String dtcSoprSanNull) {
        writeString(Pos.DTC_SOPR_SAN_NULL, dtcSoprSanNull, Len.DTC_SOPR_SAN_NULL);
    }

    /**Original name: DTC-SOPR-SAN-NULL<br>*/
    public String getDtcSoprSanNull() {
        return readString(Pos.DTC_SOPR_SAN_NULL, Len.DTC_SOPR_SAN_NULL);
    }

    public String getDtcSoprSanNullFormatted() {
        return Functions.padBlanks(getDtcSoprSanNull(), Len.DTC_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_SAN = 1;
        public static final int DTC_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_SAN = 8;
        public static final int DTC_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
