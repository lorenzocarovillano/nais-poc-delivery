package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-QTZ-TOT-DT-ULT-BIL<br>
 * Variable: W-B03-QTZ-TOT-DT-ULT-BIL from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03QtzTotDtUltBilLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03QtzTotDtUltBilLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_QTZ_TOT_DT_ULT_BIL;
    }

    public void setwB03QtzTotDtUltBil(AfDecimal wB03QtzTotDtUltBil) {
        writeDecimalAsPacked(Pos.W_B03_QTZ_TOT_DT_ULT_BIL, wB03QtzTotDtUltBil.copy());
    }

    /**Original name: W-B03-QTZ-TOT-DT-ULT-BIL<br>*/
    public AfDecimal getwB03QtzTotDtUltBil() {
        return readPackedAsDecimal(Pos.W_B03_QTZ_TOT_DT_ULT_BIL, Len.Int.W_B03_QTZ_TOT_DT_ULT_BIL, Len.Fract.W_B03_QTZ_TOT_DT_ULT_BIL);
    }

    public byte[] getwB03QtzTotDtUltBilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_QTZ_TOT_DT_ULT_BIL, Pos.W_B03_QTZ_TOT_DT_ULT_BIL);
        return buffer;
    }

    public void setwB03QtzTotDtUltBilNull(String wB03QtzTotDtUltBilNull) {
        writeString(Pos.W_B03_QTZ_TOT_DT_ULT_BIL_NULL, wB03QtzTotDtUltBilNull, Len.W_B03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    /**Original name: W-B03-QTZ-TOT-DT-ULT-BIL-NULL<br>*/
    public String getwB03QtzTotDtUltBilNull() {
        return readString(Pos.W_B03_QTZ_TOT_DT_ULT_BIL_NULL, Len.W_B03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_TOT_DT_ULT_BIL = 1;
        public static final int W_B03_QTZ_TOT_DT_ULT_BIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_TOT_DT_ULT_BIL = 7;
        public static final int W_B03_QTZ_TOT_DT_ULT_BIL_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_TOT_DT_ULT_BIL = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_TOT_DT_ULT_BIL = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
