package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: CACHE-PIENA<br>
 * Variable: CACHE-PIENA from copybook IABC0010<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CachePiena {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setCachePiena(char cachePiena) {
        this.value = cachePiena;
    }

    public char getCachePiena() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
