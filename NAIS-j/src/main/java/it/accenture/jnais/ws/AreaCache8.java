package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: AREA-CACHE-8<br>
 * Variable: AREA-CACHE-8 from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaCache8 {

    //==== PROPERTIES ====
    //Original name: REC8-NUM-QUO-DISINV-COMMGES
    private AfDecimal numQuoDisinvCommges = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-CNTRVAL-PREC
    private AfDecimal cntrvalPrec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC8-CNTRVAL-ATTUALE
    private AfDecimal cntrvalAttuale = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: REC8-NUM-QUO-INV-SWITCH
    private AfDecimal numQuoInvSwitch = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-NUM-QUO-INVST-VERS
    private AfDecimal numQuoInvstVers = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-NUM-QUO-INVST-REBATE
    private AfDecimal numQuoInvstRebate = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-NUM-QUO-DISINV-SWITCH
    private AfDecimal numQuoDisinvSwitch = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-NUM-QUO-DISINV-PREL-COSTI
    private AfDecimal numQuoDisinvPrelCosti = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-NUM-QUO-DISINV-COMP-NAV
    private AfDecimal numQuoDisinvCompNav = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: REC8-NUM-QUO-INVST-COMP-NAV
    private AfDecimal numQuoInvstCompNav = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);

    //==== METHODS ====
    public void setNumQuoDisinvCommges(AfDecimal numQuoDisinvCommges) {
        this.numQuoDisinvCommges.assign(numQuoDisinvCommges);
    }

    public AfDecimal getNumQuoDisinvCommges() {
        return this.numQuoDisinvCommges.copy();
    }

    public void setCntrvalPrec(AfDecimal cntrvalPrec) {
        this.cntrvalPrec.assign(cntrvalPrec);
    }

    public AfDecimal getCntrvalPrec() {
        return this.cntrvalPrec.copy();
    }

    public void setCntrvalAttuale(AfDecimal cntrvalAttuale) {
        this.cntrvalAttuale.assign(cntrvalAttuale);
    }

    public AfDecimal getCntrvalAttuale() {
        return this.cntrvalAttuale.copy();
    }

    public void setNumQuoInvSwitch(AfDecimal numQuoInvSwitch) {
        this.numQuoInvSwitch.assign(numQuoInvSwitch);
    }

    public AfDecimal getNumQuoInvSwitch() {
        return this.numQuoInvSwitch.copy();
    }

    public void setNumQuoInvstVers(AfDecimal numQuoInvstVers) {
        this.numQuoInvstVers.assign(numQuoInvstVers);
    }

    public AfDecimal getNumQuoInvstVers() {
        return this.numQuoInvstVers.copy();
    }

    public void setNumQuoInvstRebate(AfDecimal numQuoInvstRebate) {
        this.numQuoInvstRebate.assign(numQuoInvstRebate);
    }

    public AfDecimal getNumQuoInvstRebate() {
        return this.numQuoInvstRebate.copy();
    }

    public void setNumQuoDisinvSwitch(AfDecimal numQuoDisinvSwitch) {
        this.numQuoDisinvSwitch.assign(numQuoDisinvSwitch);
    }

    public AfDecimal getNumQuoDisinvSwitch() {
        return this.numQuoDisinvSwitch.copy();
    }

    public void setNumQuoDisinvPrelCosti(AfDecimal numQuoDisinvPrelCosti) {
        this.numQuoDisinvPrelCosti.assign(numQuoDisinvPrelCosti);
    }

    public AfDecimal getNumQuoDisinvPrelCosti() {
        return this.numQuoDisinvPrelCosti.copy();
    }

    public void setNumQuoDisinvCompNav(AfDecimal numQuoDisinvCompNav) {
        this.numQuoDisinvCompNav.assign(numQuoDisinvCompNav);
    }

    public AfDecimal getNumQuoDisinvCompNav() {
        return this.numQuoDisinvCompNav.copy();
    }

    public void setNumQuoInvstCompNav(AfDecimal numQuoInvstCompNav) {
        this.numQuoInvstCompNav.assign(numQuoInvstCompNav);
    }

    public AfDecimal getNumQuoInvstCompNav() {
        return this.numQuoInvstCompNav.copy();
    }
}
