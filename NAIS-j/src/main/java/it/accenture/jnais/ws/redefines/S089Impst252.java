package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-252<br>
 * Variable: S089-IMPST-252 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089Impst252 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089Impst252() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST252;
    }

    public void setWlquImpst252(AfDecimal wlquImpst252) {
        writeDecimalAsPacked(Pos.S089_IMPST252, wlquImpst252.copy());
    }

    public void setWlquImpst252FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST252, Pos.S089_IMPST252);
    }

    /**Original name: WLQU-IMPST-252<br>*/
    public AfDecimal getWlquImpst252() {
        return readPackedAsDecimal(Pos.S089_IMPST252, Len.Int.WLQU_IMPST252, Len.Fract.WLQU_IMPST252);
    }

    public byte[] getWlquImpst252AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST252, Pos.S089_IMPST252);
        return buffer;
    }

    public void initWlquImpst252Spaces() {
        fill(Pos.S089_IMPST252, Len.S089_IMPST252, Types.SPACE_CHAR);
    }

    public void setWlquImpst252Null(String wlquImpst252Null) {
        writeString(Pos.S089_IMPST252_NULL, wlquImpst252Null, Len.WLQU_IMPST252_NULL);
    }

    /**Original name: WLQU-IMPST-252-NULL<br>*/
    public String getWlquImpst252Null() {
        return readString(Pos.S089_IMPST252_NULL, Len.WLQU_IMPST252_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST252 = 1;
        public static final int S089_IMPST252_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST252 = 8;
        public static final int WLQU_IMPST252_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST252 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST252 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
