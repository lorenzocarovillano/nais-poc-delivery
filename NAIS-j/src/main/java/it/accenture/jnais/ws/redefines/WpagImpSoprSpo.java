package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-SOPR-SPO<br>
 * Variable: WPAG-IMP-SOPR-SPO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_SOPR_SPO;
    }

    public void setWpagImpSoprSpo(AfDecimal wpagImpSoprSpo) {
        writeDecimalAsPacked(Pos.WPAG_IMP_SOPR_SPO, wpagImpSoprSpo.copy());
    }

    public void setWpagImpSoprSpoFormatted(String wpagImpSoprSpo) {
        setWpagImpSoprSpo(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_SOPR_SPO + Len.Fract.WPAG_IMP_SOPR_SPO, Len.Fract.WPAG_IMP_SOPR_SPO, wpagImpSoprSpo));
    }

    public void setWpagImpSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_SOPR_SPO, Pos.WPAG_IMP_SOPR_SPO);
    }

    /**Original name: WPAG-IMP-SOPR-SPO<br>*/
    public AfDecimal getWpagImpSoprSpo() {
        return readPackedAsDecimal(Pos.WPAG_IMP_SOPR_SPO, Len.Int.WPAG_IMP_SOPR_SPO, Len.Fract.WPAG_IMP_SOPR_SPO);
    }

    public byte[] getWpagImpSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_SOPR_SPO, Pos.WPAG_IMP_SOPR_SPO);
        return buffer;
    }

    public void initWpagImpSoprSpoSpaces() {
        fill(Pos.WPAG_IMP_SOPR_SPO, Len.WPAG_IMP_SOPR_SPO, Types.SPACE_CHAR);
    }

    public void setWpagImpSoprSpoNull(String wpagImpSoprSpoNull) {
        writeString(Pos.WPAG_IMP_SOPR_SPO_NULL, wpagImpSoprSpoNull, Len.WPAG_IMP_SOPR_SPO_NULL);
    }

    /**Original name: WPAG-IMP-SOPR-SPO-NULL<br>*/
    public String getWpagImpSoprSpoNull() {
        return readString(Pos.WPAG_IMP_SOPR_SPO_NULL, Len.WPAG_IMP_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_SPO = 1;
        public static final int WPAG_IMP_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_SPO = 8;
        public static final int WPAG_IMP_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
