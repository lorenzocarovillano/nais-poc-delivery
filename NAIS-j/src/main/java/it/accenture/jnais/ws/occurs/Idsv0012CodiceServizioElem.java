package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0012-CODICE-SERVIZIO-ELEM<br>
 * Variables: IDSV0012-CODICE-SERVIZIO-ELEM from copybook IDSV0012<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0012CodiceServizioElem {

    //==== CONSTRUCTORS ====
    public Idsv0012CodiceServizioElem() {
        init();
    }

    //==== METHODS ====
    public void init() {
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_SERVIZIO = 8;
        public static final int TIPO_SERVIZIO = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
