package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPOL-DT-SCAD<br>
 * Variable: WPOL-DT-SCAD from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DT_SCAD;
    }

    public void setWpolDtScad(int wpolDtScad) {
        writeIntAsPacked(Pos.WPOL_DT_SCAD, wpolDtScad, Len.Int.WPOL_DT_SCAD);
    }

    public void setWpolDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DT_SCAD, Pos.WPOL_DT_SCAD);
    }

    /**Original name: WPOL-DT-SCAD<br>*/
    public int getWpolDtScad() {
        return readPackedAsInt(Pos.WPOL_DT_SCAD, Len.Int.WPOL_DT_SCAD);
    }

    public byte[] getWpolDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DT_SCAD, Pos.WPOL_DT_SCAD);
        return buffer;
    }

    public void setWpolDtScadNull(String wpolDtScadNull) {
        writeString(Pos.WPOL_DT_SCAD_NULL, wpolDtScadNull, Len.WPOL_DT_SCAD_NULL);
    }

    /**Original name: WPOL-DT-SCAD-NULL<br>*/
    public String getWpolDtScadNull() {
        return readString(Pos.WPOL_DT_SCAD_NULL, Len.WPOL_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DT_SCAD = 1;
        public static final int WPOL_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DT_SCAD = 5;
        public static final int WPOL_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
