package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-SPE<br>
 * Variable: WRST-RIS-SPE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisSpe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisSpe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_SPE;
    }

    public void setWrstRisSpe(AfDecimal wrstRisSpe) {
        writeDecimalAsPacked(Pos.WRST_RIS_SPE, wrstRisSpe.copy());
    }

    public void setWrstRisSpeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_SPE, Pos.WRST_RIS_SPE);
    }

    /**Original name: WRST-RIS-SPE<br>*/
    public AfDecimal getWrstRisSpe() {
        return readPackedAsDecimal(Pos.WRST_RIS_SPE, Len.Int.WRST_RIS_SPE, Len.Fract.WRST_RIS_SPE);
    }

    public byte[] getWrstRisSpeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_SPE, Pos.WRST_RIS_SPE);
        return buffer;
    }

    public void initWrstRisSpeSpaces() {
        fill(Pos.WRST_RIS_SPE, Len.WRST_RIS_SPE, Types.SPACE_CHAR);
    }

    public void setWrstRisSpeNull(String wrstRisSpeNull) {
        writeString(Pos.WRST_RIS_SPE_NULL, wrstRisSpeNull, Len.WRST_RIS_SPE_NULL);
    }

    /**Original name: WRST-RIS-SPE-NULL<br>*/
    public String getWrstRisSpeNull() {
        return readString(Pos.WRST_RIS_SPE_NULL, Len.WRST_RIS_SPE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_SPE = 1;
        public static final int WRST_RIS_SPE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_SPE = 8;
        public static final int WRST_RIS_SPE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_SPE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_SPE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
