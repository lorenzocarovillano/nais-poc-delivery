package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FLG<br>
 * Variable: WK-FLG from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFlg {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setWkFlg(char wkFlg) {
        this.value = wkFlg;
    }

    public char getWkFlg() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
