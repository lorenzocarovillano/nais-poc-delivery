package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-DUR-MM<br>
 * Variable: PMO-DUR-MM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_DUR_MM;
    }

    public void setPmoDurMm(int pmoDurMm) {
        writeIntAsPacked(Pos.PMO_DUR_MM, pmoDurMm, Len.Int.PMO_DUR_MM);
    }

    public void setPmoDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_DUR_MM, Pos.PMO_DUR_MM);
    }

    /**Original name: PMO-DUR-MM<br>*/
    public int getPmoDurMm() {
        return readPackedAsInt(Pos.PMO_DUR_MM, Len.Int.PMO_DUR_MM);
    }

    public byte[] getPmoDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_DUR_MM, Pos.PMO_DUR_MM);
        return buffer;
    }

    public void initPmoDurMmHighValues() {
        fill(Pos.PMO_DUR_MM, Len.PMO_DUR_MM, Types.HIGH_CHAR_VAL);
    }

    public void setPmoDurMmNull(String pmoDurMmNull) {
        writeString(Pos.PMO_DUR_MM_NULL, pmoDurMmNull, Len.PMO_DUR_MM_NULL);
    }

    /**Original name: PMO-DUR-MM-NULL<br>*/
    public String getPmoDurMmNull() {
        return readString(Pos.PMO_DUR_MM_NULL, Len.PMO_DUR_MM_NULL);
    }

    public String getPmoDurMmNullFormatted() {
        return Functions.padBlanks(getPmoDurMmNull(), Len.PMO_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_DUR_MM = 1;
        public static final int PMO_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_DUR_MM = 3;
        public static final int PMO_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
