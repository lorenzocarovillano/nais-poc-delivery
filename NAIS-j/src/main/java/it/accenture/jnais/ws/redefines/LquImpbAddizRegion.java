package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-ADDIZ-REGION<br>
 * Variable: LQU-IMPB-ADDIZ-REGION from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbAddizRegion extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbAddizRegion() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_ADDIZ_REGION;
    }

    public void setLquImpbAddizRegion(AfDecimal lquImpbAddizRegion) {
        writeDecimalAsPacked(Pos.LQU_IMPB_ADDIZ_REGION, lquImpbAddizRegion.copy());
    }

    public void setLquImpbAddizRegionFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_ADDIZ_REGION, Pos.LQU_IMPB_ADDIZ_REGION);
    }

    /**Original name: LQU-IMPB-ADDIZ-REGION<br>*/
    public AfDecimal getLquImpbAddizRegion() {
        return readPackedAsDecimal(Pos.LQU_IMPB_ADDIZ_REGION, Len.Int.LQU_IMPB_ADDIZ_REGION, Len.Fract.LQU_IMPB_ADDIZ_REGION);
    }

    public byte[] getLquImpbAddizRegionAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_ADDIZ_REGION, Pos.LQU_IMPB_ADDIZ_REGION);
        return buffer;
    }

    public void setLquImpbAddizRegionNull(String lquImpbAddizRegionNull) {
        writeString(Pos.LQU_IMPB_ADDIZ_REGION_NULL, lquImpbAddizRegionNull, Len.LQU_IMPB_ADDIZ_REGION_NULL);
    }

    /**Original name: LQU-IMPB-ADDIZ-REGION-NULL<br>*/
    public String getLquImpbAddizRegionNull() {
        return readString(Pos.LQU_IMPB_ADDIZ_REGION_NULL, Len.LQU_IMPB_ADDIZ_REGION_NULL);
    }

    public String getLquImpbAddizRegionNullFormatted() {
        return Functions.padBlanks(getLquImpbAddizRegionNull(), Len.LQU_IMPB_ADDIZ_REGION_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_ADDIZ_REGION = 1;
        public static final int LQU_IMPB_ADDIZ_REGION_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_ADDIZ_REGION = 8;
        public static final int LQU_IMPB_ADDIZ_REGION_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_ADDIZ_REGION = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_ADDIZ_REGION = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
