package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-PERC-FONDO<br>
 * Variable: WPAG-PERC-FONDO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPercFondo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPercFondo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PERC_FONDO;
    }

    public void setWpagPercFondo(AfDecimal wpagPercFondo) {
        writeDecimalAsPacked(Pos.WPAG_PERC_FONDO, wpagPercFondo.copy());
    }

    public void setWpagPercFondoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PERC_FONDO, Pos.WPAG_PERC_FONDO);
    }

    /**Original name: WPAG-PERC-FONDO<br>*/
    public AfDecimal getWpagPercFondo() {
        return readPackedAsDecimal(Pos.WPAG_PERC_FONDO, Len.Int.WPAG_PERC_FONDO, Len.Fract.WPAG_PERC_FONDO);
    }

    public byte[] getWpagPercFondoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PERC_FONDO, Pos.WPAG_PERC_FONDO);
        return buffer;
    }

    public void initWpagPercFondoSpaces() {
        fill(Pos.WPAG_PERC_FONDO, Len.WPAG_PERC_FONDO, Types.SPACE_CHAR);
    }

    public void setWpagPercFondoNull(String wpagPercFondoNull) {
        writeString(Pos.WPAG_PERC_FONDO_NULL, wpagPercFondoNull, Len.WPAG_PERC_FONDO_NULL);
    }

    /**Original name: WPAG-PERC-FONDO-NULL<br>*/
    public String getWpagPercFondoNull() {
        return readString(Pos.WPAG_PERC_FONDO_NULL, Len.WPAG_PERC_FONDO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PERC_FONDO = 1;
        public static final int WPAG_PERC_FONDO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PERC_FONDO = 4;
        public static final int WPAG_PERC_FONDO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PERC_FONDO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PERC_FONDO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
