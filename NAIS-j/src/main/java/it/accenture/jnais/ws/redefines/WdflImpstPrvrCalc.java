package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-PRVR-CALC<br>
 * Variable: WDFL-IMPST-PRVR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstPrvrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstPrvrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_PRVR_CALC;
    }

    public void setWdflImpstPrvrCalc(AfDecimal wdflImpstPrvrCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_PRVR_CALC, wdflImpstPrvrCalc.copy());
    }

    public void setWdflImpstPrvrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_PRVR_CALC, Pos.WDFL_IMPST_PRVR_CALC);
    }

    /**Original name: WDFL-IMPST-PRVR-CALC<br>*/
    public AfDecimal getWdflImpstPrvrCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_PRVR_CALC, Len.Int.WDFL_IMPST_PRVR_CALC, Len.Fract.WDFL_IMPST_PRVR_CALC);
    }

    public byte[] getWdflImpstPrvrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_PRVR_CALC, Pos.WDFL_IMPST_PRVR_CALC);
        return buffer;
    }

    public void setWdflImpstPrvrCalcNull(String wdflImpstPrvrCalcNull) {
        writeString(Pos.WDFL_IMPST_PRVR_CALC_NULL, wdflImpstPrvrCalcNull, Len.WDFL_IMPST_PRVR_CALC_NULL);
    }

    /**Original name: WDFL-IMPST-PRVR-CALC-NULL<br>*/
    public String getWdflImpstPrvrCalcNull() {
        return readString(Pos.WDFL_IMPST_PRVR_CALC_NULL, Len.WDFL_IMPST_PRVR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_PRVR_CALC = 1;
        public static final int WDFL_IMPST_PRVR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_PRVR_CALC = 8;
        public static final int WDFL_IMPST_PRVR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_PRVR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_PRVR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
