package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BJS-DT-START<br>
 * Variable: BJS-DT-START from program IABS0060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BjsDtStart extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BjsDtStart() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BJS_DT_START;
    }

    public void setBjsDtStart(long bjsDtStart) {
        writeLongAsPacked(Pos.BJS_DT_START, bjsDtStart, Len.Int.BJS_DT_START);
    }

    public void setBjsDtStartFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BJS_DT_START, Pos.BJS_DT_START);
    }

    /**Original name: BJS-DT-START<br>*/
    public long getBjsDtStart() {
        return readPackedAsLong(Pos.BJS_DT_START, Len.Int.BJS_DT_START);
    }

    public byte[] getBjsDtStartAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BJS_DT_START, Pos.BJS_DT_START);
        return buffer;
    }

    public void setBjsDtStartNull(String bjsDtStartNull) {
        writeString(Pos.BJS_DT_START_NULL, bjsDtStartNull, Len.BJS_DT_START_NULL);
    }

    /**Original name: BJS-DT-START-NULL<br>*/
    public String getBjsDtStartNull() {
        return readString(Pos.BJS_DT_START_NULL, Len.BJS_DT_START_NULL);
    }

    public String getBjsDtStartNullFormatted() {
        return Functions.padBlanks(getBjsDtStartNull(), Len.BJS_DT_START_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BJS_DT_START = 1;
        public static final int BJS_DT_START_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BJS_DT_START = 10;
        public static final int BJS_DT_START_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BJS_DT_START = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
