package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-AZ-K2<br>
 * Variable: DFA-IMP-CNBT-AZ-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtAzK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtAzK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_AZ_K2;
    }

    public void setDfaImpCnbtAzK2(AfDecimal dfaImpCnbtAzK2) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_AZ_K2, dfaImpCnbtAzK2.copy());
    }

    public void setDfaImpCnbtAzK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_AZ_K2, Pos.DFA_IMP_CNBT_AZ_K2);
    }

    /**Original name: DFA-IMP-CNBT-AZ-K2<br>*/
    public AfDecimal getDfaImpCnbtAzK2() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_AZ_K2, Len.Int.DFA_IMP_CNBT_AZ_K2, Len.Fract.DFA_IMP_CNBT_AZ_K2);
    }

    public byte[] getDfaImpCnbtAzK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_AZ_K2, Pos.DFA_IMP_CNBT_AZ_K2);
        return buffer;
    }

    public void setDfaImpCnbtAzK2Null(String dfaImpCnbtAzK2Null) {
        writeString(Pos.DFA_IMP_CNBT_AZ_K2_NULL, dfaImpCnbtAzK2Null, Len.DFA_IMP_CNBT_AZ_K2_NULL);
    }

    /**Original name: DFA-IMP-CNBT-AZ-K2-NULL<br>*/
    public String getDfaImpCnbtAzK2Null() {
        return readString(Pos.DFA_IMP_CNBT_AZ_K2_NULL, Len.DFA_IMP_CNBT_AZ_K2_NULL);
    }

    public String getDfaImpCnbtAzK2NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtAzK2Null(), Len.DFA_IMP_CNBT_AZ_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_AZ_K2 = 1;
        public static final int DFA_IMP_CNBT_AZ_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_AZ_K2 = 8;
        public static final int DFA_IMP_CNBT_AZ_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_AZ_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_AZ_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
