package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.VasDtValzz;
import it.accenture.jnais.ws.redefines.VasDtValzzCalc;
import it.accenture.jnais.ws.redefines.VasIdMoviChiu;
import it.accenture.jnais.ws.redefines.VasIdRichDisFnd;
import it.accenture.jnais.ws.redefines.VasIdRichInvstFnd;
import it.accenture.jnais.ws.redefines.VasIdTrchDiGar;
import it.accenture.jnais.ws.redefines.VasImpMovto;
import it.accenture.jnais.ws.redefines.VasMinusValenza;
import it.accenture.jnais.ws.redefines.VasNumQuo;
import it.accenture.jnais.ws.redefines.VasNumQuoLorde;
import it.accenture.jnais.ws.redefines.VasPcInvDis;
import it.accenture.jnais.ws.redefines.VasPlusValenza;
import it.accenture.jnais.ws.redefines.VasPreMovto;
import it.accenture.jnais.ws.redefines.VasValQuo;

/**Original name: VAL-AST<br>
 * Variable: VAL-AST from copybook IDBVVAS1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ValAstLdbs4910 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: VAS-ID-VAL-AST
    private int vasIdValAst = DefaultValues.INT_VAL;
    //Original name: VAS-ID-TRCH-DI-GAR
    private VasIdTrchDiGar vasIdTrchDiGar = new VasIdTrchDiGar();
    //Original name: VAS-ID-MOVI-FINRIO
    private int vasIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: VAS-ID-MOVI-CRZ
    private int vasIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: VAS-ID-MOVI-CHIU
    private VasIdMoviChiu vasIdMoviChiu = new VasIdMoviChiu();
    //Original name: VAS-DT-INI-EFF
    private int vasDtIniEff = DefaultValues.INT_VAL;
    //Original name: VAS-DT-END-EFF
    private int vasDtEndEff = DefaultValues.INT_VAL;
    //Original name: VAS-COD-COMP-ANIA
    private int vasCodCompAnia = DefaultValues.INT_VAL;
    //Original name: VAS-COD-FND
    private String vasCodFnd = DefaultValues.stringVal(Len.VAS_COD_FND);
    //Original name: VAS-NUM-QUO
    private VasNumQuo vasNumQuo = new VasNumQuo();
    //Original name: VAS-VAL-QUO
    private VasValQuo vasValQuo = new VasValQuo();
    //Original name: VAS-DT-VALZZ
    private VasDtValzz vasDtValzz = new VasDtValzz();
    //Original name: VAS-TP-VAL-AST
    private String vasTpValAst = DefaultValues.stringVal(Len.VAS_TP_VAL_AST);
    //Original name: VAS-ID-RICH-INVST-FND
    private VasIdRichInvstFnd vasIdRichInvstFnd = new VasIdRichInvstFnd();
    //Original name: VAS-ID-RICH-DIS-FND
    private VasIdRichDisFnd vasIdRichDisFnd = new VasIdRichDisFnd();
    //Original name: VAS-DS-RIGA
    private long vasDsRiga = DefaultValues.LONG_VAL;
    //Original name: VAS-DS-OPER-SQL
    private char vasDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: VAS-DS-VER
    private int vasDsVer = DefaultValues.INT_VAL;
    //Original name: VAS-DS-TS-INI-CPTZ
    private long vasDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: VAS-DS-TS-END-CPTZ
    private long vasDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: VAS-DS-UTENTE
    private String vasDsUtente = DefaultValues.stringVal(Len.VAS_DS_UTENTE);
    //Original name: VAS-DS-STATO-ELAB
    private char vasDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: VAS-PRE-MOVTO
    private VasPreMovto vasPreMovto = new VasPreMovto();
    //Original name: VAS-IMP-MOVTO
    private VasImpMovto vasImpMovto = new VasImpMovto();
    //Original name: VAS-PC-INV-DIS
    private VasPcInvDis vasPcInvDis = new VasPcInvDis();
    //Original name: VAS-NUM-QUO-LORDE
    private VasNumQuoLorde vasNumQuoLorde = new VasNumQuoLorde();
    //Original name: VAS-DT-VALZZ-CALC
    private VasDtValzzCalc vasDtValzzCalc = new VasDtValzzCalc();
    //Original name: VAS-MINUS-VALENZA
    private VasMinusValenza vasMinusValenza = new VasMinusValenza();
    //Original name: VAS-PLUS-VALENZA
    private VasPlusValenza vasPlusValenza = new VasPlusValenza();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAL_AST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setValAstBytes(buf);
    }

    public String getValAstFormatted() {
        return MarshalByteExt.bufferToStr(getValAstBytes());
    }

    public void setValAstBytes(byte[] buffer) {
        setValAstBytes(buffer, 1);
    }

    public byte[] getValAstBytes() {
        byte[] buffer = new byte[Len.VAL_AST];
        return getValAstBytes(buffer, 1);
    }

    public void setValAstBytes(byte[] buffer, int offset) {
        int position = offset;
        vasIdValAst = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_ID_VAL_AST, 0);
        position += Len.VAS_ID_VAL_AST;
        vasIdTrchDiGar.setVasIdTrchDiGarFromBuffer(buffer, position);
        position += VasIdTrchDiGar.Len.VAS_ID_TRCH_DI_GAR;
        vasIdMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_ID_MOVI_FINRIO, 0);
        position += Len.VAS_ID_MOVI_FINRIO;
        vasIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_ID_MOVI_CRZ, 0);
        position += Len.VAS_ID_MOVI_CRZ;
        vasIdMoviChiu.setVasIdMoviChiuFromBuffer(buffer, position);
        position += VasIdMoviChiu.Len.VAS_ID_MOVI_CHIU;
        vasDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_DT_INI_EFF, 0);
        position += Len.VAS_DT_INI_EFF;
        vasDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_DT_END_EFF, 0);
        position += Len.VAS_DT_END_EFF;
        vasCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_COD_COMP_ANIA, 0);
        position += Len.VAS_COD_COMP_ANIA;
        vasCodFnd = MarshalByte.readString(buffer, position, Len.VAS_COD_FND);
        position += Len.VAS_COD_FND;
        vasNumQuo.setVasNumQuoFromBuffer(buffer, position);
        position += VasNumQuo.Len.VAS_NUM_QUO;
        vasValQuo.setVasValQuoFromBuffer(buffer, position);
        position += VasValQuo.Len.VAS_VAL_QUO;
        vasDtValzz.setVasDtValzzFromBuffer(buffer, position);
        position += VasDtValzz.Len.VAS_DT_VALZZ;
        vasTpValAst = MarshalByte.readString(buffer, position, Len.VAS_TP_VAL_AST);
        position += Len.VAS_TP_VAL_AST;
        vasIdRichInvstFnd.setVasIdRichInvstFndFromBuffer(buffer, position);
        position += VasIdRichInvstFnd.Len.VAS_ID_RICH_INVST_FND;
        vasIdRichDisFnd.setVasIdRichDisFndFromBuffer(buffer, position);
        position += VasIdRichDisFnd.Len.VAS_ID_RICH_DIS_FND;
        vasDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.VAS_DS_RIGA, 0);
        position += Len.VAS_DS_RIGA;
        vasDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        vasDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.VAS_DS_VER, 0);
        position += Len.VAS_DS_VER;
        vasDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.VAS_DS_TS_INI_CPTZ, 0);
        position += Len.VAS_DS_TS_INI_CPTZ;
        vasDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.VAS_DS_TS_END_CPTZ, 0);
        position += Len.VAS_DS_TS_END_CPTZ;
        vasDsUtente = MarshalByte.readString(buffer, position, Len.VAS_DS_UTENTE);
        position += Len.VAS_DS_UTENTE;
        vasDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        vasPreMovto.setVasPreMovtoFromBuffer(buffer, position);
        position += VasPreMovto.Len.VAS_PRE_MOVTO;
        vasImpMovto.setVasImpMovtoFromBuffer(buffer, position);
        position += VasImpMovto.Len.VAS_IMP_MOVTO;
        vasPcInvDis.setVasPcInvDisFromBuffer(buffer, position);
        position += VasPcInvDis.Len.VAS_PC_INV_DIS;
        vasNumQuoLorde.setVasNumQuoLordeFromBuffer(buffer, position);
        position += VasNumQuoLorde.Len.VAS_NUM_QUO_LORDE;
        vasDtValzzCalc.setVasDtValzzCalcFromBuffer(buffer, position);
        position += VasDtValzzCalc.Len.VAS_DT_VALZZ_CALC;
        vasMinusValenza.setVasMinusValenzaFromBuffer(buffer, position);
        position += VasMinusValenza.Len.VAS_MINUS_VALENZA;
        vasPlusValenza.setVasPlusValenzaFromBuffer(buffer, position);
    }

    public byte[] getValAstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, vasIdValAst, Len.Int.VAS_ID_VAL_AST, 0);
        position += Len.VAS_ID_VAL_AST;
        vasIdTrchDiGar.getVasIdTrchDiGarAsBuffer(buffer, position);
        position += VasIdTrchDiGar.Len.VAS_ID_TRCH_DI_GAR;
        MarshalByte.writeIntAsPacked(buffer, position, vasIdMoviFinrio, Len.Int.VAS_ID_MOVI_FINRIO, 0);
        position += Len.VAS_ID_MOVI_FINRIO;
        MarshalByte.writeIntAsPacked(buffer, position, vasIdMoviCrz, Len.Int.VAS_ID_MOVI_CRZ, 0);
        position += Len.VAS_ID_MOVI_CRZ;
        vasIdMoviChiu.getVasIdMoviChiuAsBuffer(buffer, position);
        position += VasIdMoviChiu.Len.VAS_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, vasDtIniEff, Len.Int.VAS_DT_INI_EFF, 0);
        position += Len.VAS_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, vasDtEndEff, Len.Int.VAS_DT_END_EFF, 0);
        position += Len.VAS_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, vasCodCompAnia, Len.Int.VAS_COD_COMP_ANIA, 0);
        position += Len.VAS_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, vasCodFnd, Len.VAS_COD_FND);
        position += Len.VAS_COD_FND;
        vasNumQuo.getVasNumQuoAsBuffer(buffer, position);
        position += VasNumQuo.Len.VAS_NUM_QUO;
        vasValQuo.getVasValQuoAsBuffer(buffer, position);
        position += VasValQuo.Len.VAS_VAL_QUO;
        vasDtValzz.getVasDtValzzAsBuffer(buffer, position);
        position += VasDtValzz.Len.VAS_DT_VALZZ;
        MarshalByte.writeString(buffer, position, vasTpValAst, Len.VAS_TP_VAL_AST);
        position += Len.VAS_TP_VAL_AST;
        vasIdRichInvstFnd.getVasIdRichInvstFndAsBuffer(buffer, position);
        position += VasIdRichInvstFnd.Len.VAS_ID_RICH_INVST_FND;
        vasIdRichDisFnd.getVasIdRichDisFndAsBuffer(buffer, position);
        position += VasIdRichDisFnd.Len.VAS_ID_RICH_DIS_FND;
        MarshalByte.writeLongAsPacked(buffer, position, vasDsRiga, Len.Int.VAS_DS_RIGA, 0);
        position += Len.VAS_DS_RIGA;
        MarshalByte.writeChar(buffer, position, vasDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, vasDsVer, Len.Int.VAS_DS_VER, 0);
        position += Len.VAS_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, vasDsTsIniCptz, Len.Int.VAS_DS_TS_INI_CPTZ, 0);
        position += Len.VAS_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, vasDsTsEndCptz, Len.Int.VAS_DS_TS_END_CPTZ, 0);
        position += Len.VAS_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, vasDsUtente, Len.VAS_DS_UTENTE);
        position += Len.VAS_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, vasDsStatoElab);
        position += Types.CHAR_SIZE;
        vasPreMovto.getVasPreMovtoAsBuffer(buffer, position);
        position += VasPreMovto.Len.VAS_PRE_MOVTO;
        vasImpMovto.getVasImpMovtoAsBuffer(buffer, position);
        position += VasImpMovto.Len.VAS_IMP_MOVTO;
        vasPcInvDis.getVasPcInvDisAsBuffer(buffer, position);
        position += VasPcInvDis.Len.VAS_PC_INV_DIS;
        vasNumQuoLorde.getVasNumQuoLordeAsBuffer(buffer, position);
        position += VasNumQuoLorde.Len.VAS_NUM_QUO_LORDE;
        vasDtValzzCalc.getVasDtValzzCalcAsBuffer(buffer, position);
        position += VasDtValzzCalc.Len.VAS_DT_VALZZ_CALC;
        vasMinusValenza.getVasMinusValenzaAsBuffer(buffer, position);
        position += VasMinusValenza.Len.VAS_MINUS_VALENZA;
        vasPlusValenza.getVasPlusValenzaAsBuffer(buffer, position);
        return buffer;
    }

    public void setVasIdValAst(int vasIdValAst) {
        this.vasIdValAst = vasIdValAst;
    }

    public int getVasIdValAst() {
        return this.vasIdValAst;
    }

    public void setVasIdMoviFinrio(int vasIdMoviFinrio) {
        this.vasIdMoviFinrio = vasIdMoviFinrio;
    }

    public int getVasIdMoviFinrio() {
        return this.vasIdMoviFinrio;
    }

    public void setVasIdMoviCrz(int vasIdMoviCrz) {
        this.vasIdMoviCrz = vasIdMoviCrz;
    }

    public int getVasIdMoviCrz() {
        return this.vasIdMoviCrz;
    }

    public void setVasDtIniEff(int vasDtIniEff) {
        this.vasDtIniEff = vasDtIniEff;
    }

    public int getVasDtIniEff() {
        return this.vasDtIniEff;
    }

    public void setVasDtEndEff(int vasDtEndEff) {
        this.vasDtEndEff = vasDtEndEff;
    }

    public int getVasDtEndEff() {
        return this.vasDtEndEff;
    }

    public void setVasCodCompAnia(int vasCodCompAnia) {
        this.vasCodCompAnia = vasCodCompAnia;
    }

    public int getVasCodCompAnia() {
        return this.vasCodCompAnia;
    }

    public void setVasCodFnd(String vasCodFnd) {
        this.vasCodFnd = Functions.subString(vasCodFnd, Len.VAS_COD_FND);
    }

    public String getVasCodFnd() {
        return this.vasCodFnd;
    }

    public String getVasCodFndFormatted() {
        return Functions.padBlanks(getVasCodFnd(), Len.VAS_COD_FND);
    }

    public void setVasTpValAst(String vasTpValAst) {
        this.vasTpValAst = Functions.subString(vasTpValAst, Len.VAS_TP_VAL_AST);
    }

    public String getVasTpValAst() {
        return this.vasTpValAst;
    }

    public void setVasDsRiga(long vasDsRiga) {
        this.vasDsRiga = vasDsRiga;
    }

    public long getVasDsRiga() {
        return this.vasDsRiga;
    }

    public void setVasDsOperSql(char vasDsOperSql) {
        this.vasDsOperSql = vasDsOperSql;
    }

    public char getVasDsOperSql() {
        return this.vasDsOperSql;
    }

    public void setVasDsVer(int vasDsVer) {
        this.vasDsVer = vasDsVer;
    }

    public int getVasDsVer() {
        return this.vasDsVer;
    }

    public void setVasDsTsIniCptz(long vasDsTsIniCptz) {
        this.vasDsTsIniCptz = vasDsTsIniCptz;
    }

    public long getVasDsTsIniCptz() {
        return this.vasDsTsIniCptz;
    }

    public void setVasDsTsEndCptz(long vasDsTsEndCptz) {
        this.vasDsTsEndCptz = vasDsTsEndCptz;
    }

    public long getVasDsTsEndCptz() {
        return this.vasDsTsEndCptz;
    }

    public void setVasDsUtente(String vasDsUtente) {
        this.vasDsUtente = Functions.subString(vasDsUtente, Len.VAS_DS_UTENTE);
    }

    public String getVasDsUtente() {
        return this.vasDsUtente;
    }

    public void setVasDsStatoElab(char vasDsStatoElab) {
        this.vasDsStatoElab = vasDsStatoElab;
    }

    public char getVasDsStatoElab() {
        return this.vasDsStatoElab;
    }

    public VasDtValzz getVasDtValzz() {
        return vasDtValzz;
    }

    public VasDtValzzCalc getVasDtValzzCalc() {
        return vasDtValzzCalc;
    }

    public VasIdMoviChiu getVasIdMoviChiu() {
        return vasIdMoviChiu;
    }

    public VasIdRichDisFnd getVasIdRichDisFnd() {
        return vasIdRichDisFnd;
    }

    public VasIdRichInvstFnd getVasIdRichInvstFnd() {
        return vasIdRichInvstFnd;
    }

    public VasIdTrchDiGar getVasIdTrchDiGar() {
        return vasIdTrchDiGar;
    }

    public VasImpMovto getVasImpMovto() {
        return vasImpMovto;
    }

    public VasMinusValenza getVasMinusValenza() {
        return vasMinusValenza;
    }

    public VasNumQuo getVasNumQuo() {
        return vasNumQuo;
    }

    public VasNumQuoLorde getVasNumQuoLorde() {
        return vasNumQuoLorde;
    }

    public VasPcInvDis getVasPcInvDis() {
        return vasPcInvDis;
    }

    public VasPlusValenza getVasPlusValenza() {
        return vasPlusValenza;
    }

    public VasPreMovto getVasPreMovto() {
        return vasPreMovto;
    }

    public VasValQuo getVasValQuo() {
        return vasValQuo;
    }

    @Override
    public byte[] serialize() {
        return getValAstBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_ID_VAL_AST = 5;
        public static final int VAS_ID_MOVI_FINRIO = 5;
        public static final int VAS_ID_MOVI_CRZ = 5;
        public static final int VAS_DT_INI_EFF = 5;
        public static final int VAS_DT_END_EFF = 5;
        public static final int VAS_COD_COMP_ANIA = 3;
        public static final int VAS_COD_FND = 20;
        public static final int VAS_TP_VAL_AST = 2;
        public static final int VAS_DS_RIGA = 6;
        public static final int VAS_DS_OPER_SQL = 1;
        public static final int VAS_DS_VER = 5;
        public static final int VAS_DS_TS_INI_CPTZ = 10;
        public static final int VAS_DS_TS_END_CPTZ = 10;
        public static final int VAS_DS_UTENTE = 20;
        public static final int VAS_DS_STATO_ELAB = 1;
        public static final int VAL_AST = VAS_ID_VAL_AST + VasIdTrchDiGar.Len.VAS_ID_TRCH_DI_GAR + VAS_ID_MOVI_FINRIO + VAS_ID_MOVI_CRZ + VasIdMoviChiu.Len.VAS_ID_MOVI_CHIU + VAS_DT_INI_EFF + VAS_DT_END_EFF + VAS_COD_COMP_ANIA + VAS_COD_FND + VasNumQuo.Len.VAS_NUM_QUO + VasValQuo.Len.VAS_VAL_QUO + VasDtValzz.Len.VAS_DT_VALZZ + VAS_TP_VAL_AST + VasIdRichInvstFnd.Len.VAS_ID_RICH_INVST_FND + VasIdRichDisFnd.Len.VAS_ID_RICH_DIS_FND + VAS_DS_RIGA + VAS_DS_OPER_SQL + VAS_DS_VER + VAS_DS_TS_INI_CPTZ + VAS_DS_TS_END_CPTZ + VAS_DS_UTENTE + VAS_DS_STATO_ELAB + VasPreMovto.Len.VAS_PRE_MOVTO + VasImpMovto.Len.VAS_IMP_MOVTO + VasPcInvDis.Len.VAS_PC_INV_DIS + VasNumQuoLorde.Len.VAS_NUM_QUO_LORDE + VasDtValzzCalc.Len.VAS_DT_VALZZ_CALC + VasMinusValenza.Len.VAS_MINUS_VALENZA + VasPlusValenza.Len.VAS_PLUS_VALENZA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_ID_VAL_AST = 9;
            public static final int VAS_ID_MOVI_FINRIO = 9;
            public static final int VAS_ID_MOVI_CRZ = 9;
            public static final int VAS_DT_INI_EFF = 8;
            public static final int VAS_DT_END_EFF = 8;
            public static final int VAS_COD_COMP_ANIA = 5;
            public static final int VAS_DS_RIGA = 10;
            public static final int VAS_DS_VER = 9;
            public static final int VAS_DS_TS_INI_CPTZ = 18;
            public static final int VAS_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
