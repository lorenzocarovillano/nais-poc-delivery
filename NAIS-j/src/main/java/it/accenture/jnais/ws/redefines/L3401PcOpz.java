package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-PC-OPZ<br>
 * Variable: L3401-PC-OPZ from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401PcOpz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401PcOpz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_PC_OPZ;
    }

    public void setL3401PcOpz(AfDecimal l3401PcOpz) {
        writeDecimalAsPacked(Pos.L3401_PC_OPZ, l3401PcOpz.copy());
    }

    public void setL3401PcOpzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_PC_OPZ, Pos.L3401_PC_OPZ);
    }

    /**Original name: L3401-PC-OPZ<br>*/
    public AfDecimal getL3401PcOpz() {
        return readPackedAsDecimal(Pos.L3401_PC_OPZ, Len.Int.L3401_PC_OPZ, Len.Fract.L3401_PC_OPZ);
    }

    public byte[] getL3401PcOpzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_PC_OPZ, Pos.L3401_PC_OPZ);
        return buffer;
    }

    /**Original name: L3401-PC-OPZ-NULL<br>*/
    public String getL3401PcOpzNull() {
        return readString(Pos.L3401_PC_OPZ_NULL, Len.L3401_PC_OPZ_NULL);
    }

    public String getL3401PcOpzNullFormatted() {
        return Functions.padBlanks(getL3401PcOpzNull(), Len.L3401_PC_OPZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_PC_OPZ = 1;
        public static final int L3401_PC_OPZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_PC_OPZ = 4;
        public static final int L3401_PC_OPZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3401_PC_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_PC_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
