package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ACQ-EXP<br>
 * Variable: TGA-ACQ-EXP from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ACQ_EXP;
    }

    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        writeDecimalAsPacked(Pos.TGA_ACQ_EXP, tgaAcqExp.copy());
    }

    public void setTgaAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ACQ_EXP, Pos.TGA_ACQ_EXP);
    }

    /**Original name: TGA-ACQ-EXP<br>*/
    public AfDecimal getTgaAcqExp() {
        return readPackedAsDecimal(Pos.TGA_ACQ_EXP, Len.Int.TGA_ACQ_EXP, Len.Fract.TGA_ACQ_EXP);
    }

    public byte[] getTgaAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ACQ_EXP, Pos.TGA_ACQ_EXP);
        return buffer;
    }

    public void setTgaAcqExpNull(String tgaAcqExpNull) {
        writeString(Pos.TGA_ACQ_EXP_NULL, tgaAcqExpNull, Len.TGA_ACQ_EXP_NULL);
    }

    /**Original name: TGA-ACQ-EXP-NULL<br>*/
    public String getTgaAcqExpNull() {
        return readString(Pos.TGA_ACQ_EXP_NULL, Len.TGA_ACQ_EXP_NULL);
    }

    public String getTgaAcqExpNullFormatted() {
        return Functions.padBlanks(getTgaAcqExpNull(), Len.TGA_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ACQ_EXP = 1;
        public static final int TGA_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ACQ_EXP = 8;
        public static final int TGA_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
