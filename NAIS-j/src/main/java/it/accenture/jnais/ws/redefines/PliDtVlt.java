package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PLI-DT-VLT<br>
 * Variable: PLI-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PliDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PliDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PLI_DT_VLT;
    }

    public void setPliDtVlt(int pliDtVlt) {
        writeIntAsPacked(Pos.PLI_DT_VLT, pliDtVlt, Len.Int.PLI_DT_VLT);
    }

    public void setPliDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PLI_DT_VLT, Pos.PLI_DT_VLT);
    }

    /**Original name: PLI-DT-VLT<br>*/
    public int getPliDtVlt() {
        return readPackedAsInt(Pos.PLI_DT_VLT, Len.Int.PLI_DT_VLT);
    }

    public byte[] getPliDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PLI_DT_VLT, Pos.PLI_DT_VLT);
        return buffer;
    }

    public void setPliDtVltNull(String pliDtVltNull) {
        writeString(Pos.PLI_DT_VLT_NULL, pliDtVltNull, Len.PLI_DT_VLT_NULL);
    }

    /**Original name: PLI-DT-VLT-NULL<br>*/
    public String getPliDtVltNull() {
        return readString(Pos.PLI_DT_VLT_NULL, Len.PLI_DT_VLT_NULL);
    }

    public String getPliDtVltNullFormatted() {
        return Functions.padBlanks(getPliDtVltNull(), Len.PLI_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PLI_DT_VLT = 1;
        public static final int PLI_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PLI_DT_VLT = 5;
        public static final int PLI_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PLI_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
