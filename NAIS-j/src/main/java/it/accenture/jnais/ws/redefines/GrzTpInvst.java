package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-TP-INVST<br>
 * Variable: GRZ-TP-INVST from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzTpInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzTpInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_TP_INVST;
    }

    public void setGrzTpInvst(short grzTpInvst) {
        writeShortAsPacked(Pos.GRZ_TP_INVST, grzTpInvst, Len.Int.GRZ_TP_INVST);
    }

    public void setGrzTpInvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_TP_INVST, Pos.GRZ_TP_INVST);
    }

    /**Original name: GRZ-TP-INVST<br>*/
    public short getGrzTpInvst() {
        return readPackedAsShort(Pos.GRZ_TP_INVST, Len.Int.GRZ_TP_INVST);
    }

    public byte[] getGrzTpInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_TP_INVST, Pos.GRZ_TP_INVST);
        return buffer;
    }

    public void setGrzTpInvstNull(String grzTpInvstNull) {
        writeString(Pos.GRZ_TP_INVST_NULL, grzTpInvstNull, Len.GRZ_TP_INVST_NULL);
    }

    /**Original name: GRZ-TP-INVST-NULL<br>*/
    public String getGrzTpInvstNull() {
        return readString(Pos.GRZ_TP_INVST_NULL, Len.GRZ_TP_INVST_NULL);
    }

    public String getGrzTpInvstNullFormatted() {
        return Functions.padBlanks(getGrzTpInvstNull(), Len.GRZ_TP_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_TP_INVST = 1;
        public static final int GRZ_TP_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_TP_INVST = 2;
        public static final int GRZ_TP_INVST_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_TP_INVST = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
