package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-VOLO<br>
 * Variable: L3421-IMP-VOLO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_VOLO;
    }

    public void setL3421ImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_VOLO, Pos.L3421_IMP_VOLO);
    }

    /**Original name: L3421-IMP-VOLO<br>*/
    public AfDecimal getL3421ImpVolo() {
        return readPackedAsDecimal(Pos.L3421_IMP_VOLO, Len.Int.L3421_IMP_VOLO, Len.Fract.L3421_IMP_VOLO);
    }

    public byte[] getL3421ImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_VOLO, Pos.L3421_IMP_VOLO);
        return buffer;
    }

    /**Original name: L3421-IMP-VOLO-NULL<br>*/
    public String getL3421ImpVoloNull() {
        return readString(Pos.L3421_IMP_VOLO_NULL, Len.L3421_IMP_VOLO_NULL);
    }

    public String getL3421ImpVoloNullFormatted() {
        return Functions.padBlanks(getL3421ImpVoloNull(), Len.L3421_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_VOLO = 1;
        public static final int L3421_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_VOLO = 8;
        public static final int L3421_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
