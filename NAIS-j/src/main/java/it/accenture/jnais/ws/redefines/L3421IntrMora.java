package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-INTR-MORA<br>
 * Variable: L3421-INTR-MORA from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421IntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421IntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_INTR_MORA;
    }

    public void setL3421IntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_INTR_MORA, Pos.L3421_INTR_MORA);
    }

    /**Original name: L3421-INTR-MORA<br>*/
    public AfDecimal getL3421IntrMora() {
        return readPackedAsDecimal(Pos.L3421_INTR_MORA, Len.Int.L3421_INTR_MORA, Len.Fract.L3421_INTR_MORA);
    }

    public byte[] getL3421IntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_INTR_MORA, Pos.L3421_INTR_MORA);
        return buffer;
    }

    /**Original name: L3421-INTR-MORA-NULL<br>*/
    public String getL3421IntrMoraNull() {
        return readString(Pos.L3421_INTR_MORA_NULL, Len.L3421_INTR_MORA_NULL);
    }

    public String getL3421IntrMoraNullFormatted() {
        return Functions.padBlanks(getL3421IntrMoraNull(), Len.L3421_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_INTR_MORA = 1;
        public static final int L3421_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_INTR_MORA = 8;
        public static final int L3421_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
