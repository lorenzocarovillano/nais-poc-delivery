package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-MIN-TRNUT<br>
 * Variable: L3421-MIN-TRNUT from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421MinTrnut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421MinTrnut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_MIN_TRNUT;
    }

    public void setL3421MinTrnutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_MIN_TRNUT, Pos.L3421_MIN_TRNUT);
    }

    /**Original name: L3421-MIN-TRNUT<br>*/
    public AfDecimal getL3421MinTrnut() {
        return readPackedAsDecimal(Pos.L3421_MIN_TRNUT, Len.Int.L3421_MIN_TRNUT, Len.Fract.L3421_MIN_TRNUT);
    }

    public byte[] getL3421MinTrnutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_MIN_TRNUT, Pos.L3421_MIN_TRNUT);
        return buffer;
    }

    /**Original name: L3421-MIN-TRNUT-NULL<br>*/
    public String getL3421MinTrnutNull() {
        return readString(Pos.L3421_MIN_TRNUT_NULL, Len.L3421_MIN_TRNUT_NULL);
    }

    public String getL3421MinTrnutNullFormatted() {
        return Functions.padBlanks(getL3421MinTrnutNull(), Len.L3421_MIN_TRNUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_MIN_TRNUT = 1;
        public static final int L3421_MIN_TRNUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_MIN_TRNUT = 8;
        public static final int L3421_MIN_TRNUT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_MIN_TRNUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_MIN_TRNUT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
