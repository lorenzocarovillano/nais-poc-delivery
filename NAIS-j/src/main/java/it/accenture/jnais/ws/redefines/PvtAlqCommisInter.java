package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ALQ-COMMIS-INTER<br>
 * Variable: PVT-ALQ-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtAlqCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtAlqCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ALQ_COMMIS_INTER;
    }

    public void setPvtAlqCommisInter(AfDecimal pvtAlqCommisInter) {
        writeDecimalAsPacked(Pos.PVT_ALQ_COMMIS_INTER, pvtAlqCommisInter.copy());
    }

    public void setPvtAlqCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ALQ_COMMIS_INTER, Pos.PVT_ALQ_COMMIS_INTER);
    }

    /**Original name: PVT-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getPvtAlqCommisInter() {
        return readPackedAsDecimal(Pos.PVT_ALQ_COMMIS_INTER, Len.Int.PVT_ALQ_COMMIS_INTER, Len.Fract.PVT_ALQ_COMMIS_INTER);
    }

    public byte[] getPvtAlqCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ALQ_COMMIS_INTER, Pos.PVT_ALQ_COMMIS_INTER);
        return buffer;
    }

    public void setPvtAlqCommisInterNull(String pvtAlqCommisInterNull) {
        writeString(Pos.PVT_ALQ_COMMIS_INTER_NULL, pvtAlqCommisInterNull, Len.PVT_ALQ_COMMIS_INTER_NULL);
    }

    /**Original name: PVT-ALQ-COMMIS-INTER-NULL<br>*/
    public String getPvtAlqCommisInterNull() {
        return readString(Pos.PVT_ALQ_COMMIS_INTER_NULL, Len.PVT_ALQ_COMMIS_INTER_NULL);
    }

    public String getPvtAlqCommisInterNullFormatted() {
        return Functions.padBlanks(getPvtAlqCommisInterNull(), Len.PVT_ALQ_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_COMMIS_INTER = 1;
        public static final int PVT_ALQ_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_COMMIS_INTER = 4;
        public static final int PVT_ALQ_COMMIS_INTER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
