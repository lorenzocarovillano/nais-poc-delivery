package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MVV-IDP-MATR-VAL-VAR<br>
 * Variable: MVV-IDP-MATR-VAL-VAR from program LDBS1390<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MvvIdpMatrValVar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MvvIdpMatrValVar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MVV_IDP_MATR_VAL_VAR;
    }

    public void setMvvIdpMatrValVar(int mvvIdpMatrValVar) {
        writeBinaryInt(Pos.MVV_IDP_MATR_VAL_VAR, mvvIdpMatrValVar);
    }

    public void setMvvIdpMatrValVarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Types.INT_SIZE, Pos.MVV_IDP_MATR_VAL_VAR);
    }

    /**Original name: MVV-IDP-MATR-VAL-VAR<br>*/
    public int getMvvIdpMatrValVar() {
        return readBinaryInt(Pos.MVV_IDP_MATR_VAL_VAR);
    }

    public byte[] getMvvIdpMatrValVarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Types.INT_SIZE, Pos.MVV_IDP_MATR_VAL_VAR);
        return buffer;
    }

    public void setMvvIdpMatrValVarNull(String mvvIdpMatrValVarNull) {
        writeString(Pos.MVV_IDP_MATR_VAL_VAR_NULL, mvvIdpMatrValVarNull, Len.MVV_IDP_MATR_VAL_VAR_NULL);
    }

    /**Original name: MVV-IDP-MATR-VAL-VAR-NULL<br>*/
    public String getMvvIdpMatrValVarNull() {
        return readString(Pos.MVV_IDP_MATR_VAL_VAR_NULL, Len.MVV_IDP_MATR_VAL_VAR_NULL);
    }

    public String getMvvIdpMatrValVarNullFormatted() {
        return Functions.padBlanks(getMvvIdpMatrValVarNull(), Len.MVV_IDP_MATR_VAL_VAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MVV_IDP_MATR_VAL_VAR = 1;
        public static final int MVV_IDP_MATR_VAL_VAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MVV_IDP_MATR_VAL_VAR = 4;
        public static final int MVV_IDP_MATR_VAL_VAR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
