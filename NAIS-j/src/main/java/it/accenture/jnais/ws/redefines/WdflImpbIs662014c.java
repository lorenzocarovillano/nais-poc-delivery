package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-IS-662014C<br>
 * Variable: WDFL-IMPB-IS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbIs662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbIs662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_IS662014C;
    }

    public void setWdflImpbIs662014c(AfDecimal wdflImpbIs662014c) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_IS662014C, wdflImpbIs662014c.copy());
    }

    public void setWdflImpbIs662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_IS662014C, Pos.WDFL_IMPB_IS662014C);
    }

    /**Original name: WDFL-IMPB-IS-662014C<br>*/
    public AfDecimal getWdflImpbIs662014c() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_IS662014C, Len.Int.WDFL_IMPB_IS662014C, Len.Fract.WDFL_IMPB_IS662014C);
    }

    public byte[] getWdflImpbIs662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_IS662014C, Pos.WDFL_IMPB_IS662014C);
        return buffer;
    }

    public void setWdflImpbIs662014cNull(String wdflImpbIs662014cNull) {
        writeString(Pos.WDFL_IMPB_IS662014C_NULL, wdflImpbIs662014cNull, Len.WDFL_IMPB_IS662014C_NULL);
    }

    /**Original name: WDFL-IMPB-IS-662014C-NULL<br>*/
    public String getWdflImpbIs662014cNull() {
        return readString(Pos.WDFL_IMPB_IS662014C_NULL, Len.WDFL_IMPB_IS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS662014C = 1;
        public static final int WDFL_IMPB_IS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_IS662014C = 8;
        public static final int WDFL_IMPB_IS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_IS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
