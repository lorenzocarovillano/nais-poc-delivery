package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-CREDITO-IS<br>
 * Variable: DFA-CREDITO-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaCreditoIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaCreditoIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_CREDITO_IS;
    }

    public void setDfaCreditoIs(AfDecimal dfaCreditoIs) {
        writeDecimalAsPacked(Pos.DFA_CREDITO_IS, dfaCreditoIs.copy());
    }

    public void setDfaCreditoIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_CREDITO_IS, Pos.DFA_CREDITO_IS);
    }

    /**Original name: DFA-CREDITO-IS<br>*/
    public AfDecimal getDfaCreditoIs() {
        return readPackedAsDecimal(Pos.DFA_CREDITO_IS, Len.Int.DFA_CREDITO_IS, Len.Fract.DFA_CREDITO_IS);
    }

    public byte[] getDfaCreditoIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_CREDITO_IS, Pos.DFA_CREDITO_IS);
        return buffer;
    }

    public void setDfaCreditoIsNull(String dfaCreditoIsNull) {
        writeString(Pos.DFA_CREDITO_IS_NULL, dfaCreditoIsNull, Len.DFA_CREDITO_IS_NULL);
    }

    /**Original name: DFA-CREDITO-IS-NULL<br>*/
    public String getDfaCreditoIsNull() {
        return readString(Pos.DFA_CREDITO_IS_NULL, Len.DFA_CREDITO_IS_NULL);
    }

    public String getDfaCreditoIsNullFormatted() {
        return Functions.padBlanks(getDfaCreditoIsNull(), Len.DFA_CREDITO_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_CREDITO_IS = 1;
        public static final int DFA_CREDITO_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_CREDITO_IS = 8;
        public static final int DFA_CREDITO_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_CREDITO_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_CREDITO_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
