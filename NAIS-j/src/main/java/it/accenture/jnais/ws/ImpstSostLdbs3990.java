package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.IsoAlqIs;
import it.accenture.jnais.ws.redefines.IsoCumPreVers;
import it.accenture.jnais.ws.redefines.IsoDtEndPer;
import it.accenture.jnais.ws.redefines.IsoDtIniPer;
import it.accenture.jnais.ws.redefines.IsoIdMoviChiu;
import it.accenture.jnais.ws.redefines.IsoIdOgg;
import it.accenture.jnais.ws.redefines.IsoImpbIs;
import it.accenture.jnais.ws.redefines.IsoImpstSost;
import it.accenture.jnais.ws.redefines.IsoPrstzLrdAnteIs;
import it.accenture.jnais.ws.redefines.IsoPrstzNet;
import it.accenture.jnais.ws.redefines.IsoPrstzPrec;
import it.accenture.jnais.ws.redefines.IsoRisMatAnteTax;
import it.accenture.jnais.ws.redefines.IsoRisMatNetPrec;
import it.accenture.jnais.ws.redefines.IsoRisMatPostTax;

/**Original name: IMPST-SOST<br>
 * Variable: IMPST-SOST from copybook IDBVISO1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ImpstSostLdbs3990 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: ISO-ID-IMPST-SOST
    private int isoIdImpstSost = DefaultValues.INT_VAL;
    //Original name: ISO-ID-OGG
    private IsoIdOgg isoIdOgg = new IsoIdOgg();
    //Original name: ISO-TP-OGG
    private String isoTpOgg = DefaultValues.stringVal(Len.ISO_TP_OGG);
    //Original name: ISO-ID-MOVI-CRZ
    private int isoIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: ISO-ID-MOVI-CHIU
    private IsoIdMoviChiu isoIdMoviChiu = new IsoIdMoviChiu();
    //Original name: ISO-DT-INI-EFF
    private int isoDtIniEff = DefaultValues.INT_VAL;
    //Original name: ISO-DT-END-EFF
    private int isoDtEndEff = DefaultValues.INT_VAL;
    //Original name: ISO-DT-INI-PER
    private IsoDtIniPer isoDtIniPer = new IsoDtIniPer();
    //Original name: ISO-DT-END-PER
    private IsoDtEndPer isoDtEndPer = new IsoDtEndPer();
    //Original name: ISO-COD-COMP-ANIA
    private int isoCodCompAnia = DefaultValues.INT_VAL;
    //Original name: ISO-IMPST-SOST
    private IsoImpstSost isoImpstSost = new IsoImpstSost();
    //Original name: ISO-IMPB-IS
    private IsoImpbIs isoImpbIs = new IsoImpbIs();
    //Original name: ISO-ALQ-IS
    private IsoAlqIs isoAlqIs = new IsoAlqIs();
    //Original name: ISO-COD-TRB
    private String isoCodTrb = DefaultValues.stringVal(Len.ISO_COD_TRB);
    //Original name: ISO-PRSTZ-LRD-ANTE-IS
    private IsoPrstzLrdAnteIs isoPrstzLrdAnteIs = new IsoPrstzLrdAnteIs();
    //Original name: ISO-RIS-MAT-NET-PREC
    private IsoRisMatNetPrec isoRisMatNetPrec = new IsoRisMatNetPrec();
    //Original name: ISO-RIS-MAT-ANTE-TAX
    private IsoRisMatAnteTax isoRisMatAnteTax = new IsoRisMatAnteTax();
    //Original name: ISO-RIS-MAT-POST-TAX
    private IsoRisMatPostTax isoRisMatPostTax = new IsoRisMatPostTax();
    //Original name: ISO-PRSTZ-NET
    private IsoPrstzNet isoPrstzNet = new IsoPrstzNet();
    //Original name: ISO-PRSTZ-PREC
    private IsoPrstzPrec isoPrstzPrec = new IsoPrstzPrec();
    //Original name: ISO-CUM-PRE-VERS
    private IsoCumPreVers isoCumPreVers = new IsoCumPreVers();
    //Original name: ISO-TP-CALC-IMPST
    private String isoTpCalcImpst = DefaultValues.stringVal(Len.ISO_TP_CALC_IMPST);
    //Original name: ISO-IMP-GIA-TASSATO
    private AfDecimal isoImpGiaTassato = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: ISO-DS-RIGA
    private long isoDsRiga = DefaultValues.LONG_VAL;
    //Original name: ISO-DS-OPER-SQL
    private char isoDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: ISO-DS-VER
    private int isoDsVer = DefaultValues.INT_VAL;
    //Original name: ISO-DS-TS-INI-CPTZ
    private long isoDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: ISO-DS-TS-END-CPTZ
    private long isoDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: ISO-DS-UTENTE
    private String isoDsUtente = DefaultValues.stringVal(Len.ISO_DS_UTENTE);
    //Original name: ISO-DS-STATO-ELAB
    private char isoDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IMPST_SOST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setImpstSostBytes(buf);
    }

    public void setImpstSostFormatted(String data) {
        byte[] buffer = new byte[Len.IMPST_SOST];
        MarshalByte.writeString(buffer, 1, data, Len.IMPST_SOST);
        setImpstSostBytes(buffer, 1);
    }

    public String getImpstSostFormatted() {
        return MarshalByteExt.bufferToStr(getImpstSostBytes());
    }

    public void setImpstSostBytes(byte[] buffer) {
        setImpstSostBytes(buffer, 1);
    }

    public byte[] getImpstSostBytes() {
        byte[] buffer = new byte[Len.IMPST_SOST];
        return getImpstSostBytes(buffer, 1);
    }

    public void setImpstSostBytes(byte[] buffer, int offset) {
        int position = offset;
        isoIdImpstSost = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ISO_ID_IMPST_SOST, 0);
        position += Len.ISO_ID_IMPST_SOST;
        isoIdOgg.setIsoIdOggFromBuffer(buffer, position);
        position += IsoIdOgg.Len.ISO_ID_OGG;
        isoTpOgg = MarshalByte.readString(buffer, position, Len.ISO_TP_OGG);
        position += Len.ISO_TP_OGG;
        isoIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ISO_ID_MOVI_CRZ, 0);
        position += Len.ISO_ID_MOVI_CRZ;
        isoIdMoviChiu.setIsoIdMoviChiuFromBuffer(buffer, position);
        position += IsoIdMoviChiu.Len.ISO_ID_MOVI_CHIU;
        isoDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ISO_DT_INI_EFF, 0);
        position += Len.ISO_DT_INI_EFF;
        isoDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ISO_DT_END_EFF, 0);
        position += Len.ISO_DT_END_EFF;
        isoDtIniPer.setIsoDtIniPerFromBuffer(buffer, position);
        position += IsoDtIniPer.Len.ISO_DT_INI_PER;
        isoDtEndPer.setIsoDtEndPerFromBuffer(buffer, position);
        position += IsoDtEndPer.Len.ISO_DT_END_PER;
        isoCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ISO_COD_COMP_ANIA, 0);
        position += Len.ISO_COD_COMP_ANIA;
        isoImpstSost.setIsoImpstSostFromBuffer(buffer, position);
        position += IsoImpstSost.Len.ISO_IMPST_SOST;
        isoImpbIs.setIsoImpbIsFromBuffer(buffer, position);
        position += IsoImpbIs.Len.ISO_IMPB_IS;
        isoAlqIs.setIsoAlqIsFromBuffer(buffer, position);
        position += IsoAlqIs.Len.ISO_ALQ_IS;
        isoCodTrb = MarshalByte.readString(buffer, position, Len.ISO_COD_TRB);
        position += Len.ISO_COD_TRB;
        isoPrstzLrdAnteIs.setIsoPrstzLrdAnteIsFromBuffer(buffer, position);
        position += IsoPrstzLrdAnteIs.Len.ISO_PRSTZ_LRD_ANTE_IS;
        isoRisMatNetPrec.setIsoRisMatNetPrecFromBuffer(buffer, position);
        position += IsoRisMatNetPrec.Len.ISO_RIS_MAT_NET_PREC;
        isoRisMatAnteTax.setIsoRisMatAnteTaxFromBuffer(buffer, position);
        position += IsoRisMatAnteTax.Len.ISO_RIS_MAT_ANTE_TAX;
        isoRisMatPostTax.setIsoRisMatPostTaxFromBuffer(buffer, position);
        position += IsoRisMatPostTax.Len.ISO_RIS_MAT_POST_TAX;
        isoPrstzNet.setIsoPrstzNetFromBuffer(buffer, position);
        position += IsoPrstzNet.Len.ISO_PRSTZ_NET;
        isoPrstzPrec.setIsoPrstzPrecFromBuffer(buffer, position);
        position += IsoPrstzPrec.Len.ISO_PRSTZ_PREC;
        isoCumPreVers.setIsoCumPreVersFromBuffer(buffer, position);
        position += IsoCumPreVers.Len.ISO_CUM_PRE_VERS;
        isoTpCalcImpst = MarshalByte.readString(buffer, position, Len.ISO_TP_CALC_IMPST);
        position += Len.ISO_TP_CALC_IMPST;
        isoImpGiaTassato.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.ISO_IMP_GIA_TASSATO, Len.Fract.ISO_IMP_GIA_TASSATO));
        position += Len.ISO_IMP_GIA_TASSATO;
        isoDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ISO_DS_RIGA, 0);
        position += Len.ISO_DS_RIGA;
        isoDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        isoDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ISO_DS_VER, 0);
        position += Len.ISO_DS_VER;
        isoDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ISO_DS_TS_INI_CPTZ, 0);
        position += Len.ISO_DS_TS_INI_CPTZ;
        isoDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.ISO_DS_TS_END_CPTZ, 0);
        position += Len.ISO_DS_TS_END_CPTZ;
        isoDsUtente = MarshalByte.readString(buffer, position, Len.ISO_DS_UTENTE);
        position += Len.ISO_DS_UTENTE;
        isoDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getImpstSostBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, isoIdImpstSost, Len.Int.ISO_ID_IMPST_SOST, 0);
        position += Len.ISO_ID_IMPST_SOST;
        isoIdOgg.getIsoIdOggAsBuffer(buffer, position);
        position += IsoIdOgg.Len.ISO_ID_OGG;
        MarshalByte.writeString(buffer, position, isoTpOgg, Len.ISO_TP_OGG);
        position += Len.ISO_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, isoIdMoviCrz, Len.Int.ISO_ID_MOVI_CRZ, 0);
        position += Len.ISO_ID_MOVI_CRZ;
        isoIdMoviChiu.getIsoIdMoviChiuAsBuffer(buffer, position);
        position += IsoIdMoviChiu.Len.ISO_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, isoDtIniEff, Len.Int.ISO_DT_INI_EFF, 0);
        position += Len.ISO_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, isoDtEndEff, Len.Int.ISO_DT_END_EFF, 0);
        position += Len.ISO_DT_END_EFF;
        isoDtIniPer.getIsoDtIniPerAsBuffer(buffer, position);
        position += IsoDtIniPer.Len.ISO_DT_INI_PER;
        isoDtEndPer.getIsoDtEndPerAsBuffer(buffer, position);
        position += IsoDtEndPer.Len.ISO_DT_END_PER;
        MarshalByte.writeIntAsPacked(buffer, position, isoCodCompAnia, Len.Int.ISO_COD_COMP_ANIA, 0);
        position += Len.ISO_COD_COMP_ANIA;
        isoImpstSost.getIsoImpstSostAsBuffer(buffer, position);
        position += IsoImpstSost.Len.ISO_IMPST_SOST;
        isoImpbIs.getIsoImpbIsAsBuffer(buffer, position);
        position += IsoImpbIs.Len.ISO_IMPB_IS;
        isoAlqIs.getIsoAlqIsAsBuffer(buffer, position);
        position += IsoAlqIs.Len.ISO_ALQ_IS;
        MarshalByte.writeString(buffer, position, isoCodTrb, Len.ISO_COD_TRB);
        position += Len.ISO_COD_TRB;
        isoPrstzLrdAnteIs.getIsoPrstzLrdAnteIsAsBuffer(buffer, position);
        position += IsoPrstzLrdAnteIs.Len.ISO_PRSTZ_LRD_ANTE_IS;
        isoRisMatNetPrec.getIsoRisMatNetPrecAsBuffer(buffer, position);
        position += IsoRisMatNetPrec.Len.ISO_RIS_MAT_NET_PREC;
        isoRisMatAnteTax.getIsoRisMatAnteTaxAsBuffer(buffer, position);
        position += IsoRisMatAnteTax.Len.ISO_RIS_MAT_ANTE_TAX;
        isoRisMatPostTax.getIsoRisMatPostTaxAsBuffer(buffer, position);
        position += IsoRisMatPostTax.Len.ISO_RIS_MAT_POST_TAX;
        isoPrstzNet.getIsoPrstzNetAsBuffer(buffer, position);
        position += IsoPrstzNet.Len.ISO_PRSTZ_NET;
        isoPrstzPrec.getIsoPrstzPrecAsBuffer(buffer, position);
        position += IsoPrstzPrec.Len.ISO_PRSTZ_PREC;
        isoCumPreVers.getIsoCumPreVersAsBuffer(buffer, position);
        position += IsoCumPreVers.Len.ISO_CUM_PRE_VERS;
        MarshalByte.writeString(buffer, position, isoTpCalcImpst, Len.ISO_TP_CALC_IMPST);
        position += Len.ISO_TP_CALC_IMPST;
        MarshalByte.writeDecimalAsPacked(buffer, position, isoImpGiaTassato.copy());
        position += Len.ISO_IMP_GIA_TASSATO;
        MarshalByte.writeLongAsPacked(buffer, position, isoDsRiga, Len.Int.ISO_DS_RIGA, 0);
        position += Len.ISO_DS_RIGA;
        MarshalByte.writeChar(buffer, position, isoDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, isoDsVer, Len.Int.ISO_DS_VER, 0);
        position += Len.ISO_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, isoDsTsIniCptz, Len.Int.ISO_DS_TS_INI_CPTZ, 0);
        position += Len.ISO_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, isoDsTsEndCptz, Len.Int.ISO_DS_TS_END_CPTZ, 0);
        position += Len.ISO_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, isoDsUtente, Len.ISO_DS_UTENTE);
        position += Len.ISO_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, isoDsStatoElab);
        return buffer;
    }

    public void setIsoIdImpstSost(int isoIdImpstSost) {
        this.isoIdImpstSost = isoIdImpstSost;
    }

    public int getIsoIdImpstSost() {
        return this.isoIdImpstSost;
    }

    public void setIsoTpOgg(String isoTpOgg) {
        this.isoTpOgg = Functions.subString(isoTpOgg, Len.ISO_TP_OGG);
    }

    public String getIsoTpOgg() {
        return this.isoTpOgg;
    }

    public void setIsoIdMoviCrz(int isoIdMoviCrz) {
        this.isoIdMoviCrz = isoIdMoviCrz;
    }

    public int getIsoIdMoviCrz() {
        return this.isoIdMoviCrz;
    }

    public void setIsoDtIniEff(int isoDtIniEff) {
        this.isoDtIniEff = isoDtIniEff;
    }

    public int getIsoDtIniEff() {
        return this.isoDtIniEff;
    }

    public void setIsoDtEndEff(int isoDtEndEff) {
        this.isoDtEndEff = isoDtEndEff;
    }

    public int getIsoDtEndEff() {
        return this.isoDtEndEff;
    }

    public void setIsoCodCompAnia(int isoCodCompAnia) {
        this.isoCodCompAnia = isoCodCompAnia;
    }

    public int getIsoCodCompAnia() {
        return this.isoCodCompAnia;
    }

    public void setIsoCodTrb(String isoCodTrb) {
        this.isoCodTrb = Functions.subString(isoCodTrb, Len.ISO_COD_TRB);
    }

    public String getIsoCodTrb() {
        return this.isoCodTrb;
    }

    public String getIsoCodTrbFormatted() {
        return Functions.padBlanks(getIsoCodTrb(), Len.ISO_COD_TRB);
    }

    public void setIsoTpCalcImpst(String isoTpCalcImpst) {
        this.isoTpCalcImpst = Functions.subString(isoTpCalcImpst, Len.ISO_TP_CALC_IMPST);
    }

    public String getIsoTpCalcImpst() {
        return this.isoTpCalcImpst;
    }

    public void setIsoImpGiaTassato(AfDecimal isoImpGiaTassato) {
        this.isoImpGiaTassato.assign(isoImpGiaTassato);
    }

    public AfDecimal getIsoImpGiaTassato() {
        return this.isoImpGiaTassato.copy();
    }

    public void setIsoDsRiga(long isoDsRiga) {
        this.isoDsRiga = isoDsRiga;
    }

    public long getIsoDsRiga() {
        return this.isoDsRiga;
    }

    public void setIsoDsOperSql(char isoDsOperSql) {
        this.isoDsOperSql = isoDsOperSql;
    }

    public void setIsoDsOperSqlFormatted(String isoDsOperSql) {
        setIsoDsOperSql(Functions.charAt(isoDsOperSql, Types.CHAR_SIZE));
    }

    public char getIsoDsOperSql() {
        return this.isoDsOperSql;
    }

    public void setIsoDsVer(int isoDsVer) {
        this.isoDsVer = isoDsVer;
    }

    public int getIsoDsVer() {
        return this.isoDsVer;
    }

    public void setIsoDsTsIniCptz(long isoDsTsIniCptz) {
        this.isoDsTsIniCptz = isoDsTsIniCptz;
    }

    public long getIsoDsTsIniCptz() {
        return this.isoDsTsIniCptz;
    }

    public void setIsoDsTsEndCptz(long isoDsTsEndCptz) {
        this.isoDsTsEndCptz = isoDsTsEndCptz;
    }

    public long getIsoDsTsEndCptz() {
        return this.isoDsTsEndCptz;
    }

    public void setIsoDsUtente(String isoDsUtente) {
        this.isoDsUtente = Functions.subString(isoDsUtente, Len.ISO_DS_UTENTE);
    }

    public String getIsoDsUtente() {
        return this.isoDsUtente;
    }

    public void setIsoDsStatoElab(char isoDsStatoElab) {
        this.isoDsStatoElab = isoDsStatoElab;
    }

    public void setIsoDsStatoElabFormatted(String isoDsStatoElab) {
        setIsoDsStatoElab(Functions.charAt(isoDsStatoElab, Types.CHAR_SIZE));
    }

    public char getIsoDsStatoElab() {
        return this.isoDsStatoElab;
    }

    public IsoAlqIs getIsoAlqIs() {
        return isoAlqIs;
    }

    public IsoCumPreVers getIsoCumPreVers() {
        return isoCumPreVers;
    }

    public IsoDtEndPer getIsoDtEndPer() {
        return isoDtEndPer;
    }

    public IsoDtIniPer getIsoDtIniPer() {
        return isoDtIniPer;
    }

    public IsoIdMoviChiu getIsoIdMoviChiu() {
        return isoIdMoviChiu;
    }

    public IsoIdOgg getIsoIdOgg() {
        return isoIdOgg;
    }

    public IsoImpbIs getIsoImpbIs() {
        return isoImpbIs;
    }

    public IsoImpstSost getIsoImpstSost() {
        return isoImpstSost;
    }

    public IsoPrstzLrdAnteIs getIsoPrstzLrdAnteIs() {
        return isoPrstzLrdAnteIs;
    }

    public IsoPrstzNet getIsoPrstzNet() {
        return isoPrstzNet;
    }

    public IsoPrstzPrec getIsoPrstzPrec() {
        return isoPrstzPrec;
    }

    public IsoRisMatAnteTax getIsoRisMatAnteTax() {
        return isoRisMatAnteTax;
    }

    public IsoRisMatNetPrec getIsoRisMatNetPrec() {
        return isoRisMatNetPrec;
    }

    public IsoRisMatPostTax getIsoRisMatPostTax() {
        return isoRisMatPostTax;
    }

    @Override
    public byte[] serialize() {
        return getImpstSostBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_ID_IMPST_SOST = 5;
        public static final int ISO_TP_OGG = 2;
        public static final int ISO_ID_MOVI_CRZ = 5;
        public static final int ISO_DT_INI_EFF = 5;
        public static final int ISO_DT_END_EFF = 5;
        public static final int ISO_COD_COMP_ANIA = 3;
        public static final int ISO_COD_TRB = 20;
        public static final int ISO_TP_CALC_IMPST = 2;
        public static final int ISO_IMP_GIA_TASSATO = 8;
        public static final int ISO_DS_RIGA = 6;
        public static final int ISO_DS_OPER_SQL = 1;
        public static final int ISO_DS_VER = 5;
        public static final int ISO_DS_TS_INI_CPTZ = 10;
        public static final int ISO_DS_TS_END_CPTZ = 10;
        public static final int ISO_DS_UTENTE = 20;
        public static final int ISO_DS_STATO_ELAB = 1;
        public static final int IMPST_SOST = ISO_ID_IMPST_SOST + IsoIdOgg.Len.ISO_ID_OGG + ISO_TP_OGG + ISO_ID_MOVI_CRZ + IsoIdMoviChiu.Len.ISO_ID_MOVI_CHIU + ISO_DT_INI_EFF + ISO_DT_END_EFF + IsoDtIniPer.Len.ISO_DT_INI_PER + IsoDtEndPer.Len.ISO_DT_END_PER + ISO_COD_COMP_ANIA + IsoImpstSost.Len.ISO_IMPST_SOST + IsoImpbIs.Len.ISO_IMPB_IS + IsoAlqIs.Len.ISO_ALQ_IS + ISO_COD_TRB + IsoPrstzLrdAnteIs.Len.ISO_PRSTZ_LRD_ANTE_IS + IsoRisMatNetPrec.Len.ISO_RIS_MAT_NET_PREC + IsoRisMatAnteTax.Len.ISO_RIS_MAT_ANTE_TAX + IsoRisMatPostTax.Len.ISO_RIS_MAT_POST_TAX + IsoPrstzNet.Len.ISO_PRSTZ_NET + IsoPrstzPrec.Len.ISO_PRSTZ_PREC + IsoCumPreVers.Len.ISO_CUM_PRE_VERS + ISO_TP_CALC_IMPST + ISO_IMP_GIA_TASSATO + ISO_DS_RIGA + ISO_DS_OPER_SQL + ISO_DS_VER + ISO_DS_TS_INI_CPTZ + ISO_DS_TS_END_CPTZ + ISO_DS_UTENTE + ISO_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_ID_IMPST_SOST = 9;
            public static final int ISO_ID_MOVI_CRZ = 9;
            public static final int ISO_DT_INI_EFF = 8;
            public static final int ISO_DT_END_EFF = 8;
            public static final int ISO_COD_COMP_ANIA = 5;
            public static final int ISO_IMP_GIA_TASSATO = 12;
            public static final int ISO_DS_RIGA = 10;
            public static final int ISO_DS_VER = 9;
            public static final int ISO_DS_TS_INI_CPTZ = 18;
            public static final int ISO_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_IMP_GIA_TASSATO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
