package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-VIS-662014<br>
 * Variable: WBEL-IMPST-VIS-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_VIS662014;
    }

    public void setWbelImpstVis662014(AfDecimal wbelImpstVis662014) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_VIS662014, wbelImpstVis662014.copy());
    }

    public void setWbelImpstVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_VIS662014, Pos.WBEL_IMPST_VIS662014);
    }

    /**Original name: WBEL-IMPST-VIS-662014<br>*/
    public AfDecimal getWbelImpstVis662014() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_VIS662014, Len.Int.WBEL_IMPST_VIS662014, Len.Fract.WBEL_IMPST_VIS662014);
    }

    public byte[] getWbelImpstVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_VIS662014, Pos.WBEL_IMPST_VIS662014);
        return buffer;
    }

    public void initWbelImpstVis662014Spaces() {
        fill(Pos.WBEL_IMPST_VIS662014, Len.WBEL_IMPST_VIS662014, Types.SPACE_CHAR);
    }

    public void setWbelImpstVis662014Null(String wbelImpstVis662014Null) {
        writeString(Pos.WBEL_IMPST_VIS662014_NULL, wbelImpstVis662014Null, Len.WBEL_IMPST_VIS662014_NULL);
    }

    /**Original name: WBEL-IMPST-VIS-662014-NULL<br>*/
    public String getWbelImpstVis662014Null() {
        return readString(Pos.WBEL_IMPST_VIS662014_NULL, Len.WBEL_IMPST_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_VIS662014 = 1;
        public static final int WBEL_IMPST_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_VIS662014 = 8;
        public static final int WBEL_IMPST_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
