package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: ITSO0041<br>
 * Variable: ITSO0041 from program LLBM0230<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Itso0041 extends SerializableParameter {

    //==== PROPERTIES ====
    /**Original name: ITSO0041-DESC-BUSINESS<br>
	 * <pre>---------------------------------------------------------------*
	 *  AREA Output da dare in Input ad un 'eventuale
	 *  Servizio Secondario
	 * ---------------------------------------------------------------*</pre>*/
    private char itso0041DescBusiness = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ITSO0041;
    }

    @Override
    public void deserialize(byte[] buf) {
        setItso0041Bytes(buf);
    }

    public String getItso0041Formatted() {
        return String.valueOf(getItso0041DescBusiness());
    }

    public void setItso0041Bytes(byte[] buffer) {
        setItso0041Bytes(buffer, 1);
    }

    public byte[] getItso0041Bytes() {
        byte[] buffer = new byte[Len.ITSO0041];
        return getItso0041Bytes(buffer, 1);
    }

    public void setItso0041Bytes(byte[] buffer, int offset) {
        int position = offset;
        itso0041DescBusiness = MarshalByte.readChar(buffer, position);
    }

    public byte[] getItso0041Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, itso0041DescBusiness);
        return buffer;
    }

    public void setItso0041DescBusiness(char itso0041DescBusiness) {
        this.itso0041DescBusiness = itso0041DescBusiness;
    }

    public char getItso0041DescBusiness() {
        return this.itso0041DescBusiness;
    }

    @Override
    public byte[] serialize() {
        return getItso0041Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ITSO0041_DESC_BUSINESS = 1;
        public static final int ITSO0041 = ITSO0041_DESC_BUSINESS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
