package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PROV-RICOR<br>
 * Variable: DTR-PROV-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PROV_RICOR;
    }

    public void setDtrProvRicor(AfDecimal dtrProvRicor) {
        writeDecimalAsPacked(Pos.DTR_PROV_RICOR, dtrProvRicor.copy());
    }

    public void setDtrProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PROV_RICOR, Pos.DTR_PROV_RICOR);
    }

    /**Original name: DTR-PROV-RICOR<br>*/
    public AfDecimal getDtrProvRicor() {
        return readPackedAsDecimal(Pos.DTR_PROV_RICOR, Len.Int.DTR_PROV_RICOR, Len.Fract.DTR_PROV_RICOR);
    }

    public byte[] getDtrProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PROV_RICOR, Pos.DTR_PROV_RICOR);
        return buffer;
    }

    public void setDtrProvRicorNull(String dtrProvRicorNull) {
        writeString(Pos.DTR_PROV_RICOR_NULL, dtrProvRicorNull, Len.DTR_PROV_RICOR_NULL);
    }

    /**Original name: DTR-PROV-RICOR-NULL<br>*/
    public String getDtrProvRicorNull() {
        return readString(Pos.DTR_PROV_RICOR_NULL, Len.DTR_PROV_RICOR_NULL);
    }

    public String getDtrProvRicorNullFormatted() {
        return Functions.padBlanks(getDtrProvRicorNull(), Len.DTR_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PROV_RICOR = 1;
        public static final int DTR_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PROV_RICOR = 8;
        public static final int DTR_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
