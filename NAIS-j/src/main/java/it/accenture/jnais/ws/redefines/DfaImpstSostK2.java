package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-SOST-K2<br>
 * Variable: DFA-IMPST-SOST-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstSostK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstSostK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_SOST_K2;
    }

    public void setDfaImpstSostK2(AfDecimal dfaImpstSostK2) {
        writeDecimalAsPacked(Pos.DFA_IMPST_SOST_K2, dfaImpstSostK2.copy());
    }

    public void setDfaImpstSostK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_SOST_K2, Pos.DFA_IMPST_SOST_K2);
    }

    /**Original name: DFA-IMPST-SOST-K2<br>*/
    public AfDecimal getDfaImpstSostK2() {
        return readPackedAsDecimal(Pos.DFA_IMPST_SOST_K2, Len.Int.DFA_IMPST_SOST_K2, Len.Fract.DFA_IMPST_SOST_K2);
    }

    public byte[] getDfaImpstSostK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_SOST_K2, Pos.DFA_IMPST_SOST_K2);
        return buffer;
    }

    public void setDfaImpstSostK2Null(String dfaImpstSostK2Null) {
        writeString(Pos.DFA_IMPST_SOST_K2_NULL, dfaImpstSostK2Null, Len.DFA_IMPST_SOST_K2_NULL);
    }

    /**Original name: DFA-IMPST-SOST-K2-NULL<br>*/
    public String getDfaImpstSostK2Null() {
        return readString(Pos.DFA_IMPST_SOST_K2_NULL, Len.DFA_IMPST_SOST_K2_NULL);
    }

    public String getDfaImpstSostK2NullFormatted() {
        return Functions.padBlanks(getDfaImpstSostK2Null(), Len.DFA_IMPST_SOST_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K2 = 1;
        public static final int DFA_IMPST_SOST_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_SOST_K2 = 8;
        public static final int DFA_IMPST_SOST_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_SOST_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
