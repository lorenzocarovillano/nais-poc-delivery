package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-VIS<br>
 * Variable: WDFA-IMPB-VIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_VIS;
    }

    public void setWdfaImpbVis(AfDecimal wdfaImpbVis) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_VIS, wdfaImpbVis.copy());
    }

    public void setWdfaImpbVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_VIS, Pos.WDFA_IMPB_VIS);
    }

    /**Original name: WDFA-IMPB-VIS<br>*/
    public AfDecimal getWdfaImpbVis() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_VIS, Len.Int.WDFA_IMPB_VIS, Len.Fract.WDFA_IMPB_VIS);
    }

    public byte[] getWdfaImpbVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_VIS, Pos.WDFA_IMPB_VIS);
        return buffer;
    }

    public void setWdfaImpbVisNull(String wdfaImpbVisNull) {
        writeString(Pos.WDFA_IMPB_VIS_NULL, wdfaImpbVisNull, Len.WDFA_IMPB_VIS_NULL);
    }

    /**Original name: WDFA-IMPB-VIS-NULL<br>*/
    public String getWdfaImpbVisNull() {
        return readString(Pos.WDFA_IMPB_VIS_NULL, Len.WDFA_IMPB_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_VIS = 1;
        public static final int WDFA_IMPB_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_VIS = 8;
        public static final int WDFA_IMPB_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
