package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-RETE<br>
 * Variable: WS-TP-RETE from copybook LCCVXRE0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpRete {

    //==== PROPERTIES ====
    private String value = "";
    public static final String ACQUSIZIONE = "AC";
    public static final String GESTIONE = "GE";

    //==== METHODS ====
    public void setWsTpRete(String wsTpRete) {
        this.value = Functions.subString(wsTpRete, Len.WS_TP_RETE);
    }

    public String getWsTpRete() {
        return this.value;
    }

    public void setGestione() {
        value = GESTIONE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_RETE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
