package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-DT-INI-COP<br>
 * Variable: WTDR-DT-INI-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_DT_INI_COP;
    }

    public void setWtdrDtIniCop(int wtdrDtIniCop) {
        writeIntAsPacked(Pos.WTDR_DT_INI_COP, wtdrDtIniCop, Len.Int.WTDR_DT_INI_COP);
    }

    /**Original name: WTDR-DT-INI-COP<br>*/
    public int getWtdrDtIniCop() {
        return readPackedAsInt(Pos.WTDR_DT_INI_COP, Len.Int.WTDR_DT_INI_COP);
    }

    public void setWtdrDtIniCopNull(String wtdrDtIniCopNull) {
        writeString(Pos.WTDR_DT_INI_COP_NULL, wtdrDtIniCopNull, Len.WTDR_DT_INI_COP_NULL);
    }

    /**Original name: WTDR-DT-INI-COP-NULL<br>*/
    public String getWtdrDtIniCopNull() {
        return readString(Pos.WTDR_DT_INI_COP_NULL, Len.WTDR_DT_INI_COP_NULL);
    }

    public String getWtdrDtIniCopNullFormatted() {
        return Functions.padBlanks(getWtdrDtIniCopNull(), Len.WTDR_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_DT_INI_COP = 1;
        public static final int WTDR_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_DT_INI_COP = 5;
        public static final int WTDR_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
