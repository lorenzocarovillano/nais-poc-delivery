package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-DT-PRESC<br>
 * Variable: WGRZ-DT-PRESC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDtPresc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDtPresc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DT_PRESC;
    }

    public void setWgrzDtPresc(int wgrzDtPresc) {
        writeIntAsPacked(Pos.WGRZ_DT_PRESC, wgrzDtPresc, Len.Int.WGRZ_DT_PRESC);
    }

    public void setWgrzDtPrescFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DT_PRESC, Pos.WGRZ_DT_PRESC);
    }

    /**Original name: WGRZ-DT-PRESC<br>*/
    public int getWgrzDtPresc() {
        return readPackedAsInt(Pos.WGRZ_DT_PRESC, Len.Int.WGRZ_DT_PRESC);
    }

    public byte[] getWgrzDtPrescAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DT_PRESC, Pos.WGRZ_DT_PRESC);
        return buffer;
    }

    public void initWgrzDtPrescSpaces() {
        fill(Pos.WGRZ_DT_PRESC, Len.WGRZ_DT_PRESC, Types.SPACE_CHAR);
    }

    public void setWgrzDtPrescNull(String wgrzDtPrescNull) {
        writeString(Pos.WGRZ_DT_PRESC_NULL, wgrzDtPrescNull, Len.WGRZ_DT_PRESC_NULL);
    }

    /**Original name: WGRZ-DT-PRESC-NULL<br>*/
    public String getWgrzDtPrescNull() {
        return readString(Pos.WGRZ_DT_PRESC_NULL, Len.WGRZ_DT_PRESC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_PRESC = 1;
        public static final int WGRZ_DT_PRESC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_PRESC = 5;
        public static final int WGRZ_DT_PRESC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
