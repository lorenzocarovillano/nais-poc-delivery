package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-ULT-PC-PERD<br>
 * Variable: PMO-ULT-PC-PERD from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoUltPcPerd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoUltPcPerd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_ULT_PC_PERD;
    }

    public void setPmoUltPcPerd(AfDecimal pmoUltPcPerd) {
        writeDecimalAsPacked(Pos.PMO_ULT_PC_PERD, pmoUltPcPerd.copy());
    }

    public void setPmoUltPcPerdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_ULT_PC_PERD, Pos.PMO_ULT_PC_PERD);
    }

    /**Original name: PMO-ULT-PC-PERD<br>*/
    public AfDecimal getPmoUltPcPerd() {
        return readPackedAsDecimal(Pos.PMO_ULT_PC_PERD, Len.Int.PMO_ULT_PC_PERD, Len.Fract.PMO_ULT_PC_PERD);
    }

    public byte[] getPmoUltPcPerdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_ULT_PC_PERD, Pos.PMO_ULT_PC_PERD);
        return buffer;
    }

    public void initPmoUltPcPerdHighValues() {
        fill(Pos.PMO_ULT_PC_PERD, Len.PMO_ULT_PC_PERD, Types.HIGH_CHAR_VAL);
    }

    public void setPmoUltPcPerdNull(String pmoUltPcPerdNull) {
        writeString(Pos.PMO_ULT_PC_PERD_NULL, pmoUltPcPerdNull, Len.PMO_ULT_PC_PERD_NULL);
    }

    /**Original name: PMO-ULT-PC-PERD-NULL<br>*/
    public String getPmoUltPcPerdNull() {
        return readString(Pos.PMO_ULT_PC_PERD_NULL, Len.PMO_ULT_PC_PERD_NULL);
    }

    public String getPmoUltPcPerdNullFormatted() {
        return Functions.padBlanks(getPmoUltPcPerdNull(), Len.PMO_ULT_PC_PERD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_ULT_PC_PERD = 1;
        public static final int PMO_ULT_PC_PERD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_ULT_PC_PERD = 4;
        public static final int PMO_ULT_PC_PERD_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_ULT_PC_PERD = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_ULT_PC_PERD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
