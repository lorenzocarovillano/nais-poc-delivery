package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LLBV0269<br>
 * Variable: LLBV0269 from copybook LLBV0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Llbv0269 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LLBV0269-TS-PRECED
    private long tsPreced = DefaultValues.LONG_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LLBV0269;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLlbv0269Bytes(buf);
    }

    public String getLlbv0269Formatted() {
        return getOutputFormatted();
    }

    public void setLlbv0269Bytes(byte[] buffer) {
        setLlbv0269Bytes(buffer, 1);
    }

    public byte[] getLlbv0269Bytes() {
        byte[] buffer = new byte[Len.LLBV0269];
        return getLlbv0269Bytes(buffer, 1);
    }

    public void setLlbv0269Bytes(byte[] buffer, int offset) {
        int position = offset;
        setOutputBytes(buffer, position);
    }

    public byte[] getLlbv0269Bytes(byte[] buffer, int offset) {
        int position = offset;
        getOutputBytes(buffer, position);
        return buffer;
    }

    public String getOutputFormatted() {
        return MarshalByteExt.bufferToStr(getOutputBytes());
    }

    /**Original name: LLBV0269-OUTPUT<br>*/
    public byte[] getOutputBytes() {
        byte[] buffer = new byte[Len.OUTPUT];
        return getOutputBytes(buffer, 1);
    }

    public void setOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        tsPreced = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TS_PRECED, 0);
    }

    public byte[] getOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeLongAsPacked(buffer, position, tsPreced, Len.Int.TS_PRECED, 0);
        return buffer;
    }

    public void setTsPreced(long tsPreced) {
        this.tsPreced = tsPreced;
    }

    public long getTsPreced() {
        return this.tsPreced;
    }

    @Override
    public byte[] serialize() {
        return getLlbv0269Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TS_PRECED = 10;
        public static final int OUTPUT = TS_PRECED;
        public static final int LLBV0269 = OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TS_PRECED = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
