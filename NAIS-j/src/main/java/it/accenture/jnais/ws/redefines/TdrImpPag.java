package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-PAG<br>
 * Variable: TDR-IMP-PAG from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_PAG;
    }

    public void setTdrImpPag(AfDecimal tdrImpPag) {
        writeDecimalAsPacked(Pos.TDR_IMP_PAG, tdrImpPag.copy());
    }

    public void setTdrImpPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_PAG, Pos.TDR_IMP_PAG);
    }

    /**Original name: TDR-IMP-PAG<br>*/
    public AfDecimal getTdrImpPag() {
        return readPackedAsDecimal(Pos.TDR_IMP_PAG, Len.Int.TDR_IMP_PAG, Len.Fract.TDR_IMP_PAG);
    }

    public byte[] getTdrImpPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_PAG, Pos.TDR_IMP_PAG);
        return buffer;
    }

    public void setTdrImpPagNull(String tdrImpPagNull) {
        writeString(Pos.TDR_IMP_PAG_NULL, tdrImpPagNull, Len.TDR_IMP_PAG_NULL);
    }

    /**Original name: TDR-IMP-PAG-NULL<br>*/
    public String getTdrImpPagNull() {
        return readString(Pos.TDR_IMP_PAG_NULL, Len.TDR_IMP_PAG_NULL);
    }

    public String getTdrImpPagNullFormatted() {
        return Functions.padBlanks(getTdrImpPagNull(), Len.TDR_IMP_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_PAG = 1;
        public static final int TDR_IMP_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_PAG = 8;
        public static final int TDR_IMP_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
