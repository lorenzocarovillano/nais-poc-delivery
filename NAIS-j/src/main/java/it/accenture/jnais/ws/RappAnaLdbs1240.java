package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.RanDtDeces;
import it.accenture.jnais.ws.redefines.RanDtDeliberaCda;
import it.accenture.jnais.ws.redefines.RanDtNasc;
import it.accenture.jnais.ws.redefines.RanIdMoviChiu;
import it.accenture.jnais.ws.redefines.RanIdRappAnaCollg;
import it.accenture.jnais.ws.redefines.RanPcNelRapp;

/**Original name: RAPP-ANA<br>
 * Variable: RAPP-ANA from copybook IDBVRAN1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class RappAnaLdbs1240 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: RAN-ID-RAPP-ANA
    private int ranIdRappAna = DefaultValues.INT_VAL;
    //Original name: RAN-ID-RAPP-ANA-COLLG
    private RanIdRappAnaCollg ranIdRappAnaCollg = new RanIdRappAnaCollg();
    //Original name: RAN-ID-OGG
    private int ranIdOgg = DefaultValues.INT_VAL;
    //Original name: RAN-TP-OGG
    private String ranTpOgg = DefaultValues.stringVal(Len.RAN_TP_OGG);
    //Original name: RAN-ID-MOVI-CRZ
    private int ranIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: RAN-ID-MOVI-CHIU
    private RanIdMoviChiu ranIdMoviChiu = new RanIdMoviChiu();
    //Original name: RAN-DT-INI-EFF
    private int ranDtIniEff = DefaultValues.INT_VAL;
    //Original name: RAN-DT-END-EFF
    private int ranDtEndEff = DefaultValues.INT_VAL;
    //Original name: RAN-COD-COMP-ANIA
    private int ranCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RAN-COD-SOGG
    private String ranCodSogg = DefaultValues.stringVal(Len.RAN_COD_SOGG);
    //Original name: RAN-TP-RAPP-ANA
    private String ranTpRappAna = DefaultValues.stringVal(Len.RAN_TP_RAPP_ANA);
    //Original name: RAN-TP-PERS
    private char ranTpPers = DefaultValues.CHAR_VAL;
    //Original name: RAN-SEX
    private char ranSex = DefaultValues.CHAR_VAL;
    //Original name: RAN-DT-NASC
    private RanDtNasc ranDtNasc = new RanDtNasc();
    //Original name: RAN-FL-ESTAS
    private char ranFlEstas = DefaultValues.CHAR_VAL;
    //Original name: RAN-INDIR-1
    private String ranIndir1 = DefaultValues.stringVal(Len.RAN_INDIR1);
    //Original name: RAN-INDIR-2
    private String ranIndir2 = DefaultValues.stringVal(Len.RAN_INDIR2);
    //Original name: RAN-INDIR-3
    private String ranIndir3 = DefaultValues.stringVal(Len.RAN_INDIR3);
    //Original name: RAN-TP-UTLZ-INDIR-1
    private String ranTpUtlzIndir1 = DefaultValues.stringVal(Len.RAN_TP_UTLZ_INDIR1);
    //Original name: RAN-TP-UTLZ-INDIR-2
    private String ranTpUtlzIndir2 = DefaultValues.stringVal(Len.RAN_TP_UTLZ_INDIR2);
    //Original name: RAN-TP-UTLZ-INDIR-3
    private String ranTpUtlzIndir3 = DefaultValues.stringVal(Len.RAN_TP_UTLZ_INDIR3);
    //Original name: RAN-ESTR-CNT-CORR-ACCR
    private String ranEstrCntCorrAccr = DefaultValues.stringVal(Len.RAN_ESTR_CNT_CORR_ACCR);
    //Original name: RAN-ESTR-CNT-CORR-ADD
    private String ranEstrCntCorrAdd = DefaultValues.stringVal(Len.RAN_ESTR_CNT_CORR_ADD);
    //Original name: RAN-ESTR-DOCTO
    private String ranEstrDocto = DefaultValues.stringVal(Len.RAN_ESTR_DOCTO);
    //Original name: RAN-PC-NEL-RAPP
    private RanPcNelRapp ranPcNelRapp = new RanPcNelRapp();
    //Original name: RAN-TP-MEZ-PAG-ADD
    private String ranTpMezPagAdd = DefaultValues.stringVal(Len.RAN_TP_MEZ_PAG_ADD);
    //Original name: RAN-TP-MEZ-PAG-ACCR
    private String ranTpMezPagAccr = DefaultValues.stringVal(Len.RAN_TP_MEZ_PAG_ACCR);
    //Original name: RAN-COD-MATR
    private String ranCodMatr = DefaultValues.stringVal(Len.RAN_COD_MATR);
    //Original name: RAN-TP-ADEGZ
    private String ranTpAdegz = DefaultValues.stringVal(Len.RAN_TP_ADEGZ);
    //Original name: RAN-FL-TST-RSH
    private char ranFlTstRsh = DefaultValues.CHAR_VAL;
    //Original name: RAN-COD-AZ
    private String ranCodAz = DefaultValues.stringVal(Len.RAN_COD_AZ);
    //Original name: RAN-IND-PRINC
    private String ranIndPrinc = DefaultValues.stringVal(Len.RAN_IND_PRINC);
    //Original name: RAN-DT-DELIBERA-CDA
    private RanDtDeliberaCda ranDtDeliberaCda = new RanDtDeliberaCda();
    //Original name: RAN-DLG-AL-RISC
    private char ranDlgAlRisc = DefaultValues.CHAR_VAL;
    //Original name: RAN-LEGALE-RAPPR-PRINC
    private char ranLegaleRapprPrinc = DefaultValues.CHAR_VAL;
    //Original name: RAN-TP-LEGALE-RAPPR
    private String ranTpLegaleRappr = DefaultValues.stringVal(Len.RAN_TP_LEGALE_RAPPR);
    //Original name: RAN-TP-IND-PRINC
    private String ranTpIndPrinc = DefaultValues.stringVal(Len.RAN_TP_IND_PRINC);
    //Original name: RAN-TP-STAT-RID
    private String ranTpStatRid = DefaultValues.stringVal(Len.RAN_TP_STAT_RID);
    //Original name: RAN-NOME-INT-RID-LEN
    private short ranNomeIntRidLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: RAN-NOME-INT-RID
    private String ranNomeIntRid = DefaultValues.stringVal(Len.RAN_NOME_INT_RID);
    //Original name: RAN-COGN-INT-RID-LEN
    private short ranCognIntRidLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: RAN-COGN-INT-RID
    private String ranCognIntRid = DefaultValues.stringVal(Len.RAN_COGN_INT_RID);
    //Original name: RAN-COGN-INT-TRATT-LEN
    private short ranCognIntTrattLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: RAN-COGN-INT-TRATT
    private String ranCognIntTratt = DefaultValues.stringVal(Len.RAN_COGN_INT_TRATT);
    //Original name: RAN-NOME-INT-TRATT-LEN
    private short ranNomeIntTrattLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: RAN-NOME-INT-TRATT
    private String ranNomeIntTratt = DefaultValues.stringVal(Len.RAN_NOME_INT_TRATT);
    //Original name: RAN-CF-INT-RID
    private String ranCfIntRid = DefaultValues.stringVal(Len.RAN_CF_INT_RID);
    //Original name: RAN-FL-COINC-DIP-CNTR
    private char ranFlCoincDipCntr = DefaultValues.CHAR_VAL;
    //Original name: RAN-FL-COINC-INT-CNTR
    private char ranFlCoincIntCntr = DefaultValues.CHAR_VAL;
    //Original name: RAN-DT-DECES
    private RanDtDeces ranDtDeces = new RanDtDeces();
    //Original name: RAN-FL-FUMATORE
    private char ranFlFumatore = DefaultValues.CHAR_VAL;
    //Original name: RAN-DS-RIGA
    private long ranDsRiga = DefaultValues.LONG_VAL;
    //Original name: RAN-DS-OPER-SQL
    private char ranDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RAN-DS-VER
    private int ranDsVer = DefaultValues.INT_VAL;
    //Original name: RAN-DS-TS-INI-CPTZ
    private long ranDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: RAN-DS-TS-END-CPTZ
    private long ranDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: RAN-DS-UTENTE
    private String ranDsUtente = DefaultValues.stringVal(Len.RAN_DS_UTENTE);
    //Original name: RAN-DS-STATO-ELAB
    private char ranDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RAN-FL-LAV-DIP
    private char ranFlLavDip = DefaultValues.CHAR_VAL;
    //Original name: RAN-TP-VARZ-PAGAT
    private String ranTpVarzPagat = DefaultValues.stringVal(Len.RAN_TP_VARZ_PAGAT);
    //Original name: RAN-COD-RID
    private String ranCodRid = DefaultValues.stringVal(Len.RAN_COD_RID);
    //Original name: RAN-TP-CAUS-RID
    private String ranTpCausRid = DefaultValues.stringVal(Len.RAN_TP_CAUS_RID);
    //Original name: RAN-IND-MASSA-CORP
    private String ranIndMassaCorp = DefaultValues.stringVal(Len.RAN_IND_MASSA_CORP);
    //Original name: RAN-CAT-RSH-PROF
    private String ranCatRshProf = DefaultValues.stringVal(Len.RAN_CAT_RSH_PROF);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAPP_ANA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setRappAnaBytes(buf);
    }

    public void setRappAnaFormatted(String data) {
        byte[] buffer = new byte[Len.RAPP_ANA];
        MarshalByte.writeString(buffer, 1, data, Len.RAPP_ANA);
        setRappAnaBytes(buffer, 1);
    }

    public String getRappAnaFormatted() {
        return MarshalByteExt.bufferToStr(getRappAnaBytes());
    }

    public void setRappAnaBytes(byte[] buffer) {
        setRappAnaBytes(buffer, 1);
    }

    public byte[] getRappAnaBytes() {
        byte[] buffer = new byte[Len.RAPP_ANA];
        return getRappAnaBytes(buffer, 1);
    }

    public void setRappAnaBytes(byte[] buffer, int offset) {
        int position = offset;
        ranIdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_ID_RAPP_ANA, 0);
        position += Len.RAN_ID_RAPP_ANA;
        ranIdRappAnaCollg.setRanIdRappAnaCollgFromBuffer(buffer, position);
        position += RanIdRappAnaCollg.Len.RAN_ID_RAPP_ANA_COLLG;
        ranIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_ID_OGG, 0);
        position += Len.RAN_ID_OGG;
        ranTpOgg = MarshalByte.readString(buffer, position, Len.RAN_TP_OGG);
        position += Len.RAN_TP_OGG;
        ranIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_ID_MOVI_CRZ, 0);
        position += Len.RAN_ID_MOVI_CRZ;
        ranIdMoviChiu.setRanIdMoviChiuFromBuffer(buffer, position);
        position += RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU;
        ranDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_DT_INI_EFF, 0);
        position += Len.RAN_DT_INI_EFF;
        ranDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_DT_END_EFF, 0);
        position += Len.RAN_DT_END_EFF;
        ranCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_COD_COMP_ANIA, 0);
        position += Len.RAN_COD_COMP_ANIA;
        ranCodSogg = MarshalByte.readString(buffer, position, Len.RAN_COD_SOGG);
        position += Len.RAN_COD_SOGG;
        ranTpRappAna = MarshalByte.readString(buffer, position, Len.RAN_TP_RAPP_ANA);
        position += Len.RAN_TP_RAPP_ANA;
        ranTpPers = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranSex = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranDtNasc.setRanDtNascFromBuffer(buffer, position);
        position += RanDtNasc.Len.RAN_DT_NASC;
        ranFlEstas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranIndir1 = MarshalByte.readString(buffer, position, Len.RAN_INDIR1);
        position += Len.RAN_INDIR1;
        ranIndir2 = MarshalByte.readString(buffer, position, Len.RAN_INDIR2);
        position += Len.RAN_INDIR2;
        ranIndir3 = MarshalByte.readString(buffer, position, Len.RAN_INDIR3);
        position += Len.RAN_INDIR3;
        ranTpUtlzIndir1 = MarshalByte.readString(buffer, position, Len.RAN_TP_UTLZ_INDIR1);
        position += Len.RAN_TP_UTLZ_INDIR1;
        ranTpUtlzIndir2 = MarshalByte.readString(buffer, position, Len.RAN_TP_UTLZ_INDIR2);
        position += Len.RAN_TP_UTLZ_INDIR2;
        ranTpUtlzIndir3 = MarshalByte.readString(buffer, position, Len.RAN_TP_UTLZ_INDIR3);
        position += Len.RAN_TP_UTLZ_INDIR3;
        ranEstrCntCorrAccr = MarshalByte.readString(buffer, position, Len.RAN_ESTR_CNT_CORR_ACCR);
        position += Len.RAN_ESTR_CNT_CORR_ACCR;
        ranEstrCntCorrAdd = MarshalByte.readString(buffer, position, Len.RAN_ESTR_CNT_CORR_ADD);
        position += Len.RAN_ESTR_CNT_CORR_ADD;
        ranEstrDocto = MarshalByte.readString(buffer, position, Len.RAN_ESTR_DOCTO);
        position += Len.RAN_ESTR_DOCTO;
        ranPcNelRapp.setRanPcNelRappFromBuffer(buffer, position);
        position += RanPcNelRapp.Len.RAN_PC_NEL_RAPP;
        ranTpMezPagAdd = MarshalByte.readString(buffer, position, Len.RAN_TP_MEZ_PAG_ADD);
        position += Len.RAN_TP_MEZ_PAG_ADD;
        ranTpMezPagAccr = MarshalByte.readString(buffer, position, Len.RAN_TP_MEZ_PAG_ACCR);
        position += Len.RAN_TP_MEZ_PAG_ACCR;
        ranCodMatr = MarshalByte.readString(buffer, position, Len.RAN_COD_MATR);
        position += Len.RAN_COD_MATR;
        ranTpAdegz = MarshalByte.readString(buffer, position, Len.RAN_TP_ADEGZ);
        position += Len.RAN_TP_ADEGZ;
        ranFlTstRsh = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranCodAz = MarshalByte.readString(buffer, position, Len.RAN_COD_AZ);
        position += Len.RAN_COD_AZ;
        ranIndPrinc = MarshalByte.readString(buffer, position, Len.RAN_IND_PRINC);
        position += Len.RAN_IND_PRINC;
        ranDtDeliberaCda.setRanDtDeliberaCdaFromBuffer(buffer, position);
        position += RanDtDeliberaCda.Len.RAN_DT_DELIBERA_CDA;
        ranDlgAlRisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranLegaleRapprPrinc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranTpLegaleRappr = MarshalByte.readString(buffer, position, Len.RAN_TP_LEGALE_RAPPR);
        position += Len.RAN_TP_LEGALE_RAPPR;
        ranTpIndPrinc = MarshalByte.readString(buffer, position, Len.RAN_TP_IND_PRINC);
        position += Len.RAN_TP_IND_PRINC;
        ranTpStatRid = MarshalByte.readString(buffer, position, Len.RAN_TP_STAT_RID);
        position += Len.RAN_TP_STAT_RID;
        setRanNomeIntRidVcharBytes(buffer, position);
        position += Len.RAN_NOME_INT_RID_VCHAR;
        setRanCognIntRidVcharBytes(buffer, position);
        position += Len.RAN_COGN_INT_RID_VCHAR;
        setRanCognIntTrattVcharBytes(buffer, position);
        position += Len.RAN_COGN_INT_TRATT_VCHAR;
        setRanNomeIntTrattVcharBytes(buffer, position);
        position += Len.RAN_NOME_INT_TRATT_VCHAR;
        ranCfIntRid = MarshalByte.readString(buffer, position, Len.RAN_CF_INT_RID);
        position += Len.RAN_CF_INT_RID;
        ranFlCoincDipCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranFlCoincIntCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranDtDeces.setRanDtDecesFromBuffer(buffer, position);
        position += RanDtDeces.Len.RAN_DT_DECES;
        ranFlFumatore = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RAN_DS_RIGA, 0);
        position += Len.RAN_DS_RIGA;
        ranDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RAN_DS_VER, 0);
        position += Len.RAN_DS_VER;
        ranDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RAN_DS_TS_INI_CPTZ, 0);
        position += Len.RAN_DS_TS_INI_CPTZ;
        ranDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RAN_DS_TS_END_CPTZ, 0);
        position += Len.RAN_DS_TS_END_CPTZ;
        ranDsUtente = MarshalByte.readString(buffer, position, Len.RAN_DS_UTENTE);
        position += Len.RAN_DS_UTENTE;
        ranDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranFlLavDip = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ranTpVarzPagat = MarshalByte.readString(buffer, position, Len.RAN_TP_VARZ_PAGAT);
        position += Len.RAN_TP_VARZ_PAGAT;
        ranCodRid = MarshalByte.readString(buffer, position, Len.RAN_COD_RID);
        position += Len.RAN_COD_RID;
        ranTpCausRid = MarshalByte.readString(buffer, position, Len.RAN_TP_CAUS_RID);
        position += Len.RAN_TP_CAUS_RID;
        ranIndMassaCorp = MarshalByte.readString(buffer, position, Len.RAN_IND_MASSA_CORP);
        position += Len.RAN_IND_MASSA_CORP;
        ranCatRshProf = MarshalByte.readString(buffer, position, Len.RAN_CAT_RSH_PROF);
    }

    public byte[] getRappAnaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ranIdRappAna, Len.Int.RAN_ID_RAPP_ANA, 0);
        position += Len.RAN_ID_RAPP_ANA;
        ranIdRappAnaCollg.getRanIdRappAnaCollgAsBuffer(buffer, position);
        position += RanIdRappAnaCollg.Len.RAN_ID_RAPP_ANA_COLLG;
        MarshalByte.writeIntAsPacked(buffer, position, ranIdOgg, Len.Int.RAN_ID_OGG, 0);
        position += Len.RAN_ID_OGG;
        MarshalByte.writeString(buffer, position, ranTpOgg, Len.RAN_TP_OGG);
        position += Len.RAN_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, ranIdMoviCrz, Len.Int.RAN_ID_MOVI_CRZ, 0);
        position += Len.RAN_ID_MOVI_CRZ;
        ranIdMoviChiu.getRanIdMoviChiuAsBuffer(buffer, position);
        position += RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, ranDtIniEff, Len.Int.RAN_DT_INI_EFF, 0);
        position += Len.RAN_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, ranDtEndEff, Len.Int.RAN_DT_END_EFF, 0);
        position += Len.RAN_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, ranCodCompAnia, Len.Int.RAN_COD_COMP_ANIA, 0);
        position += Len.RAN_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, ranCodSogg, Len.RAN_COD_SOGG);
        position += Len.RAN_COD_SOGG;
        MarshalByte.writeString(buffer, position, ranTpRappAna, Len.RAN_TP_RAPP_ANA);
        position += Len.RAN_TP_RAPP_ANA;
        MarshalByte.writeChar(buffer, position, ranTpPers);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, ranSex);
        position += Types.CHAR_SIZE;
        ranDtNasc.getRanDtNascAsBuffer(buffer, position);
        position += RanDtNasc.Len.RAN_DT_NASC;
        MarshalByte.writeChar(buffer, position, ranFlEstas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ranIndir1, Len.RAN_INDIR1);
        position += Len.RAN_INDIR1;
        MarshalByte.writeString(buffer, position, ranIndir2, Len.RAN_INDIR2);
        position += Len.RAN_INDIR2;
        MarshalByte.writeString(buffer, position, ranIndir3, Len.RAN_INDIR3);
        position += Len.RAN_INDIR3;
        MarshalByte.writeString(buffer, position, ranTpUtlzIndir1, Len.RAN_TP_UTLZ_INDIR1);
        position += Len.RAN_TP_UTLZ_INDIR1;
        MarshalByte.writeString(buffer, position, ranTpUtlzIndir2, Len.RAN_TP_UTLZ_INDIR2);
        position += Len.RAN_TP_UTLZ_INDIR2;
        MarshalByte.writeString(buffer, position, ranTpUtlzIndir3, Len.RAN_TP_UTLZ_INDIR3);
        position += Len.RAN_TP_UTLZ_INDIR3;
        MarshalByte.writeString(buffer, position, ranEstrCntCorrAccr, Len.RAN_ESTR_CNT_CORR_ACCR);
        position += Len.RAN_ESTR_CNT_CORR_ACCR;
        MarshalByte.writeString(buffer, position, ranEstrCntCorrAdd, Len.RAN_ESTR_CNT_CORR_ADD);
        position += Len.RAN_ESTR_CNT_CORR_ADD;
        MarshalByte.writeString(buffer, position, ranEstrDocto, Len.RAN_ESTR_DOCTO);
        position += Len.RAN_ESTR_DOCTO;
        ranPcNelRapp.getRanPcNelRappAsBuffer(buffer, position);
        position += RanPcNelRapp.Len.RAN_PC_NEL_RAPP;
        MarshalByte.writeString(buffer, position, ranTpMezPagAdd, Len.RAN_TP_MEZ_PAG_ADD);
        position += Len.RAN_TP_MEZ_PAG_ADD;
        MarshalByte.writeString(buffer, position, ranTpMezPagAccr, Len.RAN_TP_MEZ_PAG_ACCR);
        position += Len.RAN_TP_MEZ_PAG_ACCR;
        MarshalByte.writeString(buffer, position, ranCodMatr, Len.RAN_COD_MATR);
        position += Len.RAN_COD_MATR;
        MarshalByte.writeString(buffer, position, ranTpAdegz, Len.RAN_TP_ADEGZ);
        position += Len.RAN_TP_ADEGZ;
        MarshalByte.writeChar(buffer, position, ranFlTstRsh);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ranCodAz, Len.RAN_COD_AZ);
        position += Len.RAN_COD_AZ;
        MarshalByte.writeString(buffer, position, ranIndPrinc, Len.RAN_IND_PRINC);
        position += Len.RAN_IND_PRINC;
        ranDtDeliberaCda.getRanDtDeliberaCdaAsBuffer(buffer, position);
        position += RanDtDeliberaCda.Len.RAN_DT_DELIBERA_CDA;
        MarshalByte.writeChar(buffer, position, ranDlgAlRisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, ranLegaleRapprPrinc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ranTpLegaleRappr, Len.RAN_TP_LEGALE_RAPPR);
        position += Len.RAN_TP_LEGALE_RAPPR;
        MarshalByte.writeString(buffer, position, ranTpIndPrinc, Len.RAN_TP_IND_PRINC);
        position += Len.RAN_TP_IND_PRINC;
        MarshalByte.writeString(buffer, position, ranTpStatRid, Len.RAN_TP_STAT_RID);
        position += Len.RAN_TP_STAT_RID;
        getRanNomeIntRidVcharBytes(buffer, position);
        position += Len.RAN_NOME_INT_RID_VCHAR;
        getRanCognIntRidVcharBytes(buffer, position);
        position += Len.RAN_COGN_INT_RID_VCHAR;
        getRanCognIntTrattVcharBytes(buffer, position);
        position += Len.RAN_COGN_INT_TRATT_VCHAR;
        getRanNomeIntTrattVcharBytes(buffer, position);
        position += Len.RAN_NOME_INT_TRATT_VCHAR;
        MarshalByte.writeString(buffer, position, ranCfIntRid, Len.RAN_CF_INT_RID);
        position += Len.RAN_CF_INT_RID;
        MarshalByte.writeChar(buffer, position, ranFlCoincDipCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, ranFlCoincIntCntr);
        position += Types.CHAR_SIZE;
        ranDtDeces.getRanDtDecesAsBuffer(buffer, position);
        position += RanDtDeces.Len.RAN_DT_DECES;
        MarshalByte.writeChar(buffer, position, ranFlFumatore);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, ranDsRiga, Len.Int.RAN_DS_RIGA, 0);
        position += Len.RAN_DS_RIGA;
        MarshalByte.writeChar(buffer, position, ranDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, ranDsVer, Len.Int.RAN_DS_VER, 0);
        position += Len.RAN_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, ranDsTsIniCptz, Len.Int.RAN_DS_TS_INI_CPTZ, 0);
        position += Len.RAN_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, ranDsTsEndCptz, Len.Int.RAN_DS_TS_END_CPTZ, 0);
        position += Len.RAN_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, ranDsUtente, Len.RAN_DS_UTENTE);
        position += Len.RAN_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, ranDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, ranFlLavDip);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ranTpVarzPagat, Len.RAN_TP_VARZ_PAGAT);
        position += Len.RAN_TP_VARZ_PAGAT;
        MarshalByte.writeString(buffer, position, ranCodRid, Len.RAN_COD_RID);
        position += Len.RAN_COD_RID;
        MarshalByte.writeString(buffer, position, ranTpCausRid, Len.RAN_TP_CAUS_RID);
        position += Len.RAN_TP_CAUS_RID;
        MarshalByte.writeString(buffer, position, ranIndMassaCorp, Len.RAN_IND_MASSA_CORP);
        position += Len.RAN_IND_MASSA_CORP;
        MarshalByte.writeString(buffer, position, ranCatRshProf, Len.RAN_CAT_RSH_PROF);
        return buffer;
    }

    public void setRanIdRappAna(int ranIdRappAna) {
        this.ranIdRappAna = ranIdRappAna;
    }

    public int getRanIdRappAna() {
        return this.ranIdRappAna;
    }

    public void setRanIdOgg(int ranIdOgg) {
        this.ranIdOgg = ranIdOgg;
    }

    public int getRanIdOgg() {
        return this.ranIdOgg;
    }

    public void setRanTpOgg(String ranTpOgg) {
        this.ranTpOgg = Functions.subString(ranTpOgg, Len.RAN_TP_OGG);
    }

    public String getRanTpOgg() {
        return this.ranTpOgg;
    }

    public void setRanIdMoviCrz(int ranIdMoviCrz) {
        this.ranIdMoviCrz = ranIdMoviCrz;
    }

    public int getRanIdMoviCrz() {
        return this.ranIdMoviCrz;
    }

    public void setRanDtIniEff(int ranDtIniEff) {
        this.ranDtIniEff = ranDtIniEff;
    }

    public int getRanDtIniEff() {
        return this.ranDtIniEff;
    }

    public void setRanDtEndEff(int ranDtEndEff) {
        this.ranDtEndEff = ranDtEndEff;
    }

    public int getRanDtEndEff() {
        return this.ranDtEndEff;
    }

    public void setRanCodCompAnia(int ranCodCompAnia) {
        this.ranCodCompAnia = ranCodCompAnia;
    }

    public int getRanCodCompAnia() {
        return this.ranCodCompAnia;
    }

    public void setRanCodSogg(String ranCodSogg) {
        this.ranCodSogg = Functions.subString(ranCodSogg, Len.RAN_COD_SOGG);
    }

    public String getRanCodSogg() {
        return this.ranCodSogg;
    }

    public String getRanCodSoggFormatted() {
        return Functions.padBlanks(getRanCodSogg(), Len.RAN_COD_SOGG);
    }

    public void setRanTpRappAna(String ranTpRappAna) {
        this.ranTpRappAna = Functions.subString(ranTpRappAna, Len.RAN_TP_RAPP_ANA);
    }

    public String getRanTpRappAna() {
        return this.ranTpRappAna;
    }

    public void setRanTpPers(char ranTpPers) {
        this.ranTpPers = ranTpPers;
    }

    public char getRanTpPers() {
        return this.ranTpPers;
    }

    public void setRanSex(char ranSex) {
        this.ranSex = ranSex;
    }

    public char getRanSex() {
        return this.ranSex;
    }

    public void setRanFlEstas(char ranFlEstas) {
        this.ranFlEstas = ranFlEstas;
    }

    public char getRanFlEstas() {
        return this.ranFlEstas;
    }

    public void setRanIndir1(String ranIndir1) {
        this.ranIndir1 = Functions.subString(ranIndir1, Len.RAN_INDIR1);
    }

    public String getRanIndir1() {
        return this.ranIndir1;
    }

    public String getRanIndir1Formatted() {
        return Functions.padBlanks(getRanIndir1(), Len.RAN_INDIR1);
    }

    public void setRanIndir2(String ranIndir2) {
        this.ranIndir2 = Functions.subString(ranIndir2, Len.RAN_INDIR2);
    }

    public String getRanIndir2() {
        return this.ranIndir2;
    }

    public String getRanIndir2Formatted() {
        return Functions.padBlanks(getRanIndir2(), Len.RAN_INDIR2);
    }

    public void setRanIndir3(String ranIndir3) {
        this.ranIndir3 = Functions.subString(ranIndir3, Len.RAN_INDIR3);
    }

    public String getRanIndir3() {
        return this.ranIndir3;
    }

    public String getRanIndir3Formatted() {
        return Functions.padBlanks(getRanIndir3(), Len.RAN_INDIR3);
    }

    public void setRanTpUtlzIndir1(String ranTpUtlzIndir1) {
        this.ranTpUtlzIndir1 = Functions.subString(ranTpUtlzIndir1, Len.RAN_TP_UTLZ_INDIR1);
    }

    public String getRanTpUtlzIndir1() {
        return this.ranTpUtlzIndir1;
    }

    public String getRanTpUtlzIndir1Formatted() {
        return Functions.padBlanks(getRanTpUtlzIndir1(), Len.RAN_TP_UTLZ_INDIR1);
    }

    public void setRanTpUtlzIndir2(String ranTpUtlzIndir2) {
        this.ranTpUtlzIndir2 = Functions.subString(ranTpUtlzIndir2, Len.RAN_TP_UTLZ_INDIR2);
    }

    public String getRanTpUtlzIndir2() {
        return this.ranTpUtlzIndir2;
    }

    public String getRanTpUtlzIndir2Formatted() {
        return Functions.padBlanks(getRanTpUtlzIndir2(), Len.RAN_TP_UTLZ_INDIR2);
    }

    public void setRanTpUtlzIndir3(String ranTpUtlzIndir3) {
        this.ranTpUtlzIndir3 = Functions.subString(ranTpUtlzIndir3, Len.RAN_TP_UTLZ_INDIR3);
    }

    public String getRanTpUtlzIndir3() {
        return this.ranTpUtlzIndir3;
    }

    public String getRanTpUtlzIndir3Formatted() {
        return Functions.padBlanks(getRanTpUtlzIndir3(), Len.RAN_TP_UTLZ_INDIR3);
    }

    public void setRanEstrCntCorrAccr(String ranEstrCntCorrAccr) {
        this.ranEstrCntCorrAccr = Functions.subString(ranEstrCntCorrAccr, Len.RAN_ESTR_CNT_CORR_ACCR);
    }

    public String getRanEstrCntCorrAccr() {
        return this.ranEstrCntCorrAccr;
    }

    public String getRanEstrCntCorrAccrFormatted() {
        return Functions.padBlanks(getRanEstrCntCorrAccr(), Len.RAN_ESTR_CNT_CORR_ACCR);
    }

    public void setRanEstrCntCorrAdd(String ranEstrCntCorrAdd) {
        this.ranEstrCntCorrAdd = Functions.subString(ranEstrCntCorrAdd, Len.RAN_ESTR_CNT_CORR_ADD);
    }

    public String getRanEstrCntCorrAdd() {
        return this.ranEstrCntCorrAdd;
    }

    public String getRanEstrCntCorrAddFormatted() {
        return Functions.padBlanks(getRanEstrCntCorrAdd(), Len.RAN_ESTR_CNT_CORR_ADD);
    }

    public void setRanEstrDocto(String ranEstrDocto) {
        this.ranEstrDocto = Functions.subString(ranEstrDocto, Len.RAN_ESTR_DOCTO);
    }

    public String getRanEstrDocto() {
        return this.ranEstrDocto;
    }

    public String getRanEstrDoctoFormatted() {
        return Functions.padBlanks(getRanEstrDocto(), Len.RAN_ESTR_DOCTO);
    }

    public void setRanTpMezPagAdd(String ranTpMezPagAdd) {
        this.ranTpMezPagAdd = Functions.subString(ranTpMezPagAdd, Len.RAN_TP_MEZ_PAG_ADD);
    }

    public String getRanTpMezPagAdd() {
        return this.ranTpMezPagAdd;
    }

    public String getRanTpMezPagAddFormatted() {
        return Functions.padBlanks(getRanTpMezPagAdd(), Len.RAN_TP_MEZ_PAG_ADD);
    }

    public void setRanTpMezPagAccr(String ranTpMezPagAccr) {
        this.ranTpMezPagAccr = Functions.subString(ranTpMezPagAccr, Len.RAN_TP_MEZ_PAG_ACCR);
    }

    public String getRanTpMezPagAccr() {
        return this.ranTpMezPagAccr;
    }

    public String getRanTpMezPagAccrFormatted() {
        return Functions.padBlanks(getRanTpMezPagAccr(), Len.RAN_TP_MEZ_PAG_ACCR);
    }

    public void setRanCodMatr(String ranCodMatr) {
        this.ranCodMatr = Functions.subString(ranCodMatr, Len.RAN_COD_MATR);
    }

    public String getRanCodMatr() {
        return this.ranCodMatr;
    }

    public String getRanCodMatrFormatted() {
        return Functions.padBlanks(getRanCodMatr(), Len.RAN_COD_MATR);
    }

    public void setRanTpAdegz(String ranTpAdegz) {
        this.ranTpAdegz = Functions.subString(ranTpAdegz, Len.RAN_TP_ADEGZ);
    }

    public String getRanTpAdegz() {
        return this.ranTpAdegz;
    }

    public String getRanTpAdegzFormatted() {
        return Functions.padBlanks(getRanTpAdegz(), Len.RAN_TP_ADEGZ);
    }

    public void setRanFlTstRsh(char ranFlTstRsh) {
        this.ranFlTstRsh = ranFlTstRsh;
    }

    public char getRanFlTstRsh() {
        return this.ranFlTstRsh;
    }

    public void setRanCodAz(String ranCodAz) {
        this.ranCodAz = Functions.subString(ranCodAz, Len.RAN_COD_AZ);
    }

    public String getRanCodAz() {
        return this.ranCodAz;
    }

    public void setRanIndPrinc(String ranIndPrinc) {
        this.ranIndPrinc = Functions.subString(ranIndPrinc, Len.RAN_IND_PRINC);
    }

    public String getRanIndPrinc() {
        return this.ranIndPrinc;
    }

    public String getRanIndPrincFormatted() {
        return Functions.padBlanks(getRanIndPrinc(), Len.RAN_IND_PRINC);
    }

    public void setRanDlgAlRisc(char ranDlgAlRisc) {
        this.ranDlgAlRisc = ranDlgAlRisc;
    }

    public char getRanDlgAlRisc() {
        return this.ranDlgAlRisc;
    }

    public void setRanLegaleRapprPrinc(char ranLegaleRapprPrinc) {
        this.ranLegaleRapprPrinc = ranLegaleRapprPrinc;
    }

    public char getRanLegaleRapprPrinc() {
        return this.ranLegaleRapprPrinc;
    }

    public void setRanTpLegaleRappr(String ranTpLegaleRappr) {
        this.ranTpLegaleRappr = Functions.subString(ranTpLegaleRappr, Len.RAN_TP_LEGALE_RAPPR);
    }

    public String getRanTpLegaleRappr() {
        return this.ranTpLegaleRappr;
    }

    public String getRanTpLegaleRapprFormatted() {
        return Functions.padBlanks(getRanTpLegaleRappr(), Len.RAN_TP_LEGALE_RAPPR);
    }

    public void setRanTpIndPrinc(String ranTpIndPrinc) {
        this.ranTpIndPrinc = Functions.subString(ranTpIndPrinc, Len.RAN_TP_IND_PRINC);
    }

    public String getRanTpIndPrinc() {
        return this.ranTpIndPrinc;
    }

    public String getRanTpIndPrincFormatted() {
        return Functions.padBlanks(getRanTpIndPrinc(), Len.RAN_TP_IND_PRINC);
    }

    public void setRanTpStatRid(String ranTpStatRid) {
        this.ranTpStatRid = Functions.subString(ranTpStatRid, Len.RAN_TP_STAT_RID);
    }

    public String getRanTpStatRid() {
        return this.ranTpStatRid;
    }

    public String getRanTpStatRidFormatted() {
        return Functions.padBlanks(getRanTpStatRid(), Len.RAN_TP_STAT_RID);
    }

    public void setRanNomeIntRidVcharFormatted(String data) {
        byte[] buffer = new byte[Len.RAN_NOME_INT_RID_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.RAN_NOME_INT_RID_VCHAR);
        setRanNomeIntRidVcharBytes(buffer, 1);
    }

    public String getRanNomeIntRidVcharFormatted() {
        return MarshalByteExt.bufferToStr(getRanNomeIntRidVcharBytes());
    }

    /**Original name: RAN-NOME-INT-RID-VCHAR<br>*/
    public byte[] getRanNomeIntRidVcharBytes() {
        byte[] buffer = new byte[Len.RAN_NOME_INT_RID_VCHAR];
        return getRanNomeIntRidVcharBytes(buffer, 1);
    }

    public void setRanNomeIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ranNomeIntRidLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ranNomeIntRid = MarshalByte.readString(buffer, position, Len.RAN_NOME_INT_RID);
    }

    public byte[] getRanNomeIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ranNomeIntRidLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ranNomeIntRid, Len.RAN_NOME_INT_RID);
        return buffer;
    }

    public void setRanNomeIntRidLen(short ranNomeIntRidLen) {
        this.ranNomeIntRidLen = ranNomeIntRidLen;
    }

    public short getRanNomeIntRidLen() {
        return this.ranNomeIntRidLen;
    }

    public void setRanNomeIntRid(String ranNomeIntRid) {
        this.ranNomeIntRid = Functions.subString(ranNomeIntRid, Len.RAN_NOME_INT_RID);
    }

    public String getRanNomeIntRid() {
        return this.ranNomeIntRid;
    }

    public void setRanCognIntRidVcharFormatted(String data) {
        byte[] buffer = new byte[Len.RAN_COGN_INT_RID_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.RAN_COGN_INT_RID_VCHAR);
        setRanCognIntRidVcharBytes(buffer, 1);
    }

    public String getRanCognIntRidVcharFormatted() {
        return MarshalByteExt.bufferToStr(getRanCognIntRidVcharBytes());
    }

    /**Original name: RAN-COGN-INT-RID-VCHAR<br>*/
    public byte[] getRanCognIntRidVcharBytes() {
        byte[] buffer = new byte[Len.RAN_COGN_INT_RID_VCHAR];
        return getRanCognIntRidVcharBytes(buffer, 1);
    }

    public void setRanCognIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ranCognIntRidLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ranCognIntRid = MarshalByte.readString(buffer, position, Len.RAN_COGN_INT_RID);
    }

    public byte[] getRanCognIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ranCognIntRidLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ranCognIntRid, Len.RAN_COGN_INT_RID);
        return buffer;
    }

    public void setRanCognIntRidLen(short ranCognIntRidLen) {
        this.ranCognIntRidLen = ranCognIntRidLen;
    }

    public short getRanCognIntRidLen() {
        return this.ranCognIntRidLen;
    }

    public void setRanCognIntRid(String ranCognIntRid) {
        this.ranCognIntRid = Functions.subString(ranCognIntRid, Len.RAN_COGN_INT_RID);
    }

    public String getRanCognIntRid() {
        return this.ranCognIntRid;
    }

    public void setRanCognIntTrattVcharFormatted(String data) {
        byte[] buffer = new byte[Len.RAN_COGN_INT_TRATT_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.RAN_COGN_INT_TRATT_VCHAR);
        setRanCognIntTrattVcharBytes(buffer, 1);
    }

    public String getRanCognIntTrattVcharFormatted() {
        return MarshalByteExt.bufferToStr(getRanCognIntTrattVcharBytes());
    }

    /**Original name: RAN-COGN-INT-TRATT-VCHAR<br>*/
    public byte[] getRanCognIntTrattVcharBytes() {
        byte[] buffer = new byte[Len.RAN_COGN_INT_TRATT_VCHAR];
        return getRanCognIntTrattVcharBytes(buffer, 1);
    }

    public void setRanCognIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ranCognIntTrattLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ranCognIntTratt = MarshalByte.readString(buffer, position, Len.RAN_COGN_INT_TRATT);
    }

    public byte[] getRanCognIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ranCognIntTrattLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ranCognIntTratt, Len.RAN_COGN_INT_TRATT);
        return buffer;
    }

    public void setRanCognIntTrattLen(short ranCognIntTrattLen) {
        this.ranCognIntTrattLen = ranCognIntTrattLen;
    }

    public short getRanCognIntTrattLen() {
        return this.ranCognIntTrattLen;
    }

    public void setRanCognIntTratt(String ranCognIntTratt) {
        this.ranCognIntTratt = Functions.subString(ranCognIntTratt, Len.RAN_COGN_INT_TRATT);
    }

    public String getRanCognIntTratt() {
        return this.ranCognIntTratt;
    }

    public void setRanNomeIntTrattVcharFormatted(String data) {
        byte[] buffer = new byte[Len.RAN_NOME_INT_TRATT_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.RAN_NOME_INT_TRATT_VCHAR);
        setRanNomeIntTrattVcharBytes(buffer, 1);
    }

    public String getRanNomeIntTrattVcharFormatted() {
        return MarshalByteExt.bufferToStr(getRanNomeIntTrattVcharBytes());
    }

    /**Original name: RAN-NOME-INT-TRATT-VCHAR<br>*/
    public byte[] getRanNomeIntTrattVcharBytes() {
        byte[] buffer = new byte[Len.RAN_NOME_INT_TRATT_VCHAR];
        return getRanNomeIntTrattVcharBytes(buffer, 1);
    }

    public void setRanNomeIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ranNomeIntTrattLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ranNomeIntTratt = MarshalByte.readString(buffer, position, Len.RAN_NOME_INT_TRATT);
    }

    public byte[] getRanNomeIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ranNomeIntTrattLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ranNomeIntTratt, Len.RAN_NOME_INT_TRATT);
        return buffer;
    }

    public void setRanNomeIntTrattLen(short ranNomeIntTrattLen) {
        this.ranNomeIntTrattLen = ranNomeIntTrattLen;
    }

    public short getRanNomeIntTrattLen() {
        return this.ranNomeIntTrattLen;
    }

    public void setRanNomeIntTratt(String ranNomeIntTratt) {
        this.ranNomeIntTratt = Functions.subString(ranNomeIntTratt, Len.RAN_NOME_INT_TRATT);
    }

    public String getRanNomeIntTratt() {
        return this.ranNomeIntTratt;
    }

    public void setRanCfIntRid(String ranCfIntRid) {
        this.ranCfIntRid = Functions.subString(ranCfIntRid, Len.RAN_CF_INT_RID);
    }

    public String getRanCfIntRid() {
        return this.ranCfIntRid;
    }

    public String getRanCfIntRidFormatted() {
        return Functions.padBlanks(getRanCfIntRid(), Len.RAN_CF_INT_RID);
    }

    public void setRanFlCoincDipCntr(char ranFlCoincDipCntr) {
        this.ranFlCoincDipCntr = ranFlCoincDipCntr;
    }

    public char getRanFlCoincDipCntr() {
        return this.ranFlCoincDipCntr;
    }

    public void setRanFlCoincIntCntr(char ranFlCoincIntCntr) {
        this.ranFlCoincIntCntr = ranFlCoincIntCntr;
    }

    public char getRanFlCoincIntCntr() {
        return this.ranFlCoincIntCntr;
    }

    public void setRanFlFumatore(char ranFlFumatore) {
        this.ranFlFumatore = ranFlFumatore;
    }

    public char getRanFlFumatore() {
        return this.ranFlFumatore;
    }

    public void setRanDsRiga(long ranDsRiga) {
        this.ranDsRiga = ranDsRiga;
    }

    public long getRanDsRiga() {
        return this.ranDsRiga;
    }

    public void setRanDsOperSql(char ranDsOperSql) {
        this.ranDsOperSql = ranDsOperSql;
    }

    public void setRanDsOperSqlFormatted(String ranDsOperSql) {
        setRanDsOperSql(Functions.charAt(ranDsOperSql, Types.CHAR_SIZE));
    }

    public char getRanDsOperSql() {
        return this.ranDsOperSql;
    }

    public void setRanDsVer(int ranDsVer) {
        this.ranDsVer = ranDsVer;
    }

    public int getRanDsVer() {
        return this.ranDsVer;
    }

    public void setRanDsTsIniCptz(long ranDsTsIniCptz) {
        this.ranDsTsIniCptz = ranDsTsIniCptz;
    }

    public long getRanDsTsIniCptz() {
        return this.ranDsTsIniCptz;
    }

    public void setRanDsTsEndCptz(long ranDsTsEndCptz) {
        this.ranDsTsEndCptz = ranDsTsEndCptz;
    }

    public long getRanDsTsEndCptz() {
        return this.ranDsTsEndCptz;
    }

    public void setRanDsUtente(String ranDsUtente) {
        this.ranDsUtente = Functions.subString(ranDsUtente, Len.RAN_DS_UTENTE);
    }

    public String getRanDsUtente() {
        return this.ranDsUtente;
    }

    public void setRanDsStatoElab(char ranDsStatoElab) {
        this.ranDsStatoElab = ranDsStatoElab;
    }

    public void setRanDsStatoElabFormatted(String ranDsStatoElab) {
        setRanDsStatoElab(Functions.charAt(ranDsStatoElab, Types.CHAR_SIZE));
    }

    public char getRanDsStatoElab() {
        return this.ranDsStatoElab;
    }

    public void setRanFlLavDip(char ranFlLavDip) {
        this.ranFlLavDip = ranFlLavDip;
    }

    public char getRanFlLavDip() {
        return this.ranFlLavDip;
    }

    public void setRanTpVarzPagat(String ranTpVarzPagat) {
        this.ranTpVarzPagat = Functions.subString(ranTpVarzPagat, Len.RAN_TP_VARZ_PAGAT);
    }

    public String getRanTpVarzPagat() {
        return this.ranTpVarzPagat;
    }

    public String getRanTpVarzPagatFormatted() {
        return Functions.padBlanks(getRanTpVarzPagat(), Len.RAN_TP_VARZ_PAGAT);
    }

    public void setRanCodRid(String ranCodRid) {
        this.ranCodRid = Functions.subString(ranCodRid, Len.RAN_COD_RID);
    }

    public String getRanCodRid() {
        return this.ranCodRid;
    }

    public String getRanCodRidFormatted() {
        return Functions.padBlanks(getRanCodRid(), Len.RAN_COD_RID);
    }

    public void setRanTpCausRid(String ranTpCausRid) {
        this.ranTpCausRid = Functions.subString(ranTpCausRid, Len.RAN_TP_CAUS_RID);
    }

    public String getRanTpCausRid() {
        return this.ranTpCausRid;
    }

    public String getRanTpCausRidFormatted() {
        return Functions.padBlanks(getRanTpCausRid(), Len.RAN_TP_CAUS_RID);
    }

    public void setRanIndMassaCorp(String ranIndMassaCorp) {
        this.ranIndMassaCorp = Functions.subString(ranIndMassaCorp, Len.RAN_IND_MASSA_CORP);
    }

    public String getRanIndMassaCorp() {
        return this.ranIndMassaCorp;
    }

    public String getRanIndMassaCorpFormatted() {
        return Functions.padBlanks(getRanIndMassaCorp(), Len.RAN_IND_MASSA_CORP);
    }

    public void setRanCatRshProf(String ranCatRshProf) {
        this.ranCatRshProf = Functions.subString(ranCatRshProf, Len.RAN_CAT_RSH_PROF);
    }

    public String getRanCatRshProf() {
        return this.ranCatRshProf;
    }

    public String getRanCatRshProfFormatted() {
        return Functions.padBlanks(getRanCatRshProf(), Len.RAN_CAT_RSH_PROF);
    }

    public RanDtDeces getRanDtDeces() {
        return ranDtDeces;
    }

    public RanDtDeliberaCda getRanDtDeliberaCda() {
        return ranDtDeliberaCda;
    }

    public RanDtNasc getRanDtNasc() {
        return ranDtNasc;
    }

    public RanIdMoviChiu getRanIdMoviChiu() {
        return ranIdMoviChiu;
    }

    public RanIdRappAnaCollg getRanIdRappAnaCollg() {
        return ranIdRappAnaCollg;
    }

    public RanPcNelRapp getRanPcNelRapp() {
        return ranPcNelRapp;
    }

    @Override
    public byte[] serialize() {
        return getRappAnaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_ID_RAPP_ANA = 5;
        public static final int RAN_ID_OGG = 5;
        public static final int RAN_TP_OGG = 2;
        public static final int RAN_ID_MOVI_CRZ = 5;
        public static final int RAN_DT_INI_EFF = 5;
        public static final int RAN_DT_END_EFF = 5;
        public static final int RAN_COD_COMP_ANIA = 3;
        public static final int RAN_COD_SOGG = 20;
        public static final int RAN_TP_RAPP_ANA = 2;
        public static final int RAN_TP_PERS = 1;
        public static final int RAN_SEX = 1;
        public static final int RAN_FL_ESTAS = 1;
        public static final int RAN_INDIR1 = 20;
        public static final int RAN_INDIR2 = 20;
        public static final int RAN_INDIR3 = 20;
        public static final int RAN_TP_UTLZ_INDIR1 = 2;
        public static final int RAN_TP_UTLZ_INDIR2 = 2;
        public static final int RAN_TP_UTLZ_INDIR3 = 2;
        public static final int RAN_ESTR_CNT_CORR_ACCR = 20;
        public static final int RAN_ESTR_CNT_CORR_ADD = 20;
        public static final int RAN_ESTR_DOCTO = 20;
        public static final int RAN_TP_MEZ_PAG_ADD = 2;
        public static final int RAN_TP_MEZ_PAG_ACCR = 2;
        public static final int RAN_COD_MATR = 20;
        public static final int RAN_TP_ADEGZ = 2;
        public static final int RAN_FL_TST_RSH = 1;
        public static final int RAN_COD_AZ = 30;
        public static final int RAN_IND_PRINC = 2;
        public static final int RAN_DLG_AL_RISC = 1;
        public static final int RAN_LEGALE_RAPPR_PRINC = 1;
        public static final int RAN_TP_LEGALE_RAPPR = 2;
        public static final int RAN_TP_IND_PRINC = 2;
        public static final int RAN_TP_STAT_RID = 2;
        public static final int RAN_NOME_INT_RID_LEN = 2;
        public static final int RAN_NOME_INT_RID = 100;
        public static final int RAN_NOME_INT_RID_VCHAR = RAN_NOME_INT_RID_LEN + RAN_NOME_INT_RID;
        public static final int RAN_COGN_INT_RID_LEN = 2;
        public static final int RAN_COGN_INT_RID = 100;
        public static final int RAN_COGN_INT_RID_VCHAR = RAN_COGN_INT_RID_LEN + RAN_COGN_INT_RID;
        public static final int RAN_COGN_INT_TRATT_LEN = 2;
        public static final int RAN_COGN_INT_TRATT = 100;
        public static final int RAN_COGN_INT_TRATT_VCHAR = RAN_COGN_INT_TRATT_LEN + RAN_COGN_INT_TRATT;
        public static final int RAN_NOME_INT_TRATT_LEN = 2;
        public static final int RAN_NOME_INT_TRATT = 100;
        public static final int RAN_NOME_INT_TRATT_VCHAR = RAN_NOME_INT_TRATT_LEN + RAN_NOME_INT_TRATT;
        public static final int RAN_CF_INT_RID = 16;
        public static final int RAN_FL_COINC_DIP_CNTR = 1;
        public static final int RAN_FL_COINC_INT_CNTR = 1;
        public static final int RAN_FL_FUMATORE = 1;
        public static final int RAN_DS_RIGA = 6;
        public static final int RAN_DS_OPER_SQL = 1;
        public static final int RAN_DS_VER = 5;
        public static final int RAN_DS_TS_INI_CPTZ = 10;
        public static final int RAN_DS_TS_END_CPTZ = 10;
        public static final int RAN_DS_UTENTE = 20;
        public static final int RAN_DS_STATO_ELAB = 1;
        public static final int RAN_FL_LAV_DIP = 1;
        public static final int RAN_TP_VARZ_PAGAT = 2;
        public static final int RAN_COD_RID = 11;
        public static final int RAN_TP_CAUS_RID = 2;
        public static final int RAN_IND_MASSA_CORP = 2;
        public static final int RAN_CAT_RSH_PROF = 2;
        public static final int RAPP_ANA = RAN_ID_RAPP_ANA + RanIdRappAnaCollg.Len.RAN_ID_RAPP_ANA_COLLG + RAN_ID_OGG + RAN_TP_OGG + RAN_ID_MOVI_CRZ + RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU + RAN_DT_INI_EFF + RAN_DT_END_EFF + RAN_COD_COMP_ANIA + RAN_COD_SOGG + RAN_TP_RAPP_ANA + RAN_TP_PERS + RAN_SEX + RanDtNasc.Len.RAN_DT_NASC + RAN_FL_ESTAS + RAN_INDIR1 + RAN_INDIR2 + RAN_INDIR3 + RAN_TP_UTLZ_INDIR1 + RAN_TP_UTLZ_INDIR2 + RAN_TP_UTLZ_INDIR3 + RAN_ESTR_CNT_CORR_ACCR + RAN_ESTR_CNT_CORR_ADD + RAN_ESTR_DOCTO + RanPcNelRapp.Len.RAN_PC_NEL_RAPP + RAN_TP_MEZ_PAG_ADD + RAN_TP_MEZ_PAG_ACCR + RAN_COD_MATR + RAN_TP_ADEGZ + RAN_FL_TST_RSH + RAN_COD_AZ + RAN_IND_PRINC + RanDtDeliberaCda.Len.RAN_DT_DELIBERA_CDA + RAN_DLG_AL_RISC + RAN_LEGALE_RAPPR_PRINC + RAN_TP_LEGALE_RAPPR + RAN_TP_IND_PRINC + RAN_TP_STAT_RID + RAN_NOME_INT_RID_VCHAR + RAN_COGN_INT_RID_VCHAR + RAN_COGN_INT_TRATT_VCHAR + RAN_NOME_INT_TRATT_VCHAR + RAN_CF_INT_RID + RAN_FL_COINC_DIP_CNTR + RAN_FL_COINC_INT_CNTR + RanDtDeces.Len.RAN_DT_DECES + RAN_FL_FUMATORE + RAN_DS_RIGA + RAN_DS_OPER_SQL + RAN_DS_VER + RAN_DS_TS_INI_CPTZ + RAN_DS_TS_END_CPTZ + RAN_DS_UTENTE + RAN_DS_STATO_ELAB + RAN_FL_LAV_DIP + RAN_TP_VARZ_PAGAT + RAN_COD_RID + RAN_TP_CAUS_RID + RAN_IND_MASSA_CORP + RAN_CAT_RSH_PROF;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_ID_RAPP_ANA = 9;
            public static final int RAN_ID_OGG = 9;
            public static final int RAN_ID_MOVI_CRZ = 9;
            public static final int RAN_DT_INI_EFF = 8;
            public static final int RAN_DT_END_EFF = 8;
            public static final int RAN_COD_COMP_ANIA = 5;
            public static final int RAN_DS_RIGA = 10;
            public static final int RAN_DS_VER = 9;
            public static final int RAN_DS_TS_INI_CPTZ = 18;
            public static final int RAN_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
