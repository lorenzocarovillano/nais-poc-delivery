package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: E06-CUM-PRE-ATT-TAKE-P<br>
 * Variable: E06-CUM-PRE-ATT-TAKE-P from program IDBSE060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E06CumPreAttTakeP extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E06CumPreAttTakeP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E06_CUM_PRE_ATT_TAKE_P;
    }

    public void setE06CumPreAttTakeP(AfDecimal e06CumPreAttTakeP) {
        writeDecimalAsPacked(Pos.E06_CUM_PRE_ATT_TAKE_P, e06CumPreAttTakeP.copy());
    }

    public void setE06CumPreAttTakePFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E06_CUM_PRE_ATT_TAKE_P, Pos.E06_CUM_PRE_ATT_TAKE_P);
    }

    /**Original name: E06-CUM-PRE-ATT-TAKE-P<br>*/
    public AfDecimal getE06CumPreAttTakeP() {
        return readPackedAsDecimal(Pos.E06_CUM_PRE_ATT_TAKE_P, Len.Int.E06_CUM_PRE_ATT_TAKE_P, Len.Fract.E06_CUM_PRE_ATT_TAKE_P);
    }

    public byte[] getE06CumPreAttTakePAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E06_CUM_PRE_ATT_TAKE_P, Pos.E06_CUM_PRE_ATT_TAKE_P);
        return buffer;
    }

    public void setE06CumPreAttTakePNull(String e06CumPreAttTakePNull) {
        writeString(Pos.E06_CUM_PRE_ATT_TAKE_P_NULL, e06CumPreAttTakePNull, Len.E06_CUM_PRE_ATT_TAKE_P_NULL);
    }

    /**Original name: E06-CUM-PRE-ATT-TAKE-P-NULL<br>*/
    public String getE06CumPreAttTakePNull() {
        return readString(Pos.E06_CUM_PRE_ATT_TAKE_P_NULL, Len.E06_CUM_PRE_ATT_TAKE_P_NULL);
    }

    public String getE06CumPreAttTakePNullFormatted() {
        return Functions.padBlanks(getE06CumPreAttTakePNull(), Len.E06_CUM_PRE_ATT_TAKE_P_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E06_CUM_PRE_ATT_TAKE_P = 1;
        public static final int E06_CUM_PRE_ATT_TAKE_P_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E06_CUM_PRE_ATT_TAKE_P = 8;
        public static final int E06_CUM_PRE_ATT_TAKE_P_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E06_CUM_PRE_ATT_TAKE_P = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int E06_CUM_PRE_ATT_TAKE_P = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
