package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-IMP-REINVST<br>
 * Variable: OCO-IMP-REINVST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoImpReinvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoImpReinvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_IMP_REINVST;
    }

    public void setOcoImpReinvst(AfDecimal ocoImpReinvst) {
        writeDecimalAsPacked(Pos.OCO_IMP_REINVST, ocoImpReinvst.copy());
    }

    public void setOcoImpReinvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_IMP_REINVST, Pos.OCO_IMP_REINVST);
    }

    /**Original name: OCO-IMP-REINVST<br>*/
    public AfDecimal getOcoImpReinvst() {
        return readPackedAsDecimal(Pos.OCO_IMP_REINVST, Len.Int.OCO_IMP_REINVST, Len.Fract.OCO_IMP_REINVST);
    }

    public byte[] getOcoImpReinvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_IMP_REINVST, Pos.OCO_IMP_REINVST);
        return buffer;
    }

    public void setOcoImpReinvstNull(String ocoImpReinvstNull) {
        writeString(Pos.OCO_IMP_REINVST_NULL, ocoImpReinvstNull, Len.OCO_IMP_REINVST_NULL);
    }

    /**Original name: OCO-IMP-REINVST-NULL<br>*/
    public String getOcoImpReinvstNull() {
        return readString(Pos.OCO_IMP_REINVST_NULL, Len.OCO_IMP_REINVST_NULL);
    }

    public String getOcoImpReinvstNullFormatted() {
        return Functions.padBlanks(getOcoImpReinvstNull(), Len.OCO_IMP_REINVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_IMP_REINVST = 1;
        public static final int OCO_IMP_REINVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_IMP_REINVST = 8;
        public static final int OCO_IMP_REINVST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_IMP_REINVST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_IMP_REINVST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
