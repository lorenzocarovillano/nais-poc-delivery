package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-MM-1O-RAT<br>
 * Variable: L3401-MM-1O-RAT from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401Mm1oRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401Mm1oRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_MM1O_RAT;
    }

    public void setL3401Mm1oRat(short l3401Mm1oRat) {
        writeShortAsPacked(Pos.L3401_MM1O_RAT, l3401Mm1oRat, Len.Int.L3401_MM1O_RAT);
    }

    public void setL3401Mm1oRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_MM1O_RAT, Pos.L3401_MM1O_RAT);
    }

    /**Original name: L3401-MM-1O-RAT<br>*/
    public short getL3401Mm1oRat() {
        return readPackedAsShort(Pos.L3401_MM1O_RAT, Len.Int.L3401_MM1O_RAT);
    }

    public byte[] getL3401Mm1oRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_MM1O_RAT, Pos.L3401_MM1O_RAT);
        return buffer;
    }

    /**Original name: L3401-MM-1O-RAT-NULL<br>*/
    public String getL3401Mm1oRatNull() {
        return readString(Pos.L3401_MM1O_RAT_NULL, Len.L3401_MM1O_RAT_NULL);
    }

    public String getL3401Mm1oRatNullFormatted() {
        return Functions.padBlanks(getL3401Mm1oRatNull(), Len.L3401_MM1O_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_MM1O_RAT = 1;
        public static final int L3401_MM1O_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_MM1O_RAT = 2;
        public static final int L3401_MM1O_RAT_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_MM1O_RAT = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
