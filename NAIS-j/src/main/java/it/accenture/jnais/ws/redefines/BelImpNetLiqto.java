package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMP-NET-LIQTO<br>
 * Variable: BEL-IMP-NET-LIQTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpNetLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpNetLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMP_NET_LIQTO;
    }

    public void setBelImpNetLiqto(AfDecimal belImpNetLiqto) {
        writeDecimalAsPacked(Pos.BEL_IMP_NET_LIQTO, belImpNetLiqto.copy());
    }

    public void setBelImpNetLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMP_NET_LIQTO, Pos.BEL_IMP_NET_LIQTO);
    }

    /**Original name: BEL-IMP-NET-LIQTO<br>*/
    public AfDecimal getBelImpNetLiqto() {
        return readPackedAsDecimal(Pos.BEL_IMP_NET_LIQTO, Len.Int.BEL_IMP_NET_LIQTO, Len.Fract.BEL_IMP_NET_LIQTO);
    }

    public byte[] getBelImpNetLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMP_NET_LIQTO, Pos.BEL_IMP_NET_LIQTO);
        return buffer;
    }

    public void setBelImpNetLiqtoNull(String belImpNetLiqtoNull) {
        writeString(Pos.BEL_IMP_NET_LIQTO_NULL, belImpNetLiqtoNull, Len.BEL_IMP_NET_LIQTO_NULL);
    }

    /**Original name: BEL-IMP-NET-LIQTO-NULL<br>*/
    public String getBelImpNetLiqtoNull() {
        return readString(Pos.BEL_IMP_NET_LIQTO_NULL, Len.BEL_IMP_NET_LIQTO_NULL);
    }

    public String getBelImpNetLiqtoNullFormatted() {
        return Functions.padBlanks(getBelImpNetLiqtoNull(), Len.BEL_IMP_NET_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMP_NET_LIQTO = 1;
        public static final int BEL_IMP_NET_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMP_NET_LIQTO = 8;
        public static final int BEL_IMP_NET_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMP_NET_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMP_NET_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
