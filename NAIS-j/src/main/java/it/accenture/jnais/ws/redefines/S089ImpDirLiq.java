package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-DIR-LIQ<br>
 * Variable: S089-IMP-DIR-LIQ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpDirLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpDirLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_DIR_LIQ;
    }

    public void setWlquImpDirLiq(AfDecimal wlquImpDirLiq) {
        writeDecimalAsPacked(Pos.S089_IMP_DIR_LIQ, wlquImpDirLiq.copy());
    }

    public void setWlquImpDirLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_DIR_LIQ, Pos.S089_IMP_DIR_LIQ);
    }

    /**Original name: WLQU-IMP-DIR-LIQ<br>*/
    public AfDecimal getWlquImpDirLiq() {
        return readPackedAsDecimal(Pos.S089_IMP_DIR_LIQ, Len.Int.WLQU_IMP_DIR_LIQ, Len.Fract.WLQU_IMP_DIR_LIQ);
    }

    public byte[] getWlquImpDirLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_DIR_LIQ, Pos.S089_IMP_DIR_LIQ);
        return buffer;
    }

    public void initWlquImpDirLiqSpaces() {
        fill(Pos.S089_IMP_DIR_LIQ, Len.S089_IMP_DIR_LIQ, Types.SPACE_CHAR);
    }

    public void setWlquImpDirLiqNull(String wlquImpDirLiqNull) {
        writeString(Pos.S089_IMP_DIR_LIQ_NULL, wlquImpDirLiqNull, Len.WLQU_IMP_DIR_LIQ_NULL);
    }

    /**Original name: WLQU-IMP-DIR-LIQ-NULL<br>*/
    public String getWlquImpDirLiqNull() {
        return readString(Pos.S089_IMP_DIR_LIQ_NULL, Len.WLQU_IMP_DIR_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_DIR_LIQ = 1;
        public static final int S089_IMP_DIR_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_DIR_LIQ = 8;
        public static final int WLQU_IMP_DIR_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_DIR_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_DIR_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
