package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ALQ-PROV-ACQ<br>
 * Variable: WTGA-ALQ-PROV-ACQ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAlqProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAlqProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ALQ_PROV_ACQ;
    }

    public void setWtgaAlqProvAcq(AfDecimal wtgaAlqProvAcq) {
        writeDecimalAsPacked(Pos.WTGA_ALQ_PROV_ACQ, wtgaAlqProvAcq.copy());
    }

    public void setWtgaAlqProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ALQ_PROV_ACQ, Pos.WTGA_ALQ_PROV_ACQ);
    }

    /**Original name: WTGA-ALQ-PROV-ACQ<br>*/
    public AfDecimal getWtgaAlqProvAcq() {
        return readPackedAsDecimal(Pos.WTGA_ALQ_PROV_ACQ, Len.Int.WTGA_ALQ_PROV_ACQ, Len.Fract.WTGA_ALQ_PROV_ACQ);
    }

    public byte[] getWtgaAlqProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ALQ_PROV_ACQ, Pos.WTGA_ALQ_PROV_ACQ);
        return buffer;
    }

    public void initWtgaAlqProvAcqSpaces() {
        fill(Pos.WTGA_ALQ_PROV_ACQ, Len.WTGA_ALQ_PROV_ACQ, Types.SPACE_CHAR);
    }

    public void setWtgaAlqProvAcqNull(String wtgaAlqProvAcqNull) {
        writeString(Pos.WTGA_ALQ_PROV_ACQ_NULL, wtgaAlqProvAcqNull, Len.WTGA_ALQ_PROV_ACQ_NULL);
    }

    /**Original name: WTGA-ALQ-PROV-ACQ-NULL<br>*/
    public String getWtgaAlqProvAcqNull() {
        return readString(Pos.WTGA_ALQ_PROV_ACQ_NULL, Len.WTGA_ALQ_PROV_ACQ_NULL);
    }

    public String getWtgaAlqProvAcqNullFormatted() {
        return Functions.padBlanks(getWtgaAlqProvAcqNull(), Len.WTGA_ALQ_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_PROV_ACQ = 1;
        public static final int WTGA_ALQ_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_PROV_ACQ = 4;
        public static final int WTGA_ALQ_PROV_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
