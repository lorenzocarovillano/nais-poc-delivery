package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-CONT<br>
 * Variable: WPCO-DT-CONT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtCont extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtCont() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_CONT;
    }

    public void setWpcoDtCont(int wpcoDtCont) {
        writeIntAsPacked(Pos.WPCO_DT_CONT, wpcoDtCont, Len.Int.WPCO_DT_CONT);
    }

    public void setDpcoDtContFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_CONT, Pos.WPCO_DT_CONT);
    }

    /**Original name: WPCO-DT-CONT<br>*/
    public int getWpcoDtCont() {
        return readPackedAsInt(Pos.WPCO_DT_CONT, Len.Int.WPCO_DT_CONT);
    }

    public byte[] getWpcoDtContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_CONT, Pos.WPCO_DT_CONT);
        return buffer;
    }

    public void setWpcoDtContNull(String wpcoDtContNull) {
        writeString(Pos.WPCO_DT_CONT_NULL, wpcoDtContNull, Len.WPCO_DT_CONT_NULL);
    }

    /**Original name: WPCO-DT-CONT-NULL<br>*/
    public String getWpcoDtContNull() {
        return readString(Pos.WPCO_DT_CONT_NULL, Len.WPCO_DT_CONT_NULL);
    }

    public String getWpcoDtContNullFormatted() {
        return Functions.padBlanks(getWpcoDtContNull(), Len.WPCO_DT_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_CONT = 1;
        public static final int WPCO_DT_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_CONT = 5;
        public static final int WPCO_DT_CONT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_CONT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
