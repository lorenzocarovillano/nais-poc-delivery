package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-1382011L<br>
 * Variable: WDFL-IMPST-VIS-1382011L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVis1382011l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVis1382011l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS1382011L;
    }

    public void setWdflImpstVis1382011l(AfDecimal wdflImpstVis1382011l) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS1382011L, wdflImpstVis1382011l.copy());
    }

    public void setWdflImpstVis1382011lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS1382011L, Pos.WDFL_IMPST_VIS1382011L);
    }

    /**Original name: WDFL-IMPST-VIS-1382011L<br>*/
    public AfDecimal getWdflImpstVis1382011l() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS1382011L, Len.Int.WDFL_IMPST_VIS1382011L, Len.Fract.WDFL_IMPST_VIS1382011L);
    }

    public byte[] getWdflImpstVis1382011lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS1382011L, Pos.WDFL_IMPST_VIS1382011L);
        return buffer;
    }

    public void setWdflImpstVis1382011lNull(String wdflImpstVis1382011lNull) {
        writeString(Pos.WDFL_IMPST_VIS1382011L_NULL, wdflImpstVis1382011lNull, Len.WDFL_IMPST_VIS1382011L_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-1382011L-NULL<br>*/
    public String getWdflImpstVis1382011lNull() {
        return readString(Pos.WDFL_IMPST_VIS1382011L_NULL, Len.WDFL_IMPST_VIS1382011L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS1382011L = 1;
        public static final int WDFL_IMPST_VIS1382011L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS1382011L = 8;
        public static final int WDFL_IMPST_VIS1382011L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS1382011L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS1382011L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
