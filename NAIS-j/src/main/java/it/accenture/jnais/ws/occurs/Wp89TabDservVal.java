package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvp891;
import it.accenture.jnais.copy.Wp89Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WP89-TAB-DSERV-VAL<br>
 * Variables: WP89-TAB-DSERV-VAL from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wp89TabDservVal {

    //==== PROPERTIES ====
    //Original name: LCCVP891
    private Lccvp891 lccvp891 = new Lccvp891();

    //==== METHODS ====
    public void setWp89TabDservValBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp891.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp891.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp891.Len.Int.ID_PTF, 0));
        position += Lccvp891.Len.ID_PTF;
        lccvp891.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp89TabDservValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp891.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp891.getIdPtf(), Lccvp891.Len.Int.ID_PTF, 0);
        position += Lccvp891.Len.ID_PTF;
        lccvp891.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWp89TabDservValSpaces() {
        lccvp891.initLccvp891Spaces();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP89_TAB_DSERV_VAL = WpolStatus.Len.STATUS + Lccvp891.Len.ID_PTF + Wp89Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
