package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-1O-PER-GG<br>
 * Variable: WB03-DUR-1O-PER-GG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Dur1oPerGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Dur1oPerGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR1O_PER_GG;
    }

    public void setWb03Dur1oPerGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR1O_PER_GG, Pos.WB03_DUR1O_PER_GG);
    }

    /**Original name: WB03-DUR-1O-PER-GG<br>*/
    public int getWb03Dur1oPerGg() {
        return readPackedAsInt(Pos.WB03_DUR1O_PER_GG, Len.Int.WB03_DUR1O_PER_GG);
    }

    public byte[] getWb03Dur1oPerGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR1O_PER_GG, Pos.WB03_DUR1O_PER_GG);
        return buffer;
    }

    public void setWb03Dur1oPerGgNull(String wb03Dur1oPerGgNull) {
        writeString(Pos.WB03_DUR1O_PER_GG_NULL, wb03Dur1oPerGgNull, Len.WB03_DUR1O_PER_GG_NULL);
    }

    /**Original name: WB03-DUR-1O-PER-GG-NULL<br>*/
    public String getWb03Dur1oPerGgNull() {
        return readString(Pos.WB03_DUR1O_PER_GG_NULL, Len.WB03_DUR1O_PER_GG_NULL);
    }

    public String getWb03Dur1oPerGgNullFormatted() {
        return Functions.padBlanks(getWb03Dur1oPerGgNull(), Len.WB03_DUR1O_PER_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR1O_PER_GG = 1;
        public static final int WB03_DUR1O_PER_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR1O_PER_GG = 3;
        public static final int WB03_DUR1O_PER_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR1O_PER_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
