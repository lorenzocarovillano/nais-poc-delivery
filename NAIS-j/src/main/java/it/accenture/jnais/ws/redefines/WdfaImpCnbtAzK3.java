package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-CNBT-AZ-K3<br>
 * Variable: WDFA-IMP-CNBT-AZ-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpCnbtAzK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpCnbtAzK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_CNBT_AZ_K3;
    }

    public void setWdfaImpCnbtAzK3(AfDecimal wdfaImpCnbtAzK3) {
        writeDecimalAsPacked(Pos.WDFA_IMP_CNBT_AZ_K3, wdfaImpCnbtAzK3.copy());
    }

    public void setWdfaImpCnbtAzK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_CNBT_AZ_K3, Pos.WDFA_IMP_CNBT_AZ_K3);
    }

    /**Original name: WDFA-IMP-CNBT-AZ-K3<br>*/
    public AfDecimal getWdfaImpCnbtAzK3() {
        return readPackedAsDecimal(Pos.WDFA_IMP_CNBT_AZ_K3, Len.Int.WDFA_IMP_CNBT_AZ_K3, Len.Fract.WDFA_IMP_CNBT_AZ_K3);
    }

    public byte[] getWdfaImpCnbtAzK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_CNBT_AZ_K3, Pos.WDFA_IMP_CNBT_AZ_K3);
        return buffer;
    }

    public void setWdfaImpCnbtAzK3Null(String wdfaImpCnbtAzK3Null) {
        writeString(Pos.WDFA_IMP_CNBT_AZ_K3_NULL, wdfaImpCnbtAzK3Null, Len.WDFA_IMP_CNBT_AZ_K3_NULL);
    }

    /**Original name: WDFA-IMP-CNBT-AZ-K3-NULL<br>*/
    public String getWdfaImpCnbtAzK3Null() {
        return readString(Pos.WDFA_IMP_CNBT_AZ_K3_NULL, Len.WDFA_IMP_CNBT_AZ_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_AZ_K3 = 1;
        public static final int WDFA_IMP_CNBT_AZ_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_AZ_K3 = 8;
        public static final int WDFA_IMP_CNBT_AZ_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_AZ_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_AZ_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
