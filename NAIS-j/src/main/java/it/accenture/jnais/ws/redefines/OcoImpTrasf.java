package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-IMP-TRASF<br>
 * Variable: OCO-IMP-TRASF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoImpTrasf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoImpTrasf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_IMP_TRASF;
    }

    public void setOcoImpTrasf(AfDecimal ocoImpTrasf) {
        writeDecimalAsPacked(Pos.OCO_IMP_TRASF, ocoImpTrasf.copy());
    }

    public void setOcoImpTrasfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_IMP_TRASF, Pos.OCO_IMP_TRASF);
    }

    /**Original name: OCO-IMP-TRASF<br>*/
    public AfDecimal getOcoImpTrasf() {
        return readPackedAsDecimal(Pos.OCO_IMP_TRASF, Len.Int.OCO_IMP_TRASF, Len.Fract.OCO_IMP_TRASF);
    }

    public byte[] getOcoImpTrasfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_IMP_TRASF, Pos.OCO_IMP_TRASF);
        return buffer;
    }

    public void setOcoImpTrasfNull(String ocoImpTrasfNull) {
        writeString(Pos.OCO_IMP_TRASF_NULL, ocoImpTrasfNull, Len.OCO_IMP_TRASF_NULL);
    }

    /**Original name: OCO-IMP-TRASF-NULL<br>*/
    public String getOcoImpTrasfNull() {
        return readString(Pos.OCO_IMP_TRASF_NULL, Len.OCO_IMP_TRASF_NULL);
    }

    public String getOcoImpTrasfNullFormatted() {
        return Functions.padBlanks(getOcoImpTrasfNull(), Len.OCO_IMP_TRASF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_IMP_TRASF = 1;
        public static final int OCO_IMP_TRASF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_IMP_TRASF = 8;
        public static final int OCO_IMP_TRASF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_IMP_TRASF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_IMP_TRASF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
