package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: LCCC0490-TAB-GRZ<br>
 * Variables: LCCC0490-TAB-GRZ from copybook LCCC0490<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0490TabGrz {

    //==== PROPERTIES ====
    /**Original name: LCCC0490-COD-TARI<br>
	 * <pre>      Codice tariffa</pre>*/
    private String codTari = DefaultValues.stringVal(Len.COD_TARI);
    /**Original name: LCCC0490-TP-GAR<br>
	 * <pre>      Tipo garanzia</pre>*/
    private String tpGar = DefaultValues.stringVal(Len.TP_GAR);
    /**Original name: LCCC0490-FRAZIONAMENTO<br>
	 * <pre>      Frazionamento</pre>*/
    private String frazionamento = DefaultValues.stringVal(Len.FRAZIONAMENTO);
    /**Original name: LCCC0490-TP-PER-PRE<br>
	 * <pre>      Tipo periodo premio</pre>*/
    private char tpPerPre = DefaultValues.CHAR_VAL;
    /**Original name: LCCC0490-DT-DECOR<br>
	 * <pre>      Data decorrenza</pre>*/
    private String dtDecor = DefaultValues.stringVal(Len.DT_DECOR);
    /**Original name: LCCC0490-TIPO-RICORRENZA<br>
	 * <pre>      Tipologia ricorrenza
	 *          "Q" --> Quietanzamento
	 *          "G" --> Generazione tranche</pre>*/
    private char tipoRicorrenza = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTabGrzBytes(byte[] buffer, int offset) {
        int position = offset;
        codTari = MarshalByte.readString(buffer, position, Len.COD_TARI);
        position += Len.COD_TARI;
        tpGar = MarshalByte.readFixedString(buffer, position, Len.TP_GAR);
        position += Len.TP_GAR;
        frazionamento = MarshalByte.readFixedString(buffer, position, Len.FRAZIONAMENTO);
        position += Len.FRAZIONAMENTO;
        tpPerPre = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtDecor = MarshalByte.readFixedString(buffer, position, Len.DT_DECOR);
        position += Len.DT_DECOR;
        tipoRicorrenza = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTabGrzBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
        position += Len.COD_TARI;
        MarshalByte.writeString(buffer, position, tpGar, Len.TP_GAR);
        position += Len.TP_GAR;
        MarshalByte.writeString(buffer, position, frazionamento, Len.FRAZIONAMENTO);
        position += Len.FRAZIONAMENTO;
        MarshalByte.writeChar(buffer, position, tpPerPre);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, dtDecor, Len.DT_DECOR);
        position += Len.DT_DECOR;
        MarshalByte.writeChar(buffer, position, tipoRicorrenza);
        return buffer;
    }

    public void initTabGrzSpaces() {
        codTari = "";
        tpGar = "";
        frazionamento = "";
        tpPerPre = Types.SPACE_CHAR;
        dtDecor = "";
        tipoRicorrenza = Types.SPACE_CHAR;
    }

    public void setCodTari(String codTari) {
        this.codTari = Functions.subString(codTari, Len.COD_TARI);
    }

    public String getCodTari() {
        return this.codTari;
    }

    public String getCodTariFormatted() {
        return Functions.padBlanks(getCodTari(), Len.COD_TARI);
    }

    public void setLccc0490TpGar(short lccc0490TpGar) {
        this.tpGar = NumericDisplay.asString(lccc0490TpGar, Len.TP_GAR);
    }

    public void setLccc0490TpGarFormatted(String lccc0490TpGar) {
        this.tpGar = Trunc.toUnsignedNumeric(lccc0490TpGar, Len.TP_GAR);
    }

    public short getLccc0490TpGar() {
        return NumericDisplay.asShort(this.tpGar);
    }

    public String getTpGarFormatted() {
        return this.tpGar;
    }

    public String getTpGarAsString() {
        return getTpGarFormatted();
    }

    public void setLccc0490FrazionamentoFormatted(String lccc0490Frazionamento) {
        this.frazionamento = Trunc.toUnsignedNumeric(lccc0490Frazionamento, Len.FRAZIONAMENTO);
    }

    public int getFrazionamento() {
        return NumericDisplay.asInt(this.frazionamento);
    }

    public String getFrazionamentoFormatted() {
        return this.frazionamento;
    }

    public String getFrazionamentoAsString() {
        return getFrazionamentoFormatted();
    }

    public void setTpPerPre(char tpPerPre) {
        this.tpPerPre = tpPerPre;
    }

    public void setLccc0490TpPerPreFormatted(String lccc0490TpPerPre) {
        setTpPerPre(Functions.charAt(lccc0490TpPerPre, Types.CHAR_SIZE));
    }

    public char getTpPerPre() {
        return this.tpPerPre;
    }

    public void setLccc0490DtDecor(int lccc0490DtDecor) {
        this.dtDecor = NumericDisplay.asString(lccc0490DtDecor, Len.DT_DECOR);
    }

    public void setLccc0490DtDecorFormatted(String lccc0490DtDecor) {
        this.dtDecor = Trunc.toUnsignedNumeric(lccc0490DtDecor, Len.DT_DECOR);
    }

    public int getLccc0490DtDecor() {
        return NumericDisplay.asInt(this.dtDecor);
    }

    public String getDtDecorFormatted() {
        return this.dtDecor;
    }

    public String getDtDecorAsString() {
        return getDtDecorFormatted();
    }

    public void setTipoRicorrenza(char tipoRicorrenza) {
        this.tipoRicorrenza = tipoRicorrenza;
    }

    public void setLccc0490TipoRicorrenzaFormatted(String lccc0490TipoRicorrenza) {
        setTipoRicorrenza(Functions.charAt(lccc0490TipoRicorrenza, Types.CHAR_SIZE));
    }

    public char getTipoRicorrenza() {
        return this.tipoRicorrenza;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_TARI = 12;
        public static final int TP_GAR = 1;
        public static final int FRAZIONAMENTO = 5;
        public static final int TP_PER_PRE = 1;
        public static final int DT_DECOR = 8;
        public static final int TIPO_RICORRENZA = 1;
        public static final int TAB_GRZ = COD_TARI + TP_GAR + FRAZIONAMENTO + TP_PER_PRE + DT_DECOR + TIPO_RICORRENZA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
