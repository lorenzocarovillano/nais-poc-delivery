package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-IMP-MOVTO<br>
 * Variable: RIF-IMP-MOVTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifImpMovto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifImpMovto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_IMP_MOVTO;
    }

    public void setRifImpMovto(AfDecimal rifImpMovto) {
        writeDecimalAsPacked(Pos.RIF_IMP_MOVTO, rifImpMovto.copy());
    }

    public void setRifImpMovtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_IMP_MOVTO, Pos.RIF_IMP_MOVTO);
    }

    /**Original name: RIF-IMP-MOVTO<br>*/
    public AfDecimal getRifImpMovto() {
        return readPackedAsDecimal(Pos.RIF_IMP_MOVTO, Len.Int.RIF_IMP_MOVTO, Len.Fract.RIF_IMP_MOVTO);
    }

    public byte[] getRifImpMovtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_IMP_MOVTO, Pos.RIF_IMP_MOVTO);
        return buffer;
    }

    public void setRifImpMovtoNull(String rifImpMovtoNull) {
        writeString(Pos.RIF_IMP_MOVTO_NULL, rifImpMovtoNull, Len.RIF_IMP_MOVTO_NULL);
    }

    /**Original name: RIF-IMP-MOVTO-NULL<br>*/
    public String getRifImpMovtoNull() {
        return readString(Pos.RIF_IMP_MOVTO_NULL, Len.RIF_IMP_MOVTO_NULL);
    }

    public String getRifImpMovtoNullFormatted() {
        return Functions.padBlanks(getRifImpMovtoNull(), Len.RIF_IMP_MOVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_IMP_MOVTO = 1;
        public static final int RIF_IMP_MOVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_IMP_MOVTO = 8;
        public static final int RIF_IMP_MOVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_IMP_MOVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RIF_IMP_MOVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
