package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IMP-RIMB<br>
 * Variable: WTLI-IMP-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IMP_RIMB;
    }

    public void setWtliImpRimb(AfDecimal wtliImpRimb) {
        writeDecimalAsPacked(Pos.WTLI_IMP_RIMB, wtliImpRimb.copy());
    }

    public void setWtliImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IMP_RIMB, Pos.WTLI_IMP_RIMB);
    }

    /**Original name: WTLI-IMP-RIMB<br>*/
    public AfDecimal getWtliImpRimb() {
        return readPackedAsDecimal(Pos.WTLI_IMP_RIMB, Len.Int.WTLI_IMP_RIMB, Len.Fract.WTLI_IMP_RIMB);
    }

    public byte[] getWtliImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IMP_RIMB, Pos.WTLI_IMP_RIMB);
        return buffer;
    }

    public void initWtliImpRimbSpaces() {
        fill(Pos.WTLI_IMP_RIMB, Len.WTLI_IMP_RIMB, Types.SPACE_CHAR);
    }

    public void setWtliImpRimbNull(String wtliImpRimbNull) {
        writeString(Pos.WTLI_IMP_RIMB_NULL, wtliImpRimbNull, Len.WTLI_IMP_RIMB_NULL);
    }

    /**Original name: WTLI-IMP-RIMB-NULL<br>*/
    public String getWtliImpRimbNull() {
        return readString(Pos.WTLI_IMP_RIMB_NULL, Len.WTLI_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_RIMB = 1;
        public static final int WTLI_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_RIMB = 8;
        public static final int WTLI_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
