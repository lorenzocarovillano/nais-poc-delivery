package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-ETA-MM-1O-ASSTO<br>
 * Variable: L3421-ETA-MM-1O-ASSTO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421EtaMm1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421EtaMm1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_ETA_MM1O_ASSTO;
    }

    public void setL3421EtaMm1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_ETA_MM1O_ASSTO, Pos.L3421_ETA_MM1O_ASSTO);
    }

    /**Original name: L3421-ETA-MM-1O-ASSTO<br>*/
    public short getL3421EtaMm1oAssto() {
        return readPackedAsShort(Pos.L3421_ETA_MM1O_ASSTO, Len.Int.L3421_ETA_MM1O_ASSTO);
    }

    public byte[] getL3421EtaMm1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_ETA_MM1O_ASSTO, Pos.L3421_ETA_MM1O_ASSTO);
        return buffer;
    }

    /**Original name: L3421-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getL3421EtaMm1oAsstoNull() {
        return readString(Pos.L3421_ETA_MM1O_ASSTO_NULL, Len.L3421_ETA_MM1O_ASSTO_NULL);
    }

    public String getL3421EtaMm1oAsstoNullFormatted() {
        return Functions.padBlanks(getL3421EtaMm1oAsstoNull(), Len.L3421_ETA_MM1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_ETA_MM1O_ASSTO = 1;
        public static final int L3421_ETA_MM1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_ETA_MM1O_ASSTO = 2;
        public static final int L3421_ETA_MM1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_ETA_MM1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
