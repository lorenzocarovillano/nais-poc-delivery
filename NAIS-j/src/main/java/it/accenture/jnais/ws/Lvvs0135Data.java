package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv4911;
import it.accenture.jnais.ws.enums.WkIdCodLivFlag;
import it.accenture.jnais.ws.enums.WkNumQuo;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.WkTabArro;
import it.accenture.jnais.ws.occurs.Wl19TabFnd;
import it.accenture.jnais.ws.occurs.WtgaTabTran;
import it.accenture.jnais.ws.occurs.WvasTabValAst;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0135<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0135Data {

    //==== PROPERTIES ====
    public static final int WVAS_TAB_VAL_AST_MAXOCCURS = 1250;
    public static final int WK_TAB_ARRO_MAXOCCURS = 20;
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 20;
    public static final int DGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int DL19_TAB_FND_MAXOCCURS = 250;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0135";
    //Original name: LDBS4910
    private String ldbs4910 = "LDBS4910";
    /**Original name: WK-VAL-FONDO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkValFondo = new AfDecimal("0", 15, 3);
    //Original name: WK-ID-COD-LIV-FLAG
    private WkIdCodLivFlag wkIdCodLivFlag = new WkIdCodLivFlag();
    //Original name: WK-NUM-QUO
    private WkNumQuo wkNumQuo = new WkNumQuo();
    //Original name: WK-APP-ARRO
    private String wkAppArro = "0";
    //Original name: WK-APP-ARRO-1
    private String wkAppArro1 = "0";
    //Original name: WK-TAB-ARRO
    private WkTabArro[] wkTabArro = new WkTabArro[WK_TAB_ARRO_MAXOCCURS];
    //Original name: WK-VARIABILI
    private WkVariabiliLvvs0135 wkVariabili = new WkVariabiliLvvs0135();
    //Original name: VAL-AST
    private ValAstLdbs4910 valAst = new ValAstLdbs4910();
    //Original name: QUOTZ-FND-UNIT
    private QuotzFndUnit quotzFndUnit = new QuotzFndUnit();
    //Original name: LDBV4911
    private Ldbv4911 ldbv4911 = new Ldbv4911();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private WtgaTabTran[] dtgaTabTran = new WtgaTabTran[DTGA_TAB_TRAN_MAXOCCURS];
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DGRZ-TAB-GAR
    private WgrzTabGar[] dgrzTabGar = new WgrzTabGar[DGRZ_TAB_GAR_MAXOCCURS];
    /**Original name: DL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short dl19EleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DL19-TAB-FND
    private Wl19TabFnd[] dl19TabFnd = new Wl19TabFnd[DL19_TAB_FND_MAXOCCURS];
    //Original name: WVAS-ELE-VAL-AST-MAX
    private short wvasEleValAstMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WVAS-TAB-VAL-AST
    private LazyArrayCopy<WvasTabValAst> wvasTabValAst = new LazyArrayCopy<WvasTabValAst>(new WvasTabValAst(), 1, WVAS_TAB_VAL_AST_MAXOCCURS);
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = "00";
    //Original name: IX-INDICI
    private IxIndiciLvvs0116 ixIndici = new IxIndiciLvvs0116();
    /**Original name: WK-LABEL<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);

    //==== CONSTRUCTORS ====
    public Lvvs0135Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wkTabArroIdx = 1; wkTabArroIdx <= WK_TAB_ARRO_MAXOCCURS; wkTabArroIdx++) {
            wkTabArro[wkTabArroIdx - 1] = new WkTabArro();
        }
        for (int dtgaTabTranIdx = 1; dtgaTabTranIdx <= DTGA_TAB_TRAN_MAXOCCURS; dtgaTabTranIdx++) {
            dtgaTabTran[dtgaTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int dgrzTabGarIdx = 1; dgrzTabGarIdx <= DGRZ_TAB_GAR_MAXOCCURS; dgrzTabGarIdx++) {
            dgrzTabGar[dgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int dl19TabFndIdx = 1; dl19TabFndIdx <= DL19_TAB_FND_MAXOCCURS; dl19TabFndIdx++) {
            dl19TabFnd[dl19TabFndIdx - 1] = new Wl19TabFnd();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs4910() {
        return this.ldbs4910;
    }

    public void setWkValFondo(AfDecimal wkValFondo) {
        this.wkValFondo.assign(wkValFondo);
    }

    public AfDecimal getWkValFondo() {
        return this.wkValFondo.copy();
    }

    public void setWkAppArro(short wkAppArro) {
        this.wkAppArro = NumericDisplay.asString(wkAppArro, Len.WK_APP_ARRO);
    }

    public void setWkAppArroFormatted(String wkAppArro) {
        this.wkAppArro = Trunc.toUnsignedNumeric(wkAppArro, Len.WK_APP_ARRO);
    }

    public short getWkAppArro() {
        return NumericDisplay.asShort(this.wkAppArro);
    }

    public String getWkAppArroFormatted() {
        return this.wkAppArro;
    }

    public void setWkAppArro1Formatted(String wkAppArro1) {
        this.wkAppArro1 = Trunc.toUnsignedNumeric(wkAppArro1, Len.WK_APP_ARRO1);
    }

    public short getWkAppArro1() {
        return NumericDisplay.asShort(this.wkAppArro1);
    }

    public void setWkAreaArroFormatted(String data) {
        byte[] buffer = new byte[Len.WK_AREA_ARRO];
        MarshalByte.writeString(buffer, 1, data, Len.WK_AREA_ARRO);
        setWkAreaArroBytes(buffer, 1);
    }

    /**Original name: WK-AREA-ARRO<br>*/
    public byte[] getWkAreaArroBytes() {
        byte[] buffer = new byte[Len.WK_AREA_ARRO];
        return getWkAreaArroBytes(buffer, 1);
    }

    public void setWkAreaArroBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= WK_TAB_ARRO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wkTabArro[idx - 1].setWkTabArroBytes(buffer, position);
                position += WkTabArro.Len.WK_TAB_ARRO;
            }
            else {
                wkTabArro[idx - 1].initWkTabArroSpaces();
                position += WkTabArro.Len.WK_TAB_ARRO;
            }
        }
    }

    public byte[] getWkAreaArroBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= WK_TAB_ARRO_MAXOCCURS; idx++) {
            wkTabArro[idx - 1].getWkTabArroBytes(buffer, position);
            position += WkTabArro.Len.WK_TAB_ARRO;
        }
        return buffer;
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                dtgaTabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDgrzAreaGraFormatted(String data) {
        byte[] buffer = new byte[Len.DGRZ_AREA_GRA];
        MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GRA);
        setDgrzAreaGraBytes(buffer, 1);
    }

    public void setDgrzAreaGraBytes(byte[] buffer, int offset) {
        int position = offset;
        dgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DGRZ_TAB_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dgrzTabGar[idx - 1].setTabGarBytes(buffer, position);
                position += WgrzTabGar.Len.TAB_GAR;
            }
            else {
                dgrzTabGar[idx - 1].initTabGarSpaces();
                position += WgrzTabGar.Len.TAB_GAR;
            }
        }
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setDl19EleFndMax(short dl19EleFndMax) {
        this.dl19EleFndMax = dl19EleFndMax;
    }

    public short getDl19EleFndMax() {
        return this.dl19EleFndMax;
    }

    public void setWvasEleValAstMax(short wvasEleValAstMax) {
        this.wvasEleValAstMax = wvasEleValAstMax;
    }

    public short getWvasEleValAstMax() {
        return this.wvasEleValAstMax;
    }

    public void setInRcodeFormatted(String inRcode) {
        this.inRcode = Trunc.toUnsignedNumeric(inRcode, Len.IN_RCODE);
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public short getInRcode() {
        return NumericDisplay.asShort(this.inRcode);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public WgrzTabGar getDgrzTabGar(int idx) {
        return dgrzTabGar[idx - 1];
    }

    public Wl19TabFnd getDl19TabFnd(int idx) {
        return dl19TabFnd[idx - 1];
    }

    public WtgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran[idx - 1];
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs0116 getIxIndici() {
        return ixIndici;
    }

    public Ldbv4911 getLdbv4911() {
        return ldbv4911;
    }

    public QuotzFndUnit getQuotzFndUnit() {
        return quotzFndUnit;
    }

    public ValAstLdbs4910 getValAst() {
        return valAst;
    }

    public WkIdCodLivFlag getWkIdCodLivFlag() {
        return wkIdCodLivFlag;
    }

    public WkNumQuo getWkNumQuo() {
        return wkNumQuo;
    }

    public WkTabArro getWkTabArro(int idx) {
        return wkTabArro[idx - 1];
    }

    public WkVariabiliLvvs0135 getWkVariabili() {
        return wkVariabili;
    }

    public WvasTabValAst getWvasTabValAst(int idx) {
        return wvasTabValAst.get(idx - 1);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IN_RCODE = 2;
        public static final int WK_LABEL = 30;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + Lvvs0135Data.DTGA_TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;
        public static final int DGRZ_ELE_GAR_MAX = 2;
        public static final int DGRZ_AREA_GRA = DGRZ_ELE_GAR_MAX + Lvvs0135Data.DGRZ_TAB_GAR_MAXOCCURS * WgrzTabGar.Len.TAB_GAR;
        public static final int WK_AREA_ARRO = Lvvs0135Data.WK_TAB_ARRO_MAXOCCURS * WkTabArro.Len.WK_TAB_ARRO;
        public static final int WK_APP_ARRO = 1;
        public static final int WK_APP_ARRO1 = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
