package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-DA-RIMB<br>
 * Variable: S089-IMPST-DA-RIMB from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstDaRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstDaRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_DA_RIMB;
    }

    public void setWlquImpstDaRimb(AfDecimal wlquImpstDaRimb) {
        writeDecimalAsPacked(Pos.S089_IMPST_DA_RIMB, wlquImpstDaRimb.copy());
    }

    public void setWlquImpstDaRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_DA_RIMB, Pos.S089_IMPST_DA_RIMB);
    }

    /**Original name: WLQU-IMPST-DA-RIMB<br>*/
    public AfDecimal getWlquImpstDaRimb() {
        return readPackedAsDecimal(Pos.S089_IMPST_DA_RIMB, Len.Int.WLQU_IMPST_DA_RIMB, Len.Fract.WLQU_IMPST_DA_RIMB);
    }

    public byte[] getWlquImpstDaRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_DA_RIMB, Pos.S089_IMPST_DA_RIMB);
        return buffer;
    }

    public void initWlquImpstDaRimbSpaces() {
        fill(Pos.S089_IMPST_DA_RIMB, Len.S089_IMPST_DA_RIMB, Types.SPACE_CHAR);
    }

    public void setWlquImpstDaRimbNull(String wlquImpstDaRimbNull) {
        writeString(Pos.S089_IMPST_DA_RIMB_NULL, wlquImpstDaRimbNull, Len.WLQU_IMPST_DA_RIMB_NULL);
    }

    /**Original name: WLQU-IMPST-DA-RIMB-NULL<br>*/
    public String getWlquImpstDaRimbNull() {
        return readString(Pos.S089_IMPST_DA_RIMB_NULL, Len.WLQU_IMPST_DA_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_DA_RIMB = 1;
        public static final int S089_IMPST_DA_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_DA_RIMB = 8;
        public static final int WLQU_IMPST_DA_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_DA_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_DA_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
