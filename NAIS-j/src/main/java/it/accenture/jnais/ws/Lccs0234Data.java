package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.ws.enums.FlRicerca;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0234<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0234Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*
	 * --> NOME PROGRAMMA</pre>*/
    private String wkPgm = "LCCS0234";
    //Original name: IX-GRZ
    private short ixGrz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TGA
    private short ixTga = DefaultValues.BIN_SHORT_VAL;
    /**Original name: FL-RICERCA<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAGS
	 * ----------------------------------------------------------------*</pre>*/
    private FlRicerca flRicerca = new FlRicerca();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setIxGrz(short ixGrz) {
        this.ixGrz = ixGrz;
    }

    public short getIxGrz() {
        return this.ixGrz;
    }

    public void setIxTga(short ixTga) {
        this.ixTga = ixTga;
    }

    public short getIxTga() {
        return this.ixTga;
    }

    public FlRicerca getFlRicerca() {
        return flRicerca;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }
}
