package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-IMP-RIMB<br>
 * Variable: PRE-IMP-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_IMP_RIMB;
    }

    public void setPreImpRimb(AfDecimal preImpRimb) {
        writeDecimalAsPacked(Pos.PRE_IMP_RIMB, preImpRimb.copy());
    }

    public void setPreImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_IMP_RIMB, Pos.PRE_IMP_RIMB);
    }

    /**Original name: PRE-IMP-RIMB<br>*/
    public AfDecimal getPreImpRimb() {
        return readPackedAsDecimal(Pos.PRE_IMP_RIMB, Len.Int.PRE_IMP_RIMB, Len.Fract.PRE_IMP_RIMB);
    }

    public byte[] getPreImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_IMP_RIMB, Pos.PRE_IMP_RIMB);
        return buffer;
    }

    public void setPreImpRimbNull(String preImpRimbNull) {
        writeString(Pos.PRE_IMP_RIMB_NULL, preImpRimbNull, Len.PRE_IMP_RIMB_NULL);
    }

    /**Original name: PRE-IMP-RIMB-NULL<br>*/
    public String getPreImpRimbNull() {
        return readString(Pos.PRE_IMP_RIMB_NULL, Len.PRE_IMP_RIMB_NULL);
    }

    public String getPreImpRimbNullFormatted() {
        return Functions.padBlanks(getPreImpRimbNull(), Len.PRE_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_IMP_RIMB = 1;
        public static final int PRE_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_IMP_RIMB = 8;
        public static final int PRE_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
