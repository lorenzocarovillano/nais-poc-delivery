package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WMOV-ID-RICH<br>
 * Variable: WMOV-ID-RICH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmovIdRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmovIdRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMOV_ID_RICH;
    }

    public void setWmovIdRich(int wmovIdRich) {
        writeIntAsPacked(Pos.WMOV_ID_RICH, wmovIdRich, Len.Int.WMOV_ID_RICH);
    }

    public void setWmovIdRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMOV_ID_RICH, Pos.WMOV_ID_RICH);
    }

    /**Original name: WMOV-ID-RICH<br>*/
    public int getWmovIdRich() {
        return readPackedAsInt(Pos.WMOV_ID_RICH, Len.Int.WMOV_ID_RICH);
    }

    public byte[] getWmovIdRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMOV_ID_RICH, Pos.WMOV_ID_RICH);
        return buffer;
    }

    public void setWmovIdRichNull(String wmovIdRichNull) {
        writeString(Pos.WMOV_ID_RICH_NULL, wmovIdRichNull, Len.WMOV_ID_RICH_NULL);
    }

    /**Original name: WMOV-ID-RICH-NULL<br>*/
    public String getWmovIdRichNull() {
        return readString(Pos.WMOV_ID_RICH_NULL, Len.WMOV_ID_RICH_NULL);
    }

    public String getWmovIdRichNullFormatted() {
        return Functions.padBlanks(getWmovIdRichNull(), Len.WMOV_ID_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMOV_ID_RICH = 1;
        public static final int WMOV_ID_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_ID_RICH = 5;
        public static final int WMOV_ID_RICH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMOV_ID_RICH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
