package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.V1391TabParam;

/**Original name: V1391-AREA-ACTU<br>
 * Variable: V1391-AREA-ACTU from copybook LDBV1391<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class V1391AreaActu extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_MAXOCCURS = 100;
    //Original name: V1391-COD-ACTU
    private String codActu = DefaultValues.stringVal(Len.COD_ACTU);
    //Original name: V1391-COD-COMPAGNIA-ANIA
    private String codCompagniaAnia = DefaultValues.stringVal(Len.COD_COMPAGNIA_ANIA);
    //Original name: V1391-ELE-MAX-ACTU
    private String eleMaxActu = DefaultValues.stringVal(Len.ELE_MAX_ACTU);
    //Original name: V1391-TAB-PARAM
    private V1391TabParam[] tabParam = new V1391TabParam[TAB_PARAM_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public V1391AreaActu() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.V1391_AREA_ACTU;
    }

    @Override
    public void deserialize(byte[] buf) {
        setV1391AreaActuBytes(buf);
    }

    public void init() {
        for (int tabParamIdx = 1; tabParamIdx <= TAB_PARAM_MAXOCCURS; tabParamIdx++) {
            tabParam[tabParamIdx - 1] = new V1391TabParam();
        }
    }

    public String getV1391AreaActuFormatted() {
        return MarshalByteExt.bufferToStr(getV1391AreaActuBytes());
    }

    public void setV1391AreaActuBytes(byte[] buffer) {
        setV1391AreaActuBytes(buffer, 1);
    }

    public byte[] getV1391AreaActuBytes() {
        byte[] buffer = new byte[Len.V1391_AREA_ACTU];
        return getV1391AreaActuBytes(buffer, 1);
    }

    public void setV1391AreaActuBytes(byte[] buffer, int offset) {
        int position = offset;
        codActu = MarshalByte.readString(buffer, position, Len.COD_ACTU);
        position += Len.COD_ACTU;
        codCompagniaAnia = MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        eleMaxActu = MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_ACTU);
        position += Len.ELE_MAX_ACTU;
        for (int idx = 1; idx <= TAB_PARAM_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabParam[idx - 1].setTabParamBytes(buffer, position);
                position += V1391TabParam.Len.TAB_PARAM;
            }
            else {
                tabParam[idx - 1].initTabParamSpaces();
                position += V1391TabParam.Len.TAB_PARAM;
            }
        }
    }

    public byte[] getV1391AreaActuBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codActu, Len.COD_ACTU);
        position += Len.COD_ACTU;
        MarshalByte.writeString(buffer, position, codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
        position += Len.COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, eleMaxActu, Len.ELE_MAX_ACTU);
        position += Len.ELE_MAX_ACTU;
        for (int idx = 1; idx <= TAB_PARAM_MAXOCCURS; idx++) {
            tabParam[idx - 1].getTabParamBytes(buffer, position);
            position += V1391TabParam.Len.TAB_PARAM;
        }
        return buffer;
    }

    public void setCodActu(String codActu) {
        this.codActu = Functions.subString(codActu, Len.COD_ACTU);
    }

    public String getCodActu() {
        return this.codActu;
    }

    public String getCodActuFormatted() {
        return Functions.padBlanks(getCodActu(), Len.COD_ACTU);
    }

    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.codCompagniaAnia = NumericDisplay.asString(codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public void setCodCompagniaAniaFormatted(String codCompagniaAnia) {
        this.codCompagniaAnia = Trunc.toUnsignedNumeric(codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
    }

    public int getCodCompagniaAnia() {
        return NumericDisplay.asInt(this.codCompagniaAnia);
    }

    public String getCodCompagniaAniaFormatted() {
        return this.codCompagniaAnia;
    }

    public void setEleMaxActu(short eleMaxActu) {
        this.eleMaxActu = NumericDisplay.asString(eleMaxActu, Len.ELE_MAX_ACTU);
    }

    public void setEleMaxActuFormatted(String eleMaxActu) {
        this.eleMaxActu = Trunc.toUnsignedNumeric(eleMaxActu, Len.ELE_MAX_ACTU);
    }

    public short getEleMaxActu() {
        return NumericDisplay.asShort(this.eleMaxActu);
    }

    public V1391TabParam getTabParam(int idx) {
        return tabParam[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getV1391AreaActuBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ACTU = 12;
        public static final int COD_COMPAGNIA_ANIA = 5;
        public static final int ELE_MAX_ACTU = 2;
        public static final int V1391_AREA_ACTU = COD_ACTU + COD_COMPAGNIA_ANIA + ELE_MAX_ACTU + V1391AreaActu.TAB_PARAM_MAXOCCURS * V1391TabParam.Len.TAB_PARAM;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
