package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-SOST-EFFLQ<br>
 * Variable: DFL-ACCPRE-SOST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreSostEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreSostEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_SOST_EFFLQ;
    }

    public void setDflAccpreSostEfflq(AfDecimal dflAccpreSostEfflq) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_SOST_EFFLQ, dflAccpreSostEfflq.copy());
    }

    public void setDflAccpreSostEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_SOST_EFFLQ, Pos.DFL_ACCPRE_SOST_EFFLQ);
    }

    /**Original name: DFL-ACCPRE-SOST-EFFLQ<br>*/
    public AfDecimal getDflAccpreSostEfflq() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_SOST_EFFLQ, Len.Int.DFL_ACCPRE_SOST_EFFLQ, Len.Fract.DFL_ACCPRE_SOST_EFFLQ);
    }

    public byte[] getDflAccpreSostEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_SOST_EFFLQ, Pos.DFL_ACCPRE_SOST_EFFLQ);
        return buffer;
    }

    public void setDflAccpreSostEfflqNull(String dflAccpreSostEfflqNull) {
        writeString(Pos.DFL_ACCPRE_SOST_EFFLQ_NULL, dflAccpreSostEfflqNull, Len.DFL_ACCPRE_SOST_EFFLQ_NULL);
    }

    /**Original name: DFL-ACCPRE-SOST-EFFLQ-NULL<br>*/
    public String getDflAccpreSostEfflqNull() {
        return readString(Pos.DFL_ACCPRE_SOST_EFFLQ_NULL, Len.DFL_ACCPRE_SOST_EFFLQ_NULL);
    }

    public String getDflAccpreSostEfflqNullFormatted() {
        return Functions.padBlanks(getDflAccpreSostEfflqNull(), Len.DFL_ACCPRE_SOST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_SOST_EFFLQ = 1;
        public static final int DFL_ACCPRE_SOST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_SOST_EFFLQ = 8;
        public static final int DFL_ACCPRE_SOST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_SOST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_SOST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
