package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-GAR-CNBT<br>
 * Variable: WADE-IMP-GAR-CNBT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpGarCnbt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpGarCnbt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_GAR_CNBT;
    }

    public void setWadeImpGarCnbt(AfDecimal wadeImpGarCnbt) {
        writeDecimalAsPacked(Pos.WADE_IMP_GAR_CNBT, wadeImpGarCnbt.copy());
    }

    public void setWadeImpGarCnbtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_GAR_CNBT, Pos.WADE_IMP_GAR_CNBT);
    }

    /**Original name: WADE-IMP-GAR-CNBT<br>*/
    public AfDecimal getWadeImpGarCnbt() {
        return readPackedAsDecimal(Pos.WADE_IMP_GAR_CNBT, Len.Int.WADE_IMP_GAR_CNBT, Len.Fract.WADE_IMP_GAR_CNBT);
    }

    public byte[] getWadeImpGarCnbtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_GAR_CNBT, Pos.WADE_IMP_GAR_CNBT);
        return buffer;
    }

    public void initWadeImpGarCnbtSpaces() {
        fill(Pos.WADE_IMP_GAR_CNBT, Len.WADE_IMP_GAR_CNBT, Types.SPACE_CHAR);
    }

    public void setWadeImpGarCnbtNull(String wadeImpGarCnbtNull) {
        writeString(Pos.WADE_IMP_GAR_CNBT_NULL, wadeImpGarCnbtNull, Len.WADE_IMP_GAR_CNBT_NULL);
    }

    /**Original name: WADE-IMP-GAR-CNBT-NULL<br>*/
    public String getWadeImpGarCnbtNull() {
        return readString(Pos.WADE_IMP_GAR_CNBT_NULL, Len.WADE_IMP_GAR_CNBT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_GAR_CNBT = 1;
        public static final int WADE_IMP_GAR_CNBT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_GAR_CNBT = 8;
        public static final int WADE_IMP_GAR_CNBT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_GAR_CNBT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_GAR_CNBT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
