package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-PC-REVRSB<br>
 * Variable: L3401-PC-REVRSB from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401PcRevrsb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401PcRevrsb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_PC_REVRSB;
    }

    public void setL3401PcRevrsb(AfDecimal l3401PcRevrsb) {
        writeDecimalAsPacked(Pos.L3401_PC_REVRSB, l3401PcRevrsb.copy());
    }

    public void setL3401PcRevrsbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_PC_REVRSB, Pos.L3401_PC_REVRSB);
    }

    /**Original name: L3401-PC-REVRSB<br>*/
    public AfDecimal getL3401PcRevrsb() {
        return readPackedAsDecimal(Pos.L3401_PC_REVRSB, Len.Int.L3401_PC_REVRSB, Len.Fract.L3401_PC_REVRSB);
    }

    public byte[] getL3401PcRevrsbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_PC_REVRSB, Pos.L3401_PC_REVRSB);
        return buffer;
    }

    /**Original name: L3401-PC-REVRSB-NULL<br>*/
    public String getL3401PcRevrsbNull() {
        return readString(Pos.L3401_PC_REVRSB_NULL, Len.L3401_PC_REVRSB_NULL);
    }

    public String getL3401PcRevrsbNullFormatted() {
        return Functions.padBlanks(getL3401PcRevrsbNull(), Len.L3401_PC_REVRSB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_PC_REVRSB = 1;
        public static final int L3401_PC_REVRSB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_PC_REVRSB = 4;
        public static final int L3401_PC_REVRSB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3401_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
