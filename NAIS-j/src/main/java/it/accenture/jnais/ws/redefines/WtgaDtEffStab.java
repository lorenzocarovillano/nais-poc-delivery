package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DT-EFF-STAB<br>
 * Variable: WTGA-DT-EFF-STAB from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDtEffStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDtEffStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DT_EFF_STAB;
    }

    public void setWtgaDtEffStab(int wtgaDtEffStab) {
        writeIntAsPacked(Pos.WTGA_DT_EFF_STAB, wtgaDtEffStab, Len.Int.WTGA_DT_EFF_STAB);
    }

    public void setWtgaDtEffStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DT_EFF_STAB, Pos.WTGA_DT_EFF_STAB);
    }

    /**Original name: WTGA-DT-EFF-STAB<br>*/
    public int getWtgaDtEffStab() {
        return readPackedAsInt(Pos.WTGA_DT_EFF_STAB, Len.Int.WTGA_DT_EFF_STAB);
    }

    public byte[] getWtgaDtEffStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DT_EFF_STAB, Pos.WTGA_DT_EFF_STAB);
        return buffer;
    }

    public void initWtgaDtEffStabSpaces() {
        fill(Pos.WTGA_DT_EFF_STAB, Len.WTGA_DT_EFF_STAB, Types.SPACE_CHAR);
    }

    public void setWtgaDtEffStabNull(String wtgaDtEffStabNull) {
        writeString(Pos.WTGA_DT_EFF_STAB_NULL, wtgaDtEffStabNull, Len.WTGA_DT_EFF_STAB_NULL);
    }

    /**Original name: WTGA-DT-EFF-STAB-NULL<br>*/
    public String getWtgaDtEffStabNull() {
        return readString(Pos.WTGA_DT_EFF_STAB_NULL, Len.WTGA_DT_EFF_STAB_NULL);
    }

    public String getWtgaDtEffStabNullFormatted() {
        return Functions.padBlanks(getWtgaDtEffStabNull(), Len.WTGA_DT_EFF_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DT_EFF_STAB = 1;
        public static final int WTGA_DT_EFF_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DT_EFF_STAB = 5;
        public static final int WTGA_DT_EFF_STAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DT_EFF_STAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
