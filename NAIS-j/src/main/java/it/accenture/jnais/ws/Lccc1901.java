package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Lccc1901FlEseguibile;
import it.accenture.jnais.ws.occurs.Lccc1901CodTariOccurs;

/**Original name: LCCC1901-AREA<br>
 * Variable: LCCC1901-AREA from program LCCS1900<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccc1901 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int COD_TARI_OCCURS_MAXOCCURS = 20;
    //Original name: LCCC1901-GAR-MAX
    private String garMax = DefaultValues.stringVal(Len.GAR_MAX);
    //Original name: LCCC1901-COD-TARI-OCCURS
    private Lccc1901CodTariOccurs[] codTariOccurs = new Lccc1901CodTariOccurs[COD_TARI_OCCURS_MAXOCCURS];
    //Original name: LCCC1901-FL-ESEGUIBILE
    private Lccc1901FlEseguibile flEseguibile = new Lccc1901FlEseguibile();

    //==== CONSTRUCTORS ====
    public Lccc1901() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC1901;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccc1901Bytes(buf);
    }

    public void init() {
        for (int codTariOccursIdx = 1; codTariOccursIdx <= COD_TARI_OCCURS_MAXOCCURS; codTariOccursIdx++) {
            codTariOccurs[codTariOccursIdx - 1] = new Lccc1901CodTariOccurs();
        }
    }

    public String getLccc1901Formatted() {
        return MarshalByteExt.bufferToStr(getLccc1901Bytes());
    }

    public void setLccc1901Bytes(byte[] buffer) {
        setLccc1901Bytes(buffer, 1);
    }

    public byte[] getLccc1901Bytes() {
        byte[] buffer = new byte[Len.LCCC1901];
        return getLccc1901Bytes(buffer, 1);
    }

    public void setLccc1901Bytes(byte[] buffer, int offset) {
        int position = offset;
        garMax = MarshalByte.readFixedString(buffer, position, Len.GAR_MAX);
        position += Len.GAR_MAX;
        for (int idx = 1; idx <= COD_TARI_OCCURS_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                codTariOccurs[idx - 1].setCodTariOccursBytes(buffer, position);
                position += Lccc1901CodTariOccurs.Len.COD_TARI_OCCURS;
            }
            else {
                codTariOccurs[idx - 1].initCodTariOccursSpaces();
                position += Lccc1901CodTariOccurs.Len.COD_TARI_OCCURS;
            }
        }
        flEseguibile.setFlEseguibile(MarshalByte.readChar(buffer, position));
    }

    public byte[] getLccc1901Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, garMax, Len.GAR_MAX);
        position += Len.GAR_MAX;
        for (int idx = 1; idx <= COD_TARI_OCCURS_MAXOCCURS; idx++) {
            codTariOccurs[idx - 1].getCodTariOccursBytes(buffer, position);
            position += Lccc1901CodTariOccurs.Len.COD_TARI_OCCURS;
        }
        MarshalByte.writeChar(buffer, position, flEseguibile.getFlEseguibile());
        return buffer;
    }

    public void setGarMax(short garMax) {
        this.garMax = NumericDisplay.asString(garMax, Len.GAR_MAX);
    }

    public void setGarMaxFormatted(String garMax) {
        this.garMax = Trunc.toUnsignedNumeric(garMax, Len.GAR_MAX);
    }

    public short getGarMax() {
        return NumericDisplay.asShort(this.garMax);
    }

    public Lccc1901CodTariOccurs getCodTariOccurs(int idx) {
        return codTariOccurs[idx - 1];
    }

    public Lccc1901FlEseguibile getFlEseguibile() {
        return flEseguibile;
    }

    @Override
    public byte[] serialize() {
        return getLccc1901Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GAR_MAX = 4;
        public static final int LCCC1901 = GAR_MAX + Lccc1901.COD_TARI_OCCURS_MAXOCCURS * Lccc1901CodTariOccurs.Len.COD_TARI_OCCURS + Lccc1901FlEseguibile.Len.FL_ESEGUIBILE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
