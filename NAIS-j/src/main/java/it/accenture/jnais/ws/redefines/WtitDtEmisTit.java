package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-EMIS-TIT<br>
 * Variable: WTIT-DT-EMIS-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtEmisTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtEmisTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_EMIS_TIT;
    }

    public void setWtitDtEmisTit(int wtitDtEmisTit) {
        writeIntAsPacked(Pos.WTIT_DT_EMIS_TIT, wtitDtEmisTit, Len.Int.WTIT_DT_EMIS_TIT);
    }

    public void setWtitDtEmisTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_EMIS_TIT, Pos.WTIT_DT_EMIS_TIT);
    }

    /**Original name: WTIT-DT-EMIS-TIT<br>*/
    public int getWtitDtEmisTit() {
        return readPackedAsInt(Pos.WTIT_DT_EMIS_TIT, Len.Int.WTIT_DT_EMIS_TIT);
    }

    public byte[] getWtitDtEmisTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_EMIS_TIT, Pos.WTIT_DT_EMIS_TIT);
        return buffer;
    }

    public void initWtitDtEmisTitSpaces() {
        fill(Pos.WTIT_DT_EMIS_TIT, Len.WTIT_DT_EMIS_TIT, Types.SPACE_CHAR);
    }

    public void setWtitDtEmisTitNull(String wtitDtEmisTitNull) {
        writeString(Pos.WTIT_DT_EMIS_TIT_NULL, wtitDtEmisTitNull, Len.WTIT_DT_EMIS_TIT_NULL);
    }

    /**Original name: WTIT-DT-EMIS-TIT-NULL<br>*/
    public String getWtitDtEmisTitNull() {
        return readString(Pos.WTIT_DT_EMIS_TIT_NULL, Len.WTIT_DT_EMIS_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_EMIS_TIT = 1;
        public static final int WTIT_DT_EMIS_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_EMIS_TIT = 5;
        public static final int WTIT_DT_EMIS_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_EMIS_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
