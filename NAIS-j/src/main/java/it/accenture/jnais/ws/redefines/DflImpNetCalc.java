package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-NET-CALC<br>
 * Variable: DFL-IMP-NET-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpNetCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpNetCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_NET_CALC;
    }

    public void setDflImpNetCalc(AfDecimal dflImpNetCalc) {
        writeDecimalAsPacked(Pos.DFL_IMP_NET_CALC, dflImpNetCalc.copy());
    }

    public void setDflImpNetCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_NET_CALC, Pos.DFL_IMP_NET_CALC);
    }

    /**Original name: DFL-IMP-NET-CALC<br>*/
    public AfDecimal getDflImpNetCalc() {
        return readPackedAsDecimal(Pos.DFL_IMP_NET_CALC, Len.Int.DFL_IMP_NET_CALC, Len.Fract.DFL_IMP_NET_CALC);
    }

    public byte[] getDflImpNetCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_NET_CALC, Pos.DFL_IMP_NET_CALC);
        return buffer;
    }

    public void setDflImpNetCalcNull(String dflImpNetCalcNull) {
        writeString(Pos.DFL_IMP_NET_CALC_NULL, dflImpNetCalcNull, Len.DFL_IMP_NET_CALC_NULL);
    }

    /**Original name: DFL-IMP-NET-CALC-NULL<br>*/
    public String getDflImpNetCalcNull() {
        return readString(Pos.DFL_IMP_NET_CALC_NULL, Len.DFL_IMP_NET_CALC_NULL);
    }

    public String getDflImpNetCalcNullFormatted() {
        return Functions.padBlanks(getDflImpNetCalcNull(), Len.DFL_IMP_NET_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_NET_CALC = 1;
        public static final int DFL_IMP_NET_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_NET_CALC = 8;
        public static final int DFL_IMP_NET_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_NET_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_NET_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
