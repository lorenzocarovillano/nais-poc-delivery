package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SOPR-PROF<br>
 * Variable: DTC-SOPR-PROF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_SOPR_PROF;
    }

    public void setDtcSoprProf(AfDecimal dtcSoprProf) {
        writeDecimalAsPacked(Pos.DTC_SOPR_PROF, dtcSoprProf.copy());
    }

    public void setDtcSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_SOPR_PROF, Pos.DTC_SOPR_PROF);
    }

    /**Original name: DTC-SOPR-PROF<br>*/
    public AfDecimal getDtcSoprProf() {
        return readPackedAsDecimal(Pos.DTC_SOPR_PROF, Len.Int.DTC_SOPR_PROF, Len.Fract.DTC_SOPR_PROF);
    }

    public byte[] getDtcSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_SOPR_PROF, Pos.DTC_SOPR_PROF);
        return buffer;
    }

    public void setDtcSoprProfNull(String dtcSoprProfNull) {
        writeString(Pos.DTC_SOPR_PROF_NULL, dtcSoprProfNull, Len.DTC_SOPR_PROF_NULL);
    }

    /**Original name: DTC-SOPR-PROF-NULL<br>*/
    public String getDtcSoprProfNull() {
        return readString(Pos.DTC_SOPR_PROF_NULL, Len.DTC_SOPR_PROF_NULL);
    }

    public String getDtcSoprProfNullFormatted() {
        return Functions.padBlanks(getDtcSoprProfNull(), Len.DTC_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_PROF = 1;
        public static final int DTC_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_PROF = 8;
        public static final int DTC_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
