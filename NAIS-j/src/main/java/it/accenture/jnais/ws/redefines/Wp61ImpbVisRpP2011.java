package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP61-IMPB-VIS-RP-P2011<br>
 * Variable: WP61-IMPB-VIS-RP-P2011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61ImpbVisRpP2011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61ImpbVisRpP2011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_IMPB_VIS_RP_P2011;
    }

    public void setWp61ImpbVisRpP2011(AfDecimal wp61ImpbVisRpP2011) {
        writeDecimalAsPacked(Pos.WP61_IMPB_VIS_RP_P2011, wp61ImpbVisRpP2011.copy());
    }

    public void setWp61ImpbVisRpP2011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_IMPB_VIS_RP_P2011, Pos.WP61_IMPB_VIS_RP_P2011);
    }

    /**Original name: WP61-IMPB-VIS-RP-P2011<br>*/
    public AfDecimal getWp61ImpbVisRpP2011() {
        return readPackedAsDecimal(Pos.WP61_IMPB_VIS_RP_P2011, Len.Int.WP61_IMPB_VIS_RP_P2011, Len.Fract.WP61_IMPB_VIS_RP_P2011);
    }

    public byte[] getWp61ImpbVisRpP2011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_IMPB_VIS_RP_P2011, Pos.WP61_IMPB_VIS_RP_P2011);
        return buffer;
    }

    public void setWp61ImpbVisRpP2011Null(String wp61ImpbVisRpP2011Null) {
        writeString(Pos.WP61_IMPB_VIS_RP_P2011_NULL, wp61ImpbVisRpP2011Null, Len.WP61_IMPB_VIS_RP_P2011_NULL);
    }

    /**Original name: WP61-IMPB-VIS-RP-P2011-NULL<br>*/
    public String getWp61ImpbVisRpP2011Null() {
        return readString(Pos.WP61_IMPB_VIS_RP_P2011_NULL, Len.WP61_IMPB_VIS_RP_P2011_NULL);
    }

    public String getDp61ImpbVisRpP2011NullFormatted() {
        return Functions.padBlanks(getWp61ImpbVisRpP2011Null(), Len.WP61_IMPB_VIS_RP_P2011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_IMPB_VIS_RP_P2011 = 1;
        public static final int WP61_IMPB_VIS_RP_P2011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_IMPB_VIS_RP_P2011 = 8;
        public static final int WP61_IMPB_VIS_RP_P2011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_IMPB_VIS_RP_P2011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_IMPB_VIS_RP_P2011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
