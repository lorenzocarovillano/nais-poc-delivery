package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-STIPULA-FINANZ<br>
 * Variable: WP67-DT-STIPULA-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtStipulaFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtStipulaFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_STIPULA_FINANZ;
    }

    public void setWp67DtStipulaFinanz(int wp67DtStipulaFinanz) {
        writeIntAsPacked(Pos.WP67_DT_STIPULA_FINANZ, wp67DtStipulaFinanz, Len.Int.WP67_DT_STIPULA_FINANZ);
    }

    public void setWp67DtStipulaFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_STIPULA_FINANZ, Pos.WP67_DT_STIPULA_FINANZ);
    }

    /**Original name: WP67-DT-STIPULA-FINANZ<br>*/
    public int getWp67DtStipulaFinanz() {
        return readPackedAsInt(Pos.WP67_DT_STIPULA_FINANZ, Len.Int.WP67_DT_STIPULA_FINANZ);
    }

    public byte[] getWp67DtStipulaFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_STIPULA_FINANZ, Pos.WP67_DT_STIPULA_FINANZ);
        return buffer;
    }

    public void setWp67DtStipulaFinanzNull(String wp67DtStipulaFinanzNull) {
        writeString(Pos.WP67_DT_STIPULA_FINANZ_NULL, wp67DtStipulaFinanzNull, Len.WP67_DT_STIPULA_FINANZ_NULL);
    }

    /**Original name: WP67-DT-STIPULA-FINANZ-NULL<br>*/
    public String getWp67DtStipulaFinanzNull() {
        return readString(Pos.WP67_DT_STIPULA_FINANZ_NULL, Len.WP67_DT_STIPULA_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_STIPULA_FINANZ = 1;
        public static final int WP67_DT_STIPULA_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_STIPULA_FINANZ = 5;
        public static final int WP67_DT_STIPULA_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_STIPULA_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
