package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-INTR-RIT-PAG-C<br>
 * Variable: DFL-IMP-INTR-RIT-PAG-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpIntrRitPagC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpIntrRitPagC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_INTR_RIT_PAG_C;
    }

    public void setDflImpIntrRitPagC(AfDecimal dflImpIntrRitPagC) {
        writeDecimalAsPacked(Pos.DFL_IMP_INTR_RIT_PAG_C, dflImpIntrRitPagC.copy());
    }

    public void setDflImpIntrRitPagCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_INTR_RIT_PAG_C, Pos.DFL_IMP_INTR_RIT_PAG_C);
    }

    /**Original name: DFL-IMP-INTR-RIT-PAG-C<br>*/
    public AfDecimal getDflImpIntrRitPagC() {
        return readPackedAsDecimal(Pos.DFL_IMP_INTR_RIT_PAG_C, Len.Int.DFL_IMP_INTR_RIT_PAG_C, Len.Fract.DFL_IMP_INTR_RIT_PAG_C);
    }

    public byte[] getDflImpIntrRitPagCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_INTR_RIT_PAG_C, Pos.DFL_IMP_INTR_RIT_PAG_C);
        return buffer;
    }

    public void setDflImpIntrRitPagCNull(String dflImpIntrRitPagCNull) {
        writeString(Pos.DFL_IMP_INTR_RIT_PAG_C_NULL, dflImpIntrRitPagCNull, Len.DFL_IMP_INTR_RIT_PAG_C_NULL);
    }

    /**Original name: DFL-IMP-INTR-RIT-PAG-C-NULL<br>*/
    public String getDflImpIntrRitPagCNull() {
        return readString(Pos.DFL_IMP_INTR_RIT_PAG_C_NULL, Len.DFL_IMP_INTR_RIT_PAG_C_NULL);
    }

    public String getDflImpIntrRitPagCNullFormatted() {
        return Functions.padBlanks(getDflImpIntrRitPagCNull(), Len.DFL_IMP_INTR_RIT_PAG_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_INTR_RIT_PAG_C = 1;
        public static final int DFL_IMP_INTR_RIT_PAG_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_INTR_RIT_PAG_C = 8;
        public static final int DFL_IMP_INTR_RIT_PAG_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_INTR_RIT_PAG_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_INTR_RIT_PAG_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
