package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SPE-MED<br>
 * Variable: TIT-TOT-SPE-MED from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SPE_MED;
    }

    public void setTitTotSpeMed(AfDecimal titTotSpeMed) {
        writeDecimalAsPacked(Pos.TIT_TOT_SPE_MED, titTotSpeMed.copy());
    }

    public void setTitTotSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SPE_MED, Pos.TIT_TOT_SPE_MED);
    }

    /**Original name: TIT-TOT-SPE-MED<br>*/
    public AfDecimal getTitTotSpeMed() {
        return readPackedAsDecimal(Pos.TIT_TOT_SPE_MED, Len.Int.TIT_TOT_SPE_MED, Len.Fract.TIT_TOT_SPE_MED);
    }

    public byte[] getTitTotSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SPE_MED, Pos.TIT_TOT_SPE_MED);
        return buffer;
    }

    public void setTitTotSpeMedNull(String titTotSpeMedNull) {
        writeString(Pos.TIT_TOT_SPE_MED_NULL, titTotSpeMedNull, Len.TIT_TOT_SPE_MED_NULL);
    }

    /**Original name: TIT-TOT-SPE-MED-NULL<br>*/
    public String getTitTotSpeMedNull() {
        return readString(Pos.TIT_TOT_SPE_MED_NULL, Len.TIT_TOT_SPE_MED_NULL);
    }

    public String getTitTotSpeMedNullFormatted() {
        return Functions.padBlanks(getTitTotSpeMedNull(), Len.TIT_TOT_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SPE_MED = 1;
        public static final int TIT_TOT_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SPE_MED = 8;
        public static final int TIT_TOT_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
