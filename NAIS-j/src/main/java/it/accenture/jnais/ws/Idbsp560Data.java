package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndQuestAdegVer;
import it.accenture.jnais.copy.QuestAdegVerDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSP560<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsp560Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-QUEST-ADEG-VER
    private IndQuestAdegVer indQuestAdegVer = new IndQuestAdegVer();
    //Original name: QUEST-ADEG-VER-DB
    private QuestAdegVerDb questAdegVerDb = new QuestAdegVerDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndQuestAdegVer getIndQuestAdegVer() {
        return indQuestAdegVer;
    }

    public QuestAdegVerDb getQuestAdegVerDb() {
        return questAdegVerDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
