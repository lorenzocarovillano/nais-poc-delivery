package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-EC-RIV-COLL<br>
 * Variable: WPCO-DT-ULT-EC-RIV-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEcRivColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEcRivColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_EC_RIV_COLL;
    }

    public void setWpcoDtUltEcRivColl(int wpcoDtUltEcRivColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_EC_RIV_COLL, wpcoDtUltEcRivColl, Len.Int.WPCO_DT_ULT_EC_RIV_COLL);
    }

    public void setDpcoDtUltEcRivCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_EC_RIV_COLL, Pos.WPCO_DT_ULT_EC_RIV_COLL);
    }

    /**Original name: WPCO-DT-ULT-EC-RIV-COLL<br>*/
    public int getWpcoDtUltEcRivColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_EC_RIV_COLL, Len.Int.WPCO_DT_ULT_EC_RIV_COLL);
    }

    public byte[] getWpcoDtUltEcRivCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_EC_RIV_COLL, Pos.WPCO_DT_ULT_EC_RIV_COLL);
        return buffer;
    }

    public void setWpcoDtUltEcRivCollNull(String wpcoDtUltEcRivCollNull) {
        writeString(Pos.WPCO_DT_ULT_EC_RIV_COLL_NULL, wpcoDtUltEcRivCollNull, Len.WPCO_DT_ULT_EC_RIV_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULT-EC-RIV-COLL-NULL<br>*/
    public String getWpcoDtUltEcRivCollNull() {
        return readString(Pos.WPCO_DT_ULT_EC_RIV_COLL_NULL, Len.WPCO_DT_ULT_EC_RIV_COLL_NULL);
    }

    public String getWpcoDtUltEcRivCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEcRivCollNull(), Len.WPCO_DT_ULT_EC_RIV_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_RIV_COLL = 1;
        public static final int WPCO_DT_ULT_EC_RIV_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_EC_RIV_COLL = 5;
        public static final int WPCO_DT_ULT_EC_RIV_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_EC_RIV_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
