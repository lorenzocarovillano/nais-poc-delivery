package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-INTMORA-VAL<br>
 * Variable: WK-INTMORA-VAL from program LOAS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkIntmoraVal {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char Y = 'Y';
    public static final char N = 'N';

    //==== METHODS ====
    public void setWkIntmoraVal(char wkIntmoraVal) {
        this.value = wkIntmoraVal;
    }

    public char getWkIntmoraVal() {
        return this.value;
    }

    public void setY() {
        value = Y;
    }

    public boolean isN() {
        return value == N;
    }

    public void setN() {
        value = N;
    }
}
