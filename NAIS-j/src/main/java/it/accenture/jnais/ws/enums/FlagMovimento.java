package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-MOVIMENTO<br>
 * Variable: FLAG-MOVIMENTO from program LVVS0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagMovimento {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK_MOVI = 'S';
    public static final char KO_MOVI = 'N';

    //==== METHODS ====
    public void setFlagMovimento(char flagMovimento) {
        this.value = flagMovimento;
    }

    public char getFlagMovimento() {
        return this.value;
    }

    public boolean isOkMovi() {
        return value == OK_MOVI;
    }

    public void setOkMovi() {
        value = OK_MOVI;
    }

    public boolean isKoMovi() {
        return value == KO_MOVI;
    }

    public void setKoMovi() {
        value = KO_MOVI;
    }
}
