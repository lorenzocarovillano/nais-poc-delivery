package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BTC-DT-EFF<br>
 * Variable: BTC-DT-EFF from program IABS0040<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BtcDtEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BtcDtEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_DT_EFF;
    }

    public void setBtcDtEff(int btcDtEff) {
        writeIntAsPacked(Pos.BTC_DT_EFF, btcDtEff, Len.Int.BTC_DT_EFF);
    }

    public void setBtcDtEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BTC_DT_EFF, Pos.BTC_DT_EFF);
    }

    /**Original name: BTC-DT-EFF<br>*/
    public int getBtcDtEff() {
        return readPackedAsInt(Pos.BTC_DT_EFF, Len.Int.BTC_DT_EFF);
    }

    public byte[] getBtcDtEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BTC_DT_EFF, Pos.BTC_DT_EFF);
        return buffer;
    }

    public void setBtcDtEffNull(String btcDtEffNull) {
        writeString(Pos.BTC_DT_EFF_NULL, btcDtEffNull, Len.BTC_DT_EFF_NULL);
    }

    /**Original name: BTC-DT-EFF-NULL<br>*/
    public String getBtcDtEffNull() {
        return readString(Pos.BTC_DT_EFF_NULL, Len.BTC_DT_EFF_NULL);
    }

    public String getBtcDtEffNullFormatted() {
        return Functions.padBlanks(getBtcDtEffNull(), Len.BTC_DT_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BTC_DT_EFF = 1;
        public static final int BTC_DT_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BTC_DT_EFF = 5;
        public static final int BTC_DT_EFF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BTC_DT_EFF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
