package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ALQ-PROV-RICOR<br>
 * Variable: TGA-ALQ-PROV-RICOR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAlqProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAlqProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ALQ_PROV_RICOR;
    }

    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        writeDecimalAsPacked(Pos.TGA_ALQ_PROV_RICOR, tgaAlqProvRicor.copy());
    }

    public void setTgaAlqProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ALQ_PROV_RICOR, Pos.TGA_ALQ_PROV_RICOR);
    }

    /**Original name: TGA-ALQ-PROV-RICOR<br>*/
    public AfDecimal getTgaAlqProvRicor() {
        return readPackedAsDecimal(Pos.TGA_ALQ_PROV_RICOR, Len.Int.TGA_ALQ_PROV_RICOR, Len.Fract.TGA_ALQ_PROV_RICOR);
    }

    public byte[] getTgaAlqProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ALQ_PROV_RICOR, Pos.TGA_ALQ_PROV_RICOR);
        return buffer;
    }

    public void setTgaAlqProvRicorNull(String tgaAlqProvRicorNull) {
        writeString(Pos.TGA_ALQ_PROV_RICOR_NULL, tgaAlqProvRicorNull, Len.TGA_ALQ_PROV_RICOR_NULL);
    }

    /**Original name: TGA-ALQ-PROV-RICOR-NULL<br>*/
    public String getTgaAlqProvRicorNull() {
        return readString(Pos.TGA_ALQ_PROV_RICOR_NULL, Len.TGA_ALQ_PROV_RICOR_NULL);
    }

    public String getTgaAlqProvRicorNullFormatted() {
        return Functions.padBlanks(getTgaAlqProvRicorNull(), Len.TGA_ALQ_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_PROV_RICOR = 1;
        public static final int TGA_ALQ_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_PROV_RICOR = 4;
        public static final int TGA_ALQ_PROV_RICOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
