package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PRE-TOT<br>
 * Variable: WTIT-TOT-PRE-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PRE_TOT;
    }

    public void setWtitTotPreTot(AfDecimal wtitTotPreTot) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PRE_TOT, wtitTotPreTot.copy());
    }

    public void setWtitTotPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PRE_TOT, Pos.WTIT_TOT_PRE_TOT);
    }

    /**Original name: WTIT-TOT-PRE-TOT<br>*/
    public AfDecimal getWtitTotPreTot() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PRE_TOT, Len.Int.WTIT_TOT_PRE_TOT, Len.Fract.WTIT_TOT_PRE_TOT);
    }

    public byte[] getWtitTotPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PRE_TOT, Pos.WTIT_TOT_PRE_TOT);
        return buffer;
    }

    public void initWtitTotPreTotSpaces() {
        fill(Pos.WTIT_TOT_PRE_TOT, Len.WTIT_TOT_PRE_TOT, Types.SPACE_CHAR);
    }

    public void setWtitTotPreTotNull(String wtitTotPreTotNull) {
        writeString(Pos.WTIT_TOT_PRE_TOT_NULL, wtitTotPreTotNull, Len.WTIT_TOT_PRE_TOT_NULL);
    }

    /**Original name: WTIT-TOT-PRE-TOT-NULL<br>*/
    public String getWtitTotPreTotNull() {
        return readString(Pos.WTIT_TOT_PRE_TOT_NULL, Len.WTIT_TOT_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_TOT = 1;
        public static final int WTIT_TOT_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_TOT = 8;
        public static final int WTIT_TOT_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
