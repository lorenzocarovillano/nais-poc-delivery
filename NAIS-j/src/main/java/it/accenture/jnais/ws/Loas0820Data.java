package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ldbv3611;
import it.accenture.jnais.copy.Loar0820;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.RappRete;
import it.accenture.jnais.copy.TrchLiq;
import it.accenture.jnais.ws.enums.WkFineMov;
import it.accenture.jnais.ws.enums.WkFineTli;
import it.accenture.jnais.ws.enums.WkMov;
import it.accenture.jnais.ws.enums.WkOutriva;
import it.accenture.jnais.ws.enums.WkRiscParz;
import it.accenture.jnais.ws.enums.WkVersAgg;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpRete;
import it.accenture.jnais.ws.occurs.WkMovEle;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0820<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0820Data {

    //==== PROPERTIES ====
    public static final int WK_MOV_ELE_MAXOCCURS = 100;
    /**Original name: FS-OUT<br>
	 * <pre>  -- File Status</pre>*/
    private String fsOut = "00";
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: RAPP-RETE
    private RappRete rappRete = new RappRete();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: TRCH-LIQ
    private TrchLiq trchLiq = new TrchLiq();
    //Original name: LDBV3611
    private Ldbv3611 ldbv3611 = new Ldbv3611();
    //Original name: LDBV3621-ID-MOVI-CRZ
    private int ldbv3621IdMoviCrz = DefaultValues.INT_VAL;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *  COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LOAS0820";
    //Original name: LDBS4240
    private String ldbs4240 = "LDBS4240";
    //Original name: LDBS3610
    private String ldbs3610 = "LDBS3610";
    //Original name: LDBS3620
    private String ldbs3620 = "LDBS3620";
    //Original name: LOAR0820
    private Loar0820 loar0820 = new Loar0820();
    /**Original name: WK-OUTRIVA<br>
	 * <pre> --> Falg di stato apertura file di output</pre>*/
    private WkOutriva wkOutriva = new WkOutriva();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    /**Original name: WK-DT-ELAB<br>
	 * <pre>----------------------------------------------------------------*
	 *  VARIBILI E CAMPI DI COMODO
	 * ----------------------------------------------------------------*</pre>*/
    private String wkDtElab = DefaultValues.stringVal(Len.WK_DT_ELAB);
    //Original name: WK-PRE-NET
    private AfDecimal wkPreNet = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-RIS-MAT
    private AfDecimal wkRisMat = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-PRSTZ-ULT
    private AfDecimal wkPrstzUlt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-INCR-PRSTZ
    private AfDecimal wkIncrPrstz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-IMP-LRD-EFFLQ
    private AfDecimal wkImpLrdEfflq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-GESTIONE-MSG-ERR
    private WkGestioneMsgErr wkGestioneMsgErr = new WkGestioneMsgErr();
    /**Original name: WK-APPO-DT-NO-SEGNO<br>
	 * <pre> --> Formattazione Date</pre>*/
    private String wkAppoDtNoSegno = DefaultValues.stringVal(Len.WK_APPO_DT_NO_SEGNO);
    //Original name: WK-APPO-DT-N
    private WkAppoDtN wkAppoDtN = new WkAppoDtN();
    //Original name: WK-APPO-DT-X
    private WkAppoDtX wkAppoDtX = new WkAppoDtX();
    //Original name: FILLER-WK-APPO-DT-7
    private String flr1 = DefaultValues.stringVal(Len.FLR1);
    //Original name: WK-DT-7
    private String wkDt7 = DefaultValues.stringVal(Len.WK_DT7);
    //Original name: WK-FINE-MOV
    private WkFineMov wkFineMov = new WkFineMov();
    //Original name: WK-FINE-TLI
    private WkFineTli wkFineTli = new WkFineTli();
    //Original name: WK-VERS-AGG
    private WkVersAgg wkVersAgg = new WkVersAgg();
    //Original name: WK-RISC-PARZ
    private WkRiscParz wkRiscParz = new WkRiscParz();
    //Original name: WK-MOV-ELE
    private WkMovEle[] wkMovEle = new WkMovEle[WK_MOV_ELE_MAXOCCURS];
    //Original name: WK-IND-MOV
    private short wkIndMov = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK-IND-MOV-MAX
    private short wkIndMovMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK-MOV
    private WkMov wkMov = new WkMov();
    //Original name: IX-INDICI
    private IxIndiciLoas0820 ixIndici = new IxIndiciLoas0820();
    /**Original name: WS-TP-RETE<br>
	 * <pre>*****************************************************************
	 *     TP_RETE (TIPO RAPPORTO RETE)
	 * *****************************************************************</pre>*/
    private WsTpRete wsTpRete = new WsTpRete();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();

    //==== CONSTRUCTORS ====
    public Loas0820Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wkMovEleIdx = 1; wkMovEleIdx <= WK_MOV_ELE_MAXOCCURS; wkMovEleIdx++) {
            wkMovEle[wkMovEleIdx - 1] = new WkMovEle();
        }
    }

    public void setFsOut(String fsOut) {
        this.fsOut = Functions.subString(fsOut, Len.FS_OUT);
    }

    public String getFsOut() {
        return this.fsOut;
    }

    public String getLdbv3621Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv3621Bytes());
    }

    /**Original name: LDBV3621<br>
	 * <pre>  --> WHERE CONDITION TRANCHE DI LIQUIDAZIONE</pre>*/
    public byte[] getLdbv3621Bytes() {
        byte[] buffer = new byte[Len.LDBV3621];
        return getLdbv3621Bytes(buffer, 1);
    }

    public byte[] getLdbv3621Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv3621IdMoviCrz, Len.Int.LDBV3621_ID_MOVI_CRZ, 0);
        return buffer;
    }

    public void setLdbv3621IdMoviCrz(int ldbv3621IdMoviCrz) {
        this.ldbv3621IdMoviCrz = ldbv3621IdMoviCrz;
    }

    public int getLdbv3621IdMoviCrz() {
        return this.ldbv3621IdMoviCrz;
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs4240() {
        return this.ldbs4240;
    }

    public String getLdbs3610() {
        return this.ldbs3610;
    }

    public String getLdbs3620() {
        return this.ldbs3620;
    }

    public void setWkDtElabFormatted(String wkDtElab) {
        this.wkDtElab = Trunc.toUnsignedNumeric(wkDtElab, Len.WK_DT_ELAB);
    }

    public int getWkDtElab() {
        return NumericDisplay.asInt(this.wkDtElab);
    }

    public String getWkDtElabFormatted() {
        return this.wkDtElab;
    }

    public void setWkPreNet(AfDecimal wkPreNet) {
        this.wkPreNet.assign(wkPreNet);
    }

    public AfDecimal getWkPreNet() {
        return this.wkPreNet.copy();
    }

    public void setWkRisMat(AfDecimal wkRisMat) {
        this.wkRisMat.assign(wkRisMat);
    }

    public AfDecimal getWkRisMat() {
        return this.wkRisMat.copy();
    }

    public void setWkPrstzUlt(AfDecimal wkPrstzUlt) {
        this.wkPrstzUlt.assign(wkPrstzUlt);
    }

    public AfDecimal getWkPrstzUlt() {
        return this.wkPrstzUlt.copy();
    }

    public void setWkIncrPrstz(AfDecimal wkIncrPrstz) {
        this.wkIncrPrstz.assign(wkIncrPrstz);
    }

    public AfDecimal getWkIncrPrstz() {
        return this.wkIncrPrstz.copy();
    }

    public void setWkImpLrdEfflq(AfDecimal wkImpLrdEfflq) {
        this.wkImpLrdEfflq.assign(wkImpLrdEfflq);
    }

    public AfDecimal getWkImpLrdEfflq() {
        return this.wkImpLrdEfflq.copy();
    }

    public void setWkAppoDtNoSegno(int wkAppoDtNoSegno) {
        this.wkAppoDtNoSegno = NumericDisplay.asString(wkAppoDtNoSegno, Len.WK_APPO_DT_NO_SEGNO);
    }

    public void setWkAppoDtNoSegnoFormatted(String wkAppoDtNoSegno) {
        this.wkAppoDtNoSegno = Trunc.toUnsignedNumeric(wkAppoDtNoSegno, Len.WK_APPO_DT_NO_SEGNO);
    }

    public void setWkAppoDtNoSegnoFromBuffer(byte[] buffer) {
        wkAppoDtNoSegno = MarshalByte.readFixedString(buffer, 1, Len.WK_APPO_DT_NO_SEGNO);
    }

    public int getWkAppoDtNoSegno() {
        return NumericDisplay.asInt(this.wkAppoDtNoSegno);
    }

    public String getWkAppoDtNoSegnoFormatted() {
        return this.wkAppoDtNoSegno;
    }

    public void setWkAppoDt7Bytes(byte[] buffer) {
        setWkAppoDt7Bytes(buffer, 1);
    }

    public void setWkAppoDt7Bytes(byte[] buffer, int offset) {
        int position = offset;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
        position += Len.FLR1;
        wkDt7 = MarshalByte.readString(buffer, position, Len.WK_DT7);
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public void setWkDt7(String wkDt7) {
        this.wkDt7 = Functions.subString(wkDt7, Len.WK_DT7);
    }

    public String getWkDt7() {
        return this.wkDt7;
    }

    public void setWkIndMov(short wkIndMov) {
        this.wkIndMov = wkIndMov;
    }

    public short getWkIndMov() {
        return this.wkIndMov;
    }

    public void setWkIndMovMax(short wkIndMovMax) {
        this.wkIndMovMax = wkIndMovMax;
    }

    public short getWkIndMovMax() {
        return this.wkIndMovMax;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IxIndiciLoas0820 getIxIndici() {
        return ixIndici;
    }

    public Ldbv3611 getLdbv3611() {
        return ldbv3611;
    }

    public Loar0820 getLoar0820() {
        return loar0820;
    }

    public Movi getMovi() {
        return movi;
    }

    public RappRete getRappRete() {
        return rappRete;
    }

    public TrchLiq getTrchLiq() {
        return trchLiq;
    }

    public WkAppoDtN getWkAppoDtN() {
        return wkAppoDtN;
    }

    public WkAppoDtX getWkAppoDtX() {
        return wkAppoDtX;
    }

    public WkFineMov getWkFineMov() {
        return wkFineMov;
    }

    public WkFineTli getWkFineTli() {
        return wkFineTli;
    }

    public WkGestioneMsgErr getWkGestioneMsgErr() {
        return wkGestioneMsgErr;
    }

    public WkMov getWkMov() {
        return wkMov;
    }

    public WkMovEle getWkMovEle(int idx) {
        return wkMovEle[idx - 1];
    }

    public WkOutriva getWkOutriva() {
        return wkOutriva;
    }

    public WkRiscParz getWkRiscParz() {
        return wkRiscParz;
    }

    public WkVersAgg getWkVersAgg() {
        return wkVersAgg;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpRete getWsTpRete() {
        return wsTpRete;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_DT_ELAB = 8;
        public static final int WK_APPO_DT_NO_SEGNO = 8;
        public static final int FLR1 = 3;
        public static final int WK_DT7 = 7;
        public static final int FS_OUT = 2;
        public static final int LDBV3621_ID_MOVI_CRZ = 5;
        public static final int LDBV3621 = LDBV3621_ID_MOVI_CRZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV3621_ID_MOVI_CRZ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
