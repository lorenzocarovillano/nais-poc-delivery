package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-LRD-EFFLQ<br>
 * Variable: WDFL-IMP-LRD-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpLrdEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpLrdEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_LRD_EFFLQ;
    }

    public void setWdflImpLrdEfflq(AfDecimal wdflImpLrdEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMP_LRD_EFFLQ, wdflImpLrdEfflq.copy());
    }

    public void setWdflImpLrdEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_LRD_EFFLQ, Pos.WDFL_IMP_LRD_EFFLQ);
    }

    /**Original name: WDFL-IMP-LRD-EFFLQ<br>*/
    public AfDecimal getWdflImpLrdEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMP_LRD_EFFLQ, Len.Int.WDFL_IMP_LRD_EFFLQ, Len.Fract.WDFL_IMP_LRD_EFFLQ);
    }

    public byte[] getWdflImpLrdEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_LRD_EFFLQ, Pos.WDFL_IMP_LRD_EFFLQ);
        return buffer;
    }

    public void setWdflImpLrdEfflqNull(String wdflImpLrdEfflqNull) {
        writeString(Pos.WDFL_IMP_LRD_EFFLQ_NULL, wdflImpLrdEfflqNull, Len.WDFL_IMP_LRD_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMP-LRD-EFFLQ-NULL<br>*/
    public String getWdflImpLrdEfflqNull() {
        return readString(Pos.WDFL_IMP_LRD_EFFLQ_NULL, Len.WDFL_IMP_LRD_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_LRD_EFFLQ = 1;
        public static final int WDFL_IMP_LRD_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_LRD_EFFLQ = 8;
        public static final int WDFL_IMP_LRD_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_LRD_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_LRD_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
