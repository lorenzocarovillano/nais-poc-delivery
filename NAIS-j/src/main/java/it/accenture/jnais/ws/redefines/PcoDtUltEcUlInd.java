package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-UL-IND<br>
 * Variable: PCO-DT-ULT-EC-UL-IND from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcUlInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcUlInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_UL_IND;
    }

    public void setPcoDtUltEcUlInd(int pcoDtUltEcUlInd) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_UL_IND, pcoDtUltEcUlInd, Len.Int.PCO_DT_ULT_EC_UL_IND);
    }

    public void setPcoDtUltEcUlIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_UL_IND, Pos.PCO_DT_ULT_EC_UL_IND);
    }

    /**Original name: PCO-DT-ULT-EC-UL-IND<br>*/
    public int getPcoDtUltEcUlInd() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_UL_IND, Len.Int.PCO_DT_ULT_EC_UL_IND);
    }

    public byte[] getPcoDtUltEcUlIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_UL_IND, Pos.PCO_DT_ULT_EC_UL_IND);
        return buffer;
    }

    public void initPcoDtUltEcUlIndHighValues() {
        fill(Pos.PCO_DT_ULT_EC_UL_IND, Len.PCO_DT_ULT_EC_UL_IND, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcUlIndNull(String pcoDtUltEcUlIndNull) {
        writeString(Pos.PCO_DT_ULT_EC_UL_IND_NULL, pcoDtUltEcUlIndNull, Len.PCO_DT_ULT_EC_UL_IND_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-UL-IND-NULL<br>*/
    public String getPcoDtUltEcUlIndNull() {
        return readString(Pos.PCO_DT_ULT_EC_UL_IND_NULL, Len.PCO_DT_ULT_EC_UL_IND_NULL);
    }

    public String getPcoDtUltEcUlIndNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcUlIndNull(), Len.PCO_DT_ULT_EC_UL_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_UL_IND = 1;
        public static final int PCO_DT_ULT_EC_UL_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_UL_IND = 5;
        public static final int PCO_DT_ULT_EC_UL_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_UL_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
