package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-AT92-C<br>
 * Variable: PCO-DT-ULT-ELAB-AT92-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabAt92C extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabAt92C() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_AT92_C;
    }

    public void setPcoDtUltElabAt92C(int pcoDtUltElabAt92C) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_AT92_C, pcoDtUltElabAt92C, Len.Int.PCO_DT_ULT_ELAB_AT92_C);
    }

    public void setPcoDtUltElabAt92CFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT92_C, Pos.PCO_DT_ULT_ELAB_AT92_C);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT92-C<br>*/
    public int getPcoDtUltElabAt92C() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_AT92_C, Len.Int.PCO_DT_ULT_ELAB_AT92_C);
    }

    public byte[] getPcoDtUltElabAt92CAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT92_C, Pos.PCO_DT_ULT_ELAB_AT92_C);
        return buffer;
    }

    public void initPcoDtUltElabAt92CHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_AT92_C, Len.PCO_DT_ULT_ELAB_AT92_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabAt92CNull(String pcoDtUltElabAt92CNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_AT92_C_NULL, pcoDtUltElabAt92CNull, Len.PCO_DT_ULT_ELAB_AT92_C_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT92-C-NULL<br>*/
    public String getPcoDtUltElabAt92CNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_AT92_C_NULL, Len.PCO_DT_ULT_ELAB_AT92_C_NULL);
    }

    public String getPcoDtUltElabAt92CNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabAt92CNull(), Len.PCO_DT_ULT_ELAB_AT92_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT92_C = 1;
        public static final int PCO_DT_ULT_ELAB_AT92_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT92_C = 5;
        public static final int PCO_DT_ULT_ELAB_AT92_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_AT92_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
