package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-6101<br>
 * Variable: SW-6101 from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Sw6101 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO6101 = 'S';
    public static final char NO_TROVATO6101 = 'N';

    //==== METHODS ====
    public void setSw6101(char sw6101) {
        this.value = sw6101;
    }

    public char getSw6101() {
        return this.value;
    }

    public boolean isTrovato6101() {
        return value == TROVATO6101;
    }

    public void setTrovato6101() {
        value = TROVATO6101;
    }

    public boolean isNoTrovato6101() {
        return value == NO_TROVATO6101;
    }

    public void setNoTrovato6101() {
        value = NO_TROVATO6101;
    }
}
