package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-RIAT-RIASS-RSH<br>
 * Variable: WPCO-DT-RIAT-RIASS-RSH from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtRiatRiassRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtRiatRiassRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_RIAT_RIASS_RSH;
    }

    public void setWpcoDtRiatRiassRsh(int wpcoDtRiatRiassRsh) {
        writeIntAsPacked(Pos.WPCO_DT_RIAT_RIASS_RSH, wpcoDtRiatRiassRsh, Len.Int.WPCO_DT_RIAT_RIASS_RSH);
    }

    public void setDpcoDtRiatRiassRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_RIAT_RIASS_RSH, Pos.WPCO_DT_RIAT_RIASS_RSH);
    }

    /**Original name: WPCO-DT-RIAT-RIASS-RSH<br>*/
    public int getWpcoDtRiatRiassRsh() {
        return readPackedAsInt(Pos.WPCO_DT_RIAT_RIASS_RSH, Len.Int.WPCO_DT_RIAT_RIASS_RSH);
    }

    public byte[] getWpcoDtRiatRiassRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_RIAT_RIASS_RSH, Pos.WPCO_DT_RIAT_RIASS_RSH);
        return buffer;
    }

    public void setWpcoDtRiatRiassRshNull(String wpcoDtRiatRiassRshNull) {
        writeString(Pos.WPCO_DT_RIAT_RIASS_RSH_NULL, wpcoDtRiatRiassRshNull, Len.WPCO_DT_RIAT_RIASS_RSH_NULL);
    }

    /**Original name: WPCO-DT-RIAT-RIASS-RSH-NULL<br>*/
    public String getWpcoDtRiatRiassRshNull() {
        return readString(Pos.WPCO_DT_RIAT_RIASS_RSH_NULL, Len.WPCO_DT_RIAT_RIASS_RSH_NULL);
    }

    public String getWpcoDtRiatRiassRshNullFormatted() {
        return Functions.padBlanks(getWpcoDtRiatRiassRshNull(), Len.WPCO_DT_RIAT_RIASS_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_RIAT_RIASS_RSH = 1;
        public static final int WPCO_DT_RIAT_RIASS_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_RIAT_RIASS_RSH = 5;
        public static final int WPCO_DT_RIAT_RIASS_RSH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_RIAT_RIASS_RSH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
