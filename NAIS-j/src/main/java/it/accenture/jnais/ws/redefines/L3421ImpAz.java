package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-AZ<br>
 * Variable: L3421-IMP-AZ from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_AZ;
    }

    public void setL3421ImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_AZ, Pos.L3421_IMP_AZ);
    }

    /**Original name: L3421-IMP-AZ<br>*/
    public AfDecimal getL3421ImpAz() {
        return readPackedAsDecimal(Pos.L3421_IMP_AZ, Len.Int.L3421_IMP_AZ, Len.Fract.L3421_IMP_AZ);
    }

    public byte[] getL3421ImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_AZ, Pos.L3421_IMP_AZ);
        return buffer;
    }

    /**Original name: L3421-IMP-AZ-NULL<br>*/
    public String getL3421ImpAzNull() {
        return readString(Pos.L3421_IMP_AZ_NULL, Len.L3421_IMP_AZ_NULL);
    }

    public String getL3421ImpAzNullFormatted() {
        return Functions.padBlanks(getL3421ImpAzNull(), Len.L3421_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_AZ = 1;
        public static final int L3421_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_AZ = 8;
        public static final int L3421_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
