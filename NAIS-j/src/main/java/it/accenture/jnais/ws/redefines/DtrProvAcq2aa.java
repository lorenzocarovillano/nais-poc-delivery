package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PROV-ACQ-2AA<br>
 * Variable: DTR-PROV-ACQ-2AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PROV_ACQ2AA;
    }

    public void setDtrProvAcq2aa(AfDecimal dtrProvAcq2aa) {
        writeDecimalAsPacked(Pos.DTR_PROV_ACQ2AA, dtrProvAcq2aa.copy());
    }

    public void setDtrProvAcq2aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PROV_ACQ2AA, Pos.DTR_PROV_ACQ2AA);
    }

    /**Original name: DTR-PROV-ACQ-2AA<br>*/
    public AfDecimal getDtrProvAcq2aa() {
        return readPackedAsDecimal(Pos.DTR_PROV_ACQ2AA, Len.Int.DTR_PROV_ACQ2AA, Len.Fract.DTR_PROV_ACQ2AA);
    }

    public byte[] getDtrProvAcq2aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PROV_ACQ2AA, Pos.DTR_PROV_ACQ2AA);
        return buffer;
    }

    public void setDtrProvAcq2aaNull(String dtrProvAcq2aaNull) {
        writeString(Pos.DTR_PROV_ACQ2AA_NULL, dtrProvAcq2aaNull, Len.DTR_PROV_ACQ2AA_NULL);
    }

    /**Original name: DTR-PROV-ACQ-2AA-NULL<br>*/
    public String getDtrProvAcq2aaNull() {
        return readString(Pos.DTR_PROV_ACQ2AA_NULL, Len.DTR_PROV_ACQ2AA_NULL);
    }

    public String getDtrProvAcq2aaNullFormatted() {
        return Functions.padBlanks(getDtrProvAcq2aaNull(), Len.DTR_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PROV_ACQ2AA = 1;
        public static final int DTR_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PROV_ACQ2AA = 8;
        public static final int DTR_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
