package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PROV-ACQ-RICOR<br>
 * Variable: B03-PROV-ACQ-RICOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03ProvAcqRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03ProvAcqRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PROV_ACQ_RICOR;
    }

    public void setB03ProvAcqRicor(AfDecimal b03ProvAcqRicor) {
        writeDecimalAsPacked(Pos.B03_PROV_ACQ_RICOR, b03ProvAcqRicor.copy());
    }

    public void setB03ProvAcqRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PROV_ACQ_RICOR, Pos.B03_PROV_ACQ_RICOR);
    }

    /**Original name: B03-PROV-ACQ-RICOR<br>*/
    public AfDecimal getB03ProvAcqRicor() {
        return readPackedAsDecimal(Pos.B03_PROV_ACQ_RICOR, Len.Int.B03_PROV_ACQ_RICOR, Len.Fract.B03_PROV_ACQ_RICOR);
    }

    public byte[] getB03ProvAcqRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PROV_ACQ_RICOR, Pos.B03_PROV_ACQ_RICOR);
        return buffer;
    }

    public void setB03ProvAcqRicorNull(String b03ProvAcqRicorNull) {
        writeString(Pos.B03_PROV_ACQ_RICOR_NULL, b03ProvAcqRicorNull, Len.B03_PROV_ACQ_RICOR_NULL);
    }

    /**Original name: B03-PROV-ACQ-RICOR-NULL<br>*/
    public String getB03ProvAcqRicorNull() {
        return readString(Pos.B03_PROV_ACQ_RICOR_NULL, Len.B03_PROV_ACQ_RICOR_NULL);
    }

    public String getB03ProvAcqRicorNullFormatted() {
        return Functions.padBlanks(getB03ProvAcqRicorNull(), Len.B03_PROV_ACQ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PROV_ACQ_RICOR = 1;
        public static final int B03_PROV_ACQ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PROV_ACQ_RICOR = 8;
        public static final int B03_PROV_ACQ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PROV_ACQ_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PROV_ACQ_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
