package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-COS-RUN-ASSVA-IDC<br>
 * Variable: TGA-COS-RUN-ASSVA-IDC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCosRunAssvaIdc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCosRunAssvaIdc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_COS_RUN_ASSVA_IDC;
    }

    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        writeDecimalAsPacked(Pos.TGA_COS_RUN_ASSVA_IDC, tgaCosRunAssvaIdc.copy());
    }

    public void setTgaCosRunAssvaIdcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_COS_RUN_ASSVA_IDC, Pos.TGA_COS_RUN_ASSVA_IDC);
    }

    /**Original name: TGA-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getTgaCosRunAssvaIdc() {
        return readPackedAsDecimal(Pos.TGA_COS_RUN_ASSVA_IDC, Len.Int.TGA_COS_RUN_ASSVA_IDC, Len.Fract.TGA_COS_RUN_ASSVA_IDC);
    }

    public byte[] getTgaCosRunAssvaIdcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_COS_RUN_ASSVA_IDC, Pos.TGA_COS_RUN_ASSVA_IDC);
        return buffer;
    }

    public void setTgaCosRunAssvaIdcNull(String tgaCosRunAssvaIdcNull) {
        writeString(Pos.TGA_COS_RUN_ASSVA_IDC_NULL, tgaCosRunAssvaIdcNull, Len.TGA_COS_RUN_ASSVA_IDC_NULL);
    }

    /**Original name: TGA-COS-RUN-ASSVA-IDC-NULL<br>*/
    public String getTgaCosRunAssvaIdcNull() {
        return readString(Pos.TGA_COS_RUN_ASSVA_IDC_NULL, Len.TGA_COS_RUN_ASSVA_IDC_NULL);
    }

    public String getTgaCosRunAssvaIdcNullFormatted() {
        return Functions.padBlanks(getTgaCosRunAssvaIdcNull(), Len.TGA_COS_RUN_ASSVA_IDC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_COS_RUN_ASSVA_IDC = 1;
        public static final int TGA_COS_RUN_ASSVA_IDC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_COS_RUN_ASSVA_IDC = 8;
        public static final int TGA_COS_RUN_ASSVA_IDC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
