package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PROV-ACQ<br>
 * Variable: B03-PROV-ACQ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03ProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03ProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PROV_ACQ;
    }

    public void setB03ProvAcq(AfDecimal b03ProvAcq) {
        writeDecimalAsPacked(Pos.B03_PROV_ACQ, b03ProvAcq.copy());
    }

    public void setB03ProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PROV_ACQ, Pos.B03_PROV_ACQ);
    }

    /**Original name: B03-PROV-ACQ<br>*/
    public AfDecimal getB03ProvAcq() {
        return readPackedAsDecimal(Pos.B03_PROV_ACQ, Len.Int.B03_PROV_ACQ, Len.Fract.B03_PROV_ACQ);
    }

    public byte[] getB03ProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PROV_ACQ, Pos.B03_PROV_ACQ);
        return buffer;
    }

    public void setB03ProvAcqNull(String b03ProvAcqNull) {
        writeString(Pos.B03_PROV_ACQ_NULL, b03ProvAcqNull, Len.B03_PROV_ACQ_NULL);
    }

    /**Original name: B03-PROV-ACQ-NULL<br>*/
    public String getB03ProvAcqNull() {
        return readString(Pos.B03_PROV_ACQ_NULL, Len.B03_PROV_ACQ_NULL);
    }

    public String getB03ProvAcqNullFormatted() {
        return Functions.padBlanks(getB03ProvAcqNull(), Len.B03_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PROV_ACQ = 1;
        public static final int B03_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PROV_ACQ = 8;
        public static final int B03_PROV_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PROV_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
