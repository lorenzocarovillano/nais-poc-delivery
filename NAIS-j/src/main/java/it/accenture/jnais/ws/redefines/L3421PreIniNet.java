package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-INI-NET<br>
 * Variable: L3421-PRE-INI-NET from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PreIniNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PreIniNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_INI_NET;
    }

    public void setL3421PreIniNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_INI_NET, Pos.L3421_PRE_INI_NET);
    }

    /**Original name: L3421-PRE-INI-NET<br>*/
    public AfDecimal getL3421PreIniNet() {
        return readPackedAsDecimal(Pos.L3421_PRE_INI_NET, Len.Int.L3421_PRE_INI_NET, Len.Fract.L3421_PRE_INI_NET);
    }

    public byte[] getL3421PreIniNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_INI_NET, Pos.L3421_PRE_INI_NET);
        return buffer;
    }

    /**Original name: L3421-PRE-INI-NET-NULL<br>*/
    public String getL3421PreIniNetNull() {
        return readString(Pos.L3421_PRE_INI_NET_NULL, Len.L3421_PRE_INI_NET_NULL);
    }

    public String getL3421PreIniNetNullFormatted() {
        return Functions.padBlanks(getL3421PreIniNetNull(), Len.L3421_PRE_INI_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_INI_NET = 1;
        public static final int L3421_PRE_INI_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_INI_NET = 8;
        public static final int L3421_PRE_INI_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_INI_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_INI_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
