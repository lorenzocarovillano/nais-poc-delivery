package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-VIS<br>
 * Variable: DFA-IMPST-VIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpstVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpstVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST_VIS;
    }

    public void setDfaImpstVis(AfDecimal dfaImpstVis) {
        writeDecimalAsPacked(Pos.DFA_IMPST_VIS, dfaImpstVis.copy());
    }

    public void setDfaImpstVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST_VIS, Pos.DFA_IMPST_VIS);
    }

    /**Original name: DFA-IMPST-VIS<br>*/
    public AfDecimal getDfaImpstVis() {
        return readPackedAsDecimal(Pos.DFA_IMPST_VIS, Len.Int.DFA_IMPST_VIS, Len.Fract.DFA_IMPST_VIS);
    }

    public byte[] getDfaImpstVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST_VIS, Pos.DFA_IMPST_VIS);
        return buffer;
    }

    public void setDfaImpstVisNull(String dfaImpstVisNull) {
        writeString(Pos.DFA_IMPST_VIS_NULL, dfaImpstVisNull, Len.DFA_IMPST_VIS_NULL);
    }

    /**Original name: DFA-IMPST-VIS-NULL<br>*/
    public String getDfaImpstVisNull() {
        return readString(Pos.DFA_IMPST_VIS_NULL, Len.DFA_IMPST_VIS_NULL);
    }

    public String getDfaImpstVisNullFormatted() {
        return Functions.padBlanks(getDfaImpstVisNull(), Len.DFA_IMPST_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_VIS = 1;
        public static final int DFA_IMPST_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST_VIS = 8;
        public static final int DFA_IMPST_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
