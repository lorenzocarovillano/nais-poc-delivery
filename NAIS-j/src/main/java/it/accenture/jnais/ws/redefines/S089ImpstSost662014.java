package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-SOST-662014<br>
 * Variable: S089-IMPST-SOST-662014 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstSost662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstSost662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_SOST662014;
    }

    public void setWlquImpstSost662014(AfDecimal wlquImpstSost662014) {
        writeDecimalAsPacked(Pos.S089_IMPST_SOST662014, wlquImpstSost662014.copy());
    }

    public void setWlquImpstSost662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_SOST662014, Pos.S089_IMPST_SOST662014);
    }

    /**Original name: WLQU-IMPST-SOST-662014<br>*/
    public AfDecimal getWlquImpstSost662014() {
        return readPackedAsDecimal(Pos.S089_IMPST_SOST662014, Len.Int.WLQU_IMPST_SOST662014, Len.Fract.WLQU_IMPST_SOST662014);
    }

    public byte[] getWlquImpstSost662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_SOST662014, Pos.S089_IMPST_SOST662014);
        return buffer;
    }

    public void initWlquImpstSost662014Spaces() {
        fill(Pos.S089_IMPST_SOST662014, Len.S089_IMPST_SOST662014, Types.SPACE_CHAR);
    }

    public void setWlquImpstSost662014Null(String wlquImpstSost662014Null) {
        writeString(Pos.S089_IMPST_SOST662014_NULL, wlquImpstSost662014Null, Len.WLQU_IMPST_SOST662014_NULL);
    }

    /**Original name: WLQU-IMPST-SOST-662014-NULL<br>*/
    public String getWlquImpstSost662014Null() {
        return readString(Pos.S089_IMPST_SOST662014_NULL, Len.WLQU_IMPST_SOST662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_SOST662014 = 1;
        public static final int S089_IMPST_SOST662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_SOST662014 = 8;
        public static final int WLQU_IMPST_SOST662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_SOST662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_SOST662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
