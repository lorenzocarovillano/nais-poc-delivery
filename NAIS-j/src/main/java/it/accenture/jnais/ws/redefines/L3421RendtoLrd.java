package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-RENDTO-LRD<br>
 * Variable: L3421-RENDTO-LRD from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421RendtoLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421RendtoLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_RENDTO_LRD;
    }

    public void setL3421RendtoLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_RENDTO_LRD, Pos.L3421_RENDTO_LRD);
    }

    /**Original name: L3421-RENDTO-LRD<br>*/
    public AfDecimal getL3421RendtoLrd() {
        return readPackedAsDecimal(Pos.L3421_RENDTO_LRD, Len.Int.L3421_RENDTO_LRD, Len.Fract.L3421_RENDTO_LRD);
    }

    public byte[] getL3421RendtoLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_RENDTO_LRD, Pos.L3421_RENDTO_LRD);
        return buffer;
    }

    /**Original name: L3421-RENDTO-LRD-NULL<br>*/
    public String getL3421RendtoLrdNull() {
        return readString(Pos.L3421_RENDTO_LRD_NULL, Len.L3421_RENDTO_LRD_NULL);
    }

    public String getL3421RendtoLrdNullFormatted() {
        return Functions.padBlanks(getL3421RendtoLrdNull(), Len.L3421_RENDTO_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_RENDTO_LRD = 1;
        public static final int L3421_RENDTO_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_RENDTO_LRD = 8;
        public static final int L3421_RENDTO_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_RENDTO_LRD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_RENDTO_LRD = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
