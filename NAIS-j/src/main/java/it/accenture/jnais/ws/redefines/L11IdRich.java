package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L11-ID-RICH<br>
 * Variable: L11-ID-RICH from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L11IdRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L11IdRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L11_ID_RICH;
    }

    public void setL11IdRich(int l11IdRich) {
        writeIntAsPacked(Pos.L11_ID_RICH, l11IdRich, Len.Int.L11_ID_RICH);
    }

    public void setL11IdRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L11_ID_RICH, Pos.L11_ID_RICH);
    }

    /**Original name: L11-ID-RICH<br>*/
    public int getL11IdRich() {
        return readPackedAsInt(Pos.L11_ID_RICH, Len.Int.L11_ID_RICH);
    }

    public byte[] getL11IdRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L11_ID_RICH, Pos.L11_ID_RICH);
        return buffer;
    }

    public void setL11IdRichNull(String l11IdRichNull) {
        writeString(Pos.L11_ID_RICH_NULL, l11IdRichNull, Len.L11_ID_RICH_NULL);
    }

    /**Original name: L11-ID-RICH-NULL<br>*/
    public String getL11IdRichNull() {
        return readString(Pos.L11_ID_RICH_NULL, Len.L11_ID_RICH_NULL);
    }

    public String getL11IdRichNullFormatted() {
        return Functions.padBlanks(getL11IdRichNull(), Len.L11_ID_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L11_ID_RICH = 1;
        public static final int L11_ID_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L11_ID_RICH = 5;
        public static final int L11_ID_RICH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L11_ID_RICH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
