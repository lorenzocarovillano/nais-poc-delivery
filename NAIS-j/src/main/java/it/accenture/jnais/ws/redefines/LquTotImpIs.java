package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-IS<br>
 * Variable: LQU-TOT-IMP-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_IS;
    }

    public void setLquTotImpIs(AfDecimal lquTotImpIs) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_IS, lquTotImpIs.copy());
    }

    public void setLquTotImpIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_IS, Pos.LQU_TOT_IMP_IS);
    }

    /**Original name: LQU-TOT-IMP-IS<br>*/
    public AfDecimal getLquTotImpIs() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_IS, Len.Int.LQU_TOT_IMP_IS, Len.Fract.LQU_TOT_IMP_IS);
    }

    public byte[] getLquTotImpIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_IS, Pos.LQU_TOT_IMP_IS);
        return buffer;
    }

    public void setLquTotImpIsNull(String lquTotImpIsNull) {
        writeString(Pos.LQU_TOT_IMP_IS_NULL, lquTotImpIsNull, Len.LQU_TOT_IMP_IS_NULL);
    }

    /**Original name: LQU-TOT-IMP-IS-NULL<br>*/
    public String getLquTotImpIsNull() {
        return readString(Pos.LQU_TOT_IMP_IS_NULL, Len.LQU_TOT_IMP_IS_NULL);
    }

    public String getLquTotImpIsNullFormatted() {
        return Functions.padBlanks(getLquTotImpIsNull(), Len.LQU_TOT_IMP_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_IS = 1;
        public static final int LQU_TOT_IMP_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_IS = 8;
        public static final int LQU_TOT_IMP_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
