package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-CAR-GEST<br>
 * Variable: WTDR-TOT-CAR-GEST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_CAR_GEST;
    }

    public void setWtdrTotCarGest(AfDecimal wtdrTotCarGest) {
        writeDecimalAsPacked(Pos.WTDR_TOT_CAR_GEST, wtdrTotCarGest.copy());
    }

    /**Original name: WTDR-TOT-CAR-GEST<br>*/
    public AfDecimal getWtdrTotCarGest() {
        return readPackedAsDecimal(Pos.WTDR_TOT_CAR_GEST, Len.Int.WTDR_TOT_CAR_GEST, Len.Fract.WTDR_TOT_CAR_GEST);
    }

    public void setWtdrTotCarGestNull(String wtdrTotCarGestNull) {
        writeString(Pos.WTDR_TOT_CAR_GEST_NULL, wtdrTotCarGestNull, Len.WTDR_TOT_CAR_GEST_NULL);
    }

    /**Original name: WTDR-TOT-CAR-GEST-NULL<br>*/
    public String getWtdrTotCarGestNull() {
        return readString(Pos.WTDR_TOT_CAR_GEST_NULL, Len.WTDR_TOT_CAR_GEST_NULL);
    }

    public String getWtdrTotCarGestNullFormatted() {
        return Functions.padBlanks(getWtdrTotCarGestNull(), Len.WTDR_TOT_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_GEST = 1;
        public static final int WTDR_TOT_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_GEST = 8;
        public static final int WTDR_TOT_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
