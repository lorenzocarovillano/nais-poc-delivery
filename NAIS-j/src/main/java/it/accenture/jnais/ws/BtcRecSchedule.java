package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IBtcRecSchedule;

/**Original name: BTC-REC-SCHEDULE<br>
 * Variable: BTC-REC-SCHEDULE from copybook IDBVBRS1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcRecSchedule extends SerializableParameter implements IBtcRecSchedule {

    //==== PROPERTIES ====
    //Original name: BRS-ID-BATCH
    private int idBatch = DefaultValues.BIN_INT_VAL;
    //Original name: BRS-ID-JOB
    private int idJob = DefaultValues.BIN_INT_VAL;
    //Original name: BRS-ID-RECORD
    private int idRecord = DefaultValues.BIN_INT_VAL;
    //Original name: BRS-TYPE-RECORD
    private String typeRecord = DefaultValues.stringVal(Len.TYPE_RECORD);
    //Original name: BRS-DATA-RECORD-LEN
    private short dataRecordLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BRS-DATA-RECORD
    private String dataRecord = DefaultValues.stringVal(Len.DATA_RECORD);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_REC_SCHEDULE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcRecScheduleBytes(buf);
    }

    public void setBtcRecScheduleBytes(byte[] buffer) {
        setBtcRecScheduleBytes(buffer, 1);
    }

    public byte[] getBtcRecScheduleBytes() {
        byte[] buffer = new byte[Len.BTC_REC_SCHEDULE];
        return getBtcRecScheduleBytes(buffer, 1);
    }

    public void setBtcRecScheduleBytes(byte[] buffer, int offset) {
        int position = offset;
        idBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idJob = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idRecord = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        typeRecord = MarshalByte.readString(buffer, position, Len.TYPE_RECORD);
        position += Len.TYPE_RECORD;
        setDataRecordVcharBytes(buffer, position);
    }

    public byte[] getBtcRecScheduleBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, idBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idJob);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idRecord);
        position += Types.INT_SIZE;
        MarshalByte.writeString(buffer, position, typeRecord, Len.TYPE_RECORD);
        position += Len.TYPE_RECORD;
        getDataRecordVcharBytes(buffer, position);
        return buffer;
    }

    @Override
    public void setIdBatch(int idBatch) {
        this.idBatch = idBatch;
    }

    @Override
    public int getIdBatch() {
        return this.idBatch;
    }

    @Override
    public void setIdJob(int idJob) {
        this.idJob = idJob;
    }

    @Override
    public int getIdJob() {
        return this.idJob;
    }

    @Override
    public void setIdRecord(int idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int getIdRecord() {
        return this.idRecord;
    }

    @Override
    public void setTypeRecord(String typeRecord) {
        this.typeRecord = Functions.subString(typeRecord, Len.TYPE_RECORD);
    }

    @Override
    public String getTypeRecord() {
        return this.typeRecord;
    }

    public String getTypeRecordFormatted() {
        return Functions.padBlanks(getTypeRecord(), Len.TYPE_RECORD);
    }

    public void setDataRecordVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DATA_RECORD_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DATA_RECORD_VCHAR);
        setDataRecordVcharBytes(buffer, 1);
    }

    public String getDataRecordVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDataRecordVcharBytes());
    }

    /**Original name: BRS-DATA-RECORD-VCHAR<br>*/
    public byte[] getDataRecordVcharBytes() {
        byte[] buffer = new byte[Len.DATA_RECORD_VCHAR];
        return getDataRecordVcharBytes(buffer, 1);
    }

    public void setDataRecordVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        dataRecordLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        dataRecord = MarshalByte.readString(buffer, position, Len.DATA_RECORD);
    }

    public byte[] getDataRecordVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, dataRecordLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, dataRecord, Len.DATA_RECORD);
        return buffer;
    }

    public void setDataRecordLen(short dataRecordLen) {
        this.dataRecordLen = dataRecordLen;
    }

    public short getDataRecordLen() {
        return this.dataRecordLen;
    }

    public void setDataRecord(String dataRecord) {
        this.dataRecord = Functions.subString(dataRecord, Len.DATA_RECORD);
    }

    public String getDataRecord() {
        return this.dataRecord;
    }

    @Override
    public String getDataRecordVchar() {
        throw new FieldNotMappedException("dataRecordVchar");
    }

    @Override
    public void setDataRecordVchar(String dataRecordVchar) {
        throw new FieldNotMappedException("dataRecordVchar");
    }

    @Override
    public String getTypeRecordObj() {
        return getTypeRecord();
    }

    @Override
    public void setTypeRecordObj(String typeRecordObj) {
        setTypeRecord(typeRecordObj);
    }

    @Override
    public byte[] serialize() {
        return getBtcRecScheduleBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_BATCH = 4;
        public static final int ID_JOB = 4;
        public static final int ID_RECORD = 4;
        public static final int TYPE_RECORD = 3;
        public static final int DATA_RECORD_LEN = 2;
        public static final int DATA_RECORD = 10000;
        public static final int DATA_RECORD_VCHAR = DATA_RECORD_LEN + DATA_RECORD;
        public static final int BTC_REC_SCHEDULE = ID_BATCH + ID_JOB + ID_RECORD + TYPE_RECORD + DATA_RECORD_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
