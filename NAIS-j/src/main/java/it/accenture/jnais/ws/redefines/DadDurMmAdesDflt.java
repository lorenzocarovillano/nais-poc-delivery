package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-DUR-MM-ADES-DFLT<br>
 * Variable: DAD-DUR-MM-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadDurMmAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadDurMmAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_DUR_MM_ADES_DFLT;
    }

    public void setDadDurMmAdesDflt(int dadDurMmAdesDflt) {
        writeIntAsPacked(Pos.DAD_DUR_MM_ADES_DFLT, dadDurMmAdesDflt, Len.Int.DAD_DUR_MM_ADES_DFLT);
    }

    public void setDadDurMmAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_DUR_MM_ADES_DFLT, Pos.DAD_DUR_MM_ADES_DFLT);
    }

    /**Original name: DAD-DUR-MM-ADES-DFLT<br>*/
    public int getDadDurMmAdesDflt() {
        return readPackedAsInt(Pos.DAD_DUR_MM_ADES_DFLT, Len.Int.DAD_DUR_MM_ADES_DFLT);
    }

    public byte[] getDadDurMmAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_DUR_MM_ADES_DFLT, Pos.DAD_DUR_MM_ADES_DFLT);
        return buffer;
    }

    public void setDadDurMmAdesDfltNull(String dadDurMmAdesDfltNull) {
        writeString(Pos.DAD_DUR_MM_ADES_DFLT_NULL, dadDurMmAdesDfltNull, Len.DAD_DUR_MM_ADES_DFLT_NULL);
    }

    /**Original name: DAD-DUR-MM-ADES-DFLT-NULL<br>*/
    public String getDadDurMmAdesDfltNull() {
        return readString(Pos.DAD_DUR_MM_ADES_DFLT_NULL, Len.DAD_DUR_MM_ADES_DFLT_NULL);
    }

    public String getDadDurMmAdesDfltNullFormatted() {
        return Functions.padBlanks(getDadDurMmAdesDfltNull(), Len.DAD_DUR_MM_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_DUR_MM_ADES_DFLT = 1;
        public static final int DAD_DUR_MM_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_DUR_MM_ADES_DFLT = 3;
        public static final int DAD_DUR_MM_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_DUR_MM_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
