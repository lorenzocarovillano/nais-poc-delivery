package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEP-PC-DEL-BNFICR<br>
 * Variable: WBEP-PC-DEL-BNFICR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbepPcDelBnficr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbepPcDelBnficr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEP_PC_DEL_BNFICR;
    }

    public void setWbepPcDelBnficr(AfDecimal wbepPcDelBnficr) {
        writeDecimalAsPacked(Pos.WBEP_PC_DEL_BNFICR, wbepPcDelBnficr.copy());
    }

    public void setWbepPcDelBnficrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEP_PC_DEL_BNFICR, Pos.WBEP_PC_DEL_BNFICR);
    }

    /**Original name: WBEP-PC-DEL-BNFICR<br>*/
    public AfDecimal getWbepPcDelBnficr() {
        return readPackedAsDecimal(Pos.WBEP_PC_DEL_BNFICR, Len.Int.WBEP_PC_DEL_BNFICR, Len.Fract.WBEP_PC_DEL_BNFICR);
    }

    public byte[] getWbepPcDelBnficrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEP_PC_DEL_BNFICR, Pos.WBEP_PC_DEL_BNFICR);
        return buffer;
    }

    public void initWbepPcDelBnficrSpaces() {
        fill(Pos.WBEP_PC_DEL_BNFICR, Len.WBEP_PC_DEL_BNFICR, Types.SPACE_CHAR);
    }

    public void setWbepPcDelBnficrNull(String wbepPcDelBnficrNull) {
        writeString(Pos.WBEP_PC_DEL_BNFICR_NULL, wbepPcDelBnficrNull, Len.WBEP_PC_DEL_BNFICR_NULL);
    }

    /**Original name: WBEP-PC-DEL-BNFICR-NULL<br>*/
    public String getWbepPcDelBnficrNull() {
        return readString(Pos.WBEP_PC_DEL_BNFICR_NULL, Len.WBEP_PC_DEL_BNFICR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEP_PC_DEL_BNFICR = 1;
        public static final int WBEP_PC_DEL_BNFICR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEP_PC_DEL_BNFICR = 4;
        public static final int WBEP_PC_DEL_BNFICR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEP_PC_DEL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEP_PC_DEL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
