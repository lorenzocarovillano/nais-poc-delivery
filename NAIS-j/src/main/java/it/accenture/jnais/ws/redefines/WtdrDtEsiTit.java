package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-DT-ESI-TIT<br>
 * Variable: WTDR-DT-ESI-TIT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrDtEsiTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrDtEsiTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_DT_ESI_TIT;
    }

    public void setWtdrDtEsiTit(int wtdrDtEsiTit) {
        writeIntAsPacked(Pos.WTDR_DT_ESI_TIT, wtdrDtEsiTit, Len.Int.WTDR_DT_ESI_TIT);
    }

    /**Original name: WTDR-DT-ESI-TIT<br>*/
    public int getWtdrDtEsiTit() {
        return readPackedAsInt(Pos.WTDR_DT_ESI_TIT, Len.Int.WTDR_DT_ESI_TIT);
    }

    public void setWtdrDtEsiTitNull(String wtdrDtEsiTitNull) {
        writeString(Pos.WTDR_DT_ESI_TIT_NULL, wtdrDtEsiTitNull, Len.WTDR_DT_ESI_TIT_NULL);
    }

    /**Original name: WTDR-DT-ESI-TIT-NULL<br>*/
    public String getWtdrDtEsiTitNull() {
        return readString(Pos.WTDR_DT_ESI_TIT_NULL, Len.WTDR_DT_ESI_TIT_NULL);
    }

    public String getWtdrDtEsiTitNullFormatted() {
        return Functions.padBlanks(getWtdrDtEsiTitNull(), Len.WTDR_DT_ESI_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_DT_ESI_TIT = 1;
        public static final int WTDR_DT_ESI_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_DT_ESI_TIT = 5;
        public static final int WTDR_DT_ESI_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_DT_ESI_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
