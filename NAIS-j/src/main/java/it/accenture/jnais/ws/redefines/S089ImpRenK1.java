package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-REN-K1<br>
 * Variable: S089-IMP-REN-K1 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpRenK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpRenK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_REN_K1;
    }

    public void setWlquImpRenK1(AfDecimal wlquImpRenK1) {
        writeDecimalAsPacked(Pos.S089_IMP_REN_K1, wlquImpRenK1.copy());
    }

    public void setWlquImpRenK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_REN_K1, Pos.S089_IMP_REN_K1);
    }

    /**Original name: WLQU-IMP-REN-K1<br>*/
    public AfDecimal getWlquImpRenK1() {
        return readPackedAsDecimal(Pos.S089_IMP_REN_K1, Len.Int.WLQU_IMP_REN_K1, Len.Fract.WLQU_IMP_REN_K1);
    }

    public byte[] getWlquImpRenK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_REN_K1, Pos.S089_IMP_REN_K1);
        return buffer;
    }

    public void initWlquImpRenK1Spaces() {
        fill(Pos.S089_IMP_REN_K1, Len.S089_IMP_REN_K1, Types.SPACE_CHAR);
    }

    public void setWlquImpRenK1Null(String wlquImpRenK1Null) {
        writeString(Pos.S089_IMP_REN_K1_NULL, wlquImpRenK1Null, Len.WLQU_IMP_REN_K1_NULL);
    }

    /**Original name: WLQU-IMP-REN-K1-NULL<br>*/
    public String getWlquImpRenK1Null() {
        return readString(Pos.S089_IMP_REN_K1_NULL, Len.WLQU_IMP_REN_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_REN_K1 = 1;
        public static final int S089_IMP_REN_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_REN_K1 = 8;
        public static final int WLQU_IMP_REN_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_REN_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_REN_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
