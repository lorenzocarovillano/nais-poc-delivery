package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-END2006-EFFLQ<br>
 * Variable: WDFL-MONT-END2006-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontEnd2006Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontEnd2006Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_END2006_EFFLQ;
    }

    public void setWdflMontEnd2006Efflq(AfDecimal wdflMontEnd2006Efflq) {
        writeDecimalAsPacked(Pos.WDFL_MONT_END2006_EFFLQ, wdflMontEnd2006Efflq.copy());
    }

    public void setWdflMontEnd2006EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_END2006_EFFLQ, Pos.WDFL_MONT_END2006_EFFLQ);
    }

    /**Original name: WDFL-MONT-END2006-EFFLQ<br>*/
    public AfDecimal getWdflMontEnd2006Efflq() {
        return readPackedAsDecimal(Pos.WDFL_MONT_END2006_EFFLQ, Len.Int.WDFL_MONT_END2006_EFFLQ, Len.Fract.WDFL_MONT_END2006_EFFLQ);
    }

    public byte[] getWdflMontEnd2006EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_END2006_EFFLQ, Pos.WDFL_MONT_END2006_EFFLQ);
        return buffer;
    }

    public void setWdflMontEnd2006EfflqNull(String wdflMontEnd2006EfflqNull) {
        writeString(Pos.WDFL_MONT_END2006_EFFLQ_NULL, wdflMontEnd2006EfflqNull, Len.WDFL_MONT_END2006_EFFLQ_NULL);
    }

    /**Original name: WDFL-MONT-END2006-EFFLQ-NULL<br>*/
    public String getWdflMontEnd2006EfflqNull() {
        return readString(Pos.WDFL_MONT_END2006_EFFLQ_NULL, Len.WDFL_MONT_END2006_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2006_EFFLQ = 1;
        public static final int WDFL_MONT_END2006_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2006_EFFLQ = 8;
        public static final int WDFL_MONT_END2006_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2006_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2006_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
