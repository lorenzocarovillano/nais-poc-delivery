package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WisoTabImpSost;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0090<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0090Data {

    //==== PROPERTIES ====
    public static final int DISO_TAB_ISO_MAXOCCURS = 10;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0090";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: DISO-ELE-ISO-MAX
    private short disoEleIsoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DISO-TAB-ISO
    private WisoTabImpSost[] disoTabIso = new WisoTabImpSost[DISO_TAB_ISO_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISO
    private short ixTabIso = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0090Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int disoTabIsoIdx = 1; disoTabIsoIdx <= DISO_TAB_ISO_MAXOCCURS; disoTabIsoIdx++) {
            disoTabIso[disoTabIsoIdx - 1] = new WisoTabImpSost();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDisoAreaIsoFormatted(String data) {
        byte[] buffer = new byte[Len.DISO_AREA_ISO];
        MarshalByte.writeString(buffer, 1, data, Len.DISO_AREA_ISO);
        setDisoAreaIsoBytes(buffer, 1);
    }

    public void setDisoAreaIsoBytes(byte[] buffer, int offset) {
        int position = offset;
        disoEleIsoMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DISO_TAB_ISO_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                disoTabIso[idx - 1].setWisoTabImpSostBytes(buffer, position);
                position += WisoTabImpSost.Len.WISO_TAB_IMP_SOST;
            }
            else {
                disoTabIso[idx - 1].initWisoTabImpSostSpaces();
                position += WisoTabImpSost.Len.WISO_TAB_IMP_SOST;
            }
        }
    }

    public void setDisoEleIsoMax(short disoEleIsoMax) {
        this.disoEleIsoMax = disoEleIsoMax;
    }

    public short getDisoEleIsoMax() {
        return this.disoEleIsoMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabIso(short ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public short getIxTabIso() {
        return this.ixTabIso;
    }

    public WisoTabImpSost getDisoTabIso(int idx) {
        return disoTabIso[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DISO_ELE_ISO_MAX = 2;
        public static final int DISO_AREA_ISO = DISO_ELE_ISO_MAX + Lvvs0090Data.DISO_TAB_ISO_MAXOCCURS * WisoTabImpSost.Len.WISO_TAB_IMP_SOST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
