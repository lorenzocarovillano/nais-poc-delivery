package it.accenture.jnais.ws;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.GarDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndRappRete;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSP010<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsp010Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WK-ID-OGG
    private String wkIdOgg = "000000000";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-RICH-EST
    private IndRappRete indRichEst = new IndRappRete();
    //Original name: RICH-EST-DB
    private GarDb richEstDb = new GarDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWkIdOgg(int wkIdOgg) {
        this.wkIdOgg = NumericDisplay.asString(wkIdOgg, Len.WK_ID_OGG);
    }

    public void setWkIdOggFormatted(String wkIdOgg) {
        this.wkIdOgg = Trunc.toUnsignedNumeric(wkIdOgg, Len.WK_ID_OGG);
    }

    public int getWkIdOgg() {
        return NumericDisplay.asInt(this.wkIdOgg);
    }

    public String getWkIdOggFormatted() {
        return this.wkIdOgg;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndRappRete getIndRichEst() {
        return indRichEst;
    }

    public GarDb getRichEstDb() {
        return richEstDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WK_ID_OGG = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
