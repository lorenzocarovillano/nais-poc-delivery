package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-252-ANTIC<br>
 * Variable: WDFA-IMPST-252-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpst252Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpst252Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST252_ANTIC;
    }

    public void setWdfaImpst252Antic(AfDecimal wdfaImpst252Antic) {
        writeDecimalAsPacked(Pos.WDFA_IMPST252_ANTIC, wdfaImpst252Antic.copy());
    }

    public void setWdfaImpst252AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST252_ANTIC, Pos.WDFA_IMPST252_ANTIC);
    }

    /**Original name: WDFA-IMPST-252-ANTIC<br>*/
    public AfDecimal getWdfaImpst252Antic() {
        return readPackedAsDecimal(Pos.WDFA_IMPST252_ANTIC, Len.Int.WDFA_IMPST252_ANTIC, Len.Fract.WDFA_IMPST252_ANTIC);
    }

    public byte[] getWdfaImpst252AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST252_ANTIC, Pos.WDFA_IMPST252_ANTIC);
        return buffer;
    }

    public void setWdfaImpst252AnticNull(String wdfaImpst252AnticNull) {
        writeString(Pos.WDFA_IMPST252_ANTIC_NULL, wdfaImpst252AnticNull, Len.WDFA_IMPST252_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPST-252-ANTIC-NULL<br>*/
    public String getWdfaImpst252AnticNull() {
        return readString(Pos.WDFA_IMPST252_ANTIC_NULL, Len.WDFA_IMPST252_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST252_ANTIC = 1;
        public static final int WDFA_IMPST252_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST252_ANTIC = 8;
        public static final int WDFA_IMPST252_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST252_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST252_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
