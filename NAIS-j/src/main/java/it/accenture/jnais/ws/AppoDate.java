package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: APPO-DATE<br>
 * Variable: APPO-DATE from program LVVS1280<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AppoDate {

    //==== PROPERTIES ====
    //Original name: AA-APPO
    private String aaAppo = DefaultValues.stringVal(Len.AA_APPO);
    //Original name: MM-APPO
    private String mmAppo = DefaultValues.stringVal(Len.MM_APPO);
    //Original name: GG-APPO
    private String ggAppo = DefaultValues.stringVal(Len.GG_APPO);

    //==== METHODS ====
    public void setAppoDateFormatted(String data) {
        byte[] buffer = new byte[Len.APPO_DATE];
        MarshalByte.writeString(buffer, 1, data, Len.APPO_DATE);
        setAppoDateBytes(buffer, 1);
    }

    public byte[] getAppoDateBytes() {
        byte[] buffer = new byte[Len.APPO_DATE];
        return getAppoDateBytes(buffer, 1);
    }

    public void setAppoDateBytes(byte[] buffer, int offset) {
        int position = offset;
        aaAppo = MarshalByte.readFixedString(buffer, position, Len.AA_APPO);
        position += Len.AA_APPO;
        mmAppo = MarshalByte.readFixedString(buffer, position, Len.MM_APPO);
        position += Len.MM_APPO;
        ggAppo = MarshalByte.readFixedString(buffer, position, Len.GG_APPO);
    }

    public byte[] getAppoDateBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaAppo, Len.AA_APPO);
        position += Len.AA_APPO;
        MarshalByte.writeString(buffer, position, mmAppo, Len.MM_APPO);
        position += Len.MM_APPO;
        MarshalByte.writeString(buffer, position, ggAppo, Len.GG_APPO);
        return buffer;
    }

    public void setAaAppo(short aaAppo) {
        this.aaAppo = NumericDisplay.asString(aaAppo, Len.AA_APPO);
    }

    public short getAaAppo() {
        return NumericDisplay.asShort(this.aaAppo);
    }

    public void setMmAppo(short mmAppo) {
        this.mmAppo = NumericDisplay.asString(mmAppo, Len.MM_APPO);
    }

    public short getMmAppo() {
        return NumericDisplay.asShort(this.mmAppo);
    }

    public void setGgAppo(short ggAppo) {
        this.ggAppo = NumericDisplay.asString(ggAppo, Len.GG_APPO);
    }

    public short getGgAppo() {
        return NumericDisplay.asShort(this.ggAppo);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA_APPO = 4;
        public static final int MM_APPO = 2;
        public static final int GG_APPO = 2;
        public static final int APPO_DATE = AA_APPO + MM_APPO + GG_APPO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
