package it.accenture.jnais.ws;

/**Original name: INDICI<br>
 * Variable: INDICI from program LCCS0450<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IndiciLccs0450 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-GRZ
    private int tabGrz = 0;
    //Original name: IX-TAB-TGA
    private int tabTga = 0;
    //Original name: IX-TAB-MAT
    private int tabMat = 0;
    //Original name: IX-TAB-TOT
    private int tabTot = 0;
    //Original name: IX-IND-TAR
    private int indTar = 0;
    //Original name: IX-TAB-DTC
    private int tabDtc = 0;
    //Original name: IX-TAB-ADE
    private int tabAde = 0;
    //Original name: IX-TAB-GAR
    private int tabGar = 0;
    //Original name: IX-TAB-L19
    private int tabL19 = 0;
    //Original name: IX-TAB-WL19
    private int tabWl19 = 0;

    //==== METHODS ====
    public void setTabGrz(int tabGrz) {
        this.tabGrz = tabGrz;
    }

    public int getTabGrz() {
        return this.tabGrz;
    }

    public void setTabTga(int tabTga) {
        this.tabTga = tabTga;
    }

    public int getTabTga() {
        return this.tabTga;
    }

    public void setTabMat(int tabMat) {
        this.tabMat = tabMat;
    }

    public int getTabMat() {
        return this.tabMat;
    }

    public void setTabTot(int tabTot) {
        this.tabTot = tabTot;
    }

    public int getTabTot() {
        return this.tabTot;
    }

    public void setIndTar(int indTar) {
        this.indTar = indTar;
    }

    public int getIndTar() {
        return this.indTar;
    }

    public void setTabDtc(int tabDtc) {
        this.tabDtc = tabDtc;
    }

    public int getTabDtc() {
        return this.tabDtc;
    }

    public void setTabAde(int tabAde) {
        this.tabAde = tabAde;
    }

    public int getTabAde() {
        return this.tabAde;
    }

    public void setTabGar(int tabGar) {
        this.tabGar = tabGar;
    }

    public int getTabGar() {
        return this.tabGar;
    }

    public void setTabL19(int tabL19) {
        this.tabL19 = tabL19;
    }

    public int getTabL19() {
        return this.tabL19;
    }

    public void setTabWl19(int tabWl19) {
        this.tabWl19 = tabWl19;
    }

    public int getTabWl19() {
        return this.tabWl19;
    }
}
