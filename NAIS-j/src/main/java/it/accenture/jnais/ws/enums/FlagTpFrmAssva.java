package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-TP-FRM-ASSVA<br>
 * Variable: FLAG-TP-FRM-ASSVA from program LOAS9000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTpFrmAssva {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_TP_FRM_ASSVA);
    public static final String INDIVIDUALE = "IN";
    public static final String COLLETTIVA = "CO";

    //==== METHODS ====
    public void setFlagTpFrmAssva(String flagTpFrmAssva) {
        this.value = Functions.subString(flagTpFrmAssva, Len.FLAG_TP_FRM_ASSVA);
    }

    public String getFlagTpFrmAssva() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_TP_FRM_ASSVA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
