package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-AZ<br>
 * Variable: WADE-IMP-AZ from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_AZ;
    }

    public void setWadeImpAz(AfDecimal wadeImpAz) {
        writeDecimalAsPacked(Pos.WADE_IMP_AZ, wadeImpAz.copy());
    }

    public void setWadeImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_AZ, Pos.WADE_IMP_AZ);
    }

    /**Original name: WADE-IMP-AZ<br>*/
    public AfDecimal getWadeImpAz() {
        return readPackedAsDecimal(Pos.WADE_IMP_AZ, Len.Int.WADE_IMP_AZ, Len.Fract.WADE_IMP_AZ);
    }

    public byte[] getWadeImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_AZ, Pos.WADE_IMP_AZ);
        return buffer;
    }

    public void initWadeImpAzSpaces() {
        fill(Pos.WADE_IMP_AZ, Len.WADE_IMP_AZ, Types.SPACE_CHAR);
    }

    public void setWadeImpAzNull(String wadeImpAzNull) {
        writeString(Pos.WADE_IMP_AZ_NULL, wadeImpAzNull, Len.WADE_IMP_AZ_NULL);
    }

    /**Original name: WADE-IMP-AZ-NULL<br>*/
    public String getWadeImpAzNull() {
        return readString(Pos.WADE_IMP_AZ_NULL, Len.WADE_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_AZ = 1;
        public static final int WADE_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_AZ = 8;
        public static final int WADE_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
