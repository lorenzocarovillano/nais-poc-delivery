package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PROV-ACQ-1AA<br>
 * Variable: WTDR-TOT-PROV-ACQ-1AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PROV_ACQ1AA;
    }

    public void setWtdrTotProvAcq1aa(AfDecimal wtdrTotProvAcq1aa) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PROV_ACQ1AA, wtdrTotProvAcq1aa.copy());
    }

    /**Original name: WTDR-TOT-PROV-ACQ-1AA<br>*/
    public AfDecimal getWtdrTotProvAcq1aa() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PROV_ACQ1AA, Len.Int.WTDR_TOT_PROV_ACQ1AA, Len.Fract.WTDR_TOT_PROV_ACQ1AA);
    }

    public void setWtdrTotProvAcq1aaNull(String wtdrTotProvAcq1aaNull) {
        writeString(Pos.WTDR_TOT_PROV_ACQ1AA_NULL, wtdrTotProvAcq1aaNull, Len.WTDR_TOT_PROV_ACQ1AA_NULL);
    }

    /**Original name: WTDR-TOT-PROV-ACQ-1AA-NULL<br>*/
    public String getWtdrTotProvAcq1aaNull() {
        return readString(Pos.WTDR_TOT_PROV_ACQ1AA_NULL, Len.WTDR_TOT_PROV_ACQ1AA_NULL);
    }

    public String getWtdrTotProvAcq1aaNullFormatted() {
        return Functions.padBlanks(getWtdrTotProvAcq1aaNull(), Len.WTDR_TOT_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_ACQ1AA = 1;
        public static final int WTDR_TOT_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_ACQ1AA = 8;
        public static final int WTDR_TOT_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
