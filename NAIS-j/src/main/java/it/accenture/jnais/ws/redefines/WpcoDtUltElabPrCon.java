package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-PR-CON<br>
 * Variable: WPCO-DT-ULT-ELAB-PR-CON from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabPrCon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabPrCon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_PR_CON;
    }

    public void setWpcoDtUltElabPrCon(int wpcoDtUltElabPrCon) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_PR_CON, wpcoDtUltElabPrCon, Len.Int.WPCO_DT_ULT_ELAB_PR_CON);
    }

    public void setDpcoDtUltElabPrConFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PR_CON, Pos.WPCO_DT_ULT_ELAB_PR_CON);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PR-CON<br>*/
    public int getWpcoDtUltElabPrCon() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_PR_CON, Len.Int.WPCO_DT_ULT_ELAB_PR_CON);
    }

    public byte[] getWpcoDtUltElabPrConAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PR_CON, Pos.WPCO_DT_ULT_ELAB_PR_CON);
        return buffer;
    }

    public void setWpcoDtUltElabPrConNull(String wpcoDtUltElabPrConNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_PR_CON_NULL, wpcoDtUltElabPrConNull, Len.WPCO_DT_ULT_ELAB_PR_CON_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PR-CON-NULL<br>*/
    public String getWpcoDtUltElabPrConNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_PR_CON_NULL, Len.WPCO_DT_ULT_ELAB_PR_CON_NULL);
    }

    public String getWpcoDtUltElabPrConNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabPrConNull(), Len.WPCO_DT_ULT_ELAB_PR_CON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PR_CON = 1;
        public static final int WPCO_DT_ULT_ELAB_PR_CON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PR_CON = 5;
        public static final int WPCO_DT_ULT_ELAB_PR_CON_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_PR_CON = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
