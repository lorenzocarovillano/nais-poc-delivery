package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.Lccc0023IdMoviAnn;
import it.accenture.jnais.ws.redefines.Lccc0023IdOgg;
import it.accenture.jnais.ws.redefines.Lccc0023IdRich;
import it.accenture.jnais.ws.redefines.Lccc0023TpMovi;

/**Original name: LCCC0023-TAB-MOV-FUTURI<br>
 * Variables: LCCC0023-TAB-MOV-FUTURI from copybook LCCC0023<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0023TabMovFuturi {

    //==== PROPERTIES ====
    //Original name: LCCC0023-ID-MOVI
    private int lccc0023IdMovi = DefaultValues.INT_VAL;
    //Original name: LCCC0023-ID-OGG
    private Lccc0023IdOgg lccc0023IdOgg = new Lccc0023IdOgg();
    //Original name: LCCC0023-IB-OGG
    private String lccc0023IbOgg = DefaultValues.stringVal(Len.LCCC0023_IB_OGG);
    //Original name: LCCC0023-IB-MOVI
    private String lccc0023IbMovi = DefaultValues.stringVal(Len.LCCC0023_IB_MOVI);
    //Original name: LCCC0023-TP-OGG
    private String lccc0023TpOgg = DefaultValues.stringVal(Len.LCCC0023_TP_OGG);
    //Original name: LCCC0023-ID-RICH
    private Lccc0023IdRich lccc0023IdRich = new Lccc0023IdRich();
    //Original name: LCCC0023-TP-MOVI
    private Lccc0023TpMovi lccc0023TpMovi = new Lccc0023TpMovi();
    //Original name: LCCC0023-DT-EFF
    private int lccc0023DtEff = DefaultValues.INT_VAL;
    //Original name: LCCC0023-ID-MOVI-ANN
    private Lccc0023IdMoviAnn lccc0023IdMoviAnn = new Lccc0023IdMoviAnn();
    //Original name: LCCC0023-ID-MOVI-PRENOTAZ
    private int lccc0023IdMoviPrenotaz = DefaultValues.INT_VAL;
    //Original name: LCCC0023-TP-GRAVITA-MOVIMENTO
    private String lccc0023TpGravitaMovimento = DefaultValues.stringVal(Len.LCCC0023_TP_GRAVITA_MOVIMENTO);
    //Original name: LCCC0023-DS-TS-CPTZ
    private long lccc0023DsTsCptz = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setTabMovFuturiBytes(byte[] buffer, int offset) {
        int position = offset;
        lccc0023IdMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0023_ID_MOVI, 0);
        position += Len.LCCC0023_ID_MOVI;
        lccc0023IdOgg.setLccc0023IdOggFromBuffer(buffer, position);
        position += Lccc0023IdOgg.Len.LCCC0023_ID_OGG;
        lccc0023IbOgg = MarshalByte.readString(buffer, position, Len.LCCC0023_IB_OGG);
        position += Len.LCCC0023_IB_OGG;
        lccc0023IbMovi = MarshalByte.readString(buffer, position, Len.LCCC0023_IB_MOVI);
        position += Len.LCCC0023_IB_MOVI;
        lccc0023TpOgg = MarshalByte.readString(buffer, position, Len.LCCC0023_TP_OGG);
        position += Len.LCCC0023_TP_OGG;
        lccc0023IdRich.setLccc0023IdRichFromBuffer(buffer, position);
        position += Lccc0023IdRich.Len.LCCC0023_ID_RICH;
        lccc0023TpMovi.setLccc0023TpMoviFromBuffer(buffer, position);
        position += Lccc0023TpMovi.Len.LCCC0023_TP_MOVI;
        lccc0023DtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0023_DT_EFF, 0);
        position += Len.LCCC0023_DT_EFF;
        lccc0023IdMoviAnn.setLccc0023IdMoviAnnFromBuffer(buffer, position);
        position += Lccc0023IdMoviAnn.Len.LCCC0023_ID_MOVI_ANN;
        lccc0023IdMoviPrenotaz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LCCC0023_ID_MOVI_PRENOTAZ, 0);
        position += Len.LCCC0023_ID_MOVI_PRENOTAZ;
        lccc0023TpGravitaMovimento = MarshalByte.readString(buffer, position, Len.LCCC0023_TP_GRAVITA_MOVIMENTO);
        position += Len.LCCC0023_TP_GRAVITA_MOVIMENTO;
        lccc0023DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.LCCC0023_DS_TS_CPTZ, 0);
    }

    public byte[] getTabMovFuturiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0023IdMovi, Len.Int.LCCC0023_ID_MOVI, 0);
        position += Len.LCCC0023_ID_MOVI;
        lccc0023IdOgg.getLccc0023IdOggAsBuffer(buffer, position);
        position += Lccc0023IdOgg.Len.LCCC0023_ID_OGG;
        MarshalByte.writeString(buffer, position, lccc0023IbOgg, Len.LCCC0023_IB_OGG);
        position += Len.LCCC0023_IB_OGG;
        MarshalByte.writeString(buffer, position, lccc0023IbMovi, Len.LCCC0023_IB_MOVI);
        position += Len.LCCC0023_IB_MOVI;
        MarshalByte.writeString(buffer, position, lccc0023TpOgg, Len.LCCC0023_TP_OGG);
        position += Len.LCCC0023_TP_OGG;
        lccc0023IdRich.getLccc0023IdRichAsBuffer(buffer, position);
        position += Lccc0023IdRich.Len.LCCC0023_ID_RICH;
        lccc0023TpMovi.getLccc0023TpMoviAsBuffer(buffer, position);
        position += Lccc0023TpMovi.Len.LCCC0023_TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0023DtEff, Len.Int.LCCC0023_DT_EFF, 0);
        position += Len.LCCC0023_DT_EFF;
        lccc0023IdMoviAnn.getLccc0023IdMoviAnnAsBuffer(buffer, position);
        position += Lccc0023IdMoviAnn.Len.LCCC0023_ID_MOVI_ANN;
        MarshalByte.writeIntAsPacked(buffer, position, lccc0023IdMoviPrenotaz, Len.Int.LCCC0023_ID_MOVI_PRENOTAZ, 0);
        position += Len.LCCC0023_ID_MOVI_PRENOTAZ;
        MarshalByte.writeString(buffer, position, lccc0023TpGravitaMovimento, Len.LCCC0023_TP_GRAVITA_MOVIMENTO);
        position += Len.LCCC0023_TP_GRAVITA_MOVIMENTO;
        MarshalByte.writeLongAsPacked(buffer, position, lccc0023DsTsCptz, Len.Int.LCCC0023_DS_TS_CPTZ, 0);
        return buffer;
    }

    public void initTabMovFuturiSpaces() {
        lccc0023IdMovi = Types.INVALID_INT_VAL;
        lccc0023IdOgg.initLccc0023IdOggSpaces();
        lccc0023IbOgg = "";
        lccc0023IbMovi = "";
        lccc0023TpOgg = "";
        lccc0023IdRich.initLccc0023IdRichSpaces();
        lccc0023TpMovi.initLccc0023TpMoviSpaces();
        lccc0023DtEff = Types.INVALID_INT_VAL;
        lccc0023IdMoviAnn.initLccc0023IdMoviAnnSpaces();
        lccc0023IdMoviPrenotaz = Types.INVALID_INT_VAL;
        lccc0023TpGravitaMovimento = "";
        lccc0023DsTsCptz = Types.INVALID_LONG_VAL;
    }

    public void setLccc0023IdMovi(int lccc0023IdMovi) {
        this.lccc0023IdMovi = lccc0023IdMovi;
    }

    public int getLccc0023IdMovi() {
        return this.lccc0023IdMovi;
    }

    public void setLccc0023IbOgg(String lccc0023IbOgg) {
        this.lccc0023IbOgg = Functions.subString(lccc0023IbOgg, Len.LCCC0023_IB_OGG);
    }

    public String getLccc0023IbOgg() {
        return this.lccc0023IbOgg;
    }

    public void setLccc0023IbMovi(String lccc0023IbMovi) {
        this.lccc0023IbMovi = Functions.subString(lccc0023IbMovi, Len.LCCC0023_IB_MOVI);
    }

    public String getLccc0023IbMovi() {
        return this.lccc0023IbMovi;
    }

    public void setLccc0023TpOgg(String lccc0023TpOgg) {
        this.lccc0023TpOgg = Functions.subString(lccc0023TpOgg, Len.LCCC0023_TP_OGG);
    }

    public String getLccc0023TpOgg() {
        return this.lccc0023TpOgg;
    }

    public void setLccc0023DtEff(int lccc0023DtEff) {
        this.lccc0023DtEff = lccc0023DtEff;
    }

    public int getLccc0023DtEff() {
        return this.lccc0023DtEff;
    }

    public void setLccc0023IdMoviPrenotaz(int lccc0023IdMoviPrenotaz) {
        this.lccc0023IdMoviPrenotaz = lccc0023IdMoviPrenotaz;
    }

    public int getLccc0023IdMoviPrenotaz() {
        return this.lccc0023IdMoviPrenotaz;
    }

    public void setLccc0023TpGravitaMovimento(String lccc0023TpGravitaMovimento) {
        this.lccc0023TpGravitaMovimento = Functions.subString(lccc0023TpGravitaMovimento, Len.LCCC0023_TP_GRAVITA_MOVIMENTO);
    }

    public String getLccc0023TpGravitaMovimento() {
        return this.lccc0023TpGravitaMovimento;
    }

    public void setLccc0023DsTsCptz(long lccc0023DsTsCptz) {
        this.lccc0023DsTsCptz = lccc0023DsTsCptz;
    }

    public long getLccc0023DsTsCptz() {
        return this.lccc0023DsTsCptz;
    }

    public Lccc0023IdMoviAnn getLccc0023IdMoviAnn() {
        return lccc0023IdMoviAnn;
    }

    public Lccc0023IdOgg getLccc0023IdOgg() {
        return lccc0023IdOgg;
    }

    public Lccc0023IdRich getLccc0023IdRich() {
        return lccc0023IdRich;
    }

    public Lccc0023TpMovi getLccc0023TpMovi() {
        return lccc0023TpMovi;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_MOVI = 5;
        public static final int LCCC0023_IB_OGG = 40;
        public static final int LCCC0023_IB_MOVI = 40;
        public static final int LCCC0023_TP_OGG = 2;
        public static final int LCCC0023_DT_EFF = 5;
        public static final int LCCC0023_ID_MOVI_PRENOTAZ = 5;
        public static final int LCCC0023_TP_GRAVITA_MOVIMENTO = 2;
        public static final int LCCC0023_DS_TS_CPTZ = 10;
        public static final int TAB_MOV_FUTURI = LCCC0023_ID_MOVI + Lccc0023IdOgg.Len.LCCC0023_ID_OGG + LCCC0023_IB_OGG + LCCC0023_IB_MOVI + LCCC0023_TP_OGG + Lccc0023IdRich.Len.LCCC0023_ID_RICH + Lccc0023TpMovi.Len.LCCC0023_TP_MOVI + LCCC0023_DT_EFF + Lccc0023IdMoviAnn.Len.LCCC0023_ID_MOVI_ANN + LCCC0023_ID_MOVI_PRENOTAZ + LCCC0023_TP_GRAVITA_MOVIMENTO + LCCC0023_DS_TS_CPTZ;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0023_ID_MOVI = 9;
            public static final int LCCC0023_DT_EFF = 8;
            public static final int LCCC0023_ID_MOVI_PRENOTAZ = 9;
            public static final int LCCC0023_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
