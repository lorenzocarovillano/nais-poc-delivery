package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DT-RST<br>
 * Variable: WS-DT-RST from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDtRst {

    //==== PROPERTIES ====
    //Original name: WS-DT-RST-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WS-DT-RST-MM
    private String mm = "12";
    //Original name: WS-DT-RST-GG
    private String gg = "31";

    //==== METHODS ====
    public byte[] getWsDtRstBytes() {
        byte[] buffer = new byte[Len.WS_DT_RST];
        return getWsDtRstBytes(buffer, 1);
    }

    public byte[] getWsDtRstBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa, Len.AA);
        position += Len.AA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setAa(short aa) {
        this.aa = NumericDisplay.asString(aa, Len.AA);
    }

    public void setAaFormatted(String aa) {
        this.aa = Trunc.toUnsignedNumeric(aa, Len.AA);
    }

    public short getAa() {
        return NumericDisplay.asShort(this.aa);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WS_DT_RST = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
