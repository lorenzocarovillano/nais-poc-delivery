package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-IL-COLL<br>
 * Variable: PCO-DT-ULT-EC-IL-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcIlColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcIlColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_IL_COLL;
    }

    public void setPcoDtUltEcIlColl(int pcoDtUltEcIlColl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_IL_COLL, pcoDtUltEcIlColl, Len.Int.PCO_DT_ULT_EC_IL_COLL);
    }

    public void setPcoDtUltEcIlCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_IL_COLL, Pos.PCO_DT_ULT_EC_IL_COLL);
    }

    /**Original name: PCO-DT-ULT-EC-IL-COLL<br>*/
    public int getPcoDtUltEcIlColl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_IL_COLL, Len.Int.PCO_DT_ULT_EC_IL_COLL);
    }

    public byte[] getPcoDtUltEcIlCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_IL_COLL, Pos.PCO_DT_ULT_EC_IL_COLL);
        return buffer;
    }

    public void initPcoDtUltEcIlCollHighValues() {
        fill(Pos.PCO_DT_ULT_EC_IL_COLL, Len.PCO_DT_ULT_EC_IL_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcIlCollNull(String pcoDtUltEcIlCollNull) {
        writeString(Pos.PCO_DT_ULT_EC_IL_COLL_NULL, pcoDtUltEcIlCollNull, Len.PCO_DT_ULT_EC_IL_COLL_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-IL-COLL-NULL<br>*/
    public String getPcoDtUltEcIlCollNull() {
        return readString(Pos.PCO_DT_ULT_EC_IL_COLL_NULL, Len.PCO_DT_ULT_EC_IL_COLL_NULL);
    }

    public String getPcoDtUltEcIlCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcIlCollNull(), Len.PCO_DT_ULT_EC_IL_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_IL_COLL = 1;
        public static final int PCO_DT_ULT_EC_IL_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_IL_COLL = 5;
        public static final int PCO_DT_ULT_EC_IL_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_IL_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
