package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MONT-END2000-DFZ<br>
 * Variable: DFL-MONT-END2000-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMontEnd2000Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMontEnd2000Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MONT_END2000_DFZ;
    }

    public void setDflMontEnd2000Dfz(AfDecimal dflMontEnd2000Dfz) {
        writeDecimalAsPacked(Pos.DFL_MONT_END2000_DFZ, dflMontEnd2000Dfz.copy());
    }

    public void setDflMontEnd2000DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MONT_END2000_DFZ, Pos.DFL_MONT_END2000_DFZ);
    }

    /**Original name: DFL-MONT-END2000-DFZ<br>*/
    public AfDecimal getDflMontEnd2000Dfz() {
        return readPackedAsDecimal(Pos.DFL_MONT_END2000_DFZ, Len.Int.DFL_MONT_END2000_DFZ, Len.Fract.DFL_MONT_END2000_DFZ);
    }

    public byte[] getDflMontEnd2000DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MONT_END2000_DFZ, Pos.DFL_MONT_END2000_DFZ);
        return buffer;
    }

    public void setDflMontEnd2000DfzNull(String dflMontEnd2000DfzNull) {
        writeString(Pos.DFL_MONT_END2000_DFZ_NULL, dflMontEnd2000DfzNull, Len.DFL_MONT_END2000_DFZ_NULL);
    }

    /**Original name: DFL-MONT-END2000-DFZ-NULL<br>*/
    public String getDflMontEnd2000DfzNull() {
        return readString(Pos.DFL_MONT_END2000_DFZ_NULL, Len.DFL_MONT_END2000_DFZ_NULL);
    }

    public String getDflMontEnd2000DfzNullFormatted() {
        return Functions.padBlanks(getDflMontEnd2000DfzNull(), Len.DFL_MONT_END2000_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MONT_END2000_DFZ = 1;
        public static final int DFL_MONT_END2000_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MONT_END2000_DFZ = 8;
        public static final int DFL_MONT_END2000_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MONT_END2000_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_MONT_END2000_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
