package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PROV-1AA-ACQ<br>
 * Variable: WTGA-PROV-1AA-ACQ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaProv1aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaProv1aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PROV1AA_ACQ;
    }

    public void setWtgaProv1aaAcq(AfDecimal wtgaProv1aaAcq) {
        writeDecimalAsPacked(Pos.WTGA_PROV1AA_ACQ, wtgaProv1aaAcq.copy());
    }

    public void setWtgaProv1aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PROV1AA_ACQ, Pos.WTGA_PROV1AA_ACQ);
    }

    /**Original name: WTGA-PROV-1AA-ACQ<br>*/
    public AfDecimal getWtgaProv1aaAcq() {
        return readPackedAsDecimal(Pos.WTGA_PROV1AA_ACQ, Len.Int.WTGA_PROV1AA_ACQ, Len.Fract.WTGA_PROV1AA_ACQ);
    }

    public byte[] getWtgaProv1aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PROV1AA_ACQ, Pos.WTGA_PROV1AA_ACQ);
        return buffer;
    }

    public void initWtgaProv1aaAcqSpaces() {
        fill(Pos.WTGA_PROV1AA_ACQ, Len.WTGA_PROV1AA_ACQ, Types.SPACE_CHAR);
    }

    public void setWtgaProv1aaAcqNull(String wtgaProv1aaAcqNull) {
        writeString(Pos.WTGA_PROV1AA_ACQ_NULL, wtgaProv1aaAcqNull, Len.WTGA_PROV1AA_ACQ_NULL);
    }

    /**Original name: WTGA-PROV-1AA-ACQ-NULL<br>*/
    public String getWtgaProv1aaAcqNull() {
        return readString(Pos.WTGA_PROV1AA_ACQ_NULL, Len.WTGA_PROV1AA_ACQ_NULL);
    }

    public String getWtgaProv1aaAcqNullFormatted() {
        return Functions.padBlanks(getWtgaProv1aaAcqNull(), Len.WTGA_PROV1AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PROV1AA_ACQ = 1;
        public static final int WTGA_PROV1AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PROV1AA_ACQ = 8;
        public static final int WTGA_PROV1AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PROV1AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
