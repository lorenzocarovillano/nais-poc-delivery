package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB05-PROG-SCHEDA-VALOR<br>
 * Variable: WB05-PROG-SCHEDA-VALOR from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb05ProgSchedaValor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb05ProgSchedaValor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB05_PROG_SCHEDA_VALOR;
    }

    public void setWb05ProgSchedaValor(int wb05ProgSchedaValor) {
        writeIntAsPacked(Pos.WB05_PROG_SCHEDA_VALOR, wb05ProgSchedaValor, Len.Int.WB05_PROG_SCHEDA_VALOR);
    }

    public void setWb05ProgSchedaValorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB05_PROG_SCHEDA_VALOR, Pos.WB05_PROG_SCHEDA_VALOR);
    }

    /**Original name: WB05-PROG-SCHEDA-VALOR<br>*/
    public int getWb05ProgSchedaValor() {
        return readPackedAsInt(Pos.WB05_PROG_SCHEDA_VALOR, Len.Int.WB05_PROG_SCHEDA_VALOR);
    }

    public byte[] getWb05ProgSchedaValorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB05_PROG_SCHEDA_VALOR, Pos.WB05_PROG_SCHEDA_VALOR);
        return buffer;
    }

    public void setWb05ProgSchedaValorNull(String wb05ProgSchedaValorNull) {
        writeString(Pos.WB05_PROG_SCHEDA_VALOR_NULL, wb05ProgSchedaValorNull, Len.WB05_PROG_SCHEDA_VALOR_NULL);
    }

    /**Original name: WB05-PROG-SCHEDA-VALOR-NULL<br>*/
    public String getWb05ProgSchedaValorNull() {
        return readString(Pos.WB05_PROG_SCHEDA_VALOR_NULL, Len.WB05_PROG_SCHEDA_VALOR_NULL);
    }

    public String getWb05ProgSchedaValorNullFormatted() {
        return Functions.padBlanks(getWb05ProgSchedaValorNull(), Len.WB05_PROG_SCHEDA_VALOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB05_PROG_SCHEDA_VALOR = 1;
        public static final int WB05_PROG_SCHEDA_VALOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_PROG_SCHEDA_VALOR = 5;
        public static final int WB05_PROG_SCHEDA_VALOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB05_PROG_SCHEDA_VALOR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
