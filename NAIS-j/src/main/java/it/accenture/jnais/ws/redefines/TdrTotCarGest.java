package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-CAR-GEST<br>
 * Variable: TDR-TOT-CAR-GEST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_CAR_GEST;
    }

    public void setTdrTotCarGest(AfDecimal tdrTotCarGest) {
        writeDecimalAsPacked(Pos.TDR_TOT_CAR_GEST, tdrTotCarGest.copy());
    }

    public void setTdrTotCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_CAR_GEST, Pos.TDR_TOT_CAR_GEST);
    }

    /**Original name: TDR-TOT-CAR-GEST<br>*/
    public AfDecimal getTdrTotCarGest() {
        return readPackedAsDecimal(Pos.TDR_TOT_CAR_GEST, Len.Int.TDR_TOT_CAR_GEST, Len.Fract.TDR_TOT_CAR_GEST);
    }

    public byte[] getTdrTotCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_CAR_GEST, Pos.TDR_TOT_CAR_GEST);
        return buffer;
    }

    public void setTdrTotCarGestNull(String tdrTotCarGestNull) {
        writeString(Pos.TDR_TOT_CAR_GEST_NULL, tdrTotCarGestNull, Len.TDR_TOT_CAR_GEST_NULL);
    }

    /**Original name: TDR-TOT-CAR-GEST-NULL<br>*/
    public String getTdrTotCarGestNull() {
        return readString(Pos.TDR_TOT_CAR_GEST_NULL, Len.TDR_TOT_CAR_GEST_NULL);
    }

    public String getTdrTotCarGestNullFormatted() {
        return Functions.padBlanks(getTdrTotCarGestNull(), Len.TDR_TOT_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_GEST = 1;
        public static final int TDR_TOT_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_GEST = 8;
        public static final int TDR_TOT_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
