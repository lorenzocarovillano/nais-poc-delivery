package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-VLT<br>
 * Variable: TIT-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_VLT;
    }

    public void setTitDtVlt(int titDtVlt) {
        writeIntAsPacked(Pos.TIT_DT_VLT, titDtVlt, Len.Int.TIT_DT_VLT);
    }

    public void setTitDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_VLT, Pos.TIT_DT_VLT);
    }

    /**Original name: TIT-DT-VLT<br>*/
    public int getTitDtVlt() {
        return readPackedAsInt(Pos.TIT_DT_VLT, Len.Int.TIT_DT_VLT);
    }

    public byte[] getTitDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_VLT, Pos.TIT_DT_VLT);
        return buffer;
    }

    public void setTitDtVltNull(String titDtVltNull) {
        writeString(Pos.TIT_DT_VLT_NULL, titDtVltNull, Len.TIT_DT_VLT_NULL);
    }

    /**Original name: TIT-DT-VLT-NULL<br>*/
    public String getTitDtVltNull() {
        return readString(Pos.TIT_DT_VLT_NULL, Len.TIT_DT_VLT_NULL);
    }

    public String getTitDtVltNullFormatted() {
        return Functions.padBlanks(getTitDtVltNull(), Len.TIT_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_VLT = 1;
        public static final int TIT_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_VLT = 5;
        public static final int TIT_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
