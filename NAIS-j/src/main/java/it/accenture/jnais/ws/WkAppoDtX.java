package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-APPO-DT-X<br>
 * Variable: WK-APPO-DT-X from program LOAS0820<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAppoDtX {

    //==== PROPERTIES ====
    //Original name: WK-DT-X-GG
    private String gg = DefaultValues.stringVal(Len.GG);
    //Original name: FILLER-WK-APPO-DT-X
    private char flr1 = '/';
    //Original name: WK-DT-X-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: FILLER-WK-APPO-DT-X-1
    private char flr2 = '/';
    //Original name: WK-DT-X-AAAA
    private String aaaa = DefaultValues.stringVal(Len.AAAA);

    //==== METHODS ====
    public String getWkAppoDtXFormatted() {
        return MarshalByteExt.bufferToStr(getWkAppoDtXBytes());
    }

    public byte[] getWkAppoDtXBytes() {
        byte[] buffer = new byte[Len.WK_APPO_DT_X];
        return getWkAppoDtXBytes(buffer, 1);
    }

    public byte[] getWkAppoDtXBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        position += Len.GG;
        MarshalByte.writeChar(buffer, position, flr1);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeChar(buffer, position, flr2);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, aaaa, Len.AAAA);
        return buffer;
    }

    public void setGgFormatted(String gg) {
        this.gg = Trunc.toUnsignedNumeric(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    public char getFlr1() {
        return this.flr1;
    }

    public void setMmFormatted(String mm) {
        this.mm = Trunc.toUnsignedNumeric(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public char getFlr2() {
        return this.flr2;
    }

    public void setAaaaFormatted(String aaaa) {
        this.aaaa = Trunc.toUnsignedNumeric(aaaa, Len.AAAA);
    }

    public short getAaaa() {
        return NumericDisplay.asShort(this.aaaa);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GG = 2;
        public static final int MM = 2;
        public static final int AAAA = 4;
        public static final int FLR1 = 1;
        public static final int WK_APPO_DT_X = GG + MM + AAAA + 2 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
