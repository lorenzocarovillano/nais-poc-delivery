package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-FLAG-STOR-ESECUZ<br>
 * Variable: IABI0011-FLAG-STOR-ESECUZ from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011FlagStorEsecuz {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagStorEsecuz(char flagStorEsecuz) {
        this.value = flagStorEsecuz;
    }

    public void setFlagStorEsecuzFormatted(String flagStorEsecuz) {
        setFlagStorEsecuz(Functions.charAt(flagStorEsecuz, Types.CHAR_SIZE));
    }

    public char getFlagStorEsecuz() {
        return this.value;
    }

    public boolean isYes() {
        return value == YES;
    }

    public boolean isNo() {
        return value == NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_STOR_ESECUZ = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
