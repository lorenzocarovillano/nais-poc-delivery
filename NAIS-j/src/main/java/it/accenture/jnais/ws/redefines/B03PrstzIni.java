package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRSTZ-INI<br>
 * Variable: B03-PRSTZ-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrstzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PrstzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRSTZ_INI;
    }

    public void setB03PrstzIni(AfDecimal b03PrstzIni) {
        writeDecimalAsPacked(Pos.B03_PRSTZ_INI, b03PrstzIni.copy());
    }

    public void setB03PrstzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRSTZ_INI, Pos.B03_PRSTZ_INI);
    }

    /**Original name: B03-PRSTZ-INI<br>*/
    public AfDecimal getB03PrstzIni() {
        return readPackedAsDecimal(Pos.B03_PRSTZ_INI, Len.Int.B03_PRSTZ_INI, Len.Fract.B03_PRSTZ_INI);
    }

    public byte[] getB03PrstzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRSTZ_INI, Pos.B03_PRSTZ_INI);
        return buffer;
    }

    public void setB03PrstzIniNull(String b03PrstzIniNull) {
        writeString(Pos.B03_PRSTZ_INI_NULL, b03PrstzIniNull, Len.B03_PRSTZ_INI_NULL);
    }

    /**Original name: B03-PRSTZ-INI-NULL<br>*/
    public String getB03PrstzIniNull() {
        return readString(Pos.B03_PRSTZ_INI_NULL, Len.B03_PRSTZ_INI_NULL);
    }

    public String getB03PrstzIniNullFormatted() {
        return Functions.padBlanks(getB03PrstzIniNull(), Len.B03_PRSTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRSTZ_INI = 1;
        public static final int B03_PRSTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRSTZ_INI = 8;
        public static final int B03_PRSTZ_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRSTZ_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRSTZ_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
