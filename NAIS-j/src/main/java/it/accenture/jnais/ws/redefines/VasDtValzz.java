package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-DT-VALZZ<br>
 * Variable: VAS-DT-VALZZ from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasDtValzz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasDtValzz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_DT_VALZZ;
    }

    public void setVasDtValzz(int vasDtValzz) {
        writeIntAsPacked(Pos.VAS_DT_VALZZ, vasDtValzz, Len.Int.VAS_DT_VALZZ);
    }

    public void setVasDtValzzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_DT_VALZZ, Pos.VAS_DT_VALZZ);
    }

    /**Original name: VAS-DT-VALZZ<br>*/
    public int getVasDtValzz() {
        return readPackedAsInt(Pos.VAS_DT_VALZZ, Len.Int.VAS_DT_VALZZ);
    }

    public byte[] getVasDtValzzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_DT_VALZZ, Pos.VAS_DT_VALZZ);
        return buffer;
    }

    public void setVasDtValzzNull(String vasDtValzzNull) {
        writeString(Pos.VAS_DT_VALZZ_NULL, vasDtValzzNull, Len.VAS_DT_VALZZ_NULL);
    }

    /**Original name: VAS-DT-VALZZ-NULL<br>*/
    public String getVasDtValzzNull() {
        return readString(Pos.VAS_DT_VALZZ_NULL, Len.VAS_DT_VALZZ_NULL);
    }

    public String getVasDtValzzNullFormatted() {
        return Functions.padBlanks(getVasDtValzzNull(), Len.VAS_DT_VALZZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_DT_VALZZ = 1;
        public static final int VAS_DT_VALZZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_DT_VALZZ = 5;
        public static final int VAS_DT_VALZZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_DT_VALZZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
