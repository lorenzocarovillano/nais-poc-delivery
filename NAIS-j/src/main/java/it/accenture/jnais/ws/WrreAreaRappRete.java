package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WrreTabRappRete;

/**Original name: WRRE-AREA-RAPP-RETE<br>
 * Variable: WRRE-AREA-RAPP-RETE from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WrreAreaRappRete extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_RRE_MAXOCCURS = 20;
    //Original name: WRRE-ELE-RAPP-RETE-MAX
    private short eleRappReteMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRRE-TAB-RRE
    private WrreTabRappRete[] tabRre = new WrreTabRappRete[TAB_RRE_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WrreAreaRappRete() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_AREA_RAPP_RETE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWrreAreaRappReteBytes(buf);
    }

    public void init() {
        for (int tabRreIdx = 1; tabRreIdx <= TAB_RRE_MAXOCCURS; tabRreIdx++) {
            tabRre[tabRreIdx - 1] = new WrreTabRappRete();
        }
    }

    public String getWrreAreaRappReteFormatted() {
        return MarshalByteExt.bufferToStr(getWrreAreaRappReteBytes());
    }

    public void setWrreAreaRappReteBytes(byte[] buffer) {
        setWrreAreaRappReteBytes(buffer, 1);
    }

    public byte[] getWrreAreaRappReteBytes() {
        byte[] buffer = new byte[Len.WRRE_AREA_RAPP_RETE];
        return getWrreAreaRappReteBytes(buffer, 1);
    }

    public void setWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        eleRappReteMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_RRE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabRre[idx - 1].setWrreTabRappReteBytes(buffer, position);
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
            else {
                tabRre[idx - 1].initWrreTabRappReteSpaces();
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
        }
    }

    public byte[] getWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleRappReteMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_RRE_MAXOCCURS; idx++) {
            tabRre[idx - 1].getTabRreBytes(buffer, position);
            position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
        }
        return buffer;
    }

    public void setEleRappReteMax(short eleRappReteMax) {
        this.eleRappReteMax = eleRappReteMax;
    }

    public short getEleRappReteMax() {
        return this.eleRappReteMax;
    }

    public WrreTabRappRete getTabRre(int idx) {
        return tabRre[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWrreAreaRappReteBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_RAPP_RETE_MAX = 2;
        public static final int WRRE_AREA_RAPP_RETE = ELE_RAPP_RETE_MAX + WrreAreaRappRete.TAB_RRE_MAXOCCURS * WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
