package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EOF-FILESQS1<br>
 * Variable: EOF-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EofFilesqs1 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setEofFilesqs1(char eofFilesqs1) {
        this.value = eofFilesqs1;
    }

    public char getEofFilesqs1() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
