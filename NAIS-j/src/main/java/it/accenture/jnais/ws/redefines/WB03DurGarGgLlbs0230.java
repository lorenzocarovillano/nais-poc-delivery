package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-GAR-GG<br>
 * Variable: W-B03-DUR-GAR-GG from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurGarGgLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurGarGgLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_GAR_GG;
    }

    public void setwB03DurGarGg(int wB03DurGarGg) {
        writeIntAsPacked(Pos.W_B03_DUR_GAR_GG, wB03DurGarGg, Len.Int.W_B03_DUR_GAR_GG);
    }

    /**Original name: W-B03-DUR-GAR-GG<br>*/
    public int getwB03DurGarGg() {
        return readPackedAsInt(Pos.W_B03_DUR_GAR_GG, Len.Int.W_B03_DUR_GAR_GG);
    }

    public byte[] getwB03DurGarGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_GAR_GG, Pos.W_B03_DUR_GAR_GG);
        return buffer;
    }

    public void setwB03DurGarGgNull(String wB03DurGarGgNull) {
        writeString(Pos.W_B03_DUR_GAR_GG_NULL, wB03DurGarGgNull, Len.W_B03_DUR_GAR_GG_NULL);
    }

    /**Original name: W-B03-DUR-GAR-GG-NULL<br>*/
    public String getwB03DurGarGgNull() {
        return readString(Pos.W_B03_DUR_GAR_GG_NULL, Len.W_B03_DUR_GAR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GAR_GG = 1;
        public static final int W_B03_DUR_GAR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GAR_GG = 3;
        public static final int W_B03_DUR_GAR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_GAR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
