package it.accenture.jnais.ws.enums;

/**Original name: IABV0006-GUIDE-ROWS-READ<br>
 * Variable: IABV0006-GUIDE-ROWS-READ from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006GuideRowsRead {

    //==== PROPERTIES ====
    private char value = 'Y';
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setGuideRowsRead(char guideRowsRead) {
        this.value = guideRowsRead;
    }

    public char getGuideRowsRead() {
        return this.value;
    }

    public boolean isIabv0006GuideRowsReadYes() {
        return value == YES;
    }

    public void setIabv0006GuideRowsReadYes() {
        value = YES;
    }

    public boolean isIabv0006GuideRowsReadNo() {
        return value == NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GUIDE_ROWS_READ = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
