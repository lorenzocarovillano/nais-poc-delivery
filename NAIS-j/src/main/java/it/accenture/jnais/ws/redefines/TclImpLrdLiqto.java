package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-LRD-LIQTO<br>
 * Variable: TCL-IMP-LRD-LIQTO from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpLrdLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpLrdLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_LRD_LIQTO;
    }

    public void setTclImpLrdLiqto(AfDecimal tclImpLrdLiqto) {
        writeDecimalAsPacked(Pos.TCL_IMP_LRD_LIQTO, tclImpLrdLiqto.copy());
    }

    public void setTclImpLrdLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_LRD_LIQTO, Pos.TCL_IMP_LRD_LIQTO);
    }

    /**Original name: TCL-IMP-LRD-LIQTO<br>*/
    public AfDecimal getTclImpLrdLiqto() {
        return readPackedAsDecimal(Pos.TCL_IMP_LRD_LIQTO, Len.Int.TCL_IMP_LRD_LIQTO, Len.Fract.TCL_IMP_LRD_LIQTO);
    }

    public byte[] getTclImpLrdLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_LRD_LIQTO, Pos.TCL_IMP_LRD_LIQTO);
        return buffer;
    }

    public void setTclImpLrdLiqtoNull(String tclImpLrdLiqtoNull) {
        writeString(Pos.TCL_IMP_LRD_LIQTO_NULL, tclImpLrdLiqtoNull, Len.TCL_IMP_LRD_LIQTO_NULL);
    }

    /**Original name: TCL-IMP-LRD-LIQTO-NULL<br>*/
    public String getTclImpLrdLiqtoNull() {
        return readString(Pos.TCL_IMP_LRD_LIQTO_NULL, Len.TCL_IMP_LRD_LIQTO_NULL);
    }

    public String getTclImpLrdLiqtoNullFormatted() {
        return Functions.padBlanks(getTclImpLrdLiqtoNull(), Len.TCL_IMP_LRD_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_LRD_LIQTO = 1;
        public static final int TCL_IMP_LRD_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_LRD_LIQTO = 8;
        public static final int TCL_IMP_LRD_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_LRD_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_LRD_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
