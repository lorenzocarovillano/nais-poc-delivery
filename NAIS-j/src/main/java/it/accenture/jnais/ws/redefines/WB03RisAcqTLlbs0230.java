package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-ACQ-T<br>
 * Variable: W-B03-RIS-ACQ-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisAcqTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisAcqTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_ACQ_T;
    }

    public void setwB03RisAcqT(AfDecimal wB03RisAcqT) {
        writeDecimalAsPacked(Pos.W_B03_RIS_ACQ_T, wB03RisAcqT.copy());
    }

    /**Original name: W-B03-RIS-ACQ-T<br>*/
    public AfDecimal getwB03RisAcqT() {
        return readPackedAsDecimal(Pos.W_B03_RIS_ACQ_T, Len.Int.W_B03_RIS_ACQ_T, Len.Fract.W_B03_RIS_ACQ_T);
    }

    public byte[] getwB03RisAcqTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_ACQ_T, Pos.W_B03_RIS_ACQ_T);
        return buffer;
    }

    public void setwB03RisAcqTNull(String wB03RisAcqTNull) {
        writeString(Pos.W_B03_RIS_ACQ_T_NULL, wB03RisAcqTNull, Len.W_B03_RIS_ACQ_T_NULL);
    }

    /**Original name: W-B03-RIS-ACQ-T-NULL<br>*/
    public String getwB03RisAcqTNull() {
        return readString(Pos.W_B03_RIS_ACQ_T_NULL, Len.W_B03_RIS_ACQ_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_ACQ_T = 1;
        public static final int W_B03_RIS_ACQ_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_ACQ_T = 8;
        public static final int W_B03_RIS_ACQ_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_ACQ_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_ACQ_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
