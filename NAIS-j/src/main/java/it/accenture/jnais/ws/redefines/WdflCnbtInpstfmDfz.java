package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNBT-INPSTFM-DFZ<br>
 * Variable: WDFL-CNBT-INPSTFM-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCnbtInpstfmDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCnbtInpstfmDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNBT_INPSTFM_DFZ;
    }

    public void setWdflCnbtInpstfmDfz(AfDecimal wdflCnbtInpstfmDfz) {
        writeDecimalAsPacked(Pos.WDFL_CNBT_INPSTFM_DFZ, wdflCnbtInpstfmDfz.copy());
    }

    public void setWdflCnbtInpstfmDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNBT_INPSTFM_DFZ, Pos.WDFL_CNBT_INPSTFM_DFZ);
    }

    /**Original name: WDFL-CNBT-INPSTFM-DFZ<br>*/
    public AfDecimal getWdflCnbtInpstfmDfz() {
        return readPackedAsDecimal(Pos.WDFL_CNBT_INPSTFM_DFZ, Len.Int.WDFL_CNBT_INPSTFM_DFZ, Len.Fract.WDFL_CNBT_INPSTFM_DFZ);
    }

    public byte[] getWdflCnbtInpstfmDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNBT_INPSTFM_DFZ, Pos.WDFL_CNBT_INPSTFM_DFZ);
        return buffer;
    }

    public void setWdflCnbtInpstfmDfzNull(String wdflCnbtInpstfmDfzNull) {
        writeString(Pos.WDFL_CNBT_INPSTFM_DFZ_NULL, wdflCnbtInpstfmDfzNull, Len.WDFL_CNBT_INPSTFM_DFZ_NULL);
    }

    /**Original name: WDFL-CNBT-INPSTFM-DFZ-NULL<br>*/
    public String getWdflCnbtInpstfmDfzNull() {
        return readString(Pos.WDFL_CNBT_INPSTFM_DFZ_NULL, Len.WDFL_CNBT_INPSTFM_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNBT_INPSTFM_DFZ = 1;
        public static final int WDFL_CNBT_INPSTFM_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNBT_INPSTFM_DFZ = 8;
        public static final int WDFL_CNBT_INPSTFM_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNBT_INPSTFM_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNBT_INPSTFM_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
