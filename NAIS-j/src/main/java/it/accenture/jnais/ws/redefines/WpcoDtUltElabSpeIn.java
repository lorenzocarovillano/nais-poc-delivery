package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-SPE-IN<br>
 * Variable: WPCO-DT-ULT-ELAB-SPE-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabSpeIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabSpeIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_SPE_IN;
    }

    public void setWpcoDtUltElabSpeIn(int wpcoDtUltElabSpeIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_SPE_IN, wpcoDtUltElabSpeIn, Len.Int.WPCO_DT_ULT_ELAB_SPE_IN);
    }

    public void setDpcoDtUltElabSpeInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_SPE_IN, Pos.WPCO_DT_ULT_ELAB_SPE_IN);
    }

    /**Original name: WPCO-DT-ULT-ELAB-SPE-IN<br>*/
    public int getWpcoDtUltElabSpeIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_SPE_IN, Len.Int.WPCO_DT_ULT_ELAB_SPE_IN);
    }

    public byte[] getWpcoDtUltElabSpeInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_SPE_IN, Pos.WPCO_DT_ULT_ELAB_SPE_IN);
        return buffer;
    }

    public void setWpcoDtUltElabSpeInNull(String wpcoDtUltElabSpeInNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_SPE_IN_NULL, wpcoDtUltElabSpeInNull, Len.WPCO_DT_ULT_ELAB_SPE_IN_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-SPE-IN-NULL<br>*/
    public String getWpcoDtUltElabSpeInNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_SPE_IN_NULL, Len.WPCO_DT_ULT_ELAB_SPE_IN_NULL);
    }

    public String getWpcoDtUltElabSpeInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabSpeInNull(), Len.WPCO_DT_ULT_ELAB_SPE_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_SPE_IN = 1;
        public static final int WPCO_DT_ULT_ELAB_SPE_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_SPE_IN = 5;
        public static final int WPCO_DT_ULT_ELAB_SPE_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_SPE_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
