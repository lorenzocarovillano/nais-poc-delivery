package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRL-DT-SIN-3O-ASSTO<br>
 * Variable: WGRL-DT-SIN-3O-ASSTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrlDtSin3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrlDtSin3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRL_DT_SIN3O_ASSTO;
    }

    public void setWgrlDtSin3oAssto(int wgrlDtSin3oAssto) {
        writeIntAsPacked(Pos.WGRL_DT_SIN3O_ASSTO, wgrlDtSin3oAssto, Len.Int.WGRL_DT_SIN3O_ASSTO);
    }

    public void setWgrlDtSin3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRL_DT_SIN3O_ASSTO, Pos.WGRL_DT_SIN3O_ASSTO);
    }

    /**Original name: WGRL-DT-SIN-3O-ASSTO<br>*/
    public int getWgrlDtSin3oAssto() {
        return readPackedAsInt(Pos.WGRL_DT_SIN3O_ASSTO, Len.Int.WGRL_DT_SIN3O_ASSTO);
    }

    public byte[] getWgrlDtSin3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRL_DT_SIN3O_ASSTO, Pos.WGRL_DT_SIN3O_ASSTO);
        return buffer;
    }

    public void initWgrlDtSin3oAsstoSpaces() {
        fill(Pos.WGRL_DT_SIN3O_ASSTO, Len.WGRL_DT_SIN3O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrlDtSin3oAsstoNull(String wgrlDtSin3oAsstoNull) {
        writeString(Pos.WGRL_DT_SIN3O_ASSTO_NULL, wgrlDtSin3oAsstoNull, Len.WGRL_DT_SIN3O_ASSTO_NULL);
    }

    /**Original name: WGRL-DT-SIN-3O-ASSTO-NULL<br>*/
    public String getWgrlDtSin3oAsstoNull() {
        return readString(Pos.WGRL_DT_SIN3O_ASSTO_NULL, Len.WGRL_DT_SIN3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRL_DT_SIN3O_ASSTO = 1;
        public static final int WGRL_DT_SIN3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRL_DT_SIN3O_ASSTO = 5;
        public static final int WGRL_DT_SIN3O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRL_DT_SIN3O_ASSTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
