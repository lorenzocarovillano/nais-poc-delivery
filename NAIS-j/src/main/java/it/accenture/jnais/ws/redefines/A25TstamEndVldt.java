package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-TSTAM-END-VLDT<br>
 * Variable: A25-TSTAM-END-VLDT from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25TstamEndVldt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25TstamEndVldt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_TSTAM_END_VLDT;
    }

    public void setA25TstamEndVldt(long a25TstamEndVldt) {
        writeLongAsPacked(Pos.A25_TSTAM_END_VLDT, a25TstamEndVldt, Len.Int.A25_TSTAM_END_VLDT);
    }

    public void setA25TstamEndVldtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_TSTAM_END_VLDT, Pos.A25_TSTAM_END_VLDT);
    }

    /**Original name: A25-TSTAM-END-VLDT<br>*/
    public long getA25TstamEndVldt() {
        return readPackedAsLong(Pos.A25_TSTAM_END_VLDT, Len.Int.A25_TSTAM_END_VLDT);
    }

    public byte[] getA25TstamEndVldtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_TSTAM_END_VLDT, Pos.A25_TSTAM_END_VLDT);
        return buffer;
    }

    public void setA25TstamEndVldtNull(String a25TstamEndVldtNull) {
        writeString(Pos.A25_TSTAM_END_VLDT_NULL, a25TstamEndVldtNull, Len.A25_TSTAM_END_VLDT_NULL);
    }

    /**Original name: A25-TSTAM-END-VLDT-NULL<br>*/
    public String getA25TstamEndVldtNull() {
        return readString(Pos.A25_TSTAM_END_VLDT_NULL, Len.A25_TSTAM_END_VLDT_NULL);
    }

    public String getA25TstamEndVldtNullFormatted() {
        return Functions.padBlanks(getA25TstamEndVldtNull(), Len.A25_TSTAM_END_VLDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_TSTAM_END_VLDT = 1;
        public static final int A25_TSTAM_END_VLDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_TSTAM_END_VLDT = 10;
        public static final int A25_TSTAM_END_VLDT_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_TSTAM_END_VLDT = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
