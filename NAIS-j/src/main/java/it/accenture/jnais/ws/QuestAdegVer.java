package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P56CodCan;
import it.accenture.jnais.ws.redefines.P56DtIniFntRedd;
import it.accenture.jnais.ws.redefines.P56DtIniFntRedd2;
import it.accenture.jnais.ws.redefines.P56DtIniFntRedd3;
import it.accenture.jnais.ws.redefines.P56IdAssicurati;
import it.accenture.jnais.ws.redefines.P56IdRappAna;
import it.accenture.jnais.ws.redefines.P56ImpAfi;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc1;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc2;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc3;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc4;
import it.accenture.jnais.ws.redefines.P56ImpPaEspMsc5;
import it.accenture.jnais.ws.redefines.P56ImpTotAffAcc;
import it.accenture.jnais.ws.redefines.P56ImpTotAffUtil;
import it.accenture.jnais.ws.redefines.P56ImpTotFinAcc;
import it.accenture.jnais.ws.redefines.P56ImpTotFinUtil;
import it.accenture.jnais.ws.redefines.P56NumDip;
import it.accenture.jnais.ws.redefines.P56PcEspAgPaMsc;
import it.accenture.jnais.ws.redefines.P56PcQuoDetTitEff;
import it.accenture.jnais.ws.redefines.P56PcRipPatAsVita;
import it.accenture.jnais.ws.redefines.P56PcRipPatIm;
import it.accenture.jnais.ws.redefines.P56PcRipPatSetIm;
import it.accenture.jnais.ws.redefines.P56ReddCon;
import it.accenture.jnais.ws.redefines.P56ReddFattAnnu;
import it.accenture.jnais.ws.redefines.P56TpMovi;
import it.accenture.jnais.ws.redefines.P56UltFattAnnu;

/**Original name: QUEST-ADEG-VER<br>
 * Variable: QUEST-ADEG-VER from copybook IDBVP561<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class QuestAdegVer extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P56-ID-QUEST-ADEG-VER
    private int p56IdQuestAdegVer = DefaultValues.INT_VAL;
    //Original name: P56-COD-COMP-ANIA
    private int p56CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P56-ID-MOVI-CRZ
    private int p56IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P56-ID-RAPP-ANA
    private P56IdRappAna p56IdRappAna = new P56IdRappAna();
    //Original name: P56-ID-POLI
    private int p56IdPoli = DefaultValues.INT_VAL;
    //Original name: P56-NATURA-OPRZ
    private String p56NaturaOprz = DefaultValues.stringVal(Len.P56_NATURA_OPRZ);
    //Original name: P56-ORGN-FND
    private String p56OrgnFnd = DefaultValues.stringVal(Len.P56_ORGN_FND);
    //Original name: P56-COD-QLFC-PROF
    private String p56CodQlfcProf = DefaultValues.stringVal(Len.P56_COD_QLFC_PROF);
    //Original name: P56-COD-NAZ-QLFC-PROF
    private String p56CodNazQlfcProf = DefaultValues.stringVal(Len.P56_COD_NAZ_QLFC_PROF);
    //Original name: P56-COD-PRV-QLFC-PROF
    private String p56CodPrvQlfcProf = DefaultValues.stringVal(Len.P56_COD_PRV_QLFC_PROF);
    //Original name: P56-FNT-REDD
    private String p56FntRedd = DefaultValues.stringVal(Len.P56_FNT_REDD);
    //Original name: P56-REDD-FATT-ANNU
    private P56ReddFattAnnu p56ReddFattAnnu = new P56ReddFattAnnu();
    //Original name: P56-COD-ATECO
    private String p56CodAteco = DefaultValues.stringVal(Len.P56_COD_ATECO);
    //Original name: P56-VALUT-COLL
    private String p56ValutColl = DefaultValues.stringVal(Len.P56_VALUT_COLL);
    //Original name: P56-DS-OPER-SQL
    private char p56DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P56-DS-VER
    private int p56DsVer = DefaultValues.INT_VAL;
    //Original name: P56-DS-TS-CPTZ
    private long p56DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: P56-DS-UTENTE
    private String p56DsUtente = DefaultValues.stringVal(Len.P56_DS_UTENTE);
    //Original name: P56-DS-STATO-ELAB
    private char p56DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P56-LUOGO-COSTITUZIONE-LEN
    private short p56LuogoCostituzioneLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-LUOGO-COSTITUZIONE
    private String p56LuogoCostituzione = DefaultValues.stringVal(Len.P56_LUOGO_COSTITUZIONE);
    //Original name: P56-TP-MOVI
    private P56TpMovi p56TpMovi = new P56TpMovi();
    //Original name: P56-FL-RAG-RAPP
    private char p56FlRagRapp = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PRSZ-TIT-EFF
    private char p56FlPrszTitEff = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-MOT-RISC
    private String p56TpMotRisc = DefaultValues.stringVal(Len.P56_TP_MOT_RISC);
    //Original name: P56-TP-PNT-VND
    private String p56TpPntVnd = DefaultValues.stringVal(Len.P56_TP_PNT_VND);
    //Original name: P56-TP-ADEG-VER
    private String p56TpAdegVer = DefaultValues.stringVal(Len.P56_TP_ADEG_VER);
    //Original name: P56-TP-RELA-ESEC
    private String p56TpRelaEsec = DefaultValues.stringVal(Len.P56_TP_RELA_ESEC);
    //Original name: P56-TP-SCO-FIN-RAPP
    private String p56TpScoFinRapp = DefaultValues.stringVal(Len.P56_TP_SCO_FIN_RAPP);
    //Original name: P56-FL-PRSZ-3O-PAGAT
    private char p56FlPrsz3oPagat = DefaultValues.CHAR_VAL;
    //Original name: P56-AREA-GEO-PROV-FND
    private String p56AreaGeoProvFnd = DefaultValues.stringVal(Len.P56_AREA_GEO_PROV_FND);
    //Original name: P56-TP-DEST-FND
    private String p56TpDestFnd = DefaultValues.stringVal(Len.P56_TP_DEST_FND);
    //Original name: P56-FL-PAESE-RESID-AUT
    private char p56FlPaeseResidAut = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PAESE-CIT-AUT
    private char p56FlPaeseCitAut = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PAESE-NAZ-AUT
    private char p56FlPaeseNazAut = DefaultValues.CHAR_VAL;
    //Original name: P56-COD-PROF-PREC
    private String p56CodProfPrec = DefaultValues.stringVal(Len.P56_COD_PROF_PREC);
    //Original name: P56-FL-AUT-PEP
    private char p56FlAutPep = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-IMP-CAR-PUB
    private char p56FlImpCarPub = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-LIS-TERR-SORV
    private char p56FlLisTerrSorv = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-SIT-FIN-PAT
    private String p56TpSitFinPat = DefaultValues.stringVal(Len.P56_TP_SIT_FIN_PAT);
    //Original name: P56-TP-SIT-FIN-PAT-CON
    private String p56TpSitFinPatCon = DefaultValues.stringVal(Len.P56_TP_SIT_FIN_PAT_CON);
    //Original name: P56-IMP-TOT-AFF-UTIL
    private P56ImpTotAffUtil p56ImpTotAffUtil = new P56ImpTotAffUtil();
    //Original name: P56-IMP-TOT-FIN-UTIL
    private P56ImpTotFinUtil p56ImpTotFinUtil = new P56ImpTotFinUtil();
    //Original name: P56-IMP-TOT-AFF-ACC
    private P56ImpTotAffAcc p56ImpTotAffAcc = new P56ImpTotAffAcc();
    //Original name: P56-IMP-TOT-FIN-ACC
    private P56ImpTotFinAcc p56ImpTotFinAcc = new P56ImpTotFinAcc();
    //Original name: P56-TP-FRM-GIUR-SAV
    private String p56TpFrmGiurSav = DefaultValues.stringVal(Len.P56_TP_FRM_GIUR_SAV);
    //Original name: P56-REG-COLL-POLI
    private String p56RegCollPoli = DefaultValues.stringVal(Len.P56_REG_COLL_POLI);
    //Original name: P56-NUM-TEL
    private String p56NumTel = DefaultValues.stringVal(Len.P56_NUM_TEL);
    //Original name: P56-NUM-DIP
    private P56NumDip p56NumDip = new P56NumDip();
    //Original name: P56-TP-SIT-FAM-CONV
    private String p56TpSitFamConv = DefaultValues.stringVal(Len.P56_TP_SIT_FAM_CONV);
    //Original name: P56-COD-PROF-CON
    private String p56CodProfCon = DefaultValues.stringVal(Len.P56_COD_PROF_CON);
    //Original name: P56-FL-ES-PROC-PEN
    private char p56FlEsProcPen = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-COND-CLIENTE
    private String p56TpCondCliente = DefaultValues.stringVal(Len.P56_TP_COND_CLIENTE);
    //Original name: P56-COD-SAE
    private String p56CodSae = DefaultValues.stringVal(Len.P56_COD_SAE);
    //Original name: P56-TP-OPER-ESTERO
    private String p56TpOperEstero = DefaultValues.stringVal(Len.P56_TP_OPER_ESTERO);
    //Original name: P56-STAT-OPER-ESTERO
    private String p56StatOperEstero = DefaultValues.stringVal(Len.P56_STAT_OPER_ESTERO);
    //Original name: P56-COD-PRV-SVOL-ATT
    private String p56CodPrvSvolAtt = DefaultValues.stringVal(Len.P56_COD_PRV_SVOL_ATT);
    //Original name: P56-COD-STAT-SVOL-ATT
    private String p56CodStatSvolAtt = DefaultValues.stringVal(Len.P56_COD_STAT_SVOL_ATT);
    //Original name: P56-TP-SOC
    private String p56TpSoc = DefaultValues.stringVal(Len.P56_TP_SOC);
    //Original name: P56-FL-IND-SOC-QUOT
    private char p56FlIndSocQuot = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-SIT-GIUR
    private String p56TpSitGiur = DefaultValues.stringVal(Len.P56_TP_SIT_GIUR);
    //Original name: P56-PC-QUO-DET-TIT-EFF
    private P56PcQuoDetTitEff p56PcQuoDetTitEff = new P56PcQuoDetTitEff();
    //Original name: P56-TP-PRFL-RSH-PEP
    private String p56TpPrflRshPep = DefaultValues.stringVal(Len.P56_TP_PRFL_RSH_PEP);
    //Original name: P56-TP-PEP
    private String p56TpPep = DefaultValues.stringVal(Len.P56_TP_PEP);
    //Original name: P56-FL-NOT-PREG
    private char p56FlNotPreg = DefaultValues.CHAR_VAL;
    //Original name: P56-DT-INI-FNT-REDD
    private P56DtIniFntRedd p56DtIniFntRedd = new P56DtIniFntRedd();
    //Original name: P56-FNT-REDD-2
    private String p56FntRedd2 = DefaultValues.stringVal(Len.P56_FNT_REDD2);
    //Original name: P56-DT-INI-FNT-REDD-2
    private P56DtIniFntRedd2 p56DtIniFntRedd2 = new P56DtIniFntRedd2();
    //Original name: P56-FNT-REDD-3
    private String p56FntRedd3 = DefaultValues.stringVal(Len.P56_FNT_REDD3);
    //Original name: P56-DT-INI-FNT-REDD-3
    private P56DtIniFntRedd3 p56DtIniFntRedd3 = new P56DtIniFntRedd3();
    //Original name: P56-MOT-ASS-TIT-EFF-LEN
    private short p56MotAssTitEffLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-MOT-ASS-TIT-EFF
    private String p56MotAssTitEff = DefaultValues.stringVal(Len.P56_MOT_ASS_TIT_EFF);
    //Original name: P56-FIN-COSTITUZIONE-LEN
    private short p56FinCostituzioneLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-FIN-COSTITUZIONE
    private String p56FinCostituzione = DefaultValues.stringVal(Len.P56_FIN_COSTITUZIONE);
    //Original name: P56-DESC-IMP-CAR-PUB-LEN
    private short p56DescImpCarPubLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-IMP-CAR-PUB
    private String p56DescImpCarPub = DefaultValues.stringVal(Len.P56_DESC_IMP_CAR_PUB);
    //Original name: P56-DESC-SCO-FIN-RAPP-LEN
    private short p56DescScoFinRappLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-SCO-FIN-RAPP
    private String p56DescScoFinRapp = DefaultValues.stringVal(Len.P56_DESC_SCO_FIN_RAPP);
    //Original name: P56-DESC-PROC-PNL-LEN
    private short p56DescProcPnlLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-PROC-PNL
    private String p56DescProcPnl = DefaultValues.stringVal(Len.P56_DESC_PROC_PNL);
    //Original name: P56-DESC-NOT-PREG-LEN
    private short p56DescNotPregLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-NOT-PREG
    private String p56DescNotPreg = DefaultValues.stringVal(Len.P56_DESC_NOT_PREG);
    //Original name: P56-ID-ASSICURATI
    private P56IdAssicurati p56IdAssicurati = new P56IdAssicurati();
    //Original name: P56-REDD-CON
    private P56ReddCon p56ReddCon = new P56ReddCon();
    //Original name: P56-DESC-LIB-MOT-RISC-LEN
    private short p56DescLibMotRiscLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-LIB-MOT-RISC
    private String p56DescLibMotRisc = DefaultValues.stringVal(Len.P56_DESC_LIB_MOT_RISC);
    //Original name: P56-TP-MOT-ASS-TIT-EFF
    private String p56TpMotAssTitEff = DefaultValues.stringVal(Len.P56_TP_MOT_ASS_TIT_EFF);
    //Original name: P56-TP-RAG-RAPP
    private String p56TpRagRapp = DefaultValues.stringVal(Len.P56_TP_RAG_RAPP);
    //Original name: P56-COD-CAN
    private P56CodCan p56CodCan = new P56CodCan();
    //Original name: P56-TP-FIN-COST
    private String p56TpFinCost = DefaultValues.stringVal(Len.P56_TP_FIN_COST);
    //Original name: P56-NAZ-DEST-FND
    private String p56NazDestFnd = DefaultValues.stringVal(Len.P56_NAZ_DEST_FND);
    //Original name: P56-FL-AU-FATCA-AEOI
    private char p56FlAuFatcaAeoi = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-CAR-FIN-GIUR
    private String p56TpCarFinGiur = DefaultValues.stringVal(Len.P56_TP_CAR_FIN_GIUR);
    //Original name: P56-TP-CAR-FIN-GIUR-AT
    private String p56TpCarFinGiurAt = DefaultValues.stringVal(Len.P56_TP_CAR_FIN_GIUR_AT);
    //Original name: P56-TP-CAR-FIN-GIUR-PA
    private String p56TpCarFinGiurPa = DefaultValues.stringVal(Len.P56_TP_CAR_FIN_GIUR_PA);
    //Original name: P56-FL-ISTITUZ-FIN
    private char p56FlIstituzFin = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-ORI-FND-TIT-EFF
    private String p56TpOriFndTitEff = DefaultValues.stringVal(Len.P56_TP_ORI_FND_TIT_EFF);
    //Original name: P56-PC-ESP-AG-PA-MSC
    private P56PcEspAgPaMsc p56PcEspAgPaMsc = new P56PcEspAgPaMsc();
    //Original name: P56-FL-PR-TR-USA
    private char p56FlPrTrUsa = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PR-TR-NO-USA
    private char p56FlPrTrNoUsa = DefaultValues.CHAR_VAL;
    //Original name: P56-PC-RIP-PAT-AS-VITA
    private P56PcRipPatAsVita p56PcRipPatAsVita = new P56PcRipPatAsVita();
    //Original name: P56-PC-RIP-PAT-IM
    private P56PcRipPatIm p56PcRipPatIm = new P56PcRipPatIm();
    //Original name: P56-PC-RIP-PAT-SET-IM
    private P56PcRipPatSetIm p56PcRipPatSetIm = new P56PcRipPatSetIm();
    //Original name: P56-TP-STATUS-AEOI
    private String p56TpStatusAeoi = DefaultValues.stringVal(Len.P56_TP_STATUS_AEOI);
    //Original name: P56-TP-STATUS-FATCA
    private String p56TpStatusFatca = DefaultValues.stringVal(Len.P56_TP_STATUS_FATCA);
    //Original name: P56-FL-RAPP-PA-MSC
    private char p56FlRappPaMsc = DefaultValues.CHAR_VAL;
    //Original name: P56-COD-COMUN-SVOL-ATT
    private String p56CodComunSvolAtt = DefaultValues.stringVal(Len.P56_COD_COMUN_SVOL_ATT);
    //Original name: P56-TP-DT-1O-CON-CLI
    private String p56TpDt1oConCli = DefaultValues.stringVal(Len.P56_TP_DT1O_CON_CLI);
    //Original name: P56-TP-MOD-EN-RELA-INT
    private String p56TpModEnRelaInt = DefaultValues.stringVal(Len.P56_TP_MOD_EN_RELA_INT);
    //Original name: P56-TP-REDD-ANNU-LRD
    private String p56TpReddAnnuLrd = DefaultValues.stringVal(Len.P56_TP_REDD_ANNU_LRD);
    //Original name: P56-TP-REDD-CON
    private String p56TpReddCon = DefaultValues.stringVal(Len.P56_TP_REDD_CON);
    //Original name: P56-TP-OPER-SOC-FID
    private String p56TpOperSocFid = DefaultValues.stringVal(Len.P56_TP_OPER_SOC_FID);
    //Original name: P56-COD-PA-ESP-MSC-1
    private String p56CodPaEspMsc1 = DefaultValues.stringVal(Len.P56_COD_PA_ESP_MSC1);
    //Original name: P56-IMP-PA-ESP-MSC-1
    private P56ImpPaEspMsc1 p56ImpPaEspMsc1 = new P56ImpPaEspMsc1();
    //Original name: P56-COD-PA-ESP-MSC-2
    private String p56CodPaEspMsc2 = DefaultValues.stringVal(Len.P56_COD_PA_ESP_MSC2);
    //Original name: P56-IMP-PA-ESP-MSC-2
    private P56ImpPaEspMsc2 p56ImpPaEspMsc2 = new P56ImpPaEspMsc2();
    //Original name: P56-COD-PA-ESP-MSC-3
    private String p56CodPaEspMsc3 = DefaultValues.stringVal(Len.P56_COD_PA_ESP_MSC3);
    //Original name: P56-IMP-PA-ESP-MSC-3
    private P56ImpPaEspMsc3 p56ImpPaEspMsc3 = new P56ImpPaEspMsc3();
    //Original name: P56-COD-PA-ESP-MSC-4
    private String p56CodPaEspMsc4 = DefaultValues.stringVal(Len.P56_COD_PA_ESP_MSC4);
    //Original name: P56-IMP-PA-ESP-MSC-4
    private P56ImpPaEspMsc4 p56ImpPaEspMsc4 = new P56ImpPaEspMsc4();
    //Original name: P56-COD-PA-ESP-MSC-5
    private String p56CodPaEspMsc5 = DefaultValues.stringVal(Len.P56_COD_PA_ESP_MSC5);
    //Original name: P56-IMP-PA-ESP-MSC-5
    private P56ImpPaEspMsc5 p56ImpPaEspMsc5 = new P56ImpPaEspMsc5();
    //Original name: P56-DESC-ORGN-FND-LEN
    private short p56DescOrgnFndLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-ORGN-FND
    private String p56DescOrgnFnd = DefaultValues.stringVal(Len.P56_DESC_ORGN_FND);
    //Original name: P56-COD-AUT-DUE-DIL
    private String p56CodAutDueDil = DefaultValues.stringVal(Len.P56_COD_AUT_DUE_DIL);
    //Original name: P56-FL-PR-QUEST-FATCA
    private char p56FlPrQuestFatca = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PR-QUEST-AEOI
    private char p56FlPrQuestAeoi = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PR-QUEST-OFAC
    private char p56FlPrQuestOfac = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PR-QUEST-KYC
    private char p56FlPrQuestKyc = DefaultValues.CHAR_VAL;
    //Original name: P56-FL-PR-QUEST-MSCQ
    private char p56FlPrQuestMscq = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-NOT-PREG
    private String p56TpNotPreg = DefaultValues.stringVal(Len.P56_TP_NOT_PREG);
    //Original name: P56-TP-PROC-PNL
    private String p56TpProcPnl = DefaultValues.stringVal(Len.P56_TP_PROC_PNL);
    //Original name: P56-COD-IMP-CAR-PUB
    private String p56CodImpCarPub = DefaultValues.stringVal(Len.P56_COD_IMP_CAR_PUB);
    //Original name: P56-OPRZ-SOSPETTE
    private char p56OprzSospette = DefaultValues.CHAR_VAL;
    //Original name: P56-ULT-FATT-ANNU
    private P56UltFattAnnu p56UltFattAnnu = new P56UltFattAnnu();
    //Original name: P56-DESC-PEP-LEN
    private short p56DescPepLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-PEP
    private String p56DescPep = DefaultValues.stringVal(Len.P56_DESC_PEP);
    //Original name: P56-NUM-TEL-2
    private String p56NumTel2 = DefaultValues.stringVal(Len.P56_NUM_TEL2);
    //Original name: P56-IMP-AFI
    private P56ImpAfi p56ImpAfi = new P56ImpAfi();
    //Original name: P56-FL-NEW-PRO
    private char p56FlNewPro = DefaultValues.CHAR_VAL;
    //Original name: P56-TP-MOT-CAMBIO-CNTR
    private String p56TpMotCambioCntr = DefaultValues.stringVal(Len.P56_TP_MOT_CAMBIO_CNTR);
    //Original name: P56-DESC-MOT-CAMBIO-CN-LEN
    private short p56DescMotCambioCnLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P56-DESC-MOT-CAMBIO-CN
    private String p56DescMotCambioCn = DefaultValues.stringVal(Len.P56_DESC_MOT_CAMBIO_CN);
    //Original name: P56-COD-SOGG
    private String p56CodSogg = DefaultValues.stringVal(Len.P56_COD_SOGG);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.QUEST_ADEG_VER;
    }

    @Override
    public void deserialize(byte[] buf) {
        setQuestAdegVerBytes(buf);
    }

    public void setQuestAdegVerBytes(byte[] buffer) {
        setQuestAdegVerBytes(buffer, 1);
    }

    public byte[] getQuestAdegVerBytes() {
        byte[] buffer = new byte[Len.QUEST_ADEG_VER];
        return getQuestAdegVerBytes(buffer, 1);
    }

    public void setQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        p56IdQuestAdegVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P56_ID_QUEST_ADEG_VER, 0);
        position += Len.P56_ID_QUEST_ADEG_VER;
        p56CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P56_COD_COMP_ANIA, 0);
        position += Len.P56_COD_COMP_ANIA;
        p56IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P56_ID_MOVI_CRZ, 0);
        position += Len.P56_ID_MOVI_CRZ;
        p56IdRappAna.setP56IdRappAnaFromBuffer(buffer, position);
        position += P56IdRappAna.Len.P56_ID_RAPP_ANA;
        p56IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P56_ID_POLI, 0);
        position += Len.P56_ID_POLI;
        p56NaturaOprz = MarshalByte.readString(buffer, position, Len.P56_NATURA_OPRZ);
        position += Len.P56_NATURA_OPRZ;
        p56OrgnFnd = MarshalByte.readString(buffer, position, Len.P56_ORGN_FND);
        position += Len.P56_ORGN_FND;
        p56CodQlfcProf = MarshalByte.readString(buffer, position, Len.P56_COD_QLFC_PROF);
        position += Len.P56_COD_QLFC_PROF;
        p56CodNazQlfcProf = MarshalByte.readString(buffer, position, Len.P56_COD_NAZ_QLFC_PROF);
        position += Len.P56_COD_NAZ_QLFC_PROF;
        p56CodPrvQlfcProf = MarshalByte.readString(buffer, position, Len.P56_COD_PRV_QLFC_PROF);
        position += Len.P56_COD_PRV_QLFC_PROF;
        p56FntRedd = MarshalByte.readString(buffer, position, Len.P56_FNT_REDD);
        position += Len.P56_FNT_REDD;
        p56ReddFattAnnu.setP56ReddFattAnnuFromBuffer(buffer, position);
        position += P56ReddFattAnnu.Len.P56_REDD_FATT_ANNU;
        p56CodAteco = MarshalByte.readString(buffer, position, Len.P56_COD_ATECO);
        position += Len.P56_COD_ATECO;
        p56ValutColl = MarshalByte.readString(buffer, position, Len.P56_VALUT_COLL);
        position += Len.P56_VALUT_COLL;
        p56DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P56_DS_VER, 0);
        position += Len.P56_DS_VER;
        p56DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P56_DS_TS_CPTZ, 0);
        position += Len.P56_DS_TS_CPTZ;
        p56DsUtente = MarshalByte.readString(buffer, position, Len.P56_DS_UTENTE);
        position += Len.P56_DS_UTENTE;
        p56DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setP56LuogoCostituzioneVcharBytes(buffer, position);
        position += Len.P56_LUOGO_COSTITUZIONE_VCHAR;
        p56TpMovi.setP56TpMoviFromBuffer(buffer, position);
        position += P56TpMovi.Len.P56_TP_MOVI;
        p56FlRagRapp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPrszTitEff = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpMotRisc = MarshalByte.readString(buffer, position, Len.P56_TP_MOT_RISC);
        position += Len.P56_TP_MOT_RISC;
        p56TpPntVnd = MarshalByte.readString(buffer, position, Len.P56_TP_PNT_VND);
        position += Len.P56_TP_PNT_VND;
        p56TpAdegVer = MarshalByte.readString(buffer, position, Len.P56_TP_ADEG_VER);
        position += Len.P56_TP_ADEG_VER;
        p56TpRelaEsec = MarshalByte.readString(buffer, position, Len.P56_TP_RELA_ESEC);
        position += Len.P56_TP_RELA_ESEC;
        p56TpScoFinRapp = MarshalByte.readString(buffer, position, Len.P56_TP_SCO_FIN_RAPP);
        position += Len.P56_TP_SCO_FIN_RAPP;
        p56FlPrsz3oPagat = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56AreaGeoProvFnd = MarshalByte.readString(buffer, position, Len.P56_AREA_GEO_PROV_FND);
        position += Len.P56_AREA_GEO_PROV_FND;
        p56TpDestFnd = MarshalByte.readString(buffer, position, Len.P56_TP_DEST_FND);
        position += Len.P56_TP_DEST_FND;
        p56FlPaeseResidAut = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPaeseCitAut = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPaeseNazAut = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56CodProfPrec = MarshalByte.readString(buffer, position, Len.P56_COD_PROF_PREC);
        position += Len.P56_COD_PROF_PREC;
        p56FlAutPep = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlImpCarPub = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlLisTerrSorv = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpSitFinPat = MarshalByte.readString(buffer, position, Len.P56_TP_SIT_FIN_PAT);
        position += Len.P56_TP_SIT_FIN_PAT;
        p56TpSitFinPatCon = MarshalByte.readString(buffer, position, Len.P56_TP_SIT_FIN_PAT_CON);
        position += Len.P56_TP_SIT_FIN_PAT_CON;
        p56ImpTotAffUtil.setP56ImpTotAffUtilFromBuffer(buffer, position);
        position += P56ImpTotAffUtil.Len.P56_IMP_TOT_AFF_UTIL;
        p56ImpTotFinUtil.setP56ImpTotFinUtilFromBuffer(buffer, position);
        position += P56ImpTotFinUtil.Len.P56_IMP_TOT_FIN_UTIL;
        p56ImpTotAffAcc.setP56ImpTotAffAccFromBuffer(buffer, position);
        position += P56ImpTotAffAcc.Len.P56_IMP_TOT_AFF_ACC;
        p56ImpTotFinAcc.setP56ImpTotFinAccFromBuffer(buffer, position);
        position += P56ImpTotFinAcc.Len.P56_IMP_TOT_FIN_ACC;
        p56TpFrmGiurSav = MarshalByte.readString(buffer, position, Len.P56_TP_FRM_GIUR_SAV);
        position += Len.P56_TP_FRM_GIUR_SAV;
        p56RegCollPoli = MarshalByte.readString(buffer, position, Len.P56_REG_COLL_POLI);
        position += Len.P56_REG_COLL_POLI;
        p56NumTel = MarshalByte.readString(buffer, position, Len.P56_NUM_TEL);
        position += Len.P56_NUM_TEL;
        p56NumDip.setP56NumDipFromBuffer(buffer, position);
        position += P56NumDip.Len.P56_NUM_DIP;
        p56TpSitFamConv = MarshalByte.readString(buffer, position, Len.P56_TP_SIT_FAM_CONV);
        position += Len.P56_TP_SIT_FAM_CONV;
        p56CodProfCon = MarshalByte.readString(buffer, position, Len.P56_COD_PROF_CON);
        position += Len.P56_COD_PROF_CON;
        p56FlEsProcPen = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpCondCliente = MarshalByte.readString(buffer, position, Len.P56_TP_COND_CLIENTE);
        position += Len.P56_TP_COND_CLIENTE;
        p56CodSae = MarshalByte.readString(buffer, position, Len.P56_COD_SAE);
        position += Len.P56_COD_SAE;
        p56TpOperEstero = MarshalByte.readString(buffer, position, Len.P56_TP_OPER_ESTERO);
        position += Len.P56_TP_OPER_ESTERO;
        p56StatOperEstero = MarshalByte.readString(buffer, position, Len.P56_STAT_OPER_ESTERO);
        position += Len.P56_STAT_OPER_ESTERO;
        p56CodPrvSvolAtt = MarshalByte.readString(buffer, position, Len.P56_COD_PRV_SVOL_ATT);
        position += Len.P56_COD_PRV_SVOL_ATT;
        p56CodStatSvolAtt = MarshalByte.readString(buffer, position, Len.P56_COD_STAT_SVOL_ATT);
        position += Len.P56_COD_STAT_SVOL_ATT;
        p56TpSoc = MarshalByte.readString(buffer, position, Len.P56_TP_SOC);
        position += Len.P56_TP_SOC;
        p56FlIndSocQuot = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpSitGiur = MarshalByte.readString(buffer, position, Len.P56_TP_SIT_GIUR);
        position += Len.P56_TP_SIT_GIUR;
        p56PcQuoDetTitEff.setP56PcQuoDetTitEffFromBuffer(buffer, position);
        position += P56PcQuoDetTitEff.Len.P56_PC_QUO_DET_TIT_EFF;
        p56TpPrflRshPep = MarshalByte.readString(buffer, position, Len.P56_TP_PRFL_RSH_PEP);
        position += Len.P56_TP_PRFL_RSH_PEP;
        p56TpPep = MarshalByte.readString(buffer, position, Len.P56_TP_PEP);
        position += Len.P56_TP_PEP;
        p56FlNotPreg = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56DtIniFntRedd.setP56DtIniFntReddFromBuffer(buffer, position);
        position += P56DtIniFntRedd.Len.P56_DT_INI_FNT_REDD;
        p56FntRedd2 = MarshalByte.readString(buffer, position, Len.P56_FNT_REDD2);
        position += Len.P56_FNT_REDD2;
        p56DtIniFntRedd2.setP56DtIniFntRedd2FromBuffer(buffer, position);
        position += P56DtIniFntRedd2.Len.P56_DT_INI_FNT_REDD2;
        p56FntRedd3 = MarshalByte.readString(buffer, position, Len.P56_FNT_REDD3);
        position += Len.P56_FNT_REDD3;
        p56DtIniFntRedd3.setP56DtIniFntRedd3FromBuffer(buffer, position);
        position += P56DtIniFntRedd3.Len.P56_DT_INI_FNT_REDD3;
        setP56MotAssTitEffVcharBytes(buffer, position);
        position += Len.P56_MOT_ASS_TIT_EFF_VCHAR;
        setP56FinCostituzioneVcharBytes(buffer, position);
        position += Len.P56_FIN_COSTITUZIONE_VCHAR;
        setP56DescImpCarPubVcharBytes(buffer, position);
        position += Len.P56_DESC_IMP_CAR_PUB_VCHAR;
        setP56DescScoFinRappVcharBytes(buffer, position);
        position += Len.P56_DESC_SCO_FIN_RAPP_VCHAR;
        setP56DescProcPnlVcharBytes(buffer, position);
        position += Len.P56_DESC_PROC_PNL_VCHAR;
        setP56DescNotPregVcharBytes(buffer, position);
        position += Len.P56_DESC_NOT_PREG_VCHAR;
        p56IdAssicurati.setP56IdAssicuratiFromBuffer(buffer, position);
        position += P56IdAssicurati.Len.P56_ID_ASSICURATI;
        p56ReddCon.setP56ReddConFromBuffer(buffer, position);
        position += P56ReddCon.Len.P56_REDD_CON;
        setP56DescLibMotRiscVcharBytes(buffer, position);
        position += Len.P56_DESC_LIB_MOT_RISC_VCHAR;
        p56TpMotAssTitEff = MarshalByte.readString(buffer, position, Len.P56_TP_MOT_ASS_TIT_EFF);
        position += Len.P56_TP_MOT_ASS_TIT_EFF;
        p56TpRagRapp = MarshalByte.readString(buffer, position, Len.P56_TP_RAG_RAPP);
        position += Len.P56_TP_RAG_RAPP;
        p56CodCan.setP56CodCanFromBuffer(buffer, position);
        position += P56CodCan.Len.P56_COD_CAN;
        p56TpFinCost = MarshalByte.readString(buffer, position, Len.P56_TP_FIN_COST);
        position += Len.P56_TP_FIN_COST;
        p56NazDestFnd = MarshalByte.readString(buffer, position, Len.P56_NAZ_DEST_FND);
        position += Len.P56_NAZ_DEST_FND;
        p56FlAuFatcaAeoi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpCarFinGiur = MarshalByte.readString(buffer, position, Len.P56_TP_CAR_FIN_GIUR);
        position += Len.P56_TP_CAR_FIN_GIUR;
        p56TpCarFinGiurAt = MarshalByte.readString(buffer, position, Len.P56_TP_CAR_FIN_GIUR_AT);
        position += Len.P56_TP_CAR_FIN_GIUR_AT;
        p56TpCarFinGiurPa = MarshalByte.readString(buffer, position, Len.P56_TP_CAR_FIN_GIUR_PA);
        position += Len.P56_TP_CAR_FIN_GIUR_PA;
        p56FlIstituzFin = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpOriFndTitEff = MarshalByte.readString(buffer, position, Len.P56_TP_ORI_FND_TIT_EFF);
        position += Len.P56_TP_ORI_FND_TIT_EFF;
        p56PcEspAgPaMsc.setP56PcEspAgPaMscFromBuffer(buffer, position);
        position += P56PcEspAgPaMsc.Len.P56_PC_ESP_AG_PA_MSC;
        p56FlPrTrUsa = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPrTrNoUsa = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56PcRipPatAsVita.setP56PcRipPatAsVitaFromBuffer(buffer, position);
        position += P56PcRipPatAsVita.Len.P56_PC_RIP_PAT_AS_VITA;
        p56PcRipPatIm.setP56PcRipPatImFromBuffer(buffer, position);
        position += P56PcRipPatIm.Len.P56_PC_RIP_PAT_IM;
        p56PcRipPatSetIm.setP56PcRipPatSetImFromBuffer(buffer, position);
        position += P56PcRipPatSetIm.Len.P56_PC_RIP_PAT_SET_IM;
        p56TpStatusAeoi = MarshalByte.readString(buffer, position, Len.P56_TP_STATUS_AEOI);
        position += Len.P56_TP_STATUS_AEOI;
        p56TpStatusFatca = MarshalByte.readString(buffer, position, Len.P56_TP_STATUS_FATCA);
        position += Len.P56_TP_STATUS_FATCA;
        p56FlRappPaMsc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56CodComunSvolAtt = MarshalByte.readString(buffer, position, Len.P56_COD_COMUN_SVOL_ATT);
        position += Len.P56_COD_COMUN_SVOL_ATT;
        p56TpDt1oConCli = MarshalByte.readString(buffer, position, Len.P56_TP_DT1O_CON_CLI);
        position += Len.P56_TP_DT1O_CON_CLI;
        p56TpModEnRelaInt = MarshalByte.readString(buffer, position, Len.P56_TP_MOD_EN_RELA_INT);
        position += Len.P56_TP_MOD_EN_RELA_INT;
        p56TpReddAnnuLrd = MarshalByte.readString(buffer, position, Len.P56_TP_REDD_ANNU_LRD);
        position += Len.P56_TP_REDD_ANNU_LRD;
        p56TpReddCon = MarshalByte.readString(buffer, position, Len.P56_TP_REDD_CON);
        position += Len.P56_TP_REDD_CON;
        p56TpOperSocFid = MarshalByte.readString(buffer, position, Len.P56_TP_OPER_SOC_FID);
        position += Len.P56_TP_OPER_SOC_FID;
        p56CodPaEspMsc1 = MarshalByte.readString(buffer, position, Len.P56_COD_PA_ESP_MSC1);
        position += Len.P56_COD_PA_ESP_MSC1;
        p56ImpPaEspMsc1.setP56ImpPaEspMsc1FromBuffer(buffer, position);
        position += P56ImpPaEspMsc1.Len.P56_IMP_PA_ESP_MSC1;
        p56CodPaEspMsc2 = MarshalByte.readString(buffer, position, Len.P56_COD_PA_ESP_MSC2);
        position += Len.P56_COD_PA_ESP_MSC2;
        p56ImpPaEspMsc2.setP56ImpPaEspMsc2FromBuffer(buffer, position);
        position += P56ImpPaEspMsc2.Len.P56_IMP_PA_ESP_MSC2;
        p56CodPaEspMsc3 = MarshalByte.readString(buffer, position, Len.P56_COD_PA_ESP_MSC3);
        position += Len.P56_COD_PA_ESP_MSC3;
        p56ImpPaEspMsc3.setP56ImpPaEspMsc3FromBuffer(buffer, position);
        position += P56ImpPaEspMsc3.Len.P56_IMP_PA_ESP_MSC3;
        p56CodPaEspMsc4 = MarshalByte.readString(buffer, position, Len.P56_COD_PA_ESP_MSC4);
        position += Len.P56_COD_PA_ESP_MSC4;
        p56ImpPaEspMsc4.setP56ImpPaEspMsc4FromBuffer(buffer, position);
        position += P56ImpPaEspMsc4.Len.P56_IMP_PA_ESP_MSC4;
        p56CodPaEspMsc5 = MarshalByte.readString(buffer, position, Len.P56_COD_PA_ESP_MSC5);
        position += Len.P56_COD_PA_ESP_MSC5;
        p56ImpPaEspMsc5.setP56ImpPaEspMsc5FromBuffer(buffer, position);
        position += P56ImpPaEspMsc5.Len.P56_IMP_PA_ESP_MSC5;
        setP56DescOrgnFndVcharBytes(buffer, position);
        position += Len.P56_DESC_ORGN_FND_VCHAR;
        p56CodAutDueDil = MarshalByte.readString(buffer, position, Len.P56_COD_AUT_DUE_DIL);
        position += Len.P56_COD_AUT_DUE_DIL;
        p56FlPrQuestFatca = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPrQuestAeoi = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPrQuestOfac = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPrQuestKyc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56FlPrQuestMscq = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpNotPreg = MarshalByte.readString(buffer, position, Len.P56_TP_NOT_PREG);
        position += Len.P56_TP_NOT_PREG;
        p56TpProcPnl = MarshalByte.readString(buffer, position, Len.P56_TP_PROC_PNL);
        position += Len.P56_TP_PROC_PNL;
        p56CodImpCarPub = MarshalByte.readString(buffer, position, Len.P56_COD_IMP_CAR_PUB);
        position += Len.P56_COD_IMP_CAR_PUB;
        p56OprzSospette = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56UltFattAnnu.setP56UltFattAnnuFromBuffer(buffer, position);
        position += P56UltFattAnnu.Len.P56_ULT_FATT_ANNU;
        setP56DescPepVcharBytes(buffer, position);
        position += Len.P56_DESC_PEP_VCHAR;
        p56NumTel2 = MarshalByte.readString(buffer, position, Len.P56_NUM_TEL2);
        position += Len.P56_NUM_TEL2;
        p56ImpAfi.setP56ImpAfiFromBuffer(buffer, position);
        position += P56ImpAfi.Len.P56_IMP_AFI;
        p56FlNewPro = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p56TpMotCambioCntr = MarshalByte.readString(buffer, position, Len.P56_TP_MOT_CAMBIO_CNTR);
        position += Len.P56_TP_MOT_CAMBIO_CNTR;
        setP56DescMotCambioCnVcharBytes(buffer, position);
        position += Len.P56_DESC_MOT_CAMBIO_CN_VCHAR;
        p56CodSogg = MarshalByte.readString(buffer, position, Len.P56_COD_SOGG);
    }

    public byte[] getQuestAdegVerBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p56IdQuestAdegVer, Len.Int.P56_ID_QUEST_ADEG_VER, 0);
        position += Len.P56_ID_QUEST_ADEG_VER;
        MarshalByte.writeIntAsPacked(buffer, position, p56CodCompAnia, Len.Int.P56_COD_COMP_ANIA, 0);
        position += Len.P56_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, p56IdMoviCrz, Len.Int.P56_ID_MOVI_CRZ, 0);
        position += Len.P56_ID_MOVI_CRZ;
        p56IdRappAna.getP56IdRappAnaAsBuffer(buffer, position);
        position += P56IdRappAna.Len.P56_ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, p56IdPoli, Len.Int.P56_ID_POLI, 0);
        position += Len.P56_ID_POLI;
        MarshalByte.writeString(buffer, position, p56NaturaOprz, Len.P56_NATURA_OPRZ);
        position += Len.P56_NATURA_OPRZ;
        MarshalByte.writeString(buffer, position, p56OrgnFnd, Len.P56_ORGN_FND);
        position += Len.P56_ORGN_FND;
        MarshalByte.writeString(buffer, position, p56CodQlfcProf, Len.P56_COD_QLFC_PROF);
        position += Len.P56_COD_QLFC_PROF;
        MarshalByte.writeString(buffer, position, p56CodNazQlfcProf, Len.P56_COD_NAZ_QLFC_PROF);
        position += Len.P56_COD_NAZ_QLFC_PROF;
        MarshalByte.writeString(buffer, position, p56CodPrvQlfcProf, Len.P56_COD_PRV_QLFC_PROF);
        position += Len.P56_COD_PRV_QLFC_PROF;
        MarshalByte.writeString(buffer, position, p56FntRedd, Len.P56_FNT_REDD);
        position += Len.P56_FNT_REDD;
        p56ReddFattAnnu.getP56ReddFattAnnuAsBuffer(buffer, position);
        position += P56ReddFattAnnu.Len.P56_REDD_FATT_ANNU;
        MarshalByte.writeString(buffer, position, p56CodAteco, Len.P56_COD_ATECO);
        position += Len.P56_COD_ATECO;
        MarshalByte.writeString(buffer, position, p56ValutColl, Len.P56_VALUT_COLL);
        position += Len.P56_VALUT_COLL;
        MarshalByte.writeChar(buffer, position, p56DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p56DsVer, Len.Int.P56_DS_VER, 0);
        position += Len.P56_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p56DsTsCptz, Len.Int.P56_DS_TS_CPTZ, 0);
        position += Len.P56_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, p56DsUtente, Len.P56_DS_UTENTE);
        position += Len.P56_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p56DsStatoElab);
        position += Types.CHAR_SIZE;
        getP56LuogoCostituzioneVcharBytes(buffer, position);
        position += Len.P56_LUOGO_COSTITUZIONE_VCHAR;
        p56TpMovi.getP56TpMoviAsBuffer(buffer, position);
        position += P56TpMovi.Len.P56_TP_MOVI;
        MarshalByte.writeChar(buffer, position, p56FlRagRapp);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPrszTitEff);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpMotRisc, Len.P56_TP_MOT_RISC);
        position += Len.P56_TP_MOT_RISC;
        MarshalByte.writeString(buffer, position, p56TpPntVnd, Len.P56_TP_PNT_VND);
        position += Len.P56_TP_PNT_VND;
        MarshalByte.writeString(buffer, position, p56TpAdegVer, Len.P56_TP_ADEG_VER);
        position += Len.P56_TP_ADEG_VER;
        MarshalByte.writeString(buffer, position, p56TpRelaEsec, Len.P56_TP_RELA_ESEC);
        position += Len.P56_TP_RELA_ESEC;
        MarshalByte.writeString(buffer, position, p56TpScoFinRapp, Len.P56_TP_SCO_FIN_RAPP);
        position += Len.P56_TP_SCO_FIN_RAPP;
        MarshalByte.writeChar(buffer, position, p56FlPrsz3oPagat);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56AreaGeoProvFnd, Len.P56_AREA_GEO_PROV_FND);
        position += Len.P56_AREA_GEO_PROV_FND;
        MarshalByte.writeString(buffer, position, p56TpDestFnd, Len.P56_TP_DEST_FND);
        position += Len.P56_TP_DEST_FND;
        MarshalByte.writeChar(buffer, position, p56FlPaeseResidAut);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPaeseCitAut);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPaeseNazAut);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56CodProfPrec, Len.P56_COD_PROF_PREC);
        position += Len.P56_COD_PROF_PREC;
        MarshalByte.writeChar(buffer, position, p56FlAutPep);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlImpCarPub);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlLisTerrSorv);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpSitFinPat, Len.P56_TP_SIT_FIN_PAT);
        position += Len.P56_TP_SIT_FIN_PAT;
        MarshalByte.writeString(buffer, position, p56TpSitFinPatCon, Len.P56_TP_SIT_FIN_PAT_CON);
        position += Len.P56_TP_SIT_FIN_PAT_CON;
        p56ImpTotAffUtil.getP56ImpTotAffUtilAsBuffer(buffer, position);
        position += P56ImpTotAffUtil.Len.P56_IMP_TOT_AFF_UTIL;
        p56ImpTotFinUtil.getP56ImpTotFinUtilAsBuffer(buffer, position);
        position += P56ImpTotFinUtil.Len.P56_IMP_TOT_FIN_UTIL;
        p56ImpTotAffAcc.getP56ImpTotAffAccAsBuffer(buffer, position);
        position += P56ImpTotAffAcc.Len.P56_IMP_TOT_AFF_ACC;
        p56ImpTotFinAcc.getP56ImpTotFinAccAsBuffer(buffer, position);
        position += P56ImpTotFinAcc.Len.P56_IMP_TOT_FIN_ACC;
        MarshalByte.writeString(buffer, position, p56TpFrmGiurSav, Len.P56_TP_FRM_GIUR_SAV);
        position += Len.P56_TP_FRM_GIUR_SAV;
        MarshalByte.writeString(buffer, position, p56RegCollPoli, Len.P56_REG_COLL_POLI);
        position += Len.P56_REG_COLL_POLI;
        MarshalByte.writeString(buffer, position, p56NumTel, Len.P56_NUM_TEL);
        position += Len.P56_NUM_TEL;
        p56NumDip.getP56NumDipAsBuffer(buffer, position);
        position += P56NumDip.Len.P56_NUM_DIP;
        MarshalByte.writeString(buffer, position, p56TpSitFamConv, Len.P56_TP_SIT_FAM_CONV);
        position += Len.P56_TP_SIT_FAM_CONV;
        MarshalByte.writeString(buffer, position, p56CodProfCon, Len.P56_COD_PROF_CON);
        position += Len.P56_COD_PROF_CON;
        MarshalByte.writeChar(buffer, position, p56FlEsProcPen);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpCondCliente, Len.P56_TP_COND_CLIENTE);
        position += Len.P56_TP_COND_CLIENTE;
        MarshalByte.writeString(buffer, position, p56CodSae, Len.P56_COD_SAE);
        position += Len.P56_COD_SAE;
        MarshalByte.writeString(buffer, position, p56TpOperEstero, Len.P56_TP_OPER_ESTERO);
        position += Len.P56_TP_OPER_ESTERO;
        MarshalByte.writeString(buffer, position, p56StatOperEstero, Len.P56_STAT_OPER_ESTERO);
        position += Len.P56_STAT_OPER_ESTERO;
        MarshalByte.writeString(buffer, position, p56CodPrvSvolAtt, Len.P56_COD_PRV_SVOL_ATT);
        position += Len.P56_COD_PRV_SVOL_ATT;
        MarshalByte.writeString(buffer, position, p56CodStatSvolAtt, Len.P56_COD_STAT_SVOL_ATT);
        position += Len.P56_COD_STAT_SVOL_ATT;
        MarshalByte.writeString(buffer, position, p56TpSoc, Len.P56_TP_SOC);
        position += Len.P56_TP_SOC;
        MarshalByte.writeChar(buffer, position, p56FlIndSocQuot);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpSitGiur, Len.P56_TP_SIT_GIUR);
        position += Len.P56_TP_SIT_GIUR;
        p56PcQuoDetTitEff.getP56PcQuoDetTitEffAsBuffer(buffer, position);
        position += P56PcQuoDetTitEff.Len.P56_PC_QUO_DET_TIT_EFF;
        MarshalByte.writeString(buffer, position, p56TpPrflRshPep, Len.P56_TP_PRFL_RSH_PEP);
        position += Len.P56_TP_PRFL_RSH_PEP;
        MarshalByte.writeString(buffer, position, p56TpPep, Len.P56_TP_PEP);
        position += Len.P56_TP_PEP;
        MarshalByte.writeChar(buffer, position, p56FlNotPreg);
        position += Types.CHAR_SIZE;
        p56DtIniFntRedd.getP56DtIniFntReddAsBuffer(buffer, position);
        position += P56DtIniFntRedd.Len.P56_DT_INI_FNT_REDD;
        MarshalByte.writeString(buffer, position, p56FntRedd2, Len.P56_FNT_REDD2);
        position += Len.P56_FNT_REDD2;
        p56DtIniFntRedd2.getP56DtIniFntRedd2AsBuffer(buffer, position);
        position += P56DtIniFntRedd2.Len.P56_DT_INI_FNT_REDD2;
        MarshalByte.writeString(buffer, position, p56FntRedd3, Len.P56_FNT_REDD3);
        position += Len.P56_FNT_REDD3;
        p56DtIniFntRedd3.getP56DtIniFntRedd3AsBuffer(buffer, position);
        position += P56DtIniFntRedd3.Len.P56_DT_INI_FNT_REDD3;
        getP56MotAssTitEffVcharBytes(buffer, position);
        position += Len.P56_MOT_ASS_TIT_EFF_VCHAR;
        getP56FinCostituzioneVcharBytes(buffer, position);
        position += Len.P56_FIN_COSTITUZIONE_VCHAR;
        getP56DescImpCarPubVcharBytes(buffer, position);
        position += Len.P56_DESC_IMP_CAR_PUB_VCHAR;
        getP56DescScoFinRappVcharBytes(buffer, position);
        position += Len.P56_DESC_SCO_FIN_RAPP_VCHAR;
        getP56DescProcPnlVcharBytes(buffer, position);
        position += Len.P56_DESC_PROC_PNL_VCHAR;
        getP56DescNotPregVcharBytes(buffer, position);
        position += Len.P56_DESC_NOT_PREG_VCHAR;
        p56IdAssicurati.getP56IdAssicuratiAsBuffer(buffer, position);
        position += P56IdAssicurati.Len.P56_ID_ASSICURATI;
        p56ReddCon.getP56ReddConAsBuffer(buffer, position);
        position += P56ReddCon.Len.P56_REDD_CON;
        getP56DescLibMotRiscVcharBytes(buffer, position);
        position += Len.P56_DESC_LIB_MOT_RISC_VCHAR;
        MarshalByte.writeString(buffer, position, p56TpMotAssTitEff, Len.P56_TP_MOT_ASS_TIT_EFF);
        position += Len.P56_TP_MOT_ASS_TIT_EFF;
        MarshalByte.writeString(buffer, position, p56TpRagRapp, Len.P56_TP_RAG_RAPP);
        position += Len.P56_TP_RAG_RAPP;
        p56CodCan.getP56CodCanAsBuffer(buffer, position);
        position += P56CodCan.Len.P56_COD_CAN;
        MarshalByte.writeString(buffer, position, p56TpFinCost, Len.P56_TP_FIN_COST);
        position += Len.P56_TP_FIN_COST;
        MarshalByte.writeString(buffer, position, p56NazDestFnd, Len.P56_NAZ_DEST_FND);
        position += Len.P56_NAZ_DEST_FND;
        MarshalByte.writeChar(buffer, position, p56FlAuFatcaAeoi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpCarFinGiur, Len.P56_TP_CAR_FIN_GIUR);
        position += Len.P56_TP_CAR_FIN_GIUR;
        MarshalByte.writeString(buffer, position, p56TpCarFinGiurAt, Len.P56_TP_CAR_FIN_GIUR_AT);
        position += Len.P56_TP_CAR_FIN_GIUR_AT;
        MarshalByte.writeString(buffer, position, p56TpCarFinGiurPa, Len.P56_TP_CAR_FIN_GIUR_PA);
        position += Len.P56_TP_CAR_FIN_GIUR_PA;
        MarshalByte.writeChar(buffer, position, p56FlIstituzFin);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpOriFndTitEff, Len.P56_TP_ORI_FND_TIT_EFF);
        position += Len.P56_TP_ORI_FND_TIT_EFF;
        p56PcEspAgPaMsc.getP56PcEspAgPaMscAsBuffer(buffer, position);
        position += P56PcEspAgPaMsc.Len.P56_PC_ESP_AG_PA_MSC;
        MarshalByte.writeChar(buffer, position, p56FlPrTrUsa);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPrTrNoUsa);
        position += Types.CHAR_SIZE;
        p56PcRipPatAsVita.getP56PcRipPatAsVitaAsBuffer(buffer, position);
        position += P56PcRipPatAsVita.Len.P56_PC_RIP_PAT_AS_VITA;
        p56PcRipPatIm.getP56PcRipPatImAsBuffer(buffer, position);
        position += P56PcRipPatIm.Len.P56_PC_RIP_PAT_IM;
        p56PcRipPatSetIm.getP56PcRipPatSetImAsBuffer(buffer, position);
        position += P56PcRipPatSetIm.Len.P56_PC_RIP_PAT_SET_IM;
        MarshalByte.writeString(buffer, position, p56TpStatusAeoi, Len.P56_TP_STATUS_AEOI);
        position += Len.P56_TP_STATUS_AEOI;
        MarshalByte.writeString(buffer, position, p56TpStatusFatca, Len.P56_TP_STATUS_FATCA);
        position += Len.P56_TP_STATUS_FATCA;
        MarshalByte.writeChar(buffer, position, p56FlRappPaMsc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56CodComunSvolAtt, Len.P56_COD_COMUN_SVOL_ATT);
        position += Len.P56_COD_COMUN_SVOL_ATT;
        MarshalByte.writeString(buffer, position, p56TpDt1oConCli, Len.P56_TP_DT1O_CON_CLI);
        position += Len.P56_TP_DT1O_CON_CLI;
        MarshalByte.writeString(buffer, position, p56TpModEnRelaInt, Len.P56_TP_MOD_EN_RELA_INT);
        position += Len.P56_TP_MOD_EN_RELA_INT;
        MarshalByte.writeString(buffer, position, p56TpReddAnnuLrd, Len.P56_TP_REDD_ANNU_LRD);
        position += Len.P56_TP_REDD_ANNU_LRD;
        MarshalByte.writeString(buffer, position, p56TpReddCon, Len.P56_TP_REDD_CON);
        position += Len.P56_TP_REDD_CON;
        MarshalByte.writeString(buffer, position, p56TpOperSocFid, Len.P56_TP_OPER_SOC_FID);
        position += Len.P56_TP_OPER_SOC_FID;
        MarshalByte.writeString(buffer, position, p56CodPaEspMsc1, Len.P56_COD_PA_ESP_MSC1);
        position += Len.P56_COD_PA_ESP_MSC1;
        p56ImpPaEspMsc1.getP56ImpPaEspMsc1AsBuffer(buffer, position);
        position += P56ImpPaEspMsc1.Len.P56_IMP_PA_ESP_MSC1;
        MarshalByte.writeString(buffer, position, p56CodPaEspMsc2, Len.P56_COD_PA_ESP_MSC2);
        position += Len.P56_COD_PA_ESP_MSC2;
        p56ImpPaEspMsc2.getP56ImpPaEspMsc2AsBuffer(buffer, position);
        position += P56ImpPaEspMsc2.Len.P56_IMP_PA_ESP_MSC2;
        MarshalByte.writeString(buffer, position, p56CodPaEspMsc3, Len.P56_COD_PA_ESP_MSC3);
        position += Len.P56_COD_PA_ESP_MSC3;
        p56ImpPaEspMsc3.getP56ImpPaEspMsc3AsBuffer(buffer, position);
        position += P56ImpPaEspMsc3.Len.P56_IMP_PA_ESP_MSC3;
        MarshalByte.writeString(buffer, position, p56CodPaEspMsc4, Len.P56_COD_PA_ESP_MSC4);
        position += Len.P56_COD_PA_ESP_MSC4;
        p56ImpPaEspMsc4.getP56ImpPaEspMsc4AsBuffer(buffer, position);
        position += P56ImpPaEspMsc4.Len.P56_IMP_PA_ESP_MSC4;
        MarshalByte.writeString(buffer, position, p56CodPaEspMsc5, Len.P56_COD_PA_ESP_MSC5);
        position += Len.P56_COD_PA_ESP_MSC5;
        p56ImpPaEspMsc5.getP56ImpPaEspMsc5AsBuffer(buffer, position);
        position += P56ImpPaEspMsc5.Len.P56_IMP_PA_ESP_MSC5;
        getP56DescOrgnFndVcharBytes(buffer, position);
        position += Len.P56_DESC_ORGN_FND_VCHAR;
        MarshalByte.writeString(buffer, position, p56CodAutDueDil, Len.P56_COD_AUT_DUE_DIL);
        position += Len.P56_COD_AUT_DUE_DIL;
        MarshalByte.writeChar(buffer, position, p56FlPrQuestFatca);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPrQuestAeoi);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPrQuestOfac);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPrQuestKyc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, p56FlPrQuestMscq);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpNotPreg, Len.P56_TP_NOT_PREG);
        position += Len.P56_TP_NOT_PREG;
        MarshalByte.writeString(buffer, position, p56TpProcPnl, Len.P56_TP_PROC_PNL);
        position += Len.P56_TP_PROC_PNL;
        MarshalByte.writeString(buffer, position, p56CodImpCarPub, Len.P56_COD_IMP_CAR_PUB);
        position += Len.P56_COD_IMP_CAR_PUB;
        MarshalByte.writeChar(buffer, position, p56OprzSospette);
        position += Types.CHAR_SIZE;
        p56UltFattAnnu.getP56UltFattAnnuAsBuffer(buffer, position);
        position += P56UltFattAnnu.Len.P56_ULT_FATT_ANNU;
        getP56DescPepVcharBytes(buffer, position);
        position += Len.P56_DESC_PEP_VCHAR;
        MarshalByte.writeString(buffer, position, p56NumTel2, Len.P56_NUM_TEL2);
        position += Len.P56_NUM_TEL2;
        p56ImpAfi.getP56ImpAfiAsBuffer(buffer, position);
        position += P56ImpAfi.Len.P56_IMP_AFI;
        MarshalByte.writeChar(buffer, position, p56FlNewPro);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, p56TpMotCambioCntr, Len.P56_TP_MOT_CAMBIO_CNTR);
        position += Len.P56_TP_MOT_CAMBIO_CNTR;
        getP56DescMotCambioCnVcharBytes(buffer, position);
        position += Len.P56_DESC_MOT_CAMBIO_CN_VCHAR;
        MarshalByte.writeString(buffer, position, p56CodSogg, Len.P56_COD_SOGG);
        return buffer;
    }

    public void setP56IdQuestAdegVer(int p56IdQuestAdegVer) {
        this.p56IdQuestAdegVer = p56IdQuestAdegVer;
    }

    public int getP56IdQuestAdegVer() {
        return this.p56IdQuestAdegVer;
    }

    public void setP56CodCompAnia(int p56CodCompAnia) {
        this.p56CodCompAnia = p56CodCompAnia;
    }

    public int getP56CodCompAnia() {
        return this.p56CodCompAnia;
    }

    public void setP56IdMoviCrz(int p56IdMoviCrz) {
        this.p56IdMoviCrz = p56IdMoviCrz;
    }

    public int getP56IdMoviCrz() {
        return this.p56IdMoviCrz;
    }

    public void setP56IdPoli(int p56IdPoli) {
        this.p56IdPoli = p56IdPoli;
    }

    public int getP56IdPoli() {
        return this.p56IdPoli;
    }

    public void setP56NaturaOprz(String p56NaturaOprz) {
        this.p56NaturaOprz = Functions.subString(p56NaturaOprz, Len.P56_NATURA_OPRZ);
    }

    public String getP56NaturaOprz() {
        return this.p56NaturaOprz;
    }

    public String getP56NaturaOprzFormatted() {
        return Functions.padBlanks(getP56NaturaOprz(), Len.P56_NATURA_OPRZ);
    }

    public void setP56OrgnFnd(String p56OrgnFnd) {
        this.p56OrgnFnd = Functions.subString(p56OrgnFnd, Len.P56_ORGN_FND);
    }

    public String getP56OrgnFnd() {
        return this.p56OrgnFnd;
    }

    public String getP56OrgnFndFormatted() {
        return Functions.padBlanks(getP56OrgnFnd(), Len.P56_ORGN_FND);
    }

    public void setP56CodQlfcProf(String p56CodQlfcProf) {
        this.p56CodQlfcProf = Functions.subString(p56CodQlfcProf, Len.P56_COD_QLFC_PROF);
    }

    public String getP56CodQlfcProf() {
        return this.p56CodQlfcProf;
    }

    public String getP56CodQlfcProfFormatted() {
        return Functions.padBlanks(getP56CodQlfcProf(), Len.P56_COD_QLFC_PROF);
    }

    public void setP56CodNazQlfcProf(String p56CodNazQlfcProf) {
        this.p56CodNazQlfcProf = Functions.subString(p56CodNazQlfcProf, Len.P56_COD_NAZ_QLFC_PROF);
    }

    public String getP56CodNazQlfcProf() {
        return this.p56CodNazQlfcProf;
    }

    public String getP56CodNazQlfcProfFormatted() {
        return Functions.padBlanks(getP56CodNazQlfcProf(), Len.P56_COD_NAZ_QLFC_PROF);
    }

    public void setP56CodPrvQlfcProf(String p56CodPrvQlfcProf) {
        this.p56CodPrvQlfcProf = Functions.subString(p56CodPrvQlfcProf, Len.P56_COD_PRV_QLFC_PROF);
    }

    public String getP56CodPrvQlfcProf() {
        return this.p56CodPrvQlfcProf;
    }

    public String getP56CodPrvQlfcProfFormatted() {
        return Functions.padBlanks(getP56CodPrvQlfcProf(), Len.P56_COD_PRV_QLFC_PROF);
    }

    public void setP56FntRedd(String p56FntRedd) {
        this.p56FntRedd = Functions.subString(p56FntRedd, Len.P56_FNT_REDD);
    }

    public String getP56FntRedd() {
        return this.p56FntRedd;
    }

    public String getP56FntReddFormatted() {
        return Functions.padBlanks(getP56FntRedd(), Len.P56_FNT_REDD);
    }

    public void setP56CodAteco(String p56CodAteco) {
        this.p56CodAteco = Functions.subString(p56CodAteco, Len.P56_COD_ATECO);
    }

    public String getP56CodAteco() {
        return this.p56CodAteco;
    }

    public String getP56CodAtecoFormatted() {
        return Functions.padBlanks(getP56CodAteco(), Len.P56_COD_ATECO);
    }

    public void setP56ValutColl(String p56ValutColl) {
        this.p56ValutColl = Functions.subString(p56ValutColl, Len.P56_VALUT_COLL);
    }

    public String getP56ValutColl() {
        return this.p56ValutColl;
    }

    public String getP56ValutCollFormatted() {
        return Functions.padBlanks(getP56ValutColl(), Len.P56_VALUT_COLL);
    }

    public void setP56DsOperSql(char p56DsOperSql) {
        this.p56DsOperSql = p56DsOperSql;
    }

    public void setP56DsOperSqlFormatted(String p56DsOperSql) {
        setP56DsOperSql(Functions.charAt(p56DsOperSql, Types.CHAR_SIZE));
    }

    public char getP56DsOperSql() {
        return this.p56DsOperSql;
    }

    public void setP56DsVer(int p56DsVer) {
        this.p56DsVer = p56DsVer;
    }

    public int getP56DsVer() {
        return this.p56DsVer;
    }

    public void setP56DsTsCptz(long p56DsTsCptz) {
        this.p56DsTsCptz = p56DsTsCptz;
    }

    public long getP56DsTsCptz() {
        return this.p56DsTsCptz;
    }

    public void setP56DsUtente(String p56DsUtente) {
        this.p56DsUtente = Functions.subString(p56DsUtente, Len.P56_DS_UTENTE);
    }

    public String getP56DsUtente() {
        return this.p56DsUtente;
    }

    public void setP56DsStatoElab(char p56DsStatoElab) {
        this.p56DsStatoElab = p56DsStatoElab;
    }

    public void setP56DsStatoElabFormatted(String p56DsStatoElab) {
        setP56DsStatoElab(Functions.charAt(p56DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP56DsStatoElab() {
        return this.p56DsStatoElab;
    }

    public void setP56LuogoCostituzioneVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_LUOGO_COSTITUZIONE_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_LUOGO_COSTITUZIONE_VCHAR);
        setP56LuogoCostituzioneVcharBytes(buffer, 1);
    }

    public String getP56LuogoCostituzioneVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56LuogoCostituzioneVcharBytes());
    }

    /**Original name: P56-LUOGO-COSTITUZIONE-VCHAR<br>*/
    public byte[] getP56LuogoCostituzioneVcharBytes() {
        byte[] buffer = new byte[Len.P56_LUOGO_COSTITUZIONE_VCHAR];
        return getP56LuogoCostituzioneVcharBytes(buffer, 1);
    }

    public void setP56LuogoCostituzioneVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56LuogoCostituzioneLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56LuogoCostituzione = MarshalByte.readString(buffer, position, Len.P56_LUOGO_COSTITUZIONE);
    }

    public byte[] getP56LuogoCostituzioneVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56LuogoCostituzioneLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56LuogoCostituzione, Len.P56_LUOGO_COSTITUZIONE);
        return buffer;
    }

    public void setP56LuogoCostituzioneLen(short p56LuogoCostituzioneLen) {
        this.p56LuogoCostituzioneLen = p56LuogoCostituzioneLen;
    }

    public short getP56LuogoCostituzioneLen() {
        return this.p56LuogoCostituzioneLen;
    }

    public void setP56LuogoCostituzione(String p56LuogoCostituzione) {
        this.p56LuogoCostituzione = Functions.subString(p56LuogoCostituzione, Len.P56_LUOGO_COSTITUZIONE);
    }

    public String getP56LuogoCostituzione() {
        return this.p56LuogoCostituzione;
    }

    public void setP56FlRagRapp(char p56FlRagRapp) {
        this.p56FlRagRapp = p56FlRagRapp;
    }

    public char getP56FlRagRapp() {
        return this.p56FlRagRapp;
    }

    public void setP56FlPrszTitEff(char p56FlPrszTitEff) {
        this.p56FlPrszTitEff = p56FlPrszTitEff;
    }

    public char getP56FlPrszTitEff() {
        return this.p56FlPrszTitEff;
    }

    public void setP56TpMotRisc(String p56TpMotRisc) {
        this.p56TpMotRisc = Functions.subString(p56TpMotRisc, Len.P56_TP_MOT_RISC);
    }

    public String getP56TpMotRisc() {
        return this.p56TpMotRisc;
    }

    public String getP56TpMotRiscFormatted() {
        return Functions.padBlanks(getP56TpMotRisc(), Len.P56_TP_MOT_RISC);
    }

    public void setP56TpPntVnd(String p56TpPntVnd) {
        this.p56TpPntVnd = Functions.subString(p56TpPntVnd, Len.P56_TP_PNT_VND);
    }

    public String getP56TpPntVnd() {
        return this.p56TpPntVnd;
    }

    public String getP56TpPntVndFormatted() {
        return Functions.padBlanks(getP56TpPntVnd(), Len.P56_TP_PNT_VND);
    }

    public void setP56TpAdegVer(String p56TpAdegVer) {
        this.p56TpAdegVer = Functions.subString(p56TpAdegVer, Len.P56_TP_ADEG_VER);
    }

    public String getP56TpAdegVer() {
        return this.p56TpAdegVer;
    }

    public String getP56TpAdegVerFormatted() {
        return Functions.padBlanks(getP56TpAdegVer(), Len.P56_TP_ADEG_VER);
    }

    public void setP56TpRelaEsec(String p56TpRelaEsec) {
        this.p56TpRelaEsec = Functions.subString(p56TpRelaEsec, Len.P56_TP_RELA_ESEC);
    }

    public String getP56TpRelaEsec() {
        return this.p56TpRelaEsec;
    }

    public String getP56TpRelaEsecFormatted() {
        return Functions.padBlanks(getP56TpRelaEsec(), Len.P56_TP_RELA_ESEC);
    }

    public void setP56TpScoFinRapp(String p56TpScoFinRapp) {
        this.p56TpScoFinRapp = Functions.subString(p56TpScoFinRapp, Len.P56_TP_SCO_FIN_RAPP);
    }

    public String getP56TpScoFinRapp() {
        return this.p56TpScoFinRapp;
    }

    public String getP56TpScoFinRappFormatted() {
        return Functions.padBlanks(getP56TpScoFinRapp(), Len.P56_TP_SCO_FIN_RAPP);
    }

    public void setP56FlPrsz3oPagat(char p56FlPrsz3oPagat) {
        this.p56FlPrsz3oPagat = p56FlPrsz3oPagat;
    }

    public char getP56FlPrsz3oPagat() {
        return this.p56FlPrsz3oPagat;
    }

    public void setP56AreaGeoProvFnd(String p56AreaGeoProvFnd) {
        this.p56AreaGeoProvFnd = Functions.subString(p56AreaGeoProvFnd, Len.P56_AREA_GEO_PROV_FND);
    }

    public String getP56AreaGeoProvFnd() {
        return this.p56AreaGeoProvFnd;
    }

    public String getP56AreaGeoProvFndFormatted() {
        return Functions.padBlanks(getP56AreaGeoProvFnd(), Len.P56_AREA_GEO_PROV_FND);
    }

    public void setP56TpDestFnd(String p56TpDestFnd) {
        this.p56TpDestFnd = Functions.subString(p56TpDestFnd, Len.P56_TP_DEST_FND);
    }

    public String getP56TpDestFnd() {
        return this.p56TpDestFnd;
    }

    public String getP56TpDestFndFormatted() {
        return Functions.padBlanks(getP56TpDestFnd(), Len.P56_TP_DEST_FND);
    }

    public void setP56FlPaeseResidAut(char p56FlPaeseResidAut) {
        this.p56FlPaeseResidAut = p56FlPaeseResidAut;
    }

    public char getP56FlPaeseResidAut() {
        return this.p56FlPaeseResidAut;
    }

    public void setP56FlPaeseCitAut(char p56FlPaeseCitAut) {
        this.p56FlPaeseCitAut = p56FlPaeseCitAut;
    }

    public char getP56FlPaeseCitAut() {
        return this.p56FlPaeseCitAut;
    }

    public void setP56FlPaeseNazAut(char p56FlPaeseNazAut) {
        this.p56FlPaeseNazAut = p56FlPaeseNazAut;
    }

    public char getP56FlPaeseNazAut() {
        return this.p56FlPaeseNazAut;
    }

    public void setP56CodProfPrec(String p56CodProfPrec) {
        this.p56CodProfPrec = Functions.subString(p56CodProfPrec, Len.P56_COD_PROF_PREC);
    }

    public String getP56CodProfPrec() {
        return this.p56CodProfPrec;
    }

    public String getP56CodProfPrecFormatted() {
        return Functions.padBlanks(getP56CodProfPrec(), Len.P56_COD_PROF_PREC);
    }

    public void setP56FlAutPep(char p56FlAutPep) {
        this.p56FlAutPep = p56FlAutPep;
    }

    public char getP56FlAutPep() {
        return this.p56FlAutPep;
    }

    public void setP56FlImpCarPub(char p56FlImpCarPub) {
        this.p56FlImpCarPub = p56FlImpCarPub;
    }

    public char getP56FlImpCarPub() {
        return this.p56FlImpCarPub;
    }

    public void setP56FlLisTerrSorv(char p56FlLisTerrSorv) {
        this.p56FlLisTerrSorv = p56FlLisTerrSorv;
    }

    public char getP56FlLisTerrSorv() {
        return this.p56FlLisTerrSorv;
    }

    public void setP56TpSitFinPat(String p56TpSitFinPat) {
        this.p56TpSitFinPat = Functions.subString(p56TpSitFinPat, Len.P56_TP_SIT_FIN_PAT);
    }

    public String getP56TpSitFinPat() {
        return this.p56TpSitFinPat;
    }

    public String getP56TpSitFinPatFormatted() {
        return Functions.padBlanks(getP56TpSitFinPat(), Len.P56_TP_SIT_FIN_PAT);
    }

    public void setP56TpSitFinPatCon(String p56TpSitFinPatCon) {
        this.p56TpSitFinPatCon = Functions.subString(p56TpSitFinPatCon, Len.P56_TP_SIT_FIN_PAT_CON);
    }

    public String getP56TpSitFinPatCon() {
        return this.p56TpSitFinPatCon;
    }

    public String getP56TpSitFinPatConFormatted() {
        return Functions.padBlanks(getP56TpSitFinPatCon(), Len.P56_TP_SIT_FIN_PAT_CON);
    }

    public void setP56TpFrmGiurSav(String p56TpFrmGiurSav) {
        this.p56TpFrmGiurSav = Functions.subString(p56TpFrmGiurSav, Len.P56_TP_FRM_GIUR_SAV);
    }

    public String getP56TpFrmGiurSav() {
        return this.p56TpFrmGiurSav;
    }

    public String getP56TpFrmGiurSavFormatted() {
        return Functions.padBlanks(getP56TpFrmGiurSav(), Len.P56_TP_FRM_GIUR_SAV);
    }

    public void setP56RegCollPoli(String p56RegCollPoli) {
        this.p56RegCollPoli = Functions.subString(p56RegCollPoli, Len.P56_REG_COLL_POLI);
    }

    public String getP56RegCollPoli() {
        return this.p56RegCollPoli;
    }

    public String getP56RegCollPoliFormatted() {
        return Functions.padBlanks(getP56RegCollPoli(), Len.P56_REG_COLL_POLI);
    }

    public void setP56NumTel(String p56NumTel) {
        this.p56NumTel = Functions.subString(p56NumTel, Len.P56_NUM_TEL);
    }

    public String getP56NumTel() {
        return this.p56NumTel;
    }

    public String getP56NumTelFormatted() {
        return Functions.padBlanks(getP56NumTel(), Len.P56_NUM_TEL);
    }

    public void setP56TpSitFamConv(String p56TpSitFamConv) {
        this.p56TpSitFamConv = Functions.subString(p56TpSitFamConv, Len.P56_TP_SIT_FAM_CONV);
    }

    public String getP56TpSitFamConv() {
        return this.p56TpSitFamConv;
    }

    public String getP56TpSitFamConvFormatted() {
        return Functions.padBlanks(getP56TpSitFamConv(), Len.P56_TP_SIT_FAM_CONV);
    }

    public void setP56CodProfCon(String p56CodProfCon) {
        this.p56CodProfCon = Functions.subString(p56CodProfCon, Len.P56_COD_PROF_CON);
    }

    public String getP56CodProfCon() {
        return this.p56CodProfCon;
    }

    public String getP56CodProfConFormatted() {
        return Functions.padBlanks(getP56CodProfCon(), Len.P56_COD_PROF_CON);
    }

    public void setP56FlEsProcPen(char p56FlEsProcPen) {
        this.p56FlEsProcPen = p56FlEsProcPen;
    }

    public char getP56FlEsProcPen() {
        return this.p56FlEsProcPen;
    }

    public void setP56TpCondCliente(String p56TpCondCliente) {
        this.p56TpCondCliente = Functions.subString(p56TpCondCliente, Len.P56_TP_COND_CLIENTE);
    }

    public String getP56TpCondCliente() {
        return this.p56TpCondCliente;
    }

    public String getP56TpCondClienteFormatted() {
        return Functions.padBlanks(getP56TpCondCliente(), Len.P56_TP_COND_CLIENTE);
    }

    public void setP56CodSae(String p56CodSae) {
        this.p56CodSae = Functions.subString(p56CodSae, Len.P56_COD_SAE);
    }

    public String getP56CodSae() {
        return this.p56CodSae;
    }

    public String getP56CodSaeFormatted() {
        return Functions.padBlanks(getP56CodSae(), Len.P56_COD_SAE);
    }

    public void setP56TpOperEstero(String p56TpOperEstero) {
        this.p56TpOperEstero = Functions.subString(p56TpOperEstero, Len.P56_TP_OPER_ESTERO);
    }

    public String getP56TpOperEstero() {
        return this.p56TpOperEstero;
    }

    public String getP56TpOperEsteroFormatted() {
        return Functions.padBlanks(getP56TpOperEstero(), Len.P56_TP_OPER_ESTERO);
    }

    public void setP56StatOperEstero(String p56StatOperEstero) {
        this.p56StatOperEstero = Functions.subString(p56StatOperEstero, Len.P56_STAT_OPER_ESTERO);
    }

    public String getP56StatOperEstero() {
        return this.p56StatOperEstero;
    }

    public String getP56StatOperEsteroFormatted() {
        return Functions.padBlanks(getP56StatOperEstero(), Len.P56_STAT_OPER_ESTERO);
    }

    public void setP56CodPrvSvolAtt(String p56CodPrvSvolAtt) {
        this.p56CodPrvSvolAtt = Functions.subString(p56CodPrvSvolAtt, Len.P56_COD_PRV_SVOL_ATT);
    }

    public String getP56CodPrvSvolAtt() {
        return this.p56CodPrvSvolAtt;
    }

    public String getP56CodPrvSvolAttFormatted() {
        return Functions.padBlanks(getP56CodPrvSvolAtt(), Len.P56_COD_PRV_SVOL_ATT);
    }

    public void setP56CodStatSvolAtt(String p56CodStatSvolAtt) {
        this.p56CodStatSvolAtt = Functions.subString(p56CodStatSvolAtt, Len.P56_COD_STAT_SVOL_ATT);
    }

    public String getP56CodStatSvolAtt() {
        return this.p56CodStatSvolAtt;
    }

    public String getP56CodStatSvolAttFormatted() {
        return Functions.padBlanks(getP56CodStatSvolAtt(), Len.P56_COD_STAT_SVOL_ATT);
    }

    public void setP56TpSoc(String p56TpSoc) {
        this.p56TpSoc = Functions.subString(p56TpSoc, Len.P56_TP_SOC);
    }

    public String getP56TpSoc() {
        return this.p56TpSoc;
    }

    public String getP56TpSocFormatted() {
        return Functions.padBlanks(getP56TpSoc(), Len.P56_TP_SOC);
    }

    public void setP56FlIndSocQuot(char p56FlIndSocQuot) {
        this.p56FlIndSocQuot = p56FlIndSocQuot;
    }

    public char getP56FlIndSocQuot() {
        return this.p56FlIndSocQuot;
    }

    public void setP56TpSitGiur(String p56TpSitGiur) {
        this.p56TpSitGiur = Functions.subString(p56TpSitGiur, Len.P56_TP_SIT_GIUR);
    }

    public String getP56TpSitGiur() {
        return this.p56TpSitGiur;
    }

    public String getP56TpSitGiurFormatted() {
        return Functions.padBlanks(getP56TpSitGiur(), Len.P56_TP_SIT_GIUR);
    }

    public void setP56TpPrflRshPep(String p56TpPrflRshPep) {
        this.p56TpPrflRshPep = Functions.subString(p56TpPrflRshPep, Len.P56_TP_PRFL_RSH_PEP);
    }

    public String getP56TpPrflRshPep() {
        return this.p56TpPrflRshPep;
    }

    public String getP56TpPrflRshPepFormatted() {
        return Functions.padBlanks(getP56TpPrflRshPep(), Len.P56_TP_PRFL_RSH_PEP);
    }

    public void setP56TpPep(String p56TpPep) {
        this.p56TpPep = Functions.subString(p56TpPep, Len.P56_TP_PEP);
    }

    public String getP56TpPep() {
        return this.p56TpPep;
    }

    public String getP56TpPepFormatted() {
        return Functions.padBlanks(getP56TpPep(), Len.P56_TP_PEP);
    }

    public void setP56FlNotPreg(char p56FlNotPreg) {
        this.p56FlNotPreg = p56FlNotPreg;
    }

    public char getP56FlNotPreg() {
        return this.p56FlNotPreg;
    }

    public void setP56FntRedd2(String p56FntRedd2) {
        this.p56FntRedd2 = Functions.subString(p56FntRedd2, Len.P56_FNT_REDD2);
    }

    public String getP56FntRedd2() {
        return this.p56FntRedd2;
    }

    public String getP56FntRedd2Formatted() {
        return Functions.padBlanks(getP56FntRedd2(), Len.P56_FNT_REDD2);
    }

    public void setP56FntRedd3(String p56FntRedd3) {
        this.p56FntRedd3 = Functions.subString(p56FntRedd3, Len.P56_FNT_REDD3);
    }

    public String getP56FntRedd3() {
        return this.p56FntRedd3;
    }

    public String getP56FntRedd3Formatted() {
        return Functions.padBlanks(getP56FntRedd3(), Len.P56_FNT_REDD3);
    }

    public void setP56MotAssTitEffVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_MOT_ASS_TIT_EFF_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_MOT_ASS_TIT_EFF_VCHAR);
        setP56MotAssTitEffVcharBytes(buffer, 1);
    }

    public String getP56MotAssTitEffVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56MotAssTitEffVcharBytes());
    }

    /**Original name: P56-MOT-ASS-TIT-EFF-VCHAR<br>*/
    public byte[] getP56MotAssTitEffVcharBytes() {
        byte[] buffer = new byte[Len.P56_MOT_ASS_TIT_EFF_VCHAR];
        return getP56MotAssTitEffVcharBytes(buffer, 1);
    }

    public void setP56MotAssTitEffVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56MotAssTitEffLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56MotAssTitEff = MarshalByte.readString(buffer, position, Len.P56_MOT_ASS_TIT_EFF);
    }

    public byte[] getP56MotAssTitEffVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56MotAssTitEffLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56MotAssTitEff, Len.P56_MOT_ASS_TIT_EFF);
        return buffer;
    }

    public void setP56MotAssTitEffLen(short p56MotAssTitEffLen) {
        this.p56MotAssTitEffLen = p56MotAssTitEffLen;
    }

    public short getP56MotAssTitEffLen() {
        return this.p56MotAssTitEffLen;
    }

    public void setP56MotAssTitEff(String p56MotAssTitEff) {
        this.p56MotAssTitEff = Functions.subString(p56MotAssTitEff, Len.P56_MOT_ASS_TIT_EFF);
    }

    public String getP56MotAssTitEff() {
        return this.p56MotAssTitEff;
    }

    public void setP56FinCostituzioneVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_FIN_COSTITUZIONE_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_FIN_COSTITUZIONE_VCHAR);
        setP56FinCostituzioneVcharBytes(buffer, 1);
    }

    public String getP56FinCostituzioneVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56FinCostituzioneVcharBytes());
    }

    /**Original name: P56-FIN-COSTITUZIONE-VCHAR<br>*/
    public byte[] getP56FinCostituzioneVcharBytes() {
        byte[] buffer = new byte[Len.P56_FIN_COSTITUZIONE_VCHAR];
        return getP56FinCostituzioneVcharBytes(buffer, 1);
    }

    public void setP56FinCostituzioneVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56FinCostituzioneLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56FinCostituzione = MarshalByte.readString(buffer, position, Len.P56_FIN_COSTITUZIONE);
    }

    public byte[] getP56FinCostituzioneVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56FinCostituzioneLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56FinCostituzione, Len.P56_FIN_COSTITUZIONE);
        return buffer;
    }

    public void setP56FinCostituzioneLen(short p56FinCostituzioneLen) {
        this.p56FinCostituzioneLen = p56FinCostituzioneLen;
    }

    public short getP56FinCostituzioneLen() {
        return this.p56FinCostituzioneLen;
    }

    public void setP56FinCostituzione(String p56FinCostituzione) {
        this.p56FinCostituzione = Functions.subString(p56FinCostituzione, Len.P56_FIN_COSTITUZIONE);
    }

    public String getP56FinCostituzione() {
        return this.p56FinCostituzione;
    }

    public void setP56DescImpCarPubVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_IMP_CAR_PUB_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_IMP_CAR_PUB_VCHAR);
        setP56DescImpCarPubVcharBytes(buffer, 1);
    }

    public String getP56DescImpCarPubVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescImpCarPubVcharBytes());
    }

    /**Original name: P56-DESC-IMP-CAR-PUB-VCHAR<br>*/
    public byte[] getP56DescImpCarPubVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_IMP_CAR_PUB_VCHAR];
        return getP56DescImpCarPubVcharBytes(buffer, 1);
    }

    public void setP56DescImpCarPubVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescImpCarPubLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescImpCarPub = MarshalByte.readString(buffer, position, Len.P56_DESC_IMP_CAR_PUB);
    }

    public byte[] getP56DescImpCarPubVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescImpCarPubLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescImpCarPub, Len.P56_DESC_IMP_CAR_PUB);
        return buffer;
    }

    public void setP56DescImpCarPubLen(short p56DescImpCarPubLen) {
        this.p56DescImpCarPubLen = p56DescImpCarPubLen;
    }

    public short getP56DescImpCarPubLen() {
        return this.p56DescImpCarPubLen;
    }

    public void setP56DescImpCarPub(String p56DescImpCarPub) {
        this.p56DescImpCarPub = Functions.subString(p56DescImpCarPub, Len.P56_DESC_IMP_CAR_PUB);
    }

    public String getP56DescImpCarPub() {
        return this.p56DescImpCarPub;
    }

    public void setP56DescScoFinRappVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_SCO_FIN_RAPP_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_SCO_FIN_RAPP_VCHAR);
        setP56DescScoFinRappVcharBytes(buffer, 1);
    }

    public String getP56DescScoFinRappVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescScoFinRappVcharBytes());
    }

    /**Original name: P56-DESC-SCO-FIN-RAPP-VCHAR<br>*/
    public byte[] getP56DescScoFinRappVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_SCO_FIN_RAPP_VCHAR];
        return getP56DescScoFinRappVcharBytes(buffer, 1);
    }

    public void setP56DescScoFinRappVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescScoFinRappLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescScoFinRapp = MarshalByte.readString(buffer, position, Len.P56_DESC_SCO_FIN_RAPP);
    }

    public byte[] getP56DescScoFinRappVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescScoFinRappLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescScoFinRapp, Len.P56_DESC_SCO_FIN_RAPP);
        return buffer;
    }

    public void setP56DescScoFinRappLen(short p56DescScoFinRappLen) {
        this.p56DescScoFinRappLen = p56DescScoFinRappLen;
    }

    public short getP56DescScoFinRappLen() {
        return this.p56DescScoFinRappLen;
    }

    public void setP56DescScoFinRapp(String p56DescScoFinRapp) {
        this.p56DescScoFinRapp = Functions.subString(p56DescScoFinRapp, Len.P56_DESC_SCO_FIN_RAPP);
    }

    public String getP56DescScoFinRapp() {
        return this.p56DescScoFinRapp;
    }

    public void setP56DescProcPnlVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_PROC_PNL_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_PROC_PNL_VCHAR);
        setP56DescProcPnlVcharBytes(buffer, 1);
    }

    public String getP56DescProcPnlVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescProcPnlVcharBytes());
    }

    /**Original name: P56-DESC-PROC-PNL-VCHAR<br>*/
    public byte[] getP56DescProcPnlVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_PROC_PNL_VCHAR];
        return getP56DescProcPnlVcharBytes(buffer, 1);
    }

    public void setP56DescProcPnlVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescProcPnlLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescProcPnl = MarshalByte.readString(buffer, position, Len.P56_DESC_PROC_PNL);
    }

    public byte[] getP56DescProcPnlVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescProcPnlLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescProcPnl, Len.P56_DESC_PROC_PNL);
        return buffer;
    }

    public void setP56DescProcPnlLen(short p56DescProcPnlLen) {
        this.p56DescProcPnlLen = p56DescProcPnlLen;
    }

    public short getP56DescProcPnlLen() {
        return this.p56DescProcPnlLen;
    }

    public void setP56DescProcPnl(String p56DescProcPnl) {
        this.p56DescProcPnl = Functions.subString(p56DescProcPnl, Len.P56_DESC_PROC_PNL);
    }

    public String getP56DescProcPnl() {
        return this.p56DescProcPnl;
    }

    public void setP56DescNotPregVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_NOT_PREG_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_NOT_PREG_VCHAR);
        setP56DescNotPregVcharBytes(buffer, 1);
    }

    public String getP56DescNotPregVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescNotPregVcharBytes());
    }

    /**Original name: P56-DESC-NOT-PREG-VCHAR<br>*/
    public byte[] getP56DescNotPregVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_NOT_PREG_VCHAR];
        return getP56DescNotPregVcharBytes(buffer, 1);
    }

    public void setP56DescNotPregVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescNotPregLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescNotPreg = MarshalByte.readString(buffer, position, Len.P56_DESC_NOT_PREG);
    }

    public byte[] getP56DescNotPregVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescNotPregLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescNotPreg, Len.P56_DESC_NOT_PREG);
        return buffer;
    }

    public void setP56DescNotPregLen(short p56DescNotPregLen) {
        this.p56DescNotPregLen = p56DescNotPregLen;
    }

    public short getP56DescNotPregLen() {
        return this.p56DescNotPregLen;
    }

    public void setP56DescNotPreg(String p56DescNotPreg) {
        this.p56DescNotPreg = Functions.subString(p56DescNotPreg, Len.P56_DESC_NOT_PREG);
    }

    public String getP56DescNotPreg() {
        return this.p56DescNotPreg;
    }

    public void setP56DescLibMotRiscVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_LIB_MOT_RISC_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_LIB_MOT_RISC_VCHAR);
        setP56DescLibMotRiscVcharBytes(buffer, 1);
    }

    public String getP56DescLibMotRiscVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescLibMotRiscVcharBytes());
    }

    /**Original name: P56-DESC-LIB-MOT-RISC-VCHAR<br>*/
    public byte[] getP56DescLibMotRiscVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_LIB_MOT_RISC_VCHAR];
        return getP56DescLibMotRiscVcharBytes(buffer, 1);
    }

    public void setP56DescLibMotRiscVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescLibMotRiscLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescLibMotRisc = MarshalByte.readString(buffer, position, Len.P56_DESC_LIB_MOT_RISC);
    }

    public byte[] getP56DescLibMotRiscVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescLibMotRiscLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescLibMotRisc, Len.P56_DESC_LIB_MOT_RISC);
        return buffer;
    }

    public void setP56DescLibMotRiscLen(short p56DescLibMotRiscLen) {
        this.p56DescLibMotRiscLen = p56DescLibMotRiscLen;
    }

    public short getP56DescLibMotRiscLen() {
        return this.p56DescLibMotRiscLen;
    }

    public void setP56DescLibMotRisc(String p56DescLibMotRisc) {
        this.p56DescLibMotRisc = Functions.subString(p56DescLibMotRisc, Len.P56_DESC_LIB_MOT_RISC);
    }

    public String getP56DescLibMotRisc() {
        return this.p56DescLibMotRisc;
    }

    public void setP56TpMotAssTitEff(String p56TpMotAssTitEff) {
        this.p56TpMotAssTitEff = Functions.subString(p56TpMotAssTitEff, Len.P56_TP_MOT_ASS_TIT_EFF);
    }

    public String getP56TpMotAssTitEff() {
        return this.p56TpMotAssTitEff;
    }

    public String getP56TpMotAssTitEffFormatted() {
        return Functions.padBlanks(getP56TpMotAssTitEff(), Len.P56_TP_MOT_ASS_TIT_EFF);
    }

    public void setP56TpRagRapp(String p56TpRagRapp) {
        this.p56TpRagRapp = Functions.subString(p56TpRagRapp, Len.P56_TP_RAG_RAPP);
    }

    public String getP56TpRagRapp() {
        return this.p56TpRagRapp;
    }

    public String getP56TpRagRappFormatted() {
        return Functions.padBlanks(getP56TpRagRapp(), Len.P56_TP_RAG_RAPP);
    }

    public void setP56TpFinCost(String p56TpFinCost) {
        this.p56TpFinCost = Functions.subString(p56TpFinCost, Len.P56_TP_FIN_COST);
    }

    public String getP56TpFinCost() {
        return this.p56TpFinCost;
    }

    public String getP56TpFinCostFormatted() {
        return Functions.padBlanks(getP56TpFinCost(), Len.P56_TP_FIN_COST);
    }

    public void setP56NazDestFnd(String p56NazDestFnd) {
        this.p56NazDestFnd = Functions.subString(p56NazDestFnd, Len.P56_NAZ_DEST_FND);
    }

    public String getP56NazDestFnd() {
        return this.p56NazDestFnd;
    }

    public String getP56NazDestFndFormatted() {
        return Functions.padBlanks(getP56NazDestFnd(), Len.P56_NAZ_DEST_FND);
    }

    public void setP56FlAuFatcaAeoi(char p56FlAuFatcaAeoi) {
        this.p56FlAuFatcaAeoi = p56FlAuFatcaAeoi;
    }

    public char getP56FlAuFatcaAeoi() {
        return this.p56FlAuFatcaAeoi;
    }

    public void setP56TpCarFinGiur(String p56TpCarFinGiur) {
        this.p56TpCarFinGiur = Functions.subString(p56TpCarFinGiur, Len.P56_TP_CAR_FIN_GIUR);
    }

    public String getP56TpCarFinGiur() {
        return this.p56TpCarFinGiur;
    }

    public String getP56TpCarFinGiurFormatted() {
        return Functions.padBlanks(getP56TpCarFinGiur(), Len.P56_TP_CAR_FIN_GIUR);
    }

    public void setP56TpCarFinGiurAt(String p56TpCarFinGiurAt) {
        this.p56TpCarFinGiurAt = Functions.subString(p56TpCarFinGiurAt, Len.P56_TP_CAR_FIN_GIUR_AT);
    }

    public String getP56TpCarFinGiurAt() {
        return this.p56TpCarFinGiurAt;
    }

    public String getP56TpCarFinGiurAtFormatted() {
        return Functions.padBlanks(getP56TpCarFinGiurAt(), Len.P56_TP_CAR_FIN_GIUR_AT);
    }

    public void setP56TpCarFinGiurPa(String p56TpCarFinGiurPa) {
        this.p56TpCarFinGiurPa = Functions.subString(p56TpCarFinGiurPa, Len.P56_TP_CAR_FIN_GIUR_PA);
    }

    public String getP56TpCarFinGiurPa() {
        return this.p56TpCarFinGiurPa;
    }

    public String getP56TpCarFinGiurPaFormatted() {
        return Functions.padBlanks(getP56TpCarFinGiurPa(), Len.P56_TP_CAR_FIN_GIUR_PA);
    }

    public void setP56FlIstituzFin(char p56FlIstituzFin) {
        this.p56FlIstituzFin = p56FlIstituzFin;
    }

    public char getP56FlIstituzFin() {
        return this.p56FlIstituzFin;
    }

    public void setP56TpOriFndTitEff(String p56TpOriFndTitEff) {
        this.p56TpOriFndTitEff = Functions.subString(p56TpOriFndTitEff, Len.P56_TP_ORI_FND_TIT_EFF);
    }

    public String getP56TpOriFndTitEff() {
        return this.p56TpOriFndTitEff;
    }

    public String getP56TpOriFndTitEffFormatted() {
        return Functions.padBlanks(getP56TpOriFndTitEff(), Len.P56_TP_ORI_FND_TIT_EFF);
    }

    public void setP56FlPrTrUsa(char p56FlPrTrUsa) {
        this.p56FlPrTrUsa = p56FlPrTrUsa;
    }

    public char getP56FlPrTrUsa() {
        return this.p56FlPrTrUsa;
    }

    public void setP56FlPrTrNoUsa(char p56FlPrTrNoUsa) {
        this.p56FlPrTrNoUsa = p56FlPrTrNoUsa;
    }

    public char getP56FlPrTrNoUsa() {
        return this.p56FlPrTrNoUsa;
    }

    public void setP56TpStatusAeoi(String p56TpStatusAeoi) {
        this.p56TpStatusAeoi = Functions.subString(p56TpStatusAeoi, Len.P56_TP_STATUS_AEOI);
    }

    public String getP56TpStatusAeoi() {
        return this.p56TpStatusAeoi;
    }

    public String getP56TpStatusAeoiFormatted() {
        return Functions.padBlanks(getP56TpStatusAeoi(), Len.P56_TP_STATUS_AEOI);
    }

    public void setP56TpStatusFatca(String p56TpStatusFatca) {
        this.p56TpStatusFatca = Functions.subString(p56TpStatusFatca, Len.P56_TP_STATUS_FATCA);
    }

    public String getP56TpStatusFatca() {
        return this.p56TpStatusFatca;
    }

    public String getP56TpStatusFatcaFormatted() {
        return Functions.padBlanks(getP56TpStatusFatca(), Len.P56_TP_STATUS_FATCA);
    }

    public void setP56FlRappPaMsc(char p56FlRappPaMsc) {
        this.p56FlRappPaMsc = p56FlRappPaMsc;
    }

    public char getP56FlRappPaMsc() {
        return this.p56FlRappPaMsc;
    }

    public void setP56CodComunSvolAtt(String p56CodComunSvolAtt) {
        this.p56CodComunSvolAtt = Functions.subString(p56CodComunSvolAtt, Len.P56_COD_COMUN_SVOL_ATT);
    }

    public String getP56CodComunSvolAtt() {
        return this.p56CodComunSvolAtt;
    }

    public String getP56CodComunSvolAttFormatted() {
        return Functions.padBlanks(getP56CodComunSvolAtt(), Len.P56_COD_COMUN_SVOL_ATT);
    }

    public void setP56TpDt1oConCli(String p56TpDt1oConCli) {
        this.p56TpDt1oConCli = Functions.subString(p56TpDt1oConCli, Len.P56_TP_DT1O_CON_CLI);
    }

    public String getP56TpDt1oConCli() {
        return this.p56TpDt1oConCli;
    }

    public String getP56TpDt1oConCliFormatted() {
        return Functions.padBlanks(getP56TpDt1oConCli(), Len.P56_TP_DT1O_CON_CLI);
    }

    public void setP56TpModEnRelaInt(String p56TpModEnRelaInt) {
        this.p56TpModEnRelaInt = Functions.subString(p56TpModEnRelaInt, Len.P56_TP_MOD_EN_RELA_INT);
    }

    public String getP56TpModEnRelaInt() {
        return this.p56TpModEnRelaInt;
    }

    public String getP56TpModEnRelaIntFormatted() {
        return Functions.padBlanks(getP56TpModEnRelaInt(), Len.P56_TP_MOD_EN_RELA_INT);
    }

    public void setP56TpReddAnnuLrd(String p56TpReddAnnuLrd) {
        this.p56TpReddAnnuLrd = Functions.subString(p56TpReddAnnuLrd, Len.P56_TP_REDD_ANNU_LRD);
    }

    public String getP56TpReddAnnuLrd() {
        return this.p56TpReddAnnuLrd;
    }

    public String getP56TpReddAnnuLrdFormatted() {
        return Functions.padBlanks(getP56TpReddAnnuLrd(), Len.P56_TP_REDD_ANNU_LRD);
    }

    public void setP56TpReddCon(String p56TpReddCon) {
        this.p56TpReddCon = Functions.subString(p56TpReddCon, Len.P56_TP_REDD_CON);
    }

    public String getP56TpReddCon() {
        return this.p56TpReddCon;
    }

    public String getP56TpReddConFormatted() {
        return Functions.padBlanks(getP56TpReddCon(), Len.P56_TP_REDD_CON);
    }

    public void setP56TpOperSocFid(String p56TpOperSocFid) {
        this.p56TpOperSocFid = Functions.subString(p56TpOperSocFid, Len.P56_TP_OPER_SOC_FID);
    }

    public String getP56TpOperSocFid() {
        return this.p56TpOperSocFid;
    }

    public String getP56TpOperSocFidFormatted() {
        return Functions.padBlanks(getP56TpOperSocFid(), Len.P56_TP_OPER_SOC_FID);
    }

    public void setP56CodPaEspMsc1(String p56CodPaEspMsc1) {
        this.p56CodPaEspMsc1 = Functions.subString(p56CodPaEspMsc1, Len.P56_COD_PA_ESP_MSC1);
    }

    public String getP56CodPaEspMsc1() {
        return this.p56CodPaEspMsc1;
    }

    public String getP56CodPaEspMsc1Formatted() {
        return Functions.padBlanks(getP56CodPaEspMsc1(), Len.P56_COD_PA_ESP_MSC1);
    }

    public void setP56CodPaEspMsc2(String p56CodPaEspMsc2) {
        this.p56CodPaEspMsc2 = Functions.subString(p56CodPaEspMsc2, Len.P56_COD_PA_ESP_MSC2);
    }

    public String getP56CodPaEspMsc2() {
        return this.p56CodPaEspMsc2;
    }

    public String getP56CodPaEspMsc2Formatted() {
        return Functions.padBlanks(getP56CodPaEspMsc2(), Len.P56_COD_PA_ESP_MSC2);
    }

    public void setP56CodPaEspMsc3(String p56CodPaEspMsc3) {
        this.p56CodPaEspMsc3 = Functions.subString(p56CodPaEspMsc3, Len.P56_COD_PA_ESP_MSC3);
    }

    public String getP56CodPaEspMsc3() {
        return this.p56CodPaEspMsc3;
    }

    public String getP56CodPaEspMsc3Formatted() {
        return Functions.padBlanks(getP56CodPaEspMsc3(), Len.P56_COD_PA_ESP_MSC3);
    }

    public void setP56CodPaEspMsc4(String p56CodPaEspMsc4) {
        this.p56CodPaEspMsc4 = Functions.subString(p56CodPaEspMsc4, Len.P56_COD_PA_ESP_MSC4);
    }

    public String getP56CodPaEspMsc4() {
        return this.p56CodPaEspMsc4;
    }

    public String getP56CodPaEspMsc4Formatted() {
        return Functions.padBlanks(getP56CodPaEspMsc4(), Len.P56_COD_PA_ESP_MSC4);
    }

    public void setP56CodPaEspMsc5(String p56CodPaEspMsc5) {
        this.p56CodPaEspMsc5 = Functions.subString(p56CodPaEspMsc5, Len.P56_COD_PA_ESP_MSC5);
    }

    public String getP56CodPaEspMsc5() {
        return this.p56CodPaEspMsc5;
    }

    public String getP56CodPaEspMsc5Formatted() {
        return Functions.padBlanks(getP56CodPaEspMsc5(), Len.P56_COD_PA_ESP_MSC5);
    }

    public void setP56DescOrgnFndVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_ORGN_FND_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_ORGN_FND_VCHAR);
        setP56DescOrgnFndVcharBytes(buffer, 1);
    }

    public String getP56DescOrgnFndVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescOrgnFndVcharBytes());
    }

    /**Original name: P56-DESC-ORGN-FND-VCHAR<br>*/
    public byte[] getP56DescOrgnFndVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_ORGN_FND_VCHAR];
        return getP56DescOrgnFndVcharBytes(buffer, 1);
    }

    public void setP56DescOrgnFndVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescOrgnFndLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescOrgnFnd = MarshalByte.readString(buffer, position, Len.P56_DESC_ORGN_FND);
    }

    public byte[] getP56DescOrgnFndVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescOrgnFndLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescOrgnFnd, Len.P56_DESC_ORGN_FND);
        return buffer;
    }

    public void setP56DescOrgnFndLen(short p56DescOrgnFndLen) {
        this.p56DescOrgnFndLen = p56DescOrgnFndLen;
    }

    public short getP56DescOrgnFndLen() {
        return this.p56DescOrgnFndLen;
    }

    public void setP56DescOrgnFnd(String p56DescOrgnFnd) {
        this.p56DescOrgnFnd = Functions.subString(p56DescOrgnFnd, Len.P56_DESC_ORGN_FND);
    }

    public String getP56DescOrgnFnd() {
        return this.p56DescOrgnFnd;
    }

    public void setP56CodAutDueDil(String p56CodAutDueDil) {
        this.p56CodAutDueDil = Functions.subString(p56CodAutDueDil, Len.P56_COD_AUT_DUE_DIL);
    }

    public String getP56CodAutDueDil() {
        return this.p56CodAutDueDil;
    }

    public String getP56CodAutDueDilFormatted() {
        return Functions.padBlanks(getP56CodAutDueDil(), Len.P56_COD_AUT_DUE_DIL);
    }

    public void setP56FlPrQuestFatca(char p56FlPrQuestFatca) {
        this.p56FlPrQuestFatca = p56FlPrQuestFatca;
    }

    public char getP56FlPrQuestFatca() {
        return this.p56FlPrQuestFatca;
    }

    public void setP56FlPrQuestAeoi(char p56FlPrQuestAeoi) {
        this.p56FlPrQuestAeoi = p56FlPrQuestAeoi;
    }

    public char getP56FlPrQuestAeoi() {
        return this.p56FlPrQuestAeoi;
    }

    public void setP56FlPrQuestOfac(char p56FlPrQuestOfac) {
        this.p56FlPrQuestOfac = p56FlPrQuestOfac;
    }

    public char getP56FlPrQuestOfac() {
        return this.p56FlPrQuestOfac;
    }

    public void setP56FlPrQuestKyc(char p56FlPrQuestKyc) {
        this.p56FlPrQuestKyc = p56FlPrQuestKyc;
    }

    public char getP56FlPrQuestKyc() {
        return this.p56FlPrQuestKyc;
    }

    public void setP56FlPrQuestMscq(char p56FlPrQuestMscq) {
        this.p56FlPrQuestMscq = p56FlPrQuestMscq;
    }

    public char getP56FlPrQuestMscq() {
        return this.p56FlPrQuestMscq;
    }

    public void setP56TpNotPreg(String p56TpNotPreg) {
        this.p56TpNotPreg = Functions.subString(p56TpNotPreg, Len.P56_TP_NOT_PREG);
    }

    public String getP56TpNotPreg() {
        return this.p56TpNotPreg;
    }

    public String getP56TpNotPregFormatted() {
        return Functions.padBlanks(getP56TpNotPreg(), Len.P56_TP_NOT_PREG);
    }

    public void setP56TpProcPnl(String p56TpProcPnl) {
        this.p56TpProcPnl = Functions.subString(p56TpProcPnl, Len.P56_TP_PROC_PNL);
    }

    public String getP56TpProcPnl() {
        return this.p56TpProcPnl;
    }

    public String getP56TpProcPnlFormatted() {
        return Functions.padBlanks(getP56TpProcPnl(), Len.P56_TP_PROC_PNL);
    }

    public void setP56CodImpCarPub(String p56CodImpCarPub) {
        this.p56CodImpCarPub = Functions.subString(p56CodImpCarPub, Len.P56_COD_IMP_CAR_PUB);
    }

    public String getP56CodImpCarPub() {
        return this.p56CodImpCarPub;
    }

    public String getP56CodImpCarPubFormatted() {
        return Functions.padBlanks(getP56CodImpCarPub(), Len.P56_COD_IMP_CAR_PUB);
    }

    public void setP56OprzSospette(char p56OprzSospette) {
        this.p56OprzSospette = p56OprzSospette;
    }

    public char getP56OprzSospette() {
        return this.p56OprzSospette;
    }

    public void setP56DescPepVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_PEP_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_PEP_VCHAR);
        setP56DescPepVcharBytes(buffer, 1);
    }

    public String getP56DescPepVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescPepVcharBytes());
    }

    /**Original name: P56-DESC-PEP-VCHAR<br>*/
    public byte[] getP56DescPepVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_PEP_VCHAR];
        return getP56DescPepVcharBytes(buffer, 1);
    }

    public void setP56DescPepVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescPepLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescPep = MarshalByte.readString(buffer, position, Len.P56_DESC_PEP);
    }

    public byte[] getP56DescPepVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescPepLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescPep, Len.P56_DESC_PEP);
        return buffer;
    }

    public void setP56DescPepLen(short p56DescPepLen) {
        this.p56DescPepLen = p56DescPepLen;
    }

    public short getP56DescPepLen() {
        return this.p56DescPepLen;
    }

    public void setP56DescPep(String p56DescPep) {
        this.p56DescPep = Functions.subString(p56DescPep, Len.P56_DESC_PEP);
    }

    public String getP56DescPep() {
        return this.p56DescPep;
    }

    public void setP56NumTel2(String p56NumTel2) {
        this.p56NumTel2 = Functions.subString(p56NumTel2, Len.P56_NUM_TEL2);
    }

    public String getP56NumTel2() {
        return this.p56NumTel2;
    }

    public String getP56NumTel2Formatted() {
        return Functions.padBlanks(getP56NumTel2(), Len.P56_NUM_TEL2);
    }

    public void setP56FlNewPro(char p56FlNewPro) {
        this.p56FlNewPro = p56FlNewPro;
    }

    public char getP56FlNewPro() {
        return this.p56FlNewPro;
    }

    public void setP56TpMotCambioCntr(String p56TpMotCambioCntr) {
        this.p56TpMotCambioCntr = Functions.subString(p56TpMotCambioCntr, Len.P56_TP_MOT_CAMBIO_CNTR);
    }

    public String getP56TpMotCambioCntr() {
        return this.p56TpMotCambioCntr;
    }

    public String getP56TpMotCambioCntrFormatted() {
        return Functions.padBlanks(getP56TpMotCambioCntr(), Len.P56_TP_MOT_CAMBIO_CNTR);
    }

    public void setP56DescMotCambioCnVcharFormatted(String data) {
        byte[] buffer = new byte[Len.P56_DESC_MOT_CAMBIO_CN_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.P56_DESC_MOT_CAMBIO_CN_VCHAR);
        setP56DescMotCambioCnVcharBytes(buffer, 1);
    }

    public String getP56DescMotCambioCnVcharFormatted() {
        return MarshalByteExt.bufferToStr(getP56DescMotCambioCnVcharBytes());
    }

    /**Original name: P56-DESC-MOT-CAMBIO-CN-VCHAR<br>*/
    public byte[] getP56DescMotCambioCnVcharBytes() {
        byte[] buffer = new byte[Len.P56_DESC_MOT_CAMBIO_CN_VCHAR];
        return getP56DescMotCambioCnVcharBytes(buffer, 1);
    }

    public void setP56DescMotCambioCnVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        p56DescMotCambioCnLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        p56DescMotCambioCn = MarshalByte.readString(buffer, position, Len.P56_DESC_MOT_CAMBIO_CN);
    }

    public byte[] getP56DescMotCambioCnVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, p56DescMotCambioCnLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, p56DescMotCambioCn, Len.P56_DESC_MOT_CAMBIO_CN);
        return buffer;
    }

    public void setP56DescMotCambioCnLen(short p56DescMotCambioCnLen) {
        this.p56DescMotCambioCnLen = p56DescMotCambioCnLen;
    }

    public short getP56DescMotCambioCnLen() {
        return this.p56DescMotCambioCnLen;
    }

    public void setP56DescMotCambioCn(String p56DescMotCambioCn) {
        this.p56DescMotCambioCn = Functions.subString(p56DescMotCambioCn, Len.P56_DESC_MOT_CAMBIO_CN);
    }

    public String getP56DescMotCambioCn() {
        return this.p56DescMotCambioCn;
    }

    public void setP56CodSogg(String p56CodSogg) {
        this.p56CodSogg = Functions.subString(p56CodSogg, Len.P56_COD_SOGG);
    }

    public String getP56CodSogg() {
        return this.p56CodSogg;
    }

    public String getP56CodSoggFormatted() {
        return Functions.padBlanks(getP56CodSogg(), Len.P56_COD_SOGG);
    }

    public P56CodCan getP56CodCan() {
        return p56CodCan;
    }

    public P56DtIniFntRedd2 getP56DtIniFntRedd2() {
        return p56DtIniFntRedd2;
    }

    public P56DtIniFntRedd3 getP56DtIniFntRedd3() {
        return p56DtIniFntRedd3;
    }

    public P56DtIniFntRedd getP56DtIniFntRedd() {
        return p56DtIniFntRedd;
    }

    public P56IdAssicurati getP56IdAssicurati() {
        return p56IdAssicurati;
    }

    public P56IdRappAna getP56IdRappAna() {
        return p56IdRappAna;
    }

    public P56ImpAfi getP56ImpAfi() {
        return p56ImpAfi;
    }

    public P56ImpPaEspMsc1 getP56ImpPaEspMsc1() {
        return p56ImpPaEspMsc1;
    }

    public P56ImpPaEspMsc2 getP56ImpPaEspMsc2() {
        return p56ImpPaEspMsc2;
    }

    public P56ImpPaEspMsc3 getP56ImpPaEspMsc3() {
        return p56ImpPaEspMsc3;
    }

    public P56ImpPaEspMsc4 getP56ImpPaEspMsc4() {
        return p56ImpPaEspMsc4;
    }

    public P56ImpPaEspMsc5 getP56ImpPaEspMsc5() {
        return p56ImpPaEspMsc5;
    }

    public P56ImpTotAffAcc getP56ImpTotAffAcc() {
        return p56ImpTotAffAcc;
    }

    public P56ImpTotAffUtil getP56ImpTotAffUtil() {
        return p56ImpTotAffUtil;
    }

    public P56ImpTotFinAcc getP56ImpTotFinAcc() {
        return p56ImpTotFinAcc;
    }

    public P56ImpTotFinUtil getP56ImpTotFinUtil() {
        return p56ImpTotFinUtil;
    }

    public P56NumDip getP56NumDip() {
        return p56NumDip;
    }

    public P56PcEspAgPaMsc getP56PcEspAgPaMsc() {
        return p56PcEspAgPaMsc;
    }

    public P56PcQuoDetTitEff getP56PcQuoDetTitEff() {
        return p56PcQuoDetTitEff;
    }

    public P56PcRipPatAsVita getP56PcRipPatAsVita() {
        return p56PcRipPatAsVita;
    }

    public P56PcRipPatIm getP56PcRipPatIm() {
        return p56PcRipPatIm;
    }

    public P56PcRipPatSetIm getP56PcRipPatSetIm() {
        return p56PcRipPatSetIm;
    }

    public P56ReddCon getP56ReddCon() {
        return p56ReddCon;
    }

    public P56ReddFattAnnu getP56ReddFattAnnu() {
        return p56ReddFattAnnu;
    }

    public P56TpMovi getP56TpMovi() {
        return p56TpMovi;
    }

    public P56UltFattAnnu getP56UltFattAnnu() {
        return p56UltFattAnnu;
    }

    @Override
    public byte[] serialize() {
        return getQuestAdegVerBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_ID_QUEST_ADEG_VER = 5;
        public static final int P56_COD_COMP_ANIA = 3;
        public static final int P56_ID_MOVI_CRZ = 5;
        public static final int P56_ID_POLI = 5;
        public static final int P56_NATURA_OPRZ = 2;
        public static final int P56_ORGN_FND = 2;
        public static final int P56_COD_QLFC_PROF = 20;
        public static final int P56_COD_NAZ_QLFC_PROF = 4;
        public static final int P56_COD_PRV_QLFC_PROF = 4;
        public static final int P56_FNT_REDD = 2;
        public static final int P56_COD_ATECO = 20;
        public static final int P56_VALUT_COLL = 2;
        public static final int P56_DS_OPER_SQL = 1;
        public static final int P56_DS_VER = 5;
        public static final int P56_DS_TS_CPTZ = 10;
        public static final int P56_DS_UTENTE = 20;
        public static final int P56_DS_STATO_ELAB = 1;
        public static final int P56_LUOGO_COSTITUZIONE_LEN = 2;
        public static final int P56_LUOGO_COSTITUZIONE = 250;
        public static final int P56_LUOGO_COSTITUZIONE_VCHAR = P56_LUOGO_COSTITUZIONE_LEN + P56_LUOGO_COSTITUZIONE;
        public static final int P56_FL_RAG_RAPP = 1;
        public static final int P56_FL_PRSZ_TIT_EFF = 1;
        public static final int P56_TP_MOT_RISC = 2;
        public static final int P56_TP_PNT_VND = 2;
        public static final int P56_TP_ADEG_VER = 2;
        public static final int P56_TP_RELA_ESEC = 2;
        public static final int P56_TP_SCO_FIN_RAPP = 2;
        public static final int P56_FL_PRSZ3O_PAGAT = 1;
        public static final int P56_AREA_GEO_PROV_FND = 4;
        public static final int P56_TP_DEST_FND = 2;
        public static final int P56_FL_PAESE_RESID_AUT = 1;
        public static final int P56_FL_PAESE_CIT_AUT = 1;
        public static final int P56_FL_PAESE_NAZ_AUT = 1;
        public static final int P56_COD_PROF_PREC = 20;
        public static final int P56_FL_AUT_PEP = 1;
        public static final int P56_FL_IMP_CAR_PUB = 1;
        public static final int P56_FL_LIS_TERR_SORV = 1;
        public static final int P56_TP_SIT_FIN_PAT = 2;
        public static final int P56_TP_SIT_FIN_PAT_CON = 2;
        public static final int P56_TP_FRM_GIUR_SAV = 2;
        public static final int P56_REG_COLL_POLI = 4;
        public static final int P56_NUM_TEL = 20;
        public static final int P56_TP_SIT_FAM_CONV = 2;
        public static final int P56_COD_PROF_CON = 20;
        public static final int P56_FL_ES_PROC_PEN = 1;
        public static final int P56_TP_COND_CLIENTE = 2;
        public static final int P56_COD_SAE = 4;
        public static final int P56_TP_OPER_ESTERO = 2;
        public static final int P56_STAT_OPER_ESTERO = 4;
        public static final int P56_COD_PRV_SVOL_ATT = 4;
        public static final int P56_COD_STAT_SVOL_ATT = 4;
        public static final int P56_TP_SOC = 2;
        public static final int P56_FL_IND_SOC_QUOT = 1;
        public static final int P56_TP_SIT_GIUR = 2;
        public static final int P56_TP_PRFL_RSH_PEP = 2;
        public static final int P56_TP_PEP = 2;
        public static final int P56_FL_NOT_PREG = 1;
        public static final int P56_FNT_REDD2 = 2;
        public static final int P56_FNT_REDD3 = 2;
        public static final int P56_MOT_ASS_TIT_EFF_LEN = 2;
        public static final int P56_MOT_ASS_TIT_EFF = 250;
        public static final int P56_MOT_ASS_TIT_EFF_VCHAR = P56_MOT_ASS_TIT_EFF_LEN + P56_MOT_ASS_TIT_EFF;
        public static final int P56_FIN_COSTITUZIONE_LEN = 2;
        public static final int P56_FIN_COSTITUZIONE = 250;
        public static final int P56_FIN_COSTITUZIONE_VCHAR = P56_FIN_COSTITUZIONE_LEN + P56_FIN_COSTITUZIONE;
        public static final int P56_DESC_IMP_CAR_PUB_LEN = 2;
        public static final int P56_DESC_IMP_CAR_PUB = 250;
        public static final int P56_DESC_IMP_CAR_PUB_VCHAR = P56_DESC_IMP_CAR_PUB_LEN + P56_DESC_IMP_CAR_PUB;
        public static final int P56_DESC_SCO_FIN_RAPP_LEN = 2;
        public static final int P56_DESC_SCO_FIN_RAPP = 250;
        public static final int P56_DESC_SCO_FIN_RAPP_VCHAR = P56_DESC_SCO_FIN_RAPP_LEN + P56_DESC_SCO_FIN_RAPP;
        public static final int P56_DESC_PROC_PNL_LEN = 2;
        public static final int P56_DESC_PROC_PNL = 250;
        public static final int P56_DESC_PROC_PNL_VCHAR = P56_DESC_PROC_PNL_LEN + P56_DESC_PROC_PNL;
        public static final int P56_DESC_NOT_PREG_LEN = 2;
        public static final int P56_DESC_NOT_PREG = 250;
        public static final int P56_DESC_NOT_PREG_VCHAR = P56_DESC_NOT_PREG_LEN + P56_DESC_NOT_PREG;
        public static final int P56_DESC_LIB_MOT_RISC_LEN = 2;
        public static final int P56_DESC_LIB_MOT_RISC = 250;
        public static final int P56_DESC_LIB_MOT_RISC_VCHAR = P56_DESC_LIB_MOT_RISC_LEN + P56_DESC_LIB_MOT_RISC;
        public static final int P56_TP_MOT_ASS_TIT_EFF = 2;
        public static final int P56_TP_RAG_RAPP = 2;
        public static final int P56_TP_FIN_COST = 2;
        public static final int P56_NAZ_DEST_FND = 4;
        public static final int P56_FL_AU_FATCA_AEOI = 1;
        public static final int P56_TP_CAR_FIN_GIUR = 2;
        public static final int P56_TP_CAR_FIN_GIUR_AT = 2;
        public static final int P56_TP_CAR_FIN_GIUR_PA = 2;
        public static final int P56_FL_ISTITUZ_FIN = 1;
        public static final int P56_TP_ORI_FND_TIT_EFF = 2;
        public static final int P56_FL_PR_TR_USA = 1;
        public static final int P56_FL_PR_TR_NO_USA = 1;
        public static final int P56_TP_STATUS_AEOI = 2;
        public static final int P56_TP_STATUS_FATCA = 2;
        public static final int P56_FL_RAPP_PA_MSC = 1;
        public static final int P56_COD_COMUN_SVOL_ATT = 4;
        public static final int P56_TP_DT1O_CON_CLI = 2;
        public static final int P56_TP_MOD_EN_RELA_INT = 2;
        public static final int P56_TP_REDD_ANNU_LRD = 2;
        public static final int P56_TP_REDD_CON = 2;
        public static final int P56_TP_OPER_SOC_FID = 2;
        public static final int P56_COD_PA_ESP_MSC1 = 4;
        public static final int P56_COD_PA_ESP_MSC2 = 4;
        public static final int P56_COD_PA_ESP_MSC3 = 4;
        public static final int P56_COD_PA_ESP_MSC4 = 4;
        public static final int P56_COD_PA_ESP_MSC5 = 4;
        public static final int P56_DESC_ORGN_FND_LEN = 2;
        public static final int P56_DESC_ORGN_FND = 250;
        public static final int P56_DESC_ORGN_FND_VCHAR = P56_DESC_ORGN_FND_LEN + P56_DESC_ORGN_FND;
        public static final int P56_COD_AUT_DUE_DIL = 10;
        public static final int P56_FL_PR_QUEST_FATCA = 1;
        public static final int P56_FL_PR_QUEST_AEOI = 1;
        public static final int P56_FL_PR_QUEST_OFAC = 1;
        public static final int P56_FL_PR_QUEST_KYC = 1;
        public static final int P56_FL_PR_QUEST_MSCQ = 1;
        public static final int P56_TP_NOT_PREG = 2;
        public static final int P56_TP_PROC_PNL = 2;
        public static final int P56_COD_IMP_CAR_PUB = 2;
        public static final int P56_OPRZ_SOSPETTE = 1;
        public static final int P56_DESC_PEP_LEN = 2;
        public static final int P56_DESC_PEP = 100;
        public static final int P56_DESC_PEP_VCHAR = P56_DESC_PEP_LEN + P56_DESC_PEP;
        public static final int P56_NUM_TEL2 = 20;
        public static final int P56_FL_NEW_PRO = 1;
        public static final int P56_TP_MOT_CAMBIO_CNTR = 2;
        public static final int P56_DESC_MOT_CAMBIO_CN_LEN = 2;
        public static final int P56_DESC_MOT_CAMBIO_CN = 250;
        public static final int P56_DESC_MOT_CAMBIO_CN_VCHAR = P56_DESC_MOT_CAMBIO_CN_LEN + P56_DESC_MOT_CAMBIO_CN;
        public static final int P56_COD_SOGG = 20;
        public static final int QUEST_ADEG_VER = P56_ID_QUEST_ADEG_VER + P56_COD_COMP_ANIA + P56_ID_MOVI_CRZ + P56IdRappAna.Len.P56_ID_RAPP_ANA + P56_ID_POLI + P56_NATURA_OPRZ + P56_ORGN_FND + P56_COD_QLFC_PROF + P56_COD_NAZ_QLFC_PROF + P56_COD_PRV_QLFC_PROF + P56_FNT_REDD + P56ReddFattAnnu.Len.P56_REDD_FATT_ANNU + P56_COD_ATECO + P56_VALUT_COLL + P56_DS_OPER_SQL + P56_DS_VER + P56_DS_TS_CPTZ + P56_DS_UTENTE + P56_DS_STATO_ELAB + P56_LUOGO_COSTITUZIONE_VCHAR + P56TpMovi.Len.P56_TP_MOVI + P56_FL_RAG_RAPP + P56_FL_PRSZ_TIT_EFF + P56_TP_MOT_RISC + P56_TP_PNT_VND + P56_TP_ADEG_VER + P56_TP_RELA_ESEC + P56_TP_SCO_FIN_RAPP + P56_FL_PRSZ3O_PAGAT + P56_AREA_GEO_PROV_FND + P56_TP_DEST_FND + P56_FL_PAESE_RESID_AUT + P56_FL_PAESE_CIT_AUT + P56_FL_PAESE_NAZ_AUT + P56_COD_PROF_PREC + P56_FL_AUT_PEP + P56_FL_IMP_CAR_PUB + P56_FL_LIS_TERR_SORV + P56_TP_SIT_FIN_PAT + P56_TP_SIT_FIN_PAT_CON + P56ImpTotAffUtil.Len.P56_IMP_TOT_AFF_UTIL + P56ImpTotFinUtil.Len.P56_IMP_TOT_FIN_UTIL + P56ImpTotAffAcc.Len.P56_IMP_TOT_AFF_ACC + P56ImpTotFinAcc.Len.P56_IMP_TOT_FIN_ACC + P56_TP_FRM_GIUR_SAV + P56_REG_COLL_POLI + P56_NUM_TEL + P56NumDip.Len.P56_NUM_DIP + P56_TP_SIT_FAM_CONV + P56_COD_PROF_CON + P56_FL_ES_PROC_PEN + P56_TP_COND_CLIENTE + P56_COD_SAE + P56_TP_OPER_ESTERO + P56_STAT_OPER_ESTERO + P56_COD_PRV_SVOL_ATT + P56_COD_STAT_SVOL_ATT + P56_TP_SOC + P56_FL_IND_SOC_QUOT + P56_TP_SIT_GIUR + P56PcQuoDetTitEff.Len.P56_PC_QUO_DET_TIT_EFF + P56_TP_PRFL_RSH_PEP + P56_TP_PEP + P56_FL_NOT_PREG + P56DtIniFntRedd.Len.P56_DT_INI_FNT_REDD + P56_FNT_REDD2 + P56DtIniFntRedd2.Len.P56_DT_INI_FNT_REDD2 + P56_FNT_REDD3 + P56DtIniFntRedd3.Len.P56_DT_INI_FNT_REDD3 + P56_MOT_ASS_TIT_EFF_VCHAR + P56_FIN_COSTITUZIONE_VCHAR + P56_DESC_IMP_CAR_PUB_VCHAR + P56_DESC_SCO_FIN_RAPP_VCHAR + P56_DESC_PROC_PNL_VCHAR + P56_DESC_NOT_PREG_VCHAR + P56IdAssicurati.Len.P56_ID_ASSICURATI + P56ReddCon.Len.P56_REDD_CON + P56_DESC_LIB_MOT_RISC_VCHAR + P56_TP_MOT_ASS_TIT_EFF + P56_TP_RAG_RAPP + P56CodCan.Len.P56_COD_CAN + P56_TP_FIN_COST + P56_NAZ_DEST_FND + P56_FL_AU_FATCA_AEOI + P56_TP_CAR_FIN_GIUR + P56_TP_CAR_FIN_GIUR_AT + P56_TP_CAR_FIN_GIUR_PA + P56_FL_ISTITUZ_FIN + P56_TP_ORI_FND_TIT_EFF + P56PcEspAgPaMsc.Len.P56_PC_ESP_AG_PA_MSC + P56_FL_PR_TR_USA + P56_FL_PR_TR_NO_USA + P56PcRipPatAsVita.Len.P56_PC_RIP_PAT_AS_VITA + P56PcRipPatIm.Len.P56_PC_RIP_PAT_IM + P56PcRipPatSetIm.Len.P56_PC_RIP_PAT_SET_IM + P56_TP_STATUS_AEOI + P56_TP_STATUS_FATCA + P56_FL_RAPP_PA_MSC + P56_COD_COMUN_SVOL_ATT + P56_TP_DT1O_CON_CLI + P56_TP_MOD_EN_RELA_INT + P56_TP_REDD_ANNU_LRD + P56_TP_REDD_CON + P56_TP_OPER_SOC_FID + P56_COD_PA_ESP_MSC1 + P56ImpPaEspMsc1.Len.P56_IMP_PA_ESP_MSC1 + P56_COD_PA_ESP_MSC2 + P56ImpPaEspMsc2.Len.P56_IMP_PA_ESP_MSC2 + P56_COD_PA_ESP_MSC3 + P56ImpPaEspMsc3.Len.P56_IMP_PA_ESP_MSC3 + P56_COD_PA_ESP_MSC4 + P56ImpPaEspMsc4.Len.P56_IMP_PA_ESP_MSC4 + P56_COD_PA_ESP_MSC5 + P56ImpPaEspMsc5.Len.P56_IMP_PA_ESP_MSC5 + P56_DESC_ORGN_FND_VCHAR + P56_COD_AUT_DUE_DIL + P56_FL_PR_QUEST_FATCA + P56_FL_PR_QUEST_AEOI + P56_FL_PR_QUEST_OFAC + P56_FL_PR_QUEST_KYC + P56_FL_PR_QUEST_MSCQ + P56_TP_NOT_PREG + P56_TP_PROC_PNL + P56_COD_IMP_CAR_PUB + P56_OPRZ_SOSPETTE + P56UltFattAnnu.Len.P56_ULT_FATT_ANNU + P56_DESC_PEP_VCHAR + P56_NUM_TEL2 + P56ImpAfi.Len.P56_IMP_AFI + P56_FL_NEW_PRO + P56_TP_MOT_CAMBIO_CNTR + P56_DESC_MOT_CAMBIO_CN_VCHAR + P56_COD_SOGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_ID_QUEST_ADEG_VER = 9;
            public static final int P56_COD_COMP_ANIA = 5;
            public static final int P56_ID_MOVI_CRZ = 9;
            public static final int P56_ID_POLI = 9;
            public static final int P56_DS_VER = 9;
            public static final int P56_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
