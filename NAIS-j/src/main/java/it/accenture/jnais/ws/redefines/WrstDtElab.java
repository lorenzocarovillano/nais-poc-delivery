package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRST-DT-ELAB<br>
 * Variable: WRST-DT-ELAB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstDtElab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstDtElab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_DT_ELAB;
    }

    public void setWrstDtElab(int wrstDtElab) {
        writeIntAsPacked(Pos.WRST_DT_ELAB, wrstDtElab, Len.Int.WRST_DT_ELAB);
    }

    public void setWrstDtElabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_DT_ELAB, Pos.WRST_DT_ELAB);
    }

    /**Original name: WRST-DT-ELAB<br>*/
    public int getWrstDtElab() {
        return readPackedAsInt(Pos.WRST_DT_ELAB, Len.Int.WRST_DT_ELAB);
    }

    public byte[] getWrstDtElabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_DT_ELAB, Pos.WRST_DT_ELAB);
        return buffer;
    }

    public void initWrstDtElabSpaces() {
        fill(Pos.WRST_DT_ELAB, Len.WRST_DT_ELAB, Types.SPACE_CHAR);
    }

    public void setWrstDtElabNull(String wrstDtElabNull) {
        writeString(Pos.WRST_DT_ELAB_NULL, wrstDtElabNull, Len.WRST_DT_ELAB_NULL);
    }

    /**Original name: WRST-DT-ELAB-NULL<br>*/
    public String getWrstDtElabNull() {
        return readString(Pos.WRST_DT_ELAB_NULL, Len.WRST_DT_ELAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_DT_ELAB = 1;
        public static final int WRST_DT_ELAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_DT_ELAB = 5;
        public static final int WRST_DT_ELAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_DT_ELAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
