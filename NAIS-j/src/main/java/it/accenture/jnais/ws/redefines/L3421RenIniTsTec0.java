package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-REN-INI-TS-TEC-0<br>
 * Variable: L3421-REN-INI-TS-TEC-0 from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421RenIniTsTec0 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421RenIniTsTec0() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_REN_INI_TS_TEC0;
    }

    public void setL3421RenIniTsTec0FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_REN_INI_TS_TEC0, Pos.L3421_REN_INI_TS_TEC0);
    }

    /**Original name: L3421-REN-INI-TS-TEC-0<br>*/
    public AfDecimal getL3421RenIniTsTec0() {
        return readPackedAsDecimal(Pos.L3421_REN_INI_TS_TEC0, Len.Int.L3421_REN_INI_TS_TEC0, Len.Fract.L3421_REN_INI_TS_TEC0);
    }

    public byte[] getL3421RenIniTsTec0AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_REN_INI_TS_TEC0, Pos.L3421_REN_INI_TS_TEC0);
        return buffer;
    }

    /**Original name: L3421-REN-INI-TS-TEC-0-NULL<br>*/
    public String getL3421RenIniTsTec0Null() {
        return readString(Pos.L3421_REN_INI_TS_TEC0_NULL, Len.L3421_REN_INI_TS_TEC0_NULL);
    }

    public String getL3421RenIniTsTec0NullFormatted() {
        return Functions.padBlanks(getL3421RenIniTsTec0Null(), Len.L3421_REN_INI_TS_TEC0_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_REN_INI_TS_TEC0 = 1;
        public static final int L3421_REN_INI_TS_TEC0_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_REN_INI_TS_TEC0 = 8;
        public static final int L3421_REN_INI_TS_TEC0_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_REN_INI_TS_TEC0 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_REN_INI_TS_TEC0 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
