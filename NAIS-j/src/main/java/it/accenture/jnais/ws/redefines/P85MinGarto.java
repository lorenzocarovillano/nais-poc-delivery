package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-MIN-GARTO<br>
 * Variable: P85-MIN-GARTO from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85MinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85MinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_MIN_GARTO;
    }

    public void setP85MinGarto(AfDecimal p85MinGarto) {
        writeDecimalAsPacked(Pos.P85_MIN_GARTO, p85MinGarto.copy());
    }

    public void setP85MinGartoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_MIN_GARTO, Pos.P85_MIN_GARTO);
    }

    /**Original name: P85-MIN-GARTO<br>*/
    public AfDecimal getP85MinGarto() {
        return readPackedAsDecimal(Pos.P85_MIN_GARTO, Len.Int.P85_MIN_GARTO, Len.Fract.P85_MIN_GARTO);
    }

    public byte[] getP85MinGartoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_MIN_GARTO, Pos.P85_MIN_GARTO);
        return buffer;
    }

    public void setP85MinGartoNull(String p85MinGartoNull) {
        writeString(Pos.P85_MIN_GARTO_NULL, p85MinGartoNull, Len.P85_MIN_GARTO_NULL);
    }

    /**Original name: P85-MIN-GARTO-NULL<br>*/
    public String getP85MinGartoNull() {
        return readString(Pos.P85_MIN_GARTO_NULL, Len.P85_MIN_GARTO_NULL);
    }

    public String getP85MinGartoNullFormatted() {
        return Functions.padBlanks(getP85MinGartoNull(), Len.P85_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_MIN_GARTO = 1;
        public static final int P85_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_MIN_GARTO = 8;
        public static final int P85_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_MIN_GARTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_MIN_GARTO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
