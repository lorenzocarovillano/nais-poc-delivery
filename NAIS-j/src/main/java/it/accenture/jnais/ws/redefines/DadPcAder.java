package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-PC-ADER<br>
 * Variable: DAD-PC-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadPcAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadPcAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_PC_ADER;
    }

    public void setDadPcAder(AfDecimal dadPcAder) {
        writeDecimalAsPacked(Pos.DAD_PC_ADER, dadPcAder.copy());
    }

    public void setDadPcAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_PC_ADER, Pos.DAD_PC_ADER);
    }

    /**Original name: DAD-PC-ADER<br>*/
    public AfDecimal getDadPcAder() {
        return readPackedAsDecimal(Pos.DAD_PC_ADER, Len.Int.DAD_PC_ADER, Len.Fract.DAD_PC_ADER);
    }

    public byte[] getDadPcAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_PC_ADER, Pos.DAD_PC_ADER);
        return buffer;
    }

    public void setDadPcAderNull(String dadPcAderNull) {
        writeString(Pos.DAD_PC_ADER_NULL, dadPcAderNull, Len.DAD_PC_ADER_NULL);
    }

    /**Original name: DAD-PC-ADER-NULL<br>*/
    public String getDadPcAderNull() {
        return readString(Pos.DAD_PC_ADER_NULL, Len.DAD_PC_ADER_NULL);
    }

    public String getDadPcAderNullFormatted() {
        return Functions.padBlanks(getDadPcAderNull(), Len.DAD_PC_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_PC_ADER = 1;
        public static final int DAD_PC_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_PC_ADER = 4;
        public static final int DAD_PC_ADER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
