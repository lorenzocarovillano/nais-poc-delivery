package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ETA-MM-2O-ASSTO<br>
 * Variable: GRZ-ETA-MM-2O-ASSTO from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzEtaMm2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzEtaMm2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ETA_MM2O_ASSTO;
    }

    public void setGrzEtaMm2oAssto(short grzEtaMm2oAssto) {
        writeShortAsPacked(Pos.GRZ_ETA_MM2O_ASSTO, grzEtaMm2oAssto, Len.Int.GRZ_ETA_MM2O_ASSTO);
    }

    public void setGrzEtaMm2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ETA_MM2O_ASSTO, Pos.GRZ_ETA_MM2O_ASSTO);
    }

    /**Original name: GRZ-ETA-MM-2O-ASSTO<br>*/
    public short getGrzEtaMm2oAssto() {
        return readPackedAsShort(Pos.GRZ_ETA_MM2O_ASSTO, Len.Int.GRZ_ETA_MM2O_ASSTO);
    }

    public byte[] getGrzEtaMm2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ETA_MM2O_ASSTO, Pos.GRZ_ETA_MM2O_ASSTO);
        return buffer;
    }

    public void setGrzEtaMm2oAsstoNull(String grzEtaMm2oAsstoNull) {
        writeString(Pos.GRZ_ETA_MM2O_ASSTO_NULL, grzEtaMm2oAsstoNull, Len.GRZ_ETA_MM2O_ASSTO_NULL);
    }

    /**Original name: GRZ-ETA-MM-2O-ASSTO-NULL<br>*/
    public String getGrzEtaMm2oAsstoNull() {
        return readString(Pos.GRZ_ETA_MM2O_ASSTO_NULL, Len.GRZ_ETA_MM2O_ASSTO_NULL);
    }

    public String getGrzEtaMm2oAsstoNullFormatted() {
        return Functions.padBlanks(getGrzEtaMm2oAsstoNull(), Len.GRZ_ETA_MM2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_MM2O_ASSTO = 1;
        public static final int GRZ_ETA_MM2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ETA_MM2O_ASSTO = 2;
        public static final int GRZ_ETA_MM2O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ETA_MM2O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
