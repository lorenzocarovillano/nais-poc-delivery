package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-ABB<br>
 * Variable: WRST-RIS-ABB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisAbb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisAbb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_ABB;
    }

    public void setWrstRisAbb(AfDecimal wrstRisAbb) {
        writeDecimalAsPacked(Pos.WRST_RIS_ABB, wrstRisAbb.copy());
    }

    public void setWrstRisAbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_ABB, Pos.WRST_RIS_ABB);
    }

    /**Original name: WRST-RIS-ABB<br>*/
    public AfDecimal getWrstRisAbb() {
        return readPackedAsDecimal(Pos.WRST_RIS_ABB, Len.Int.WRST_RIS_ABB, Len.Fract.WRST_RIS_ABB);
    }

    public byte[] getWrstRisAbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_ABB, Pos.WRST_RIS_ABB);
        return buffer;
    }

    public void initWrstRisAbbSpaces() {
        fill(Pos.WRST_RIS_ABB, Len.WRST_RIS_ABB, Types.SPACE_CHAR);
    }

    public void setWrstRisAbbNull(String wrstRisAbbNull) {
        writeString(Pos.WRST_RIS_ABB_NULL, wrstRisAbbNull, Len.WRST_RIS_ABB_NULL);
    }

    /**Original name: WRST-RIS-ABB-NULL<br>*/
    public String getWrstRisAbbNull() {
        return readString(Pos.WRST_RIS_ABB_NULL, Len.WRST_RIS_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_ABB = 1;
        public static final int WRST_RIS_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_ABB = 8;
        public static final int WRST_RIS_ABB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_ABB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_ABB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
