package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDettTitDiRat;
import it.accenture.jnais.copy.RichDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS3460<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs3460Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-DETT-TIT-DI-RAT
    private IndDettTitDiRat indDettTitDiRat = new IndDettTitDiRat();
    //Original name: DETT-TIT-DI-RAT-DB
    private RichDb dettTitDiRatDb = new RichDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public RichDb getDettTitDiRatDb() {
        return dettTitDiRatDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDettTitDiRat getIndDettTitDiRat() {
        return indDettTitDiRat;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
