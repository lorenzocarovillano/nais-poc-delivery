package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-LRD<br>
 * Variable: WPAG-PRE-LRD from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_LRD;
    }

    public void setWpagPreLrd(AfDecimal wpagPreLrd) {
        writeDecimalAsPacked(Pos.WPAG_PRE_LRD, wpagPreLrd.copy());
    }

    public void setWpagPreLrdFormatted(String wpagPreLrd) {
        setWpagPreLrd(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_LRD + Len.Fract.WPAG_PRE_LRD, Len.Fract.WPAG_PRE_LRD, wpagPreLrd));
    }

    public void setWpagPreLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_LRD, Pos.WPAG_PRE_LRD);
    }

    /**Original name: WPAG-PRE-LRD<br>*/
    public AfDecimal getWpagPreLrd() {
        return readPackedAsDecimal(Pos.WPAG_PRE_LRD, Len.Int.WPAG_PRE_LRD, Len.Fract.WPAG_PRE_LRD);
    }

    public byte[] getWpagPreLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_LRD, Pos.WPAG_PRE_LRD);
        return buffer;
    }

    public void initWpagPreLrdSpaces() {
        fill(Pos.WPAG_PRE_LRD, Len.WPAG_PRE_LRD, Types.SPACE_CHAR);
    }

    public void setWpagPreLrdNull(String wpagPreLrdNull) {
        writeString(Pos.WPAG_PRE_LRD_NULL, wpagPreLrdNull, Len.WPAG_PRE_LRD_NULL);
    }

    /**Original name: WPAG-PRE-LRD-NULL<br>*/
    public String getWpagPreLrdNull() {
        return readString(Pos.WPAG_PRE_LRD_NULL, Len.WPAG_PRE_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_LRD = 1;
        public static final int WPAG_PRE_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_LRD = 8;
        public static final int WPAG_PRE_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_LRD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_LRD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
