package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PROV-RICOR<br>
 * Variable: TDR-TOT-PROV-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PROV_RICOR;
    }

    public void setTdrTotProvRicor(AfDecimal tdrTotProvRicor) {
        writeDecimalAsPacked(Pos.TDR_TOT_PROV_RICOR, tdrTotProvRicor.copy());
    }

    public void setTdrTotProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PROV_RICOR, Pos.TDR_TOT_PROV_RICOR);
    }

    /**Original name: TDR-TOT-PROV-RICOR<br>*/
    public AfDecimal getTdrTotProvRicor() {
        return readPackedAsDecimal(Pos.TDR_TOT_PROV_RICOR, Len.Int.TDR_TOT_PROV_RICOR, Len.Fract.TDR_TOT_PROV_RICOR);
    }

    public byte[] getTdrTotProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PROV_RICOR, Pos.TDR_TOT_PROV_RICOR);
        return buffer;
    }

    public void setTdrTotProvRicorNull(String tdrTotProvRicorNull) {
        writeString(Pos.TDR_TOT_PROV_RICOR_NULL, tdrTotProvRicorNull, Len.TDR_TOT_PROV_RICOR_NULL);
    }

    /**Original name: TDR-TOT-PROV-RICOR-NULL<br>*/
    public String getTdrTotProvRicorNull() {
        return readString(Pos.TDR_TOT_PROV_RICOR_NULL, Len.TDR_TOT_PROV_RICOR_NULL);
    }

    public String getTdrTotProvRicorNullFormatted() {
        return Functions.padBlanks(getTdrTotProvRicorNull(), Len.TDR_TOT_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_RICOR = 1;
        public static final int TDR_TOT_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_RICOR = 8;
        public static final int TDR_TOT_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
