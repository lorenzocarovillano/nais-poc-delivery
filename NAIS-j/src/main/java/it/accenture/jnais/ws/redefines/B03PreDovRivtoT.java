package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-DOV-RIVTO-T<br>
 * Variable: B03-PRE-DOV-RIVTO-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreDovRivtoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreDovRivtoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_DOV_RIVTO_T;
    }

    public void setB03PreDovRivtoT(AfDecimal b03PreDovRivtoT) {
        writeDecimalAsPacked(Pos.B03_PRE_DOV_RIVTO_T, b03PreDovRivtoT.copy());
    }

    public void setB03PreDovRivtoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_DOV_RIVTO_T, Pos.B03_PRE_DOV_RIVTO_T);
    }

    /**Original name: B03-PRE-DOV-RIVTO-T<br>*/
    public AfDecimal getB03PreDovRivtoT() {
        return readPackedAsDecimal(Pos.B03_PRE_DOV_RIVTO_T, Len.Int.B03_PRE_DOV_RIVTO_T, Len.Fract.B03_PRE_DOV_RIVTO_T);
    }

    public byte[] getB03PreDovRivtoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_DOV_RIVTO_T, Pos.B03_PRE_DOV_RIVTO_T);
        return buffer;
    }

    public void setB03PreDovRivtoTNull(String b03PreDovRivtoTNull) {
        writeString(Pos.B03_PRE_DOV_RIVTO_T_NULL, b03PreDovRivtoTNull, Len.B03_PRE_DOV_RIVTO_T_NULL);
    }

    /**Original name: B03-PRE-DOV-RIVTO-T-NULL<br>*/
    public String getB03PreDovRivtoTNull() {
        return readString(Pos.B03_PRE_DOV_RIVTO_T_NULL, Len.B03_PRE_DOV_RIVTO_T_NULL);
    }

    public String getB03PreDovRivtoTNullFormatted() {
        return Functions.padBlanks(getB03PreDovRivtoTNull(), Len.B03_PRE_DOV_RIVTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_DOV_RIVTO_T = 1;
        public static final int B03_PRE_DOV_RIVTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_DOV_RIVTO_T = 8;
        public static final int B03_PRE_DOV_RIVTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_DOV_RIVTO_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_DOV_RIVTO_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
