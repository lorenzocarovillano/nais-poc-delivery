package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ALQ-RETR-T<br>
 * Variable: B03-ALQ-RETR-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AlqRetrT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AlqRetrT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ALQ_RETR_T;
    }

    public void setB03AlqRetrT(AfDecimal b03AlqRetrT) {
        writeDecimalAsPacked(Pos.B03_ALQ_RETR_T, b03AlqRetrT.copy());
    }

    public void setB03AlqRetrTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ALQ_RETR_T, Pos.B03_ALQ_RETR_T);
    }

    /**Original name: B03-ALQ-RETR-T<br>*/
    public AfDecimal getB03AlqRetrT() {
        return readPackedAsDecimal(Pos.B03_ALQ_RETR_T, Len.Int.B03_ALQ_RETR_T, Len.Fract.B03_ALQ_RETR_T);
    }

    public byte[] getB03AlqRetrTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ALQ_RETR_T, Pos.B03_ALQ_RETR_T);
        return buffer;
    }

    public void setB03AlqRetrTNull(String b03AlqRetrTNull) {
        writeString(Pos.B03_ALQ_RETR_T_NULL, b03AlqRetrTNull, Len.B03_ALQ_RETR_T_NULL);
    }

    /**Original name: B03-ALQ-RETR-T-NULL<br>*/
    public String getB03AlqRetrTNull() {
        return readString(Pos.B03_ALQ_RETR_T_NULL, Len.B03_ALQ_RETR_T_NULL);
    }

    public String getB03AlqRetrTNullFormatted() {
        return Functions.padBlanks(getB03AlqRetrTNull(), Len.B03_ALQ_RETR_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ALQ_RETR_T = 1;
        public static final int B03_ALQ_RETR_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ALQ_RETR_T = 4;
        public static final int B03_ALQ_RETR_T_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ALQ_RETR_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ALQ_RETR_T = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
