package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0001-LETT-ULT-IMMAGINE<br>
 * Variable: IDSV0001-LETT-ULT-IMMAGINE from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001LettUltImmagine {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setLettUltImmagine(char lettUltImmagine) {
        this.value = lettUltImmagine;
    }

    public char getLettUltImmagine() {
        return this.value;
    }

    public boolean isIdsv0001LettUltImmagineSi() {
        return value == SI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LETT_ULT_IMMAGINE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
