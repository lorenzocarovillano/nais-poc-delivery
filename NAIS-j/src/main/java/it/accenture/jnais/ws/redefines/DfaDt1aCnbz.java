package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-DT-1A-CNBZ<br>
 * Variable: DFA-DT-1A-CNBZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaDt1aCnbz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaDt1aCnbz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_DT1A_CNBZ;
    }

    public void setDfaDt1aCnbz(int dfaDt1aCnbz) {
        writeIntAsPacked(Pos.DFA_DT1A_CNBZ, dfaDt1aCnbz, Len.Int.DFA_DT1A_CNBZ);
    }

    public void setDfaDt1aCnbzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_DT1A_CNBZ, Pos.DFA_DT1A_CNBZ);
    }

    /**Original name: DFA-DT-1A-CNBZ<br>*/
    public int getDfaDt1aCnbz() {
        return readPackedAsInt(Pos.DFA_DT1A_CNBZ, Len.Int.DFA_DT1A_CNBZ);
    }

    public byte[] getDfaDt1aCnbzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_DT1A_CNBZ, Pos.DFA_DT1A_CNBZ);
        return buffer;
    }

    public void setDfaDt1aCnbzNull(String dfaDt1aCnbzNull) {
        writeString(Pos.DFA_DT1A_CNBZ_NULL, dfaDt1aCnbzNull, Len.DFA_DT1A_CNBZ_NULL);
    }

    /**Original name: DFA-DT-1A-CNBZ-NULL<br>*/
    public String getDfaDt1aCnbzNull() {
        return readString(Pos.DFA_DT1A_CNBZ_NULL, Len.DFA_DT1A_CNBZ_NULL);
    }

    public String getDfaDt1aCnbzNullFormatted() {
        return Functions.padBlanks(getDfaDt1aCnbzNull(), Len.DFA_DT1A_CNBZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_DT1A_CNBZ = 1;
        public static final int DFA_DT1A_CNBZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_DT1A_CNBZ = 5;
        public static final int DFA_DT1A_CNBZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_DT1A_CNBZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
