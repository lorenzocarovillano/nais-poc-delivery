package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-SP-Z-OPZ-EMIS<br>
 * Variable: B03-QTZ-SP-Z-OPZ-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzSpZOpzEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzSpZOpzEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_SP_Z_OPZ_EMIS;
    }

    public void setB03QtzSpZOpzEmis(AfDecimal b03QtzSpZOpzEmis) {
        writeDecimalAsPacked(Pos.B03_QTZ_SP_Z_OPZ_EMIS, b03QtzSpZOpzEmis.copy());
    }

    public void setB03QtzSpZOpzEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_SP_Z_OPZ_EMIS, Pos.B03_QTZ_SP_Z_OPZ_EMIS);
    }

    /**Original name: B03-QTZ-SP-Z-OPZ-EMIS<br>*/
    public AfDecimal getB03QtzSpZOpzEmis() {
        return readPackedAsDecimal(Pos.B03_QTZ_SP_Z_OPZ_EMIS, Len.Int.B03_QTZ_SP_Z_OPZ_EMIS, Len.Fract.B03_QTZ_SP_Z_OPZ_EMIS);
    }

    public byte[] getB03QtzSpZOpzEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_SP_Z_OPZ_EMIS, Pos.B03_QTZ_SP_Z_OPZ_EMIS);
        return buffer;
    }

    public void setB03QtzSpZOpzEmisNull(String b03QtzSpZOpzEmisNull) {
        writeString(Pos.B03_QTZ_SP_Z_OPZ_EMIS_NULL, b03QtzSpZOpzEmisNull, Len.B03_QTZ_SP_Z_OPZ_EMIS_NULL);
    }

    /**Original name: B03-QTZ-SP-Z-OPZ-EMIS-NULL<br>*/
    public String getB03QtzSpZOpzEmisNull() {
        return readString(Pos.B03_QTZ_SP_Z_OPZ_EMIS_NULL, Len.B03_QTZ_SP_Z_OPZ_EMIS_NULL);
    }

    public String getB03QtzSpZOpzEmisNullFormatted() {
        return Functions.padBlanks(getB03QtzSpZOpzEmisNull(), Len.B03_QTZ_SP_Z_OPZ_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_OPZ_EMIS = 1;
        public static final int B03_QTZ_SP_Z_OPZ_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_OPZ_EMIS = 7;
        public static final int B03_QTZ_SP_Z_OPZ_EMIS_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_OPZ_EMIS = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_OPZ_EMIS = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
