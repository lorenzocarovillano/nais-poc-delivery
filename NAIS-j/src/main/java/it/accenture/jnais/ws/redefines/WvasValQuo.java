package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-VAL-QUO<br>
 * Variable: WVAS-VAL-QUO from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasValQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasValQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_VAL_QUO;
    }

    public void setWvasValQuo(AfDecimal wvasValQuo) {
        writeDecimalAsPacked(Pos.WVAS_VAL_QUO, wvasValQuo.copy());
    }

    /**Original name: WVAS-VAL-QUO<br>*/
    public AfDecimal getWvasValQuo() {
        return readPackedAsDecimal(Pos.WVAS_VAL_QUO, Len.Int.WVAS_VAL_QUO, Len.Fract.WVAS_VAL_QUO);
    }

    public void setWvasValQuoNull(String wvasValQuoNull) {
        writeString(Pos.WVAS_VAL_QUO_NULL, wvasValQuoNull, Len.WVAS_VAL_QUO_NULL);
    }

    /**Original name: WVAS-VAL-QUO-NULL<br>*/
    public String getWvasValQuoNull() {
        return readString(Pos.WVAS_VAL_QUO_NULL, Len.WVAS_VAL_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_VAL_QUO = 1;
        public static final int WVAS_VAL_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_VAL_QUO = 7;
        public static final int WVAS_VAL_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_VAL_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_VAL_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
