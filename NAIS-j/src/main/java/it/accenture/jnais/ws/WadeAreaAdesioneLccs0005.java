package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvade1;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WADE-AREA-ADESIONE<br>
 * Variable: WADE-AREA-ADESIONE from program LCCS0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WadeAreaAdesioneLccs0005 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WADE-ELE-ADES-MAX
    private short wadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1 lccvade1 = new Lccvade1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_AREA_ADESIONE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWadeAreaAdesioneBytes(buf);
    }

    public String getWadeAreaAdesioneFormatted() {
        return MarshalByteExt.bufferToStr(getWadeAreaAdesioneBytes());
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer) {
        setWadeAreaAdesioneBytes(buffer, 1);
    }

    public byte[] getWadeAreaAdesioneBytes() {
        byte[] buffer = new byte[Len.WADE_AREA_ADESIONE];
        return getWadeAreaAdesioneBytes(buffer, 1);
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWadeTabAdesBytes(buffer, position);
    }

    public byte[] getWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wadeEleAdesMax);
        position += Types.SHORT_SIZE;
        getWadeTabAdesBytes(buffer, position);
        return buffer;
    }

    public void setWadeEleAdesMax(short wadeEleAdesMax) {
        this.wadeEleAdesMax = wadeEleAdesMax;
    }

    public short getWadeEleAdesMax() {
        return this.wadeEleAdesMax;
    }

    public void setWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1.Len.Int.ID_PTF, 0));
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvade1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvade1.getIdPtf(), Lccvade1.Len.Int.ID_PTF, 0);
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvade1 getLccvade1() {
        return lccvade1;
    }

    @Override
    public byte[] serialize() {
        return getWadeAreaAdesioneBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_ELE_ADES_MAX = 2;
        public static final int WADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int WADE_AREA_ADESIONE = WADE_ELE_ADES_MAX + WADE_TAB_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
