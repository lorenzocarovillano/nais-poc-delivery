package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PRE-TOT<br>
 * Variable: WTDR-TOT-PRE-TOT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PRE_TOT;
    }

    public void setWtdrTotPreTot(AfDecimal wtdrTotPreTot) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PRE_TOT, wtdrTotPreTot.copy());
    }

    /**Original name: WTDR-TOT-PRE-TOT<br>*/
    public AfDecimal getWtdrTotPreTot() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PRE_TOT, Len.Int.WTDR_TOT_PRE_TOT, Len.Fract.WTDR_TOT_PRE_TOT);
    }

    public void setWtdrTotPreTotNull(String wtdrTotPreTotNull) {
        writeString(Pos.WTDR_TOT_PRE_TOT_NULL, wtdrTotPreTotNull, Len.WTDR_TOT_PRE_TOT_NULL);
    }

    /**Original name: WTDR-TOT-PRE-TOT-NULL<br>*/
    public String getWtdrTotPreTotNull() {
        return readString(Pos.WTDR_TOT_PRE_TOT_NULL, Len.WTDR_TOT_PRE_TOT_NULL);
    }

    public String getWtdrTotPreTotNullFormatted() {
        return Functions.padBlanks(getWtdrTotPreTotNull(), Len.WTDR_TOT_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_TOT = 1;
        public static final int WTDR_TOT_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_TOT = 8;
        public static final int WTDR_TOT_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
