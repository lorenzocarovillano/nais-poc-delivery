package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-PRESTITO<br>
 * Variable: WK-PRESTITO from program LVVS0098<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkPrestito {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_PRESTITO);
    public static final String ATTIVO = "SI";
    public static final String NON_ATTIVO = "NO";

    //==== METHODS ====
    public void setWkPrestito(String wkPrestito) {
        this.value = Functions.subString(wkPrestito, Len.WK_PRESTITO);
    }

    public String getWkPrestito() {
        return this.value;
    }

    public boolean isAttivo() {
        return value.equals(ATTIVO);
    }

    public void setAttivo() {
        value = ATTIVO;
    }

    public void setNonAttivo() {
        value = NON_ATTIVO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_PRESTITO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
