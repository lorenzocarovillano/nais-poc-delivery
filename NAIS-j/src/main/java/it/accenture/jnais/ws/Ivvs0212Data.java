package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.commons.data.to.ICtrlAutOper;
import it.accenture.jnais.copy.CtrlAutOper;
import it.accenture.jnais.copy.IndCtrlAutOper;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IVVS0212<br>
 * Generated as a class for rule WS.<br>*/
public class Ivvs0212Data implements ICtrlAutOper {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>**************************************************************
	 *  NOMI PGM
	 * **************************************************************</pre>*/
    private String wkPgm = "IVVS0212";
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: CTRL-AUT-OPER
    private CtrlAutOper ctrlAutOper = new CtrlAutOper();
    //Original name: IND-CTRL-AUT-OPER
    private IndCtrlAutOper indCtrlAutOper = new IndCtrlAutOper();
    //Original name: UNZIP-STRUCTURE
    private UnzipStructure unzipStructure = new UnzipStructure();
    //Original name: SW-SWITCH
    private SwSwitchIvvs0212 swSwitch = new SwSwitchIvvs0212();
    //Original name: INDICI
    private Indici indici = new Indici();
    //Original name: LIMITE-VARIABILI-UNZIPPED
    private short limiteVariabiliUnzipped = ((short)100);
    //Original name: WS-VARIABILI
    private WsVariabiliIvvs0212 wsVariabili = new WsVariabiliIvvs0212();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public String getDescrizErrDb2Formatted() {
        return Functions.padBlanks(getDescrizErrDb2(), Len.DESCRIZ_ERR_DB2);
    }

    public short getLimiteVariabiliUnzipped() {
        return this.limiteVariabiliUnzipped;
    }

    @Override
    public int getCodCompagniaAnia() {
        return ctrlAutOper.getCaoCodCompagniaAnia();
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.ctrlAutOper.setCaoCodCompagniaAnia(codCompagniaAnia);
    }

    @Override
    public String getCodCond() {
        return ctrlAutOper.getCaoCodCond();
    }

    @Override
    public void setCodCond(String codCond) {
        this.ctrlAutOper.setCaoCodCond(codCond);
    }

    @Override
    public String getCodCondObj() {
        if (indCtrlAutOper.getCodCond() >= 0) {
            return getCodCond();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCondObj(String codCondObj) {
        if (codCondObj != null) {
            setCodCond(codCondObj);
            indCtrlAutOper.setCodCond(((short)0));
        }
        else {
            indCtrlAutOper.setCodCond(((short)-1));
        }
    }

    @Override
    public int getCodErrore() {
        return ctrlAutOper.getCaoCodErrore();
    }

    @Override
    public void setCodErrore(int codErrore) {
        this.ctrlAutOper.setCaoCodErrore(codErrore);
    }

    @Override
    public int getCodLivAut() {
        return ctrlAutOper.getCaoCodLivAut();
    }

    @Override
    public void setCodLivAut(int codLivAut) {
        this.ctrlAutOper.setCaoCodLivAut(codLivAut);
    }

    public CtrlAutOper getCtrlAutOper() {
        return ctrlAutOper;
    }

    @Override
    public int getIdCtrlAutOper() {
        return ctrlAutOper.getCaoIdCtrlAutOper();
    }

    @Override
    public void setIdCtrlAutOper(int idCtrlAutOper) {
        this.ctrlAutOper.setCaoIdCtrlAutOper(idCtrlAutOper);
    }

    public IndCtrlAutOper getIndCtrlAutOper() {
        return indCtrlAutOper;
    }

    public Indici getIndici() {
        return indici;
    }

    @Override
    public String getKeyAutOper1() {
        return ctrlAutOper.getCaoKeyAutOper1();
    }

    @Override
    public void setKeyAutOper1(String keyAutOper1) {
        this.ctrlAutOper.setCaoKeyAutOper1(keyAutOper1);
    }

    @Override
    public String getKeyAutOper1Obj() {
        if (indCtrlAutOper.getKeyAutOper1() >= 0) {
            return getKeyAutOper1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyAutOper1Obj(String keyAutOper1Obj) {
        if (keyAutOper1Obj != null) {
            setKeyAutOper1(keyAutOper1Obj);
            indCtrlAutOper.setKeyAutOper1(((short)0));
        }
        else {
            indCtrlAutOper.setKeyAutOper1(((short)-1));
        }
    }

    @Override
    public String getKeyAutOper2() {
        return ctrlAutOper.getCaoKeyAutOper2();
    }

    @Override
    public void setKeyAutOper2(String keyAutOper2) {
        this.ctrlAutOper.setCaoKeyAutOper2(keyAutOper2);
    }

    @Override
    public String getKeyAutOper2Obj() {
        if (indCtrlAutOper.getKeyAutOper2() >= 0) {
            return getKeyAutOper2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyAutOper2Obj(String keyAutOper2Obj) {
        if (keyAutOper2Obj != null) {
            setKeyAutOper2(keyAutOper2Obj);
            indCtrlAutOper.setKeyAutOper2(((short)0));
        }
        else {
            indCtrlAutOper.setKeyAutOper2(((short)-1));
        }
    }

    @Override
    public String getKeyAutOper3() {
        return ctrlAutOper.getCaoKeyAutOper3();
    }

    @Override
    public void setKeyAutOper3(String keyAutOper3) {
        this.ctrlAutOper.setCaoKeyAutOper3(keyAutOper3);
    }

    @Override
    public String getKeyAutOper3Obj() {
        if (indCtrlAutOper.getKeyAutOper3() >= 0) {
            return getKeyAutOper3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyAutOper3Obj(String keyAutOper3Obj) {
        if (keyAutOper3Obj != null) {
            setKeyAutOper3(keyAutOper3Obj);
            indCtrlAutOper.setKeyAutOper3(((short)0));
        }
        else {
            indCtrlAutOper.setKeyAutOper3(((short)-1));
        }
    }

    @Override
    public String getKeyAutOper4() {
        return ctrlAutOper.getCaoKeyAutOper4();
    }

    @Override
    public void setKeyAutOper4(String keyAutOper4) {
        this.ctrlAutOper.setCaoKeyAutOper4(keyAutOper4);
    }

    @Override
    public String getKeyAutOper4Obj() {
        if (indCtrlAutOper.getKeyAutOper4() >= 0) {
            return getKeyAutOper4();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyAutOper4Obj(String keyAutOper4Obj) {
        if (keyAutOper4Obj != null) {
            setKeyAutOper4(keyAutOper4Obj);
            indCtrlAutOper.setKeyAutOper4(((short)0));
        }
        else {
            indCtrlAutOper.setKeyAutOper4(((short)-1));
        }
    }

    @Override
    public String getKeyAutOper5() {
        return ctrlAutOper.getCaoKeyAutOper5();
    }

    @Override
    public void setKeyAutOper5(String keyAutOper5) {
        this.ctrlAutOper.setCaoKeyAutOper5(keyAutOper5);
    }

    @Override
    public String getKeyAutOper5Obj() {
        if (indCtrlAutOper.getKeyAutOper5() >= 0) {
            return getKeyAutOper5();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyAutOper5Obj(String keyAutOper5Obj) {
        if (keyAutOper5Obj != null) {
            setKeyAutOper5(keyAutOper5Obj);
            indCtrlAutOper.setKeyAutOper5(((short)0));
        }
        else {
            indCtrlAutOper.setKeyAutOper5(((short)-1));
        }
    }

    @Override
    public String getModuloVerifica() {
        return ctrlAutOper.getCaoModuloVerifica();
    }

    @Override
    public void setModuloVerifica(String moduloVerifica) {
        this.ctrlAutOper.setCaoModuloVerifica(moduloVerifica);
    }

    @Override
    public String getModuloVerificaObj() {
        if (indCtrlAutOper.getModuloVerifica() >= 0) {
            return getModuloVerifica();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModuloVerificaObj(String moduloVerificaObj) {
        if (moduloVerificaObj != null) {
            setModuloVerifica(moduloVerificaObj);
            indCtrlAutOper.setModuloVerifica(((short)0));
        }
        else {
            indCtrlAutOper.setModuloVerifica(((short)-1));
        }
    }

    @Override
    public String getParamAutOperVchar() {
        return ctrlAutOper.getCaoParamAutOperVcharFormatted();
    }

    @Override
    public void setParamAutOperVchar(String paramAutOperVchar) {
        this.ctrlAutOper.setCaoParamAutOperVcharFormatted(paramAutOperVchar);
    }

    @Override
    public String getParamAutOperVcharObj() {
        if (indCtrlAutOper.getParamAutOper() >= 0) {
            return getParamAutOperVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setParamAutOperVcharObj(String paramAutOperVcharObj) {
        if (paramAutOperVcharObj != null) {
            setParamAutOperVchar(paramAutOperVcharObj);
            indCtrlAutOper.setParamAutOper(((short)0));
        }
        else {
            indCtrlAutOper.setParamAutOper(((short)-1));
        }
    }

    @Override
    public short getProgCond() {
        return ctrlAutOper.getCaoProgCond().getCaoProgCond();
    }

    @Override
    public void setProgCond(short progCond) {
        this.ctrlAutOper.getCaoProgCond().setCaoProgCond(progCond);
    }

    @Override
    public Short getProgCondObj() {
        if (indCtrlAutOper.getProgCond() >= 0) {
            return ((Short)getProgCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setProgCondObj(Short progCondObj) {
        if (progCondObj != null) {
            setProgCond(((short)progCondObj));
            indCtrlAutOper.setProgCond(((short)0));
        }
        else {
            indCtrlAutOper.setProgCond(((short)-1));
        }
    }

    @Override
    public char getRisultCond() {
        return ctrlAutOper.getCaoRisultCond();
    }

    @Override
    public void setRisultCond(char risultCond) {
        this.ctrlAutOper.setCaoRisultCond(risultCond);
    }

    @Override
    public Character getRisultCondObj() {
        if (indCtrlAutOper.getRisultCond() >= 0) {
            return ((Character)getRisultCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisultCondObj(Character risultCondObj) {
        if (risultCondObj != null) {
            setRisultCond(((char)risultCondObj));
            indCtrlAutOper.setRisultCond(((short)0));
        }
        else {
            indCtrlAutOper.setRisultCond(((short)-1));
        }
    }

    @Override
    public char getStatoAttivazione() {
        return ctrlAutOper.getCaoStatoAttivazione();
    }

    @Override
    public void setStatoAttivazione(char statoAttivazione) {
        this.ctrlAutOper.setCaoStatoAttivazione(statoAttivazione);
    }

    @Override
    public Character getStatoAttivazioneObj() {
        if (indCtrlAutOper.getStatoAttivazione() >= 0) {
            return ((Character)getStatoAttivazione());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStatoAttivazioneObj(Character statoAttivazioneObj) {
        if (statoAttivazioneObj != null) {
            setStatoAttivazione(((char)statoAttivazioneObj));
            indCtrlAutOper.setStatoAttivazione(((short)0));
        }
        else {
            indCtrlAutOper.setStatoAttivazione(((short)-1));
        }
    }

    public SwSwitchIvvs0212 getSwSwitch() {
        return swSwitch;
    }

    @Override
    public String getTpMotDeroga() {
        return ctrlAutOper.getCaoTpMotDeroga();
    }

    @Override
    public void setTpMotDeroga(String tpMotDeroga) {
        this.ctrlAutOper.setCaoTpMotDeroga(tpMotDeroga);
    }

    @Override
    public int getTpMovi() {
        return ctrlAutOper.getCaoTpMovi().getCaoTpMovi();
    }

    @Override
    public void setTpMovi(int tpMovi) {
        this.ctrlAutOper.getCaoTpMovi().setCaoTpMovi(tpMovi);
    }

    @Override
    public Integer getTpMoviObj() {
        if (indCtrlAutOper.getTpMovi() >= 0) {
            return ((Integer)getTpMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMoviObj(Integer tpMoviObj) {
        if (tpMoviObj != null) {
            setTpMovi(((int)tpMoviObj));
            indCtrlAutOper.setTpMovi(((short)0));
        }
        else {
            indCtrlAutOper.setTpMovi(((short)-1));
        }
    }

    public UnzipStructure getUnzipStructure() {
        return unzipStructure;
    }

    public WsVariabiliIvvs0212 getWsVariabili() {
        return wsVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_SQLCODE = 5;
        public static final int DESCRIZ_ERR_DB2 = 300;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
