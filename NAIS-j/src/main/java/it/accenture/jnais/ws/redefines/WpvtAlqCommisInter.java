package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-ALQ-COMMIS-INTER<br>
 * Variable: WPVT-ALQ-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtAlqCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtAlqCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ALQ_COMMIS_INTER;
    }

    public void setWpvtAlqCommisInter(AfDecimal wpvtAlqCommisInter) {
        writeDecimalAsPacked(Pos.WPVT_ALQ_COMMIS_INTER, wpvtAlqCommisInter.copy());
    }

    public void setWpvtAlqCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ALQ_COMMIS_INTER, Pos.WPVT_ALQ_COMMIS_INTER);
    }

    /**Original name: WPVT-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getWpvtAlqCommisInter() {
        return readPackedAsDecimal(Pos.WPVT_ALQ_COMMIS_INTER, Len.Int.WPVT_ALQ_COMMIS_INTER, Len.Fract.WPVT_ALQ_COMMIS_INTER);
    }

    public byte[] getWpvtAlqCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ALQ_COMMIS_INTER, Pos.WPVT_ALQ_COMMIS_INTER);
        return buffer;
    }

    public void initWpvtAlqCommisInterSpaces() {
        fill(Pos.WPVT_ALQ_COMMIS_INTER, Len.WPVT_ALQ_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWpvtAlqCommisInterNull(String wpvtAlqCommisInterNull) {
        writeString(Pos.WPVT_ALQ_COMMIS_INTER_NULL, wpvtAlqCommisInterNull, Len.WPVT_ALQ_COMMIS_INTER_NULL);
    }

    /**Original name: WPVT-ALQ-COMMIS-INTER-NULL<br>*/
    public String getWpvtAlqCommisInterNull() {
        return readString(Pos.WPVT_ALQ_COMMIS_INTER_NULL, Len.WPVT_ALQ_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_COMMIS_INTER = 1;
        public static final int WPVT_ALQ_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_COMMIS_INTER = 4;
        public static final int WPVT_ALQ_COMMIS_INTER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
