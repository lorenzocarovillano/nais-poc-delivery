package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-VAL-NUM<br>
 * Variable: PCA-VAL-NUM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaValNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaValNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_VAL_NUM;
    }

    public void setPcaValNum(long pcaValNum) {
        writeLongAsPacked(Pos.PCA_VAL_NUM, pcaValNum, Len.Int.PCA_VAL_NUM);
    }

    public void setPcaValNumFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_VAL_NUM, Pos.PCA_VAL_NUM);
    }

    /**Original name: PCA-VAL-NUM<br>*/
    public long getPcaValNum() {
        return readPackedAsLong(Pos.PCA_VAL_NUM, Len.Int.PCA_VAL_NUM);
    }

    public byte[] getPcaValNumAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_VAL_NUM, Pos.PCA_VAL_NUM);
        return buffer;
    }

    public void setPcaValNumNull(String pcaValNumNull) {
        writeString(Pos.PCA_VAL_NUM_NULL, pcaValNumNull, Len.PCA_VAL_NUM_NULL);
    }

    /**Original name: PCA-VAL-NUM-NULL<br>*/
    public String getPcaValNumNull() {
        return readString(Pos.PCA_VAL_NUM_NULL, Len.PCA_VAL_NUM_NULL);
    }

    public String getPcaValNumNullFormatted() {
        return Functions.padBlanks(getPcaValNumNull(), Len.PCA_VAL_NUM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_VAL_NUM = 1;
        public static final int PCA_VAL_NUM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_VAL_NUM = 8;
        public static final int PCA_VAL_NUM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_VAL_NUM = 14;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
