package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L11-ID-OGG-1RIO<br>
 * Variable: L11-ID-OGG-1RIO from program LCCS0024<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L11IdOgg1rio extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L11IdOgg1rio() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L11_ID_OGG1RIO;
    }

    public void setL11IdOgg1rio(int l11IdOgg1rio) {
        writeIntAsPacked(Pos.L11_ID_OGG1RIO, l11IdOgg1rio, Len.Int.L11_ID_OGG1RIO);
    }

    public void setL11IdOgg1rioFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L11_ID_OGG1RIO, Pos.L11_ID_OGG1RIO);
    }

    /**Original name: L11-ID-OGG-1RIO<br>*/
    public int getL11IdOgg1rio() {
        return readPackedAsInt(Pos.L11_ID_OGG1RIO, Len.Int.L11_ID_OGG1RIO);
    }

    public byte[] getL11IdOgg1rioAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L11_ID_OGG1RIO, Pos.L11_ID_OGG1RIO);
        return buffer;
    }

    public void setL11IdOgg1rioNull(String l11IdOgg1rioNull) {
        writeString(Pos.L11_ID_OGG1RIO_NULL, l11IdOgg1rioNull, Len.L11_ID_OGG1RIO_NULL);
    }

    /**Original name: L11-ID-OGG-1RIO-NULL<br>*/
    public String getL11IdOgg1rioNull() {
        return readString(Pos.L11_ID_OGG1RIO_NULL, Len.L11_ID_OGG1RIO_NULL);
    }

    public String getL11IdOgg1rioNullFormatted() {
        return Functions.padBlanks(getL11IdOgg1rioNull(), Len.L11_ID_OGG1RIO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L11_ID_OGG1RIO = 1;
        public static final int L11_ID_OGG1RIO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L11_ID_OGG1RIO = 5;
        public static final int L11_ID_OGG1RIO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L11_ID_OGG1RIO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
