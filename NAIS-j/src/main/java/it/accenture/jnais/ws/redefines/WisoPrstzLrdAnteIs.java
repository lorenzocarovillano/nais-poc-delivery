package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-PRSTZ-LRD-ANTE-IS<br>
 * Variable: WISO-PRSTZ-LRD-ANTE-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoPrstzLrdAnteIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoPrstzLrdAnteIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_PRSTZ_LRD_ANTE_IS;
    }

    public void setWisoPrstzLrdAnteIs(AfDecimal wisoPrstzLrdAnteIs) {
        writeDecimalAsPacked(Pos.WISO_PRSTZ_LRD_ANTE_IS, wisoPrstzLrdAnteIs.copy());
    }

    public void setWisoPrstzLrdAnteIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_PRSTZ_LRD_ANTE_IS, Pos.WISO_PRSTZ_LRD_ANTE_IS);
    }

    /**Original name: WISO-PRSTZ-LRD-ANTE-IS<br>*/
    public AfDecimal getWisoPrstzLrdAnteIs() {
        return readPackedAsDecimal(Pos.WISO_PRSTZ_LRD_ANTE_IS, Len.Int.WISO_PRSTZ_LRD_ANTE_IS, Len.Fract.WISO_PRSTZ_LRD_ANTE_IS);
    }

    public byte[] getWisoPrstzLrdAnteIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_PRSTZ_LRD_ANTE_IS, Pos.WISO_PRSTZ_LRD_ANTE_IS);
        return buffer;
    }

    public void initWisoPrstzLrdAnteIsSpaces() {
        fill(Pos.WISO_PRSTZ_LRD_ANTE_IS, Len.WISO_PRSTZ_LRD_ANTE_IS, Types.SPACE_CHAR);
    }

    public void setWisoPrstzLrdAnteIsNull(String wisoPrstzLrdAnteIsNull) {
        writeString(Pos.WISO_PRSTZ_LRD_ANTE_IS_NULL, wisoPrstzLrdAnteIsNull, Len.WISO_PRSTZ_LRD_ANTE_IS_NULL);
    }

    /**Original name: WISO-PRSTZ-LRD-ANTE-IS-NULL<br>*/
    public String getWisoPrstzLrdAnteIsNull() {
        return readString(Pos.WISO_PRSTZ_LRD_ANTE_IS_NULL, Len.WISO_PRSTZ_LRD_ANTE_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_PRSTZ_LRD_ANTE_IS = 1;
        public static final int WISO_PRSTZ_LRD_ANTE_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_PRSTZ_LRD_ANTE_IS = 8;
        public static final int WISO_PRSTZ_LRD_ANTE_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_PRSTZ_LRD_ANTE_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_PRSTZ_LRD_ANTE_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
