package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: LOAC0002-TAB-FUNZ-DEROGA<br>
 * Variable: LOAC0002-TAB-FUNZ-DEROGA from program LOAS0310<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Loac0002TabFunzDeroga extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_FUNZ_DEROG_BLOCC_MAXOCCURS = 43;
    public static final int ELE_FUNZ_DEROGA_MAXOCCURS = 1;

    //==== CONSTRUCTORS ====
    public Loac0002TabFunzDeroga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LOAC0002_TAB_FUNZ_DEROGA;
    }

    @Override
    public void init() {
        int position = 1;
        writeInt(position, 6006, Len.Int.FLR1, SignType.NO_SIGN);
        position += Len.FLR1;
        writeInt(position, 5003, Len.Int.FLR2, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5005, Len.Int.FLR4, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5006, Len.Int.FLR6, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5008, Len.Int.FLR8, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 3010, Len.Int.FLR10, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 3012, Len.Int.FLR12, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5009, Len.Int.FLR14, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5010, Len.Int.FLR16, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 3018, Len.Int.FLR18, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 3019, Len.Int.FLR20, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5017, Len.Int.FLR22, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5018, Len.Int.FLR24, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 3015, Len.Int.FLR26, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 3017, Len.Int.FLR28, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5014, Len.Int.FLR30, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5015, Len.Int.FLR32, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5019, Len.Int.FLR34, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5020, Len.Int.FLR36, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5026, Len.Int.FLR38, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 5027, Len.Int.FLR40, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 6009, Len.Int.FLR42, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2028, Len.Int.FLR44, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2063, Len.Int.FLR46, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2071, Len.Int.FLR48, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2316, Len.Int.FLR50, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2317, Len.Int.FLR52, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2318, Len.Int.FLR54, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2319, Len.Int.FLR56, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2320, Len.Int.FLR58, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2321, Len.Int.FLR60, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2322, Len.Int.FLR62, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2323, Len.Int.FLR64, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2061, Len.Int.FLR66, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2062, Len.Int.FLR68, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2318, Len.Int.FLR70, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2319, Len.Int.FLR72, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2316, Len.Int.FLR74, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2317, Len.Int.FLR76, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2324, Len.Int.FLR78, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2325, Len.Int.FLR80, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2322, Len.Int.FLR82, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2323, Len.Int.FLR84, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
        position += Types.CHAR_SIZE;
        writeInt(position, 2072, Len.Int.FLR86, SignType.NO_SIGN);
        position += Len.FLR1;
        writeChar(position, 'B');
    }

    /**Original name: LOAC0002-FUNZ-ESEC<br>*/
    public int getFunzEsec(int funzEsecIdx) {
        int position = Pos.loac0002FunzEsec(funzEsecIdx - 1);
        return readNumDispUnsignedInt(position, Len.FUNZ_ESEC);
    }

    /**Original name: LOAC0002-FUNZ-DEROG<br>*/
    public int getFunzDerog(int funzDerogIdx1, int funzDerogIdx2) {
        int position = Pos.loac0002FunzDerog(funzDerogIdx1 - 1, funzDerogIdx2 - 1);
        return readNumDispUnsignedInt(position, Len.FUNZ_DEROG);
    }

    /**Original name: LOAC0002-TIPO-DEROGA<br>
	 * <pre>    TIPO DEROGA - 'N' NON BLOCCANTE - 'B' BLOCCANTE</pre>*/
    public char getTipoDeroga(int tipoDerogaIdx1, int tipoDerogaIdx2) {
        int position = Pos.loac0002TipoDeroga(tipoDerogaIdx1 - 1, tipoDerogaIdx2 - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LOAC0002_TAB_FUNZ_DEROGA = 1;
        public static final int FLR1 = LOAC0002_TAB_FUNZ_DEROGA;
        public static final int FLR2 = FLR1 + Len.FLR1;
        public static final int FLR3 = FLR2 + Len.FLR1;
        public static final int FLR4 = FLR3 + Len.FLR3;
        public static final int FLR5 = FLR4 + Len.FLR1;
        public static final int FLR6 = FLR5 + Len.FLR3;
        public static final int FLR7 = FLR6 + Len.FLR1;
        public static final int FLR8 = FLR7 + Len.FLR3;
        public static final int FLR9 = FLR8 + Len.FLR1;
        public static final int FLR10 = FLR9 + Len.FLR3;
        public static final int FLR11 = FLR10 + Len.FLR1;
        public static final int FLR12 = FLR11 + Len.FLR3;
        public static final int FLR13 = FLR12 + Len.FLR1;
        public static final int FLR14 = FLR13 + Len.FLR3;
        public static final int FLR15 = FLR14 + Len.FLR1;
        public static final int FLR16 = FLR15 + Len.FLR3;
        public static final int FLR17 = FLR16 + Len.FLR1;
        public static final int FLR18 = FLR17 + Len.FLR3;
        public static final int FLR19 = FLR18 + Len.FLR1;
        public static final int FLR20 = FLR19 + Len.FLR3;
        public static final int FLR21 = FLR20 + Len.FLR1;
        public static final int FLR22 = FLR21 + Len.FLR3;
        public static final int FLR23 = FLR22 + Len.FLR1;
        public static final int FLR24 = FLR23 + Len.FLR3;
        public static final int FLR25 = FLR24 + Len.FLR1;
        public static final int FLR26 = FLR25 + Len.FLR3;
        public static final int FLR27 = FLR26 + Len.FLR1;
        public static final int FLR28 = FLR27 + Len.FLR3;
        public static final int FLR29 = FLR28 + Len.FLR1;
        public static final int FLR30 = FLR29 + Len.FLR3;
        public static final int FLR31 = FLR30 + Len.FLR1;
        public static final int FLR32 = FLR31 + Len.FLR3;
        public static final int FLR33 = FLR32 + Len.FLR1;
        public static final int FLR34 = FLR33 + Len.FLR3;
        public static final int FLR35 = FLR34 + Len.FLR1;
        public static final int FLR36 = FLR35 + Len.FLR3;
        public static final int FLR37 = FLR36 + Len.FLR1;
        public static final int FLR38 = FLR37 + Len.FLR3;
        public static final int FLR39 = FLR38 + Len.FLR1;
        public static final int FLR40 = FLR39 + Len.FLR3;
        public static final int FLR41 = FLR40 + Len.FLR1;
        public static final int FLR42 = FLR41 + Len.FLR3;
        public static final int FLR43 = FLR42 + Len.FLR1;
        public static final int FLR44 = FLR43 + Len.FLR3;
        public static final int FLR45 = FLR44 + Len.FLR1;
        public static final int FLR46 = FLR45 + Len.FLR3;
        public static final int FLR47 = FLR46 + Len.FLR1;
        public static final int FLR48 = FLR47 + Len.FLR3;
        public static final int FLR49 = FLR48 + Len.FLR1;
        public static final int FLR50 = FLR49 + Len.FLR3;
        public static final int FLR51 = FLR50 + Len.FLR1;
        public static final int FLR52 = FLR51 + Len.FLR3;
        public static final int FLR53 = FLR52 + Len.FLR1;
        public static final int FLR54 = FLR53 + Len.FLR3;
        public static final int FLR55 = FLR54 + Len.FLR1;
        public static final int FLR56 = FLR55 + Len.FLR3;
        public static final int FLR57 = FLR56 + Len.FLR1;
        public static final int FLR58 = FLR57 + Len.FLR3;
        public static final int FLR59 = FLR58 + Len.FLR1;
        public static final int FLR60 = FLR59 + Len.FLR3;
        public static final int FLR61 = FLR60 + Len.FLR1;
        public static final int FLR62 = FLR61 + Len.FLR3;
        public static final int FLR63 = FLR62 + Len.FLR1;
        public static final int FLR64 = FLR63 + Len.FLR3;
        public static final int FLR65 = FLR64 + Len.FLR1;
        public static final int FLR66 = FLR65 + Len.FLR3;
        public static final int FLR67 = FLR66 + Len.FLR1;
        public static final int FLR68 = FLR67 + Len.FLR3;
        public static final int FLR69 = FLR68 + Len.FLR1;
        public static final int FLR70 = FLR69 + Len.FLR3;
        public static final int FLR71 = FLR70 + Len.FLR1;
        public static final int FLR72 = FLR71 + Len.FLR3;
        public static final int FLR73 = FLR72 + Len.FLR1;
        public static final int FLR74 = FLR73 + Len.FLR3;
        public static final int FLR75 = FLR74 + Len.FLR1;
        public static final int FLR76 = FLR75 + Len.FLR3;
        public static final int FLR77 = FLR76 + Len.FLR1;
        public static final int FLR78 = FLR77 + Len.FLR3;
        public static final int FLR79 = FLR78 + Len.FLR1;
        public static final int FLR80 = FLR79 + Len.FLR3;
        public static final int FLR81 = FLR80 + Len.FLR1;
        public static final int FLR82 = FLR81 + Len.FLR3;
        public static final int FLR83 = FLR82 + Len.FLR1;
        public static final int FLR84 = FLR83 + Len.FLR3;
        public static final int FLR85 = FLR84 + Len.FLR1;
        public static final int FLR86 = FLR85 + Len.FLR3;
        public static final int FLR87 = FLR86 + Len.FLR1;
        public static final int LOAC0002_TAB_FUNZ_DEROGA_R = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int loac0002EleFunzDeroga(int idx) {
            return LOAC0002_TAB_FUNZ_DEROGA_R + idx * Len.ELE_FUNZ_DEROGA;
        }

        public static int loac0002FunzEsec(int idx) {
            return loac0002EleFunzDeroga(idx);
        }

        public static int loac0002TabFunzDerogBlocc(int idx1, int idx2) {
            return loac0002FunzEsec(idx1) + Len.FUNZ_ESEC + idx2 * Len.TAB_FUNZ_DEROG_BLOCC;
        }

        public static int loac0002FunzDerog(int idx1, int idx2) {
            return loac0002TabFunzDerogBlocc(idx1, idx2);
        }

        public static int loac0002TipoDeroga(int idx1, int idx2) {
            return loac0002FunzDerog(idx1, idx2) + Len.FUNZ_DEROG;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 5;
        public static final int FLR3 = 1;
        public static final int FUNZ_ESEC = 5;
        public static final int FUNZ_DEROG = 5;
        public static final int TIPO_DEROGA = 1;
        public static final int TAB_FUNZ_DEROG_BLOCC = FUNZ_DEROG + TIPO_DEROGA;
        public static final int ELE_FUNZ_DEROGA = FUNZ_ESEC + Loac0002TabFunzDeroga.TAB_FUNZ_DEROG_BLOCC_MAXOCCURS * TAB_FUNZ_DEROG_BLOCC;
        public static final int LOAC0002_TAB_FUNZ_DEROGA = 44 * FLR1 + 43 * FLR3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int FLR1 = 5;
            public static final int FLR2 = 5;
            public static final int FLR4 = 5;
            public static final int FLR6 = 5;
            public static final int FLR8 = 5;
            public static final int FLR10 = 5;
            public static final int FLR12 = 5;
            public static final int FLR14 = 5;
            public static final int FLR16 = 5;
            public static final int FLR18 = 5;
            public static final int FLR20 = 5;
            public static final int FLR22 = 5;
            public static final int FLR24 = 5;
            public static final int FLR26 = 5;
            public static final int FLR28 = 5;
            public static final int FLR30 = 5;
            public static final int FLR32 = 5;
            public static final int FLR34 = 5;
            public static final int FLR36 = 5;
            public static final int FLR38 = 5;
            public static final int FLR40 = 5;
            public static final int FLR42 = 5;
            public static final int FLR44 = 5;
            public static final int FLR46 = 5;
            public static final int FLR48 = 5;
            public static final int FLR50 = 5;
            public static final int FLR52 = 5;
            public static final int FLR54 = 5;
            public static final int FLR56 = 5;
            public static final int FLR58 = 5;
            public static final int FLR60 = 5;
            public static final int FLR62 = 5;
            public static final int FLR64 = 5;
            public static final int FLR66 = 5;
            public static final int FLR68 = 5;
            public static final int FLR70 = 5;
            public static final int FLR72 = 5;
            public static final int FLR74 = 5;
            public static final int FLR76 = 5;
            public static final int FLR78 = 5;
            public static final int FLR80 = 5;
            public static final int FLR82 = 5;
            public static final int FLR84 = 5;
            public static final int FLR86 = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
