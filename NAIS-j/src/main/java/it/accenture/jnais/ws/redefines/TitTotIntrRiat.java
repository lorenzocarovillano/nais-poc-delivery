package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-INTR-RIAT<br>
 * Variable: TIT-TOT-INTR-RIAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_INTR_RIAT;
    }

    public void setTitTotIntrRiat(AfDecimal titTotIntrRiat) {
        writeDecimalAsPacked(Pos.TIT_TOT_INTR_RIAT, titTotIntrRiat.copy());
    }

    public void setTitTotIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_INTR_RIAT, Pos.TIT_TOT_INTR_RIAT);
    }

    /**Original name: TIT-TOT-INTR-RIAT<br>*/
    public AfDecimal getTitTotIntrRiat() {
        return readPackedAsDecimal(Pos.TIT_TOT_INTR_RIAT, Len.Int.TIT_TOT_INTR_RIAT, Len.Fract.TIT_TOT_INTR_RIAT);
    }

    public byte[] getTitTotIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_INTR_RIAT, Pos.TIT_TOT_INTR_RIAT);
        return buffer;
    }

    public void setTitTotIntrRiatNull(String titTotIntrRiatNull) {
        writeString(Pos.TIT_TOT_INTR_RIAT_NULL, titTotIntrRiatNull, Len.TIT_TOT_INTR_RIAT_NULL);
    }

    /**Original name: TIT-TOT-INTR-RIAT-NULL<br>*/
    public String getTitTotIntrRiatNull() {
        return readString(Pos.TIT_TOT_INTR_RIAT_NULL, Len.TIT_TOT_INTR_RIAT_NULL);
    }

    public String getTitTotIntrRiatNullFormatted() {
        return Functions.padBlanks(getTitTotIntrRiatNull(), Len.TIT_TOT_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_RIAT = 1;
        public static final int TIT_TOT_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_RIAT = 8;
        public static final int TIT_TOT_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
