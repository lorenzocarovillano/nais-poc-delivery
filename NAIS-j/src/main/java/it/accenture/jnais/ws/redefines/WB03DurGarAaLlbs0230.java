package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-GAR-AA<br>
 * Variable: W-B03-DUR-GAR-AA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurGarAaLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurGarAaLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_GAR_AA;
    }

    public void setwB03DurGarAa(int wB03DurGarAa) {
        writeIntAsPacked(Pos.W_B03_DUR_GAR_AA, wB03DurGarAa, Len.Int.W_B03_DUR_GAR_AA);
    }

    /**Original name: W-B03-DUR-GAR-AA<br>*/
    public int getwB03DurGarAa() {
        return readPackedAsInt(Pos.W_B03_DUR_GAR_AA, Len.Int.W_B03_DUR_GAR_AA);
    }

    public byte[] getwB03DurGarAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_GAR_AA, Pos.W_B03_DUR_GAR_AA);
        return buffer;
    }

    public void setwB03DurGarAaNull(String wB03DurGarAaNull) {
        writeString(Pos.W_B03_DUR_GAR_AA_NULL, wB03DurGarAaNull, Len.W_B03_DUR_GAR_AA_NULL);
    }

    /**Original name: W-B03-DUR-GAR-AA-NULL<br>*/
    public String getwB03DurGarAaNull() {
        return readString(Pos.W_B03_DUR_GAR_AA_NULL, Len.W_B03_DUR_GAR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GAR_AA = 1;
        public static final int W_B03_DUR_GAR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GAR_AA = 3;
        public static final int W_B03_DUR_GAR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_GAR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
