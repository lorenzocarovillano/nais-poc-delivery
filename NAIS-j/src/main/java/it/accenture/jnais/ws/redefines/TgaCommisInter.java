package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-COMMIS-INTER<br>
 * Variable: TGA-COMMIS-INTER from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_COMMIS_INTER;
    }

    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        writeDecimalAsPacked(Pos.TGA_COMMIS_INTER, tgaCommisInter.copy());
    }

    public void setTgaCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_COMMIS_INTER, Pos.TGA_COMMIS_INTER);
    }

    /**Original name: TGA-COMMIS-INTER<br>*/
    public AfDecimal getTgaCommisInter() {
        return readPackedAsDecimal(Pos.TGA_COMMIS_INTER, Len.Int.TGA_COMMIS_INTER, Len.Fract.TGA_COMMIS_INTER);
    }

    public byte[] getTgaCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_COMMIS_INTER, Pos.TGA_COMMIS_INTER);
        return buffer;
    }

    public void setTgaCommisInterNull(String tgaCommisInterNull) {
        writeString(Pos.TGA_COMMIS_INTER_NULL, tgaCommisInterNull, Len.TGA_COMMIS_INTER_NULL);
    }

    /**Original name: TGA-COMMIS-INTER-NULL<br>*/
    public String getTgaCommisInterNull() {
        return readString(Pos.TGA_COMMIS_INTER_NULL, Len.TGA_COMMIS_INTER_NULL);
    }

    public String getTgaCommisInterNullFormatted() {
        return Functions.padBlanks(getTgaCommisInterNull(), Len.TGA_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_COMMIS_INTER = 1;
        public static final int TGA_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_COMMIS_INTER = 8;
        public static final int TGA_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
