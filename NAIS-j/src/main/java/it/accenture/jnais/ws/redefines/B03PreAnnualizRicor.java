package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-ANNUALIZ-RICOR<br>
 * Variable: B03-PRE-ANNUALIZ-RICOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PreAnnualizRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PreAnnualizRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_ANNUALIZ_RICOR;
    }

    public void setB03PreAnnualizRicor(AfDecimal b03PreAnnualizRicor) {
        writeDecimalAsPacked(Pos.B03_PRE_ANNUALIZ_RICOR, b03PreAnnualizRicor.copy());
    }

    public void setB03PreAnnualizRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_ANNUALIZ_RICOR, Pos.B03_PRE_ANNUALIZ_RICOR);
    }

    /**Original name: B03-PRE-ANNUALIZ-RICOR<br>*/
    public AfDecimal getB03PreAnnualizRicor() {
        return readPackedAsDecimal(Pos.B03_PRE_ANNUALIZ_RICOR, Len.Int.B03_PRE_ANNUALIZ_RICOR, Len.Fract.B03_PRE_ANNUALIZ_RICOR);
    }

    public byte[] getB03PreAnnualizRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_ANNUALIZ_RICOR, Pos.B03_PRE_ANNUALIZ_RICOR);
        return buffer;
    }

    public void setB03PreAnnualizRicorNull(String b03PreAnnualizRicorNull) {
        writeString(Pos.B03_PRE_ANNUALIZ_RICOR_NULL, b03PreAnnualizRicorNull, Len.B03_PRE_ANNUALIZ_RICOR_NULL);
    }

    /**Original name: B03-PRE-ANNUALIZ-RICOR-NULL<br>*/
    public String getB03PreAnnualizRicorNull() {
        return readString(Pos.B03_PRE_ANNUALIZ_RICOR_NULL, Len.B03_PRE_ANNUALIZ_RICOR_NULL);
    }

    public String getB03PreAnnualizRicorNullFormatted() {
        return Functions.padBlanks(getB03PreAnnualizRicorNull(), Len.B03_PRE_ANNUALIZ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_ANNUALIZ_RICOR = 1;
        public static final int B03_PRE_ANNUALIZ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_ANNUALIZ_RICOR = 8;
        public static final int B03_PRE_ANNUALIZ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_ANNUALIZ_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_ANNUALIZ_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
