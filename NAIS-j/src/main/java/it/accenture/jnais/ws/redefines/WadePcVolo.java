package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PC-VOLO<br>
 * Variable: WADE-PC-VOLO from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePcVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePcVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PC_VOLO;
    }

    public void setWadePcVolo(AfDecimal wadePcVolo) {
        writeDecimalAsPacked(Pos.WADE_PC_VOLO, wadePcVolo.copy());
    }

    public void setWadePcVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PC_VOLO, Pos.WADE_PC_VOLO);
    }

    /**Original name: WADE-PC-VOLO<br>*/
    public AfDecimal getWadePcVolo() {
        return readPackedAsDecimal(Pos.WADE_PC_VOLO, Len.Int.WADE_PC_VOLO, Len.Fract.WADE_PC_VOLO);
    }

    public byte[] getWadePcVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PC_VOLO, Pos.WADE_PC_VOLO);
        return buffer;
    }

    public void initWadePcVoloSpaces() {
        fill(Pos.WADE_PC_VOLO, Len.WADE_PC_VOLO, Types.SPACE_CHAR);
    }

    public void setWadePcVoloNull(String wadePcVoloNull) {
        writeString(Pos.WADE_PC_VOLO_NULL, wadePcVoloNull, Len.WADE_PC_VOLO_NULL);
    }

    /**Original name: WADE-PC-VOLO-NULL<br>*/
    public String getWadePcVoloNull() {
        return readString(Pos.WADE_PC_VOLO_NULL, Len.WADE_PC_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PC_VOLO = 1;
        public static final int WADE_PC_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PC_VOLO = 4;
        public static final int WADE_PC_VOLO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
