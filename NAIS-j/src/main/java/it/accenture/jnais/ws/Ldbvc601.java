package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbvc601TgaOutput;

/**Original name: LDBVC601<br>
 * Variable: LDBVC601 from copybook LDBVC601<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbvc601 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBVC601-TGA-OUTPUT
    private Ldbvc601TgaOutput ldbvc601TgaOutput = new Ldbvc601TgaOutput();
    //Original name: LC601-TP-STAT-BUS
    private String lc601TpStatBus = DefaultValues.stringVal(Len.LC601_TP_STAT_BUS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVC601;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbvc601Bytes(buf);
    }

    public void setLdbvc601Bytes(byte[] buffer) {
        setLdbvc601Bytes(buffer, 1);
    }

    public byte[] getLdbvc601Bytes() {
        byte[] buffer = new byte[Len.LDBVC601];
        return getLdbvc601Bytes(buffer, 1);
    }

    public void setLdbvc601Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbvc601TgaOutput.setLdbvc601TgaOutputBytes(buffer, position);
        position += Ldbvc601TgaOutput.Len.LDBVC601_TGA_OUTPUT;
        setLdbvc601StbOutputBytes(buffer, position);
    }

    public byte[] getLdbvc601Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbvc601TgaOutput.getLdbvc601TgaOutputBytes(buffer, position);
        position += Ldbvc601TgaOutput.Len.LDBVC601_TGA_OUTPUT;
        getLdbvc601StbOutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbvc601StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        lc601TpStatBus = MarshalByte.readString(buffer, position, Len.LC601_TP_STAT_BUS);
    }

    public byte[] getLdbvc601StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, lc601TpStatBus, Len.LC601_TP_STAT_BUS);
        return buffer;
    }

    public void setLc601TpStatBus(String lc601TpStatBus) {
        this.lc601TpStatBus = Functions.subString(lc601TpStatBus, Len.LC601_TP_STAT_BUS);
    }

    public String getLc601TpStatBus() {
        return this.lc601TpStatBus;
    }

    public Ldbvc601TgaOutput getLdbvc601TgaOutput() {
        return ldbvc601TgaOutput;
    }

    @Override
    public byte[] serialize() {
        return getLdbvc601Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LC601_TP_STAT_BUS = 2;
        public static final int LDBVC601_STB_OUTPUT = LC601_TP_STAT_BUS;
        public static final int LDBVC601 = Ldbvc601TgaOutput.Len.LDBVC601_TGA_OUTPUT + LDBVC601_STB_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
