package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-RIT-ACC<br>
 * Variable: TCL-IMP-RIT-ACC from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpRitAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpRitAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_RIT_ACC;
    }

    public void setTclImpRitAcc(AfDecimal tclImpRitAcc) {
        writeDecimalAsPacked(Pos.TCL_IMP_RIT_ACC, tclImpRitAcc.copy());
    }

    public void setTclImpRitAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_RIT_ACC, Pos.TCL_IMP_RIT_ACC);
    }

    /**Original name: TCL-IMP-RIT-ACC<br>*/
    public AfDecimal getTclImpRitAcc() {
        return readPackedAsDecimal(Pos.TCL_IMP_RIT_ACC, Len.Int.TCL_IMP_RIT_ACC, Len.Fract.TCL_IMP_RIT_ACC);
    }

    public byte[] getTclImpRitAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_RIT_ACC, Pos.TCL_IMP_RIT_ACC);
        return buffer;
    }

    public void setTclImpRitAccNull(String tclImpRitAccNull) {
        writeString(Pos.TCL_IMP_RIT_ACC_NULL, tclImpRitAccNull, Len.TCL_IMP_RIT_ACC_NULL);
    }

    /**Original name: TCL-IMP-RIT-ACC-NULL<br>*/
    public String getTclImpRitAccNull() {
        return readString(Pos.TCL_IMP_RIT_ACC_NULL, Len.TCL_IMP_RIT_ACC_NULL);
    }

    public String getTclImpRitAccNullFormatted() {
        return Functions.padBlanks(getTclImpRitAccNull(), Len.TCL_IMP_RIT_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIT_ACC = 1;
        public static final int TCL_IMP_RIT_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIT_ACC = 8;
        public static final int TCL_IMP_RIT_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIT_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIT_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
