package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettQuestDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndImpstSost;
import it.accenture.jnais.copy.Ldbv5001;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS5000<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs5000Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-DETT-QUEST
    private IndImpstSost indDettQuest = new IndImpstSost();
    //Original name: DETT-QUEST-DB
    private DettQuestDb dettQuestDb = new DettQuestDb();
    //Original name: LDBV5001
    private Ldbv5001 ldbv5001 = new Ldbv5001();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public DettQuestDb getDettQuestDb() {
        return dettQuestDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndImpstSost getIndDettQuest() {
        return indDettQuest;
    }

    public Ldbv5001 getLdbv5001() {
        return ldbv5001;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
