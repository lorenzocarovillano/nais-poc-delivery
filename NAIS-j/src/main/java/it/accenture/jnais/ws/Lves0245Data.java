package it.accenture.jnais.ws;

import it.accenture.jnais.ws.enums.FlRicercaLves0245;
import it.accenture.jnais.ws.enums.WsCompagnia;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpMezPag;
import it.accenture.jnais.ws.enums.WsTpRappAna;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVES0245<br>
 * Generated as a class for rule WS.<br>*/
public class Lves0245Data {

    //==== PROPERTIES ====
    //Original name: WS-VARIABILI
    private WsVariabiliLves0245 wsVariabili = new WsVariabiliLves0245();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 * --  Tipologiche di PTF
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-RAPP-ANA<br>
	 * <pre>*****************************************************************
	 *     TP_RAPP_ANA (Tipo Rapporto Anagrafico)
	 * *****************************************************************</pre>*/
    private WsTpRappAna wsTpRappAna = new WsTpRappAna();
    /**Original name: WS-TP-MEZ-PAG<br>
	 * <pre>*****************************************************************
	 *     TP_MEZ_PAG (Tipo Mezzo Pagamento) versione BNL
	 * *****************************************************************</pre>*/
    private WsTpMezPag wsTpMezPag = new WsTpMezPag();
    //Original name: IX-INDICI
    private IxIndiciLves0245 ixIndici = new IxIndiciLves0245();
    /**Original name: FL-RICERCA<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG E SWITCH
	 * ----------------------------------------------------------------*</pre>*/
    private FlRicercaLves0245 flRicerca = new FlRicercaLves0245();
    //Original name: WK-QUIETANZAMENTO
    private boolean wkQuietanzamento = false;
    //Original name: WK-GETRA
    private boolean wkGetra = false;
    //Original name: WS-COMPAGNIA
    private WsCompagnia wsCompagnia = new WsCompagnia();

    //==== METHODS ====
    public void setWkQuietanzamento(boolean wkQuietanzamento) {
        this.wkQuietanzamento = wkQuietanzamento;
    }

    public boolean isWkQuietanzamento() {
        return this.wkQuietanzamento;
    }

    public void setWkGetra(boolean wkGetra) {
        this.wkGetra = wkGetra;
    }

    public boolean isWkGetra() {
        return this.wkGetra;
    }

    public FlRicercaLves0245 getFlRicerca() {
        return flRicerca;
    }

    public IxIndiciLves0245 getIxIndici() {
        return ixIndici;
    }

    public WsCompagnia getWsCompagnia() {
        return wsCompagnia;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpMezPag getWsTpMezPag() {
        return wsTpMezPag;
    }

    public WsTpRappAna getWsTpRappAna() {
        return wsTpRappAna;
    }

    public WsVariabiliLves0245 getWsVariabili() {
        return wsVariabili;
    }
}
