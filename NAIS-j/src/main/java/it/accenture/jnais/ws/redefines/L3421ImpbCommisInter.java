package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMPB-COMMIS-INTER<br>
 * Variable: L3421-IMPB-COMMIS-INTER from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpbCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpbCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMPB_COMMIS_INTER;
    }

    public void setL3421ImpbCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMPB_COMMIS_INTER, Pos.L3421_IMPB_COMMIS_INTER);
    }

    /**Original name: L3421-IMPB-COMMIS-INTER<br>*/
    public AfDecimal getL3421ImpbCommisInter() {
        return readPackedAsDecimal(Pos.L3421_IMPB_COMMIS_INTER, Len.Int.L3421_IMPB_COMMIS_INTER, Len.Fract.L3421_IMPB_COMMIS_INTER);
    }

    public byte[] getL3421ImpbCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMPB_COMMIS_INTER, Pos.L3421_IMPB_COMMIS_INTER);
        return buffer;
    }

    /**Original name: L3421-IMPB-COMMIS-INTER-NULL<br>*/
    public String getL3421ImpbCommisInterNull() {
        return readString(Pos.L3421_IMPB_COMMIS_INTER_NULL, Len.L3421_IMPB_COMMIS_INTER_NULL);
    }

    public String getL3421ImpbCommisInterNullFormatted() {
        return Functions.padBlanks(getL3421ImpbCommisInterNull(), Len.L3421_IMPB_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_COMMIS_INTER = 1;
        public static final int L3421_IMPB_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_COMMIS_INTER = 8;
        public static final int L3421_IMPB_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
