package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-CNBT-ANTIRAC<br>
 * Variable: WDTR-CNBT-ANTIRAC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_CNBT_ANTIRAC;
    }

    public void setWdtrCnbtAntirac(AfDecimal wdtrCnbtAntirac) {
        writeDecimalAsPacked(Pos.WDTR_CNBT_ANTIRAC, wdtrCnbtAntirac.copy());
    }

    /**Original name: WDTR-CNBT-ANTIRAC<br>*/
    public AfDecimal getWdtrCnbtAntirac() {
        return readPackedAsDecimal(Pos.WDTR_CNBT_ANTIRAC, Len.Int.WDTR_CNBT_ANTIRAC, Len.Fract.WDTR_CNBT_ANTIRAC);
    }

    public void setWdtrCnbtAntiracNull(String wdtrCnbtAntiracNull) {
        writeString(Pos.WDTR_CNBT_ANTIRAC_NULL, wdtrCnbtAntiracNull, Len.WDTR_CNBT_ANTIRAC_NULL);
    }

    /**Original name: WDTR-CNBT-ANTIRAC-NULL<br>*/
    public String getWdtrCnbtAntiracNull() {
        return readString(Pos.WDTR_CNBT_ANTIRAC_NULL, Len.WDTR_CNBT_ANTIRAC_NULL);
    }

    public String getWdtrCnbtAntiracNullFormatted() {
        return Functions.padBlanks(getWdtrCnbtAntiracNull(), Len.WDTR_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_CNBT_ANTIRAC = 1;
        public static final int WDTR_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_CNBT_ANTIRAC = 8;
        public static final int WDTR_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
