package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-STATI-PRENOTAZIONE<br>
 * Variable: WS-STATI-PRENOTAZIONE from copybook LLBV0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsStatiPrenotazione {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char PRENOTATA = 'P';
    public static final char ESEGUITA = 'E';
    public static final char ESEGUITA_KO = 'K';
    public static final char ANNULLATA = 'A';
    public static final char CERTIFICATA = 'C';
    public static final char CONSOLIDATA = 'S';

    //==== METHODS ====
    public void setStatiPrenotazione(char statiPrenotazione) {
        this.value = statiPrenotazione;
    }

    public char getStatiPrenotazione() {
        return this.value;
    }

    public void setEseguita() {
        value = ESEGUITA;
    }
}
