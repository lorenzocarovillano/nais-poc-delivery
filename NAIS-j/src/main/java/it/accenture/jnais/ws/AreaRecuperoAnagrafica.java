package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.WkTimestamp;

/**Original name: AREA-RECUPERO-ANAGRAFICA<br>
 * Variable: AREA-RECUPERO-ANAGRAFICA from program LVVS2720<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaRecuperoAnagrafica {

    //==== PROPERTIES ====
    //Original name: WK-TIMESTAMP
    private WkTimestamp wkTimestamp = new WkTimestamp();
    //Original name: WK-ID-STRINGA
    private String wkIdStringa = DefaultValues.stringVal(Len.WK_ID_STRINGA);
    //Original name: WK-ID-NUM
    private long wkIdNum = DefaultValues.LONG_VAL;
    //Original name: WK-ID-PERS
    private int wkIdPers = DefaultValues.INT_VAL;
    //Original name: WK-COD-PRT-IVA
    private String wkCodPrtIva = DefaultValues.stringVal(Len.WK_COD_PRT_IVA);
    //Original name: WK-COD-FISC
    private String wkCodFisc = DefaultValues.stringVal(Len.WK_COD_FISC);

    //==== METHODS ====
    public void setWkIdStringa(String wkIdStringa) {
        this.wkIdStringa = Functions.subString(wkIdStringa, Len.WK_ID_STRINGA);
    }

    public String getWkIdStringa() {
        return this.wkIdStringa;
    }

    public String getWkIdStringaFormatted() {
        return Functions.padBlanks(getWkIdStringa(), Len.WK_ID_STRINGA);
    }

    public void setWkIdNum(long wkIdNum) {
        this.wkIdNum = wkIdNum;
    }

    public long getWkIdNum() {
        return this.wkIdNum;
    }

    public void setWkIdPers(int wkIdPers) {
        this.wkIdPers = wkIdPers;
    }

    public int getWkIdPers() {
        return this.wkIdPers;
    }

    public void setWkCodPrtIva(String wkCodPrtIva) {
        this.wkCodPrtIva = Functions.subString(wkCodPrtIva, Len.WK_COD_PRT_IVA);
    }

    public String getWkCodPrtIva() {
        return this.wkCodPrtIva;
    }

    public void setWkCodFisc(String wkCodFisc) {
        this.wkCodFisc = Functions.subString(wkCodFisc, Len.WK_COD_FISC);
    }

    public String getWkCodFisc() {
        return this.wkCodFisc;
    }

    public WkTimestamp getWkTimestamp() {
        return wkTimestamp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ID_STRINGA = 20;
        public static final int WK_ID_NUM = 11;
        public static final int WK_COD_PRT_IVA = 11;
        public static final int WK_COD_FISC = 16;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
