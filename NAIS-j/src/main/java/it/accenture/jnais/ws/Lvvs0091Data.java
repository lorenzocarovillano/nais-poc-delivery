package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.occurs.WisoTabImpSost;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0091<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0091Data {

    //==== PROPERTIES ====
    public static final int DISO_TAB_ISO_MAXOCCURS = 10;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: DISO-ELE-ISO-MAX
    private short disoEleIsoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DISO-TAB-ISO
    private WisoTabImpSost[] disoTabIso = new WisoTabImpSost[DISO_TAB_ISO_MAXOCCURS];
    //Original name: LDBV2901
    private Ldbv2901 ldbv2901 = new Ldbv2901();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISO
    private short ixTabIso = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0091Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int disoTabIsoIdx = 1; disoTabIsoIdx <= DISO_TAB_ISO_MAXOCCURS; disoTabIsoIdx++) {
            disoTabIso[disoTabIsoIdx - 1] = new WisoTabImpSost();
        }
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDisoEleIsoMax(short disoEleIsoMax) {
        this.disoEleIsoMax = disoEleIsoMax;
    }

    public short getDisoEleIsoMax() {
        return this.disoEleIsoMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabIso(short ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public short getIxTabIso() {
        return this.ixTabIso;
    }

    public WisoTabImpSost getDisoTabIso(int idx) {
        return disoTabIso[idx - 1];
    }

    public Ldbv2901 getLdbv2901() {
        return ldbv2901;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
