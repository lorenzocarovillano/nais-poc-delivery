package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-TIT<br>
 * Variable: WS-TP-TIT from copybook LCCVXTT0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpTit {

    //==== PROPERTIES ====
    private String value = "";
    public static final String PREMIO = "PR";
    public static final String INTERESSI_PRESTITO = "IP";
    public static final String RECUPERO_PROVV = "RP";
    public static final String PRELIEVO_COSTI = "PC";
    public static final String MANAGEMENT_FEE = "ME";
    public static final String RIMBORSO_PRESTITO = "RR";
    public static final String RIMBORSO_PREMIO = "RO";
    public static final String PROVVIGIONALE = "PV";
    public static final String ACCR_PROVV = "AP";

    //==== METHODS ====
    public void setWsTpTit(String wsTpTit) {
        this.value = Functions.subString(wsTpTit, Len.WS_TP_TIT);
    }

    public String getWsTpTit() {
        return this.value;
    }

    public void setPremio() {
        value = PREMIO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_TIT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
