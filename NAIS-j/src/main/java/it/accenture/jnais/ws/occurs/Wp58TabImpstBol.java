package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvp581;
import it.accenture.jnais.copy.Wp58Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WP58-TAB-IMPST-BOL<br>
 * Variables: WP58-TAB-IMPST-BOL from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wp58TabImpstBol {

    //==== PROPERTIES ====
    //Original name: LCCVP581
    private Lccvp581 lccvp581 = new Lccvp581();

    //==== METHODS ====
    public void setWp58TabImpstBolBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp581.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp581.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp581.Len.Int.ID_PTF, 0));
        position += Lccvp581.Len.ID_PTF;
        lccvp581.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp58TabImpstBolBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp581.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp581.getIdPtf(), Lccvp581.Len.Int.ID_PTF, 0);
        position += Lccvp581.Len.ID_PTF;
        lccvp581.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWp58TabImpstBolSpaces() {
        lccvp581.initLccvp581Spaces();
    }

    public Lccvp581 getLccvp581() {
        return lccvp581;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP58_TAB_IMPST_BOL = WpolStatus.Len.STATUS + Lccvp581.Len.ID_PTF + Wp58Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
