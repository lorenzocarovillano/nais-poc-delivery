package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BBT-COMMIT-FREQUENCY<br>
 * Variable: BBT-COMMIT-FREQUENCY from program IABS0050<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BbtCommitFrequency extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BbtCommitFrequency() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BBT_COMMIT_FREQUENCY;
    }

    public void setBbtCommitFrequency(int bbtCommitFrequency) {
        writeBinaryInt(Pos.BBT_COMMIT_FREQUENCY, bbtCommitFrequency);
    }

    public void setBbtCommitFrequencyFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Types.INT_SIZE, Pos.BBT_COMMIT_FREQUENCY);
    }

    /**Original name: BBT-COMMIT-FREQUENCY<br>*/
    public int getBbtCommitFrequency() {
        return readBinaryInt(Pos.BBT_COMMIT_FREQUENCY);
    }

    public byte[] getBbtCommitFrequencyAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Types.INT_SIZE, Pos.BBT_COMMIT_FREQUENCY);
        return buffer;
    }

    public void setBbtCommitFrequencyNull(String bbtCommitFrequencyNull) {
        writeString(Pos.BBT_COMMIT_FREQUENCY_NULL, bbtCommitFrequencyNull, Len.BBT_COMMIT_FREQUENCY_NULL);
    }

    /**Original name: BBT-COMMIT-FREQUENCY-NULL<br>*/
    public String getBbtCommitFrequencyNull() {
        return readString(Pos.BBT_COMMIT_FREQUENCY_NULL, Len.BBT_COMMIT_FREQUENCY_NULL);
    }

    public String getBbtCommitFrequencyNullFormatted() {
        return Functions.padBlanks(getBbtCommitFrequencyNull(), Len.BBT_COMMIT_FREQUENCY_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BBT_COMMIT_FREQUENCY = 1;
        public static final int BBT_COMMIT_FREQUENCY_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BBT_COMMIT_FREQUENCY = 4;
        public static final int BBT_COMMIT_FREQUENCY_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
