package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-READ-FILESQS6<br>
 * Variable: FLAG-READ-FILESQS6 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagReadFilesqs6 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagReadFilesqs6(char flagReadFilesqs6) {
        this.value = flagReadFilesqs6;
    }

    public char getFlagReadFilesqs6() {
        return this.value;
    }

    public boolean isReadFilesqs6Si() {
        return value == SI;
    }

    public void setReadFilesqs6No() {
        value = NO;
    }
}
