package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: LDBV1351-STAT-BUS-TAB<br>
 * Variable: LDBV1351-STAT-BUS-TAB from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ldbv1351StatBusTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int STAT_BUS_ELE_MAXOCCURS = 7;

    //==== CONSTRUCTORS ====
    public Ldbv1351StatBusTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV1351_STAT_BUS_TAB;
    }

    public void setLdbv1351StatBusTabBytes(byte[] buffer) {
        setLdbv1351StatBusTabBytes(buffer, 1);
    }

    public void setLdbv1351StatBusTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LDBV1351_STAT_BUS_TAB, Pos.LDBV1351_STAT_BUS_TAB);
    }

    public byte[] getLdbv1351StatBusTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LDBV1351_STAT_BUS_TAB, Pos.LDBV1351_STAT_BUS_TAB);
        return buffer;
    }

    public void setLdbv1361TpStatBus(int ldbv1361TpStatBusIdx, String ldbv1361TpStatBus) {
        int position = Pos.ldbv1351TpStatBus(ldbv1361TpStatBusIdx - 1);
        writeString(position, ldbv1361TpStatBus, Len.TP_STAT_BUS);
    }

    /**Original name: LDBV1361-TP-STAT-BUS<br>*/
    public String getLdbv1361TpStatBus(int ldbv1361TpStatBusIdx) {
        int position = Pos.ldbv1351TpStatBus(ldbv1361TpStatBusIdx - 1);
        return readString(position, Len.TP_STAT_BUS);
    }

    public void setTpStatBus01(String tpStatBus01) {
        writeString(Pos.TP_STAT_BUS01, tpStatBus01, Len.TP_STAT_BUS01);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-01<br>*/
    public String getTpStatBus01() {
        return readString(Pos.TP_STAT_BUS01, Len.TP_STAT_BUS01);
    }

    public void setTpStatBus02(String tpStatBus02) {
        writeString(Pos.TP_STAT_BUS02, tpStatBus02, Len.TP_STAT_BUS02);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-02<br>*/
    public String getTpStatBus02() {
        return readString(Pos.TP_STAT_BUS02, Len.TP_STAT_BUS02);
    }

    public void setTpStatBus03(String tpStatBus03) {
        writeString(Pos.TP_STAT_BUS03, tpStatBus03, Len.TP_STAT_BUS03);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-03<br>*/
    public String getTpStatBus03() {
        return readString(Pos.TP_STAT_BUS03, Len.TP_STAT_BUS03);
    }

    public void setTpStatBus04(String tpStatBus04) {
        writeString(Pos.TP_STAT_BUS04, tpStatBus04, Len.TP_STAT_BUS04);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-04<br>*/
    public String getTpStatBus04() {
        return readString(Pos.TP_STAT_BUS04, Len.TP_STAT_BUS04);
    }

    public void setTpStatBus05(String tpStatBus05) {
        writeString(Pos.TP_STAT_BUS05, tpStatBus05, Len.TP_STAT_BUS05);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-05<br>*/
    public String getTpStatBus05() {
        return readString(Pos.TP_STAT_BUS05, Len.TP_STAT_BUS05);
    }

    public void setTpStatBus06(String tpStatBus06) {
        writeString(Pos.TP_STAT_BUS06, tpStatBus06, Len.TP_STAT_BUS06);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-06<br>*/
    public String getTpStatBus06() {
        return readString(Pos.TP_STAT_BUS06, Len.TP_STAT_BUS06);
    }

    public void setTpStatBus07(String tpStatBus07) {
        writeString(Pos.TP_STAT_BUS07, tpStatBus07, Len.TP_STAT_BUS07);
    }

    /**Original name: LDBV1351-TP-STAT-BUS-07<br>*/
    public String getTpStatBus07() {
        return readString(Pos.TP_STAT_BUS07, Len.TP_STAT_BUS07);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LDBV1351_STAT_BUS_TAB = 1;
        public static final int LDBV1351_STAT_BUS_ELE_EST = 1;
        public static final int TP_STAT_BUS01 = LDBV1351_STAT_BUS_ELE_EST;
        public static final int TP_STAT_BUS02 = TP_STAT_BUS01 + Len.TP_STAT_BUS01;
        public static final int TP_STAT_BUS03 = TP_STAT_BUS02 + Len.TP_STAT_BUS02;
        public static final int TP_STAT_BUS04 = TP_STAT_BUS03 + Len.TP_STAT_BUS03;
        public static final int TP_STAT_BUS05 = TP_STAT_BUS04 + Len.TP_STAT_BUS04;
        public static final int TP_STAT_BUS06 = TP_STAT_BUS05 + Len.TP_STAT_BUS05;
        public static final int TP_STAT_BUS07 = TP_STAT_BUS06 + Len.TP_STAT_BUS06;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ldbv1351StatBusEle(int idx) {
            return LDBV1351_STAT_BUS_TAB + idx * Len.STAT_BUS_ELE;
        }

        public static int ldbv1351TpStatBus(int idx) {
            return ldbv1351StatBusEle(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_STAT_BUS = 2;
        public static final int STAT_BUS_ELE = TP_STAT_BUS;
        public static final int TP_STAT_BUS01 = 2;
        public static final int TP_STAT_BUS02 = 2;
        public static final int TP_STAT_BUS03 = 2;
        public static final int TP_STAT_BUS04 = 2;
        public static final int TP_STAT_BUS05 = 2;
        public static final int TP_STAT_BUS06 = 2;
        public static final int LDBV1351_STAT_BUS_TAB = Ldbv1351StatBusTab.STAT_BUS_ELE_MAXOCCURS * STAT_BUS_ELE;
        public static final int TP_STAT_BUS07 = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
