package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0033-MACRO-FUNZIONE<br>
 * Variable: LCCC0033-MACRO-FUNZIONE from copybook LCCC0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0033MacroFunzione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.MACRO_FUNZIONE);
    public static final String VENDITA = "VE";
    public static final String STORNI = "ST";

    //==== METHODS ====
    public void setMacroFunzione(String macroFunzione) {
        this.value = Functions.subString(macroFunzione, Len.MACRO_FUNZIONE);
    }

    public String getMacroFunzione() {
        return this.value;
    }

    public boolean isVendita() {
        return value.equals(VENDITA);
    }

    public void setLccc0033Vendita() {
        value = VENDITA;
    }

    public boolean isStorni() {
        return value.equals(STORNI);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MACRO_FUNZIONE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
