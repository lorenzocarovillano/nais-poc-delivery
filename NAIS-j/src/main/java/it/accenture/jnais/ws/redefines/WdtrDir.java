package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-DIR<br>
 * Variable: WDTR-DIR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_DIR;
    }

    public void setWdtrDir(AfDecimal wdtrDir) {
        writeDecimalAsPacked(Pos.WDTR_DIR, wdtrDir.copy());
    }

    /**Original name: WDTR-DIR<br>*/
    public AfDecimal getWdtrDir() {
        return readPackedAsDecimal(Pos.WDTR_DIR, Len.Int.WDTR_DIR, Len.Fract.WDTR_DIR);
    }

    public void setWdtrDirNull(String wdtrDirNull) {
        writeString(Pos.WDTR_DIR_NULL, wdtrDirNull, Len.WDTR_DIR_NULL);
    }

    /**Original name: WDTR-DIR-NULL<br>*/
    public String getWdtrDirNull() {
        return readString(Pos.WDTR_DIR_NULL, Len.WDTR_DIR_NULL);
    }

    public String getWdtrDirNullFormatted() {
        return Functions.padBlanks(getWdtrDirNull(), Len.WDTR_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_DIR = 1;
        public static final int WDTR_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_DIR = 8;
        public static final int WDTR_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
