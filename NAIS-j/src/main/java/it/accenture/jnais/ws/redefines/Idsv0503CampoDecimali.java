package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0503-CAMPO-DECIMALI<br>
 * Variable: IDSV0503-CAMPO-DECIMALI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Idsv0503CampoDecimali extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_STRINGA_DECIMALI_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public Idsv0503CampoDecimali() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0503_CAMPO_DECIMALI;
    }

    public void setIdsv0503CampoDecimali(String idsv0503CampoDecimali) {
        writeString(Pos.IDSV0503_CAMPO_DECIMALI, idsv0503CampoDecimali, Len.IDSV0503_CAMPO_DECIMALI);
    }

    /**Original name: IDSV0503-CAMPO-DECIMALI<br>*/
    public String getIdsv0503CampoDecimali() {
        return readString(Pos.IDSV0503_CAMPO_DECIMALI, Len.IDSV0503_CAMPO_DECIMALI);
    }

    public String getIdsv0503CampoDecimaliFormatted() {
        return Functions.padBlanks(getIdsv0503CampoDecimali(), Len.IDSV0503_CAMPO_DECIMALI);
    }

    public void setEleStringaDecimali(int eleStringaDecimaliIdx, char eleStringaDecimali) {
        int position = Pos.idsv0503EleStringaDecimali(eleStringaDecimaliIdx - 1);
        writeChar(position, eleStringaDecimali);
    }

    /**Original name: IDSV0503-ELE-STRINGA-DECIMALI<br>*/
    public char getEleStringaDecimali(int eleStringaDecimaliIdx) {
        int position = Pos.idsv0503EleStringaDecimali(eleStringaDecimaliIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSV0503_CAMPO_DECIMALI = 1;
        public static final int IDSV0503_ARRAY_STR_DECIMALI = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int idsv0503EleStringaDecimali(int idx) {
            return IDSV0503_ARRAY_STR_DECIMALI + idx * Len.ELE_STRINGA_DECIMALI;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRINGA_DECIMALI = 1;
        public static final int IDSV0503_CAMPO_DECIMALI = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
