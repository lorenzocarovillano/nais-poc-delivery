package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-DT-RICOR-PREC<br>
 * Variable: PMO-DT-RICOR-PREC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoDtRicorPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoDtRicorPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_DT_RICOR_PREC;
    }

    public void setPmoDtRicorPrec(int pmoDtRicorPrec) {
        writeIntAsPacked(Pos.PMO_DT_RICOR_PREC, pmoDtRicorPrec, Len.Int.PMO_DT_RICOR_PREC);
    }

    public void setPmoDtRicorPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_DT_RICOR_PREC, Pos.PMO_DT_RICOR_PREC);
    }

    /**Original name: PMO-DT-RICOR-PREC<br>*/
    public int getPmoDtRicorPrec() {
        return readPackedAsInt(Pos.PMO_DT_RICOR_PREC, Len.Int.PMO_DT_RICOR_PREC);
    }

    public byte[] getPmoDtRicorPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_DT_RICOR_PREC, Pos.PMO_DT_RICOR_PREC);
        return buffer;
    }

    public void initPmoDtRicorPrecHighValues() {
        fill(Pos.PMO_DT_RICOR_PREC, Len.PMO_DT_RICOR_PREC, Types.HIGH_CHAR_VAL);
    }

    public void setPmoDtRicorPrecNull(String pmoDtRicorPrecNull) {
        writeString(Pos.PMO_DT_RICOR_PREC_NULL, pmoDtRicorPrecNull, Len.PMO_DT_RICOR_PREC_NULL);
    }

    /**Original name: PMO-DT-RICOR-PREC-NULL<br>*/
    public String getPmoDtRicorPrecNull() {
        return readString(Pos.PMO_DT_RICOR_PREC_NULL, Len.PMO_DT_RICOR_PREC_NULL);
    }

    public String getPmoDtRicorPrecNullFormatted() {
        return Functions.padBlanks(getPmoDtRicorPrecNull(), Len.PMO_DT_RICOR_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_DT_RICOR_PREC = 1;
        public static final int PMO_DT_RICOR_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_DT_RICOR_PREC = 5;
        public static final int PMO_DT_RICOR_PREC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_DT_RICOR_PREC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
