package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-MANFEE-RICOR<br>
 * Variable: WDTR-MANFEE-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_MANFEE_RICOR;
    }

    public void setWdtrManfeeRicor(AfDecimal wdtrManfeeRicor) {
        writeDecimalAsPacked(Pos.WDTR_MANFEE_RICOR, wdtrManfeeRicor.copy());
    }

    /**Original name: WDTR-MANFEE-RICOR<br>*/
    public AfDecimal getWdtrManfeeRicor() {
        return readPackedAsDecimal(Pos.WDTR_MANFEE_RICOR, Len.Int.WDTR_MANFEE_RICOR, Len.Fract.WDTR_MANFEE_RICOR);
    }

    public void setWdtrManfeeRicorNull(String wdtrManfeeRicorNull) {
        writeString(Pos.WDTR_MANFEE_RICOR_NULL, wdtrManfeeRicorNull, Len.WDTR_MANFEE_RICOR_NULL);
    }

    /**Original name: WDTR-MANFEE-RICOR-NULL<br>*/
    public String getWdtrManfeeRicorNull() {
        return readString(Pos.WDTR_MANFEE_RICOR_NULL, Len.WDTR_MANFEE_RICOR_NULL);
    }

    public String getWdtrManfeeRicorNullFormatted() {
        return Functions.padBlanks(getWdtrManfeeRicorNull(), Len.WDTR_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_MANFEE_RICOR = 1;
        public static final int WDTR_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_MANFEE_RICOR = 8;
        public static final int WDTR_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
