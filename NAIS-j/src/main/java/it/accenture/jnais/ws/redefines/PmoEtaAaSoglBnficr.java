package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-ETA-AA-SOGL-BNFICR<br>
 * Variable: PMO-ETA-AA-SOGL-BNFICR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoEtaAaSoglBnficr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoEtaAaSoglBnficr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_ETA_AA_SOGL_BNFICR;
    }

    public void setPmoEtaAaSoglBnficr(short pmoEtaAaSoglBnficr) {
        writeShortAsPacked(Pos.PMO_ETA_AA_SOGL_BNFICR, pmoEtaAaSoglBnficr, Len.Int.PMO_ETA_AA_SOGL_BNFICR);
    }

    public void setPmoEtaAaSoglBnficrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_ETA_AA_SOGL_BNFICR, Pos.PMO_ETA_AA_SOGL_BNFICR);
    }

    /**Original name: PMO-ETA-AA-SOGL-BNFICR<br>*/
    public short getPmoEtaAaSoglBnficr() {
        return readPackedAsShort(Pos.PMO_ETA_AA_SOGL_BNFICR, Len.Int.PMO_ETA_AA_SOGL_BNFICR);
    }

    public byte[] getPmoEtaAaSoglBnficrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_ETA_AA_SOGL_BNFICR, Pos.PMO_ETA_AA_SOGL_BNFICR);
        return buffer;
    }

    public void initPmoEtaAaSoglBnficrHighValues() {
        fill(Pos.PMO_ETA_AA_SOGL_BNFICR, Len.PMO_ETA_AA_SOGL_BNFICR, Types.HIGH_CHAR_VAL);
    }

    public void setPmoEtaAaSoglBnficrNull(String pmoEtaAaSoglBnficrNull) {
        writeString(Pos.PMO_ETA_AA_SOGL_BNFICR_NULL, pmoEtaAaSoglBnficrNull, Len.PMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    /**Original name: PMO-ETA-AA-SOGL-BNFICR-NULL<br>*/
    public String getPmoEtaAaSoglBnficrNull() {
        return readString(Pos.PMO_ETA_AA_SOGL_BNFICR_NULL, Len.PMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    public String getPmoEtaAaSoglBnficrNullFormatted() {
        return Functions.padBlanks(getPmoEtaAaSoglBnficrNull(), Len.PMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_ETA_AA_SOGL_BNFICR = 1;
        public static final int PMO_ETA_AA_SOGL_BNFICR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_ETA_AA_SOGL_BNFICR = 2;
        public static final int PMO_ETA_AA_SOGL_BNFICR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_ETA_AA_SOGL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
