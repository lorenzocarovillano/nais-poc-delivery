package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-OLD-TS-TEC<br>
 * Variable: L3421-OLD-TS-TEC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421OldTsTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421OldTsTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_OLD_TS_TEC;
    }

    public void setL3421OldTsTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_OLD_TS_TEC, Pos.L3421_OLD_TS_TEC);
    }

    /**Original name: L3421-OLD-TS-TEC<br>*/
    public AfDecimal getL3421OldTsTec() {
        return readPackedAsDecimal(Pos.L3421_OLD_TS_TEC, Len.Int.L3421_OLD_TS_TEC, Len.Fract.L3421_OLD_TS_TEC);
    }

    public byte[] getL3421OldTsTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_OLD_TS_TEC, Pos.L3421_OLD_TS_TEC);
        return buffer;
    }

    /**Original name: L3421-OLD-TS-TEC-NULL<br>*/
    public String getL3421OldTsTecNull() {
        return readString(Pos.L3421_OLD_TS_TEC_NULL, Len.L3421_OLD_TS_TEC_NULL);
    }

    public String getL3421OldTsTecNullFormatted() {
        return Functions.padBlanks(getL3421OldTsTecNull(), Len.L3421_OLD_TS_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_OLD_TS_TEC = 1;
        public static final int L3421_OLD_TS_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_OLD_TS_TEC = 8;
        public static final int L3421_OLD_TS_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_OLD_TS_TEC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_OLD_TS_TEC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
