package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: STW-ID-MOVI-CHIU<br>
 * Variable: STW-ID-MOVI-CHIU from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class StwIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public StwIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STW_ID_MOVI_CHIU;
    }

    public void setStwIdMoviChiu(int stwIdMoviChiu) {
        writeIntAsPacked(Pos.STW_ID_MOVI_CHIU, stwIdMoviChiu, Len.Int.STW_ID_MOVI_CHIU);
    }

    public void setStwIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.STW_ID_MOVI_CHIU, Pos.STW_ID_MOVI_CHIU);
    }

    /**Original name: STW-ID-MOVI-CHIU<br>*/
    public int getStwIdMoviChiu() {
        return readPackedAsInt(Pos.STW_ID_MOVI_CHIU, Len.Int.STW_ID_MOVI_CHIU);
    }

    public byte[] getStwIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.STW_ID_MOVI_CHIU, Pos.STW_ID_MOVI_CHIU);
        return buffer;
    }

    public void setStwIdMoviChiuNull(String stwIdMoviChiuNull) {
        writeString(Pos.STW_ID_MOVI_CHIU_NULL, stwIdMoviChiuNull, Len.STW_ID_MOVI_CHIU_NULL);
    }

    /**Original name: STW-ID-MOVI-CHIU-NULL<br>*/
    public String getStwIdMoviChiuNull() {
        return readString(Pos.STW_ID_MOVI_CHIU_NULL, Len.STW_ID_MOVI_CHIU_NULL);
    }

    public String getStwIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getStwIdMoviChiuNull(), Len.STW_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int STW_ID_MOVI_CHIU = 1;
        public static final int STW_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STW_ID_MOVI_CHIU = 5;
        public static final int STW_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int STW_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
