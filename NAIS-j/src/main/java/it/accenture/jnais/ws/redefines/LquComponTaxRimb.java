package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-COMPON-TAX-RIMB<br>
 * Variable: LQU-COMPON-TAX-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquComponTaxRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquComponTaxRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_COMPON_TAX_RIMB;
    }

    public void setLquComponTaxRimb(AfDecimal lquComponTaxRimb) {
        writeDecimalAsPacked(Pos.LQU_COMPON_TAX_RIMB, lquComponTaxRimb.copy());
    }

    public void setLquComponTaxRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_COMPON_TAX_RIMB, Pos.LQU_COMPON_TAX_RIMB);
    }

    /**Original name: LQU-COMPON-TAX-RIMB<br>*/
    public AfDecimal getLquComponTaxRimb() {
        return readPackedAsDecimal(Pos.LQU_COMPON_TAX_RIMB, Len.Int.LQU_COMPON_TAX_RIMB, Len.Fract.LQU_COMPON_TAX_RIMB);
    }

    public byte[] getLquComponTaxRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_COMPON_TAX_RIMB, Pos.LQU_COMPON_TAX_RIMB);
        return buffer;
    }

    public void setLquComponTaxRimbNull(String lquComponTaxRimbNull) {
        writeString(Pos.LQU_COMPON_TAX_RIMB_NULL, lquComponTaxRimbNull, Len.LQU_COMPON_TAX_RIMB_NULL);
    }

    /**Original name: LQU-COMPON-TAX-RIMB-NULL<br>*/
    public String getLquComponTaxRimbNull() {
        return readString(Pos.LQU_COMPON_TAX_RIMB_NULL, Len.LQU_COMPON_TAX_RIMB_NULL);
    }

    public String getLquComponTaxRimbNullFormatted() {
        return Functions.padBlanks(getLquComponTaxRimbNull(), Len.LQU_COMPON_TAX_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_COMPON_TAX_RIMB = 1;
        public static final int LQU_COMPON_TAX_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_COMPON_TAX_RIMB = 8;
        public static final int LQU_COMPON_TAX_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_COMPON_TAX_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_COMPON_TAX_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
