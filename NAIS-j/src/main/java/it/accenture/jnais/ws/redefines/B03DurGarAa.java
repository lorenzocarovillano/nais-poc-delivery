package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-GAR-AA<br>
 * Variable: B03-DUR-GAR-AA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurGarAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurGarAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_GAR_AA;
    }

    public void setB03DurGarAa(int b03DurGarAa) {
        writeIntAsPacked(Pos.B03_DUR_GAR_AA, b03DurGarAa, Len.Int.B03_DUR_GAR_AA);
    }

    public void setB03DurGarAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_GAR_AA, Pos.B03_DUR_GAR_AA);
    }

    /**Original name: B03-DUR-GAR-AA<br>*/
    public int getB03DurGarAa() {
        return readPackedAsInt(Pos.B03_DUR_GAR_AA, Len.Int.B03_DUR_GAR_AA);
    }

    public byte[] getB03DurGarAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_GAR_AA, Pos.B03_DUR_GAR_AA);
        return buffer;
    }

    public void setB03DurGarAaNull(String b03DurGarAaNull) {
        writeString(Pos.B03_DUR_GAR_AA_NULL, b03DurGarAaNull, Len.B03_DUR_GAR_AA_NULL);
    }

    /**Original name: B03-DUR-GAR-AA-NULL<br>*/
    public String getB03DurGarAaNull() {
        return readString(Pos.B03_DUR_GAR_AA_NULL, Len.B03_DUR_GAR_AA_NULL);
    }

    public String getB03DurGarAaNullFormatted() {
        return Functions.padBlanks(getB03DurGarAaNull(), Len.B03_DUR_GAR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_GAR_AA = 1;
        public static final int B03_DUR_GAR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_GAR_AA = 3;
        public static final int B03_DUR_GAR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_GAR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
