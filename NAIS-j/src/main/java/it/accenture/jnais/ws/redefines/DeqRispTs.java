package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-RISP-TS<br>
 * Variable: DEQ-RISP-TS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqRispTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqRispTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_RISP_TS;
    }

    public void setDeqRispTs(AfDecimal deqRispTs) {
        writeDecimalAsPacked(Pos.DEQ_RISP_TS, deqRispTs.copy());
    }

    public void setDeqRispTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_RISP_TS, Pos.DEQ_RISP_TS);
    }

    /**Original name: DEQ-RISP-TS<br>*/
    public AfDecimal getDeqRispTs() {
        return readPackedAsDecimal(Pos.DEQ_RISP_TS, Len.Int.DEQ_RISP_TS, Len.Fract.DEQ_RISP_TS);
    }

    public byte[] getDeqRispTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_RISP_TS, Pos.DEQ_RISP_TS);
        return buffer;
    }

    public void setDeqRispTsNull(String deqRispTsNull) {
        writeString(Pos.DEQ_RISP_TS_NULL, deqRispTsNull, Len.DEQ_RISP_TS_NULL);
    }

    /**Original name: DEQ-RISP-TS-NULL<br>*/
    public String getDeqRispTsNull() {
        return readString(Pos.DEQ_RISP_TS_NULL, Len.DEQ_RISP_TS_NULL);
    }

    public String getDeqRispTsNullFormatted() {
        return Functions.padBlanks(getDeqRispTsNull(), Len.DEQ_RISP_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_TS = 1;
        public static final int DEQ_RISP_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_TS = 8;
        public static final int DEQ_RISP_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_TS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_TS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
