package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-INTR-FRAZ<br>
 * Variable: WTIT-TOT-INTR-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_INTR_FRAZ;
    }

    public void setWtitTotIntrFraz(AfDecimal wtitTotIntrFraz) {
        writeDecimalAsPacked(Pos.WTIT_TOT_INTR_FRAZ, wtitTotIntrFraz.copy());
    }

    public void setWtitTotIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_INTR_FRAZ, Pos.WTIT_TOT_INTR_FRAZ);
    }

    /**Original name: WTIT-TOT-INTR-FRAZ<br>*/
    public AfDecimal getWtitTotIntrFraz() {
        return readPackedAsDecimal(Pos.WTIT_TOT_INTR_FRAZ, Len.Int.WTIT_TOT_INTR_FRAZ, Len.Fract.WTIT_TOT_INTR_FRAZ);
    }

    public byte[] getWtitTotIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_INTR_FRAZ, Pos.WTIT_TOT_INTR_FRAZ);
        return buffer;
    }

    public void initWtitTotIntrFrazSpaces() {
        fill(Pos.WTIT_TOT_INTR_FRAZ, Len.WTIT_TOT_INTR_FRAZ, Types.SPACE_CHAR);
    }

    public void setWtitTotIntrFrazNull(String wtitTotIntrFrazNull) {
        writeString(Pos.WTIT_TOT_INTR_FRAZ_NULL, wtitTotIntrFrazNull, Len.WTIT_TOT_INTR_FRAZ_NULL);
    }

    /**Original name: WTIT-TOT-INTR-FRAZ-NULL<br>*/
    public String getWtitTotIntrFrazNull() {
        return readString(Pos.WTIT_TOT_INTR_FRAZ_NULL, Len.WTIT_TOT_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_FRAZ = 1;
        public static final int WTIT_TOT_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_FRAZ = 8;
        public static final int WTIT_TOT_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
