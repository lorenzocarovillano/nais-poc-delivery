package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-PC-ABB-TS-662014<br>
 * Variable: S089-PC-ABB-TS-662014 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089PcAbbTs662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089PcAbbTs662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_PC_ABB_TS662014;
    }

    public void setWlquPcAbbTs662014(AfDecimal wlquPcAbbTs662014) {
        writeDecimalAsPacked(Pos.S089_PC_ABB_TS662014, wlquPcAbbTs662014.copy());
    }

    public void setWlquPcAbbTs662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_PC_ABB_TS662014, Pos.S089_PC_ABB_TS662014);
    }

    /**Original name: WLQU-PC-ABB-TS-662014<br>*/
    public AfDecimal getWlquPcAbbTs662014() {
        return readPackedAsDecimal(Pos.S089_PC_ABB_TS662014, Len.Int.WLQU_PC_ABB_TS662014, Len.Fract.WLQU_PC_ABB_TS662014);
    }

    public byte[] getWlquPcAbbTs662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_PC_ABB_TS662014, Pos.S089_PC_ABB_TS662014);
        return buffer;
    }

    public void initWlquPcAbbTs662014Spaces() {
        fill(Pos.S089_PC_ABB_TS662014, Len.S089_PC_ABB_TS662014, Types.SPACE_CHAR);
    }

    public void setWlquPcAbbTs662014Null(String wlquPcAbbTs662014Null) {
        writeString(Pos.S089_PC_ABB_TS662014_NULL, wlquPcAbbTs662014Null, Len.WLQU_PC_ABB_TS662014_NULL);
    }

    /**Original name: WLQU-PC-ABB-TS-662014-NULL<br>*/
    public String getWlquPcAbbTs662014Null() {
        return readString(Pos.S089_PC_ABB_TS662014_NULL, Len.WLQU_PC_ABB_TS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_PC_ABB_TS662014 = 1;
        public static final int S089_PC_ABB_TS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_PC_ABB_TS662014 = 4;
        public static final int WLQU_PC_ABB_TS662014_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_PC_ABB_TS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_PC_ABB_TS662014 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
