package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-FLAG-RIC-DEROGA-ERR<br>
 * Variable: WS-FLAG-RIC-DEROGA-ERR from copybook IEAV9903<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFlagRicDerogaErr {

    //==== PROPERTIES ====
    private String value = "0";
    public static final String CERCA_DEROGA_ERR = "0";
    public static final String DEROGA_PRESENTE_ERR = "1";

    //==== METHODS ====
    public void setWsFlagRicDerogaErr(short wsFlagRicDerogaErr) {
        this.value = NumericDisplay.asString(wsFlagRicDerogaErr, Len.WS_FLAG_RIC_DEROGA_ERR);
    }

    public void setWsFlagRicDerogaErrFormatted(String wsFlagRicDerogaErr) {
        this.value = Trunc.toUnsignedNumeric(wsFlagRicDerogaErr, Len.WS_FLAG_RIC_DEROGA_ERR);
    }

    public short getWsFlagRicDerogaErr() {
        return NumericDisplay.asShort(this.value);
    }

    public String getWsFlagRicDerogaErrFormatted() {
        return this.value;
    }

    public void setCercaDerogaErr() {
        setWsFlagRicDerogaErrFormatted(CERCA_DEROGA_ERR);
    }

    public boolean isDerogaPresenteErr() {
        return getWsFlagRicDerogaErrFormatted().equals(DEROGA_PRESENTE_ERR);
    }

    public void setDerogaPresenteErr() {
        setWsFlagRicDerogaErrFormatted(DEROGA_PRESENTE_ERR);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_FLAG_RIC_DEROGA_ERR = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
