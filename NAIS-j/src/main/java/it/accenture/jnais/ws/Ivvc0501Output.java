package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Ivvc0501TabStringheTot;

/**Original name: IVVC0501-OUTPUT<br>
 * Variable: IVVC0501-OUTPUT from copybook IVVC0501<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ivvc0501Output extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_STRINGHE_TOT_MAXOCCURS = 20;
    //Original name: IVVC0501-TP-STRINGA
    private char tpStringa = DefaultValues.CHAR_VAL;
    //Original name: IVVC0501-MAX-TAB-STR
    private short maxTabStr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IVVC0501-TAB-STRINGHE-TOT
    private Ivvc0501TabStringheTot[] tabStringheTot = new Ivvc0501TabStringheTot[TAB_STRINGHE_TOT_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0501Output() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IVVC0501_OUTPUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIvvc0501OutputBytes(buf);
    }

    public void init() {
        for (int tabStringheTotIdx = 1; tabStringheTotIdx <= TAB_STRINGHE_TOT_MAXOCCURS; tabStringheTotIdx++) {
            tabStringheTot[tabStringheTotIdx - 1] = new Ivvc0501TabStringheTot();
        }
    }

    public String getIvvc0501OutputFormatted() {
        return MarshalByteExt.bufferToStr(getIvvc0501OutputBytes());
    }

    public void setIvvc0501OutputBytes(byte[] buffer) {
        setIvvc0501OutputBytes(buffer, 1);
    }

    public byte[] getIvvc0501OutputBytes() {
        byte[] buffer = new byte[Len.IVVC0501_OUTPUT];
        return getIvvc0501OutputBytes(buffer, 1);
    }

    public void setIvvc0501OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        tpStringa = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        maxTabStr = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_STRINGHE_TOT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabStringheTot[idx - 1].setTabStringheTotBytes(buffer, position);
                position += Ivvc0501TabStringheTot.Len.TAB_STRINGHE_TOT;
            }
            else {
                tabStringheTot[idx - 1].initTabStringheTotSpaces();
                position += Ivvc0501TabStringheTot.Len.TAB_STRINGHE_TOT;
            }
        }
    }

    public byte[] getIvvc0501OutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, tpStringa);
        position += Types.CHAR_SIZE;
        MarshalByte.writeBinaryShort(buffer, position, maxTabStr);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_STRINGHE_TOT_MAXOCCURS; idx++) {
            tabStringheTot[idx - 1].getTabStringheTotBytes(buffer, position);
            position += Ivvc0501TabStringheTot.Len.TAB_STRINGHE_TOT;
        }
        return buffer;
    }

    public void setTpStringa(char tpStringa) {
        this.tpStringa = tpStringa;
    }

    public char getTpStringa() {
        return this.tpStringa;
    }

    public void setMaxTabStr(short maxTabStr) {
        this.maxTabStr = maxTabStr;
    }

    public short getMaxTabStr() {
        return this.maxTabStr;
    }

    public Ivvc0501TabStringheTot getTabStringheTot(int idx) {
        return tabStringheTot[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getIvvc0501OutputBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_STRINGA = 1;
        public static final int MAX_TAB_STR = 2;
        public static final int IVVC0501_OUTPUT = TP_STRINGA + MAX_TAB_STR + Ivvc0501Output.TAB_STRINGHE_TOT_MAXOCCURS * Ivvc0501TabStringheTot.Len.TAB_STRINGHE_TOT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
