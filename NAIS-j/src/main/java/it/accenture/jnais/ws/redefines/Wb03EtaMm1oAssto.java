package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ETA-MM-1O-ASSTO<br>
 * Variable: WB03-ETA-MM-1O-ASSTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03EtaMm1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03EtaMm1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ETA_MM1O_ASSTO;
    }

    public void setWb03EtaMm1oAssto(int wb03EtaMm1oAssto) {
        writeIntAsPacked(Pos.WB03_ETA_MM1O_ASSTO, wb03EtaMm1oAssto, Len.Int.WB03_ETA_MM1O_ASSTO);
    }

    public void setWb03EtaMm1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ETA_MM1O_ASSTO, Pos.WB03_ETA_MM1O_ASSTO);
    }

    /**Original name: WB03-ETA-MM-1O-ASSTO<br>*/
    public int getWb03EtaMm1oAssto() {
        return readPackedAsInt(Pos.WB03_ETA_MM1O_ASSTO, Len.Int.WB03_ETA_MM1O_ASSTO);
    }

    public byte[] getWb03EtaMm1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ETA_MM1O_ASSTO, Pos.WB03_ETA_MM1O_ASSTO);
        return buffer;
    }

    public void setWb03EtaMm1oAsstoNull(String wb03EtaMm1oAsstoNull) {
        writeString(Pos.WB03_ETA_MM1O_ASSTO_NULL, wb03EtaMm1oAsstoNull, Len.WB03_ETA_MM1O_ASSTO_NULL);
    }

    /**Original name: WB03-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getWb03EtaMm1oAsstoNull() {
        return readString(Pos.WB03_ETA_MM1O_ASSTO_NULL, Len.WB03_ETA_MM1O_ASSTO_NULL);
    }

    public String getWb03EtaMm1oAsstoNullFormatted() {
        return Functions.padBlanks(getWb03EtaMm1oAsstoNull(), Len.WB03_ETA_MM1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ETA_MM1O_ASSTO = 1;
        public static final int WB03_ETA_MM1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ETA_MM1O_ASSTO = 3;
        public static final int WB03_ETA_MM1O_ASSTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ETA_MM1O_ASSTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
