package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ALQ-PROV-INC<br>
 * Variable: TGA-ALQ-PROV-INC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAlqProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAlqProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ALQ_PROV_INC;
    }

    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        writeDecimalAsPacked(Pos.TGA_ALQ_PROV_INC, tgaAlqProvInc.copy());
    }

    public void setTgaAlqProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ALQ_PROV_INC, Pos.TGA_ALQ_PROV_INC);
    }

    /**Original name: TGA-ALQ-PROV-INC<br>*/
    public AfDecimal getTgaAlqProvInc() {
        return readPackedAsDecimal(Pos.TGA_ALQ_PROV_INC, Len.Int.TGA_ALQ_PROV_INC, Len.Fract.TGA_ALQ_PROV_INC);
    }

    public byte[] getTgaAlqProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ALQ_PROV_INC, Pos.TGA_ALQ_PROV_INC);
        return buffer;
    }

    public void setTgaAlqProvIncNull(String tgaAlqProvIncNull) {
        writeString(Pos.TGA_ALQ_PROV_INC_NULL, tgaAlqProvIncNull, Len.TGA_ALQ_PROV_INC_NULL);
    }

    /**Original name: TGA-ALQ-PROV-INC-NULL<br>*/
    public String getTgaAlqProvIncNull() {
        return readString(Pos.TGA_ALQ_PROV_INC_NULL, Len.TGA_ALQ_PROV_INC_NULL);
    }

    public String getTgaAlqProvIncNullFormatted() {
        return Functions.padBlanks(getTgaAlqProvIncNull(), Len.TGA_ALQ_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_PROV_INC = 1;
        public static final int TGA_ALQ_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_PROV_INC = 4;
        public static final int TGA_ALQ_PROV_INC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
