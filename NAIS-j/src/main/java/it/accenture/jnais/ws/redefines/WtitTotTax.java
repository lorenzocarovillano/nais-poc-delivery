package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-TAX<br>
 * Variable: WTIT-TOT-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_TAX;
    }

    public void setWtitTotTax(AfDecimal wtitTotTax) {
        writeDecimalAsPacked(Pos.WTIT_TOT_TAX, wtitTotTax.copy());
    }

    public void setWtitTotTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_TAX, Pos.WTIT_TOT_TAX);
    }

    /**Original name: WTIT-TOT-TAX<br>*/
    public AfDecimal getWtitTotTax() {
        return readPackedAsDecimal(Pos.WTIT_TOT_TAX, Len.Int.WTIT_TOT_TAX, Len.Fract.WTIT_TOT_TAX);
    }

    public byte[] getWtitTotTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_TAX, Pos.WTIT_TOT_TAX);
        return buffer;
    }

    public void initWtitTotTaxSpaces() {
        fill(Pos.WTIT_TOT_TAX, Len.WTIT_TOT_TAX, Types.SPACE_CHAR);
    }

    public void setWtitTotTaxNull(String wtitTotTaxNull) {
        writeString(Pos.WTIT_TOT_TAX_NULL, wtitTotTaxNull, Len.WTIT_TOT_TAX_NULL);
    }

    /**Original name: WTIT-TOT-TAX-NULL<br>*/
    public String getWtitTotTaxNull() {
        return readString(Pos.WTIT_TOT_TAX_NULL, Len.WTIT_TOT_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_TAX = 1;
        public static final int WTIT_TOT_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_TAX = 8;
        public static final int WTIT_TOT_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
