package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.MfzCosOprz;
import it.accenture.jnais.ws.redefines.MfzDtEffMoviFinrio;
import it.accenture.jnais.ws.redefines.MfzDtElab;
import it.accenture.jnais.ws.redefines.MfzDtRichMovi;
import it.accenture.jnais.ws.redefines.MfzIdAdes;
import it.accenture.jnais.ws.redefines.MfzIdLiq;
import it.accenture.jnais.ws.redefines.MfzIdMoviChiu;
import it.accenture.jnais.ws.redefines.MfzIdTitCont;

/**Original name: MOVI-FINRIO<br>
 * Variable: MOVI-FINRIO from copybook IDBVMFZ1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class MoviFinrioLdbs5950 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: MFZ-ID-MOVI-FINRIO
    private int mfzIdMoviFinrio = DefaultValues.INT_VAL;
    //Original name: MFZ-ID-ADES
    private MfzIdAdes mfzIdAdes = new MfzIdAdes();
    //Original name: MFZ-ID-LIQ
    private MfzIdLiq mfzIdLiq = new MfzIdLiq();
    //Original name: MFZ-ID-TIT-CONT
    private MfzIdTitCont mfzIdTitCont = new MfzIdTitCont();
    //Original name: MFZ-ID-MOVI-CRZ
    private int mfzIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: MFZ-ID-MOVI-CHIU
    private MfzIdMoviChiu mfzIdMoviChiu = new MfzIdMoviChiu();
    //Original name: MFZ-DT-INI-EFF
    private int mfzDtIniEff = DefaultValues.INT_VAL;
    //Original name: MFZ-DT-END-EFF
    private int mfzDtEndEff = DefaultValues.INT_VAL;
    //Original name: MFZ-COD-COMP-ANIA
    private int mfzCodCompAnia = DefaultValues.INT_VAL;
    //Original name: MFZ-TP-MOVI-FINRIO
    private String mfzTpMoviFinrio = DefaultValues.stringVal(Len.MFZ_TP_MOVI_FINRIO);
    //Original name: MFZ-DT-EFF-MOVI-FINRIO
    private MfzDtEffMoviFinrio mfzDtEffMoviFinrio = new MfzDtEffMoviFinrio();
    //Original name: MFZ-DT-ELAB
    private MfzDtElab mfzDtElab = new MfzDtElab();
    //Original name: MFZ-DT-RICH-MOVI
    private MfzDtRichMovi mfzDtRichMovi = new MfzDtRichMovi();
    //Original name: MFZ-COS-OPRZ
    private MfzCosOprz mfzCosOprz = new MfzCosOprz();
    //Original name: MFZ-STAT-MOVI
    private String mfzStatMovi = DefaultValues.stringVal(Len.MFZ_STAT_MOVI);
    //Original name: MFZ-DS-RIGA
    private long mfzDsRiga = DefaultValues.LONG_VAL;
    //Original name: MFZ-DS-OPER-SQL
    private char mfzDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: MFZ-DS-VER
    private int mfzDsVer = DefaultValues.INT_VAL;
    //Original name: MFZ-DS-TS-INI-CPTZ
    private long mfzDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: MFZ-DS-TS-END-CPTZ
    private long mfzDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: MFZ-DS-UTENTE
    private String mfzDsUtente = DefaultValues.stringVal(Len.MFZ_DS_UTENTE);
    //Original name: MFZ-DS-STATO-ELAB
    private char mfzDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: MFZ-TP-CAUS-RETTIFICA
    private String mfzTpCausRettifica = DefaultValues.stringVal(Len.MFZ_TP_CAUS_RETTIFICA);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOVI_FINRIO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMoviFinrioBytes(buf);
    }

    public void setMoviFinrioFormatted(String data) {
        byte[] buffer = new byte[Len.MOVI_FINRIO];
        MarshalByte.writeString(buffer, 1, data, Len.MOVI_FINRIO);
        setMoviFinrioBytes(buffer, 1);
    }

    public String getMoviFinrioFormatted() {
        return MarshalByteExt.bufferToStr(getMoviFinrioBytes());
    }

    public void setMoviFinrioBytes(byte[] buffer) {
        setMoviFinrioBytes(buffer, 1);
    }

    public byte[] getMoviFinrioBytes() {
        byte[] buffer = new byte[Len.MOVI_FINRIO];
        return getMoviFinrioBytes(buffer, 1);
    }

    public void setMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        mfzIdMoviFinrio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MFZ_ID_MOVI_FINRIO, 0);
        position += Len.MFZ_ID_MOVI_FINRIO;
        mfzIdAdes.setMfzIdAdesFromBuffer(buffer, position);
        position += MfzIdAdes.Len.MFZ_ID_ADES;
        mfzIdLiq.setMfzIdLiqFromBuffer(buffer, position);
        position += MfzIdLiq.Len.MFZ_ID_LIQ;
        mfzIdTitCont.setMfzIdTitContFromBuffer(buffer, position);
        position += MfzIdTitCont.Len.MFZ_ID_TIT_CONT;
        mfzIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MFZ_ID_MOVI_CRZ, 0);
        position += Len.MFZ_ID_MOVI_CRZ;
        mfzIdMoviChiu.setMfzIdMoviChiuFromBuffer(buffer, position);
        position += MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU;
        mfzDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MFZ_DT_INI_EFF, 0);
        position += Len.MFZ_DT_INI_EFF;
        mfzDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MFZ_DT_END_EFF, 0);
        position += Len.MFZ_DT_END_EFF;
        mfzCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MFZ_COD_COMP_ANIA, 0);
        position += Len.MFZ_COD_COMP_ANIA;
        mfzTpMoviFinrio = MarshalByte.readString(buffer, position, Len.MFZ_TP_MOVI_FINRIO);
        position += Len.MFZ_TP_MOVI_FINRIO;
        mfzDtEffMoviFinrio.setMfzDtEffMoviFinrioFromBuffer(buffer, position);
        position += MfzDtEffMoviFinrio.Len.MFZ_DT_EFF_MOVI_FINRIO;
        mfzDtElab.setMfzDtElabFromBuffer(buffer, position);
        position += MfzDtElab.Len.MFZ_DT_ELAB;
        mfzDtRichMovi.setMfzDtRichMoviFromBuffer(buffer, position);
        position += MfzDtRichMovi.Len.MFZ_DT_RICH_MOVI;
        mfzCosOprz.setMfzCosOprzFromBuffer(buffer, position);
        position += MfzCosOprz.Len.MFZ_COS_OPRZ;
        mfzStatMovi = MarshalByte.readString(buffer, position, Len.MFZ_STAT_MOVI);
        position += Len.MFZ_STAT_MOVI;
        mfzDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MFZ_DS_RIGA, 0);
        position += Len.MFZ_DS_RIGA;
        mfzDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mfzDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MFZ_DS_VER, 0);
        position += Len.MFZ_DS_VER;
        mfzDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MFZ_DS_TS_INI_CPTZ, 0);
        position += Len.MFZ_DS_TS_INI_CPTZ;
        mfzDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MFZ_DS_TS_END_CPTZ, 0);
        position += Len.MFZ_DS_TS_END_CPTZ;
        mfzDsUtente = MarshalByte.readString(buffer, position, Len.MFZ_DS_UTENTE);
        position += Len.MFZ_DS_UTENTE;
        mfzDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mfzTpCausRettifica = MarshalByte.readString(buffer, position, Len.MFZ_TP_CAUS_RETTIFICA);
    }

    public byte[] getMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, mfzIdMoviFinrio, Len.Int.MFZ_ID_MOVI_FINRIO, 0);
        position += Len.MFZ_ID_MOVI_FINRIO;
        mfzIdAdes.getMfzIdAdesAsBuffer(buffer, position);
        position += MfzIdAdes.Len.MFZ_ID_ADES;
        mfzIdLiq.getMfzIdLiqAsBuffer(buffer, position);
        position += MfzIdLiq.Len.MFZ_ID_LIQ;
        mfzIdTitCont.getMfzIdTitContAsBuffer(buffer, position);
        position += MfzIdTitCont.Len.MFZ_ID_TIT_CONT;
        MarshalByte.writeIntAsPacked(buffer, position, mfzIdMoviCrz, Len.Int.MFZ_ID_MOVI_CRZ, 0);
        position += Len.MFZ_ID_MOVI_CRZ;
        mfzIdMoviChiu.getMfzIdMoviChiuAsBuffer(buffer, position);
        position += MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, mfzDtIniEff, Len.Int.MFZ_DT_INI_EFF, 0);
        position += Len.MFZ_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, mfzDtEndEff, Len.Int.MFZ_DT_END_EFF, 0);
        position += Len.MFZ_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, mfzCodCompAnia, Len.Int.MFZ_COD_COMP_ANIA, 0);
        position += Len.MFZ_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, mfzTpMoviFinrio, Len.MFZ_TP_MOVI_FINRIO);
        position += Len.MFZ_TP_MOVI_FINRIO;
        mfzDtEffMoviFinrio.getMfzDtEffMoviFinrioAsBuffer(buffer, position);
        position += MfzDtEffMoviFinrio.Len.MFZ_DT_EFF_MOVI_FINRIO;
        mfzDtElab.getMfzDtElabAsBuffer(buffer, position);
        position += MfzDtElab.Len.MFZ_DT_ELAB;
        mfzDtRichMovi.getMfzDtRichMoviAsBuffer(buffer, position);
        position += MfzDtRichMovi.Len.MFZ_DT_RICH_MOVI;
        mfzCosOprz.getMfzCosOprzAsBuffer(buffer, position);
        position += MfzCosOprz.Len.MFZ_COS_OPRZ;
        MarshalByte.writeString(buffer, position, mfzStatMovi, Len.MFZ_STAT_MOVI);
        position += Len.MFZ_STAT_MOVI;
        MarshalByte.writeLongAsPacked(buffer, position, mfzDsRiga, Len.Int.MFZ_DS_RIGA, 0);
        position += Len.MFZ_DS_RIGA;
        MarshalByte.writeChar(buffer, position, mfzDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, mfzDsVer, Len.Int.MFZ_DS_VER, 0);
        position += Len.MFZ_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, mfzDsTsIniCptz, Len.Int.MFZ_DS_TS_INI_CPTZ, 0);
        position += Len.MFZ_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, mfzDsTsEndCptz, Len.Int.MFZ_DS_TS_END_CPTZ, 0);
        position += Len.MFZ_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, mfzDsUtente, Len.MFZ_DS_UTENTE);
        position += Len.MFZ_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, mfzDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, mfzTpCausRettifica, Len.MFZ_TP_CAUS_RETTIFICA);
        return buffer;
    }

    public void setMfzIdMoviFinrio(int mfzIdMoviFinrio) {
        this.mfzIdMoviFinrio = mfzIdMoviFinrio;
    }

    public int getMfzIdMoviFinrio() {
        return this.mfzIdMoviFinrio;
    }

    public void setMfzIdMoviCrz(int mfzIdMoviCrz) {
        this.mfzIdMoviCrz = mfzIdMoviCrz;
    }

    public int getMfzIdMoviCrz() {
        return this.mfzIdMoviCrz;
    }

    public void setMfzDtIniEff(int mfzDtIniEff) {
        this.mfzDtIniEff = mfzDtIniEff;
    }

    public int getMfzDtIniEff() {
        return this.mfzDtIniEff;
    }

    public void setMfzDtEndEff(int mfzDtEndEff) {
        this.mfzDtEndEff = mfzDtEndEff;
    }

    public int getMfzDtEndEff() {
        return this.mfzDtEndEff;
    }

    public void setMfzCodCompAnia(int mfzCodCompAnia) {
        this.mfzCodCompAnia = mfzCodCompAnia;
    }

    public int getMfzCodCompAnia() {
        return this.mfzCodCompAnia;
    }

    public void setMfzTpMoviFinrio(String mfzTpMoviFinrio) {
        this.mfzTpMoviFinrio = Functions.subString(mfzTpMoviFinrio, Len.MFZ_TP_MOVI_FINRIO);
    }

    public String getMfzTpMoviFinrio() {
        return this.mfzTpMoviFinrio;
    }

    public void setMfzStatMovi(String mfzStatMovi) {
        this.mfzStatMovi = Functions.subString(mfzStatMovi, Len.MFZ_STAT_MOVI);
    }

    public String getMfzStatMovi() {
        return this.mfzStatMovi;
    }

    public void setMfzDsRiga(long mfzDsRiga) {
        this.mfzDsRiga = mfzDsRiga;
    }

    public long getMfzDsRiga() {
        return this.mfzDsRiga;
    }

    public void setMfzDsOperSql(char mfzDsOperSql) {
        this.mfzDsOperSql = mfzDsOperSql;
    }

    public void setMfzDsOperSqlFormatted(String mfzDsOperSql) {
        setMfzDsOperSql(Functions.charAt(mfzDsOperSql, Types.CHAR_SIZE));
    }

    public char getMfzDsOperSql() {
        return this.mfzDsOperSql;
    }

    public void setMfzDsVer(int mfzDsVer) {
        this.mfzDsVer = mfzDsVer;
    }

    public void setMfzDsVerFormatted(String mfzDsVer) {
        setMfzDsVer(PicParser.display(new PicParams("S9(9)V").setUsage(PicUsage.PACKED)).parseInt(mfzDsVer));
    }

    public int getMfzDsVer() {
        return this.mfzDsVer;
    }

    public void setMfzDsTsIniCptz(long mfzDsTsIniCptz) {
        this.mfzDsTsIniCptz = mfzDsTsIniCptz;
    }

    public long getMfzDsTsIniCptz() {
        return this.mfzDsTsIniCptz;
    }

    public void setMfzDsTsEndCptz(long mfzDsTsEndCptz) {
        this.mfzDsTsEndCptz = mfzDsTsEndCptz;
    }

    public long getMfzDsTsEndCptz() {
        return this.mfzDsTsEndCptz;
    }

    public void setMfzDsUtente(String mfzDsUtente) {
        this.mfzDsUtente = Functions.subString(mfzDsUtente, Len.MFZ_DS_UTENTE);
    }

    public String getMfzDsUtente() {
        return this.mfzDsUtente;
    }

    public void setMfzDsStatoElab(char mfzDsStatoElab) {
        this.mfzDsStatoElab = mfzDsStatoElab;
    }

    public void setMfzDsStatoElabFormatted(String mfzDsStatoElab) {
        setMfzDsStatoElab(Functions.charAt(mfzDsStatoElab, Types.CHAR_SIZE));
    }

    public char getMfzDsStatoElab() {
        return this.mfzDsStatoElab;
    }

    public void setMfzTpCausRettifica(String mfzTpCausRettifica) {
        this.mfzTpCausRettifica = Functions.subString(mfzTpCausRettifica, Len.MFZ_TP_CAUS_RETTIFICA);
    }

    public String getMfzTpCausRettifica() {
        return this.mfzTpCausRettifica;
    }

    public String getMfzTpCausRettificaFormatted() {
        return Functions.padBlanks(getMfzTpCausRettifica(), Len.MFZ_TP_CAUS_RETTIFICA);
    }

    public MfzCosOprz getMfzCosOprz() {
        return mfzCosOprz;
    }

    public MfzDtEffMoviFinrio getMfzDtEffMoviFinrio() {
        return mfzDtEffMoviFinrio;
    }

    public MfzDtElab getMfzDtElab() {
        return mfzDtElab;
    }

    public MfzDtRichMovi getMfzDtRichMovi() {
        return mfzDtRichMovi;
    }

    public MfzIdAdes getMfzIdAdes() {
        return mfzIdAdes;
    }

    public MfzIdLiq getMfzIdLiq() {
        return mfzIdLiq;
    }

    public MfzIdMoviChiu getMfzIdMoviChiu() {
        return mfzIdMoviChiu;
    }

    public MfzIdTitCont getMfzIdTitCont() {
        return mfzIdTitCont;
    }

    @Override
    public byte[] serialize() {
        return getMoviFinrioBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_ID_MOVI_FINRIO = 5;
        public static final int MFZ_ID_MOVI_CRZ = 5;
        public static final int MFZ_DT_INI_EFF = 5;
        public static final int MFZ_DT_END_EFF = 5;
        public static final int MFZ_COD_COMP_ANIA = 3;
        public static final int MFZ_TP_MOVI_FINRIO = 2;
        public static final int MFZ_STAT_MOVI = 2;
        public static final int MFZ_DS_RIGA = 6;
        public static final int MFZ_DS_OPER_SQL = 1;
        public static final int MFZ_DS_VER = 5;
        public static final int MFZ_DS_TS_INI_CPTZ = 10;
        public static final int MFZ_DS_TS_END_CPTZ = 10;
        public static final int MFZ_DS_UTENTE = 20;
        public static final int MFZ_DS_STATO_ELAB = 1;
        public static final int MFZ_TP_CAUS_RETTIFICA = 2;
        public static final int MOVI_FINRIO = MFZ_ID_MOVI_FINRIO + MfzIdAdes.Len.MFZ_ID_ADES + MfzIdLiq.Len.MFZ_ID_LIQ + MfzIdTitCont.Len.MFZ_ID_TIT_CONT + MFZ_ID_MOVI_CRZ + MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU + MFZ_DT_INI_EFF + MFZ_DT_END_EFF + MFZ_COD_COMP_ANIA + MFZ_TP_MOVI_FINRIO + MfzDtEffMoviFinrio.Len.MFZ_DT_EFF_MOVI_FINRIO + MfzDtElab.Len.MFZ_DT_ELAB + MfzDtRichMovi.Len.MFZ_DT_RICH_MOVI + MfzCosOprz.Len.MFZ_COS_OPRZ + MFZ_STAT_MOVI + MFZ_DS_RIGA + MFZ_DS_OPER_SQL + MFZ_DS_VER + MFZ_DS_TS_INI_CPTZ + MFZ_DS_TS_END_CPTZ + MFZ_DS_UTENTE + MFZ_DS_STATO_ELAB + MFZ_TP_CAUS_RETTIFICA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_ID_MOVI_FINRIO = 9;
            public static final int MFZ_ID_MOVI_CRZ = 9;
            public static final int MFZ_DT_INI_EFF = 8;
            public static final int MFZ_DT_END_EFF = 8;
            public static final int MFZ_COD_COMP_ANIA = 5;
            public static final int MFZ_DS_RIGA = 10;
            public static final int MFZ_DS_VER = 9;
            public static final int MFZ_DS_TS_INI_CPTZ = 18;
            public static final int MFZ_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
