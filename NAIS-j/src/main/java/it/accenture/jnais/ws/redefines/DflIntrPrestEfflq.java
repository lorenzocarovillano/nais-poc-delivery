package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-INTR-PREST-EFFLQ<br>
 * Variable: DFL-INTR-PREST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIntrPrestEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIntrPrestEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_INTR_PREST_EFFLQ;
    }

    public void setDflIntrPrestEfflq(AfDecimal dflIntrPrestEfflq) {
        writeDecimalAsPacked(Pos.DFL_INTR_PREST_EFFLQ, dflIntrPrestEfflq.copy());
    }

    public void setDflIntrPrestEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_INTR_PREST_EFFLQ, Pos.DFL_INTR_PREST_EFFLQ);
    }

    /**Original name: DFL-INTR-PREST-EFFLQ<br>*/
    public AfDecimal getDflIntrPrestEfflq() {
        return readPackedAsDecimal(Pos.DFL_INTR_PREST_EFFLQ, Len.Int.DFL_INTR_PREST_EFFLQ, Len.Fract.DFL_INTR_PREST_EFFLQ);
    }

    public byte[] getDflIntrPrestEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_INTR_PREST_EFFLQ, Pos.DFL_INTR_PREST_EFFLQ);
        return buffer;
    }

    public void setDflIntrPrestEfflqNull(String dflIntrPrestEfflqNull) {
        writeString(Pos.DFL_INTR_PREST_EFFLQ_NULL, dflIntrPrestEfflqNull, Len.DFL_INTR_PREST_EFFLQ_NULL);
    }

    /**Original name: DFL-INTR-PREST-EFFLQ-NULL<br>*/
    public String getDflIntrPrestEfflqNull() {
        return readString(Pos.DFL_INTR_PREST_EFFLQ_NULL, Len.DFL_INTR_PREST_EFFLQ_NULL);
    }

    public String getDflIntrPrestEfflqNullFormatted() {
        return Functions.padBlanks(getDflIntrPrestEfflqNull(), Len.DFL_INTR_PREST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_INTR_PREST_EFFLQ = 1;
        public static final int DFL_INTR_PREST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_INTR_PREST_EFFLQ = 8;
        public static final int DFL_INTR_PREST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_INTR_PREST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_INTR_PREST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
