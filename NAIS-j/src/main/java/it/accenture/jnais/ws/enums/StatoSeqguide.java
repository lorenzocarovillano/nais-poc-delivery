package it.accenture.jnais.ws.enums;

/**Original name: STATO-SEQGUIDE<br>
 * Variable: STATO-SEQGUIDE from copybook IABVSQG1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class StatoSeqguide {

    //==== PROPERTIES ====
    private char value = 'C';
    public static final char APERTO = 'A';
    public static final char CHIUSO = 'C';

    //==== METHODS ====
    public void setStatoSeqguide(char statoSeqguide) {
        this.value = statoSeqguide;
    }

    public char getStatoSeqguide() {
        return this.value;
    }

    public boolean isAperto() {
        return value == APERTO;
    }

    public void setAperto() {
        value = APERTO;
    }

    public void setChiuso() {
        value = CHIUSO;
    }
}
