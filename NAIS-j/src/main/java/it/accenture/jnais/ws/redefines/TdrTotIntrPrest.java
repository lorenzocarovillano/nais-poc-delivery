package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-INTR-PREST<br>
 * Variable: TDR-TOT-INTR-PREST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_INTR_PREST;
    }

    public void setTdrTotIntrPrest(AfDecimal tdrTotIntrPrest) {
        writeDecimalAsPacked(Pos.TDR_TOT_INTR_PREST, tdrTotIntrPrest.copy());
    }

    public void setTdrTotIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_INTR_PREST, Pos.TDR_TOT_INTR_PREST);
    }

    /**Original name: TDR-TOT-INTR-PREST<br>*/
    public AfDecimal getTdrTotIntrPrest() {
        return readPackedAsDecimal(Pos.TDR_TOT_INTR_PREST, Len.Int.TDR_TOT_INTR_PREST, Len.Fract.TDR_TOT_INTR_PREST);
    }

    public byte[] getTdrTotIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_INTR_PREST, Pos.TDR_TOT_INTR_PREST);
        return buffer;
    }

    public void setTdrTotIntrPrestNull(String tdrTotIntrPrestNull) {
        writeString(Pos.TDR_TOT_INTR_PREST_NULL, tdrTotIntrPrestNull, Len.TDR_TOT_INTR_PREST_NULL);
    }

    /**Original name: TDR-TOT-INTR-PREST-NULL<br>*/
    public String getTdrTotIntrPrestNull() {
        return readString(Pos.TDR_TOT_INTR_PREST_NULL, Len.TDR_TOT_INTR_PREST_NULL);
    }

    public String getTdrTotIntrPrestNullFormatted() {
        return Functions.padBlanks(getTdrTotIntrPrestNull(), Len.TDR_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_PREST = 1;
        public static final int TDR_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_PREST = 8;
        public static final int TDR_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
