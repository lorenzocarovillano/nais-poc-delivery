package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRSTZ-INI<br>
 * Variable: TGA-PRSTZ-INI from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrstzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrstzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRSTZ_INI;
    }

    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        writeDecimalAsPacked(Pos.TGA_PRSTZ_INI, tgaPrstzIni.copy());
    }

    public void setTgaPrstzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRSTZ_INI, Pos.TGA_PRSTZ_INI);
    }

    /**Original name: TGA-PRSTZ-INI<br>*/
    public AfDecimal getTgaPrstzIni() {
        return readPackedAsDecimal(Pos.TGA_PRSTZ_INI, Len.Int.TGA_PRSTZ_INI, Len.Fract.TGA_PRSTZ_INI);
    }

    public byte[] getTgaPrstzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRSTZ_INI, Pos.TGA_PRSTZ_INI);
        return buffer;
    }

    public void setTgaPrstzIniNull(String tgaPrstzIniNull) {
        writeString(Pos.TGA_PRSTZ_INI_NULL, tgaPrstzIniNull, Len.TGA_PRSTZ_INI_NULL);
    }

    /**Original name: TGA-PRSTZ-INI-NULL<br>*/
    public String getTgaPrstzIniNull() {
        return readString(Pos.TGA_PRSTZ_INI_NULL, Len.TGA_PRSTZ_INI_NULL);
    }

    public String getTgaPrstzIniNullFormatted() {
        return Functions.padBlanks(getTgaPrstzIniNull(), Len.TGA_PRSTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI = 1;
        public static final int TGA_PRSTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRSTZ_INI = 8;
        public static final int TGA_PRSTZ_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRSTZ_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
