package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-SPE-AGE<br>
 * Variable: WDTC-SPE-AGE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_SPE_AGE;
    }

    public void setWdtcSpeAge(AfDecimal wdtcSpeAge) {
        writeDecimalAsPacked(Pos.WDTC_SPE_AGE, wdtcSpeAge.copy());
    }

    public void setWdtcSpeAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_SPE_AGE, Pos.WDTC_SPE_AGE);
    }

    /**Original name: WDTC-SPE-AGE<br>*/
    public AfDecimal getWdtcSpeAge() {
        return readPackedAsDecimal(Pos.WDTC_SPE_AGE, Len.Int.WDTC_SPE_AGE, Len.Fract.WDTC_SPE_AGE);
    }

    public byte[] getWdtcSpeAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_SPE_AGE, Pos.WDTC_SPE_AGE);
        return buffer;
    }

    public void initWdtcSpeAgeSpaces() {
        fill(Pos.WDTC_SPE_AGE, Len.WDTC_SPE_AGE, Types.SPACE_CHAR);
    }

    public void setWdtcSpeAgeNull(String wdtcSpeAgeNull) {
        writeString(Pos.WDTC_SPE_AGE_NULL, wdtcSpeAgeNull, Len.WDTC_SPE_AGE_NULL);
    }

    /**Original name: WDTC-SPE-AGE-NULL<br>*/
    public String getWdtcSpeAgeNull() {
        return readString(Pos.WDTC_SPE_AGE_NULL, Len.WDTC_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_SPE_AGE = 1;
        public static final int WDTC_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_SPE_AGE = 8;
        public static final int WDTC_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
