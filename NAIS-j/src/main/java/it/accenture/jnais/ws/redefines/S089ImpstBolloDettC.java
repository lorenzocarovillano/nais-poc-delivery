package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-BOLLO-DETT-C<br>
 * Variable: S089-IMPST-BOLLO-DETT-C from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_BOLLO_DETT_C;
    }

    public void setWlquImpstBolloDettC(AfDecimal wlquImpstBolloDettC) {
        writeDecimalAsPacked(Pos.S089_IMPST_BOLLO_DETT_C, wlquImpstBolloDettC.copy());
    }

    public void setWlquImpstBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_BOLLO_DETT_C, Pos.S089_IMPST_BOLLO_DETT_C);
    }

    /**Original name: WLQU-IMPST-BOLLO-DETT-C<br>*/
    public AfDecimal getWlquImpstBolloDettC() {
        return readPackedAsDecimal(Pos.S089_IMPST_BOLLO_DETT_C, Len.Int.WLQU_IMPST_BOLLO_DETT_C, Len.Fract.WLQU_IMPST_BOLLO_DETT_C);
    }

    public byte[] getWlquImpstBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_BOLLO_DETT_C, Pos.S089_IMPST_BOLLO_DETT_C);
        return buffer;
    }

    public void initWlquImpstBolloDettCSpaces() {
        fill(Pos.S089_IMPST_BOLLO_DETT_C, Len.S089_IMPST_BOLLO_DETT_C, Types.SPACE_CHAR);
    }

    public void setWlquImpstBolloDettCNull(String wlquImpstBolloDettCNull) {
        writeString(Pos.S089_IMPST_BOLLO_DETT_C_NULL, wlquImpstBolloDettCNull, Len.WLQU_IMPST_BOLLO_DETT_C_NULL);
    }

    /**Original name: WLQU-IMPST-BOLLO-DETT-C-NULL<br>*/
    public String getWlquImpstBolloDettCNull() {
        return readString(Pos.S089_IMPST_BOLLO_DETT_C_NULL, Len.WLQU_IMPST_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_DETT_C = 1;
        public static final int S089_IMPST_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_DETT_C = 8;
        public static final int WLQU_IMPST_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
