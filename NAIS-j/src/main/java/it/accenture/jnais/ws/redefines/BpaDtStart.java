package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BPA-DT-START<br>
 * Variable: BPA-DT-START from program IABS0130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BpaDtStart extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BpaDtStart() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BPA_DT_START;
    }

    public void setBpaDtStart(long bpaDtStart) {
        writeLongAsPacked(Pos.BPA_DT_START, bpaDtStart, Len.Int.BPA_DT_START);
    }

    public void setBpaDtStartFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BPA_DT_START, Pos.BPA_DT_START);
    }

    /**Original name: BPA-DT-START<br>*/
    public long getBpaDtStart() {
        return readPackedAsLong(Pos.BPA_DT_START, Len.Int.BPA_DT_START);
    }

    public byte[] getBpaDtStartAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BPA_DT_START, Pos.BPA_DT_START);
        return buffer;
    }

    public void initBpaDtStartHighValues() {
        fill(Pos.BPA_DT_START, Len.BPA_DT_START, Types.HIGH_CHAR_VAL);
    }

    public void setBpaDtStartNull(String bpaDtStartNull) {
        writeString(Pos.BPA_DT_START_NULL, bpaDtStartNull, Len.BPA_DT_START_NULL);
    }

    /**Original name: BPA-DT-START-NULL<br>*/
    public String getBpaDtStartNull() {
        return readString(Pos.BPA_DT_START_NULL, Len.BPA_DT_START_NULL);
    }

    public String getBpaDtStartNullFormatted() {
        return Functions.padBlanks(getBpaDtStartNull(), Len.BPA_DT_START_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BPA_DT_START = 1;
        public static final int BPA_DT_START_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BPA_DT_START = 10;
        public static final int BPA_DT_START_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BPA_DT_START = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
