package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-SOPR-SAN<br>
 * Variable: WTGA-IMP-SOPR-SAN from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_SOPR_SAN;
    }

    public void setWtgaImpSoprSan(AfDecimal wtgaImpSoprSan) {
        writeDecimalAsPacked(Pos.WTGA_IMP_SOPR_SAN, wtgaImpSoprSan.copy());
    }

    public void setWtgaImpSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_SOPR_SAN, Pos.WTGA_IMP_SOPR_SAN);
    }

    /**Original name: WTGA-IMP-SOPR-SAN<br>*/
    public AfDecimal getWtgaImpSoprSan() {
        return readPackedAsDecimal(Pos.WTGA_IMP_SOPR_SAN, Len.Int.WTGA_IMP_SOPR_SAN, Len.Fract.WTGA_IMP_SOPR_SAN);
    }

    public byte[] getWtgaImpSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_SOPR_SAN, Pos.WTGA_IMP_SOPR_SAN);
        return buffer;
    }

    public void initWtgaImpSoprSanSpaces() {
        fill(Pos.WTGA_IMP_SOPR_SAN, Len.WTGA_IMP_SOPR_SAN, Types.SPACE_CHAR);
    }

    public void setWtgaImpSoprSanNull(String wtgaImpSoprSanNull) {
        writeString(Pos.WTGA_IMP_SOPR_SAN_NULL, wtgaImpSoprSanNull, Len.WTGA_IMP_SOPR_SAN_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-SAN-NULL<br>*/
    public String getWtgaImpSoprSanNull() {
        return readString(Pos.WTGA_IMP_SOPR_SAN_NULL, Len.WTGA_IMP_SOPR_SAN_NULL);
    }

    public String getWtgaImpSoprSanNullFormatted() {
        return Functions.padBlanks(getWtgaImpSoprSanNull(), Len.WTGA_IMP_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_SAN = 1;
        public static final int WTGA_IMP_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_SAN = 8;
        public static final int WTGA_IMP_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
