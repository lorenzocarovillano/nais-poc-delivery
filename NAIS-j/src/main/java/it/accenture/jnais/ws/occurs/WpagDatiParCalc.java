package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpagValDt;
import it.accenture.jnais.ws.redefines.WpagValImp;
import it.accenture.jnais.ws.redefines.WpagValNum;
import it.accenture.jnais.ws.redefines.WpagValPc;
import it.accenture.jnais.ws.redefines.WpagValTs;

/**Original name: WPAG-DATI-PAR-CALC<br>
 * Variables: WPAG-DATI-PAR-CALC from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagDatiParCalc {

    //==== PROPERTIES ====
    //Original name: WPAG-COD-PARAM
    private String wpagCodParam = DefaultValues.stringVal(Len.WPAG_COD_PARAM);
    //Original name: WPAG-TP-D
    private String wpagTpD = DefaultValues.stringVal(Len.WPAG_TP_D);
    //Original name: WPAG-VAL-DT
    private WpagValDt wpagValDt = new WpagValDt();
    //Original name: WPAG-VAL-IMP
    private WpagValImp wpagValImp = new WpagValImp();
    //Original name: WPAG-VAL-TS
    private WpagValTs wpagValTs = new WpagValTs();
    //Original name: WPAG-VAL-NUM
    private WpagValNum wpagValNum = new WpagValNum();
    //Original name: WPAG-VAL-PC
    private WpagValPc wpagValPc = new WpagValPc();
    //Original name: WPAG-VAL-STR
    private String wpagValStr = DefaultValues.stringVal(Len.WPAG_VAL_STR);
    //Original name: WPAG-VAL-FL
    private char wpagValFl = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWpagDatiParCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagCodParam = MarshalByte.readString(buffer, position, Len.WPAG_COD_PARAM);
        position += Len.WPAG_COD_PARAM;
        wpagTpD = MarshalByte.readString(buffer, position, Len.WPAG_TP_D);
        position += Len.WPAG_TP_D;
        wpagValDt.setWpagValDtFromBuffer(buffer, position);
        position += WpagValDt.Len.WPAG_VAL_DT;
        wpagValImp.setWpagValImpFromBuffer(buffer, position);
        position += WpagValImp.Len.WPAG_VAL_IMP;
        wpagValTs.setWpagValTsFromBuffer(buffer, position);
        position += WpagValTs.Len.WPAG_VAL_TS;
        wpagValNum.setWpagValNumFromBuffer(buffer, position);
        position += WpagValNum.Len.WPAG_VAL_NUM;
        wpagValPc.setWpagValPcFromBuffer(buffer, position);
        position += WpagValPc.Len.WPAG_VAL_PC;
        wpagValStr = MarshalByte.readString(buffer, position, Len.WPAG_VAL_STR);
        position += Len.WPAG_VAL_STR;
        wpagValFl = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWpagDatiParCalcBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagCodParam, Len.WPAG_COD_PARAM);
        position += Len.WPAG_COD_PARAM;
        MarshalByte.writeString(buffer, position, wpagTpD, Len.WPAG_TP_D);
        position += Len.WPAG_TP_D;
        wpagValDt.getWpagValDtAsBuffer(buffer, position);
        position += WpagValDt.Len.WPAG_VAL_DT;
        wpagValImp.getWpagValImpAsBuffer(buffer, position);
        position += WpagValImp.Len.WPAG_VAL_IMP;
        wpagValTs.getWpagValTsAsBuffer(buffer, position);
        position += WpagValTs.Len.WPAG_VAL_TS;
        wpagValNum.getWpagValNumAsBuffer(buffer, position);
        position += WpagValNum.Len.WPAG_VAL_NUM;
        wpagValPc.getWpagValPcAsBuffer(buffer, position);
        position += WpagValPc.Len.WPAG_VAL_PC;
        MarshalByte.writeString(buffer, position, wpagValStr, Len.WPAG_VAL_STR);
        position += Len.WPAG_VAL_STR;
        MarshalByte.writeChar(buffer, position, wpagValFl);
        return buffer;
    }

    public void initWpagDatiParCalcSpaces() {
        wpagCodParam = "";
        wpagTpD = "";
        wpagValDt.initWpagValDtSpaces();
        wpagValImp.initWpagValImpSpaces();
        wpagValTs.initWpagValTsSpaces();
        wpagValNum.initWpagValNumSpaces();
        wpagValPc.initWpagValPcSpaces();
        wpagValStr = "";
        wpagValFl = Types.SPACE_CHAR;
    }

    public void setWpagCodParam(String wpagCodParam) {
        this.wpagCodParam = Functions.subString(wpagCodParam, Len.WPAG_COD_PARAM);
    }

    public String getWpagCodParam() {
        return this.wpagCodParam;
    }

    public void setWpagTpD(String wpagTpD) {
        this.wpagTpD = Functions.subString(wpagTpD, Len.WPAG_TP_D);
    }

    public String getWpagTpD() {
        return this.wpagTpD;
    }

    public void setWpagValStr(String wpagValStr) {
        this.wpagValStr = Functions.subString(wpagValStr, Len.WPAG_VAL_STR);
    }

    public String getWpagValStr() {
        return this.wpagValStr;
    }

    public void setWpagValFl(char wpagValFl) {
        this.wpagValFl = wpagValFl;
    }

    public char getWpagValFl() {
        return this.wpagValFl;
    }

    public WpagValDt getWpagValDt() {
        return wpagValDt;
    }

    public WpagValImp getWpagValImp() {
        return wpagValImp;
    }

    public WpagValNum getWpagValNum() {
        return wpagValNum;
    }

    public WpagValPc getWpagValPc() {
        return wpagValPc;
    }

    public WpagValTs getWpagValTs() {
        return wpagValTs;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_COD_PARAM = 20;
        public static final int WPAG_TP_D = 2;
        public static final int WPAG_VAL_STR = 12;
        public static final int WPAG_VAL_FL = 1;
        public static final int WPAG_DATI_PAR_CALC = WPAG_COD_PARAM + WPAG_TP_D + WpagValDt.Len.WPAG_VAL_DT + WpagValImp.Len.WPAG_VAL_IMP + WpagValTs.Len.WPAG_VAL_TS + WpagValNum.Len.WPAG_VAL_NUM + WpagValPc.Len.WPAG_VAL_PC + WPAG_VAL_STR + WPAG_VAL_FL;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
