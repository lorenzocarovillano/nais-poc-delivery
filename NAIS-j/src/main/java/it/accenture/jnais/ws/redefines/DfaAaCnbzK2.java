package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-AA-CNBZ-K2<br>
 * Variable: DFA-AA-CNBZ-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAaCnbzK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAaCnbzK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_AA_CNBZ_K2;
    }

    public void setDfaAaCnbzK2(short dfaAaCnbzK2) {
        writeShortAsPacked(Pos.DFA_AA_CNBZ_K2, dfaAaCnbzK2, Len.Int.DFA_AA_CNBZ_K2);
    }

    public void setDfaAaCnbzK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_AA_CNBZ_K2, Pos.DFA_AA_CNBZ_K2);
    }

    /**Original name: DFA-AA-CNBZ-K2<br>*/
    public short getDfaAaCnbzK2() {
        return readPackedAsShort(Pos.DFA_AA_CNBZ_K2, Len.Int.DFA_AA_CNBZ_K2);
    }

    public byte[] getDfaAaCnbzK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_AA_CNBZ_K2, Pos.DFA_AA_CNBZ_K2);
        return buffer;
    }

    public void setDfaAaCnbzK2Null(String dfaAaCnbzK2Null) {
        writeString(Pos.DFA_AA_CNBZ_K2_NULL, dfaAaCnbzK2Null, Len.DFA_AA_CNBZ_K2_NULL);
    }

    /**Original name: DFA-AA-CNBZ-K2-NULL<br>*/
    public String getDfaAaCnbzK2Null() {
        return readString(Pos.DFA_AA_CNBZ_K2_NULL, Len.DFA_AA_CNBZ_K2_NULL);
    }

    public String getDfaAaCnbzK2NullFormatted() {
        return Functions.padBlanks(getDfaAaCnbzK2Null(), Len.DFA_AA_CNBZ_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_AA_CNBZ_K2 = 1;
        public static final int DFA_AA_CNBZ_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_AA_CNBZ_K2 = 3;
        public static final int DFA_AA_CNBZ_K2_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_AA_CNBZ_K2 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
