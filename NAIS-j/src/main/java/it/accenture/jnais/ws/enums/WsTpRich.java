package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-RICH<br>
 * Variable: WS-TP-RICH from copybook LLBV0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpRich {

    //==== PROPERTIES ====
    private String value = "";
    public static final String ESTRAZ_MASS = "B1";
    public static final String AGG_ESTRAZ = "B2";
    public static final String CALC_RISERV = "B3";
    public static final String CONSOLIDAM = "B4";
    public static final String ESTR_E_CALC = "B5";
    public static final String AGG_ESTR_E_CALC = "B6";
    public static final String VERIFICA = "B7";
    public static final String CERTIF_ESTR = "B8";
    public static final String CERTIFIC_CALC = "B9";
    public static final String AGG_CALC = "BA";
    public static final String SVECCH_PREN_ANN = "BB";
    public static final String ANN_ESTRAZ_MASS = "BC";
    public static final String ESTR_SOMM_PAG = "BD";
    public static final String AGG_SOMM_PAG = "BE";
    public static final String CERT_SOMM_PAG = "BF";
    public static final String CONSOL_SOMM_PAG = "BG";

    //==== METHODS ====
    public void setTpRich(String tpRich) {
        this.value = Functions.subString(tpRich, Len.TP_RICH);
    }

    public String getTpRich() {
        return this.value;
    }

    public boolean isEstrazMass() {
        return value.equals(ESTRAZ_MASS);
    }

    public void setEstrazMass() {
        value = ESTRAZ_MASS;
    }

    public boolean isAggEstraz() {
        return value.equals(AGG_ESTRAZ);
    }

    public void setAggEstraz() {
        value = AGG_ESTRAZ;
    }

    public void setCalcRiserv() {
        value = CALC_RISERV;
    }

    public boolean isAggEstrECalc() {
        return value.equals(AGG_ESTR_E_CALC);
    }

    public boolean isAggCalc() {
        return value.equals(AGG_CALC);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_RICH = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
