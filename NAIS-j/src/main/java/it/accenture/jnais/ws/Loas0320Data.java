package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.DettTitDiRat;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.TitRat;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.ws.enums.Flag;
import it.accenture.jnais.ws.enums.WkFineDtr;
import it.accenture.jnais.ws.occurs.WdtrTabDettTira;
import it.accenture.jnais.ws.occurs.WtdrTabTitRat;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0320<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0320Data {

    //==== PROPERTIES ====
    public static final int WTDR_TAB_TIT_RAT_MAXOCCURS = 12;
    public static final int WDTR_TAB_DETT_TIRA_MAXOCCURS = 60;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *      VARIABILI DI WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LOAS0320";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: IX-INDICI
    private IxIndiciLoas0320 ixIndici = new IxIndiciLoas0320();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IO-LCCS0090
    private LinkArea areaIoLccs0090 = new LinkArea();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: TIT-RAT
    private TitRat titRat = new TitRat();
    //Original name: DETT-TIT-DI-RAT
    private DettTitDiRat dettTitDiRat = new DettTitDiRat();
    //Original name: WK-DTR-MAX-A
    private short wkDtrMaxA = ((short)60);
    //Original name: WK-TDR-MAX-A
    private short wkTdrMaxA = ((short)12);
    /**Original name: WS-ID-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *          CAMPI DI APPOGGIO VARI
	 * ----------------------------------------------------------------*</pre>*/
    private int wsIdGaranzia = DefaultValues.INT_VAL;
    //Original name: WS-ID-PTF-GAR
    private int wsIdPtfGar = DefaultValues.INT_VAL;
    //Original name: WS-ID-TIT-RAT
    private int wsIdTitRat = DefaultValues.INT_VAL;
    //Original name: WS-ID-PTF-TIT-RAT
    private int wsIdPtfTitRat = DefaultValues.INT_VAL;
    //Original name: FLAG
    private Flag flag = new Flag();
    //Original name: WK-FINE-DTR
    private WkFineDtr wkFineDtr = new WkFineDtr();
    //Original name: WK-LABEL-ERR
    private String wkLabelErr = DefaultValues.stringVal(Len.WK_LABEL_ERR);
    /**Original name: WK-ID-TIT-RAT<br>
	 * <pre> -- ID Titolo Di Rata</pre>*/
    private int wkIdTitRat = DefaultValues.INT_VAL;
    //Original name: S234-DATI-INPUT
    private S234DatiInput s234DatiInput = new S234DatiInput();
    //Original name: S234-DATI-OUTPUT
    private WcomDatiOutput s234DatiOutput = new WcomDatiOutput();
    //Original name: WRIC-AREA-RICHIESTA
    private WricAreaRichiesta wricAreaRichiesta = new WricAreaRichiesta();
    //Original name: WTDR-ELE-TDR-MAX
    private short wtdrEleTdrMax = ((short)0);
    //Original name: WTDR-TAB-TIT-RAT
    private WtdrTabTitRat[] wtdrTabTitRat = new WtdrTabTitRat[WTDR_TAB_TIT_RAT_MAXOCCURS];
    //Original name: WDTR-ELE-DTR-MAX
    private short wdtrEleDtrMax = ((short)0);
    //Original name: WDTR-TAB-DETT-TIRA
    private WdtrTabDettTira[] wdtrTabDettTira = new WdtrTabDettTira[WDTR_TAB_DETT_TIRA_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Loas0320Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wtdrTabTitRatIdx = 1; wtdrTabTitRatIdx <= WTDR_TAB_TIT_RAT_MAXOCCURS; wtdrTabTitRatIdx++) {
            wtdrTabTitRat[wtdrTabTitRatIdx - 1] = new WtdrTabTitRat();
        }
        for (int wdtrTabDettTiraIdx = 1; wdtrTabDettTiraIdx <= WDTR_TAB_DETT_TIRA_MAXOCCURS; wdtrTabDettTiraIdx++) {
            wdtrTabDettTira[wdtrTabDettTiraIdx - 1] = new WdtrTabDettTira();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public short getWkDtrMaxA() {
        return this.wkDtrMaxA;
    }

    public short getWkTdrMaxA() {
        return this.wkTdrMaxA;
    }

    public void setWsIdGaranzia(int wsIdGaranzia) {
        this.wsIdGaranzia = wsIdGaranzia;
    }

    public int getWsIdGaranzia() {
        return this.wsIdGaranzia;
    }

    public void setWsIdPtfGar(int wsIdPtfGar) {
        this.wsIdPtfGar = wsIdPtfGar;
    }

    public int getWsIdPtfGar() {
        return this.wsIdPtfGar;
    }

    public void setWsIdTitRat(int wsIdTitRat) {
        this.wsIdTitRat = wsIdTitRat;
    }

    public int getWsIdTitRat() {
        return this.wsIdTitRat;
    }

    public void setWsIdPtfTitRat(int wsIdPtfTitRat) {
        this.wsIdPtfTitRat = wsIdPtfTitRat;
    }

    public int getWsIdPtfTitRat() {
        return this.wsIdPtfTitRat;
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public void setWkIdTitRat(int wkIdTitRat) {
        this.wkIdTitRat = wkIdTitRat;
    }

    public int getWkIdTitRat() {
        return this.wkIdTitRat;
    }

    public void setWtdrEleTdrMax(short wtdrEleTdrMax) {
        this.wtdrEleTdrMax = wtdrEleTdrMax;
    }

    public short getWtdrEleTdrMax() {
        return this.wtdrEleTdrMax;
    }

    public void setWdtrEleDtrMax(short wdtrEleDtrMax) {
        this.wdtrEleDtrMax = wdtrEleDtrMax;
    }

    public short getWdtrEleDtrMax() {
        return this.wdtrEleDtrMax;
    }

    public LinkArea getAreaIoLccs0090() {
        return areaIoLccs0090;
    }

    public DettTitDiRat getDettTitDiRat() {
        return dettTitDiRat;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Flag getFlag() {
        return flag;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IxIndiciLoas0320 getIxIndici() {
        return ixIndici;
    }

    public S234DatiInput getS234DatiInput() {
        return s234DatiInput;
    }

    public WcomDatiOutput getS234DatiOutput() {
        return s234DatiOutput;
    }

    public TitRat getTitRat() {
        return titRat;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public WdtrTabDettTira getWdtrTabDettTira(int idx) {
        return wdtrTabDettTira[idx - 1];
    }

    public WkFineDtr getWkFineDtr() {
        return wkFineDtr;
    }

    public WricAreaRichiesta getWricAreaRichiesta() {
        return wricAreaRichiesta;
    }

    public WtdrTabTitRat getWtdrTabTitRat(int idx) {
        return wtdrTabTitRat[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LABEL_ERR = 30;
        public static final int WK_STRING = 85;
        public static final int WK_TABELLA = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
