package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BJS-TP-MOVI<br>
 * Variable: BJS-TP-MOVI from program IABS0060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BjsTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BjsTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BJS_TP_MOVI;
    }

    public void setBjsTpMovi(int bjsTpMovi) {
        writeIntAsPacked(Pos.BJS_TP_MOVI, bjsTpMovi, Len.Int.BJS_TP_MOVI);
    }

    public void setBjsTpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BJS_TP_MOVI, Pos.BJS_TP_MOVI);
    }

    /**Original name: BJS-TP-MOVI<br>*/
    public int getBjsTpMovi() {
        return readPackedAsInt(Pos.BJS_TP_MOVI, Len.Int.BJS_TP_MOVI);
    }

    public byte[] getBjsTpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BJS_TP_MOVI, Pos.BJS_TP_MOVI);
        return buffer;
    }

    public void setBjsTpMoviNull(String bjsTpMoviNull) {
        writeString(Pos.BJS_TP_MOVI_NULL, bjsTpMoviNull, Len.BJS_TP_MOVI_NULL);
    }

    /**Original name: BJS-TP-MOVI-NULL<br>*/
    public String getBjsTpMoviNull() {
        return readString(Pos.BJS_TP_MOVI_NULL, Len.BJS_TP_MOVI_NULL);
    }

    public String getBjsTpMoviNullFormatted() {
        return Functions.padBlanks(getBjsTpMoviNull(), Len.BJS_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BJS_TP_MOVI = 1;
        public static final int BJS_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BJS_TP_MOVI = 3;
        public static final int BJS_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BJS_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
