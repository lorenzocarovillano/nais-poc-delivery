package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvp861;
import it.accenture.jnais.copy.Wp86Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WP86-TAB-MOT-LIQ<br>
 * Variables: WP86-TAB-MOT-LIQ from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wp86TabMotLiq {

    //==== PROPERTIES ====
    //Original name: LCCVP861
    private Lccvp861 lccvp861 = new Lccvp861();

    //==== METHODS ====
    public void setWp86TabMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvp861.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvp861.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvp861.Len.Int.ID_PTF, 0));
        position += Lccvp861.Len.ID_PTF;
        lccvp861.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWp86TabMotLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvp861.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvp861.getIdPtf(), Lccvp861.Len.Int.ID_PTF, 0);
        position += Lccvp861.Len.ID_PTF;
        lccvp861.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWp86TabMotLiqSpaces() {
        lccvp861.initLccvp861Spaces();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WP86_TAB_MOT_LIQ = WpolStatus.Len.STATUS + Lccvp861.Len.ID_PTF + Wp86Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
