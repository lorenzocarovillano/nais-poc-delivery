package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-PNL<br>
 * Variable: S089-IMP-PNL from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpPnl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpPnl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_PNL;
    }

    public void setWlquImpPnl(AfDecimal wlquImpPnl) {
        writeDecimalAsPacked(Pos.S089_IMP_PNL, wlquImpPnl.copy());
    }

    public void setWlquImpPnlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_PNL, Pos.S089_IMP_PNL);
    }

    /**Original name: WLQU-IMP-PNL<br>*/
    public AfDecimal getWlquImpPnl() {
        return readPackedAsDecimal(Pos.S089_IMP_PNL, Len.Int.WLQU_IMP_PNL, Len.Fract.WLQU_IMP_PNL);
    }

    public byte[] getWlquImpPnlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_PNL, Pos.S089_IMP_PNL);
        return buffer;
    }

    public void initWlquImpPnlSpaces() {
        fill(Pos.S089_IMP_PNL, Len.S089_IMP_PNL, Types.SPACE_CHAR);
    }

    public void setWlquImpPnlNull(String wlquImpPnlNull) {
        writeString(Pos.S089_IMP_PNL_NULL, wlquImpPnlNull, Len.WLQU_IMP_PNL_NULL);
    }

    /**Original name: WLQU-IMP-PNL-NULL<br>*/
    public String getWlquImpPnlNull() {
        return readString(Pos.S089_IMP_PNL_NULL, Len.WLQU_IMP_PNL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_PNL = 1;
        public static final int S089_IMP_PNL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_PNL = 8;
        public static final int WLQU_IMP_PNL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
