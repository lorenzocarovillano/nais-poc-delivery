package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0140-CODICE-TP-PREMIO-T<br>
 * Variable: ISPC0140-CODICE-TP-PREMIO-T from copybook ISPC0140<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0140CodiceTpPremioT {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.CODICE_TP_PREMIO_T);
    public static final String ANNUO_T = "ANNUO";
    public static final String RATA_T = "RATA";
    public static final String FIRMA_T = "FIRMA";

    //==== METHODS ====
    public void setCodiceTpPremioT(String codiceTpPremioT) {
        this.value = Functions.subString(codiceTpPremioT, Len.CODICE_TP_PREMIO_T);
    }

    public String getCodiceTpPremioT() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_TP_PREMIO_T = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
