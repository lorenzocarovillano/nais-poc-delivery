package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvtga1;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WTGA-TAB-TRAN<br>
 * Variables: WTGA-TAB-TRAN from copybook LCCVTGAB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WtgaTabTran {

    //==== PROPERTIES ====
    //Original name: LCCVTGA1
    private Lccvtga1 lccvtga1 = new Lccvtga1();

    //==== METHODS ====
    public void setWtgaTabTranBytes(byte[] buffer) {
        setTabTranBytes(buffer, 1);
    }

    public byte[] getTabTranBytes() {
        byte[] buffer = new byte[Len.TAB_TRAN];
        return getTabTranBytes(buffer, 1);
    }

    public void setTabTranBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtga1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtga1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtga1.Len.Int.ID_PTF, 0));
        position += Lccvtga1.Len.ID_PTF;
        lccvtga1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getTabTranBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvtga1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvtga1.getIdPtf(), Lccvtga1.Len.Int.ID_PTF, 0);
        position += Lccvtga1.Len.ID_PTF;
        lccvtga1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initTabTranSpaces() {
        lccvtga1.initLccvtga1Spaces();
    }

    public Lccvtga1 getLccvtga1() {
        return lccvtga1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TAB_TRAN = WpolStatus.Len.STATUS + Lccvtga1.Len.ID_PTF + WtgaDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
