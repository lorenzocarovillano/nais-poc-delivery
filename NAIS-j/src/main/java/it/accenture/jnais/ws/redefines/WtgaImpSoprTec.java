package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-SOPR-TEC<br>
 * Variable: WTGA-IMP-SOPR-TEC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_SOPR_TEC;
    }

    public void setWtgaImpSoprTec(AfDecimal wtgaImpSoprTec) {
        writeDecimalAsPacked(Pos.WTGA_IMP_SOPR_TEC, wtgaImpSoprTec.copy());
    }

    public void setWtgaImpSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_SOPR_TEC, Pos.WTGA_IMP_SOPR_TEC);
    }

    /**Original name: WTGA-IMP-SOPR-TEC<br>*/
    public AfDecimal getWtgaImpSoprTec() {
        return readPackedAsDecimal(Pos.WTGA_IMP_SOPR_TEC, Len.Int.WTGA_IMP_SOPR_TEC, Len.Fract.WTGA_IMP_SOPR_TEC);
    }

    public byte[] getWtgaImpSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_SOPR_TEC, Pos.WTGA_IMP_SOPR_TEC);
        return buffer;
    }

    public void initWtgaImpSoprTecSpaces() {
        fill(Pos.WTGA_IMP_SOPR_TEC, Len.WTGA_IMP_SOPR_TEC, Types.SPACE_CHAR);
    }

    public void setWtgaImpSoprTecNull(String wtgaImpSoprTecNull) {
        writeString(Pos.WTGA_IMP_SOPR_TEC_NULL, wtgaImpSoprTecNull, Len.WTGA_IMP_SOPR_TEC_NULL);
    }

    /**Original name: WTGA-IMP-SOPR-TEC-NULL<br>*/
    public String getWtgaImpSoprTecNull() {
        return readString(Pos.WTGA_IMP_SOPR_TEC_NULL, Len.WTGA_IMP_SOPR_TEC_NULL);
    }

    public String getWtgaImpSoprTecNullFormatted() {
        return Functions.padBlanks(getWtgaImpSoprTecNull(), Len.WTGA_IMP_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_TEC = 1;
        public static final int WTGA_IMP_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_SOPR_TEC = 8;
        public static final int WTGA_IMP_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
