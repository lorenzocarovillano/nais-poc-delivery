package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-FLAG-CONVERSAZIONE<br>
 * Variable: WCOM-FLAG-CONVERSAZIONE from program LOAS0110<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomFlagConversazione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char VENDITA = 'V';
    public static final char POST_VENDITA = 'P';

    //==== METHODS ====
    public void setWcomFlagConversazione(char wcomFlagConversazione) {
        this.value = wcomFlagConversazione;
    }

    public char getWcomFlagConversazione() {
        return this.value;
    }

    public void setPostVendita() {
        value = POST_VENDITA;
    }
}
