package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-RB-IN<br>
 * Variable: PCO-DT-ULTC-RB-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcRbIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcRbIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_RB_IN;
    }

    public void setPcoDtUltcRbIn(int pcoDtUltcRbIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_RB_IN, pcoDtUltcRbIn, Len.Int.PCO_DT_ULTC_RB_IN);
    }

    public void setPcoDtUltcRbInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_RB_IN, Pos.PCO_DT_ULTC_RB_IN);
    }

    /**Original name: PCO-DT-ULTC-RB-IN<br>*/
    public int getPcoDtUltcRbIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_RB_IN, Len.Int.PCO_DT_ULTC_RB_IN);
    }

    public byte[] getPcoDtUltcRbInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_RB_IN, Pos.PCO_DT_ULTC_RB_IN);
        return buffer;
    }

    public void initPcoDtUltcRbInHighValues() {
        fill(Pos.PCO_DT_ULTC_RB_IN, Len.PCO_DT_ULTC_RB_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcRbInNull(String pcoDtUltcRbInNull) {
        writeString(Pos.PCO_DT_ULTC_RB_IN_NULL, pcoDtUltcRbInNull, Len.PCO_DT_ULTC_RB_IN_NULL);
    }

    /**Original name: PCO-DT-ULTC-RB-IN-NULL<br>*/
    public String getPcoDtUltcRbInNull() {
        return readString(Pos.PCO_DT_ULTC_RB_IN_NULL, Len.PCO_DT_ULTC_RB_IN_NULL);
    }

    public String getPcoDtUltcRbInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcRbInNull(), Len.PCO_DT_ULTC_RB_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_RB_IN = 1;
        public static final int PCO_DT_ULTC_RB_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_RB_IN = 5;
        public static final int PCO_DT_ULTC_RB_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_RB_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
