package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-NUM-GG-ARR-INTR-PR<br>
 * Variable: WPCO-NUM-GG-ARR-INTR-PR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoNumGgArrIntrPr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoNumGgArrIntrPr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_NUM_GG_ARR_INTR_PR;
    }

    public void setWpcoNumGgArrIntrPr(int wpcoNumGgArrIntrPr) {
        writeIntAsPacked(Pos.WPCO_NUM_GG_ARR_INTR_PR, wpcoNumGgArrIntrPr, Len.Int.WPCO_NUM_GG_ARR_INTR_PR);
    }

    public void setDpcoNumGgArrIntrPrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_NUM_GG_ARR_INTR_PR, Pos.WPCO_NUM_GG_ARR_INTR_PR);
    }

    /**Original name: WPCO-NUM-GG-ARR-INTR-PR<br>*/
    public int getWpcoNumGgArrIntrPr() {
        return readPackedAsInt(Pos.WPCO_NUM_GG_ARR_INTR_PR, Len.Int.WPCO_NUM_GG_ARR_INTR_PR);
    }

    public byte[] getWpcoNumGgArrIntrPrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_NUM_GG_ARR_INTR_PR, Pos.WPCO_NUM_GG_ARR_INTR_PR);
        return buffer;
    }

    public void setWpcoNumGgArrIntrPrNull(String wpcoNumGgArrIntrPrNull) {
        writeString(Pos.WPCO_NUM_GG_ARR_INTR_PR_NULL, wpcoNumGgArrIntrPrNull, Len.WPCO_NUM_GG_ARR_INTR_PR_NULL);
    }

    /**Original name: WPCO-NUM-GG-ARR-INTR-PR-NULL<br>*/
    public String getWpcoNumGgArrIntrPrNull() {
        return readString(Pos.WPCO_NUM_GG_ARR_INTR_PR_NULL, Len.WPCO_NUM_GG_ARR_INTR_PR_NULL);
    }

    public String getWpcoNumGgArrIntrPrNullFormatted() {
        return Functions.padBlanks(getWpcoNumGgArrIntrPrNull(), Len.WPCO_NUM_GG_ARR_INTR_PR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_NUM_GG_ARR_INTR_PR = 1;
        public static final int WPCO_NUM_GG_ARR_INTR_PR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_NUM_GG_ARR_INTR_PR = 3;
        public static final int WPCO_NUM_GG_ARR_INTR_PR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_NUM_GG_ARR_INTR_PR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
