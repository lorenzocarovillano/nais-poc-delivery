package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ETA-AA-1O-ASSTO<br>
 * Variable: WTGA-ETA-AA-1O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaEtaAa1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaEtaAa1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ETA_AA1O_ASSTO;
    }

    public void setWtgaEtaAa1oAssto(short wtgaEtaAa1oAssto) {
        writeShortAsPacked(Pos.WTGA_ETA_AA1O_ASSTO, wtgaEtaAa1oAssto, Len.Int.WTGA_ETA_AA1O_ASSTO);
    }

    public void setWtgaEtaAa1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ETA_AA1O_ASSTO, Pos.WTGA_ETA_AA1O_ASSTO);
    }

    /**Original name: WTGA-ETA-AA-1O-ASSTO<br>*/
    public short getWtgaEtaAa1oAssto() {
        return readPackedAsShort(Pos.WTGA_ETA_AA1O_ASSTO, Len.Int.WTGA_ETA_AA1O_ASSTO);
    }

    public byte[] getWtgaEtaAa1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ETA_AA1O_ASSTO, Pos.WTGA_ETA_AA1O_ASSTO);
        return buffer;
    }

    public void initWtgaEtaAa1oAsstoSpaces() {
        fill(Pos.WTGA_ETA_AA1O_ASSTO, Len.WTGA_ETA_AA1O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWtgaEtaAa1oAsstoNull(String wtgaEtaAa1oAsstoNull) {
        writeString(Pos.WTGA_ETA_AA1O_ASSTO_NULL, wtgaEtaAa1oAsstoNull, Len.WTGA_ETA_AA1O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getWtgaEtaAa1oAsstoNull() {
        return readString(Pos.WTGA_ETA_AA1O_ASSTO_NULL, Len.WTGA_ETA_AA1O_ASSTO_NULL);
    }

    public String getWtgaEtaAa1oAsstoNullFormatted() {
        return Functions.padBlanks(getWtgaEtaAa1oAsstoNull(), Len.WTGA_ETA_AA1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_AA1O_ASSTO = 1;
        public static final int WTGA_ETA_AA1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_AA1O_ASSTO = 2;
        public static final int WTGA_ETA_AA1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ETA_AA1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
