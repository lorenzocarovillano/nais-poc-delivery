package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-EFFLQ<br>
 * Variable: TCL-IMP-EFFLQ from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_EFFLQ;
    }

    public void setTclImpEfflq(AfDecimal tclImpEfflq) {
        writeDecimalAsPacked(Pos.TCL_IMP_EFFLQ, tclImpEfflq.copy());
    }

    public void setTclImpEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_EFFLQ, Pos.TCL_IMP_EFFLQ);
    }

    /**Original name: TCL-IMP-EFFLQ<br>*/
    public AfDecimal getTclImpEfflq() {
        return readPackedAsDecimal(Pos.TCL_IMP_EFFLQ, Len.Int.TCL_IMP_EFFLQ, Len.Fract.TCL_IMP_EFFLQ);
    }

    public byte[] getTclImpEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_EFFLQ, Pos.TCL_IMP_EFFLQ);
        return buffer;
    }

    public void setTclImpEfflqNull(String tclImpEfflqNull) {
        writeString(Pos.TCL_IMP_EFFLQ_NULL, tclImpEfflqNull, Len.TCL_IMP_EFFLQ_NULL);
    }

    /**Original name: TCL-IMP-EFFLQ-NULL<br>*/
    public String getTclImpEfflqNull() {
        return readString(Pos.TCL_IMP_EFFLQ_NULL, Len.TCL_IMP_EFFLQ_NULL);
    }

    public String getTclImpEfflqNullFormatted() {
        return Functions.padBlanks(getTclImpEfflqNull(), Len.TCL_IMP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_EFFLQ = 1;
        public static final int TCL_IMP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_EFFLQ = 8;
        public static final int TCL_IMP_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
