package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-ID-BATCH<br>
 * Variable: WCOM-ID-BATCH from program LOAS0110<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WcomIdBatch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WcomIdBatch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCOM_ID_BATCH;
    }

    public void setWcomIdBatch(int wcomIdBatch) {
        writeIntAsPacked(Pos.WCOM_ID_BATCH, wcomIdBatch, Len.Int.WCOM_ID_BATCH);
    }

    public void setWcomIdBatchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WCOM_ID_BATCH, Pos.WCOM_ID_BATCH);
    }

    /**Original name: WCOM-ID-BATCH<br>*/
    public int getWcomIdBatch() {
        return readPackedAsInt(Pos.WCOM_ID_BATCH, Len.Int.WCOM_ID_BATCH);
    }

    public byte[] getWcomIdBatchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WCOM_ID_BATCH, Pos.WCOM_ID_BATCH);
        return buffer;
    }

    /**Original name: WCOM-ID-BATCH-NULL<br>*/
    public String getWcomIdBatchNull() {
        return readString(Pos.WCOM_ID_BATCH_NULL, Len.WCOM_ID_BATCH_NULL);
    }

    public String getWcomIdBatchNullFormatted() {
        return Functions.padBlanks(getWcomIdBatchNull(), Len.WCOM_ID_BATCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WCOM_ID_BATCH = 1;
        public static final int WCOM_ID_BATCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_ID_BATCH = 5;
        public static final int WCOM_ID_BATCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WCOM_ID_BATCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
