package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import com.modernsystems.ctu.data.NumericDisplaySigned;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IParamComp;
import it.accenture.jnais.copy.Idsv0003Idss0150;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamComp;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.ParamCompDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDSS0150<br>
 * Generated as a class for rule WS.<br>*/
public class Idss0150Data implements IParamComp {

    //==== PROPERTIES ====
    //Original name: WK-PGM
    private String wkPgm = "IDSS0150";
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WK-SQLCODE
    private int wkSqlcode = DefaultValues.INT_VAL;
    //Original name: WK-SQLCODE-ED
    private String wkSqlcodeEd = DefaultValues.stringVal(Len.WK_SQLCODE_ED);
    //Original name: WK-LABEL
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);
    //Original name: DIFFERENZA-DISPLAY
    private String differenzaDisplay = DefaultValues.stringVal(Len.DIFFERENZA_DISPLAY);
    //Original name: LIMITE-POSITIVO
    private short limitePositivo = ((short)59);
    //Original name: LIMITE-NEGATIVO
    private short limiteNegativo = ((short)-40);
    //Original name: WK-DATA-AMG
    private String wkDataAmg = "";
    //Original name: WK-PARAM
    private String wkParam = "0";
    //Original name: WK-LIVELLO-GRAVITA
    private WkLivelloGravita wkLivelloGravita = new WkLivelloGravita();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: IND-PARAM-COMP
    private IndParamComp indParamComp = new IndParamComp();
    //Original name: PARAM-COMP-DB
    private ParamCompDb paramCompDb = new ParamCompDb();
    //Original name: IDSV0003
    private Idsv0003Idss0150 idsv0003 = new Idsv0003Idss0150();
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmFormatted() {
        return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWkSqlcode(int wkSqlcode) {
        this.wkSqlcode = wkSqlcode;
    }

    public int getWkSqlcode() {
        return this.wkSqlcode;
    }

    public String getWkSqlcodeFormatted() {
        return NumericDisplaySigned.asString(getWkSqlcode(), Len.WK_SQLCODE);
    }

    public String getWkSqlcodeAsString() {
        return getWkSqlcodeFormatted();
    }

    public void setWkSqlcodeEd(long wkSqlcodeEd) {
        this.wkSqlcodeEd = PicFormatter.display("Z(8)9-").format(wkSqlcodeEd).toString();
    }

    public long getWkSqlcodeEd() {
        return PicParser.display("Z(8)9-").parseLong(this.wkSqlcodeEd);
    }

    public String getWkSqlcodeEdFormatted() {
        return this.wkSqlcodeEd;
    }

    public String getWkSqlcodeEdAsString() {
        return getWkSqlcodeEdFormatted();
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public void setDifferenzaDisplay(long differenzaDisplay) {
        this.differenzaDisplay = PicFormatter.display("Z9(9)").format(differenzaDisplay).toString();
    }

    public long getDifferenzaDisplay() {
        return PicParser.display("Z9(9)").parseLong(this.differenzaDisplay);
    }

    public String getDifferenzaDisplayFormatted() {
        return this.differenzaDisplay;
    }

    public String getDifferenzaDisplayAsString() {
        return getDifferenzaDisplayFormatted();
    }

    public short getLimitePositivo() {
        return this.limitePositivo;
    }

    public short getLimiteNegativo() {
        return this.limiteNegativo;
    }

    public void setWkDataAmg(String wkDataAmg) {
        this.wkDataAmg = Functions.subString(wkDataAmg, Len.WK_DATA_AMG);
    }

    public String getWkDataAmg() {
        return this.wkDataAmg;
    }

    public String getWkDataAmgFormatted() {
        return Functions.padBlanks(getWkDataAmg(), Len.WK_DATA_AMG);
    }

    public void setWkParam(short wkParam) {
        this.wkParam = NumericDisplay.asString(wkParam, Len.WK_PARAM);
    }

    public void setWkParamFromBuffer(byte[] buffer) {
        wkParam = MarshalByte.readFixedString(buffer, 1, Len.WK_PARAM);
    }

    public short getWkParam() {
        return NumericDisplay.asShort(this.wkParam);
    }

    public String getWkParamFormatted() {
        return this.wkParam;
    }

    @Override
    public short getAaUti() {
        throw new FieldNotMappedException("aaUti");
    }

    @Override
    public void setAaUti(short aaUti) {
        throw new FieldNotMappedException("aaUti");
    }

    @Override
    public Short getAaUtiObj() {
        return ((Short)getAaUti());
    }

    @Override
    public void setAaUtiObj(Short aaUtiObj) {
        setAaUti(((short)aaUtiObj));
    }

    @Override
    public AfDecimal getArrotPre() {
        throw new FieldNotMappedException("arrotPre");
    }

    @Override
    public void setArrotPre(AfDecimal arrotPre) {
        throw new FieldNotMappedException("arrotPre");
    }

    @Override
    public AfDecimal getArrotPreObj() {
        return getArrotPre();
    }

    @Override
    public void setArrotPreObj(AfDecimal arrotPreObj) {
        setArrotPre(new AfDecimal(arrotPreObj, 15, 3));
    }

    @Override
    public char getCalcRshComun() {
        throw new FieldNotMappedException("calcRshComun");
    }

    @Override
    public void setCalcRshComun(char calcRshComun) {
        throw new FieldNotMappedException("calcRshComun");
    }

    @Override
    public String getCodCompIsvap() {
        throw new FieldNotMappedException("codCompIsvap");
    }

    @Override
    public void setCodCompIsvap(String codCompIsvap) {
        throw new FieldNotMappedException("codCompIsvap");
    }

    @Override
    public String getCodCompIsvapObj() {
        return getCodCompIsvap();
    }

    @Override
    public void setCodCompIsvapObj(String codCompIsvapObj) {
        setCodCompIsvap(codCompIsvapObj);
    }

    @Override
    public String getCodCompLdap() {
        throw new FieldNotMappedException("codCompLdap");
    }

    @Override
    public void setCodCompLdap(String codCompLdap) {
        throw new FieldNotMappedException("codCompLdap");
    }

    @Override
    public String getCodCompLdapObj() {
        return getCodCompLdap();
    }

    @Override
    public void setCodCompLdapObj(String codCompLdapObj) {
        setCodCompLdap(codCompLdapObj);
    }

    @Override
    public String getCodFiscMef() {
        throw new FieldNotMappedException("codFiscMef");
    }

    @Override
    public void setCodFiscMef(String codFiscMef) {
        throw new FieldNotMappedException("codFiscMef");
    }

    @Override
    public String getCodFiscMefObj() {
        return getCodFiscMef();
    }

    @Override
    public void setCodFiscMefObj(String codFiscMefObj) {
        setCodFiscMef(codFiscMefObj);
    }

    @Override
    public String getCodSoggFtzAssto() {
        throw new FieldNotMappedException("codSoggFtzAssto");
    }

    @Override
    public void setCodSoggFtzAssto(String codSoggFtzAssto) {
        throw new FieldNotMappedException("codSoggFtzAssto");
    }

    @Override
    public String getCodSoggFtzAsstoObj() {
        return getCodSoggFtzAssto();
    }

    @Override
    public void setCodSoggFtzAsstoObj(String codSoggFtzAsstoObj) {
        setCodSoggFtzAssto(codSoggFtzAsstoObj);
    }

    @Override
    public String getCodTratCirt() {
        throw new FieldNotMappedException("codTratCirt");
    }

    @Override
    public void setCodTratCirt(String codTratCirt) {
        throw new FieldNotMappedException("codTratCirt");
    }

    @Override
    public String getCodTratCirtObj() {
        return getCodTratCirt();
    }

    @Override
    public void setCodTratCirtObj(String codTratCirtObj) {
        setCodTratCirt(codTratCirtObj);
    }

    @Override
    public char getCrz1aRatIntrPr() {
        throw new FieldNotMappedException("crz1aRatIntrPr");
    }

    @Override
    public void setCrz1aRatIntrPr(char crz1aRatIntrPr) {
        throw new FieldNotMappedException("crz1aRatIntrPr");
    }

    @Override
    public Character getCrz1aRatIntrPrObj() {
        return ((Character)getCrz1aRatIntrPr());
    }

    @Override
    public void setCrz1aRatIntrPrObj(Character crz1aRatIntrPrObj) {
        setCrz1aRatIntrPr(((char)crz1aRatIntrPrObj));
    }

    @Override
    public String getDescCompVchar() {
        throw new FieldNotMappedException("descCompVchar");
    }

    @Override
    public void setDescCompVchar(String descCompVchar) {
        throw new FieldNotMappedException("descCompVchar");
    }

    @Override
    public String getDescCompVcharObj() {
        return getDescCompVchar();
    }

    @Override
    public void setDescCompVcharObj(String descCompVcharObj) {
        setDescCompVchar(descCompVcharObj);
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtContDb() {
        return paramCompDb.getDtContDb();
    }

    @Override
    public void setDtContDb(String dtContDb) {
        this.paramCompDb.setDtContDb(dtContDb);
    }

    @Override
    public String getDtContDbObj() {
        if (indParamComp.getDtCont() >= 0) {
            return getDtContDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtContDbObj(String dtContDbObj) {
        if (dtContDbObj != null) {
            setDtContDb(dtContDbObj);
            indParamComp.setDtCont(((short)0));
        }
        else {
            indParamComp.setDtCont(((short)-1));
        }
    }

    @Override
    public String getDtEstrAssMag70aDb() {
        throw new FieldNotMappedException("dtEstrAssMag70aDb");
    }

    @Override
    public void setDtEstrAssMag70aDb(String dtEstrAssMag70aDb) {
        throw new FieldNotMappedException("dtEstrAssMag70aDb");
    }

    @Override
    public String getDtEstrAssMag70aDbObj() {
        return getDtEstrAssMag70aDb();
    }

    @Override
    public void setDtEstrAssMag70aDbObj(String dtEstrAssMag70aDbObj) {
        setDtEstrAssMag70aDb(dtEstrAssMag70aDbObj);
    }

    @Override
    public String getDtEstrAssMin70aDb() {
        throw new FieldNotMappedException("dtEstrAssMin70aDb");
    }

    @Override
    public void setDtEstrAssMin70aDb(String dtEstrAssMin70aDb) {
        throw new FieldNotMappedException("dtEstrAssMin70aDb");
    }

    @Override
    public String getDtEstrAssMin70aDbObj() {
        return getDtEstrAssMin70aDb();
    }

    @Override
    public void setDtEstrAssMin70aDbObj(String dtEstrAssMin70aDbObj) {
        setDtEstrAssMin70aDb(dtEstrAssMin70aDbObj);
    }

    @Override
    public String getDtRiatRiassCommDb() {
        throw new FieldNotMappedException("dtRiatRiassCommDb");
    }

    @Override
    public void setDtRiatRiassCommDb(String dtRiatRiassCommDb) {
        throw new FieldNotMappedException("dtRiatRiassCommDb");
    }

    @Override
    public String getDtRiatRiassCommDbObj() {
        return getDtRiatRiassCommDb();
    }

    @Override
    public void setDtRiatRiassCommDbObj(String dtRiatRiassCommDbObj) {
        setDtRiatRiassCommDb(dtRiatRiassCommDbObj);
    }

    @Override
    public String getDtRiatRiassRshDb() {
        throw new FieldNotMappedException("dtRiatRiassRshDb");
    }

    @Override
    public void setDtRiatRiassRshDb(String dtRiatRiassRshDb) {
        throw new FieldNotMappedException("dtRiatRiassRshDb");
    }

    @Override
    public String getDtRiatRiassRshDbObj() {
        return getDtRiatRiassRshDb();
    }

    @Override
    public void setDtRiatRiassRshDbObj(String dtRiatRiassRshDbObj) {
        setDtRiatRiassRshDb(dtRiatRiassRshDbObj);
    }

    @Override
    public String getDtRiclRiriasComDb() {
        throw new FieldNotMappedException("dtRiclRiriasComDb");
    }

    @Override
    public void setDtRiclRiriasComDb(String dtRiclRiriasComDb) {
        throw new FieldNotMappedException("dtRiclRiriasComDb");
    }

    @Override
    public String getDtRiclRiriasComDbObj() {
        return getDtRiclRiriasComDb();
    }

    @Override
    public void setDtRiclRiriasComDbObj(String dtRiclRiriasComDbObj) {
        setDtRiclRiriasComDb(dtRiclRiriasComDbObj);
    }

    @Override
    public String getDtUltAggErogReDb() {
        throw new FieldNotMappedException("dtUltAggErogReDb");
    }

    @Override
    public void setDtUltAggErogReDb(String dtUltAggErogReDb) {
        throw new FieldNotMappedException("dtUltAggErogReDb");
    }

    @Override
    public String getDtUltAggErogReDbObj() {
        return getDtUltAggErogReDb();
    }

    @Override
    public void setDtUltAggErogReDbObj(String dtUltAggErogReDbObj) {
        setDtUltAggErogReDb(dtUltAggErogReDbObj);
    }

    @Override
    public String getDtUltBollCoriCDb() {
        throw new FieldNotMappedException("dtUltBollCoriCDb");
    }

    @Override
    public void setDtUltBollCoriCDb(String dtUltBollCoriCDb) {
        throw new FieldNotMappedException("dtUltBollCoriCDb");
    }

    @Override
    public String getDtUltBollCoriCDbObj() {
        return getDtUltBollCoriCDb();
    }

    @Override
    public void setDtUltBollCoriCDbObj(String dtUltBollCoriCDbObj) {
        setDtUltBollCoriCDb(dtUltBollCoriCDbObj);
    }

    @Override
    public String getDtUltBollCoriIDb() {
        throw new FieldNotMappedException("dtUltBollCoriIDb");
    }

    @Override
    public void setDtUltBollCoriIDb(String dtUltBollCoriIDb) {
        throw new FieldNotMappedException("dtUltBollCoriIDb");
    }

    @Override
    public String getDtUltBollCoriIDbObj() {
        return getDtUltBollCoriIDb();
    }

    @Override
    public void setDtUltBollCoriIDbObj(String dtUltBollCoriIDbObj) {
        setDtUltBollCoriIDb(dtUltBollCoriIDbObj);
    }

    @Override
    public String getDtUltBollCotrCDb() {
        throw new FieldNotMappedException("dtUltBollCotrCDb");
    }

    @Override
    public void setDtUltBollCotrCDb(String dtUltBollCotrCDb) {
        throw new FieldNotMappedException("dtUltBollCotrCDb");
    }

    @Override
    public String getDtUltBollCotrCDbObj() {
        return getDtUltBollCotrCDb();
    }

    @Override
    public void setDtUltBollCotrCDbObj(String dtUltBollCotrCDbObj) {
        setDtUltBollCotrCDb(dtUltBollCotrCDbObj);
    }

    @Override
    public String getDtUltBollCotrIDb() {
        throw new FieldNotMappedException("dtUltBollCotrIDb");
    }

    @Override
    public void setDtUltBollCotrIDb(String dtUltBollCotrIDb) {
        throw new FieldNotMappedException("dtUltBollCotrIDb");
    }

    @Override
    public String getDtUltBollCotrIDbObj() {
        return getDtUltBollCotrIDb();
    }

    @Override
    public void setDtUltBollCotrIDbObj(String dtUltBollCotrIDbObj) {
        setDtUltBollCotrIDb(dtUltBollCotrIDbObj);
    }

    @Override
    public String getDtUltBollEmesDb() {
        throw new FieldNotMappedException("dtUltBollEmesDb");
    }

    @Override
    public void setDtUltBollEmesDb(String dtUltBollEmesDb) {
        throw new FieldNotMappedException("dtUltBollEmesDb");
    }

    @Override
    public String getDtUltBollEmesDbObj() {
        return getDtUltBollEmesDb();
    }

    @Override
    public void setDtUltBollEmesDbObj(String dtUltBollEmesDbObj) {
        setDtUltBollEmesDb(dtUltBollEmesDbObj);
    }

    @Override
    public String getDtUltBollEmesIDb() {
        throw new FieldNotMappedException("dtUltBollEmesIDb");
    }

    @Override
    public void setDtUltBollEmesIDb(String dtUltBollEmesIDb) {
        throw new FieldNotMappedException("dtUltBollEmesIDb");
    }

    @Override
    public String getDtUltBollEmesIDbObj() {
        return getDtUltBollEmesIDb();
    }

    @Override
    public void setDtUltBollEmesIDbObj(String dtUltBollEmesIDbObj) {
        setDtUltBollEmesIDb(dtUltBollEmesIDbObj);
    }

    @Override
    public String getDtUltBollLiqDb() {
        throw new FieldNotMappedException("dtUltBollLiqDb");
    }

    @Override
    public void setDtUltBollLiqDb(String dtUltBollLiqDb) {
        throw new FieldNotMappedException("dtUltBollLiqDb");
    }

    @Override
    public String getDtUltBollLiqDbObj() {
        return getDtUltBollLiqDb();
    }

    @Override
    public void setDtUltBollLiqDbObj(String dtUltBollLiqDbObj) {
        setDtUltBollLiqDb(dtUltBollLiqDbObj);
    }

    @Override
    public String getDtUltBollPerfCDb() {
        throw new FieldNotMappedException("dtUltBollPerfCDb");
    }

    @Override
    public void setDtUltBollPerfCDb(String dtUltBollPerfCDb) {
        throw new FieldNotMappedException("dtUltBollPerfCDb");
    }

    @Override
    public String getDtUltBollPerfCDbObj() {
        return getDtUltBollPerfCDb();
    }

    @Override
    public void setDtUltBollPerfCDbObj(String dtUltBollPerfCDbObj) {
        setDtUltBollPerfCDb(dtUltBollPerfCDbObj);
    }

    @Override
    public String getDtUltBollPerfIDb() {
        throw new FieldNotMappedException("dtUltBollPerfIDb");
    }

    @Override
    public void setDtUltBollPerfIDb(String dtUltBollPerfIDb) {
        throw new FieldNotMappedException("dtUltBollPerfIDb");
    }

    @Override
    public String getDtUltBollPerfIDbObj() {
        return getDtUltBollPerfIDb();
    }

    @Override
    public void setDtUltBollPerfIDbObj(String dtUltBollPerfIDbObj) {
        setDtUltBollPerfIDb(dtUltBollPerfIDbObj);
    }

    @Override
    public String getDtUltBollPreCDb() {
        throw new FieldNotMappedException("dtUltBollPreCDb");
    }

    @Override
    public void setDtUltBollPreCDb(String dtUltBollPreCDb) {
        throw new FieldNotMappedException("dtUltBollPreCDb");
    }

    @Override
    public String getDtUltBollPreCDbObj() {
        return getDtUltBollPreCDb();
    }

    @Override
    public void setDtUltBollPreCDbObj(String dtUltBollPreCDbObj) {
        setDtUltBollPreCDb(dtUltBollPreCDbObj);
    }

    @Override
    public String getDtUltBollPreIDb() {
        throw new FieldNotMappedException("dtUltBollPreIDb");
    }

    @Override
    public void setDtUltBollPreIDb(String dtUltBollPreIDb) {
        throw new FieldNotMappedException("dtUltBollPreIDb");
    }

    @Override
    public String getDtUltBollPreIDbObj() {
        return getDtUltBollPreIDb();
    }

    @Override
    public void setDtUltBollPreIDbObj(String dtUltBollPreIDbObj) {
        setDtUltBollPreIDb(dtUltBollPreIDbObj);
    }

    @Override
    public String getDtUltBollQuieCDb() {
        throw new FieldNotMappedException("dtUltBollQuieCDb");
    }

    @Override
    public void setDtUltBollQuieCDb(String dtUltBollQuieCDb) {
        throw new FieldNotMappedException("dtUltBollQuieCDb");
    }

    @Override
    public String getDtUltBollQuieCDbObj() {
        return getDtUltBollQuieCDb();
    }

    @Override
    public void setDtUltBollQuieCDbObj(String dtUltBollQuieCDbObj) {
        setDtUltBollQuieCDb(dtUltBollQuieCDbObj);
    }

    @Override
    public String getDtUltBollQuieIDb() {
        throw new FieldNotMappedException("dtUltBollQuieIDb");
    }

    @Override
    public void setDtUltBollQuieIDb(String dtUltBollQuieIDb) {
        throw new FieldNotMappedException("dtUltBollQuieIDb");
    }

    @Override
    public String getDtUltBollQuieIDbObj() {
        return getDtUltBollQuieIDb();
    }

    @Override
    public void setDtUltBollQuieIDbObj(String dtUltBollQuieIDbObj) {
        setDtUltBollQuieIDb(dtUltBollQuieIDbObj);
    }

    @Override
    public String getDtUltBollRiatDb() {
        throw new FieldNotMappedException("dtUltBollRiatDb");
    }

    @Override
    public void setDtUltBollRiatDb(String dtUltBollRiatDb) {
        throw new FieldNotMappedException("dtUltBollRiatDb");
    }

    @Override
    public String getDtUltBollRiatDbObj() {
        return getDtUltBollRiatDb();
    }

    @Override
    public void setDtUltBollRiatDbObj(String dtUltBollRiatDbObj) {
        setDtUltBollRiatDb(dtUltBollRiatDbObj);
    }

    @Override
    public String getDtUltBollRiatIDb() {
        throw new FieldNotMappedException("dtUltBollRiatIDb");
    }

    @Override
    public void setDtUltBollRiatIDb(String dtUltBollRiatIDb) {
        throw new FieldNotMappedException("dtUltBollRiatIDb");
    }

    @Override
    public String getDtUltBollRiatIDbObj() {
        return getDtUltBollRiatIDb();
    }

    @Override
    public void setDtUltBollRiatIDbObj(String dtUltBollRiatIDbObj) {
        setDtUltBollRiatIDb(dtUltBollRiatIDbObj);
    }

    @Override
    public String getDtUltBollRpClDb() {
        throw new FieldNotMappedException("dtUltBollRpClDb");
    }

    @Override
    public void setDtUltBollRpClDb(String dtUltBollRpClDb) {
        throw new FieldNotMappedException("dtUltBollRpClDb");
    }

    @Override
    public String getDtUltBollRpClDbObj() {
        return getDtUltBollRpClDb();
    }

    @Override
    public void setDtUltBollRpClDbObj(String dtUltBollRpClDbObj) {
        setDtUltBollRpClDb(dtUltBollRpClDbObj);
    }

    @Override
    public String getDtUltBollRpInDb() {
        throw new FieldNotMappedException("dtUltBollRpInDb");
    }

    @Override
    public void setDtUltBollRpInDb(String dtUltBollRpInDb) {
        throw new FieldNotMappedException("dtUltBollRpInDb");
    }

    @Override
    public String getDtUltBollRpInDbObj() {
        return getDtUltBollRpInDb();
    }

    @Override
    public void setDtUltBollRpInDbObj(String dtUltBollRpInDbObj) {
        setDtUltBollRpInDb(dtUltBollRpInDbObj);
    }

    @Override
    public String getDtUltBollRspClDb() {
        throw new FieldNotMappedException("dtUltBollRspClDb");
    }

    @Override
    public void setDtUltBollRspClDb(String dtUltBollRspClDb) {
        throw new FieldNotMappedException("dtUltBollRspClDb");
    }

    @Override
    public String getDtUltBollRspClDbObj() {
        return getDtUltBollRspClDb();
    }

    @Override
    public void setDtUltBollRspClDbObj(String dtUltBollRspClDbObj) {
        setDtUltBollRspClDb(dtUltBollRspClDbObj);
    }

    @Override
    public String getDtUltBollRspInDb() {
        throw new FieldNotMappedException("dtUltBollRspInDb");
    }

    @Override
    public void setDtUltBollRspInDb(String dtUltBollRspInDb) {
        throw new FieldNotMappedException("dtUltBollRspInDb");
    }

    @Override
    public String getDtUltBollRspInDbObj() {
        return getDtUltBollRspInDb();
    }

    @Override
    public void setDtUltBollRspInDbObj(String dtUltBollRspInDbObj) {
        setDtUltBollRspInDb(dtUltBollRspInDbObj);
    }

    @Override
    public String getDtUltBollSdIDb() {
        throw new FieldNotMappedException("dtUltBollSdIDb");
    }

    @Override
    public void setDtUltBollSdIDb(String dtUltBollSdIDb) {
        throw new FieldNotMappedException("dtUltBollSdIDb");
    }

    @Override
    public String getDtUltBollSdIDbObj() {
        return getDtUltBollSdIDb();
    }

    @Override
    public void setDtUltBollSdIDbObj(String dtUltBollSdIDbObj) {
        setDtUltBollSdIDb(dtUltBollSdIDbObj);
    }

    @Override
    public String getDtUltBollSdnlIDb() {
        throw new FieldNotMappedException("dtUltBollSdnlIDb");
    }

    @Override
    public void setDtUltBollSdnlIDb(String dtUltBollSdnlIDb) {
        throw new FieldNotMappedException("dtUltBollSdnlIDb");
    }

    @Override
    public String getDtUltBollSdnlIDbObj() {
        return getDtUltBollSdnlIDb();
    }

    @Override
    public void setDtUltBollSdnlIDbObj(String dtUltBollSdnlIDbObj) {
        setDtUltBollSdnlIDb(dtUltBollSdnlIDbObj);
    }

    @Override
    public String getDtUltBollSndenDb() {
        throw new FieldNotMappedException("dtUltBollSndenDb");
    }

    @Override
    public void setDtUltBollSndenDb(String dtUltBollSndenDb) {
        throw new FieldNotMappedException("dtUltBollSndenDb");
    }

    @Override
    public String getDtUltBollSndenDbObj() {
        return getDtUltBollSndenDb();
    }

    @Override
    public void setDtUltBollSndenDbObj(String dtUltBollSndenDbObj) {
        setDtUltBollSndenDb(dtUltBollSndenDbObj);
    }

    @Override
    public String getDtUltBollSndnlqDb() {
        throw new FieldNotMappedException("dtUltBollSndnlqDb");
    }

    @Override
    public void setDtUltBollSndnlqDb(String dtUltBollSndnlqDb) {
        throw new FieldNotMappedException("dtUltBollSndnlqDb");
    }

    @Override
    public String getDtUltBollSndnlqDbObj() {
        return getDtUltBollSndnlqDb();
    }

    @Override
    public void setDtUltBollSndnlqDbObj(String dtUltBollSndnlqDbObj) {
        setDtUltBollSndnlqDb(dtUltBollSndnlqDbObj);
    }

    @Override
    public String getDtUltBollStorDb() {
        throw new FieldNotMappedException("dtUltBollStorDb");
    }

    @Override
    public void setDtUltBollStorDb(String dtUltBollStorDb) {
        throw new FieldNotMappedException("dtUltBollStorDb");
    }

    @Override
    public String getDtUltBollStorDbObj() {
        return getDtUltBollStorDb();
    }

    @Override
    public void setDtUltBollStorDbObj(String dtUltBollStorDbObj) {
        setDtUltBollStorDb(dtUltBollStorDbObj);
    }

    @Override
    public String getDtUltBollStorIDb() {
        throw new FieldNotMappedException("dtUltBollStorIDb");
    }

    @Override
    public void setDtUltBollStorIDb(String dtUltBollStorIDb) {
        throw new FieldNotMappedException("dtUltBollStorIDb");
    }

    @Override
    public String getDtUltBollStorIDbObj() {
        return getDtUltBollStorIDb();
    }

    @Override
    public void setDtUltBollStorIDbObj(String dtUltBollStorIDbObj) {
        setDtUltBollStorIDb(dtUltBollStorIDbObj);
    }

    @Override
    public String getDtUltEcIlCollDb() {
        throw new FieldNotMappedException("dtUltEcIlCollDb");
    }

    @Override
    public void setDtUltEcIlCollDb(String dtUltEcIlCollDb) {
        throw new FieldNotMappedException("dtUltEcIlCollDb");
    }

    @Override
    public String getDtUltEcIlCollDbObj() {
        return getDtUltEcIlCollDb();
    }

    @Override
    public void setDtUltEcIlCollDbObj(String dtUltEcIlCollDbObj) {
        setDtUltEcIlCollDb(dtUltEcIlCollDbObj);
    }

    @Override
    public String getDtUltEcIlIndDb() {
        throw new FieldNotMappedException("dtUltEcIlIndDb");
    }

    @Override
    public void setDtUltEcIlIndDb(String dtUltEcIlIndDb) {
        throw new FieldNotMappedException("dtUltEcIlIndDb");
    }

    @Override
    public String getDtUltEcIlIndDbObj() {
        return getDtUltEcIlIndDb();
    }

    @Override
    public void setDtUltEcIlIndDbObj(String dtUltEcIlIndDbObj) {
        setDtUltEcIlIndDb(dtUltEcIlIndDbObj);
    }

    @Override
    public String getDtUltEcMrmCollDb() {
        throw new FieldNotMappedException("dtUltEcMrmCollDb");
    }

    @Override
    public void setDtUltEcMrmCollDb(String dtUltEcMrmCollDb) {
        throw new FieldNotMappedException("dtUltEcMrmCollDb");
    }

    @Override
    public String getDtUltEcMrmCollDbObj() {
        return getDtUltEcMrmCollDb();
    }

    @Override
    public void setDtUltEcMrmCollDbObj(String dtUltEcMrmCollDbObj) {
        setDtUltEcMrmCollDb(dtUltEcMrmCollDbObj);
    }

    @Override
    public String getDtUltEcMrmIndDb() {
        throw new FieldNotMappedException("dtUltEcMrmIndDb");
    }

    @Override
    public void setDtUltEcMrmIndDb(String dtUltEcMrmIndDb) {
        throw new FieldNotMappedException("dtUltEcMrmIndDb");
    }

    @Override
    public String getDtUltEcMrmIndDbObj() {
        return getDtUltEcMrmIndDb();
    }

    @Override
    public void setDtUltEcMrmIndDbObj(String dtUltEcMrmIndDbObj) {
        setDtUltEcMrmIndDb(dtUltEcMrmIndDbObj);
    }

    @Override
    public String getDtUltEcRivCollDb() {
        throw new FieldNotMappedException("dtUltEcRivCollDb");
    }

    @Override
    public void setDtUltEcRivCollDb(String dtUltEcRivCollDb) {
        throw new FieldNotMappedException("dtUltEcRivCollDb");
    }

    @Override
    public String getDtUltEcRivCollDbObj() {
        return getDtUltEcRivCollDb();
    }

    @Override
    public void setDtUltEcRivCollDbObj(String dtUltEcRivCollDbObj) {
        setDtUltEcRivCollDb(dtUltEcRivCollDbObj);
    }

    @Override
    public String getDtUltEcRivIndDb() {
        throw new FieldNotMappedException("dtUltEcRivIndDb");
    }

    @Override
    public void setDtUltEcRivIndDb(String dtUltEcRivIndDb) {
        throw new FieldNotMappedException("dtUltEcRivIndDb");
    }

    @Override
    public String getDtUltEcRivIndDbObj() {
        return getDtUltEcRivIndDb();
    }

    @Override
    public void setDtUltEcRivIndDbObj(String dtUltEcRivIndDbObj) {
        setDtUltEcRivIndDb(dtUltEcRivIndDbObj);
    }

    @Override
    public String getDtUltEcTcmCollDb() {
        throw new FieldNotMappedException("dtUltEcTcmCollDb");
    }

    @Override
    public void setDtUltEcTcmCollDb(String dtUltEcTcmCollDb) {
        throw new FieldNotMappedException("dtUltEcTcmCollDb");
    }

    @Override
    public String getDtUltEcTcmCollDbObj() {
        return getDtUltEcTcmCollDb();
    }

    @Override
    public void setDtUltEcTcmCollDbObj(String dtUltEcTcmCollDbObj) {
        setDtUltEcTcmCollDb(dtUltEcTcmCollDbObj);
    }

    @Override
    public String getDtUltEcTcmIndDb() {
        throw new FieldNotMappedException("dtUltEcTcmIndDb");
    }

    @Override
    public void setDtUltEcTcmIndDb(String dtUltEcTcmIndDb) {
        throw new FieldNotMappedException("dtUltEcTcmIndDb");
    }

    @Override
    public String getDtUltEcTcmIndDbObj() {
        return getDtUltEcTcmIndDb();
    }

    @Override
    public void setDtUltEcTcmIndDbObj(String dtUltEcTcmIndDbObj) {
        setDtUltEcTcmIndDb(dtUltEcTcmIndDbObj);
    }

    @Override
    public String getDtUltEcUlCollDb() {
        throw new FieldNotMappedException("dtUltEcUlCollDb");
    }

    @Override
    public void setDtUltEcUlCollDb(String dtUltEcUlCollDb) {
        throw new FieldNotMappedException("dtUltEcUlCollDb");
    }

    @Override
    public String getDtUltEcUlCollDbObj() {
        return getDtUltEcUlCollDb();
    }

    @Override
    public void setDtUltEcUlCollDbObj(String dtUltEcUlCollDbObj) {
        setDtUltEcUlCollDb(dtUltEcUlCollDbObj);
    }

    @Override
    public String getDtUltEcUlIndDb() {
        throw new FieldNotMappedException("dtUltEcUlIndDb");
    }

    @Override
    public void setDtUltEcUlIndDb(String dtUltEcUlIndDb) {
        throw new FieldNotMappedException("dtUltEcUlIndDb");
    }

    @Override
    public String getDtUltEcUlIndDbObj() {
        return getDtUltEcUlIndDb();
    }

    @Override
    public void setDtUltEcUlIndDbObj(String dtUltEcUlIndDbObj) {
        setDtUltEcUlIndDb(dtUltEcUlIndDbObj);
    }

    @Override
    public String getDtUltElabAt92CDb() {
        throw new FieldNotMappedException("dtUltElabAt92CDb");
    }

    @Override
    public void setDtUltElabAt92CDb(String dtUltElabAt92CDb) {
        throw new FieldNotMappedException("dtUltElabAt92CDb");
    }

    @Override
    public String getDtUltElabAt92CDbObj() {
        return getDtUltElabAt92CDb();
    }

    @Override
    public void setDtUltElabAt92CDbObj(String dtUltElabAt92CDbObj) {
        setDtUltElabAt92CDb(dtUltElabAt92CDbObj);
    }

    @Override
    public String getDtUltElabAt92IDb() {
        throw new FieldNotMappedException("dtUltElabAt92IDb");
    }

    @Override
    public void setDtUltElabAt92IDb(String dtUltElabAt92IDb) {
        throw new FieldNotMappedException("dtUltElabAt92IDb");
    }

    @Override
    public String getDtUltElabAt92IDbObj() {
        return getDtUltElabAt92IDb();
    }

    @Override
    public void setDtUltElabAt92IDbObj(String dtUltElabAt92IDbObj) {
        setDtUltElabAt92IDb(dtUltElabAt92IDbObj);
    }

    @Override
    public String getDtUltElabAt93CDb() {
        throw new FieldNotMappedException("dtUltElabAt93CDb");
    }

    @Override
    public void setDtUltElabAt93CDb(String dtUltElabAt93CDb) {
        throw new FieldNotMappedException("dtUltElabAt93CDb");
    }

    @Override
    public String getDtUltElabAt93CDbObj() {
        return getDtUltElabAt93CDb();
    }

    @Override
    public void setDtUltElabAt93CDbObj(String dtUltElabAt93CDbObj) {
        setDtUltElabAt93CDb(dtUltElabAt93CDbObj);
    }

    @Override
    public String getDtUltElabAt93IDb() {
        throw new FieldNotMappedException("dtUltElabAt93IDb");
    }

    @Override
    public void setDtUltElabAt93IDb(String dtUltElabAt93IDb) {
        throw new FieldNotMappedException("dtUltElabAt93IDb");
    }

    @Override
    public String getDtUltElabAt93IDbObj() {
        return getDtUltElabAt93IDb();
    }

    @Override
    public void setDtUltElabAt93IDbObj(String dtUltElabAt93IDbObj) {
        setDtUltElabAt93IDb(dtUltElabAt93IDbObj);
    }

    @Override
    public String getDtUltElabCommefDb() {
        throw new FieldNotMappedException("dtUltElabCommefDb");
    }

    @Override
    public void setDtUltElabCommefDb(String dtUltElabCommefDb) {
        throw new FieldNotMappedException("dtUltElabCommefDb");
    }

    @Override
    public String getDtUltElabCommefDbObj() {
        return getDtUltElabCommefDb();
    }

    @Override
    public void setDtUltElabCommefDbObj(String dtUltElabCommefDbObj) {
        setDtUltElabCommefDb(dtUltElabCommefDbObj);
    }

    @Override
    public String getDtUltElabCosAtDb() {
        throw new FieldNotMappedException("dtUltElabCosAtDb");
    }

    @Override
    public void setDtUltElabCosAtDb(String dtUltElabCosAtDb) {
        throw new FieldNotMappedException("dtUltElabCosAtDb");
    }

    @Override
    public String getDtUltElabCosAtDbObj() {
        return getDtUltElabCosAtDb();
    }

    @Override
    public void setDtUltElabCosAtDbObj(String dtUltElabCosAtDbObj) {
        setDtUltElabCosAtDb(dtUltElabCosAtDbObj);
    }

    @Override
    public String getDtUltElabCosStDb() {
        throw new FieldNotMappedException("dtUltElabCosStDb");
    }

    @Override
    public void setDtUltElabCosStDb(String dtUltElabCosStDb) {
        throw new FieldNotMappedException("dtUltElabCosStDb");
    }

    @Override
    public String getDtUltElabCosStDbObj() {
        return getDtUltElabCosStDb();
    }

    @Override
    public void setDtUltElabCosStDbObj(String dtUltElabCosStDbObj) {
        setDtUltElabCosStDb(dtUltElabCosStDbObj);
    }

    @Override
    public String getDtUltElabLiqmefDb() {
        throw new FieldNotMappedException("dtUltElabLiqmefDb");
    }

    @Override
    public void setDtUltElabLiqmefDb(String dtUltElabLiqmefDb) {
        throw new FieldNotMappedException("dtUltElabLiqmefDb");
    }

    @Override
    public String getDtUltElabLiqmefDbObj() {
        return getDtUltElabLiqmefDb();
    }

    @Override
    public void setDtUltElabLiqmefDbObj(String dtUltElabLiqmefDbObj) {
        setDtUltElabLiqmefDb(dtUltElabLiqmefDbObj);
    }

    @Override
    public String getDtUltElabPaspasDb() {
        throw new FieldNotMappedException("dtUltElabPaspasDb");
    }

    @Override
    public void setDtUltElabPaspasDb(String dtUltElabPaspasDb) {
        throw new FieldNotMappedException("dtUltElabPaspasDb");
    }

    @Override
    public String getDtUltElabPaspasDbObj() {
        return getDtUltElabPaspasDb();
    }

    @Override
    public void setDtUltElabPaspasDbObj(String dtUltElabPaspasDbObj) {
        setDtUltElabPaspasDb(dtUltElabPaspasDbObj);
    }

    @Override
    public String getDtUltElabPrAutDb() {
        throw new FieldNotMappedException("dtUltElabPrAutDb");
    }

    @Override
    public void setDtUltElabPrAutDb(String dtUltElabPrAutDb) {
        throw new FieldNotMappedException("dtUltElabPrAutDb");
    }

    @Override
    public String getDtUltElabPrAutDbObj() {
        return getDtUltElabPrAutDb();
    }

    @Override
    public void setDtUltElabPrAutDbObj(String dtUltElabPrAutDbObj) {
        setDtUltElabPrAutDb(dtUltElabPrAutDbObj);
    }

    @Override
    public String getDtUltElabPrConDb() {
        throw new FieldNotMappedException("dtUltElabPrConDb");
    }

    @Override
    public void setDtUltElabPrConDb(String dtUltElabPrConDb) {
        throw new FieldNotMappedException("dtUltElabPrConDb");
    }

    @Override
    public String getDtUltElabPrConDbObj() {
        return getDtUltElabPrConDb();
    }

    @Override
    public void setDtUltElabPrConDbObj(String dtUltElabPrConDbObj) {
        setDtUltElabPrConDb(dtUltElabPrConDbObj);
    }

    @Override
    public String getDtUltElabPrlcosDb() {
        throw new FieldNotMappedException("dtUltElabPrlcosDb");
    }

    @Override
    public void setDtUltElabPrlcosDb(String dtUltElabPrlcosDb) {
        throw new FieldNotMappedException("dtUltElabPrlcosDb");
    }

    @Override
    public String getDtUltElabPrlcosDbObj() {
        return getDtUltElabPrlcosDb();
    }

    @Override
    public void setDtUltElabPrlcosDbObj(String dtUltElabPrlcosDbObj) {
        setDtUltElabPrlcosDb(dtUltElabPrlcosDbObj);
    }

    @Override
    public String getDtUltElabRedproDb() {
        throw new FieldNotMappedException("dtUltElabRedproDb");
    }

    @Override
    public void setDtUltElabRedproDb(String dtUltElabRedproDb) {
        throw new FieldNotMappedException("dtUltElabRedproDb");
    }

    @Override
    public String getDtUltElabRedproDbObj() {
        return getDtUltElabRedproDb();
    }

    @Override
    public void setDtUltElabRedproDbObj(String dtUltElabRedproDbObj) {
        setDtUltElabRedproDb(dtUltElabRedproDbObj);
    }

    @Override
    public String getDtUltElabSpeInDb() {
        throw new FieldNotMappedException("dtUltElabSpeInDb");
    }

    @Override
    public void setDtUltElabSpeInDb(String dtUltElabSpeInDb) {
        throw new FieldNotMappedException("dtUltElabSpeInDb");
    }

    @Override
    public String getDtUltElabSpeInDbObj() {
        return getDtUltElabSpeInDb();
    }

    @Override
    public void setDtUltElabSpeInDbObj(String dtUltElabSpeInDbObj) {
        setDtUltElabSpeInDb(dtUltElabSpeInDbObj);
    }

    @Override
    public String getDtUltElabTakePDb() {
        throw new FieldNotMappedException("dtUltElabTakePDb");
    }

    @Override
    public void setDtUltElabTakePDb(String dtUltElabTakePDb) {
        throw new FieldNotMappedException("dtUltElabTakePDb");
    }

    @Override
    public String getDtUltElabTakePDbObj() {
        return getDtUltElabTakePDb();
    }

    @Override
    public void setDtUltElabTakePDbObj(String dtUltElabTakePDbObj) {
        setDtUltElabTakePDb(dtUltElabTakePDbObj);
    }

    @Override
    public String getDtUltEstrDecCoDb() {
        throw new FieldNotMappedException("dtUltEstrDecCoDb");
    }

    @Override
    public void setDtUltEstrDecCoDb(String dtUltEstrDecCoDb) {
        throw new FieldNotMappedException("dtUltEstrDecCoDb");
    }

    @Override
    public String getDtUltEstrDecCoDbObj() {
        return getDtUltEstrDecCoDb();
    }

    @Override
    public void setDtUltEstrDecCoDbObj(String dtUltEstrDecCoDbObj) {
        setDtUltEstrDecCoDb(dtUltEstrDecCoDbObj);
    }

    @Override
    public String getDtUltEstrazFugDb() {
        throw new FieldNotMappedException("dtUltEstrazFugDb");
    }

    @Override
    public void setDtUltEstrazFugDb(String dtUltEstrazFugDb) {
        throw new FieldNotMappedException("dtUltEstrazFugDb");
    }

    @Override
    public String getDtUltEstrazFugDbObj() {
        return getDtUltEstrazFugDb();
    }

    @Override
    public void setDtUltEstrazFugDbObj(String dtUltEstrazFugDbObj) {
        setDtUltEstrazFugDb(dtUltEstrazFugDbObj);
    }

    @Override
    public String getDtUltQtzoClDb() {
        throw new FieldNotMappedException("dtUltQtzoClDb");
    }

    @Override
    public void setDtUltQtzoClDb(String dtUltQtzoClDb) {
        throw new FieldNotMappedException("dtUltQtzoClDb");
    }

    @Override
    public String getDtUltQtzoClDbObj() {
        return getDtUltQtzoClDb();
    }

    @Override
    public void setDtUltQtzoClDbObj(String dtUltQtzoClDbObj) {
        setDtUltQtzoClDb(dtUltQtzoClDbObj);
    }

    @Override
    public String getDtUltQtzoInDb() {
        throw new FieldNotMappedException("dtUltQtzoInDb");
    }

    @Override
    public void setDtUltQtzoInDb(String dtUltQtzoInDb) {
        throw new FieldNotMappedException("dtUltQtzoInDb");
    }

    @Override
    public String getDtUltQtzoInDbObj() {
        return getDtUltQtzoInDb();
    }

    @Override
    public void setDtUltQtzoInDbObj(String dtUltQtzoInDbObj) {
        setDtUltQtzoInDb(dtUltQtzoInDbObj);
    }

    @Override
    public String getDtUltRiclPreDb() {
        throw new FieldNotMappedException("dtUltRiclPreDb");
    }

    @Override
    public void setDtUltRiclPreDb(String dtUltRiclPreDb) {
        throw new FieldNotMappedException("dtUltRiclPreDb");
    }

    @Override
    public String getDtUltRiclPreDbObj() {
        return getDtUltRiclPreDb();
    }

    @Override
    public void setDtUltRiclPreDbObj(String dtUltRiclPreDbObj) {
        setDtUltRiclPreDb(dtUltRiclPreDbObj);
    }

    @Override
    public String getDtUltRiclRiassDb() {
        throw new FieldNotMappedException("dtUltRiclRiassDb");
    }

    @Override
    public void setDtUltRiclRiassDb(String dtUltRiclRiassDb) {
        throw new FieldNotMappedException("dtUltRiclRiassDb");
    }

    @Override
    public String getDtUltRiclRiassDbObj() {
        return getDtUltRiclRiassDb();
    }

    @Override
    public void setDtUltRiclRiassDbObj(String dtUltRiclRiassDbObj) {
        setDtUltRiclRiassDb(dtUltRiclRiassDbObj);
    }

    @Override
    public String getDtUltRinnCollDb() {
        throw new FieldNotMappedException("dtUltRinnCollDb");
    }

    @Override
    public void setDtUltRinnCollDb(String dtUltRinnCollDb) {
        throw new FieldNotMappedException("dtUltRinnCollDb");
    }

    @Override
    public String getDtUltRinnCollDbObj() {
        return getDtUltRinnCollDb();
    }

    @Override
    public void setDtUltRinnCollDbObj(String dtUltRinnCollDbObj) {
        setDtUltRinnCollDb(dtUltRinnCollDbObj);
    }

    @Override
    public String getDtUltRinnGaracDb() {
        throw new FieldNotMappedException("dtUltRinnGaracDb");
    }

    @Override
    public void setDtUltRinnGaracDb(String dtUltRinnGaracDb) {
        throw new FieldNotMappedException("dtUltRinnGaracDb");
    }

    @Override
    public String getDtUltRinnGaracDbObj() {
        return getDtUltRinnGaracDb();
    }

    @Override
    public void setDtUltRinnGaracDbObj(String dtUltRinnGaracDbObj) {
        setDtUltRinnGaracDb(dtUltRinnGaracDbObj);
    }

    @Override
    public String getDtUltRinnTacDb() {
        throw new FieldNotMappedException("dtUltRinnTacDb");
    }

    @Override
    public void setDtUltRinnTacDb(String dtUltRinnTacDb) {
        throw new FieldNotMappedException("dtUltRinnTacDb");
    }

    @Override
    public String getDtUltRinnTacDbObj() {
        return getDtUltRinnTacDb();
    }

    @Override
    public void setDtUltRinnTacDbObj(String dtUltRinnTacDbObj) {
        setDtUltRinnTacDb(dtUltRinnTacDbObj);
    }

    @Override
    public String getDtUltRivalClDb() {
        throw new FieldNotMappedException("dtUltRivalClDb");
    }

    @Override
    public void setDtUltRivalClDb(String dtUltRivalClDb) {
        throw new FieldNotMappedException("dtUltRivalClDb");
    }

    @Override
    public String getDtUltRivalClDbObj() {
        return getDtUltRivalClDb();
    }

    @Override
    public void setDtUltRivalClDbObj(String dtUltRivalClDbObj) {
        setDtUltRivalClDb(dtUltRivalClDbObj);
    }

    @Override
    public String getDtUltRivalInDb() {
        throw new FieldNotMappedException("dtUltRivalInDb");
    }

    @Override
    public void setDtUltRivalInDb(String dtUltRivalInDb) {
        throw new FieldNotMappedException("dtUltRivalInDb");
    }

    @Override
    public String getDtUltRivalInDbObj() {
        return getDtUltRivalInDb();
    }

    @Override
    public void setDtUltRivalInDbObj(String dtUltRivalInDbObj) {
        setDtUltRivalInDb(dtUltRivalInDbObj);
    }

    @Override
    public String getDtUltTabulRiassDb() {
        throw new FieldNotMappedException("dtUltTabulRiassDb");
    }

    @Override
    public void setDtUltTabulRiassDb(String dtUltTabulRiassDb) {
        throw new FieldNotMappedException("dtUltTabulRiassDb");
    }

    @Override
    public String getDtUltTabulRiassDbObj() {
        return getDtUltTabulRiassDb();
    }

    @Override
    public void setDtUltTabulRiassDbObj(String dtUltTabulRiassDbObj) {
        setDtUltTabulRiassDb(dtUltTabulRiassDbObj);
    }

    @Override
    public String getDtUltcBnsfdtClDb() {
        throw new FieldNotMappedException("dtUltcBnsfdtClDb");
    }

    @Override
    public void setDtUltcBnsfdtClDb(String dtUltcBnsfdtClDb) {
        throw new FieldNotMappedException("dtUltcBnsfdtClDb");
    }

    @Override
    public String getDtUltcBnsfdtClDbObj() {
        return getDtUltcBnsfdtClDb();
    }

    @Override
    public void setDtUltcBnsfdtClDbObj(String dtUltcBnsfdtClDbObj) {
        setDtUltcBnsfdtClDb(dtUltcBnsfdtClDbObj);
    }

    @Override
    public String getDtUltcBnsfdtInDb() {
        throw new FieldNotMappedException("dtUltcBnsfdtInDb");
    }

    @Override
    public void setDtUltcBnsfdtInDb(String dtUltcBnsfdtInDb) {
        throw new FieldNotMappedException("dtUltcBnsfdtInDb");
    }

    @Override
    public String getDtUltcBnsfdtInDbObj() {
        return getDtUltcBnsfdtInDb();
    }

    @Override
    public void setDtUltcBnsfdtInDbObj(String dtUltcBnsfdtInDbObj) {
        setDtUltcBnsfdtInDb(dtUltcBnsfdtInDbObj);
    }

    @Override
    public String getDtUltcBnsricClDb() {
        throw new FieldNotMappedException("dtUltcBnsricClDb");
    }

    @Override
    public void setDtUltcBnsricClDb(String dtUltcBnsricClDb) {
        throw new FieldNotMappedException("dtUltcBnsricClDb");
    }

    @Override
    public String getDtUltcBnsricClDbObj() {
        return getDtUltcBnsricClDb();
    }

    @Override
    public void setDtUltcBnsricClDbObj(String dtUltcBnsricClDbObj) {
        setDtUltcBnsricClDb(dtUltcBnsricClDbObj);
    }

    @Override
    public String getDtUltcBnsricInDb() {
        throw new FieldNotMappedException("dtUltcBnsricInDb");
    }

    @Override
    public void setDtUltcBnsricInDb(String dtUltcBnsricInDb) {
        throw new FieldNotMappedException("dtUltcBnsricInDb");
    }

    @Override
    public String getDtUltcBnsricInDbObj() {
        return getDtUltcBnsricInDb();
    }

    @Override
    public void setDtUltcBnsricInDbObj(String dtUltcBnsricInDbObj) {
        setDtUltcBnsricInDb(dtUltcBnsricInDbObj);
    }

    @Override
    public String getDtUltcIsClDb() {
        throw new FieldNotMappedException("dtUltcIsClDb");
    }

    @Override
    public void setDtUltcIsClDb(String dtUltcIsClDb) {
        throw new FieldNotMappedException("dtUltcIsClDb");
    }

    @Override
    public String getDtUltcIsClDbObj() {
        return getDtUltcIsClDb();
    }

    @Override
    public void setDtUltcIsClDbObj(String dtUltcIsClDbObj) {
        setDtUltcIsClDb(dtUltcIsClDbObj);
    }

    @Override
    public String getDtUltcIsInDb() {
        throw new FieldNotMappedException("dtUltcIsInDb");
    }

    @Override
    public void setDtUltcIsInDb(String dtUltcIsInDb) {
        throw new FieldNotMappedException("dtUltcIsInDb");
    }

    @Override
    public String getDtUltcIsInDbObj() {
        return getDtUltcIsInDb();
    }

    @Override
    public void setDtUltcIsInDbObj(String dtUltcIsInDbObj) {
        setDtUltcIsInDb(dtUltcIsInDbObj);
    }

    @Override
    public String getDtUltcMarsolDb() {
        throw new FieldNotMappedException("dtUltcMarsolDb");
    }

    @Override
    public void setDtUltcMarsolDb(String dtUltcMarsolDb) {
        throw new FieldNotMappedException("dtUltcMarsolDb");
    }

    @Override
    public String getDtUltcMarsolDbObj() {
        return getDtUltcMarsolDb();
    }

    @Override
    public void setDtUltcMarsolDbObj(String dtUltcMarsolDbObj) {
        setDtUltcMarsolDb(dtUltcMarsolDbObj);
    }

    @Override
    public String getDtUltcPildiAaCDb() {
        throw new FieldNotMappedException("dtUltcPildiAaCDb");
    }

    @Override
    public void setDtUltcPildiAaCDb(String dtUltcPildiAaCDb) {
        throw new FieldNotMappedException("dtUltcPildiAaCDb");
    }

    @Override
    public String getDtUltcPildiAaCDbObj() {
        return getDtUltcPildiAaCDb();
    }

    @Override
    public void setDtUltcPildiAaCDbObj(String dtUltcPildiAaCDbObj) {
        setDtUltcPildiAaCDb(dtUltcPildiAaCDbObj);
    }

    @Override
    public String getDtUltcPildiAaIDb() {
        throw new FieldNotMappedException("dtUltcPildiAaIDb");
    }

    @Override
    public void setDtUltcPildiAaIDb(String dtUltcPildiAaIDb) {
        throw new FieldNotMappedException("dtUltcPildiAaIDb");
    }

    @Override
    public String getDtUltcPildiAaIDbObj() {
        return getDtUltcPildiAaIDb();
    }

    @Override
    public void setDtUltcPildiAaIDbObj(String dtUltcPildiAaIDbObj) {
        setDtUltcPildiAaIDb(dtUltcPildiAaIDbObj);
    }

    @Override
    public String getDtUltcPildiMmCDb() {
        throw new FieldNotMappedException("dtUltcPildiMmCDb");
    }

    @Override
    public void setDtUltcPildiMmCDb(String dtUltcPildiMmCDb) {
        throw new FieldNotMappedException("dtUltcPildiMmCDb");
    }

    @Override
    public String getDtUltcPildiMmCDbObj() {
        return getDtUltcPildiMmCDb();
    }

    @Override
    public void setDtUltcPildiMmCDbObj(String dtUltcPildiMmCDbObj) {
        setDtUltcPildiMmCDb(dtUltcPildiMmCDbObj);
    }

    @Override
    public String getDtUltcPildiMmIDb() {
        throw new FieldNotMappedException("dtUltcPildiMmIDb");
    }

    @Override
    public void setDtUltcPildiMmIDb(String dtUltcPildiMmIDb) {
        throw new FieldNotMappedException("dtUltcPildiMmIDb");
    }

    @Override
    public String getDtUltcPildiMmIDbObj() {
        return getDtUltcPildiMmIDb();
    }

    @Override
    public void setDtUltcPildiMmIDbObj(String dtUltcPildiMmIDbObj) {
        setDtUltcPildiMmIDb(dtUltcPildiMmIDbObj);
    }

    @Override
    public String getDtUltcPildiTrIDb() {
        throw new FieldNotMappedException("dtUltcPildiTrIDb");
    }

    @Override
    public void setDtUltcPildiTrIDb(String dtUltcPildiTrIDb) {
        throw new FieldNotMappedException("dtUltcPildiTrIDb");
    }

    @Override
    public String getDtUltcPildiTrIDbObj() {
        return getDtUltcPildiTrIDb();
    }

    @Override
    public void setDtUltcPildiTrIDbObj(String dtUltcPildiTrIDbObj) {
        setDtUltcPildiTrIDb(dtUltcPildiTrIDbObj);
    }

    @Override
    public String getDtUltcRbClDb() {
        throw new FieldNotMappedException("dtUltcRbClDb");
    }

    @Override
    public void setDtUltcRbClDb(String dtUltcRbClDb) {
        throw new FieldNotMappedException("dtUltcRbClDb");
    }

    @Override
    public String getDtUltcRbClDbObj() {
        return getDtUltcRbClDb();
    }

    @Override
    public void setDtUltcRbClDbObj(String dtUltcRbClDbObj) {
        setDtUltcRbClDb(dtUltcRbClDbObj);
    }

    @Override
    public String getDtUltcRbInDb() {
        throw new FieldNotMappedException("dtUltcRbInDb");
    }

    @Override
    public void setDtUltcRbInDb(String dtUltcRbInDb) {
        throw new FieldNotMappedException("dtUltcRbInDb");
    }

    @Override
    public String getDtUltcRbInDbObj() {
        return getDtUltcRbInDb();
    }

    @Override
    public void setDtUltcRbInDbObj(String dtUltcRbInDbObj) {
        setDtUltcRbInDb(dtUltcRbInDbObj);
    }

    @Override
    public String getDtUltelriscparPrDb() {
        throw new FieldNotMappedException("dtUltelriscparPrDb");
    }

    @Override
    public void setDtUltelriscparPrDb(String dtUltelriscparPrDb) {
        throw new FieldNotMappedException("dtUltelriscparPrDb");
    }

    @Override
    public String getDtUltelriscparPrDbObj() {
        return getDtUltelriscparPrDb();
    }

    @Override
    public void setDtUltelriscparPrDbObj(String dtUltelriscparPrDbObj) {
        setDtUltelriscparPrDb(dtUltelriscparPrDbObj);
    }

    @Override
    public String getDtUltgzCedCollDb() {
        throw new FieldNotMappedException("dtUltgzCedCollDb");
    }

    @Override
    public void setDtUltgzCedCollDb(String dtUltgzCedCollDb) {
        throw new FieldNotMappedException("dtUltgzCedCollDb");
    }

    @Override
    public String getDtUltgzCedCollDbObj() {
        return getDtUltgzCedCollDb();
    }

    @Override
    public void setDtUltgzCedCollDbObj(String dtUltgzCedCollDbObj) {
        setDtUltgzCedCollDb(dtUltgzCedCollDbObj);
    }

    @Override
    public String getDtUltgzCedDb() {
        throw new FieldNotMappedException("dtUltgzCedDb");
    }

    @Override
    public void setDtUltgzCedDb(String dtUltgzCedDb) {
        throw new FieldNotMappedException("dtUltgzCedDb");
    }

    @Override
    public String getDtUltgzCedDbObj() {
        return getDtUltgzCedDb();
    }

    @Override
    public void setDtUltgzCedDbObj(String dtUltgzCedDbObj) {
        setDtUltgzCedDb(dtUltgzCedDbObj);
    }

    @Override
    public String getDtUltgzTrchEClDb() {
        throw new FieldNotMappedException("dtUltgzTrchEClDb");
    }

    @Override
    public void setDtUltgzTrchEClDb(String dtUltgzTrchEClDb) {
        throw new FieldNotMappedException("dtUltgzTrchEClDb");
    }

    @Override
    public String getDtUltgzTrchEClDbObj() {
        return getDtUltgzTrchEClDb();
    }

    @Override
    public void setDtUltgzTrchEClDbObj(String dtUltgzTrchEClDbObj) {
        setDtUltgzTrchEClDb(dtUltgzTrchEClDbObj);
    }

    @Override
    public String getDtUltgzTrchEInDb() {
        throw new FieldNotMappedException("dtUltgzTrchEInDb");
    }

    @Override
    public void setDtUltgzTrchEInDb(String dtUltgzTrchEInDb) {
        throw new FieldNotMappedException("dtUltgzTrchEInDb");
    }

    @Override
    public String getDtUltgzTrchEInDbObj() {
        return getDtUltgzTrchEInDb();
    }

    @Override
    public void setDtUltgzTrchEInDbObj(String dtUltgzTrchEInDbObj) {
        setDtUltgzTrchEInDb(dtUltgzTrchEInDbObj);
    }

    @Override
    public String getDtUltscElabClDb() {
        throw new FieldNotMappedException("dtUltscElabClDb");
    }

    @Override
    public void setDtUltscElabClDb(String dtUltscElabClDb) {
        throw new FieldNotMappedException("dtUltscElabClDb");
    }

    @Override
    public String getDtUltscElabClDbObj() {
        return getDtUltscElabClDb();
    }

    @Override
    public void setDtUltscElabClDbObj(String dtUltscElabClDbObj) {
        setDtUltscElabClDb(dtUltscElabClDbObj);
    }

    @Override
    public String getDtUltscElabInDb() {
        throw new FieldNotMappedException("dtUltscElabInDb");
    }

    @Override
    public void setDtUltscElabInDb(String dtUltscElabInDb) {
        throw new FieldNotMappedException("dtUltscElabInDb");
    }

    @Override
    public String getDtUltscElabInDbObj() {
        return getDtUltscElabInDb();
    }

    @Override
    public void setDtUltscElabInDbObj(String dtUltscElabInDbObj) {
        setDtUltscElabInDb(dtUltscElabInDbObj);
    }

    @Override
    public String getDtUltscOpzClDb() {
        throw new FieldNotMappedException("dtUltscOpzClDb");
    }

    @Override
    public void setDtUltscOpzClDb(String dtUltscOpzClDb) {
        throw new FieldNotMappedException("dtUltscOpzClDb");
    }

    @Override
    public String getDtUltscOpzClDbObj() {
        return getDtUltscOpzClDb();
    }

    @Override
    public void setDtUltscOpzClDbObj(String dtUltscOpzClDbObj) {
        setDtUltscOpzClDb(dtUltscOpzClDbObj);
    }

    @Override
    public String getDtUltscOpzInDb() {
        throw new FieldNotMappedException("dtUltscOpzInDb");
    }

    @Override
    public void setDtUltscOpzInDb(String dtUltscOpzInDb) {
        throw new FieldNotMappedException("dtUltscOpzInDb");
    }

    @Override
    public String getDtUltscOpzInDbObj() {
        return getDtUltscOpzInDb();
    }

    @Override
    public void setDtUltscOpzInDbObj(String dtUltscOpzInDbObj) {
        setDtUltscOpzInDb(dtUltscOpzInDbObj);
    }

    @Override
    public char getFlFrazProvAcq() {
        throw new FieldNotMappedException("flFrazProvAcq");
    }

    @Override
    public void setFlFrazProvAcq(char flFrazProvAcq) {
        throw new FieldNotMappedException("flFrazProvAcq");
    }

    @Override
    public Character getFlFrazProvAcqObj() {
        return ((Character)getFlFrazProvAcq());
    }

    @Override
    public void setFlFrazProvAcqObj(Character flFrazProvAcqObj) {
        setFlFrazProvAcq(((char)flFrazProvAcqObj));
    }

    @Override
    public char getFlGestPlusv() {
        throw new FieldNotMappedException("flGestPlusv");
    }

    @Override
    public void setFlGestPlusv(char flGestPlusv) {
        throw new FieldNotMappedException("flGestPlusv");
    }

    @Override
    public Character getFlGestPlusvObj() {
        return ((Character)getFlGestPlusv());
    }

    @Override
    public void setFlGestPlusvObj(Character flGestPlusvObj) {
        setFlGestPlusv(((char)flGestPlusvObj));
    }

    @Override
    public short getFlLivDebug() {
        return paramComp.getPcoFlLivDebug();
    }

    @Override
    public void setFlLivDebug(short flLivDebug) {
        this.paramComp.setPcoFlLivDebug(flLivDebug);
    }

    @Override
    public char getFlRcsPoliNoperf() {
        throw new FieldNotMappedException("flRcsPoliNoperf");
    }

    @Override
    public void setFlRcsPoliNoperf(char flRcsPoliNoperf) {
        throw new FieldNotMappedException("flRcsPoliNoperf");
    }

    @Override
    public Character getFlRcsPoliNoperfObj() {
        return ((Character)getFlRcsPoliNoperf());
    }

    @Override
    public void setFlRcsPoliNoperfObj(Character flRcsPoliNoperfObj) {
        setFlRcsPoliNoperf(((char)flRcsPoliNoperfObj));
    }

    @Override
    public char getFlRvcPerf() {
        throw new FieldNotMappedException("flRvcPerf");
    }

    @Override
    public void setFlRvcPerf(char flRvcPerf) {
        throw new FieldNotMappedException("flRvcPerf");
    }

    @Override
    public Character getFlRvcPerfObj() {
        return ((Character)getFlRvcPerf());
    }

    @Override
    public void setFlRvcPerfObj(Character flRvcPerfObj) {
        setFlRvcPerf(((char)flRvcPerfObj));
    }

    @Override
    public char getFlVisualVinpg() {
        throw new FieldNotMappedException("flVisualVinpg");
    }

    @Override
    public void setFlVisualVinpg(char flVisualVinpg) {
        throw new FieldNotMappedException("flVisualVinpg");
    }

    @Override
    public Character getFlVisualVinpgObj() {
        return ((Character)getFlVisualVinpg());
    }

    @Override
    public void setFlVisualVinpgObj(Character flVisualVinpgObj) {
        setFlVisualVinpg(((char)flVisualVinpgObj));
    }

    @Override
    public int getFrqCostiAtt() {
        throw new FieldNotMappedException("frqCostiAtt");
    }

    @Override
    public void setFrqCostiAtt(int frqCostiAtt) {
        throw new FieldNotMappedException("frqCostiAtt");
    }

    @Override
    public Integer getFrqCostiAttObj() {
        return ((Integer)getFrqCostiAtt());
    }

    @Override
    public void setFrqCostiAttObj(Integer frqCostiAttObj) {
        setFrqCostiAtt(((int)frqCostiAttObj));
    }

    @Override
    public int getFrqCostiStornati() {
        throw new FieldNotMappedException("frqCostiStornati");
    }

    @Override
    public void setFrqCostiStornati(int frqCostiStornati) {
        throw new FieldNotMappedException("frqCostiStornati");
    }

    @Override
    public Integer getFrqCostiStornatiObj() {
        return ((Integer)getFrqCostiStornati());
    }

    @Override
    public void setFrqCostiStornatiObj(Integer frqCostiStornatiObj) {
        setFrqCostiStornati(((int)frqCostiStornatiObj));
    }

    @Override
    public int getGgIntrRitPag() {
        throw new FieldNotMappedException("ggIntrRitPag");
    }

    @Override
    public void setGgIntrRitPag(int ggIntrRitPag) {
        throw new FieldNotMappedException("ggIntrRitPag");
    }

    @Override
    public Integer getGgIntrRitPagObj() {
        return ((Integer)getGgIntrRitPag());
    }

    @Override
    public void setGgIntrRitPagObj(Integer ggIntrRitPagObj) {
        setGgIntrRitPag(((int)ggIntrRitPagObj));
    }

    @Override
    public short getGgMaxRecProv() {
        throw new FieldNotMappedException("ggMaxRecProv");
    }

    @Override
    public void setGgMaxRecProv(short ggMaxRecProv) {
        throw new FieldNotMappedException("ggMaxRecProv");
    }

    @Override
    public Short getGgMaxRecProvObj() {
        return ((Short)getGgMaxRecProv());
    }

    @Override
    public void setGgMaxRecProvObj(Short ggMaxRecProvObj) {
        setGgMaxRecProv(((short)ggMaxRecProvObj));
    }

    public Idsv0003Idss0150 getIdsv0003() {
        return idsv0003;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    @Override
    public AfDecimal getImpAssSociale() {
        throw new FieldNotMappedException("impAssSociale");
    }

    @Override
    public void setImpAssSociale(AfDecimal impAssSociale) {
        throw new FieldNotMappedException("impAssSociale");
    }

    @Override
    public AfDecimal getImpAssSocialeObj() {
        return getImpAssSociale();
    }

    @Override
    public void setImpAssSocialeObj(AfDecimal impAssSocialeObj) {
        setImpAssSociale(new AfDecimal(impAssSocialeObj, 15, 3));
    }

    public IndParamComp getIndParamComp() {
        return indParamComp;
    }

    @Override
    public AfDecimal getLimVltr() {
        throw new FieldNotMappedException("limVltr");
    }

    @Override
    public void setLimVltr(AfDecimal limVltr) {
        throw new FieldNotMappedException("limVltr");
    }

    @Override
    public AfDecimal getLimVltrObj() {
        return getLimVltr();
    }

    @Override
    public void setLimVltrObj(AfDecimal limVltrObj) {
        setLimVltr(new AfDecimal(limVltrObj, 15, 3));
    }

    @Override
    public AfDecimal getLmCSubrshConIn() {
        throw new FieldNotMappedException("lmCSubrshConIn");
    }

    @Override
    public void setLmCSubrshConIn(AfDecimal lmCSubrshConIn) {
        throw new FieldNotMappedException("lmCSubrshConIn");
    }

    @Override
    public AfDecimal getLmCSubrshConInObj() {
        return getLmCSubrshConIn();
    }

    @Override
    public void setLmCSubrshConInObj(AfDecimal lmCSubrshConInObj) {
        setLmCSubrshConIn(new AfDecimal(lmCSubrshConInObj, 6, 3));
    }

    @Override
    public AfDecimal getLmRisConInt() {
        throw new FieldNotMappedException("lmRisConInt");
    }

    @Override
    public void setLmRisConInt(AfDecimal lmRisConInt) {
        throw new FieldNotMappedException("lmRisConInt");
    }

    @Override
    public AfDecimal getLmRisConIntObj() {
        return getLmRisConInt();
    }

    @Override
    public void setLmRisConIntObj(AfDecimal lmRisConIntObj) {
        setLmRisConInt(new AfDecimal(lmRisConIntObj, 6, 3));
    }

    @Override
    public char getModComnzInvstSw() {
        throw new FieldNotMappedException("modComnzInvstSw");
    }

    @Override
    public void setModComnzInvstSw(char modComnzInvstSw) {
        throw new FieldNotMappedException("modComnzInvstSw");
    }

    @Override
    public Character getModComnzInvstSwObj() {
        return ((Character)getModComnzInvstSw());
    }

    @Override
    public void setModComnzInvstSwObj(Character modComnzInvstSwObj) {
        setModComnzInvstSw(((char)modComnzInvstSwObj));
    }

    @Override
    public String getModIntrPrest() {
        throw new FieldNotMappedException("modIntrPrest");
    }

    @Override
    public void setModIntrPrest(String modIntrPrest) {
        throw new FieldNotMappedException("modIntrPrest");
    }

    @Override
    public String getModIntrPrestObj() {
        return getModIntrPrest();
    }

    @Override
    public void setModIntrPrestObj(String modIntrPrestObj) {
        setModIntrPrest(modIntrPrestObj);
    }

    @Override
    public int getNumGgArrIntrPr() {
        throw new FieldNotMappedException("numGgArrIntrPr");
    }

    @Override
    public void setNumGgArrIntrPr(int numGgArrIntrPr) {
        throw new FieldNotMappedException("numGgArrIntrPr");
    }

    @Override
    public Integer getNumGgArrIntrPrObj() {
        return ((Integer)getNumGgArrIntrPr());
    }

    @Override
    public void setNumGgArrIntrPrObj(Integer numGgArrIntrPrObj) {
        setNumGgArrIntrPr(((int)numGgArrIntrPrObj));
    }

    @Override
    public int getNumMmCalcMora() {
        throw new FieldNotMappedException("numMmCalcMora");
    }

    @Override
    public void setNumMmCalcMora(int numMmCalcMora) {
        throw new FieldNotMappedException("numMmCalcMora");
    }

    @Override
    public Integer getNumMmCalcMoraObj() {
        return ((Integer)getNumMmCalcMora());
    }

    @Override
    public void setNumMmCalcMoraObj(Integer numMmCalcMoraObj) {
        setNumMmCalcMora(((int)numMmCalcMoraObj));
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public ParamCompDb getParamCompDb() {
        return paramCompDb;
    }

    @Override
    public AfDecimal getPcCSubrshMarsol() {
        throw new FieldNotMappedException("pcCSubrshMarsol");
    }

    @Override
    public void setPcCSubrshMarsol(AfDecimal pcCSubrshMarsol) {
        throw new FieldNotMappedException("pcCSubrshMarsol");
    }

    @Override
    public AfDecimal getPcCSubrshMarsolObj() {
        return getPcCSubrshMarsol();
    }

    @Override
    public void setPcCSubrshMarsolObj(AfDecimal pcCSubrshMarsolObj) {
        setPcCSubrshMarsol(new AfDecimal(pcCSubrshMarsolObj, 6, 3));
    }

    @Override
    public AfDecimal getPcGarNoriskMars() {
        throw new FieldNotMappedException("pcGarNoriskMars");
    }

    @Override
    public void setPcGarNoriskMars(AfDecimal pcGarNoriskMars) {
        throw new FieldNotMappedException("pcGarNoriskMars");
    }

    @Override
    public AfDecimal getPcGarNoriskMarsObj() {
        return getPcGarNoriskMars();
    }

    @Override
    public void setPcGarNoriskMarsObj(AfDecimal pcGarNoriskMarsObj) {
        setPcGarNoriskMars(new AfDecimal(pcGarNoriskMarsObj, 6, 3));
    }

    @Override
    public AfDecimal getPcProv1aaAcq() {
        throw new FieldNotMappedException("pcProv1aaAcq");
    }

    @Override
    public void setPcProv1aaAcq(AfDecimal pcProv1aaAcq) {
        throw new FieldNotMappedException("pcProv1aaAcq");
    }

    @Override
    public AfDecimal getPcProv1aaAcqObj() {
        return getPcProv1aaAcq();
    }

    @Override
    public void setPcProv1aaAcqObj(AfDecimal pcProv1aaAcqObj) {
        setPcProv1aaAcq(new AfDecimal(pcProv1aaAcqObj, 6, 3));
    }

    @Override
    public AfDecimal getPcRidImp1382011() {
        throw new FieldNotMappedException("pcRidImp1382011");
    }

    @Override
    public void setPcRidImp1382011(AfDecimal pcRidImp1382011) {
        throw new FieldNotMappedException("pcRidImp1382011");
    }

    @Override
    public AfDecimal getPcRidImp1382011Obj() {
        return getPcRidImp1382011();
    }

    @Override
    public void setPcRidImp1382011Obj(AfDecimal pcRidImp1382011Obj) {
        setPcRidImp1382011(new AfDecimal(pcRidImp1382011Obj, 6, 3));
    }

    @Override
    public AfDecimal getPcRidImp662014() {
        throw new FieldNotMappedException("pcRidImp662014");
    }

    @Override
    public void setPcRidImp662014(AfDecimal pcRidImp662014) {
        throw new FieldNotMappedException("pcRidImp662014");
    }

    @Override
    public AfDecimal getPcRidImp662014Obj() {
        return getPcRidImp662014();
    }

    @Override
    public void setPcRidImp662014Obj(AfDecimal pcRidImp662014Obj) {
        setPcRidImp662014(new AfDecimal(pcRidImp662014Obj, 6, 3));
    }

    @Override
    public AfDecimal getPcRmMarsol() {
        throw new FieldNotMappedException("pcRmMarsol");
    }

    @Override
    public void setPcRmMarsol(AfDecimal pcRmMarsol) {
        throw new FieldNotMappedException("pcRmMarsol");
    }

    @Override
    public AfDecimal getPcRmMarsolObj() {
        return getPcRmMarsol();
    }

    @Override
    public void setPcRmMarsolObj(AfDecimal pcRmMarsolObj) {
        setPcRmMarsol(new AfDecimal(pcRmMarsolObj, 6, 3));
    }

    @Override
    public int getPcoCodCompAnia() {
        throw new FieldNotMappedException("pcoCodCompAnia");
    }

    @Override
    public void setPcoCodCompAnia(int pcoCodCompAnia) {
        throw new FieldNotMappedException("pcoCodCompAnia");
    }

    @Override
    public AfDecimal getSoglAmlPrePer() {
        throw new FieldNotMappedException("soglAmlPrePer");
    }

    @Override
    public void setSoglAmlPrePer(AfDecimal soglAmlPrePer) {
        throw new FieldNotMappedException("soglAmlPrePer");
    }

    @Override
    public AfDecimal getSoglAmlPrePerObj() {
        return getSoglAmlPrePer();
    }

    @Override
    public void setSoglAmlPrePerObj(AfDecimal soglAmlPrePerObj) {
        setSoglAmlPrePer(new AfDecimal(soglAmlPrePerObj, 15, 3));
    }

    @Override
    public AfDecimal getSoglAmlPreSavR() {
        throw new FieldNotMappedException("soglAmlPreSavR");
    }

    @Override
    public void setSoglAmlPreSavR(AfDecimal soglAmlPreSavR) {
        throw new FieldNotMappedException("soglAmlPreSavR");
    }

    @Override
    public AfDecimal getSoglAmlPreSavRObj() {
        return getSoglAmlPreSavR();
    }

    @Override
    public void setSoglAmlPreSavRObj(AfDecimal soglAmlPreSavRObj) {
        setSoglAmlPreSavR(new AfDecimal(soglAmlPreSavRObj, 15, 3));
    }

    @Override
    public AfDecimal getSoglAmlPreUni() {
        throw new FieldNotMappedException("soglAmlPreUni");
    }

    @Override
    public void setSoglAmlPreUni(AfDecimal soglAmlPreUni) {
        throw new FieldNotMappedException("soglAmlPreUni");
    }

    @Override
    public AfDecimal getSoglAmlPreUniObj() {
        return getSoglAmlPreUni();
    }

    @Override
    public void setSoglAmlPreUniObj(AfDecimal soglAmlPreUniObj) {
        setSoglAmlPreUni(new AfDecimal(soglAmlPreUniObj, 15, 3));
    }

    @Override
    public String getStstXRegioneDb() {
        throw new FieldNotMappedException("ststXRegioneDb");
    }

    @Override
    public void setStstXRegioneDb(String ststXRegioneDb) {
        throw new FieldNotMappedException("ststXRegioneDb");
    }

    @Override
    public String getStstXRegioneDbObj() {
        return getStstXRegioneDb();
    }

    @Override
    public void setStstXRegioneDbObj(String ststXRegioneDbObj) {
        setStstXRegioneDb(ststXRegioneDbObj);
    }

    @Override
    public String getTpLivGenzTit() {
        throw new FieldNotMappedException("tpLivGenzTit");
    }

    @Override
    public void setTpLivGenzTit(String tpLivGenzTit) {
        throw new FieldNotMappedException("tpLivGenzTit");
    }

    @Override
    public String getTpModRival() {
        throw new FieldNotMappedException("tpModRival");
    }

    @Override
    public void setTpModRival(String tpModRival) {
        throw new FieldNotMappedException("tpModRival");
    }

    @Override
    public char getTpRatPerf() {
        throw new FieldNotMappedException("tpRatPerf");
    }

    @Override
    public void setTpRatPerf(char tpRatPerf) {
        throw new FieldNotMappedException("tpRatPerf");
    }

    @Override
    public Character getTpRatPerfObj() {
        return ((Character)getTpRatPerf());
    }

    @Override
    public void setTpRatPerfObj(Character tpRatPerfObj) {
        setTpRatPerf(((char)tpRatPerfObj));
    }

    @Override
    public String getTpValzzDtVlt() {
        throw new FieldNotMappedException("tpValzzDtVlt");
    }

    @Override
    public void setTpValzzDtVlt(String tpValzzDtVlt) {
        throw new FieldNotMappedException("tpValzzDtVlt");
    }

    @Override
    public String getTpValzzDtVltObj() {
        return getTpValzzDtVlt();
    }

    @Override
    public void setTpValzzDtVltObj(String tpValzzDtVltObj) {
        setTpValzzDtVlt(tpValzzDtVltObj);
    }

    public WkLivelloGravita getWkLivelloGravita() {
        return wkLivelloGravita;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_SQLCODE_ED = 10;
        public static final int WK_LABEL = 30;
        public static final int DIFFERENZA_DISPLAY = 10;
        public static final int WK_PARAM = 1;
        public static final int WK_PGM = 8;
        public static final int WK_DATA_AMG = 8;
        public static final int WK_SQLCODE = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
