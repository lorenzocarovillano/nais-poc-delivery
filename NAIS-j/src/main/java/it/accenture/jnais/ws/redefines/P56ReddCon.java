package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-REDD-CON<br>
 * Variable: P56-REDD-CON from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ReddCon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ReddCon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_REDD_CON;
    }

    public void setP56ReddCon(AfDecimal p56ReddCon) {
        writeDecimalAsPacked(Pos.P56_REDD_CON, p56ReddCon.copy());
    }

    public void setP56ReddConFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_REDD_CON, Pos.P56_REDD_CON);
    }

    /**Original name: P56-REDD-CON<br>*/
    public AfDecimal getP56ReddCon() {
        return readPackedAsDecimal(Pos.P56_REDD_CON, Len.Int.P56_REDD_CON, Len.Fract.P56_REDD_CON);
    }

    public byte[] getP56ReddConAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_REDD_CON, Pos.P56_REDD_CON);
        return buffer;
    }

    public void setP56ReddConNull(String p56ReddConNull) {
        writeString(Pos.P56_REDD_CON_NULL, p56ReddConNull, Len.P56_REDD_CON_NULL);
    }

    /**Original name: P56-REDD-CON-NULL<br>*/
    public String getP56ReddConNull() {
        return readString(Pos.P56_REDD_CON_NULL, Len.P56_REDD_CON_NULL);
    }

    public String getP56ReddConNullFormatted() {
        return Functions.padBlanks(getP56ReddConNull(), Len.P56_REDD_CON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_REDD_CON = 1;
        public static final int P56_REDD_CON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_REDD_CON = 8;
        public static final int P56_REDD_CON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_REDD_CON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_REDD_CON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
