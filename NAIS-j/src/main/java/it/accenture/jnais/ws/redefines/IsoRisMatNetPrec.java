package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-RIS-MAT-NET-PREC<br>
 * Variable: ISO-RIS-MAT-NET-PREC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoRisMatNetPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoRisMatNetPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_RIS_MAT_NET_PREC;
    }

    public void setIsoRisMatNetPrec(AfDecimal isoRisMatNetPrec) {
        writeDecimalAsPacked(Pos.ISO_RIS_MAT_NET_PREC, isoRisMatNetPrec.copy());
    }

    public void setIsoRisMatNetPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_RIS_MAT_NET_PREC, Pos.ISO_RIS_MAT_NET_PREC);
    }

    /**Original name: ISO-RIS-MAT-NET-PREC<br>*/
    public AfDecimal getIsoRisMatNetPrec() {
        return readPackedAsDecimal(Pos.ISO_RIS_MAT_NET_PREC, Len.Int.ISO_RIS_MAT_NET_PREC, Len.Fract.ISO_RIS_MAT_NET_PREC);
    }

    public byte[] getIsoRisMatNetPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_RIS_MAT_NET_PREC, Pos.ISO_RIS_MAT_NET_PREC);
        return buffer;
    }

    public void setIsoRisMatNetPrecNull(String isoRisMatNetPrecNull) {
        writeString(Pos.ISO_RIS_MAT_NET_PREC_NULL, isoRisMatNetPrecNull, Len.ISO_RIS_MAT_NET_PREC_NULL);
    }

    /**Original name: ISO-RIS-MAT-NET-PREC-NULL<br>*/
    public String getIsoRisMatNetPrecNull() {
        return readString(Pos.ISO_RIS_MAT_NET_PREC_NULL, Len.ISO_RIS_MAT_NET_PREC_NULL);
    }

    public String getIsoRisMatNetPrecNullFormatted() {
        return Functions.padBlanks(getIsoRisMatNetPrecNull(), Len.ISO_RIS_MAT_NET_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_RIS_MAT_NET_PREC = 1;
        public static final int ISO_RIS_MAT_NET_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_RIS_MAT_NET_PREC = 8;
        public static final int ISO_RIS_MAT_NET_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_RIS_MAT_NET_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_RIS_MAT_NET_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
