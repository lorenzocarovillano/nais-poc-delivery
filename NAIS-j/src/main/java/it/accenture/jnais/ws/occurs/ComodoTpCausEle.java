package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: COMODO-TP-CAUS-ELE<br>
 * Variables: COMODO-TP-CAUS-ELE from program IVVS0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class ComodoTpCausEle {

    //==== PROPERTIES ====
    //Original name: COMODO-TP-CAUS
    private String comodoTpCaus = DefaultValues.stringVal(Len.COMODO_TP_CAUS);

    //==== METHODS ====
    public byte[] getTpCausEleBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, comodoTpCaus, Len.COMODO_TP_CAUS);
        return buffer;
    }

    public void setComodoTpCaus(String comodoTpCaus) {
        this.comodoTpCaus = Functions.subString(comodoTpCaus, Len.COMODO_TP_CAUS);
    }

    public String getComodoTpCaus() {
        return this.comodoTpCaus;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COMODO_TP_CAUS = 2;
        public static final int TP_CAUS_ELE = COMODO_TP_CAUS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
