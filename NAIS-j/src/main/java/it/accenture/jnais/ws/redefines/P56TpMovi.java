package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-TP-MOVI<br>
 * Variable: P56-TP-MOVI from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56TpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56TpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_TP_MOVI;
    }

    public void setP56TpMovi(int p56TpMovi) {
        writeIntAsPacked(Pos.P56_TP_MOVI, p56TpMovi, Len.Int.P56_TP_MOVI);
    }

    public void setP56TpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_TP_MOVI, Pos.P56_TP_MOVI);
    }

    /**Original name: P56-TP-MOVI<br>*/
    public int getP56TpMovi() {
        return readPackedAsInt(Pos.P56_TP_MOVI, Len.Int.P56_TP_MOVI);
    }

    public byte[] getP56TpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_TP_MOVI, Pos.P56_TP_MOVI);
        return buffer;
    }

    public void setP56TpMoviNull(String p56TpMoviNull) {
        writeString(Pos.P56_TP_MOVI_NULL, p56TpMoviNull, Len.P56_TP_MOVI_NULL);
    }

    /**Original name: P56-TP-MOVI-NULL<br>*/
    public String getP56TpMoviNull() {
        return readString(Pos.P56_TP_MOVI_NULL, Len.P56_TP_MOVI_NULL);
    }

    public String getP56TpMoviNullFormatted() {
        return Functions.padBlanks(getP56TpMoviNull(), Len.P56_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_TP_MOVI = 1;
        public static final int P56_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_TP_MOVI = 3;
        public static final int P56_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
