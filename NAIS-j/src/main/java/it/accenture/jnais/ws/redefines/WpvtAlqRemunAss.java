package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-ALQ-REMUN-ASS<br>
 * Variable: WPVT-ALQ-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtAlqRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtAlqRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ALQ_REMUN_ASS;
    }

    public void setWpvtAlqRemunAss(AfDecimal wpvtAlqRemunAss) {
        writeDecimalAsPacked(Pos.WPVT_ALQ_REMUN_ASS, wpvtAlqRemunAss.copy());
    }

    public void setWpvtAlqRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ALQ_REMUN_ASS, Pos.WPVT_ALQ_REMUN_ASS);
    }

    /**Original name: WPVT-ALQ-REMUN-ASS<br>*/
    public AfDecimal getWpvtAlqRemunAss() {
        return readPackedAsDecimal(Pos.WPVT_ALQ_REMUN_ASS, Len.Int.WPVT_ALQ_REMUN_ASS, Len.Fract.WPVT_ALQ_REMUN_ASS);
    }

    public byte[] getWpvtAlqRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ALQ_REMUN_ASS, Pos.WPVT_ALQ_REMUN_ASS);
        return buffer;
    }

    public void initWpvtAlqRemunAssSpaces() {
        fill(Pos.WPVT_ALQ_REMUN_ASS, Len.WPVT_ALQ_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWpvtAlqRemunAssNull(String wpvtAlqRemunAssNull) {
        writeString(Pos.WPVT_ALQ_REMUN_ASS_NULL, wpvtAlqRemunAssNull, Len.WPVT_ALQ_REMUN_ASS_NULL);
    }

    /**Original name: WPVT-ALQ-REMUN-ASS-NULL<br>*/
    public String getWpvtAlqRemunAssNull() {
        return readString(Pos.WPVT_ALQ_REMUN_ASS_NULL, Len.WPVT_ALQ_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_REMUN_ASS = 1;
        public static final int WPVT_ALQ_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_REMUN_ASS = 4;
        public static final int WPVT_ALQ_REMUN_ASS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
