package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RAN-DT-DELIBERA-CDA<br>
 * Variable: RAN-DT-DELIBERA-CDA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RanDtDeliberaCda extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RanDtDeliberaCda() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAN_DT_DELIBERA_CDA;
    }

    public void setRanDtDeliberaCda(int ranDtDeliberaCda) {
        writeIntAsPacked(Pos.RAN_DT_DELIBERA_CDA, ranDtDeliberaCda, Len.Int.RAN_DT_DELIBERA_CDA);
    }

    public void setRanDtDeliberaCdaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RAN_DT_DELIBERA_CDA, Pos.RAN_DT_DELIBERA_CDA);
    }

    /**Original name: RAN-DT-DELIBERA-CDA<br>*/
    public int getRanDtDeliberaCda() {
        return readPackedAsInt(Pos.RAN_DT_DELIBERA_CDA, Len.Int.RAN_DT_DELIBERA_CDA);
    }

    public byte[] getRanDtDeliberaCdaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RAN_DT_DELIBERA_CDA, Pos.RAN_DT_DELIBERA_CDA);
        return buffer;
    }

    public void setRanDtDeliberaCdaNull(String ranDtDeliberaCdaNull) {
        writeString(Pos.RAN_DT_DELIBERA_CDA_NULL, ranDtDeliberaCdaNull, Len.RAN_DT_DELIBERA_CDA_NULL);
    }

    /**Original name: RAN-DT-DELIBERA-CDA-NULL<br>*/
    public String getRanDtDeliberaCdaNull() {
        return readString(Pos.RAN_DT_DELIBERA_CDA_NULL, Len.RAN_DT_DELIBERA_CDA_NULL);
    }

    public String getRanDtDeliberaCdaNullFormatted() {
        return Functions.padBlanks(getRanDtDeliberaCdaNull(), Len.RAN_DT_DELIBERA_CDA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RAN_DT_DELIBERA_CDA = 1;
        public static final int RAN_DT_DELIBERA_CDA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_DT_DELIBERA_CDA = 5;
        public static final int RAN_DT_DELIBERA_CDA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_DT_DELIBERA_CDA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
