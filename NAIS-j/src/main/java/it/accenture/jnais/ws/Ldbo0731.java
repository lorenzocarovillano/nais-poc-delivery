package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBO0731<br>
 * Variable: LDBO0731 from program LDBS0730<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbo0731 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBO0731-ID-PADRE
    private int ldbo0731IdPadre = DefaultValues.INT_VAL;
    //Original name: LDBO0731-STB
    private String ldbo0731Stb = DefaultValues.stringVal(Len.LDBO0731_STB);
    //Original name: LDBO0731-TGA
    private String ldbo0731Tga = DefaultValues.stringVal(Len.LDBO0731_TGA);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBO0731;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbo0731Bytes(buf);
    }

    public void setLdbo0731Bytes(byte[] buffer) {
        setLdbo0731Bytes(buffer, 1);
    }

    public byte[] getLdbo0731Bytes() {
        byte[] buffer = new byte[Len.LDBO0731];
        return getLdbo0731Bytes(buffer, 1);
    }

    public void setLdbo0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaLdbo0731Bytes(buffer, position);
    }

    public byte[] getLdbo0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaLdbo0731Bytes(buffer, position);
        return buffer;
    }

    public void setAreaLdbo0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbo0731IdPadre = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LDBO0731_ID_PADRE, 0);
        position += Len.LDBO0731_ID_PADRE;
        ldbo0731Stb = MarshalByte.readString(buffer, position, Len.LDBO0731_STB);
        position += Len.LDBO0731_STB;
        ldbo0731Tga = MarshalByte.readString(buffer, position, Len.LDBO0731_TGA);
    }

    public byte[] getAreaLdbo0731Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbo0731IdPadre, Len.Int.LDBO0731_ID_PADRE, 0);
        position += Len.LDBO0731_ID_PADRE;
        MarshalByte.writeString(buffer, position, ldbo0731Stb, Len.LDBO0731_STB);
        position += Len.LDBO0731_STB;
        MarshalByte.writeString(buffer, position, ldbo0731Tga, Len.LDBO0731_TGA);
        return buffer;
    }

    public void setLdbo0731IdPadre(int ldbo0731IdPadre) {
        this.ldbo0731IdPadre = ldbo0731IdPadre;
    }

    public int getLdbo0731IdPadre() {
        return this.ldbo0731IdPadre;
    }

    public void setLdbo0731Stb(String ldbo0731Stb) {
        this.ldbo0731Stb = Functions.subString(ldbo0731Stb, Len.LDBO0731_STB);
    }

    public String getLdbo0731Stb() {
        return this.ldbo0731Stb;
    }

    public void setLdbo0731Tga(String ldbo0731Tga) {
        this.ldbo0731Tga = Functions.subString(ldbo0731Tga, Len.LDBO0731_TGA);
    }

    public String getLdbo0731Tga() {
        return this.ldbo0731Tga;
    }

    @Override
    public byte[] serialize() {
        return getLdbo0731Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBO0731_ID_PADRE = 5;
        public static final int LDBO0731_STB = 2000;
        public static final int LDBO0731_TGA = 2000;
        public static final int AREA_LDBO0731 = LDBO0731_ID_PADRE + LDBO0731_STB + LDBO0731_TGA;
        public static final int LDBO0731 = AREA_LDBO0731;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBO0731_ID_PADRE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
