package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-TS-RIVAL-FIS<br>
 * Variable: WTGA-TS-RIVAL-FIS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaTsRivalFis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaTsRivalFis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_TS_RIVAL_FIS;
    }

    public void setWtgaTsRivalFis(AfDecimal wtgaTsRivalFis) {
        writeDecimalAsPacked(Pos.WTGA_TS_RIVAL_FIS, wtgaTsRivalFis.copy());
    }

    public void setWtgaTsRivalFisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_TS_RIVAL_FIS, Pos.WTGA_TS_RIVAL_FIS);
    }

    /**Original name: WTGA-TS-RIVAL-FIS<br>*/
    public AfDecimal getWtgaTsRivalFis() {
        return readPackedAsDecimal(Pos.WTGA_TS_RIVAL_FIS, Len.Int.WTGA_TS_RIVAL_FIS, Len.Fract.WTGA_TS_RIVAL_FIS);
    }

    public byte[] getWtgaTsRivalFisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_TS_RIVAL_FIS, Pos.WTGA_TS_RIVAL_FIS);
        return buffer;
    }

    public void initWtgaTsRivalFisSpaces() {
        fill(Pos.WTGA_TS_RIVAL_FIS, Len.WTGA_TS_RIVAL_FIS, Types.SPACE_CHAR);
    }

    public void setWtgaTsRivalFisNull(String wtgaTsRivalFisNull) {
        writeString(Pos.WTGA_TS_RIVAL_FIS_NULL, wtgaTsRivalFisNull, Len.WTGA_TS_RIVAL_FIS_NULL);
    }

    /**Original name: WTGA-TS-RIVAL-FIS-NULL<br>*/
    public String getWtgaTsRivalFisNull() {
        return readString(Pos.WTGA_TS_RIVAL_FIS_NULL, Len.WTGA_TS_RIVAL_FIS_NULL);
    }

    public String getWtgaTsRivalFisNullFormatted() {
        return Functions.padBlanks(getWtgaTsRivalFisNull(), Len.WTGA_TS_RIVAL_FIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_TS_RIVAL_FIS = 1;
        public static final int WTGA_TS_RIVAL_FIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_TS_RIVAL_FIS = 8;
        public static final int WTGA_TS_RIVAL_FIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_TS_RIVAL_FIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_TS_RIVAL_FIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
