package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DT-PRESC<br>
 * Variable: WADE-DT-PRESC from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtPresc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtPresc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_PRESC;
    }

    public void setWadeDtPresc(int wadeDtPresc) {
        writeIntAsPacked(Pos.WADE_DT_PRESC, wadeDtPresc, Len.Int.WADE_DT_PRESC);
    }

    public void setWadeDtPrescFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_PRESC, Pos.WADE_DT_PRESC);
    }

    /**Original name: WADE-DT-PRESC<br>*/
    public int getWadeDtPresc() {
        return readPackedAsInt(Pos.WADE_DT_PRESC, Len.Int.WADE_DT_PRESC);
    }

    public byte[] getWadeDtPrescAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_PRESC, Pos.WADE_DT_PRESC);
        return buffer;
    }

    public void initWadeDtPrescSpaces() {
        fill(Pos.WADE_DT_PRESC, Len.WADE_DT_PRESC, Types.SPACE_CHAR);
    }

    public void setWadeDtPrescNull(String wadeDtPrescNull) {
        writeString(Pos.WADE_DT_PRESC_NULL, wadeDtPrescNull, Len.WADE_DT_PRESC_NULL);
    }

    /**Original name: WADE-DT-PRESC-NULL<br>*/
    public String getWadeDtPrescNull() {
        return readString(Pos.WADE_DT_PRESC_NULL, Len.WADE_DT_PRESC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_PRESC = 1;
        public static final int WADE_DT_PRESC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_PRESC = 5;
        public static final int WADE_DT_PRESC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_PRESC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
