package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WODE-ID-MOVI-CHIU<br>
 * Variable: WODE-ID-MOVI-CHIU from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WodeIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WodeIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WODE_ID_MOVI_CHIU;
    }

    public void setWodeIdMoviChiu(int wodeIdMoviChiu) {
        writeIntAsPacked(Pos.WODE_ID_MOVI_CHIU, wodeIdMoviChiu, Len.Int.WODE_ID_MOVI_CHIU);
    }

    /**Original name: WODE-ID-MOVI-CHIU<br>*/
    public int getWodeIdMoviChiu() {
        return readPackedAsInt(Pos.WODE_ID_MOVI_CHIU, Len.Int.WODE_ID_MOVI_CHIU);
    }

    public void setWodeIdMoviChiuNull(String wodeIdMoviChiuNull) {
        writeString(Pos.WODE_ID_MOVI_CHIU_NULL, wodeIdMoviChiuNull, Len.WODE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WODE-ID-MOVI-CHIU-NULL<br>*/
    public String getWodeIdMoviChiuNull() {
        return readString(Pos.WODE_ID_MOVI_CHIU_NULL, Len.WODE_ID_MOVI_CHIU_NULL);
    }

    public String getWodeIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getWodeIdMoviChiuNull(), Len.WODE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WODE_ID_MOVI_CHIU = 1;
        public static final int WODE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WODE_ID_MOVI_CHIU = 5;
        public static final int WODE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WODE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
