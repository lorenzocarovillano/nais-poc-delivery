package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-VAL-DT<br>
 * Variable: PCA-VAL-DT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaValDt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaValDt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_VAL_DT;
    }

    public void setPcaValDt(int pcaValDt) {
        writeIntAsPacked(Pos.PCA_VAL_DT, pcaValDt, Len.Int.PCA_VAL_DT);
    }

    public void setPcaValDtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_VAL_DT, Pos.PCA_VAL_DT);
    }

    /**Original name: PCA-VAL-DT<br>*/
    public int getPcaValDt() {
        return readPackedAsInt(Pos.PCA_VAL_DT, Len.Int.PCA_VAL_DT);
    }

    public byte[] getPcaValDtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_VAL_DT, Pos.PCA_VAL_DT);
        return buffer;
    }

    public void setPcaValDtNull(String pcaValDtNull) {
        writeString(Pos.PCA_VAL_DT_NULL, pcaValDtNull, Len.PCA_VAL_DT_NULL);
    }

    /**Original name: PCA-VAL-DT-NULL<br>*/
    public String getPcaValDtNull() {
        return readString(Pos.PCA_VAL_DT_NULL, Len.PCA_VAL_DT_NULL);
    }

    public String getPcaValDtNullFormatted() {
        return Functions.padBlanks(getPcaValDtNull(), Len.PCA_VAL_DT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_VAL_DT = 1;
        public static final int PCA_VAL_DT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_VAL_DT = 5;
        public static final int PCA_VAL_DT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_VAL_DT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
