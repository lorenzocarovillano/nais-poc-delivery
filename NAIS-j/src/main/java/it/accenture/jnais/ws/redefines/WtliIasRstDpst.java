package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IAS-RST-DPST<br>
 * Variable: WTLI-IAS-RST-DPST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliIasRstDpst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliIasRstDpst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IAS_RST_DPST;
    }

    public void setWtliIasRstDpst(AfDecimal wtliIasRstDpst) {
        writeDecimalAsPacked(Pos.WTLI_IAS_RST_DPST, wtliIasRstDpst.copy());
    }

    public void setWtliIasRstDpstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IAS_RST_DPST, Pos.WTLI_IAS_RST_DPST);
    }

    /**Original name: WTLI-IAS-RST-DPST<br>*/
    public AfDecimal getWtliIasRstDpst() {
        return readPackedAsDecimal(Pos.WTLI_IAS_RST_DPST, Len.Int.WTLI_IAS_RST_DPST, Len.Fract.WTLI_IAS_RST_DPST);
    }

    public byte[] getWtliIasRstDpstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IAS_RST_DPST, Pos.WTLI_IAS_RST_DPST);
        return buffer;
    }

    public void initWtliIasRstDpstSpaces() {
        fill(Pos.WTLI_IAS_RST_DPST, Len.WTLI_IAS_RST_DPST, Types.SPACE_CHAR);
    }

    public void setWtliIasRstDpstNull(String wtliIasRstDpstNull) {
        writeString(Pos.WTLI_IAS_RST_DPST_NULL, wtliIasRstDpstNull, Len.WTLI_IAS_RST_DPST_NULL);
    }

    /**Original name: WTLI-IAS-RST-DPST-NULL<br>*/
    public String getWtliIasRstDpstNull() {
        return readString(Pos.WTLI_IAS_RST_DPST_NULL, Len.WTLI_IAS_RST_DPST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_RST_DPST = 1;
        public static final int WTLI_IAS_RST_DPST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IAS_RST_DPST = 8;
        public static final int WTLI_IAS_RST_DPST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_RST_DPST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IAS_RST_DPST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
