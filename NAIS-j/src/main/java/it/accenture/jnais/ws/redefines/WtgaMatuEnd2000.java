package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-MATU-END2000<br>
 * Variable: WTGA-MATU-END2000 from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaMatuEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaMatuEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_MATU_END2000;
    }

    public void setWtgaMatuEnd2000(AfDecimal wtgaMatuEnd2000) {
        writeDecimalAsPacked(Pos.WTGA_MATU_END2000, wtgaMatuEnd2000.copy());
    }

    public void setWtgaMatuEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_MATU_END2000, Pos.WTGA_MATU_END2000);
    }

    /**Original name: WTGA-MATU-END2000<br>*/
    public AfDecimal getWtgaMatuEnd2000() {
        return readPackedAsDecimal(Pos.WTGA_MATU_END2000, Len.Int.WTGA_MATU_END2000, Len.Fract.WTGA_MATU_END2000);
    }

    public byte[] getWtgaMatuEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_MATU_END2000, Pos.WTGA_MATU_END2000);
        return buffer;
    }

    public void initWtgaMatuEnd2000Spaces() {
        fill(Pos.WTGA_MATU_END2000, Len.WTGA_MATU_END2000, Types.SPACE_CHAR);
    }

    public void setWtgaMatuEnd2000Null(String wtgaMatuEnd2000Null) {
        writeString(Pos.WTGA_MATU_END2000_NULL, wtgaMatuEnd2000Null, Len.WTGA_MATU_END2000_NULL);
    }

    /**Original name: WTGA-MATU-END2000-NULL<br>*/
    public String getWtgaMatuEnd2000Null() {
        return readString(Pos.WTGA_MATU_END2000_NULL, Len.WTGA_MATU_END2000_NULL);
    }

    public String getWtgaMatuEnd2000NullFormatted() {
        return Functions.padBlanks(getWtgaMatuEnd2000Null(), Len.WTGA_MATU_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_MATU_END2000 = 1;
        public static final int WTGA_MATU_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_MATU_END2000 = 8;
        public static final int WTGA_MATU_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_MATU_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_MATU_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
