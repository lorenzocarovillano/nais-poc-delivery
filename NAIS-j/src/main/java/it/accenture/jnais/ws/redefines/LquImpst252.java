package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-252<br>
 * Variable: LQU-IMPST-252 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpst252 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpst252() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST252;
    }

    public void setLquImpst252(AfDecimal lquImpst252) {
        writeDecimalAsPacked(Pos.LQU_IMPST252, lquImpst252.copy());
    }

    public void setLquImpst252FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST252, Pos.LQU_IMPST252);
    }

    /**Original name: LQU-IMPST-252<br>*/
    public AfDecimal getLquImpst252() {
        return readPackedAsDecimal(Pos.LQU_IMPST252, Len.Int.LQU_IMPST252, Len.Fract.LQU_IMPST252);
    }

    public byte[] getLquImpst252AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST252, Pos.LQU_IMPST252);
        return buffer;
    }

    public void setLquImpst252Null(String lquImpst252Null) {
        writeString(Pos.LQU_IMPST252_NULL, lquImpst252Null, Len.LQU_IMPST252_NULL);
    }

    /**Original name: LQU-IMPST-252-NULL<br>*/
    public String getLquImpst252Null() {
        return readString(Pos.LQU_IMPST252_NULL, Len.LQU_IMPST252_NULL);
    }

    public String getLquImpst252NullFormatted() {
        return Functions.padBlanks(getLquImpst252Null(), Len.LQU_IMPST252_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST252 = 1;
        public static final int LQU_IMPST252_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST252 = 8;
        public static final int LQU_IMPST252_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST252 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST252 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
