package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-SOST-DFZ<br>
 * Variable: DFL-ACCPRE-SOST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreSostDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreSostDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_SOST_DFZ;
    }

    public void setDflAccpreSostDfz(AfDecimal dflAccpreSostDfz) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_SOST_DFZ, dflAccpreSostDfz.copy());
    }

    public void setDflAccpreSostDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_SOST_DFZ, Pos.DFL_ACCPRE_SOST_DFZ);
    }

    /**Original name: DFL-ACCPRE-SOST-DFZ<br>*/
    public AfDecimal getDflAccpreSostDfz() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_SOST_DFZ, Len.Int.DFL_ACCPRE_SOST_DFZ, Len.Fract.DFL_ACCPRE_SOST_DFZ);
    }

    public byte[] getDflAccpreSostDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_SOST_DFZ, Pos.DFL_ACCPRE_SOST_DFZ);
        return buffer;
    }

    public void setDflAccpreSostDfzNull(String dflAccpreSostDfzNull) {
        writeString(Pos.DFL_ACCPRE_SOST_DFZ_NULL, dflAccpreSostDfzNull, Len.DFL_ACCPRE_SOST_DFZ_NULL);
    }

    /**Original name: DFL-ACCPRE-SOST-DFZ-NULL<br>*/
    public String getDflAccpreSostDfzNull() {
        return readString(Pos.DFL_ACCPRE_SOST_DFZ_NULL, Len.DFL_ACCPRE_SOST_DFZ_NULL);
    }

    public String getDflAccpreSostDfzNullFormatted() {
        return Functions.padBlanks(getDflAccpreSostDfzNull(), Len.DFL_ACCPRE_SOST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_SOST_DFZ = 1;
        public static final int DFL_ACCPRE_SOST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_SOST_DFZ = 8;
        public static final int DFL_ACCPRE_SOST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_SOST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_SOST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
