package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-QUIE-C<br>
 * Variable: PCO-DT-ULT-BOLL-QUIE-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollQuieC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollQuieC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_QUIE_C;
    }

    public void setPcoDtUltBollQuieC(int pcoDtUltBollQuieC) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_QUIE_C, pcoDtUltBollQuieC, Len.Int.PCO_DT_ULT_BOLL_QUIE_C);
    }

    public void setPcoDtUltBollQuieCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_QUIE_C, Pos.PCO_DT_ULT_BOLL_QUIE_C);
    }

    /**Original name: PCO-DT-ULT-BOLL-QUIE-C<br>*/
    public int getPcoDtUltBollQuieC() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_QUIE_C, Len.Int.PCO_DT_ULT_BOLL_QUIE_C);
    }

    public byte[] getPcoDtUltBollQuieCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_QUIE_C, Pos.PCO_DT_ULT_BOLL_QUIE_C);
        return buffer;
    }

    public void initPcoDtUltBollQuieCHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_QUIE_C, Len.PCO_DT_ULT_BOLL_QUIE_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollQuieCNull(String pcoDtUltBollQuieCNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_QUIE_C_NULL, pcoDtUltBollQuieCNull, Len.PCO_DT_ULT_BOLL_QUIE_C_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-QUIE-C-NULL<br>*/
    public String getPcoDtUltBollQuieCNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_QUIE_C_NULL, Len.PCO_DT_ULT_BOLL_QUIE_C_NULL);
    }

    public String getPcoDtUltBollQuieCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollQuieCNull(), Len.PCO_DT_ULT_BOLL_QUIE_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_QUIE_C = 1;
        public static final int PCO_DT_ULT_BOLL_QUIE_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_QUIE_C = 5;
        public static final int PCO_DT_ULT_BOLL_QUIE_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_QUIE_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
