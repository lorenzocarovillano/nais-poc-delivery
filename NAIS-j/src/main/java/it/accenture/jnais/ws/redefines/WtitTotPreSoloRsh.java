package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PRE-SOLO-RSH<br>
 * Variable: WTIT-TOT-PRE-SOLO-RSH from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PRE_SOLO_RSH;
    }

    public void setWtitTotPreSoloRsh(AfDecimal wtitTotPreSoloRsh) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PRE_SOLO_RSH, wtitTotPreSoloRsh.copy());
    }

    public void setWtitTotPreSoloRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PRE_SOLO_RSH, Pos.WTIT_TOT_PRE_SOLO_RSH);
    }

    /**Original name: WTIT-TOT-PRE-SOLO-RSH<br>*/
    public AfDecimal getWtitTotPreSoloRsh() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PRE_SOLO_RSH, Len.Int.WTIT_TOT_PRE_SOLO_RSH, Len.Fract.WTIT_TOT_PRE_SOLO_RSH);
    }

    public byte[] getWtitTotPreSoloRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PRE_SOLO_RSH, Pos.WTIT_TOT_PRE_SOLO_RSH);
        return buffer;
    }

    public void initWtitTotPreSoloRshSpaces() {
        fill(Pos.WTIT_TOT_PRE_SOLO_RSH, Len.WTIT_TOT_PRE_SOLO_RSH, Types.SPACE_CHAR);
    }

    public void setWtitTotPreSoloRshNull(String wtitTotPreSoloRshNull) {
        writeString(Pos.WTIT_TOT_PRE_SOLO_RSH_NULL, wtitTotPreSoloRshNull, Len.WTIT_TOT_PRE_SOLO_RSH_NULL);
    }

    /**Original name: WTIT-TOT-PRE-SOLO-RSH-NULL<br>*/
    public String getWtitTotPreSoloRshNull() {
        return readString(Pos.WTIT_TOT_PRE_SOLO_RSH_NULL, Len.WTIT_TOT_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_SOLO_RSH = 1;
        public static final int WTIT_TOT_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_SOLO_RSH = 8;
        public static final int WTIT_TOT_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
