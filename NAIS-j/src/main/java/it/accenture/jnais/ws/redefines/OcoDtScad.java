package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-DT-SCAD<br>
 * Variable: OCO-DT-SCAD from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_DT_SCAD;
    }

    public void setOcoDtScad(int ocoDtScad) {
        writeIntAsPacked(Pos.OCO_DT_SCAD, ocoDtScad, Len.Int.OCO_DT_SCAD);
    }

    public void setOcoDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_DT_SCAD, Pos.OCO_DT_SCAD);
    }

    /**Original name: OCO-DT-SCAD<br>*/
    public int getOcoDtScad() {
        return readPackedAsInt(Pos.OCO_DT_SCAD, Len.Int.OCO_DT_SCAD);
    }

    public byte[] getOcoDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_DT_SCAD, Pos.OCO_DT_SCAD);
        return buffer;
    }

    public void setOcoDtScadNull(String ocoDtScadNull) {
        writeString(Pos.OCO_DT_SCAD_NULL, ocoDtScadNull, Len.OCO_DT_SCAD_NULL);
    }

    /**Original name: OCO-DT-SCAD-NULL<br>*/
    public String getOcoDtScadNull() {
        return readString(Pos.OCO_DT_SCAD_NULL, Len.OCO_DT_SCAD_NULL);
    }

    public String getOcoDtScadNullFormatted() {
        return Functions.padBlanks(getOcoDtScadNull(), Len.OCO_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_DT_SCAD = 1;
        public static final int OCO_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_DT_SCAD = 5;
        public static final int OCO_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
