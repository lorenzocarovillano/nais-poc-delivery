package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-SOGL-AML-PRE-SAV-R<br>
 * Variable: WPCO-SOGL-AML-PRE-SAV-R from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoSoglAmlPreSavR extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoSoglAmlPreSavR() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_SOGL_AML_PRE_SAV_R;
    }

    public void setWpcoSoglAmlPreSavR(AfDecimal wpcoSoglAmlPreSavR) {
        writeDecimalAsPacked(Pos.WPCO_SOGL_AML_PRE_SAV_R, wpcoSoglAmlPreSavR.copy());
    }

    public void setDpcoSoglAmlPreSavRFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_SOGL_AML_PRE_SAV_R, Pos.WPCO_SOGL_AML_PRE_SAV_R);
    }

    /**Original name: WPCO-SOGL-AML-PRE-SAV-R<br>*/
    public AfDecimal getWpcoSoglAmlPreSavR() {
        return readPackedAsDecimal(Pos.WPCO_SOGL_AML_PRE_SAV_R, Len.Int.WPCO_SOGL_AML_PRE_SAV_R, Len.Fract.WPCO_SOGL_AML_PRE_SAV_R);
    }

    public byte[] getWpcoSoglAmlPreSavRAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_SOGL_AML_PRE_SAV_R, Pos.WPCO_SOGL_AML_PRE_SAV_R);
        return buffer;
    }

    public void setWpcoSoglAmlPreSavRNull(String wpcoSoglAmlPreSavRNull) {
        writeString(Pos.WPCO_SOGL_AML_PRE_SAV_R_NULL, wpcoSoglAmlPreSavRNull, Len.WPCO_SOGL_AML_PRE_SAV_R_NULL);
    }

    /**Original name: WPCO-SOGL-AML-PRE-SAV-R-NULL<br>*/
    public String getWpcoSoglAmlPreSavRNull() {
        return readString(Pos.WPCO_SOGL_AML_PRE_SAV_R_NULL, Len.WPCO_SOGL_AML_PRE_SAV_R_NULL);
    }

    public String getWpcoSoglAmlPreSavRNullFormatted() {
        return Functions.padBlanks(getWpcoSoglAmlPreSavRNull(), Len.WPCO_SOGL_AML_PRE_SAV_R_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_SOGL_AML_PRE_SAV_R = 1;
        public static final int WPCO_SOGL_AML_PRE_SAV_R_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_SOGL_AML_PRE_SAV_R = 8;
        public static final int WPCO_SOGL_AML_PRE_SAV_R_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_SOGL_AML_PRE_SAV_R = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_SOGL_AML_PRE_SAV_R = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
