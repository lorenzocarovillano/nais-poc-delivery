package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-IMP-AZ<br>
 * Variable: DTC-IMP-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_IMP_AZ;
    }

    public void setDtcImpAz(AfDecimal dtcImpAz) {
        writeDecimalAsPacked(Pos.DTC_IMP_AZ, dtcImpAz.copy());
    }

    public void setDtcImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_IMP_AZ, Pos.DTC_IMP_AZ);
    }

    /**Original name: DTC-IMP-AZ<br>*/
    public AfDecimal getDtcImpAz() {
        return readPackedAsDecimal(Pos.DTC_IMP_AZ, Len.Int.DTC_IMP_AZ, Len.Fract.DTC_IMP_AZ);
    }

    public byte[] getDtcImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_IMP_AZ, Pos.DTC_IMP_AZ);
        return buffer;
    }

    public void setDtcImpAzNull(String dtcImpAzNull) {
        writeString(Pos.DTC_IMP_AZ_NULL, dtcImpAzNull, Len.DTC_IMP_AZ_NULL);
    }

    /**Original name: DTC-IMP-AZ-NULL<br>*/
    public String getDtcImpAzNull() {
        return readString(Pos.DTC_IMP_AZ_NULL, Len.DTC_IMP_AZ_NULL);
    }

    public String getDtcImpAzNullFormatted() {
        return Functions.padBlanks(getDtcImpAzNull(), Len.DTC_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_IMP_AZ = 1;
        public static final int DTC_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_IMP_AZ = 8;
        public static final int DTC_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
