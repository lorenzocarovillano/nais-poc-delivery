package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-TFR-STRC<br>
 * Variable: TGA-IMP-TFR-STRC from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_TFR_STRC;
    }

    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        writeDecimalAsPacked(Pos.TGA_IMP_TFR_STRC, tgaImpTfrStrc.copy());
    }

    public void setTgaImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_TFR_STRC, Pos.TGA_IMP_TFR_STRC);
    }

    /**Original name: TGA-IMP-TFR-STRC<br>*/
    public AfDecimal getTgaImpTfrStrc() {
        return readPackedAsDecimal(Pos.TGA_IMP_TFR_STRC, Len.Int.TGA_IMP_TFR_STRC, Len.Fract.TGA_IMP_TFR_STRC);
    }

    public byte[] getTgaImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_TFR_STRC, Pos.TGA_IMP_TFR_STRC);
        return buffer;
    }

    public void setTgaImpTfrStrcNull(String tgaImpTfrStrcNull) {
        writeString(Pos.TGA_IMP_TFR_STRC_NULL, tgaImpTfrStrcNull, Len.TGA_IMP_TFR_STRC_NULL);
    }

    /**Original name: TGA-IMP-TFR-STRC-NULL<br>*/
    public String getTgaImpTfrStrcNull() {
        return readString(Pos.TGA_IMP_TFR_STRC_NULL, Len.TGA_IMP_TFR_STRC_NULL);
    }

    public String getTgaImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getTgaImpTfrStrcNull(), Len.TGA_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_TFR_STRC = 1;
        public static final int TGA_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_TFR_STRC = 8;
        public static final int TGA_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
