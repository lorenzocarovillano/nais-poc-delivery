package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RAN-DT-NASC<br>
 * Variable: RAN-DT-NASC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RanDtNasc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RanDtNasc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAN_DT_NASC;
    }

    public void setRanDtNasc(int ranDtNasc) {
        writeIntAsPacked(Pos.RAN_DT_NASC, ranDtNasc, Len.Int.RAN_DT_NASC);
    }

    public void setRanDtNascFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RAN_DT_NASC, Pos.RAN_DT_NASC);
    }

    /**Original name: RAN-DT-NASC<br>*/
    public int getRanDtNasc() {
        return readPackedAsInt(Pos.RAN_DT_NASC, Len.Int.RAN_DT_NASC);
    }

    public byte[] getRanDtNascAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RAN_DT_NASC, Pos.RAN_DT_NASC);
        return buffer;
    }

    public void setRanDtNascNull(String ranDtNascNull) {
        writeString(Pos.RAN_DT_NASC_NULL, ranDtNascNull, Len.RAN_DT_NASC_NULL);
    }

    /**Original name: RAN-DT-NASC-NULL<br>*/
    public String getRanDtNascNull() {
        return readString(Pos.RAN_DT_NASC_NULL, Len.RAN_DT_NASC_NULL);
    }

    public String getRanDtNascNullFormatted() {
        return Functions.padBlanks(getRanDtNascNull(), Len.RAN_DT_NASC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RAN_DT_NASC = 1;
        public static final int RAN_DT_NASC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_DT_NASC = 5;
        public static final int RAN_DT_NASC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_DT_NASC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
