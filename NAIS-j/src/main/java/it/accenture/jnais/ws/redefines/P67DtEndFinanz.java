package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-END-FINANZ<br>
 * Variable: P67-DT-END-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtEndFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtEndFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_END_FINANZ;
    }

    public void setP67DtEndFinanz(int p67DtEndFinanz) {
        writeIntAsPacked(Pos.P67_DT_END_FINANZ, p67DtEndFinanz, Len.Int.P67_DT_END_FINANZ);
    }

    public void setP67DtEndFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_END_FINANZ, Pos.P67_DT_END_FINANZ);
    }

    /**Original name: P67-DT-END-FINANZ<br>*/
    public int getP67DtEndFinanz() {
        return readPackedAsInt(Pos.P67_DT_END_FINANZ, Len.Int.P67_DT_END_FINANZ);
    }

    public byte[] getP67DtEndFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_END_FINANZ, Pos.P67_DT_END_FINANZ);
        return buffer;
    }

    public void setP67DtEndFinanzNull(String p67DtEndFinanzNull) {
        writeString(Pos.P67_DT_END_FINANZ_NULL, p67DtEndFinanzNull, Len.P67_DT_END_FINANZ_NULL);
    }

    /**Original name: P67-DT-END-FINANZ-NULL<br>*/
    public String getP67DtEndFinanzNull() {
        return readString(Pos.P67_DT_END_FINANZ_NULL, Len.P67_DT_END_FINANZ_NULL);
    }

    public String getP67DtEndFinanzNullFormatted() {
        return Functions.padBlanks(getP67DtEndFinanzNull(), Len.P67_DT_END_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_END_FINANZ = 1;
        public static final int P67_DT_END_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_END_FINANZ = 5;
        public static final int P67_DT_END_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_END_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
