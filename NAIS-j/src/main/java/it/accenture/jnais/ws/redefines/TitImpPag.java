package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-PAG<br>
 * Variable: TIT-IMP-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_PAG;
    }

    public void setTitImpPag(AfDecimal titImpPag) {
        writeDecimalAsPacked(Pos.TIT_IMP_PAG, titImpPag.copy());
    }

    public void setTitImpPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_PAG, Pos.TIT_IMP_PAG);
    }

    /**Original name: TIT-IMP-PAG<br>*/
    public AfDecimal getTitImpPag() {
        return readPackedAsDecimal(Pos.TIT_IMP_PAG, Len.Int.TIT_IMP_PAG, Len.Fract.TIT_IMP_PAG);
    }

    public byte[] getTitImpPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_PAG, Pos.TIT_IMP_PAG);
        return buffer;
    }

    public void setTitImpPagNull(String titImpPagNull) {
        writeString(Pos.TIT_IMP_PAG_NULL, titImpPagNull, Len.TIT_IMP_PAG_NULL);
    }

    /**Original name: TIT-IMP-PAG-NULL<br>*/
    public String getTitImpPagNull() {
        return readString(Pos.TIT_IMP_PAG_NULL, Len.TIT_IMP_PAG_NULL);
    }

    public String getTitImpPagNullFormatted() {
        return Functions.padBlanks(getTitImpPagNull(), Len.TIT_IMP_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_PAG = 1;
        public static final int TIT_IMP_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_PAG = 8;
        public static final int TIT_IMP_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
