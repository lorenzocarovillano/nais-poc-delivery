package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: COM-DATA<br>
 * Variable: COM-DATA from program LCCS0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ComData {

    //==== PROPERTIES ====
    //Original name: FILLER-COM-DATA
    private FillerComData fillerComData = new FillerComData();
    //Original name: C-MM
    private short cMm = DefaultValues.SHORT_VAL;
    //Original name: C-GG
    private String cGg = DefaultValues.stringVal(Len.C_GG);

    //==== METHODS ====
    public void setComDataBytes(byte[] buffer) {
        setComDataBytes(buffer, 1);
    }

    public byte[] getComDataBytes() {
        byte[] buffer = new byte[Len.COM_DATA];
        return getComDataBytes(buffer, 1);
    }

    public void setComDataBytes(byte[] buffer, int offset) {
        int position = offset;
        fillerComData.setFillerComDataBytes(buffer, position);
        position += FillerComData.Len.FILLER_COM_DATA;
        cMm = MarshalByte.readShort(buffer, position, Len.C_MM, SignType.NO_SIGN);
        position += Len.C_MM;
        cGg = MarshalByte.readFixedString(buffer, position, Len.C_GG);
    }

    public byte[] getComDataBytes(byte[] buffer, int offset) {
        int position = offset;
        fillerComData.getFillerComDataBytes(buffer, position);
        position += FillerComData.Len.FILLER_COM_DATA;
        MarshalByte.writeShort(buffer, position, cMm, Len.C_MM, SignType.NO_SIGN);
        position += Len.C_MM;
        MarshalByte.writeString(buffer, position, cGg, Len.C_GG);
        return buffer;
    }

    public void setcMm(short cMm) {
        this.cMm = cMm;
    }

    public short getcMm() {
        return this.cMm;
    }

    public void setcGg(short cGg) {
        this.cGg = NumericDisplay.asString(cGg, Len.C_GG);
    }

    public void setcGgFormatted(String cGg) {
        this.cGg = Trunc.toUnsignedNumeric(cGg, Len.C_GG);
    }

    public short getcGg() {
        return NumericDisplay.asShort(this.cGg);
    }

    public FillerComData getFillerComData() {
        return fillerComData;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int C_GG = 2;
        public static final int C_MM = 2;
        public static final int COM_DATA = C_MM + C_GG + FillerComData.Len.FILLER_COM_DATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
