package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL23-SOM-PRE-VINPG<br>
 * Variable: WL23-SOM-PRE-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23SomPreVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23SomPreVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_SOM_PRE_VINPG;
    }

    public void setWl23SomPreVinpg(AfDecimal wl23SomPreVinpg) {
        writeDecimalAsPacked(Pos.WL23_SOM_PRE_VINPG, wl23SomPreVinpg.copy());
    }

    public void setWl23SomPreVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_SOM_PRE_VINPG, Pos.WL23_SOM_PRE_VINPG);
    }

    /**Original name: WL23-SOM-PRE-VINPG<br>*/
    public AfDecimal getWl23SomPreVinpg() {
        return readPackedAsDecimal(Pos.WL23_SOM_PRE_VINPG, Len.Int.WL23_SOM_PRE_VINPG, Len.Fract.WL23_SOM_PRE_VINPG);
    }

    public byte[] getWl23SomPreVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_SOM_PRE_VINPG, Pos.WL23_SOM_PRE_VINPG);
        return buffer;
    }

    public void initWl23SomPreVinpgSpaces() {
        fill(Pos.WL23_SOM_PRE_VINPG, Len.WL23_SOM_PRE_VINPG, Types.SPACE_CHAR);
    }

    public void setWl23SomPreVinpgNull(String wl23SomPreVinpgNull) {
        writeString(Pos.WL23_SOM_PRE_VINPG_NULL, wl23SomPreVinpgNull, Len.WL23_SOM_PRE_VINPG_NULL);
    }

    /**Original name: WL23-SOM-PRE-VINPG-NULL<br>*/
    public String getWl23SomPreVinpgNull() {
        return readString(Pos.WL23_SOM_PRE_VINPG_NULL, Len.WL23_SOM_PRE_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_SOM_PRE_VINPG = 1;
        public static final int WL23_SOM_PRE_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_SOM_PRE_VINPG = 8;
        public static final int WL23_SOM_PRE_VINPG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WL23_SOM_PRE_VINPG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_SOM_PRE_VINPG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
