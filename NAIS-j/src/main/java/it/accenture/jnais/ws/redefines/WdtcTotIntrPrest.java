package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-TOT-INTR-PREST<br>
 * Variable: WDTC-TOT-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_TOT_INTR_PREST;
    }

    public void setWdtcTotIntrPrest(AfDecimal wdtcTotIntrPrest) {
        writeDecimalAsPacked(Pos.WDTC_TOT_INTR_PREST, wdtcTotIntrPrest.copy());
    }

    public void setWdtcTotIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_TOT_INTR_PREST, Pos.WDTC_TOT_INTR_PREST);
    }

    /**Original name: WDTC-TOT-INTR-PREST<br>*/
    public AfDecimal getWdtcTotIntrPrest() {
        return readPackedAsDecimal(Pos.WDTC_TOT_INTR_PREST, Len.Int.WDTC_TOT_INTR_PREST, Len.Fract.WDTC_TOT_INTR_PREST);
    }

    public byte[] getWdtcTotIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_TOT_INTR_PREST, Pos.WDTC_TOT_INTR_PREST);
        return buffer;
    }

    public void initWdtcTotIntrPrestSpaces() {
        fill(Pos.WDTC_TOT_INTR_PREST, Len.WDTC_TOT_INTR_PREST, Types.SPACE_CHAR);
    }

    public void setWdtcTotIntrPrestNull(String wdtcTotIntrPrestNull) {
        writeString(Pos.WDTC_TOT_INTR_PREST_NULL, wdtcTotIntrPrestNull, Len.WDTC_TOT_INTR_PREST_NULL);
    }

    /**Original name: WDTC-TOT-INTR-PREST-NULL<br>*/
    public String getWdtcTotIntrPrestNull() {
        return readString(Pos.WDTC_TOT_INTR_PREST_NULL, Len.WDTC_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_TOT_INTR_PREST = 1;
        public static final int WDTC_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_TOT_INTR_PREST = 8;
        public static final int WDTC_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
