package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-ACQ-1O-ANNO<br>
 * Variable: WPAG-RATA-ACQ-1O-ANNO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataAcq1oAnno extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataAcq1oAnno() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_ACQ1O_ANNO;
    }

    public void setWpagRataAcq1oAnno(AfDecimal wpagRataAcq1oAnno) {
        writeDecimalAsPacked(Pos.WPAG_RATA_ACQ1O_ANNO, wpagRataAcq1oAnno.copy());
    }

    public void setWpagRataAcq1oAnnoFormatted(String wpagRataAcq1oAnno) {
        setWpagRataAcq1oAnno(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_ACQ1O_ANNO + Len.Fract.WPAG_RATA_ACQ1O_ANNO, Len.Fract.WPAG_RATA_ACQ1O_ANNO, wpagRataAcq1oAnno));
    }

    public void setWpagRataAcq1oAnnoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_ACQ1O_ANNO, Pos.WPAG_RATA_ACQ1O_ANNO);
    }

    /**Original name: WPAG-RATA-ACQ-1O-ANNO<br>*/
    public AfDecimal getWpagRataAcq1oAnno() {
        return readPackedAsDecimal(Pos.WPAG_RATA_ACQ1O_ANNO, Len.Int.WPAG_RATA_ACQ1O_ANNO, Len.Fract.WPAG_RATA_ACQ1O_ANNO);
    }

    public byte[] getWpagRataAcq1oAnnoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_ACQ1O_ANNO, Pos.WPAG_RATA_ACQ1O_ANNO);
        return buffer;
    }

    public void initWpagRataAcq1oAnnoSpaces() {
        fill(Pos.WPAG_RATA_ACQ1O_ANNO, Len.WPAG_RATA_ACQ1O_ANNO, Types.SPACE_CHAR);
    }

    public void setWpagRataAcq1oAnnoNull(String wpagRataAcq1oAnnoNull) {
        writeString(Pos.WPAG_RATA_ACQ1O_ANNO_NULL, wpagRataAcq1oAnnoNull, Len.WPAG_RATA_ACQ1O_ANNO_NULL);
    }

    /**Original name: WPAG-RATA-ACQ-1O-ANNO-NULL<br>*/
    public String getWpagRataAcq1oAnnoNull() {
        return readString(Pos.WPAG_RATA_ACQ1O_ANNO_NULL, Len.WPAG_RATA_ACQ1O_ANNO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ACQ1O_ANNO = 1;
        public static final int WPAG_RATA_ACQ1O_ANNO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ACQ1O_ANNO = 8;
        public static final int WPAG_RATA_ACQ1O_ANNO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ACQ1O_ANNO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ACQ1O_ANNO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
