package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-MOV-ELE<br>
 * Variables: WK-MOV-ELE from program LOAS0820<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkMovEle {

    //==== PROPERTIES ====
    //Original name: WK-TP-MOVI
    private String tpMovi = DefaultValues.stringVal(Len.TP_MOVI);
    //Original name: WK-DT-EFF
    private int dtEff = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setTpMovi(String tpMovi) {
        this.tpMovi = Functions.subString(tpMovi, Len.TP_MOVI);
    }

    public String getTpMovi() {
        return this.tpMovi;
    }

    public void setDtEff(int dtEff) {
        this.dtEff = dtEff;
    }

    public int getDtEff() {
        return this.dtEff;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_MOVI = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
