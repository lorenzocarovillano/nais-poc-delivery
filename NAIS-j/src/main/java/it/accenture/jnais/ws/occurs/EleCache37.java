package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ELE-CACHE-37<br>
 * Variables: ELE-CACHE-37 from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class EleCache37 {

    //==== PROPERTIES ====
    //Original name: C37-COD-FND
    private String codFnd = DefaultValues.stringVal(Len.COD_FND);
    //Original name: C37-NUM-QUO-PREC
    private AfDecimal numQuoPrec = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DISINV-RISC-PARZ
    private AfDecimal numQuoDisinvRiscParz = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DIS-RISC-PARZ-PRG
    private AfDecimal numQuoDisRiscParzPrg = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DISINV-IMPOS-SOST
    private AfDecimal numQuoDisinvImposSost = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DISINV-COMMGES
    private AfDecimal numQuoDisinvCommges = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DISINV-PREL-COSTI
    private AfDecimal numQuoDisinvPrelCosti = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DISINV-SWITCH
    private AfDecimal numQuoDisinvSwitch = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-INVST-SWITCH
    private AfDecimal numQuoInvstSwitch = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-INVST-VERS
    private AfDecimal numQuoInvstVers = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-INVST-REBATE
    private AfDecimal numQuoInvstRebate = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-INVST-PURO-RISC
    private AfDecimal numQuoInvstPuroRisc = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-ATTUALE
    private AfDecimal numQuoAttuale = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-DISINV-COMP-NAV
    private AfDecimal numQuoDisinvCompNav = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: C37-NUM-QUO-INVST-COMP-NAV
    private AfDecimal numQuoInvstCompNav = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);

    //==== METHODS ====
    public void setCodFnd(String codFnd) {
        this.codFnd = Functions.subString(codFnd, Len.COD_FND);
    }

    public String getCodFnd() {
        return this.codFnd;
    }

    public void setNumQuoPrec(AfDecimal numQuoPrec) {
        this.numQuoPrec.assign(numQuoPrec);
    }

    public AfDecimal getNumQuoPrec() {
        return this.numQuoPrec.copy();
    }

    public void setNumQuoDisinvRiscParz(AfDecimal numQuoDisinvRiscParz) {
        this.numQuoDisinvRiscParz.assign(numQuoDisinvRiscParz);
    }

    public AfDecimal getNumQuoDisinvRiscParz() {
        return this.numQuoDisinvRiscParz.copy();
    }

    public void setNumQuoDisRiscParzPrg(AfDecimal numQuoDisRiscParzPrg) {
        this.numQuoDisRiscParzPrg.assign(numQuoDisRiscParzPrg);
    }

    public AfDecimal getNumQuoDisRiscParzPrg() {
        return this.numQuoDisRiscParzPrg.copy();
    }

    public void setNumQuoDisinvImposSost(AfDecimal numQuoDisinvImposSost) {
        this.numQuoDisinvImposSost.assign(numQuoDisinvImposSost);
    }

    public AfDecimal getNumQuoDisinvImposSost() {
        return this.numQuoDisinvImposSost.copy();
    }

    public void setNumQuoDisinvCommges(AfDecimal numQuoDisinvCommges) {
        this.numQuoDisinvCommges.assign(numQuoDisinvCommges);
    }

    public AfDecimal getNumQuoDisinvCommges() {
        return this.numQuoDisinvCommges.copy();
    }

    public void setNumQuoDisinvPrelCosti(AfDecimal numQuoDisinvPrelCosti) {
        this.numQuoDisinvPrelCosti.assign(numQuoDisinvPrelCosti);
    }

    public AfDecimal getNumQuoDisinvPrelCosti() {
        return this.numQuoDisinvPrelCosti.copy();
    }

    public void setNumQuoDisinvSwitch(AfDecimal numQuoDisinvSwitch) {
        this.numQuoDisinvSwitch.assign(numQuoDisinvSwitch);
    }

    public AfDecimal getNumQuoDisinvSwitch() {
        return this.numQuoDisinvSwitch.copy();
    }

    public void setNumQuoInvstSwitch(AfDecimal numQuoInvstSwitch) {
        this.numQuoInvstSwitch.assign(numQuoInvstSwitch);
    }

    public AfDecimal getNumQuoInvstSwitch() {
        return this.numQuoInvstSwitch.copy();
    }

    public void setNumQuoInvstVers(AfDecimal numQuoInvstVers) {
        this.numQuoInvstVers.assign(numQuoInvstVers);
    }

    public AfDecimal getNumQuoInvstVers() {
        return this.numQuoInvstVers.copy();
    }

    public void setNumQuoInvstRebate(AfDecimal numQuoInvstRebate) {
        this.numQuoInvstRebate.assign(numQuoInvstRebate);
    }

    public AfDecimal getNumQuoInvstRebate() {
        return this.numQuoInvstRebate.copy();
    }

    public void setNumQuoInvstPuroRisc(AfDecimal numQuoInvstPuroRisc) {
        this.numQuoInvstPuroRisc.assign(numQuoInvstPuroRisc);
    }

    public AfDecimal getNumQuoInvstPuroRisc() {
        return this.numQuoInvstPuroRisc.copy();
    }

    public void setNumQuoAttuale(AfDecimal numQuoAttuale) {
        this.numQuoAttuale.assign(numQuoAttuale);
    }

    public AfDecimal getNumQuoAttuale() {
        return this.numQuoAttuale.copy();
    }

    public void setNumQuoDisinvCompNav(AfDecimal numQuoDisinvCompNav) {
        this.numQuoDisinvCompNav.assign(numQuoDisinvCompNav);
    }

    public AfDecimal getNumQuoDisinvCompNav() {
        return this.numQuoDisinvCompNav.copy();
    }

    public void setNumQuoInvstCompNav(AfDecimal numQuoInvstCompNav) {
        this.numQuoInvstCompNav.assign(numQuoInvstCompNav);
    }

    public AfDecimal getNumQuoInvstCompNav() {
        return this.numQuoInvstCompNav.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
