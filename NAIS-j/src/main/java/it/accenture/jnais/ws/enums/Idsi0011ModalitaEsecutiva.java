package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSI0011-MODALITA-ESECUTIVA<br>
 * Variable: IDSI0011-MODALITA-ESECUTIVA from copybook IDSI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0011ModalitaEsecutiva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ON_LINE = 'O';
    public static final char BATCH = 'B';
    public static final char BATCH_INFR = 'I';

    //==== METHODS ====
    public void setIdsi0011ModalitaEsecutiva(char idsi0011ModalitaEsecutiva) {
        this.value = idsi0011ModalitaEsecutiva;
    }

    public char getIdsi0011ModalitaEsecutiva() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSI0011_MODALITA_ESECUTIVA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
