package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-DT-RICH<br>
 * Variable: S089-DT-RICH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089DtRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_DT_RICH;
    }

    public void setWlquDtRich(int wlquDtRich) {
        writeIntAsPacked(Pos.S089_DT_RICH, wlquDtRich, Len.Int.WLQU_DT_RICH);
    }

    public void setWlquDtRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_DT_RICH, Pos.S089_DT_RICH);
    }

    /**Original name: WLQU-DT-RICH<br>*/
    public int getWlquDtRich() {
        return readPackedAsInt(Pos.S089_DT_RICH, Len.Int.WLQU_DT_RICH);
    }

    public byte[] getWlquDtRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_DT_RICH, Pos.S089_DT_RICH);
        return buffer;
    }

    public void initWlquDtRichSpaces() {
        fill(Pos.S089_DT_RICH, Len.S089_DT_RICH, Types.SPACE_CHAR);
    }

    public void setWlquDtRichNull(String wlquDtRichNull) {
        writeString(Pos.S089_DT_RICH_NULL, wlquDtRichNull, Len.WLQU_DT_RICH_NULL);
    }

    /**Original name: WLQU-DT-RICH-NULL<br>*/
    public String getWlquDtRichNull() {
        return readString(Pos.S089_DT_RICH_NULL, Len.WLQU_DT_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_DT_RICH = 1;
        public static final int S089_DT_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_DT_RICH = 5;
        public static final int WLQU_DT_RICH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_DT_RICH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
