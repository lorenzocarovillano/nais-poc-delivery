package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0501-POS-SEGNO<br>
 * Variable: IDSV0501-POS-SEGNO from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0501PosSegno {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ANTERIORE = 'A';
    public static final char POSTERIORE = 'P';

    //==== METHODS ====
    public void setIdsv0501PosSegno(char idsv0501PosSegno) {
        this.value = idsv0501PosSegno;
    }

    public char getIdsv0501PosSegno() {
        return this.value;
    }

    public boolean isIdsv0501SegnoAnteriore() {
        return value == ANTERIORE;
    }

    public void setIdsv0501SegnoAnteriore() {
        value = ANTERIORE;
    }

    public boolean isIdsv0501SegnoPosteriore() {
        return value == POSTERIORE;
    }
}
