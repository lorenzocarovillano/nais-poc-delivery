package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-SOST-K3<br>
 * Variable: WDFA-IMPST-SOST-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstSostK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstSostK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_SOST_K3;
    }

    public void setWdfaImpstSostK3(AfDecimal wdfaImpstSostK3) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_SOST_K3, wdfaImpstSostK3.copy());
    }

    public void setWdfaImpstSostK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_SOST_K3, Pos.WDFA_IMPST_SOST_K3);
    }

    /**Original name: WDFA-IMPST-SOST-K3<br>*/
    public AfDecimal getWdfaImpstSostK3() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_SOST_K3, Len.Int.WDFA_IMPST_SOST_K3, Len.Fract.WDFA_IMPST_SOST_K3);
    }

    public byte[] getWdfaImpstSostK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_SOST_K3, Pos.WDFA_IMPST_SOST_K3);
        return buffer;
    }

    public void setWdfaImpstSostK3Null(String wdfaImpstSostK3Null) {
        writeString(Pos.WDFA_IMPST_SOST_K3_NULL, wdfaImpstSostK3Null, Len.WDFA_IMPST_SOST_K3_NULL);
    }

    /**Original name: WDFA-IMPST-SOST-K3-NULL<br>*/
    public String getWdfaImpstSostK3Null() {
        return readString(Pos.WDFA_IMPST_SOST_K3_NULL, Len.WDFA_IMPST_SOST_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K3 = 1;
        public static final int WDFA_IMPST_SOST_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K3 = 8;
        public static final int WDFA_IMPST_SOST_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
