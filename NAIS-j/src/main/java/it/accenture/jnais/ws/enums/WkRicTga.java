package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-RIC-TGA<br>
 * Variable: WK-RIC-TGA from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkRicTga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATA = 'S';
    public static final char NON_TROVATA = 'N';

    //==== METHODS ====
    public void setWkRicTga(char wkRicTga) {
        this.value = wkRicTga;
    }

    public char getWkRicTga() {
        return this.value;
    }

    public boolean isTrovata() {
        return value == TROVATA;
    }

    public void setTrovata() {
        value = TROVATA;
    }

    public void setNonTrovata() {
        value = NON_TROVATA;
    }
}
