package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-CAR-INC<br>
 * Variable: WTGA-IMP-CAR-INC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_CAR_INC;
    }

    public void setWtgaImpCarInc(AfDecimal wtgaImpCarInc) {
        writeDecimalAsPacked(Pos.WTGA_IMP_CAR_INC, wtgaImpCarInc.copy());
    }

    public void setWtgaImpCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_CAR_INC, Pos.WTGA_IMP_CAR_INC);
    }

    /**Original name: WTGA-IMP-CAR-INC<br>*/
    public AfDecimal getWtgaImpCarInc() {
        return readPackedAsDecimal(Pos.WTGA_IMP_CAR_INC, Len.Int.WTGA_IMP_CAR_INC, Len.Fract.WTGA_IMP_CAR_INC);
    }

    public byte[] getWtgaImpCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_CAR_INC, Pos.WTGA_IMP_CAR_INC);
        return buffer;
    }

    public void initWtgaImpCarIncSpaces() {
        fill(Pos.WTGA_IMP_CAR_INC, Len.WTGA_IMP_CAR_INC, Types.SPACE_CHAR);
    }

    public void setWtgaImpCarIncNull(String wtgaImpCarIncNull) {
        writeString(Pos.WTGA_IMP_CAR_INC_NULL, wtgaImpCarIncNull, Len.WTGA_IMP_CAR_INC_NULL);
    }

    /**Original name: WTGA-IMP-CAR-INC-NULL<br>*/
    public String getWtgaImpCarIncNull() {
        return readString(Pos.WTGA_IMP_CAR_INC_NULL, Len.WTGA_IMP_CAR_INC_NULL);
    }

    public String getWtgaImpCarIncNullFormatted() {
        return Functions.padBlanks(getWtgaImpCarIncNull(), Len.WTGA_IMP_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_CAR_INC = 1;
        public static final int WTGA_IMP_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_CAR_INC = 8;
        public static final int WTGA_IMP_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
