package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: BENEFICIARIO-CEDOLA<br>
 * Variable: BENEFICIARIO-CEDOLA from program LVES0270<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class BeneficiarioCedola {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NO = 'N';
    public static final char SI = 'S';

    //==== METHODS ====
    public void setBeneficiarioCedola(char beneficiarioCedola) {
        this.value = beneficiarioCedola;
    }

    public char getBeneficiarioCedola() {
        return this.value;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }

    public void setSi() {
        value = SI;
    }
}
