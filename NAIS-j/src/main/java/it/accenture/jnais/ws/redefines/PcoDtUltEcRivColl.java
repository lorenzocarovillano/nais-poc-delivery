package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-RIV-COLL<br>
 * Variable: PCO-DT-ULT-EC-RIV-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcRivColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcRivColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_RIV_COLL;
    }

    public void setPcoDtUltEcRivColl(int pcoDtUltEcRivColl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_RIV_COLL, pcoDtUltEcRivColl, Len.Int.PCO_DT_ULT_EC_RIV_COLL);
    }

    public void setPcoDtUltEcRivCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_RIV_COLL, Pos.PCO_DT_ULT_EC_RIV_COLL);
    }

    /**Original name: PCO-DT-ULT-EC-RIV-COLL<br>*/
    public int getPcoDtUltEcRivColl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_RIV_COLL, Len.Int.PCO_DT_ULT_EC_RIV_COLL);
    }

    public byte[] getPcoDtUltEcRivCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_RIV_COLL, Pos.PCO_DT_ULT_EC_RIV_COLL);
        return buffer;
    }

    public void initPcoDtUltEcRivCollHighValues() {
        fill(Pos.PCO_DT_ULT_EC_RIV_COLL, Len.PCO_DT_ULT_EC_RIV_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcRivCollNull(String pcoDtUltEcRivCollNull) {
        writeString(Pos.PCO_DT_ULT_EC_RIV_COLL_NULL, pcoDtUltEcRivCollNull, Len.PCO_DT_ULT_EC_RIV_COLL_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-RIV-COLL-NULL<br>*/
    public String getPcoDtUltEcRivCollNull() {
        return readString(Pos.PCO_DT_ULT_EC_RIV_COLL_NULL, Len.PCO_DT_ULT_EC_RIV_COLL_NULL);
    }

    public String getPcoDtUltEcRivCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcRivCollNull(), Len.PCO_DT_ULT_EC_RIV_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_RIV_COLL = 1;
        public static final int PCO_DT_ULT_EC_RIV_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_RIV_COLL = 5;
        public static final int PCO_DT_ULT_EC_RIV_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_RIV_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
