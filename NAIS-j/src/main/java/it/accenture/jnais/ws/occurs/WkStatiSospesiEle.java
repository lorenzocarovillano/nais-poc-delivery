package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-STATI-SOSPESI-ELE<br>
 * Variables: WK-STATI-SOSPESI-ELE from copybook IABV0007<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkStatiSospesiEle {

    //==== PROPERTIES ====
    //Original name: WK-STATI-SOSP-PK
    private long wkStatiSospPk = DefaultValues.LONG_VAL;

    //==== METHODS ====
    public void setWkStatiSospPk(long wkStatiSospPk) {
        this.wkStatiSospPk = wkStatiSospPk;
    }

    public long getWkStatiSospPk() {
        return this.wkStatiSospPk;
    }
}
