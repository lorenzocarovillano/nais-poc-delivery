package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ETA-MM-2O-ASSTO<br>
 * Variable: WTGA-ETA-MM-2O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaEtaMm2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaEtaMm2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ETA_MM2O_ASSTO;
    }

    public void setWtgaEtaMm2oAssto(short wtgaEtaMm2oAssto) {
        writeShortAsPacked(Pos.WTGA_ETA_MM2O_ASSTO, wtgaEtaMm2oAssto, Len.Int.WTGA_ETA_MM2O_ASSTO);
    }

    public void setWtgaEtaMm2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ETA_MM2O_ASSTO, Pos.WTGA_ETA_MM2O_ASSTO);
    }

    /**Original name: WTGA-ETA-MM-2O-ASSTO<br>*/
    public short getWtgaEtaMm2oAssto() {
        return readPackedAsShort(Pos.WTGA_ETA_MM2O_ASSTO, Len.Int.WTGA_ETA_MM2O_ASSTO);
    }

    public byte[] getWtgaEtaMm2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ETA_MM2O_ASSTO, Pos.WTGA_ETA_MM2O_ASSTO);
        return buffer;
    }

    public void initWtgaEtaMm2oAsstoSpaces() {
        fill(Pos.WTGA_ETA_MM2O_ASSTO, Len.WTGA_ETA_MM2O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWtgaEtaMm2oAsstoNull(String wtgaEtaMm2oAsstoNull) {
        writeString(Pos.WTGA_ETA_MM2O_ASSTO_NULL, wtgaEtaMm2oAsstoNull, Len.WTGA_ETA_MM2O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-MM-2O-ASSTO-NULL<br>*/
    public String getWtgaEtaMm2oAsstoNull() {
        return readString(Pos.WTGA_ETA_MM2O_ASSTO_NULL, Len.WTGA_ETA_MM2O_ASSTO_NULL);
    }

    public String getWtgaEtaMm2oAsstoNullFormatted() {
        return Functions.padBlanks(getWtgaEtaMm2oAsstoNull(), Len.WTGA_ETA_MM2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_MM2O_ASSTO = 1;
        public static final int WTGA_ETA_MM2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_MM2O_ASSTO = 2;
        public static final int WTGA_ETA_MM2O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ETA_MM2O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
