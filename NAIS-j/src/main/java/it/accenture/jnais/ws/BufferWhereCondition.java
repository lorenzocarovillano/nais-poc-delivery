package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: BUFFER-WHERE-CONDITION<br>
 * Variable: BUFFER-WHERE-CONDITION from program LRGM0380<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class BufferWhereCondition {

    //==== PROPERTIES ====
    //Original name: WK-DT-RICOR-DA
    private String da = DefaultValues.stringVal(Len.DA);
    //Original name: WK-DT-RICOR-A
    private String a = DefaultValues.stringVal(Len.A);
    //Original name: FILLER-BUFFER-WHERE-CONDITION
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== METHODS ====
    public void setBufferWhereConditionFormatted(String data) {
        byte[] buffer = new byte[Len.BUFFER_WHERE_CONDITION];
        MarshalByte.writeString(buffer, 1, data, Len.BUFFER_WHERE_CONDITION);
        setBufferWhereConditionBytes(buffer, 1);
    }

    public void setBufferWhereConditionBytes(byte[] buffer, int offset) {
        int position = offset;
        da = MarshalByte.readFixedString(buffer, position, Len.DA);
        position += Len.DA;
        a = MarshalByte.readFixedString(buffer, position, Len.A);
        position += Len.A;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DA = 8;
        public static final int A = 8;
        public static final int FLR1 = 284;
        public static final int BUFFER_WHERE_CONDITION = DA + A + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
