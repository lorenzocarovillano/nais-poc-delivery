package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-ETA-AA-2O-ASSTO<br>
 * Variable: WGRZ-ETA-AA-2O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzEtaAa2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzEtaAa2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ETA_AA2O_ASSTO;
    }

    public void setWgrzEtaAa2oAssto(short wgrzEtaAa2oAssto) {
        writeShortAsPacked(Pos.WGRZ_ETA_AA2O_ASSTO, wgrzEtaAa2oAssto, Len.Int.WGRZ_ETA_AA2O_ASSTO);
    }

    public void setWgrzEtaAa2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ETA_AA2O_ASSTO, Pos.WGRZ_ETA_AA2O_ASSTO);
    }

    /**Original name: WGRZ-ETA-AA-2O-ASSTO<br>*/
    public short getWgrzEtaAa2oAssto() {
        return readPackedAsShort(Pos.WGRZ_ETA_AA2O_ASSTO, Len.Int.WGRZ_ETA_AA2O_ASSTO);
    }

    public byte[] getWgrzEtaAa2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ETA_AA2O_ASSTO, Pos.WGRZ_ETA_AA2O_ASSTO);
        return buffer;
    }

    public void initWgrzEtaAa2oAsstoSpaces() {
        fill(Pos.WGRZ_ETA_AA2O_ASSTO, Len.WGRZ_ETA_AA2O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzEtaAa2oAsstoNull(String wgrzEtaAa2oAsstoNull) {
        writeString(Pos.WGRZ_ETA_AA2O_ASSTO_NULL, wgrzEtaAa2oAsstoNull, Len.WGRZ_ETA_AA2O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ETA-AA-2O-ASSTO-NULL<br>*/
    public String getWgrzEtaAa2oAsstoNull() {
        return readString(Pos.WGRZ_ETA_AA2O_ASSTO_NULL, Len.WGRZ_ETA_AA2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_AA2O_ASSTO = 1;
        public static final int WGRZ_ETA_AA2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_AA2O_ASSTO = 2;
        public static final int WGRZ_ETA_AA2O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ETA_AA2O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
