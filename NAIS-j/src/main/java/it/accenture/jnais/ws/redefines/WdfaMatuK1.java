package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-MATU-K1<br>
 * Variable: WDFA-MATU-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMatuK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMatuK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MATU_K1;
    }

    public void setWdfaMatuK1(AfDecimal wdfaMatuK1) {
        writeDecimalAsPacked(Pos.WDFA_MATU_K1, wdfaMatuK1.copy());
    }

    public void setWdfaMatuK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MATU_K1, Pos.WDFA_MATU_K1);
    }

    /**Original name: WDFA-MATU-K1<br>*/
    public AfDecimal getWdfaMatuK1() {
        return readPackedAsDecimal(Pos.WDFA_MATU_K1, Len.Int.WDFA_MATU_K1, Len.Fract.WDFA_MATU_K1);
    }

    public byte[] getWdfaMatuK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MATU_K1, Pos.WDFA_MATU_K1);
        return buffer;
    }

    public void setWdfaMatuK1Null(String wdfaMatuK1Null) {
        writeString(Pos.WDFA_MATU_K1_NULL, wdfaMatuK1Null, Len.WDFA_MATU_K1_NULL);
    }

    /**Original name: WDFA-MATU-K1-NULL<br>*/
    public String getWdfaMatuK1Null() {
        return readString(Pos.WDFA_MATU_K1_NULL, Len.WDFA_MATU_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_K1 = 1;
        public static final int WDFA_MATU_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_K1 = 8;
        public static final int WDFA_MATU_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
