package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PRE-TOT<br>
 * Variable: WDTR-PRE-TOT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PRE_TOT;
    }

    public void setWdtrPreTot(AfDecimal wdtrPreTot) {
        writeDecimalAsPacked(Pos.WDTR_PRE_TOT, wdtrPreTot.copy());
    }

    /**Original name: WDTR-PRE-TOT<br>*/
    public AfDecimal getWdtrPreTot() {
        return readPackedAsDecimal(Pos.WDTR_PRE_TOT, Len.Int.WDTR_PRE_TOT, Len.Fract.WDTR_PRE_TOT);
    }

    public void setWdtrPreTotNull(String wdtrPreTotNull) {
        writeString(Pos.WDTR_PRE_TOT_NULL, wdtrPreTotNull, Len.WDTR_PRE_TOT_NULL);
    }

    /**Original name: WDTR-PRE-TOT-NULL<br>*/
    public String getWdtrPreTotNull() {
        return readString(Pos.WDTR_PRE_TOT_NULL, Len.WDTR_PRE_TOT_NULL);
    }

    public String getWdtrPreTotNullFormatted() {
        return Functions.padBlanks(getWdtrPreTotNull(), Len.WDTR_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_TOT = 1;
        public static final int WDTR_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PRE_TOT = 8;
        public static final int WDTR_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
