package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CPT-DT-STAB<br>
 * Variable: B03-CPT-DT-STAB from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CptDtStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CptDtStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CPT_DT_STAB;
    }

    public void setB03CptDtStab(AfDecimal b03CptDtStab) {
        writeDecimalAsPacked(Pos.B03_CPT_DT_STAB, b03CptDtStab.copy());
    }

    public void setB03CptDtStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CPT_DT_STAB, Pos.B03_CPT_DT_STAB);
    }

    /**Original name: B03-CPT-DT-STAB<br>*/
    public AfDecimal getB03CptDtStab() {
        return readPackedAsDecimal(Pos.B03_CPT_DT_STAB, Len.Int.B03_CPT_DT_STAB, Len.Fract.B03_CPT_DT_STAB);
    }

    public byte[] getB03CptDtStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CPT_DT_STAB, Pos.B03_CPT_DT_STAB);
        return buffer;
    }

    public void setB03CptDtStabNull(String b03CptDtStabNull) {
        writeString(Pos.B03_CPT_DT_STAB_NULL, b03CptDtStabNull, Len.B03_CPT_DT_STAB_NULL);
    }

    /**Original name: B03-CPT-DT-STAB-NULL<br>*/
    public String getB03CptDtStabNull() {
        return readString(Pos.B03_CPT_DT_STAB_NULL, Len.B03_CPT_DT_STAB_NULL);
    }

    public String getB03CptDtStabNullFormatted() {
        return Functions.padBlanks(getB03CptDtStabNull(), Len.B03_CPT_DT_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CPT_DT_STAB = 1;
        public static final int B03_CPT_DT_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CPT_DT_STAB = 8;
        public static final int B03_CPT_DT_STAB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CPT_DT_STAB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CPT_DT_STAB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
