package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PC-RIP-PRE<br>
 * Variable: L3421-PC-RIP-PRE from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PcRipPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PcRipPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PC_RIP_PRE;
    }

    public void setL3421PcRipPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PC_RIP_PRE, Pos.L3421_PC_RIP_PRE);
    }

    /**Original name: L3421-PC-RIP-PRE<br>*/
    public AfDecimal getL3421PcRipPre() {
        return readPackedAsDecimal(Pos.L3421_PC_RIP_PRE, Len.Int.L3421_PC_RIP_PRE, Len.Fract.L3421_PC_RIP_PRE);
    }

    public byte[] getL3421PcRipPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PC_RIP_PRE, Pos.L3421_PC_RIP_PRE);
        return buffer;
    }

    /**Original name: L3421-PC-RIP-PRE-NULL<br>*/
    public String getL3421PcRipPreNull() {
        return readString(Pos.L3421_PC_RIP_PRE_NULL, Len.L3421_PC_RIP_PRE_NULL);
    }

    public String getL3421PcRipPreNullFormatted() {
        return Functions.padBlanks(getL3421PcRipPreNull(), Len.L3421_PC_RIP_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PC_RIP_PRE = 1;
        public static final int L3421_PC_RIP_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PC_RIP_PRE = 4;
        public static final int L3421_PC_RIP_PRE_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PC_RIP_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
