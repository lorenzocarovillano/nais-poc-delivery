package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-IRPEF-K1-ANTI<br>
 * Variable: WDFA-IMPB-IRPEF-K1-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbIrpefK1Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbIrpefK1Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_IRPEF_K1_ANTI;
    }

    public void setWdfaImpbIrpefK1Anti(AfDecimal wdfaImpbIrpefK1Anti) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_IRPEF_K1_ANTI, wdfaImpbIrpefK1Anti.copy());
    }

    public void setWdfaImpbIrpefK1AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_IRPEF_K1_ANTI, Pos.WDFA_IMPB_IRPEF_K1_ANTI);
    }

    /**Original name: WDFA-IMPB-IRPEF-K1-ANTI<br>*/
    public AfDecimal getWdfaImpbIrpefK1Anti() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_IRPEF_K1_ANTI, Len.Int.WDFA_IMPB_IRPEF_K1_ANTI, Len.Fract.WDFA_IMPB_IRPEF_K1_ANTI);
    }

    public byte[] getWdfaImpbIrpefK1AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_IRPEF_K1_ANTI, Pos.WDFA_IMPB_IRPEF_K1_ANTI);
        return buffer;
    }

    public void setWdfaImpbIrpefK1AntiNull(String wdfaImpbIrpefK1AntiNull) {
        writeString(Pos.WDFA_IMPB_IRPEF_K1_ANTI_NULL, wdfaImpbIrpefK1AntiNull, Len.WDFA_IMPB_IRPEF_K1_ANTI_NULL);
    }

    /**Original name: WDFA-IMPB-IRPEF-K1-ANTI-NULL<br>*/
    public String getWdfaImpbIrpefK1AntiNull() {
        return readString(Pos.WDFA_IMPB_IRPEF_K1_ANTI_NULL, Len.WDFA_IMPB_IRPEF_K1_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IRPEF_K1_ANTI = 1;
        public static final int WDFA_IMPB_IRPEF_K1_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IRPEF_K1_ANTI = 8;
        public static final int WDFA_IMPB_IRPEF_K1_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IRPEF_K1_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IRPEF_K1_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
