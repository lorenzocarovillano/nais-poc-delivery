package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SOPR-PROF<br>
 * Variable: DTR-SOPR-PROF from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SOPR_PROF;
    }

    public void setDtrSoprProf(AfDecimal dtrSoprProf) {
        writeDecimalAsPacked(Pos.DTR_SOPR_PROF, dtrSoprProf.copy());
    }

    public void setDtrSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SOPR_PROF, Pos.DTR_SOPR_PROF);
    }

    /**Original name: DTR-SOPR-PROF<br>*/
    public AfDecimal getDtrSoprProf() {
        return readPackedAsDecimal(Pos.DTR_SOPR_PROF, Len.Int.DTR_SOPR_PROF, Len.Fract.DTR_SOPR_PROF);
    }

    public byte[] getDtrSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SOPR_PROF, Pos.DTR_SOPR_PROF);
        return buffer;
    }

    public void setDtrSoprProfNull(String dtrSoprProfNull) {
        writeString(Pos.DTR_SOPR_PROF_NULL, dtrSoprProfNull, Len.DTR_SOPR_PROF_NULL);
    }

    /**Original name: DTR-SOPR-PROF-NULL<br>*/
    public String getDtrSoprProfNull() {
        return readString(Pos.DTR_SOPR_PROF_NULL, Len.DTR_SOPR_PROF_NULL);
    }

    public String getDtrSoprProfNullFormatted() {
        return Functions.padBlanks(getDtrSoprProfNull(), Len.DTR_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_PROF = 1;
        public static final int DTR_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_PROF = 8;
        public static final int DTR_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
