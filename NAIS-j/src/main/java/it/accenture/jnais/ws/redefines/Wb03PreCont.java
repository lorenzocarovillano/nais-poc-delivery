package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-CONT<br>
 * Variable: WB03-PRE-CONT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreCont extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreCont() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_CONT;
    }

    public void setWb03PreCont(AfDecimal wb03PreCont) {
        writeDecimalAsPacked(Pos.WB03_PRE_CONT, wb03PreCont.copy());
    }

    public void setWb03PreContFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_CONT, Pos.WB03_PRE_CONT);
    }

    /**Original name: WB03-PRE-CONT<br>*/
    public AfDecimal getWb03PreCont() {
        return readPackedAsDecimal(Pos.WB03_PRE_CONT, Len.Int.WB03_PRE_CONT, Len.Fract.WB03_PRE_CONT);
    }

    public byte[] getWb03PreContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_CONT, Pos.WB03_PRE_CONT);
        return buffer;
    }

    public void setWb03PreContNull(String wb03PreContNull) {
        writeString(Pos.WB03_PRE_CONT_NULL, wb03PreContNull, Len.WB03_PRE_CONT_NULL);
    }

    /**Original name: WB03-PRE-CONT-NULL<br>*/
    public String getWb03PreContNull() {
        return readString(Pos.WB03_PRE_CONT_NULL, Len.WB03_PRE_CONT_NULL);
    }

    public String getWb03PreContNullFormatted() {
        return Functions.padBlanks(getWb03PreContNull(), Len.WB03_PRE_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_CONT = 1;
        public static final int WB03_PRE_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_CONT = 8;
        public static final int WB03_PRE_CONT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_CONT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_CONT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
