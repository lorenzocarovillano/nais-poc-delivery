package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WCOM-INTERVALLO-ELAB<br>
 * Variable: WCOM-INTERVALLO-ELAB from program LOAS0310<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WcomIntervalloElab extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WCOM-PERIODO-ELAB-DA
    private String da = DefaultValues.stringVal(Len.DA);
    //Original name: WCOM-PERIODO-ELAB-A
    private String a = DefaultValues.stringVal(Len.A);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCOM_INTERVALLO_ELAB;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWcomIntervalloElabBytes(buf);
    }

    public String getWcomIntervalloElabFormatted() {
        return MarshalByteExt.bufferToStr(getWcomIntervalloElabBytes());
    }

    public void setWcomIntervalloElabBytes(byte[] buffer) {
        setWcomIntervalloElabBytes(buffer, 1);
    }

    public byte[] getWcomIntervalloElabBytes() {
        byte[] buffer = new byte[Len.WCOM_INTERVALLO_ELAB];
        return getWcomIntervalloElabBytes(buffer, 1);
    }

    public void setWcomIntervalloElabBytes(byte[] buffer, int offset) {
        int position = offset;
        da = MarshalByte.readFixedString(buffer, position, Len.DA);
        position += Len.DA;
        a = MarshalByte.readFixedString(buffer, position, Len.A);
    }

    public byte[] getWcomIntervalloElabBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, da, Len.DA);
        position += Len.DA;
        MarshalByte.writeString(buffer, position, a, Len.A);
        return buffer;
    }

    public void setDa(int da) {
        this.da = NumericDisplay.asString(da, Len.DA);
    }

    public void setDaFormatted(String da) {
        this.da = Trunc.toUnsignedNumeric(da, Len.DA);
    }

    public void setDaFromBuffer(byte[] buffer) {
        da = MarshalByte.readFixedString(buffer, 1, Len.DA);
    }

    public int getDa() {
        return NumericDisplay.asInt(this.da);
    }

    public String getDaFormatted() {
        return this.da;
    }

    public void setA(int a) {
        this.a = NumericDisplay.asString(a, Len.A);
    }

    public void setAFormatted(String a) {
        this.a = Trunc.toUnsignedNumeric(a, Len.A);
    }

    public void setAFromBuffer(byte[] buffer) {
        a = MarshalByte.readFixedString(buffer, 1, Len.A);
    }

    public int getA() {
        return NumericDisplay.asInt(this.a);
    }

    public String getAFormatted() {
        return this.a;
    }

    @Override
    public byte[] serialize() {
        return getWcomIntervalloElabBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DA = 8;
        public static final int A = 8;
        public static final int WCOM_INTERVALLO_ELAB = DA + A;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
