package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-MATU-K2<br>
 * Variable: WDFA-MATU-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMatuK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMatuK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MATU_K2;
    }

    public void setWdfaMatuK2(AfDecimal wdfaMatuK2) {
        writeDecimalAsPacked(Pos.WDFA_MATU_K2, wdfaMatuK2.copy());
    }

    public void setWdfaMatuK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MATU_K2, Pos.WDFA_MATU_K2);
    }

    /**Original name: WDFA-MATU-K2<br>*/
    public AfDecimal getWdfaMatuK2() {
        return readPackedAsDecimal(Pos.WDFA_MATU_K2, Len.Int.WDFA_MATU_K2, Len.Fract.WDFA_MATU_K2);
    }

    public byte[] getWdfaMatuK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MATU_K2, Pos.WDFA_MATU_K2);
        return buffer;
    }

    public void setWdfaMatuK2Null(String wdfaMatuK2Null) {
        writeString(Pos.WDFA_MATU_K2_NULL, wdfaMatuK2Null, Len.WDFA_MATU_K2_NULL);
    }

    /**Original name: WDFA-MATU-K2-NULL<br>*/
    public String getWdfaMatuK2Null() {
        return readString(Pos.WDFA_MATU_K2_NULL, Len.WDFA_MATU_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_K2 = 1;
        public static final int WDFA_MATU_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MATU_K2 = 8;
        public static final int WDFA_MATU_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MATU_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
