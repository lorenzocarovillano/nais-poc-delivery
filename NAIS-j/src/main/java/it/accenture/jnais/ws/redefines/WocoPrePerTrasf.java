package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-PRE-PER-TRASF<br>
 * Variable: WOCO-PRE-PER-TRASF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoPrePerTrasf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoPrePerTrasf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_PRE_PER_TRASF;
    }

    public void setWocoPrePerTrasf(AfDecimal wocoPrePerTrasf) {
        writeDecimalAsPacked(Pos.WOCO_PRE_PER_TRASF, wocoPrePerTrasf.copy());
    }

    public void setWocoPrePerTrasfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_PRE_PER_TRASF, Pos.WOCO_PRE_PER_TRASF);
    }

    /**Original name: WOCO-PRE-PER-TRASF<br>*/
    public AfDecimal getWocoPrePerTrasf() {
        return readPackedAsDecimal(Pos.WOCO_PRE_PER_TRASF, Len.Int.WOCO_PRE_PER_TRASF, Len.Fract.WOCO_PRE_PER_TRASF);
    }

    public byte[] getWocoPrePerTrasfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_PRE_PER_TRASF, Pos.WOCO_PRE_PER_TRASF);
        return buffer;
    }

    public void initWocoPrePerTrasfSpaces() {
        fill(Pos.WOCO_PRE_PER_TRASF, Len.WOCO_PRE_PER_TRASF, Types.SPACE_CHAR);
    }

    public void setWocoPrePerTrasfNull(String wocoPrePerTrasfNull) {
        writeString(Pos.WOCO_PRE_PER_TRASF_NULL, wocoPrePerTrasfNull, Len.WOCO_PRE_PER_TRASF_NULL);
    }

    /**Original name: WOCO-PRE-PER-TRASF-NULL<br>*/
    public String getWocoPrePerTrasfNull() {
        return readString(Pos.WOCO_PRE_PER_TRASF_NULL, Len.WOCO_PRE_PER_TRASF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_PRE_PER_TRASF = 1;
        public static final int WOCO_PRE_PER_TRASF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_PRE_PER_TRASF = 8;
        public static final int WOCO_PRE_PER_TRASF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_PRE_PER_TRASF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_PRE_PER_TRASF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
