package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IAS-ONER-PRVNT-FIN<br>
 * Variable: TLI-IAS-ONER-PRVNT-FIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliIasOnerPrvntFin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliIasOnerPrvntFin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IAS_ONER_PRVNT_FIN;
    }

    public void setTliIasOnerPrvntFin(AfDecimal tliIasOnerPrvntFin) {
        writeDecimalAsPacked(Pos.TLI_IAS_ONER_PRVNT_FIN, tliIasOnerPrvntFin.copy());
    }

    public void setTliIasOnerPrvntFinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IAS_ONER_PRVNT_FIN, Pos.TLI_IAS_ONER_PRVNT_FIN);
    }

    /**Original name: TLI-IAS-ONER-PRVNT-FIN<br>*/
    public AfDecimal getTliIasOnerPrvntFin() {
        return readPackedAsDecimal(Pos.TLI_IAS_ONER_PRVNT_FIN, Len.Int.TLI_IAS_ONER_PRVNT_FIN, Len.Fract.TLI_IAS_ONER_PRVNT_FIN);
    }

    public byte[] getTliIasOnerPrvntFinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IAS_ONER_PRVNT_FIN, Pos.TLI_IAS_ONER_PRVNT_FIN);
        return buffer;
    }

    public void setTliIasOnerPrvntFinNull(String tliIasOnerPrvntFinNull) {
        writeString(Pos.TLI_IAS_ONER_PRVNT_FIN_NULL, tliIasOnerPrvntFinNull, Len.TLI_IAS_ONER_PRVNT_FIN_NULL);
    }

    /**Original name: TLI-IAS-ONER-PRVNT-FIN-NULL<br>*/
    public String getTliIasOnerPrvntFinNull() {
        return readString(Pos.TLI_IAS_ONER_PRVNT_FIN_NULL, Len.TLI_IAS_ONER_PRVNT_FIN_NULL);
    }

    public String getTliIasOnerPrvntFinNullFormatted() {
        return Functions.padBlanks(getTliIasOnerPrvntFinNull(), Len.TLI_IAS_ONER_PRVNT_FIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IAS_ONER_PRVNT_FIN = 1;
        public static final int TLI_IAS_ONER_PRVNT_FIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IAS_ONER_PRVNT_FIN = 8;
        public static final int TLI_IAS_ONER_PRVNT_FIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IAS_ONER_PRVNT_FIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IAS_ONER_PRVNT_FIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
