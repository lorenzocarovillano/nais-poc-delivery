package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.INumAdeGarTra;
import it.accenture.jnais.ws.redefines.NumUltNumLin;

/**Original name: NUM-ADE-GAR-TRA<br>
 * Variable: NUM-ADE-GAR-TRA from copybook IDBVNUM1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class NumAdeGarTraIdbsnum0 extends SerializableParameter implements INumAdeGarTra {

    //==== PROPERTIES ====
    //Original name: NUM-COD-COMP-ANIA
    private int numCodCompAnia = DefaultValues.INT_VAL;
    //Original name: NUM-ID-OGG
    private int numIdOgg = DefaultValues.INT_VAL;
    //Original name: NUM-TP-OGG
    private String numTpOgg = DefaultValues.stringVal(Len.NUM_TP_OGG);
    //Original name: NUM-ULT-NUM-LIN
    private NumUltNumLin numUltNumLin = new NumUltNumLin();
    //Original name: NUM-DS-OPER-SQL
    private char numDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: NUM-DS-VER
    private int numDsVer = DefaultValues.INT_VAL;
    //Original name: NUM-DS-TS-CPTZ
    private long numDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: NUM-DS-UTENTE
    private String numDsUtente = DefaultValues.stringVal(Len.NUM_DS_UTENTE);
    //Original name: NUM-DS-STATO-ELAB
    private char numDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.NUM_ADE_GAR_TRA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setNumAdeGarTraBytes(buf);
    }

    public void setNumAdeGarTraBytes(byte[] buffer) {
        setNumAdeGarTraBytes(buffer, 1);
    }

    public byte[] getNumAdeGarTraBytes() {
        byte[] buffer = new byte[Len.NUM_ADE_GAR_TRA];
        return getNumAdeGarTraBytes(buffer, 1);
    }

    public void setNumAdeGarTraBytes(byte[] buffer, int offset) {
        int position = offset;
        numCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_COD_COMP_ANIA, 0);
        position += Len.NUM_COD_COMP_ANIA;
        numIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_ID_OGG, 0);
        position += Len.NUM_ID_OGG;
        numTpOgg = MarshalByte.readString(buffer, position, Len.NUM_TP_OGG);
        position += Len.NUM_TP_OGG;
        numUltNumLin.setNumUltNumLinFromBuffer(buffer, position);
        position += NumUltNumLin.Len.NUM_ULT_NUM_LIN;
        numDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        numDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_DS_VER, 0);
        position += Len.NUM_DS_VER;
        numDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.NUM_DS_TS_CPTZ, 0);
        position += Len.NUM_DS_TS_CPTZ;
        numDsUtente = MarshalByte.readString(buffer, position, Len.NUM_DS_UTENTE);
        position += Len.NUM_DS_UTENTE;
        numDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getNumAdeGarTraBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, numCodCompAnia, Len.Int.NUM_COD_COMP_ANIA, 0);
        position += Len.NUM_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, numIdOgg, Len.Int.NUM_ID_OGG, 0);
        position += Len.NUM_ID_OGG;
        MarshalByte.writeString(buffer, position, numTpOgg, Len.NUM_TP_OGG);
        position += Len.NUM_TP_OGG;
        numUltNumLin.getNumUltNumLinAsBuffer(buffer, position);
        position += NumUltNumLin.Len.NUM_ULT_NUM_LIN;
        MarshalByte.writeChar(buffer, position, numDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, numDsVer, Len.Int.NUM_DS_VER, 0);
        position += Len.NUM_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, numDsTsCptz, Len.Int.NUM_DS_TS_CPTZ, 0);
        position += Len.NUM_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, numDsUtente, Len.NUM_DS_UTENTE);
        position += Len.NUM_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, numDsStatoElab);
        return buffer;
    }

    public void setNumCodCompAnia(int numCodCompAnia) {
        this.numCodCompAnia = numCodCompAnia;
    }

    public int getNumCodCompAnia() {
        return this.numCodCompAnia;
    }

    public void setNumIdOgg(int numIdOgg) {
        this.numIdOgg = numIdOgg;
    }

    public int getNumIdOgg() {
        return this.numIdOgg;
    }

    public void setNumTpOgg(String numTpOgg) {
        this.numTpOgg = Functions.subString(numTpOgg, Len.NUM_TP_OGG);
    }

    public String getNumTpOgg() {
        return this.numTpOgg;
    }

    public void setNumDsOperSql(char numDsOperSql) {
        this.numDsOperSql = numDsOperSql;
    }

    public void setNumDsOperSqlFormatted(String numDsOperSql) {
        setNumDsOperSql(Functions.charAt(numDsOperSql, Types.CHAR_SIZE));
    }

    public char getNumDsOperSql() {
        return this.numDsOperSql;
    }

    public void setNumDsVer(int numDsVer) {
        this.numDsVer = numDsVer;
    }

    public int getNumDsVer() {
        return this.numDsVer;
    }

    public void setNumDsTsCptz(long numDsTsCptz) {
        this.numDsTsCptz = numDsTsCptz;
    }

    public long getNumDsTsCptz() {
        return this.numDsTsCptz;
    }

    public void setNumDsUtente(String numDsUtente) {
        this.numDsUtente = Functions.subString(numDsUtente, Len.NUM_DS_UTENTE);
    }

    public String getNumDsUtente() {
        return this.numDsUtente;
    }

    public void setNumDsStatoElab(char numDsStatoElab) {
        this.numDsStatoElab = numDsStatoElab;
    }

    public void setNumDsStatoElabFormatted(String numDsStatoElab) {
        setNumDsStatoElab(Functions.charAt(numDsStatoElab, Types.CHAR_SIZE));
    }

    public char getNumDsStatoElab() {
        return this.numDsStatoElab;
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public int getIdOgg() {
        throw new FieldNotMappedException("idOgg");
    }

    @Override
    public void setIdOgg(int idOgg) {
        throw new FieldNotMappedException("idOgg");
    }

    public NumUltNumLin getNumUltNumLin() {
        return numUltNumLin;
    }

    @Override
    public String getTpOgg() {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public void setTpOgg(String tpOgg) {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public int getUltNumLin() {
        throw new FieldNotMappedException("ultNumLin");
    }

    @Override
    public void setUltNumLin(int ultNumLin) {
        throw new FieldNotMappedException("ultNumLin");
    }

    @Override
    public Integer getUltNumLinObj() {
        return ((Integer)getUltNumLin());
    }

    @Override
    public void setUltNumLinObj(Integer ultNumLinObj) {
        setUltNumLin(((int)ultNumLinObj));
    }

    @Override
    public byte[] serialize() {
        return getNumAdeGarTraBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_COD_COMP_ANIA = 3;
        public static final int NUM_ID_OGG = 5;
        public static final int NUM_TP_OGG = 2;
        public static final int NUM_DS_OPER_SQL = 1;
        public static final int NUM_DS_VER = 5;
        public static final int NUM_DS_TS_CPTZ = 10;
        public static final int NUM_DS_UTENTE = 20;
        public static final int NUM_DS_STATO_ELAB = 1;
        public static final int NUM_ADE_GAR_TRA = NUM_COD_COMP_ANIA + NUM_ID_OGG + NUM_TP_OGG + NumUltNumLin.Len.NUM_ULT_NUM_LIN + NUM_DS_OPER_SQL + NUM_DS_VER + NUM_DS_TS_CPTZ + NUM_DS_UTENTE + NUM_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NUM_COD_COMP_ANIA = 5;
            public static final int NUM_ID_OGG = 9;
            public static final int NUM_DS_VER = 9;
            public static final int NUM_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
