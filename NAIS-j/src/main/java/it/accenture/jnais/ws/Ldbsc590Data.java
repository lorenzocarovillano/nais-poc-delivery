package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGravitaErrore;
import it.accenture.jnais.copy.Ldbvc591;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSC590<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsc590Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-MOVI
    private IndGravitaErrore indMovi = new IndGravitaErrore();
    //Original name: MOV-DT-EFF-DB
    private String movDtEffDb = DefaultValues.stringVal(Len.MOV_DT_EFF_DB);
    //Original name: LDBVC591
    private Ldbvc591 ldbvc591 = new Ldbvc591();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setMovDtEffDb(String movDtEffDb) {
        this.movDtEffDb = Functions.subString(movDtEffDb, Len.MOV_DT_EFF_DB);
    }

    public String getMovDtEffDb() {
        return this.movDtEffDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGravitaErrore getIndMovi() {
        return indMovi;
    }

    public Ldbvc591 getLdbvc591() {
        return ldbvc591;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_DT_EFF_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
