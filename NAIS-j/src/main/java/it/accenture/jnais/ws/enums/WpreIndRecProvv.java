package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPRE-IND-REC-PROVV<br>
 * Variable: WPRE-IND-REC-PROVV from copybook LOAC0560<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpreIndRecProvv {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIndRecProvv(char indRecProvv) {
        this.value = indRecProvv;
    }

    public char getIndRecProvv() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IND_REC_PROVV = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
