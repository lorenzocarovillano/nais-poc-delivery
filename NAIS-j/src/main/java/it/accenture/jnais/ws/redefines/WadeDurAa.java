package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DUR-AA<br>
 * Variable: WADE-DUR-AA from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DUR_AA;
    }

    public void setWadeDurAa(int wadeDurAa) {
        writeIntAsPacked(Pos.WADE_DUR_AA, wadeDurAa, Len.Int.WADE_DUR_AA);
    }

    public void setWadeDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DUR_AA, Pos.WADE_DUR_AA);
    }

    /**Original name: WADE-DUR-AA<br>*/
    public int getWadeDurAa() {
        return readPackedAsInt(Pos.WADE_DUR_AA, Len.Int.WADE_DUR_AA);
    }

    public byte[] getWadeDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DUR_AA, Pos.WADE_DUR_AA);
        return buffer;
    }

    public void initWadeDurAaSpaces() {
        fill(Pos.WADE_DUR_AA, Len.WADE_DUR_AA, Types.SPACE_CHAR);
    }

    public void setWadeDurAaNull(String wadeDurAaNull) {
        writeString(Pos.WADE_DUR_AA_NULL, wadeDurAaNull, Len.WADE_DUR_AA_NULL);
    }

    /**Original name: WADE-DUR-AA-NULL<br>*/
    public String getWadeDurAaNull() {
        return readString(Pos.WADE_DUR_AA_NULL, Len.WADE_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DUR_AA = 1;
        public static final int WADE_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DUR_AA = 3;
        public static final int WADE_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
