package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-BOLLO-TOT-VL<br>
 * Variable: WDFL-IMPST-BOLLO-TOT-VL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstBolloTotVl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstBolloTotVl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_BOLLO_TOT_VL;
    }

    public void setWdflImpstBolloTotVl(AfDecimal wdflImpstBolloTotVl) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_BOLLO_TOT_VL, wdflImpstBolloTotVl.copy());
    }

    public void setWdflImpstBolloTotVlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_TOT_VL, Pos.WDFL_IMPST_BOLLO_TOT_VL);
    }

    /**Original name: WDFL-IMPST-BOLLO-TOT-VL<br>*/
    public AfDecimal getWdflImpstBolloTotVl() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_BOLLO_TOT_VL, Len.Int.WDFL_IMPST_BOLLO_TOT_VL, Len.Fract.WDFL_IMPST_BOLLO_TOT_VL);
    }

    public byte[] getWdflImpstBolloTotVlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_BOLLO_TOT_VL, Pos.WDFL_IMPST_BOLLO_TOT_VL);
        return buffer;
    }

    public void setWdflImpstBolloTotVlNull(String wdflImpstBolloTotVlNull) {
        writeString(Pos.WDFL_IMPST_BOLLO_TOT_VL_NULL, wdflImpstBolloTotVlNull, Len.WDFL_IMPST_BOLLO_TOT_VL_NULL);
    }

    /**Original name: WDFL-IMPST-BOLLO-TOT-VL-NULL<br>*/
    public String getWdflImpstBolloTotVlNull() {
        return readString(Pos.WDFL_IMPST_BOLLO_TOT_VL_NULL, Len.WDFL_IMPST_BOLLO_TOT_VL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_TOT_VL = 1;
        public static final int WDFL_IMPST_BOLLO_TOT_VL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_BOLLO_TOT_VL = 8;
        public static final int WDFL_IMPST_BOLLO_TOT_VL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_TOT_VL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_BOLLO_TOT_VL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
