package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PC-ADER<br>
 * Variable: WADE-PC-ADER from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePcAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePcAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PC_ADER;
    }

    public void setWadePcAder(AfDecimal wadePcAder) {
        writeDecimalAsPacked(Pos.WADE_PC_ADER, wadePcAder.copy());
    }

    public void setWadePcAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PC_ADER, Pos.WADE_PC_ADER);
    }

    /**Original name: WADE-PC-ADER<br>*/
    public AfDecimal getWadePcAder() {
        return readPackedAsDecimal(Pos.WADE_PC_ADER, Len.Int.WADE_PC_ADER, Len.Fract.WADE_PC_ADER);
    }

    public byte[] getWadePcAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PC_ADER, Pos.WADE_PC_ADER);
        return buffer;
    }

    public void initWadePcAderSpaces() {
        fill(Pos.WADE_PC_ADER, Len.WADE_PC_ADER, Types.SPACE_CHAR);
    }

    public void setWadePcAderNull(String wadePcAderNull) {
        writeString(Pos.WADE_PC_ADER_NULL, wadePcAderNull, Len.WADE_PC_ADER_NULL);
    }

    /**Original name: WADE-PC-ADER-NULL<br>*/
    public String getWadePcAderNull() {
        return readString(Pos.WADE_PC_ADER_NULL, Len.WADE_PC_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PC_ADER = 1;
        public static final int WADE_PC_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PC_ADER = 4;
        public static final int WADE_PC_ADER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
