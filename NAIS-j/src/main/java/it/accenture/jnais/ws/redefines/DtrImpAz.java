package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-IMP-AZ<br>
 * Variable: DTR-IMP-AZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_IMP_AZ;
    }

    public void setDtrImpAz(AfDecimal dtrImpAz) {
        writeDecimalAsPacked(Pos.DTR_IMP_AZ, dtrImpAz.copy());
    }

    public void setDtrImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_IMP_AZ, Pos.DTR_IMP_AZ);
    }

    /**Original name: DTR-IMP-AZ<br>*/
    public AfDecimal getDtrImpAz() {
        return readPackedAsDecimal(Pos.DTR_IMP_AZ, Len.Int.DTR_IMP_AZ, Len.Fract.DTR_IMP_AZ);
    }

    public byte[] getDtrImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_IMP_AZ, Pos.DTR_IMP_AZ);
        return buffer;
    }

    public void setDtrImpAzNull(String dtrImpAzNull) {
        writeString(Pos.DTR_IMP_AZ_NULL, dtrImpAzNull, Len.DTR_IMP_AZ_NULL);
    }

    /**Original name: DTR-IMP-AZ-NULL<br>*/
    public String getDtrImpAzNull() {
        return readString(Pos.DTR_IMP_AZ_NULL, Len.DTR_IMP_AZ_NULL);
    }

    public String getDtrImpAzNullFormatted() {
        return Functions.padBlanks(getDtrImpAzNull(), Len.DTR_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_IMP_AZ = 1;
        public static final int DTR_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_IMP_AZ = 8;
        public static final int DTR_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
