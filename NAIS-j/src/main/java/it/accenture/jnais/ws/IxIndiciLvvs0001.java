package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs0001 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PARAM-OGG
    private short ixTabParamOgg = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-DCO
    private short ixTabDco = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ADE
    private short ixTabAde = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-GRZ
    private short ixTabGrz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-POG
    private short ixTabPog = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-PMO
    private short ixTabPmo = DefaultValues.BIN_SHORT_VAL;
    //Original name: CONT-FETCH
    private short contFetch = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIxTabParamOgg(short ixTabParamOgg) {
        this.ixTabParamOgg = ixTabParamOgg;
    }

    public short getIxTabParamOgg() {
        return this.ixTabParamOgg;
    }

    public void setIxTabDco(short ixTabDco) {
        this.ixTabDco = ixTabDco;
    }

    public short getIxTabDco() {
        return this.ixTabDco;
    }

    public void setIxTabAde(short ixTabAde) {
        this.ixTabAde = ixTabAde;
    }

    public short getIxTabAde() {
        return this.ixTabAde;
    }

    public void setIxTabGrz(short ixTabGrz) {
        this.ixTabGrz = ixTabGrz;
    }

    public short getIxTabGrz() {
        return this.ixTabGrz;
    }

    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public void setIxTabPog(short ixTabPog) {
        this.ixTabPog = ixTabPog;
    }

    public short getIxTabPog() {
        return this.ixTabPog;
    }

    public void setIxTabPmo(short ixTabPmo) {
        this.ixTabPmo = ixTabPmo;
    }

    public short getIxTabPmo() {
        return this.ixTabPmo;
    }

    public void setContFetch(short contFetch) {
        this.contFetch = contFetch;
    }

    public short getContFetch() {
        return this.contFetch;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }
}
