package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-PC-SERV-VAL<br>
 * Variable: WPMO-PC-SERV-VAL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoPcServVal extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoPcServVal() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_PC_SERV_VAL;
    }

    public void setWpmoPcServVal(AfDecimal wpmoPcServVal) {
        writeDecimalAsPacked(Pos.WPMO_PC_SERV_VAL, wpmoPcServVal.copy());
    }

    public void setWpmoPcServValFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_PC_SERV_VAL, Pos.WPMO_PC_SERV_VAL);
    }

    /**Original name: WPMO-PC-SERV-VAL<br>*/
    public AfDecimal getWpmoPcServVal() {
        return readPackedAsDecimal(Pos.WPMO_PC_SERV_VAL, Len.Int.WPMO_PC_SERV_VAL, Len.Fract.WPMO_PC_SERV_VAL);
    }

    public byte[] getWpmoPcServValAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_PC_SERV_VAL, Pos.WPMO_PC_SERV_VAL);
        return buffer;
    }

    public void initWpmoPcServValSpaces() {
        fill(Pos.WPMO_PC_SERV_VAL, Len.WPMO_PC_SERV_VAL, Types.SPACE_CHAR);
    }

    public void setWpmoPcServValNull(String wpmoPcServValNull) {
        writeString(Pos.WPMO_PC_SERV_VAL_NULL, wpmoPcServValNull, Len.WPMO_PC_SERV_VAL_NULL);
    }

    /**Original name: WPMO-PC-SERV-VAL-NULL<br>*/
    public String getWpmoPcServValNull() {
        return readString(Pos.WPMO_PC_SERV_VAL_NULL, Len.WPMO_PC_SERV_VAL_NULL);
    }

    public String getWpmoPcServValNullFormatted() {
        return Functions.padBlanks(getWpmoPcServValNull(), Len.WPMO_PC_SERV_VAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_PC_SERV_VAL = 1;
        public static final int WPMO_PC_SERV_VAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_PC_SERV_VAL = 4;
        public static final int WPMO_PC_SERV_VAL_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_PC_SERV_VAL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_PC_SERV_VAL = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
