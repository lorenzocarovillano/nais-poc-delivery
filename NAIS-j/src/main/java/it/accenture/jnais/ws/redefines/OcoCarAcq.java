package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-CAR-ACQ<br>
 * Variable: OCO-CAR-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_CAR_ACQ;
    }

    public void setOcoCarAcq(AfDecimal ocoCarAcq) {
        writeDecimalAsPacked(Pos.OCO_CAR_ACQ, ocoCarAcq.copy());
    }

    public void setOcoCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_CAR_ACQ, Pos.OCO_CAR_ACQ);
    }

    /**Original name: OCO-CAR-ACQ<br>*/
    public AfDecimal getOcoCarAcq() {
        return readPackedAsDecimal(Pos.OCO_CAR_ACQ, Len.Int.OCO_CAR_ACQ, Len.Fract.OCO_CAR_ACQ);
    }

    public byte[] getOcoCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_CAR_ACQ, Pos.OCO_CAR_ACQ);
        return buffer;
    }

    public void setOcoCarAcqNull(String ocoCarAcqNull) {
        writeString(Pos.OCO_CAR_ACQ_NULL, ocoCarAcqNull, Len.OCO_CAR_ACQ_NULL);
    }

    /**Original name: OCO-CAR-ACQ-NULL<br>*/
    public String getOcoCarAcqNull() {
        return readString(Pos.OCO_CAR_ACQ_NULL, Len.OCO_CAR_ACQ_NULL);
    }

    public String getOcoCarAcqNullFormatted() {
        return Functions.padBlanks(getOcoCarAcqNull(), Len.OCO_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_CAR_ACQ = 1;
        public static final int OCO_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_CAR_ACQ = 8;
        public static final int OCO_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
