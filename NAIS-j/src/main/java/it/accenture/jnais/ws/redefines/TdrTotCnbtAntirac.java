package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-CNBT-ANTIRAC<br>
 * Variable: TDR-TOT-CNBT-ANTIRAC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_CNBT_ANTIRAC;
    }

    public void setTdrTotCnbtAntirac(AfDecimal tdrTotCnbtAntirac) {
        writeDecimalAsPacked(Pos.TDR_TOT_CNBT_ANTIRAC, tdrTotCnbtAntirac.copy());
    }

    public void setTdrTotCnbtAntiracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_CNBT_ANTIRAC, Pos.TDR_TOT_CNBT_ANTIRAC);
    }

    /**Original name: TDR-TOT-CNBT-ANTIRAC<br>*/
    public AfDecimal getTdrTotCnbtAntirac() {
        return readPackedAsDecimal(Pos.TDR_TOT_CNBT_ANTIRAC, Len.Int.TDR_TOT_CNBT_ANTIRAC, Len.Fract.TDR_TOT_CNBT_ANTIRAC);
    }

    public byte[] getTdrTotCnbtAntiracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_CNBT_ANTIRAC, Pos.TDR_TOT_CNBT_ANTIRAC);
        return buffer;
    }

    public void setTdrTotCnbtAntiracNull(String tdrTotCnbtAntiracNull) {
        writeString(Pos.TDR_TOT_CNBT_ANTIRAC_NULL, tdrTotCnbtAntiracNull, Len.TDR_TOT_CNBT_ANTIRAC_NULL);
    }

    /**Original name: TDR-TOT-CNBT-ANTIRAC-NULL<br>*/
    public String getTdrTotCnbtAntiracNull() {
        return readString(Pos.TDR_TOT_CNBT_ANTIRAC_NULL, Len.TDR_TOT_CNBT_ANTIRAC_NULL);
    }

    public String getTdrTotCnbtAntiracNullFormatted() {
        return Functions.padBlanks(getTdrTotCnbtAntiracNull(), Len.TDR_TOT_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CNBT_ANTIRAC = 1;
        public static final int TDR_TOT_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CNBT_ANTIRAC = 8;
        public static final int TDR_TOT_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
