package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV1691<br>
 * Variable: LDBV1691 from copybook LDBV1691<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv1691 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV1691-ID-POLIZZA
    private int idPolizza = DefaultValues.INT_VAL;
    //Original name: LDBV1691-TP-STAT-BUS
    private String tpStatBus = DefaultValues.stringVal(Len.TP_STAT_BUS);
    //Original name: LDBV1691-IMP-TOT
    private int impTot = DefaultValues.INT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV1691;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv1691Bytes(buf);
    }

    public void setLdbv1691Bytes(byte[] buffer) {
        setLdbv1691Bytes(buffer, 1);
    }

    public byte[] getLdbv1691Bytes() {
        byte[] buffer = new byte[Len.LDBV1691];
        return getLdbv1691Bytes(buffer, 1);
    }

    public void setLdbv1691Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPolizza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLIZZA, 0);
        position += Len.ID_POLIZZA;
        tpStatBus = MarshalByte.readString(buffer, position, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        impTot = MarshalByte.readPackedAsInt(buffer, position, Len.Int.IMP_TOT, 0);
    }

    public byte[] getLdbv1691Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPolizza, Len.Int.ID_POLIZZA, 0);
        position += Len.ID_POLIZZA;
        MarshalByte.writeString(buffer, position, tpStatBus, Len.TP_STAT_BUS);
        position += Len.TP_STAT_BUS;
        MarshalByte.writeIntAsPacked(buffer, position, impTot, Len.Int.IMP_TOT, 0);
        return buffer;
    }

    public void setIdPolizza(int idPolizza) {
        this.idPolizza = idPolizza;
    }

    public int getIdPolizza() {
        return this.idPolizza;
    }

    public void setTpStatBus(String tpStatBus) {
        this.tpStatBus = Functions.subString(tpStatBus, Len.TP_STAT_BUS);
    }

    public String getTpStatBus() {
        return this.tpStatBus;
    }

    public void setImpTot(int impTot) {
        this.impTot = impTot;
    }

    public int getImpTot() {
        return this.impTot;
    }

    @Override
    public byte[] serialize() {
        return getLdbv1691Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLIZZA = 5;
        public static final int TP_STAT_BUS = 2;
        public static final int IMP_TOT = 5;
        public static final int LDBV1691 = ID_POLIZZA + TP_STAT_BUS + IMP_TOT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLIZZA = 9;
            public static final int IMP_TOT = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
