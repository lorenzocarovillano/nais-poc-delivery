package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvrre1;
import it.accenture.jnais.copy.WrreDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WRRE-TAB-RAPP-RETE<br>
 * Variables: WRRE-TAB-RAPP-RETE from program IDSS0160<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WrreTabRappRete {

    //==== PROPERTIES ====
    //Original name: LCCVRRE1
    private Lccvrre1 lccvrre1 = new Lccvrre1();

    //==== METHODS ====
    public void setWrreTabRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvrre1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvrre1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvrre1.Len.Int.ID_PTF, 0));
        position += Lccvrre1.Len.ID_PTF;
        lccvrre1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getTabRreBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvrre1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvrre1.getIdPtf(), Lccvrre1.Len.Int.ID_PTF, 0);
        position += Lccvrre1.Len.ID_PTF;
        lccvrre1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWrreTabRappReteSpaces() {
        lccvrre1.initLccvrre1Spaces();
    }

    public Lccvrre1 getLccvrre1() {
        return lccvrre1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_TAB_RAPP_RETE = WpolStatus.Len.STATUS + Lccvrre1.Len.ID_PTF + WrreDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
