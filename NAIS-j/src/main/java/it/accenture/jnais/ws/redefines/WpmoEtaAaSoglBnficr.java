package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-ETA-AA-SOGL-BNFICR<br>
 * Variable: WPMO-ETA-AA-SOGL-BNFICR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoEtaAaSoglBnficr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoEtaAaSoglBnficr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_ETA_AA_SOGL_BNFICR;
    }

    public void setWpmoEtaAaSoglBnficr(short wpmoEtaAaSoglBnficr) {
        writeShortAsPacked(Pos.WPMO_ETA_AA_SOGL_BNFICR, wpmoEtaAaSoglBnficr, Len.Int.WPMO_ETA_AA_SOGL_BNFICR);
    }

    public void setWpmoEtaAaSoglBnficrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_ETA_AA_SOGL_BNFICR, Pos.WPMO_ETA_AA_SOGL_BNFICR);
    }

    /**Original name: WPMO-ETA-AA-SOGL-BNFICR<br>*/
    public short getWpmoEtaAaSoglBnficr() {
        return readPackedAsShort(Pos.WPMO_ETA_AA_SOGL_BNFICR, Len.Int.WPMO_ETA_AA_SOGL_BNFICR);
    }

    public byte[] getWpmoEtaAaSoglBnficrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_ETA_AA_SOGL_BNFICR, Pos.WPMO_ETA_AA_SOGL_BNFICR);
        return buffer;
    }

    public void initWpmoEtaAaSoglBnficrSpaces() {
        fill(Pos.WPMO_ETA_AA_SOGL_BNFICR, Len.WPMO_ETA_AA_SOGL_BNFICR, Types.SPACE_CHAR);
    }

    public void setWpmoEtaAaSoglBnficrNull(String wpmoEtaAaSoglBnficrNull) {
        writeString(Pos.WPMO_ETA_AA_SOGL_BNFICR_NULL, wpmoEtaAaSoglBnficrNull, Len.WPMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    /**Original name: WPMO-ETA-AA-SOGL-BNFICR-NULL<br>*/
    public String getWpmoEtaAaSoglBnficrNull() {
        return readString(Pos.WPMO_ETA_AA_SOGL_BNFICR_NULL, Len.WPMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    public String getWpmoEtaAaSoglBnficrNullFormatted() {
        return Functions.padBlanks(getWpmoEtaAaSoglBnficrNull(), Len.WPMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_ETA_AA_SOGL_BNFICR = 1;
        public static final int WPMO_ETA_AA_SOGL_BNFICR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_ETA_AA_SOGL_BNFICR = 2;
        public static final int WPMO_ETA_AA_SOGL_BNFICR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_ETA_AA_SOGL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
