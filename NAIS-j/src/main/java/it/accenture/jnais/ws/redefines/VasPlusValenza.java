package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-PLUS-VALENZA<br>
 * Variable: VAS-PLUS-VALENZA from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasPlusValenza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasPlusValenza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_PLUS_VALENZA;
    }

    public void setVasPlusValenza(AfDecimal vasPlusValenza) {
        writeDecimalAsPacked(Pos.VAS_PLUS_VALENZA, vasPlusValenza.copy());
    }

    public void setVasPlusValenzaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_PLUS_VALENZA, Pos.VAS_PLUS_VALENZA);
    }

    /**Original name: VAS-PLUS-VALENZA<br>*/
    public AfDecimal getVasPlusValenza() {
        return readPackedAsDecimal(Pos.VAS_PLUS_VALENZA, Len.Int.VAS_PLUS_VALENZA, Len.Fract.VAS_PLUS_VALENZA);
    }

    public byte[] getVasPlusValenzaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_PLUS_VALENZA, Pos.VAS_PLUS_VALENZA);
        return buffer;
    }

    public void setVasPlusValenzaNull(String vasPlusValenzaNull) {
        writeString(Pos.VAS_PLUS_VALENZA_NULL, vasPlusValenzaNull, Len.VAS_PLUS_VALENZA_NULL);
    }

    /**Original name: VAS-PLUS-VALENZA-NULL<br>*/
    public String getVasPlusValenzaNull() {
        return readString(Pos.VAS_PLUS_VALENZA_NULL, Len.VAS_PLUS_VALENZA_NULL);
    }

    public String getVasPlusValenzaNullFormatted() {
        return Functions.padBlanks(getVasPlusValenzaNull(), Len.VAS_PLUS_VALENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_PLUS_VALENZA = 1;
        public static final int VAS_PLUS_VALENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_PLUS_VALENZA = 8;
        public static final int VAS_PLUS_VALENZA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_PLUS_VALENZA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_PLUS_VALENZA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
