package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSO0021-ELEMENTS-STR-DATO<br>
 * Variables: IDSO0021-ELEMENTS-STR-DATO from copybook IDSO0021<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idso0021ElementsStrDato {

    //==== PROPERTIES ====
    //Original name: IDSO0021-CODICE-DATO
    private String codiceDato = DefaultValues.stringVal(Len.CODICE_DATO);
    //Original name: IDSO0021-FLAG-KEY
    private char flagKey = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-FLAG-RETURN-CODE
    private char flagReturnCode = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-FLAG-CALL-USING
    private char flagCallUsing = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-FLAG-WHERE-COND
    private char flagWhereCond = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-FLAG-REDEFINES
    private char flagRedefines = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-TIPO-DATO
    private String tipoDato = DefaultValues.stringVal(Len.TIPO_DATO);
    //Original name: IDSO0021-INIZIO-POSIZIONE
    private int inizioPosizione = DefaultValues.INT_VAL;
    //Original name: IDSO0021-LUNGHEZZA-DATO
    private int lunghezzaDato = DefaultValues.INT_VAL;
    //Original name: IDSO0021-LUNGHEZZA-DATO-NOM
    private int lunghezzaDatoNom = DefaultValues.INT_VAL;
    //Original name: IDSO0021-PRECISIONE-DATO
    private short precisioneDato = DefaultValues.SHORT_VAL;
    //Original name: IDSO0021-FORMATTAZIONE-DATO
    private String formattazioneDato = DefaultValues.stringVal(Len.FORMATTAZIONE_DATO);
    //Original name: IDSO0021-SERV-CONVERSIONE
    private String servConversione = DefaultValues.stringVal(Len.SERV_CONVERSIONE);
    //Original name: IDSO0021-AREA-CONV-STANDARD
    private char areaConvStandard = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-CODICE-DOMINIO
    private String codiceDominio = DefaultValues.stringVal(Len.CODICE_DOMINIO);
    //Original name: IDSO0021-SERVIZIO-VALIDAZIONE
    private String servizioValidazione = DefaultValues.stringVal(Len.SERVIZIO_VALIDAZIONE);
    //Original name: IDSO0021-RICORRENZA
    private int ricorrenza = DefaultValues.INT_VAL;
    //Original name: IDSO0021-CLONE
    private char clone = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-OBBLIGATORIETA
    private char obbligatorieta = DefaultValues.CHAR_VAL;
    //Original name: IDSO0021-VALORE-DEFAULT
    private String valoreDefault = DefaultValues.stringVal(Len.VALORE_DEFAULT);

    //==== METHODS ====
    public void setElementsStrDatoBytes(byte[] buffer) {
        setElementsStrDatoBytes(buffer, 1);
    }

    public void setElementsStrDatoBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceDato = MarshalByte.readString(buffer, position, Len.CODICE_DATO);
        position += Len.CODICE_DATO;
        flagKey = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flagReturnCode = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flagCallUsing = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flagWhereCond = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flagRedefines = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tipoDato = MarshalByte.readString(buffer, position, Len.TIPO_DATO);
        position += Len.TIPO_DATO;
        inizioPosizione = MarshalByte.readPackedAsInt(buffer, position, Len.Int.INIZIO_POSIZIONE, 0);
        position += Len.INIZIO_POSIZIONE;
        lunghezzaDato = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA_DATO, 0);
        position += Len.LUNGHEZZA_DATO;
        lunghezzaDatoNom = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA_DATO_NOM, 0);
        position += Len.LUNGHEZZA_DATO_NOM;
        precisioneDato = MarshalByte.readPackedAsShort(buffer, position, Len.Int.PRECISIONE_DATO, 0);
        position += Len.PRECISIONE_DATO;
        formattazioneDato = MarshalByte.readString(buffer, position, Len.FORMATTAZIONE_DATO);
        position += Len.FORMATTAZIONE_DATO;
        servConversione = MarshalByte.readString(buffer, position, Len.SERV_CONVERSIONE);
        position += Len.SERV_CONVERSIONE;
        areaConvStandard = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codiceDominio = MarshalByte.readString(buffer, position, Len.CODICE_DOMINIO);
        position += Len.CODICE_DOMINIO;
        servizioValidazione = MarshalByte.readString(buffer, position, Len.SERVIZIO_VALIDAZIONE);
        position += Len.SERVIZIO_VALIDAZIONE;
        ricorrenza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RICORRENZA, 0);
        position += Len.RICORRENZA;
        clone = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        obbligatorieta = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        valoreDefault = MarshalByte.readString(buffer, position, Len.VALORE_DEFAULT);
    }

    public byte[] getElementsStrDatoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codiceDato, Len.CODICE_DATO);
        position += Len.CODICE_DATO;
        MarshalByte.writeChar(buffer, position, flagKey);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagReturnCode);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagCallUsing);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagWhereCond);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flagRedefines);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tipoDato, Len.TIPO_DATO);
        position += Len.TIPO_DATO;
        MarshalByte.writeIntAsPacked(buffer, position, inizioPosizione, Len.Int.INIZIO_POSIZIONE, 0);
        position += Len.INIZIO_POSIZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezzaDato, Len.Int.LUNGHEZZA_DATO, 0);
        position += Len.LUNGHEZZA_DATO;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezzaDatoNom, Len.Int.LUNGHEZZA_DATO_NOM, 0);
        position += Len.LUNGHEZZA_DATO_NOM;
        MarshalByte.writeShortAsPacked(buffer, position, precisioneDato, Len.Int.PRECISIONE_DATO, 0);
        position += Len.PRECISIONE_DATO;
        MarshalByte.writeString(buffer, position, formattazioneDato, Len.FORMATTAZIONE_DATO);
        position += Len.FORMATTAZIONE_DATO;
        MarshalByte.writeString(buffer, position, servConversione, Len.SERV_CONVERSIONE);
        position += Len.SERV_CONVERSIONE;
        MarshalByte.writeChar(buffer, position, areaConvStandard);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codiceDominio, Len.CODICE_DOMINIO);
        position += Len.CODICE_DOMINIO;
        MarshalByte.writeString(buffer, position, servizioValidazione, Len.SERVIZIO_VALIDAZIONE);
        position += Len.SERVIZIO_VALIDAZIONE;
        MarshalByte.writeIntAsPacked(buffer, position, ricorrenza, Len.Int.RICORRENZA, 0);
        position += Len.RICORRENZA;
        MarshalByte.writeChar(buffer, position, clone);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, obbligatorieta);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, valoreDefault, Len.VALORE_DEFAULT);
        return buffer;
    }

    public void initElementsStrDatoHighValues() {
        codiceDato = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CODICE_DATO);
        flagKey = Types.HIGH_CHAR_VAL;
        flagReturnCode = Types.HIGH_CHAR_VAL;
        flagCallUsing = Types.HIGH_CHAR_VAL;
        flagWhereCond = Types.HIGH_CHAR_VAL;
        flagRedefines = Types.HIGH_CHAR_VAL;
        tipoDato = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TIPO_DATO);
        inizioPosizione = Types.HIGH_INT_VAL;
        lunghezzaDato = Types.HIGH_INT_VAL;
        lunghezzaDatoNom = Types.HIGH_INT_VAL;
        precisioneDato = Types.HIGH_SHORT_VAL;
        formattazioneDato = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.FORMATTAZIONE_DATO);
        servConversione = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.SERV_CONVERSIONE);
        areaConvStandard = Types.HIGH_CHAR_VAL;
        codiceDominio = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CODICE_DOMINIO);
        servizioValidazione = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.SERVIZIO_VALIDAZIONE);
        ricorrenza = Types.HIGH_INT_VAL;
        clone = Types.HIGH_CHAR_VAL;
        obbligatorieta = Types.HIGH_CHAR_VAL;
        valoreDefault = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.VALORE_DEFAULT);
    }

    public void initElementsStrDatoSpaces() {
        codiceDato = "";
        flagKey = Types.SPACE_CHAR;
        flagReturnCode = Types.SPACE_CHAR;
        flagCallUsing = Types.SPACE_CHAR;
        flagWhereCond = Types.SPACE_CHAR;
        flagRedefines = Types.SPACE_CHAR;
        tipoDato = "";
        inizioPosizione = Types.INVALID_INT_VAL;
        lunghezzaDato = Types.INVALID_INT_VAL;
        lunghezzaDatoNom = Types.INVALID_INT_VAL;
        precisioneDato = Types.INVALID_SHORT_VAL;
        formattazioneDato = "";
        servConversione = "";
        areaConvStandard = Types.SPACE_CHAR;
        codiceDominio = "";
        servizioValidazione = "";
        ricorrenza = Types.INVALID_INT_VAL;
        clone = Types.SPACE_CHAR;
        obbligatorieta = Types.SPACE_CHAR;
        valoreDefault = "";
    }

    public void setCodiceDato(String codiceDato) {
        this.codiceDato = Functions.subString(codiceDato, Len.CODICE_DATO);
    }

    public String getCodiceDato() {
        return this.codiceDato;
    }

    public void setFlagKey(char flagKey) {
        this.flagKey = flagKey;
    }

    public char getFlagKey() {
        return this.flagKey;
    }

    public void setFlagReturnCode(char flagReturnCode) {
        this.flagReturnCode = flagReturnCode;
    }

    public char getFlagReturnCode() {
        return this.flagReturnCode;
    }

    public void setFlagCallUsing(char flagCallUsing) {
        this.flagCallUsing = flagCallUsing;
    }

    public char getFlagCallUsing() {
        return this.flagCallUsing;
    }

    public void setFlagWhereCond(char flagWhereCond) {
        this.flagWhereCond = flagWhereCond;
    }

    public char getFlagWhereCond() {
        return this.flagWhereCond;
    }

    public void setFlagRedefines(char flagRedefines) {
        this.flagRedefines = flagRedefines;
    }

    public char getFlagRedefines() {
        return this.flagRedefines;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = Functions.subString(tipoDato, Len.TIPO_DATO);
    }

    public String getTipoDato() {
        return this.tipoDato;
    }

    public void setInizioPosizione(int inizioPosizione) {
        this.inizioPosizione = inizioPosizione;
    }

    public int getInizioPosizione() {
        return this.inizioPosizione;
    }

    public void setLunghezzaDato(int lunghezzaDato) {
        this.lunghezzaDato = lunghezzaDato;
    }

    public int getLunghezzaDato() {
        return this.lunghezzaDato;
    }

    public void setLunghezzaDatoNom(int lunghezzaDatoNom) {
        this.lunghezzaDatoNom = lunghezzaDatoNom;
    }

    public int getLunghezzaDatoNom() {
        return this.lunghezzaDatoNom;
    }

    public void setPrecisioneDato(short precisioneDato) {
        this.precisioneDato = precisioneDato;
    }

    public short getPrecisioneDato() {
        return this.precisioneDato;
    }

    public void setFormattazioneDato(String formattazioneDato) {
        this.formattazioneDato = Functions.subString(formattazioneDato, Len.FORMATTAZIONE_DATO);
    }

    public String getFormattazioneDato() {
        return this.formattazioneDato;
    }

    public void setServConversione(String servConversione) {
        this.servConversione = Functions.subString(servConversione, Len.SERV_CONVERSIONE);
    }

    public String getServConversione() {
        return this.servConversione;
    }

    public void setAreaConvStandard(char areaConvStandard) {
        this.areaConvStandard = areaConvStandard;
    }

    public char getAreaConvStandard() {
        return this.areaConvStandard;
    }

    public void setCodiceDominio(String codiceDominio) {
        this.codiceDominio = Functions.subString(codiceDominio, Len.CODICE_DOMINIO);
    }

    public String getCodiceDominio() {
        return this.codiceDominio;
    }

    public void setServizioValidazione(String servizioValidazione) {
        this.servizioValidazione = Functions.subString(servizioValidazione, Len.SERVIZIO_VALIDAZIONE);
    }

    public String getServizioValidazione() {
        return this.servizioValidazione;
    }

    public void setRicorrenza(int ricorrenza) {
        this.ricorrenza = ricorrenza;
    }

    public int getRicorrenza() {
        return this.ricorrenza;
    }

    public void setClone(char clone) {
        this.clone = clone;
    }

    public char getClone() {
        return this.clone;
    }

    public void setObbligatorieta(char obbligatorieta) {
        this.obbligatorieta = obbligatorieta;
    }

    public char getObbligatorieta() {
        return this.obbligatorieta;
    }

    public void setValoreDefault(String valoreDefault) {
        this.valoreDefault = Functions.subString(valoreDefault, Len.VALORE_DEFAULT);
    }

    public String getValoreDefault() {
        return this.valoreDefault;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_DATO = 30;
        public static final int TIPO_DATO = 2;
        public static final int FORMATTAZIONE_DATO = 20;
        public static final int SERV_CONVERSIONE = 8;
        public static final int CODICE_DOMINIO = 30;
        public static final int SERVIZIO_VALIDAZIONE = 8;
        public static final int VALORE_DEFAULT = 10;
        public static final int FLAG_KEY = 1;
        public static final int FLAG_RETURN_CODE = 1;
        public static final int FLAG_CALL_USING = 1;
        public static final int FLAG_WHERE_COND = 1;
        public static final int FLAG_REDEFINES = 1;
        public static final int INIZIO_POSIZIONE = 3;
        public static final int LUNGHEZZA_DATO = 3;
        public static final int LUNGHEZZA_DATO_NOM = 3;
        public static final int PRECISIONE_DATO = 2;
        public static final int AREA_CONV_STANDARD = 1;
        public static final int RICORRENZA = 3;
        public static final int CLONE = 1;
        public static final int OBBLIGATORIETA = 1;
        public static final int ELEMENTS_STR_DATO = CODICE_DATO + FLAG_KEY + FLAG_RETURN_CODE + FLAG_CALL_USING + FLAG_WHERE_COND + FLAG_REDEFINES + TIPO_DATO + INIZIO_POSIZIONE + LUNGHEZZA_DATO + LUNGHEZZA_DATO_NOM + PRECISIONE_DATO + FORMATTAZIONE_DATO + SERV_CONVERSIONE + AREA_CONV_STANDARD + CODICE_DOMINIO + SERVIZIO_VALIDAZIONE + RICORRENZA + CLONE + OBBLIGATORIETA + VALORE_DEFAULT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int INIZIO_POSIZIONE = 5;
            public static final int LUNGHEZZA_DATO = 5;
            public static final int LUNGHEZZA_DATO_NOM = 5;
            public static final int PRECISIONE_DATO = 2;
            public static final int RICORRENZA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
