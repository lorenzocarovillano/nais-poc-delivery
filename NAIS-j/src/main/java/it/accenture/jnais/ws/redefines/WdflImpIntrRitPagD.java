package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-INTR-RIT-PAG-D<br>
 * Variable: WDFL-IMP-INTR-RIT-PAG-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpIntrRitPagD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpIntrRitPagD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_INTR_RIT_PAG_D;
    }

    public void setWdflImpIntrRitPagD(AfDecimal wdflImpIntrRitPagD) {
        writeDecimalAsPacked(Pos.WDFL_IMP_INTR_RIT_PAG_D, wdflImpIntrRitPagD.copy());
    }

    public void setWdflImpIntrRitPagDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_INTR_RIT_PAG_D, Pos.WDFL_IMP_INTR_RIT_PAG_D);
    }

    /**Original name: WDFL-IMP-INTR-RIT-PAG-D<br>*/
    public AfDecimal getWdflImpIntrRitPagD() {
        return readPackedAsDecimal(Pos.WDFL_IMP_INTR_RIT_PAG_D, Len.Int.WDFL_IMP_INTR_RIT_PAG_D, Len.Fract.WDFL_IMP_INTR_RIT_PAG_D);
    }

    public byte[] getWdflImpIntrRitPagDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_INTR_RIT_PAG_D, Pos.WDFL_IMP_INTR_RIT_PAG_D);
        return buffer;
    }

    public void setWdflImpIntrRitPagDNull(String wdflImpIntrRitPagDNull) {
        writeString(Pos.WDFL_IMP_INTR_RIT_PAG_D_NULL, wdflImpIntrRitPagDNull, Len.WDFL_IMP_INTR_RIT_PAG_D_NULL);
    }

    /**Original name: WDFL-IMP-INTR-RIT-PAG-D-NULL<br>*/
    public String getWdflImpIntrRitPagDNull() {
        return readString(Pos.WDFL_IMP_INTR_RIT_PAG_D_NULL, Len.WDFL_IMP_INTR_RIT_PAG_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_INTR_RIT_PAG_D = 1;
        public static final int WDFL_IMP_INTR_RIT_PAG_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_INTR_RIT_PAG_D = 8;
        public static final int WDFL_IMP_INTR_RIT_PAG_D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_INTR_RIT_PAG_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_INTR_RIT_PAG_D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
