package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS2720<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs2720 {

    //==== PROPERTIES ====
    //Original name: IX-DCLGEN
    private short dclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short tabRan = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-STRINGA
    private short stringa = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-CONVERS
    private short convers = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-P58
    private int tabP58 = 0;

    //==== METHODS ====
    public void setDclgen(short dclgen) {
        this.dclgen = dclgen;
    }

    public short getDclgen() {
        return this.dclgen;
    }

    public void setTabRan(short tabRan) {
        this.tabRan = tabRan;
    }

    public short getTabRan() {
        return this.tabRan;
    }

    public void setStringa(short stringa) {
        this.stringa = stringa;
    }

    public short getStringa() {
        return this.stringa;
    }

    public void setConvers(short convers) {
        this.convers = convers;
    }

    public short getConvers() {
        return this.convers;
    }

    public void setTabP58(int tabP58) {
        this.tabP58 = tabP58;
    }

    public int getTabP58() {
        return this.tabP58;
    }
}
