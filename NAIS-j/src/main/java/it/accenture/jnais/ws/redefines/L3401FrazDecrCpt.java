package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-FRAZ-DECR-CPT<br>
 * Variable: L3401-FRAZ-DECR-CPT from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401FrazDecrCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401FrazDecrCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_FRAZ_DECR_CPT;
    }

    public void setL3401FrazDecrCpt(int l3401FrazDecrCpt) {
        writeIntAsPacked(Pos.L3401_FRAZ_DECR_CPT, l3401FrazDecrCpt, Len.Int.L3401_FRAZ_DECR_CPT);
    }

    public void setL3401FrazDecrCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_FRAZ_DECR_CPT, Pos.L3401_FRAZ_DECR_CPT);
    }

    /**Original name: L3401-FRAZ-DECR-CPT<br>*/
    public int getL3401FrazDecrCpt() {
        return readPackedAsInt(Pos.L3401_FRAZ_DECR_CPT, Len.Int.L3401_FRAZ_DECR_CPT);
    }

    public byte[] getL3401FrazDecrCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_FRAZ_DECR_CPT, Pos.L3401_FRAZ_DECR_CPT);
        return buffer;
    }

    /**Original name: L3401-FRAZ-DECR-CPT-NULL<br>*/
    public String getL3401FrazDecrCptNull() {
        return readString(Pos.L3401_FRAZ_DECR_CPT_NULL, Len.L3401_FRAZ_DECR_CPT_NULL);
    }

    public String getL3401FrazDecrCptNullFormatted() {
        return Functions.padBlanks(getL3401FrazDecrCptNull(), Len.L3401_FRAZ_DECR_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_FRAZ_DECR_CPT = 1;
        public static final int L3401_FRAZ_DECR_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_FRAZ_DECR_CPT = 3;
        public static final int L3401_FRAZ_DECR_CPT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_FRAZ_DECR_CPT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
