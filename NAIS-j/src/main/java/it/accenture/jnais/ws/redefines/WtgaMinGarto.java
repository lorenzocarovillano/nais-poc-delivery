package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-MIN-GARTO<br>
 * Variable: WTGA-MIN-GARTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaMinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaMinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_MIN_GARTO;
    }

    public void setWtgaMinGarto(AfDecimal wtgaMinGarto) {
        writeDecimalAsPacked(Pos.WTGA_MIN_GARTO, wtgaMinGarto.copy());
    }

    public void setWtgaMinGartoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_MIN_GARTO, Pos.WTGA_MIN_GARTO);
    }

    /**Original name: WTGA-MIN-GARTO<br>*/
    public AfDecimal getWtgaMinGarto() {
        return readPackedAsDecimal(Pos.WTGA_MIN_GARTO, Len.Int.WTGA_MIN_GARTO, Len.Fract.WTGA_MIN_GARTO);
    }

    public byte[] getWtgaMinGartoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_MIN_GARTO, Pos.WTGA_MIN_GARTO);
        return buffer;
    }

    public void initWtgaMinGartoSpaces() {
        fill(Pos.WTGA_MIN_GARTO, Len.WTGA_MIN_GARTO, Types.SPACE_CHAR);
    }

    public void setWtgaMinGartoNull(String wtgaMinGartoNull) {
        writeString(Pos.WTGA_MIN_GARTO_NULL, wtgaMinGartoNull, Len.WTGA_MIN_GARTO_NULL);
    }

    /**Original name: WTGA-MIN-GARTO-NULL<br>*/
    public String getWtgaMinGartoNull() {
        return readString(Pos.WTGA_MIN_GARTO_NULL, Len.WTGA_MIN_GARTO_NULL);
    }

    public String getWtgaMinGartoNullFormatted() {
        return Functions.padBlanks(getWtgaMinGartoNull(), Len.WTGA_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_MIN_GARTO = 1;
        public static final int WTGA_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_MIN_GARTO = 8;
        public static final int WTGA_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_MIN_GARTO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_MIN_GARTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
