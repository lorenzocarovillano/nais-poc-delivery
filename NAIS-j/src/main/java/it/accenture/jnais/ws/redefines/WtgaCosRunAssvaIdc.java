package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-COS-RUN-ASSVA-IDC<br>
 * Variable: WTGA-COS-RUN-ASSVA-IDC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCosRunAssvaIdc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCosRunAssvaIdc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_COS_RUN_ASSVA_IDC;
    }

    public void setWtgaCosRunAssvaIdc(AfDecimal wtgaCosRunAssvaIdc) {
        writeDecimalAsPacked(Pos.WTGA_COS_RUN_ASSVA_IDC, wtgaCosRunAssvaIdc.copy());
    }

    public void setWtgaCosRunAssvaIdcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_COS_RUN_ASSVA_IDC, Pos.WTGA_COS_RUN_ASSVA_IDC);
    }

    /**Original name: WTGA-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getWtgaCosRunAssvaIdc() {
        return readPackedAsDecimal(Pos.WTGA_COS_RUN_ASSVA_IDC, Len.Int.WTGA_COS_RUN_ASSVA_IDC, Len.Fract.WTGA_COS_RUN_ASSVA_IDC);
    }

    public byte[] getWtgaCosRunAssvaIdcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_COS_RUN_ASSVA_IDC, Pos.WTGA_COS_RUN_ASSVA_IDC);
        return buffer;
    }

    public void initWtgaCosRunAssvaIdcSpaces() {
        fill(Pos.WTGA_COS_RUN_ASSVA_IDC, Len.WTGA_COS_RUN_ASSVA_IDC, Types.SPACE_CHAR);
    }

    public void setWtgaCosRunAssvaIdcNull(String wtgaCosRunAssvaIdcNull) {
        writeString(Pos.WTGA_COS_RUN_ASSVA_IDC_NULL, wtgaCosRunAssvaIdcNull, Len.WTGA_COS_RUN_ASSVA_IDC_NULL);
    }

    /**Original name: WTGA-COS-RUN-ASSVA-IDC-NULL<br>*/
    public String getWtgaCosRunAssvaIdcNull() {
        return readString(Pos.WTGA_COS_RUN_ASSVA_IDC_NULL, Len.WTGA_COS_RUN_ASSVA_IDC_NULL);
    }

    public String getWtgaCosRunAssvaIdcNullFormatted() {
        return Functions.padBlanks(getWtgaCosRunAssvaIdcNull(), Len.WTGA_COS_RUN_ASSVA_IDC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_COS_RUN_ASSVA_IDC = 1;
        public static final int WTGA_COS_RUN_ASSVA_IDC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_COS_RUN_ASSVA_IDC = 8;
        public static final int WTGA_COS_RUN_ASSVA_IDC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
