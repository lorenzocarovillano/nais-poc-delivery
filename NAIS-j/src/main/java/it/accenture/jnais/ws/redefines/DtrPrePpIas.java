package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PRE-PP-IAS<br>
 * Variable: DTR-PRE-PP-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PRE_PP_IAS;
    }

    public void setDtrPrePpIas(AfDecimal dtrPrePpIas) {
        writeDecimalAsPacked(Pos.DTR_PRE_PP_IAS, dtrPrePpIas.copy());
    }

    public void setDtrPrePpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PRE_PP_IAS, Pos.DTR_PRE_PP_IAS);
    }

    /**Original name: DTR-PRE-PP-IAS<br>*/
    public AfDecimal getDtrPrePpIas() {
        return readPackedAsDecimal(Pos.DTR_PRE_PP_IAS, Len.Int.DTR_PRE_PP_IAS, Len.Fract.DTR_PRE_PP_IAS);
    }

    public byte[] getDtrPrePpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PRE_PP_IAS, Pos.DTR_PRE_PP_IAS);
        return buffer;
    }

    public void setDtrPrePpIasNull(String dtrPrePpIasNull) {
        writeString(Pos.DTR_PRE_PP_IAS_NULL, dtrPrePpIasNull, Len.DTR_PRE_PP_IAS_NULL);
    }

    /**Original name: DTR-PRE-PP-IAS-NULL<br>*/
    public String getDtrPrePpIasNull() {
        return readString(Pos.DTR_PRE_PP_IAS_NULL, Len.DTR_PRE_PP_IAS_NULL);
    }

    public String getDtrPrePpIasNullFormatted() {
        return Functions.padBlanks(getDtrPrePpIasNull(), Len.DTR_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PRE_PP_IAS = 1;
        public static final int DTR_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PRE_PP_IAS = 8;
        public static final int DTR_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
