package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-PRSTZ-LRD-ANTE-IS<br>
 * Variable: ISO-PRSTZ-LRD-ANTE-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoPrstzLrdAnteIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoPrstzLrdAnteIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_PRSTZ_LRD_ANTE_IS;
    }

    public void setIsoPrstzLrdAnteIs(AfDecimal isoPrstzLrdAnteIs) {
        writeDecimalAsPacked(Pos.ISO_PRSTZ_LRD_ANTE_IS, isoPrstzLrdAnteIs.copy());
    }

    public void setIsoPrstzLrdAnteIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_PRSTZ_LRD_ANTE_IS, Pos.ISO_PRSTZ_LRD_ANTE_IS);
    }

    /**Original name: ISO-PRSTZ-LRD-ANTE-IS<br>*/
    public AfDecimal getIsoPrstzLrdAnteIs() {
        return readPackedAsDecimal(Pos.ISO_PRSTZ_LRD_ANTE_IS, Len.Int.ISO_PRSTZ_LRD_ANTE_IS, Len.Fract.ISO_PRSTZ_LRD_ANTE_IS);
    }

    public byte[] getIsoPrstzLrdAnteIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_PRSTZ_LRD_ANTE_IS, Pos.ISO_PRSTZ_LRD_ANTE_IS);
        return buffer;
    }

    public void setIsoPrstzLrdAnteIsNull(String isoPrstzLrdAnteIsNull) {
        writeString(Pos.ISO_PRSTZ_LRD_ANTE_IS_NULL, isoPrstzLrdAnteIsNull, Len.ISO_PRSTZ_LRD_ANTE_IS_NULL);
    }

    /**Original name: ISO-PRSTZ-LRD-ANTE-IS-NULL<br>*/
    public String getIsoPrstzLrdAnteIsNull() {
        return readString(Pos.ISO_PRSTZ_LRD_ANTE_IS_NULL, Len.ISO_PRSTZ_LRD_ANTE_IS_NULL);
    }

    public String getIsoPrstzLrdAnteIsNullFormatted() {
        return Functions.padBlanks(getIsoPrstzLrdAnteIsNull(), Len.ISO_PRSTZ_LRD_ANTE_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_PRSTZ_LRD_ANTE_IS = 1;
        public static final int ISO_PRSTZ_LRD_ANTE_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_PRSTZ_LRD_ANTE_IS = 8;
        public static final int ISO_PRSTZ_LRD_ANTE_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_PRSTZ_LRD_ANTE_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_PRSTZ_LRD_ANTE_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
