package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-MIN-GARTO<br>
 * Variable: WRST-RIS-MIN-GARTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisMinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisMinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_MIN_GARTO;
    }

    public void setWrstRisMinGarto(AfDecimal wrstRisMinGarto) {
        writeDecimalAsPacked(Pos.WRST_RIS_MIN_GARTO, wrstRisMinGarto.copy());
    }

    public void setWrstRisMinGartoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_MIN_GARTO, Pos.WRST_RIS_MIN_GARTO);
    }

    /**Original name: WRST-RIS-MIN-GARTO<br>*/
    public AfDecimal getWrstRisMinGarto() {
        return readPackedAsDecimal(Pos.WRST_RIS_MIN_GARTO, Len.Int.WRST_RIS_MIN_GARTO, Len.Fract.WRST_RIS_MIN_GARTO);
    }

    public byte[] getWrstRisMinGartoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_MIN_GARTO, Pos.WRST_RIS_MIN_GARTO);
        return buffer;
    }

    public void initWrstRisMinGartoSpaces() {
        fill(Pos.WRST_RIS_MIN_GARTO, Len.WRST_RIS_MIN_GARTO, Types.SPACE_CHAR);
    }

    public void setWrstRisMinGartoNull(String wrstRisMinGartoNull) {
        writeString(Pos.WRST_RIS_MIN_GARTO_NULL, wrstRisMinGartoNull, Len.WRST_RIS_MIN_GARTO_NULL);
    }

    /**Original name: WRST-RIS-MIN-GARTO-NULL<br>*/
    public String getWrstRisMinGartoNull() {
        return readString(Pos.WRST_RIS_MIN_GARTO_NULL, Len.WRST_RIS_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MIN_GARTO = 1;
        public static final int WRST_RIS_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MIN_GARTO = 8;
        public static final int WRST_RIS_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MIN_GARTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MIN_GARTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
