package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COEFF-RIS-2-T<br>
 * Variable: WB03-COEFF-RIS-2-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CoeffRis2T extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CoeffRis2T() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COEFF_RIS2_T;
    }

    public void setWb03CoeffRis2T(AfDecimal wb03CoeffRis2T) {
        writeDecimalAsPacked(Pos.WB03_COEFF_RIS2_T, wb03CoeffRis2T.copy());
    }

    public void setWb03CoeffRis2TFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COEFF_RIS2_T, Pos.WB03_COEFF_RIS2_T);
    }

    /**Original name: WB03-COEFF-RIS-2-T<br>*/
    public AfDecimal getWb03CoeffRis2T() {
        return readPackedAsDecimal(Pos.WB03_COEFF_RIS2_T, Len.Int.WB03_COEFF_RIS2_T, Len.Fract.WB03_COEFF_RIS2_T);
    }

    public byte[] getWb03CoeffRis2TAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COEFF_RIS2_T, Pos.WB03_COEFF_RIS2_T);
        return buffer;
    }

    public void setWb03CoeffRis2TNull(String wb03CoeffRis2TNull) {
        writeString(Pos.WB03_COEFF_RIS2_T_NULL, wb03CoeffRis2TNull, Len.WB03_COEFF_RIS2_T_NULL);
    }

    /**Original name: WB03-COEFF-RIS-2-T-NULL<br>*/
    public String getWb03CoeffRis2TNull() {
        return readString(Pos.WB03_COEFF_RIS2_T_NULL, Len.WB03_COEFF_RIS2_T_NULL);
    }

    public String getWb03CoeffRis2TNullFormatted() {
        return Functions.padBlanks(getWb03CoeffRis2TNull(), Len.WB03_COEFF_RIS2_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_RIS2_T = 1;
        public static final int WB03_COEFF_RIS2_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_RIS2_T = 8;
        public static final int WB03_COEFF_RIS2_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_RIS2_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_RIS2_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
