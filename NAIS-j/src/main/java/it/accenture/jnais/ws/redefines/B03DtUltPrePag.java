package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-ULT-PRE-PAG<br>
 * Variable: B03-DT-ULT-PRE-PAG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtUltPrePag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtUltPrePag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_ULT_PRE_PAG;
    }

    public void setB03DtUltPrePag(int b03DtUltPrePag) {
        writeIntAsPacked(Pos.B03_DT_ULT_PRE_PAG, b03DtUltPrePag, Len.Int.B03_DT_ULT_PRE_PAG);
    }

    public void setB03DtUltPrePagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_ULT_PRE_PAG, Pos.B03_DT_ULT_PRE_PAG);
    }

    /**Original name: B03-DT-ULT-PRE-PAG<br>*/
    public int getB03DtUltPrePag() {
        return readPackedAsInt(Pos.B03_DT_ULT_PRE_PAG, Len.Int.B03_DT_ULT_PRE_PAG);
    }

    public byte[] getB03DtUltPrePagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_ULT_PRE_PAG, Pos.B03_DT_ULT_PRE_PAG);
        return buffer;
    }

    public void setB03DtUltPrePagNull(String b03DtUltPrePagNull) {
        writeString(Pos.B03_DT_ULT_PRE_PAG_NULL, b03DtUltPrePagNull, Len.B03_DT_ULT_PRE_PAG_NULL);
    }

    /**Original name: B03-DT-ULT-PRE-PAG-NULL<br>*/
    public String getB03DtUltPrePagNull() {
        return readString(Pos.B03_DT_ULT_PRE_PAG_NULL, Len.B03_DT_ULT_PRE_PAG_NULL);
    }

    public String getB03DtUltPrePagNullFormatted() {
        return Functions.padBlanks(getB03DtUltPrePagNull(), Len.B03_DT_ULT_PRE_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_ULT_PRE_PAG = 1;
        public static final int B03_DT_ULT_PRE_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_ULT_PRE_PAG = 5;
        public static final int B03_DT_ULT_PRE_PAG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_ULT_PRE_PAG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
