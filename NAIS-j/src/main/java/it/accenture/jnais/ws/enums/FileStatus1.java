package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FILE-STATUS<br>
 * Variable: FILE-STATUS from copybook IABCSQ99<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FileStatus1 {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FILE_STATUS);
    public static final String OK = "00";
    public static final String END_OF_FILE = "10";
    public static final String DUPLICATE_KEY = "22";
    public static final String FILE_NOT_PRESENT = "35";
    public static final String DEF_FD_NOT_VALID = "39";
    public static final String FILE_NOT_OPEN = "49";

    //==== METHODS ====
    public void setFileStatus(String fileStatus) {
        this.value = Functions.subString(fileStatus, Len.FILE_STATUS);
    }

    public String getFileStatus() {
        return this.value;
    }

    public boolean isFileStatusOk() {
        return value.equals(OK);
    }

    public boolean isFileStatusEndOfFile() {
        return value.equals(END_OF_FILE);
    }

    public void setFileStatusEndOfFile() {
        value = END_OF_FILE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FILE_STATUS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
