package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-ETA-AA-1O-ASSTO<br>
 * Variable: WGRZ-ETA-AA-1O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzEtaAa1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzEtaAa1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ETA_AA1O_ASSTO;
    }

    public void setWgrzEtaAa1oAssto(short wgrzEtaAa1oAssto) {
        writeShortAsPacked(Pos.WGRZ_ETA_AA1O_ASSTO, wgrzEtaAa1oAssto, Len.Int.WGRZ_ETA_AA1O_ASSTO);
    }

    public void setWgrzEtaAa1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ETA_AA1O_ASSTO, Pos.WGRZ_ETA_AA1O_ASSTO);
    }

    /**Original name: WGRZ-ETA-AA-1O-ASSTO<br>*/
    public short getWgrzEtaAa1oAssto() {
        return readPackedAsShort(Pos.WGRZ_ETA_AA1O_ASSTO, Len.Int.WGRZ_ETA_AA1O_ASSTO);
    }

    public byte[] getWgrzEtaAa1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ETA_AA1O_ASSTO, Pos.WGRZ_ETA_AA1O_ASSTO);
        return buffer;
    }

    public void initWgrzEtaAa1oAsstoSpaces() {
        fill(Pos.WGRZ_ETA_AA1O_ASSTO, Len.WGRZ_ETA_AA1O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzEtaAa1oAsstoNull(String wgrzEtaAa1oAsstoNull) {
        writeString(Pos.WGRZ_ETA_AA1O_ASSTO_NULL, wgrzEtaAa1oAsstoNull, Len.WGRZ_ETA_AA1O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getWgrzEtaAa1oAsstoNull() {
        return readString(Pos.WGRZ_ETA_AA1O_ASSTO_NULL, Len.WGRZ_ETA_AA1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_AA1O_ASSTO = 1;
        public static final int WGRZ_ETA_AA1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_AA1O_ASSTO = 2;
        public static final int WGRZ_ETA_AA1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ETA_AA1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
