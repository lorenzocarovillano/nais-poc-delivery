package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MOV-ID-MOVI-ANN<br>
 * Variable: MOV-ID-MOVI-ANN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MovIdMoviAnn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MovIdMoviAnn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOV_ID_MOVI_ANN;
    }

    public void setMovIdMoviAnn(int movIdMoviAnn) {
        writeIntAsPacked(Pos.MOV_ID_MOVI_ANN, movIdMoviAnn, Len.Int.MOV_ID_MOVI_ANN);
    }

    public void setMovIdMoviAnnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MOV_ID_MOVI_ANN, Pos.MOV_ID_MOVI_ANN);
    }

    /**Original name: MOV-ID-MOVI-ANN<br>*/
    public int getMovIdMoviAnn() {
        return readPackedAsInt(Pos.MOV_ID_MOVI_ANN, Len.Int.MOV_ID_MOVI_ANN);
    }

    public byte[] getMovIdMoviAnnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MOV_ID_MOVI_ANN, Pos.MOV_ID_MOVI_ANN);
        return buffer;
    }

    public void setMovIdMoviAnnNull(String movIdMoviAnnNull) {
        writeString(Pos.MOV_ID_MOVI_ANN_NULL, movIdMoviAnnNull, Len.MOV_ID_MOVI_ANN_NULL);
    }

    /**Original name: MOV-ID-MOVI-ANN-NULL<br>*/
    public String getMovIdMoviAnnNull() {
        return readString(Pos.MOV_ID_MOVI_ANN_NULL, Len.MOV_ID_MOVI_ANN_NULL);
    }

    public String getMovIdMoviAnnNullFormatted() {
        return Functions.padBlanks(getMovIdMoviAnnNull(), Len.MOV_ID_MOVI_ANN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MOV_ID_MOVI_ANN = 1;
        public static final int MOV_ID_MOVI_ANN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_ID_MOVI_ANN = 5;
        public static final int MOV_ID_MOVI_ANN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV_ID_MOVI_ANN = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
