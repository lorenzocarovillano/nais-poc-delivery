package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Lccc006TrattDati;
import it.accenture.jnais.ws.redefines.Ispc0140CompValGenericoP;

/**Original name: ISPC0140-COMPONENTE-P<br>
 * Variables: ISPC0140-COMPONENTE-P from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140ComponenteP {

    //==== PROPERTIES ====
    //Original name: ISPC0140-CODICE-COMPONENTE-P
    private String ispc0140CodiceComponenteP = DefaultValues.stringVal(Len.ISPC0140_CODICE_COMPONENTE_P);
    //Original name: ISPC0140-DESCR-COMPONENTE-P
    private String ispc0140DescrComponenteP = DefaultValues.stringVal(Len.ISPC0140_DESCR_COMPONENTE_P);
    //Original name: ISPC0140-COMP-TIPO-DATO-P
    private char ispc0140CompTipoDatoP = DefaultValues.CHAR_VAL;
    //Original name: ISPC0140-COMP-VAL-GENERICO-P
    private Ispc0140CompValGenericoP ispc0140CompValGenericoP = new Ispc0140CompValGenericoP();
    //Original name: ISPC0140-TRATTAMENTO-PROVV-P
    private Lccc006TrattDati ispc0140TrattamentoProvvP = new Lccc006TrattDati();

    //==== METHODS ====
    public void setComponentePBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0140CodiceComponenteP = MarshalByte.readString(buffer, position, Len.ISPC0140_CODICE_COMPONENTE_P);
        position += Len.ISPC0140_CODICE_COMPONENTE_P;
        ispc0140DescrComponenteP = MarshalByte.readString(buffer, position, Len.ISPC0140_DESCR_COMPONENTE_P);
        position += Len.ISPC0140_DESCR_COMPONENTE_P;
        ispc0140CompTipoDatoP = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ispc0140CompValGenericoP.setIspc0140CompValGenericoPFromBuffer(buffer, position);
        position += Ispc0140CompValGenericoP.Len.ISPC0140_COMP_VAL_GENERICO_P;
        ispc0140TrattamentoProvvP.setFlagModCalcPreP(MarshalByte.readChar(buffer, position));
    }

    public byte[] getComponentePBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ispc0140CodiceComponenteP, Len.ISPC0140_CODICE_COMPONENTE_P);
        position += Len.ISPC0140_CODICE_COMPONENTE_P;
        MarshalByte.writeString(buffer, position, ispc0140DescrComponenteP, Len.ISPC0140_DESCR_COMPONENTE_P);
        position += Len.ISPC0140_DESCR_COMPONENTE_P;
        MarshalByte.writeChar(buffer, position, ispc0140CompTipoDatoP);
        position += Types.CHAR_SIZE;
        ispc0140CompValGenericoP.getIspc0140CompValGenericoPAsBuffer(buffer, position);
        position += Ispc0140CompValGenericoP.Len.ISPC0140_COMP_VAL_GENERICO_P;
        MarshalByte.writeChar(buffer, position, ispc0140TrattamentoProvvP.getFlagModCalcPreP());
        return buffer;
    }

    public void initComponentePSpaces() {
        ispc0140CodiceComponenteP = "";
        ispc0140DescrComponenteP = "";
        ispc0140CompTipoDatoP = Types.SPACE_CHAR;
        ispc0140CompValGenericoP.initIspc0140CompValGenericoPSpaces();
        ispc0140TrattamentoProvvP.setFlagModCalcPreP(Types.SPACE_CHAR);
    }

    public void setIspc0140CodiceComponenteP(String ispc0140CodiceComponenteP) {
        this.ispc0140CodiceComponenteP = Functions.subString(ispc0140CodiceComponenteP, Len.ISPC0140_CODICE_COMPONENTE_P);
    }

    public String getIspc0140CodiceComponenteP() {
        return this.ispc0140CodiceComponenteP;
    }

    public void setIspc0140DescrComponenteP(String ispc0140DescrComponenteP) {
        this.ispc0140DescrComponenteP = Functions.subString(ispc0140DescrComponenteP, Len.ISPC0140_DESCR_COMPONENTE_P);
    }

    public String getIspc0140DescrComponenteP() {
        return this.ispc0140DescrComponenteP;
    }

    public void setIspc0140CompTipoDatoP(char ispc0140CompTipoDatoP) {
        this.ispc0140CompTipoDatoP = ispc0140CompTipoDatoP;
    }

    public char getIspc0140CompTipoDatoP() {
        return this.ispc0140CompTipoDatoP;
    }

    public Ispc0140CompValGenericoP getIspc0140CompValGenericoP() {
        return ispc0140CompValGenericoP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ISPC0140_CODICE_COMPONENTE_P = 12;
        public static final int ISPC0140_DESCR_COMPONENTE_P = 30;
        public static final int ISPC0140_COMP_TIPO_DATO_P = 1;
        public static final int COMPONENTE_P = ISPC0140_CODICE_COMPONENTE_P + ISPC0140_DESCR_COMPONENTE_P + ISPC0140_COMP_TIPO_DATO_P + Ispc0140CompValGenericoP.Len.ISPC0140_COMP_VAL_GENERICO_P + Lccc006TrattDati.Len.FLAG_MOD_CALC_PRE_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
