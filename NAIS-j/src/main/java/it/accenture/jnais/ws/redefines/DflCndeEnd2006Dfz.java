package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNDE-END2006-DFZ<br>
 * Variable: DFL-CNDE-END2006-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCndeEnd2006Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCndeEnd2006Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNDE_END2006_DFZ;
    }

    public void setDflCndeEnd2006Dfz(AfDecimal dflCndeEnd2006Dfz) {
        writeDecimalAsPacked(Pos.DFL_CNDE_END2006_DFZ, dflCndeEnd2006Dfz.copy());
    }

    public void setDflCndeEnd2006DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNDE_END2006_DFZ, Pos.DFL_CNDE_END2006_DFZ);
    }

    /**Original name: DFL-CNDE-END2006-DFZ<br>*/
    public AfDecimal getDflCndeEnd2006Dfz() {
        return readPackedAsDecimal(Pos.DFL_CNDE_END2006_DFZ, Len.Int.DFL_CNDE_END2006_DFZ, Len.Fract.DFL_CNDE_END2006_DFZ);
    }

    public byte[] getDflCndeEnd2006DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNDE_END2006_DFZ, Pos.DFL_CNDE_END2006_DFZ);
        return buffer;
    }

    public void setDflCndeEnd2006DfzNull(String dflCndeEnd2006DfzNull) {
        writeString(Pos.DFL_CNDE_END2006_DFZ_NULL, dflCndeEnd2006DfzNull, Len.DFL_CNDE_END2006_DFZ_NULL);
    }

    /**Original name: DFL-CNDE-END2006-DFZ-NULL<br>*/
    public String getDflCndeEnd2006DfzNull() {
        return readString(Pos.DFL_CNDE_END2006_DFZ_NULL, Len.DFL_CNDE_END2006_DFZ_NULL);
    }

    public String getDflCndeEnd2006DfzNullFormatted() {
        return Functions.padBlanks(getDflCndeEnd2006DfzNull(), Len.DFL_CNDE_END2006_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_END2006_DFZ = 1;
        public static final int DFL_CNDE_END2006_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_END2006_DFZ = 8;
        public static final int DFL_CNDE_END2006_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_END2006_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_END2006_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
