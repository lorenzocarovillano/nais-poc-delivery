package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUJUL-X<br>
 * Variable: A2K-OUJUL-X from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class A2kOujulX {

    //==== PROPERTIES ====
    //Original name: A2K-OUJSS
    private String oujss = DefaultValues.stringVal(Len.OUJSS);
    //Original name: A2K-OUJAA
    private String oujaa = DefaultValues.stringVal(Len.OUJAA);
    //Original name: A2K-OUJGGG
    private String oujggg = DefaultValues.stringVal(Len.OUJGGG);

    //==== METHODS ====
    public void setA2kOujulFormatted(String a2kOujul) {
        int position = 1;
        byte[] buffer = getA2kOujulXBytes();
        MarshalByte.writeString(buffer, position, Trunc.toNumeric(a2kOujul, Len.A2K_OUJUL), Len.A2K_OUJUL);
        setA2kOujulXBytes(buffer);
    }

    /**Original name: A2K-OUJUL<br>
	 * <pre>                                            formato AAAAGGG giuli</pre>*/
    public int getA2kOujul() {
        int position = 1;
        return MarshalByte.readInt(getA2kOujulXBytes(), position, Len.Int.A2K_OUJUL, SignType.NO_SIGN);
    }

    public void setA2kOujulXBytes(byte[] buffer) {
        setA2kOujulXBytes(buffer, 1);
    }

    /**Original name: A2K-OUJUL-X<br>*/
    public byte[] getA2kOujulXBytes() {
        byte[] buffer = new byte[Len.A2K_OUJUL_X];
        return getA2kOujulXBytes(buffer, 1);
    }

    public void setA2kOujulXBytes(byte[] buffer, int offset) {
        int position = offset;
        oujss = MarshalByte.readFixedString(buffer, position, Len.OUJSS);
        position += Len.OUJSS;
        oujaa = MarshalByte.readFixedString(buffer, position, Len.OUJAA);
        position += Len.OUJAA;
        oujggg = MarshalByte.readFixedString(buffer, position, Len.OUJGGG);
    }

    public byte[] getA2kOujulXBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, oujss, Len.OUJSS);
        position += Len.OUJSS;
        MarshalByte.writeString(buffer, position, oujaa, Len.OUJAA);
        position += Len.OUJAA;
        MarshalByte.writeString(buffer, position, oujggg, Len.OUJGGG);
        return buffer;
    }

    public void initA2kOujulXSpaces() {
        oujss = "";
        oujaa = "";
        oujggg = "";
    }

    public void setOujssFormatted(String oujss) {
        this.oujss = Trunc.toUnsignedNumeric(oujss, Len.OUJSS);
    }

    public short getOujss() {
        return NumericDisplay.asShort(this.oujss);
    }

    public void setOujaaFormatted(String oujaa) {
        this.oujaa = Trunc.toUnsignedNumeric(oujaa, Len.OUJAA);
    }

    public short getOujaa() {
        return NumericDisplay.asShort(this.oujaa);
    }

    public void setOujggg(short oujggg) {
        this.oujggg = NumericDisplay.asString(oujggg, Len.OUJGGG);
    }

    public short getOujggg() {
        return NumericDisplay.asShort(this.oujggg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUJSS = 2;
        public static final int OUJAA = 2;
        public static final int OUJGGG = 3;
        public static final int A2K_OUJUL_X = OUJSS + OUJAA + OUJGGG;
        public static final int A2K_OUJUL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A2K_OUJUL = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
