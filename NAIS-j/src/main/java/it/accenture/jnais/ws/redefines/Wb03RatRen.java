package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RAT-REN<br>
 * Variable: WB03-RAT-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RatRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RatRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RAT_REN;
    }

    public void setWb03RatRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RAT_REN, Pos.WB03_RAT_REN);
    }

    /**Original name: WB03-RAT-REN<br>*/
    public AfDecimal getWb03RatRen() {
        return readPackedAsDecimal(Pos.WB03_RAT_REN, Len.Int.WB03_RAT_REN, Len.Fract.WB03_RAT_REN);
    }

    public byte[] getWb03RatRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RAT_REN, Pos.WB03_RAT_REN);
        return buffer;
    }

    public void setWb03RatRenNull(String wb03RatRenNull) {
        writeString(Pos.WB03_RAT_REN_NULL, wb03RatRenNull, Len.WB03_RAT_REN_NULL);
    }

    /**Original name: WB03-RAT-REN-NULL<br>*/
    public String getWb03RatRenNull() {
        return readString(Pos.WB03_RAT_REN_NULL, Len.WB03_RAT_REN_NULL);
    }

    public String getWb03RatRenNullFormatted() {
        return Functions.padBlanks(getWb03RatRenNull(), Len.WB03_RAT_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RAT_REN = 1;
        public static final int WB03_RAT_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RAT_REN = 8;
        public static final int WB03_RAT_REN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RAT_REN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RAT_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
