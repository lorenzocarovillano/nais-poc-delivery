package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CODICE-FISCALE<br>
 * Variable: FLAG-CODICE-FISCALE from program LVVS2720<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCodiceFiscale {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCodiceFiscale(char flagCodiceFiscale) {
        this.value = flagCodiceFiscale;
    }

    public char getFlagCodiceFiscale() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
