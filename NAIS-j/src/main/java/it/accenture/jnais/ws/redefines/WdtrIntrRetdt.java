package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-INTR-RETDT<br>
 * Variable: WDTR-INTR-RETDT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_INTR_RETDT;
    }

    public void setWdtrIntrRetdt(AfDecimal wdtrIntrRetdt) {
        writeDecimalAsPacked(Pos.WDTR_INTR_RETDT, wdtrIntrRetdt.copy());
    }

    /**Original name: WDTR-INTR-RETDT<br>*/
    public AfDecimal getWdtrIntrRetdt() {
        return readPackedAsDecimal(Pos.WDTR_INTR_RETDT, Len.Int.WDTR_INTR_RETDT, Len.Fract.WDTR_INTR_RETDT);
    }

    public void setWdtrIntrRetdtNull(String wdtrIntrRetdtNull) {
        writeString(Pos.WDTR_INTR_RETDT_NULL, wdtrIntrRetdtNull, Len.WDTR_INTR_RETDT_NULL);
    }

    /**Original name: WDTR-INTR-RETDT-NULL<br>*/
    public String getWdtrIntrRetdtNull() {
        return readString(Pos.WDTR_INTR_RETDT_NULL, Len.WDTR_INTR_RETDT_NULL);
    }

    public String getWdtrIntrRetdtNullFormatted() {
        return Functions.padBlanks(getWdtrIntrRetdtNull(), Len.WDTR_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_RETDT = 1;
        public static final int WDTR_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_RETDT = 8;
        public static final int WDTR_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
