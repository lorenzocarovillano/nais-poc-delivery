package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-QTZ-TOT-EMIS<br>
 * Variable: WB03-QTZ-TOT-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03QtzTotEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03QtzTotEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_QTZ_TOT_EMIS;
    }

    public void setWb03QtzTotEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_QTZ_TOT_EMIS, Pos.WB03_QTZ_TOT_EMIS);
    }

    /**Original name: WB03-QTZ-TOT-EMIS<br>*/
    public AfDecimal getWb03QtzTotEmis() {
        return readPackedAsDecimal(Pos.WB03_QTZ_TOT_EMIS, Len.Int.WB03_QTZ_TOT_EMIS, Len.Fract.WB03_QTZ_TOT_EMIS);
    }

    public byte[] getWb03QtzTotEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_QTZ_TOT_EMIS, Pos.WB03_QTZ_TOT_EMIS);
        return buffer;
    }

    public void setWb03QtzTotEmisNull(String wb03QtzTotEmisNull) {
        writeString(Pos.WB03_QTZ_TOT_EMIS_NULL, wb03QtzTotEmisNull, Len.WB03_QTZ_TOT_EMIS_NULL);
    }

    /**Original name: WB03-QTZ-TOT-EMIS-NULL<br>*/
    public String getWb03QtzTotEmisNull() {
        return readString(Pos.WB03_QTZ_TOT_EMIS_NULL, Len.WB03_QTZ_TOT_EMIS_NULL);
    }

    public String getWb03QtzTotEmisNullFormatted() {
        return Functions.padBlanks(getWb03QtzTotEmisNull(), Len.WB03_QTZ_TOT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_TOT_EMIS = 1;
        public static final int WB03_QTZ_TOT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_TOT_EMIS = 7;
        public static final int WB03_QTZ_TOT_EMIS_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_TOT_EMIS = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_TOT_EMIS = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
