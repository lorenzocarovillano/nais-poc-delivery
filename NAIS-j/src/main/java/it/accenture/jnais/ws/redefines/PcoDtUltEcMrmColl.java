package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-MRM-COLL<br>
 * Variable: PCO-DT-ULT-EC-MRM-COLL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcMrmColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcMrmColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_MRM_COLL;
    }

    public void setPcoDtUltEcMrmColl(int pcoDtUltEcMrmColl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_MRM_COLL, pcoDtUltEcMrmColl, Len.Int.PCO_DT_ULT_EC_MRM_COLL);
    }

    public void setPcoDtUltEcMrmCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_MRM_COLL, Pos.PCO_DT_ULT_EC_MRM_COLL);
    }

    /**Original name: PCO-DT-ULT-EC-MRM-COLL<br>*/
    public int getPcoDtUltEcMrmColl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_MRM_COLL, Len.Int.PCO_DT_ULT_EC_MRM_COLL);
    }

    public byte[] getPcoDtUltEcMrmCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_MRM_COLL, Pos.PCO_DT_ULT_EC_MRM_COLL);
        return buffer;
    }

    public void initPcoDtUltEcMrmCollHighValues() {
        fill(Pos.PCO_DT_ULT_EC_MRM_COLL, Len.PCO_DT_ULT_EC_MRM_COLL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcMrmCollNull(String pcoDtUltEcMrmCollNull) {
        writeString(Pos.PCO_DT_ULT_EC_MRM_COLL_NULL, pcoDtUltEcMrmCollNull, Len.PCO_DT_ULT_EC_MRM_COLL_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-MRM-COLL-NULL<br>*/
    public String getPcoDtUltEcMrmCollNull() {
        return readString(Pos.PCO_DT_ULT_EC_MRM_COLL_NULL, Len.PCO_DT_ULT_EC_MRM_COLL_NULL);
    }

    public String getPcoDtUltEcMrmCollNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcMrmCollNull(), Len.PCO_DT_ULT_EC_MRM_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_MRM_COLL = 1;
        public static final int PCO_DT_ULT_EC_MRM_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_MRM_COLL = 5;
        public static final int PCO_DT_ULT_EC_MRM_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_MRM_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
