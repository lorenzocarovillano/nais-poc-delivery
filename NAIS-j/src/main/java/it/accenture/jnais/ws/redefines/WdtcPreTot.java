package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PRE-TOT<br>
 * Variable: WDTC-PRE-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PRE_TOT;
    }

    public void setWdtcPreTot(AfDecimal wdtcPreTot) {
        writeDecimalAsPacked(Pos.WDTC_PRE_TOT, wdtcPreTot.copy());
    }

    public void setWdtcPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PRE_TOT, Pos.WDTC_PRE_TOT);
    }

    /**Original name: WDTC-PRE-TOT<br>*/
    public AfDecimal getWdtcPreTot() {
        return readPackedAsDecimal(Pos.WDTC_PRE_TOT, Len.Int.WDTC_PRE_TOT, Len.Fract.WDTC_PRE_TOT);
    }

    public byte[] getWdtcPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PRE_TOT, Pos.WDTC_PRE_TOT);
        return buffer;
    }

    public void initWdtcPreTotSpaces() {
        fill(Pos.WDTC_PRE_TOT, Len.WDTC_PRE_TOT, Types.SPACE_CHAR);
    }

    public void setWdtcPreTotNull(String wdtcPreTotNull) {
        writeString(Pos.WDTC_PRE_TOT_NULL, wdtcPreTotNull, Len.WDTC_PRE_TOT_NULL);
    }

    /**Original name: WDTC-PRE-TOT-NULL<br>*/
    public String getWdtcPreTotNull() {
        return readString(Pos.WDTC_PRE_TOT_NULL, Len.WDTC_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_TOT = 1;
        public static final int WDTC_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_TOT = 8;
        public static final int WDTC_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
