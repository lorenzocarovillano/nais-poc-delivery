package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-IMP-TRASFE<br>
 * Variable: WDTR-IMP-TRASFE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_IMP_TRASFE;
    }

    public void setWdtrImpTrasfe(AfDecimal wdtrImpTrasfe) {
        writeDecimalAsPacked(Pos.WDTR_IMP_TRASFE, wdtrImpTrasfe.copy());
    }

    /**Original name: WDTR-IMP-TRASFE<br>*/
    public AfDecimal getWdtrImpTrasfe() {
        return readPackedAsDecimal(Pos.WDTR_IMP_TRASFE, Len.Int.WDTR_IMP_TRASFE, Len.Fract.WDTR_IMP_TRASFE);
    }

    public void setWdtrImpTrasfeNull(String wdtrImpTrasfeNull) {
        writeString(Pos.WDTR_IMP_TRASFE_NULL, wdtrImpTrasfeNull, Len.WDTR_IMP_TRASFE_NULL);
    }

    /**Original name: WDTR-IMP-TRASFE-NULL<br>*/
    public String getWdtrImpTrasfeNull() {
        return readString(Pos.WDTR_IMP_TRASFE_NULL, Len.WDTR_IMP_TRASFE_NULL);
    }

    public String getWdtrImpTrasfeNullFormatted() {
        return Functions.padBlanks(getWdtrImpTrasfeNull(), Len.WDTR_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_TRASFE = 1;
        public static final int WDTR_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_TRASFE = 8;
        public static final int WDTR_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
