package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: AREA-LOAS0350<br>
 * Variable: AREA-LOAS0350 from program LOAS0350<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaLoas0350 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: S350-FORMA-ASS
    private String formaAss = DefaultValues.stringVal(Len.FORMA_ASS);
    //Original name: S350-FL-TP-OPZ-SCAD
    private char flTpOpzScad = DefaultValues.CHAR_VAL;
    //Original name: S350-FL-BONUS-RICORRENTE
    private char flBonusRicorrente = DefaultValues.CHAR_VAL;
    //Original name: S350-FL-BONUS-FEDELTA
    private char flBonusFedelta = DefaultValues.CHAR_VAL;
    //Original name: S350-TP-ELAB-COSTI
    private String tpElabCosti = DefaultValues.stringVal(Len.TP_ELAB_COSTI);
    //Original name: S350-DT-ULT-ELABORAZIONE
    private String dtUltElaborazione = DefaultValues.stringVal(Len.DT_ULT_ELABORAZIONE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_LOAS0350;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaLoas0350Bytes(buf);
    }

    public String getAreaLoas350Formatted() {
        return getAreaDatiFormatted();
    }

    public void setAreaLoas0350Bytes(byte[] buffer) {
        setAreaLoas0350Bytes(buffer, 1);
    }

    public byte[] getAreaLoas0350Bytes() {
        byte[] buffer = new byte[Len.AREA_LOAS0350];
        return getAreaLoas0350Bytes(buffer, 1);
    }

    public void setAreaLoas0350Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaDatiBytes(buffer, position);
    }

    public byte[] getAreaLoas0350Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaDatiBytes(buffer, position);
        return buffer;
    }

    public String getAreaDatiFormatted() {
        return MarshalByteExt.bufferToStr(getAreaDatiBytes());
    }

    /**Original name: S350-AREA-DATI<br>
	 * <pre> allineamento 10/09/2008</pre>*/
    public byte[] getAreaDatiBytes() {
        byte[] buffer = new byte[Len.AREA_DATI];
        return getAreaDatiBytes(buffer, 1);
    }

    public void setAreaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        formaAss = MarshalByte.readString(buffer, position, Len.FORMA_ASS);
        position += Len.FORMA_ASS;
        flTpOpzScad = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flBonusRicorrente = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flBonusFedelta = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpElabCosti = MarshalByte.readString(buffer, position, Len.TP_ELAB_COSTI);
        position += Len.TP_ELAB_COSTI;
        dtUltElaborazione = MarshalByte.readFixedString(buffer, position, Len.DT_ULT_ELABORAZIONE);
    }

    public byte[] getAreaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, formaAss, Len.FORMA_ASS);
        position += Len.FORMA_ASS;
        MarshalByte.writeChar(buffer, position, flTpOpzScad);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flBonusRicorrente);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flBonusFedelta);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpElabCosti, Len.TP_ELAB_COSTI);
        position += Len.TP_ELAB_COSTI;
        MarshalByte.writeString(buffer, position, dtUltElaborazione, Len.DT_ULT_ELABORAZIONE);
        return buffer;
    }

    public void setFormaAss(String formaAss) {
        this.formaAss = Functions.subString(formaAss, Len.FORMA_ASS);
    }

    public String getFormaAss() {
        return this.formaAss;
    }

    public void setFlTpOpzScad(char flTpOpzScad) {
        this.flTpOpzScad = flTpOpzScad;
    }

    public char getFlTpOpzScad() {
        return this.flTpOpzScad;
    }

    public void setFlBonusRicorrente(char flBonusRicorrente) {
        this.flBonusRicorrente = flBonusRicorrente;
    }

    public char getFlBonusRicorrente() {
        return this.flBonusRicorrente;
    }

    public void setFlBonusFedelta(char flBonusFedelta) {
        this.flBonusFedelta = flBonusFedelta;
    }

    public char getFlBonusFedelta() {
        return this.flBonusFedelta;
    }

    public void setTpElabCosti(String tpElabCosti) {
        this.tpElabCosti = Functions.subString(tpElabCosti, Len.TP_ELAB_COSTI);
    }

    public String getTpElabCosti() {
        return this.tpElabCosti;
    }

    public void setDtUltElaborazioneFormatted(String dtUltElaborazione) {
        this.dtUltElaborazione = Trunc.toUnsignedNumeric(dtUltElaborazione, Len.DT_ULT_ELABORAZIONE);
    }

    public int getDtUltElaborazione() {
        return NumericDisplay.asInt(this.dtUltElaborazione);
    }

    @Override
    public byte[] serialize() {
        return getAreaLoas0350Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORMA_ASS = 2;
        public static final int FL_TP_OPZ_SCAD = 1;
        public static final int FL_BONUS_RICORRENTE = 1;
        public static final int FL_BONUS_FEDELTA = 1;
        public static final int TP_ELAB_COSTI = 2;
        public static final int DT_ULT_ELABORAZIONE = 8;
        public static final int AREA_DATI = FORMA_ASS + FL_TP_OPZ_SCAD + FL_BONUS_RICORRENTE + FL_BONUS_FEDELTA + TP_ELAB_COSTI + DT_ULT_ELABORAZIONE;
        public static final int AREA_LOAS0350 = AREA_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
