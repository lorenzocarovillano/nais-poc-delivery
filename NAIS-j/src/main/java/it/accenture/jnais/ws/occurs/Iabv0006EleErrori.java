package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Iabv0006DescType;

/**Original name: IABV0006-ELE-ERRORI<br>
 * Variables: IABV0006-ELE-ERRORI from copybook IABV0006<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Iabv0006EleErrori {

    //==== PROPERTIES ====
    //Original name: IABV0006-DESC-TYPE
    private Iabv0006DescType descType = new Iabv0006DescType();
    //Original name: IABV0006-CUSTOM-COUNT-DESC
    private String customCountDesc = DefaultValues.stringVal(Len.CUSTOM_COUNT_DESC);
    //Original name: IABV0006-CUSTOM-COUNT
    private String customCount = DefaultValues.stringVal(Len.CUSTOM_COUNT);

    //==== METHODS ====
    public void setEleErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        descType.setDescType(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        customCountDesc = MarshalByte.readString(buffer, position, Len.CUSTOM_COUNT_DESC);
        position += Len.CUSTOM_COUNT_DESC;
        customCount = MarshalByte.readFixedString(buffer, position, Len.CUSTOM_COUNT);
    }

    public byte[] getEleErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, descType.getDescType());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, customCountDesc, Len.CUSTOM_COUNT_DESC);
        position += Len.CUSTOM_COUNT_DESC;
        MarshalByte.writeString(buffer, position, customCount, Len.CUSTOM_COUNT);
        return buffer;
    }

    public void initEleErroriSpaces() {
        descType.setDescType(Types.SPACE_CHAR);
        customCountDesc = "";
        customCount = "";
    }

    public void setCustomCountDesc(String customCountDesc) {
        this.customCountDesc = Functions.subString(customCountDesc, Len.CUSTOM_COUNT_DESC);
    }

    public String getCustomCountDesc() {
        return this.customCountDesc;
    }

    public void setIabv0006CustomCount(int iabv0006CustomCount) {
        this.customCount = NumericDisplay.asString(iabv0006CustomCount, Len.CUSTOM_COUNT);
    }

    public void setIabv0006CustomCountFormatted(String iabv0006CustomCount) {
        this.customCount = Trunc.toUnsignedNumeric(iabv0006CustomCount, Len.CUSTOM_COUNT);
    }

    public int getIabv0006CustomCount() {
        return NumericDisplay.asInt(this.customCount);
    }

    public Iabv0006DescType getDescType() {
        return descType;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CUSTOM_COUNT_DESC = 50;
        public static final int CUSTOM_COUNT = 9;
        public static final int ELE_ERRORI = Iabv0006DescType.Len.DESC_TYPE + CUSTOM_COUNT_DESC + CUSTOM_COUNT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
