package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-AZ<br>
 * Variable: TIT-IMP-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_AZ;
    }

    public void setTitImpAz(AfDecimal titImpAz) {
        writeDecimalAsPacked(Pos.TIT_IMP_AZ, titImpAz.copy());
    }

    public void setTitImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_AZ, Pos.TIT_IMP_AZ);
    }

    /**Original name: TIT-IMP-AZ<br>*/
    public AfDecimal getTitImpAz() {
        return readPackedAsDecimal(Pos.TIT_IMP_AZ, Len.Int.TIT_IMP_AZ, Len.Fract.TIT_IMP_AZ);
    }

    public byte[] getTitImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_AZ, Pos.TIT_IMP_AZ);
        return buffer;
    }

    public void setTitImpAzNull(String titImpAzNull) {
        writeString(Pos.TIT_IMP_AZ_NULL, titImpAzNull, Len.TIT_IMP_AZ_NULL);
    }

    /**Original name: TIT-IMP-AZ-NULL<br>*/
    public String getTitImpAzNull() {
        return readString(Pos.TIT_IMP_AZ_NULL, Len.TIT_IMP_AZ_NULL);
    }

    public String getTitImpAzNullFormatted() {
        return Functions.padBlanks(getTitImpAzNull(), Len.TIT_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_AZ = 1;
        public static final int TIT_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_AZ = 8;
        public static final int TIT_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
