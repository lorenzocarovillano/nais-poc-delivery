package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRIF-DT-INVST-CALC<br>
 * Variable: WRIF-DT-INVST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifDtInvstCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifDtInvstCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_DT_INVST_CALC;
    }

    public void setWrifDtInvstCalc(int wrifDtInvstCalc) {
        writeIntAsPacked(Pos.WRIF_DT_INVST_CALC, wrifDtInvstCalc, Len.Int.WRIF_DT_INVST_CALC);
    }

    public void setWrifDtInvstCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_DT_INVST_CALC, Pos.WRIF_DT_INVST_CALC);
    }

    /**Original name: WRIF-DT-INVST-CALC<br>*/
    public int getWrifDtInvstCalc() {
        return readPackedAsInt(Pos.WRIF_DT_INVST_CALC, Len.Int.WRIF_DT_INVST_CALC);
    }

    public byte[] getWrifDtInvstCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_DT_INVST_CALC, Pos.WRIF_DT_INVST_CALC);
        return buffer;
    }

    public void initWrifDtInvstCalcSpaces() {
        fill(Pos.WRIF_DT_INVST_CALC, Len.WRIF_DT_INVST_CALC, Types.SPACE_CHAR);
    }

    public void setWrifDtInvstCalcNull(String wrifDtInvstCalcNull) {
        writeString(Pos.WRIF_DT_INVST_CALC_NULL, wrifDtInvstCalcNull, Len.WRIF_DT_INVST_CALC_NULL);
    }

    /**Original name: WRIF-DT-INVST-CALC-NULL<br>*/
    public String getWrifDtInvstCalcNull() {
        return readString(Pos.WRIF_DT_INVST_CALC_NULL, Len.WRIF_DT_INVST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_DT_INVST_CALC = 1;
        public static final int WRIF_DT_INVST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_DT_INVST_CALC = 5;
        public static final int WRIF_DT_INVST_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_DT_INVST_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
