package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WbepTabBeneficiari;

/**Original name: WBEP-AREA-BENEFICIARI<br>
 * Variable: WBEP-AREA-BENEFICIARI from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WbepAreaBeneficiari extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_BENEFICIARI_MAXOCCURS = 20;
    //Original name: WBEP-ELE-BENEFICIARI
    private short eleBeneficiari = DefaultValues.BIN_SHORT_VAL;
    //Original name: WBEP-TAB-BENEFICIARI
    private WbepTabBeneficiari[] tabBeneficiari = new WbepTabBeneficiari[TAB_BENEFICIARI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WbepAreaBeneficiari() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEP_AREA_BENEFICIARI;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWbepAreaBeneficiariBytes(buf);
    }

    public void init() {
        for (int tabBeneficiariIdx = 1; tabBeneficiariIdx <= TAB_BENEFICIARI_MAXOCCURS; tabBeneficiariIdx++) {
            tabBeneficiari[tabBeneficiariIdx - 1] = new WbepTabBeneficiari();
        }
    }

    public String getWbepAreaBeneficiariFormatted() {
        return MarshalByteExt.bufferToStr(getWbepAreaBeneficiariBytes());
    }

    public void setWbepAreaBeneficiariBytes(byte[] buffer) {
        setWbepAreaBeneficiariBytes(buffer, 1);
    }

    public byte[] getWbepAreaBeneficiariBytes() {
        byte[] buffer = new byte[Len.WBEP_AREA_BENEFICIARI];
        return getWbepAreaBeneficiariBytes(buffer, 1);
    }

    public void setWbepAreaBeneficiariBytes(byte[] buffer, int offset) {
        int position = offset;
        eleBeneficiari = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_BENEFICIARI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabBeneficiari[idx - 1].setWbepTabBeneficiariBytes(buffer, position);
                position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
            }
            else {
                tabBeneficiari[idx - 1].initWbepTabBeneficiariSpaces();
                position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
            }
        }
    }

    public byte[] getWbepAreaBeneficiariBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleBeneficiari);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_BENEFICIARI_MAXOCCURS; idx++) {
            tabBeneficiari[idx - 1].getWbepTabBeneficiariBytes(buffer, position);
            position += WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;
        }
        return buffer;
    }

    public void setEleBeneficiari(short eleBeneficiari) {
        this.eleBeneficiari = eleBeneficiari;
    }

    public short getEleBeneficiari() {
        return this.eleBeneficiari;
    }

    public WbepTabBeneficiari getTabBeneficiari(int idx) {
        return tabBeneficiari[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWbepAreaBeneficiariBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_BENEFICIARI = 2;
        public static final int WBEP_AREA_BENEFICIARI = ELE_BENEFICIARI + WbepAreaBeneficiari.TAB_BENEFICIARI_MAXOCCURS * WbepTabBeneficiari.Len.WBEP_TAB_BENEFICIARI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
