package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPLI-IMP-LIQ<br>
 * Variable: WPLI-IMP-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpliImpLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpliImpLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPLI_IMP_LIQ;
    }

    public void setWpliImpLiq(AfDecimal wpliImpLiq) {
        writeDecimalAsPacked(Pos.WPLI_IMP_LIQ, wpliImpLiq.copy());
    }

    public void setWpliImpLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPLI_IMP_LIQ, Pos.WPLI_IMP_LIQ);
    }

    /**Original name: WPLI-IMP-LIQ<br>*/
    public AfDecimal getWpliImpLiq() {
        return readPackedAsDecimal(Pos.WPLI_IMP_LIQ, Len.Int.WPLI_IMP_LIQ, Len.Fract.WPLI_IMP_LIQ);
    }

    public byte[] getWpliImpLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPLI_IMP_LIQ, Pos.WPLI_IMP_LIQ);
        return buffer;
    }

    public void initWpliImpLiqSpaces() {
        fill(Pos.WPLI_IMP_LIQ, Len.WPLI_IMP_LIQ, Types.SPACE_CHAR);
    }

    public void setWpliImpLiqNull(String wpliImpLiqNull) {
        writeString(Pos.WPLI_IMP_LIQ_NULL, wpliImpLiqNull, Len.WPLI_IMP_LIQ_NULL);
    }

    /**Original name: WPLI-IMP-LIQ-NULL<br>*/
    public String getWpliImpLiqNull() {
        return readString(Pos.WPLI_IMP_LIQ_NULL, Len.WPLI_IMP_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPLI_IMP_LIQ = 1;
        public static final int WPLI_IMP_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_IMP_LIQ = 8;
        public static final int WPLI_IMP_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPLI_IMP_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPLI_IMP_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
