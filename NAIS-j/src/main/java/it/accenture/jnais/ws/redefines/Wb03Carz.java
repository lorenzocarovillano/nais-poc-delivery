package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CARZ<br>
 * Variable: WB03-CARZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Carz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Carz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CARZ;
    }

    public void setWb03Carz(int wb03Carz) {
        writeIntAsPacked(Pos.WB03_CARZ, wb03Carz, Len.Int.WB03_CARZ);
    }

    public void setWb03CarzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CARZ, Pos.WB03_CARZ);
    }

    /**Original name: WB03-CARZ<br>*/
    public int getWb03Carz() {
        return readPackedAsInt(Pos.WB03_CARZ, Len.Int.WB03_CARZ);
    }

    public byte[] getWb03CarzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CARZ, Pos.WB03_CARZ);
        return buffer;
    }

    public void setWb03CarzNull(String wb03CarzNull) {
        writeString(Pos.WB03_CARZ_NULL, wb03CarzNull, Len.WB03_CARZ_NULL);
    }

    /**Original name: WB03-CARZ-NULL<br>*/
    public String getWb03CarzNull() {
        return readString(Pos.WB03_CARZ_NULL, Len.WB03_CARZ_NULL);
    }

    public String getWb03CarzNullFormatted() {
        return Functions.padBlanks(getWb03CarzNull(), Len.WB03_CARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CARZ = 1;
        public static final int WB03_CARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CARZ = 3;
        public static final int WB03_CARZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CARZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
