package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB05-VAL-PC<br>
 * Variable: WB05-VAL-PC from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb05ValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb05ValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB05_VAL_PC;
    }

    public void setWb05ValPc(AfDecimal wb05ValPc) {
        writeDecimalAsPacked(Pos.WB05_VAL_PC, wb05ValPc.copy());
    }

    public void setWb05ValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB05_VAL_PC, Pos.WB05_VAL_PC);
    }

    /**Original name: WB05-VAL-PC<br>*/
    public AfDecimal getWb05ValPc() {
        return readPackedAsDecimal(Pos.WB05_VAL_PC, Len.Int.WB05_VAL_PC, Len.Fract.WB05_VAL_PC);
    }

    public byte[] getWb05ValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB05_VAL_PC, Pos.WB05_VAL_PC);
        return buffer;
    }

    public void setWb05ValPcNull(String wb05ValPcNull) {
        writeString(Pos.WB05_VAL_PC_NULL, wb05ValPcNull, Len.WB05_VAL_PC_NULL);
    }

    /**Original name: WB05-VAL-PC-NULL<br>*/
    public String getWb05ValPcNull() {
        return readString(Pos.WB05_VAL_PC_NULL, Len.WB05_VAL_PC_NULL);
    }

    public String getWb05ValPcNullFormatted() {
        return Functions.padBlanks(getWb05ValPcNull(), Len.WB05_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB05_VAL_PC = 1;
        public static final int WB05_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_VAL_PC = 8;
        public static final int WB05_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB05_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB05_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
