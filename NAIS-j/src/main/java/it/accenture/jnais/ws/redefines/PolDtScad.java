package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DT-SCAD<br>
 * Variable: POL-DT-SCAD from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DT_SCAD;
    }

    public void setPolDtScad(int polDtScad) {
        writeIntAsPacked(Pos.POL_DT_SCAD, polDtScad, Len.Int.POL_DT_SCAD);
    }

    public void setPolDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DT_SCAD, Pos.POL_DT_SCAD);
    }

    /**Original name: POL-DT-SCAD<br>*/
    public int getPolDtScad() {
        return readPackedAsInt(Pos.POL_DT_SCAD, Len.Int.POL_DT_SCAD);
    }

    public byte[] getPolDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DT_SCAD, Pos.POL_DT_SCAD);
        return buffer;
    }

    public void setPolDtScadNull(String polDtScadNull) {
        writeString(Pos.POL_DT_SCAD_NULL, polDtScadNull, Len.POL_DT_SCAD_NULL);
    }

    /**Original name: POL-DT-SCAD-NULL<br>*/
    public String getPolDtScadNull() {
        return readString(Pos.POL_DT_SCAD_NULL, Len.POL_DT_SCAD_NULL);
    }

    public String getPolDtScadNullFormatted() {
        return Functions.padBlanks(getPolDtScadNull(), Len.POL_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DT_SCAD = 1;
        public static final int POL_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DT_SCAD = 5;
        public static final int POL_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
