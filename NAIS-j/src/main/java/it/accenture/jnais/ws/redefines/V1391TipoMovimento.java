package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: V1391-TIPO-MOVIMENTO<br>
 * Variable: V1391-TIPO-MOVIMENTO from program LDBS1390<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class V1391TipoMovimento extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public V1391TipoMovimento() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.V1391_TIPO_MOVIMENTO;
    }

    public void setV1391TipoMovimento(int v1391TipoMovimento) {
        writeIntAsPacked(Pos.V1391_TIPO_MOVIMENTO, v1391TipoMovimento, Len.Int.V1391_TIPO_MOVIMENTO);
    }

    public void setV1391TipoMovimentoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.V1391_TIPO_MOVIMENTO, Pos.V1391_TIPO_MOVIMENTO);
    }

    /**Original name: V1391-TIPO-MOVIMENTO<br>*/
    public int getV1391TipoMovimento() {
        return readPackedAsInt(Pos.V1391_TIPO_MOVIMENTO, Len.Int.V1391_TIPO_MOVIMENTO);
    }

    public byte[] getV1391TipoMovimentoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.V1391_TIPO_MOVIMENTO, Pos.V1391_TIPO_MOVIMENTO);
        return buffer;
    }

    public void initV1391TipoMovimentoSpaces() {
        fill(Pos.V1391_TIPO_MOVIMENTO, Len.V1391_TIPO_MOVIMENTO, Types.SPACE_CHAR);
    }

    public void setV1391TipoMovimentoNull(String v1391TipoMovimentoNull) {
        writeString(Pos.V1391_TIPO_MOVIMENTO_NULL, v1391TipoMovimentoNull, Len.V1391_TIPO_MOVIMENTO_NULL);
    }

    /**Original name: V1391-TIPO-MOVIMENTO-NULL<br>*/
    public String getV1391TipoMovimentoNull() {
        return readString(Pos.V1391_TIPO_MOVIMENTO_NULL, Len.V1391_TIPO_MOVIMENTO_NULL);
    }

    public String getV1391TipoMovimentoNullFormatted() {
        return Functions.padBlanks(getV1391TipoMovimentoNull(), Len.V1391_TIPO_MOVIMENTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int V1391_TIPO_MOVIMENTO = 1;
        public static final int V1391_TIPO_MOVIMENTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int V1391_TIPO_MOVIMENTO = 3;
        public static final int V1391_TIPO_MOVIMENTO_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int V1391_TIPO_MOVIMENTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
