package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-INTR-RIT-PAG<br>
 * Variable: TCL-IMP-INTR-RIT-PAG from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_INTR_RIT_PAG;
    }

    public void setTclImpIntrRitPag(AfDecimal tclImpIntrRitPag) {
        writeDecimalAsPacked(Pos.TCL_IMP_INTR_RIT_PAG, tclImpIntrRitPag.copy());
    }

    public void setTclImpIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_INTR_RIT_PAG, Pos.TCL_IMP_INTR_RIT_PAG);
    }

    /**Original name: TCL-IMP-INTR-RIT-PAG<br>*/
    public AfDecimal getTclImpIntrRitPag() {
        return readPackedAsDecimal(Pos.TCL_IMP_INTR_RIT_PAG, Len.Int.TCL_IMP_INTR_RIT_PAG, Len.Fract.TCL_IMP_INTR_RIT_PAG);
    }

    public byte[] getTclImpIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_INTR_RIT_PAG, Pos.TCL_IMP_INTR_RIT_PAG);
        return buffer;
    }

    public void setTclImpIntrRitPagNull(String tclImpIntrRitPagNull) {
        writeString(Pos.TCL_IMP_INTR_RIT_PAG_NULL, tclImpIntrRitPagNull, Len.TCL_IMP_INTR_RIT_PAG_NULL);
    }

    /**Original name: TCL-IMP-INTR-RIT-PAG-NULL<br>*/
    public String getTclImpIntrRitPagNull() {
        return readString(Pos.TCL_IMP_INTR_RIT_PAG_NULL, Len.TCL_IMP_INTR_RIT_PAG_NULL);
    }

    public String getTclImpIntrRitPagNullFormatted() {
        return Functions.padBlanks(getTclImpIntrRitPagNull(), Len.TCL_IMP_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_INTR_RIT_PAG = 1;
        public static final int TCL_IMP_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_INTR_RIT_PAG = 8;
        public static final int TCL_IMP_INTR_RIT_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_INTR_RIT_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_INTR_RIT_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
