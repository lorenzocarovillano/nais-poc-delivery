package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-ELEMENTI-PADRE-RED<br>
 * Variables: WK-ELEMENTI-PADRE-RED from program IDSS0020<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkElementiPadreRed {

    //==== PROPERTIES ====
    //Original name: WK-INDICE-PADRE-RED
    private String indicePadreRed = DefaultValues.stringVal(Len.INDICE_PADRE_RED);
    //Original name: WK-LUNG-PADRE-RED
    private int lungPadreRed = DefaultValues.INT_VAL;
    //Original name: WK-NUMERO-RICORRENZE-RED
    private int numeroRicorrenzeRed = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setIndicePadreRed(short indicePadreRed) {
        this.indicePadreRed = NumericDisplay.asString(indicePadreRed, Len.INDICE_PADRE_RED);
    }

    public void setIndicePadreRedFormatted(String indicePadreRed) {
        this.indicePadreRed = Trunc.toUnsignedNumeric(indicePadreRed, Len.INDICE_PADRE_RED);
    }

    public short getIndicePadreRed() {
        return NumericDisplay.asShort(this.indicePadreRed);
    }

    public void setLungPadreRed(int lungPadreRed) {
        this.lungPadreRed = lungPadreRed;
    }

    public int getLungPadreRed() {
        return this.lungPadreRed;
    }

    public void setNumeroRicorrenzeRed(int numeroRicorrenzeRed) {
        this.numeroRicorrenzeRed = numeroRicorrenzeRed;
    }

    public int getNumeroRicorrenzeRed() {
        return this.numeroRicorrenzeRed;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INDICE_PADRE_RED = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
