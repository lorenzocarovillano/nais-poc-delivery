package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RINN-TAC<br>
 * Variable: PCO-DT-ULT-RINN-TAC from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRinnTac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRinnTac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RINN_TAC;
    }

    public void setPcoDtUltRinnTac(int pcoDtUltRinnTac) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RINN_TAC, pcoDtUltRinnTac, Len.Int.PCO_DT_ULT_RINN_TAC);
    }

    public void setPcoDtUltRinnTacFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RINN_TAC, Pos.PCO_DT_ULT_RINN_TAC);
    }

    /**Original name: PCO-DT-ULT-RINN-TAC<br>*/
    public int getPcoDtUltRinnTac() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RINN_TAC, Len.Int.PCO_DT_ULT_RINN_TAC);
    }

    public byte[] getPcoDtUltRinnTacAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RINN_TAC, Pos.PCO_DT_ULT_RINN_TAC);
        return buffer;
    }

    public void initPcoDtUltRinnTacHighValues() {
        fill(Pos.PCO_DT_ULT_RINN_TAC, Len.PCO_DT_ULT_RINN_TAC, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRinnTacNull(String pcoDtUltRinnTacNull) {
        writeString(Pos.PCO_DT_ULT_RINN_TAC_NULL, pcoDtUltRinnTacNull, Len.PCO_DT_ULT_RINN_TAC_NULL);
    }

    /**Original name: PCO-DT-ULT-RINN-TAC-NULL<br>*/
    public String getPcoDtUltRinnTacNull() {
        return readString(Pos.PCO_DT_ULT_RINN_TAC_NULL, Len.PCO_DT_ULT_RINN_TAC_NULL);
    }

    public String getPcoDtUltRinnTacNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRinnTacNull(), Len.PCO_DT_ULT_RINN_TAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RINN_TAC = 1;
        public static final int PCO_DT_ULT_RINN_TAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RINN_TAC = 5;
        public static final int PCO_DT_ULT_RINN_TAC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RINN_TAC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
