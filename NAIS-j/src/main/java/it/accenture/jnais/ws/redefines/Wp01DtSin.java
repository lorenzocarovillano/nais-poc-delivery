package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-DT-SIN<br>
 * Variable: WP01-DT-SIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01DtSin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01DtSin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_DT_SIN;
    }

    public void setWp01DtSin(int wp01DtSin) {
        writeIntAsPacked(Pos.WP01_DT_SIN, wp01DtSin, Len.Int.WP01_DT_SIN);
    }

    public void setWp01DtSinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_DT_SIN, Pos.WP01_DT_SIN);
    }

    /**Original name: WP01-DT-SIN<br>*/
    public int getWp01DtSin() {
        return readPackedAsInt(Pos.WP01_DT_SIN, Len.Int.WP01_DT_SIN);
    }

    public byte[] getWp01DtSinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_DT_SIN, Pos.WP01_DT_SIN);
        return buffer;
    }

    public void setWp01DtSinNull(String wp01DtSinNull) {
        writeString(Pos.WP01_DT_SIN_NULL, wp01DtSinNull, Len.WP01_DT_SIN_NULL);
    }

    /**Original name: WP01-DT-SIN-NULL<br>*/
    public String getWp01DtSinNull() {
        return readString(Pos.WP01_DT_SIN_NULL, Len.WP01_DT_SIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_DT_SIN = 1;
        public static final int WP01_DT_SIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_DT_SIN = 5;
        public static final int WP01_DT_SIN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_DT_SIN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
