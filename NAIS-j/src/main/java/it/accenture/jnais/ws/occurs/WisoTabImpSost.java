package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccviso1;
import it.accenture.jnais.copy.WisoDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WISO-TAB-IMP-SOST<br>
 * Variables: WISO-TAB-IMP-SOST from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WisoTabImpSost {

    //==== PROPERTIES ====
    //Original name: LCCVISO1
    private Lccviso1 lccviso1 = new Lccviso1();

    //==== METHODS ====
    public void setWisoTabImpSostBytes(byte[] buffer, int offset) {
        int position = offset;
        lccviso1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccviso1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccviso1.Len.Int.ID_PTF, 0));
        position += Lccviso1.Len.ID_PTF;
        lccviso1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWisoTabImpSostBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccviso1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccviso1.getIdPtf(), Lccviso1.Len.Int.ID_PTF, 0);
        position += Lccviso1.Len.ID_PTF;
        lccviso1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWisoTabImpSostSpaces() {
        lccviso1.initLccviso1Spaces();
    }

    public Lccviso1 getLccviso1() {
        return lccviso1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_TAB_IMP_SOST = WpolStatus.Len.STATUS + Lccviso1.Len.ID_PTF + WisoDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
