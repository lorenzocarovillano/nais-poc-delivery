package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P89-DT-INI-CNTRL-FND<br>
 * Variable: P89-DT-INI-CNTRL-FND from program IDBSP890<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P89DtIniCntrlFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P89DtIniCntrlFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P89_DT_INI_CNTRL_FND;
    }

    public void setP89DtIniCntrlFnd(int p89DtIniCntrlFnd) {
        writeIntAsPacked(Pos.P89_DT_INI_CNTRL_FND, p89DtIniCntrlFnd, Len.Int.P89_DT_INI_CNTRL_FND);
    }

    public void setP89DtIniCntrlFndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P89_DT_INI_CNTRL_FND, Pos.P89_DT_INI_CNTRL_FND);
    }

    /**Original name: P89-DT-INI-CNTRL-FND<br>*/
    public int getP89DtIniCntrlFnd() {
        return readPackedAsInt(Pos.P89_DT_INI_CNTRL_FND, Len.Int.P89_DT_INI_CNTRL_FND);
    }

    public byte[] getP89DtIniCntrlFndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P89_DT_INI_CNTRL_FND, Pos.P89_DT_INI_CNTRL_FND);
        return buffer;
    }

    public void setP89DtIniCntrlFndNull(String p89DtIniCntrlFndNull) {
        writeString(Pos.P89_DT_INI_CNTRL_FND_NULL, p89DtIniCntrlFndNull, Len.P89_DT_INI_CNTRL_FND_NULL);
    }

    /**Original name: P89-DT-INI-CNTRL-FND-NULL<br>*/
    public String getP89DtIniCntrlFndNull() {
        return readString(Pos.P89_DT_INI_CNTRL_FND_NULL, Len.P89_DT_INI_CNTRL_FND_NULL);
    }

    public String getP89DtIniCntrlFndNullFormatted() {
        return Functions.padBlanks(getP89DtIniCntrlFndNull(), Len.P89_DT_INI_CNTRL_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P89_DT_INI_CNTRL_FND = 1;
        public static final int P89_DT_INI_CNTRL_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P89_DT_INI_CNTRL_FND = 5;
        public static final int P89_DT_INI_CNTRL_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P89_DT_INI_CNTRL_FND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
