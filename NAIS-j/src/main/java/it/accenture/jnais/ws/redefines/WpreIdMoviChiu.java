package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPRE-ID-MOVI-CHIU<br>
 * Variable: WPRE-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_ID_MOVI_CHIU;
    }

    public void setWpreIdMoviChiu(int wpreIdMoviChiu) {
        writeIntAsPacked(Pos.WPRE_ID_MOVI_CHIU, wpreIdMoviChiu, Len.Int.WPRE_ID_MOVI_CHIU);
    }

    public void setWpreIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_ID_MOVI_CHIU, Pos.WPRE_ID_MOVI_CHIU);
    }

    /**Original name: WPRE-ID-MOVI-CHIU<br>*/
    public int getWpreIdMoviChiu() {
        return readPackedAsInt(Pos.WPRE_ID_MOVI_CHIU, Len.Int.WPRE_ID_MOVI_CHIU);
    }

    public byte[] getWpreIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_ID_MOVI_CHIU, Pos.WPRE_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWpreIdMoviChiuSpaces() {
        fill(Pos.WPRE_ID_MOVI_CHIU, Len.WPRE_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWpreIdMoviChiuNull(String wpreIdMoviChiuNull) {
        writeString(Pos.WPRE_ID_MOVI_CHIU_NULL, wpreIdMoviChiuNull, Len.WPRE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WPRE-ID-MOVI-CHIU-NULL<br>*/
    public String getWpreIdMoviChiuNull() {
        return readString(Pos.WPRE_ID_MOVI_CHIU_NULL, Len.WPRE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_ID_MOVI_CHIU = 1;
        public static final int WPRE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_ID_MOVI_CHIU = 5;
        public static final int WPRE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
