package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WK-APPO-IAS<br>
 * Variable: WK-APPO-IAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkAppoIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkAppoIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_APPO_IAS;
    }

    public void setWkAppoIas(AfDecimal wkAppoIas) {
        writeDecimal(Pos.WK_APPO_IAS, wkAppoIas.copy());
    }

    /**Original name: WK-APPO-IAS<br>*/
    public AfDecimal getWkAppoIas() {
        return readDecimal(Pos.WK_APPO_IAS, Len.Int.WK_APPO_IAS, Len.Fract.WK_APPO_IAS);
    }

    public String getIas2Formatted() {
        return readFixedString(Pos.IAS2, Len.IAS2);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_APPO_IAS = 1;
        public static final int WK_DIFF_IAS_V = 1;
        public static final int IAS1 = WK_DIFF_IAS_V;
        public static final int IAS2 = IAS1 + Len.IAS1;
        public static final int IAS_DEC = IAS2 + Len.IAS2;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int IAS1 = 9;
        public static final int IAS2 = 2;
        public static final int WK_APPO_IAS = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_APPO_IAS = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_APPO_IAS = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
