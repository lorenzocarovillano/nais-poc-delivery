package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IIMPST-252-CALC<br>
 * Variable: DFL-IIMPST-252-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIimpst252Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIimpst252Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IIMPST252_CALC;
    }

    public void setDflIimpst252Calc(AfDecimal dflIimpst252Calc) {
        writeDecimalAsPacked(Pos.DFL_IIMPST252_CALC, dflIimpst252Calc.copy());
    }

    public void setDflIimpst252CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IIMPST252_CALC, Pos.DFL_IIMPST252_CALC);
    }

    /**Original name: DFL-IIMPST-252-CALC<br>*/
    public AfDecimal getDflIimpst252Calc() {
        return readPackedAsDecimal(Pos.DFL_IIMPST252_CALC, Len.Int.DFL_IIMPST252_CALC, Len.Fract.DFL_IIMPST252_CALC);
    }

    public byte[] getDflIimpst252CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IIMPST252_CALC, Pos.DFL_IIMPST252_CALC);
        return buffer;
    }

    public void setDflIimpst252CalcNull(String dflIimpst252CalcNull) {
        writeString(Pos.DFL_IIMPST252_CALC_NULL, dflIimpst252CalcNull, Len.DFL_IIMPST252_CALC_NULL);
    }

    /**Original name: DFL-IIMPST-252-CALC-NULL<br>*/
    public String getDflIimpst252CalcNull() {
        return readString(Pos.DFL_IIMPST252_CALC_NULL, Len.DFL_IIMPST252_CALC_NULL);
    }

    public String getDflIimpst252CalcNullFormatted() {
        return Functions.padBlanks(getDflIimpst252CalcNull(), Len.DFL_IIMPST252_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST252_CALC = 1;
        public static final int DFL_IIMPST252_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST252_CALC = 8;
        public static final int DFL_IIMPST252_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST252_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST252_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
