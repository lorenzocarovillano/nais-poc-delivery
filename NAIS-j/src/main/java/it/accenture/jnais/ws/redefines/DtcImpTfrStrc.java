package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-IMP-TFR-STRC<br>
 * Variable: DTC-IMP-TFR-STRC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_IMP_TFR_STRC;
    }

    public void setDtcImpTfrStrc(AfDecimal dtcImpTfrStrc) {
        writeDecimalAsPacked(Pos.DTC_IMP_TFR_STRC, dtcImpTfrStrc.copy());
    }

    public void setDtcImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_IMP_TFR_STRC, Pos.DTC_IMP_TFR_STRC);
    }

    /**Original name: DTC-IMP-TFR-STRC<br>*/
    public AfDecimal getDtcImpTfrStrc() {
        return readPackedAsDecimal(Pos.DTC_IMP_TFR_STRC, Len.Int.DTC_IMP_TFR_STRC, Len.Fract.DTC_IMP_TFR_STRC);
    }

    public byte[] getDtcImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_IMP_TFR_STRC, Pos.DTC_IMP_TFR_STRC);
        return buffer;
    }

    public void setDtcImpTfrStrcNull(String dtcImpTfrStrcNull) {
        writeString(Pos.DTC_IMP_TFR_STRC_NULL, dtcImpTfrStrcNull, Len.DTC_IMP_TFR_STRC_NULL);
    }

    /**Original name: DTC-IMP-TFR-STRC-NULL<br>*/
    public String getDtcImpTfrStrcNull() {
        return readString(Pos.DTC_IMP_TFR_STRC_NULL, Len.DTC_IMP_TFR_STRC_NULL);
    }

    public String getDtcImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getDtcImpTfrStrcNull(), Len.DTC_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_IMP_TFR_STRC = 1;
        public static final int DTC_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_IMP_TFR_STRC = 8;
        public static final int DTC_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
