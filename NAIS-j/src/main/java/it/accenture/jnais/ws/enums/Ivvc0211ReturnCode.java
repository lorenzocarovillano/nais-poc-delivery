package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-RETURN-CODE<br>
 * Variable: IVVC0211-RETURN-CODE from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211ReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.RETURN_CODE);
    public static final String SUCCESSFUL_RC = "00";
    public static final String GENERIC_ERROR = "KO";
    public static final String SQL_ERROR = "D3";
    public static final String DT_VLDT_PROD_NOT_V = "DT";
    public static final String BUFFER_NOT_VALID = "BU";
    public static final String AREA_NOT_FOUND = "AF";
    public static final String STEP_ELAB_NOT_VALID = "SE";
    public static final String FLAG_AREA_NOT_VALID = "FA";
    public static final String TIPO_MOVIM_NOT_VALID = "TM";
    public static final String VERS_PROD_NOT_VALID = "VP";
    public static final String VERS_OPZ_NOT_VALID = "VO";
    public static final String CALCOLO_NON_COMPLETO = "CA";
    public static final String CACHE_PIENA = "CP";
    public static final String TIPO_SCHEDA_NOT_VALID = "TS";
    public static final String FUNZ_NON_DEFINITA = "FU";
    public static final String GLOVARLIST_VUOTA = "GV";
    public static final String TRATTATO_NOT_VALID = "TV";
    public static final String SKIP_CALL_ACTUATOR = "SA";

    //==== METHODS ====
    public void setReturnCode(String returnCode) {
        this.value = Functions.subString(returnCode, Len.RETURN_CODE);
    }

    public String getReturnCode() {
        return this.value;
    }

    public boolean isSuccessfulRc() {
        return value.equals(SUCCESSFUL_RC);
    }

    public void setSuccessfulRc() {
        value = SUCCESSFUL_RC;
    }

    public void setGenericError() {
        value = GENERIC_ERROR;
    }

    public void setSqlError() {
        value = SQL_ERROR;
    }

    public void setDtVldtProdNotV() {
        value = DT_VLDT_PROD_NOT_V;
    }

    public void setBufferNotValid() {
        value = BUFFER_NOT_VALID;
    }

    public void setAreaNotFound() {
        value = AREA_NOT_FOUND;
    }

    public void setStepElabNotValid() {
        value = STEP_ELAB_NOT_VALID;
    }

    public void setFlagAreaNotValid() {
        value = FLAG_AREA_NOT_VALID;
    }

    public void setTipoMovimNotValid() {
        value = TIPO_MOVIM_NOT_VALID;
    }

    public void setVersProdNotValid() {
        value = VERS_PROD_NOT_VALID;
    }

    public void setVersOpzNotValid() {
        value = VERS_OPZ_NOT_VALID;
    }

    public boolean isCalcoloNonCompleto() {
        return value.equals(CALCOLO_NON_COMPLETO);
    }

    public boolean isCachePiena() {
        return value.equals(CACHE_PIENA);
    }

    public void setTipoSchedaNotValid() {
        value = TIPO_SCHEDA_NOT_VALID;
    }

    public void setFunzNonDefinita() {
        value = FUNZ_NON_DEFINITA;
    }

    public boolean isS211GlovarlistVuota() {
        return value.equals(GLOVARLIST_VUOTA);
    }

    public void setGlovarlistVuota() {
        value = GLOVARLIST_VUOTA;
    }

    public void setTrattatoNotValid() {
        value = TRATTATO_NOT_VALID;
    }

    public boolean isS211SkipCallActuator() {
        return value.equals(SKIP_CALL_ACTUATOR);
    }

    public void setSkipCallActuator() {
        value = SKIP_CALL_ACTUATOR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
