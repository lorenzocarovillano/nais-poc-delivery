package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-INI<br>
 * Variable: WTGA-PRSTZ-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_INI;
    }

    public void setWtgaPrstzIni(AfDecimal wtgaPrstzIni) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_INI, wtgaPrstzIni.copy());
    }

    public void setWtgaPrstzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_INI, Pos.WTGA_PRSTZ_INI);
    }

    /**Original name: WTGA-PRSTZ-INI<br>*/
    public AfDecimal getWtgaPrstzIni() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_INI, Len.Int.WTGA_PRSTZ_INI, Len.Fract.WTGA_PRSTZ_INI);
    }

    public byte[] getWtgaPrstzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_INI, Pos.WTGA_PRSTZ_INI);
        return buffer;
    }

    public void initWtgaPrstzIniSpaces() {
        fill(Pos.WTGA_PRSTZ_INI, Len.WTGA_PRSTZ_INI, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzIniNull(String wtgaPrstzIniNull) {
        writeString(Pos.WTGA_PRSTZ_INI_NULL, wtgaPrstzIniNull, Len.WTGA_PRSTZ_INI_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-NULL<br>*/
    public String getWtgaPrstzIniNull() {
        return readString(Pos.WTGA_PRSTZ_INI_NULL, Len.WTGA_PRSTZ_INI_NULL);
    }

    public String getWtgaPrstzIniNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzIniNull(), Len.WTGA_PRSTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI = 1;
        public static final int WTGA_PRSTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI = 8;
        public static final int WTGA_PRSTZ_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
