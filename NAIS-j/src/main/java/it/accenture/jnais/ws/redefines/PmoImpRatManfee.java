package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-IMP-RAT-MANFEE<br>
 * Variable: PMO-IMP-RAT-MANFEE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoImpRatManfee extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoImpRatManfee() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_IMP_RAT_MANFEE;
    }

    public void setPmoImpRatManfee(AfDecimal pmoImpRatManfee) {
        writeDecimalAsPacked(Pos.PMO_IMP_RAT_MANFEE, pmoImpRatManfee.copy());
    }

    public void setPmoImpRatManfeeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_IMP_RAT_MANFEE, Pos.PMO_IMP_RAT_MANFEE);
    }

    /**Original name: PMO-IMP-RAT-MANFEE<br>*/
    public AfDecimal getPmoImpRatManfee() {
        return readPackedAsDecimal(Pos.PMO_IMP_RAT_MANFEE, Len.Int.PMO_IMP_RAT_MANFEE, Len.Fract.PMO_IMP_RAT_MANFEE);
    }

    public byte[] getPmoImpRatManfeeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_IMP_RAT_MANFEE, Pos.PMO_IMP_RAT_MANFEE);
        return buffer;
    }

    public void initPmoImpRatManfeeHighValues() {
        fill(Pos.PMO_IMP_RAT_MANFEE, Len.PMO_IMP_RAT_MANFEE, Types.HIGH_CHAR_VAL);
    }

    public void setPmoImpRatManfeeNull(String pmoImpRatManfeeNull) {
        writeString(Pos.PMO_IMP_RAT_MANFEE_NULL, pmoImpRatManfeeNull, Len.PMO_IMP_RAT_MANFEE_NULL);
    }

    /**Original name: PMO-IMP-RAT-MANFEE-NULL<br>*/
    public String getPmoImpRatManfeeNull() {
        return readString(Pos.PMO_IMP_RAT_MANFEE_NULL, Len.PMO_IMP_RAT_MANFEE_NULL);
    }

    public String getPmoImpRatManfeeNullFormatted() {
        return Functions.padBlanks(getPmoImpRatManfeeNull(), Len.PMO_IMP_RAT_MANFEE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_IMP_RAT_MANFEE = 1;
        public static final int PMO_IMP_RAT_MANFEE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_IMP_RAT_MANFEE = 8;
        public static final int PMO_IMP_RAT_MANFEE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_IMP_RAT_MANFEE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_IMP_RAT_MANFEE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
