package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-ID-1O-ASSTO<br>
 * Variable: L3401-ID-1O-ASSTO from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401Id1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401Id1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_ID1O_ASSTO;
    }

    public void setL3401Id1oAssto(int l3401Id1oAssto) {
        writeIntAsPacked(Pos.L3401_ID1O_ASSTO, l3401Id1oAssto, Len.Int.L3401_ID1O_ASSTO);
    }

    public void setL3401Id1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_ID1O_ASSTO, Pos.L3401_ID1O_ASSTO);
    }

    /**Original name: L3401-ID-1O-ASSTO<br>*/
    public int getL3401Id1oAssto() {
        return readPackedAsInt(Pos.L3401_ID1O_ASSTO, Len.Int.L3401_ID1O_ASSTO);
    }

    public byte[] getL3401Id1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_ID1O_ASSTO, Pos.L3401_ID1O_ASSTO);
        return buffer;
    }

    /**Original name: L3401-ID-1O-ASSTO-NULL<br>*/
    public String getL3401Id1oAsstoNull() {
        return readString(Pos.L3401_ID1O_ASSTO_NULL, Len.L3401_ID1O_ASSTO_NULL);
    }

    public String getL3401Id1oAsstoNullFormatted() {
        return Functions.padBlanks(getL3401Id1oAsstoNull(), Len.L3401_ID1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_ID1O_ASSTO = 1;
        public static final int L3401_ID1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ID1O_ASSTO = 5;
        public static final int L3401_ID1O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ID1O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
