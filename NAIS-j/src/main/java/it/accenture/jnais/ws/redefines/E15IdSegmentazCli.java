package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E15-ID-SEGMENTAZ-CLI<br>
 * Variable: E15-ID-SEGMENTAZ-CLI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E15IdSegmentazCli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E15IdSegmentazCli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E15_ID_SEGMENTAZ_CLI;
    }

    public void setE15IdSegmentazCli(int e15IdSegmentazCli) {
        writeIntAsPacked(Pos.E15_ID_SEGMENTAZ_CLI, e15IdSegmentazCli, Len.Int.E15_ID_SEGMENTAZ_CLI);
    }

    public void setE15IdSegmentazCliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E15_ID_SEGMENTAZ_CLI, Pos.E15_ID_SEGMENTAZ_CLI);
    }

    /**Original name: E15-ID-SEGMENTAZ-CLI<br>*/
    public int getE15IdSegmentazCli() {
        return readPackedAsInt(Pos.E15_ID_SEGMENTAZ_CLI, Len.Int.E15_ID_SEGMENTAZ_CLI);
    }

    public byte[] getE15IdSegmentazCliAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E15_ID_SEGMENTAZ_CLI, Pos.E15_ID_SEGMENTAZ_CLI);
        return buffer;
    }

    public void setE15IdSegmentazCliNull(String e15IdSegmentazCliNull) {
        writeString(Pos.E15_ID_SEGMENTAZ_CLI_NULL, e15IdSegmentazCliNull, Len.E15_ID_SEGMENTAZ_CLI_NULL);
    }

    /**Original name: E15-ID-SEGMENTAZ-CLI-NULL<br>*/
    public String getE15IdSegmentazCliNull() {
        return readString(Pos.E15_ID_SEGMENTAZ_CLI_NULL, Len.E15_ID_SEGMENTAZ_CLI_NULL);
    }

    public String getE15IdSegmentazCliNullFormatted() {
        return Functions.padBlanks(getE15IdSegmentazCliNull(), Len.E15_ID_SEGMENTAZ_CLI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E15_ID_SEGMENTAZ_CLI = 1;
        public static final int E15_ID_SEGMENTAZ_CLI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E15_ID_SEGMENTAZ_CLI = 5;
        public static final int E15_ID_SEGMENTAZ_CLI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E15_ID_SEGMENTAZ_CLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
