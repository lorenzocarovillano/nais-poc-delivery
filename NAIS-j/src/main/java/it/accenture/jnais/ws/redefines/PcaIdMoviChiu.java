package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-ID-MOVI-CHIU<br>
 * Variable: PCA-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_ID_MOVI_CHIU;
    }

    public void setPcaIdMoviChiu(int pcaIdMoviChiu) {
        writeIntAsPacked(Pos.PCA_ID_MOVI_CHIU, pcaIdMoviChiu, Len.Int.PCA_ID_MOVI_CHIU);
    }

    public void setPcaIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_ID_MOVI_CHIU, Pos.PCA_ID_MOVI_CHIU);
    }

    /**Original name: PCA-ID-MOVI-CHIU<br>*/
    public int getPcaIdMoviChiu() {
        return readPackedAsInt(Pos.PCA_ID_MOVI_CHIU, Len.Int.PCA_ID_MOVI_CHIU);
    }

    public byte[] getPcaIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_ID_MOVI_CHIU, Pos.PCA_ID_MOVI_CHIU);
        return buffer;
    }

    public void setPcaIdMoviChiuNull(String pcaIdMoviChiuNull) {
        writeString(Pos.PCA_ID_MOVI_CHIU_NULL, pcaIdMoviChiuNull, Len.PCA_ID_MOVI_CHIU_NULL);
    }

    /**Original name: PCA-ID-MOVI-CHIU-NULL<br>*/
    public String getPcaIdMoviChiuNull() {
        return readString(Pos.PCA_ID_MOVI_CHIU_NULL, Len.PCA_ID_MOVI_CHIU_NULL);
    }

    public String getPcaIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPcaIdMoviChiuNull(), Len.PCA_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_ID_MOVI_CHIU = 1;
        public static final int PCA_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_ID_MOVI_CHIU = 5;
        public static final int PCA_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
