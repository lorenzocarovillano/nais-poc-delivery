package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB05-VAL-IMP<br>
 * Variable: WB05-VAL-IMP from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb05ValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb05ValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB05_VAL_IMP;
    }

    public void setWb05ValImp(AfDecimal wb05ValImp) {
        writeDecimalAsPacked(Pos.WB05_VAL_IMP, wb05ValImp.copy());
    }

    public void setWb05ValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB05_VAL_IMP, Pos.WB05_VAL_IMP);
    }

    /**Original name: WB05-VAL-IMP<br>*/
    public AfDecimal getWb05ValImp() {
        return readPackedAsDecimal(Pos.WB05_VAL_IMP, Len.Int.WB05_VAL_IMP, Len.Fract.WB05_VAL_IMP);
    }

    public byte[] getWb05ValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB05_VAL_IMP, Pos.WB05_VAL_IMP);
        return buffer;
    }

    public void setWb05ValImpNull(String wb05ValImpNull) {
        writeString(Pos.WB05_VAL_IMP_NULL, wb05ValImpNull, Len.WB05_VAL_IMP_NULL);
    }

    /**Original name: WB05-VAL-IMP-NULL<br>*/
    public String getWb05ValImpNull() {
        return readString(Pos.WB05_VAL_IMP_NULL, Len.WB05_VAL_IMP_NULL);
    }

    public String getWb05ValImpNullFormatted() {
        return Functions.padBlanks(getWb05ValImpNull(), Len.WB05_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB05_VAL_IMP = 1;
        public static final int WB05_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_VAL_IMP = 10;
        public static final int WB05_VAL_IMP_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB05_VAL_IMP = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB05_VAL_IMP = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
