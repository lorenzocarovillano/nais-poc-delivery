package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.EstrCntDiagnCedLrgs0660;
import it.accenture.jnais.copy.EstrCntDiagnRivLrgs0660;
import it.accenture.jnais.copy.Iabcsq99;
import it.accenture.jnais.copy.Iabvsqs1;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.ImpstSost;
import it.accenture.jnais.copy.Ldbv6191;
import it.accenture.jnais.copy.Ldbvf301;
import it.accenture.jnais.copy.Ldbvf321;
import it.accenture.jnais.copy.Loar0171;
import it.accenture.jnais.copy.Lrgc0661;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.WrinRecOutput;
import it.accenture.jnais.ws.enums.FlagGarRamo3;
import it.accenture.jnais.ws.enums.FlagGarRamo3Stornata;
import it.accenture.jnais.ws.enums.FlagTrovato;
import it.accenture.jnais.ws.enums.TipologiaEc;
import it.accenture.jnais.ws.occurs.EleAreaErr;
import it.accenture.jnais.ws.occurs.EleCache10;
import it.accenture.jnais.ws.occurs.EleCache3;
import it.accenture.jnais.ws.occurs.EleCache35;
import it.accenture.jnais.ws.occurs.EleCache37;
import it.accenture.jnais.ws.occurs.EleCache9;
import it.accenture.jnais.ws.occurs.Wp84TabP85;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LRGS0660<br>
 * Generated as a class for rule WS.<br>*/
public class Lrgs0660Data {

    //==== PROPERTIES ====
    public static final int ELE_AREA_ERR_MAXOCCURS = 500;
    public static final int WP84_TAB_P85_MAXOCCURS = 30;
    public static final int ELE_CACHE37_MAXOCCURS = 100;
    public static final int ELE_CACHE35_MAXOCCURS = 100;
    public static final int ELE_CACHE3_MAXOCCURS = 50;
    public static final int ELE_CACHE10_MAXOCCURS = 350;
    public static final int ELE_CACHE9_MAXOCCURS = 5;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *    COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LRGS0660";
    //Original name: AREA-FLAG
    private AreaFlag areaFlag = new AreaFlag();
    //Original name: TIPOLOGIA-EC
    private TipologiaEc tipologiaEc = new TipologiaEc();
    //Original name: IX-INDICI
    private IxIndiciLrgs0660 ixIndici = new IxIndiciLrgs0660();
    //Original name: WS-VARIABILI
    private WsVariabiliLrgs0660 wsVariabili = new WsVariabiliLrgs0660();
    //Original name: DATE-WORK
    private DateWork dateWork = new DateWork();
    //Original name: WORK-CALCOLI
    private WorkCalcoli workCalcoli = new WorkCalcoli();
    //Original name: TRACCIATO-FILE
    private TracciatoFile tracciatoFile = new TracciatoFile();
    //Original name: TRACCIATO-FILE-GRAV
    private TracciatoFileGrav tracciatoFileGrav = new TracciatoFileGrav();
    //Original name: ELE-MAX-ERRORI
    private int eleMaxErrori = DefaultValues.INT_VAL;
    //Original name: ELE-AREA-ERR
    private LazyArrayCopy<EleAreaErr> eleAreaErr = new LazyArrayCopy<EleAreaErr>(new EleAreaErr(), 1, ELE_AREA_ERR_MAXOCCURS);
    //Original name: WK-AREA-FILE
    private WkAreaFile wkAreaFile = new WkAreaFile();
    /**Original name: FLAG-TROVATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private FlagTrovato flagTrovato = new FlagTrovato();
    //Original name: FLAG-GAR-RAMO3
    private FlagGarRamo3 flagGarRamo3 = new FlagGarRamo3();
    //Original name: FLAG-GAR-RAMO3-STORNATA
    private FlagGarRamo3Stornata flagGarRamo3Stornata = new FlagGarRamo3Stornata();
    //Original name: WK-AREA-CONTATORI
    private WkAreaContatori wkAreaContatori = new WkAreaContatori();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: IABVSQS1
    private Iabvsqs1 iabvsqs1 = new Iabvsqs1();
    //Original name: IABCSQ99
    private Iabcsq99 iabcsq99 = new Iabcsq99();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: IMPST-SOST
    private ImpstSost impstSost = new ImpstSost();
    //Original name: LDBVF301
    private Ldbvf301 ldbvf301 = new Ldbvf301();
    //Original name: LDBVF321
    private Ldbvf321 ldbvf321 = new Ldbvf321();
    //Original name: LDBV6191
    private Ldbv6191 ldbv6191 = new Ldbv6191();
    //Original name: LOAR0171
    private Loar0171 loar0171 = new Loar0171();
    //Original name: ESTR-CNT-DIAGN-CED
    private EstrCntDiagnCedLrgs0660 estrCntDiagnCed = new EstrCntDiagnCedLrgs0660();
    //Original name: ESTR-CNT-DIAGN-RIV
    private EstrCntDiagnRivLrgs0660 estrCntDiagnRiv = new EstrCntDiagnRivLrgs0660();
    //Original name: WP84-TAB-P85
    private Wp84TabP85[] wp84TabP85 = new Wp84TabP85[WP84_TAB_P85_MAXOCCURS];
    //Original name: IX-MAX-C37
    private int ixMaxC37 = DefaultValues.INT_VAL;
    //Original name: ELE-CACHE-37
    private EleCache37[] eleCache37 = new EleCache37[ELE_CACHE37_MAXOCCURS];
    //Original name: IX-MAX-C35
    private int ixMaxC35 = DefaultValues.INT_VAL;
    //Original name: ELE-CACHE-35
    private EleCache35[] eleCache35 = new EleCache35[ELE_CACHE35_MAXOCCURS];
    //Original name: ELE-MAX-CACHE-3
    private String eleMaxCache3 = DefaultValues.stringVal(Len.ELE_MAX_CACHE3);
    //Original name: ELE-CACHE-3
    private EleCache3[] eleCache3 = new EleCache3[ELE_CACHE3_MAXOCCURS];
    //Original name: ELE-MAX-CACHE-10
    private String eleMaxCache10 = DefaultValues.stringVal(Len.ELE_MAX_CACHE10);
    //Original name: ELE-CACHE-10
    private EleCache10[] eleCache10 = new EleCache10[ELE_CACHE10_MAXOCCURS];
    //Original name: AREA-CACHE-8
    private AreaCache8 areaCache8 = new AreaCache8();
    //Original name: ELE-MAX-CACHE-9
    private String eleMaxCache9 = DefaultValues.stringVal(Len.ELE_MAX_CACHE9);
    //Original name: ELE-CACHE-9
    private EleCache9[] eleCache9 = new EleCache9[ELE_CACHE9_MAXOCCURS];
    //Original name: WRIN-REC-OUTPUT
    private WrinRecOutput wrinRecOutput = new WrinRecOutput();
    //Original name: WOUT-REC-OUTPUT
    private WrinRecOutput woutRecOutput = new WrinRecOutput();
    //Original name: AREA-IO-ISPS0580
    private AreaIoIsps0580 areaIoIsps0580 = new AreaIoIsps0580();
    //Original name: AREA-IO-ISPS0590
    private AreaIoIsps0590 areaIoIsps0590 = new AreaIoIsps0590();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: LRGC0661
    private Lrgc0661 lrgc0661 = new Lrgc0661();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();

    //==== CONSTRUCTORS ====
    public Lrgs0660Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wp84TabP85Idx = 1; wp84TabP85Idx <= WP84_TAB_P85_MAXOCCURS; wp84TabP85Idx++) {
            wp84TabP85[wp84TabP85Idx - 1] = new Wp84TabP85();
        }
        for (int eleCache37Idx = 1; eleCache37Idx <= ELE_CACHE37_MAXOCCURS; eleCache37Idx++) {
            eleCache37[eleCache37Idx - 1] = new EleCache37();
        }
        for (int eleCache35Idx = 1; eleCache35Idx <= ELE_CACHE35_MAXOCCURS; eleCache35Idx++) {
            eleCache35[eleCache35Idx - 1] = new EleCache35();
        }
        for (int eleCache3Idx = 1; eleCache3Idx <= ELE_CACHE3_MAXOCCURS; eleCache3Idx++) {
            eleCache3[eleCache3Idx - 1] = new EleCache3();
        }
        for (int eleCache10Idx = 1; eleCache10Idx <= ELE_CACHE10_MAXOCCURS; eleCache10Idx++) {
            eleCache10[eleCache10Idx - 1] = new EleCache10();
        }
        for (int eleCache9Idx = 1; eleCache9Idx <= ELE_CACHE9_MAXOCCURS; eleCache9Idx++) {
            eleCache9[eleCache9Idx - 1] = new EleCache9();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setEleMaxErrori(int eleMaxErrori) {
        this.eleMaxErrori = eleMaxErrori;
    }

    public int getEleMaxErrori() {
        return this.eleMaxErrori;
    }

    public void setIxMaxC37(int ixMaxC37) {
        this.ixMaxC37 = ixMaxC37;
    }

    public int getIxMaxC37() {
        return this.ixMaxC37;
    }

    public void setIxMaxC35(int ixMaxC35) {
        this.ixMaxC35 = ixMaxC35;
    }

    public int getIxMaxC35() {
        return this.ixMaxC35;
    }

    public void setEleMaxCache3(int eleMaxCache3) {
        this.eleMaxCache3 = NumericDisplay.asString(eleMaxCache3, Len.ELE_MAX_CACHE3);
    }

    public void setEleMaxCache3Formatted(String eleMaxCache3) {
        this.eleMaxCache3 = Trunc.toUnsignedNumeric(eleMaxCache3, Len.ELE_MAX_CACHE3);
    }

    public int getEleMaxCache3() {
        return NumericDisplay.asInt(this.eleMaxCache3);
    }

    public void setEleMaxCache10(int eleMaxCache10) {
        this.eleMaxCache10 = NumericDisplay.asString(eleMaxCache10, Len.ELE_MAX_CACHE10);
    }

    public void setEleMaxCache10Formatted(String eleMaxCache10) {
        this.eleMaxCache10 = Trunc.toUnsignedNumeric(eleMaxCache10, Len.ELE_MAX_CACHE10);
    }

    public int getEleMaxCache10() {
        return NumericDisplay.asInt(this.eleMaxCache10);
    }

    public void setEleMaxCache9(int eleMaxCache9) {
        this.eleMaxCache9 = NumericDisplay.asString(eleMaxCache9, Len.ELE_MAX_CACHE9);
    }

    public void setEleMaxCache9Formatted(String eleMaxCache9) {
        this.eleMaxCache9 = Trunc.toUnsignedNumeric(eleMaxCache9, Len.ELE_MAX_CACHE9);
    }

    public int getEleMaxCache9() {
        return NumericDisplay.asInt(this.eleMaxCache9);
    }

    public void setWrinRecFileinFormatted(String data) {
        byte[] buffer = new byte[Len.WRIN_REC_FILEIN];
        MarshalByte.writeString(buffer, 1, data, Len.WRIN_REC_FILEIN);
        setWrinRecFileinBytes(buffer, 1);
    }

    public void setWrinRecFileinBytes(byte[] buffer, int offset) {
        int position = offset;
        wrinRecOutput.setWrinRecOutputBytes(buffer, position);
    }

    public String getAreaLrgc0031Formatted() {
        return woutRecOutput.getWrinRecOutputFormatted();
    }

    public AreaCache8 getAreaCache8() {
        return areaCache8;
    }

    public AreaFlag getAreaFlag() {
        return areaFlag;
    }

    public AreaIoIsps0580 getAreaIoIsps0580() {
        return areaIoIsps0580;
    }

    public AreaIoIsps0590 getAreaIoIsps0590() {
        return areaIoIsps0590;
    }

    public DateWork getDateWork() {
        return dateWork;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public EleAreaErr getEleAreaErr(int idx) {
        return eleAreaErr.get(idx - 1);
    }

    public EleCache10 getEleCache10(int idx) {
        return eleCache10[idx - 1];
    }

    public EleCache35 getEleCache35(int idx) {
        return eleCache35[idx - 1];
    }

    public EleCache37 getEleCache37(int idx) {
        return eleCache37[idx - 1];
    }

    public EleCache3 getEleCache3(int idx) {
        return eleCache3[idx - 1];
    }

    public EleCache9 getEleCache9(int idx) {
        return eleCache9[idx - 1];
    }

    public EstrCntDiagnCedLrgs0660 getEstrCntDiagnCed() {
        return estrCntDiagnCed;
    }

    public EstrCntDiagnRivLrgs0660 getEstrCntDiagnRiv() {
        return estrCntDiagnRiv;
    }

    public FlagGarRamo3 getFlagGarRamo3() {
        return flagGarRamo3;
    }

    public FlagGarRamo3Stornata getFlagGarRamo3Stornata() {
        return flagGarRamo3Stornata;
    }

    public FlagTrovato getFlagTrovato() {
        return flagTrovato;
    }

    public Iabcsq99 getIabcsq99() {
        return iabcsq99;
    }

    public Iabvsqs1 getIabvsqs1() {
        return iabvsqs1;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public ImpstSost getImpstSost() {
        return impstSost;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public IxIndiciLrgs0660 getIxIndici() {
        return ixIndici;
    }

    public Ldbv6191 getLdbv6191() {
        return ldbv6191;
    }

    public Ldbvf301 getLdbvf301() {
        return ldbvf301;
    }

    public Ldbvf321 getLdbvf321() {
        return ldbvf321;
    }

    public Loar0171 getLoar0171() {
        return loar0171;
    }

    public Lrgc0661 getLrgc0661() {
        return lrgc0661;
    }

    public Poli getPoli() {
        return poli;
    }

    public TipologiaEc getTipologiaEc() {
        return tipologiaEc;
    }

    public TracciatoFile getTracciatoFile() {
        return tracciatoFile;
    }

    public TracciatoFileGrav getTracciatoFileGrav() {
        return tracciatoFileGrav;
    }

    public WkAreaContatori getWkAreaContatori() {
        return wkAreaContatori;
    }

    public WkAreaFile getWkAreaFile() {
        return wkAreaFile;
    }

    public WorkCalcoli getWorkCalcoli() {
        return workCalcoli;
    }

    public WrinRecOutput getWoutRecOutput() {
        return woutRecOutput;
    }

    public Wp84TabP85 getWp84TabP85(int idx) {
        return wp84TabP85[idx - 1];
    }

    public WrinRecOutput getWrinRecOutput() {
        return wrinRecOutput;
    }

    public WsVariabiliLrgs0660 getWsVariabili() {
        return wsVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_ERRORI = 9;
        public static final int IX_MAX_C37 = 9;
        public static final int IX_MAX_C35 = 9;
        public static final int ELE_MAX_CACHE3 = 9;
        public static final int ELE_MAX_CACHE10 = 9;
        public static final int ELE_MAX_CACHE9 = 9;
        public static final int WRIN_REC_FILEIN = WrinRecOutput.Len.WRIN_REC_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
