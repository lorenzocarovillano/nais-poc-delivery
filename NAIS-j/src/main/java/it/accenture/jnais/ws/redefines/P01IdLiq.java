package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-ID-LIQ<br>
 * Variable: P01-ID-LIQ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01IdLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01IdLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_ID_LIQ;
    }

    public void setP01IdLiq(int p01IdLiq) {
        writeIntAsPacked(Pos.P01_ID_LIQ, p01IdLiq, Len.Int.P01_ID_LIQ);
    }

    public void setP01IdLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_ID_LIQ, Pos.P01_ID_LIQ);
    }

    /**Original name: P01-ID-LIQ<br>*/
    public int getP01IdLiq() {
        return readPackedAsInt(Pos.P01_ID_LIQ, Len.Int.P01_ID_LIQ);
    }

    public byte[] getP01IdLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_ID_LIQ, Pos.P01_ID_LIQ);
        return buffer;
    }

    public void setP01IdLiqNull(String p01IdLiqNull) {
        writeString(Pos.P01_ID_LIQ_NULL, p01IdLiqNull, Len.P01_ID_LIQ_NULL);
    }

    /**Original name: P01-ID-LIQ-NULL<br>*/
    public String getP01IdLiqNull() {
        return readString(Pos.P01_ID_LIQ_NULL, Len.P01_ID_LIQ_NULL);
    }

    public String getP01IdLiqNullFormatted() {
        return Functions.padBlanks(getP01IdLiqNull(), Len.P01_ID_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_ID_LIQ = 1;
        public static final int P01_ID_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_ID_LIQ = 5;
        public static final int P01_ID_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_ID_LIQ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
