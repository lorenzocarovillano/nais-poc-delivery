package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IINT-PREST-EFFLQ<br>
 * Variable: WDFL-IINT-PREST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIintPrestEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIintPrestEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IINT_PREST_EFFLQ;
    }

    public void setWdflIintPrestEfflq(AfDecimal wdflIintPrestEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IINT_PREST_EFFLQ, wdflIintPrestEfflq.copy());
    }

    public void setWdflIintPrestEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IINT_PREST_EFFLQ, Pos.WDFL_IINT_PREST_EFFLQ);
    }

    /**Original name: WDFL-IINT-PREST-EFFLQ<br>*/
    public AfDecimal getWdflIintPrestEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IINT_PREST_EFFLQ, Len.Int.WDFL_IINT_PREST_EFFLQ, Len.Fract.WDFL_IINT_PREST_EFFLQ);
    }

    public byte[] getWdflIintPrestEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IINT_PREST_EFFLQ, Pos.WDFL_IINT_PREST_EFFLQ);
        return buffer;
    }

    public void setWdflIintPrestEfflqNull(String wdflIintPrestEfflqNull) {
        writeString(Pos.WDFL_IINT_PREST_EFFLQ_NULL, wdflIintPrestEfflqNull, Len.WDFL_IINT_PREST_EFFLQ_NULL);
    }

    /**Original name: WDFL-IINT-PREST-EFFLQ-NULL<br>*/
    public String getWdflIintPrestEfflqNull() {
        return readString(Pos.WDFL_IINT_PREST_EFFLQ_NULL, Len.WDFL_IINT_PREST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IINT_PREST_EFFLQ = 1;
        public static final int WDFL_IINT_PREST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IINT_PREST_EFFLQ = 8;
        public static final int WDFL_IINT_PREST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IINT_PREST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IINT_PREST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
