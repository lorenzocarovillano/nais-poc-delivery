package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TABELLA-STATI-BATCH-ELEMENTS<br>
 * Variables: TABELLA-STATI-BATCH-ELEMENTS from copybook IABV0007<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TabellaStatiBatchElements {

    //==== PROPERTIES ====
    //Original name: BAT-COD-BATCH-STATE
    private char codBatchState = DefaultValues.CHAR_VAL;
    //Original name: BAT-DES
    private String des = DefaultValues.stringVal(Len.DES);
    //Original name: BAT-FLAG-TO-EXECUTE
    private char flagToExecute = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setCodBatchState(char codBatchState) {
        this.codBatchState = codBatchState;
    }

    public char getCodBatchState() {
        return this.codBatchState;
    }

    public void setDes(String des) {
        this.des = Functions.subString(des, Len.DES);
    }

    public String getDes() {
        return this.des;
    }

    public void setFlagToExecute(char flagToExecute) {
        this.flagToExecute = flagToExecute;
    }

    public char getFlagToExecute() {
        return this.flagToExecute;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DES = 50;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
