package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-LM-RIS-CON-INT<br>
 * Variable: WPCO-LM-RIS-CON-INT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoLmRisConInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoLmRisConInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_LM_RIS_CON_INT;
    }

    public void setWpcoLmRisConInt(AfDecimal wpcoLmRisConInt) {
        writeDecimalAsPacked(Pos.WPCO_LM_RIS_CON_INT, wpcoLmRisConInt.copy());
    }

    public void setDpcoLmRisConIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_LM_RIS_CON_INT, Pos.WPCO_LM_RIS_CON_INT);
    }

    /**Original name: WPCO-LM-RIS-CON-INT<br>*/
    public AfDecimal getWpcoLmRisConInt() {
        return readPackedAsDecimal(Pos.WPCO_LM_RIS_CON_INT, Len.Int.WPCO_LM_RIS_CON_INT, Len.Fract.WPCO_LM_RIS_CON_INT);
    }

    public byte[] getWpcoLmRisConIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_LM_RIS_CON_INT, Pos.WPCO_LM_RIS_CON_INT);
        return buffer;
    }

    public void setWpcoLmRisConIntNull(String wpcoLmRisConIntNull) {
        writeString(Pos.WPCO_LM_RIS_CON_INT_NULL, wpcoLmRisConIntNull, Len.WPCO_LM_RIS_CON_INT_NULL);
    }

    /**Original name: WPCO-LM-RIS-CON-INT-NULL<br>*/
    public String getWpcoLmRisConIntNull() {
        return readString(Pos.WPCO_LM_RIS_CON_INT_NULL, Len.WPCO_LM_RIS_CON_INT_NULL);
    }

    public String getWpcoLmRisConIntNullFormatted() {
        return Functions.padBlanks(getWpcoLmRisConIntNull(), Len.WPCO_LM_RIS_CON_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_LM_RIS_CON_INT = 1;
        public static final int WPCO_LM_RIS_CON_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_LM_RIS_CON_INT = 4;
        public static final int WPCO_LM_RIS_CON_INT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_LM_RIS_CON_INT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_LM_RIS_CON_INT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
