package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUGMA-X<br>
 * Variable: A2K-OUGMA-X from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class A2kOugmaX {

    //==== PROPERTIES ====
    //Original name: A2K-OUGG
    private String ougg = DefaultValues.stringVal(Len.OUGG);
    //Original name: A2K-OUMM
    private String oumm = DefaultValues.stringVal(Len.OUMM);
    //Original name: A2K-OUSS
    private String ouss = DefaultValues.stringVal(Len.OUSS);
    //Original name: A2K-OUAA
    private String ouaa = DefaultValues.stringVal(Len.OUAA);

    //==== METHODS ====
    public void setA2kOugmaFormatted(String a2kOugma) {
        int position = 1;
        byte[] buffer = getA2kOugmaXBytes();
        MarshalByte.writeString(buffer, position, Trunc.toNumeric(a2kOugma, Len.A2K_OUGMA), Len.A2K_OUGMA);
        setA2kOugmaXBytes(buffer);
    }

    /**Original name: A2K-OUGMA<br>*/
    public int getA2kOugma() {
        int position = 1;
        return MarshalByte.readInt(getA2kOugmaXBytes(), position, Len.Int.A2K_OUGMA, SignType.NO_SIGN);
    }

    public String getA2kOugmaFormatted() {
        int position = 1;
        return MarshalByte.readFixedString(getA2kOugmaXBytes(), position, Len.A2K_OUGMA);
    }

    public void setA2kOugmaXBytes(byte[] buffer) {
        setA2kOugmaXBytes(buffer, 1);
    }

    /**Original name: A2K-OUGMA-X<br>*/
    public byte[] getA2kOugmaXBytes() {
        byte[] buffer = new byte[Len.A2K_OUGMA_X];
        return getA2kOugmaXBytes(buffer, 1);
    }

    public void setA2kOugmaXBytes(byte[] buffer, int offset) {
        int position = offset;
        ougg = MarshalByte.readFixedString(buffer, position, Len.OUGG);
        position += Len.OUGG;
        oumm = MarshalByte.readFixedString(buffer, position, Len.OUMM);
        position += Len.OUMM;
        ouss = MarshalByte.readFixedString(buffer, position, Len.OUSS);
        position += Len.OUSS;
        ouaa = MarshalByte.readFixedString(buffer, position, Len.OUAA);
    }

    public byte[] getA2kOugmaXBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ougg, Len.OUGG);
        position += Len.OUGG;
        MarshalByte.writeString(buffer, position, oumm, Len.OUMM);
        position += Len.OUMM;
        MarshalByte.writeString(buffer, position, ouss, Len.OUSS);
        position += Len.OUSS;
        MarshalByte.writeString(buffer, position, ouaa, Len.OUAA);
        return buffer;
    }

    public void initA2kOugmaXSpaces() {
        ougg = "";
        oumm = "";
        ouss = "";
        ouaa = "";
    }

    public short getOumm() {
        return NumericDisplay.asShort(this.oumm);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUGG = 2;
        public static final int OUMM = 2;
        public static final int OUSS = 2;
        public static final int OUAA = 2;
        public static final int A2K_OUGMA_X = OUGG + OUMM + OUSS + OUAA;
        public static final int A2K_OUGMA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A2K_OUGMA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
