package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-CNBT-ANTIRAC<br>
 * Variable: WTIT-TOT-CNBT-ANTIRAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_CNBT_ANTIRAC;
    }

    public void setWtitTotCnbtAntirac(AfDecimal wtitTotCnbtAntirac) {
        writeDecimalAsPacked(Pos.WTIT_TOT_CNBT_ANTIRAC, wtitTotCnbtAntirac.copy());
    }

    public void setWtitTotCnbtAntiracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_CNBT_ANTIRAC, Pos.WTIT_TOT_CNBT_ANTIRAC);
    }

    /**Original name: WTIT-TOT-CNBT-ANTIRAC<br>*/
    public AfDecimal getWtitTotCnbtAntirac() {
        return readPackedAsDecimal(Pos.WTIT_TOT_CNBT_ANTIRAC, Len.Int.WTIT_TOT_CNBT_ANTIRAC, Len.Fract.WTIT_TOT_CNBT_ANTIRAC);
    }

    public byte[] getWtitTotCnbtAntiracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_CNBT_ANTIRAC, Pos.WTIT_TOT_CNBT_ANTIRAC);
        return buffer;
    }

    public void initWtitTotCnbtAntiracSpaces() {
        fill(Pos.WTIT_TOT_CNBT_ANTIRAC, Len.WTIT_TOT_CNBT_ANTIRAC, Types.SPACE_CHAR);
    }

    public void setWtitTotCnbtAntiracNull(String wtitTotCnbtAntiracNull) {
        writeString(Pos.WTIT_TOT_CNBT_ANTIRAC_NULL, wtitTotCnbtAntiracNull, Len.WTIT_TOT_CNBT_ANTIRAC_NULL);
    }

    /**Original name: WTIT-TOT-CNBT-ANTIRAC-NULL<br>*/
    public String getWtitTotCnbtAntiracNull() {
        return readString(Pos.WTIT_TOT_CNBT_ANTIRAC_NULL, Len.WTIT_TOT_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CNBT_ANTIRAC = 1;
        public static final int WTIT_TOT_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CNBT_ANTIRAC = 8;
        public static final int WTIT_TOT_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
