package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-MANFEE-REC<br>
 * Variable: WTIT-TOT-MANFEE-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotManfeeRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotManfeeRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_MANFEE_REC;
    }

    public void setWtitTotManfeeRec(AfDecimal wtitTotManfeeRec) {
        writeDecimalAsPacked(Pos.WTIT_TOT_MANFEE_REC, wtitTotManfeeRec.copy());
    }

    public void setWtitTotManfeeRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_MANFEE_REC, Pos.WTIT_TOT_MANFEE_REC);
    }

    /**Original name: WTIT-TOT-MANFEE-REC<br>*/
    public AfDecimal getWtitTotManfeeRec() {
        return readPackedAsDecimal(Pos.WTIT_TOT_MANFEE_REC, Len.Int.WTIT_TOT_MANFEE_REC, Len.Fract.WTIT_TOT_MANFEE_REC);
    }

    public byte[] getWtitTotManfeeRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_MANFEE_REC, Pos.WTIT_TOT_MANFEE_REC);
        return buffer;
    }

    public void initWtitTotManfeeRecSpaces() {
        fill(Pos.WTIT_TOT_MANFEE_REC, Len.WTIT_TOT_MANFEE_REC, Types.SPACE_CHAR);
    }

    public void setWtitTotManfeeRecNull(String wtitTotManfeeRecNull) {
        writeString(Pos.WTIT_TOT_MANFEE_REC_NULL, wtitTotManfeeRecNull, Len.WTIT_TOT_MANFEE_REC_NULL);
    }

    /**Original name: WTIT-TOT-MANFEE-REC-NULL<br>*/
    public String getWtitTotManfeeRecNull() {
        return readString(Pos.WTIT_TOT_MANFEE_REC_NULL, Len.WTIT_TOT_MANFEE_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_MANFEE_REC = 1;
        public static final int WTIT_TOT_MANFEE_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_MANFEE_REC = 8;
        public static final int WTIT_TOT_MANFEE_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_MANFEE_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_MANFEE_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
