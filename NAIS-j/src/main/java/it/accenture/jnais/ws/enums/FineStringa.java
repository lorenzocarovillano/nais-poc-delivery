package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FINE-STRINGA<br>
 * Variable: FINE-STRINGA from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FineStringa {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFineStringa(char fineStringa) {
        this.value = fineStringa;
    }

    public char getFineStringa() {
        return this.value;
    }

    public boolean isFineStringaSi() {
        return value == SI;
    }

    public void setFineStringaSi() {
        value = SI;
    }

    public void setFineStringaNo() {
        value = NO;
    }
}
