package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-INTR-RIT-PAG<br>
 * Variable: S089-IMP-INTR-RIT-PAG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_INTR_RIT_PAG;
    }

    public void setWlquImpIntrRitPag(AfDecimal wlquImpIntrRitPag) {
        writeDecimalAsPacked(Pos.S089_IMP_INTR_RIT_PAG, wlquImpIntrRitPag.copy());
    }

    public void setWlquImpIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_INTR_RIT_PAG, Pos.S089_IMP_INTR_RIT_PAG);
    }

    /**Original name: WLQU-IMP-INTR-RIT-PAG<br>*/
    public AfDecimal getWlquImpIntrRitPag() {
        return readPackedAsDecimal(Pos.S089_IMP_INTR_RIT_PAG, Len.Int.WLQU_IMP_INTR_RIT_PAG, Len.Fract.WLQU_IMP_INTR_RIT_PAG);
    }

    public byte[] getWlquImpIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_INTR_RIT_PAG, Pos.S089_IMP_INTR_RIT_PAG);
        return buffer;
    }

    public void initWlquImpIntrRitPagSpaces() {
        fill(Pos.S089_IMP_INTR_RIT_PAG, Len.S089_IMP_INTR_RIT_PAG, Types.SPACE_CHAR);
    }

    public void setWlquImpIntrRitPagNull(String wlquImpIntrRitPagNull) {
        writeString(Pos.S089_IMP_INTR_RIT_PAG_NULL, wlquImpIntrRitPagNull, Len.WLQU_IMP_INTR_RIT_PAG_NULL);
    }

    /**Original name: WLQU-IMP-INTR-RIT-PAG-NULL<br>*/
    public String getWlquImpIntrRitPagNull() {
        return readString(Pos.S089_IMP_INTR_RIT_PAG_NULL, Len.WLQU_IMP_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_INTR_RIT_PAG = 1;
        public static final int S089_IMP_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_INTR_RIT_PAG = 8;
        public static final int WLQU_IMP_INTR_RIT_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_INTR_RIT_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_INTR_RIT_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
