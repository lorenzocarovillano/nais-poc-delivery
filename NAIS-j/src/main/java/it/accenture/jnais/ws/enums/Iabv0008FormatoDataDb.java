package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-FORMATO-DATA-DB<br>
 * Variable: IABV0008-FORMATO-DATA-DB from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008FormatoDataDb {

    //==== PROPERTIES ====
    private String value = "ISO";
    public static final String ISO = "ISO";
    public static final String EUR = "EUR";

    //==== METHODS ====
    public String getFormatoDataDb() {
        return this.value;
    }
}
