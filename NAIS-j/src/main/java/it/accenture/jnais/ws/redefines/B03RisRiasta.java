package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-RIASTA<br>
 * Variable: B03-RIS-RIASTA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisRiasta extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisRiasta() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_RIASTA;
    }

    public void setB03RisRiasta(AfDecimal b03RisRiasta) {
        writeDecimalAsPacked(Pos.B03_RIS_RIASTA, b03RisRiasta.copy());
    }

    public void setB03RisRiastaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_RIASTA, Pos.B03_RIS_RIASTA);
    }

    /**Original name: B03-RIS-RIASTA<br>*/
    public AfDecimal getB03RisRiasta() {
        return readPackedAsDecimal(Pos.B03_RIS_RIASTA, Len.Int.B03_RIS_RIASTA, Len.Fract.B03_RIS_RIASTA);
    }

    public byte[] getB03RisRiastaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_RIASTA, Pos.B03_RIS_RIASTA);
        return buffer;
    }

    public void setB03RisRiastaNull(String b03RisRiastaNull) {
        writeString(Pos.B03_RIS_RIASTA_NULL, b03RisRiastaNull, Len.B03_RIS_RIASTA_NULL);
    }

    /**Original name: B03-RIS-RIASTA-NULL<br>*/
    public String getB03RisRiastaNull() {
        return readString(Pos.B03_RIS_RIASTA_NULL, Len.B03_RIS_RIASTA_NULL);
    }

    public String getB03RisRiastaNullFormatted() {
        return Functions.padBlanks(getB03RisRiastaNull(), Len.B03_RIS_RIASTA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_RIASTA = 1;
        public static final int B03_RIS_RIASTA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_RIASTA = 8;
        public static final int B03_RIS_RIASTA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_RIASTA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_RIASTA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
