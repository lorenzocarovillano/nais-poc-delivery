package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-VARLIST<br>
 * Variable: WK-VARLIST from program IVVS0212<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkVarlist {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_FINE_VARLIST = 'S';
    public static final char NO_FINE_VARLIST = 'N';

    //==== METHODS ====
    public void setWkVarlist(char wkVarlist) {
        this.value = wkVarlist;
    }

    public char getWkVarlist() {
        return this.value;
    }

    public boolean isSiFineVarlist() {
        return value == SI_FINE_VARLIST;
    }

    public void setSiFineVarlist() {
        value = SI_FINE_VARLIST;
    }

    public void setNoFineVarlist() {
        value = NO_FINE_VARLIST;
    }
}
