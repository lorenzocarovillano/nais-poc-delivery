package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P58-IMPST-BOLLO-DETT-V<br>
 * Variable: P58-IMPST-BOLLO-DETT-V from program IDBSP580<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P58ImpstBolloDettV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P58ImpstBolloDettV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P58_IMPST_BOLLO_DETT_V;
    }

    public void setP58ImpstBolloDettV(AfDecimal p58ImpstBolloDettV) {
        writeDecimalAsPacked(Pos.P58_IMPST_BOLLO_DETT_V, p58ImpstBolloDettV.copy());
    }

    public void setP58ImpstBolloDettVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P58_IMPST_BOLLO_DETT_V, Pos.P58_IMPST_BOLLO_DETT_V);
    }

    /**Original name: P58-IMPST-BOLLO-DETT-V<br>*/
    public AfDecimal getP58ImpstBolloDettV() {
        return readPackedAsDecimal(Pos.P58_IMPST_BOLLO_DETT_V, Len.Int.P58_IMPST_BOLLO_DETT_V, Len.Fract.P58_IMPST_BOLLO_DETT_V);
    }

    public byte[] getP58ImpstBolloDettVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P58_IMPST_BOLLO_DETT_V, Pos.P58_IMPST_BOLLO_DETT_V);
        return buffer;
    }

    public void setP58ImpstBolloDettVNull(String p58ImpstBolloDettVNull) {
        writeString(Pos.P58_IMPST_BOLLO_DETT_V_NULL, p58ImpstBolloDettVNull, Len.P58_IMPST_BOLLO_DETT_V_NULL);
    }

    /**Original name: P58-IMPST-BOLLO-DETT-V-NULL<br>*/
    public String getP58ImpstBolloDettVNull() {
        return readString(Pos.P58_IMPST_BOLLO_DETT_V_NULL, Len.P58_IMPST_BOLLO_DETT_V_NULL);
    }

    public String getP58ImpstBolloDettVNullFormatted() {
        return Functions.padBlanks(getP58ImpstBolloDettVNull(), Len.P58_IMPST_BOLLO_DETT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P58_IMPST_BOLLO_DETT_V = 1;
        public static final int P58_IMPST_BOLLO_DETT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P58_IMPST_BOLLO_DETT_V = 8;
        public static final int P58_IMPST_BOLLO_DETT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P58_IMPST_BOLLO_DETT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P58_IMPST_BOLLO_DETT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
