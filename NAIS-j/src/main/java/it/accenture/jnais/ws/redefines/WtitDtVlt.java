package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-VLT<br>
 * Variable: WTIT-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_VLT;
    }

    public void setWtitDtVlt(int wtitDtVlt) {
        writeIntAsPacked(Pos.WTIT_DT_VLT, wtitDtVlt, Len.Int.WTIT_DT_VLT);
    }

    public void setWtitDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_VLT, Pos.WTIT_DT_VLT);
    }

    /**Original name: WTIT-DT-VLT<br>*/
    public int getWtitDtVlt() {
        return readPackedAsInt(Pos.WTIT_DT_VLT, Len.Int.WTIT_DT_VLT);
    }

    public byte[] getWtitDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_VLT, Pos.WTIT_DT_VLT);
        return buffer;
    }

    public void initWtitDtVltSpaces() {
        fill(Pos.WTIT_DT_VLT, Len.WTIT_DT_VLT, Types.SPACE_CHAR);
    }

    public void setWtitDtVltNull(String wtitDtVltNull) {
        writeString(Pos.WTIT_DT_VLT_NULL, wtitDtVltNull, Len.WTIT_DT_VLT_NULL);
    }

    /**Original name: WTIT-DT-VLT-NULL<br>*/
    public String getWtitDtVltNull() {
        return readString(Pos.WTIT_DT_VLT_NULL, Len.WTIT_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_VLT = 1;
        public static final int WTIT_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_VLT = 5;
        public static final int WTIT_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
