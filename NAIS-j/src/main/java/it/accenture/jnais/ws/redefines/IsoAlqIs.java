package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-ALQ-IS<br>
 * Variable: ISO-ALQ-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoAlqIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoAlqIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_ALQ_IS;
    }

    public void setIsoAlqIs(AfDecimal isoAlqIs) {
        writeDecimalAsPacked(Pos.ISO_ALQ_IS, isoAlqIs.copy());
    }

    public void setIsoAlqIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_ALQ_IS, Pos.ISO_ALQ_IS);
    }

    /**Original name: ISO-ALQ-IS<br>*/
    public AfDecimal getIsoAlqIs() {
        return readPackedAsDecimal(Pos.ISO_ALQ_IS, Len.Int.ISO_ALQ_IS, Len.Fract.ISO_ALQ_IS);
    }

    public byte[] getIsoAlqIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_ALQ_IS, Pos.ISO_ALQ_IS);
        return buffer;
    }

    public void setIsoAlqIsNull(String isoAlqIsNull) {
        writeString(Pos.ISO_ALQ_IS_NULL, isoAlqIsNull, Len.ISO_ALQ_IS_NULL);
    }

    /**Original name: ISO-ALQ-IS-NULL<br>*/
    public String getIsoAlqIsNull() {
        return readString(Pos.ISO_ALQ_IS_NULL, Len.ISO_ALQ_IS_NULL);
    }

    public String getIsoAlqIsNullFormatted() {
        return Functions.padBlanks(getIsoAlqIsNull(), Len.ISO_ALQ_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_ALQ_IS = 1;
        public static final int ISO_ALQ_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_ALQ_IS = 4;
        public static final int ISO_ALQ_IS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_ALQ_IS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_ALQ_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
