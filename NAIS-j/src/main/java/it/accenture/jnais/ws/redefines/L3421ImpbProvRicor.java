package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMPB-PROV-RICOR<br>
 * Variable: L3421-IMPB-PROV-RICOR from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpbProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpbProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMPB_PROV_RICOR;
    }

    public void setL3421ImpbProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMPB_PROV_RICOR, Pos.L3421_IMPB_PROV_RICOR);
    }

    /**Original name: L3421-IMPB-PROV-RICOR<br>*/
    public AfDecimal getL3421ImpbProvRicor() {
        return readPackedAsDecimal(Pos.L3421_IMPB_PROV_RICOR, Len.Int.L3421_IMPB_PROV_RICOR, Len.Fract.L3421_IMPB_PROV_RICOR);
    }

    public byte[] getL3421ImpbProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMPB_PROV_RICOR, Pos.L3421_IMPB_PROV_RICOR);
        return buffer;
    }

    /**Original name: L3421-IMPB-PROV-RICOR-NULL<br>*/
    public String getL3421ImpbProvRicorNull() {
        return readString(Pos.L3421_IMPB_PROV_RICOR_NULL, Len.L3421_IMPB_PROV_RICOR_NULL);
    }

    public String getL3421ImpbProvRicorNullFormatted() {
        return Functions.padBlanks(getL3421ImpbProvRicorNull(), Len.L3421_IMPB_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_PROV_RICOR = 1;
        public static final int L3421_IMPB_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMPB_PROV_RICOR = 8;
        public static final int L3421_IMPB_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMPB_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
