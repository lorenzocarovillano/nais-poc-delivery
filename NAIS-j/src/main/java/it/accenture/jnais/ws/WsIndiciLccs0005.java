package it.accenture.jnais.ws;

/**Original name: WS-INDICI<br>
 * Variable: WS-INDICI from program LCCS0005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsIndiciLccs0005 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-MDE
    private short mde = ((short)0);
    //Original name: IX-TAB-TGA
    private short tga = ((short)0);
    //Original name: IX-TAB-GRZ
    private short grz = ((short)0);

    //==== METHODS ====
    public void setMde(short mde) {
        this.mde = mde;
    }

    public short getMde() {
        return this.mde;
    }

    public void setTga(short tga) {
        this.tga = tga;
    }

    public short getTga() {
        return this.tga;
    }

    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }
}
