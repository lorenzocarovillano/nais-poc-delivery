package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-ACQ<br>
 * Variable: WPAG-IMP-ACQ from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ACQ;
    }

    public void setWpagImpAcq(AfDecimal wpagImpAcq) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ACQ, wpagImpAcq.copy());
    }

    public void setWpagImpAcqFormatted(String wpagImpAcq) {
        setWpagImpAcq(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_ACQ + Len.Fract.WPAG_IMP_ACQ, Len.Fract.WPAG_IMP_ACQ, wpagImpAcq));
    }

    public void setWpagImpAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ACQ, Pos.WPAG_IMP_ACQ);
    }

    /**Original name: WPAG-IMP-ACQ<br>*/
    public AfDecimal getWpagImpAcq() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ACQ, Len.Int.WPAG_IMP_ACQ, Len.Fract.WPAG_IMP_ACQ);
    }

    public byte[] getWpagImpAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ACQ, Pos.WPAG_IMP_ACQ);
        return buffer;
    }

    public void initWpagImpAcqSpaces() {
        fill(Pos.WPAG_IMP_ACQ, Len.WPAG_IMP_ACQ, Types.SPACE_CHAR);
    }

    public void setWpagImpAcqNull(String wpagImpAcqNull) {
        writeString(Pos.WPAG_IMP_ACQ_NULL, wpagImpAcqNull, Len.WPAG_IMP_ACQ_NULL);
    }

    /**Original name: WPAG-IMP-ACQ-NULL<br>*/
    public String getWpagImpAcqNull() {
        return readString(Pos.WPAG_IMP_ACQ_NULL, Len.WPAG_IMP_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ACQ = 1;
        public static final int WPAG_IMP_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ACQ = 8;
        public static final int WPAG_IMP_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
