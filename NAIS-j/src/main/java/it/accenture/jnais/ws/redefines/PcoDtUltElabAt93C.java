package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-AT93-C<br>
 * Variable: PCO-DT-ULT-ELAB-AT93-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabAt93C extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabAt93C() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_AT93_C;
    }

    public void setPcoDtUltElabAt93C(int pcoDtUltElabAt93C) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_AT93_C, pcoDtUltElabAt93C, Len.Int.PCO_DT_ULT_ELAB_AT93_C);
    }

    public void setPcoDtUltElabAt93CFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT93_C, Pos.PCO_DT_ULT_ELAB_AT93_C);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT93-C<br>*/
    public int getPcoDtUltElabAt93C() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_AT93_C, Len.Int.PCO_DT_ULT_ELAB_AT93_C);
    }

    public byte[] getPcoDtUltElabAt93CAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_AT93_C, Pos.PCO_DT_ULT_ELAB_AT93_C);
        return buffer;
    }

    public void initPcoDtUltElabAt93CHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_AT93_C, Len.PCO_DT_ULT_ELAB_AT93_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabAt93CNull(String pcoDtUltElabAt93CNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_AT93_C_NULL, pcoDtUltElabAt93CNull, Len.PCO_DT_ULT_ELAB_AT93_C_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-AT93-C-NULL<br>*/
    public String getPcoDtUltElabAt93CNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_AT93_C_NULL, Len.PCO_DT_ULT_ELAB_AT93_C_NULL);
    }

    public String getPcoDtUltElabAt93CNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabAt93CNull(), Len.PCO_DT_ULT_ELAB_AT93_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT93_C = 1;
        public static final int PCO_DT_ULT_ELAB_AT93_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_AT93_C = 5;
        public static final int PCO_DT_ULT_ELAB_AT93_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_AT93_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
