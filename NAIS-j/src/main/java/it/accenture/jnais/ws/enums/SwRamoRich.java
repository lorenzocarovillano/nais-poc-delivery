package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-RAMO-RICH<br>
 * Variable: SW-RAMO-RICH from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwRamoRich {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setSwRamoRich(char swRamoRich) {
        this.value = swRamoRich;
    }

    public char getSwRamoRich() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
