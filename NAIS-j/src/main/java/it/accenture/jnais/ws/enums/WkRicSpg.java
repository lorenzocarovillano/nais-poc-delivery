package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-RIC-SPG<br>
 * Variable: WK-RIC-SPG from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkRicSpg {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATA = 'S';
    public static final char NON_TROVATA = 'N';

    //==== METHODS ====
    public void setWkRicSpg(char wkRicSpg) {
        this.value = wkRicSpg;
    }

    public char getWkRicSpg() {
        return this.value;
    }

    public boolean isTrovata() {
        return value == TROVATA;
    }

    public void setTrovata() {
        value = TROVATA;
    }

    public void setNonTrovata() {
        value = NON_TROVATA;
    }
}
