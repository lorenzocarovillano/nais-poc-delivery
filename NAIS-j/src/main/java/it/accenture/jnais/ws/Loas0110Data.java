package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.ws.enums.WcomFlagConversazione;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0110<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0110Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------
	 *      VARIABILI DI WORKING
	 * ----------------------------------------------------------------</pre>*/
    private String wkPgm = "LOAS0110";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    /**Original name: IX-TAB-PMO<br>
	 * <pre>  ---------------------------------------------------------------
	 *      INDICI
	 *   ---------------------------------------------------------------</pre>*/
    private short ixTabPmo = ((short)0);
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: AREA-IO-LCCS0090
    private LinkArea areaIoLccs0090 = new LinkArea();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: PARAM-MOVI
    private ParamMovi paramMovi = new ParamMovi();
    /**Original name: WK-TP-OGG-MBS<br>
	 * <pre>----------------------------------------------------------------*
	 *     ALTRE AREE
	 * ----------------------------------------------------------------*</pre>*/
    private String wkTpOggMbs = DefaultValues.stringVal(Len.WK_TP_OGG_MBS);
    //Original name: WK-LABEL-ERR
    private String wkLabelErr = "";
    /**Original name: WCOM-FLAG-CONVERSAZIONE<br>
	 * <pre> --  FLAG VENDITA - POST VENDITA</pre>*/
    private WcomFlagConversazione wcomFlagConversazione = new WcomFlagConversazione();
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    //Original name: AREA-IO-LCCS0005
    private Lccv0005 lccv0005 = new Lccv0005();
    //Original name: WRRE-AREA-RAP-RETE
    private WrreAreaRappRete wrreAreaRapRete = new WrreAreaRappRete();
    //Original name: WNOT-AREA-NOTE-OGG
    private WnotAreaNoteOgg wnotAreaNoteOgg = new WnotAreaNoteOgg();
    //Original name: S234-DATI-INPUT
    private S234DatiInput s234DatiInput = new S234DatiInput();
    //Original name: S234-DATI-OUTPUT
    private WcomDatiOutput s234DatiOutput = new WcomDatiOutput();
    //Original name: S0024-AREA-COMUNE
    private S0024AreaComune s0024AreaComune = new S0024AreaComune();
    //Original name: AREA-IO-LCCS0024
    private AreaIoLccs0024 areaIoLccs0024 = new AreaIoLccs0024();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setIxTabPmo(short ixTabPmo) {
        this.ixTabPmo = ixTabPmo;
    }

    public short getIxTabPmo() {
        return this.ixTabPmo;
    }

    public void setWkTpOggMbs(String wkTpOggMbs) {
        this.wkTpOggMbs = Functions.subString(wkTpOggMbs, Len.WK_TP_OGG_MBS);
    }

    public String getWkTpOggMbs() {
        return this.wkTpOggMbs;
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public AreaIoLccs0024 getAreaIoLccs0024() {
        return areaIoLccs0024;
    }

    public LinkArea getAreaIoLccs0090() {
        return areaIoLccs0090;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Lccv0005 getLccv0005() {
        return lccv0005;
    }

    public ParamMovi getParamMovi() {
        return paramMovi;
    }

    public S0024AreaComune getS0024AreaComune() {
        return s0024AreaComune;
    }

    public S234DatiInput getS234DatiInput() {
        return s234DatiInput;
    }

    public WcomDatiOutput getS234DatiOutput() {
        return s234DatiOutput;
    }

    public WcomFlagConversazione getWcomFlagConversazione() {
        return wcomFlagConversazione;
    }

    public WnotAreaNoteOgg getWnotAreaNoteOgg() {
        return wnotAreaNoteOgg;
    }

    public WrreAreaRappRete getWrreAreaRapRete() {
        return wrreAreaRapRete;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TP_OGG_MBS = 2;
        public static final int WK_LABEL_ERR = 30;
        public static final int WK_TABELLA = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
