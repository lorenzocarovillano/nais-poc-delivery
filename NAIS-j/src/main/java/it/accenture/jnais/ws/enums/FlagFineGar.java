package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-GAR<br>
 * Variable: FLAG-FINE-GAR from program LVVS2760<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineGar {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagFineGar(char flagFineGar) {
        this.value = flagFineGar;
    }

    public char getFlagFineGar() {
        return this.value;
    }
}
