package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-RAT-LRD-IND<br>
 * Variable: WADE-RAT-LRD-IND from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeRatLrdInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeRatLrdInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_RAT_LRD_IND;
    }

    public void setWadeRatLrdInd(AfDecimal wadeRatLrdInd) {
        writeDecimalAsPacked(Pos.WADE_RAT_LRD_IND, wadeRatLrdInd.copy());
    }

    public void setWadeRatLrdIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_RAT_LRD_IND, Pos.WADE_RAT_LRD_IND);
    }

    /**Original name: WADE-RAT-LRD-IND<br>*/
    public AfDecimal getWadeRatLrdInd() {
        return readPackedAsDecimal(Pos.WADE_RAT_LRD_IND, Len.Int.WADE_RAT_LRD_IND, Len.Fract.WADE_RAT_LRD_IND);
    }

    public byte[] getWadeRatLrdIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_RAT_LRD_IND, Pos.WADE_RAT_LRD_IND);
        return buffer;
    }

    public void initWadeRatLrdIndSpaces() {
        fill(Pos.WADE_RAT_LRD_IND, Len.WADE_RAT_LRD_IND, Types.SPACE_CHAR);
    }

    public void setWadeRatLrdIndNull(String wadeRatLrdIndNull) {
        writeString(Pos.WADE_RAT_LRD_IND_NULL, wadeRatLrdIndNull, Len.WADE_RAT_LRD_IND_NULL);
    }

    /**Original name: WADE-RAT-LRD-IND-NULL<br>*/
    public String getWadeRatLrdIndNull() {
        return readString(Pos.WADE_RAT_LRD_IND_NULL, Len.WADE_RAT_LRD_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_RAT_LRD_IND = 1;
        public static final int WADE_RAT_LRD_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_RAT_LRD_IND = 8;
        public static final int WADE_RAT_LRD_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_RAT_LRD_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_RAT_LRD_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
