package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-REMUN-ASS<br>
 * Variable: WTGA-REMUN-ASS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_REMUN_ASS;
    }

    public void setWtgaRemunAss(AfDecimal wtgaRemunAss) {
        writeDecimalAsPacked(Pos.WTGA_REMUN_ASS, wtgaRemunAss.copy());
    }

    public void setWtgaRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_REMUN_ASS, Pos.WTGA_REMUN_ASS);
    }

    /**Original name: WTGA-REMUN-ASS<br>*/
    public AfDecimal getWtgaRemunAss() {
        return readPackedAsDecimal(Pos.WTGA_REMUN_ASS, Len.Int.WTGA_REMUN_ASS, Len.Fract.WTGA_REMUN_ASS);
    }

    public byte[] getWtgaRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_REMUN_ASS, Pos.WTGA_REMUN_ASS);
        return buffer;
    }

    public void initWtgaRemunAssSpaces() {
        fill(Pos.WTGA_REMUN_ASS, Len.WTGA_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWtgaRemunAssNull(String wtgaRemunAssNull) {
        writeString(Pos.WTGA_REMUN_ASS_NULL, wtgaRemunAssNull, Len.WTGA_REMUN_ASS_NULL);
    }

    /**Original name: WTGA-REMUN-ASS-NULL<br>*/
    public String getWtgaRemunAssNull() {
        return readString(Pos.WTGA_REMUN_ASS_NULL, Len.WTGA_REMUN_ASS_NULL);
    }

    public String getWtgaRemunAssNullFormatted() {
        return Functions.padBlanks(getWtgaRemunAssNull(), Len.WTGA_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_REMUN_ASS = 1;
        public static final int WTGA_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_REMUN_ASS = 8;
        public static final int WTGA_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
