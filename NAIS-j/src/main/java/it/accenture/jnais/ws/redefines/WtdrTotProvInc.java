package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PROV-INC<br>
 * Variable: WTDR-TOT-PROV-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PROV_INC;
    }

    public void setWtdrTotProvInc(AfDecimal wtdrTotProvInc) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PROV_INC, wtdrTotProvInc.copy());
    }

    /**Original name: WTDR-TOT-PROV-INC<br>*/
    public AfDecimal getWtdrTotProvInc() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PROV_INC, Len.Int.WTDR_TOT_PROV_INC, Len.Fract.WTDR_TOT_PROV_INC);
    }

    public void setWtdrTotProvIncNull(String wtdrTotProvIncNull) {
        writeString(Pos.WTDR_TOT_PROV_INC_NULL, wtdrTotProvIncNull, Len.WTDR_TOT_PROV_INC_NULL);
    }

    /**Original name: WTDR-TOT-PROV-INC-NULL<br>*/
    public String getWtdrTotProvIncNull() {
        return readString(Pos.WTDR_TOT_PROV_INC_NULL, Len.WTDR_TOT_PROV_INC_NULL);
    }

    public String getWtdrTotProvIncNullFormatted() {
        return Functions.padBlanks(getWtdrTotProvIncNull(), Len.WTDR_TOT_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_INC = 1;
        public static final int WTDR_TOT_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PROV_INC = 8;
        public static final int WTDR_TOT_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
