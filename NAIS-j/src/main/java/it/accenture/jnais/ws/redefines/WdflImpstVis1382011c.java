package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-1382011C<br>
 * Variable: WDFL-IMPST-VIS-1382011C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVis1382011c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVis1382011c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS1382011C;
    }

    public void setWdflImpstVis1382011c(AfDecimal wdflImpstVis1382011c) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS1382011C, wdflImpstVis1382011c.copy());
    }

    public void setWdflImpstVis1382011cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS1382011C, Pos.WDFL_IMPST_VIS1382011C);
    }

    /**Original name: WDFL-IMPST-VIS-1382011C<br>*/
    public AfDecimal getWdflImpstVis1382011c() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS1382011C, Len.Int.WDFL_IMPST_VIS1382011C, Len.Fract.WDFL_IMPST_VIS1382011C);
    }

    public byte[] getWdflImpstVis1382011cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS1382011C, Pos.WDFL_IMPST_VIS1382011C);
        return buffer;
    }

    public void setWdflImpstVis1382011cNull(String wdflImpstVis1382011cNull) {
        writeString(Pos.WDFL_IMPST_VIS1382011C_NULL, wdflImpstVis1382011cNull, Len.WDFL_IMPST_VIS1382011C_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-1382011C-NULL<br>*/
    public String getWdflImpstVis1382011cNull() {
        return readString(Pos.WDFL_IMPST_VIS1382011C_NULL, Len.WDFL_IMPST_VIS1382011C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS1382011C = 1;
        public static final int WDFL_IMPST_VIS1382011C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS1382011C = 8;
        public static final int WDFL_IMPST_VIS1382011C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS1382011C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS1382011C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
