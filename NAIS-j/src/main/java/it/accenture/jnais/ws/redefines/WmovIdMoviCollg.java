package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WMOV-ID-MOVI-COLLG<br>
 * Variable: WMOV-ID-MOVI-COLLG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmovIdMoviCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmovIdMoviCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMOV_ID_MOVI_COLLG;
    }

    public void setWmovIdMoviCollg(int wmovIdMoviCollg) {
        writeIntAsPacked(Pos.WMOV_ID_MOVI_COLLG, wmovIdMoviCollg, Len.Int.WMOV_ID_MOVI_COLLG);
    }

    public void setWmovIdMoviCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMOV_ID_MOVI_COLLG, Pos.WMOV_ID_MOVI_COLLG);
    }

    /**Original name: WMOV-ID-MOVI-COLLG<br>*/
    public int getWmovIdMoviCollg() {
        return readPackedAsInt(Pos.WMOV_ID_MOVI_COLLG, Len.Int.WMOV_ID_MOVI_COLLG);
    }

    public byte[] getWmovIdMoviCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMOV_ID_MOVI_COLLG, Pos.WMOV_ID_MOVI_COLLG);
        return buffer;
    }

    public void setWmovIdMoviCollgNull(String wmovIdMoviCollgNull) {
        writeString(Pos.WMOV_ID_MOVI_COLLG_NULL, wmovIdMoviCollgNull, Len.WMOV_ID_MOVI_COLLG_NULL);
    }

    /**Original name: WMOV-ID-MOVI-COLLG-NULL<br>*/
    public String getWmovIdMoviCollgNull() {
        return readString(Pos.WMOV_ID_MOVI_COLLG_NULL, Len.WMOV_ID_MOVI_COLLG_NULL);
    }

    public String getWmovIdMoviCollgNullFormatted() {
        return Functions.padBlanks(getWmovIdMoviCollgNull(), Len.WMOV_ID_MOVI_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMOV_ID_MOVI_COLLG = 1;
        public static final int WMOV_ID_MOVI_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMOV_ID_MOVI_COLLG = 5;
        public static final int WMOV_ID_MOVI_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMOV_ID_MOVI_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
