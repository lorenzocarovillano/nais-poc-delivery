package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-IMP-TFR<br>
 * Variable: WTIT-IMP-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_IMP_TFR;
    }

    public void setWtitImpTfr(AfDecimal wtitImpTfr) {
        writeDecimalAsPacked(Pos.WTIT_IMP_TFR, wtitImpTfr.copy());
    }

    public void setWtitImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_IMP_TFR, Pos.WTIT_IMP_TFR);
    }

    /**Original name: WTIT-IMP-TFR<br>*/
    public AfDecimal getWtitImpTfr() {
        return readPackedAsDecimal(Pos.WTIT_IMP_TFR, Len.Int.WTIT_IMP_TFR, Len.Fract.WTIT_IMP_TFR);
    }

    public byte[] getWtitImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_IMP_TFR, Pos.WTIT_IMP_TFR);
        return buffer;
    }

    public void initWtitImpTfrSpaces() {
        fill(Pos.WTIT_IMP_TFR, Len.WTIT_IMP_TFR, Types.SPACE_CHAR);
    }

    public void setWtitImpTfrNull(String wtitImpTfrNull) {
        writeString(Pos.WTIT_IMP_TFR_NULL, wtitImpTfrNull, Len.WTIT_IMP_TFR_NULL);
    }

    /**Original name: WTIT-IMP-TFR-NULL<br>*/
    public String getWtitImpTfrNull() {
        return readString(Pos.WTIT_IMP_TFR_NULL, Len.WTIT_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_TFR = 1;
        public static final int WTIT_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_IMP_TFR = 8;
        public static final int WTIT_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
