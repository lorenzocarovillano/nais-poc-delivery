package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-PREST-ULT<br>
 * Variable: WPAG-IMP-PREST-ULT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpPrestUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpPrestUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_PREST_ULT;
    }

    public void setWpagImpPrestUlt(AfDecimal wpagImpPrestUlt) {
        writeDecimalAsPacked(Pos.WPAG_IMP_PREST_ULT, wpagImpPrestUlt.copy());
    }

    public void setWpagImpPrestUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_PREST_ULT, Pos.WPAG_IMP_PREST_ULT);
    }

    /**Original name: WPAG-IMP-PREST-ULT<br>*/
    public AfDecimal getWpagImpPrestUlt() {
        return readPackedAsDecimal(Pos.WPAG_IMP_PREST_ULT, Len.Int.WPAG_IMP_PREST_ULT, Len.Fract.WPAG_IMP_PREST_ULT);
    }

    public byte[] getWpagImpPrestUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_PREST_ULT, Pos.WPAG_IMP_PREST_ULT);
        return buffer;
    }

    public void initWpagImpPrestUltSpaces() {
        fill(Pos.WPAG_IMP_PREST_ULT, Len.WPAG_IMP_PREST_ULT, Types.SPACE_CHAR);
    }

    public void setWpagImpPrestUltNull(String wpagImpPrestUltNull) {
        writeString(Pos.WPAG_IMP_PREST_ULT_NULL, wpagImpPrestUltNull, Len.WPAG_IMP_PREST_ULT_NULL);
    }

    /**Original name: WPAG-IMP-PREST-ULT-NULL<br>*/
    public String getWpagImpPrestUltNull() {
        return readString(Pos.WPAG_IMP_PREST_ULT_NULL, Len.WPAG_IMP_PREST_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PREST_ULT = 1;
        public static final int WPAG_IMP_PREST_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PREST_ULT = 8;
        public static final int WPAG_IMP_PREST_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PREST_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PREST_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
