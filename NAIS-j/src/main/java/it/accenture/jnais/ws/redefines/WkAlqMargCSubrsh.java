package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-ALQ-MARG-C-SUBRSH<br>
 * Variable: WK-ALQ-MARG-C-SUBRSH from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkAlqMargCSubrsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkAlqMargCSubrsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_ALQ_MARG_C_SUBRSH;
    }

    public void setWkAlqMargCSubrsh(AfDecimal wkAlqMargCSubrsh) {
        writeDecimalAsPacked(Pos.WK_ALQ_MARG_C_SUBRSH, wkAlqMargCSubrsh.copy());
    }

    /**Original name: WK-ALQ-MARG-C-SUBRSH<br>*/
    public AfDecimal getWkAlqMargCSubrsh() {
        return readPackedAsDecimal(Pos.WK_ALQ_MARG_C_SUBRSH, Len.Int.WK_ALQ_MARG_C_SUBRSH, Len.Fract.WK_ALQ_MARG_C_SUBRSH);
    }

    public void setWkAlqMargCSubrshNull(String wkAlqMargCSubrshNull) {
        writeString(Pos.WK_ALQ_MARG_C_SUBRSH_NULL, wkAlqMargCSubrshNull, Len.WK_ALQ_MARG_C_SUBRSH_NULL);
    }

    /**Original name: WK-ALQ-MARG-C-SUBRSH-NULL<br>*/
    public String getWkAlqMargCSubrshNull() {
        return readString(Pos.WK_ALQ_MARG_C_SUBRSH_NULL, Len.WK_ALQ_MARG_C_SUBRSH_NULL);
    }

    public String getWkAlqMargCSubrshNullFormatted() {
        return Functions.padBlanks(getWkAlqMargCSubrshNull(), Len.WK_ALQ_MARG_C_SUBRSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_ALQ_MARG_C_SUBRSH = 1;
        public static final int WK_ALQ_MARG_C_SUBRSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_ALQ_MARG_C_SUBRSH = 4;
        public static final int WK_ALQ_MARG_C_SUBRSH_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
