package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-TAX-SEP-EFFLQ<br>
 * Variable: WDFL-IMPB-TAX-SEP-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbTaxSepEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbTaxSepEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_TAX_SEP_EFFLQ;
    }

    public void setWdflImpbTaxSepEfflq(AfDecimal wdflImpbTaxSepEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_TAX_SEP_EFFLQ, wdflImpbTaxSepEfflq.copy());
    }

    public void setWdflImpbTaxSepEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_TAX_SEP_EFFLQ, Pos.WDFL_IMPB_TAX_SEP_EFFLQ);
    }

    /**Original name: WDFL-IMPB-TAX-SEP-EFFLQ<br>*/
    public AfDecimal getWdflImpbTaxSepEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_TAX_SEP_EFFLQ, Len.Int.WDFL_IMPB_TAX_SEP_EFFLQ, Len.Fract.WDFL_IMPB_TAX_SEP_EFFLQ);
    }

    public byte[] getWdflImpbTaxSepEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_TAX_SEP_EFFLQ, Pos.WDFL_IMPB_TAX_SEP_EFFLQ);
        return buffer;
    }

    public void setWdflImpbTaxSepEfflqNull(String wdflImpbTaxSepEfflqNull) {
        writeString(Pos.WDFL_IMPB_TAX_SEP_EFFLQ_NULL, wdflImpbTaxSepEfflqNull, Len.WDFL_IMPB_TAX_SEP_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPB-TAX-SEP-EFFLQ-NULL<br>*/
    public String getWdflImpbTaxSepEfflqNull() {
        return readString(Pos.WDFL_IMPB_TAX_SEP_EFFLQ_NULL, Len.WDFL_IMPB_TAX_SEP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TAX_SEP_EFFLQ = 1;
        public static final int WDFL_IMPB_TAX_SEP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TAX_SEP_EFFLQ = 8;
        public static final int WDFL_IMPB_TAX_SEP_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TAX_SEP_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
