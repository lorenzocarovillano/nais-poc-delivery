package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-INTR-PREST<br>
 * Variable: WTDR-TOT-INTR-PREST from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_INTR_PREST;
    }

    public void setWtdrTotIntrPrest(AfDecimal wtdrTotIntrPrest) {
        writeDecimalAsPacked(Pos.WTDR_TOT_INTR_PREST, wtdrTotIntrPrest.copy());
    }

    /**Original name: WTDR-TOT-INTR-PREST<br>*/
    public AfDecimal getWtdrTotIntrPrest() {
        return readPackedAsDecimal(Pos.WTDR_TOT_INTR_PREST, Len.Int.WTDR_TOT_INTR_PREST, Len.Fract.WTDR_TOT_INTR_PREST);
    }

    public void setWtdrTotIntrPrestNull(String wtdrTotIntrPrestNull) {
        writeString(Pos.WTDR_TOT_INTR_PREST_NULL, wtdrTotIntrPrestNull, Len.WTDR_TOT_INTR_PREST_NULL);
    }

    /**Original name: WTDR-TOT-INTR-PREST-NULL<br>*/
    public String getWtdrTotIntrPrestNull() {
        return readString(Pos.WTDR_TOT_INTR_PREST_NULL, Len.WTDR_TOT_INTR_PREST_NULL);
    }

    public String getWtdrTotIntrPrestNullFormatted() {
        return Functions.padBlanks(getWtdrTotIntrPrestNull(), Len.WTDR_TOT_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_PREST = 1;
        public static final int WTDR_TOT_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_PREST = 8;
        public static final int WTDR_TOT_INTR_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
