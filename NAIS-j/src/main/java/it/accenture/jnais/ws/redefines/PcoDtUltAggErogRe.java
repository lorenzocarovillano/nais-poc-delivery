package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-AGG-EROG-RE<br>
 * Variable: PCO-DT-ULT-AGG-EROG-RE from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltAggErogRe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltAggErogRe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_AGG_EROG_RE;
    }

    public void setPcoDtUltAggErogRe(int pcoDtUltAggErogRe) {
        writeIntAsPacked(Pos.PCO_DT_ULT_AGG_EROG_RE, pcoDtUltAggErogRe, Len.Int.PCO_DT_ULT_AGG_EROG_RE);
    }

    public void setPcoDtUltAggErogReFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_AGG_EROG_RE, Pos.PCO_DT_ULT_AGG_EROG_RE);
    }

    /**Original name: PCO-DT-ULT-AGG-EROG-RE<br>*/
    public int getPcoDtUltAggErogRe() {
        return readPackedAsInt(Pos.PCO_DT_ULT_AGG_EROG_RE, Len.Int.PCO_DT_ULT_AGG_EROG_RE);
    }

    public byte[] getPcoDtUltAggErogReAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_AGG_EROG_RE, Pos.PCO_DT_ULT_AGG_EROG_RE);
        return buffer;
    }

    public void initPcoDtUltAggErogReHighValues() {
        fill(Pos.PCO_DT_ULT_AGG_EROG_RE, Len.PCO_DT_ULT_AGG_EROG_RE, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltAggErogReNull(String pcoDtUltAggErogReNull) {
        writeString(Pos.PCO_DT_ULT_AGG_EROG_RE_NULL, pcoDtUltAggErogReNull, Len.PCO_DT_ULT_AGG_EROG_RE_NULL);
    }

    /**Original name: PCO-DT-ULT-AGG-EROG-RE-NULL<br>*/
    public String getPcoDtUltAggErogReNull() {
        return readString(Pos.PCO_DT_ULT_AGG_EROG_RE_NULL, Len.PCO_DT_ULT_AGG_EROG_RE_NULL);
    }

    public String getPcoDtUltAggErogReNullFormatted() {
        return Functions.padBlanks(getPcoDtUltAggErogReNull(), Len.PCO_DT_ULT_AGG_EROG_RE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_AGG_EROG_RE = 1;
        public static final int PCO_DT_ULT_AGG_EROG_RE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_AGG_EROG_RE = 5;
        public static final int PCO_DT_ULT_AGG_EROG_RE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_AGG_EROG_RE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
