package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.V1391IdpMatrValVar;
import it.accenture.jnais.ws.redefines.V1391LunghezzaDato;
import it.accenture.jnais.ws.redefines.V1391PrecisioneDato;
import it.accenture.jnais.ws.redefines.V1391TipoMovimento;

/**Original name: V1391-TAB-PARAM<br>
 * Variables: V1391-TAB-PARAM from copybook LDBV1391<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class V1391TabParam {

    //==== PROPERTIES ====
    /**Original name: V1391-TIPO-DATO<br>
	 * <pre>- area ADA</pre>*/
    private String v1391TipoDato = DefaultValues.stringVal(Len.V1391_TIPO_DATO);
    //Original name: V1391-LUNGHEZZA-DATO
    private V1391LunghezzaDato v1391LunghezzaDato = new V1391LunghezzaDato();
    //Original name: V1391-PRECISIONE-DATO
    private V1391PrecisioneDato v1391PrecisioneDato = new V1391PrecisioneDato();
    //Original name: V1391-FORMATTAZIONE-DATO
    private String v1391FormattazioneDato = DefaultValues.stringVal(Len.V1391_FORMATTAZIONE_DATO);
    /**Original name: V1391-ID-MATR-VAL-VAR<br>
	 * <pre>-> area MVV</pre>*/
    private int v1391IdMatrValVar = DefaultValues.BIN_INT_VAL;
    //Original name: V1391-IDP-MATR-VAL-VAR
    private V1391IdpMatrValVar v1391IdpMatrValVar = new V1391IdpMatrValVar();
    //Original name: V1391-TIPO-MOVIMENTO
    private V1391TipoMovimento v1391TipoMovimento = new V1391TipoMovimento();
    //Original name: V1391-COD-DATO-EXT
    private String v1391CodDatoExt = DefaultValues.stringVal(Len.V1391_COD_DATO_EXT);
    //Original name: V1391-OBBLIGATORIETA
    private char v1391Obbligatorieta = DefaultValues.CHAR_VAL;
    //Original name: V1391-VALORE-DEFAULT
    private String v1391ValoreDefault = DefaultValues.stringVal(Len.V1391_VALORE_DEFAULT);
    //Original name: V1391-COD-STR-DATO-PTF
    private String v1391CodStrDatoPtf = DefaultValues.stringVal(Len.V1391_COD_STR_DATO_PTF);
    //Original name: V1391-COD-DATO-PTF
    private String v1391CodDatoPtf = DefaultValues.stringVal(Len.V1391_COD_DATO_PTF);
    //Original name: V1391-COD-PARAMETRO
    private String v1391CodParametro = DefaultValues.stringVal(Len.V1391_COD_PARAMETRO);
    //Original name: V1391-OPERAZIONE
    private String v1391Operazione = DefaultValues.stringVal(Len.V1391_OPERAZIONE);
    //Original name: V1391-LIVELLO-OPERAZIONE
    private String v1391LivelloOperazione = DefaultValues.stringVal(Len.V1391_LIVELLO_OPERAZIONE);
    //Original name: V1391-TIPO-OGGETTO
    private String v1391TipoOggetto = DefaultValues.stringVal(Len.V1391_TIPO_OGGETTO);
    //Original name: V1391-WHERE-CONDITION
    private String v1391WhereCondition = DefaultValues.stringVal(Len.V1391_WHERE_CONDITION);
    //Original name: V1391-SERVIZIO-LETTURA
    private String v1391ServizioLettura = DefaultValues.stringVal(Len.V1391_SERVIZIO_LETTURA);
    //Original name: V1391-MODULO-CALCOLO
    private String v1391ModuloCalcolo = DefaultValues.stringVal(Len.V1391_MODULO_CALCOLO);
    //Original name: V1391-COD-DATO-INTERNO
    private String v1391CodDatoInterno = DefaultValues.stringVal(Len.V1391_COD_DATO_INTERNO);

    //==== METHODS ====
    public void setTabParamBytes(byte[] buffer, int offset) {
        int position = offset;
        v1391TipoDato = MarshalByte.readString(buffer, position, Len.V1391_TIPO_DATO);
        position += Len.V1391_TIPO_DATO;
        v1391LunghezzaDato.setV1391LunghezzaDatoFromBuffer(buffer, position);
        position += V1391LunghezzaDato.Len.V1391_LUNGHEZZA_DATO;
        v1391PrecisioneDato.setV1391PrecisioneDatoFromBuffer(buffer, position);
        position += V1391PrecisioneDato.Len.V1391_PRECISIONE_DATO;
        v1391FormattazioneDato = MarshalByte.readString(buffer, position, Len.V1391_FORMATTAZIONE_DATO);
        position += Len.V1391_FORMATTAZIONE_DATO;
        v1391IdMatrValVar = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        v1391IdpMatrValVar.setV1391IdpMatrValVarFromBuffer(buffer, position);
        position += Types.INT_SIZE;
        v1391TipoMovimento.setV1391TipoMovimentoFromBuffer(buffer, position);
        position += V1391TipoMovimento.Len.V1391_TIPO_MOVIMENTO;
        v1391CodDatoExt = MarshalByte.readString(buffer, position, Len.V1391_COD_DATO_EXT);
        position += Len.V1391_COD_DATO_EXT;
        v1391Obbligatorieta = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        v1391ValoreDefault = MarshalByte.readString(buffer, position, Len.V1391_VALORE_DEFAULT);
        position += Len.V1391_VALORE_DEFAULT;
        v1391CodStrDatoPtf = MarshalByte.readString(buffer, position, Len.V1391_COD_STR_DATO_PTF);
        position += Len.V1391_COD_STR_DATO_PTF;
        v1391CodDatoPtf = MarshalByte.readString(buffer, position, Len.V1391_COD_DATO_PTF);
        position += Len.V1391_COD_DATO_PTF;
        v1391CodParametro = MarshalByte.readString(buffer, position, Len.V1391_COD_PARAMETRO);
        position += Len.V1391_COD_PARAMETRO;
        v1391Operazione = MarshalByte.readString(buffer, position, Len.V1391_OPERAZIONE);
        position += Len.V1391_OPERAZIONE;
        v1391LivelloOperazione = MarshalByte.readString(buffer, position, Len.V1391_LIVELLO_OPERAZIONE);
        position += Len.V1391_LIVELLO_OPERAZIONE;
        v1391TipoOggetto = MarshalByte.readString(buffer, position, Len.V1391_TIPO_OGGETTO);
        position += Len.V1391_TIPO_OGGETTO;
        v1391WhereCondition = MarshalByte.readString(buffer, position, Len.V1391_WHERE_CONDITION);
        position += Len.V1391_WHERE_CONDITION;
        v1391ServizioLettura = MarshalByte.readString(buffer, position, Len.V1391_SERVIZIO_LETTURA);
        position += Len.V1391_SERVIZIO_LETTURA;
        v1391ModuloCalcolo = MarshalByte.readString(buffer, position, Len.V1391_MODULO_CALCOLO);
        position += Len.V1391_MODULO_CALCOLO;
        v1391CodDatoInterno = MarshalByte.readString(buffer, position, Len.V1391_COD_DATO_INTERNO);
    }

    public byte[] getTabParamBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, v1391TipoDato, Len.V1391_TIPO_DATO);
        position += Len.V1391_TIPO_DATO;
        v1391LunghezzaDato.getV1391LunghezzaDatoAsBuffer(buffer, position);
        position += V1391LunghezzaDato.Len.V1391_LUNGHEZZA_DATO;
        v1391PrecisioneDato.getV1391PrecisioneDatoAsBuffer(buffer, position);
        position += V1391PrecisioneDato.Len.V1391_PRECISIONE_DATO;
        MarshalByte.writeString(buffer, position, v1391FormattazioneDato, Len.V1391_FORMATTAZIONE_DATO);
        position += Len.V1391_FORMATTAZIONE_DATO;
        MarshalByte.writeBinaryInt(buffer, position, v1391IdMatrValVar);
        position += Types.INT_SIZE;
        v1391IdpMatrValVar.getV1391IdpMatrValVarAsBuffer(buffer, position);
        position += Types.INT_SIZE;
        v1391TipoMovimento.getV1391TipoMovimentoAsBuffer(buffer, position);
        position += V1391TipoMovimento.Len.V1391_TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, v1391CodDatoExt, Len.V1391_COD_DATO_EXT);
        position += Len.V1391_COD_DATO_EXT;
        MarshalByte.writeChar(buffer, position, v1391Obbligatorieta);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, v1391ValoreDefault, Len.V1391_VALORE_DEFAULT);
        position += Len.V1391_VALORE_DEFAULT;
        MarshalByte.writeString(buffer, position, v1391CodStrDatoPtf, Len.V1391_COD_STR_DATO_PTF);
        position += Len.V1391_COD_STR_DATO_PTF;
        MarshalByte.writeString(buffer, position, v1391CodDatoPtf, Len.V1391_COD_DATO_PTF);
        position += Len.V1391_COD_DATO_PTF;
        MarshalByte.writeString(buffer, position, v1391CodParametro, Len.V1391_COD_PARAMETRO);
        position += Len.V1391_COD_PARAMETRO;
        MarshalByte.writeString(buffer, position, v1391Operazione, Len.V1391_OPERAZIONE);
        position += Len.V1391_OPERAZIONE;
        MarshalByte.writeString(buffer, position, v1391LivelloOperazione, Len.V1391_LIVELLO_OPERAZIONE);
        position += Len.V1391_LIVELLO_OPERAZIONE;
        MarshalByte.writeString(buffer, position, v1391TipoOggetto, Len.V1391_TIPO_OGGETTO);
        position += Len.V1391_TIPO_OGGETTO;
        MarshalByte.writeString(buffer, position, v1391WhereCondition, Len.V1391_WHERE_CONDITION);
        position += Len.V1391_WHERE_CONDITION;
        MarshalByte.writeString(buffer, position, v1391ServizioLettura, Len.V1391_SERVIZIO_LETTURA);
        position += Len.V1391_SERVIZIO_LETTURA;
        MarshalByte.writeString(buffer, position, v1391ModuloCalcolo, Len.V1391_MODULO_CALCOLO);
        position += Len.V1391_MODULO_CALCOLO;
        MarshalByte.writeString(buffer, position, v1391CodDatoInterno, Len.V1391_COD_DATO_INTERNO);
        return buffer;
    }

    public void initTabParamSpaces() {
        v1391TipoDato = "";
        v1391LunghezzaDato.initV1391LunghezzaDatoSpaces();
        v1391PrecisioneDato.initV1391PrecisioneDatoSpaces();
        v1391FormattazioneDato = "";
        v1391IdMatrValVar = Types.INVALID_INT_VAL;
        v1391IdpMatrValVar.initV1391IdpMatrValVarSpaces();
        v1391TipoMovimento.initV1391TipoMovimentoSpaces();
        v1391CodDatoExt = "";
        v1391Obbligatorieta = Types.SPACE_CHAR;
        v1391ValoreDefault = "";
        v1391CodStrDatoPtf = "";
        v1391CodDatoPtf = "";
        v1391CodParametro = "";
        v1391Operazione = "";
        v1391LivelloOperazione = "";
        v1391TipoOggetto = "";
        v1391WhereCondition = "";
        v1391ServizioLettura = "";
        v1391ModuloCalcolo = "";
        v1391CodDatoInterno = "";
    }

    public void setV1391TipoDato(String v1391TipoDato) {
        this.v1391TipoDato = Functions.subString(v1391TipoDato, Len.V1391_TIPO_DATO);
    }

    public String getV1391TipoDato() {
        return this.v1391TipoDato;
    }

    public String getV1391TipoDatoFormatted() {
        return Functions.padBlanks(getV1391TipoDato(), Len.V1391_TIPO_DATO);
    }

    public void setV1391FormattazioneDato(String v1391FormattazioneDato) {
        this.v1391FormattazioneDato = Functions.subString(v1391FormattazioneDato, Len.V1391_FORMATTAZIONE_DATO);
    }

    public String getV1391FormattazioneDato() {
        return this.v1391FormattazioneDato;
    }

    public String getV1391FormattazioneDatoFormatted() {
        return Functions.padBlanks(getV1391FormattazioneDato(), Len.V1391_FORMATTAZIONE_DATO);
    }

    public void setV1391IdMatrValVar(int v1391IdMatrValVar) {
        this.v1391IdMatrValVar = v1391IdMatrValVar;
    }

    public int getV1391IdMatrValVar() {
        return this.v1391IdMatrValVar;
    }

    public void setV1391CodDatoExt(String v1391CodDatoExt) {
        this.v1391CodDatoExt = Functions.subString(v1391CodDatoExt, Len.V1391_COD_DATO_EXT);
    }

    public String getV1391CodDatoExt() {
        return this.v1391CodDatoExt;
    }

    public void setV1391Obbligatorieta(char v1391Obbligatorieta) {
        this.v1391Obbligatorieta = v1391Obbligatorieta;
    }

    public void setV1391ObbligatorietaFormatted(String v1391Obbligatorieta) {
        setV1391Obbligatorieta(Functions.charAt(v1391Obbligatorieta, Types.CHAR_SIZE));
    }

    public char getV1391Obbligatorieta() {
        return this.v1391Obbligatorieta;
    }

    public void setV1391ValoreDefault(String v1391ValoreDefault) {
        this.v1391ValoreDefault = Functions.subString(v1391ValoreDefault, Len.V1391_VALORE_DEFAULT);
    }

    public String getV1391ValoreDefault() {
        return this.v1391ValoreDefault;
    }

    public String getV1391ValoreDefaultFormatted() {
        return Functions.padBlanks(getV1391ValoreDefault(), Len.V1391_VALORE_DEFAULT);
    }

    public void setV1391CodStrDatoPtf(String v1391CodStrDatoPtf) {
        this.v1391CodStrDatoPtf = Functions.subString(v1391CodStrDatoPtf, Len.V1391_COD_STR_DATO_PTF);
    }

    public String getV1391CodStrDatoPtf() {
        return this.v1391CodStrDatoPtf;
    }

    public void setV1391CodDatoPtf(String v1391CodDatoPtf) {
        this.v1391CodDatoPtf = Functions.subString(v1391CodDatoPtf, Len.V1391_COD_DATO_PTF);
    }

    public String getV1391CodDatoPtf() {
        return this.v1391CodDatoPtf;
    }

    public void setV1391CodParametro(String v1391CodParametro) {
        this.v1391CodParametro = Functions.subString(v1391CodParametro, Len.V1391_COD_PARAMETRO);
    }

    public String getV1391CodParametro() {
        return this.v1391CodParametro;
    }

    public void setV1391Operazione(String v1391Operazione) {
        this.v1391Operazione = Functions.subString(v1391Operazione, Len.V1391_OPERAZIONE);
    }

    public String getV1391Operazione() {
        return this.v1391Operazione;
    }

    public void setV1391LivelloOperazione(String v1391LivelloOperazione) {
        this.v1391LivelloOperazione = Functions.subString(v1391LivelloOperazione, Len.V1391_LIVELLO_OPERAZIONE);
    }

    public String getV1391LivelloOperazione() {
        return this.v1391LivelloOperazione;
    }

    public String getV1391LivelloOperazioneFormatted() {
        return Functions.padBlanks(getV1391LivelloOperazione(), Len.V1391_LIVELLO_OPERAZIONE);
    }

    public void setV1391TipoOggetto(String v1391TipoOggetto) {
        this.v1391TipoOggetto = Functions.subString(v1391TipoOggetto, Len.V1391_TIPO_OGGETTO);
    }

    public String getV1391TipoOggetto() {
        return this.v1391TipoOggetto;
    }

    public String getV1391TipoOggettoFormatted() {
        return Functions.padBlanks(getV1391TipoOggetto(), Len.V1391_TIPO_OGGETTO);
    }

    public void setV1391WhereCondition(String v1391WhereCondition) {
        this.v1391WhereCondition = Functions.subString(v1391WhereCondition, Len.V1391_WHERE_CONDITION);
    }

    public String getV1391WhereCondition() {
        return this.v1391WhereCondition;
    }

    public String getV1391WhereConditionFormatted() {
        return Functions.padBlanks(getV1391WhereCondition(), Len.V1391_WHERE_CONDITION);
    }

    public void setV1391ServizioLettura(String v1391ServizioLettura) {
        this.v1391ServizioLettura = Functions.subString(v1391ServizioLettura, Len.V1391_SERVIZIO_LETTURA);
    }

    public String getV1391ServizioLettura() {
        return this.v1391ServizioLettura;
    }

    public String getV1391ServizioLetturaFormatted() {
        return Functions.padBlanks(getV1391ServizioLettura(), Len.V1391_SERVIZIO_LETTURA);
    }

    public void setV1391ModuloCalcolo(String v1391ModuloCalcolo) {
        this.v1391ModuloCalcolo = Functions.subString(v1391ModuloCalcolo, Len.V1391_MODULO_CALCOLO);
    }

    public String getV1391ModuloCalcolo() {
        return this.v1391ModuloCalcolo;
    }

    public String getV1391ModuloCalcoloFormatted() {
        return Functions.padBlanks(getV1391ModuloCalcolo(), Len.V1391_MODULO_CALCOLO);
    }

    public void setV1391CodDatoInterno(String v1391CodDatoInterno) {
        this.v1391CodDatoInterno = Functions.subString(v1391CodDatoInterno, Len.V1391_COD_DATO_INTERNO);
    }

    public String getV1391CodDatoInterno() {
        return this.v1391CodDatoInterno;
    }

    public V1391IdpMatrValVar getV1391IdpMatrValVar() {
        return v1391IdpMatrValVar;
    }

    public V1391LunghezzaDato getV1391LunghezzaDato() {
        return v1391LunghezzaDato;
    }

    public V1391PrecisioneDato getV1391PrecisioneDato() {
        return v1391PrecisioneDato;
    }

    public V1391TipoMovimento getV1391TipoMovimento() {
        return v1391TipoMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int V1391_TIPO_DATO = 2;
        public static final int V1391_FORMATTAZIONE_DATO = 20;
        public static final int V1391_ID_MATR_VAL_VAR = 4;
        public static final int V1391_COD_DATO_EXT = 30;
        public static final int V1391_OBBLIGATORIETA = 1;
        public static final int V1391_VALORE_DEFAULT = 50;
        public static final int V1391_COD_STR_DATO_PTF = 30;
        public static final int V1391_COD_DATO_PTF = 30;
        public static final int V1391_COD_PARAMETRO = 20;
        public static final int V1391_OPERAZIONE = 15;
        public static final int V1391_LIVELLO_OPERAZIONE = 3;
        public static final int V1391_TIPO_OGGETTO = 2;
        public static final int V1391_WHERE_CONDITION = 300;
        public static final int V1391_SERVIZIO_LETTURA = 8;
        public static final int V1391_MODULO_CALCOLO = 8;
        public static final int V1391_COD_DATO_INTERNO = 30;
        public static final int TAB_PARAM = V1391_TIPO_DATO + V1391LunghezzaDato.Len.V1391_LUNGHEZZA_DATO + V1391PrecisioneDato.Len.V1391_PRECISIONE_DATO + V1391_FORMATTAZIONE_DATO + V1391_ID_MATR_VAL_VAR + V1391IdpMatrValVar.Len.V1391_IDP_MATR_VAL_VAR + V1391TipoMovimento.Len.V1391_TIPO_MOVIMENTO + V1391_COD_DATO_EXT + V1391_OBBLIGATORIETA + V1391_VALORE_DEFAULT + V1391_COD_STR_DATO_PTF + V1391_COD_DATO_PTF + V1391_COD_PARAMETRO + V1391_OPERAZIONE + V1391_LIVELLO_OPERAZIONE + V1391_TIPO_OGGETTO + V1391_WHERE_CONDITION + V1391_SERVIZIO_LETTURA + V1391_MODULO_CALCOLO + V1391_COD_DATO_INTERNO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
