package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-TRATTATO-VALIDO<br>
 * Variable: FLAG-TRATTATO-VALIDO from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTrattatoValido {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagTrattatoValido(char flagTrattatoValido) {
        this.value = flagTrattatoValido;
    }

    public char getFlagTrattatoValido() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
