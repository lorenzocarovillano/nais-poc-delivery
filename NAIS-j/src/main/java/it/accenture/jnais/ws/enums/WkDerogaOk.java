package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-DEROGA-OK<br>
 * Variable: WK-DEROGA-OK from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkDerogaOk {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkDerogaOk(char wkDerogaOk) {
        this.value = wkDerogaOk;
    }

    public char getWkDerogaOk() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
