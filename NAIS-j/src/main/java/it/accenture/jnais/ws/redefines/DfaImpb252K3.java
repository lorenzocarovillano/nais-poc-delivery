package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-252-K3<br>
 * Variable: DFA-IMPB-252-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpb252K3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpb252K3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB252_K3;
    }

    public void setDfaImpb252K3(AfDecimal dfaImpb252K3) {
        writeDecimalAsPacked(Pos.DFA_IMPB252_K3, dfaImpb252K3.copy());
    }

    public void setDfaImpb252K3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB252_K3, Pos.DFA_IMPB252_K3);
    }

    /**Original name: DFA-IMPB-252-K3<br>*/
    public AfDecimal getDfaImpb252K3() {
        return readPackedAsDecimal(Pos.DFA_IMPB252_K3, Len.Int.DFA_IMPB252_K3, Len.Fract.DFA_IMPB252_K3);
    }

    public byte[] getDfaImpb252K3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB252_K3, Pos.DFA_IMPB252_K3);
        return buffer;
    }

    public void setDfaImpb252K3Null(String dfaImpb252K3Null) {
        writeString(Pos.DFA_IMPB252_K3_NULL, dfaImpb252K3Null, Len.DFA_IMPB252_K3_NULL);
    }

    /**Original name: DFA-IMPB-252-K3-NULL<br>*/
    public String getDfaImpb252K3Null() {
        return readString(Pos.DFA_IMPB252_K3_NULL, Len.DFA_IMPB252_K3_NULL);
    }

    public String getDfaImpb252K3NullFormatted() {
        return Functions.padBlanks(getDfaImpb252K3Null(), Len.DFA_IMPB252_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB252_K3 = 1;
        public static final int DFA_IMPB252_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB252_K3 = 8;
        public static final int DFA_IMPB252_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB252_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB252_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
