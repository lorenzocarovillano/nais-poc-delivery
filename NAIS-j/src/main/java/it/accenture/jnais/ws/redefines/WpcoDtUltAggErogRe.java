package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-AGG-EROG-RE<br>
 * Variable: WPCO-DT-ULT-AGG-EROG-RE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltAggErogRe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltAggErogRe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_AGG_EROG_RE;
    }

    public void setWpcoDtUltAggErogRe(int wpcoDtUltAggErogRe) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_AGG_EROG_RE, wpcoDtUltAggErogRe, Len.Int.WPCO_DT_ULT_AGG_EROG_RE);
    }

    public void setDpcoDtUltAggErogReFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_AGG_EROG_RE, Pos.WPCO_DT_ULT_AGG_EROG_RE);
    }

    /**Original name: WPCO-DT-ULT-AGG-EROG-RE<br>*/
    public int getWpcoDtUltAggErogRe() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_AGG_EROG_RE, Len.Int.WPCO_DT_ULT_AGG_EROG_RE);
    }

    public byte[] getWpcoDtUltAggErogReAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_AGG_EROG_RE, Pos.WPCO_DT_ULT_AGG_EROG_RE);
        return buffer;
    }

    public void setWpcoDtUltAggErogReNull(String wpcoDtUltAggErogReNull) {
        writeString(Pos.WPCO_DT_ULT_AGG_EROG_RE_NULL, wpcoDtUltAggErogReNull, Len.WPCO_DT_ULT_AGG_EROG_RE_NULL);
    }

    /**Original name: WPCO-DT-ULT-AGG-EROG-RE-NULL<br>*/
    public String getWpcoDtUltAggErogReNull() {
        return readString(Pos.WPCO_DT_ULT_AGG_EROG_RE_NULL, Len.WPCO_DT_ULT_AGG_EROG_RE_NULL);
    }

    public String getWpcoDtUltAggErogReNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltAggErogReNull(), Len.WPCO_DT_ULT_AGG_EROG_RE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_AGG_EROG_RE = 1;
        public static final int WPCO_DT_ULT_AGG_EROG_RE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_AGG_EROG_RE = 5;
        public static final int WPCO_DT_ULT_AGG_EROG_RE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_AGG_EROG_RE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
