package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ABB-TOT-INI<br>
 * Variable: WTGA-ABB-TOT-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAbbTotIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAbbTotIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ABB_TOT_INI;
    }

    public void setWtgaAbbTotIni(AfDecimal wtgaAbbTotIni) {
        writeDecimalAsPacked(Pos.WTGA_ABB_TOT_INI, wtgaAbbTotIni.copy());
    }

    public void setWtgaAbbTotIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ABB_TOT_INI, Pos.WTGA_ABB_TOT_INI);
    }

    /**Original name: WTGA-ABB-TOT-INI<br>*/
    public AfDecimal getWtgaAbbTotIni() {
        return readPackedAsDecimal(Pos.WTGA_ABB_TOT_INI, Len.Int.WTGA_ABB_TOT_INI, Len.Fract.WTGA_ABB_TOT_INI);
    }

    public byte[] getWtgaAbbTotIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ABB_TOT_INI, Pos.WTGA_ABB_TOT_INI);
        return buffer;
    }

    public void initWtgaAbbTotIniSpaces() {
        fill(Pos.WTGA_ABB_TOT_INI, Len.WTGA_ABB_TOT_INI, Types.SPACE_CHAR);
    }

    public void setWtgaAbbTotIniNull(String wtgaAbbTotIniNull) {
        writeString(Pos.WTGA_ABB_TOT_INI_NULL, wtgaAbbTotIniNull, Len.WTGA_ABB_TOT_INI_NULL);
    }

    /**Original name: WTGA-ABB-TOT-INI-NULL<br>*/
    public String getWtgaAbbTotIniNull() {
        return readString(Pos.WTGA_ABB_TOT_INI_NULL, Len.WTGA_ABB_TOT_INI_NULL);
    }

    public String getWtgaAbbTotIniNullFormatted() {
        return Functions.padBlanks(getWtgaAbbTotIniNull(), Len.WTGA_ABB_TOT_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ABB_TOT_INI = 1;
        public static final int WTGA_ABB_TOT_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ABB_TOT_INI = 8;
        public static final int WTGA_ABB_TOT_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ABB_TOT_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ABB_TOT_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
