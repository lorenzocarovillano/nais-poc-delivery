package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-RIT-TFR-IPT<br>
 * Variable: WBEL-RIT-TFR-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelRitTfrIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelRitTfrIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_RIT_TFR_IPT;
    }

    public void setWbelRitTfrIpt(AfDecimal wbelRitTfrIpt) {
        writeDecimalAsPacked(Pos.WBEL_RIT_TFR_IPT, wbelRitTfrIpt.copy());
    }

    public void setWbelRitTfrIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_RIT_TFR_IPT, Pos.WBEL_RIT_TFR_IPT);
    }

    /**Original name: WBEL-RIT-TFR-IPT<br>*/
    public AfDecimal getWbelRitTfrIpt() {
        return readPackedAsDecimal(Pos.WBEL_RIT_TFR_IPT, Len.Int.WBEL_RIT_TFR_IPT, Len.Fract.WBEL_RIT_TFR_IPT);
    }

    public byte[] getWbelRitTfrIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_RIT_TFR_IPT, Pos.WBEL_RIT_TFR_IPT);
        return buffer;
    }

    public void initWbelRitTfrIptSpaces() {
        fill(Pos.WBEL_RIT_TFR_IPT, Len.WBEL_RIT_TFR_IPT, Types.SPACE_CHAR);
    }

    public void setWbelRitTfrIptNull(String wbelRitTfrIptNull) {
        writeString(Pos.WBEL_RIT_TFR_IPT_NULL, wbelRitTfrIptNull, Len.WBEL_RIT_TFR_IPT_NULL);
    }

    /**Original name: WBEL-RIT-TFR-IPT-NULL<br>*/
    public String getWbelRitTfrIptNull() {
        return readString(Pos.WBEL_RIT_TFR_IPT_NULL, Len.WBEL_RIT_TFR_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_RIT_TFR_IPT = 1;
        public static final int WBEL_RIT_TFR_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_RIT_TFR_IPT = 8;
        public static final int WBEL_RIT_TFR_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_RIT_TFR_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_RIT_TFR_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
