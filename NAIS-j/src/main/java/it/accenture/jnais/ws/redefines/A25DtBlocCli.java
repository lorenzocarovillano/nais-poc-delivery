package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-BLOC-CLI<br>
 * Variable: A25-DT-BLOC-CLI from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25DtBlocCli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25DtBlocCli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT_BLOC_CLI;
    }

    public void setA25DtBlocCli(int a25DtBlocCli) {
        writeIntAsPacked(Pos.A25_DT_BLOC_CLI, a25DtBlocCli, Len.Int.A25_DT_BLOC_CLI);
    }

    public void setA25DtBlocCliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT_BLOC_CLI, Pos.A25_DT_BLOC_CLI);
    }

    /**Original name: A25-DT-BLOC-CLI<br>*/
    public int getA25DtBlocCli() {
        return readPackedAsInt(Pos.A25_DT_BLOC_CLI, Len.Int.A25_DT_BLOC_CLI);
    }

    public byte[] getA25DtBlocCliAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT_BLOC_CLI, Pos.A25_DT_BLOC_CLI);
        return buffer;
    }

    public void setA25DtBlocCliNull(String a25DtBlocCliNull) {
        writeString(Pos.A25_DT_BLOC_CLI_NULL, a25DtBlocCliNull, Len.A25_DT_BLOC_CLI_NULL);
    }

    /**Original name: A25-DT-BLOC-CLI-NULL<br>*/
    public String getA25DtBlocCliNull() {
        return readString(Pos.A25_DT_BLOC_CLI_NULL, Len.A25_DT_BLOC_CLI_NULL);
    }

    public String getA25DtBlocCliNullFormatted() {
        return Functions.padBlanks(getA25DtBlocCliNull(), Len.A25_DT_BLOC_CLI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT_BLOC_CLI = 1;
        public static final int A25_DT_BLOC_CLI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT_BLOC_CLI = 5;
        public static final int A25_DT_BLOC_CLI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT_BLOC_CLI = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
