package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-MANFEE-ANTIC<br>
 * Variable: L3421-MANFEE-ANTIC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_MANFEE_ANTIC;
    }

    public void setL3421ManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_MANFEE_ANTIC, Pos.L3421_MANFEE_ANTIC);
    }

    /**Original name: L3421-MANFEE-ANTIC<br>*/
    public AfDecimal getL3421ManfeeAntic() {
        return readPackedAsDecimal(Pos.L3421_MANFEE_ANTIC, Len.Int.L3421_MANFEE_ANTIC, Len.Fract.L3421_MANFEE_ANTIC);
    }

    public byte[] getL3421ManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_MANFEE_ANTIC, Pos.L3421_MANFEE_ANTIC);
        return buffer;
    }

    /**Original name: L3421-MANFEE-ANTIC-NULL<br>*/
    public String getL3421ManfeeAnticNull() {
        return readString(Pos.L3421_MANFEE_ANTIC_NULL, Len.L3421_MANFEE_ANTIC_NULL);
    }

    public String getL3421ManfeeAnticNullFormatted() {
        return Functions.padBlanks(getL3421ManfeeAnticNull(), Len.L3421_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_MANFEE_ANTIC = 1;
        public static final int L3421_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_MANFEE_ANTIC = 8;
        public static final int L3421_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
