package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CAR-ACQ-NON-SCON<br>
 * Variable: WB03-CAR-ACQ-NON-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CarAcqNonScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CarAcqNonScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CAR_ACQ_NON_SCON;
    }

    public void setWb03CarAcqNonSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CAR_ACQ_NON_SCON, Pos.WB03_CAR_ACQ_NON_SCON);
    }

    /**Original name: WB03-CAR-ACQ-NON-SCON<br>*/
    public AfDecimal getWb03CarAcqNonScon() {
        return readPackedAsDecimal(Pos.WB03_CAR_ACQ_NON_SCON, Len.Int.WB03_CAR_ACQ_NON_SCON, Len.Fract.WB03_CAR_ACQ_NON_SCON);
    }

    public byte[] getWb03CarAcqNonSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CAR_ACQ_NON_SCON, Pos.WB03_CAR_ACQ_NON_SCON);
        return buffer;
    }

    public void setWb03CarAcqNonSconNull(String wb03CarAcqNonSconNull) {
        writeString(Pos.WB03_CAR_ACQ_NON_SCON_NULL, wb03CarAcqNonSconNull, Len.WB03_CAR_ACQ_NON_SCON_NULL);
    }

    /**Original name: WB03-CAR-ACQ-NON-SCON-NULL<br>*/
    public String getWb03CarAcqNonSconNull() {
        return readString(Pos.WB03_CAR_ACQ_NON_SCON_NULL, Len.WB03_CAR_ACQ_NON_SCON_NULL);
    }

    public String getWb03CarAcqNonSconNullFormatted() {
        return Functions.padBlanks(getWb03CarAcqNonSconNull(), Len.WB03_CAR_ACQ_NON_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CAR_ACQ_NON_SCON = 1;
        public static final int WB03_CAR_ACQ_NON_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CAR_ACQ_NON_SCON = 8;
        public static final int WB03_CAR_ACQ_NON_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CAR_ACQ_NON_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CAR_ACQ_NON_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
