package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-RIS-MAT<br>
 * Variable: OCO-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_RIS_MAT;
    }

    public void setOcoRisMat(AfDecimal ocoRisMat) {
        writeDecimalAsPacked(Pos.OCO_RIS_MAT, ocoRisMat.copy());
    }

    public void setOcoRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_RIS_MAT, Pos.OCO_RIS_MAT);
    }

    /**Original name: OCO-RIS-MAT<br>*/
    public AfDecimal getOcoRisMat() {
        return readPackedAsDecimal(Pos.OCO_RIS_MAT, Len.Int.OCO_RIS_MAT, Len.Fract.OCO_RIS_MAT);
    }

    public byte[] getOcoRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_RIS_MAT, Pos.OCO_RIS_MAT);
        return buffer;
    }

    public void setOcoRisMatNull(String ocoRisMatNull) {
        writeString(Pos.OCO_RIS_MAT_NULL, ocoRisMatNull, Len.OCO_RIS_MAT_NULL);
    }

    /**Original name: OCO-RIS-MAT-NULL<br>*/
    public String getOcoRisMatNull() {
        return readString(Pos.OCO_RIS_MAT_NULL, Len.OCO_RIS_MAT_NULL);
    }

    public String getOcoRisMatNullFormatted() {
        return Functions.padBlanks(getOcoRisMatNull(), Len.OCO_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_RIS_MAT = 1;
        public static final int OCO_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_RIS_MAT = 8;
        public static final int OCO_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int OCO_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
