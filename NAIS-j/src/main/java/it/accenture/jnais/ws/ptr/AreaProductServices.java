package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.notifier.ChangeNotifier;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;

/**Original name: AREA-PRODUCT-SERVICES<br>
 * Variable: AREA-PRODUCT-SERVICES from copybook IJCCMQ03<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class AreaProductServices extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int IJCCMQ03_CAR_MAXOCCURS = 5000000;
    private int ijccmq03CarListenerSize = IJCCMQ03_CAR_MAXOCCURS;
    private IValueChangeListener ijccmq03CarListener = new Ijccmq03CarListener();
    public static final int IJCCMQ03_ELE_ERRORI_MAXOCCURS = 10;
    public static final int IJCCMQ03_ADDRESSES_MAXOCCURS = 5;
    private Pos pos = new Pos(this);
    public static final char IJCCMQ03_ON_LINE = 'O';
    public static final char IJCCMQ03_BATCH = 'B';
    public static final char IJCCMQ03_BATCH_INFR = 'I';
    public static final char IJCCMQ03_SERVICE_INVOCATION = 'S';
    public static final char IJCCMQ03_DATA_REQUEST = 'D';
    public static final String IJCCMQ03_ESITO_OK = "OK";
    public static final String IJCCMQ03_ESITO_KO = "KO";
    private ChangeNotifier ijccmq03LengthDatiServizioNotifier = new ChangeNotifier();
    public static final String IJCCMQ00_NO_DEBUG = "0";
    public static final String IJCCMQ00_DEBUG_BASSO = "1";
    public static final String IJCCMQ00_DEBUG_MEDIO = "2";
    public static final String IJCCMQ00_DEBUG_ELEVATO = "3";
    public static final String IJCCMQ00_DEBUG_ESASPERATO = "4";
    private static final String[] IJCCMQ00_ANY_APPL_DBG = new String[] {"1", "2", "3", "4"};
    public static final String IJCCMQ00_ARCH_BATCH_DBG = "5";
    public static final String IJCCMQ00_COM_COB_JAV_DBG = "6";
    public static final String IJCCMQ00_STRESS_TEST_DBG = "7";
    public static final String IJCCMQ00_BUSINESS_DBG = "8";
    public static final String IJCCMQ00_TOT_TUNING_DBG = "9";
    private static final String[] IJCCMQ00_ANY_TUNING_DBG = new String[] {"5", "6", "7", "8", "9"};

    //==== CONSTRUCTORS ====
    public AreaProductServices() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return getAreaProductServicesSize();
    }

    public int getAreaProductServicesSize() {
        return Len.IJCCMQ03_HEADER + getIjccmq03AreaDatiServizioSize();
    }

    public void initAreaProductServicesLowValues() {
        getIjccmq03LengthDatiServizioNotifier().notifyMaxOccursListeners();
        fill(Pos.AREA_PRODUCT_SERVICES, getAreaProductServicesSize(), Types.LOW_CHAR_VAL);
    }

    public void setIjccmq03ModalitaEsecutiva(char ijccmq03ModalitaEsecutiva) {
        writeChar(Pos.IJCCMQ03_MODALITA_ESECUTIVA, ijccmq03ModalitaEsecutiva);
    }

    /**Original name: IJCCMQ03-MODALITA-ESECUTIVA<br>*/
    public char getIjccmq03ModalitaEsecutiva() {
        return readChar(Pos.IJCCMQ03_MODALITA_ESECUTIVA);
    }

    public void setIjccmq03JavaServiceName(String ijccmq03JavaServiceName) {
        writeString(Pos.IJCCMQ03_JAVA_SERVICE_NAME, ijccmq03JavaServiceName, Len.IJCCMQ03_JAVA_SERVICE_NAME);
    }

    /**Original name: IJCCMQ03-JAVA-SERVICE-NAME<br>*/
    public String getIjccmq03JavaServiceName() {
        return readString(Pos.IJCCMQ03_JAVA_SERVICE_NAME, Len.IJCCMQ03_JAVA_SERVICE_NAME);
    }

    public void setIjccmq03Esito(String ijccmq03Esito) {
        writeString(Pos.IJCCMQ03_ESITO, ijccmq03Esito, Len.IJCCMQ03_ESITO);
    }

    /**Original name: IJCCMQ03-ESITO<br>*/
    public String getIjccmq03Esito() {
        return readString(Pos.IJCCMQ03_ESITO, Len.IJCCMQ03_ESITO);
    }

    public boolean isIjccmq03EsitoOk() {
        return getIjccmq03Esito().equals(IJCCMQ03_ESITO_OK);
    }

    public void setIjccmq03EsitoOk() {
        setIjccmq03Esito(IJCCMQ03_ESITO_OK);
    }

    public void setIjccmq03LengthDatiServizio(int ijccmq03LengthDatiServizio) {
        writeBinaryInt(Pos.IJCCMQ03_LENGTH_DATI_SERVIZIO, ijccmq03LengthDatiServizio);
        this.ijccmq03LengthDatiServizioNotifier.setValue(ijccmq03LengthDatiServizio);
    }

    public int getIjccmq03LengthDatiServizio() {
        return readBinaryInt(Pos.IJCCMQ03_LENGTH_DATI_SERVIZIO);
    }

    /**Original name: IJCCMQ03-MAX-ELE-ERRORI<br>*/
    public short getIjccmq03MaxEleErrori() {
        return readBinaryShort(Pos.IJCCMQ03_MAX_ELE_ERRORI);
    }

    /**Original name: IJCCMQ03-DESC-ERRORE<br>*/
    public String getIjccmq03DescErrore(int ijccmq03DescErroreIdx) {
        int position = Pos.ijccmq03DescErrore(ijccmq03DescErroreIdx - 1);
        return readString(position, Len.IJCCMQ03_DESC_ERRORE);
    }

    public String getIjccmq03CodErroreFormatted(int ijccmq03CodErroreIdx) {
        int position = Pos.ijccmq03CodErrore(ijccmq03CodErroreIdx - 1);
        return readFixedString(position, Len.IJCCMQ03_COD_ERRORE);
    }

    public String getIjccmq03LivGravitaBeFormatted(int ijccmq03LivGravitaBeIdx) {
        int position = Pos.ijccmq03LivGravitaBe(ijccmq03LivGravitaBeIdx - 1);
        return readFixedString(position, Len.IJCCMQ03_LIV_GRAVITA_BE);
    }

    public void setIjccmq03AreaAddressesBytes(byte[] buffer) {
        setIjccmq03AreaAddressesBytes(buffer, 1);
    }

    public void setIjccmq03AreaAddressesBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IJCCMQ03_AREA_ADDRESSES, Pos.IJCCMQ03_AREA_ADDRESSES);
    }

    public void setIjccmq03UserName(String ijccmq03UserName) {
        writeString(Pos.IJCCMQ03_USER_NAME, ijccmq03UserName, Len.IJCCMQ03_USER_NAME);
    }

    /**Original name: IJCCMQ03-USER-NAME<br>*/
    public String getIjccmq03UserName() {
        return readString(Pos.IJCCMQ03_USER_NAME, Len.IJCCMQ03_USER_NAME);
    }

    public void setIjccmq00LivelloDebug(short ijccmq00LivelloDebug) {
        writeShort(Pos.IJCCMQ00_LIVELLO_DEBUG, ijccmq00LivelloDebug, Len.Int.IJCCMQ00_LIVELLO_DEBUG, SignType.NO_SIGN);
    }

    public void setIjccmq00LivelloDebugFormatted(String ijccmq00LivelloDebug) {
        writeString(Pos.IJCCMQ00_LIVELLO_DEBUG, Trunc.toUnsignedNumeric(ijccmq00LivelloDebug, Len.IJCCMQ00_LIVELLO_DEBUG), Len.IJCCMQ00_LIVELLO_DEBUG);
    }

    /**Original name: IJCCMQ00-LIVELLO-DEBUG<br>*/
    public short getIjccmq00LivelloDebug() {
        return readNumDispUnsignedShort(Pos.IJCCMQ00_LIVELLO_DEBUG, Len.IJCCMQ00_LIVELLO_DEBUG);
    }

    public int getIjccmq03AreaDatiServizioSize() {
        return Types.CHAR_SIZE * ijccmq03CarListenerSize;
    }

    public void setIjccmq03AreaDatiServizioBytes(byte[] buffer) {
        setIjccmq03AreaDatiServizioBytes(buffer, 1);
    }

    /**Original name: IJCCMQ03-AREA-DATI-SERVIZIO<br>*/
    public byte[] getIjccmq03AreaDatiServizioBytes() {
        byte[] buffer = new byte[getIjccmq03AreaDatiServizioSize()];
        return getIjccmq03AreaDatiServizioBytes(buffer, 1);
    }

    public void setIjccmq03AreaDatiServizioBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, getIjccmq03AreaDatiServizioSize(), Pos.IJCCMQ03_AREA_DATI_SERVIZIO);
    }

    public byte[] getIjccmq03AreaDatiServizioBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, getIjccmq03AreaDatiServizioSize(), Pos.IJCCMQ03_AREA_DATI_SERVIZIO);
        return buffer;
    }

    public IValueChangeListener getIjccmq03CarListener() {
        return ijccmq03CarListener;
    }

    public ChangeNotifier getIjccmq03LengthDatiServizioNotifier() {
        return ijccmq03LengthDatiServizioNotifier;
    }

    public void notifyListeners() {
        getIjccmq03LengthDatiServizioNotifier().setValue(getIjccmq03LengthDatiServizio());
    }

    //==== INNER CLASSES ====
    /**Original name: IJCCMQ03-CAR<br>*/
    public class Ijccmq03CarListener implements IValueChangeListener {

        //==== METHODS ====
        public void change() {
            ijccmq03CarListenerSize = IJCCMQ03_CAR_MAXOCCURS;
        }

        public void change(int value) {
            ijccmq03CarListenerSize = value < 1 ? 0 : (value > IJCCMQ03_CAR_MAXOCCURS ? IJCCMQ03_CAR_MAXOCCURS : value);
        }
    }

    public static class Pos {

        //==== PROPERTIES ====
        public static final int AREA_PRODUCT_SERVICES = 1;
        public static final int IJCCMQ03_HEADER = AREA_PRODUCT_SERVICES;
        public static final int IJCCMQ03_MODALITA_ESECUTIVA = IJCCMQ03_HEADER;
        public static final int IJCCMQ03_JAVA_SERVICE_NAME = IJCCMQ03_MODALITA_ESECUTIVA + Len.IJCCMQ03_MODALITA_ESECUTIVA;
        public static final int IJCCMQ03_INFO_FRAMES = IJCCMQ03_JAVA_SERVICE_NAME + Len.IJCCMQ03_JAVA_SERVICE_NAME;
        public static final int IJCCMQ03_ID_APPLICATION = IJCCMQ03_INFO_FRAMES;
        public static final int IJCCMQ03_TOT_NUM_FRAMES = IJCCMQ03_ID_APPLICATION + Len.IJCCMQ03_ID_APPLICATION;
        public static final int IJCCMQ03_NUM_FRAME = IJCCMQ03_TOT_NUM_FRAMES + Len.IJCCMQ03_TOT_NUM_FRAMES;
        public static final int IJCCMQ03_LENGTH_DATA_FRAME = IJCCMQ03_NUM_FRAME + Len.IJCCMQ03_NUM_FRAME;
        public static final int IJCCMQ03_CALL_METHOD = IJCCMQ03_LENGTH_DATA_FRAME + Len.IJCCMQ03_LENGTH_DATA_FRAME;
        public static final int IJCCMQ03_ESITO = IJCCMQ03_CALL_METHOD + Len.IJCCMQ03_CALL_METHOD;
        public static final int IJCCMQ03_LENGTH_DATI_SERVIZIO = IJCCMQ03_ESITO + Len.IJCCMQ03_ESITO;
        public static final int IJCCMQ03_MAX_ELE_ERRORI = IJCCMQ03_LENGTH_DATI_SERVIZIO + Len.IJCCMQ03_LENGTH_DATI_SERVIZIO;
        public static final int IJCCMQ03_TAB_ERRORI_FRONT_END = IJCCMQ03_MAX_ELE_ERRORI + Len.IJCCMQ03_MAX_ELE_ERRORI;
        public static final int IJCCMQ03_LENGTH_DATI_OUTPUT = ijccmq03TipoTrattFe(IJCCMQ03_ELE_ERRORI_MAXOCCURS - 1) + Len.IJCCMQ03_TIPO_TRATT_FE;
        public static final int IJCCMQ03_VAR_AMBIENTE = IJCCMQ03_LENGTH_DATI_OUTPUT + Len.IJCCMQ03_LENGTH_DATI_OUTPUT;
        public static final int IJCCMQ03_AREA_ADDRESSES = IJCCMQ03_VAR_AMBIENTE + Len.IJCCMQ03_VAR_AMBIENTE;
        public static final int IJCCMQ03_USER_NAME = ijccmq03Address(IJCCMQ03_ADDRESSES_MAXOCCURS - 1) + Len.IJCCMQ03_ADDRESS;
        public static final int FLR1 = IJCCMQ03_USER_NAME + Len.IJCCMQ03_USER_NAME;
        public static final int IJCCMQ00_LIVELLO_DEBUG = FLR1 + Len.FLR1;
        public static final int IJCCMQ03_AREA_DATI_SERVIZIO = IJCCMQ00_LIVELLO_DEBUG + Len.IJCCMQ00_LIVELLO_DEBUG;
        private AreaProductServices outer;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        public Pos(AreaProductServices outer) {
            this.outer = outer;
        }

        //==== METHODS ====
        public static int ijccmq03EleErrori(int idx) {
            return IJCCMQ03_TAB_ERRORI_FRONT_END + idx * Len.IJCCMQ03_ELE_ERRORI;
        }

        public static int ijccmq03DescErrore(int idx) {
            return ijccmq03EleErrori(idx);
        }

        public static int ijccmq03CodErrore(int idx) {
            return ijccmq03DescErrore(idx) + Len.IJCCMQ03_DESC_ERRORE;
        }

        public static int ijccmq03LivGravitaBe(int idx) {
            return ijccmq03CodErrore(idx) + Len.IJCCMQ03_COD_ERRORE;
        }

        public static int ijccmq03TipoTrattFe(int idx) {
            return ijccmq03LivGravitaBe(idx) + Len.IJCCMQ03_LIV_GRAVITA_BE;
        }

        public static int ijccmq03Addresses(int idx) {
            return IJCCMQ03_AREA_ADDRESSES + idx * Len.IJCCMQ03_ADDRESSES;
        }

        public static int ijccmq03AddressType(int idx) {
            return ijccmq03Addresses(idx);
        }

        public static int ijccmq03Address(int idx) {
            return ijccmq03AddressType(idx) + Len.IJCCMQ03_ADDRESS_TYPE;
        }

        public static int ijccmq03Car(int idx) {
            return IJCCMQ03_AREA_DATI_SERVIZIO + idx * Len.IJCCMQ03_CAR;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int IJCCMQ03_MODALITA_ESECUTIVA = 1;
        public static final int IJCCMQ03_JAVA_SERVICE_NAME = 100;
        public static final int IJCCMQ03_ID_APPLICATION = 26;
        public static final int IJCCMQ03_TOT_NUM_FRAMES = 2;
        public static final int IJCCMQ03_NUM_FRAME = 2;
        public static final int IJCCMQ03_LENGTH_DATA_FRAME = 4;
        public static final int IJCCMQ03_CALL_METHOD = 1;
        public static final int IJCCMQ03_ESITO = 2;
        public static final int IJCCMQ03_LENGTH_DATI_SERVIZIO = 4;
        public static final int IJCCMQ03_MAX_ELE_ERRORI = 2;
        public static final int IJCCMQ03_DESC_ERRORE = 200;
        public static final int IJCCMQ03_COD_ERRORE = 6;
        public static final int IJCCMQ03_LIV_GRAVITA_BE = 1;
        public static final int IJCCMQ03_TIPO_TRATT_FE = 1;
        public static final int IJCCMQ03_ELE_ERRORI = IJCCMQ03_DESC_ERRORE + IJCCMQ03_COD_ERRORE + IJCCMQ03_LIV_GRAVITA_BE + IJCCMQ03_TIPO_TRATT_FE;
        public static final int IJCCMQ03_LENGTH_DATI_OUTPUT = 4;
        public static final int IJCCMQ03_VAR_AMBIENTE = 224;
        public static final int IJCCMQ03_ADDRESS_TYPE = 1;
        public static final int IJCCMQ03_ADDRESS = 4;
        public static final int IJCCMQ03_ADDRESSES = IJCCMQ03_ADDRESS_TYPE + IJCCMQ03_ADDRESS;
        public static final int IJCCMQ03_USER_NAME = 20;
        public static final int FLR1 = 2;
        public static final int IJCCMQ00_LIVELLO_DEBUG = 1;
        public static final int IJCCMQ03_CAR = 1;
        public static final int IJCCMQ03_INFO_FRAMES = IJCCMQ03_ID_APPLICATION + IJCCMQ03_TOT_NUM_FRAMES + IJCCMQ03_NUM_FRAME + IJCCMQ03_LENGTH_DATA_FRAME + IJCCMQ03_CALL_METHOD;
        public static final int IJCCMQ03_TAB_ERRORI_FRONT_END = AreaProductServices.IJCCMQ03_ELE_ERRORI_MAXOCCURS * IJCCMQ03_ELE_ERRORI;
        public static final int IJCCMQ03_AREA_ADDRESSES = AreaProductServices.IJCCMQ03_ADDRESSES_MAXOCCURS * IJCCMQ03_ADDRESSES;
        public static final int IJCCMQ03_HEADER = IJCCMQ03_MODALITA_ESECUTIVA + IJCCMQ03_JAVA_SERVICE_NAME + IJCCMQ03_INFO_FRAMES + IJCCMQ03_ESITO + IJCCMQ03_LENGTH_DATI_SERVIZIO + IJCCMQ03_MAX_ELE_ERRORI + IJCCMQ03_TAB_ERRORI_FRONT_END + IJCCMQ03_LENGTH_DATI_OUTPUT + IJCCMQ03_VAR_AMBIENTE + IJCCMQ03_AREA_ADDRESSES + IJCCMQ03_USER_NAME + IJCCMQ00_LIVELLO_DEBUG + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IJCCMQ00_LIVELLO_DEBUG = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
