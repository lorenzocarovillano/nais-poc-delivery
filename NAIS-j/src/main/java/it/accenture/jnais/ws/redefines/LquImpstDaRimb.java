package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-DA-RIMB<br>
 * Variable: LQU-IMPST-DA-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstDaRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstDaRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_DA_RIMB;
    }

    public void setLquImpstDaRimb(AfDecimal lquImpstDaRimb) {
        writeDecimalAsPacked(Pos.LQU_IMPST_DA_RIMB, lquImpstDaRimb.copy());
    }

    public void setLquImpstDaRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_DA_RIMB, Pos.LQU_IMPST_DA_RIMB);
    }

    /**Original name: LQU-IMPST-DA-RIMB<br>*/
    public AfDecimal getLquImpstDaRimb() {
        return readPackedAsDecimal(Pos.LQU_IMPST_DA_RIMB, Len.Int.LQU_IMPST_DA_RIMB, Len.Fract.LQU_IMPST_DA_RIMB);
    }

    public byte[] getLquImpstDaRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_DA_RIMB, Pos.LQU_IMPST_DA_RIMB);
        return buffer;
    }

    public void setLquImpstDaRimbNull(String lquImpstDaRimbNull) {
        writeString(Pos.LQU_IMPST_DA_RIMB_NULL, lquImpstDaRimbNull, Len.LQU_IMPST_DA_RIMB_NULL);
    }

    /**Original name: LQU-IMPST-DA-RIMB-NULL<br>*/
    public String getLquImpstDaRimbNull() {
        return readString(Pos.LQU_IMPST_DA_RIMB_NULL, Len.LQU_IMPST_DA_RIMB_NULL);
    }

    public String getLquImpstDaRimbNullFormatted() {
        return Functions.padBlanks(getLquImpstDaRimbNull(), Len.LQU_IMPST_DA_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_DA_RIMB = 1;
        public static final int LQU_IMPST_DA_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_DA_RIMB = 8;
        public static final int LQU_IMPST_DA_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_DA_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_DA_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
