package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-IS-IN<br>
 * Variable: PCO-DT-ULTC-IS-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcIsIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcIsIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_IS_IN;
    }

    public void setPcoDtUltcIsIn(int pcoDtUltcIsIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_IS_IN, pcoDtUltcIsIn, Len.Int.PCO_DT_ULTC_IS_IN);
    }

    public void setPcoDtUltcIsInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_IS_IN, Pos.PCO_DT_ULTC_IS_IN);
    }

    /**Original name: PCO-DT-ULTC-IS-IN<br>*/
    public int getPcoDtUltcIsIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_IS_IN, Len.Int.PCO_DT_ULTC_IS_IN);
    }

    public byte[] getPcoDtUltcIsInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_IS_IN, Pos.PCO_DT_ULTC_IS_IN);
        return buffer;
    }

    public void initPcoDtUltcIsInHighValues() {
        fill(Pos.PCO_DT_ULTC_IS_IN, Len.PCO_DT_ULTC_IS_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcIsInNull(String pcoDtUltcIsInNull) {
        writeString(Pos.PCO_DT_ULTC_IS_IN_NULL, pcoDtUltcIsInNull, Len.PCO_DT_ULTC_IS_IN_NULL);
    }

    /**Original name: PCO-DT-ULTC-IS-IN-NULL<br>*/
    public String getPcoDtUltcIsInNull() {
        return readString(Pos.PCO_DT_ULTC_IS_IN_NULL, Len.PCO_DT_ULTC_IS_IN_NULL);
    }

    public String getPcoDtUltcIsInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcIsInNull(), Len.PCO_DT_ULTC_IS_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_IS_IN = 1;
        public static final int PCO_DT_ULTC_IS_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_IS_IN = 5;
        public static final int PCO_DT_ULTC_IS_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_IS_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
