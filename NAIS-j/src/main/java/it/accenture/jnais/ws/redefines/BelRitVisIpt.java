package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-RIT-VIS-IPT<br>
 * Variable: BEL-RIT-VIS-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelRitVisIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelRitVisIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_RIT_VIS_IPT;
    }

    public void setBelRitVisIpt(AfDecimal belRitVisIpt) {
        writeDecimalAsPacked(Pos.BEL_RIT_VIS_IPT, belRitVisIpt.copy());
    }

    public void setBelRitVisIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_RIT_VIS_IPT, Pos.BEL_RIT_VIS_IPT);
    }

    /**Original name: BEL-RIT-VIS-IPT<br>*/
    public AfDecimal getBelRitVisIpt() {
        return readPackedAsDecimal(Pos.BEL_RIT_VIS_IPT, Len.Int.BEL_RIT_VIS_IPT, Len.Fract.BEL_RIT_VIS_IPT);
    }

    public byte[] getBelRitVisIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_RIT_VIS_IPT, Pos.BEL_RIT_VIS_IPT);
        return buffer;
    }

    public void setBelRitVisIptNull(String belRitVisIptNull) {
        writeString(Pos.BEL_RIT_VIS_IPT_NULL, belRitVisIptNull, Len.BEL_RIT_VIS_IPT_NULL);
    }

    /**Original name: BEL-RIT-VIS-IPT-NULL<br>*/
    public String getBelRitVisIptNull() {
        return readString(Pos.BEL_RIT_VIS_IPT_NULL, Len.BEL_RIT_VIS_IPT_NULL);
    }

    public String getBelRitVisIptNullFormatted() {
        return Functions.padBlanks(getBelRitVisIptNull(), Len.BEL_RIT_VIS_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_RIT_VIS_IPT = 1;
        public static final int BEL_RIT_VIS_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_RIT_VIS_IPT = 8;
        public static final int BEL_RIT_VIS_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_RIT_VIS_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_RIT_VIS_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
