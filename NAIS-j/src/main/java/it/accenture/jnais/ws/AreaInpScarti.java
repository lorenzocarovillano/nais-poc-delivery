package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.ScaAdeIdAdes;
import it.accenture.jnais.ws.redefines.ScaPolIdPoli;

/**Original name: AREA-INP-SCARTI<br>
 * Variable: AREA-INP-SCARTI from program LLBM0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaInpScarti {

    //==== PROPERTIES ====
    //Original name: SCA-POL-IB-OGG
    private String scaPolIbOgg = DefaultValues.stringVal(Len.SCA_POL_IB_OGG);
    //Original name: SCA-POL-ID-POLI
    private ScaPolIdPoli scaPolIdPoli = new ScaPolIdPoli();
    //Original name: SCA-ADE-ID-ADES
    private ScaAdeIdAdes scaAdeIdAdes = new ScaAdeIdAdes();
    //Original name: FILLER-AREA-INP-SCARTI
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== METHODS ====
    public void setAreaInpScartiFormatted(String data) {
        byte[] buffer = new byte[Len.AREA_INP_SCARTI];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_INP_SCARTI);
        setAreaInpScartiBytes(buffer, 1);
    }

    public void setAreaInpScartiBytes(byte[] buffer, int offset) {
        int position = offset;
        scaPolIbOgg = MarshalByte.readString(buffer, position, Len.SCA_POL_IB_OGG);
        position += Len.SCA_POL_IB_OGG;
        scaPolIdPoli.setScaPolIdPoliFromBuffer(buffer, position);
        position += ScaPolIdPoli.Len.SCA_POL_ID_POLI;
        scaAdeIdAdes.setScaAdeIdAdesFromBuffer(buffer, position);
        position += ScaAdeIdAdes.Len.SCA_ADE_ID_ADES;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public void setScaPolIbOgg(String scaPolIbOgg) {
        this.scaPolIbOgg = Functions.subString(scaPolIbOgg, Len.SCA_POL_IB_OGG);
    }

    public String getScaPolIbOgg() {
        return this.scaPolIbOgg;
    }

    public String getScaPolIbOggFormatted() {
        return Functions.padBlanks(getScaPolIbOgg(), Len.SCA_POL_IB_OGG);
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public ScaAdeIdAdes getScaAdeIdAdes() {
        return scaAdeIdAdes;
    }

    public ScaPolIdPoli getScaPolIdPoli() {
        return scaPolIdPoli;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SCA_POL_IB_OGG = 40;
        public static final int FLR1 = 242;
        public static final int AREA_INP_SCARTI = SCA_POL_IB_OGG + ScaPolIdPoli.Len.SCA_POL_ID_POLI + ScaAdeIdAdes.Len.SCA_ADE_ID_ADES + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
