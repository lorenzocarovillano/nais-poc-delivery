package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-SCARTA-GAR<br>
 * Variable: SW-SCARTA-GAR from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwScartaGar {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char GAR = 'S';
    public static final char GAR_NO = 'N';

    //==== METHODS ====
    public void setSwScartaGar(char swScartaGar) {
        this.value = swScartaGar;
    }

    public char getSwScartaGar() {
        return this.value;
    }

    public void setGarNo() {
        value = GAR_NO;
    }
}
