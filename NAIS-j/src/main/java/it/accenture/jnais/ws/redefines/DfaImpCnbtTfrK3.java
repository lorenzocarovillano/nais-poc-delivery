package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-TFR-K3<br>
 * Variable: DFA-IMP-CNBT-TFR-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtTfrK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtTfrK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_TFR_K3;
    }

    public void setDfaImpCnbtTfrK3(AfDecimal dfaImpCnbtTfrK3) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_TFR_K3, dfaImpCnbtTfrK3.copy());
    }

    public void setDfaImpCnbtTfrK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_TFR_K3, Pos.DFA_IMP_CNBT_TFR_K3);
    }

    /**Original name: DFA-IMP-CNBT-TFR-K3<br>*/
    public AfDecimal getDfaImpCnbtTfrK3() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_TFR_K3, Len.Int.DFA_IMP_CNBT_TFR_K3, Len.Fract.DFA_IMP_CNBT_TFR_K3);
    }

    public byte[] getDfaImpCnbtTfrK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_TFR_K3, Pos.DFA_IMP_CNBT_TFR_K3);
        return buffer;
    }

    public void setDfaImpCnbtTfrK3Null(String dfaImpCnbtTfrK3Null) {
        writeString(Pos.DFA_IMP_CNBT_TFR_K3_NULL, dfaImpCnbtTfrK3Null, Len.DFA_IMP_CNBT_TFR_K3_NULL);
    }

    /**Original name: DFA-IMP-CNBT-TFR-K3-NULL<br>*/
    public String getDfaImpCnbtTfrK3Null() {
        return readString(Pos.DFA_IMP_CNBT_TFR_K3_NULL, Len.DFA_IMP_CNBT_TFR_K3_NULL);
    }

    public String getDfaImpCnbtTfrK3NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtTfrK3Null(), Len.DFA_IMP_CNBT_TFR_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_TFR_K3 = 1;
        public static final int DFA_IMP_CNBT_TFR_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_TFR_K3 = 8;
        public static final int DFA_IMP_CNBT_TFR_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_TFR_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_TFR_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
