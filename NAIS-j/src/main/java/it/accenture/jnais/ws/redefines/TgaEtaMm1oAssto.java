package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ETA-MM-1O-ASSTO<br>
 * Variable: TGA-ETA-MM-1O-ASSTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaEtaMm1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaEtaMm1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ETA_MM1O_ASSTO;
    }

    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        writeShortAsPacked(Pos.TGA_ETA_MM1O_ASSTO, tgaEtaMm1oAssto, Len.Int.TGA_ETA_MM1O_ASSTO);
    }

    public void setTgaEtaMm1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ETA_MM1O_ASSTO, Pos.TGA_ETA_MM1O_ASSTO);
    }

    /**Original name: TGA-ETA-MM-1O-ASSTO<br>*/
    public short getTgaEtaMm1oAssto() {
        return readPackedAsShort(Pos.TGA_ETA_MM1O_ASSTO, Len.Int.TGA_ETA_MM1O_ASSTO);
    }

    public byte[] getTgaEtaMm1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ETA_MM1O_ASSTO, Pos.TGA_ETA_MM1O_ASSTO);
        return buffer;
    }

    public void setTgaEtaMm1oAsstoNull(String tgaEtaMm1oAsstoNull) {
        writeString(Pos.TGA_ETA_MM1O_ASSTO_NULL, tgaEtaMm1oAsstoNull, Len.TGA_ETA_MM1O_ASSTO_NULL);
    }

    /**Original name: TGA-ETA-MM-1O-ASSTO-NULL<br>*/
    public String getTgaEtaMm1oAsstoNull() {
        return readString(Pos.TGA_ETA_MM1O_ASSTO_NULL, Len.TGA_ETA_MM1O_ASSTO_NULL);
    }

    public String getTgaEtaMm1oAsstoNullFormatted() {
        return Functions.padBlanks(getTgaEtaMm1oAsstoNull(), Len.TGA_ETA_MM1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ETA_MM1O_ASSTO = 1;
        public static final int TGA_ETA_MM1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ETA_MM1O_ASSTO = 2;
        public static final int TGA_ETA_MM1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ETA_MM1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
