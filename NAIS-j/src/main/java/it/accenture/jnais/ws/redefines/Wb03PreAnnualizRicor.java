package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-ANNUALIZ-RICOR<br>
 * Variable: WB03-PRE-ANNUALIZ-RICOR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreAnnualizRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreAnnualizRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_ANNUALIZ_RICOR;
    }

    public void setWb03PreAnnualizRicor(AfDecimal wb03PreAnnualizRicor) {
        writeDecimalAsPacked(Pos.WB03_PRE_ANNUALIZ_RICOR, wb03PreAnnualizRicor.copy());
    }

    public void setWb03PreAnnualizRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_ANNUALIZ_RICOR, Pos.WB03_PRE_ANNUALIZ_RICOR);
    }

    /**Original name: WB03-PRE-ANNUALIZ-RICOR<br>*/
    public AfDecimal getWb03PreAnnualizRicor() {
        return readPackedAsDecimal(Pos.WB03_PRE_ANNUALIZ_RICOR, Len.Int.WB03_PRE_ANNUALIZ_RICOR, Len.Fract.WB03_PRE_ANNUALIZ_RICOR);
    }

    public byte[] getWb03PreAnnualizRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_ANNUALIZ_RICOR, Pos.WB03_PRE_ANNUALIZ_RICOR);
        return buffer;
    }

    public void setWb03PreAnnualizRicorNull(String wb03PreAnnualizRicorNull) {
        writeString(Pos.WB03_PRE_ANNUALIZ_RICOR_NULL, wb03PreAnnualizRicorNull, Len.WB03_PRE_ANNUALIZ_RICOR_NULL);
    }

    /**Original name: WB03-PRE-ANNUALIZ-RICOR-NULL<br>*/
    public String getWb03PreAnnualizRicorNull() {
        return readString(Pos.WB03_PRE_ANNUALIZ_RICOR_NULL, Len.WB03_PRE_ANNUALIZ_RICOR_NULL);
    }

    public String getWb03PreAnnualizRicorNullFormatted() {
        return Functions.padBlanks(getWb03PreAnnualizRicorNull(), Len.WB03_PRE_ANNUALIZ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_ANNUALIZ_RICOR = 1;
        public static final int WB03_PRE_ANNUALIZ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_ANNUALIZ_RICOR = 8;
        public static final int WB03_PRE_ANNUALIZ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_ANNUALIZ_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_ANNUALIZ_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
