package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-SDNL-I<br>
 * Variable: PCO-DT-ULT-BOLL-SDNL-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollSdnlI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollSdnlI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_SDNL_I;
    }

    public void setPcoDtUltBollSdnlI(int pcoDtUltBollSdnlI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_SDNL_I, pcoDtUltBollSdnlI, Len.Int.PCO_DT_ULT_BOLL_SDNL_I);
    }

    public void setPcoDtUltBollSdnlIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SDNL_I, Pos.PCO_DT_ULT_BOLL_SDNL_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-SDNL-I<br>*/
    public int getPcoDtUltBollSdnlI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_SDNL_I, Len.Int.PCO_DT_ULT_BOLL_SDNL_I);
    }

    public byte[] getPcoDtUltBollSdnlIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_SDNL_I, Pos.PCO_DT_ULT_BOLL_SDNL_I);
        return buffer;
    }

    public void initPcoDtUltBollSdnlIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_SDNL_I, Len.PCO_DT_ULT_BOLL_SDNL_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollSdnlINull(String pcoDtUltBollSdnlINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_SDNL_I_NULL, pcoDtUltBollSdnlINull, Len.PCO_DT_ULT_BOLL_SDNL_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-SDNL-I-NULL<br>*/
    public String getPcoDtUltBollSdnlINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_SDNL_I_NULL, Len.PCO_DT_ULT_BOLL_SDNL_I_NULL);
    }

    public String getPcoDtUltBollSdnlINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollSdnlINull(), Len.PCO_DT_ULT_BOLL_SDNL_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SDNL_I = 1;
        public static final int PCO_DT_ULT_BOLL_SDNL_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_SDNL_I = 5;
        public static final int PCO_DT_ULT_BOLL_SDNL_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_SDNL_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
