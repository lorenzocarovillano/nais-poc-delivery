package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-ADDIZ-COMUN<br>
 * Variable: LQU-ADDIZ-COMUN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquAddizComun extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquAddizComun() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_ADDIZ_COMUN;
    }

    public void setLquAddizComun(AfDecimal lquAddizComun) {
        writeDecimalAsPacked(Pos.LQU_ADDIZ_COMUN, lquAddizComun.copy());
    }

    public void setLquAddizComunFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_ADDIZ_COMUN, Pos.LQU_ADDIZ_COMUN);
    }

    /**Original name: LQU-ADDIZ-COMUN<br>*/
    public AfDecimal getLquAddizComun() {
        return readPackedAsDecimal(Pos.LQU_ADDIZ_COMUN, Len.Int.LQU_ADDIZ_COMUN, Len.Fract.LQU_ADDIZ_COMUN);
    }

    public byte[] getLquAddizComunAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_ADDIZ_COMUN, Pos.LQU_ADDIZ_COMUN);
        return buffer;
    }

    public void setLquAddizComunNull(String lquAddizComunNull) {
        writeString(Pos.LQU_ADDIZ_COMUN_NULL, lquAddizComunNull, Len.LQU_ADDIZ_COMUN_NULL);
    }

    /**Original name: LQU-ADDIZ-COMUN-NULL<br>*/
    public String getLquAddizComunNull() {
        return readString(Pos.LQU_ADDIZ_COMUN_NULL, Len.LQU_ADDIZ_COMUN_NULL);
    }

    public String getLquAddizComunNullFormatted() {
        return Functions.padBlanks(getLquAddizComunNull(), Len.LQU_ADDIZ_COMUN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_ADDIZ_COMUN = 1;
        public static final int LQU_ADDIZ_COMUN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_ADDIZ_COMUN = 8;
        public static final int LQU_ADDIZ_COMUN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_ADDIZ_COMUN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_ADDIZ_COMUN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
