package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RES-PRE-ATT-CALC<br>
 * Variable: WDFL-RES-PRE-ATT-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflResPreAttCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflResPreAttCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RES_PRE_ATT_CALC;
    }

    public void setWdflResPreAttCalc(AfDecimal wdflResPreAttCalc) {
        writeDecimalAsPacked(Pos.WDFL_RES_PRE_ATT_CALC, wdflResPreAttCalc.copy());
    }

    public void setWdflResPreAttCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RES_PRE_ATT_CALC, Pos.WDFL_RES_PRE_ATT_CALC);
    }

    /**Original name: WDFL-RES-PRE-ATT-CALC<br>*/
    public AfDecimal getWdflResPreAttCalc() {
        return readPackedAsDecimal(Pos.WDFL_RES_PRE_ATT_CALC, Len.Int.WDFL_RES_PRE_ATT_CALC, Len.Fract.WDFL_RES_PRE_ATT_CALC);
    }

    public byte[] getWdflResPreAttCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RES_PRE_ATT_CALC, Pos.WDFL_RES_PRE_ATT_CALC);
        return buffer;
    }

    public void setWdflResPreAttCalcNull(String wdflResPreAttCalcNull) {
        writeString(Pos.WDFL_RES_PRE_ATT_CALC_NULL, wdflResPreAttCalcNull, Len.WDFL_RES_PRE_ATT_CALC_NULL);
    }

    /**Original name: WDFL-RES-PRE-ATT-CALC-NULL<br>*/
    public String getWdflResPreAttCalcNull() {
        return readString(Pos.WDFL_RES_PRE_ATT_CALC_NULL, Len.WDFL_RES_PRE_ATT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRE_ATT_CALC = 1;
        public static final int WDFL_RES_PRE_ATT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRE_ATT_CALC = 8;
        public static final int WDFL_RES_PRE_ATT_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRE_ATT_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRE_ATT_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
