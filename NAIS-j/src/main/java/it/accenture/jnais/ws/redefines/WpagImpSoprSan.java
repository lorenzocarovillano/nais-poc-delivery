package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-SOPR-SAN<br>
 * Variable: WPAG-IMP-SOPR-SAN from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_SOPR_SAN;
    }

    public void setWpagImpSoprSan(AfDecimal wpagImpSoprSan) {
        writeDecimalAsPacked(Pos.WPAG_IMP_SOPR_SAN, wpagImpSoprSan.copy());
    }

    public void setWpagImpSoprSanFormatted(String wpagImpSoprSan) {
        setWpagImpSoprSan(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_SOPR_SAN + Len.Fract.WPAG_IMP_SOPR_SAN, Len.Fract.WPAG_IMP_SOPR_SAN, wpagImpSoprSan));
    }

    public void setWpagImpSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_SOPR_SAN, Pos.WPAG_IMP_SOPR_SAN);
    }

    /**Original name: WPAG-IMP-SOPR-SAN<br>*/
    public AfDecimal getWpagImpSoprSan() {
        return readPackedAsDecimal(Pos.WPAG_IMP_SOPR_SAN, Len.Int.WPAG_IMP_SOPR_SAN, Len.Fract.WPAG_IMP_SOPR_SAN);
    }

    public byte[] getWpagImpSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_SOPR_SAN, Pos.WPAG_IMP_SOPR_SAN);
        return buffer;
    }

    public void initWpagImpSoprSanSpaces() {
        fill(Pos.WPAG_IMP_SOPR_SAN, Len.WPAG_IMP_SOPR_SAN, Types.SPACE_CHAR);
    }

    public void setWpagImpSoprSanNull(String wpagImpSoprSanNull) {
        writeString(Pos.WPAG_IMP_SOPR_SAN_NULL, wpagImpSoprSanNull, Len.WPAG_IMP_SOPR_SAN_NULL);
    }

    /**Original name: WPAG-IMP-SOPR-SAN-NULL<br>*/
    public String getWpagImpSoprSanNull() {
        return readString(Pos.WPAG_IMP_SOPR_SAN_NULL, Len.WPAG_IMP_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_SAN = 1;
        public static final int WPAG_IMP_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_SOPR_SAN = 8;
        public static final int WPAG_IMP_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
