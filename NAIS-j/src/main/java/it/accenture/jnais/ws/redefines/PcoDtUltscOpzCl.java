package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTSC-OPZ-CL<br>
 * Variable: PCO-DT-ULTSC-OPZ-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltscOpzCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltscOpzCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTSC_OPZ_CL;
    }

    public void setPcoDtUltscOpzCl(int pcoDtUltscOpzCl) {
        writeIntAsPacked(Pos.PCO_DT_ULTSC_OPZ_CL, pcoDtUltscOpzCl, Len.Int.PCO_DT_ULTSC_OPZ_CL);
    }

    public void setPcoDtUltscOpzClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTSC_OPZ_CL, Pos.PCO_DT_ULTSC_OPZ_CL);
    }

    /**Original name: PCO-DT-ULTSC-OPZ-CL<br>*/
    public int getPcoDtUltscOpzCl() {
        return readPackedAsInt(Pos.PCO_DT_ULTSC_OPZ_CL, Len.Int.PCO_DT_ULTSC_OPZ_CL);
    }

    public byte[] getPcoDtUltscOpzClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTSC_OPZ_CL, Pos.PCO_DT_ULTSC_OPZ_CL);
        return buffer;
    }

    public void initPcoDtUltscOpzClHighValues() {
        fill(Pos.PCO_DT_ULTSC_OPZ_CL, Len.PCO_DT_ULTSC_OPZ_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltscOpzClNull(String pcoDtUltscOpzClNull) {
        writeString(Pos.PCO_DT_ULTSC_OPZ_CL_NULL, pcoDtUltscOpzClNull, Len.PCO_DT_ULTSC_OPZ_CL_NULL);
    }

    /**Original name: PCO-DT-ULTSC-OPZ-CL-NULL<br>*/
    public String getPcoDtUltscOpzClNull() {
        return readString(Pos.PCO_DT_ULTSC_OPZ_CL_NULL, Len.PCO_DT_ULTSC_OPZ_CL_NULL);
    }

    public String getPcoDtUltscOpzClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltscOpzClNull(), Len.PCO_DT_ULTSC_OPZ_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_OPZ_CL = 1;
        public static final int PCO_DT_ULTSC_OPZ_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_OPZ_CL = 5;
        public static final int PCO_DT_ULTSC_OPZ_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTSC_OPZ_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
