package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-PRVR-IPT<br>
 * Variable: WBEL-IMPST-PRVR-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstPrvrIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstPrvrIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_PRVR_IPT;
    }

    public void setWbelImpstPrvrIpt(AfDecimal wbelImpstPrvrIpt) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_PRVR_IPT, wbelImpstPrvrIpt.copy());
    }

    public void setWbelImpstPrvrIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_PRVR_IPT, Pos.WBEL_IMPST_PRVR_IPT);
    }

    /**Original name: WBEL-IMPST-PRVR-IPT<br>*/
    public AfDecimal getWbelImpstPrvrIpt() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_PRVR_IPT, Len.Int.WBEL_IMPST_PRVR_IPT, Len.Fract.WBEL_IMPST_PRVR_IPT);
    }

    public byte[] getWbelImpstPrvrIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_PRVR_IPT, Pos.WBEL_IMPST_PRVR_IPT);
        return buffer;
    }

    public void initWbelImpstPrvrIptSpaces() {
        fill(Pos.WBEL_IMPST_PRVR_IPT, Len.WBEL_IMPST_PRVR_IPT, Types.SPACE_CHAR);
    }

    public void setWbelImpstPrvrIptNull(String wbelImpstPrvrIptNull) {
        writeString(Pos.WBEL_IMPST_PRVR_IPT_NULL, wbelImpstPrvrIptNull, Len.WBEL_IMPST_PRVR_IPT_NULL);
    }

    /**Original name: WBEL-IMPST-PRVR-IPT-NULL<br>*/
    public String getWbelImpstPrvrIptNull() {
        return readString(Pos.WBEL_IMPST_PRVR_IPT_NULL, Len.WBEL_IMPST_PRVR_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_PRVR_IPT = 1;
        public static final int WBEL_IMPST_PRVR_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_PRVR_IPT = 8;
        public static final int WBEL_IMPST_PRVR_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_PRVR_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_PRVR_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
