package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-ELAB<br>
 * Variable: FLAG-FINE-ELAB from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineElab {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagFineElab(char flagFineElab) {
        this.value = flagFineElab;
    }

    public char getFlagFineElab() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
