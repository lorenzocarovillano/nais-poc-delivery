package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program IDSS0160<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndici {

    //==== PROPERTIES ====
    //Original name: IX-TABB
    private short ixTabb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AP-TABB
    private short ixApTabb = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-DATO
    private short ixDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-AP-DATO
    private short ixApDato = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-IDSS0020
    private short ixIdss0020 = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-STR
    private short indStr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-CNO
    private short ixTabCno = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RRE
    private short ixTabRre = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-VAL
    private short ixTabVal = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-WKS-TAB
    private short ixWksTab = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setIxTabb(short ixTabb) {
        this.ixTabb = ixTabb;
    }

    public short getIxTabb() {
        return this.ixTabb;
    }

    public void setIxApTabb(short ixApTabb) {
        this.ixApTabb = ixApTabb;
    }

    public short getIxApTabb() {
        return this.ixApTabb;
    }

    public void setIxDato(short ixDato) {
        this.ixDato = ixDato;
    }

    public short getIxDato() {
        return this.ixDato;
    }

    public void setIxApDato(short ixApDato) {
        this.ixApDato = ixApDato;
    }

    public short getIxApDato() {
        return this.ixApDato;
    }

    public void setIxIdss0020(short ixIdss0020) {
        this.ixIdss0020 = ixIdss0020;
    }

    public short getIxIdss0020() {
        return this.ixIdss0020;
    }

    public void setIndStr(short indStr) {
        this.indStr = indStr;
    }

    public short getIndStr() {
        return this.indStr;
    }

    public void setIxTabCno(short ixTabCno) {
        this.ixTabCno = ixTabCno;
    }

    public short getIxTabCno() {
        return this.ixTabCno;
    }

    public void setIxTabRre(short ixTabRre) {
        this.ixTabRre = ixTabRre;
    }

    public short getIxTabRre() {
        return this.ixTabRre;
    }

    public void setIxTabVal(short ixTabVal) {
        this.ixTabVal = ixTabVal;
    }

    public short getIxTabVal() {
        return this.ixTabVal;
    }

    public void setIxWksTab(short ixWksTab) {
        this.ixWksTab = ixWksTab;
    }

    public short getIxWksTab() {
        return this.ixWksTab;
    }
}
