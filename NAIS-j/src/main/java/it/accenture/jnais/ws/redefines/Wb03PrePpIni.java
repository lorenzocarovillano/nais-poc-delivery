package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-PP-INI<br>
 * Variable: WB03-PRE-PP-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrePpIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrePpIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_PP_INI;
    }

    public void setWb03PrePpIni(AfDecimal wb03PrePpIni) {
        writeDecimalAsPacked(Pos.WB03_PRE_PP_INI, wb03PrePpIni.copy());
    }

    public void setWb03PrePpIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_PP_INI, Pos.WB03_PRE_PP_INI);
    }

    /**Original name: WB03-PRE-PP-INI<br>*/
    public AfDecimal getWb03PrePpIni() {
        return readPackedAsDecimal(Pos.WB03_PRE_PP_INI, Len.Int.WB03_PRE_PP_INI, Len.Fract.WB03_PRE_PP_INI);
    }

    public byte[] getWb03PrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_PP_INI, Pos.WB03_PRE_PP_INI);
        return buffer;
    }

    public void setWb03PrePpIniNull(String wb03PrePpIniNull) {
        writeString(Pos.WB03_PRE_PP_INI_NULL, wb03PrePpIniNull, Len.WB03_PRE_PP_INI_NULL);
    }

    /**Original name: WB03-PRE-PP-INI-NULL<br>*/
    public String getWb03PrePpIniNull() {
        return readString(Pos.WB03_PRE_PP_INI_NULL, Len.WB03_PRE_PP_INI_NULL);
    }

    public String getWb03PrePpIniNullFormatted() {
        return Functions.padBlanks(getWb03PrePpIniNull(), Len.WB03_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_PP_INI = 1;
        public static final int WB03_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_PP_INI = 8;
        public static final int WB03_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
