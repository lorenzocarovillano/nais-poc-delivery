package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-IMP-COMMIS-INT<br>
 * Variable: WPAG-RATA-IMP-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataImpCommisInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataImpCommisInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_IMP_COMMIS_INT;
    }

    public void setWpagRataImpCommisInt(AfDecimal wpagRataImpCommisInt) {
        writeDecimalAsPacked(Pos.WPAG_RATA_IMP_COMMIS_INT, wpagRataImpCommisInt.copy());
    }

    public void setWpagRataImpCommisIntFormatted(String wpagRataImpCommisInt) {
        setWpagRataImpCommisInt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_IMP_COMMIS_INT + Len.Fract.WPAG_RATA_IMP_COMMIS_INT, Len.Fract.WPAG_RATA_IMP_COMMIS_INT, wpagRataImpCommisInt));
    }

    public void setWpagRataImpCommisIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_IMP_COMMIS_INT, Pos.WPAG_RATA_IMP_COMMIS_INT);
    }

    /**Original name: WPAG-RATA-IMP-COMMIS-INT<br>*/
    public AfDecimal getWpagRataImpCommisInt() {
        return readPackedAsDecimal(Pos.WPAG_RATA_IMP_COMMIS_INT, Len.Int.WPAG_RATA_IMP_COMMIS_INT, Len.Fract.WPAG_RATA_IMP_COMMIS_INT);
    }

    public byte[] getWpagRataImpCommisIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_IMP_COMMIS_INT, Pos.WPAG_RATA_IMP_COMMIS_INT);
        return buffer;
    }

    public void initWpagRataImpCommisIntSpaces() {
        fill(Pos.WPAG_RATA_IMP_COMMIS_INT, Len.WPAG_RATA_IMP_COMMIS_INT, Types.SPACE_CHAR);
    }

    public void setWpagRataImpCommisIntNull(String wpagRataImpCommisIntNull) {
        writeString(Pos.WPAG_RATA_IMP_COMMIS_INT_NULL, wpagRataImpCommisIntNull, Len.WPAG_RATA_IMP_COMMIS_INT_NULL);
    }

    /**Original name: WPAG-RATA-IMP-COMMIS-INT-NULL<br>*/
    public String getWpagRataImpCommisIntNull() {
        return readString(Pos.WPAG_RATA_IMP_COMMIS_INT_NULL, Len.WPAG_RATA_IMP_COMMIS_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_IMP_COMMIS_INT = 1;
        public static final int WPAG_RATA_IMP_COMMIS_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_IMP_COMMIS_INT = 8;
        public static final int WPAG_RATA_IMP_COMMIS_INT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_IMP_COMMIS_INT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_IMP_COMMIS_INT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
