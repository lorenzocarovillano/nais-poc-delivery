package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-TS-FINANZ<br>
 * Variable: WP67-TS-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67TsFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67TsFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_TS_FINANZ;
    }

    public void setWp67TsFinanz(AfDecimal wp67TsFinanz) {
        writeDecimalAsPacked(Pos.WP67_TS_FINANZ, wp67TsFinanz.copy());
    }

    public void setWp67TsFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_TS_FINANZ, Pos.WP67_TS_FINANZ);
    }

    /**Original name: WP67-TS-FINANZ<br>*/
    public AfDecimal getWp67TsFinanz() {
        return readPackedAsDecimal(Pos.WP67_TS_FINANZ, Len.Int.WP67_TS_FINANZ, Len.Fract.WP67_TS_FINANZ);
    }

    public byte[] getWp67TsFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_TS_FINANZ, Pos.WP67_TS_FINANZ);
        return buffer;
    }

    public void setWp67TsFinanzNull(String wp67TsFinanzNull) {
        writeString(Pos.WP67_TS_FINANZ_NULL, wp67TsFinanzNull, Len.WP67_TS_FINANZ_NULL);
    }

    /**Original name: WP67-TS-FINANZ-NULL<br>*/
    public String getWp67TsFinanzNull() {
        return readString(Pos.WP67_TS_FINANZ_NULL, Len.WP67_TS_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_TS_FINANZ = 1;
        public static final int WP67_TS_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_TS_FINANZ = 8;
        public static final int WP67_TS_FINANZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_TS_FINANZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_TS_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
