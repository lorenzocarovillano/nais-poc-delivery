package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-DT-EFF<br>
 * Variable: P01-DT-EFF from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01DtEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01DtEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_DT_EFF;
    }

    public void setP01DtEff(int p01DtEff) {
        writeIntAsPacked(Pos.P01_DT_EFF, p01DtEff, Len.Int.P01_DT_EFF);
    }

    public void setP01DtEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_DT_EFF, Pos.P01_DT_EFF);
    }

    /**Original name: P01-DT-EFF<br>*/
    public int getP01DtEff() {
        return readPackedAsInt(Pos.P01_DT_EFF, Len.Int.P01_DT_EFF);
    }

    public byte[] getP01DtEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_DT_EFF, Pos.P01_DT_EFF);
        return buffer;
    }

    public void setP01DtEffNull(String p01DtEffNull) {
        writeString(Pos.P01_DT_EFF_NULL, p01DtEffNull, Len.P01_DT_EFF_NULL);
    }

    /**Original name: P01-DT-EFF-NULL<br>*/
    public String getP01DtEffNull() {
        return readString(Pos.P01_DT_EFF_NULL, Len.P01_DT_EFF_NULL);
    }

    public String getP01DtEffNullFormatted() {
        return Functions.padBlanks(getP01DtEffNull(), Len.P01_DT_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_DT_EFF = 1;
        public static final int P01_DT_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_DT_EFF = 5;
        public static final int P01_DT_EFF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_DT_EFF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
