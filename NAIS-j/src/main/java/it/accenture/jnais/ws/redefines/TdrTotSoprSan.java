package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SOPR-SAN<br>
 * Variable: TDR-TOT-SOPR-SAN from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SOPR_SAN;
    }

    public void setTdrTotSoprSan(AfDecimal tdrTotSoprSan) {
        writeDecimalAsPacked(Pos.TDR_TOT_SOPR_SAN, tdrTotSoprSan.copy());
    }

    public void setTdrTotSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SOPR_SAN, Pos.TDR_TOT_SOPR_SAN);
    }

    /**Original name: TDR-TOT-SOPR-SAN<br>*/
    public AfDecimal getTdrTotSoprSan() {
        return readPackedAsDecimal(Pos.TDR_TOT_SOPR_SAN, Len.Int.TDR_TOT_SOPR_SAN, Len.Fract.TDR_TOT_SOPR_SAN);
    }

    public byte[] getTdrTotSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SOPR_SAN, Pos.TDR_TOT_SOPR_SAN);
        return buffer;
    }

    public void setTdrTotSoprSanNull(String tdrTotSoprSanNull) {
        writeString(Pos.TDR_TOT_SOPR_SAN_NULL, tdrTotSoprSanNull, Len.TDR_TOT_SOPR_SAN_NULL);
    }

    /**Original name: TDR-TOT-SOPR-SAN-NULL<br>*/
    public String getTdrTotSoprSanNull() {
        return readString(Pos.TDR_TOT_SOPR_SAN_NULL, Len.TDR_TOT_SOPR_SAN_NULL);
    }

    public String getTdrTotSoprSanNullFormatted() {
        return Functions.padBlanks(getTdrTotSoprSanNull(), Len.TDR_TOT_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_SAN = 1;
        public static final int TDR_TOT_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_SAN = 8;
        public static final int TDR_TOT_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
