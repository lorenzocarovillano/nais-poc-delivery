package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-INCR-PRSTZ<br>
 * Variable: WTGA-INCR-PRSTZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaIncrPrstz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaIncrPrstz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_INCR_PRSTZ;
    }

    public void setWtgaIncrPrstz(AfDecimal wtgaIncrPrstz) {
        writeDecimalAsPacked(Pos.WTGA_INCR_PRSTZ, wtgaIncrPrstz.copy());
    }

    public void setWtgaIncrPrstzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_INCR_PRSTZ, Pos.WTGA_INCR_PRSTZ);
    }

    /**Original name: WTGA-INCR-PRSTZ<br>*/
    public AfDecimal getWtgaIncrPrstz() {
        return readPackedAsDecimal(Pos.WTGA_INCR_PRSTZ, Len.Int.WTGA_INCR_PRSTZ, Len.Fract.WTGA_INCR_PRSTZ);
    }

    public byte[] getWtgaIncrPrstzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_INCR_PRSTZ, Pos.WTGA_INCR_PRSTZ);
        return buffer;
    }

    public void initWtgaIncrPrstzSpaces() {
        fill(Pos.WTGA_INCR_PRSTZ, Len.WTGA_INCR_PRSTZ, Types.SPACE_CHAR);
    }

    public void setWtgaIncrPrstzNull(String wtgaIncrPrstzNull) {
        writeString(Pos.WTGA_INCR_PRSTZ_NULL, wtgaIncrPrstzNull, Len.WTGA_INCR_PRSTZ_NULL);
    }

    /**Original name: WTGA-INCR-PRSTZ-NULL<br>*/
    public String getWtgaIncrPrstzNull() {
        return readString(Pos.WTGA_INCR_PRSTZ_NULL, Len.WTGA_INCR_PRSTZ_NULL);
    }

    public String getWtgaIncrPrstzNullFormatted() {
        return Functions.padBlanks(getWtgaIncrPrstzNull(), Len.WTGA_INCR_PRSTZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_INCR_PRSTZ = 1;
        public static final int WTGA_INCR_PRSTZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_INCR_PRSTZ = 8;
        public static final int WTGA_INCR_PRSTZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_INCR_PRSTZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_INCR_PRSTZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
