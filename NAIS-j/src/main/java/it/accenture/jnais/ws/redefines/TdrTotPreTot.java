package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PRE-TOT<br>
 * Variable: TDR-TOT-PRE-TOT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PRE_TOT;
    }

    public void setTdrTotPreTot(AfDecimal tdrTotPreTot) {
        writeDecimalAsPacked(Pos.TDR_TOT_PRE_TOT, tdrTotPreTot.copy());
    }

    public void setTdrTotPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PRE_TOT, Pos.TDR_TOT_PRE_TOT);
    }

    /**Original name: TDR-TOT-PRE-TOT<br>*/
    public AfDecimal getTdrTotPreTot() {
        return readPackedAsDecimal(Pos.TDR_TOT_PRE_TOT, Len.Int.TDR_TOT_PRE_TOT, Len.Fract.TDR_TOT_PRE_TOT);
    }

    public byte[] getTdrTotPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PRE_TOT, Pos.TDR_TOT_PRE_TOT);
        return buffer;
    }

    public void setTdrTotPreTotNull(String tdrTotPreTotNull) {
        writeString(Pos.TDR_TOT_PRE_TOT_NULL, tdrTotPreTotNull, Len.TDR_TOT_PRE_TOT_NULL);
    }

    /**Original name: TDR-TOT-PRE-TOT-NULL<br>*/
    public String getTdrTotPreTotNull() {
        return readString(Pos.TDR_TOT_PRE_TOT_NULL, Len.TDR_TOT_PRE_TOT_NULL);
    }

    public String getTdrTotPreTotNullFormatted() {
        return Functions.padBlanks(getTdrTotPreTotNull(), Len.TDR_TOT_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_TOT = 1;
        public static final int TDR_TOT_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_TOT = 8;
        public static final int TDR_TOT_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
