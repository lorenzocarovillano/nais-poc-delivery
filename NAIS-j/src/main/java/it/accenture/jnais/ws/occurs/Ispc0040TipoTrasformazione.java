package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ispc0040CodTpTrasf;

/**Original name: ISPC0040-TIPO-TRASFORMAZIONE<br>
 * Variables: ISPC0040-TIPO-TRASFORMAZIONE from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040TipoTrasformazione {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-TP-TRASF
    private Ispc0040CodTpTrasf codTpTrasf = new Ispc0040CodTpTrasf();
    //Original name: ISPC0040-DESCR-TP-TRASF
    private String descrTpTrasf = DefaultValues.stringVal(Len.DESCR_TP_TRASF);

    //==== METHODS ====
    public void setTipoTrasformazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        codTpTrasf.setCodTpTrasf(MarshalByte.readString(buffer, position, Ispc0040CodTpTrasf.Len.COD_TP_TRASF));
        position += Ispc0040CodTpTrasf.Len.COD_TP_TRASF;
        descrTpTrasf = MarshalByte.readString(buffer, position, Len.DESCR_TP_TRASF);
    }

    public byte[] getTipoTrasformazioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codTpTrasf.getCodTpTrasf(), Ispc0040CodTpTrasf.Len.COD_TP_TRASF);
        position += Ispc0040CodTpTrasf.Len.COD_TP_TRASF;
        MarshalByte.writeString(buffer, position, descrTpTrasf, Len.DESCR_TP_TRASF);
        return buffer;
    }

    public void initTipoTrasformazioneSpaces() {
        codTpTrasf.setCodTpTrasf("");
        descrTpTrasf = "";
    }

    public void setDescrTpTrasf(String descrTpTrasf) {
        this.descrTpTrasf = Functions.subString(descrTpTrasf, Len.DESCR_TP_TRASF);
    }

    public String getDescrTpTrasf() {
        return this.descrTpTrasf;
    }

    public Ispc0040CodTpTrasf getCodTpTrasf() {
        return codTpTrasf;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCR_TP_TRASF = 30;
        public static final int TIPO_TRASFORMAZIONE = Ispc0040CodTpTrasf.Len.COD_TP_TRASF + DESCR_TP_TRASF;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
