package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RAT-REN<br>
 * Variable: W-B03-RAT-REN from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RatRenLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RatRenLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RAT_REN;
    }

    public void setwB03RatRen(AfDecimal wB03RatRen) {
        writeDecimalAsPacked(Pos.W_B03_RAT_REN, wB03RatRen.copy());
    }

    /**Original name: W-B03-RAT-REN<br>*/
    public AfDecimal getwB03RatRen() {
        return readPackedAsDecimal(Pos.W_B03_RAT_REN, Len.Int.W_B03_RAT_REN, Len.Fract.W_B03_RAT_REN);
    }

    public byte[] getwB03RatRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RAT_REN, Pos.W_B03_RAT_REN);
        return buffer;
    }

    public void setwB03RatRenNull(String wB03RatRenNull) {
        writeString(Pos.W_B03_RAT_REN_NULL, wB03RatRenNull, Len.W_B03_RAT_REN_NULL);
    }

    /**Original name: W-B03-RAT-REN-NULL<br>*/
    public String getwB03RatRenNull() {
        return readString(Pos.W_B03_RAT_REN_NULL, Len.W_B03_RAT_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RAT_REN = 1;
        public static final int W_B03_RAT_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RAT_REN = 8;
        public static final int W_B03_RAT_REN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RAT_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RAT_REN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
