package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-CORTVO<br>
 * Variable: TCL-IMP-CORTVO from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpCortvo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpCortvo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_CORTVO;
    }

    public void setTclImpCortvo(AfDecimal tclImpCortvo) {
        writeDecimalAsPacked(Pos.TCL_IMP_CORTVO, tclImpCortvo.copy());
    }

    public void setTclImpCortvoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_CORTVO, Pos.TCL_IMP_CORTVO);
    }

    /**Original name: TCL-IMP-CORTVO<br>*/
    public AfDecimal getTclImpCortvo() {
        return readPackedAsDecimal(Pos.TCL_IMP_CORTVO, Len.Int.TCL_IMP_CORTVO, Len.Fract.TCL_IMP_CORTVO);
    }

    public byte[] getTclImpCortvoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_CORTVO, Pos.TCL_IMP_CORTVO);
        return buffer;
    }

    public void setTclImpCortvoNull(String tclImpCortvoNull) {
        writeString(Pos.TCL_IMP_CORTVO_NULL, tclImpCortvoNull, Len.TCL_IMP_CORTVO_NULL);
    }

    /**Original name: TCL-IMP-CORTVO-NULL<br>*/
    public String getTclImpCortvoNull() {
        return readString(Pos.TCL_IMP_CORTVO_NULL, Len.TCL_IMP_CORTVO_NULL);
    }

    public String getTclImpCortvoNullFormatted() {
        return Functions.padBlanks(getTclImpCortvoNull(), Len.TCL_IMP_CORTVO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_CORTVO = 1;
        public static final int TCL_IMP_CORTVO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_CORTVO = 8;
        public static final int TCL_IMP_CORTVO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_CORTVO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_CORTVO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
