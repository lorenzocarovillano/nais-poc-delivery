package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import it.accenture.jnais.ws.redefines.WpagImpAderTdr;
import it.accenture.jnais.ws.redefines.WpagImpAzTdr;
import it.accenture.jnais.ws.redefines.WpagImpCarAcqTdr;
import it.accenture.jnais.ws.redefines.WpagImpCarGestTdr;
import it.accenture.jnais.ws.redefines.WpagImpCarIncTdr;
import it.accenture.jnais.ws.redefines.WpagImpIntRiatt;
import it.accenture.jnais.ws.redefines.WpagImpTfrTdr;
import it.accenture.jnais.ws.redefines.WpagImpVoloTdr;
import it.accenture.jnais.ws.redefines.WpagRataAcq1oAnno;
import it.accenture.jnais.ws.redefines.WpagRataAcq2oAnno;
import it.accenture.jnais.ws.redefines.WpagRataAltriSopr;
import it.accenture.jnais.ws.redefines.WpagRataDiritti;
import it.accenture.jnais.ws.redefines.WpagRataImpAcqExp;
import it.accenture.jnais.ws.redefines.WpagRataImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagRataImpRenAss;
import it.accenture.jnais.ws.redefines.WpagRataIncas;
import it.accenture.jnais.ws.redefines.WpagRataIntFraz;
import it.accenture.jnais.ws.redefines.WpagRataIntRetrodt;
import it.accenture.jnais.ws.redefines.WpagRataPremioNetto;
import it.accenture.jnais.ws.redefines.WpagRataPremioPuroIas;
import it.accenture.jnais.ws.redefines.WpagRataPremioRisc;
import it.accenture.jnais.ws.redefines.WpagRataPremioTot;
import it.accenture.jnais.ws.redefines.WpagRataRicor;
import it.accenture.jnais.ws.redefines.WpagRataSoprProfes;
import it.accenture.jnais.ws.redefines.WpagRataSoprSanit;
import it.accenture.jnais.ws.redefines.WpagRataSoprSport;
import it.accenture.jnais.ws.redefines.WpagRataSoprTecn;
import it.accenture.jnais.ws.redefines.WpagRataSpeseMediche;
import it.accenture.jnais.ws.redefines.WpagRataTasse;

/**Original name: WPAG-DATI-TIT-RATA<br>
 * Variables: WPAG-DATI-TIT-RATA from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagDatiTitRata {

    //==== PROPERTIES ====
    //Original name: WPAG-RATA-PREMIO-NETTO
    private WpagRataPremioNetto wpagRataPremioNetto = new WpagRataPremioNetto();
    //Original name: WPAG-RATA-INT-FRAZ
    private WpagRataIntFraz wpagRataIntFraz = new WpagRataIntFraz();
    //Original name: WPAG-RATA-INT-RETRODT
    private WpagRataIntRetrodt wpagRataIntRetrodt = new WpagRataIntRetrodt();
    //Original name: WPAG-RATA-DIRITTI
    private WpagRataDiritti wpagRataDiritti = new WpagRataDiritti();
    //Original name: WPAG-RATA-SPESE-MEDICHE
    private WpagRataSpeseMediche wpagRataSpeseMediche = new WpagRataSpeseMediche();
    //Original name: WPAG-RATA-TASSE
    private WpagRataTasse wpagRataTasse = new WpagRataTasse();
    //Original name: WPAG-RATA-SOPR-SANIT
    private WpagRataSoprSanit wpagRataSoprSanit = new WpagRataSoprSanit();
    //Original name: WPAG-RATA-SOPR-PROFES
    private WpagRataSoprProfes wpagRataSoprProfes = new WpagRataSoprProfes();
    //Original name: WPAG-RATA-SOPR-SPORT
    private WpagRataSoprSport wpagRataSoprSport = new WpagRataSoprSport();
    //Original name: WPAG-RATA-SOPR-TECN
    private WpagRataSoprTecn wpagRataSoprTecn = new WpagRataSoprTecn();
    //Original name: WPAG-RATA-ALTRI-SOPR
    private WpagRataAltriSopr wpagRataAltriSopr = new WpagRataAltriSopr();
    //Original name: WPAG-RATA-PREMIO-TOT
    private WpagRataPremioTot wpagRataPremioTot = new WpagRataPremioTot();
    //Original name: WPAG-RATA-PREMIO-PURO-IAS
    private WpagRataPremioPuroIas wpagRataPremioPuroIas = new WpagRataPremioPuroIas();
    //Original name: WPAG-RATA-PREMIO-RISC
    private WpagRataPremioRisc wpagRataPremioRisc = new WpagRataPremioRisc();
    //Original name: WPAG-IMP-CAR-ACQ-TDR
    private WpagImpCarAcqTdr wpagImpCarAcqTdr = new WpagImpCarAcqTdr();
    //Original name: WPAG-IMP-CAR-INC-TDR
    private WpagImpCarIncTdr wpagImpCarIncTdr = new WpagImpCarIncTdr();
    //Original name: WPAG-IMP-CAR-GEST-TDR
    private WpagImpCarGestTdr wpagImpCarGestTdr = new WpagImpCarGestTdr();
    //Original name: WPAG-RATA-ACQ-1O-ANNO
    private WpagRataAcq1oAnno wpagRataAcq1oAnno = new WpagRataAcq1oAnno();
    //Original name: WPAG-RATA-ACQ-2O-ANNO
    private WpagRataAcq2oAnno wpagRataAcq2oAnno = new WpagRataAcq2oAnno();
    //Original name: WPAG-RATA-RICOR
    private WpagRataRicor wpagRataRicor = new WpagRataRicor();
    //Original name: WPAG-RATA-INCAS
    private WpagRataIncas wpagRataIncas = new WpagRataIncas();
    //Original name: WPAG-IMP-AZ-TDR
    private WpagImpAzTdr wpagImpAzTdr = new WpagImpAzTdr();
    //Original name: WPAG-IMP-ADER-TDR
    private WpagImpAderTdr wpagImpAderTdr = new WpagImpAderTdr();
    //Original name: WPAG-IMP-TFR-TDR
    private WpagImpTfrTdr wpagImpTfrTdr = new WpagImpTfrTdr();
    //Original name: WPAG-IMP-VOLO-TDR
    private WpagImpVoloTdr wpagImpVoloTdr = new WpagImpVoloTdr();
    //Original name: WPAG-IMP-INT-RIATT
    private WpagImpIntRiatt wpagImpIntRiatt = new WpagImpIntRiatt();
    //Original name: WPAG-RATA-IMP-ACQ-EXP
    private WpagRataImpAcqExp wpagRataImpAcqExp = new WpagRataImpAcqExp();
    //Original name: WPAG-RATA-IMP-REN-ASS
    private WpagRataImpRenAss wpagRataImpRenAss = new WpagRataImpRenAss();
    //Original name: WPAG-RATA-IMP-COMMIS-INT
    private WpagRataImpCommisInt wpagRataImpCommisInt = new WpagRataImpCommisInt();
    //Original name: WPAG-RATA-ANTIRACKET
    private AfDecimal wpagRataAntiracket = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WPAG-RATA-AUTOGEN-INC
    private char wpagRataAutogenInc = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWpagDatiTitRataBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagRataPremioNetto.setWpagRataPremioNettoFromBuffer(buffer, position);
        position += WpagRataPremioNetto.Len.WPAG_RATA_PREMIO_NETTO;
        wpagRataIntFraz.setWpagRataIntFrazFromBuffer(buffer, position);
        position += WpagRataIntFraz.Len.WPAG_RATA_INT_FRAZ;
        wpagRataIntRetrodt.setWpagRataIntRetrodtFromBuffer(buffer, position);
        position += WpagRataIntRetrodt.Len.WPAG_RATA_INT_RETRODT;
        wpagRataDiritti.setWpagRataDirittiFromBuffer(buffer, position);
        position += WpagRataDiritti.Len.WPAG_RATA_DIRITTI;
        wpagRataSpeseMediche.setWpagRataSpeseMedicheFromBuffer(buffer, position);
        position += WpagRataSpeseMediche.Len.WPAG_RATA_SPESE_MEDICHE;
        wpagRataTasse.setWpagRataTasseFromBuffer(buffer, position);
        position += WpagRataTasse.Len.WPAG_RATA_TASSE;
        wpagRataSoprSanit.setWpagRataSoprSanitFromBuffer(buffer, position);
        position += WpagRataSoprSanit.Len.WPAG_RATA_SOPR_SANIT;
        wpagRataSoprProfes.setWpagRataSoprProfesFromBuffer(buffer, position);
        position += WpagRataSoprProfes.Len.WPAG_RATA_SOPR_PROFES;
        wpagRataSoprSport.setWpagRataSoprSportFromBuffer(buffer, position);
        position += WpagRataSoprSport.Len.WPAG_RATA_SOPR_SPORT;
        wpagRataSoprTecn.setWpagRataSoprTecnFromBuffer(buffer, position);
        position += WpagRataSoprTecn.Len.WPAG_RATA_SOPR_TECN;
        wpagRataAltriSopr.setWpagRataAltriSoprFromBuffer(buffer, position);
        position += WpagRataAltriSopr.Len.WPAG_RATA_ALTRI_SOPR;
        wpagRataPremioTot.setWpagRataPremioTotFromBuffer(buffer, position);
        position += WpagRataPremioTot.Len.WPAG_RATA_PREMIO_TOT;
        wpagRataPremioPuroIas.setWpagRataPremioPuroIasFromBuffer(buffer, position);
        position += WpagRataPremioPuroIas.Len.WPAG_RATA_PREMIO_PURO_IAS;
        wpagRataPremioRisc.setWpagRataPremioRiscFromBuffer(buffer, position);
        position += WpagRataPremioRisc.Len.WPAG_RATA_PREMIO_RISC;
        wpagImpCarAcqTdr.setWpagImpCarAcqTdrFromBuffer(buffer, position);
        position += WpagImpCarAcqTdr.Len.WPAG_IMP_CAR_ACQ_TDR;
        wpagImpCarIncTdr.setWpagImpCarIncTdrFromBuffer(buffer, position);
        position += WpagImpCarIncTdr.Len.WPAG_IMP_CAR_INC_TDR;
        wpagImpCarGestTdr.setWpagImpCarGestTdrFromBuffer(buffer, position);
        position += WpagImpCarGestTdr.Len.WPAG_IMP_CAR_GEST_TDR;
        wpagRataAcq1oAnno.setWpagRataAcq1oAnnoFromBuffer(buffer, position);
        position += WpagRataAcq1oAnno.Len.WPAG_RATA_ACQ1O_ANNO;
        wpagRataAcq2oAnno.setWpagRataAcq2oAnnoFromBuffer(buffer, position);
        position += WpagRataAcq2oAnno.Len.WPAG_RATA_ACQ2O_ANNO;
        wpagRataRicor.setWpagRataRicorFromBuffer(buffer, position);
        position += WpagRataRicor.Len.WPAG_RATA_RICOR;
        wpagRataIncas.setWpagRataIncasFromBuffer(buffer, position);
        position += WpagRataIncas.Len.WPAG_RATA_INCAS;
        wpagImpAzTdr.setWpagImpAzTdrFromBuffer(buffer, position);
        position += WpagImpAzTdr.Len.WPAG_IMP_AZ_TDR;
        wpagImpAderTdr.setWpagImpAderTdrFromBuffer(buffer, position);
        position += WpagImpAderTdr.Len.WPAG_IMP_ADER_TDR;
        wpagImpTfrTdr.setWpagImpTfrTdrFromBuffer(buffer, position);
        position += WpagImpTfrTdr.Len.WPAG_IMP_TFR_TDR;
        wpagImpVoloTdr.setWpagImpVoloTdrFromBuffer(buffer, position);
        position += WpagImpVoloTdr.Len.WPAG_IMP_VOLO_TDR;
        wpagImpIntRiatt.setWpagImpIntRiattFromBuffer(buffer, position);
        position += WpagImpIntRiatt.Len.WPAG_IMP_INT_RIATT;
        wpagRataImpAcqExp.setWpagRataImpAcqExpFromBuffer(buffer, position);
        position += WpagRataImpAcqExp.Len.WPAG_RATA_IMP_ACQ_EXP;
        wpagRataImpRenAss.setWpagRataImpRenAssFromBuffer(buffer, position);
        position += WpagRataImpRenAss.Len.WPAG_RATA_IMP_REN_ASS;
        wpagRataImpCommisInt.setWpagRataImpCommisIntFromBuffer(buffer, position);
        position += WpagRataImpCommisInt.Len.WPAG_RATA_IMP_COMMIS_INT;
        wpagRataAntiracket.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WPAG_RATA_ANTIRACKET, Len.Fract.WPAG_RATA_ANTIRACKET));
        position += Len.WPAG_RATA_ANTIRACKET;
        wpagRataAutogenInc = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWpagDatiTitRataBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagRataPremioNetto.getWpagRataPremioNettoAsBuffer(buffer, position);
        position += WpagRataPremioNetto.Len.WPAG_RATA_PREMIO_NETTO;
        wpagRataIntFraz.getWpagRataIntFrazAsBuffer(buffer, position);
        position += WpagRataIntFraz.Len.WPAG_RATA_INT_FRAZ;
        wpagRataIntRetrodt.getWpagRataIntRetrodtAsBuffer(buffer, position);
        position += WpagRataIntRetrodt.Len.WPAG_RATA_INT_RETRODT;
        wpagRataDiritti.getWpagRataDirittiAsBuffer(buffer, position);
        position += WpagRataDiritti.Len.WPAG_RATA_DIRITTI;
        wpagRataSpeseMediche.getWpagRataSpeseMedicheAsBuffer(buffer, position);
        position += WpagRataSpeseMediche.Len.WPAG_RATA_SPESE_MEDICHE;
        wpagRataTasse.getWpagRataTasseAsBuffer(buffer, position);
        position += WpagRataTasse.Len.WPAG_RATA_TASSE;
        wpagRataSoprSanit.getWpagRataSoprSanitAsBuffer(buffer, position);
        position += WpagRataSoprSanit.Len.WPAG_RATA_SOPR_SANIT;
        wpagRataSoprProfes.getWpagRataSoprProfesAsBuffer(buffer, position);
        position += WpagRataSoprProfes.Len.WPAG_RATA_SOPR_PROFES;
        wpagRataSoprSport.getWpagRataSoprSportAsBuffer(buffer, position);
        position += WpagRataSoprSport.Len.WPAG_RATA_SOPR_SPORT;
        wpagRataSoprTecn.getWpagRataSoprTecnAsBuffer(buffer, position);
        position += WpagRataSoprTecn.Len.WPAG_RATA_SOPR_TECN;
        wpagRataAltriSopr.getWpagRataAltriSoprAsBuffer(buffer, position);
        position += WpagRataAltriSopr.Len.WPAG_RATA_ALTRI_SOPR;
        wpagRataPremioTot.getWpagRataPremioTotAsBuffer(buffer, position);
        position += WpagRataPremioTot.Len.WPAG_RATA_PREMIO_TOT;
        wpagRataPremioPuroIas.getWpagRataPremioPuroIasAsBuffer(buffer, position);
        position += WpagRataPremioPuroIas.Len.WPAG_RATA_PREMIO_PURO_IAS;
        wpagRataPremioRisc.getWpagRataPremioRiscAsBuffer(buffer, position);
        position += WpagRataPremioRisc.Len.WPAG_RATA_PREMIO_RISC;
        wpagImpCarAcqTdr.getWpagImpCarAcqTdrAsBuffer(buffer, position);
        position += WpagImpCarAcqTdr.Len.WPAG_IMP_CAR_ACQ_TDR;
        wpagImpCarIncTdr.getWpagImpCarIncTdrAsBuffer(buffer, position);
        position += WpagImpCarIncTdr.Len.WPAG_IMP_CAR_INC_TDR;
        wpagImpCarGestTdr.getWpagImpCarGestTdrAsBuffer(buffer, position);
        position += WpagImpCarGestTdr.Len.WPAG_IMP_CAR_GEST_TDR;
        wpagRataAcq1oAnno.getWpagRataAcq1oAnnoAsBuffer(buffer, position);
        position += WpagRataAcq1oAnno.Len.WPAG_RATA_ACQ1O_ANNO;
        wpagRataAcq2oAnno.getWpagRataAcq2oAnnoAsBuffer(buffer, position);
        position += WpagRataAcq2oAnno.Len.WPAG_RATA_ACQ2O_ANNO;
        wpagRataRicor.getWpagRataRicorAsBuffer(buffer, position);
        position += WpagRataRicor.Len.WPAG_RATA_RICOR;
        wpagRataIncas.getWpagRataIncasAsBuffer(buffer, position);
        position += WpagRataIncas.Len.WPAG_RATA_INCAS;
        wpagImpAzTdr.getWpagImpAzTdrAsBuffer(buffer, position);
        position += WpagImpAzTdr.Len.WPAG_IMP_AZ_TDR;
        wpagImpAderTdr.getWpagImpAderTdrAsBuffer(buffer, position);
        position += WpagImpAderTdr.Len.WPAG_IMP_ADER_TDR;
        wpagImpTfrTdr.getWpagImpTfrTdrAsBuffer(buffer, position);
        position += WpagImpTfrTdr.Len.WPAG_IMP_TFR_TDR;
        wpagImpVoloTdr.getWpagImpVoloTdrAsBuffer(buffer, position);
        position += WpagImpVoloTdr.Len.WPAG_IMP_VOLO_TDR;
        wpagImpIntRiatt.getWpagImpIntRiattAsBuffer(buffer, position);
        position += WpagImpIntRiatt.Len.WPAG_IMP_INT_RIATT;
        wpagRataImpAcqExp.getWpagRataImpAcqExpAsBuffer(buffer, position);
        position += WpagRataImpAcqExp.Len.WPAG_RATA_IMP_ACQ_EXP;
        wpagRataImpRenAss.getWpagRataImpRenAssAsBuffer(buffer, position);
        position += WpagRataImpRenAss.Len.WPAG_RATA_IMP_REN_ASS;
        wpagRataImpCommisInt.getWpagRataImpCommisIntAsBuffer(buffer, position);
        position += WpagRataImpCommisInt.Len.WPAG_RATA_IMP_COMMIS_INT;
        MarshalByte.writeDecimalAsPacked(buffer, position, wpagRataAntiracket.copy());
        position += Len.WPAG_RATA_ANTIRACKET;
        MarshalByte.writeChar(buffer, position, wpagRataAutogenInc);
        return buffer;
    }

    public void initWpagDatiTitRataSpaces() {
        wpagRataPremioNetto.initWpagRataPremioNettoSpaces();
        wpagRataIntFraz.initWpagRataIntFrazSpaces();
        wpagRataIntRetrodt.initWpagRataIntRetrodtSpaces();
        wpagRataDiritti.initWpagRataDirittiSpaces();
        wpagRataSpeseMediche.initWpagRataSpeseMedicheSpaces();
        wpagRataTasse.initWpagRataTasseSpaces();
        wpagRataSoprSanit.initWpagRataSoprSanitSpaces();
        wpagRataSoprProfes.initWpagRataSoprProfesSpaces();
        wpagRataSoprSport.initWpagRataSoprSportSpaces();
        wpagRataSoprTecn.initWpagRataSoprTecnSpaces();
        wpagRataAltriSopr.initWpagRataAltriSoprSpaces();
        wpagRataPremioTot.initWpagRataPremioTotSpaces();
        wpagRataPremioPuroIas.initWpagRataPremioPuroIasSpaces();
        wpagRataPremioRisc.initWpagRataPremioRiscSpaces();
        wpagImpCarAcqTdr.initWpagImpCarAcqTdrSpaces();
        wpagImpCarIncTdr.initWpagImpCarIncTdrSpaces();
        wpagImpCarGestTdr.initWpagImpCarGestTdrSpaces();
        wpagRataAcq1oAnno.initWpagRataAcq1oAnnoSpaces();
        wpagRataAcq2oAnno.initWpagRataAcq2oAnnoSpaces();
        wpagRataRicor.initWpagRataRicorSpaces();
        wpagRataIncas.initWpagRataIncasSpaces();
        wpagImpAzTdr.initWpagImpAzTdrSpaces();
        wpagImpAderTdr.initWpagImpAderTdrSpaces();
        wpagImpTfrTdr.initWpagImpTfrTdrSpaces();
        wpagImpVoloTdr.initWpagImpVoloTdrSpaces();
        wpagImpIntRiatt.initWpagImpIntRiattSpaces();
        wpagRataImpAcqExp.initWpagRataImpAcqExpSpaces();
        wpagRataImpRenAss.initWpagRataImpRenAssSpaces();
        wpagRataImpCommisInt.initWpagRataImpCommisIntSpaces();
        wpagRataAntiracket.setNaN();
        wpagRataAutogenInc = Types.SPACE_CHAR;
    }

    public void setWpagRataAntiracket(AfDecimal wpagRataAntiracket) {
        this.wpagRataAntiracket.assign(wpagRataAntiracket);
    }

    public void setWpagRataAntiracketFormatted(String wpagRataAntiracket) {
        setWpagRataAntiracket(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_ANTIRACKET + Len.Fract.WPAG_RATA_ANTIRACKET, Len.Fract.WPAG_RATA_ANTIRACKET, wpagRataAntiracket));
    }

    public AfDecimal getWpagRataAntiracket() {
        return this.wpagRataAntiracket.copy();
    }

    public void setWpagRataAutogenInc(char wpagRataAutogenInc) {
        this.wpagRataAutogenInc = wpagRataAutogenInc;
    }

    public char getWpagRataAutogenInc() {
        return this.wpagRataAutogenInc;
    }

    public WpagImpAderTdr getWpagImpAderTdr() {
        return wpagImpAderTdr;
    }

    public WpagImpAzTdr getWpagImpAzTdr() {
        return wpagImpAzTdr;
    }

    public WpagImpCarAcqTdr getWpagImpCarAcqTdr() {
        return wpagImpCarAcqTdr;
    }

    public WpagImpCarGestTdr getWpagImpCarGestTdr() {
        return wpagImpCarGestTdr;
    }

    public WpagImpCarIncTdr getWpagImpCarIncTdr() {
        return wpagImpCarIncTdr;
    }

    public WpagImpIntRiatt getWpagImpIntRiatt() {
        return wpagImpIntRiatt;
    }

    public WpagImpTfrTdr getWpagImpTfrTdr() {
        return wpagImpTfrTdr;
    }

    public WpagImpVoloTdr getWpagImpVoloTdr() {
        return wpagImpVoloTdr;
    }

    public WpagRataAcq1oAnno getWpagRataAcq1oAnno() {
        return wpagRataAcq1oAnno;
    }

    public WpagRataAcq2oAnno getWpagRataAcq2oAnno() {
        return wpagRataAcq2oAnno;
    }

    public WpagRataAltriSopr getWpagRataAltriSopr() {
        return wpagRataAltriSopr;
    }

    public WpagRataDiritti getWpagRataDiritti() {
        return wpagRataDiritti;
    }

    public WpagRataImpAcqExp getWpagRataImpAcqExp() {
        return wpagRataImpAcqExp;
    }

    public WpagRataImpCommisInt getWpagRataImpCommisInt() {
        return wpagRataImpCommisInt;
    }

    public WpagRataImpRenAss getWpagRataImpRenAss() {
        return wpagRataImpRenAss;
    }

    public WpagRataIncas getWpagRataIncas() {
        return wpagRataIncas;
    }

    public WpagRataIntFraz getWpagRataIntFraz() {
        return wpagRataIntFraz;
    }

    public WpagRataIntRetrodt getWpagRataIntRetrodt() {
        return wpagRataIntRetrodt;
    }

    public WpagRataPremioNetto getWpagRataPremioNetto() {
        return wpagRataPremioNetto;
    }

    public WpagRataPremioPuroIas getWpagRataPremioPuroIas() {
        return wpagRataPremioPuroIas;
    }

    public WpagRataPremioRisc getWpagRataPremioRisc() {
        return wpagRataPremioRisc;
    }

    public WpagRataPremioTot getWpagRataPremioTot() {
        return wpagRataPremioTot;
    }

    public WpagRataRicor getWpagRataRicor() {
        return wpagRataRicor;
    }

    public WpagRataSoprProfes getWpagRataSoprProfes() {
        return wpagRataSoprProfes;
    }

    public WpagRataSoprSanit getWpagRataSoprSanit() {
        return wpagRataSoprSanit;
    }

    public WpagRataSoprSport getWpagRataSoprSport() {
        return wpagRataSoprSport;
    }

    public WpagRataSoprTecn getWpagRataSoprTecn() {
        return wpagRataSoprTecn;
    }

    public WpagRataSpeseMediche getWpagRataSpeseMediche() {
        return wpagRataSpeseMediche;
    }

    public WpagRataTasse getWpagRataTasse() {
        return wpagRataTasse;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_ANTIRACKET = 8;
        public static final int WPAG_RATA_AUTOGEN_INC = 1;
        public static final int WPAG_DATI_TIT_RATA = WpagRataPremioNetto.Len.WPAG_RATA_PREMIO_NETTO + WpagRataIntFraz.Len.WPAG_RATA_INT_FRAZ + WpagRataIntRetrodt.Len.WPAG_RATA_INT_RETRODT + WpagRataDiritti.Len.WPAG_RATA_DIRITTI + WpagRataSpeseMediche.Len.WPAG_RATA_SPESE_MEDICHE + WpagRataTasse.Len.WPAG_RATA_TASSE + WpagRataSoprSanit.Len.WPAG_RATA_SOPR_SANIT + WpagRataSoprProfes.Len.WPAG_RATA_SOPR_PROFES + WpagRataSoprSport.Len.WPAG_RATA_SOPR_SPORT + WpagRataSoprTecn.Len.WPAG_RATA_SOPR_TECN + WpagRataAltriSopr.Len.WPAG_RATA_ALTRI_SOPR + WpagRataPremioTot.Len.WPAG_RATA_PREMIO_TOT + WpagRataPremioPuroIas.Len.WPAG_RATA_PREMIO_PURO_IAS + WpagRataPremioRisc.Len.WPAG_RATA_PREMIO_RISC + WpagImpCarAcqTdr.Len.WPAG_IMP_CAR_ACQ_TDR + WpagImpCarIncTdr.Len.WPAG_IMP_CAR_INC_TDR + WpagImpCarGestTdr.Len.WPAG_IMP_CAR_GEST_TDR + WpagRataAcq1oAnno.Len.WPAG_RATA_ACQ1O_ANNO + WpagRataAcq2oAnno.Len.WPAG_RATA_ACQ2O_ANNO + WpagRataRicor.Len.WPAG_RATA_RICOR + WpagRataIncas.Len.WPAG_RATA_INCAS + WpagImpAzTdr.Len.WPAG_IMP_AZ_TDR + WpagImpAderTdr.Len.WPAG_IMP_ADER_TDR + WpagImpTfrTdr.Len.WPAG_IMP_TFR_TDR + WpagImpVoloTdr.Len.WPAG_IMP_VOLO_TDR + WpagImpIntRiatt.Len.WPAG_IMP_INT_RIATT + WpagRataImpAcqExp.Len.WPAG_RATA_IMP_ACQ_EXP + WpagRataImpRenAss.Len.WPAG_RATA_IMP_REN_ASS + WpagRataImpCommisInt.Len.WPAG_RATA_IMP_COMMIS_INT + WPAG_RATA_ANTIRACKET + WPAG_RATA_AUTOGEN_INC;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ANTIRACKET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_ANTIRACKET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
