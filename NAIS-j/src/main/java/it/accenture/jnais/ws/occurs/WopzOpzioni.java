package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WOPZ-OPZIONI<br>
 * Variables: WOPZ-OPZIONI from copybook IVVC0217<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WopzOpzioni {

    //==== PROPERTIES ====
    public static final int GAR_POLIZZA_MAXOCCURS = 20;
    public static final int GAR_OPZIONE_MAXOCCURS = 20;
    //Original name: WOPZ-COD-TIPO-OPZIONE
    private String codTipoOpzione = DefaultValues.stringVal(Len.COD_TIPO_OPZIONE);
    //Original name: WOPZ-DESC-TIPO-OPZIONE
    private String descTipoOpzione = DefaultValues.stringVal(Len.DESC_TIPO_OPZIONE);
    //Original name: WOPZ-NUM-GAR-POL-MAX-ELE
    private String numGarPolMaxEle = DefaultValues.stringVal(Len.NUM_GAR_POL_MAX_ELE);
    //Original name: WOPZ-GAR-POLIZZA
    private WopzGarPolizza[] garPolizza = new WopzGarPolizza[GAR_POLIZZA_MAXOCCURS];
    //Original name: WOPZ-NUM-GAR-OPZ-MAX-ELE
    private String numGarOpzMaxEle = DefaultValues.stringVal(Len.NUM_GAR_OPZ_MAX_ELE);
    //Original name: WOPZ-GAR-OPZIONE
    private WopzGarOpzione[] garOpzione = new WopzGarOpzione[GAR_OPZIONE_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WopzOpzioni() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int garPolizzaIdx = 1; garPolizzaIdx <= GAR_POLIZZA_MAXOCCURS; garPolizzaIdx++) {
            garPolizza[garPolizzaIdx - 1] = new WopzGarPolizza();
        }
        for (int garOpzioneIdx = 1; garOpzioneIdx <= GAR_OPZIONE_MAXOCCURS; garOpzioneIdx++) {
            garOpzione[garOpzioneIdx - 1] = new WopzGarOpzione();
        }
    }

    public void setOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        codTipoOpzione = MarshalByte.readString(buffer, position, Len.COD_TIPO_OPZIONE);
        position += Len.COD_TIPO_OPZIONE;
        descTipoOpzione = MarshalByte.readString(buffer, position, Len.DESC_TIPO_OPZIONE);
        position += Len.DESC_TIPO_OPZIONE;
        numGarPolMaxEle = MarshalByte.readFixedString(buffer, position, Len.NUM_GAR_POL_MAX_ELE);
        position += Len.NUM_GAR_POL_MAX_ELE;
        for (int idx = 1; idx <= GAR_POLIZZA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                garPolizza[idx - 1].setGarPolizzaBytes(buffer, position);
                position += WopzGarPolizza.Len.GAR_POLIZZA;
            }
            else {
                garPolizza[idx - 1].initGarPolizzaSpaces();
                position += WopzGarPolizza.Len.GAR_POLIZZA;
            }
        }
        numGarOpzMaxEle = MarshalByte.readFixedString(buffer, position, Len.NUM_GAR_OPZ_MAX_ELE);
        position += Len.NUM_GAR_OPZ_MAX_ELE;
        for (int idx = 1; idx <= GAR_OPZIONE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                garOpzione[idx - 1].setGarOpzioneBytes(buffer, position);
                position += WopzGarOpzione.Len.GAR_OPZIONE;
            }
            else {
                garOpzione[idx - 1].initGarOpzioneSpaces();
                position += WopzGarOpzione.Len.GAR_OPZIONE;
            }
        }
    }

    public byte[] getOpzioniBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codTipoOpzione, Len.COD_TIPO_OPZIONE);
        position += Len.COD_TIPO_OPZIONE;
        MarshalByte.writeString(buffer, position, descTipoOpzione, Len.DESC_TIPO_OPZIONE);
        position += Len.DESC_TIPO_OPZIONE;
        MarshalByte.writeString(buffer, position, numGarPolMaxEle, Len.NUM_GAR_POL_MAX_ELE);
        position += Len.NUM_GAR_POL_MAX_ELE;
        for (int idx = 1; idx <= GAR_POLIZZA_MAXOCCURS; idx++) {
            garPolizza[idx - 1].getGarPolizzaBytes(buffer, position);
            position += WopzGarPolizza.Len.GAR_POLIZZA;
        }
        MarshalByte.writeString(buffer, position, numGarOpzMaxEle, Len.NUM_GAR_OPZ_MAX_ELE);
        position += Len.NUM_GAR_OPZ_MAX_ELE;
        for (int idx = 1; idx <= GAR_OPZIONE_MAXOCCURS; idx++) {
            garOpzione[idx - 1].getGarOpzioneBytes(buffer, position);
            position += WopzGarOpzione.Len.GAR_OPZIONE;
        }
        return buffer;
    }

    public void initOpzioniSpaces() {
        codTipoOpzione = "";
        descTipoOpzione = "";
        numGarPolMaxEle = "";
        for (int idx = 1; idx <= GAR_POLIZZA_MAXOCCURS; idx++) {
            garPolizza[idx - 1].initGarPolizzaSpaces();
        }
        numGarOpzMaxEle = "";
        for (int idx = 1; idx <= GAR_OPZIONE_MAXOCCURS; idx++) {
            garOpzione[idx - 1].initGarOpzioneSpaces();
        }
    }

    public void setCodTipoOpzione(String codTipoOpzione) {
        this.codTipoOpzione = Functions.subString(codTipoOpzione, Len.COD_TIPO_OPZIONE);
    }

    public String getCodTipoOpzione() {
        return this.codTipoOpzione;
    }

    public String getWopzCodTipoOpzioneFormatted() {
        return Functions.padBlanks(getCodTipoOpzione(), Len.COD_TIPO_OPZIONE);
    }

    public void setDescTipoOpzione(String descTipoOpzione) {
        this.descTipoOpzione = Functions.subString(descTipoOpzione, Len.DESC_TIPO_OPZIONE);
    }

    public String getDescTipoOpzione() {
        return this.descTipoOpzione;
    }

    public void setWopzNumGarPolMaxEleFormatted(String wopzNumGarPolMaxEle) {
        this.numGarPolMaxEle = Trunc.toUnsignedNumeric(wopzNumGarPolMaxEle, Len.NUM_GAR_POL_MAX_ELE);
    }

    public short getWopzNumGarPolMaxEle() {
        return NumericDisplay.asShort(this.numGarPolMaxEle);
    }

    public void setWopzNumGarOpzMaxEleFormatted(String wopzNumGarOpzMaxEle) {
        this.numGarOpzMaxEle = Trunc.toUnsignedNumeric(wopzNumGarOpzMaxEle, Len.NUM_GAR_OPZ_MAX_ELE);
    }

    public short getWopzNumGarOpzMaxEle() {
        return NumericDisplay.asShort(this.numGarOpzMaxEle);
    }

    public WopzGarOpzione getGarOpzione(int idx) {
        return garOpzione[idx - 1];
    }

    public WopzGarPolizza getGarPolizza(int idx) {
        return garPolizza[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_TIPO_OPZIONE = 2;
        public static final int DESC_TIPO_OPZIONE = 30;
        public static final int NUM_GAR_POL_MAX_ELE = 3;
        public static final int NUM_GAR_OPZ_MAX_ELE = 3;
        public static final int OPZIONI = COD_TIPO_OPZIONE + DESC_TIPO_OPZIONE + NUM_GAR_POL_MAX_ELE + WopzOpzioni.GAR_POLIZZA_MAXOCCURS * WopzGarPolizza.Len.GAR_POLIZZA + NUM_GAR_OPZ_MAX_ELE + WopzOpzioni.GAR_OPZIONE_MAXOCCURS * WopzGarOpzione.Len.GAR_OPZIONE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
