package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTGZ-TRCH-E-CL<br>
 * Variable: PCO-DT-ULTGZ-TRCH-E-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltgzTrchECl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltgzTrchECl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTGZ_TRCH_E_CL;
    }

    public void setPcoDtUltgzTrchECl(int pcoDtUltgzTrchECl) {
        writeIntAsPacked(Pos.PCO_DT_ULTGZ_TRCH_E_CL, pcoDtUltgzTrchECl, Len.Int.PCO_DT_ULTGZ_TRCH_E_CL);
    }

    public void setPcoDtUltgzTrchEClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTGZ_TRCH_E_CL, Pos.PCO_DT_ULTGZ_TRCH_E_CL);
    }

    /**Original name: PCO-DT-ULTGZ-TRCH-E-CL<br>*/
    public int getPcoDtUltgzTrchECl() {
        return readPackedAsInt(Pos.PCO_DT_ULTGZ_TRCH_E_CL, Len.Int.PCO_DT_ULTGZ_TRCH_E_CL);
    }

    public byte[] getPcoDtUltgzTrchEClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTGZ_TRCH_E_CL, Pos.PCO_DT_ULTGZ_TRCH_E_CL);
        return buffer;
    }

    public void initPcoDtUltgzTrchEClHighValues() {
        fill(Pos.PCO_DT_ULTGZ_TRCH_E_CL, Len.PCO_DT_ULTGZ_TRCH_E_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltgzTrchEClNull(String pcoDtUltgzTrchEClNull) {
        writeString(Pos.PCO_DT_ULTGZ_TRCH_E_CL_NULL, pcoDtUltgzTrchEClNull, Len.PCO_DT_ULTGZ_TRCH_E_CL_NULL);
    }

    /**Original name: PCO-DT-ULTGZ-TRCH-E-CL-NULL<br>*/
    public String getPcoDtUltgzTrchEClNull() {
        return readString(Pos.PCO_DT_ULTGZ_TRCH_E_CL_NULL, Len.PCO_DT_ULTGZ_TRCH_E_CL_NULL);
    }

    public String getPcoDtUltgzTrchEClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltgzTrchEClNull(), Len.PCO_DT_ULTGZ_TRCH_E_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_TRCH_E_CL = 1;
        public static final int PCO_DT_ULTGZ_TRCH_E_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTGZ_TRCH_E_CL = 5;
        public static final int PCO_DT_ULTGZ_TRCH_E_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTGZ_TRCH_E_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
