package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-ID-TROVATO<br>
 * Variable: FLAG-ID-TROVATO from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagIdTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagIdTrovato(char flagIdTrovato) {
        this.value = flagIdTrovato;
    }

    public char getFlagIdTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
