package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-GAR-RAMO3-STORNATA<br>
 * Variable: FLAG-GAR-RAMO3-STORNATA from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagGarRamo3Stornata {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagGarRamo3Stornata(char flagGarRamo3Stornata) {
        this.value = flagGarRamo3Stornata;
    }

    public char getFlagGarRamo3Stornata() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
