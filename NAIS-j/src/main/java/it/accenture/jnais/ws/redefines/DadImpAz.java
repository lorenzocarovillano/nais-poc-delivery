package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-IMP-AZ<br>
 * Variable: DAD-IMP-AZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_IMP_AZ;
    }

    public void setDadImpAz(AfDecimal dadImpAz) {
        writeDecimalAsPacked(Pos.DAD_IMP_AZ, dadImpAz.copy());
    }

    public void setDadImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_IMP_AZ, Pos.DAD_IMP_AZ);
    }

    /**Original name: DAD-IMP-AZ<br>*/
    public AfDecimal getDadImpAz() {
        return readPackedAsDecimal(Pos.DAD_IMP_AZ, Len.Int.DAD_IMP_AZ, Len.Fract.DAD_IMP_AZ);
    }

    public byte[] getDadImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_IMP_AZ, Pos.DAD_IMP_AZ);
        return buffer;
    }

    public void setDadImpAzNull(String dadImpAzNull) {
        writeString(Pos.DAD_IMP_AZ_NULL, dadImpAzNull, Len.DAD_IMP_AZ_NULL);
    }

    /**Original name: DAD-IMP-AZ-NULL<br>*/
    public String getDadImpAzNull() {
        return readString(Pos.DAD_IMP_AZ_NULL, Len.DAD_IMP_AZ_NULL);
    }

    public String getDadImpAzNullFormatted() {
        return Functions.padBlanks(getDadImpAzNull(), Len.DAD_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_IMP_AZ = 1;
        public static final int DAD_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IMP_AZ = 8;
        public static final int DAD_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
