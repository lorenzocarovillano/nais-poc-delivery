package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DUR-ABB<br>
 * Variable: TGA-DUR-ABB from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDurAbb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDurAbb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DUR_ABB;
    }

    public void setTgaDurAbb(int tgaDurAbb) {
        writeIntAsPacked(Pos.TGA_DUR_ABB, tgaDurAbb, Len.Int.TGA_DUR_ABB);
    }

    public void setTgaDurAbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DUR_ABB, Pos.TGA_DUR_ABB);
    }

    /**Original name: TGA-DUR-ABB<br>*/
    public int getTgaDurAbb() {
        return readPackedAsInt(Pos.TGA_DUR_ABB, Len.Int.TGA_DUR_ABB);
    }

    public byte[] getTgaDurAbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DUR_ABB, Pos.TGA_DUR_ABB);
        return buffer;
    }

    public void setTgaDurAbbNull(String tgaDurAbbNull) {
        writeString(Pos.TGA_DUR_ABB_NULL, tgaDurAbbNull, Len.TGA_DUR_ABB_NULL);
    }

    /**Original name: TGA-DUR-ABB-NULL<br>*/
    public String getTgaDurAbbNull() {
        return readString(Pos.TGA_DUR_ABB_NULL, Len.TGA_DUR_ABB_NULL);
    }

    public String getTgaDurAbbNullFormatted() {
        return Functions.padBlanks(getTgaDurAbbNull(), Len.TGA_DUR_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DUR_ABB = 1;
        public static final int TGA_DUR_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DUR_ABB = 4;
        public static final int TGA_DUR_ABB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DUR_ABB = 6;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
