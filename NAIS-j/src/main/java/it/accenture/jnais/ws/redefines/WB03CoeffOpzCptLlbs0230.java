package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-COEFF-OPZ-CPT<br>
 * Variable: W-B03-COEFF-OPZ-CPT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CoeffOpzCptLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CoeffOpzCptLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COEFF_OPZ_CPT;
    }

    public void setwB03CoeffOpzCpt(AfDecimal wB03CoeffOpzCpt) {
        writeDecimalAsPacked(Pos.W_B03_COEFF_OPZ_CPT, wB03CoeffOpzCpt.copy());
    }

    /**Original name: W-B03-COEFF-OPZ-CPT<br>*/
    public AfDecimal getwB03CoeffOpzCpt() {
        return readPackedAsDecimal(Pos.W_B03_COEFF_OPZ_CPT, Len.Int.W_B03_COEFF_OPZ_CPT, Len.Fract.W_B03_COEFF_OPZ_CPT);
    }

    public byte[] getwB03CoeffOpzCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COEFF_OPZ_CPT, Pos.W_B03_COEFF_OPZ_CPT);
        return buffer;
    }

    public void setwB03CoeffOpzCptNull(String wB03CoeffOpzCptNull) {
        writeString(Pos.W_B03_COEFF_OPZ_CPT_NULL, wB03CoeffOpzCptNull, Len.W_B03_COEFF_OPZ_CPT_NULL);
    }

    /**Original name: W-B03-COEFF-OPZ-CPT-NULL<br>*/
    public String getwB03CoeffOpzCptNull() {
        return readString(Pos.W_B03_COEFF_OPZ_CPT_NULL, Len.W_B03_COEFF_OPZ_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_OPZ_CPT = 1;
        public static final int W_B03_COEFF_OPZ_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_OPZ_CPT = 4;
        public static final int W_B03_COEFF_OPZ_CPT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_OPZ_CPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_OPZ_CPT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
