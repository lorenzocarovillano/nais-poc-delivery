package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ALQ-PROV-ACQ<br>
 * Variable: PVT-ALQ-PROV-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtAlqProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtAlqProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ALQ_PROV_ACQ;
    }

    public void setPvtAlqProvAcq(AfDecimal pvtAlqProvAcq) {
        writeDecimalAsPacked(Pos.PVT_ALQ_PROV_ACQ, pvtAlqProvAcq.copy());
    }

    public void setPvtAlqProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ALQ_PROV_ACQ, Pos.PVT_ALQ_PROV_ACQ);
    }

    /**Original name: PVT-ALQ-PROV-ACQ<br>*/
    public AfDecimal getPvtAlqProvAcq() {
        return readPackedAsDecimal(Pos.PVT_ALQ_PROV_ACQ, Len.Int.PVT_ALQ_PROV_ACQ, Len.Fract.PVT_ALQ_PROV_ACQ);
    }

    public byte[] getPvtAlqProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ALQ_PROV_ACQ, Pos.PVT_ALQ_PROV_ACQ);
        return buffer;
    }

    public void setPvtAlqProvAcqNull(String pvtAlqProvAcqNull) {
        writeString(Pos.PVT_ALQ_PROV_ACQ_NULL, pvtAlqProvAcqNull, Len.PVT_ALQ_PROV_ACQ_NULL);
    }

    /**Original name: PVT-ALQ-PROV-ACQ-NULL<br>*/
    public String getPvtAlqProvAcqNull() {
        return readString(Pos.PVT_ALQ_PROV_ACQ_NULL, Len.PVT_ALQ_PROV_ACQ_NULL);
    }

    public String getPvtAlqProvAcqNullFormatted() {
        return Functions.padBlanks(getPvtAlqProvAcqNull(), Len.PVT_ALQ_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_PROV_ACQ = 1;
        public static final int PVT_ALQ_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_PROV_ACQ = 4;
        public static final int PVT_ALQ_PROV_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
