package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-ALQ-PROV-RICOR<br>
 * Variable: WPVT-ALQ-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtAlqProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtAlqProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ALQ_PROV_RICOR;
    }

    public void setWpvtAlqProvRicor(AfDecimal wpvtAlqProvRicor) {
        writeDecimalAsPacked(Pos.WPVT_ALQ_PROV_RICOR, wpvtAlqProvRicor.copy());
    }

    public void setWpvtAlqProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ALQ_PROV_RICOR, Pos.WPVT_ALQ_PROV_RICOR);
    }

    /**Original name: WPVT-ALQ-PROV-RICOR<br>*/
    public AfDecimal getWpvtAlqProvRicor() {
        return readPackedAsDecimal(Pos.WPVT_ALQ_PROV_RICOR, Len.Int.WPVT_ALQ_PROV_RICOR, Len.Fract.WPVT_ALQ_PROV_RICOR);
    }

    public byte[] getWpvtAlqProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ALQ_PROV_RICOR, Pos.WPVT_ALQ_PROV_RICOR);
        return buffer;
    }

    public void initWpvtAlqProvRicorSpaces() {
        fill(Pos.WPVT_ALQ_PROV_RICOR, Len.WPVT_ALQ_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWpvtAlqProvRicorNull(String wpvtAlqProvRicorNull) {
        writeString(Pos.WPVT_ALQ_PROV_RICOR_NULL, wpvtAlqProvRicorNull, Len.WPVT_ALQ_PROV_RICOR_NULL);
    }

    /**Original name: WPVT-ALQ-PROV-RICOR-NULL<br>*/
    public String getWpvtAlqProvRicorNull() {
        return readString(Pos.WPVT_ALQ_PROV_RICOR_NULL, Len.WPVT_ALQ_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_PROV_RICOR = 1;
        public static final int WPVT_ALQ_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_PROV_RICOR = 4;
        public static final int WPVT_ALQ_PROV_RICOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
