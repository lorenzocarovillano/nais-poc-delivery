package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-MAT-EFF<br>
 * Variable: WRST-RIS-MAT-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisMatEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisMatEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_MAT_EFF;
    }

    public void setWrstRisMatEff(AfDecimal wrstRisMatEff) {
        writeDecimalAsPacked(Pos.WRST_RIS_MAT_EFF, wrstRisMatEff.copy());
    }

    public void setWrstRisMatEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_MAT_EFF, Pos.WRST_RIS_MAT_EFF);
    }

    /**Original name: WRST-RIS-MAT-EFF<br>*/
    public AfDecimal getWrstRisMatEff() {
        return readPackedAsDecimal(Pos.WRST_RIS_MAT_EFF, Len.Int.WRST_RIS_MAT_EFF, Len.Fract.WRST_RIS_MAT_EFF);
    }

    public byte[] getWrstRisMatEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_MAT_EFF, Pos.WRST_RIS_MAT_EFF);
        return buffer;
    }

    public void initWrstRisMatEffSpaces() {
        fill(Pos.WRST_RIS_MAT_EFF, Len.WRST_RIS_MAT_EFF, Types.SPACE_CHAR);
    }

    public void setWrstRisMatEffNull(String wrstRisMatEffNull) {
        writeString(Pos.WRST_RIS_MAT_EFF_NULL, wrstRisMatEffNull, Len.WRST_RIS_MAT_EFF_NULL);
    }

    /**Original name: WRST-RIS-MAT-EFF-NULL<br>*/
    public String getWrstRisMatEffNull() {
        return readString(Pos.WRST_RIS_MAT_EFF_NULL, Len.WRST_RIS_MAT_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MAT_EFF = 1;
        public static final int WRST_RIS_MAT_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_MAT_EFF = 8;
        public static final int WRST_RIS_MAT_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MAT_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_MAT_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
