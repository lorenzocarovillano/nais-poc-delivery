package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ACQ-EXP<br>
 * Variable: WTGA-ACQ-EXP from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ACQ_EXP;
    }

    public void setWtgaAcqExp(AfDecimal wtgaAcqExp) {
        writeDecimalAsPacked(Pos.WTGA_ACQ_EXP, wtgaAcqExp.copy());
    }

    public void setWtgaAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ACQ_EXP, Pos.WTGA_ACQ_EXP);
    }

    /**Original name: WTGA-ACQ-EXP<br>*/
    public AfDecimal getWtgaAcqExp() {
        return readPackedAsDecimal(Pos.WTGA_ACQ_EXP, Len.Int.WTGA_ACQ_EXP, Len.Fract.WTGA_ACQ_EXP);
    }

    public byte[] getWtgaAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ACQ_EXP, Pos.WTGA_ACQ_EXP);
        return buffer;
    }

    public void initWtgaAcqExpSpaces() {
        fill(Pos.WTGA_ACQ_EXP, Len.WTGA_ACQ_EXP, Types.SPACE_CHAR);
    }

    public void setWtgaAcqExpNull(String wtgaAcqExpNull) {
        writeString(Pos.WTGA_ACQ_EXP_NULL, wtgaAcqExpNull, Len.WTGA_ACQ_EXP_NULL);
    }

    /**Original name: WTGA-ACQ-EXP-NULL<br>*/
    public String getWtgaAcqExpNull() {
        return readString(Pos.WTGA_ACQ_EXP_NULL, Len.WTGA_ACQ_EXP_NULL);
    }

    public String getWtgaAcqExpNullFormatted() {
        return Functions.padBlanks(getWtgaAcqExpNull(), Len.WTGA_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ACQ_EXP = 1;
        public static final int WTGA_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ACQ_EXP = 8;
        public static final int WTGA_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
