package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-PREST-RES-EFF<br>
 * Variable: PRE-PREST-RES-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PrePrestResEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PrePrestResEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_PREST_RES_EFF;
    }

    public void setPrePrestResEff(AfDecimal prePrestResEff) {
        writeDecimalAsPacked(Pos.PRE_PREST_RES_EFF, prePrestResEff.copy());
    }

    public void setPrePrestResEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_PREST_RES_EFF, Pos.PRE_PREST_RES_EFF);
    }

    /**Original name: PRE-PREST-RES-EFF<br>*/
    public AfDecimal getPrePrestResEff() {
        return readPackedAsDecimal(Pos.PRE_PREST_RES_EFF, Len.Int.PRE_PREST_RES_EFF, Len.Fract.PRE_PREST_RES_EFF);
    }

    public byte[] getPrePrestResEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_PREST_RES_EFF, Pos.PRE_PREST_RES_EFF);
        return buffer;
    }

    public void setPrePrestResEffNull(String prePrestResEffNull) {
        writeString(Pos.PRE_PREST_RES_EFF_NULL, prePrestResEffNull, Len.PRE_PREST_RES_EFF_NULL);
    }

    /**Original name: PRE-PREST-RES-EFF-NULL<br>*/
    public String getPrePrestResEffNull() {
        return readString(Pos.PRE_PREST_RES_EFF_NULL, Len.PRE_PREST_RES_EFF_NULL);
    }

    public String getPrePrestResEffNullFormatted() {
        return Functions.padBlanks(getPrePrestResEffNull(), Len.PRE_PREST_RES_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_PREST_RES_EFF = 1;
        public static final int PRE_PREST_RES_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_PREST_RES_EFF = 8;
        public static final int PRE_PREST_RES_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_PREST_RES_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_PREST_RES_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
