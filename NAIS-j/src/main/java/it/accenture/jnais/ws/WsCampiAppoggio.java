package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-CAMPI-APPOGGIO<br>
 * Variable: WS-CAMPI-APPOGGIO from program IDSS0140<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class WsCampiAppoggio {

    //==== PROPERTIES ====
    //Original name: PARTE-INTERA-MITT
    private String parteInteraMitt = DefaultValues.stringVal(Len.PARTE_INTERA_MITT);
    //Original name: PARTE-INTERA-DEST
    private String parteInteraDest = DefaultValues.stringVal(Len.PARTE_INTERA_DEST);
    //Original name: WS-NUM-DI-1
    private String wsNumDi1 = DefaultValues.stringVal(Len.WS_NUM_DI1);
    //Original name: WS-NUM-DI-3
    private String wsNumDi3 = DefaultValues.stringVal(Len.WS_NUM_DI3);
    //Original name: WS-NUM-DI-5
    private String wsNumDi5 = DefaultValues.stringVal(Len.WS_NUM_DI5);
    //Original name: WS-NUM-DI-8
    private String wsNumDi8 = DefaultValues.stringVal(Len.WS_NUM_DI8);
    //Original name: WS-NUM-DI-9
    private String wsNumDi9 = DefaultValues.stringVal(Len.WS_NUM_DI9);

    //==== METHODS ====
    public void setParteInteraMitt(short parteInteraMitt) {
        this.parteInteraMitt = NumericDisplay.asString(parteInteraMitt, Len.PARTE_INTERA_MITT);
    }

    public void setParteInteraMittFormatted(String parteInteraMitt) {
        this.parteInteraMitt = Trunc.toUnsignedNumeric(parteInteraMitt, Len.PARTE_INTERA_MITT);
    }

    public short getParteInteraMitt() {
        return NumericDisplay.asShort(this.parteInteraMitt);
    }

    public void setParteInteraDestFormatted(String parteInteraDest) {
        this.parteInteraDest = Trunc.toUnsignedNumeric(parteInteraDest, Len.PARTE_INTERA_DEST);
    }

    public short getParteInteraDest() {
        return NumericDisplay.asShort(this.parteInteraDest);
    }

    public void setWsNumDi1Formatted(String wsNumDi1) {
        this.wsNumDi1 = Trunc.toUnsignedNumeric(wsNumDi1, Len.WS_NUM_DI1);
    }

    public short getWsNumDi1() {
        return NumericDisplay.asShort(this.wsNumDi1);
    }

    public void setWsNumDi3Formatted(String wsNumDi3) {
        this.wsNumDi3 = Trunc.toUnsignedNumeric(wsNumDi3, Len.WS_NUM_DI3);
    }

    public short getWsNumDi3() {
        return NumericDisplay.asShort(this.wsNumDi3);
    }

    public void setWsNumDi5Formatted(String wsNumDi5) {
        this.wsNumDi5 = Trunc.toUnsignedNumeric(wsNumDi5, Len.WS_NUM_DI5);
    }

    public int getWsNumDi5() {
        return NumericDisplay.asInt(this.wsNumDi5);
    }

    public void setWsNumDi8Formatted(String wsNumDi8) {
        this.wsNumDi8 = Trunc.toUnsignedNumeric(wsNumDi8, Len.WS_NUM_DI8);
    }

    public int getWsNumDi8() {
        return NumericDisplay.asInt(this.wsNumDi8);
    }

    public void setWsNumDi9Formatted(String wsNumDi9) {
        this.wsNumDi9 = Trunc.toUnsignedNumeric(wsNumDi9, Len.WS_NUM_DI9);
    }

    public int getWsNumDi9() {
        return NumericDisplay.asInt(this.wsNumDi9);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PARTE_INTERA_MITT = 3;
        public static final int PARTE_INTERA_DEST = 3;
        public static final int WS_NUM_DI1 = 1;
        public static final int FLR1 = 39;
        public static final int WS_NUM_DI3 = 3;
        public static final int FLR2 = 37;
        public static final int WS_NUM_DI5 = 5;
        public static final int FLR3 = 35;
        public static final int WS_NUM_DI8 = 8;
        public static final int FLR4 = 32;
        public static final int WS_NUM_DI9 = 9;
        public static final int FLR5 = 31;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
