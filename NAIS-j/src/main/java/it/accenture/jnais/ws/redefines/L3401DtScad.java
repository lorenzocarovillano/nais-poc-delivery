package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DT-SCAD<br>
 * Variable: L3401-DT-SCAD from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DT_SCAD;
    }

    public void setL3401DtScad(int l3401DtScad) {
        writeIntAsPacked(Pos.L3401_DT_SCAD, l3401DtScad, Len.Int.L3401_DT_SCAD);
    }

    public void setL3401DtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DT_SCAD, Pos.L3401_DT_SCAD);
    }

    /**Original name: L3401-DT-SCAD<br>*/
    public int getL3401DtScad() {
        return readPackedAsInt(Pos.L3401_DT_SCAD, Len.Int.L3401_DT_SCAD);
    }

    public byte[] getL3401DtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DT_SCAD, Pos.L3401_DT_SCAD);
        return buffer;
    }

    /**Original name: L3401-DT-SCAD-NULL<br>*/
    public String getL3401DtScadNull() {
        return readString(Pos.L3401_DT_SCAD_NULL, Len.L3401_DT_SCAD_NULL);
    }

    public String getL3401DtScadNullFormatted() {
        return Functions.padBlanks(getL3401DtScadNull(), Len.L3401_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DT_SCAD = 1;
        public static final int L3401_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DT_SCAD = 5;
        public static final int L3401_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
