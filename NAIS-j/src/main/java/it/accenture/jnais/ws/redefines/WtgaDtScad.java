package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DT-SCAD<br>
 * Variable: WTGA-DT-SCAD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DT_SCAD;
    }

    public void setWtgaDtScad(int wtgaDtScad) {
        writeIntAsPacked(Pos.WTGA_DT_SCAD, wtgaDtScad, Len.Int.WTGA_DT_SCAD);
    }

    public void setWtgaDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DT_SCAD, Pos.WTGA_DT_SCAD);
    }

    /**Original name: WTGA-DT-SCAD<br>*/
    public int getWtgaDtScad() {
        return readPackedAsInt(Pos.WTGA_DT_SCAD, Len.Int.WTGA_DT_SCAD);
    }

    public byte[] getWtgaDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DT_SCAD, Pos.WTGA_DT_SCAD);
        return buffer;
    }

    public void initWtgaDtScadSpaces() {
        fill(Pos.WTGA_DT_SCAD, Len.WTGA_DT_SCAD, Types.SPACE_CHAR);
    }

    public void setWtgaDtScadNull(String wtgaDtScadNull) {
        writeString(Pos.WTGA_DT_SCAD_NULL, wtgaDtScadNull, Len.WTGA_DT_SCAD_NULL);
    }

    /**Original name: WTGA-DT-SCAD-NULL<br>*/
    public String getWtgaDtScadNull() {
        return readString(Pos.WTGA_DT_SCAD_NULL, Len.WTGA_DT_SCAD_NULL);
    }

    public String getWtgaDtScadNullFormatted() {
        return Functions.padBlanks(getWtgaDtScadNull(), Len.WTGA_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DT_SCAD = 1;
        public static final int WTGA_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DT_SCAD = 5;
        public static final int WTGA_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
