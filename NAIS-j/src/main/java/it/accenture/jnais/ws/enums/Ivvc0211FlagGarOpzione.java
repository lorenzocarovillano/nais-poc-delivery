package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IVVC0211-FLAG-GAR-OPZIONE<br>
 * Variable: IVVC0211-FLAG-GAR-OPZIONE from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211FlagGarOpzione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagGarOpzione(char flagGarOpzione) {
        this.value = flagGarOpzione;
    }

    public char getFlagGarOpzione() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setS211FlagGarOpzioneNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_GAR_OPZIONE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
