package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ALQ-PRVR<br>
 * Variable: DFA-ALQ-PRVR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAlqPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAlqPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ALQ_PRVR;
    }

    public void setDfaAlqPrvr(AfDecimal dfaAlqPrvr) {
        writeDecimalAsPacked(Pos.DFA_ALQ_PRVR, dfaAlqPrvr.copy());
    }

    public void setDfaAlqPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ALQ_PRVR, Pos.DFA_ALQ_PRVR);
    }

    /**Original name: DFA-ALQ-PRVR<br>*/
    public AfDecimal getDfaAlqPrvr() {
        return readPackedAsDecimal(Pos.DFA_ALQ_PRVR, Len.Int.DFA_ALQ_PRVR, Len.Fract.DFA_ALQ_PRVR);
    }

    public byte[] getDfaAlqPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ALQ_PRVR, Pos.DFA_ALQ_PRVR);
        return buffer;
    }

    public void setDfaAlqPrvrNull(String dfaAlqPrvrNull) {
        writeString(Pos.DFA_ALQ_PRVR_NULL, dfaAlqPrvrNull, Len.DFA_ALQ_PRVR_NULL);
    }

    /**Original name: DFA-ALQ-PRVR-NULL<br>*/
    public String getDfaAlqPrvrNull() {
        return readString(Pos.DFA_ALQ_PRVR_NULL, Len.DFA_ALQ_PRVR_NULL);
    }

    public String getDfaAlqPrvrNullFormatted() {
        return Functions.padBlanks(getDfaAlqPrvrNull(), Len.DFA_ALQ_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ALQ_PRVR = 1;
        public static final int DFA_ALQ_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ALQ_PRVR = 4;
        public static final int DFA_ALQ_PRVR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ALQ_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_ALQ_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
