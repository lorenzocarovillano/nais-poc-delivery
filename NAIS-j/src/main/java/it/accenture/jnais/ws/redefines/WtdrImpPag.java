package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-PAG<br>
 * Variable: WTDR-IMP-PAG from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_PAG;
    }

    public void setWtdrImpPag(AfDecimal wtdrImpPag) {
        writeDecimalAsPacked(Pos.WTDR_IMP_PAG, wtdrImpPag.copy());
    }

    /**Original name: WTDR-IMP-PAG<br>*/
    public AfDecimal getWtdrImpPag() {
        return readPackedAsDecimal(Pos.WTDR_IMP_PAG, Len.Int.WTDR_IMP_PAG, Len.Fract.WTDR_IMP_PAG);
    }

    public void setWtdrImpPagNull(String wtdrImpPagNull) {
        writeString(Pos.WTDR_IMP_PAG_NULL, wtdrImpPagNull, Len.WTDR_IMP_PAG_NULL);
    }

    /**Original name: WTDR-IMP-PAG-NULL<br>*/
    public String getWtdrImpPagNull() {
        return readString(Pos.WTDR_IMP_PAG_NULL, Len.WTDR_IMP_PAG_NULL);
    }

    public String getWtdrImpPagNullFormatted() {
        return Functions.padBlanks(getWtdrImpPagNull(), Len.WTDR_IMP_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_PAG = 1;
        public static final int WTDR_IMP_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_PAG = 8;
        public static final int WTDR_IMP_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
