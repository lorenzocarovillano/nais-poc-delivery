package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRSTZ-AGG-ULT<br>
 * Variable: WB03-PRSTZ-AGG-ULT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrstzAggUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrstzAggUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRSTZ_AGG_ULT;
    }

    public void setWb03PrstzAggUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRSTZ_AGG_ULT, Pos.WB03_PRSTZ_AGG_ULT);
    }

    /**Original name: WB03-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getWb03PrstzAggUlt() {
        return readPackedAsDecimal(Pos.WB03_PRSTZ_AGG_ULT, Len.Int.WB03_PRSTZ_AGG_ULT, Len.Fract.WB03_PRSTZ_AGG_ULT);
    }

    public byte[] getWb03PrstzAggUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRSTZ_AGG_ULT, Pos.WB03_PRSTZ_AGG_ULT);
        return buffer;
    }

    public void setWb03PrstzAggUltNull(String wb03PrstzAggUltNull) {
        writeString(Pos.WB03_PRSTZ_AGG_ULT_NULL, wb03PrstzAggUltNull, Len.WB03_PRSTZ_AGG_ULT_NULL);
    }

    /**Original name: WB03-PRSTZ-AGG-ULT-NULL<br>*/
    public String getWb03PrstzAggUltNull() {
        return readString(Pos.WB03_PRSTZ_AGG_ULT_NULL, Len.WB03_PRSTZ_AGG_ULT_NULL);
    }

    public String getWb03PrstzAggUltNullFormatted() {
        return Functions.padBlanks(getWb03PrstzAggUltNull(), Len.WB03_PRSTZ_AGG_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_AGG_ULT = 1;
        public static final int WB03_PRSTZ_AGG_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_AGG_ULT = 8;
        public static final int WB03_PRSTZ_AGG_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_AGG_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_AGG_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
