package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-VAL-PC<br>
 * Variable: WPAG-VAL-PC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagValPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagValPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_VAL_PC;
    }

    public void setWpagValPc(AfDecimal wpagValPc) {
        writeDecimalAsPacked(Pos.WPAG_VAL_PC, wpagValPc.copy());
    }

    public void setWpagValPcFormatted(String wpagValPc) {
        setWpagValPc(PicParser.display(new PicParams("S9(3)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_VAL_PC + Len.Fract.WPAG_VAL_PC, Len.Fract.WPAG_VAL_PC, wpagValPc));
    }

    public void setWpagValPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_VAL_PC, Pos.WPAG_VAL_PC);
    }

    /**Original name: WPAG-VAL-PC<br>*/
    public AfDecimal getWpagValPc() {
        return readPackedAsDecimal(Pos.WPAG_VAL_PC, Len.Int.WPAG_VAL_PC, Len.Fract.WPAG_VAL_PC);
    }

    public byte[] getWpagValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_VAL_PC, Pos.WPAG_VAL_PC);
        return buffer;
    }

    public void initWpagValPcSpaces() {
        fill(Pos.WPAG_VAL_PC, Len.WPAG_VAL_PC, Types.SPACE_CHAR);
    }

    public void setWpagValPcNull(String wpagValPcNull) {
        writeString(Pos.WPAG_VAL_PC_NULL, wpagValPcNull, Len.WPAG_VAL_PC_NULL);
    }

    /**Original name: WPAG-VAL-PC-NULL<br>*/
    public String getWpagValPcNull() {
        return readString(Pos.WPAG_VAL_PC_NULL, Len.WPAG_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_PC = 1;
        public static final int WPAG_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_PC = 4;
        public static final int WPAG_VAL_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_VAL_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_VAL_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
