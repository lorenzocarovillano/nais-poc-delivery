package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-ID-MOVI-CHIU<br>
 * Variable: WDCO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_ID_MOVI_CHIU;
    }

    public void setWdcoIdMoviChiu(int wdcoIdMoviChiu) {
        writeIntAsPacked(Pos.WDCO_ID_MOVI_CHIU, wdcoIdMoviChiu, Len.Int.WDCO_ID_MOVI_CHIU);
    }

    public void setWdcoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_ID_MOVI_CHIU, Pos.WDCO_ID_MOVI_CHIU);
    }

    /**Original name: WDCO-ID-MOVI-CHIU<br>*/
    public int getWdcoIdMoviChiu() {
        return readPackedAsInt(Pos.WDCO_ID_MOVI_CHIU, Len.Int.WDCO_ID_MOVI_CHIU);
    }

    public byte[] getWdcoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_ID_MOVI_CHIU, Pos.WDCO_ID_MOVI_CHIU);
        return buffer;
    }

    public void setWdcoIdMoviChiuNull(String wdcoIdMoviChiuNull) {
        writeString(Pos.WDCO_ID_MOVI_CHIU_NULL, wdcoIdMoviChiuNull, Len.WDCO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WDCO-ID-MOVI-CHIU-NULL<br>*/
    public String getWdcoIdMoviChiuNull() {
        return readString(Pos.WDCO_ID_MOVI_CHIU_NULL, Len.WDCO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_ID_MOVI_CHIU = 1;
        public static final int WDCO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_ID_MOVI_CHIU = 5;
        public static final int WDCO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
