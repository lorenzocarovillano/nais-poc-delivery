package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DT-APPLZ-CONV<br>
 * Variable: POL-DT-APPLZ-CONV from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDtApplzConv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDtApplzConv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DT_APPLZ_CONV;
    }

    public void setPolDtApplzConv(int polDtApplzConv) {
        writeIntAsPacked(Pos.POL_DT_APPLZ_CONV, polDtApplzConv, Len.Int.POL_DT_APPLZ_CONV);
    }

    public void setPolDtApplzConvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DT_APPLZ_CONV, Pos.POL_DT_APPLZ_CONV);
    }

    /**Original name: POL-DT-APPLZ-CONV<br>*/
    public int getPolDtApplzConv() {
        return readPackedAsInt(Pos.POL_DT_APPLZ_CONV, Len.Int.POL_DT_APPLZ_CONV);
    }

    public byte[] getPolDtApplzConvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DT_APPLZ_CONV, Pos.POL_DT_APPLZ_CONV);
        return buffer;
    }

    public void setPolDtApplzConvNull(String polDtApplzConvNull) {
        writeString(Pos.POL_DT_APPLZ_CONV_NULL, polDtApplzConvNull, Len.POL_DT_APPLZ_CONV_NULL);
    }

    /**Original name: POL-DT-APPLZ-CONV-NULL<br>*/
    public String getPolDtApplzConvNull() {
        return readString(Pos.POL_DT_APPLZ_CONV_NULL, Len.POL_DT_APPLZ_CONV_NULL);
    }

    public String getPolDtApplzConvNullFormatted() {
        return Functions.padBlanks(getPolDtApplzConvNull(), Len.POL_DT_APPLZ_CONV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DT_APPLZ_CONV = 1;
        public static final int POL_DT_APPLZ_CONV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DT_APPLZ_CONV = 5;
        public static final int POL_DT_APPLZ_CONV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DT_APPLZ_CONV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
