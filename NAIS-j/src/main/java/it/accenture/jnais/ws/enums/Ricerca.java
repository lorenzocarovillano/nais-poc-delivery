package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: RICERCA<br>
 * Variable: RICERCA from program LCCS0490<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ricerca {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setRicerca(char ricerca) {
        this.value = ricerca;
    }

    public char getRicerca() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
