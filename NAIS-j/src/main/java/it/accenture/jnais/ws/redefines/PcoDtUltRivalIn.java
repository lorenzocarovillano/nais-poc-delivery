package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RIVAL-IN<br>
 * Variable: PCO-DT-ULT-RIVAL-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRivalIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRivalIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RIVAL_IN;
    }

    public void setPcoDtUltRivalIn(int pcoDtUltRivalIn) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RIVAL_IN, pcoDtUltRivalIn, Len.Int.PCO_DT_ULT_RIVAL_IN);
    }

    public void setPcoDtUltRivalInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RIVAL_IN, Pos.PCO_DT_ULT_RIVAL_IN);
    }

    /**Original name: PCO-DT-ULT-RIVAL-IN<br>*/
    public int getPcoDtUltRivalIn() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RIVAL_IN, Len.Int.PCO_DT_ULT_RIVAL_IN);
    }

    public byte[] getPcoDtUltRivalInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RIVAL_IN, Pos.PCO_DT_ULT_RIVAL_IN);
        return buffer;
    }

    public void initPcoDtUltRivalInHighValues() {
        fill(Pos.PCO_DT_ULT_RIVAL_IN, Len.PCO_DT_ULT_RIVAL_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRivalInNull(String pcoDtUltRivalInNull) {
        writeString(Pos.PCO_DT_ULT_RIVAL_IN_NULL, pcoDtUltRivalInNull, Len.PCO_DT_ULT_RIVAL_IN_NULL);
    }

    /**Original name: PCO-DT-ULT-RIVAL-IN-NULL<br>*/
    public String getPcoDtUltRivalInNull() {
        return readString(Pos.PCO_DT_ULT_RIVAL_IN_NULL, Len.PCO_DT_ULT_RIVAL_IN_NULL);
    }

    public String getPcoDtUltRivalInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRivalInNull(), Len.PCO_DT_ULT_RIVAL_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RIVAL_IN = 1;
        public static final int PCO_DT_ULT_RIVAL_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RIVAL_IN = 5;
        public static final int PCO_DT_ULT_RIVAL_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RIVAL_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
