package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-DT-CAMBIO-VLT<br>
 * Variable: RIF-DT-CAMBIO-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifDtCambioVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifDtCambioVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_DT_CAMBIO_VLT;
    }

    public void setRifDtCambioVlt(int rifDtCambioVlt) {
        writeIntAsPacked(Pos.RIF_DT_CAMBIO_VLT, rifDtCambioVlt, Len.Int.RIF_DT_CAMBIO_VLT);
    }

    public void setRifDtCambioVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_DT_CAMBIO_VLT, Pos.RIF_DT_CAMBIO_VLT);
    }

    /**Original name: RIF-DT-CAMBIO-VLT<br>*/
    public int getRifDtCambioVlt() {
        return readPackedAsInt(Pos.RIF_DT_CAMBIO_VLT, Len.Int.RIF_DT_CAMBIO_VLT);
    }

    public byte[] getRifDtCambioVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_DT_CAMBIO_VLT, Pos.RIF_DT_CAMBIO_VLT);
        return buffer;
    }

    public void setRifDtCambioVltNull(String rifDtCambioVltNull) {
        writeString(Pos.RIF_DT_CAMBIO_VLT_NULL, rifDtCambioVltNull, Len.RIF_DT_CAMBIO_VLT_NULL);
    }

    /**Original name: RIF-DT-CAMBIO-VLT-NULL<br>*/
    public String getRifDtCambioVltNull() {
        return readString(Pos.RIF_DT_CAMBIO_VLT_NULL, Len.RIF_DT_CAMBIO_VLT_NULL);
    }

    public String getRifDtCambioVltNullFormatted() {
        return Functions.padBlanks(getRifDtCambioVltNull(), Len.RIF_DT_CAMBIO_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_DT_CAMBIO_VLT = 1;
        public static final int RIF_DT_CAMBIO_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_DT_CAMBIO_VLT = 5;
        public static final int RIF_DT_CAMBIO_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_DT_CAMBIO_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
