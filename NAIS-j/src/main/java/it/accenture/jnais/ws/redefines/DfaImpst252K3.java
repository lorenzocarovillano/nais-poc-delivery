package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPST-252-K3<br>
 * Variable: DFA-IMPST-252-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpst252K3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpst252K3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPST252_K3;
    }

    public void setDfaImpst252K3(AfDecimal dfaImpst252K3) {
        writeDecimalAsPacked(Pos.DFA_IMPST252_K3, dfaImpst252K3.copy());
    }

    public void setDfaImpst252K3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPST252_K3, Pos.DFA_IMPST252_K3);
    }

    /**Original name: DFA-IMPST-252-K3<br>*/
    public AfDecimal getDfaImpst252K3() {
        return readPackedAsDecimal(Pos.DFA_IMPST252_K3, Len.Int.DFA_IMPST252_K3, Len.Fract.DFA_IMPST252_K3);
    }

    public byte[] getDfaImpst252K3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPST252_K3, Pos.DFA_IMPST252_K3);
        return buffer;
    }

    public void setDfaImpst252K3Null(String dfaImpst252K3Null) {
        writeString(Pos.DFA_IMPST252_K3_NULL, dfaImpst252K3Null, Len.DFA_IMPST252_K3_NULL);
    }

    /**Original name: DFA-IMPST-252-K3-NULL<br>*/
    public String getDfaImpst252K3Null() {
        return readString(Pos.DFA_IMPST252_K3_NULL, Len.DFA_IMPST252_K3_NULL);
    }

    public String getDfaImpst252K3NullFormatted() {
        return Functions.padBlanks(getDfaImpst252K3Null(), Len.DFA_IMPST252_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPST252_K3 = 1;
        public static final int DFA_IMPST252_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPST252_K3 = 8;
        public static final int DFA_IMPST252_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPST252_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPST252_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
