package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-VERSIONE<br>
 * Variable: FLAG-VERSIONE from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagVersione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char EMISSIONE = 'E';
    public static final char ULTIMA = 'U';
    public static final char SPAZIO = ' ';

    //==== METHODS ====
    public void setFlagVersione(char flagVersione) {
        this.value = flagVersione;
    }

    public void setFlagVersioneFormatted(String flagVersione) {
        setFlagVersione(Functions.charAt(flagVersione, Types.CHAR_SIZE));
    }

    public char getFlagVersione() {
        return this.value;
    }

    public boolean isEmissione() {
        return value == EMISSIONE;
    }
}
