package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-MESI-CARENZA<br>
 * Variable: WK-MESI-CARENZA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkMesiCarenza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkMesiCarenza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_MESI_CARENZA;
    }

    public void setWkMesiCarenza(int wkMesiCarenza) {
        writeIntAsPacked(Pos.WK_MESI_CARENZA, wkMesiCarenza, Len.Int.WK_MESI_CARENZA);
    }

    /**Original name: WK-MESI-CARENZA<br>*/
    public int getWkMesiCarenza() {
        return readPackedAsInt(Pos.WK_MESI_CARENZA, Len.Int.WK_MESI_CARENZA);
    }

    public void setWkMesiCarenzaNull(String wkMesiCarenzaNull) {
        writeString(Pos.WK_MESI_CARENZA_NULL, wkMesiCarenzaNull, Len.WK_MESI_CARENZA_NULL);
    }

    /**Original name: WK-MESI-CARENZA-NULL<br>*/
    public String getWkMesiCarenzaNull() {
        return readString(Pos.WK_MESI_CARENZA_NULL, Len.WK_MESI_CARENZA_NULL);
    }

    public String getWkMesiCarenzaNullFormatted() {
        return Functions.padBlanks(getWkMesiCarenzaNull(), Len.WK_MESI_CARENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_MESI_CARENZA = 1;
        public static final int WK_MESI_CARENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_MESI_CARENZA = 3;
        public static final int WK_MESI_CARENZA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_MESI_CARENZA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
