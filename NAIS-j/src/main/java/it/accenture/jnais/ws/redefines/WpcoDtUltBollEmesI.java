package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-EMES-I<br>
 * Variable: WPCO-DT-ULT-BOLL-EMES-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollEmesI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollEmesI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_EMES_I;
    }

    public void setWpcoDtUltBollEmesI(int wpcoDtUltBollEmesI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_EMES_I, wpcoDtUltBollEmesI, Len.Int.WPCO_DT_ULT_BOLL_EMES_I);
    }

    public void setDpcoDtUltBollEmesIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_EMES_I, Pos.WPCO_DT_ULT_BOLL_EMES_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-EMES-I<br>*/
    public int getWpcoDtUltBollEmesI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_EMES_I, Len.Int.WPCO_DT_ULT_BOLL_EMES_I);
    }

    public byte[] getWpcoDtUltBollEmesIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_EMES_I, Pos.WPCO_DT_ULT_BOLL_EMES_I);
        return buffer;
    }

    public void setWpcoDtUltBollEmesINull(String wpcoDtUltBollEmesINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_EMES_I_NULL, wpcoDtUltBollEmesINull, Len.WPCO_DT_ULT_BOLL_EMES_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-EMES-I-NULL<br>*/
    public String getWpcoDtUltBollEmesINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_EMES_I_NULL, Len.WPCO_DT_ULT_BOLL_EMES_I_NULL);
    }

    public String getWpcoDtUltBollEmesINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollEmesINull(), Len.WPCO_DT_ULT_BOLL_EMES_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_EMES_I = 1;
        public static final int WPCO_DT_ULT_BOLL_EMES_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_EMES_I = 5;
        public static final int WPCO_DT_ULT_BOLL_EMES_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_EMES_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
