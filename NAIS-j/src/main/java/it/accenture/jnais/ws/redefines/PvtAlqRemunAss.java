package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-ALQ-REMUN-ASS<br>
 * Variable: PVT-ALQ-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtAlqRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtAlqRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_ALQ_REMUN_ASS;
    }

    public void setPvtAlqRemunAss(AfDecimal pvtAlqRemunAss) {
        writeDecimalAsPacked(Pos.PVT_ALQ_REMUN_ASS, pvtAlqRemunAss.copy());
    }

    public void setPvtAlqRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_ALQ_REMUN_ASS, Pos.PVT_ALQ_REMUN_ASS);
    }

    /**Original name: PVT-ALQ-REMUN-ASS<br>*/
    public AfDecimal getPvtAlqRemunAss() {
        return readPackedAsDecimal(Pos.PVT_ALQ_REMUN_ASS, Len.Int.PVT_ALQ_REMUN_ASS, Len.Fract.PVT_ALQ_REMUN_ASS);
    }

    public byte[] getPvtAlqRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_ALQ_REMUN_ASS, Pos.PVT_ALQ_REMUN_ASS);
        return buffer;
    }

    public void setPvtAlqRemunAssNull(String pvtAlqRemunAssNull) {
        writeString(Pos.PVT_ALQ_REMUN_ASS_NULL, pvtAlqRemunAssNull, Len.PVT_ALQ_REMUN_ASS_NULL);
    }

    /**Original name: PVT-ALQ-REMUN-ASS-NULL<br>*/
    public String getPvtAlqRemunAssNull() {
        return readString(Pos.PVT_ALQ_REMUN_ASS_NULL, Len.PVT_ALQ_REMUN_ASS_NULL);
    }

    public String getPvtAlqRemunAssNullFormatted() {
        return Functions.padBlanks(getPvtAlqRemunAssNull(), Len.PVT_ALQ_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_REMUN_ASS = 1;
        public static final int PVT_ALQ_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_ALQ_REMUN_ASS = 4;
        public static final int PVT_ALQ_REMUN_ASS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_ALQ_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
