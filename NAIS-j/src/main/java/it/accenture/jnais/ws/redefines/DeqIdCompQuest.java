package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-ID-COMP-QUEST<br>
 * Variable: DEQ-ID-COMP-QUEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqIdCompQuest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqIdCompQuest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_ID_COMP_QUEST;
    }

    public void setDeqIdCompQuest(int deqIdCompQuest) {
        writeIntAsPacked(Pos.DEQ_ID_COMP_QUEST, deqIdCompQuest, Len.Int.DEQ_ID_COMP_QUEST);
    }

    public void setDeqIdCompQuestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_ID_COMP_QUEST, Pos.DEQ_ID_COMP_QUEST);
    }

    /**Original name: DEQ-ID-COMP-QUEST<br>*/
    public int getDeqIdCompQuest() {
        return readPackedAsInt(Pos.DEQ_ID_COMP_QUEST, Len.Int.DEQ_ID_COMP_QUEST);
    }

    public byte[] getDeqIdCompQuestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_ID_COMP_QUEST, Pos.DEQ_ID_COMP_QUEST);
        return buffer;
    }

    public void setDeqIdCompQuestNull(String deqIdCompQuestNull) {
        writeString(Pos.DEQ_ID_COMP_QUEST_NULL, deqIdCompQuestNull, Len.DEQ_ID_COMP_QUEST_NULL);
    }

    /**Original name: DEQ-ID-COMP-QUEST-NULL<br>*/
    public String getDeqIdCompQuestNull() {
        return readString(Pos.DEQ_ID_COMP_QUEST_NULL, Len.DEQ_ID_COMP_QUEST_NULL);
    }

    public String getDeqIdCompQuestNullFormatted() {
        return Functions.padBlanks(getDeqIdCompQuestNull(), Len.DEQ_ID_COMP_QUEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_ID_COMP_QUEST = 1;
        public static final int DEQ_ID_COMP_QUEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_ID_COMP_QUEST = 5;
        public static final int DEQ_ID_COMP_QUEST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_ID_COMP_QUEST = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
