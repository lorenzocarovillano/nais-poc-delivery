package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SOPR-TEC<br>
 * Variable: DTC-SOPR-TEC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_SOPR_TEC;
    }

    public void setDtcSoprTec(AfDecimal dtcSoprTec) {
        writeDecimalAsPacked(Pos.DTC_SOPR_TEC, dtcSoprTec.copy());
    }

    public void setDtcSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_SOPR_TEC, Pos.DTC_SOPR_TEC);
    }

    /**Original name: DTC-SOPR-TEC<br>*/
    public AfDecimal getDtcSoprTec() {
        return readPackedAsDecimal(Pos.DTC_SOPR_TEC, Len.Int.DTC_SOPR_TEC, Len.Fract.DTC_SOPR_TEC);
    }

    public byte[] getDtcSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_SOPR_TEC, Pos.DTC_SOPR_TEC);
        return buffer;
    }

    public void setDtcSoprTecNull(String dtcSoprTecNull) {
        writeString(Pos.DTC_SOPR_TEC_NULL, dtcSoprTecNull, Len.DTC_SOPR_TEC_NULL);
    }

    /**Original name: DTC-SOPR-TEC-NULL<br>*/
    public String getDtcSoprTecNull() {
        return readString(Pos.DTC_SOPR_TEC_NULL, Len.DTC_SOPR_TEC_NULL);
    }

    public String getDtcSoprTecNullFormatted() {
        return Functions.padBlanks(getDtcSoprTecNull(), Len.DTC_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_TEC = 1;
        public static final int DTC_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_TEC = 8;
        public static final int DTC_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
