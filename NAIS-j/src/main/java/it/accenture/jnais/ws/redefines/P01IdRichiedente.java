package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P01-ID-RICHIEDENTE<br>
 * Variable: P01-ID-RICHIEDENTE from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P01IdRichiedente extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P01IdRichiedente() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P01_ID_RICHIEDENTE;
    }

    public void setP01IdRichiedente(int p01IdRichiedente) {
        writeIntAsPacked(Pos.P01_ID_RICHIEDENTE, p01IdRichiedente, Len.Int.P01_ID_RICHIEDENTE);
    }

    public void setP01IdRichiedenteFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P01_ID_RICHIEDENTE, Pos.P01_ID_RICHIEDENTE);
    }

    /**Original name: P01-ID-RICHIEDENTE<br>*/
    public int getP01IdRichiedente() {
        return readPackedAsInt(Pos.P01_ID_RICHIEDENTE, Len.Int.P01_ID_RICHIEDENTE);
    }

    public byte[] getP01IdRichiedenteAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P01_ID_RICHIEDENTE, Pos.P01_ID_RICHIEDENTE);
        return buffer;
    }

    public void setP01IdRichiedenteNull(String p01IdRichiedenteNull) {
        writeString(Pos.P01_ID_RICHIEDENTE_NULL, p01IdRichiedenteNull, Len.P01_ID_RICHIEDENTE_NULL);
    }

    /**Original name: P01-ID-RICHIEDENTE-NULL<br>*/
    public String getP01IdRichiedenteNull() {
        return readString(Pos.P01_ID_RICHIEDENTE_NULL, Len.P01_ID_RICHIEDENTE_NULL);
    }

    public String getP01IdRichiedenteNullFormatted() {
        return Functions.padBlanks(getP01IdRichiedenteNull(), Len.P01_ID_RICHIEDENTE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P01_ID_RICHIEDENTE = 1;
        public static final int P01_ID_RICHIEDENTE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P01_ID_RICHIEDENTE = 5;
        public static final int P01_ID_RICHIEDENTE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P01_ID_RICHIEDENTE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
