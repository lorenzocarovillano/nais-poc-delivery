package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IS-1382011D<br>
 * Variable: DFL-IS-1382011D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIs1382011d extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIs1382011d() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IS1382011D;
    }

    public void setDflIs1382011d(AfDecimal dflIs1382011d) {
        writeDecimalAsPacked(Pos.DFL_IS1382011D, dflIs1382011d.copy());
    }

    public void setDflIs1382011dFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IS1382011D, Pos.DFL_IS1382011D);
    }

    /**Original name: DFL-IS-1382011D<br>*/
    public AfDecimal getDflIs1382011d() {
        return readPackedAsDecimal(Pos.DFL_IS1382011D, Len.Int.DFL_IS1382011D, Len.Fract.DFL_IS1382011D);
    }

    public byte[] getDflIs1382011dAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IS1382011D, Pos.DFL_IS1382011D);
        return buffer;
    }

    public void setDflIs1382011dNull(String dflIs1382011dNull) {
        writeString(Pos.DFL_IS1382011D_NULL, dflIs1382011dNull, Len.DFL_IS1382011D_NULL);
    }

    /**Original name: DFL-IS-1382011D-NULL<br>*/
    public String getDflIs1382011dNull() {
        return readString(Pos.DFL_IS1382011D_NULL, Len.DFL_IS1382011D_NULL);
    }

    public String getDflIs1382011dNullFormatted() {
        return Functions.padBlanks(getDflIs1382011dNull(), Len.DFL_IS1382011D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IS1382011D = 1;
        public static final int DFL_IS1382011D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IS1382011D = 8;
        public static final int DFL_IS1382011D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IS1382011D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IS1382011D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
