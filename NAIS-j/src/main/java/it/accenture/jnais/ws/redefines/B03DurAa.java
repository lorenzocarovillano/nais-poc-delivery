package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-AA<br>
 * Variable: B03-DUR-AA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_AA;
    }

    public void setB03DurAa(int b03DurAa) {
        writeIntAsPacked(Pos.B03_DUR_AA, b03DurAa, Len.Int.B03_DUR_AA);
    }

    public void setB03DurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_AA, Pos.B03_DUR_AA);
    }

    /**Original name: B03-DUR-AA<br>*/
    public int getB03DurAa() {
        return readPackedAsInt(Pos.B03_DUR_AA, Len.Int.B03_DUR_AA);
    }

    public byte[] getB03DurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_AA, Pos.B03_DUR_AA);
        return buffer;
    }

    public void setB03DurAaNull(String b03DurAaNull) {
        writeString(Pos.B03_DUR_AA_NULL, b03DurAaNull, Len.B03_DUR_AA_NULL);
    }

    /**Original name: B03-DUR-AA-NULL<br>*/
    public String getB03DurAaNull() {
        return readString(Pos.B03_DUR_AA_NULL, Len.B03_DUR_AA_NULL);
    }

    public String getB03DurAaNullFormatted() {
        return Functions.padBlanks(getB03DurAaNull(), Len.B03_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_AA = 1;
        public static final int B03_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_AA = 3;
        public static final int B03_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
