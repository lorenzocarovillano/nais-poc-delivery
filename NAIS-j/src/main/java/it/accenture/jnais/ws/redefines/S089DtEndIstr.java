package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-DT-END-ISTR<br>
 * Variable: S089-DT-END-ISTR from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtEndIstr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089DtEndIstr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_DT_END_ISTR;
    }

    public void setWlquDtEndIstr(int wlquDtEndIstr) {
        writeIntAsPacked(Pos.S089_DT_END_ISTR, wlquDtEndIstr, Len.Int.WLQU_DT_END_ISTR);
    }

    public void setWlquDtEndIstrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_DT_END_ISTR, Pos.S089_DT_END_ISTR);
    }

    /**Original name: WLQU-DT-END-ISTR<br>*/
    public int getWlquDtEndIstr() {
        return readPackedAsInt(Pos.S089_DT_END_ISTR, Len.Int.WLQU_DT_END_ISTR);
    }

    public byte[] getWlquDtEndIstrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_DT_END_ISTR, Pos.S089_DT_END_ISTR);
        return buffer;
    }

    public void initWlquDtEndIstrSpaces() {
        fill(Pos.S089_DT_END_ISTR, Len.S089_DT_END_ISTR, Types.SPACE_CHAR);
    }

    public void setWlquDtEndIstrNull(String wlquDtEndIstrNull) {
        writeString(Pos.S089_DT_END_ISTR_NULL, wlquDtEndIstrNull, Len.WLQU_DT_END_ISTR_NULL);
    }

    /**Original name: WLQU-DT-END-ISTR-NULL<br>*/
    public String getWlquDtEndIstrNull() {
        return readString(Pos.S089_DT_END_ISTR_NULL, Len.WLQU_DT_END_ISTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_DT_END_ISTR = 1;
        public static final int S089_DT_END_ISTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_DT_END_ISTR = 5;
        public static final int WLQU_DT_END_ISTR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_DT_END_ISTR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
