package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: W660-MF-CALCOLATO<br>
 * Variable: W660-MF-CALCOLATO from copybook LOAC0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class W660MfCalcolato {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.MF_CALCOLATO);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setMfCalcolato(String mfCalcolato) {
        this.value = Functions.subString(mfCalcolato, Len.MF_CALCOLATO);
    }

    public String getMfCalcolato() {
        return this.value;
    }

    public boolean isW660MfCalcolatoSi() {
        return value.equals(SI);
    }

    public void setW660MfCalcolatoSi() {
        value = SI;
    }

    public void setW660MfCalcolatoNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MF_CALCOLATO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
