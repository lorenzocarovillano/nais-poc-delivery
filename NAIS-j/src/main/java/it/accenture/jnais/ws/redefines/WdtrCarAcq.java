package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-CAR-ACQ<br>
 * Variable: WDTR-CAR-ACQ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_CAR_ACQ;
    }

    public void setWdtrCarAcq(AfDecimal wdtrCarAcq) {
        writeDecimalAsPacked(Pos.WDTR_CAR_ACQ, wdtrCarAcq.copy());
    }

    /**Original name: WDTR-CAR-ACQ<br>*/
    public AfDecimal getWdtrCarAcq() {
        return readPackedAsDecimal(Pos.WDTR_CAR_ACQ, Len.Int.WDTR_CAR_ACQ, Len.Fract.WDTR_CAR_ACQ);
    }

    public void setWdtrCarAcqNull(String wdtrCarAcqNull) {
        writeString(Pos.WDTR_CAR_ACQ_NULL, wdtrCarAcqNull, Len.WDTR_CAR_ACQ_NULL);
    }

    /**Original name: WDTR-CAR-ACQ-NULL<br>*/
    public String getWdtrCarAcqNull() {
        return readString(Pos.WDTR_CAR_ACQ_NULL, Len.WDTR_CAR_ACQ_NULL);
    }

    public String getWdtrCarAcqNullFormatted() {
        return Functions.padBlanks(getWdtrCarAcqNull(), Len.WDTR_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_ACQ = 1;
        public static final int WDTR_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_ACQ = 8;
        public static final int WDTR_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
