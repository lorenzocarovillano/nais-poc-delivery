package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Loac0280FlPresDeroga;
import it.accenture.jnais.ws.enums.Loac0280GravitaDeroga;
import it.accenture.jnais.ws.enums.Loac0280LivelloDeroga;

/**Original name: WORKAREA<br>
 * Variable: WORKAREA from program LOAS0280<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Workarea extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LOAC0280-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LOAC0280-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LOAC0280-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LOAC0280-FL-PRES-DEROGA
    private Loac0280FlPresDeroga flPresDeroga = new Loac0280FlPresDeroga();
    //Original name: LOAC0280-GRAVITA-DEROGA
    private Loac0280GravitaDeroga gravitaDeroga = new Loac0280GravitaDeroga();
    //Original name: LOAC0280-LIVELLO-DEROGA
    private Loac0280LivelloDeroga livelloDeroga = new Loac0280LivelloDeroga();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WORKAREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWorkareaBytes(buf);
    }

    public String getAreaIoLoas0280Formatted() {
        return MarshalByteExt.bufferToStr(getWorkareaBytes());
    }

    public void setWorkareaBytes(byte[] buffer) {
        setWorkareaBytes(buffer, 1);
    }

    public byte[] getWorkareaBytes() {
        byte[] buffer = new byte[Len.WORKAREA];
        return getWorkareaBytes(buffer, 1);
    }

    public void setWorkareaBytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getWorkareaBytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public String getTpOggFormatted() {
        return Functions.padBlanks(getTpOgg(), Len.TP_OGG);
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        flPresDeroga.setFlPresDeroga(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        gravitaDeroga.setGravitaDeroga(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        livelloDeroga.setLivelloDeroga(MarshalByte.readString(buffer, position, Loac0280LivelloDeroga.Len.LIVELLO_DEROGA));
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, flPresDeroga.getFlPresDeroga());
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, gravitaDeroga.getGravitaDeroga());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, livelloDeroga.getLivelloDeroga(), Loac0280LivelloDeroga.Len.LIVELLO_DEROGA);
        return buffer;
    }

    public Loac0280FlPresDeroga getFlPresDeroga() {
        return flPresDeroga;
    }

    public Loac0280GravitaDeroga getGravitaDeroga() {
        return gravitaDeroga;
    }

    public Loac0280LivelloDeroga getLivelloDeroga() {
        return livelloDeroga;
    }

    @Override
    public byte[] serialize() {
        return getWorkareaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int TP_OGG = 2;
        public static final int DATI_INPUT = ID_POLI + ID_ADES + TP_OGG;
        public static final int DATI_OUTPUT = Loac0280FlPresDeroga.Len.FL_PRES_DEROGA + Loac0280GravitaDeroga.Len.GRAVITA_DEROGA + Loac0280LivelloDeroga.Len.LIVELLO_DEROGA;
        public static final int WORKAREA = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
