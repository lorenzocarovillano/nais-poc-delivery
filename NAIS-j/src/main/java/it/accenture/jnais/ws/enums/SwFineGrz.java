package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-FINE-GRZ<br>
 * Variable: SW-FINE-GRZ from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwFineGrz {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char GRZ = 'S';
    public static final char GRZ_NO = 'N';

    //==== METHODS ====
    public void setSwFineGrz(char swFineGrz) {
        this.value = swFineGrz;
    }

    public char getSwFineGrz() {
        return this.value;
    }

    public void setGrz() {
        value = GRZ;
    }

    public boolean isGrzNo() {
        return value == GRZ_NO;
    }

    public void setGrzNo() {
        value = GRZ_NO;
    }
}
