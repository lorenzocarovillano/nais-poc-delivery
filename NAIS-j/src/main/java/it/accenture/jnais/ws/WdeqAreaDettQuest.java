package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WdeqTab;

/**Original name: WDEQ-AREA-DETT-QUEST<br>
 * Variable: WDEQ-AREA-DETT-QUEST from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WdeqAreaDettQuest {

    //==== PROPERTIES ====
    //Original name: WDEQ-ELE-DETT-QUEST-MAX
    private short wdeqEleDettQuestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WDEQ-TAB
    private WdeqTab wdeqTab = new WdeqTab();

    //==== METHODS ====
    public void setWdeqAreaDettQuestFormatted(String data) {
        byte[] buffer = new byte[Len.WDEQ_AREA_DETT_QUEST];
        MarshalByte.writeString(buffer, 1, data, Len.WDEQ_AREA_DETT_QUEST);
        setWdeqAreaDettQuestBytes(buffer, 1);
    }

    public void setWdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        wdeqEleDettQuestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wdeqTab.setWdeqTabBytes(buffer, position);
    }

    public byte[] getWdeqAreaDettQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdeqEleDettQuestMax);
        position += Types.SHORT_SIZE;
        wdeqTab.getWdeqTabBytes(buffer, position);
        return buffer;
    }

    public void setWdeqEleDettQuestMax(short wdeqEleDettQuestMax) {
        this.wdeqEleDettQuestMax = wdeqEleDettQuestMax;
    }

    public short getWdeqEleDettQuestMax() {
        return this.wdeqEleDettQuestMax;
    }

    public WdeqTab getWdeqTab() {
        return wdeqTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_ELE_DETT_QUEST_MAX = 2;
        public static final int WDEQ_AREA_DETT_QUEST = WDEQ_ELE_DETT_QUEST_MAX + WdeqTab.Len.WDEQ_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
