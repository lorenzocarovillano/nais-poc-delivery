package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BTC-DT-END<br>
 * Variable: BTC-DT-END from program IABS0040<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BtcDtEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BtcDtEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_DT_END;
    }

    public void setBtcDtEnd(long btcDtEnd) {
        writeLongAsPacked(Pos.BTC_DT_END, btcDtEnd, Len.Int.BTC_DT_END);
    }

    public void setBtcDtEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BTC_DT_END, Pos.BTC_DT_END);
    }

    /**Original name: BTC-DT-END<br>*/
    public long getBtcDtEnd() {
        return readPackedAsLong(Pos.BTC_DT_END, Len.Int.BTC_DT_END);
    }

    public byte[] getBtcDtEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BTC_DT_END, Pos.BTC_DT_END);
        return buffer;
    }

    public void setBtcDtEndNull(String btcDtEndNull) {
        writeString(Pos.BTC_DT_END_NULL, btcDtEndNull, Len.BTC_DT_END_NULL);
    }

    /**Original name: BTC-DT-END-NULL<br>*/
    public String getBtcDtEndNull() {
        return readString(Pos.BTC_DT_END_NULL, Len.BTC_DT_END_NULL);
    }

    public String getBtcDtEndNullFormatted() {
        return Functions.padBlanks(getBtcDtEndNull(), Len.BTC_DT_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BTC_DT_END = 1;
        public static final int BTC_DT_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BTC_DT_END = 10;
        public static final int BTC_DT_END_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BTC_DT_END = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
