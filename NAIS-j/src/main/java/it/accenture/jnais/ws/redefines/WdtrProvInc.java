package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PROV-INC<br>
 * Variable: WDTR-PROV-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PROV_INC;
    }

    public void setWdtrProvInc(AfDecimal wdtrProvInc) {
        writeDecimalAsPacked(Pos.WDTR_PROV_INC, wdtrProvInc.copy());
    }

    /**Original name: WDTR-PROV-INC<br>*/
    public AfDecimal getWdtrProvInc() {
        return readPackedAsDecimal(Pos.WDTR_PROV_INC, Len.Int.WDTR_PROV_INC, Len.Fract.WDTR_PROV_INC);
    }

    public void setWdtrProvIncNull(String wdtrProvIncNull) {
        writeString(Pos.WDTR_PROV_INC_NULL, wdtrProvIncNull, Len.WDTR_PROV_INC_NULL);
    }

    /**Original name: WDTR-PROV-INC-NULL<br>*/
    public String getWdtrProvIncNull() {
        return readString(Pos.WDTR_PROV_INC_NULL, Len.WDTR_PROV_INC_NULL);
    }

    public String getWdtrProvIncNullFormatted() {
        return Functions.padBlanks(getWdtrProvIncNull(), Len.WDTR_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_INC = 1;
        public static final int WDTR_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_INC = 8;
        public static final int WDTR_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
