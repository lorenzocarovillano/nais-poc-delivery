package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-IS-K3<br>
 * Variable: DFA-IMPB-IS-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpbIsK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpbIsK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB_IS_K3;
    }

    public void setDfaImpbIsK3(AfDecimal dfaImpbIsK3) {
        writeDecimalAsPacked(Pos.DFA_IMPB_IS_K3, dfaImpbIsK3.copy());
    }

    public void setDfaImpbIsK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB_IS_K3, Pos.DFA_IMPB_IS_K3);
    }

    /**Original name: DFA-IMPB-IS-K3<br>*/
    public AfDecimal getDfaImpbIsK3() {
        return readPackedAsDecimal(Pos.DFA_IMPB_IS_K3, Len.Int.DFA_IMPB_IS_K3, Len.Fract.DFA_IMPB_IS_K3);
    }

    public byte[] getDfaImpbIsK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB_IS_K3, Pos.DFA_IMPB_IS_K3);
        return buffer;
    }

    public void setDfaImpbIsK3Null(String dfaImpbIsK3Null) {
        writeString(Pos.DFA_IMPB_IS_K3_NULL, dfaImpbIsK3Null, Len.DFA_IMPB_IS_K3_NULL);
    }

    /**Original name: DFA-IMPB-IS-K3-NULL<br>*/
    public String getDfaImpbIsK3Null() {
        return readString(Pos.DFA_IMPB_IS_K3_NULL, Len.DFA_IMPB_IS_K3_NULL);
    }

    public String getDfaImpbIsK3NullFormatted() {
        return Functions.padBlanks(getDfaImpbIsK3Null(), Len.DFA_IMPB_IS_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K3 = 1;
        public static final int DFA_IMPB_IS_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB_IS_K3 = 8;
        public static final int DFA_IMPB_IS_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB_IS_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
