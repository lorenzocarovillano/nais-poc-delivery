package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: LCCC0023-ID-RICH<br>
 * Variable: LCCC0023-ID-RICH from program LCCS0023<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0023IdRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0023IdRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0023_ID_RICH;
    }

    public void setLccc0023IdRich(int lccc0023IdRich) {
        writeIntAsPacked(Pos.LCCC0023_ID_RICH, lccc0023IdRich, Len.Int.LCCC0023_ID_RICH);
    }

    public void setLccc0023IdRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0023_ID_RICH, Pos.LCCC0023_ID_RICH);
    }

    /**Original name: LCCC0023-ID-RICH<br>*/
    public int getLccc0023IdRich() {
        return readPackedAsInt(Pos.LCCC0023_ID_RICH, Len.Int.LCCC0023_ID_RICH);
    }

    public byte[] getLccc0023IdRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0023_ID_RICH, Pos.LCCC0023_ID_RICH);
        return buffer;
    }

    public void initLccc0023IdRichSpaces() {
        fill(Pos.LCCC0023_ID_RICH, Len.LCCC0023_ID_RICH, Types.SPACE_CHAR);
    }

    public void setLccc0023IdRichNull(String lccc0023IdRichNull) {
        writeString(Pos.LCCC0023_ID_RICH_NULL, lccc0023IdRichNull, Len.LCCC0023_ID_RICH_NULL);
    }

    /**Original name: LCCC0023-ID-RICH-NULL<br>*/
    public String getLccc0023IdRichNull() {
        return readString(Pos.LCCC0023_ID_RICH_NULL, Len.LCCC0023_ID_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_RICH = 1;
        public static final int LCCC0023_ID_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_RICH = 5;
        public static final int LCCC0023_ID_RICH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0023_ID_RICH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
