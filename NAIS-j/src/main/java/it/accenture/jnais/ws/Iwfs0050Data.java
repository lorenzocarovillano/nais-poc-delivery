package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.WsCampoOutput;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IWFS0050<br>
 * Generated as a class for rule WS.<br>*/
public class Iwfs0050Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM
    private String wkPgm = "IWFS0050";
    //Original name: WK-LABEL
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);
    //Original name: WK-CONT-NEG
    private short wkContNeg = DefaultValues.BIN_SHORT_VAL;
    /**Original name: IND-STRINGA<br>
	 * <pre>**************************************************************
	 *  INDICI
	 * **************************************************************</pre>*/
    private short indStringa = DefaultValues.SHORT_VAL;
    //Original name: IND-INTERI
    private short indInteri = DefaultValues.SHORT_VAL;
    public static final short INTERI = ((short)13);
    //Original name: IND-DECIMALI
    private short indDecimali = DefaultValues.SHORT_VAL;
    public static final short DECIMALI = ((short)14);
    /**Original name: WK-LIMITE-STRINGA<br>
	 * <pre>**************************************************************
	 *  LIMITI
	 * **************************************************************</pre>*/
    private String wkLimiteStringa = "20";
    //Original name: WK-START-RICERCA
    private String wkStartRicerca = DefaultValues.stringVal(Len.WK_START_RICERCA);
    /**Original name: VIRGOLA<br>
	 * <pre>**************************************************************
	 *  COMODO
	 * **************************************************************</pre>*/
    private char virgola = ',';
    //Original name: POSIZIONE-VIRGOLA
    private String posizioneVirgola = "00";
    //Original name: WS-CAMPO-OUTPUT
    private WsCampoOutput wsCampoOutput = new WsCampoOutput();
    //Original name: WS-ARRAY-STRINGA-INPUT
    private String wsArrayStringaInput = DefaultValues.stringVal(Len.WS_ARRAY_STRINGA_INPUT);

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public void setWkContNeg(short wkContNeg) {
        this.wkContNeg = wkContNeg;
    }

    public short getWkContNeg() {
        return this.wkContNeg;
    }

    public void setIndStringa(short indStringa) {
        this.indStringa = indStringa;
    }

    public short getIndStringa() {
        return this.indStringa;
    }

    public void setIndInteri(short indInteri) {
        this.indInteri = indInteri;
    }

    public short getIndInteri() {
        return this.indInteri;
    }

    public void setInteri() {
        indInteri = INTERI;
    }

    public void setIndDecimali(short indDecimali) {
        this.indDecimali = indDecimali;
    }

    public short getIndDecimali() {
        return this.indDecimali;
    }

    public void setDecimali() {
        indDecimali = DECIMALI;
    }

    public short getWkLimiteStringa() {
        return NumericDisplay.asShort(this.wkLimiteStringa);
    }

    public String getWkLimiteStringaFormatted() {
        return this.wkLimiteStringa;
    }

    public void setWkStartRicerca(short wkStartRicerca) {
        this.wkStartRicerca = NumericDisplay.asString(wkStartRicerca, Len.WK_START_RICERCA);
    }

    public void setWkStartRicercaFormatted(String wkStartRicerca) {
        this.wkStartRicerca = Trunc.toUnsignedNumeric(wkStartRicerca, Len.WK_START_RICERCA);
    }

    public short getWkStartRicerca() {
        return NumericDisplay.asShort(this.wkStartRicerca);
    }

    public char getVirgola() {
        return this.virgola;
    }

    public void setPosizioneVirgola(short posizioneVirgola) {
        this.posizioneVirgola = NumericDisplay.asString(posizioneVirgola, Len.POSIZIONE_VIRGOLA);
    }

    public short getPosizioneVirgola() {
        return NumericDisplay.asShort(this.posizioneVirgola);
    }

    public String getPosizioneVirgolaFormatted() {
        return this.posizioneVirgola;
    }

    public void setWsArrayStringaInput(String wsArrayStringaInput) {
        this.wsArrayStringaInput = Functions.subString(wsArrayStringaInput, Len.WS_ARRAY_STRINGA_INPUT);
    }

    public String getWsArrayStringaInput() {
        return this.wsArrayStringaInput;
    }

    public String getWsArrayStringaInputFormatted() {
        return Functions.padBlanks(getWsArrayStringaInput(), Len.WS_ARRAY_STRINGA_INPUT);
    }

    public WsCampoOutput getWsCampoOutput() {
        return wsCampoOutput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_NOME_TABELLA = 15;
        public static final int WK_LABEL = 30;
        public static final int WK_START_RICERCA = 2;
        public static final int WS_ARRAY_STRINGA_INPUT = 20;
        public static final int POSIZIONE_VIRGOLA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
