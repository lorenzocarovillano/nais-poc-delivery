package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-SP-Z-COUP-EMIS<br>
 * Variable: B03-QTZ-SP-Z-COUP-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzSpZCoupEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzSpZCoupEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_SP_Z_COUP_EMIS;
    }

    public void setB03QtzSpZCoupEmis(AfDecimal b03QtzSpZCoupEmis) {
        writeDecimalAsPacked(Pos.B03_QTZ_SP_Z_COUP_EMIS, b03QtzSpZCoupEmis.copy());
    }

    public void setB03QtzSpZCoupEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_SP_Z_COUP_EMIS, Pos.B03_QTZ_SP_Z_COUP_EMIS);
    }

    /**Original name: B03-QTZ-SP-Z-COUP-EMIS<br>*/
    public AfDecimal getB03QtzSpZCoupEmis() {
        return readPackedAsDecimal(Pos.B03_QTZ_SP_Z_COUP_EMIS, Len.Int.B03_QTZ_SP_Z_COUP_EMIS, Len.Fract.B03_QTZ_SP_Z_COUP_EMIS);
    }

    public byte[] getB03QtzSpZCoupEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_SP_Z_COUP_EMIS, Pos.B03_QTZ_SP_Z_COUP_EMIS);
        return buffer;
    }

    public void setB03QtzSpZCoupEmisNull(String b03QtzSpZCoupEmisNull) {
        writeString(Pos.B03_QTZ_SP_Z_COUP_EMIS_NULL, b03QtzSpZCoupEmisNull, Len.B03_QTZ_SP_Z_COUP_EMIS_NULL);
    }

    /**Original name: B03-QTZ-SP-Z-COUP-EMIS-NULL<br>*/
    public String getB03QtzSpZCoupEmisNull() {
        return readString(Pos.B03_QTZ_SP_Z_COUP_EMIS_NULL, Len.B03_QTZ_SP_Z_COUP_EMIS_NULL);
    }

    public String getB03QtzSpZCoupEmisNullFormatted() {
        return Functions.padBlanks(getB03QtzSpZCoupEmisNull(), Len.B03_QTZ_SP_Z_COUP_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_COUP_EMIS = 1;
        public static final int B03_QTZ_SP_Z_COUP_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_SP_Z_COUP_EMIS = 7;
        public static final int B03_QTZ_SP_Z_COUP_EMIS_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_COUP_EMIS = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_SP_Z_COUP_EMIS = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
