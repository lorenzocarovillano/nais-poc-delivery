package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PRE-SOLO-RSH<br>
 * Variable: WTDR-TOT-PRE-SOLO-RSH from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PRE_SOLO_RSH;
    }

    public void setWtdrTotPreSoloRsh(AfDecimal wtdrTotPreSoloRsh) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PRE_SOLO_RSH, wtdrTotPreSoloRsh.copy());
    }

    /**Original name: WTDR-TOT-PRE-SOLO-RSH<br>*/
    public AfDecimal getWtdrTotPreSoloRsh() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PRE_SOLO_RSH, Len.Int.WTDR_TOT_PRE_SOLO_RSH, Len.Fract.WTDR_TOT_PRE_SOLO_RSH);
    }

    public void setWtdrTotPreSoloRshNull(String wtdrTotPreSoloRshNull) {
        writeString(Pos.WTDR_TOT_PRE_SOLO_RSH_NULL, wtdrTotPreSoloRshNull, Len.WTDR_TOT_PRE_SOLO_RSH_NULL);
    }

    /**Original name: WTDR-TOT-PRE-SOLO-RSH-NULL<br>*/
    public String getWtdrTotPreSoloRshNull() {
        return readString(Pos.WTDR_TOT_PRE_SOLO_RSH_NULL, Len.WTDR_TOT_PRE_SOLO_RSH_NULL);
    }

    public String getWtdrTotPreSoloRshNullFormatted() {
        return Functions.padBlanks(getWtdrTotPreSoloRshNull(), Len.WTDR_TOT_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_SOLO_RSH = 1;
        public static final int WTDR_TOT_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_SOLO_RSH = 8;
        public static final int WTDR_TOT_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
