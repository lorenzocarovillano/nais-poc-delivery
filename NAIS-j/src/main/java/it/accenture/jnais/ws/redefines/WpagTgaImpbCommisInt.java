package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-TGA-IMPB-COMMIS-INT<br>
 * Variable: WPAG-TGA-IMPB-COMMIS-INT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpbCommisInt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpbCommisInt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMPB_COMMIS_INT;
    }

    public void setWpagTgaImpbCommisIntFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMPB_COMMIS_INT, Pos.WPAG_TGA_IMPB_COMMIS_INT);
    }

    public byte[] getWpagTgaImpbCommisIntAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMPB_COMMIS_INT, Pos.WPAG_TGA_IMPB_COMMIS_INT);
        return buffer;
    }

    public void initWpagTgaImpbCommisIntSpaces() {
        fill(Pos.WPAG_TGA_IMPB_COMMIS_INT, Len.WPAG_TGA_IMPB_COMMIS_INT, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpbCommisIntNull(String wpagTgaImpbCommisIntNull) {
        writeString(Pos.WPAG_TGA_IMPB_COMMIS_INT_NULL, wpagTgaImpbCommisIntNull, Len.WPAG_TGA_IMPB_COMMIS_INT_NULL);
    }

    /**Original name: WPAG-TGA-IMPB-COMMIS-INT-NULL<br>*/
    public String getWpagTgaImpbCommisIntNull() {
        return readString(Pos.WPAG_TGA_IMPB_COMMIS_INT_NULL, Len.WPAG_TGA_IMPB_COMMIS_INT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMPB_COMMIS_INT = 1;
        public static final int WPAG_TGA_IMPB_COMMIS_INT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMPB_COMMIS_INT = 8;
        public static final int WPAG_TGA_IMPB_COMMIS_INT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
