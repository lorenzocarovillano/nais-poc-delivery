package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-RIAT<br>
 * Variable: WPCO-DT-ULT-BOLL-RIAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_RIAT;
    }

    public void setWpcoDtUltBollRiat(int wpcoDtUltBollRiat) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_RIAT, wpcoDtUltBollRiat, Len.Int.WPCO_DT_ULT_BOLL_RIAT);
    }

    public void setDpcoDtUltBollRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RIAT, Pos.WPCO_DT_ULT_BOLL_RIAT);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RIAT<br>*/
    public int getWpcoDtUltBollRiat() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_RIAT, Len.Int.WPCO_DT_ULT_BOLL_RIAT);
    }

    public byte[] getWpcoDtUltBollRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RIAT, Pos.WPCO_DT_ULT_BOLL_RIAT);
        return buffer;
    }

    public void setWpcoDtUltBollRiatNull(String wpcoDtUltBollRiatNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_RIAT_NULL, wpcoDtUltBollRiatNull, Len.WPCO_DT_ULT_BOLL_RIAT_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RIAT-NULL<br>*/
    public String getWpcoDtUltBollRiatNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_RIAT_NULL, Len.WPCO_DT_ULT_BOLL_RIAT_NULL);
    }

    public String getWpcoDtUltBollRiatNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollRiatNull(), Len.WPCO_DT_ULT_BOLL_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RIAT = 1;
        public static final int WPCO_DT_ULT_BOLL_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RIAT = 5;
        public static final int WPCO_DT_ULT_BOLL_RIAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_RIAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
