package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-DATA-FINE<br>
 * Variable: WK-DATA-FINE from program LVVS2720<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkDataFine {

    //==== PROPERTIES ====
    //Original name: WK-ANNO-FINE
    private String annoFine = DefaultValues.stringVal(Len.ANNO_FINE);
    //Original name: WK-MESE-FINE
    private String meseFine = DefaultValues.stringVal(Len.MESE_FINE);
    //Original name: WK-GG-FINE
    private String ggFine = DefaultValues.stringVal(Len.GG_FINE);

    //==== METHODS ====
    public byte[] getWkDataFineBytes() {
        byte[] buffer = new byte[Len.WK_DATA_FINE];
        return getWkDataFineBytes(buffer, 1);
    }

    public byte[] getWkDataFineBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, annoFine, Len.ANNO_FINE);
        position += Len.ANNO_FINE;
        MarshalByte.writeString(buffer, position, meseFine, Len.MESE_FINE);
        position += Len.MESE_FINE;
        MarshalByte.writeString(buffer, position, ggFine, Len.GG_FINE);
        return buffer;
    }

    public void setAnnoFineFormatted(String annoFine) {
        this.annoFine = Trunc.toUnsignedNumeric(annoFine, Len.ANNO_FINE);
    }

    public short getAnnoFine() {
        return NumericDisplay.asShort(this.annoFine);
    }

    public void setMeseFine(short meseFine) {
        this.meseFine = NumericDisplay.asString(meseFine, Len.MESE_FINE);
    }

    public short getMeseFine() {
        return NumericDisplay.asShort(this.meseFine);
    }

    public void setGgFine(short ggFine) {
        this.ggFine = NumericDisplay.asString(ggFine, Len.GG_FINE);
    }

    public short getGgFine() {
        return NumericDisplay.asShort(this.ggFine);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNO_FINE = 4;
        public static final int MESE_FINE = 2;
        public static final int GG_FINE = 2;
        public static final int WK_DATA_FINE = ANNO_FINE + MESE_FINE + GG_FINE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
