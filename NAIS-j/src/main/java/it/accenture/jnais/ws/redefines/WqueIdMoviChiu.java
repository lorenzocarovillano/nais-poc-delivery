package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WQUE-ID-MOVI-CHIU<br>
 * Variable: WQUE-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WqueIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WqueIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WQUE_ID_MOVI_CHIU;
    }

    public void setWqueIdMoviChiu(int wqueIdMoviChiu) {
        writeIntAsPacked(Pos.WQUE_ID_MOVI_CHIU, wqueIdMoviChiu, Len.Int.WQUE_ID_MOVI_CHIU);
    }

    public void setWqueIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WQUE_ID_MOVI_CHIU, Pos.WQUE_ID_MOVI_CHIU);
    }

    /**Original name: WQUE-ID-MOVI-CHIU<br>*/
    public int getWqueIdMoviChiu() {
        return readPackedAsInt(Pos.WQUE_ID_MOVI_CHIU, Len.Int.WQUE_ID_MOVI_CHIU);
    }

    public byte[] getWqueIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WQUE_ID_MOVI_CHIU, Pos.WQUE_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWqueIdMoviChiuSpaces() {
        fill(Pos.WQUE_ID_MOVI_CHIU, Len.WQUE_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWqueIdMoviChiuNull(String wqueIdMoviChiuNull) {
        writeString(Pos.WQUE_ID_MOVI_CHIU_NULL, wqueIdMoviChiuNull, Len.WQUE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WQUE-ID-MOVI-CHIU-NULL<br>*/
    public String getWqueIdMoviChiuNull() {
        return readString(Pos.WQUE_ID_MOVI_CHIU_NULL, Len.WQUE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WQUE_ID_MOVI_CHIU = 1;
        public static final int WQUE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WQUE_ID_MOVI_CHIU = 5;
        public static final int WQUE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WQUE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
