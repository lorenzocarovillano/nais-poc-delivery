package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IBilaVarCalcP;
import it.accenture.jnais.ws.redefines.B04IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B04ValImp;
import it.accenture.jnais.ws.redefines.B04ValPc;

/**Original name: BILA-VAR-CALC-P<br>
 * Variable: BILA-VAR-CALC-P from copybook IDBVB041<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BilaVarCalcPIdbsb040 extends SerializableParameter implements IBilaVarCalcP {

    //==== PROPERTIES ====
    //Original name: B04-ID-BILA-VAR-CALC-P
    private int b04IdBilaVarCalcP = DefaultValues.INT_VAL;
    //Original name: B04-COD-COMP-ANIA
    private int b04CodCompAnia = DefaultValues.INT_VAL;
    //Original name: B04-ID-RICH-ESTRAZ-MAS
    private int b04IdRichEstrazMas = DefaultValues.INT_VAL;
    //Original name: B04-ID-RICH-ESTRAZ-AGG
    private B04IdRichEstrazAgg b04IdRichEstrazAgg = new B04IdRichEstrazAgg();
    //Original name: B04-DT-RIS
    private int b04DtRis = DefaultValues.INT_VAL;
    //Original name: B04-ID-POLI
    private int b04IdPoli = DefaultValues.INT_VAL;
    //Original name: B04-ID-ADES
    private int b04IdAdes = DefaultValues.INT_VAL;
    //Original name: B04-PROG-SCHEDA-VALOR
    private int b04ProgSchedaValor = DefaultValues.INT_VAL;
    //Original name: B04-TP-RGM-FISC
    private String b04TpRgmFisc = DefaultValues.stringVal(Len.B04_TP_RGM_FISC);
    //Original name: B04-DT-INI-VLDT-PROD
    private int b04DtIniVldtProd = DefaultValues.INT_VAL;
    //Original name: B04-COD-VAR
    private String b04CodVar = DefaultValues.stringVal(Len.B04_COD_VAR);
    //Original name: B04-TP-D
    private char b04TpD = DefaultValues.CHAR_VAL;
    //Original name: B04-VAL-IMP
    private B04ValImp b04ValImp = new B04ValImp();
    //Original name: B04-VAL-PC
    private B04ValPc b04ValPc = new B04ValPc();
    //Original name: B04-VAL-STRINGA
    private String b04ValStringa = DefaultValues.stringVal(Len.B04_VAL_STRINGA);
    //Original name: B04-DS-OPER-SQL
    private char b04DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: B04-DS-VER
    private int b04DsVer = DefaultValues.INT_VAL;
    //Original name: B04-DS-TS-CPTZ
    private long b04DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: B04-DS-UTENTE
    private String b04DsUtente = DefaultValues.stringVal(Len.B04_DS_UTENTE);
    //Original name: B04-DS-STATO-ELAB
    private char b04DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: B04-AREA-D-VALOR-VAR-LEN
    private short b04AreaDValorVarLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: B04-AREA-D-VALOR-VAR
    private String b04AreaDValorVar = DefaultValues.stringVal(Len.B04_AREA_D_VALOR_VAR);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BILA_VAR_CALC_P;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBilaVarCalcPBytes(buf);
    }

    public void setBilaVarCalcPBytes(byte[] buffer) {
        setBilaVarCalcPBytes(buffer, 1);
    }

    public byte[] getBilaVarCalcPBytes() {
        byte[] buffer = new byte[Len.BILA_VAR_CALC_P];
        return getBilaVarCalcPBytes(buffer, 1);
    }

    public void setBilaVarCalcPBytes(byte[] buffer, int offset) {
        int position = offset;
        b04IdBilaVarCalcP = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_ID_BILA_VAR_CALC_P, 0);
        position += Len.B04_ID_BILA_VAR_CALC_P;
        b04CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_COD_COMP_ANIA, 0);
        position += Len.B04_COD_COMP_ANIA;
        b04IdRichEstrazMas = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B04_ID_RICH_ESTRAZ_MAS;
        b04IdRichEstrazAgg.setB04IdRichEstrazAggFromBuffer(buffer, position);
        position += B04IdRichEstrazAgg.Len.B04_ID_RICH_ESTRAZ_AGG;
        b04DtRis = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_DT_RIS, 0);
        position += Len.B04_DT_RIS;
        b04IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_ID_POLI, 0);
        position += Len.B04_ID_POLI;
        b04IdAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_ID_ADES, 0);
        position += Len.B04_ID_ADES;
        b04ProgSchedaValor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_PROG_SCHEDA_VALOR, 0);
        position += Len.B04_PROG_SCHEDA_VALOR;
        b04TpRgmFisc = MarshalByte.readString(buffer, position, Len.B04_TP_RGM_FISC);
        position += Len.B04_TP_RGM_FISC;
        b04DtIniVldtProd = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_DT_INI_VLDT_PROD, 0);
        position += Len.B04_DT_INI_VLDT_PROD;
        b04CodVar = MarshalByte.readString(buffer, position, Len.B04_COD_VAR);
        position += Len.B04_COD_VAR;
        b04TpD = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b04ValImp.setB04ValImpFromBuffer(buffer, position);
        position += B04ValImp.Len.B04_VAL_IMP;
        b04ValPc.setB04ValPcFromBuffer(buffer, position);
        position += B04ValPc.Len.B04_VAL_PC;
        b04ValStringa = MarshalByte.readString(buffer, position, Len.B04_VAL_STRINGA);
        position += Len.B04_VAL_STRINGA;
        b04DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        b04DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.B04_DS_VER, 0);
        position += Len.B04_DS_VER;
        b04DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.B04_DS_TS_CPTZ, 0);
        position += Len.B04_DS_TS_CPTZ;
        b04DsUtente = MarshalByte.readString(buffer, position, Len.B04_DS_UTENTE);
        position += Len.B04_DS_UTENTE;
        b04DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setB04AreaDValorVarVcharBytes(buffer, position);
    }

    public byte[] getBilaVarCalcPBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, b04IdBilaVarCalcP, Len.Int.B04_ID_BILA_VAR_CALC_P, 0);
        position += Len.B04_ID_BILA_VAR_CALC_P;
        MarshalByte.writeIntAsPacked(buffer, position, b04CodCompAnia, Len.Int.B04_COD_COMP_ANIA, 0);
        position += Len.B04_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, b04IdRichEstrazMas, Len.Int.B04_ID_RICH_ESTRAZ_MAS, 0);
        position += Len.B04_ID_RICH_ESTRAZ_MAS;
        b04IdRichEstrazAgg.getB04IdRichEstrazAggAsBuffer(buffer, position);
        position += B04IdRichEstrazAgg.Len.B04_ID_RICH_ESTRAZ_AGG;
        MarshalByte.writeIntAsPacked(buffer, position, b04DtRis, Len.Int.B04_DT_RIS, 0);
        position += Len.B04_DT_RIS;
        MarshalByte.writeIntAsPacked(buffer, position, b04IdPoli, Len.Int.B04_ID_POLI, 0);
        position += Len.B04_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, b04IdAdes, Len.Int.B04_ID_ADES, 0);
        position += Len.B04_ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, b04ProgSchedaValor, Len.Int.B04_PROG_SCHEDA_VALOR, 0);
        position += Len.B04_PROG_SCHEDA_VALOR;
        MarshalByte.writeString(buffer, position, b04TpRgmFisc, Len.B04_TP_RGM_FISC);
        position += Len.B04_TP_RGM_FISC;
        MarshalByte.writeIntAsPacked(buffer, position, b04DtIniVldtProd, Len.Int.B04_DT_INI_VLDT_PROD, 0);
        position += Len.B04_DT_INI_VLDT_PROD;
        MarshalByte.writeString(buffer, position, b04CodVar, Len.B04_COD_VAR);
        position += Len.B04_COD_VAR;
        MarshalByte.writeChar(buffer, position, b04TpD);
        position += Types.CHAR_SIZE;
        b04ValImp.getB04ValImpAsBuffer(buffer, position);
        position += B04ValImp.Len.B04_VAL_IMP;
        b04ValPc.getB04ValPcAsBuffer(buffer, position);
        position += B04ValPc.Len.B04_VAL_PC;
        MarshalByte.writeString(buffer, position, b04ValStringa, Len.B04_VAL_STRINGA);
        position += Len.B04_VAL_STRINGA;
        MarshalByte.writeChar(buffer, position, b04DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, b04DsVer, Len.Int.B04_DS_VER, 0);
        position += Len.B04_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, b04DsTsCptz, Len.Int.B04_DS_TS_CPTZ, 0);
        position += Len.B04_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, b04DsUtente, Len.B04_DS_UTENTE);
        position += Len.B04_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, b04DsStatoElab);
        position += Types.CHAR_SIZE;
        getB04AreaDValorVarVcharBytes(buffer, position);
        return buffer;
    }

    @Override
    public void setB04IdBilaVarCalcP(int b04IdBilaVarCalcP) {
        this.b04IdBilaVarCalcP = b04IdBilaVarCalcP;
    }

    @Override
    public int getB04IdBilaVarCalcP() {
        return this.b04IdBilaVarCalcP;
    }

    public void setB04CodCompAnia(int b04CodCompAnia) {
        this.b04CodCompAnia = b04CodCompAnia;
    }

    public int getB04CodCompAnia() {
        return this.b04CodCompAnia;
    }

    public void setB04IdRichEstrazMas(int b04IdRichEstrazMas) {
        this.b04IdRichEstrazMas = b04IdRichEstrazMas;
    }

    public int getB04IdRichEstrazMas() {
        return this.b04IdRichEstrazMas;
    }

    public void setB04DtRis(int b04DtRis) {
        this.b04DtRis = b04DtRis;
    }

    public int getB04DtRis() {
        return this.b04DtRis;
    }

    public void setB04IdPoli(int b04IdPoli) {
        this.b04IdPoli = b04IdPoli;
    }

    public int getB04IdPoli() {
        return this.b04IdPoli;
    }

    public void setB04IdAdes(int b04IdAdes) {
        this.b04IdAdes = b04IdAdes;
    }

    public int getB04IdAdes() {
        return this.b04IdAdes;
    }

    public void setB04ProgSchedaValor(int b04ProgSchedaValor) {
        this.b04ProgSchedaValor = b04ProgSchedaValor;
    }

    public int getB04ProgSchedaValor() {
        return this.b04ProgSchedaValor;
    }

    public void setB04TpRgmFisc(String b04TpRgmFisc) {
        this.b04TpRgmFisc = Functions.subString(b04TpRgmFisc, Len.B04_TP_RGM_FISC);
    }

    public String getB04TpRgmFisc() {
        return this.b04TpRgmFisc;
    }

    public void setB04DtIniVldtProd(int b04DtIniVldtProd) {
        this.b04DtIniVldtProd = b04DtIniVldtProd;
    }

    public int getB04DtIniVldtProd() {
        return this.b04DtIniVldtProd;
    }

    public void setB04CodVar(String b04CodVar) {
        this.b04CodVar = Functions.subString(b04CodVar, Len.B04_COD_VAR);
    }

    public String getB04CodVar() {
        return this.b04CodVar;
    }

    public void setB04TpD(char b04TpD) {
        this.b04TpD = b04TpD;
    }

    public char getB04TpD() {
        return this.b04TpD;
    }

    public void setB04ValStringa(String b04ValStringa) {
        this.b04ValStringa = Functions.subString(b04ValStringa, Len.B04_VAL_STRINGA);
    }

    public String getB04ValStringa() {
        return this.b04ValStringa;
    }

    public void setB04DsOperSql(char b04DsOperSql) {
        this.b04DsOperSql = b04DsOperSql;
    }

    public void setB04DsOperSqlFormatted(String b04DsOperSql) {
        setB04DsOperSql(Functions.charAt(b04DsOperSql, Types.CHAR_SIZE));
    }

    public char getB04DsOperSql() {
        return this.b04DsOperSql;
    }

    public void setB04DsVer(int b04DsVer) {
        this.b04DsVer = b04DsVer;
    }

    public int getB04DsVer() {
        return this.b04DsVer;
    }

    public void setB04DsTsCptz(long b04DsTsCptz) {
        this.b04DsTsCptz = b04DsTsCptz;
    }

    public long getB04DsTsCptz() {
        return this.b04DsTsCptz;
    }

    public void setB04DsUtente(String b04DsUtente) {
        this.b04DsUtente = Functions.subString(b04DsUtente, Len.B04_DS_UTENTE);
    }

    public String getB04DsUtente() {
        return this.b04DsUtente;
    }

    public void setB04DsStatoElab(char b04DsStatoElab) {
        this.b04DsStatoElab = b04DsStatoElab;
    }

    public void setB04DsStatoElabFormatted(String b04DsStatoElab) {
        setB04DsStatoElab(Functions.charAt(b04DsStatoElab, Types.CHAR_SIZE));
    }

    public char getB04DsStatoElab() {
        return this.b04DsStatoElab;
    }

    public void setB04AreaDValorVarVcharFormatted(String data) {
        byte[] buffer = new byte[Len.B04_AREA_D_VALOR_VAR_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.B04_AREA_D_VALOR_VAR_VCHAR);
        setB04AreaDValorVarVcharBytes(buffer, 1);
    }

    public String getB04AreaDValorVarVcharFormatted() {
        return MarshalByteExt.bufferToStr(getB04AreaDValorVarVcharBytes());
    }

    /**Original name: B04-AREA-D-VALOR-VAR-VCHAR<br>*/
    public byte[] getB04AreaDValorVarVcharBytes() {
        byte[] buffer = new byte[Len.B04_AREA_D_VALOR_VAR_VCHAR];
        return getB04AreaDValorVarVcharBytes(buffer, 1);
    }

    public void setB04AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        b04AreaDValorVarLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        b04AreaDValorVar = MarshalByte.readString(buffer, position, Len.B04_AREA_D_VALOR_VAR);
    }

    public byte[] getB04AreaDValorVarVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, b04AreaDValorVarLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, b04AreaDValorVar, Len.B04_AREA_D_VALOR_VAR);
        return buffer;
    }

    public void setB04AreaDValorVarLen(short b04AreaDValorVarLen) {
        this.b04AreaDValorVarLen = b04AreaDValorVarLen;
    }

    public short getB04AreaDValorVarLen() {
        return this.b04AreaDValorVarLen;
    }

    public void setB04AreaDValorVar(String b04AreaDValorVar) {
        this.b04AreaDValorVar = Functions.subString(b04AreaDValorVar, Len.B04_AREA_D_VALOR_VAR);
    }

    public String getB04AreaDValorVar() {
        return this.b04AreaDValorVar;
    }

    @Override
    public String getAreaDValorVarVchar() {
        throw new FieldNotMappedException("areaDValorVarVchar");
    }

    @Override
    public void setAreaDValorVarVchar(String areaDValorVarVchar) {
        throw new FieldNotMappedException("areaDValorVarVchar");
    }

    @Override
    public String getAreaDValorVarVcharObj() {
        return getAreaDValorVarVchar();
    }

    @Override
    public void setAreaDValorVarVcharObj(String areaDValorVarVcharObj) {
        setAreaDValorVarVchar(areaDValorVarVcharObj);
    }

    public B04IdRichEstrazAgg getB04IdRichEstrazAgg() {
        return b04IdRichEstrazAgg;
    }

    public B04ValImp getB04ValImp() {
        return b04ValImp;
    }

    public B04ValPc getB04ValPc() {
        return b04ValPc;
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodVar() {
        throw new FieldNotMappedException("codVar");
    }

    @Override
    public void setCodVar(String codVar) {
        throw new FieldNotMappedException("codVar");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtIniVldtProdDb() {
        throw new FieldNotMappedException("dtIniVldtProdDb");
    }

    @Override
    public void setDtIniVldtProdDb(String dtIniVldtProdDb) {
        throw new FieldNotMappedException("dtIniVldtProdDb");
    }

    @Override
    public String getDtRisDb() {
        throw new FieldNotMappedException("dtRisDb");
    }

    @Override
    public void setDtRisDb(String dtRisDb) {
        throw new FieldNotMappedException("dtRisDb");
    }

    @Override
    public int getIdAdes() {
        throw new FieldNotMappedException("idAdes");
    }

    @Override
    public void setIdAdes(int idAdes) {
        throw new FieldNotMappedException("idAdes");
    }

    @Override
    public int getIdPoli() {
        throw new FieldNotMappedException("idPoli");
    }

    @Override
    public void setIdPoli(int idPoli) {
        throw new FieldNotMappedException("idPoli");
    }

    @Override
    public int getIdRichEstrazAgg() {
        throw new FieldNotMappedException("idRichEstrazAgg");
    }

    @Override
    public void setIdRichEstrazAgg(int idRichEstrazAgg) {
        throw new FieldNotMappedException("idRichEstrazAgg");
    }

    @Override
    public Integer getIdRichEstrazAggObj() {
        return ((Integer)getIdRichEstrazAgg());
    }

    @Override
    public void setIdRichEstrazAggObj(Integer idRichEstrazAggObj) {
        setIdRichEstrazAgg(((int)idRichEstrazAggObj));
    }

    @Override
    public int getIdRichEstrazMas() {
        throw new FieldNotMappedException("idRichEstrazMas");
    }

    @Override
    public void setIdRichEstrazMas(int idRichEstrazMas) {
        throw new FieldNotMappedException("idRichEstrazMas");
    }

    @Override
    public int getProgSchedaValor() {
        throw new FieldNotMappedException("progSchedaValor");
    }

    @Override
    public void setProgSchedaValor(int progSchedaValor) {
        throw new FieldNotMappedException("progSchedaValor");
    }

    @Override
    public char getTpD() {
        throw new FieldNotMappedException("tpD");
    }

    @Override
    public void setTpD(char tpD) {
        throw new FieldNotMappedException("tpD");
    }

    @Override
    public String getTpRgmFisc() {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public AfDecimal getValImp() {
        throw new FieldNotMappedException("valImp");
    }

    @Override
    public void setValImp(AfDecimal valImp) {
        throw new FieldNotMappedException("valImp");
    }

    @Override
    public AfDecimal getValImpObj() {
        return getValImp();
    }

    @Override
    public void setValImpObj(AfDecimal valImpObj) {
        setValImp(new AfDecimal(valImpObj, 18, 7));
    }

    @Override
    public AfDecimal getValPc() {
        throw new FieldNotMappedException("valPc");
    }

    @Override
    public void setValPc(AfDecimal valPc) {
        throw new FieldNotMappedException("valPc");
    }

    @Override
    public AfDecimal getValPcObj() {
        return getValPc();
    }

    @Override
    public void setValPcObj(AfDecimal valPcObj) {
        setValPc(new AfDecimal(valPcObj, 14, 9));
    }

    @Override
    public String getValStringa() {
        throw new FieldNotMappedException("valStringa");
    }

    @Override
    public void setValStringa(String valStringa) {
        throw new FieldNotMappedException("valStringa");
    }

    @Override
    public String getValStringaObj() {
        return getValStringa();
    }

    @Override
    public void setValStringaObj(String valStringaObj) {
        setValStringa(valStringaObj);
    }

    @Override
    public byte[] serialize() {
        return getBilaVarCalcPBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int B04_ID_BILA_VAR_CALC_P = 5;
        public static final int B04_COD_COMP_ANIA = 3;
        public static final int B04_ID_RICH_ESTRAZ_MAS = 5;
        public static final int B04_DT_RIS = 5;
        public static final int B04_ID_POLI = 5;
        public static final int B04_ID_ADES = 5;
        public static final int B04_PROG_SCHEDA_VALOR = 5;
        public static final int B04_TP_RGM_FISC = 2;
        public static final int B04_DT_INI_VLDT_PROD = 5;
        public static final int B04_COD_VAR = 30;
        public static final int B04_TP_D = 1;
        public static final int B04_VAL_STRINGA = 60;
        public static final int B04_DS_OPER_SQL = 1;
        public static final int B04_DS_VER = 5;
        public static final int B04_DS_TS_CPTZ = 10;
        public static final int B04_DS_UTENTE = 20;
        public static final int B04_DS_STATO_ELAB = 1;
        public static final int B04_AREA_D_VALOR_VAR_LEN = 2;
        public static final int B04_AREA_D_VALOR_VAR = 4000;
        public static final int B04_AREA_D_VALOR_VAR_VCHAR = B04_AREA_D_VALOR_VAR_LEN + B04_AREA_D_VALOR_VAR;
        public static final int BILA_VAR_CALC_P = B04_ID_BILA_VAR_CALC_P + B04_COD_COMP_ANIA + B04_ID_RICH_ESTRAZ_MAS + B04IdRichEstrazAgg.Len.B04_ID_RICH_ESTRAZ_AGG + B04_DT_RIS + B04_ID_POLI + B04_ID_ADES + B04_PROG_SCHEDA_VALOR + B04_TP_RGM_FISC + B04_DT_INI_VLDT_PROD + B04_COD_VAR + B04_TP_D + B04ValImp.Len.B04_VAL_IMP + B04ValPc.Len.B04_VAL_PC + B04_VAL_STRINGA + B04_DS_OPER_SQL + B04_DS_VER + B04_DS_TS_CPTZ + B04_DS_UTENTE + B04_DS_STATO_ELAB + B04_AREA_D_VALOR_VAR_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B04_ID_BILA_VAR_CALC_P = 9;
            public static final int B04_COD_COMP_ANIA = 5;
            public static final int B04_ID_RICH_ESTRAZ_MAS = 9;
            public static final int B04_DT_RIS = 8;
            public static final int B04_ID_POLI = 9;
            public static final int B04_ID_ADES = 9;
            public static final int B04_PROG_SCHEDA_VALOR = 9;
            public static final int B04_DT_INI_VLDT_PROD = 8;
            public static final int B04_DS_VER = 9;
            public static final int B04_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
