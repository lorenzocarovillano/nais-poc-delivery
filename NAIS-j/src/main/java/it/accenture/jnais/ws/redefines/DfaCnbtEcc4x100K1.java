package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-CNBT-ECC-4X100-K1<br>
 * Variable: DFA-CNBT-ECC-4X100-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaCnbtEcc4x100K1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaCnbtEcc4x100K1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_CNBT_ECC4X100_K1;
    }

    public void setDfaCnbtEcc4x100K1(AfDecimal dfaCnbtEcc4x100K1) {
        writeDecimalAsPacked(Pos.DFA_CNBT_ECC4X100_K1, dfaCnbtEcc4x100K1.copy());
    }

    public void setDfaCnbtEcc4x100K1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_CNBT_ECC4X100_K1, Pos.DFA_CNBT_ECC4X100_K1);
    }

    /**Original name: DFA-CNBT-ECC-4X100-K1<br>*/
    public AfDecimal getDfaCnbtEcc4x100K1() {
        return readPackedAsDecimal(Pos.DFA_CNBT_ECC4X100_K1, Len.Int.DFA_CNBT_ECC4X100_K1, Len.Fract.DFA_CNBT_ECC4X100_K1);
    }

    public byte[] getDfaCnbtEcc4x100K1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_CNBT_ECC4X100_K1, Pos.DFA_CNBT_ECC4X100_K1);
        return buffer;
    }

    public void setDfaCnbtEcc4x100K1Null(String dfaCnbtEcc4x100K1Null) {
        writeString(Pos.DFA_CNBT_ECC4X100_K1_NULL, dfaCnbtEcc4x100K1Null, Len.DFA_CNBT_ECC4X100_K1_NULL);
    }

    /**Original name: DFA-CNBT-ECC-4X100-K1-NULL<br>*/
    public String getDfaCnbtEcc4x100K1Null() {
        return readString(Pos.DFA_CNBT_ECC4X100_K1_NULL, Len.DFA_CNBT_ECC4X100_K1_NULL);
    }

    public String getDfaCnbtEcc4x100K1NullFormatted() {
        return Functions.padBlanks(getDfaCnbtEcc4x100K1Null(), Len.DFA_CNBT_ECC4X100_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_CNBT_ECC4X100_K1 = 1;
        public static final int DFA_CNBT_ECC4X100_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_CNBT_ECC4X100_K1 = 8;
        public static final int DFA_CNBT_ECC4X100_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_CNBT_ECC4X100_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_CNBT_ECC4X100_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
