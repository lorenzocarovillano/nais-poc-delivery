package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.WaTabdgiorni;
import it.accenture.jnais.ws.redefines.WaTabdmesi;
import it.accenture.jnais.ws.redefines.WaTafestex;
import it.accenture.jnais.ws.redefines.WsData;
import it.accenture.jnais.ws.redefines.WsDataNum;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0003<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0003Data {

    //==== PROPERTIES ====
    public static final int WS_MESE_MAXOCCURS = 12;
    /**Original name: WA-INIPROG<br>
	 * <pre>**------------------------------------------------------------***</pre>*/
    private int waIniprog = 693595;
    /**Original name: WA-GG-IN-400-ANNI<br>
	 * <pre>--------------</pre>*/
    private int waGgIn400Anni = 146097;
    /**Original name: WA-GG-IN-100-ANNI<br>
	 * <pre>---------------------</pre>*/
    private int waGgIn100Anni = 36524;
    /**Original name: WA-GG-IN-4-ANNI<br>
	 * <pre>---------------------</pre>*/
    private int waGgIn4Anni = 1461;
    /**Original name: WA-TABMESI<br>
	 * <pre>-------------------
	 * --------------</pre>*/
    private String waTabmesi = "312831303130313130313031";
    //Original name: WA-TABDMESI
    private WaTabdmesi waTabdmesi = new WaTabdmesi();
    //Original name: WA-TABDGIORNI
    private WaTabdgiorni waTabdgiorni = new WaTabdgiorni();
    /**Original name: WS-DATA<br>
	 * <pre>--------------</pre>*/
    private WsData wsData = new WsData();
    //Original name: WS-DATA-NUM
    private WsDataNum wsDataNum = new WsDataNum();
    //Original name: WS-DATAP
    private int wsDatap = DefaultValues.INT_VAL;
    /**Original name: WS-ANY-NUM<br>
	 * <pre>------------</pre>*/
    private int wsAnyNum = DefaultValues.INT_VAL;
    /**Original name: WS-DATA-PASQUETTA<br>
	 * <pre>--------------</pre>*/
    private int wsDataPasquetta = DefaultValues.INT_VAL;
    /**Original name: WS-DATA-WK<br>
	 * <pre>---------------------</pre>*/
    private int wsDataWk = DefaultValues.INT_VAL;
    /**Original name: WS-DELTA<br>
	 * <pre>--------------</pre>*/
    private String wsDelta = DefaultValues.stringVal(Len.WS_DELTA);
    /**Original name: WS-DATA-MM3<br>
	 * <pre>------------</pre>*/
    private String wsDataMm3 = "000";
    /**Original name: WS-GMLAV<br>
	 * <pre>------------</pre>*/
    private String wsGmlav = DefaultValues.stringVal(Len.WS_GMLAV);
    /**Original name: WS-FMLAV<br>
	 * <pre>------------</pre>*/
    private String wsFmlav = DefaultValues.stringVal(Len.WS_FMLAV);
    /**Original name: WS-QUOTIENT<br>
	 * <pre>-----------</pre>*/
    private int wsQuotient = DefaultValues.INT_VAL;
    //Original name: WS-REMAINDER
    private int wsRemainder = DefaultValues.INT_VAL;
    //Original name: FILLER-WS-6
    private Flr2 flr2 = new Flr2();
    /**Original name: WS-MESE<br>
	 * <pre>------------</pre>*/
    private String[] wsMese = new String[WS_MESE_MAXOCCURS];
    //Original name: WS-MESI-WK
    private String wsMesiWk = DefaultValues.stringVal(Len.WS_MESI_WK);
    /**Original name: I2<br>
	 * <pre>--------------</pre>*/
    private short i2 = DefaultValues.SHORT_VAL;
    /**Original name: WS-RCODE<br>
	 * <pre>------</pre>*/
    private String wsRcode = DefaultValues.stringVal(Len.WS_RCODE);
    /**Original name: WA-TAFESTEX<br>
	 * <pre>------------</pre>*/
    private WaTafestex waTafestex = new WaTafestex();
    //Original name: WA-MAX-FESTEX
    private short waMaxFestex = ((short)20);

    //==== CONSTRUCTORS ====
    public Lccs0003Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wsMeseIdx = 1; wsMeseIdx <= WS_MESE_MAXOCCURS; wsMeseIdx++) {
            setWsMeseFormatted(wsMeseIdx, DefaultValues.stringVal(Len.WS_MESE));
        }
    }

    public int getWaIniprog() {
        return this.waIniprog;
    }

    public int getWaGgIn400Anni() {
        return this.waGgIn400Anni;
    }

    public int getWaGgIn100Anni() {
        return this.waGgIn100Anni;
    }

    public int getWaGgIn4Anni() {
        return this.waGgIn4Anni;
    }

    public String getWaTabmesi() {
        return this.waTabmesi;
    }

    public String getWaTabmesiFormatted() {
        return Functions.padBlanks(getWaTabmesi(), Len.WA_TABMESI);
    }

    public void setWsDatap(int wsDatap) {
        this.wsDatap = wsDatap;
    }

    public int getWsDatap() {
        return this.wsDatap;
    }

    public void setWsAnyNum(int wsAnyNum) {
        this.wsAnyNum = wsAnyNum;
    }

    public int getWsAnyNum() {
        return this.wsAnyNum;
    }

    public void setWsDataPasquetta(int wsDataPasquetta) {
        this.wsDataPasquetta = wsDataPasquetta;
    }

    public int getWsDataPasquetta() {
        return this.wsDataPasquetta;
    }

    public void setWsDataWk(int wsDataWk) {
        this.wsDataWk = wsDataWk;
    }

    public int getWsDataWk() {
        return this.wsDataWk;
    }

    public void setWsDelta(short wsDelta) {
        this.wsDelta = NumericDisplay.asString(wsDelta, Len.WS_DELTA);
    }

    public void setWsDeltaFormatted(String wsDelta) {
        this.wsDelta = Trunc.toUnsignedNumeric(wsDelta, Len.WS_DELTA);
    }

    public short getWsDelta() {
        return NumericDisplay.asShort(this.wsDelta);
    }

    public String getWsDeltaFormatted() {
        return this.wsDelta;
    }

    public void setWsDataMm3(short wsDataMm3) {
        this.wsDataMm3 = NumericDisplay.asString(wsDataMm3, Len.WS_DATA_MM3);
    }

    public short getWsDataMm3() {
        return NumericDisplay.asShort(this.wsDataMm3);
    }

    public String getWsDataMm3Formatted() {
        return this.wsDataMm3;
    }

    public void setWsGmlav(short wsGmlav) {
        this.wsGmlav = NumericDisplay.asString(wsGmlav, Len.WS_GMLAV);
    }

    public short getWsGmlav() {
        return NumericDisplay.asShort(this.wsGmlav);
    }

    public String getWsGmlavFormatted() {
        return this.wsGmlav;
    }

    public void setWsFmlavFormatted(String wsFmlav) {
        this.wsFmlav = Trunc.toUnsignedNumeric(wsFmlav, Len.WS_FMLAV);
    }

    public short getWsFmlav() {
        return NumericDisplay.asShort(this.wsFmlav);
    }

    public String getWsFmlavFormatted() {
        return this.wsFmlav;
    }

    public void setWsQuotient(int wsQuotient) {
        this.wsQuotient = wsQuotient;
    }

    public int getWsQuotient() {
        return this.wsQuotient;
    }

    public void setWsRemainder(int wsRemainder) {
        this.wsRemainder = wsRemainder;
    }

    public int getWsRemainder() {
        return this.wsRemainder;
    }

    public void setWsMesiFormatted(String data) {
        byte[] buffer = new byte[Len.WS_MESI];
        MarshalByte.writeString(buffer, 1, data, Len.WS_MESI);
        setWsMesiBytes(buffer, 1);
    }

    public void setWsMesiBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= WS_MESE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wsMese[idx - 1] = MarshalByte.readFixedString(buffer, position, Len.WS_MESE);
                position += Len.WS_MESE;
            }
            else {
                setWsMese(idx, Types.INVALID_SHORT_VAL);
                position += Len.WS_MESE;
            }
        }
    }

    public void setWsMese(int wsMeseIdx, short wsMese) {
        this.wsMese[wsMeseIdx - 1] = NumericDisplay.asString(wsMese, Len.WS_MESE);
    }

    public void setWsMeseFormatted(int wsMeseIdx, String wsMese) {
        this.wsMese[wsMeseIdx - 1] = Trunc.toUnsignedNumeric(wsMese, Len.WS_MESE);
    }

    public short getWsMese(int wsMeseIdx) {
        return NumericDisplay.asShort(this.wsMese[wsMeseIdx - 1]);
    }

    public String getWsMeseFormatted(int wsMeseIdx) {
        return this.wsMese[wsMeseIdx - 1];
    }

    public void setWsMesiWk(short wsMesiWk) {
        this.wsMesiWk = NumericDisplay.asString(wsMesiWk, Len.WS_MESI_WK);
    }

    public short getWsMesiWk() {
        return NumericDisplay.asShort(this.wsMesiWk);
    }

    public String getWsMesiWkFormatted() {
        return this.wsMesiWk;
    }

    public void setI2(short i2) {
        this.i2 = i2;
    }

    public short getI2() {
        return this.i2;
    }

    public void setWsRcode(short wsRcode) {
        this.wsRcode = NumericDisplay.asString(wsRcode, Len.WS_RCODE);
    }

    public void setWsRcodeFormatted(String wsRcode) {
        this.wsRcode = Trunc.toUnsignedNumeric(wsRcode, Len.WS_RCODE);
    }

    public short getWsRcode() {
        return NumericDisplay.asShort(this.wsRcode);
    }

    public String getWsRcodeFormatted() {
        return this.wsRcode;
    }

    public short getWaMaxFestex() {
        return this.waMaxFestex;
    }

    public Flr2 getFlr2() {
        return flr2;
    }

    public WaTabdgiorni getWaTabdgiorni() {
        return waTabdgiorni;
    }

    public WaTabdmesi getWaTabdmesi() {
        return waTabdmesi;
    }

    public WaTafestex getWaTafestex() {
        return waTafestex;
    }

    public WsData getWsData() {
        return wsData;
    }

    public WsDataNum getWsDataNum() {
        return wsDataNum;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MESE = 2;
        public static final int WS_DELTA = 3;
        public static final int WS_GMLAV = 2;
        public static final int WS_FMLAV = 2;
        public static final int WS_MESI_WK = 2;
        public static final int WS_RCODE = 2;
        public static final int WS_DATA_MM3 = 3;
        public static final int WA_TABMESI = 24;
        public static final int WS_MESI = Lccs0003Data.WS_MESE_MAXOCCURS * WS_MESE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
