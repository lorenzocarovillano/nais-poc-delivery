package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RAPPEL<br>
 * Variable: B03-RAPPEL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Rappel extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Rappel() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RAPPEL;
    }

    public void setB03Rappel(AfDecimal b03Rappel) {
        writeDecimalAsPacked(Pos.B03_RAPPEL, b03Rappel.copy());
    }

    public void setB03RappelFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RAPPEL, Pos.B03_RAPPEL);
    }

    /**Original name: B03-RAPPEL<br>*/
    public AfDecimal getB03Rappel() {
        return readPackedAsDecimal(Pos.B03_RAPPEL, Len.Int.B03_RAPPEL, Len.Fract.B03_RAPPEL);
    }

    public byte[] getB03RappelAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RAPPEL, Pos.B03_RAPPEL);
        return buffer;
    }

    public void setB03RappelNull(String b03RappelNull) {
        writeString(Pos.B03_RAPPEL_NULL, b03RappelNull, Len.B03_RAPPEL_NULL);
    }

    /**Original name: B03-RAPPEL-NULL<br>*/
    public String getB03RappelNull() {
        return readString(Pos.B03_RAPPEL_NULL, Len.B03_RAPPEL_NULL);
    }

    public String getB03RappelNullFormatted() {
        return Functions.padBlanks(getB03RappelNull(), Len.B03_RAPPEL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RAPPEL = 1;
        public static final int B03_RAPPEL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RAPPEL = 8;
        public static final int B03_RAPPEL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RAPPEL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RAPPEL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
