package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.notifier.ChangeNotifier;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;

/**Original name: AREA-PRODUCT-SERVICES<br>
 * Variable: AREA-PRODUCT-SERVICES from copybook IJCCMQ01<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class AreaProductServicesIsps0040 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int IJCCMQ01_CAR_MAXOCCURS = 150000;
    private int ijccmq01CarListenerSize = IJCCMQ01_CAR_MAXOCCURS;
    private IValueChangeListener ijccmq01CarListener = new Ijccmq01CarListener();
    public static final int IJCCMQ01_ELE_ERRORI_MAXOCCURS = 10;
    public static final int IJCCMQ01_ADDRESSES_MAXOCCURS = 5;
    private Pos pos = new Pos(this);
    public static final char IJCCMQ01_ON_LINE = 'O';
    public static final char IJCCMQ01_BATCH = 'B';
    public static final char IJCCMQ01_BATCH_INFR = 'I';
    public static final char IJCCMQ01_SERVICE_INVOCATION = 'S';
    public static final char IJCCMQ01_DATA_REQUEST = 'D';
    public static final String IJCCMQ01_ESITO_OK = "OK";
    public static final String IJCCMQ01_ESITO_KO = "KO";
    private ChangeNotifier ijccmq01LengthDatiServizioNotifier = new ChangeNotifier();
    public static final String IJCCMQ00_NO_DEBUG = "0";
    public static final String IJCCMQ00_DEBUG_BASSO = "1";
    public static final String IJCCMQ00_DEBUG_MEDIO = "2";
    public static final String IJCCMQ00_DEBUG_ELEVATO = "3";
    public static final String IJCCMQ00_DEBUG_ESASPERATO = "4";
    private static final String[] IJCCMQ00_ANY_APPL_DBG = new String[] {"1", "2", "3", "4"};
    public static final String IJCCMQ00_ARCH_BATCH_DBG = "5";
    public static final String IJCCMQ00_COM_COB_JAV_DBG = "6";
    public static final String IJCCMQ00_STRESS_TEST_DBG = "7";
    public static final String IJCCMQ00_BUSINESS_DBG = "8";
    public static final String IJCCMQ00_TOT_TUNING_DBG = "9";
    private static final String[] IJCCMQ00_ANY_TUNING_DBG = new String[] {"5", "6", "7", "8", "9"};

    //==== CONSTRUCTORS ====
    public AreaProductServicesIsps0040() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return getAreaProductServicesSize();
    }

    public int getAreaProductServicesSize() {
        return Len.IJCCMQ01_HEADER + getIjccmq01AreaDatiServizioSize();
    }

    public void initAreaProductServicesLowValues() {
        getIjccmq01LengthDatiServizioNotifier().notifyMaxOccursListeners();
        fill(Pos.AREA_PRODUCT_SERVICES, getAreaProductServicesSize(), Types.LOW_CHAR_VAL);
    }

    public void setIjccmq01ModalitaEsecutiva(char ijccmq01ModalitaEsecutiva) {
        writeChar(Pos.IJCCMQ01_MODALITA_ESECUTIVA, ijccmq01ModalitaEsecutiva);
    }

    /**Original name: IJCCMQ01-MODALITA-ESECUTIVA<br>*/
    public char getIjccmq01ModalitaEsecutiva() {
        return readChar(Pos.IJCCMQ01_MODALITA_ESECUTIVA);
    }

    public void setIjccmq01JavaServiceName(String ijccmq01JavaServiceName) {
        writeString(Pos.IJCCMQ01_JAVA_SERVICE_NAME, ijccmq01JavaServiceName, Len.IJCCMQ01_JAVA_SERVICE_NAME);
    }

    /**Original name: IJCCMQ01-JAVA-SERVICE-NAME<br>*/
    public String getIjccmq01JavaServiceName() {
        return readString(Pos.IJCCMQ01_JAVA_SERVICE_NAME, Len.IJCCMQ01_JAVA_SERVICE_NAME);
    }

    public void setIjccmq01Esito(String ijccmq01Esito) {
        writeString(Pos.IJCCMQ01_ESITO, ijccmq01Esito, Len.IJCCMQ01_ESITO);
    }

    /**Original name: IJCCMQ01-ESITO<br>*/
    public String getIjccmq01Esito() {
        return readString(Pos.IJCCMQ01_ESITO, Len.IJCCMQ01_ESITO);
    }

    public boolean isIjccmq01EsitoOk() {
        return getIjccmq01Esito().equals(IJCCMQ01_ESITO_OK);
    }

    public void setIjccmq01EsitoOk() {
        setIjccmq01Esito(IJCCMQ01_ESITO_OK);
    }

    public void setIjccmq01LengthDatiServizio(int ijccmq01LengthDatiServizio) {
        writeBinaryInt(Pos.IJCCMQ01_LENGTH_DATI_SERVIZIO, ijccmq01LengthDatiServizio);
        this.ijccmq01LengthDatiServizioNotifier.setValue(ijccmq01LengthDatiServizio);
    }

    public int getIjccmq01LengthDatiServizio() {
        return readBinaryInt(Pos.IJCCMQ01_LENGTH_DATI_SERVIZIO);
    }

    /**Original name: IJCCMQ01-MAX-ELE-ERRORI<br>*/
    public short getIjccmq01MaxEleErrori() {
        return readBinaryShort(Pos.IJCCMQ01_MAX_ELE_ERRORI);
    }

    /**Original name: IJCCMQ01-DESC-ERRORE<br>*/
    public String getIjccmq01DescErrore(int ijccmq01DescErroreIdx) {
        int position = Pos.ijccmq01DescErrore(ijccmq01DescErroreIdx - 1);
        return readString(position, Len.IJCCMQ01_DESC_ERRORE);
    }

    public String getIjccmq01CodErroreFormatted(int ijccmq01CodErroreIdx) {
        int position = Pos.ijccmq01CodErrore(ijccmq01CodErroreIdx - 1);
        return readFixedString(position, Len.IJCCMQ01_COD_ERRORE);
    }

    public String getIjccmq01LivGravitaBeFormatted(int ijccmq01LivGravitaBeIdx) {
        int position = Pos.ijccmq01LivGravitaBe(ijccmq01LivGravitaBeIdx - 1);
        return readFixedString(position, Len.IJCCMQ01_LIV_GRAVITA_BE);
    }

    public void setIjccmq01AreaAddressesBytes(byte[] buffer) {
        setIjccmq01AreaAddressesBytes(buffer, 1);
    }

    public void setIjccmq01AreaAddressesBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IJCCMQ01_AREA_ADDRESSES, Pos.IJCCMQ01_AREA_ADDRESSES);
    }

    public void setIjccmq01UserName(String ijccmq01UserName) {
        writeString(Pos.IJCCMQ01_USER_NAME, ijccmq01UserName, Len.IJCCMQ01_USER_NAME);
    }

    /**Original name: IJCCMQ01-USER-NAME<br>*/
    public String getIjccmq01UserName() {
        return readString(Pos.IJCCMQ01_USER_NAME, Len.IJCCMQ01_USER_NAME);
    }

    public void setIjccmq00LivelloDebugFormatted(String ijccmq00LivelloDebug) {
        writeString(Pos.IJCCMQ00_LIVELLO_DEBUG, Trunc.toUnsignedNumeric(ijccmq00LivelloDebug, Len.IJCCMQ00_LIVELLO_DEBUG), Len.IJCCMQ00_LIVELLO_DEBUG);
    }

    /**Original name: IJCCMQ00-LIVELLO-DEBUG<br>*/
    public short getIjccmq00LivelloDebug() {
        return readNumDispUnsignedShort(Pos.IJCCMQ00_LIVELLO_DEBUG, Len.IJCCMQ00_LIVELLO_DEBUG);
    }

    public int getIjccmq01AreaDatiServizioSize() {
        return Types.CHAR_SIZE * ijccmq01CarListenerSize;
    }

    public void setIjccmq01AreaDatiServizioBytes(byte[] buffer) {
        setIjccmq01AreaDatiServizioBytes(buffer, 1);
    }

    /**Original name: IJCCMQ01-AREA-DATI-SERVIZIO<br>*/
    public byte[] getIjccmq01AreaDatiServizioBytes() {
        byte[] buffer = new byte[getIjccmq01AreaDatiServizioSize()];
        return getIjccmq01AreaDatiServizioBytes(buffer, 1);
    }

    public void setIjccmq01AreaDatiServizioBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, getIjccmq01AreaDatiServizioSize(), Pos.IJCCMQ01_AREA_DATI_SERVIZIO);
    }

    public byte[] getIjccmq01AreaDatiServizioBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, getIjccmq01AreaDatiServizioSize(), Pos.IJCCMQ01_AREA_DATI_SERVIZIO);
        return buffer;
    }

    public IValueChangeListener getIjccmq01CarListener() {
        return ijccmq01CarListener;
    }

    public ChangeNotifier getIjccmq01LengthDatiServizioNotifier() {
        return ijccmq01LengthDatiServizioNotifier;
    }

    public void notifyListeners() {
        getIjccmq01LengthDatiServizioNotifier().setValue(getIjccmq01LengthDatiServizio());
    }

    //==== INNER CLASSES ====
    /**Original name: IJCCMQ01-CAR<br>*/
    public class Ijccmq01CarListener implements IValueChangeListener {

        //==== METHODS ====
        public void change() {
            ijccmq01CarListenerSize = IJCCMQ01_CAR_MAXOCCURS;
        }

        public void change(int value) {
            ijccmq01CarListenerSize = value < 1 ? 0 : (value > IJCCMQ01_CAR_MAXOCCURS ? IJCCMQ01_CAR_MAXOCCURS : value);
        }
    }

    public static class Pos {

        //==== PROPERTIES ====
        public static final int AREA_PRODUCT_SERVICES = 1;
        public static final int IJCCMQ01_HEADER = AREA_PRODUCT_SERVICES;
        public static final int IJCCMQ01_MODALITA_ESECUTIVA = IJCCMQ01_HEADER;
        public static final int IJCCMQ01_JAVA_SERVICE_NAME = IJCCMQ01_MODALITA_ESECUTIVA + Len.IJCCMQ01_MODALITA_ESECUTIVA;
        public static final int IJCCMQ01_INFO_FRAMES = IJCCMQ01_JAVA_SERVICE_NAME + Len.IJCCMQ01_JAVA_SERVICE_NAME;
        public static final int IJCCMQ01_ID_APPLICATION = IJCCMQ01_INFO_FRAMES;
        public static final int IJCCMQ01_TOT_NUM_FRAMES = IJCCMQ01_ID_APPLICATION + Len.IJCCMQ01_ID_APPLICATION;
        public static final int IJCCMQ01_NUM_FRAME = IJCCMQ01_TOT_NUM_FRAMES + Len.IJCCMQ01_TOT_NUM_FRAMES;
        public static final int IJCCMQ01_LENGTH_DATA_FRAME = IJCCMQ01_NUM_FRAME + Len.IJCCMQ01_NUM_FRAME;
        public static final int IJCCMQ01_CALL_METHOD = IJCCMQ01_LENGTH_DATA_FRAME + Len.IJCCMQ01_LENGTH_DATA_FRAME;
        public static final int IJCCMQ01_ESITO = IJCCMQ01_CALL_METHOD + Len.IJCCMQ01_CALL_METHOD;
        public static final int IJCCMQ01_LENGTH_DATI_SERVIZIO = IJCCMQ01_ESITO + Len.IJCCMQ01_ESITO;
        public static final int IJCCMQ01_MAX_ELE_ERRORI = IJCCMQ01_LENGTH_DATI_SERVIZIO + Len.IJCCMQ01_LENGTH_DATI_SERVIZIO;
        public static final int IJCCMQ01_TAB_ERRORI_FRONT_END = IJCCMQ01_MAX_ELE_ERRORI + Len.IJCCMQ01_MAX_ELE_ERRORI;
        public static final int IJCCMQ01_LENGTH_DATI_OUTPUT = ijccmq01TipoTrattFe(IJCCMQ01_ELE_ERRORI_MAXOCCURS - 1) + Len.IJCCMQ01_TIPO_TRATT_FE;
        public static final int IJCCMQ01_VAR_AMBIENTE = IJCCMQ01_LENGTH_DATI_OUTPUT + Len.IJCCMQ01_LENGTH_DATI_OUTPUT;
        public static final int IJCCMQ01_AREA_ADDRESSES = IJCCMQ01_VAR_AMBIENTE + Len.IJCCMQ01_VAR_AMBIENTE;
        public static final int IJCCMQ01_USER_NAME = ijccmq01Address(IJCCMQ01_ADDRESSES_MAXOCCURS - 1) + Len.IJCCMQ01_ADDRESS;
        public static final int FLR1 = IJCCMQ01_USER_NAME + Len.IJCCMQ01_USER_NAME;
        public static final int IJCCMQ00_LIVELLO_DEBUG = FLR1 + Len.FLR1;
        public static final int IJCCMQ01_AREA_DATI_SERVIZIO = IJCCMQ00_LIVELLO_DEBUG + Len.IJCCMQ00_LIVELLO_DEBUG;
        private AreaProductServicesIsps0040 outer;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        public Pos(AreaProductServicesIsps0040 outer) {
            this.outer = outer;
        }

        //==== METHODS ====
        public static int ijccmq01EleErrori(int idx) {
            return IJCCMQ01_TAB_ERRORI_FRONT_END + idx * Len.IJCCMQ01_ELE_ERRORI;
        }

        public static int ijccmq01DescErrore(int idx) {
            return ijccmq01EleErrori(idx);
        }

        public static int ijccmq01CodErrore(int idx) {
            return ijccmq01DescErrore(idx) + Len.IJCCMQ01_DESC_ERRORE;
        }

        public static int ijccmq01LivGravitaBe(int idx) {
            return ijccmq01CodErrore(idx) + Len.IJCCMQ01_COD_ERRORE;
        }

        public static int ijccmq01TipoTrattFe(int idx) {
            return ijccmq01LivGravitaBe(idx) + Len.IJCCMQ01_LIV_GRAVITA_BE;
        }

        public static int ijccmq01Addresses(int idx) {
            return IJCCMQ01_AREA_ADDRESSES + idx * Len.IJCCMQ01_ADDRESSES;
        }

        public static int ijccmq01AddressType(int idx) {
            return ijccmq01Addresses(idx);
        }

        public static int ijccmq01Address(int idx) {
            return ijccmq01AddressType(idx) + Len.IJCCMQ01_ADDRESS_TYPE;
        }

        public static int ijccmq01Car(int idx) {
            return IJCCMQ01_AREA_DATI_SERVIZIO + idx * Len.IJCCMQ01_CAR;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int IJCCMQ01_MODALITA_ESECUTIVA = 1;
        public static final int IJCCMQ01_JAVA_SERVICE_NAME = 100;
        public static final int IJCCMQ01_ID_APPLICATION = 26;
        public static final int IJCCMQ01_TOT_NUM_FRAMES = 2;
        public static final int IJCCMQ01_NUM_FRAME = 2;
        public static final int IJCCMQ01_LENGTH_DATA_FRAME = 4;
        public static final int IJCCMQ01_CALL_METHOD = 1;
        public static final int IJCCMQ01_ESITO = 2;
        public static final int IJCCMQ01_LENGTH_DATI_SERVIZIO = 4;
        public static final int IJCCMQ01_MAX_ELE_ERRORI = 2;
        public static final int IJCCMQ01_DESC_ERRORE = 200;
        public static final int IJCCMQ01_COD_ERRORE = 6;
        public static final int IJCCMQ01_LIV_GRAVITA_BE = 1;
        public static final int IJCCMQ01_TIPO_TRATT_FE = 1;
        public static final int IJCCMQ01_ELE_ERRORI = IJCCMQ01_DESC_ERRORE + IJCCMQ01_COD_ERRORE + IJCCMQ01_LIV_GRAVITA_BE + IJCCMQ01_TIPO_TRATT_FE;
        public static final int IJCCMQ01_LENGTH_DATI_OUTPUT = 4;
        public static final int IJCCMQ01_VAR_AMBIENTE = 224;
        public static final int IJCCMQ01_ADDRESS_TYPE = 1;
        public static final int IJCCMQ01_ADDRESS = 4;
        public static final int IJCCMQ01_ADDRESSES = IJCCMQ01_ADDRESS_TYPE + IJCCMQ01_ADDRESS;
        public static final int IJCCMQ01_USER_NAME = 20;
        public static final int FLR1 = 2;
        public static final int IJCCMQ00_LIVELLO_DEBUG = 1;
        public static final int IJCCMQ01_CAR = 1;
        public static final int IJCCMQ01_INFO_FRAMES = IJCCMQ01_ID_APPLICATION + IJCCMQ01_TOT_NUM_FRAMES + IJCCMQ01_NUM_FRAME + IJCCMQ01_LENGTH_DATA_FRAME + IJCCMQ01_CALL_METHOD;
        public static final int IJCCMQ01_TAB_ERRORI_FRONT_END = AreaProductServicesIsps0040.IJCCMQ01_ELE_ERRORI_MAXOCCURS * IJCCMQ01_ELE_ERRORI;
        public static final int IJCCMQ01_AREA_ADDRESSES = AreaProductServicesIsps0040.IJCCMQ01_ADDRESSES_MAXOCCURS * IJCCMQ01_ADDRESSES;
        public static final int IJCCMQ01_HEADER = IJCCMQ01_MODALITA_ESECUTIVA + IJCCMQ01_JAVA_SERVICE_NAME + IJCCMQ01_INFO_FRAMES + IJCCMQ01_ESITO + IJCCMQ01_LENGTH_DATI_SERVIZIO + IJCCMQ01_MAX_ELE_ERRORI + IJCCMQ01_TAB_ERRORI_FRONT_END + IJCCMQ01_LENGTH_DATI_OUTPUT + IJCCMQ01_VAR_AMBIENTE + IJCCMQ01_AREA_ADDRESSES + IJCCMQ01_USER_NAME + IJCCMQ00_LIVELLO_DEBUG + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
