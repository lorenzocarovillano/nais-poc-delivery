package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: LCCC0023-ID-MOVI-ANN<br>
 * Variable: LCCC0023-ID-MOVI-ANN from program LCCS0023<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0023IdMoviAnn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0023IdMoviAnn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0023_ID_MOVI_ANN;
    }

    public void setLccc0023IdMoviAnn(int lccc0023IdMoviAnn) {
        writeIntAsPacked(Pos.LCCC0023_ID_MOVI_ANN, lccc0023IdMoviAnn, Len.Int.LCCC0023_ID_MOVI_ANN);
    }

    public void setLccc0023IdMoviAnnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0023_ID_MOVI_ANN, Pos.LCCC0023_ID_MOVI_ANN);
    }

    /**Original name: LCCC0023-ID-MOVI-ANN<br>*/
    public int getLccc0023IdMoviAnn() {
        return readPackedAsInt(Pos.LCCC0023_ID_MOVI_ANN, Len.Int.LCCC0023_ID_MOVI_ANN);
    }

    public byte[] getLccc0023IdMoviAnnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0023_ID_MOVI_ANN, Pos.LCCC0023_ID_MOVI_ANN);
        return buffer;
    }

    public void initLccc0023IdMoviAnnSpaces() {
        fill(Pos.LCCC0023_ID_MOVI_ANN, Len.LCCC0023_ID_MOVI_ANN, Types.SPACE_CHAR);
    }

    public void setLccc0023IdMoviAnnNull(String lccc0023IdMoviAnnNull) {
        writeString(Pos.LCCC0023_ID_MOVI_ANN_NULL, lccc0023IdMoviAnnNull, Len.LCCC0023_ID_MOVI_ANN_NULL);
    }

    /**Original name: LCCC0023-ID-MOVI-ANN-NULL<br>*/
    public String getLccc0023IdMoviAnnNull() {
        return readString(Pos.LCCC0023_ID_MOVI_ANN_NULL, Len.LCCC0023_ID_MOVI_ANN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_MOVI_ANN = 1;
        public static final int LCCC0023_ID_MOVI_ANN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_MOVI_ANN = 5;
        public static final int LCCC0023_ID_MOVI_ANN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0023_ID_MOVI_ANN = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
