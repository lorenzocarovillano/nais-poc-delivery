package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WPAG-AA-DIFF-PROR<br>
 * Variable: WPAG-AA-DIFF-PROR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAaDiffPror extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAaDiffPror() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_AA_DIFF_PROR;
    }

    public void setWpagAaDiffProrFormatted(String wpagAaDiffPror) {
        writeString(Pos.WPAG_AA_DIFF_PROR, Trunc.toUnsignedNumeric(wpagAaDiffPror, Len.WPAG_AA_DIFF_PROR), Len.WPAG_AA_DIFF_PROR);
    }

    public void setWpagAaDiffProrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_AA_DIFF_PROR, Pos.WPAG_AA_DIFF_PROR);
    }

    /**Original name: WPAG-AA-DIFF-PROR<br>*/
    public short getWpagAaDiffPror() {
        return readNumDispUnsignedShort(Pos.WPAG_AA_DIFF_PROR, Len.WPAG_AA_DIFF_PROR);
    }

    public byte[] getWpagAaDiffProrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_AA_DIFF_PROR, Pos.WPAG_AA_DIFF_PROR);
        return buffer;
    }

    public void initWpagAaDiffProrSpaces() {
        fill(Pos.WPAG_AA_DIFF_PROR, Len.WPAG_AA_DIFF_PROR, Types.SPACE_CHAR);
    }

    public void setWpagAaDiffProrNull(String wpagAaDiffProrNull) {
        writeString(Pos.WPAG_AA_DIFF_PROR_NULL, wpagAaDiffProrNull, Len.WPAG_AA_DIFF_PROR_NULL);
    }

    /**Original name: WPAG-AA-DIFF-PROR-NULL<br>*/
    public String getWpagAaDiffProrNull() {
        return readString(Pos.WPAG_AA_DIFF_PROR_NULL, Len.WPAG_AA_DIFF_PROR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_AA_DIFF_PROR = 1;
        public static final int WPAG_AA_DIFF_PROR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_AA_DIFF_PROR = 2;
        public static final int WPAG_AA_DIFF_PROR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
