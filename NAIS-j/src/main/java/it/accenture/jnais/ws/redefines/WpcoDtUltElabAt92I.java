package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-AT92-I<br>
 * Variable: WPCO-DT-ULT-ELAB-AT92-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabAt92I extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabAt92I() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_AT92_I;
    }

    public void setWpcoDtUltElabAt92I(int wpcoDtUltElabAt92I) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_AT92_I, wpcoDtUltElabAt92I, Len.Int.WPCO_DT_ULT_ELAB_AT92_I);
    }

    public void setDpcoDtUltElabAt92IFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT92_I, Pos.WPCO_DT_ULT_ELAB_AT92_I);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT92-I<br>*/
    public int getWpcoDtUltElabAt92I() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_AT92_I, Len.Int.WPCO_DT_ULT_ELAB_AT92_I);
    }

    public byte[] getWpcoDtUltElabAt92IAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT92_I, Pos.WPCO_DT_ULT_ELAB_AT92_I);
        return buffer;
    }

    public void setWpcoDtUltElabAt92INull(String wpcoDtUltElabAt92INull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_AT92_I_NULL, wpcoDtUltElabAt92INull, Len.WPCO_DT_ULT_ELAB_AT92_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT92-I-NULL<br>*/
    public String getWpcoDtUltElabAt92INull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_AT92_I_NULL, Len.WPCO_DT_ULT_ELAB_AT92_I_NULL);
    }

    public String getWpcoDtUltElabAt92INullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabAt92INull(), Len.WPCO_DT_ULT_ELAB_AT92_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT92_I = 1;
        public static final int WPCO_DT_ULT_ELAB_AT92_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT92_I = 5;
        public static final int WPCO_DT_ULT_ELAB_AT92_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_AT92_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
