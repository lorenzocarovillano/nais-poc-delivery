package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.OcsCodCompAniaAcqs;
import it.accenture.jnais.ws.redefines.OcsIdMoviChiu;
import it.accenture.jnais.ws.redefines.OcsIdPoli;

/**Original name: OGG-IN-COASS<br>
 * Variable: OGG-IN-COASS from copybook IDBVOCS1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class OggInCoassIdbsocs0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: OCS-ID-OGG-IN-COASS
    private int ocsIdOggInCoass = DefaultValues.INT_VAL;
    //Original name: OCS-ID-POLI
    private OcsIdPoli ocsIdPoli = new OcsIdPoli();
    //Original name: OCS-ID-MOVI-CRZ
    private int ocsIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: OCS-ID-MOVI-CHIU
    private OcsIdMoviChiu ocsIdMoviChiu = new OcsIdMoviChiu();
    //Original name: OCS-DT-INI-EFF
    private int ocsDtIniEff = DefaultValues.INT_VAL;
    //Original name: OCS-DT-END-EFF
    private int ocsDtEndEff = DefaultValues.INT_VAL;
    //Original name: OCS-COD-COMP-ANIA
    private int ocsCodCompAnia = DefaultValues.INT_VAL;
    //Original name: OCS-COD-COASS
    private String ocsCodCoass = DefaultValues.stringVal(Len.OCS_COD_COASS);
    //Original name: OCS-TP-DLG
    private String ocsTpDlg = DefaultValues.stringVal(Len.OCS_TP_DLG);
    //Original name: OCS-COD-COMP-ANIA-ACQS
    private OcsCodCompAniaAcqs ocsCodCompAniaAcqs = new OcsCodCompAniaAcqs();
    //Original name: OCS-DS-RIGA
    private long ocsDsRiga = DefaultValues.LONG_VAL;
    //Original name: OCS-DS-OPER-SQL
    private char ocsDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: OCS-DS-VER
    private int ocsDsVer = DefaultValues.INT_VAL;
    //Original name: OCS-DS-TS-INI-CPTZ
    private long ocsDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: OCS-DS-TS-END-CPTZ
    private long ocsDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: OCS-DS-UTENTE
    private String ocsDsUtente = DefaultValues.stringVal(Len.OCS_DS_UTENTE);
    //Original name: OCS-DS-STATO-ELAB
    private char ocsDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OGG_IN_COASS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setOggInCoassBytes(buf);
    }

    public void setOggInCoassFormatted(String data) {
        byte[] buffer = new byte[Len.OGG_IN_COASS];
        MarshalByte.writeString(buffer, 1, data, Len.OGG_IN_COASS);
        setOggInCoassBytes(buffer, 1);
    }

    public String getOggInCoassFormatted() {
        return MarshalByteExt.bufferToStr(getOggInCoassBytes());
    }

    public void setOggInCoassBytes(byte[] buffer) {
        setOggInCoassBytes(buffer, 1);
    }

    public byte[] getOggInCoassBytes() {
        byte[] buffer = new byte[Len.OGG_IN_COASS];
        return getOggInCoassBytes(buffer, 1);
    }

    public void setOggInCoassBytes(byte[] buffer, int offset) {
        int position = offset;
        ocsIdOggInCoass = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCS_ID_OGG_IN_COASS, 0);
        position += Len.OCS_ID_OGG_IN_COASS;
        ocsIdPoli.setOcsIdPoliFromBuffer(buffer, position);
        position += OcsIdPoli.Len.OCS_ID_POLI;
        ocsIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCS_ID_MOVI_CRZ, 0);
        position += Len.OCS_ID_MOVI_CRZ;
        ocsIdMoviChiu.setOcsIdMoviChiuFromBuffer(buffer, position);
        position += OcsIdMoviChiu.Len.OCS_ID_MOVI_CHIU;
        ocsDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCS_DT_INI_EFF, 0);
        position += Len.OCS_DT_INI_EFF;
        ocsDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCS_DT_END_EFF, 0);
        position += Len.OCS_DT_END_EFF;
        ocsCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCS_COD_COMP_ANIA, 0);
        position += Len.OCS_COD_COMP_ANIA;
        ocsCodCoass = MarshalByte.readString(buffer, position, Len.OCS_COD_COASS);
        position += Len.OCS_COD_COASS;
        ocsTpDlg = MarshalByte.readString(buffer, position, Len.OCS_TP_DLG);
        position += Len.OCS_TP_DLG;
        ocsCodCompAniaAcqs.setOcsCodCompAniaAcqsFromBuffer(buffer, position);
        position += OcsCodCompAniaAcqs.Len.OCS_COD_COMP_ANIA_ACQS;
        ocsDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.OCS_DS_RIGA, 0);
        position += Len.OCS_DS_RIGA;
        ocsDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ocsDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.OCS_DS_VER, 0);
        position += Len.OCS_DS_VER;
        ocsDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.OCS_DS_TS_INI_CPTZ, 0);
        position += Len.OCS_DS_TS_INI_CPTZ;
        ocsDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.OCS_DS_TS_END_CPTZ, 0);
        position += Len.OCS_DS_TS_END_CPTZ;
        ocsDsUtente = MarshalByte.readString(buffer, position, Len.OCS_DS_UTENTE);
        position += Len.OCS_DS_UTENTE;
        ocsDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getOggInCoassBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ocsIdOggInCoass, Len.Int.OCS_ID_OGG_IN_COASS, 0);
        position += Len.OCS_ID_OGG_IN_COASS;
        ocsIdPoli.getOcsIdPoliAsBuffer(buffer, position);
        position += OcsIdPoli.Len.OCS_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, ocsIdMoviCrz, Len.Int.OCS_ID_MOVI_CRZ, 0);
        position += Len.OCS_ID_MOVI_CRZ;
        ocsIdMoviChiu.getOcsIdMoviChiuAsBuffer(buffer, position);
        position += OcsIdMoviChiu.Len.OCS_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, ocsDtIniEff, Len.Int.OCS_DT_INI_EFF, 0);
        position += Len.OCS_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, ocsDtEndEff, Len.Int.OCS_DT_END_EFF, 0);
        position += Len.OCS_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, ocsCodCompAnia, Len.Int.OCS_COD_COMP_ANIA, 0);
        position += Len.OCS_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, ocsCodCoass, Len.OCS_COD_COASS);
        position += Len.OCS_COD_COASS;
        MarshalByte.writeString(buffer, position, ocsTpDlg, Len.OCS_TP_DLG);
        position += Len.OCS_TP_DLG;
        ocsCodCompAniaAcqs.getOcsCodCompAniaAcqsAsBuffer(buffer, position);
        position += OcsCodCompAniaAcqs.Len.OCS_COD_COMP_ANIA_ACQS;
        MarshalByte.writeLongAsPacked(buffer, position, ocsDsRiga, Len.Int.OCS_DS_RIGA, 0);
        position += Len.OCS_DS_RIGA;
        MarshalByte.writeChar(buffer, position, ocsDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, ocsDsVer, Len.Int.OCS_DS_VER, 0);
        position += Len.OCS_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, ocsDsTsIniCptz, Len.Int.OCS_DS_TS_INI_CPTZ, 0);
        position += Len.OCS_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, ocsDsTsEndCptz, Len.Int.OCS_DS_TS_END_CPTZ, 0);
        position += Len.OCS_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, ocsDsUtente, Len.OCS_DS_UTENTE);
        position += Len.OCS_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, ocsDsStatoElab);
        return buffer;
    }

    public void setOcsIdOggInCoass(int ocsIdOggInCoass) {
        this.ocsIdOggInCoass = ocsIdOggInCoass;
    }

    public int getOcsIdOggInCoass() {
        return this.ocsIdOggInCoass;
    }

    public void setOcsIdMoviCrz(int ocsIdMoviCrz) {
        this.ocsIdMoviCrz = ocsIdMoviCrz;
    }

    public int getOcsIdMoviCrz() {
        return this.ocsIdMoviCrz;
    }

    public void setOcsDtIniEff(int ocsDtIniEff) {
        this.ocsDtIniEff = ocsDtIniEff;
    }

    public int getOcsDtIniEff() {
        return this.ocsDtIniEff;
    }

    public void setOcsDtEndEff(int ocsDtEndEff) {
        this.ocsDtEndEff = ocsDtEndEff;
    }

    public int getOcsDtEndEff() {
        return this.ocsDtEndEff;
    }

    public void setOcsCodCompAnia(int ocsCodCompAnia) {
        this.ocsCodCompAnia = ocsCodCompAnia;
    }

    public int getOcsCodCompAnia() {
        return this.ocsCodCompAnia;
    }

    public void setOcsCodCoass(String ocsCodCoass) {
        this.ocsCodCoass = Functions.subString(ocsCodCoass, Len.OCS_COD_COASS);
    }

    public String getOcsCodCoass() {
        return this.ocsCodCoass;
    }

    public String getOcsCodCoassFormatted() {
        return Functions.padBlanks(getOcsCodCoass(), Len.OCS_COD_COASS);
    }

    public void setOcsTpDlg(String ocsTpDlg) {
        this.ocsTpDlg = Functions.subString(ocsTpDlg, Len.OCS_TP_DLG);
    }

    public String getOcsTpDlg() {
        return this.ocsTpDlg;
    }

    public void setOcsDsRiga(long ocsDsRiga) {
        this.ocsDsRiga = ocsDsRiga;
    }

    public long getOcsDsRiga() {
        return this.ocsDsRiga;
    }

    public void setOcsDsOperSql(char ocsDsOperSql) {
        this.ocsDsOperSql = ocsDsOperSql;
    }

    public void setOcsDsOperSqlFormatted(String ocsDsOperSql) {
        setOcsDsOperSql(Functions.charAt(ocsDsOperSql, Types.CHAR_SIZE));
    }

    public char getOcsDsOperSql() {
        return this.ocsDsOperSql;
    }

    public void setOcsDsVer(int ocsDsVer) {
        this.ocsDsVer = ocsDsVer;
    }

    public int getOcsDsVer() {
        return this.ocsDsVer;
    }

    public void setOcsDsTsIniCptz(long ocsDsTsIniCptz) {
        this.ocsDsTsIniCptz = ocsDsTsIniCptz;
    }

    public long getOcsDsTsIniCptz() {
        return this.ocsDsTsIniCptz;
    }

    public void setOcsDsTsEndCptz(long ocsDsTsEndCptz) {
        this.ocsDsTsEndCptz = ocsDsTsEndCptz;
    }

    public long getOcsDsTsEndCptz() {
        return this.ocsDsTsEndCptz;
    }

    public void setOcsDsUtente(String ocsDsUtente) {
        this.ocsDsUtente = Functions.subString(ocsDsUtente, Len.OCS_DS_UTENTE);
    }

    public String getOcsDsUtente() {
        return this.ocsDsUtente;
    }

    public void setOcsDsStatoElab(char ocsDsStatoElab) {
        this.ocsDsStatoElab = ocsDsStatoElab;
    }

    public void setOcsDsStatoElabFormatted(String ocsDsStatoElab) {
        setOcsDsStatoElab(Functions.charAt(ocsDsStatoElab, Types.CHAR_SIZE));
    }

    public char getOcsDsStatoElab() {
        return this.ocsDsStatoElab;
    }

    public OcsCodCompAniaAcqs getOcsCodCompAniaAcqs() {
        return ocsCodCompAniaAcqs;
    }

    public OcsIdMoviChiu getOcsIdMoviChiu() {
        return ocsIdMoviChiu;
    }

    public OcsIdPoli getOcsIdPoli() {
        return ocsIdPoli;
    }

    @Override
    public byte[] serialize() {
        return getOggInCoassBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OCS_ID_OGG_IN_COASS = 5;
        public static final int OCS_ID_MOVI_CRZ = 5;
        public static final int OCS_DT_INI_EFF = 5;
        public static final int OCS_DT_END_EFF = 5;
        public static final int OCS_COD_COMP_ANIA = 3;
        public static final int OCS_COD_COASS = 5;
        public static final int OCS_TP_DLG = 2;
        public static final int OCS_DS_RIGA = 6;
        public static final int OCS_DS_OPER_SQL = 1;
        public static final int OCS_DS_VER = 5;
        public static final int OCS_DS_TS_INI_CPTZ = 10;
        public static final int OCS_DS_TS_END_CPTZ = 10;
        public static final int OCS_DS_UTENTE = 20;
        public static final int OCS_DS_STATO_ELAB = 1;
        public static final int OGG_IN_COASS = OCS_ID_OGG_IN_COASS + OcsIdPoli.Len.OCS_ID_POLI + OCS_ID_MOVI_CRZ + OcsIdMoviChiu.Len.OCS_ID_MOVI_CHIU + OCS_DT_INI_EFF + OCS_DT_END_EFF + OCS_COD_COMP_ANIA + OCS_COD_COASS + OCS_TP_DLG + OcsCodCompAniaAcqs.Len.OCS_COD_COMP_ANIA_ACQS + OCS_DS_RIGA + OCS_DS_OPER_SQL + OCS_DS_VER + OCS_DS_TS_INI_CPTZ + OCS_DS_TS_END_CPTZ + OCS_DS_UTENTE + OCS_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCS_ID_OGG_IN_COASS = 9;
            public static final int OCS_ID_MOVI_CRZ = 9;
            public static final int OCS_DT_INI_EFF = 8;
            public static final int OCS_DT_END_EFF = 8;
            public static final int OCS_COD_COMP_ANIA = 5;
            public static final int OCS_DS_RIGA = 10;
            public static final int OCS_DS_VER = 9;
            public static final int OCS_DS_TS_INI_CPTZ = 18;
            public static final int OCS_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
