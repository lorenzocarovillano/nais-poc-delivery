package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-BNS-GIA-LIQTO<br>
 * Variable: TGA-BNS-GIA-LIQTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaBnsGiaLiqto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaBnsGiaLiqto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_BNS_GIA_LIQTO;
    }

    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        writeDecimalAsPacked(Pos.TGA_BNS_GIA_LIQTO, tgaBnsGiaLiqto.copy());
    }

    public void setTgaBnsGiaLiqtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_BNS_GIA_LIQTO, Pos.TGA_BNS_GIA_LIQTO);
    }

    /**Original name: TGA-BNS-GIA-LIQTO<br>*/
    public AfDecimal getTgaBnsGiaLiqto() {
        return readPackedAsDecimal(Pos.TGA_BNS_GIA_LIQTO, Len.Int.TGA_BNS_GIA_LIQTO, Len.Fract.TGA_BNS_GIA_LIQTO);
    }

    public byte[] getTgaBnsGiaLiqtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_BNS_GIA_LIQTO, Pos.TGA_BNS_GIA_LIQTO);
        return buffer;
    }

    public void setTgaBnsGiaLiqtoNull(String tgaBnsGiaLiqtoNull) {
        writeString(Pos.TGA_BNS_GIA_LIQTO_NULL, tgaBnsGiaLiqtoNull, Len.TGA_BNS_GIA_LIQTO_NULL);
    }

    /**Original name: TGA-BNS-GIA-LIQTO-NULL<br>*/
    public String getTgaBnsGiaLiqtoNull() {
        return readString(Pos.TGA_BNS_GIA_LIQTO_NULL, Len.TGA_BNS_GIA_LIQTO_NULL);
    }

    public String getTgaBnsGiaLiqtoNullFormatted() {
        return Functions.padBlanks(getTgaBnsGiaLiqtoNull(), Len.TGA_BNS_GIA_LIQTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_BNS_GIA_LIQTO = 1;
        public static final int TGA_BNS_GIA_LIQTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_BNS_GIA_LIQTO = 8;
        public static final int TGA_BNS_GIA_LIQTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_BNS_GIA_LIQTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_BNS_GIA_LIQTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
