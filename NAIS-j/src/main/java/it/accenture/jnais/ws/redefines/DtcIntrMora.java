package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-INTR-MORA<br>
 * Variable: DTC-INTR-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_INTR_MORA;
    }

    public void setDtcIntrMora(AfDecimal dtcIntrMora) {
        writeDecimalAsPacked(Pos.DTC_INTR_MORA, dtcIntrMora.copy());
    }

    public void setDtcIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_INTR_MORA, Pos.DTC_INTR_MORA);
    }

    /**Original name: DTC-INTR-MORA<br>*/
    public AfDecimal getDtcIntrMora() {
        return readPackedAsDecimal(Pos.DTC_INTR_MORA, Len.Int.DTC_INTR_MORA, Len.Fract.DTC_INTR_MORA);
    }

    public byte[] getDtcIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_INTR_MORA, Pos.DTC_INTR_MORA);
        return buffer;
    }

    public void setDtcIntrMoraNull(String dtcIntrMoraNull) {
        writeString(Pos.DTC_INTR_MORA_NULL, dtcIntrMoraNull, Len.DTC_INTR_MORA_NULL);
    }

    /**Original name: DTC-INTR-MORA-NULL<br>*/
    public String getDtcIntrMoraNull() {
        return readString(Pos.DTC_INTR_MORA_NULL, Len.DTC_INTR_MORA_NULL);
    }

    public String getDtcIntrMoraNullFormatted() {
        return Functions.padBlanks(getDtcIntrMoraNull(), Len.DTC_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_INTR_MORA = 1;
        public static final int DTC_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_INTR_MORA = 8;
        public static final int DTC_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
