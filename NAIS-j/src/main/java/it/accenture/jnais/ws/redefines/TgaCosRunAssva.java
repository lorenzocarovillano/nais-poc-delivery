package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-COS-RUN-ASSVA<br>
 * Variable: TGA-COS-RUN-ASSVA from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaCosRunAssva extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaCosRunAssva() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_COS_RUN_ASSVA;
    }

    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        writeDecimalAsPacked(Pos.TGA_COS_RUN_ASSVA, tgaCosRunAssva.copy());
    }

    public void setTgaCosRunAssvaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_COS_RUN_ASSVA, Pos.TGA_COS_RUN_ASSVA);
    }

    /**Original name: TGA-COS-RUN-ASSVA<br>*/
    public AfDecimal getTgaCosRunAssva() {
        return readPackedAsDecimal(Pos.TGA_COS_RUN_ASSVA, Len.Int.TGA_COS_RUN_ASSVA, Len.Fract.TGA_COS_RUN_ASSVA);
    }

    public byte[] getTgaCosRunAssvaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_COS_RUN_ASSVA, Pos.TGA_COS_RUN_ASSVA);
        return buffer;
    }

    public void setTgaCosRunAssvaNull(String tgaCosRunAssvaNull) {
        writeString(Pos.TGA_COS_RUN_ASSVA_NULL, tgaCosRunAssvaNull, Len.TGA_COS_RUN_ASSVA_NULL);
    }

    /**Original name: TGA-COS-RUN-ASSVA-NULL<br>*/
    public String getTgaCosRunAssvaNull() {
        return readString(Pos.TGA_COS_RUN_ASSVA_NULL, Len.TGA_COS_RUN_ASSVA_NULL);
    }

    public String getTgaCosRunAssvaNullFormatted() {
        return Functions.padBlanks(getTgaCosRunAssvaNull(), Len.TGA_COS_RUN_ASSVA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_COS_RUN_ASSVA = 1;
        public static final int TGA_COS_RUN_ASSVA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_COS_RUN_ASSVA = 8;
        public static final int TGA_COS_RUN_ASSVA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_COS_RUN_ASSVA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_COS_RUN_ASSVA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
