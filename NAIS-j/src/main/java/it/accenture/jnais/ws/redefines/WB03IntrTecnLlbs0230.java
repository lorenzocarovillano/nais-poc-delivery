package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-INTR-TECN<br>
 * Variable: W-B03-INTR-TECN from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03IntrTecnLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03IntrTecnLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_INTR_TECN;
    }

    public void setwB03IntrTecn(AfDecimal wB03IntrTecn) {
        writeDecimalAsPacked(Pos.W_B03_INTR_TECN, wB03IntrTecn.copy());
    }

    /**Original name: W-B03-INTR-TECN<br>*/
    public AfDecimal getwB03IntrTecn() {
        return readPackedAsDecimal(Pos.W_B03_INTR_TECN, Len.Int.W_B03_INTR_TECN, Len.Fract.W_B03_INTR_TECN);
    }

    public byte[] getwB03IntrTecnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_INTR_TECN, Pos.W_B03_INTR_TECN);
        return buffer;
    }

    public void setwB03IntrTecnNull(String wB03IntrTecnNull) {
        writeString(Pos.W_B03_INTR_TECN_NULL, wB03IntrTecnNull, Len.W_B03_INTR_TECN_NULL);
    }

    /**Original name: W-B03-INTR-TECN-NULL<br>*/
    public String getwB03IntrTecnNull() {
        return readString(Pos.W_B03_INTR_TECN_NULL, Len.W_B03_INTR_TECN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_INTR_TECN = 1;
        public static final int W_B03_INTR_TECN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_INTR_TECN = 4;
        public static final int W_B03_INTR_TECN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_INTR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_INTR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
