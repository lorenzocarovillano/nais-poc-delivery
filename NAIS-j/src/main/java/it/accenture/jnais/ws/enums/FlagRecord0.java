package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-RECORD-0<br>
 * Variable: FLAG-RECORD-0 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRecord0 {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagRecord0(char flagRecord0) {
        this.value = flagRecord0;
    }

    public char getFlagRecord0() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
