package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-IMP-ADER<br>
 * Variable: DTR-IMP-ADER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_IMP_ADER;
    }

    public void setDtrImpAder(AfDecimal dtrImpAder) {
        writeDecimalAsPacked(Pos.DTR_IMP_ADER, dtrImpAder.copy());
    }

    public void setDtrImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_IMP_ADER, Pos.DTR_IMP_ADER);
    }

    /**Original name: DTR-IMP-ADER<br>*/
    public AfDecimal getDtrImpAder() {
        return readPackedAsDecimal(Pos.DTR_IMP_ADER, Len.Int.DTR_IMP_ADER, Len.Fract.DTR_IMP_ADER);
    }

    public byte[] getDtrImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_IMP_ADER, Pos.DTR_IMP_ADER);
        return buffer;
    }

    public void setDtrImpAderNull(String dtrImpAderNull) {
        writeString(Pos.DTR_IMP_ADER_NULL, dtrImpAderNull, Len.DTR_IMP_ADER_NULL);
    }

    /**Original name: DTR-IMP-ADER-NULL<br>*/
    public String getDtrImpAderNull() {
        return readString(Pos.DTR_IMP_ADER_NULL, Len.DTR_IMP_ADER_NULL);
    }

    public String getDtrImpAderNullFormatted() {
        return Functions.padBlanks(getDtrImpAderNull(), Len.DTR_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_IMP_ADER = 1;
        public static final int DTR_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_IMP_ADER = 8;
        public static final int DTR_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
