package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-IMP-TFR-STRC<br>
 * Variable: WDTR-IMP-TFR-STRC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_IMP_TFR_STRC;
    }

    public void setWdtrImpTfrStrc(AfDecimal wdtrImpTfrStrc) {
        writeDecimalAsPacked(Pos.WDTR_IMP_TFR_STRC, wdtrImpTfrStrc.copy());
    }

    /**Original name: WDTR-IMP-TFR-STRC<br>*/
    public AfDecimal getWdtrImpTfrStrc() {
        return readPackedAsDecimal(Pos.WDTR_IMP_TFR_STRC, Len.Int.WDTR_IMP_TFR_STRC, Len.Fract.WDTR_IMP_TFR_STRC);
    }

    public void setWdtrImpTfrStrcNull(String wdtrImpTfrStrcNull) {
        writeString(Pos.WDTR_IMP_TFR_STRC_NULL, wdtrImpTfrStrcNull, Len.WDTR_IMP_TFR_STRC_NULL);
    }

    /**Original name: WDTR-IMP-TFR-STRC-NULL<br>*/
    public String getWdtrImpTfrStrcNull() {
        return readString(Pos.WDTR_IMP_TFR_STRC_NULL, Len.WDTR_IMP_TFR_STRC_NULL);
    }

    public String getWdtrImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getWdtrImpTfrStrcNull(), Len.WDTR_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_TFR_STRC = 1;
        public static final int WDTR_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_TFR_STRC = 8;
        public static final int WDTR_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
