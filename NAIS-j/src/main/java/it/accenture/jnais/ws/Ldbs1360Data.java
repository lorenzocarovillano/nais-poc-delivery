package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTrchDiGar;
import it.accenture.jnais.copy.Ldbv1351;
import it.accenture.jnais.copy.TrchDiGarDb;
import it.accenture.jnais.ws.enums.FlagCausale;
import it.accenture.jnais.ws.enums.FlagStato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1360<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1360Data implements IStatOggBusTrchDiGar {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WK-ID-ADES-DA
    private int wkIdAdesDa = DefaultValues.INT_VAL;
    //Original name: WK-ID-ADES-A
    private int wkIdAdesA = DefaultValues.INT_VAL;
    //Original name: WK-ID-GAR-DA
    private int wkIdGarDa = DefaultValues.INT_VAL;
    //Original name: WK-ID-GAR-A
    private int wkIdGarA = DefaultValues.INT_VAL;
    /**Original name: FLAG-STATO<br>
	 * <pre>------------------------------------------------------------
	 *  FLAGS
	 * ------------------------------------------------------------</pre>*/
    private FlagStato flagStato = new FlagStato();
    //Original name: FLAG-CAUSALE
    private FlagCausale flagCausale = new FlagCausale();
    /**Original name: NEGAZIONE<br>
	 * <pre>------------------------------------------------------------
	 *  COSTANTI
	 * ------------------------------------------------------------</pre>*/
    private String negazione = "NOT";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: LDBV1361
    private Ldbv1351 ldbv1361 = new Ldbv1351();
    //Original name: IND-TRCH-DI-GAR
    private IndTrchDiGar indTrchDiGar = new IndTrchDiGar();
    //Original name: TRCH-DI-GAR-DB
    private TrchDiGarDb trchDiGarDb = new TrchDiGarDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        this.wkIdAdesDa = wkIdAdesDa;
    }

    @Override
    public int getWkIdAdesDa() {
        return this.wkIdAdesDa;
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        this.wkIdAdesA = wkIdAdesA;
    }

    @Override
    public int getWkIdAdesA() {
        return this.wkIdAdesA;
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        this.wkIdGarDa = wkIdGarDa;
    }

    @Override
    public int getWkIdGarDa() {
        return this.wkIdGarDa;
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        this.wkIdGarA = wkIdGarA;
    }

    @Override
    public int getWkIdGarA() {
        return this.wkIdGarA;
    }

    public String getNegazione() {
        return this.negazione;
    }

    public FlagCausale getFlagCausale() {
        return flagCausale;
    }

    public FlagStato getFlagStato() {
        return flagStato;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTrchDiGar getIndTrchDiGar() {
        return indTrchDiGar;
    }

    @Override
    public int getLc601IdMoviCrz() {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public void setLc601IdMoviCrz(int lc601IdMoviCrz) {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public int getLc601IdTrchDiGar() {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public void setLc601IdTrchDiGar(int lc601IdTrchDiGar) {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public int getLdbi0731IdAdes() {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public int getLdbi0731IdPoli() {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public int getLdbi0731IdTrch() {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public void setLdbi0731IdTrch(int ldbi0731IdTrch) {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public String getLdbi0731TpStatBus() {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    public Ldbv1351 getLdbv1361() {
        return ldbv1361;
    }

    @Override
    public String getLdbv1361TpCaus01() {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public void setLdbv1361TpCaus01(String ldbv1361TpCaus01) {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public String getLdbv1361TpCaus02() {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public void setLdbv1361TpCaus02(String ldbv1361TpCaus02) {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public String getLdbv1361TpCaus03() {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public void setLdbv1361TpCaus03(String ldbv1361TpCaus03) {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public String getLdbv1361TpCaus04() {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public void setLdbv1361TpCaus04(String ldbv1361TpCaus04) {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public String getLdbv1361TpCaus05() {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public void setLdbv1361TpCaus05(String ldbv1361TpCaus05) {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public String getLdbv1361TpCaus06() {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public void setLdbv1361TpCaus06(String ldbv1361TpCaus06) {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public String getLdbv1361TpCaus07() {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public void setLdbv1361TpCaus07(String ldbv1361TpCaus07) {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public String getLdbv1361TpStatBus01() {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01) {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public String getLdbv1361TpStatBus02() {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02) {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public String getLdbv1361TpStatBus03() {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03) {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public String getLdbv1361TpStatBus04() {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04) {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public String getLdbv1361TpStatBus05() {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05) {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public String getLdbv1361TpStatBus06() {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06) {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public String getLdbv1361TpStatBus07() {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07) {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public int getLdbv2911IdAdes() {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public void setLdbv2911IdAdes(int ldbv2911IdAdes) {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public int getLdbv2911IdPoli() {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public void setLdbv2911IdPoli(int ldbv2911IdPoli) {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public AfDecimal getLdbv2911PrstzIniNewfis() {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis) {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public int getLdbv3021IdOgg() {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public void setLdbv3021IdOgg(int ldbv3021IdOgg) {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public String getLdbv3021TpCausBus10() {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10) {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public String getLdbv3021TpCausBus11() {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11) {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public String getLdbv3021TpCausBus12() {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12) {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public String getLdbv3021TpCausBus13() {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13) {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public String getLdbv3021TpCausBus14() {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14) {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public String getLdbv3021TpCausBus15() {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15) {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public String getLdbv3021TpCausBus16() {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16) {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public String getLdbv3021TpCausBus17() {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17) {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public String getLdbv3021TpCausBus18() {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18) {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public String getLdbv3021TpCausBus19() {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19) {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public String getLdbv3021TpCausBus1() {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1) {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public String getLdbv3021TpCausBus20() {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20) {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public String getLdbv3021TpCausBus2() {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2) {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public String getLdbv3021TpCausBus3() {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3) {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public String getLdbv3021TpCausBus4() {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4) {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public String getLdbv3021TpCausBus5() {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5) {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public String getLdbv3021TpCausBus6() {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6) {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public String getLdbv3021TpCausBus7() {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7) {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public String getLdbv3021TpCausBus8() {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8) {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public String getLdbv3021TpCausBus9() {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9) {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public String getLdbv3021TpOgg() {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public void setLdbv3021TpOgg(String ldbv3021TpOgg) {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public String getLdbv3021TpStatBus() {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public void setLdbv3021TpStatBus(String ldbv3021TpStatBus) {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public int getLdbv3421IdAdes() {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public int getLdbv3421IdGar() {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public int getLdbv3421IdPoli() {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public String getLdbv3421TpOgg() {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public String getLdbv3421TpStatBus1() {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public String getLdbv3421TpStatBus2() {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public int getLdbvd511IdAdes() {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public void setLdbvd511IdAdes(int ldbvd511IdAdes) {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public int getLdbvd511IdPoli() {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public void setLdbvd511IdPoli(int ldbvd511IdPoli) {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public String getLdbvd511TpCaus() {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public void setLdbvd511TpCaus(String ldbvd511TpCaus) {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public String getLdbvd511TpOgg() {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public void setLdbvd511TpOgg(String ldbvd511TpOgg) {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public String getLdbvd511TpStatBus() {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public void setLdbvd511TpStatBus(String ldbvd511TpStatBus) {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public int getLdbve251IdAdes() {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public void setLdbve251IdAdes(int ldbve251IdAdes) {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public int getLdbve251IdMovi() {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public void setLdbve251IdMovi(int ldbve251IdMovi) {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public AfDecimal getLdbve251ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public String getLdbve251TpTrch10() {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public void setLdbve251TpTrch10(String ldbve251TpTrch10) {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public String getLdbve251TpTrch11() {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public void setLdbve251TpTrch11(String ldbve251TpTrch11) {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public String getLdbve251TpTrch12() {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public void setLdbve251TpTrch12(String ldbve251TpTrch12) {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public String getLdbve251TpTrch13() {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public void setLdbve251TpTrch13(String ldbve251TpTrch13) {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public String getLdbve251TpTrch14() {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public void setLdbve251TpTrch14(String ldbve251TpTrch14) {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public String getLdbve251TpTrch15() {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public void setLdbve251TpTrch15(String ldbve251TpTrch15) {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public String getLdbve251TpTrch16() {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public void setLdbve251TpTrch16(String ldbve251TpTrch16) {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public String getLdbve251TpTrch1() {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public void setLdbve251TpTrch1(String ldbve251TpTrch1) {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public String getLdbve251TpTrch2() {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public void setLdbve251TpTrch2(String ldbve251TpTrch2) {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public String getLdbve251TpTrch3() {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public void setLdbve251TpTrch3(String ldbve251TpTrch3) {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public String getLdbve251TpTrch4() {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public void setLdbve251TpTrch4(String ldbve251TpTrch4) {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public String getLdbve251TpTrch5() {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public void setLdbve251TpTrch5(String ldbve251TpTrch5) {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public String getLdbve251TpTrch6() {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public void setLdbve251TpTrch6(String ldbve251TpTrch6) {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public String getLdbve251TpTrch7() {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public void setLdbve251TpTrch7(String ldbve251TpTrch7) {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public String getLdbve251TpTrch8() {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public void setLdbve251TpTrch8(String ldbve251TpTrch8) {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public String getLdbve251TpTrch9() {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public void setLdbve251TpTrch9(String ldbve251TpTrch9) {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public int getLdbve261IdAdes() {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public void setLdbve261IdAdes(int ldbve261IdAdes) {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public AfDecimal getLdbve261ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public String getLdbve261TpTrch10() {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public void setLdbve261TpTrch10(String ldbve261TpTrch10) {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public String getLdbve261TpTrch11() {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public void setLdbve261TpTrch11(String ldbve261TpTrch11) {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public String getLdbve261TpTrch12() {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public void setLdbve261TpTrch12(String ldbve261TpTrch12) {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public String getLdbve261TpTrch13() {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public void setLdbve261TpTrch13(String ldbve261TpTrch13) {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public String getLdbve261TpTrch14() {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public void setLdbve261TpTrch14(String ldbve261TpTrch14) {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public String getLdbve261TpTrch15() {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public void setLdbve261TpTrch15(String ldbve261TpTrch15) {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public String getLdbve261TpTrch16() {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public void setLdbve261TpTrch16(String ldbve261TpTrch16) {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public String getLdbve261TpTrch1() {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public void setLdbve261TpTrch1(String ldbve261TpTrch1) {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public String getLdbve261TpTrch2() {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public void setLdbve261TpTrch2(String ldbve261TpTrch2) {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public String getLdbve261TpTrch3() {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public void setLdbve261TpTrch3(String ldbve261TpTrch3) {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public String getLdbve261TpTrch4() {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public void setLdbve261TpTrch4(String ldbve261TpTrch4) {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public String getLdbve261TpTrch5() {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public void setLdbve261TpTrch5(String ldbve261TpTrch5) {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public String getLdbve261TpTrch6() {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public void setLdbve261TpTrch6(String ldbve261TpTrch6) {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public String getLdbve261TpTrch7() {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public void setLdbve261TpTrch7(String ldbve261TpTrch7) {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public String getLdbve261TpTrch8() {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public void setLdbve261TpTrch8(String ldbve261TpTrch8) {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public String getLdbve261TpTrch9() {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public void setLdbve261TpTrch9(String ldbve261TpTrch9) {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public int getStbCodCompAnia() {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public char getStbDsOperSql() {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public long getStbDsRiga() {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public char getStbDsStatoElab() {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public long getStbDsTsEndCptz() {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public long getStbDsTsIniCptz() {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public String getStbDsUtente() {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public int getStbDsVer() {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public String getStbDtEndEffDb() {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public String getStbDtIniEffDb() {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public int getStbIdMoviChiu() {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        return ((Integer)getStbIdMoviChiu());
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        setStbIdMoviChiu(((int)stbIdMoviChiuObj));
    }

    @Override
    public int getStbIdMoviCrz() {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public int getStbIdOgg() {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public int getStbIdStatOggBus() {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public String getStbTpCaus() {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public String getStbTpOgg() {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public String getStbTpStatBus() {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public AfDecimal getTgaAbbAnnuUlt() {
        throw new FieldNotMappedException("tgaAbbAnnuUlt");
    }

    @Override
    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        throw new FieldNotMappedException("tgaAbbAnnuUlt");
    }

    @Override
    public AfDecimal getTgaAbbAnnuUltObj() {
        return getTgaAbbAnnuUlt();
    }

    @Override
    public void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj) {
        setTgaAbbAnnuUlt(new AfDecimal(tgaAbbAnnuUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAbbTotIni() {
        throw new FieldNotMappedException("tgaAbbTotIni");
    }

    @Override
    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        throw new FieldNotMappedException("tgaAbbTotIni");
    }

    @Override
    public AfDecimal getTgaAbbTotIniObj() {
        return getTgaAbbTotIni();
    }

    @Override
    public void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj) {
        setTgaAbbTotIni(new AfDecimal(tgaAbbTotIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAbbTotUlt() {
        throw new FieldNotMappedException("tgaAbbTotUlt");
    }

    @Override
    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        throw new FieldNotMappedException("tgaAbbTotUlt");
    }

    @Override
    public AfDecimal getTgaAbbTotUltObj() {
        return getTgaAbbTotUlt();
    }

    @Override
    public void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj) {
        setTgaAbbTotUlt(new AfDecimal(tgaAbbTotUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAcqExp() {
        throw new FieldNotMappedException("tgaAcqExp");
    }

    @Override
    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        throw new FieldNotMappedException("tgaAcqExp");
    }

    @Override
    public AfDecimal getTgaAcqExpObj() {
        return getTgaAcqExp();
    }

    @Override
    public void setTgaAcqExpObj(AfDecimal tgaAcqExpObj) {
        setTgaAcqExp(new AfDecimal(tgaAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAlqCommisInter() {
        throw new FieldNotMappedException("tgaAlqCommisInter");
    }

    @Override
    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        throw new FieldNotMappedException("tgaAlqCommisInter");
    }

    @Override
    public AfDecimal getTgaAlqCommisInterObj() {
        return getTgaAlqCommisInter();
    }

    @Override
    public void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj) {
        setTgaAlqCommisInter(new AfDecimal(tgaAlqCommisInterObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqProvAcq() {
        throw new FieldNotMappedException("tgaAlqProvAcq");
    }

    @Override
    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        throw new FieldNotMappedException("tgaAlqProvAcq");
    }

    @Override
    public AfDecimal getTgaAlqProvAcqObj() {
        return getTgaAlqProvAcq();
    }

    @Override
    public void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj) {
        setTgaAlqProvAcq(new AfDecimal(tgaAlqProvAcqObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqProvInc() {
        throw new FieldNotMappedException("tgaAlqProvInc");
    }

    @Override
    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        throw new FieldNotMappedException("tgaAlqProvInc");
    }

    @Override
    public AfDecimal getTgaAlqProvIncObj() {
        return getTgaAlqProvInc();
    }

    @Override
    public void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj) {
        setTgaAlqProvInc(new AfDecimal(tgaAlqProvIncObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqProvRicor() {
        throw new FieldNotMappedException("tgaAlqProvRicor");
    }

    @Override
    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        throw new FieldNotMappedException("tgaAlqProvRicor");
    }

    @Override
    public AfDecimal getTgaAlqProvRicorObj() {
        return getTgaAlqProvRicor();
    }

    @Override
    public void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj) {
        setTgaAlqProvRicor(new AfDecimal(tgaAlqProvRicorObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqRemunAss() {
        throw new FieldNotMappedException("tgaAlqRemunAss");
    }

    @Override
    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        throw new FieldNotMappedException("tgaAlqRemunAss");
    }

    @Override
    public AfDecimal getTgaAlqRemunAssObj() {
        return getTgaAlqRemunAss();
    }

    @Override
    public void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj) {
        setTgaAlqRemunAss(new AfDecimal(tgaAlqRemunAssObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqScon() {
        throw new FieldNotMappedException("tgaAlqScon");
    }

    @Override
    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        throw new FieldNotMappedException("tgaAlqScon");
    }

    @Override
    public AfDecimal getTgaAlqSconObj() {
        return getTgaAlqScon();
    }

    @Override
    public void setTgaAlqSconObj(AfDecimal tgaAlqSconObj) {
        setTgaAlqScon(new AfDecimal(tgaAlqSconObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqto() {
        throw new FieldNotMappedException("tgaBnsGiaLiqto");
    }

    @Override
    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        throw new FieldNotMappedException("tgaBnsGiaLiqto");
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqtoObj() {
        return getTgaBnsGiaLiqto();
    }

    @Override
    public void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj) {
        setTgaBnsGiaLiqto(new AfDecimal(tgaBnsGiaLiqtoObj, 15, 3));
    }

    @Override
    public int getTgaCodCompAnia() {
        throw new FieldNotMappedException("tgaCodCompAnia");
    }

    @Override
    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        throw new FieldNotMappedException("tgaCodCompAnia");
    }

    @Override
    public String getTgaCodDvs() {
        throw new FieldNotMappedException("tgaCodDvs");
    }

    @Override
    public void setTgaCodDvs(String tgaCodDvs) {
        throw new FieldNotMappedException("tgaCodDvs");
    }

    @Override
    public AfDecimal getTgaCommisGest() {
        throw new FieldNotMappedException("tgaCommisGest");
    }

    @Override
    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        throw new FieldNotMappedException("tgaCommisGest");
    }

    @Override
    public AfDecimal getTgaCommisGestObj() {
        return getTgaCommisGest();
    }

    @Override
    public void setTgaCommisGestObj(AfDecimal tgaCommisGestObj) {
        setTgaCommisGest(new AfDecimal(tgaCommisGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCommisInter() {
        throw new FieldNotMappedException("tgaCommisInter");
    }

    @Override
    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        throw new FieldNotMappedException("tgaCommisInter");
    }

    @Override
    public AfDecimal getTgaCommisInterObj() {
        return getTgaCommisInter();
    }

    @Override
    public void setTgaCommisInterObj(AfDecimal tgaCommisInterObj) {
        setTgaCommisInter(new AfDecimal(tgaCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCosRunAssva() {
        throw new FieldNotMappedException("tgaCosRunAssva");
    }

    @Override
    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        throw new FieldNotMappedException("tgaCosRunAssva");
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdc() {
        throw new FieldNotMappedException("tgaCosRunAssvaIdc");
    }

    @Override
    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        throw new FieldNotMappedException("tgaCosRunAssvaIdc");
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdcObj() {
        return getTgaCosRunAssvaIdc();
    }

    @Override
    public void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj) {
        setTgaCosRunAssvaIdc(new AfDecimal(tgaCosRunAssvaIdcObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCosRunAssvaObj() {
        return getTgaCosRunAssva();
    }

    @Override
    public void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj) {
        setTgaCosRunAssva(new AfDecimal(tgaCosRunAssvaObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCptInOpzRivto() {
        throw new FieldNotMappedException("tgaCptInOpzRivto");
    }

    @Override
    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        throw new FieldNotMappedException("tgaCptInOpzRivto");
    }

    @Override
    public AfDecimal getTgaCptInOpzRivtoObj() {
        return getTgaCptInOpzRivto();
    }

    @Override
    public void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj) {
        setTgaCptInOpzRivto(new AfDecimal(tgaCptInOpzRivtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCptMinScad() {
        throw new FieldNotMappedException("tgaCptMinScad");
    }

    @Override
    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        throw new FieldNotMappedException("tgaCptMinScad");
    }

    @Override
    public AfDecimal getTgaCptMinScadObj() {
        return getTgaCptMinScad();
    }

    @Override
    public void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj) {
        setTgaCptMinScad(new AfDecimal(tgaCptMinScadObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCptRshMor() {
        throw new FieldNotMappedException("tgaCptRshMor");
    }

    @Override
    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        throw new FieldNotMappedException("tgaCptRshMor");
    }

    @Override
    public AfDecimal getTgaCptRshMorObj() {
        return getTgaCptRshMor();
    }

    @Override
    public void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj) {
        setTgaCptRshMor(new AfDecimal(tgaCptRshMorObj, 15, 3));
    }

    @Override
    public char getTgaDsOperSql() {
        throw new FieldNotMappedException("tgaDsOperSql");
    }

    @Override
    public void setTgaDsOperSql(char tgaDsOperSql) {
        throw new FieldNotMappedException("tgaDsOperSql");
    }

    @Override
    public long getTgaDsRiga() {
        throw new FieldNotMappedException("tgaDsRiga");
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        throw new FieldNotMappedException("tgaDsRiga");
    }

    @Override
    public char getTgaDsStatoElab() {
        throw new FieldNotMappedException("tgaDsStatoElab");
    }

    @Override
    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        throw new FieldNotMappedException("tgaDsStatoElab");
    }

    @Override
    public long getTgaDsTsEndCptz() {
        throw new FieldNotMappedException("tgaDsTsEndCptz");
    }

    @Override
    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        throw new FieldNotMappedException("tgaDsTsEndCptz");
    }

    @Override
    public long getTgaDsTsIniCptz() {
        throw new FieldNotMappedException("tgaDsTsIniCptz");
    }

    @Override
    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        throw new FieldNotMappedException("tgaDsTsIniCptz");
    }

    @Override
    public String getTgaDsUtente() {
        throw new FieldNotMappedException("tgaDsUtente");
    }

    @Override
    public void setTgaDsUtente(String tgaDsUtente) {
        throw new FieldNotMappedException("tgaDsUtente");
    }

    @Override
    public int getTgaDsVer() {
        throw new FieldNotMappedException("tgaDsVer");
    }

    @Override
    public void setTgaDsVer(int tgaDsVer) {
        throw new FieldNotMappedException("tgaDsVer");
    }

    @Override
    public String getTgaDtDecorDb() {
        throw new FieldNotMappedException("tgaDtDecorDb");
    }

    @Override
    public void setTgaDtDecorDb(String tgaDtDecorDb) {
        throw new FieldNotMappedException("tgaDtDecorDb");
    }

    @Override
    public String getTgaDtEffStabDb() {
        throw new FieldNotMappedException("tgaDtEffStabDb");
    }

    @Override
    public void setTgaDtEffStabDb(String tgaDtEffStabDb) {
        throw new FieldNotMappedException("tgaDtEffStabDb");
    }

    @Override
    public String getTgaDtEffStabDbObj() {
        return getTgaDtEffStabDb();
    }

    @Override
    public void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj) {
        setTgaDtEffStabDb(tgaDtEffStabDbObj);
    }

    @Override
    public String getTgaDtEmisDb() {
        throw new FieldNotMappedException("tgaDtEmisDb");
    }

    @Override
    public void setTgaDtEmisDb(String tgaDtEmisDb) {
        throw new FieldNotMappedException("tgaDtEmisDb");
    }

    @Override
    public String getTgaDtEmisDbObj() {
        return getTgaDtEmisDb();
    }

    @Override
    public void setTgaDtEmisDbObj(String tgaDtEmisDbObj) {
        setTgaDtEmisDb(tgaDtEmisDbObj);
    }

    @Override
    public String getTgaDtEndEffDb() {
        throw new FieldNotMappedException("tgaDtEndEffDb");
    }

    @Override
    public void setTgaDtEndEffDb(String tgaDtEndEffDb) {
        throw new FieldNotMappedException("tgaDtEndEffDb");
    }

    @Override
    public String getTgaDtIniEffDb() {
        throw new FieldNotMappedException("tgaDtIniEffDb");
    }

    @Override
    public void setTgaDtIniEffDb(String tgaDtIniEffDb) {
        throw new FieldNotMappedException("tgaDtIniEffDb");
    }

    @Override
    public String getTgaDtIniValTarDb() {
        throw new FieldNotMappedException("tgaDtIniValTarDb");
    }

    @Override
    public void setTgaDtIniValTarDb(String tgaDtIniValTarDb) {
        throw new FieldNotMappedException("tgaDtIniValTarDb");
    }

    @Override
    public String getTgaDtIniValTarDbObj() {
        return getTgaDtIniValTarDb();
    }

    @Override
    public void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj) {
        setTgaDtIniValTarDb(tgaDtIniValTarDbObj);
    }

    @Override
    public String getTgaDtScadDb() {
        throw new FieldNotMappedException("tgaDtScadDb");
    }

    @Override
    public void setTgaDtScadDb(String tgaDtScadDb) {
        throw new FieldNotMappedException("tgaDtScadDb");
    }

    @Override
    public String getTgaDtScadDbObj() {
        return getTgaDtScadDb();
    }

    @Override
    public void setTgaDtScadDbObj(String tgaDtScadDbObj) {
        setTgaDtScadDb(tgaDtScadDbObj);
    }

    @Override
    public String getTgaDtUltAdegPrePrDb() {
        throw new FieldNotMappedException("tgaDtUltAdegPrePrDb");
    }

    @Override
    public void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb) {
        throw new FieldNotMappedException("tgaDtUltAdegPrePrDb");
    }

    @Override
    public String getTgaDtUltAdegPrePrDbObj() {
        return getTgaDtUltAdegPrePrDb();
    }

    @Override
    public void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj) {
        setTgaDtUltAdegPrePrDb(tgaDtUltAdegPrePrDbObj);
    }

    @Override
    public String getTgaDtVldtProdDb() {
        return trchDiGarDb.getVldtProdDb();
    }

    @Override
    public void setTgaDtVldtProdDb(String tgaDtVldtProdDb) {
        this.trchDiGarDb.setVldtProdDb(tgaDtVldtProdDb);
    }

    @Override
    public String getTgaDtVldtProdDbObj() {
        if (indTrchDiGar.getDtVldtProd() >= 0) {
            return getTgaDtVldtProdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj) {
        if (tgaDtVldtProdDbObj != null) {
            setTgaDtVldtProdDb(tgaDtVldtProdDbObj);
            indTrchDiGar.setDtVldtProd(((short)0));
        }
        else {
            indTrchDiGar.setDtVldtProd(((short)-1));
        }
    }

    @Override
    public int getTgaDurAa() {
        throw new FieldNotMappedException("tgaDurAa");
    }

    @Override
    public void setTgaDurAa(int tgaDurAa) {
        throw new FieldNotMappedException("tgaDurAa");
    }

    @Override
    public Integer getTgaDurAaObj() {
        return ((Integer)getTgaDurAa());
    }

    @Override
    public void setTgaDurAaObj(Integer tgaDurAaObj) {
        setTgaDurAa(((int)tgaDurAaObj));
    }

    @Override
    public int getTgaDurAbb() {
        throw new FieldNotMappedException("tgaDurAbb");
    }

    @Override
    public void setTgaDurAbb(int tgaDurAbb) {
        throw new FieldNotMappedException("tgaDurAbb");
    }

    @Override
    public Integer getTgaDurAbbObj() {
        return ((Integer)getTgaDurAbb());
    }

    @Override
    public void setTgaDurAbbObj(Integer tgaDurAbbObj) {
        setTgaDurAbb(((int)tgaDurAbbObj));
    }

    @Override
    public int getTgaDurGg() {
        throw new FieldNotMappedException("tgaDurGg");
    }

    @Override
    public void setTgaDurGg(int tgaDurGg) {
        throw new FieldNotMappedException("tgaDurGg");
    }

    @Override
    public Integer getTgaDurGgObj() {
        return ((Integer)getTgaDurGg());
    }

    @Override
    public void setTgaDurGgObj(Integer tgaDurGgObj) {
        setTgaDurGg(((int)tgaDurGgObj));
    }

    @Override
    public int getTgaDurMm() {
        throw new FieldNotMappedException("tgaDurMm");
    }

    @Override
    public void setTgaDurMm(int tgaDurMm) {
        throw new FieldNotMappedException("tgaDurMm");
    }

    @Override
    public Integer getTgaDurMmObj() {
        return ((Integer)getTgaDurMm());
    }

    @Override
    public void setTgaDurMmObj(Integer tgaDurMmObj) {
        setTgaDurMm(((int)tgaDurMmObj));
    }

    @Override
    public short getTgaEtaAa1oAssto() {
        throw new FieldNotMappedException("tgaEtaAa1oAssto");
    }

    @Override
    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        throw new FieldNotMappedException("tgaEtaAa1oAssto");
    }

    @Override
    public Short getTgaEtaAa1oAsstoObj() {
        return ((Short)getTgaEtaAa1oAssto());
    }

    @Override
    public void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj) {
        setTgaEtaAa1oAssto(((short)tgaEtaAa1oAsstoObj));
    }

    @Override
    public short getTgaEtaAa2oAssto() {
        throw new FieldNotMappedException("tgaEtaAa2oAssto");
    }

    @Override
    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        throw new FieldNotMappedException("tgaEtaAa2oAssto");
    }

    @Override
    public Short getTgaEtaAa2oAsstoObj() {
        return ((Short)getTgaEtaAa2oAssto());
    }

    @Override
    public void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj) {
        setTgaEtaAa2oAssto(((short)tgaEtaAa2oAsstoObj));
    }

    @Override
    public short getTgaEtaAa3oAssto() {
        throw new FieldNotMappedException("tgaEtaAa3oAssto");
    }

    @Override
    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        throw new FieldNotMappedException("tgaEtaAa3oAssto");
    }

    @Override
    public Short getTgaEtaAa3oAsstoObj() {
        return ((Short)getTgaEtaAa3oAssto());
    }

    @Override
    public void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj) {
        setTgaEtaAa3oAssto(((short)tgaEtaAa3oAsstoObj));
    }

    @Override
    public short getTgaEtaMm1oAssto() {
        throw new FieldNotMappedException("tgaEtaMm1oAssto");
    }

    @Override
    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        throw new FieldNotMappedException("tgaEtaMm1oAssto");
    }

    @Override
    public Short getTgaEtaMm1oAsstoObj() {
        return ((Short)getTgaEtaMm1oAssto());
    }

    @Override
    public void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj) {
        setTgaEtaMm1oAssto(((short)tgaEtaMm1oAsstoObj));
    }

    @Override
    public short getTgaEtaMm2oAssto() {
        throw new FieldNotMappedException("tgaEtaMm2oAssto");
    }

    @Override
    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        throw new FieldNotMappedException("tgaEtaMm2oAssto");
    }

    @Override
    public Short getTgaEtaMm2oAsstoObj() {
        return ((Short)getTgaEtaMm2oAssto());
    }

    @Override
    public void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj) {
        setTgaEtaMm2oAssto(((short)tgaEtaMm2oAsstoObj));
    }

    @Override
    public short getTgaEtaMm3oAssto() {
        throw new FieldNotMappedException("tgaEtaMm3oAssto");
    }

    @Override
    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        throw new FieldNotMappedException("tgaEtaMm3oAssto");
    }

    @Override
    public Short getTgaEtaMm3oAsstoObj() {
        return ((Short)getTgaEtaMm3oAssto());
    }

    @Override
    public void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj) {
        setTgaEtaMm3oAssto(((short)tgaEtaMm3oAsstoObj));
    }

    @Override
    public char getTgaFlCarCont() {
        throw new FieldNotMappedException("tgaFlCarCont");
    }

    @Override
    public void setTgaFlCarCont(char tgaFlCarCont) {
        throw new FieldNotMappedException("tgaFlCarCont");
    }

    @Override
    public Character getTgaFlCarContObj() {
        return ((Character)getTgaFlCarCont());
    }

    @Override
    public void setTgaFlCarContObj(Character tgaFlCarContObj) {
        setTgaFlCarCont(((char)tgaFlCarContObj));
    }

    @Override
    public char getTgaFlImportiForz() {
        throw new FieldNotMappedException("tgaFlImportiForz");
    }

    @Override
    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        throw new FieldNotMappedException("tgaFlImportiForz");
    }

    @Override
    public Character getTgaFlImportiForzObj() {
        return ((Character)getTgaFlImportiForz());
    }

    @Override
    public void setTgaFlImportiForzObj(Character tgaFlImportiForzObj) {
        setTgaFlImportiForz(((char)tgaFlImportiForzObj));
    }

    @Override
    public char getTgaFlProvForz() {
        throw new FieldNotMappedException("tgaFlProvForz");
    }

    @Override
    public void setTgaFlProvForz(char tgaFlProvForz) {
        throw new FieldNotMappedException("tgaFlProvForz");
    }

    @Override
    public Character getTgaFlProvForzObj() {
        return ((Character)getTgaFlProvForz());
    }

    @Override
    public void setTgaFlProvForzObj(Character tgaFlProvForzObj) {
        setTgaFlProvForz(((char)tgaFlProvForzObj));
    }

    @Override
    public String getTgaIbOgg() {
        throw new FieldNotMappedException("tgaIbOgg");
    }

    @Override
    public void setTgaIbOgg(String tgaIbOgg) {
        throw new FieldNotMappedException("tgaIbOgg");
    }

    @Override
    public String getTgaIbOggObj() {
        return getTgaIbOgg();
    }

    @Override
    public void setTgaIbOggObj(String tgaIbOggObj) {
        setTgaIbOgg(tgaIbOggObj);
    }

    @Override
    public int getTgaIdAdes() {
        throw new FieldNotMappedException("tgaIdAdes");
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        throw new FieldNotMappedException("tgaIdAdes");
    }

    @Override
    public int getTgaIdGar() {
        throw new FieldNotMappedException("tgaIdGar");
    }

    @Override
    public void setTgaIdGar(int tgaIdGar) {
        throw new FieldNotMappedException("tgaIdGar");
    }

    @Override
    public int getTgaIdMoviChiu() {
        throw new FieldNotMappedException("tgaIdMoviChiu");
    }

    @Override
    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        throw new FieldNotMappedException("tgaIdMoviChiu");
    }

    @Override
    public Integer getTgaIdMoviChiuObj() {
        return ((Integer)getTgaIdMoviChiu());
    }

    @Override
    public void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj) {
        setTgaIdMoviChiu(((int)tgaIdMoviChiuObj));
    }

    @Override
    public int getTgaIdMoviCrz() {
        throw new FieldNotMappedException("tgaIdMoviCrz");
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        throw new FieldNotMappedException("tgaIdMoviCrz");
    }

    @Override
    public int getTgaIdPoli() {
        throw new FieldNotMappedException("tgaIdPoli");
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        throw new FieldNotMappedException("tgaIdPoli");
    }

    @Override
    public int getTgaIdTrchDiGar() {
        throw new FieldNotMappedException("tgaIdTrchDiGar");
    }

    @Override
    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        throw new FieldNotMappedException("tgaIdTrchDiGar");
    }

    @Override
    public AfDecimal getTgaImpAder() {
        throw new FieldNotMappedException("tgaImpAder");
    }

    @Override
    public void setTgaImpAder(AfDecimal tgaImpAder) {
        throw new FieldNotMappedException("tgaImpAder");
    }

    @Override
    public AfDecimal getTgaImpAderObj() {
        return getTgaImpAder();
    }

    @Override
    public void setTgaImpAderObj(AfDecimal tgaImpAderObj) {
        setTgaImpAder(new AfDecimal(tgaImpAderObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpAltSopr() {
        throw new FieldNotMappedException("tgaImpAltSopr");
    }

    @Override
    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        throw new FieldNotMappedException("tgaImpAltSopr");
    }

    @Override
    public AfDecimal getTgaImpAltSoprObj() {
        return getTgaImpAltSopr();
    }

    @Override
    public void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj) {
        setTgaImpAltSopr(new AfDecimal(tgaImpAltSoprObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpAz() {
        throw new FieldNotMappedException("tgaImpAz");
    }

    @Override
    public void setTgaImpAz(AfDecimal tgaImpAz) {
        throw new FieldNotMappedException("tgaImpAz");
    }

    @Override
    public AfDecimal getTgaImpAzObj() {
        return getTgaImpAz();
    }

    @Override
    public void setTgaImpAzObj(AfDecimal tgaImpAzObj) {
        setTgaImpAz(new AfDecimal(tgaImpAzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpBns() {
        throw new FieldNotMappedException("tgaImpBns");
    }

    @Override
    public AfDecimal getTgaImpBnsAntic() {
        throw new FieldNotMappedException("tgaImpBnsAntic");
    }

    @Override
    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        throw new FieldNotMappedException("tgaImpBnsAntic");
    }

    @Override
    public AfDecimal getTgaImpBnsAnticObj() {
        return getTgaImpBnsAntic();
    }

    @Override
    public void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj) {
        setTgaImpBnsAntic(new AfDecimal(tgaImpBnsAnticObj, 15, 3));
    }

    @Override
    public void setTgaImpBns(AfDecimal tgaImpBns) {
        throw new FieldNotMappedException("tgaImpBns");
    }

    @Override
    public AfDecimal getTgaImpBnsObj() {
        return getTgaImpBns();
    }

    @Override
    public void setTgaImpBnsObj(AfDecimal tgaImpBnsObj) {
        setTgaImpBns(new AfDecimal(tgaImpBnsObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpCarAcq() {
        throw new FieldNotMappedException("tgaImpCarAcq");
    }

    @Override
    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        throw new FieldNotMappedException("tgaImpCarAcq");
    }

    @Override
    public AfDecimal getTgaImpCarAcqObj() {
        return getTgaImpCarAcq();
    }

    @Override
    public void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj) {
        setTgaImpCarAcq(new AfDecimal(tgaImpCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpCarGest() {
        throw new FieldNotMappedException("tgaImpCarGest");
    }

    @Override
    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        throw new FieldNotMappedException("tgaImpCarGest");
    }

    @Override
    public AfDecimal getTgaImpCarGestObj() {
        return getTgaImpCarGest();
    }

    @Override
    public void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj) {
        setTgaImpCarGest(new AfDecimal(tgaImpCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpCarInc() {
        throw new FieldNotMappedException("tgaImpCarInc");
    }

    @Override
    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        throw new FieldNotMappedException("tgaImpCarInc");
    }

    @Override
    public AfDecimal getTgaImpCarIncObj() {
        return getTgaImpCarInc();
    }

    @Override
    public void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj) {
        setTgaImpCarInc(new AfDecimal(tgaImpCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpScon() {
        throw new FieldNotMappedException("tgaImpScon");
    }

    @Override
    public void setTgaImpScon(AfDecimal tgaImpScon) {
        throw new FieldNotMappedException("tgaImpScon");
    }

    @Override
    public AfDecimal getTgaImpSconObj() {
        return getTgaImpScon();
    }

    @Override
    public void setTgaImpSconObj(AfDecimal tgaImpSconObj) {
        setTgaImpScon(new AfDecimal(tgaImpSconObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprProf() {
        throw new FieldNotMappedException("tgaImpSoprProf");
    }

    @Override
    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        throw new FieldNotMappedException("tgaImpSoprProf");
    }

    @Override
    public AfDecimal getTgaImpSoprProfObj() {
        return getTgaImpSoprProf();
    }

    @Override
    public void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj) {
        setTgaImpSoprProf(new AfDecimal(tgaImpSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprSan() {
        throw new FieldNotMappedException("tgaImpSoprSan");
    }

    @Override
    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        throw new FieldNotMappedException("tgaImpSoprSan");
    }

    @Override
    public AfDecimal getTgaImpSoprSanObj() {
        return getTgaImpSoprSan();
    }

    @Override
    public void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj) {
        setTgaImpSoprSan(new AfDecimal(tgaImpSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprSpo() {
        throw new FieldNotMappedException("tgaImpSoprSpo");
    }

    @Override
    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        throw new FieldNotMappedException("tgaImpSoprSpo");
    }

    @Override
    public AfDecimal getTgaImpSoprSpoObj() {
        return getTgaImpSoprSpo();
    }

    @Override
    public void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj) {
        setTgaImpSoprSpo(new AfDecimal(tgaImpSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprTec() {
        throw new FieldNotMappedException("tgaImpSoprTec");
    }

    @Override
    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        throw new FieldNotMappedException("tgaImpSoprTec");
    }

    @Override
    public AfDecimal getTgaImpSoprTecObj() {
        return getTgaImpSoprTec();
    }

    @Override
    public void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj) {
        setTgaImpSoprTec(new AfDecimal(tgaImpSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpTfr() {
        throw new FieldNotMappedException("tgaImpTfr");
    }

    @Override
    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        throw new FieldNotMappedException("tgaImpTfr");
    }

    @Override
    public AfDecimal getTgaImpTfrObj() {
        return getTgaImpTfr();
    }

    @Override
    public void setTgaImpTfrObj(AfDecimal tgaImpTfrObj) {
        setTgaImpTfr(new AfDecimal(tgaImpTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpTfrStrc() {
        throw new FieldNotMappedException("tgaImpTfrStrc");
    }

    @Override
    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        throw new FieldNotMappedException("tgaImpTfrStrc");
    }

    @Override
    public AfDecimal getTgaImpTfrStrcObj() {
        return getTgaImpTfrStrc();
    }

    @Override
    public void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj) {
        setTgaImpTfrStrc(new AfDecimal(tgaImpTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpTrasfe() {
        throw new FieldNotMappedException("tgaImpTrasfe");
    }

    @Override
    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        throw new FieldNotMappedException("tgaImpTrasfe");
    }

    @Override
    public AfDecimal getTgaImpTrasfeObj() {
        return getTgaImpTrasfe();
    }

    @Override
    public void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj) {
        setTgaImpTrasfe(new AfDecimal(tgaImpTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpVolo() {
        throw new FieldNotMappedException("tgaImpVolo");
    }

    @Override
    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        throw new FieldNotMappedException("tgaImpVolo");
    }

    @Override
    public AfDecimal getTgaImpVoloObj() {
        return getTgaImpVolo();
    }

    @Override
    public void setTgaImpVoloObj(AfDecimal tgaImpVoloObj) {
        setTgaImpVolo(new AfDecimal(tgaImpVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbCommisInter() {
        throw new FieldNotMappedException("tgaImpbCommisInter");
    }

    @Override
    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        throw new FieldNotMappedException("tgaImpbCommisInter");
    }

    @Override
    public AfDecimal getTgaImpbCommisInterObj() {
        return getTgaImpbCommisInter();
    }

    @Override
    public void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj) {
        setTgaImpbCommisInter(new AfDecimal(tgaImpbCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbProvAcq() {
        throw new FieldNotMappedException("tgaImpbProvAcq");
    }

    @Override
    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        throw new FieldNotMappedException("tgaImpbProvAcq");
    }

    @Override
    public AfDecimal getTgaImpbProvAcqObj() {
        return getTgaImpbProvAcq();
    }

    @Override
    public void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj) {
        setTgaImpbProvAcq(new AfDecimal(tgaImpbProvAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbProvInc() {
        throw new FieldNotMappedException("tgaImpbProvInc");
    }

    @Override
    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        throw new FieldNotMappedException("tgaImpbProvInc");
    }

    @Override
    public AfDecimal getTgaImpbProvIncObj() {
        return getTgaImpbProvInc();
    }

    @Override
    public void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj) {
        setTgaImpbProvInc(new AfDecimal(tgaImpbProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbProvRicor() {
        throw new FieldNotMappedException("tgaImpbProvRicor");
    }

    @Override
    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        throw new FieldNotMappedException("tgaImpbProvRicor");
    }

    @Override
    public AfDecimal getTgaImpbProvRicorObj() {
        return getTgaImpbProvRicor();
    }

    @Override
    public void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj) {
        setTgaImpbProvRicor(new AfDecimal(tgaImpbProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbRemunAss() {
        throw new FieldNotMappedException("tgaImpbRemunAss");
    }

    @Override
    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        throw new FieldNotMappedException("tgaImpbRemunAss");
    }

    @Override
    public AfDecimal getTgaImpbRemunAssObj() {
        return getTgaImpbRemunAss();
    }

    @Override
    public void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj) {
        setTgaImpbRemunAss(new AfDecimal(tgaImpbRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000() {
        throw new FieldNotMappedException("tgaImpbVisEnd2000");
    }

    @Override
    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        throw new FieldNotMappedException("tgaImpbVisEnd2000");
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000Obj() {
        return getTgaImpbVisEnd2000();
    }

    @Override
    public void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj) {
        setTgaImpbVisEnd2000(new AfDecimal(tgaImpbVisEnd2000Obj, 15, 3));
    }

    @Override
    public AfDecimal getTgaIncrPre() {
        throw new FieldNotMappedException("tgaIncrPre");
    }

    @Override
    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        throw new FieldNotMappedException("tgaIncrPre");
    }

    @Override
    public AfDecimal getTgaIncrPreObj() {
        return getTgaIncrPre();
    }

    @Override
    public void setTgaIncrPreObj(AfDecimal tgaIncrPreObj) {
        setTgaIncrPre(new AfDecimal(tgaIncrPreObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaIncrPrstz() {
        throw new FieldNotMappedException("tgaIncrPrstz");
    }

    @Override
    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        throw new FieldNotMappedException("tgaIncrPrstz");
    }

    @Override
    public AfDecimal getTgaIncrPrstzObj() {
        return getTgaIncrPrstz();
    }

    @Override
    public void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj) {
        setTgaIncrPrstz(new AfDecimal(tgaIncrPrstzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaIntrMora() {
        throw new FieldNotMappedException("tgaIntrMora");
    }

    @Override
    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        throw new FieldNotMappedException("tgaIntrMora");
    }

    @Override
    public AfDecimal getTgaIntrMoraObj() {
        return getTgaIntrMora();
    }

    @Override
    public void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj) {
        setTgaIntrMora(new AfDecimal(tgaIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaManfeeAntic() {
        throw new FieldNotMappedException("tgaManfeeAntic");
    }

    @Override
    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        throw new FieldNotMappedException("tgaManfeeAntic");
    }

    @Override
    public AfDecimal getTgaManfeeAnticObj() {
        return getTgaManfeeAntic();
    }

    @Override
    public void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj) {
        setTgaManfeeAntic(new AfDecimal(tgaManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaManfeeRicor() {
        throw new FieldNotMappedException("tgaManfeeRicor");
    }

    @Override
    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        throw new FieldNotMappedException("tgaManfeeRicor");
    }

    @Override
    public AfDecimal getTgaManfeeRicorObj() {
        return getTgaManfeeRicor();
    }

    @Override
    public void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj) {
        setTgaManfeeRicor(new AfDecimal(tgaManfeeRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaMatuEnd2000() {
        throw new FieldNotMappedException("tgaMatuEnd2000");
    }

    @Override
    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        throw new FieldNotMappedException("tgaMatuEnd2000");
    }

    @Override
    public AfDecimal getTgaMatuEnd2000Obj() {
        return getTgaMatuEnd2000();
    }

    @Override
    public void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj) {
        setTgaMatuEnd2000(new AfDecimal(tgaMatuEnd2000Obj, 15, 3));
    }

    @Override
    public AfDecimal getTgaMinGarto() {
        throw new FieldNotMappedException("tgaMinGarto");
    }

    @Override
    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        throw new FieldNotMappedException("tgaMinGarto");
    }

    @Override
    public AfDecimal getTgaMinGartoObj() {
        return getTgaMinGarto();
    }

    @Override
    public void setTgaMinGartoObj(AfDecimal tgaMinGartoObj) {
        setTgaMinGarto(new AfDecimal(tgaMinGartoObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaMinTrnut() {
        throw new FieldNotMappedException("tgaMinTrnut");
    }

    @Override
    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        throw new FieldNotMappedException("tgaMinTrnut");
    }

    @Override
    public AfDecimal getTgaMinTrnutObj() {
        return getTgaMinTrnut();
    }

    @Override
    public void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj) {
        setTgaMinTrnut(new AfDecimal(tgaMinTrnutObj, 14, 9));
    }

    @Override
    public String getTgaModCalc() {
        throw new FieldNotMappedException("tgaModCalc");
    }

    @Override
    public void setTgaModCalc(String tgaModCalc) {
        throw new FieldNotMappedException("tgaModCalc");
    }

    @Override
    public String getTgaModCalcObj() {
        return getTgaModCalc();
    }

    @Override
    public void setTgaModCalcObj(String tgaModCalcObj) {
        setTgaModCalc(tgaModCalcObj);
    }

    @Override
    public int getTgaNumGgRival() {
        throw new FieldNotMappedException("tgaNumGgRival");
    }

    @Override
    public void setTgaNumGgRival(int tgaNumGgRival) {
        throw new FieldNotMappedException("tgaNumGgRival");
    }

    @Override
    public Integer getTgaNumGgRivalObj() {
        return ((Integer)getTgaNumGgRival());
    }

    @Override
    public void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj) {
        setTgaNumGgRival(((int)tgaNumGgRivalObj));
    }

    @Override
    public AfDecimal getTgaOldTsTec() {
        throw new FieldNotMappedException("tgaOldTsTec");
    }

    @Override
    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        throw new FieldNotMappedException("tgaOldTsTec");
    }

    @Override
    public AfDecimal getTgaOldTsTecObj() {
        return getTgaOldTsTec();
    }

    @Override
    public void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj) {
        setTgaOldTsTec(new AfDecimal(tgaOldTsTecObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaPcCommisGest() {
        throw new FieldNotMappedException("tgaPcCommisGest");
    }

    @Override
    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        throw new FieldNotMappedException("tgaPcCommisGest");
    }

    @Override
    public AfDecimal getTgaPcCommisGestObj() {
        return getTgaPcCommisGest();
    }

    @Override
    public void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj) {
        setTgaPcCommisGest(new AfDecimal(tgaPcCommisGestObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPcIntrRiat() {
        throw new FieldNotMappedException("tgaPcIntrRiat");
    }

    @Override
    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        throw new FieldNotMappedException("tgaPcIntrRiat");
    }

    @Override
    public AfDecimal getTgaPcIntrRiatObj() {
        return getTgaPcIntrRiat();
    }

    @Override
    public void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj) {
        setTgaPcIntrRiat(new AfDecimal(tgaPcIntrRiatObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPcRetr() {
        throw new FieldNotMappedException("tgaPcRetr");
    }

    @Override
    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        throw new FieldNotMappedException("tgaPcRetr");
    }

    @Override
    public AfDecimal getTgaPcRetrObj() {
        return getTgaPcRetr();
    }

    @Override
    public void setTgaPcRetrObj(AfDecimal tgaPcRetrObj) {
        setTgaPcRetr(new AfDecimal(tgaPcRetrObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPcRipPre() {
        throw new FieldNotMappedException("tgaPcRipPre");
    }

    @Override
    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        throw new FieldNotMappedException("tgaPcRipPre");
    }

    @Override
    public AfDecimal getTgaPcRipPreObj() {
        return getTgaPcRipPre();
    }

    @Override
    public void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj) {
        setTgaPcRipPre(new AfDecimal(tgaPcRipPreObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPreAttDiTrch() {
        throw new FieldNotMappedException("tgaPreAttDiTrch");
    }

    @Override
    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        throw new FieldNotMappedException("tgaPreAttDiTrch");
    }

    @Override
    public AfDecimal getTgaPreAttDiTrchObj() {
        return getTgaPreAttDiTrch();
    }

    @Override
    public void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj) {
        setTgaPreAttDiTrch(new AfDecimal(tgaPreAttDiTrchObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreCasoMor() {
        throw new FieldNotMappedException("tgaPreCasoMor");
    }

    @Override
    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        throw new FieldNotMappedException("tgaPreCasoMor");
    }

    @Override
    public AfDecimal getTgaPreCasoMorObj() {
        return getTgaPreCasoMor();
    }

    @Override
    public void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj) {
        setTgaPreCasoMor(new AfDecimal(tgaPreCasoMorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreIniNet() {
        throw new FieldNotMappedException("tgaPreIniNet");
    }

    @Override
    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        throw new FieldNotMappedException("tgaPreIniNet");
    }

    @Override
    public AfDecimal getTgaPreIniNetObj() {
        return getTgaPreIniNet();
    }

    @Override
    public void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj) {
        setTgaPreIniNet(new AfDecimal(tgaPreIniNetObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreInvrioIni() {
        throw new FieldNotMappedException("tgaPreInvrioIni");
    }

    @Override
    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        throw new FieldNotMappedException("tgaPreInvrioIni");
    }

    @Override
    public AfDecimal getTgaPreInvrioIniObj() {
        return getTgaPreInvrioIni();
    }

    @Override
    public void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj) {
        setTgaPreInvrioIni(new AfDecimal(tgaPreInvrioIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreInvrioUlt() {
        throw new FieldNotMappedException("tgaPreInvrioUlt");
    }

    @Override
    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        throw new FieldNotMappedException("tgaPreInvrioUlt");
    }

    @Override
    public AfDecimal getTgaPreInvrioUltObj() {
        return getTgaPreInvrioUlt();
    }

    @Override
    public void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj) {
        setTgaPreInvrioUlt(new AfDecimal(tgaPreInvrioUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreLrd() {
        throw new FieldNotMappedException("tgaPreLrd");
    }

    @Override
    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        throw new FieldNotMappedException("tgaPreLrd");
    }

    @Override
    public AfDecimal getTgaPreLrdObj() {
        return getTgaPreLrd();
    }

    @Override
    public void setTgaPreLrdObj(AfDecimal tgaPreLrdObj) {
        setTgaPreLrd(new AfDecimal(tgaPreLrdObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrePattuito() {
        throw new FieldNotMappedException("tgaPrePattuito");
    }

    @Override
    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        throw new FieldNotMappedException("tgaPrePattuito");
    }

    @Override
    public AfDecimal getTgaPrePattuitoObj() {
        return getTgaPrePattuito();
    }

    @Override
    public void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj) {
        setTgaPrePattuito(new AfDecimal(tgaPrePattuitoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrePpIni() {
        throw new FieldNotMappedException("tgaPrePpIni");
    }

    @Override
    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        throw new FieldNotMappedException("tgaPrePpIni");
    }

    @Override
    public AfDecimal getTgaPrePpIniObj() {
        return getTgaPrePpIni();
    }

    @Override
    public void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj) {
        setTgaPrePpIni(new AfDecimal(tgaPrePpIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrePpUlt() {
        throw new FieldNotMappedException("tgaPrePpUlt");
    }

    @Override
    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        throw new FieldNotMappedException("tgaPrePpUlt");
    }

    @Override
    public AfDecimal getTgaPrePpUltObj() {
        return getTgaPrePpUlt();
    }

    @Override
    public void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj) {
        setTgaPrePpUlt(new AfDecimal(tgaPrePpUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreRivto() {
        throw new FieldNotMappedException("tgaPreRivto");
    }

    @Override
    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        throw new FieldNotMappedException("tgaPreRivto");
    }

    @Override
    public AfDecimal getTgaPreRivtoObj() {
        return getTgaPreRivto();
    }

    @Override
    public void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj) {
        setTgaPreRivto(new AfDecimal(tgaPreRivtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreStab() {
        throw new FieldNotMappedException("tgaPreStab");
    }

    @Override
    public void setTgaPreStab(AfDecimal tgaPreStab) {
        throw new FieldNotMappedException("tgaPreStab");
    }

    @Override
    public AfDecimal getTgaPreStabObj() {
        return getTgaPreStab();
    }

    @Override
    public void setTgaPreStabObj(AfDecimal tgaPreStabObj) {
        setTgaPreStab(new AfDecimal(tgaPreStabObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreTariIni() {
        throw new FieldNotMappedException("tgaPreTariIni");
    }

    @Override
    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        throw new FieldNotMappedException("tgaPreTariIni");
    }

    @Override
    public AfDecimal getTgaPreTariIniObj() {
        return getTgaPreTariIni();
    }

    @Override
    public void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj) {
        setTgaPreTariIni(new AfDecimal(tgaPreTariIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreTariUlt() {
        throw new FieldNotMappedException("tgaPreTariUlt");
    }

    @Override
    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        throw new FieldNotMappedException("tgaPreTariUlt");
    }

    @Override
    public AfDecimal getTgaPreTariUltObj() {
        return getTgaPreTariUlt();
    }

    @Override
    public void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj) {
        setTgaPreTariUlt(new AfDecimal(tgaPreTariUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreUniRivto() {
        throw new FieldNotMappedException("tgaPreUniRivto");
    }

    @Override
    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        throw new FieldNotMappedException("tgaPreUniRivto");
    }

    @Override
    public AfDecimal getTgaPreUniRivtoObj() {
        return getTgaPreUniRivto();
    }

    @Override
    public void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj) {
        setTgaPreUniRivto(new AfDecimal(tgaPreUniRivtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProv1aaAcq() {
        throw new FieldNotMappedException("tgaProv1aaAcq");
    }

    @Override
    public void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq) {
        throw new FieldNotMappedException("tgaProv1aaAcq");
    }

    @Override
    public AfDecimal getTgaProv1aaAcqObj() {
        return getTgaProv1aaAcq();
    }

    @Override
    public void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj) {
        setTgaProv1aaAcq(new AfDecimal(tgaProv1aaAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProv2aaAcq() {
        throw new FieldNotMappedException("tgaProv2aaAcq");
    }

    @Override
    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        throw new FieldNotMappedException("tgaProv2aaAcq");
    }

    @Override
    public AfDecimal getTgaProv2aaAcqObj() {
        return getTgaProv2aaAcq();
    }

    @Override
    public void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj) {
        setTgaProv2aaAcq(new AfDecimal(tgaProv2aaAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProvInc() {
        throw new FieldNotMappedException("tgaProvInc");
    }

    @Override
    public void setTgaProvInc(AfDecimal tgaProvInc) {
        throw new FieldNotMappedException("tgaProvInc");
    }

    @Override
    public AfDecimal getTgaProvIncObj() {
        return getTgaProvInc();
    }

    @Override
    public void setTgaProvIncObj(AfDecimal tgaProvIncObj) {
        setTgaProvInc(new AfDecimal(tgaProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProvRicor() {
        throw new FieldNotMappedException("tgaProvRicor");
    }

    @Override
    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        throw new FieldNotMappedException("tgaProvRicor");
    }

    @Override
    public AfDecimal getTgaProvRicorObj() {
        return getTgaProvRicor();
    }

    @Override
    public void setTgaProvRicorObj(AfDecimal tgaProvRicorObj) {
        setTgaProvRicor(new AfDecimal(tgaProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzAggIni() {
        throw new FieldNotMappedException("tgaPrstzAggIni");
    }

    @Override
    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        throw new FieldNotMappedException("tgaPrstzAggIni");
    }

    @Override
    public AfDecimal getTgaPrstzAggIniObj() {
        return getTgaPrstzAggIni();
    }

    @Override
    public void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj) {
        setTgaPrstzAggIni(new AfDecimal(tgaPrstzAggIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzAggUlt() {
        throw new FieldNotMappedException("tgaPrstzAggUlt");
    }

    @Override
    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        throw new FieldNotMappedException("tgaPrstzAggUlt");
    }

    @Override
    public AfDecimal getTgaPrstzAggUltObj() {
        return getTgaPrstzAggUlt();
    }

    @Override
    public void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj) {
        setTgaPrstzAggUlt(new AfDecimal(tgaPrstzAggUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIni() {
        throw new FieldNotMappedException("tgaPrstzIni");
    }

    @Override
    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        throw new FieldNotMappedException("tgaPrstzIni");
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfis() {
        throw new FieldNotMappedException("tgaPrstzIniNewfis");
    }

    @Override
    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        throw new FieldNotMappedException("tgaPrstzIniNewfis");
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfisObj() {
        return getTgaPrstzIniNewfis();
    }

    @Override
    public void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj) {
        setTgaPrstzIniNewfis(new AfDecimal(tgaPrstzIniNewfisObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIniNforz() {
        throw new FieldNotMappedException("tgaPrstzIniNforz");
    }

    @Override
    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        throw new FieldNotMappedException("tgaPrstzIniNforz");
    }

    @Override
    public AfDecimal getTgaPrstzIniNforzObj() {
        return getTgaPrstzIniNforz();
    }

    @Override
    public void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj) {
        setTgaPrstzIniNforz(new AfDecimal(tgaPrstzIniNforzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIniObj() {
        return getTgaPrstzIni();
    }

    @Override
    public void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj) {
        setTgaPrstzIni(new AfDecimal(tgaPrstzIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIniStab() {
        throw new FieldNotMappedException("tgaPrstzIniStab");
    }

    @Override
    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        throw new FieldNotMappedException("tgaPrstzIniStab");
    }

    @Override
    public AfDecimal getTgaPrstzIniStabObj() {
        return getTgaPrstzIniStab();
    }

    @Override
    public void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj) {
        setTgaPrstzIniStab(new AfDecimal(tgaPrstzIniStabObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzRidIni() {
        throw new FieldNotMappedException("tgaPrstzRidIni");
    }

    @Override
    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        throw new FieldNotMappedException("tgaPrstzRidIni");
    }

    @Override
    public AfDecimal getTgaPrstzRidIniObj() {
        return getTgaPrstzRidIni();
    }

    @Override
    public void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj) {
        setTgaPrstzRidIni(new AfDecimal(tgaPrstzRidIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzUlt() {
        throw new FieldNotMappedException("tgaPrstzUlt");
    }

    @Override
    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        throw new FieldNotMappedException("tgaPrstzUlt");
    }

    @Override
    public AfDecimal getTgaPrstzUltObj() {
        return getTgaPrstzUlt();
    }

    @Override
    public void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj) {
        setTgaPrstzUlt(new AfDecimal(tgaPrstzUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRatLrd() {
        throw new FieldNotMappedException("tgaRatLrd");
    }

    @Override
    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        throw new FieldNotMappedException("tgaRatLrd");
    }

    @Override
    public AfDecimal getTgaRatLrdObj() {
        return getTgaRatLrd();
    }

    @Override
    public void setTgaRatLrdObj(AfDecimal tgaRatLrdObj) {
        setTgaRatLrd(new AfDecimal(tgaRatLrdObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRemunAss() {
        throw new FieldNotMappedException("tgaRemunAss");
    }

    @Override
    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        throw new FieldNotMappedException("tgaRemunAss");
    }

    @Override
    public AfDecimal getTgaRemunAssObj() {
        return getTgaRemunAss();
    }

    @Override
    public void setTgaRemunAssObj(AfDecimal tgaRemunAssObj) {
        setTgaRemunAss(new AfDecimal(tgaRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0() {
        throw new FieldNotMappedException("tgaRenIniTsTec0");
    }

    @Override
    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        throw new FieldNotMappedException("tgaRenIniTsTec0");
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0Obj() {
        return getTgaRenIniTsTec0();
    }

    @Override
    public void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj) {
        setTgaRenIniTsTec0(new AfDecimal(tgaRenIniTsTec0Obj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRendtoLrd() {
        throw new FieldNotMappedException("tgaRendtoLrd");
    }

    @Override
    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        throw new FieldNotMappedException("tgaRendtoLrd");
    }

    @Override
    public AfDecimal getTgaRendtoLrdObj() {
        return getTgaRendtoLrd();
    }

    @Override
    public void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj) {
        setTgaRendtoLrd(new AfDecimal(tgaRendtoLrdObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaRendtoRetr() {
        throw new FieldNotMappedException("tgaRendtoRetr");
    }

    @Override
    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        throw new FieldNotMappedException("tgaRendtoRetr");
    }

    @Override
    public AfDecimal getTgaRendtoRetrObj() {
        return getTgaRendtoRetr();
    }

    @Override
    public void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj) {
        setTgaRendtoRetr(new AfDecimal(tgaRendtoRetrObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaRisMat() {
        throw new FieldNotMappedException("tgaRisMat");
    }

    @Override
    public void setTgaRisMat(AfDecimal tgaRisMat) {
        throw new FieldNotMappedException("tgaRisMat");
    }

    @Override
    public AfDecimal getTgaRisMatObj() {
        return getTgaRisMat();
    }

    @Override
    public void setTgaRisMatObj(AfDecimal tgaRisMatObj) {
        setTgaRisMat(new AfDecimal(tgaRisMatObj, 15, 3));
    }

    @Override
    public char getTgaTpAdegAbb() {
        throw new FieldNotMappedException("tgaTpAdegAbb");
    }

    @Override
    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        throw new FieldNotMappedException("tgaTpAdegAbb");
    }

    @Override
    public Character getTgaTpAdegAbbObj() {
        return ((Character)getTgaTpAdegAbb());
    }

    @Override
    public void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj) {
        setTgaTpAdegAbb(((char)tgaTpAdegAbbObj));
    }

    @Override
    public String getTgaTpManfeeAppl() {
        throw new FieldNotMappedException("tgaTpManfeeAppl");
    }

    @Override
    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        throw new FieldNotMappedException("tgaTpManfeeAppl");
    }

    @Override
    public String getTgaTpManfeeApplObj() {
        return getTgaTpManfeeAppl();
    }

    @Override
    public void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj) {
        setTgaTpManfeeAppl(tgaTpManfeeApplObj);
    }

    @Override
    public String getTgaTpRgmFisc() {
        throw new FieldNotMappedException("tgaTpRgmFisc");
    }

    @Override
    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        throw new FieldNotMappedException("tgaTpRgmFisc");
    }

    @Override
    public String getTgaTpRival() {
        throw new FieldNotMappedException("tgaTpRival");
    }

    @Override
    public void setTgaTpRival(String tgaTpRival) {
        throw new FieldNotMappedException("tgaTpRival");
    }

    @Override
    public String getTgaTpRivalObj() {
        return getTgaTpRival();
    }

    @Override
    public void setTgaTpRivalObj(String tgaTpRivalObj) {
        setTgaTpRival(tgaTpRivalObj);
    }

    @Override
    public String getTgaTpTrch() {
        throw new FieldNotMappedException("tgaTpTrch");
    }

    @Override
    public void setTgaTpTrch(String tgaTpTrch) {
        throw new FieldNotMappedException("tgaTpTrch");
    }

    @Override
    public AfDecimal getTgaTsRivalFis() {
        throw new FieldNotMappedException("tgaTsRivalFis");
    }

    @Override
    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        throw new FieldNotMappedException("tgaTsRivalFis");
    }

    @Override
    public AfDecimal getTgaTsRivalFisObj() {
        return getTgaTsRivalFis();
    }

    @Override
    public void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj) {
        setTgaTsRivalFis(new AfDecimal(tgaTsRivalFisObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaTsRivalIndiciz() {
        throw new FieldNotMappedException("tgaTsRivalIndiciz");
    }

    @Override
    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        throw new FieldNotMappedException("tgaTsRivalIndiciz");
    }

    @Override
    public AfDecimal getTgaTsRivalIndicizObj() {
        return getTgaTsRivalIndiciz();
    }

    @Override
    public void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj) {
        setTgaTsRivalIndiciz(new AfDecimal(tgaTsRivalIndicizObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaTsRivalNet() {
        throw new FieldNotMappedException("tgaTsRivalNet");
    }

    @Override
    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        throw new FieldNotMappedException("tgaTsRivalNet");
    }

    @Override
    public AfDecimal getTgaTsRivalNetObj() {
        return getTgaTsRivalNet();
    }

    @Override
    public void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj) {
        setTgaTsRivalNet(new AfDecimal(tgaTsRivalNetObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaVisEnd2000() {
        throw new FieldNotMappedException("tgaVisEnd2000");
    }

    @Override
    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        throw new FieldNotMappedException("tgaVisEnd2000");
    }

    @Override
    public AfDecimal getTgaVisEnd2000Nforz() {
        throw new FieldNotMappedException("tgaVisEnd2000Nforz");
    }

    @Override
    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        throw new FieldNotMappedException("tgaVisEnd2000Nforz");
    }

    @Override
    public AfDecimal getTgaVisEnd2000NforzObj() {
        return getTgaVisEnd2000Nforz();
    }

    @Override
    public void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj) {
        setTgaVisEnd2000Nforz(new AfDecimal(tgaVisEnd2000NforzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaVisEnd2000Obj() {
        return getTgaVisEnd2000();
    }

    @Override
    public void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj) {
        setTgaVisEnd2000(new AfDecimal(tgaVisEnd2000Obj, 15, 3));
    }

    public TrchDiGarDb getTrchDiGarDb() {
        return trchDiGarDb;
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
