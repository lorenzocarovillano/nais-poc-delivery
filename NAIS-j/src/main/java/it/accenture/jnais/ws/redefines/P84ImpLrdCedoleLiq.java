package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P84-IMP-LRD-CEDOLE-LIQ<br>
 * Variable: P84-IMP-LRD-CEDOLE-LIQ from program IDBSP840<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P84ImpLrdCedoleLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P84ImpLrdCedoleLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P84_IMP_LRD_CEDOLE_LIQ;
    }

    public void setP84ImpLrdCedoleLiq(AfDecimal p84ImpLrdCedoleLiq) {
        writeDecimalAsPacked(Pos.P84_IMP_LRD_CEDOLE_LIQ, p84ImpLrdCedoleLiq.copy());
    }

    public void setP84ImpLrdCedoleLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P84_IMP_LRD_CEDOLE_LIQ, Pos.P84_IMP_LRD_CEDOLE_LIQ);
    }

    /**Original name: P84-IMP-LRD-CEDOLE-LIQ<br>*/
    public AfDecimal getP84ImpLrdCedoleLiq() {
        return readPackedAsDecimal(Pos.P84_IMP_LRD_CEDOLE_LIQ, Len.Int.P84_IMP_LRD_CEDOLE_LIQ, Len.Fract.P84_IMP_LRD_CEDOLE_LIQ);
    }

    public byte[] getP84ImpLrdCedoleLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P84_IMP_LRD_CEDOLE_LIQ, Pos.P84_IMP_LRD_CEDOLE_LIQ);
        return buffer;
    }

    public void setP84ImpLrdCedoleLiqNull(String p84ImpLrdCedoleLiqNull) {
        writeString(Pos.P84_IMP_LRD_CEDOLE_LIQ_NULL, p84ImpLrdCedoleLiqNull, Len.P84_IMP_LRD_CEDOLE_LIQ_NULL);
    }

    /**Original name: P84-IMP-LRD-CEDOLE-LIQ-NULL<br>*/
    public String getP84ImpLrdCedoleLiqNull() {
        return readString(Pos.P84_IMP_LRD_CEDOLE_LIQ_NULL, Len.P84_IMP_LRD_CEDOLE_LIQ_NULL);
    }

    public String getP84ImpLrdCedoleLiqNullFormatted() {
        return Functions.padBlanks(getP84ImpLrdCedoleLiqNull(), Len.P84_IMP_LRD_CEDOLE_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P84_IMP_LRD_CEDOLE_LIQ = 1;
        public static final int P84_IMP_LRD_CEDOLE_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P84_IMP_LRD_CEDOLE_LIQ = 8;
        public static final int P84_IMP_LRD_CEDOLE_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P84_IMP_LRD_CEDOLE_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P84_IMP_LRD_CEDOLE_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
