package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-ID-MOVI-CHIU<br>
 * Variable: OCO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_ID_MOVI_CHIU;
    }

    public void setOcoIdMoviChiu(int ocoIdMoviChiu) {
        writeIntAsPacked(Pos.OCO_ID_MOVI_CHIU, ocoIdMoviChiu, Len.Int.OCO_ID_MOVI_CHIU);
    }

    public void setOcoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_ID_MOVI_CHIU, Pos.OCO_ID_MOVI_CHIU);
    }

    /**Original name: OCO-ID-MOVI-CHIU<br>*/
    public int getOcoIdMoviChiu() {
        return readPackedAsInt(Pos.OCO_ID_MOVI_CHIU, Len.Int.OCO_ID_MOVI_CHIU);
    }

    public byte[] getOcoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_ID_MOVI_CHIU, Pos.OCO_ID_MOVI_CHIU);
        return buffer;
    }

    public void setOcoIdMoviChiuNull(String ocoIdMoviChiuNull) {
        writeString(Pos.OCO_ID_MOVI_CHIU_NULL, ocoIdMoviChiuNull, Len.OCO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: OCO-ID-MOVI-CHIU-NULL<br>*/
    public String getOcoIdMoviChiuNull() {
        return readString(Pos.OCO_ID_MOVI_CHIU_NULL, Len.OCO_ID_MOVI_CHIU_NULL);
    }

    public String getOcoIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getOcoIdMoviChiuNull(), Len.OCO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_ID_MOVI_CHIU = 1;
        public static final int OCO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_ID_MOVI_CHIU = 5;
        public static final int OCO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
