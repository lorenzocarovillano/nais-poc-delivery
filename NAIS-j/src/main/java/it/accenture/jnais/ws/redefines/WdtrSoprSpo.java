package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SOPR-SPO<br>
 * Variable: WDTR-SOPR-SPO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SOPR_SPO;
    }

    public void setWdtrSoprSpo(AfDecimal wdtrSoprSpo) {
        writeDecimalAsPacked(Pos.WDTR_SOPR_SPO, wdtrSoprSpo.copy());
    }

    /**Original name: WDTR-SOPR-SPO<br>*/
    public AfDecimal getWdtrSoprSpo() {
        return readPackedAsDecimal(Pos.WDTR_SOPR_SPO, Len.Int.WDTR_SOPR_SPO, Len.Fract.WDTR_SOPR_SPO);
    }

    public void setWdtrSoprSpoNull(String wdtrSoprSpoNull) {
        writeString(Pos.WDTR_SOPR_SPO_NULL, wdtrSoprSpoNull, Len.WDTR_SOPR_SPO_NULL);
    }

    /**Original name: WDTR-SOPR-SPO-NULL<br>*/
    public String getWdtrSoprSpoNull() {
        return readString(Pos.WDTR_SOPR_SPO_NULL, Len.WDTR_SOPR_SPO_NULL);
    }

    public String getWdtrSoprSpoNullFormatted() {
        return Functions.padBlanks(getWdtrSoprSpoNull(), Len.WDTR_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_SPO = 1;
        public static final int WDTR_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SOPR_SPO = 8;
        public static final int WDTR_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
