package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-TS-RIVAL-INDICIZ<br>
 * Variable: WTGA-TS-RIVAL-INDICIZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaTsRivalIndiciz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaTsRivalIndiciz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_TS_RIVAL_INDICIZ;
    }

    public void setWtgaTsRivalIndiciz(AfDecimal wtgaTsRivalIndiciz) {
        writeDecimalAsPacked(Pos.WTGA_TS_RIVAL_INDICIZ, wtgaTsRivalIndiciz.copy());
    }

    public void setWtgaTsRivalIndicizFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_TS_RIVAL_INDICIZ, Pos.WTGA_TS_RIVAL_INDICIZ);
    }

    /**Original name: WTGA-TS-RIVAL-INDICIZ<br>*/
    public AfDecimal getWtgaTsRivalIndiciz() {
        return readPackedAsDecimal(Pos.WTGA_TS_RIVAL_INDICIZ, Len.Int.WTGA_TS_RIVAL_INDICIZ, Len.Fract.WTGA_TS_RIVAL_INDICIZ);
    }

    public byte[] getWtgaTsRivalIndicizAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_TS_RIVAL_INDICIZ, Pos.WTGA_TS_RIVAL_INDICIZ);
        return buffer;
    }

    public void initWtgaTsRivalIndicizSpaces() {
        fill(Pos.WTGA_TS_RIVAL_INDICIZ, Len.WTGA_TS_RIVAL_INDICIZ, Types.SPACE_CHAR);
    }

    public void setWtgaTsRivalIndicizNull(String wtgaTsRivalIndicizNull) {
        writeString(Pos.WTGA_TS_RIVAL_INDICIZ_NULL, wtgaTsRivalIndicizNull, Len.WTGA_TS_RIVAL_INDICIZ_NULL);
    }

    /**Original name: WTGA-TS-RIVAL-INDICIZ-NULL<br>*/
    public String getWtgaTsRivalIndicizNull() {
        return readString(Pos.WTGA_TS_RIVAL_INDICIZ_NULL, Len.WTGA_TS_RIVAL_INDICIZ_NULL);
    }

    public String getWtgaTsRivalIndicizNullFormatted() {
        return Functions.padBlanks(getWtgaTsRivalIndicizNull(), Len.WTGA_TS_RIVAL_INDICIZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_TS_RIVAL_INDICIZ = 1;
        public static final int WTGA_TS_RIVAL_INDICIZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_TS_RIVAL_INDICIZ = 8;
        public static final int WTGA_TS_RIVAL_INDICIZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_TS_RIVAL_INDICIZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_TS_RIVAL_INDICIZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
