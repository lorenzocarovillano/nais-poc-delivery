package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PROV-2AA-ACQ<br>
 * Variable: TGA-PROV-2AA-ACQ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaProv2aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaProv2aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PROV2AA_ACQ;
    }

    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        writeDecimalAsPacked(Pos.TGA_PROV2AA_ACQ, tgaProv2aaAcq.copy());
    }

    public void setTgaProv2aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PROV2AA_ACQ, Pos.TGA_PROV2AA_ACQ);
    }

    /**Original name: TGA-PROV-2AA-ACQ<br>*/
    public AfDecimal getTgaProv2aaAcq() {
        return readPackedAsDecimal(Pos.TGA_PROV2AA_ACQ, Len.Int.TGA_PROV2AA_ACQ, Len.Fract.TGA_PROV2AA_ACQ);
    }

    public byte[] getTgaProv2aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PROV2AA_ACQ, Pos.TGA_PROV2AA_ACQ);
        return buffer;
    }

    public void setTgaProv2aaAcqNull(String tgaProv2aaAcqNull) {
        writeString(Pos.TGA_PROV2AA_ACQ_NULL, tgaProv2aaAcqNull, Len.TGA_PROV2AA_ACQ_NULL);
    }

    /**Original name: TGA-PROV-2AA-ACQ-NULL<br>*/
    public String getTgaProv2aaAcqNull() {
        return readString(Pos.TGA_PROV2AA_ACQ_NULL, Len.TGA_PROV2AA_ACQ_NULL);
    }

    public String getTgaProv2aaAcqNullFormatted() {
        return Functions.padBlanks(getTgaProv2aaAcqNull(), Len.TGA_PROV2AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PROV2AA_ACQ = 1;
        public static final int TGA_PROV2AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PROV2AA_ACQ = 8;
        public static final int TGA_PROV2AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PROV2AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PROV2AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
