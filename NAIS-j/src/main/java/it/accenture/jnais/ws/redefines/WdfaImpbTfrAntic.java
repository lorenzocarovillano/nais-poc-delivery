package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-TFR-ANTIC<br>
 * Variable: WDFA-IMPB-TFR-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbTfrAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbTfrAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_TFR_ANTIC;
    }

    public void setWdfaImpbTfrAntic(AfDecimal wdfaImpbTfrAntic) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_TFR_ANTIC, wdfaImpbTfrAntic.copy());
    }

    public void setWdfaImpbTfrAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_TFR_ANTIC, Pos.WDFA_IMPB_TFR_ANTIC);
    }

    /**Original name: WDFA-IMPB-TFR-ANTIC<br>*/
    public AfDecimal getWdfaImpbTfrAntic() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_TFR_ANTIC, Len.Int.WDFA_IMPB_TFR_ANTIC, Len.Fract.WDFA_IMPB_TFR_ANTIC);
    }

    public byte[] getWdfaImpbTfrAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_TFR_ANTIC, Pos.WDFA_IMPB_TFR_ANTIC);
        return buffer;
    }

    public void setWdfaImpbTfrAnticNull(String wdfaImpbTfrAnticNull) {
        writeString(Pos.WDFA_IMPB_TFR_ANTIC_NULL, wdfaImpbTfrAnticNull, Len.WDFA_IMPB_TFR_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPB-TFR-ANTIC-NULL<br>*/
    public String getWdfaImpbTfrAnticNull() {
        return readString(Pos.WDFA_IMPB_TFR_ANTIC_NULL, Len.WDFA_IMPB_TFR_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_TFR_ANTIC = 1;
        public static final int WDFA_IMPB_TFR_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_TFR_ANTIC = 8;
        public static final int WDFA_IMPB_TFR_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_TFR_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_TFR_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
