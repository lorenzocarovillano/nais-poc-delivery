package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-EFF-STAB<br>
 * Variable: WB03-DT-EFF-STAB from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtEffStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtEffStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_EFF_STAB;
    }

    public void setWb03DtEffStab(int wb03DtEffStab) {
        writeIntAsPacked(Pos.WB03_DT_EFF_STAB, wb03DtEffStab, Len.Int.WB03_DT_EFF_STAB);
    }

    public void setWb03DtEffStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_EFF_STAB, Pos.WB03_DT_EFF_STAB);
    }

    /**Original name: WB03-DT-EFF-STAB<br>*/
    public int getWb03DtEffStab() {
        return readPackedAsInt(Pos.WB03_DT_EFF_STAB, Len.Int.WB03_DT_EFF_STAB);
    }

    public byte[] getWb03DtEffStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_EFF_STAB, Pos.WB03_DT_EFF_STAB);
        return buffer;
    }

    public void setWb03DtEffStabNull(String wb03DtEffStabNull) {
        writeString(Pos.WB03_DT_EFF_STAB_NULL, wb03DtEffStabNull, Len.WB03_DT_EFF_STAB_NULL);
    }

    /**Original name: WB03-DT-EFF-STAB-NULL<br>*/
    public String getWb03DtEffStabNull() {
        return readString(Pos.WB03_DT_EFF_STAB_NULL, Len.WB03_DT_EFF_STAB_NULL);
    }

    public String getWb03DtEffStabNullFormatted() {
        return Functions.padBlanks(getWb03DtEffStabNull(), Len.WB03_DT_EFF_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_EFF_STAB = 1;
        public static final int WB03_DT_EFF_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_EFF_STAB = 5;
        public static final int WB03_DT_EFF_STAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_EFF_STAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
