package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-ID-2O-ASSTO<br>
 * Variable: WGRZ-ID-2O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzId2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzId2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ID2O_ASSTO;
    }

    public void setWgrzId2oAssto(int wgrzId2oAssto) {
        writeIntAsPacked(Pos.WGRZ_ID2O_ASSTO, wgrzId2oAssto, Len.Int.WGRZ_ID2O_ASSTO);
    }

    public void setWgrzId2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ID2O_ASSTO, Pos.WGRZ_ID2O_ASSTO);
    }

    /**Original name: WGRZ-ID-2O-ASSTO<br>*/
    public int getWgrzId2oAssto() {
        return readPackedAsInt(Pos.WGRZ_ID2O_ASSTO, Len.Int.WGRZ_ID2O_ASSTO);
    }

    public byte[] getWgrzId2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ID2O_ASSTO, Pos.WGRZ_ID2O_ASSTO);
        return buffer;
    }

    public void initWgrzId2oAsstoSpaces() {
        fill(Pos.WGRZ_ID2O_ASSTO, Len.WGRZ_ID2O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzId2oAsstoNull(String wgrzId2oAsstoNull) {
        writeString(Pos.WGRZ_ID2O_ASSTO_NULL, wgrzId2oAsstoNull, Len.WGRZ_ID2O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ID-2O-ASSTO-NULL<br>*/
    public String getWgrzId2oAsstoNull() {
        return readString(Pos.WGRZ_ID2O_ASSTO_NULL, Len.WGRZ_ID2O_ASSTO_NULL);
    }

    public String getWgrzId2oAsstoNullFormatted() {
        return Functions.padBlanks(getWgrzId2oAsstoNull(), Len.WGRZ_ID2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ID2O_ASSTO = 1;
        public static final int WGRZ_ID2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ID2O_ASSTO = 5;
        public static final int WGRZ_ID2O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ID2O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
