package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv0015;
import it.accenture.jnais.copy.Ldbv3101;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.copy.WkTgaMax;
import it.accenture.jnais.ws.enums.WkFineFetch;
import it.accenture.jnais.ws.enums.WkGrzTrovata;
import it.accenture.jnais.ws.enums.WkTitTrovato;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpPerPremio;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0670<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0670Data {

    //==== PROPERTIES ====
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: TIT-CONT
    private TitCont titCont = new TitCont();
    //Original name: LDBV3101
    private Ldbv3101 ldbv3101 = new Ldbv3101();
    //Original name: DETT-TIT-CONT
    private DettTitCont dettTitCont = new DettTitCont();
    /**Original name: WS-TP-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *   AREE TIPOLOGICHE OGGETTO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    //Original name: WK-TGA-MAX
    private WkTgaMax wkTgaMax = new WkTgaMax();
    //Original name: IX-INDICI
    private WsIndiciLccs0033 ixIndici = new WsIndiciLccs0033();
    /**Original name: WK-TIT-TROVATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     AREE DI COMODO
	 * ----------------------------------------------------------------*
	 *  --> Ricerca occorrenza Titolo Contabile</pre>*/
    private WkTitTrovato wkTitTrovato = new WkTitTrovato();
    /**Original name: WK-GRZ-TROVATA<br>
	 * <pre> --> Ricerca occorrenza Garanzia</pre>*/
    private WkGrzTrovata wkGrzTrovata = new WkGrzTrovata();
    /**Original name: WK-FINE-FETCH<br>
	 * <pre> -->  Flag fine fetch</pre>*/
    private WkFineFetch wkFineFetch = new WkFineFetch();
    //Original name: WK-GESTIONE-MSG-ERR
    private WkGestioneMsgErr wkGestioneMsgErr = new WkGestioneMsgErr();
    /**Original name: WK-PGM<br>
	 * <pre> --> Nome del programma Businesss Service</pre>*/
    private String wkPgm = "LOAS0670";
    /**Original name: WK-CTR-TIT<br>
	 * <pre> --> Area di appoggio per gestione Management Fee</pre>*/
    private short wkCtrTit = DefaultValues.SHORT_VAL;
    /**Original name: WS-TP-PER-PREMIO<br>
	 * <pre>--> Tipo periodo premio (Garanzia)</pre>*/
    private WsTpPerPremio wsTpPerPremio = new WsTpPerPremio();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IDSV0015
    private Idsv0015 idsv0015 = new Idsv0015();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCtrTit(short wkCtrTit) {
        this.wkCtrTit = wkCtrTit;
    }

    public short getWkCtrTit() {
        return this.wkCtrTit;
    }

    public DettTitCont getDettTitCont() {
        return dettTitCont;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv0015 getIdsv0015() {
        return idsv0015;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public WsIndiciLccs0033 getIxIndici() {
        return ixIndici;
    }

    public Ldbv3101 getLdbv3101() {
        return ldbv3101;
    }

    public TitCont getTitCont() {
        return titCont;
    }

    public WkFineFetch getWkFineFetch() {
        return wkFineFetch;
    }

    public WkGestioneMsgErr getWkGestioneMsgErr() {
        return wkGestioneMsgErr;
    }

    public WkGrzTrovata getWkGrzTrovata() {
        return wkGrzTrovata;
    }

    public WkTgaMax getWkTgaMax() {
        return wkTgaMax;
    }

    public WkTitTrovato getWkTitTrovato() {
        return wkTitTrovato;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpPerPremio getWsTpPerPremio() {
        return wsTpPerPremio;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APPO_DT_RICOR_SUCC = 8;
        public static final int WK_APPO_DT_NUM = 8;
        public static final int WK_CURRENT_DATE = 8;
        public static final int WK_CTR_TIT = 1;
        public static final int WK_COM_AA = 4;
        public static final int WK_COM_MM = 2;
        public static final int IN_RCODE = 2;
        public static final int WK_DATA_CALCOLATA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
