package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNBT-INPSTFM-EFFLQ<br>
 * Variable: DFL-CNBT-INPSTFM-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCnbtInpstfmEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCnbtInpstfmEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNBT_INPSTFM_EFFLQ;
    }

    public void setDflCnbtInpstfmEfflq(AfDecimal dflCnbtInpstfmEfflq) {
        writeDecimalAsPacked(Pos.DFL_CNBT_INPSTFM_EFFLQ, dflCnbtInpstfmEfflq.copy());
    }

    public void setDflCnbtInpstfmEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNBT_INPSTFM_EFFLQ, Pos.DFL_CNBT_INPSTFM_EFFLQ);
    }

    /**Original name: DFL-CNBT-INPSTFM-EFFLQ<br>*/
    public AfDecimal getDflCnbtInpstfmEfflq() {
        return readPackedAsDecimal(Pos.DFL_CNBT_INPSTFM_EFFLQ, Len.Int.DFL_CNBT_INPSTFM_EFFLQ, Len.Fract.DFL_CNBT_INPSTFM_EFFLQ);
    }

    public byte[] getDflCnbtInpstfmEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNBT_INPSTFM_EFFLQ, Pos.DFL_CNBT_INPSTFM_EFFLQ);
        return buffer;
    }

    public void setDflCnbtInpstfmEfflqNull(String dflCnbtInpstfmEfflqNull) {
        writeString(Pos.DFL_CNBT_INPSTFM_EFFLQ_NULL, dflCnbtInpstfmEfflqNull, Len.DFL_CNBT_INPSTFM_EFFLQ_NULL);
    }

    /**Original name: DFL-CNBT-INPSTFM-EFFLQ-NULL<br>*/
    public String getDflCnbtInpstfmEfflqNull() {
        return readString(Pos.DFL_CNBT_INPSTFM_EFFLQ_NULL, Len.DFL_CNBT_INPSTFM_EFFLQ_NULL);
    }

    public String getDflCnbtInpstfmEfflqNullFormatted() {
        return Functions.padBlanks(getDflCnbtInpstfmEfflqNull(), Len.DFL_CNBT_INPSTFM_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNBT_INPSTFM_EFFLQ = 1;
        public static final int DFL_CNBT_INPSTFM_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNBT_INPSTFM_EFFLQ = 8;
        public static final int DFL_CNBT_INPSTFM_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNBT_INPSTFM_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNBT_INPSTFM_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
