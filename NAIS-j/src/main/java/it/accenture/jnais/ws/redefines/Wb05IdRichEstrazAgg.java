package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB05-ID-RICH-ESTRAZ-AGG<br>
 * Variable: WB05-ID-RICH-ESTRAZ-AGG from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb05IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb05IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB05_ID_RICH_ESTRAZ_AGG;
    }

    public void setWb05IdRichEstrazAgg(int wb05IdRichEstrazAgg) {
        writeIntAsPacked(Pos.WB05_ID_RICH_ESTRAZ_AGG, wb05IdRichEstrazAgg, Len.Int.WB05_ID_RICH_ESTRAZ_AGG);
    }

    public void setWb05IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB05_ID_RICH_ESTRAZ_AGG, Pos.WB05_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: WB05-ID-RICH-ESTRAZ-AGG<br>*/
    public int getWb05IdRichEstrazAgg() {
        return readPackedAsInt(Pos.WB05_ID_RICH_ESTRAZ_AGG, Len.Int.WB05_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getWb05IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB05_ID_RICH_ESTRAZ_AGG, Pos.WB05_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setWb05IdRichEstrazAggNull(String wb05IdRichEstrazAggNull) {
        writeString(Pos.WB05_ID_RICH_ESTRAZ_AGG_NULL, wb05IdRichEstrazAggNull, Len.WB05_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: WB05-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getWb05IdRichEstrazAggNull() {
        return readString(Pos.WB05_ID_RICH_ESTRAZ_AGG_NULL, Len.WB05_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getWb05IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getWb05IdRichEstrazAggNull(), Len.WB05_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB05_ID_RICH_ESTRAZ_AGG = 1;
        public static final int WB05_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB05_ID_RICH_ESTRAZ_AGG = 5;
        public static final int WB05_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB05_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
