package it.accenture.jnais.ws;

/**Original name: WK-INDICI<br>
 * Variable: WK-INDICI from program LCCS0320<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkIndici {

    //==== PROPERTIES ====
    //Original name: IX-TAB-PMO
    private short pmo = ((short)0);
    //Original name: IX-TAB-GRZ
    private short grz = ((short)0);

    //==== METHODS ====
    public void setPmo(short pmo) {
        this.pmo = pmo;
    }

    public short getPmo() {
        return this.pmo;
    }

    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
