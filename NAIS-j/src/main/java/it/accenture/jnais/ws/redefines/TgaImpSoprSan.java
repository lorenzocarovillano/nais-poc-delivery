package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-SOPR-SAN<br>
 * Variable: TGA-IMP-SOPR-SAN from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_SOPR_SAN;
    }

    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        writeDecimalAsPacked(Pos.TGA_IMP_SOPR_SAN, tgaImpSoprSan.copy());
    }

    public void setTgaImpSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_SOPR_SAN, Pos.TGA_IMP_SOPR_SAN);
    }

    /**Original name: TGA-IMP-SOPR-SAN<br>*/
    public AfDecimal getTgaImpSoprSan() {
        return readPackedAsDecimal(Pos.TGA_IMP_SOPR_SAN, Len.Int.TGA_IMP_SOPR_SAN, Len.Fract.TGA_IMP_SOPR_SAN);
    }

    public byte[] getTgaImpSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_SOPR_SAN, Pos.TGA_IMP_SOPR_SAN);
        return buffer;
    }

    public void setTgaImpSoprSanNull(String tgaImpSoprSanNull) {
        writeString(Pos.TGA_IMP_SOPR_SAN_NULL, tgaImpSoprSanNull, Len.TGA_IMP_SOPR_SAN_NULL);
    }

    /**Original name: TGA-IMP-SOPR-SAN-NULL<br>*/
    public String getTgaImpSoprSanNull() {
        return readString(Pos.TGA_IMP_SOPR_SAN_NULL, Len.TGA_IMP_SOPR_SAN_NULL);
    }

    public String getTgaImpSoprSanNullFormatted() {
        return Functions.padBlanks(getTgaImpSoprSanNull(), Len.TGA_IMP_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_SAN = 1;
        public static final int TGA_IMP_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_SOPR_SAN = 8;
        public static final int TGA_IMP_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
