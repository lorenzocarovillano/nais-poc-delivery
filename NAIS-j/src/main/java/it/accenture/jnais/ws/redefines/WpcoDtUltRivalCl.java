package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RIVAL-CL<br>
 * Variable: WPCO-DT-ULT-RIVAL-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRivalCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRivalCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RIVAL_CL;
    }

    public void setWpcoDtUltRivalCl(int wpcoDtUltRivalCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RIVAL_CL, wpcoDtUltRivalCl, Len.Int.WPCO_DT_ULT_RIVAL_CL);
    }

    public void setDpcoDtUltRivalClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RIVAL_CL, Pos.WPCO_DT_ULT_RIVAL_CL);
    }

    /**Original name: WPCO-DT-ULT-RIVAL-CL<br>*/
    public int getWpcoDtUltRivalCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RIVAL_CL, Len.Int.WPCO_DT_ULT_RIVAL_CL);
    }

    public byte[] getWpcoDtUltRivalClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RIVAL_CL, Pos.WPCO_DT_ULT_RIVAL_CL);
        return buffer;
    }

    public void setWpcoDtUltRivalClNull(String wpcoDtUltRivalClNull) {
        writeString(Pos.WPCO_DT_ULT_RIVAL_CL_NULL, wpcoDtUltRivalClNull, Len.WPCO_DT_ULT_RIVAL_CL_NULL);
    }

    /**Original name: WPCO-DT-ULT-RIVAL-CL-NULL<br>*/
    public String getWpcoDtUltRivalClNull() {
        return readString(Pos.WPCO_DT_ULT_RIVAL_CL_NULL, Len.WPCO_DT_ULT_RIVAL_CL_NULL);
    }

    public String getWpcoDtUltRivalClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRivalClNull(), Len.WPCO_DT_ULT_RIVAL_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RIVAL_CL = 1;
        public static final int WPCO_DT_ULT_RIVAL_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RIVAL_CL = 5;
        public static final int WPCO_DT_ULT_RIVAL_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RIVAL_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
