package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-VAL-TS<br>
 * Variable: WPAG-VAL-TS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagValTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagValTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_VAL_TS;
    }

    public void setWpagValTs(AfDecimal wpagValTs) {
        writeDecimalAsPacked(Pos.WPAG_VAL_TS, wpagValTs.copy());
    }

    public void setWpagValTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_VAL_TS, Pos.WPAG_VAL_TS);
    }

    /**Original name: WPAG-VAL-TS<br>*/
    public AfDecimal getWpagValTs() {
        return readPackedAsDecimal(Pos.WPAG_VAL_TS, Len.Int.WPAG_VAL_TS, Len.Fract.WPAG_VAL_TS);
    }

    public byte[] getWpagValTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_VAL_TS, Pos.WPAG_VAL_TS);
        return buffer;
    }

    public void initWpagValTsSpaces() {
        fill(Pos.WPAG_VAL_TS, Len.WPAG_VAL_TS, Types.SPACE_CHAR);
    }

    public void setWpagValTsNull(String wpagValTsNull) {
        writeString(Pos.WPAG_VAL_TS_NULL, wpagValTsNull, Len.WPAG_VAL_TS_NULL);
    }

    /**Original name: WPAG-VAL-TS-NULL<br>*/
    public String getWpagValTsNull() {
        return readString(Pos.WPAG_VAL_TS_NULL, Len.WPAG_VAL_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_TS = 1;
        public static final int WPAG_VAL_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_TS = 8;
        public static final int WPAG_VAL_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_VAL_TS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_VAL_TS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
