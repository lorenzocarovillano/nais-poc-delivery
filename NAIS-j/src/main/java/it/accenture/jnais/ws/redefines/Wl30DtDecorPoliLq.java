package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL30-DT-DECOR-POLI-LQ<br>
 * Variable: WL30-DT-DECOR-POLI-LQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl30DtDecorPoliLq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl30DtDecorPoliLq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL30_DT_DECOR_POLI_LQ;
    }

    public void setWl30DtDecorPoliLq(int wl30DtDecorPoliLq) {
        writeIntAsPacked(Pos.WL30_DT_DECOR_POLI_LQ, wl30DtDecorPoliLq, Len.Int.WL30_DT_DECOR_POLI_LQ);
    }

    public void setWl30DtDecorPoliLqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL30_DT_DECOR_POLI_LQ, Pos.WL30_DT_DECOR_POLI_LQ);
    }

    /**Original name: WL30-DT-DECOR-POLI-LQ<br>*/
    public int getWl30DtDecorPoliLq() {
        return readPackedAsInt(Pos.WL30_DT_DECOR_POLI_LQ, Len.Int.WL30_DT_DECOR_POLI_LQ);
    }

    public byte[] getWl30DtDecorPoliLqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL30_DT_DECOR_POLI_LQ, Pos.WL30_DT_DECOR_POLI_LQ);
        return buffer;
    }

    public void initWl30DtDecorPoliLqSpaces() {
        fill(Pos.WL30_DT_DECOR_POLI_LQ, Len.WL30_DT_DECOR_POLI_LQ, Types.SPACE_CHAR);
    }

    public void setWl30DtDecorPoliLqNull(String wl30DtDecorPoliLqNull) {
        writeString(Pos.WL30_DT_DECOR_POLI_LQ_NULL, wl30DtDecorPoliLqNull, Len.WL30_DT_DECOR_POLI_LQ_NULL);
    }

    /**Original name: WL30-DT-DECOR-POLI-LQ-NULL<br>*/
    public String getWl30DtDecorPoliLqNull() {
        return readString(Pos.WL30_DT_DECOR_POLI_LQ_NULL, Len.WL30_DT_DECOR_POLI_LQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL30_DT_DECOR_POLI_LQ = 1;
        public static final int WL30_DT_DECOR_POLI_LQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL30_DT_DECOR_POLI_LQ = 5;
        public static final int WL30_DT_DECOR_POLI_LQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL30_DT_DECOR_POLI_LQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
