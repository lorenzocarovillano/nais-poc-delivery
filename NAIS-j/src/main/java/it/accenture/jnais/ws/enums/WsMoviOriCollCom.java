package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-MOVI-ORI-COLL-COM<br>
 * Variable: WS-MOVI-ORI-COLL-COM from program LVVS2800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMoviOriCollCom {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_MOVI_ORI_COLL_COM);
    public static final String COMUN_RISPAR_ADE = "05003";
    public static final String RISTO_ADESIO = "03010";
    public static final String SINIS_ADESIO = "03018";
    public static final String VARIA_OPZION = "06009";

    //==== METHODS ====
    public void setWsMoviOriCollComFormatted(String wsMoviOriCollCom) {
        this.value = Trunc.toUnsignedNumeric(wsMoviOriCollCom, Len.WS_MOVI_ORI_COLL_COM);
    }

    public int getWsMoviOriCollCom() {
        return NumericDisplay.asInt(this.value);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVI_ORI_COLL_COM = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
