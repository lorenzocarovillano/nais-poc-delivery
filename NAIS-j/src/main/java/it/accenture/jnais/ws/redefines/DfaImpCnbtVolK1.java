package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-VOL-K1<br>
 * Variable: DFA-IMP-CNBT-VOL-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtVolK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtVolK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_VOL_K1;
    }

    public void setDfaImpCnbtVolK1(AfDecimal dfaImpCnbtVolK1) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_VOL_K1, dfaImpCnbtVolK1.copy());
    }

    public void setDfaImpCnbtVolK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_VOL_K1, Pos.DFA_IMP_CNBT_VOL_K1);
    }

    /**Original name: DFA-IMP-CNBT-VOL-K1<br>*/
    public AfDecimal getDfaImpCnbtVolK1() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_VOL_K1, Len.Int.DFA_IMP_CNBT_VOL_K1, Len.Fract.DFA_IMP_CNBT_VOL_K1);
    }

    public byte[] getDfaImpCnbtVolK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_VOL_K1, Pos.DFA_IMP_CNBT_VOL_K1);
        return buffer;
    }

    public void setDfaImpCnbtVolK1Null(String dfaImpCnbtVolK1Null) {
        writeString(Pos.DFA_IMP_CNBT_VOL_K1_NULL, dfaImpCnbtVolK1Null, Len.DFA_IMP_CNBT_VOL_K1_NULL);
    }

    /**Original name: DFA-IMP-CNBT-VOL-K1-NULL<br>*/
    public String getDfaImpCnbtVolK1Null() {
        return readString(Pos.DFA_IMP_CNBT_VOL_K1_NULL, Len.DFA_IMP_CNBT_VOL_K1_NULL);
    }

    public String getDfaImpCnbtVolK1NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtVolK1Null(), Len.DFA_IMP_CNBT_VOL_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_VOL_K1 = 1;
        public static final int DFA_IMP_CNBT_VOL_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_VOL_K1 = 8;
        public static final int DFA_IMP_CNBT_VOL_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_VOL_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_VOL_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
