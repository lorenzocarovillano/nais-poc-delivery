package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMPB-ACC<br>
 * Variable: S089-TOT-IMPB-ACC from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpbAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpbAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMPB_ACC;
    }

    public void setWlquTotImpbAcc(AfDecimal wlquTotImpbAcc) {
        writeDecimalAsPacked(Pos.S089_TOT_IMPB_ACC, wlquTotImpbAcc.copy());
    }

    public void setWlquTotImpbAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMPB_ACC, Pos.S089_TOT_IMPB_ACC);
    }

    /**Original name: WLQU-TOT-IMPB-ACC<br>*/
    public AfDecimal getWlquTotImpbAcc() {
        return readPackedAsDecimal(Pos.S089_TOT_IMPB_ACC, Len.Int.WLQU_TOT_IMPB_ACC, Len.Fract.WLQU_TOT_IMPB_ACC);
    }

    public byte[] getWlquTotImpbAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMPB_ACC, Pos.S089_TOT_IMPB_ACC);
        return buffer;
    }

    public void initWlquTotImpbAccSpaces() {
        fill(Pos.S089_TOT_IMPB_ACC, Len.S089_TOT_IMPB_ACC, Types.SPACE_CHAR);
    }

    public void setWlquTotImpbAccNull(String wlquTotImpbAccNull) {
        writeString(Pos.S089_TOT_IMPB_ACC_NULL, wlquTotImpbAccNull, Len.WLQU_TOT_IMPB_ACC_NULL);
    }

    /**Original name: WLQU-TOT-IMPB-ACC-NULL<br>*/
    public String getWlquTotImpbAccNull() {
        return readString(Pos.S089_TOT_IMPB_ACC_NULL, Len.WLQU_TOT_IMPB_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMPB_ACC = 1;
        public static final int S089_TOT_IMPB_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMPB_ACC = 8;
        public static final int WLQU_TOT_IMPB_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMPB_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMPB_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
