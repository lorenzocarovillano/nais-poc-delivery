package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-NET-T<br>
 * Variable: WB03-TS-NET-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsNetT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsNetT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_NET_T;
    }

    public void setWb03TsNetTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_NET_T, Pos.WB03_TS_NET_T);
    }

    /**Original name: WB03-TS-NET-T<br>*/
    public AfDecimal getWb03TsNetT() {
        return readPackedAsDecimal(Pos.WB03_TS_NET_T, Len.Int.WB03_TS_NET_T, Len.Fract.WB03_TS_NET_T);
    }

    public byte[] getWb03TsNetTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_NET_T, Pos.WB03_TS_NET_T);
        return buffer;
    }

    public void setWb03TsNetTNull(String wb03TsNetTNull) {
        writeString(Pos.WB03_TS_NET_T_NULL, wb03TsNetTNull, Len.WB03_TS_NET_T_NULL);
    }

    /**Original name: WB03-TS-NET-T-NULL<br>*/
    public String getWb03TsNetTNull() {
        return readString(Pos.WB03_TS_NET_T_NULL, Len.WB03_TS_NET_T_NULL);
    }

    public String getWb03TsNetTNullFormatted() {
        return Functions.padBlanks(getWb03TsNetTNull(), Len.WB03_TS_NET_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_NET_T = 1;
        public static final int WB03_TS_NET_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_NET_T = 8;
        public static final int WB03_TS_NET_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_NET_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_NET_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
