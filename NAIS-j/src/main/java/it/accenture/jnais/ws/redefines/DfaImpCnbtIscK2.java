package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-ISC-K2<br>
 * Variable: DFA-IMP-CNBT-ISC-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtIscK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtIscK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_ISC_K2;
    }

    public void setDfaImpCnbtIscK2(AfDecimal dfaImpCnbtIscK2) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_ISC_K2, dfaImpCnbtIscK2.copy());
    }

    public void setDfaImpCnbtIscK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_ISC_K2, Pos.DFA_IMP_CNBT_ISC_K2);
    }

    /**Original name: DFA-IMP-CNBT-ISC-K2<br>*/
    public AfDecimal getDfaImpCnbtIscK2() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_ISC_K2, Len.Int.DFA_IMP_CNBT_ISC_K2, Len.Fract.DFA_IMP_CNBT_ISC_K2);
    }

    public byte[] getDfaImpCnbtIscK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_ISC_K2, Pos.DFA_IMP_CNBT_ISC_K2);
        return buffer;
    }

    public void setDfaImpCnbtIscK2Null(String dfaImpCnbtIscK2Null) {
        writeString(Pos.DFA_IMP_CNBT_ISC_K2_NULL, dfaImpCnbtIscK2Null, Len.DFA_IMP_CNBT_ISC_K2_NULL);
    }

    /**Original name: DFA-IMP-CNBT-ISC-K2-NULL<br>*/
    public String getDfaImpCnbtIscK2Null() {
        return readString(Pos.DFA_IMP_CNBT_ISC_K2_NULL, Len.DFA_IMP_CNBT_ISC_K2_NULL);
    }

    public String getDfaImpCnbtIscK2NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtIscK2Null(), Len.DFA_IMP_CNBT_ISC_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_ISC_K2 = 1;
        public static final int DFA_IMP_CNBT_ISC_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_ISC_K2 = 8;
        public static final int DFA_IMP_CNBT_ISC_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_ISC_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_ISC_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
