package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: NOTE-OGG<br>
 * Variable: NOTE-OGG from copybook IDBVNOT1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class NoteOggIdbsnot0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: NOT-ID-NOTE-OGG
    private int idNoteOgg = DefaultValues.INT_VAL;
    //Original name: NOT-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: NOT-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: NOT-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: NOT-NOTA-OGG-LEN
    private short notaOggLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: NOT-NOTA-OGG
    private String notaOgg = DefaultValues.stringVal(Len.NOTA_OGG);
    //Original name: NOT-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: NOT-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: NOT-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: NOT-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: NOT-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.NOTE_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setNoteOggBytes(buf);
    }

    public void setNoteOggBytes(byte[] buffer) {
        setNoteOggBytes(buffer, 1);
    }

    public byte[] getNoteOggBytes() {
        byte[] buffer = new byte[Len.NOTE_OGG];
        return getNoteOggBytes(buffer, 1);
    }

    public void setNoteOggBytes(byte[] buffer, int offset) {
        int position = offset;
        idNoteOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_NOTE_OGG, 0);
        position += Len.ID_NOTE_OGG;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        setNotaOggVcharBytes(buffer, position);
        position += Len.NOTA_OGG_VCHAR;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getNoteOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idNoteOgg, Len.Int.ID_NOTE_OGG, 0);
        position += Len.ID_NOTE_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        getNotaOggVcharBytes(buffer, position);
        position += Len.NOTA_OGG_VCHAR;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        return buffer;
    }

    public void setIdNoteOgg(int idNoteOgg) {
        this.idNoteOgg = idNoteOgg;
    }

    public int getIdNoteOgg() {
        return this.idNoteOgg;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setNotaOggVcharFormatted(String data) {
        byte[] buffer = new byte[Len.NOTA_OGG_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.NOTA_OGG_VCHAR);
        setNotaOggVcharBytes(buffer, 1);
    }

    public String getNotaOggVcharFormatted() {
        return MarshalByteExt.bufferToStr(getNotaOggVcharBytes());
    }

    /**Original name: NOT-NOTA-OGG-VCHAR<br>*/
    public byte[] getNotaOggVcharBytes() {
        byte[] buffer = new byte[Len.NOTA_OGG_VCHAR];
        return getNotaOggVcharBytes(buffer, 1);
    }

    public void setNotaOggVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        notaOggLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        notaOgg = MarshalByte.readString(buffer, position, Len.NOTA_OGG);
    }

    public byte[] getNotaOggVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, notaOggLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, notaOgg, Len.NOTA_OGG);
        return buffer;
    }

    public void setNotaOggLen(short notaOggLen) {
        this.notaOggLen = notaOggLen;
    }

    public short getNotaOggLen() {
        return this.notaOggLen;
    }

    public void setNotaOgg(String notaOgg) {
        this.notaOgg = Functions.subString(notaOgg, Len.NOTA_OGG);
    }

    public String getNotaOgg() {
        return this.notaOgg;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public void setDsOperSqlFormatted(String dsOperSql) {
        setDsOperSql(Functions.charAt(dsOperSql, Types.CHAR_SIZE));
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public void setDsStatoElabFormatted(String dsStatoElab) {
        setDsStatoElab(Functions.charAt(dsStatoElab, Types.CHAR_SIZE));
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    @Override
    public byte[] serialize() {
        return getNoteOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_NOTE_OGG = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int NOTA_OGG_LEN = 2;
        public static final int NOTA_OGG = 250;
        public static final int NOTA_OGG_VCHAR = NOTA_OGG_LEN + NOTA_OGG;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int NOTE_OGG = ID_NOTE_OGG + COD_COMP_ANIA + ID_OGG + TP_OGG + NOTA_OGG_VCHAR + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_NOTE_OGG = 9;
            public static final int COD_COMP_ANIA = 5;
            public static final int ID_OGG = 9;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
