package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COEFF-OPZ-CPT<br>
 * Variable: WB03-COEFF-OPZ-CPT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CoeffOpzCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CoeffOpzCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COEFF_OPZ_CPT;
    }

    public void setWb03CoeffOpzCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COEFF_OPZ_CPT, Pos.WB03_COEFF_OPZ_CPT);
    }

    /**Original name: WB03-COEFF-OPZ-CPT<br>*/
    public AfDecimal getWb03CoeffOpzCpt() {
        return readPackedAsDecimal(Pos.WB03_COEFF_OPZ_CPT, Len.Int.WB03_COEFF_OPZ_CPT, Len.Fract.WB03_COEFF_OPZ_CPT);
    }

    public byte[] getWb03CoeffOpzCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COEFF_OPZ_CPT, Pos.WB03_COEFF_OPZ_CPT);
        return buffer;
    }

    public void setWb03CoeffOpzCptNull(String wb03CoeffOpzCptNull) {
        writeString(Pos.WB03_COEFF_OPZ_CPT_NULL, wb03CoeffOpzCptNull, Len.WB03_COEFF_OPZ_CPT_NULL);
    }

    /**Original name: WB03-COEFF-OPZ-CPT-NULL<br>*/
    public String getWb03CoeffOpzCptNull() {
        return readString(Pos.WB03_COEFF_OPZ_CPT_NULL, Len.WB03_COEFF_OPZ_CPT_NULL);
    }

    public String getWb03CoeffOpzCptNullFormatted() {
        return Functions.padBlanks(getWb03CoeffOpzCptNull(), Len.WB03_COEFF_OPZ_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_OPZ_CPT = 1;
        public static final int WB03_COEFF_OPZ_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_OPZ_CPT = 4;
        public static final int WB03_COEFF_OPZ_CPT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_OPZ_CPT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_OPZ_CPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
