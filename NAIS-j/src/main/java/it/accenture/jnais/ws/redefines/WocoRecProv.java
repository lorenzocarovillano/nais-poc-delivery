package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-REC-PROV<br>
 * Variable: WOCO-REC-PROV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoRecProv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoRecProv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_REC_PROV;
    }

    public void setWocoRecProv(AfDecimal wocoRecProv) {
        writeDecimalAsPacked(Pos.WOCO_REC_PROV, wocoRecProv.copy());
    }

    public void setWocoRecProvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_REC_PROV, Pos.WOCO_REC_PROV);
    }

    /**Original name: WOCO-REC-PROV<br>*/
    public AfDecimal getWocoRecProv() {
        return readPackedAsDecimal(Pos.WOCO_REC_PROV, Len.Int.WOCO_REC_PROV, Len.Fract.WOCO_REC_PROV);
    }

    public byte[] getWocoRecProvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_REC_PROV, Pos.WOCO_REC_PROV);
        return buffer;
    }

    public void initWocoRecProvSpaces() {
        fill(Pos.WOCO_REC_PROV, Len.WOCO_REC_PROV, Types.SPACE_CHAR);
    }

    public void setWocoRecProvNull(String wocoRecProvNull) {
        writeString(Pos.WOCO_REC_PROV_NULL, wocoRecProvNull, Len.WOCO_REC_PROV_NULL);
    }

    /**Original name: WOCO-REC-PROV-NULL<br>*/
    public String getWocoRecProvNull() {
        return readString(Pos.WOCO_REC_PROV_NULL, Len.WOCO_REC_PROV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_REC_PROV = 1;
        public static final int WOCO_REC_PROV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_REC_PROV = 8;
        public static final int WOCO_REC_PROV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_REC_PROV = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_REC_PROV = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
