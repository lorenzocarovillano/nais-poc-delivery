package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-CARZ<br>
 * Variable: W-B03-CARZ from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CarzLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CarzLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CARZ;
    }

    public void setwB03Carz(int wB03Carz) {
        writeIntAsPacked(Pos.W_B03_CARZ, wB03Carz, Len.Int.W_B03_CARZ);
    }

    /**Original name: W-B03-CARZ<br>*/
    public int getwB03Carz() {
        return readPackedAsInt(Pos.W_B03_CARZ, Len.Int.W_B03_CARZ);
    }

    public byte[] getwB03CarzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CARZ, Pos.W_B03_CARZ);
        return buffer;
    }

    public void setwB03CarzNull(String wB03CarzNull) {
        writeString(Pos.W_B03_CARZ_NULL, wB03CarzNull, Len.W_B03_CARZ_NULL);
    }

    /**Original name: W-B03-CARZ-NULL<br>*/
    public String getwB03CarzNull() {
        return readString(Pos.W_B03_CARZ_NULL, Len.W_B03_CARZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CARZ = 1;
        public static final int W_B03_CARZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CARZ = 3;
        public static final int W_B03_CARZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CARZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
