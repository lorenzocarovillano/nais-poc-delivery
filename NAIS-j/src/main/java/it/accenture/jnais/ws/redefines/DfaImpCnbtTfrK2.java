package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-TFR-K2<br>
 * Variable: DFA-IMP-CNBT-TFR-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtTfrK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtTfrK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_TFR_K2;
    }

    public void setDfaImpCnbtTfrK2(AfDecimal dfaImpCnbtTfrK2) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_TFR_K2, dfaImpCnbtTfrK2.copy());
    }

    public void setDfaImpCnbtTfrK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_TFR_K2, Pos.DFA_IMP_CNBT_TFR_K2);
    }

    /**Original name: DFA-IMP-CNBT-TFR-K2<br>*/
    public AfDecimal getDfaImpCnbtTfrK2() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_TFR_K2, Len.Int.DFA_IMP_CNBT_TFR_K2, Len.Fract.DFA_IMP_CNBT_TFR_K2);
    }

    public byte[] getDfaImpCnbtTfrK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_TFR_K2, Pos.DFA_IMP_CNBT_TFR_K2);
        return buffer;
    }

    public void setDfaImpCnbtTfrK2Null(String dfaImpCnbtTfrK2Null) {
        writeString(Pos.DFA_IMP_CNBT_TFR_K2_NULL, dfaImpCnbtTfrK2Null, Len.DFA_IMP_CNBT_TFR_K2_NULL);
    }

    /**Original name: DFA-IMP-CNBT-TFR-K2-NULL<br>*/
    public String getDfaImpCnbtTfrK2Null() {
        return readString(Pos.DFA_IMP_CNBT_TFR_K2_NULL, Len.DFA_IMP_CNBT_TFR_K2_NULL);
    }

    public String getDfaImpCnbtTfrK2NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtTfrK2Null(), Len.DFA_IMP_CNBT_TFR_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_TFR_K2 = 1;
        public static final int DFA_IMP_CNBT_TFR_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_TFR_K2 = 8;
        public static final int DFA_IMP_CNBT_TFR_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_TFR_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_TFR_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
