package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-1O-PER-MM<br>
 * Variable: B03-DUR-1O-PER-MM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Dur1oPerMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Dur1oPerMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR1O_PER_MM;
    }

    public void setB03Dur1oPerMm(int b03Dur1oPerMm) {
        writeIntAsPacked(Pos.B03_DUR1O_PER_MM, b03Dur1oPerMm, Len.Int.B03_DUR1O_PER_MM);
    }

    public void setB03Dur1oPerMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR1O_PER_MM, Pos.B03_DUR1O_PER_MM);
    }

    /**Original name: B03-DUR-1O-PER-MM<br>*/
    public int getB03Dur1oPerMm() {
        return readPackedAsInt(Pos.B03_DUR1O_PER_MM, Len.Int.B03_DUR1O_PER_MM);
    }

    public byte[] getB03Dur1oPerMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR1O_PER_MM, Pos.B03_DUR1O_PER_MM);
        return buffer;
    }

    public void setB03Dur1oPerMmNull(String b03Dur1oPerMmNull) {
        writeString(Pos.B03_DUR1O_PER_MM_NULL, b03Dur1oPerMmNull, Len.B03_DUR1O_PER_MM_NULL);
    }

    /**Original name: B03-DUR-1O-PER-MM-NULL<br>*/
    public String getB03Dur1oPerMmNull() {
        return readString(Pos.B03_DUR1O_PER_MM_NULL, Len.B03_DUR1O_PER_MM_NULL);
    }

    public String getB03Dur1oPerMmNullFormatted() {
        return Functions.padBlanks(getB03Dur1oPerMmNull(), Len.B03_DUR1O_PER_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR1O_PER_MM = 1;
        public static final int B03_DUR1O_PER_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR1O_PER_MM = 3;
        public static final int B03_DUR1O_PER_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR1O_PER_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
