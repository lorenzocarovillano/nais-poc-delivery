package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-NASC-1O-ASSTO<br>
 * Variable: B03-DT-NASC-1O-ASSTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtNasc1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtNasc1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_NASC1O_ASSTO;
    }

    public void setB03DtNasc1oAssto(int b03DtNasc1oAssto) {
        writeIntAsPacked(Pos.B03_DT_NASC1O_ASSTO, b03DtNasc1oAssto, Len.Int.B03_DT_NASC1O_ASSTO);
    }

    public void setB03DtNasc1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_NASC1O_ASSTO, Pos.B03_DT_NASC1O_ASSTO);
    }

    /**Original name: B03-DT-NASC-1O-ASSTO<br>*/
    public int getB03DtNasc1oAssto() {
        return readPackedAsInt(Pos.B03_DT_NASC1O_ASSTO, Len.Int.B03_DT_NASC1O_ASSTO);
    }

    public byte[] getB03DtNasc1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_NASC1O_ASSTO, Pos.B03_DT_NASC1O_ASSTO);
        return buffer;
    }

    public void setB03DtNasc1oAsstoNull(String b03DtNasc1oAsstoNull) {
        writeString(Pos.B03_DT_NASC1O_ASSTO_NULL, b03DtNasc1oAsstoNull, Len.B03_DT_NASC1O_ASSTO_NULL);
    }

    /**Original name: B03-DT-NASC-1O-ASSTO-NULL<br>*/
    public String getB03DtNasc1oAsstoNull() {
        return readString(Pos.B03_DT_NASC1O_ASSTO_NULL, Len.B03_DT_NASC1O_ASSTO_NULL);
    }

    public String getB03DtNasc1oAsstoNullFormatted() {
        return Functions.padBlanks(getB03DtNasc1oAsstoNull(), Len.B03_DT_NASC1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_NASC1O_ASSTO = 1;
        public static final int B03_DT_NASC1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_NASC1O_ASSTO = 5;
        public static final int B03_DT_NASC1O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_NASC1O_ASSTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
