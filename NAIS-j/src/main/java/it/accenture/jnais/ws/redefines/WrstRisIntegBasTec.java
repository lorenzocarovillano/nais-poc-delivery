package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-INTEG-BAS-TEC<br>
 * Variable: WRST-RIS-INTEG-BAS-TEC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisIntegBasTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisIntegBasTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_INTEG_BAS_TEC;
    }

    public void setWrstRisIntegBasTec(AfDecimal wrstRisIntegBasTec) {
        writeDecimalAsPacked(Pos.WRST_RIS_INTEG_BAS_TEC, wrstRisIntegBasTec.copy());
    }

    public void setWrstRisIntegBasTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_INTEG_BAS_TEC, Pos.WRST_RIS_INTEG_BAS_TEC);
    }

    /**Original name: WRST-RIS-INTEG-BAS-TEC<br>*/
    public AfDecimal getWrstRisIntegBasTec() {
        return readPackedAsDecimal(Pos.WRST_RIS_INTEG_BAS_TEC, Len.Int.WRST_RIS_INTEG_BAS_TEC, Len.Fract.WRST_RIS_INTEG_BAS_TEC);
    }

    public byte[] getWrstRisIntegBasTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_INTEG_BAS_TEC, Pos.WRST_RIS_INTEG_BAS_TEC);
        return buffer;
    }

    public void initWrstRisIntegBasTecSpaces() {
        fill(Pos.WRST_RIS_INTEG_BAS_TEC, Len.WRST_RIS_INTEG_BAS_TEC, Types.SPACE_CHAR);
    }

    public void setWrstRisIntegBasTecNull(String wrstRisIntegBasTecNull) {
        writeString(Pos.WRST_RIS_INTEG_BAS_TEC_NULL, wrstRisIntegBasTecNull, Len.WRST_RIS_INTEG_BAS_TEC_NULL);
    }

    /**Original name: WRST-RIS-INTEG-BAS-TEC-NULL<br>*/
    public String getWrstRisIntegBasTecNull() {
        return readString(Pos.WRST_RIS_INTEG_BAS_TEC_NULL, Len.WRST_RIS_INTEG_BAS_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_INTEG_BAS_TEC = 1;
        public static final int WRST_RIS_INTEG_BAS_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_INTEG_BAS_TEC = 8;
        public static final int WRST_RIS_INTEG_BAS_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_INTEG_BAS_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_INTEG_BAS_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
