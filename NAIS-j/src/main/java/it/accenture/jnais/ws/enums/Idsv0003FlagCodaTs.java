package it.accenture.jnais.ws.enums;

/**Original name: IDSV0003-FLAG-CODA-TS<br>
 * Variable: IDSV0003-FLAG-CODA-TS from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003FlagCodaTs {

    //==== PROPERTIES ====
    private char value = 'N';
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCodaTs(char flagCodaTs) {
        this.value = flagCodaTs;
    }

    public char getFlagCodaTs() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_CODA_TS = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
