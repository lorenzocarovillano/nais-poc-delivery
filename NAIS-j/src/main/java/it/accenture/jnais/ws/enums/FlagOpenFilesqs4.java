package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS4<br>
 * Variable: FLAG-OPEN-FILESQS4 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqs4 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagOpenFilesqs4(char flagOpenFilesqs4) {
        this.value = flagOpenFilesqs4;
    }

    public char getFlagOpenFilesqs4() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setOpenFilesqs4No() {
        value = NO;
    }
}
