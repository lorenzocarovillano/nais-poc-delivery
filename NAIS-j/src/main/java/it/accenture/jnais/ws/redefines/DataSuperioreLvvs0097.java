package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: DATA-SUPERIORE<br>
 * Variable: DATA-SUPERIORE from program LVVS0097<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DataSuperioreLvvs0097 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DataSuperioreLvvs0097() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DATA_SUPERIORE;
    }

    @Override
    public void init() {
        int position = 1;
        writeShort(position, ((short)0), Len.Int.AAAA_SUP, SignType.NO_SIGN);
        position += Len.AAAA_SUP;
        writeShort(position, ((short)12), Len.Int.MM_SUP, SignType.NO_SIGN);
        position += Len.MM_SUP;
        writeShort(position, ((short)31), Len.Int.GG_SUP, SignType.NO_SIGN);
    }

    public void setAaaaSupFormatted(String aaaaSup) {
        writeString(Pos.AAAA_SUP, Trunc.toUnsignedNumeric(aaaaSup, Len.AAAA_SUP), Len.AAAA_SUP);
    }

    /**Original name: AAAA-SUP<br>*/
    public short getAaaaSup() {
        return readNumDispUnsignedShort(Pos.AAAA_SUP, Len.AAAA_SUP);
    }

    public void setDataSuperioreN(int dataSuperioreN) {
        writeInt(Pos.DATA_SUPERIORE_N, dataSuperioreN, Len.Int.DATA_SUPERIORE_N, SignType.NO_SIGN);
    }

    /**Original name: DATA-SUPERIORE-N<br>*/
    public int getDataSuperioreN() {
        return readNumDispUnsignedInt(Pos.DATA_SUPERIORE_N, Len.DATA_SUPERIORE_N);
    }

    public String getDataSuperioreNFormatted() {
        return readFixedString(Pos.DATA_SUPERIORE_N, Len.DATA_SUPERIORE_N);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DATA_SUPERIORE = 1;
        public static final int AAAA_SUP = DATA_SUPERIORE;
        public static final int MM_SUP = AAAA_SUP + Len.AAAA_SUP;
        public static final int GG_SUP = MM_SUP + Len.MM_SUP;
        public static final int DATA_SUPERIORE_N = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_SUP = 4;
        public static final int MM_SUP = 2;
        public static final int GG_SUP = 2;
        public static final int DATA_SUPERIORE = AAAA_SUP + MM_SUP + GG_SUP;
        public static final int DATA_SUPERIORE_N = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int AAAA_SUP = 4;
            public static final int MM_SUP = 2;
            public static final int GG_SUP = 2;
            public static final int DATA_SUPERIORE_N = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
