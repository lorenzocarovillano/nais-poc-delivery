package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PC-AZ<br>
 * Variable: ADE-PC-AZ from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePcAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePcAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PC_AZ;
    }

    public void setAdePcAz(AfDecimal adePcAz) {
        writeDecimalAsPacked(Pos.ADE_PC_AZ, adePcAz.copy());
    }

    public void setAdePcAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PC_AZ, Pos.ADE_PC_AZ);
    }

    /**Original name: ADE-PC-AZ<br>*/
    public AfDecimal getAdePcAz() {
        return readPackedAsDecimal(Pos.ADE_PC_AZ, Len.Int.ADE_PC_AZ, Len.Fract.ADE_PC_AZ);
    }

    public byte[] getAdePcAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PC_AZ, Pos.ADE_PC_AZ);
        return buffer;
    }

    public void setAdePcAzNull(String adePcAzNull) {
        writeString(Pos.ADE_PC_AZ_NULL, adePcAzNull, Len.ADE_PC_AZ_NULL);
    }

    /**Original name: ADE-PC-AZ-NULL<br>*/
    public String getAdePcAzNull() {
        return readString(Pos.ADE_PC_AZ_NULL, Len.ADE_PC_AZ_NULL);
    }

    public String getAdePcAzNullFormatted() {
        return Functions.padBlanks(getAdePcAzNull(), Len.ADE_PC_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PC_AZ = 1;
        public static final int ADE_PC_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PC_AZ = 4;
        public static final int ADE_PC_AZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PC_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
