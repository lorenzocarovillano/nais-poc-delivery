package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-PRE-VERS<br>
 * Variable: P67-PRE-VERS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67PreVers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67PreVers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_PRE_VERS;
    }

    public void setP67PreVers(AfDecimal p67PreVers) {
        writeDecimalAsPacked(Pos.P67_PRE_VERS, p67PreVers.copy());
    }

    public void setP67PreVersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_PRE_VERS, Pos.P67_PRE_VERS);
    }

    /**Original name: P67-PRE-VERS<br>*/
    public AfDecimal getP67PreVers() {
        return readPackedAsDecimal(Pos.P67_PRE_VERS, Len.Int.P67_PRE_VERS, Len.Fract.P67_PRE_VERS);
    }

    public byte[] getP67PreVersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_PRE_VERS, Pos.P67_PRE_VERS);
        return buffer;
    }

    public void setP67PreVersNull(String p67PreVersNull) {
        writeString(Pos.P67_PRE_VERS_NULL, p67PreVersNull, Len.P67_PRE_VERS_NULL);
    }

    /**Original name: P67-PRE-VERS-NULL<br>*/
    public String getP67PreVersNull() {
        return readString(Pos.P67_PRE_VERS_NULL, Len.P67_PRE_VERS_NULL);
    }

    public String getP67PreVersNullFormatted() {
        return Functions.padBlanks(getP67PreVersNull(), Len.P67_PRE_VERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_PRE_VERS = 1;
        public static final int P67_PRE_VERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_PRE_VERS = 8;
        public static final int P67_PRE_VERS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_PRE_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_PRE_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
