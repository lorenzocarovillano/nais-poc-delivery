package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COEFF-OPZ-CPT<br>
 * Variable: B03-COEFF-OPZ-CPT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CoeffOpzCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CoeffOpzCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COEFF_OPZ_CPT;
    }

    public void setB03CoeffOpzCpt(AfDecimal b03CoeffOpzCpt) {
        writeDecimalAsPacked(Pos.B03_COEFF_OPZ_CPT, b03CoeffOpzCpt.copy());
    }

    public void setB03CoeffOpzCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COEFF_OPZ_CPT, Pos.B03_COEFF_OPZ_CPT);
    }

    /**Original name: B03-COEFF-OPZ-CPT<br>*/
    public AfDecimal getB03CoeffOpzCpt() {
        return readPackedAsDecimal(Pos.B03_COEFF_OPZ_CPT, Len.Int.B03_COEFF_OPZ_CPT, Len.Fract.B03_COEFF_OPZ_CPT);
    }

    public byte[] getB03CoeffOpzCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COEFF_OPZ_CPT, Pos.B03_COEFF_OPZ_CPT);
        return buffer;
    }

    public void setB03CoeffOpzCptNull(String b03CoeffOpzCptNull) {
        writeString(Pos.B03_COEFF_OPZ_CPT_NULL, b03CoeffOpzCptNull, Len.B03_COEFF_OPZ_CPT_NULL);
    }

    /**Original name: B03-COEFF-OPZ-CPT-NULL<br>*/
    public String getB03CoeffOpzCptNull() {
        return readString(Pos.B03_COEFF_OPZ_CPT_NULL, Len.B03_COEFF_OPZ_CPT_NULL);
    }

    public String getB03CoeffOpzCptNullFormatted() {
        return Functions.padBlanks(getB03CoeffOpzCptNull(), Len.B03_COEFF_OPZ_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COEFF_OPZ_CPT = 1;
        public static final int B03_COEFF_OPZ_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COEFF_OPZ_CPT = 4;
        public static final int B03_COEFF_OPZ_CPT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_COEFF_OPZ_CPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COEFF_OPZ_CPT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
