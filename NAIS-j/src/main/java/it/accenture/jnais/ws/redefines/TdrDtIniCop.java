package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-DT-INI-COP<br>
 * Variable: TDR-DT-INI-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_DT_INI_COP;
    }

    public void setTdrDtIniCop(int tdrDtIniCop) {
        writeIntAsPacked(Pos.TDR_DT_INI_COP, tdrDtIniCop, Len.Int.TDR_DT_INI_COP);
    }

    public void setTdrDtIniCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_DT_INI_COP, Pos.TDR_DT_INI_COP);
    }

    /**Original name: TDR-DT-INI-COP<br>*/
    public int getTdrDtIniCop() {
        return readPackedAsInt(Pos.TDR_DT_INI_COP, Len.Int.TDR_DT_INI_COP);
    }

    public byte[] getTdrDtIniCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_DT_INI_COP, Pos.TDR_DT_INI_COP);
        return buffer;
    }

    public void setTdrDtIniCopNull(String tdrDtIniCopNull) {
        writeString(Pos.TDR_DT_INI_COP_NULL, tdrDtIniCopNull, Len.TDR_DT_INI_COP_NULL);
    }

    /**Original name: TDR-DT-INI-COP-NULL<br>*/
    public String getTdrDtIniCopNull() {
        return readString(Pos.TDR_DT_INI_COP_NULL, Len.TDR_DT_INI_COP_NULL);
    }

    public String getTdrDtIniCopNullFormatted() {
        return Functions.padBlanks(getTdrDtIniCopNull(), Len.TDR_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_DT_INI_COP = 1;
        public static final int TDR_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_DT_INI_COP = 5;
        public static final int TDR_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
