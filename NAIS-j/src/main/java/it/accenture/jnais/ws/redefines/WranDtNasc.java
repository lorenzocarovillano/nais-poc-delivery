package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRAN-DT-NASC<br>
 * Variable: WRAN-DT-NASC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WranDtNasc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WranDtNasc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_DT_NASC;
    }

    public void setWranDtNasc(int wranDtNasc) {
        writeIntAsPacked(Pos.WRAN_DT_NASC, wranDtNasc, Len.Int.WRAN_DT_NASC);
    }

    public void setWranDtNascFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRAN_DT_NASC, Pos.WRAN_DT_NASC);
    }

    /**Original name: WRAN-DT-NASC<br>*/
    public int getWranDtNasc() {
        return readPackedAsInt(Pos.WRAN_DT_NASC, Len.Int.WRAN_DT_NASC);
    }

    public byte[] getWranDtNascAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRAN_DT_NASC, Pos.WRAN_DT_NASC);
        return buffer;
    }

    public void initWranDtNascSpaces() {
        fill(Pos.WRAN_DT_NASC, Len.WRAN_DT_NASC, Types.SPACE_CHAR);
    }

    public void setWranDtNascNull(String wranDtNascNull) {
        writeString(Pos.WRAN_DT_NASC_NULL, wranDtNascNull, Len.WRAN_DT_NASC_NULL);
    }

    /**Original name: WRAN-DT-NASC-NULL<br>*/
    public String getWranDtNascNull() {
        return readString(Pos.WRAN_DT_NASC_NULL, Len.WRAN_DT_NASC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRAN_DT_NASC = 1;
        public static final int WRAN_DT_NASC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_DT_NASC = 5;
        public static final int WRAN_DT_NASC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_DT_NASC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
