package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MOV-ID-RICH<br>
 * Variable: MOV-ID-RICH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MovIdRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MovIdRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOV_ID_RICH;
    }

    public void setMovIdRich(int movIdRich) {
        writeIntAsPacked(Pos.MOV_ID_RICH, movIdRich, Len.Int.MOV_ID_RICH);
    }

    public void setMovIdRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MOV_ID_RICH, Pos.MOV_ID_RICH);
    }

    /**Original name: MOV-ID-RICH<br>*/
    public int getMovIdRich() {
        return readPackedAsInt(Pos.MOV_ID_RICH, Len.Int.MOV_ID_RICH);
    }

    public byte[] getMovIdRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MOV_ID_RICH, Pos.MOV_ID_RICH);
        return buffer;
    }

    public void setMovIdRichNull(String movIdRichNull) {
        writeString(Pos.MOV_ID_RICH_NULL, movIdRichNull, Len.MOV_ID_RICH_NULL);
    }

    /**Original name: MOV-ID-RICH-NULL<br>*/
    public String getMovIdRichNull() {
        return readString(Pos.MOV_ID_RICH_NULL, Len.MOV_ID_RICH_NULL);
    }

    public String getMovIdRichNullFormatted() {
        return Functions.padBlanks(getMovIdRichNull(), Len.MOV_ID_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MOV_ID_RICH = 1;
        public static final int MOV_ID_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MOV_ID_RICH = 5;
        public static final int MOV_ID_RICH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MOV_ID_RICH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
