package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-GAR-MM<br>
 * Variable: W-B03-DUR-GAR-MM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurGarMmLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurGarMmLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_GAR_MM;
    }

    public void setwB03DurGarMm(int wB03DurGarMm) {
        writeIntAsPacked(Pos.W_B03_DUR_GAR_MM, wB03DurGarMm, Len.Int.W_B03_DUR_GAR_MM);
    }

    /**Original name: W-B03-DUR-GAR-MM<br>*/
    public int getwB03DurGarMm() {
        return readPackedAsInt(Pos.W_B03_DUR_GAR_MM, Len.Int.W_B03_DUR_GAR_MM);
    }

    public byte[] getwB03DurGarMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_GAR_MM, Pos.W_B03_DUR_GAR_MM);
        return buffer;
    }

    public void setwB03DurGarMmNull(String wB03DurGarMmNull) {
        writeString(Pos.W_B03_DUR_GAR_MM_NULL, wB03DurGarMmNull, Len.W_B03_DUR_GAR_MM_NULL);
    }

    /**Original name: W-B03-DUR-GAR-MM-NULL<br>*/
    public String getwB03DurGarMmNull() {
        return readString(Pos.W_B03_DUR_GAR_MM_NULL, Len.W_B03_DUR_GAR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GAR_MM = 1;
        public static final int W_B03_DUR_GAR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GAR_MM = 3;
        public static final int W_B03_DUR_GAR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_GAR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
