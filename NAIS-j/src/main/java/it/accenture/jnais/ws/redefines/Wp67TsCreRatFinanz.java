package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-TS-CRE-RAT-FINANZ<br>
 * Variable: WP67-TS-CRE-RAT-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67TsCreRatFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67TsCreRatFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_TS_CRE_RAT_FINANZ;
    }

    public void setWp67TsCreRatFinanz(AfDecimal wp67TsCreRatFinanz) {
        writeDecimalAsPacked(Pos.WP67_TS_CRE_RAT_FINANZ, wp67TsCreRatFinanz.copy());
    }

    public void setWp67TsCreRatFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_TS_CRE_RAT_FINANZ, Pos.WP67_TS_CRE_RAT_FINANZ);
    }

    /**Original name: WP67-TS-CRE-RAT-FINANZ<br>*/
    public AfDecimal getWp67TsCreRatFinanz() {
        return readPackedAsDecimal(Pos.WP67_TS_CRE_RAT_FINANZ, Len.Int.WP67_TS_CRE_RAT_FINANZ, Len.Fract.WP67_TS_CRE_RAT_FINANZ);
    }

    public byte[] getWp67TsCreRatFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_TS_CRE_RAT_FINANZ, Pos.WP67_TS_CRE_RAT_FINANZ);
        return buffer;
    }

    public void setWp67TsCreRatFinanzNull(String wp67TsCreRatFinanzNull) {
        writeString(Pos.WP67_TS_CRE_RAT_FINANZ_NULL, wp67TsCreRatFinanzNull, Len.WP67_TS_CRE_RAT_FINANZ_NULL);
    }

    /**Original name: WP67-TS-CRE-RAT-FINANZ-NULL<br>*/
    public String getWp67TsCreRatFinanzNull() {
        return readString(Pos.WP67_TS_CRE_RAT_FINANZ_NULL, Len.WP67_TS_CRE_RAT_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_TS_CRE_RAT_FINANZ = 1;
        public static final int WP67_TS_CRE_RAT_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_TS_CRE_RAT_FINANZ = 8;
        public static final int WP67_TS_CRE_RAT_FINANZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_TS_CRE_RAT_FINANZ = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_TS_CRE_RAT_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
