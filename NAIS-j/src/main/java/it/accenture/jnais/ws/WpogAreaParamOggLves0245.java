package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;

/**Original name: WPOG-AREA-PARAM-OGG<br>
 * Variable: WPOG-AREA-PARAM-OGG from program LVES0245<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WpogAreaParamOggLves0245 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_OGG_MAXOCCURS = 200;
    //Original name: WPOG-ELE-PARAM-OGG-MAX
    private short eleParamOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] tabParamOgg = new WpogTabParamOgg[TAB_PARAM_OGG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WpogAreaParamOggLves0245() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOG_AREA_PARAM_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWpogAreaParamOggBytes(buf);
    }

    public void init() {
        for (int tabParamOggIdx = 1; tabParamOggIdx <= TAB_PARAM_OGG_MAXOCCURS; tabParamOggIdx++) {
            tabParamOgg[tabParamOggIdx - 1] = new WpogTabParamOgg();
        }
    }

    public String getWpogAreaParamOggFormatted() {
        return MarshalByteExt.bufferToStr(getWpogAreaParamOggBytes());
    }

    public void setWpogAreaParamOggBytes(byte[] buffer) {
        setWpogAreaParamOggBytes(buffer, 1);
    }

    public byte[] getWpogAreaParamOggBytes() {
        byte[] buffer = new byte[Len.WPOG_AREA_PARAM_OGG];
        return getWpogAreaParamOggBytes(buffer, 1);
    }

    public void setWpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        eleParamOggMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PARAM_OGG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabParamOgg[idx - 1].setWpogTabParamOggBytes(buffer, position);
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
            else {
                tabParamOgg[idx - 1].initWpogTabParamOggSpaces();
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
        }
    }

    public byte[] getWpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleParamOggMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_PARAM_OGG_MAXOCCURS; idx++) {
            tabParamOgg[idx - 1].getWpogTabParamOggBytes(buffer, position);
            position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
        }
        return buffer;
    }

    public void setEleParamOggMax(short eleParamOggMax) {
        this.eleParamOggMax = eleParamOggMax;
    }

    public short getEleParamOggMax() {
        return this.eleParamOggMax;
    }

    public WpogTabParamOgg getTabParamOgg(int idx) {
        return tabParamOgg[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWpogAreaParamOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_PARAM_OGG_MAX = 2;
        public static final int WPOG_AREA_PARAM_OGG = ELE_PARAM_OGG_MAX + WpogAreaParamOggLves0245.TAB_PARAM_OGG_MAXOCCURS * WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
