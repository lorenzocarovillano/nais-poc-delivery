package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ICNB-INPSTFM-CALC<br>
 * Variable: DFL-ICNB-INPSTFM-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIcnbInpstfmCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIcnbInpstfmCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ICNB_INPSTFM_CALC;
    }

    public void setDflIcnbInpstfmCalc(AfDecimal dflIcnbInpstfmCalc) {
        writeDecimalAsPacked(Pos.DFL_ICNB_INPSTFM_CALC, dflIcnbInpstfmCalc.copy());
    }

    public void setDflIcnbInpstfmCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ICNB_INPSTFM_CALC, Pos.DFL_ICNB_INPSTFM_CALC);
    }

    /**Original name: DFL-ICNB-INPSTFM-CALC<br>*/
    public AfDecimal getDflIcnbInpstfmCalc() {
        return readPackedAsDecimal(Pos.DFL_ICNB_INPSTFM_CALC, Len.Int.DFL_ICNB_INPSTFM_CALC, Len.Fract.DFL_ICNB_INPSTFM_CALC);
    }

    public byte[] getDflIcnbInpstfmCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ICNB_INPSTFM_CALC, Pos.DFL_ICNB_INPSTFM_CALC);
        return buffer;
    }

    public void setDflIcnbInpstfmCalcNull(String dflIcnbInpstfmCalcNull) {
        writeString(Pos.DFL_ICNB_INPSTFM_CALC_NULL, dflIcnbInpstfmCalcNull, Len.DFL_ICNB_INPSTFM_CALC_NULL);
    }

    /**Original name: DFL-ICNB-INPSTFM-CALC-NULL<br>*/
    public String getDflIcnbInpstfmCalcNull() {
        return readString(Pos.DFL_ICNB_INPSTFM_CALC_NULL, Len.DFL_ICNB_INPSTFM_CALC_NULL);
    }

    public String getDflIcnbInpstfmCalcNullFormatted() {
        return Functions.padBlanks(getDflIcnbInpstfmCalcNull(), Len.DFL_ICNB_INPSTFM_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ICNB_INPSTFM_CALC = 1;
        public static final int DFL_ICNB_INPSTFM_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ICNB_INPSTFM_CALC = 8;
        public static final int DFL_ICNB_INPSTFM_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ICNB_INPSTFM_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ICNB_INPSTFM_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
