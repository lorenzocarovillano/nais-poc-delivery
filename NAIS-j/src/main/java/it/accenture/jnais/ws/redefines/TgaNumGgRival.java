package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-NUM-GG-RIVAL<br>
 * Variable: TGA-NUM-GG-RIVAL from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaNumGgRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaNumGgRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_NUM_GG_RIVAL;
    }

    public void setTgaNumGgRival(int tgaNumGgRival) {
        writeIntAsPacked(Pos.TGA_NUM_GG_RIVAL, tgaNumGgRival, Len.Int.TGA_NUM_GG_RIVAL);
    }

    public void setTgaNumGgRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_NUM_GG_RIVAL, Pos.TGA_NUM_GG_RIVAL);
    }

    /**Original name: TGA-NUM-GG-RIVAL<br>*/
    public int getTgaNumGgRival() {
        return readPackedAsInt(Pos.TGA_NUM_GG_RIVAL, Len.Int.TGA_NUM_GG_RIVAL);
    }

    public byte[] getTgaNumGgRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_NUM_GG_RIVAL, Pos.TGA_NUM_GG_RIVAL);
        return buffer;
    }

    public void setTgaNumGgRivalNull(String tgaNumGgRivalNull) {
        writeString(Pos.TGA_NUM_GG_RIVAL_NULL, tgaNumGgRivalNull, Len.TGA_NUM_GG_RIVAL_NULL);
    }

    /**Original name: TGA-NUM-GG-RIVAL-NULL<br>*/
    public String getTgaNumGgRivalNull() {
        return readString(Pos.TGA_NUM_GG_RIVAL_NULL, Len.TGA_NUM_GG_RIVAL_NULL);
    }

    public String getTgaNumGgRivalNullFormatted() {
        return Functions.padBlanks(getTgaNumGgRivalNull(), Len.TGA_NUM_GG_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_NUM_GG_RIVAL = 1;
        public static final int TGA_NUM_GG_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_NUM_GG_RIVAL = 3;
        public static final int TGA_NUM_GG_RIVAL_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_NUM_GG_RIVAL = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
