package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-ADER<br>
 * Variable: L3421-IMP-ADER from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_ADER;
    }

    public void setL3421ImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_ADER, Pos.L3421_IMP_ADER);
    }

    /**Original name: L3421-IMP-ADER<br>*/
    public AfDecimal getL3421ImpAder() {
        return readPackedAsDecimal(Pos.L3421_IMP_ADER, Len.Int.L3421_IMP_ADER, Len.Fract.L3421_IMP_ADER);
    }

    public byte[] getL3421ImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_ADER, Pos.L3421_IMP_ADER);
        return buffer;
    }

    /**Original name: L3421-IMP-ADER-NULL<br>*/
    public String getL3421ImpAderNull() {
        return readString(Pos.L3421_IMP_ADER_NULL, Len.L3421_IMP_ADER_NULL);
    }

    public String getL3421ImpAderNullFormatted() {
        return Functions.padBlanks(getL3421ImpAderNull(), Len.L3421_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_ADER = 1;
        public static final int L3421_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_ADER = 8;
        public static final int L3421_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
