package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: C216-TAB-VAL-T<br>
 * Variable: C216-TAB-VAL-T from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class C216TabValT extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VARIABILI_T_MAXOCCURS = 75;
    public static final int TAB_LIVELLO_T_MAXOCCURS = 750;
    public static final String C216_FLG_ITN_POS = "0";
    public static final String C216_FLG_ITN_NEG = "1";

    //==== CONSTRUCTORS ====
    public C216TabValT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.C216_TAB_VAL_T;
    }

    public String getIvvc0216TabValTFormatted() {
        return readFixedString(Pos.C216_TAB_VAL_T, Len.C216_TAB_VAL_T);
    }

    public void setC216TabValTBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.C216_TAB_VAL_T, Pos.C216_TAB_VAL_T);
    }

    public byte[] getC216TabValTBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.C216_TAB_VAL_T, Pos.C216_TAB_VAL_T);
        return buffer;
    }

    public void setIvvc0216IdGarT(int ivvc0216IdGarTIdx, int ivvc0216IdGarT) {
        int position = Pos.c216IdGarT(ivvc0216IdGarTIdx - 1);
        writeInt(position, ivvc0216IdGarT, Len.Int.IVVC0216_ID_GAR_T, SignType.NO_SIGN);
    }

    public void setIvvc0216IdGarTFormatted(int ivvc0216IdGarTIdx, String ivvc0216IdGarT) {
        int position = Pos.c216IdGarT(ivvc0216IdGarTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ivvc0216IdGarT, Len.ID_GAR_T), Len.ID_GAR_T);
    }

    /**Original name: C216-ID-GAR-T<br>*/
    public int getIdGarT(int idGarTIdx) {
        int position = Pos.c216IdGarT(idGarTIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_GAR_T);
    }

    public String getIdGarTFormatted(int idGarTIdx) {
        int position = Pos.c216IdGarT(idGarTIdx - 1);
        return readFixedString(position, Len.ID_GAR_T);
    }

    public void setIvvc0216CodTipoOpzioneT(int ivvc0216CodTipoOpzioneTIdx, String ivvc0216CodTipoOpzioneT) {
        int position = Pos.c216CodTipoOpzioneT(ivvc0216CodTipoOpzioneTIdx - 1);
        writeString(position, ivvc0216CodTipoOpzioneT, Len.COD_TIPO_OPZIONE_T);
    }

    /**Original name: C216-COD-TIPO-OPZIONE-T<br>*/
    public String getCodTipoOpzioneT(int codTipoOpzioneTIdx) {
        int position = Pos.c216CodTipoOpzioneT(codTipoOpzioneTIdx - 1);
        return readString(position, Len.COD_TIPO_OPZIONE_T);
    }

    public String getCodTipoOpzioneTFormatted(int codTipoOpzioneTIdx) {
        return Functions.padBlanks(getCodTipoOpzioneT(codTipoOpzioneTIdx), Len.COD_TIPO_OPZIONE_T);
    }

    public void setIvvc0216TpLivelloT(int ivvc0216TpLivelloTIdx, char ivvc0216TpLivelloT) {
        int position = Pos.c216TpLivelloT(ivvc0216TpLivelloTIdx - 1);
        writeChar(position, ivvc0216TpLivelloT);
    }

    /**Original name: C216-TP-LIVELLO-T<br>*/
    public char getTpLivelloT(int tpLivelloTIdx) {
        int position = Pos.c216TpLivelloT(tpLivelloTIdx - 1);
        return readChar(position);
    }

    public void setIvvc0216CodLivelloT(int ivvc0216CodLivelloTIdx, String ivvc0216CodLivelloT) {
        int position = Pos.c216CodLivelloT(ivvc0216CodLivelloTIdx - 1);
        writeString(position, ivvc0216CodLivelloT, Len.COD_LIVELLO_T);
    }

    /**Original name: C216-COD-LIVELLO-T<br>*/
    public String getCodLivelloT(int codLivelloTIdx) {
        int position = Pos.c216CodLivelloT(codLivelloTIdx - 1);
        return readString(position, Len.COD_LIVELLO_T);
    }

    public void setIvvc0216IdLivelloT(int ivvc0216IdLivelloTIdx, int ivvc0216IdLivelloT) {
        int position = Pos.c216IdLivelloT(ivvc0216IdLivelloTIdx - 1);
        writeInt(position, ivvc0216IdLivelloT, Len.Int.IVVC0216_ID_LIVELLO_T, SignType.NO_SIGN);
    }

    public void setIvvc0216IdLivelloTFormatted(int ivvc0216IdLivelloTIdx, String ivvc0216IdLivelloT) {
        int position = Pos.c216IdLivelloT(ivvc0216IdLivelloTIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ivvc0216IdLivelloT, Len.ID_LIVELLO_T), Len.ID_LIVELLO_T);
    }

    /**Original name: C216-ID-LIVELLO-T<br>*/
    public int getIdLivelloT(int idLivelloTIdx) {
        int position = Pos.c216IdLivelloT(idLivelloTIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_LIVELLO_T);
    }

    public String getIdLivelloTFormatted(int idLivelloTIdx) {
        int position = Pos.c216IdLivelloT(idLivelloTIdx - 1);
        return readFixedString(position, Len.ID_LIVELLO_T);
    }

    public void setIvvc0216DtDecorTrchT(int ivvc0216DtDecorTrchTIdx, String ivvc0216DtDecorTrchT) {
        int position = Pos.c216DtDecorTrchT(ivvc0216DtDecorTrchTIdx - 1);
        writeString(position, ivvc0216DtDecorTrchT, Len.DT_DECOR_TRCH_T);
    }

    /**Original name: C216-DT-DECOR-TRCH-T<br>*/
    public String getDtDecorTrchT(int dtDecorTrchTIdx) {
        int position = Pos.c216DtDecorTrchT(dtDecorTrchTIdx - 1);
        return readString(position, Len.DT_DECOR_TRCH_T);
    }

    public void setIvvc0216DtInizTariT(int ivvc0216DtInizTariTIdx, String ivvc0216DtInizTariT) {
        int position = Pos.c216DtInizTariT(ivvc0216DtInizTariTIdx - 1);
        writeString(position, ivvc0216DtInizTariT, Len.DT_INIZ_TARI_T);
    }

    /**Original name: C216-DT-INIZ-TARI-T<br>*/
    public String getDtInizTariT(int dtInizTariTIdx) {
        int position = Pos.c216DtInizTariT(dtInizTariTIdx - 1);
        return readString(position, Len.DT_INIZ_TARI_T);
    }

    public void setIvvc0216CodRgmFiscT(int ivvc0216CodRgmFiscTIdx, String ivvc0216CodRgmFiscT) {
        int position = Pos.c216CodRgmFiscT(ivvc0216CodRgmFiscTIdx - 1);
        writeString(position, ivvc0216CodRgmFiscT, Len.COD_RGM_FISC_T);
    }

    /**Original name: C216-COD-RGM-FISC-T<br>*/
    public String getCodRgmFiscT(int codRgmFiscTIdx) {
        int position = Pos.c216CodRgmFiscT(codRgmFiscTIdx - 1);
        return readString(position, Len.COD_RGM_FISC_T);
    }

    public void setIvvc0216NomeServizioT(int ivvc0216NomeServizioTIdx, String ivvc0216NomeServizioT) {
        int position = Pos.c216NomeServizioT(ivvc0216NomeServizioTIdx - 1);
        writeString(position, ivvc0216NomeServizioT, Len.NOME_SERVIZIO_T);
    }

    /**Original name: C216-NOME-SERVIZIO-T<br>*/
    public String getNomeServizioT(int nomeServizioTIdx) {
        int position = Pos.c216NomeServizioT(nomeServizioTIdx - 1);
        return readString(position, Len.NOME_SERVIZIO_T);
    }

    public void setTipoTrch(int tipoTrchIdx, short tipoTrch) {
        int position = Pos.c216TipoTrch(tipoTrchIdx - 1);
        writeShort(position, tipoTrch, Len.Int.TIPO_TRCH, SignType.NO_SIGN);
    }

    public void setTipoTrchFormatted(int tipoTrchIdx, String tipoTrch) {
        int position = Pos.c216TipoTrch(tipoTrchIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(tipoTrch, Len.TIPO_TRCH), Len.TIPO_TRCH);
    }

    /**Original name: C216-TIPO-TRCH<br>*/
    public short getTipoTrch(int tipoTrchIdx) {
        int position = Pos.c216TipoTrch(tipoTrchIdx - 1);
        return readNumDispUnsignedShort(position, Len.TIPO_TRCH);
    }

    public String getIvvc0216TipoTrchFormatted(int ivvc0216TipoTrchIdx) {
        int position = Pos.c216TipoTrch(ivvc0216TipoTrchIdx - 1);
        return readFixedString(position, Len.TIPO_TRCH);
    }

    public void setFlgItn(int flgItnIdx, short flgItn) {
        int position = Pos.c216FlgItn(flgItnIdx - 1);
        writeShort(position, flgItn, Len.Int.FLG_ITN, SignType.NO_SIGN);
    }

    public void setIvvc0216FlgItnFormatted(int ivvc0216FlgItnIdx, String ivvc0216FlgItn) {
        int position = Pos.c216FlgItn(ivvc0216FlgItnIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(ivvc0216FlgItn, Len.FLG_ITN), Len.FLG_ITN);
    }

    /**Original name: C216-FLG-ITN<br>*/
    public short getFlgItn(int flgItnIdx) {
        int position = Pos.c216FlgItn(flgItnIdx - 1);
        return readNumDispUnsignedShort(position, Len.FLG_ITN);
    }

    public String getIvvc0216FlgItnFormatted(int ivvc0216FlgItnIdx) {
        int position = Pos.c216FlgItn(ivvc0216FlgItnIdx - 1);
        return readFixedString(position, Len.FLG_ITN);
    }

    public void setIvvc0216FlgItnPos(int ivvc0216FlgItnPosIdx) {
        setIvvc0216FlgItnFormatted(ivvc0216FlgItnPosIdx, C216_FLG_ITN_POS);
    }

    public void setIvvc0216FlgItnNeg(int ivvc0216FlgItnNegIdx) {
        setIvvc0216FlgItnFormatted(ivvc0216FlgItnNegIdx, C216_FLG_ITN_NEG);
    }

    public void setEleVariabiliMaxT(int eleVariabiliMaxTIdx, short eleVariabiliMaxT) {
        int position = Pos.c216EleVariabiliMaxT(eleVariabiliMaxTIdx - 1);
        writeShortAsPacked(position, eleVariabiliMaxT, Len.Int.ELE_VARIABILI_MAX_T);
    }

    /**Original name: C216-ELE-VARIABILI-MAX-T<br>*/
    public short getEleVariabiliMaxT(int eleVariabiliMaxTIdx) {
        int position = Pos.c216EleVariabiliMaxT(eleVariabiliMaxTIdx - 1);
        return readPackedAsShort(position, Len.Int.ELE_VARIABILI_MAX_T);
    }

    public void setTabVariabiliTBytes(int tabVariabiliTIdx1, int tabVariabiliTIdx2, byte[] buffer) {
        setTabVariabiliTBytes(tabVariabiliTIdx1, tabVariabiliTIdx2, buffer, 1);
    }

    /**Original name: IVVC0216-TAB-VARIABILI-T<br>*/
    public byte[] getIvvc0216TabVariabiliTBytes(int ivvc0216TabVariabiliTIdx1, int ivvc0216TabVariabiliTIdx2) {
        byte[] buffer = new byte[Len.TAB_VARIABILI_T];
        return getIvvc0216TabVariabiliTBytes(ivvc0216TabVariabiliTIdx1, ivvc0216TabVariabiliTIdx2, buffer, 1);
    }

    public void setTabVariabiliTBytes(int tabVariabiliTIdx1, int tabVariabiliTIdx2, byte[] buffer, int offset) {
        int position = Pos.c216TabVariabiliT(tabVariabiliTIdx1 - 1, tabVariabiliTIdx2 - 1);
        setBytes(buffer, offset, Len.TAB_VARIABILI_T, position);
    }

    public byte[] getIvvc0216TabVariabiliTBytes(int ivvc0216TabVariabiliTIdx1, int ivvc0216TabVariabiliTIdx2, byte[] buffer, int offset) {
        int position = Pos.c216TabVariabiliT(ivvc0216TabVariabiliTIdx1 - 1, ivvc0216TabVariabiliTIdx2 - 1);
        getBytes(buffer, offset, Len.TAB_VARIABILI_T, position);
        return buffer;
    }

    public void initAreaVariabileTSpaces(int areaVariabileTIdx1, int areaVariabileTIdx2) {
        fill(Pos.c216AreaVariabileT((areaVariabileTIdx1 - 1), (areaVariabileTIdx2 - 1)), Len.AREA_VARIABILE_T, Types.SPACE_CHAR);
    }

    public void setCodVariabileT(int codVariabileTIdx1, int codVariabileTIdx2, String codVariabileT) {
        int position = Pos.c216CodVariabileT(codVariabileTIdx1 - 1, codVariabileTIdx2 - 1);
        writeString(position, codVariabileT, Len.COD_VARIABILE_T);
    }

    /**Original name: C216-COD-VARIABILE-T<br>*/
    public String getCodVariabileT(int codVariabileTIdx1, int codVariabileTIdx2) {
        int position = Pos.c216CodVariabileT(codVariabileTIdx1 - 1, codVariabileTIdx2 - 1);
        return readString(position, Len.COD_VARIABILE_T);
    }

    public String getCodVariabileTFormatted(int codVariabileTIdx1, int codVariabileTIdx2) {
        return Functions.padBlanks(getCodVariabileT(codVariabileTIdx1, codVariabileTIdx2), Len.COD_VARIABILE_T);
    }

    public void setTpDatoT(int tpDatoTIdx1, int tpDatoTIdx2, char tpDatoT) {
        int position = Pos.c216TpDatoT(tpDatoTIdx1 - 1, tpDatoTIdx2 - 1);
        writeChar(position, tpDatoT);
    }

    public void setTpDatoTFormatted(int tpDatoTIdx1, int tpDatoTIdx2, String tpDatoT) {
        setTpDatoT(tpDatoTIdx1, tpDatoTIdx2, Functions.charAt(tpDatoT, Types.CHAR_SIZE));
    }

    /**Original name: C216-TP-DATO-T<br>*/
    public char getTpDatoT(int tpDatoTIdx1, int tpDatoTIdx2) {
        int position = Pos.c216TpDatoT(tpDatoTIdx1 - 1, tpDatoTIdx2 - 1);
        return readChar(position);
    }

    public void setIvvc0216ValGenericoT(int ivvc0216ValGenericoTIdx1, int ivvc0216ValGenericoTIdx2, String ivvc0216ValGenericoT) {
        int position = Pos.c216ValGenericoT(ivvc0216ValGenericoTIdx1 - 1, ivvc0216ValGenericoTIdx2 - 1);
        writeString(position, ivvc0216ValGenericoT, Len.VAL_GENERICO_T);
    }

    /**Original name: IVVC0216-VAL-GENERICO-T<br>*/
    public String getIvvc0216ValGenericoT(int ivvc0216ValGenericoTIdx1, int ivvc0216ValGenericoTIdx2) {
        int position = Pos.c216ValGenericoT(ivvc0216ValGenericoTIdx1 - 1, ivvc0216ValGenericoTIdx2 - 1);
        return readString(position, Len.VAL_GENERICO_T);
    }

    public void setValImpT(int valImpTIdx1, int valImpTIdx2, AfDecimal valImpT) {
        int position = Pos.c216ValImpT(valImpTIdx1 - 1, valImpTIdx2 - 1);
        writeDecimal(position, valImpT.copy());
    }

    /**Original name: C216-VAL-IMP-T<br>*/
    public AfDecimal getValImpT(int valImpTIdx1, int valImpTIdx2) {
        int position = Pos.c216ValImpT(valImpTIdx1 - 1, valImpTIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_IMP_T, Len.Fract.VAL_IMP_T);
    }

    public void setValPercT(int valPercTIdx1, int valPercTIdx2, AfDecimal valPercT) {
        int position = Pos.c216ValPercT(valPercTIdx1 - 1, valPercTIdx2 - 1);
        writeDecimal(position, valPercT.copy(), SignType.NO_SIGN);
    }

    /**Original name: C216-VAL-PERC-T<br>*/
    public AfDecimal getValPercT(int valPercTIdx1, int valPercTIdx2) {
        int position = Pos.c216ValPercT(valPercTIdx1 - 1, valPercTIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_PERC_T, Len.Fract.VAL_PERC_T, SignType.NO_SIGN);
    }

    public void setValStrT(int valStrTIdx1, int valStrTIdx2, String valStrT) {
        int position = Pos.c216ValStrT(valStrTIdx1 - 1, valStrTIdx2 - 1);
        writeString(position, valStrT, Len.VAL_STR_T);
    }

    /**Original name: C216-VAL-STR-T<br>*/
    public String getValStrT(int valStrTIdx1, int valStrTIdx2) {
        int position = Pos.c216ValStrT(valStrTIdx1 - 1, valStrTIdx2 - 1);
        return readString(position, Len.VAL_STR_T);
    }

    public void setIvvc0216RestoTabValT(String ivvc0216RestoTabValT) {
        writeString(Pos.RESTO_TAB_VAL_T, ivvc0216RestoTabValT, Len.IVVC0216_RESTO_TAB_VAL_T);
    }

    /**Original name: IVVC0216-RESTO-TAB-VAL-T<br>*/
    public String getIvvc0216RestoTabValT() {
        return readString(Pos.RESTO_TAB_VAL_T, Len.IVVC0216_RESTO_TAB_VAL_T);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int C216_TAB_VAL_T = 1;
        public static final int C216_TAB_VAL_R_T = 1;
        public static final int FLR1 = C216_TAB_VAL_R_T;
        public static final int RESTO_TAB_VAL_T = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int c216TabLivelloT(int idx) {
            return C216_TAB_VAL_T + idx * Len.TAB_LIVELLO_T;
        }

        public static int c216IdGarT(int idx) {
            return c216TabLivelloT(idx);
        }

        public static int c216DatiLivelloT(int idx) {
            return c216IdGarT(idx) + Len.ID_GAR_T;
        }

        public static int c216CodTipoOpzioneT(int idx) {
            return c216DatiLivelloT(idx);
        }

        public static int c216TpLivelloT(int idx) {
            return c216CodTipoOpzioneT(idx) + Len.COD_TIPO_OPZIONE_T;
        }

        public static int c216CodLivelloT(int idx) {
            return c216TpLivelloT(idx) + Len.TP_LIVELLO_T;
        }

        public static int c216IdLivelloT(int idx) {
            return c216CodLivelloT(idx) + Len.COD_LIVELLO_T;
        }

        public static int c216DtDecorTrchT(int idx) {
            return c216IdLivelloT(idx) + Len.ID_LIVELLO_T;
        }

        public static int c216DtInizTariT(int idx) {
            return c216DtDecorTrchT(idx) + Len.DT_DECOR_TRCH_T;
        }

        public static int c216CodRgmFiscT(int idx) {
            return c216DtInizTariT(idx) + Len.DT_INIZ_TARI_T;
        }

        public static int c216NomeServizioT(int idx) {
            return c216CodRgmFiscT(idx) + Len.COD_RGM_FISC_T;
        }

        public static int c216TipoTrch(int idx) {
            return c216NomeServizioT(idx) + Len.NOME_SERVIZIO_T;
        }

        public static int c216FlgItn(int idx) {
            return c216TipoTrch(idx) + Len.TIPO_TRCH;
        }

        public static int c216AreaVariabiliT(int idx) {
            return c216FlgItn(idx) + Len.FLG_ITN;
        }

        public static int c216EleVariabiliMaxT(int idx) {
            return c216AreaVariabiliT(idx);
        }

        public static int c216TabVariabiliT(int idx1, int idx2) {
            return c216EleVariabiliMaxT(idx1) + Len.ELE_VARIABILI_MAX_T + idx2 * Len.TAB_VARIABILI_T;
        }

        public static int c216AreaVariabileT(int idx1, int idx2) {
            return c216TabVariabiliT(idx1, idx2);
        }

        public static int c216CodVariabileT(int idx1, int idx2) {
            return c216AreaVariabileT(idx1, idx2);
        }

        public static int c216TpDatoT(int idx1, int idx2) {
            return c216CodVariabileT(idx1, idx2) + Len.COD_VARIABILE_T;
        }

        public static int c216ValGenericoT(int idx1, int idx2) {
            return c216TpDatoT(idx1, idx2) + Len.TP_DATO_T;
        }

        public static int c216ValImpGenT(int idx1, int idx2) {
            return c216ValGenericoT(idx1, idx2);
        }

        public static int c216ValImpT(int idx1, int idx2) {
            return c216ValImpGenT(idx1, idx2);
        }

        public static int c216ValImpFillerT(int idx1, int idx2) {
            return c216ValImpT(idx1, idx2) + Len.VAL_IMP_T;
        }

        public static int c216ValPercGenT(int idx1, int idx2) {
            return c216ValGenericoT(idx1, idx2);
        }

        public static int c216ValPercT(int idx1, int idx2) {
            return c216ValPercGenT(idx1, idx2);
        }

        public static int c216ValPercFillerT(int idx1, int idx2) {
            return c216ValPercT(idx1, idx2) + Len.VAL_PERC_T;
        }

        public static int c216ValStrGenT(int idx1, int idx2) {
            return c216ValGenericoT(idx1, idx2);
        }

        public static int c216ValStrT(int idx1, int idx2) {
            return c216ValStrGenT(idx1, idx2);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GAR_T = 9;
        public static final int COD_TIPO_OPZIONE_T = 2;
        public static final int TP_LIVELLO_T = 1;
        public static final int COD_LIVELLO_T = 12;
        public static final int ID_LIVELLO_T = 9;
        public static final int DT_DECOR_TRCH_T = 8;
        public static final int DT_INIZ_TARI_T = 8;
        public static final int COD_RGM_FISC_T = 2;
        public static final int NOME_SERVIZIO_T = 8;
        public static final int TIPO_TRCH = 2;
        public static final int FLG_ITN = 1;
        public static final int ELE_VARIABILI_MAX_T = 3;
        public static final int COD_VARIABILE_T = 12;
        public static final int TP_DATO_T = 1;
        public static final int VAL_GENERICO_T = 60;
        public static final int AREA_VARIABILE_T = COD_VARIABILE_T + TP_DATO_T + VAL_GENERICO_T;
        public static final int TAB_VARIABILI_T = AREA_VARIABILE_T;
        public static final int AREA_VARIABILI_T = ELE_VARIABILI_MAX_T + C216TabValT.TAB_VARIABILI_T_MAXOCCURS * TAB_VARIABILI_T;
        public static final int DATI_LIVELLO_T = COD_TIPO_OPZIONE_T + TP_LIVELLO_T + COD_LIVELLO_T + ID_LIVELLO_T + DT_DECOR_TRCH_T + DT_INIZ_TARI_T + COD_RGM_FISC_T + NOME_SERVIZIO_T + TIPO_TRCH + FLG_ITN + AREA_VARIABILI_T;
        public static final int TAB_LIVELLO_T = ID_GAR_T + DATI_LIVELLO_T;
        public static final int VAL_IMP_T = 18;
        public static final int VAL_PERC_T = 14;
        public static final int FLR1 = 5540;
        public static final int C216_TAB_VAL_T = C216TabValT.TAB_LIVELLO_T_MAXOCCURS * TAB_LIVELLO_T;
        public static final int VAL_STR_T = 60;
        public static final int IVVC0216_RESTO_TAB_VAL_T = 4149460;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IVVC0216_ID_GAR_T = 9;
            public static final int IVVC0216_ID_LIVELLO_T = 9;
            public static final int TIPO_TRCH = 2;
            public static final int FLG_ITN = 1;
            public static final int ELE_VARIABILI_MAX_T = 4;
            public static final int VAL_IMP_T = 11;
            public static final int VAL_PERC_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP_T = 7;
            public static final int VAL_PERC_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
