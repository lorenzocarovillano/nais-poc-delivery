package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-VIS-DFZ<br>
 * Variable: WDFL-IMPB-VIS-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbVisDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbVisDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_VIS_DFZ;
    }

    public void setWdflImpbVisDfz(AfDecimal wdflImpbVisDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_VIS_DFZ, wdflImpbVisDfz.copy());
    }

    public void setWdflImpbVisDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_VIS_DFZ, Pos.WDFL_IMPB_VIS_DFZ);
    }

    /**Original name: WDFL-IMPB-VIS-DFZ<br>*/
    public AfDecimal getWdflImpbVisDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_VIS_DFZ, Len.Int.WDFL_IMPB_VIS_DFZ, Len.Fract.WDFL_IMPB_VIS_DFZ);
    }

    public byte[] getWdflImpbVisDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_VIS_DFZ, Pos.WDFL_IMPB_VIS_DFZ);
        return buffer;
    }

    public void setWdflImpbVisDfzNull(String wdflImpbVisDfzNull) {
        writeString(Pos.WDFL_IMPB_VIS_DFZ_NULL, wdflImpbVisDfzNull, Len.WDFL_IMPB_VIS_DFZ_NULL);
    }

    /**Original name: WDFL-IMPB-VIS-DFZ-NULL<br>*/
    public String getWdflImpbVisDfzNull() {
        return readString(Pos.WDFL_IMPB_VIS_DFZ_NULL, Len.WDFL_IMPB_VIS_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS_DFZ = 1;
        public static final int WDFL_IMPB_VIS_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_VIS_DFZ = 8;
        public static final int WDFL_IMPB_VIS_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_VIS_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
