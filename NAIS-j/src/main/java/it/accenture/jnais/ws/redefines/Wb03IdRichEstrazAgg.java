package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ID-RICH-ESTRAZ-AGG<br>
 * Variable: WB03-ID-RICH-ESTRAZ-AGG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ID_RICH_ESTRAZ_AGG;
    }

    public void setWb03IdRichEstrazAgg(int wb03IdRichEstrazAgg) {
        writeIntAsPacked(Pos.WB03_ID_RICH_ESTRAZ_AGG, wb03IdRichEstrazAgg, Len.Int.WB03_ID_RICH_ESTRAZ_AGG);
    }

    public void setWb03IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ID_RICH_ESTRAZ_AGG, Pos.WB03_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: WB03-ID-RICH-ESTRAZ-AGG<br>*/
    public int getWb03IdRichEstrazAgg() {
        return readPackedAsInt(Pos.WB03_ID_RICH_ESTRAZ_AGG, Len.Int.WB03_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getWb03IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ID_RICH_ESTRAZ_AGG, Pos.WB03_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setWb03IdRichEstrazAggNull(String wb03IdRichEstrazAggNull) {
        writeString(Pos.WB03_ID_RICH_ESTRAZ_AGG_NULL, wb03IdRichEstrazAggNull, Len.WB03_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: WB03-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getWb03IdRichEstrazAggNull() {
        return readString(Pos.WB03_ID_RICH_ESTRAZ_AGG_NULL, Len.WB03_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getWb03IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getWb03IdRichEstrazAggNull(), Len.WB03_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ID_RICH_ESTRAZ_AGG = 1;
        public static final int WB03_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ID_RICH_ESTRAZ_AGG = 5;
        public static final int WB03_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
