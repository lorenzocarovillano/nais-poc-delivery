package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CAMPO-ALFA<br>
 * Variable: CAMPO-ALFA from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CampoAlfa extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_STRINGA_ALFA_MAXOCCURS = 60;

    //==== CONSTRUCTORS ====
    public CampoAlfa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CAMPO_ALFA;
    }

    public void setCampoAlfa(String campoAlfa) {
        writeString(Pos.CAMPO_ALFA, campoAlfa, Len.CAMPO_ALFA);
    }

    /**Original name: CAMPO-ALFA<br>*/
    public String getCampoAlfa() {
        return readString(Pos.CAMPO_ALFA, Len.CAMPO_ALFA);
    }

    public String getCampoAlfaFormatted() {
        return Functions.padBlanks(getCampoAlfa(), Len.CAMPO_ALFA);
    }

    public void setEleStringaAlfa(int eleStringaAlfaIdx, char eleStringaAlfa) {
        int position = Pos.eleStringaAlfa(eleStringaAlfaIdx - 1);
        writeChar(position, eleStringaAlfa);
    }

    /**Original name: ELE-STRINGA-ALFA<br>*/
    public char getEleStringaAlfa(int eleStringaAlfaIdx) {
        int position = Pos.eleStringaAlfa(eleStringaAlfaIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CAMPO_ALFA = 1;
        public static final int ARRAY_STRINGA_ALFA = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int eleStringaAlfa(int idx) {
            return ARRAY_STRINGA_ALFA + idx * Len.ELE_STRINGA_ALFA;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRINGA_ALFA = 1;
        public static final int CAMPO_ALFA = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
