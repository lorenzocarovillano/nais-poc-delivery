package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CLOSE-FILESQS6<br>
 * Variable: FLAG-CLOSE-FILESQS6 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCloseFilesqs6 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCloseFilesqs6(char flagCloseFilesqs6) {
        this.value = flagCloseFilesqs6;
    }

    public char getFlagCloseFilesqs6() {
        return this.value;
    }

    public boolean isCloseFilesqs6Si() {
        return value == SI;
    }

    public void setCloseFilesqs6Si() {
        value = SI;
    }

    public void setCloseFilesqs6No() {
        value = NO;
    }
}
