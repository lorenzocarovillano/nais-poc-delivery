package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0001-ESITO<br>
 * Variable: IDSV0001-ESITO from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001Esito {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.ESITO);
    public static final String OK = "OK";
    public static final String KO = "KO";

    //==== METHODS ====
    public void setEsito(String esito) {
        this.value = Functions.subString(esito, Len.ESITO);
    }

    public String getEsito() {
        return this.value;
    }

    public boolean isIdsv0001EsitoOk() {
        return value.equals(OK);
    }

    public void setIdsv0001EsitoOk() {
        value = OK;
    }

    public boolean isIdsv0001EsitoKo() {
        return value.equals(KO);
    }

    public void setIdsv0001EsitoKo() {
        value = KO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ESITO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
