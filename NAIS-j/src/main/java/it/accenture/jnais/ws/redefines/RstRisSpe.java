package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-SPE<br>
 * Variable: RST-RIS-SPE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisSpe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisSpe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_SPE;
    }

    public void setRstRisSpe(AfDecimal rstRisSpe) {
        writeDecimalAsPacked(Pos.RST_RIS_SPE, rstRisSpe.copy());
    }

    public void setRstRisSpeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_SPE, Pos.RST_RIS_SPE);
    }

    /**Original name: RST-RIS-SPE<br>*/
    public AfDecimal getRstRisSpe() {
        return readPackedAsDecimal(Pos.RST_RIS_SPE, Len.Int.RST_RIS_SPE, Len.Fract.RST_RIS_SPE);
    }

    public byte[] getRstRisSpeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_SPE, Pos.RST_RIS_SPE);
        return buffer;
    }

    public void setRstRisSpeNull(String rstRisSpeNull) {
        writeString(Pos.RST_RIS_SPE_NULL, rstRisSpeNull, Len.RST_RIS_SPE_NULL);
    }

    /**Original name: RST-RIS-SPE-NULL<br>*/
    public String getRstRisSpeNull() {
        return readString(Pos.RST_RIS_SPE_NULL, Len.RST_RIS_SPE_NULL);
    }

    public String getRstRisSpeNullFormatted() {
        return Functions.padBlanks(getRstRisSpeNull(), Len.RST_RIS_SPE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_SPE = 1;
        public static final int RST_RIS_SPE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_SPE = 8;
        public static final int RST_RIS_SPE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_SPE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_SPE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
