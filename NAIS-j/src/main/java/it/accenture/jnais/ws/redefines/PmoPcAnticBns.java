package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-PC-ANTIC-BNS<br>
 * Variable: PMO-PC-ANTIC-BNS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoPcAnticBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoPcAnticBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_PC_ANTIC_BNS;
    }

    public void setPmoPcAnticBns(AfDecimal pmoPcAnticBns) {
        writeDecimalAsPacked(Pos.PMO_PC_ANTIC_BNS, pmoPcAnticBns.copy());
    }

    public void setPmoPcAnticBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_PC_ANTIC_BNS, Pos.PMO_PC_ANTIC_BNS);
    }

    /**Original name: PMO-PC-ANTIC-BNS<br>*/
    public AfDecimal getPmoPcAnticBns() {
        return readPackedAsDecimal(Pos.PMO_PC_ANTIC_BNS, Len.Int.PMO_PC_ANTIC_BNS, Len.Fract.PMO_PC_ANTIC_BNS);
    }

    public byte[] getPmoPcAnticBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_PC_ANTIC_BNS, Pos.PMO_PC_ANTIC_BNS);
        return buffer;
    }

    public void initPmoPcAnticBnsHighValues() {
        fill(Pos.PMO_PC_ANTIC_BNS, Len.PMO_PC_ANTIC_BNS, Types.HIGH_CHAR_VAL);
    }

    public void setPmoPcAnticBnsNull(String pmoPcAnticBnsNull) {
        writeString(Pos.PMO_PC_ANTIC_BNS_NULL, pmoPcAnticBnsNull, Len.PMO_PC_ANTIC_BNS_NULL);
    }

    /**Original name: PMO-PC-ANTIC-BNS-NULL<br>*/
    public String getPmoPcAnticBnsNull() {
        return readString(Pos.PMO_PC_ANTIC_BNS_NULL, Len.PMO_PC_ANTIC_BNS_NULL);
    }

    public String getPmoPcAnticBnsNullFormatted() {
        return Functions.padBlanks(getPmoPcAnticBnsNull(), Len.PMO_PC_ANTIC_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_PC_ANTIC_BNS = 1;
        public static final int PMO_PC_ANTIC_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_PC_ANTIC_BNS = 4;
        public static final int PMO_PC_ANTIC_BNS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_PC_ANTIC_BNS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_PC_ANTIC_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
