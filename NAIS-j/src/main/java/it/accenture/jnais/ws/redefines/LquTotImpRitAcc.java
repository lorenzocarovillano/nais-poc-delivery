package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-RIT-ACC<br>
 * Variable: LQU-TOT-IMP-RIT-ACC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpRitAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpRitAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_RIT_ACC;
    }

    public void setLquTotImpRitAcc(AfDecimal lquTotImpRitAcc) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_RIT_ACC, lquTotImpRitAcc.copy());
    }

    public void setLquTotImpRitAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_RIT_ACC, Pos.LQU_TOT_IMP_RIT_ACC);
    }

    /**Original name: LQU-TOT-IMP-RIT-ACC<br>*/
    public AfDecimal getLquTotImpRitAcc() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_RIT_ACC, Len.Int.LQU_TOT_IMP_RIT_ACC, Len.Fract.LQU_TOT_IMP_RIT_ACC);
    }

    public byte[] getLquTotImpRitAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_RIT_ACC, Pos.LQU_TOT_IMP_RIT_ACC);
        return buffer;
    }

    public void setLquTotImpRitAccNull(String lquTotImpRitAccNull) {
        writeString(Pos.LQU_TOT_IMP_RIT_ACC_NULL, lquTotImpRitAccNull, Len.LQU_TOT_IMP_RIT_ACC_NULL);
    }

    /**Original name: LQU-TOT-IMP-RIT-ACC-NULL<br>*/
    public String getLquTotImpRitAccNull() {
        return readString(Pos.LQU_TOT_IMP_RIT_ACC_NULL, Len.LQU_TOT_IMP_RIT_ACC_NULL);
    }

    public String getLquTotImpRitAccNullFormatted() {
        return Functions.padBlanks(getLquTotImpRitAccNull(), Len.LQU_TOT_IMP_RIT_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIT_ACC = 1;
        public static final int LQU_TOT_IMP_RIT_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIT_ACC = 8;
        public static final int LQU_TOT_IMP_RIT_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIT_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIT_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
