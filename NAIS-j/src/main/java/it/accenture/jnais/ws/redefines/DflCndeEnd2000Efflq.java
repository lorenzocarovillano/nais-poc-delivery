package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNDE-END2000-EFFLQ<br>
 * Variable: DFL-CNDE-END2000-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCndeEnd2000Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCndeEnd2000Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNDE_END2000_EFFLQ;
    }

    public void setDflCndeEnd2000Efflq(AfDecimal dflCndeEnd2000Efflq) {
        writeDecimalAsPacked(Pos.DFL_CNDE_END2000_EFFLQ, dflCndeEnd2000Efflq.copy());
    }

    public void setDflCndeEnd2000EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNDE_END2000_EFFLQ, Pos.DFL_CNDE_END2000_EFFLQ);
    }

    /**Original name: DFL-CNDE-END2000-EFFLQ<br>*/
    public AfDecimal getDflCndeEnd2000Efflq() {
        return readPackedAsDecimal(Pos.DFL_CNDE_END2000_EFFLQ, Len.Int.DFL_CNDE_END2000_EFFLQ, Len.Fract.DFL_CNDE_END2000_EFFLQ);
    }

    public byte[] getDflCndeEnd2000EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNDE_END2000_EFFLQ, Pos.DFL_CNDE_END2000_EFFLQ);
        return buffer;
    }

    public void setDflCndeEnd2000EfflqNull(String dflCndeEnd2000EfflqNull) {
        writeString(Pos.DFL_CNDE_END2000_EFFLQ_NULL, dflCndeEnd2000EfflqNull, Len.DFL_CNDE_END2000_EFFLQ_NULL);
    }

    /**Original name: DFL-CNDE-END2000-EFFLQ-NULL<br>*/
    public String getDflCndeEnd2000EfflqNull() {
        return readString(Pos.DFL_CNDE_END2000_EFFLQ_NULL, Len.DFL_CNDE_END2000_EFFLQ_NULL);
    }

    public String getDflCndeEnd2000EfflqNullFormatted() {
        return Functions.padBlanks(getDflCndeEnd2000EfflqNull(), Len.DFL_CNDE_END2000_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_END2000_EFFLQ = 1;
        public static final int DFL_CNDE_END2000_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_END2000_EFFLQ = 8;
        public static final int DFL_CNDE_END2000_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_END2000_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_END2000_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
