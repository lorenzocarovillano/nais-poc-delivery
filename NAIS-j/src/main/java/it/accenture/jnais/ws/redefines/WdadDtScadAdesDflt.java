package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDAD-DT-SCAD-ADES-DFLT<br>
 * Variable: WDAD-DT-SCAD-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadDtScadAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadDtScadAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_DT_SCAD_ADES_DFLT;
    }

    public void setWdadDtScadAdesDflt(int wdadDtScadAdesDflt) {
        writeIntAsPacked(Pos.WDAD_DT_SCAD_ADES_DFLT, wdadDtScadAdesDflt, Len.Int.WDAD_DT_SCAD_ADES_DFLT);
    }

    public void setWdadDtScadAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_DT_SCAD_ADES_DFLT, Pos.WDAD_DT_SCAD_ADES_DFLT);
    }

    /**Original name: WDAD-DT-SCAD-ADES-DFLT<br>*/
    public int getWdadDtScadAdesDflt() {
        return readPackedAsInt(Pos.WDAD_DT_SCAD_ADES_DFLT, Len.Int.WDAD_DT_SCAD_ADES_DFLT);
    }

    public byte[] getWdadDtScadAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_DT_SCAD_ADES_DFLT, Pos.WDAD_DT_SCAD_ADES_DFLT);
        return buffer;
    }

    public void setWdadDtScadAdesDfltNull(String wdadDtScadAdesDfltNull) {
        writeString(Pos.WDAD_DT_SCAD_ADES_DFLT_NULL, wdadDtScadAdesDfltNull, Len.WDAD_DT_SCAD_ADES_DFLT_NULL);
    }

    /**Original name: WDAD-DT-SCAD-ADES-DFLT-NULL<br>*/
    public String getWdadDtScadAdesDfltNull() {
        return readString(Pos.WDAD_DT_SCAD_ADES_DFLT_NULL, Len.WDAD_DT_SCAD_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_DT_SCAD_ADES_DFLT = 1;
        public static final int WDAD_DT_SCAD_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_DT_SCAD_ADES_DFLT = 5;
        public static final int WDAD_DT_SCAD_ADES_DFLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_DT_SCAD_ADES_DFLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
