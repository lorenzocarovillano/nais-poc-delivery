package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-PC-C-SUBRSH-MARSOL<br>
 * Variable: PCO-PC-C-SUBRSH-MARSOL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoPcCSubrshMarsol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoPcCSubrshMarsol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_PC_C_SUBRSH_MARSOL;
    }

    public void setPcoPcCSubrshMarsol(AfDecimal pcoPcCSubrshMarsol) {
        writeDecimalAsPacked(Pos.PCO_PC_C_SUBRSH_MARSOL, pcoPcCSubrshMarsol.copy());
    }

    public void setPcoPcCSubrshMarsolFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_PC_C_SUBRSH_MARSOL, Pos.PCO_PC_C_SUBRSH_MARSOL);
    }

    /**Original name: PCO-PC-C-SUBRSH-MARSOL<br>*/
    public AfDecimal getPcoPcCSubrshMarsol() {
        return readPackedAsDecimal(Pos.PCO_PC_C_SUBRSH_MARSOL, Len.Int.PCO_PC_C_SUBRSH_MARSOL, Len.Fract.PCO_PC_C_SUBRSH_MARSOL);
    }

    public byte[] getPcoPcCSubrshMarsolAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_PC_C_SUBRSH_MARSOL, Pos.PCO_PC_C_SUBRSH_MARSOL);
        return buffer;
    }

    public void initPcoPcCSubrshMarsolHighValues() {
        fill(Pos.PCO_PC_C_SUBRSH_MARSOL, Len.PCO_PC_C_SUBRSH_MARSOL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoPcCSubrshMarsolNull(String pcoPcCSubrshMarsolNull) {
        writeString(Pos.PCO_PC_C_SUBRSH_MARSOL_NULL, pcoPcCSubrshMarsolNull, Len.PCO_PC_C_SUBRSH_MARSOL_NULL);
    }

    /**Original name: PCO-PC-C-SUBRSH-MARSOL-NULL<br>*/
    public String getPcoPcCSubrshMarsolNull() {
        return readString(Pos.PCO_PC_C_SUBRSH_MARSOL_NULL, Len.PCO_PC_C_SUBRSH_MARSOL_NULL);
    }

    public String getPcoPcCSubrshMarsolNullFormatted() {
        return Functions.padBlanks(getPcoPcCSubrshMarsolNull(), Len.PCO_PC_C_SUBRSH_MARSOL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_PC_C_SUBRSH_MARSOL = 1;
        public static final int PCO_PC_C_SUBRSH_MARSOL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_PC_C_SUBRSH_MARSOL = 4;
        public static final int PCO_PC_C_SUBRSH_MARSOL_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_PC_C_SUBRSH_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_PC_C_SUBRSH_MARSOL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
