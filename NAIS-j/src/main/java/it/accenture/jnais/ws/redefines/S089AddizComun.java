package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-ADDIZ-COMUN<br>
 * Variable: S089-ADDIZ-COMUN from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089AddizComun extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089AddizComun() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_ADDIZ_COMUN;
    }

    public void setWlquAddizComun(AfDecimal wlquAddizComun) {
        writeDecimalAsPacked(Pos.S089_ADDIZ_COMUN, wlquAddizComun.copy());
    }

    public void setWlquAddizComunFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_ADDIZ_COMUN, Pos.S089_ADDIZ_COMUN);
    }

    /**Original name: WLQU-ADDIZ-COMUN<br>*/
    public AfDecimal getWlquAddizComun() {
        return readPackedAsDecimal(Pos.S089_ADDIZ_COMUN, Len.Int.WLQU_ADDIZ_COMUN, Len.Fract.WLQU_ADDIZ_COMUN);
    }

    public byte[] getWlquAddizComunAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_ADDIZ_COMUN, Pos.S089_ADDIZ_COMUN);
        return buffer;
    }

    public void initWlquAddizComunSpaces() {
        fill(Pos.S089_ADDIZ_COMUN, Len.S089_ADDIZ_COMUN, Types.SPACE_CHAR);
    }

    public void setWlquAddizComunNull(String wlquAddizComunNull) {
        writeString(Pos.S089_ADDIZ_COMUN_NULL, wlquAddizComunNull, Len.WLQU_ADDIZ_COMUN_NULL);
    }

    /**Original name: WLQU-ADDIZ-COMUN-NULL<br>*/
    public String getWlquAddizComunNull() {
        return readString(Pos.S089_ADDIZ_COMUN_NULL, Len.WLQU_ADDIZ_COMUN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_ADDIZ_COMUN = 1;
        public static final int S089_ADDIZ_COMUN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_ADDIZ_COMUN = 8;
        public static final int WLQU_ADDIZ_COMUN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_ADDIZ_COMUN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_ADDIZ_COMUN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
