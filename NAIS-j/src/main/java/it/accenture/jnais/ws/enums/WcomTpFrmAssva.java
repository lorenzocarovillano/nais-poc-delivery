package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-TP-FRM-ASSVA<br>
 * Variable: WCOM-TP-FRM-ASSVA from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomTpFrmAssva {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WCOM_TP_FRM_ASSVA);
    public static final String COLLETTIVA = "CO";
    public static final String INDIVIDUALE = "IN";

    //==== METHODS ====
    public void setWcomTpFrmAssva(String wcomTpFrmAssva) {
        this.value = Functions.subString(wcomTpFrmAssva, Len.WCOM_TP_FRM_ASSVA);
    }

    public String getWcomTpFrmAssva() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCOM_TP_FRM_ASSVA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
