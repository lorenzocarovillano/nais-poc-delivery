package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-TS-STAB-LIMITATA<br>
 * Variable: GRZ-TS-STAB-LIMITATA from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzTsStabLimitata extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzTsStabLimitata() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_TS_STAB_LIMITATA;
    }

    public void setGrzTsStabLimitata(AfDecimal grzTsStabLimitata) {
        writeDecimalAsPacked(Pos.GRZ_TS_STAB_LIMITATA, grzTsStabLimitata.copy());
    }

    public void setGrzTsStabLimitataFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_TS_STAB_LIMITATA, Pos.GRZ_TS_STAB_LIMITATA);
    }

    /**Original name: GRZ-TS-STAB-LIMITATA<br>*/
    public AfDecimal getGrzTsStabLimitata() {
        return readPackedAsDecimal(Pos.GRZ_TS_STAB_LIMITATA, Len.Int.GRZ_TS_STAB_LIMITATA, Len.Fract.GRZ_TS_STAB_LIMITATA);
    }

    public byte[] getGrzTsStabLimitataAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_TS_STAB_LIMITATA, Pos.GRZ_TS_STAB_LIMITATA);
        return buffer;
    }

    public void setGrzTsStabLimitataNull(String grzTsStabLimitataNull) {
        writeString(Pos.GRZ_TS_STAB_LIMITATA_NULL, grzTsStabLimitataNull, Len.GRZ_TS_STAB_LIMITATA_NULL);
    }

    /**Original name: GRZ-TS-STAB-LIMITATA-NULL<br>*/
    public String getGrzTsStabLimitataNull() {
        return readString(Pos.GRZ_TS_STAB_LIMITATA_NULL, Len.GRZ_TS_STAB_LIMITATA_NULL);
    }

    public String getGrzTsStabLimitataNullFormatted() {
        return Functions.padBlanks(getGrzTsStabLimitataNull(), Len.GRZ_TS_STAB_LIMITATA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_TS_STAB_LIMITATA = 1;
        public static final int GRZ_TS_STAB_LIMITATA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_TS_STAB_LIMITATA = 8;
        public static final int GRZ_TS_STAB_LIMITATA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_TS_STAB_LIMITATA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int GRZ_TS_STAB_LIMITATA = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
