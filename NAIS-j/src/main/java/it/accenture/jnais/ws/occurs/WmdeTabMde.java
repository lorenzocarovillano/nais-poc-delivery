package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.Lccvmde1;

/**Original name: WMDE-TAB-MDE<br>
 * Variables: WMDE-TAB-MDE from program LCCS0005<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WmdeTabMde {

    //==== PROPERTIES ====
    //Original name: LCCVMDE1
    private Lccvmde1 lccvmde1 = new Lccvmde1();

    //==== METHODS ====
    public Lccvmde1 getLccvmde1() {
        return lccvmde1;
    }
}
