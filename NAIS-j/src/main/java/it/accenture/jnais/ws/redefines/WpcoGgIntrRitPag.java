package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-GG-INTR-RIT-PAG<br>
 * Variable: WPCO-GG-INTR-RIT-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoGgIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoGgIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_GG_INTR_RIT_PAG;
    }

    public void setWpcoGgIntrRitPag(int wpcoGgIntrRitPag) {
        writeIntAsPacked(Pos.WPCO_GG_INTR_RIT_PAG, wpcoGgIntrRitPag, Len.Int.WPCO_GG_INTR_RIT_PAG);
    }

    public void setDpcoGgIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_GG_INTR_RIT_PAG, Pos.WPCO_GG_INTR_RIT_PAG);
    }

    /**Original name: WPCO-GG-INTR-RIT-PAG<br>*/
    public int getWpcoGgIntrRitPag() {
        return readPackedAsInt(Pos.WPCO_GG_INTR_RIT_PAG, Len.Int.WPCO_GG_INTR_RIT_PAG);
    }

    public byte[] getWpcoGgIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_GG_INTR_RIT_PAG, Pos.WPCO_GG_INTR_RIT_PAG);
        return buffer;
    }

    public void setWpcoGgIntrRitPagNull(String wpcoGgIntrRitPagNull) {
        writeString(Pos.WPCO_GG_INTR_RIT_PAG_NULL, wpcoGgIntrRitPagNull, Len.WPCO_GG_INTR_RIT_PAG_NULL);
    }

    /**Original name: WPCO-GG-INTR-RIT-PAG-NULL<br>*/
    public String getWpcoGgIntrRitPagNull() {
        return readString(Pos.WPCO_GG_INTR_RIT_PAG_NULL, Len.WPCO_GG_INTR_RIT_PAG_NULL);
    }

    public String getWpcoGgIntrRitPagNullFormatted() {
        return Functions.padBlanks(getWpcoGgIntrRitPagNull(), Len.WPCO_GG_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_GG_INTR_RIT_PAG = 1;
        public static final int WPCO_GG_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_GG_INTR_RIT_PAG = 3;
        public static final int WPCO_GG_INTR_RIT_PAG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_GG_INTR_RIT_PAG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
