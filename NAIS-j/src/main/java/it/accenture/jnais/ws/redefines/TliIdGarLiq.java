package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-ID-GAR-LIQ<br>
 * Variable: TLI-ID-GAR-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliIdGarLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliIdGarLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_ID_GAR_LIQ;
    }

    public void setTliIdGarLiq(int tliIdGarLiq) {
        writeIntAsPacked(Pos.TLI_ID_GAR_LIQ, tliIdGarLiq, Len.Int.TLI_ID_GAR_LIQ);
    }

    public void setTliIdGarLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_ID_GAR_LIQ, Pos.TLI_ID_GAR_LIQ);
    }

    /**Original name: TLI-ID-GAR-LIQ<br>*/
    public int getTliIdGarLiq() {
        return readPackedAsInt(Pos.TLI_ID_GAR_LIQ, Len.Int.TLI_ID_GAR_LIQ);
    }

    public byte[] getTliIdGarLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_ID_GAR_LIQ, Pos.TLI_ID_GAR_LIQ);
        return buffer;
    }

    public void setTliIdGarLiqNull(String tliIdGarLiqNull) {
        writeString(Pos.TLI_ID_GAR_LIQ_NULL, tliIdGarLiqNull, Len.TLI_ID_GAR_LIQ_NULL);
    }

    /**Original name: TLI-ID-GAR-LIQ-NULL<br>*/
    public String getTliIdGarLiqNull() {
        return readString(Pos.TLI_ID_GAR_LIQ_NULL, Len.TLI_ID_GAR_LIQ_NULL);
    }

    public String getTliIdGarLiqNullFormatted() {
        return Functions.padBlanks(getTliIdGarLiqNull(), Len.TLI_ID_GAR_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_ID_GAR_LIQ = 1;
        public static final int TLI_ID_GAR_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_ID_GAR_LIQ = 5;
        public static final int TLI_ID_GAR_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_ID_GAR_LIQ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
