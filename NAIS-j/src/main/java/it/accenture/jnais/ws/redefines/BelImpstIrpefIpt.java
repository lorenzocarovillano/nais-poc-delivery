package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-IRPEF-IPT<br>
 * Variable: BEL-IMPST-IRPEF-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstIrpefIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstIrpefIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_IRPEF_IPT;
    }

    public void setBelImpstIrpefIpt(AfDecimal belImpstIrpefIpt) {
        writeDecimalAsPacked(Pos.BEL_IMPST_IRPEF_IPT, belImpstIrpefIpt.copy());
    }

    public void setBelImpstIrpefIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_IRPEF_IPT, Pos.BEL_IMPST_IRPEF_IPT);
    }

    /**Original name: BEL-IMPST-IRPEF-IPT<br>*/
    public AfDecimal getBelImpstIrpefIpt() {
        return readPackedAsDecimal(Pos.BEL_IMPST_IRPEF_IPT, Len.Int.BEL_IMPST_IRPEF_IPT, Len.Fract.BEL_IMPST_IRPEF_IPT);
    }

    public byte[] getBelImpstIrpefIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_IRPEF_IPT, Pos.BEL_IMPST_IRPEF_IPT);
        return buffer;
    }

    public void setBelImpstIrpefIptNull(String belImpstIrpefIptNull) {
        writeString(Pos.BEL_IMPST_IRPEF_IPT_NULL, belImpstIrpefIptNull, Len.BEL_IMPST_IRPEF_IPT_NULL);
    }

    /**Original name: BEL-IMPST-IRPEF-IPT-NULL<br>*/
    public String getBelImpstIrpefIptNull() {
        return readString(Pos.BEL_IMPST_IRPEF_IPT_NULL, Len.BEL_IMPST_IRPEF_IPT_NULL);
    }

    public String getBelImpstIrpefIptNullFormatted() {
        return Functions.padBlanks(getBelImpstIrpefIptNull(), Len.BEL_IMPST_IRPEF_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_IRPEF_IPT = 1;
        public static final int BEL_IMPST_IRPEF_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_IRPEF_IPT = 8;
        public static final int BEL_IMPST_IRPEF_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_IRPEF_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_IRPEF_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
