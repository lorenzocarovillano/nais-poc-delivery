package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DUR-MM<br>
 * Variable: WTGA-DUR-MM from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DUR_MM;
    }

    public void setWtgaDurMm(int wtgaDurMm) {
        writeIntAsPacked(Pos.WTGA_DUR_MM, wtgaDurMm, Len.Int.WTGA_DUR_MM);
    }

    public void setWtgaDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DUR_MM, Pos.WTGA_DUR_MM);
    }

    /**Original name: WTGA-DUR-MM<br>*/
    public int getWtgaDurMm() {
        return readPackedAsInt(Pos.WTGA_DUR_MM, Len.Int.WTGA_DUR_MM);
    }

    public byte[] getWtgaDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DUR_MM, Pos.WTGA_DUR_MM);
        return buffer;
    }

    public void initWtgaDurMmSpaces() {
        fill(Pos.WTGA_DUR_MM, Len.WTGA_DUR_MM, Types.SPACE_CHAR);
    }

    public void setWtgaDurMmNull(String wtgaDurMmNull) {
        writeString(Pos.WTGA_DUR_MM_NULL, wtgaDurMmNull, Len.WTGA_DUR_MM_NULL);
    }

    /**Original name: WTGA-DUR-MM-NULL<br>*/
    public String getWtgaDurMmNull() {
        return readString(Pos.WTGA_DUR_MM_NULL, Len.WTGA_DUR_MM_NULL);
    }

    public String getWtgaDurMmNullFormatted() {
        return Functions.padBlanks(getWtgaDurMmNull(), Len.WTGA_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_MM = 1;
        public static final int WTGA_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_MM = 3;
        public static final int WTGA_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
