package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-PC-PROV-INC-DFLT<br>
 * Variable: WDAD-PC-PROV-INC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadPcProvIncDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadPcProvIncDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_PC_PROV_INC_DFLT;
    }

    public void setWdadPcProvIncDflt(AfDecimal wdadPcProvIncDflt) {
        writeDecimalAsPacked(Pos.WDAD_PC_PROV_INC_DFLT, wdadPcProvIncDflt.copy());
    }

    public void setWdadPcProvIncDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_PC_PROV_INC_DFLT, Pos.WDAD_PC_PROV_INC_DFLT);
    }

    /**Original name: WDAD-PC-PROV-INC-DFLT<br>*/
    public AfDecimal getWdadPcProvIncDflt() {
        return readPackedAsDecimal(Pos.WDAD_PC_PROV_INC_DFLT, Len.Int.WDAD_PC_PROV_INC_DFLT, Len.Fract.WDAD_PC_PROV_INC_DFLT);
    }

    public byte[] getWdadPcProvIncDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_PC_PROV_INC_DFLT, Pos.WDAD_PC_PROV_INC_DFLT);
        return buffer;
    }

    public void setWdadPcProvIncDfltNull(String wdadPcProvIncDfltNull) {
        writeString(Pos.WDAD_PC_PROV_INC_DFLT_NULL, wdadPcProvIncDfltNull, Len.WDAD_PC_PROV_INC_DFLT_NULL);
    }

    /**Original name: WDAD-PC-PROV-INC-DFLT-NULL<br>*/
    public String getWdadPcProvIncDfltNull() {
        return readString(Pos.WDAD_PC_PROV_INC_DFLT_NULL, Len.WDAD_PC_PROV_INC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_PC_PROV_INC_DFLT = 1;
        public static final int WDAD_PC_PROV_INC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_PC_PROV_INC_DFLT = 4;
        public static final int WDAD_PC_PROV_INC_DFLT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_PC_PROV_INC_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_PC_PROV_INC_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
