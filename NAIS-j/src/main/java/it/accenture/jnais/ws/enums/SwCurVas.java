package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-CUR-VAS<br>
 * Variable: SW-CUR-VAS from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwCurVas {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char INIT_CUR_VAS = 'S';
    public static final char FINE_CUR_VAS = 'N';

    //==== METHODS ====
    public void setSwCurVas(char swCurVas) {
        this.value = swCurVas;
    }

    public char getSwCurVas() {
        return this.value;
    }

    public void setInitCurVas() {
        value = INIT_CUR_VAS;
    }

    public boolean isFineCurVas() {
        return value == FINE_CUR_VAS;
    }

    public void setFineCurVas() {
        value = FINE_CUR_VAS;
    }
}
