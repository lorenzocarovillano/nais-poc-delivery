package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-DA-RIMB-EFF<br>
 * Variable: WDFL-IMPST-DA-RIMB-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstDaRimbEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstDaRimbEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_DA_RIMB_EFF;
    }

    public void setWdflImpstDaRimbEff(AfDecimal wdflImpstDaRimbEff) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_DA_RIMB_EFF, wdflImpstDaRimbEff.copy());
    }

    public void setWdflImpstDaRimbEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_DA_RIMB_EFF, Pos.WDFL_IMPST_DA_RIMB_EFF);
    }

    /**Original name: WDFL-IMPST-DA-RIMB-EFF<br>*/
    public AfDecimal getWdflImpstDaRimbEff() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_DA_RIMB_EFF, Len.Int.WDFL_IMPST_DA_RIMB_EFF, Len.Fract.WDFL_IMPST_DA_RIMB_EFF);
    }

    public byte[] getWdflImpstDaRimbEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_DA_RIMB_EFF, Pos.WDFL_IMPST_DA_RIMB_EFF);
        return buffer;
    }

    public void setWdflImpstDaRimbEffNull(String wdflImpstDaRimbEffNull) {
        writeString(Pos.WDFL_IMPST_DA_RIMB_EFF_NULL, wdflImpstDaRimbEffNull, Len.WDFL_IMPST_DA_RIMB_EFF_NULL);
    }

    /**Original name: WDFL-IMPST-DA-RIMB-EFF-NULL<br>*/
    public String getWdflImpstDaRimbEffNull() {
        return readString(Pos.WDFL_IMPST_DA_RIMB_EFF_NULL, Len.WDFL_IMPST_DA_RIMB_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_DA_RIMB_EFF = 1;
        public static final int WDFL_IMPST_DA_RIMB_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_DA_RIMB_EFF = 8;
        public static final int WDFL_IMPST_DA_RIMB_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_DA_RIMB_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_DA_RIMB_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
