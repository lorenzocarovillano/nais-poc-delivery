package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: C214-TAB-INFO<br>
 * Variables: C214-TAB-INFO from copybook IVVC0213<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class C214TabInfo {

    //==== PROPERTIES ====
    //Original name: C214-TAB-ALIAS
    private String tabAlias = DefaultValues.stringVal(Len.TAB_ALIAS);
    //Original name: C214-NUM-OCCORRENZE
    private int numOccorrenze = DefaultValues.INT_VAL;
    //Original name: C214-NUM-ELE-TAB-ALIAS
    private int numEleTabAlias = DefaultValues.INT_VAL;
    //Original name: C214-POSIZ-INI
    private int posizIni = DefaultValues.INT_VAL;
    //Original name: C214-LUNGHEZZA
    private int lunghezza = DefaultValues.INT_VAL;

    //==== METHODS ====
    public void setTabInfoBytes(byte[] buffer, int offset) {
        int position = offset;
        tabAlias = MarshalByte.readString(buffer, position, Len.TAB_ALIAS);
        position += Len.TAB_ALIAS;
        numOccorrenze = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_OCCORRENZE, 0);
        position += Len.NUM_OCCORRENZE;
        numEleTabAlias = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NUM_ELE_TAB_ALIAS, 0);
        position += Len.NUM_ELE_TAB_ALIAS;
        posizIni = MarshalByte.readPackedAsInt(buffer, position, Len.Int.POSIZ_INI, 0);
        position += Len.POSIZ_INI;
        lunghezza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LUNGHEZZA, 0);
    }

    public byte[] getTabInfoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tabAlias, Len.TAB_ALIAS);
        position += Len.TAB_ALIAS;
        MarshalByte.writeIntAsPacked(buffer, position, numOccorrenze, Len.Int.NUM_OCCORRENZE, 0);
        position += Len.NUM_OCCORRENZE;
        MarshalByte.writeIntAsPacked(buffer, position, numEleTabAlias, Len.Int.NUM_ELE_TAB_ALIAS, 0);
        position += Len.NUM_ELE_TAB_ALIAS;
        MarshalByte.writeIntAsPacked(buffer, position, posizIni, Len.Int.POSIZ_INI, 0);
        position += Len.POSIZ_INI;
        MarshalByte.writeIntAsPacked(buffer, position, lunghezza, Len.Int.LUNGHEZZA, 0);
        return buffer;
    }

    public void initTabInfoSpaces() {
        tabAlias = "";
        numOccorrenze = Types.INVALID_INT_VAL;
        numEleTabAlias = Types.INVALID_INT_VAL;
        posizIni = Types.INVALID_INT_VAL;
        lunghezza = Types.INVALID_INT_VAL;
    }

    public void setTabAlias(String tabAlias) {
        this.tabAlias = Functions.subString(tabAlias, Len.TAB_ALIAS);
    }

    public String getTabAlias() {
        return this.tabAlias;
    }

    public String getIvvc0213TabAliasFormatted() {
        return Functions.padBlanks(getTabAlias(), Len.TAB_ALIAS);
    }

    public void setNumOccorrenze(int numOccorrenze) {
        this.numOccorrenze = numOccorrenze;
    }

    public int getNumOccorrenze() {
        return this.numOccorrenze;
    }

    public void setNumEleTabAlias(int numEleTabAlias) {
        this.numEleTabAlias = numEleTabAlias;
    }

    public int getNumEleTabAlias() {
        return this.numEleTabAlias;
    }

    public void setPosizIni(int posizIni) {
        this.posizIni = posizIni;
    }

    public int getPosizIni() {
        return this.posizIni;
    }

    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }

    public int getLunghezza() {
        return this.lunghezza;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TAB_ALIAS = 3;
        public static final int NUM_OCCORRENZE = 3;
        public static final int NUM_ELE_TAB_ALIAS = 3;
        public static final int POSIZ_INI = 5;
        public static final int LUNGHEZZA = 5;
        public static final int TAB_INFO = TAB_ALIAS + NUM_OCCORRENZE + NUM_ELE_TAB_ALIAS + POSIZ_INI + LUNGHEZZA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NUM_OCCORRENZE = 5;
            public static final int NUM_ELE_TAB_ALIAS = 5;
            public static final int POSIZ_INI = 9;
            public static final int LUNGHEZZA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
