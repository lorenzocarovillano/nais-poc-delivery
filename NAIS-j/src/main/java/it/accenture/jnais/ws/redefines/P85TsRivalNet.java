package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-TS-RIVAL-NET<br>
 * Variable: P85-TS-RIVAL-NET from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85TsRivalNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85TsRivalNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_TS_RIVAL_NET;
    }

    public void setP85TsRivalNet(AfDecimal p85TsRivalNet) {
        writeDecimalAsPacked(Pos.P85_TS_RIVAL_NET, p85TsRivalNet.copy());
    }

    public void setP85TsRivalNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_TS_RIVAL_NET, Pos.P85_TS_RIVAL_NET);
    }

    /**Original name: P85-TS-RIVAL-NET<br>*/
    public AfDecimal getP85TsRivalNet() {
        return readPackedAsDecimal(Pos.P85_TS_RIVAL_NET, Len.Int.P85_TS_RIVAL_NET, Len.Fract.P85_TS_RIVAL_NET);
    }

    public byte[] getP85TsRivalNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_TS_RIVAL_NET, Pos.P85_TS_RIVAL_NET);
        return buffer;
    }

    public void setP85TsRivalNetNull(String p85TsRivalNetNull) {
        writeString(Pos.P85_TS_RIVAL_NET_NULL, p85TsRivalNetNull, Len.P85_TS_RIVAL_NET_NULL);
    }

    /**Original name: P85-TS-RIVAL-NET-NULL<br>*/
    public String getP85TsRivalNetNull() {
        return readString(Pos.P85_TS_RIVAL_NET_NULL, Len.P85_TS_RIVAL_NET_NULL);
    }

    public String getP85TsRivalNetNullFormatted() {
        return Functions.padBlanks(getP85TsRivalNetNull(), Len.P85_TS_RIVAL_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_TS_RIVAL_NET = 1;
        public static final int P85_TS_RIVAL_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_TS_RIVAL_NET = 8;
        public static final int P85_TS_RIVAL_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_TS_RIVAL_NET = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_TS_RIVAL_NET = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
