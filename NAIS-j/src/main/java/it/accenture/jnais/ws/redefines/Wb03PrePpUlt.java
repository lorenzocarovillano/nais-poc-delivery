package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-PP-ULT<br>
 * Variable: WB03-PRE-PP-ULT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrePpUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrePpUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_PP_ULT;
    }

    public void setWb03PrePpUlt(AfDecimal wb03PrePpUlt) {
        writeDecimalAsPacked(Pos.WB03_PRE_PP_ULT, wb03PrePpUlt.copy());
    }

    public void setWb03PrePpUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_PP_ULT, Pos.WB03_PRE_PP_ULT);
    }

    /**Original name: WB03-PRE-PP-ULT<br>*/
    public AfDecimal getWb03PrePpUlt() {
        return readPackedAsDecimal(Pos.WB03_PRE_PP_ULT, Len.Int.WB03_PRE_PP_ULT, Len.Fract.WB03_PRE_PP_ULT);
    }

    public byte[] getWb03PrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_PP_ULT, Pos.WB03_PRE_PP_ULT);
        return buffer;
    }

    public void setWb03PrePpUltNull(String wb03PrePpUltNull) {
        writeString(Pos.WB03_PRE_PP_ULT_NULL, wb03PrePpUltNull, Len.WB03_PRE_PP_ULT_NULL);
    }

    /**Original name: WB03-PRE-PP-ULT-NULL<br>*/
    public String getWb03PrePpUltNull() {
        return readString(Pos.WB03_PRE_PP_ULT_NULL, Len.WB03_PRE_PP_ULT_NULL);
    }

    public String getWb03PrePpUltNullFormatted() {
        return Functions.padBlanks(getWb03PrePpUltNull(), Len.WB03_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_PP_ULT = 1;
        public static final int WB03_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_PP_ULT = 8;
        public static final int WB03_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
