package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-ATT-DI-TRCH<br>
 * Variable: TGA-PRE-ATT-DI-TRCH from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreAttDiTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreAttDiTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_ATT_DI_TRCH;
    }

    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        writeDecimalAsPacked(Pos.TGA_PRE_ATT_DI_TRCH, tgaPreAttDiTrch.copy());
    }

    public void setTgaPreAttDiTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_ATT_DI_TRCH, Pos.TGA_PRE_ATT_DI_TRCH);
    }

    /**Original name: TGA-PRE-ATT-DI-TRCH<br>*/
    public AfDecimal getTgaPreAttDiTrch() {
        return readPackedAsDecimal(Pos.TGA_PRE_ATT_DI_TRCH, Len.Int.TGA_PRE_ATT_DI_TRCH, Len.Fract.TGA_PRE_ATT_DI_TRCH);
    }

    public byte[] getTgaPreAttDiTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_ATT_DI_TRCH, Pos.TGA_PRE_ATT_DI_TRCH);
        return buffer;
    }

    public void setTgaPreAttDiTrchNull(String tgaPreAttDiTrchNull) {
        writeString(Pos.TGA_PRE_ATT_DI_TRCH_NULL, tgaPreAttDiTrchNull, Len.TGA_PRE_ATT_DI_TRCH_NULL);
    }

    /**Original name: TGA-PRE-ATT-DI-TRCH-NULL<br>*/
    public String getTgaPreAttDiTrchNull() {
        return readString(Pos.TGA_PRE_ATT_DI_TRCH_NULL, Len.TGA_PRE_ATT_DI_TRCH_NULL);
    }

    public String getTgaPreAttDiTrchNullFormatted() {
        return Functions.padBlanks(getTgaPreAttDiTrchNull(), Len.TGA_PRE_ATT_DI_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_ATT_DI_TRCH = 1;
        public static final int TGA_PRE_ATT_DI_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_ATT_DI_TRCH = 8;
        public static final int TGA_PRE_ATT_DI_TRCH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_ATT_DI_TRCH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_ATT_DI_TRCH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
