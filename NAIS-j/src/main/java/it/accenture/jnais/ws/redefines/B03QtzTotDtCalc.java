package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-TOT-DT-CALC<br>
 * Variable: B03-QTZ-TOT-DT-CALC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzTotDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzTotDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_TOT_DT_CALC;
    }

    public void setB03QtzTotDtCalc(AfDecimal b03QtzTotDtCalc) {
        writeDecimalAsPacked(Pos.B03_QTZ_TOT_DT_CALC, b03QtzTotDtCalc.copy());
    }

    public void setB03QtzTotDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_TOT_DT_CALC, Pos.B03_QTZ_TOT_DT_CALC);
    }

    /**Original name: B03-QTZ-TOT-DT-CALC<br>*/
    public AfDecimal getB03QtzTotDtCalc() {
        return readPackedAsDecimal(Pos.B03_QTZ_TOT_DT_CALC, Len.Int.B03_QTZ_TOT_DT_CALC, Len.Fract.B03_QTZ_TOT_DT_CALC);
    }

    public byte[] getB03QtzTotDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_TOT_DT_CALC, Pos.B03_QTZ_TOT_DT_CALC);
        return buffer;
    }

    public void setB03QtzTotDtCalcNull(String b03QtzTotDtCalcNull) {
        writeString(Pos.B03_QTZ_TOT_DT_CALC_NULL, b03QtzTotDtCalcNull, Len.B03_QTZ_TOT_DT_CALC_NULL);
    }

    /**Original name: B03-QTZ-TOT-DT-CALC-NULL<br>*/
    public String getB03QtzTotDtCalcNull() {
        return readString(Pos.B03_QTZ_TOT_DT_CALC_NULL, Len.B03_QTZ_TOT_DT_CALC_NULL);
    }

    public String getB03QtzTotDtCalcNullFormatted() {
        return Functions.padBlanks(getB03QtzTotDtCalcNull(), Len.B03_QTZ_TOT_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_TOT_DT_CALC = 1;
        public static final int B03_QTZ_TOT_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_TOT_DT_CALC = 7;
        public static final int B03_QTZ_TOT_DT_CALC_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_TOT_DT_CALC = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_TOT_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
