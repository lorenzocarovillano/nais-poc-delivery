package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-IS-K2<br>
 * Variable: WDFA-IMPB-IS-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbIsK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbIsK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_IS_K2;
    }

    public void setWdfaImpbIsK2(AfDecimal wdfaImpbIsK2) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_IS_K2, wdfaImpbIsK2.copy());
    }

    public void setWdfaImpbIsK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_IS_K2, Pos.WDFA_IMPB_IS_K2);
    }

    /**Original name: WDFA-IMPB-IS-K2<br>*/
    public AfDecimal getWdfaImpbIsK2() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_IS_K2, Len.Int.WDFA_IMPB_IS_K2, Len.Fract.WDFA_IMPB_IS_K2);
    }

    public byte[] getWdfaImpbIsK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_IS_K2, Pos.WDFA_IMPB_IS_K2);
        return buffer;
    }

    public void setWdfaImpbIsK2Null(String wdfaImpbIsK2Null) {
        writeString(Pos.WDFA_IMPB_IS_K2_NULL, wdfaImpbIsK2Null, Len.WDFA_IMPB_IS_K2_NULL);
    }

    /**Original name: WDFA-IMPB-IS-K2-NULL<br>*/
    public String getWdfaImpbIsK2Null() {
        return readString(Pos.WDFA_IMPB_IS_K2_NULL, Len.WDFA_IMPB_IS_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K2 = 1;
        public static final int WDFA_IMPB_IS_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K2 = 8;
        public static final int WDFA_IMPB_IS_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
