package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.Ldbv1301;
import it.accenture.jnais.copy.RappAna;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.FlagCodiceFiscale;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagFineLetturaP58;
import it.accenture.jnais.ws.enums.FlagPartitaIva;
import it.accenture.jnais.ws.enums.FlagPersonaFisica;
import it.accenture.jnais.ws.enums.FlagPersonaGiuridica;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0037;
import it.accenture.jnais.ws.occurs.Wp58TabImpstBol;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2720<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2720Data {

    //==== PROPERTIES ====
    public static final int DP58_TAB_P58_MAXOCCURS = 75;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2720";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = DefaultValues.stringVal(Len.WK_CALL_PGM);
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    //Original name: LDBSE590
    private String ldbse590 = "LDBSE590";
    //Original name: WK-DATA-SEPARATA
    private WkDataSeparata wkDataSeparata = new WkDataSeparata();
    //Original name: WK-DATA-INIZIO
    private WkDataInizio wkDataInizio = new WkDataInizio();
    //Original name: WK-DATA-INIZIO-D
    private int wkDataInizioD = DefaultValues.INT_VAL;
    //Original name: WK-DATA-FINE
    private WkDataFine wkDataFine = new WkDataFine();
    //Original name: WK-DATA-FINE-D
    private int wkDataFineD = DefaultValues.INT_VAL;
    //Original name: WK-VAR-MOVI-COMUN
    private WkVarMoviComun wkVarMoviComun = new WkVarMoviComun();
    //Original name: AREA-RECUPERO-ANAGRAFICA
    private AreaRecuperoAnagrafica areaRecuperoAnagrafica = new AreaRecuperoAnagrafica();
    //Original name: AREA-CALL-IWFS0050
    private AreaCallIwfs0050 areaCallIwfs0050 = new AreaCallIwfs0050();
    //Original name: LDBVE391
    private Ldbve391 ldbve391 = new Ldbve391();
    //Original name: LDBVE421
    private Ldbve421 ldbve421 = new Ldbve421();
    //Original name: LDBV1241
    private Ldbv1241Lvvs2720 ldbv1241 = new Ldbv1241Lvvs2720();
    //Original name: LDBV1301
    private Ldbv1301 ldbv1301 = new Ldbv1301();
    //Original name: RAPP-ANA
    private RappAna rappAna = new RappAna();
    //Original name: PERS
    private Pers pers = new Pers();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: IMPST-BOLLO
    private ImpstBollo impstBollo = new ImpstBollo();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   LISTA TABELLE E TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0037 wsMovimento = new WsMovimentoLvvs0037();
    //Original name: DP58-ELE-P58-MAX
    private short dp58EleP58Max = DefaultValues.BIN_SHORT_VAL;
    //Original name: DP58-TAB-P58
    private Wp58TabImpstBol[] dp58TabP58 = new Wp58TabImpstBol[DP58_TAB_P58_MAXOCCURS];
    //Original name: DPOL-ELE-POL-MAX
    private short dpolElePolMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private IxIndiciLvvs2720 ixIndici = new IxIndiciLvvs2720();
    /**Original name: FLAG-CUR-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: FLAG-FINE-LETTURA-P58
    private FlagFineLetturaP58 flagFineLetturaP58 = new FlagFineLetturaP58();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: FLAG-PERSONA-FISICA
    private FlagPersonaFisica flagPersonaFisica = new FlagPersonaFisica();
    //Original name: FLAG-PERSONA-GIURIDICA
    private FlagPersonaGiuridica flagPersonaGiuridica = new FlagPersonaGiuridica();
    //Original name: FLAG-CODICE-FISCALE
    private FlagCodiceFiscale flagCodiceFiscale = new FlagCodiceFiscale();
    //Original name: FLAG-PARTITA-IVA
    private FlagPartitaIva flagPartitaIva = new FlagPartitaIva();
    //Original name: INT-REGISTER1
    private int intRegister1 = DefaultValues.INT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs2720Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dp58TabP58Idx = 1; dp58TabP58Idx <= DP58_TAB_P58_MAXOCCURS; dp58TabP58Idx++) {
            dp58TabP58[dp58TabP58Idx - 1] = new Wp58TabImpstBol();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getWkPgmFormatted() {
        return Functions.padBlanks(getWkPgm(), Len.WK_PGM);
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public String getLdbse590() {
        return this.ldbse590;
    }

    public void setWkDataInizioD(int wkDataInizioD) {
        this.wkDataInizioD = wkDataInizioD;
    }

    public int getWkDataInizioD() {
        return this.wkDataInizioD;
    }

    public void setWkDataFineD(int wkDataFineD) {
        this.wkDataFineD = wkDataFineD;
    }

    public int getWkDataFineD() {
        return this.wkDataFineD;
    }

    public void setDp58AreaP58Formatted(String data) {
        byte[] buffer = new byte[Len.DP58_AREA_P58];
        MarshalByte.writeString(buffer, 1, data, Len.DP58_AREA_P58);
        setDp58AreaP58Bytes(buffer, 1);
    }

    public void setDp58AreaP58Bytes(byte[] buffer, int offset) {
        int position = offset;
        dp58EleP58Max = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DP58_TAB_P58_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dp58TabP58[idx - 1].setWp58TabImpstBolBytes(buffer, position);
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
            else {
                dp58TabP58[idx - 1].initWp58TabImpstBolSpaces();
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
        }
    }

    public void setDp58EleP58Max(short dp58EleP58Max) {
        this.dp58EleP58Max = dp58EleP58Max;
    }

    public short getDp58EleP58Max() {
        return this.dp58EleP58Max;
    }

    public void setDpolAreaPolFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POL];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POL);
        setDpolAreaPolBytes(buffer, 1);
    }

    public void setDpolAreaPolBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePolMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPoliBytes(buffer, position);
    }

    public void setDpolElePolMax(short dpolElePolMax) {
        this.dpolElePolMax = dpolElePolMax;
    }

    public short getDpolElePolMax() {
        return this.dpolElePolMax;
    }

    public void setDpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setIntRegister1(int intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public int getIntRegister1() {
        return this.intRegister1;
    }

    public AreaCallIwfs0050 getAreaCallIwfs0050() {
        return areaCallIwfs0050;
    }

    public AreaRecuperoAnagrafica getAreaRecuperoAnagrafica() {
        return areaRecuperoAnagrafica;
    }

    public Wp58TabImpstBol getDp58TabP58(int idx) {
        return dp58TabP58[idx - 1];
    }

    public FlagCodiceFiscale getFlagCodiceFiscale() {
        return flagCodiceFiscale;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagFineLetturaP58 getFlagFineLetturaP58() {
        return flagFineLetturaP58;
    }

    public FlagPartitaIva getFlagPartitaIva() {
        return flagPartitaIva;
    }

    public FlagPersonaFisica getFlagPersonaFisica() {
        return flagPersonaFisica;
    }

    public FlagPersonaGiuridica getFlagPersonaGiuridica() {
        return flagPersonaGiuridica;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public ImpstBollo getImpstBollo() {
        return impstBollo;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs2720 getIxIndici() {
        return ixIndici;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public Ldbv1241Lvvs2720 getLdbv1241() {
        return ldbv1241;
    }

    public Ldbv1301 getLdbv1301() {
        return ldbv1301;
    }

    public Ldbve391 getLdbve391() {
        return ldbve391;
    }

    public Ldbve421 getLdbve421() {
        return ldbve421;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public Pers getPers() {
        return pers;
    }

    public RappAna getRappAna() {
        return rappAna;
    }

    public WkDataFine getWkDataFine() {
        return wkDataFine;
    }

    public WkDataInizio getWkDataInizio() {
        return wkDataInizio;
    }

    public WkDataSeparata getWkDataSeparata() {
        return wkDataSeparata;
    }

    public WkVarMoviComun getWkVarMoviComun() {
        return wkVarMoviComun;
    }

    public WsMovimentoLvvs0037 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;
        public static final int DP58_ELE_P58_MAX = 2;
        public static final int DP58_AREA_P58 = DP58_ELE_P58_MAX + Lvvs2720Data.DP58_TAB_P58_MAXOCCURS * Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
        public static final int DPOL_ELE_POL_MAX = 2;
        public static final int DPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POL = DPOL_ELE_POL_MAX + DPOL_TAB_POLI;
        public static final int WK_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
