package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ETA-RAGGN-DT-CALC<br>
 * Variable: B03-ETA-RAGGN-DT-CALC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03EtaRaggnDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03EtaRaggnDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ETA_RAGGN_DT_CALC;
    }

    public void setB03EtaRaggnDtCalc(AfDecimal b03EtaRaggnDtCalc) {
        writeDecimalAsPacked(Pos.B03_ETA_RAGGN_DT_CALC, b03EtaRaggnDtCalc.copy());
    }

    public void setB03EtaRaggnDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ETA_RAGGN_DT_CALC, Pos.B03_ETA_RAGGN_DT_CALC);
    }

    /**Original name: B03-ETA-RAGGN-DT-CALC<br>*/
    public AfDecimal getB03EtaRaggnDtCalc() {
        return readPackedAsDecimal(Pos.B03_ETA_RAGGN_DT_CALC, Len.Int.B03_ETA_RAGGN_DT_CALC, Len.Fract.B03_ETA_RAGGN_DT_CALC);
    }

    public byte[] getB03EtaRaggnDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ETA_RAGGN_DT_CALC, Pos.B03_ETA_RAGGN_DT_CALC);
        return buffer;
    }

    public void setB03EtaRaggnDtCalcNull(String b03EtaRaggnDtCalcNull) {
        writeString(Pos.B03_ETA_RAGGN_DT_CALC_NULL, b03EtaRaggnDtCalcNull, Len.B03_ETA_RAGGN_DT_CALC_NULL);
    }

    /**Original name: B03-ETA-RAGGN-DT-CALC-NULL<br>*/
    public String getB03EtaRaggnDtCalcNull() {
        return readString(Pos.B03_ETA_RAGGN_DT_CALC_NULL, Len.B03_ETA_RAGGN_DT_CALC_NULL);
    }

    public String getB03EtaRaggnDtCalcNullFormatted() {
        return Functions.padBlanks(getB03EtaRaggnDtCalcNull(), Len.B03_ETA_RAGGN_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ETA_RAGGN_DT_CALC = 1;
        public static final int B03_ETA_RAGGN_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ETA_RAGGN_DT_CALC = 4;
        public static final int B03_ETA_RAGGN_DT_CALC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ETA_RAGGN_DT_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ETA_RAGGN_DT_CALC = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
