package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-ID-RAPP-RETE<br>
 * Variable: TDR-ID-RAPP-RETE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrIdRappRete extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrIdRappRete() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_ID_RAPP_RETE;
    }

    public void setTdrIdRappRete(int tdrIdRappRete) {
        writeIntAsPacked(Pos.TDR_ID_RAPP_RETE, tdrIdRappRete, Len.Int.TDR_ID_RAPP_RETE);
    }

    public void setTdrIdRappReteFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_ID_RAPP_RETE, Pos.TDR_ID_RAPP_RETE);
    }

    /**Original name: TDR-ID-RAPP-RETE<br>*/
    public int getTdrIdRappRete() {
        return readPackedAsInt(Pos.TDR_ID_RAPP_RETE, Len.Int.TDR_ID_RAPP_RETE);
    }

    public byte[] getTdrIdRappReteAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_ID_RAPP_RETE, Pos.TDR_ID_RAPP_RETE);
        return buffer;
    }

    public void setTdrIdRappReteNull(String tdrIdRappReteNull) {
        writeString(Pos.TDR_ID_RAPP_RETE_NULL, tdrIdRappReteNull, Len.TDR_ID_RAPP_RETE_NULL);
    }

    /**Original name: TDR-ID-RAPP-RETE-NULL<br>*/
    public String getTdrIdRappReteNull() {
        return readString(Pos.TDR_ID_RAPP_RETE_NULL, Len.TDR_ID_RAPP_RETE_NULL);
    }

    public String getTdrIdRappReteNullFormatted() {
        return Functions.padBlanks(getTdrIdRappReteNull(), Len.TDR_ID_RAPP_RETE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_ID_RAPP_RETE = 1;
        public static final int TDR_ID_RAPP_RETE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_ID_RAPP_RETE = 5;
        public static final int TDR_ID_RAPP_RETE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_ID_RAPP_RETE = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
