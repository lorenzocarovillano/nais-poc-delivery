package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvb031;
import it.accenture.jnais.copy.Wb03Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: AREA-OUT<br>
 * Variable: AREA-OUT from program LLBS0240<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaOut extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WB03-ELE-B03-MAX
    private short wb03EleB03Max = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVB031
    private Lccvb031 lccvb031 = new Lccvb031();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_OUT;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaOutBytes(buf);
    }

    public void setAreaOutBytes(byte[] buffer) {
        setAreaOutBytes(buffer, 1);
    }

    public byte[] getAreaOutBytes() {
        byte[] buffer = new byte[Len.AREA_OUT];
        return getAreaOutBytes(buffer, 1);
    }

    public void setAreaOutBytes(byte[] buffer, int offset) {
        int position = offset;
        setWb03AreaB03Bytes(buffer, position);
    }

    public byte[] getAreaOutBytes(byte[] buffer, int offset) {
        int position = offset;
        getWb03AreaB03Bytes(buffer, position);
        return buffer;
    }

    public void setWb03AreaB03Bytes(byte[] buffer, int offset) {
        int position = offset;
        wb03EleB03Max = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWb03TabB03Bytes(buffer, position);
    }

    public byte[] getWb03AreaB03Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wb03EleB03Max);
        position += Types.SHORT_SIZE;
        getWb03TabB03Bytes(buffer, position);
        return buffer;
    }

    public void setWb03EleB03Max(short wb03EleB03Max) {
        this.wb03EleB03Max = wb03EleB03Max;
    }

    public short getWb03EleB03Max() {
        return this.wb03EleB03Max;
    }

    public void setWb03TabB03Bytes(byte[] buffer, int offset) {
        int position = offset;
        lccvb031.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvb031.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvb031.Len.Int.ID_PTF, 0));
        position += Lccvb031.Len.ID_PTF;
        lccvb031.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWb03TabB03Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvb031.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvb031.getIdPtf(), Lccvb031.Len.Int.ID_PTF, 0);
        position += Lccvb031.Len.ID_PTF;
        lccvb031.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvb031 getLccvb031() {
        return lccvb031;
    }

    @Override
    public byte[] serialize() {
        return getAreaOutBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ELE_B03_MAX = 2;
        public static final int WB03_TAB_B03 = WpolStatus.Len.STATUS + Lccvb031.Len.ID_PTF + Wb03Dati.Len.DATI;
        public static final int WB03_AREA_B03 = WB03_ELE_B03_MAX + WB03_TAB_B03;
        public static final int AREA_OUT = WB03_AREA_B03;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
