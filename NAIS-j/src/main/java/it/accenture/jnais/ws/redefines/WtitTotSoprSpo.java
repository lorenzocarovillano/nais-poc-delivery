package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SOPR-SPO<br>
 * Variable: WTIT-TOT-SOPR-SPO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SOPR_SPO;
    }

    public void setWtitTotSoprSpo(AfDecimal wtitTotSoprSpo) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SOPR_SPO, wtitTotSoprSpo.copy());
    }

    public void setWtitTotSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SOPR_SPO, Pos.WTIT_TOT_SOPR_SPO);
    }

    /**Original name: WTIT-TOT-SOPR-SPO<br>*/
    public AfDecimal getWtitTotSoprSpo() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SOPR_SPO, Len.Int.WTIT_TOT_SOPR_SPO, Len.Fract.WTIT_TOT_SOPR_SPO);
    }

    public byte[] getWtitTotSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SOPR_SPO, Pos.WTIT_TOT_SOPR_SPO);
        return buffer;
    }

    public void initWtitTotSoprSpoSpaces() {
        fill(Pos.WTIT_TOT_SOPR_SPO, Len.WTIT_TOT_SOPR_SPO, Types.SPACE_CHAR);
    }

    public void setWtitTotSoprSpoNull(String wtitTotSoprSpoNull) {
        writeString(Pos.WTIT_TOT_SOPR_SPO_NULL, wtitTotSoprSpoNull, Len.WTIT_TOT_SOPR_SPO_NULL);
    }

    /**Original name: WTIT-TOT-SOPR-SPO-NULL<br>*/
    public String getWtitTotSoprSpoNull() {
        return readString(Pos.WTIT_TOT_SOPR_SPO_NULL, Len.WTIT_TOT_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_SPO = 1;
        public static final int WTIT_TOT_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_SPO = 8;
        public static final int WTIT_TOT_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
