package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-DT-ATTIV-VINPG<br>
 * Variable: L23-DT-ATTIV-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23DtAttivVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23DtAttivVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_DT_ATTIV_VINPG;
    }

    public void setL23DtAttivVinpg(int l23DtAttivVinpg) {
        writeIntAsPacked(Pos.L23_DT_ATTIV_VINPG, l23DtAttivVinpg, Len.Int.L23_DT_ATTIV_VINPG);
    }

    public void setL23DtAttivVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_DT_ATTIV_VINPG, Pos.L23_DT_ATTIV_VINPG);
    }

    /**Original name: L23-DT-ATTIV-VINPG<br>*/
    public int getL23DtAttivVinpg() {
        return readPackedAsInt(Pos.L23_DT_ATTIV_VINPG, Len.Int.L23_DT_ATTIV_VINPG);
    }

    public byte[] getL23DtAttivVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_DT_ATTIV_VINPG, Pos.L23_DT_ATTIV_VINPG);
        return buffer;
    }

    public void setL23DtAttivVinpgNull(String l23DtAttivVinpgNull) {
        writeString(Pos.L23_DT_ATTIV_VINPG_NULL, l23DtAttivVinpgNull, Len.L23_DT_ATTIV_VINPG_NULL);
    }

    /**Original name: L23-DT-ATTIV-VINPG-NULL<br>*/
    public String getL23DtAttivVinpgNull() {
        return readString(Pos.L23_DT_ATTIV_VINPG_NULL, Len.L23_DT_ATTIV_VINPG_NULL);
    }

    public String getL23DtAttivVinpgNullFormatted() {
        return Functions.padBlanks(getL23DtAttivVinpgNull(), Len.L23_DT_ATTIV_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_DT_ATTIV_VINPG = 1;
        public static final int L23_DT_ATTIV_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_DT_ATTIV_VINPG = 5;
        public static final int L23_DT_ATTIV_VINPG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_DT_ATTIV_VINPG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
