package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WK-DATA-FINE<br>
 * Variable: WK-DATA-FINE from program LVVS2750<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDataFineLvvs2750 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDataFineLvvs2750() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DATA_FINE;
    }

    public void setWkDataFineFormatted(String data) {
        writeString(Pos.WK_DATA_FINE, data, Len.WK_DATA_FINE);
    }

    public void setWkDataFineD(int wkDataFineD) {
        writeIntAsPacked(Pos.WK_DATA_FINE_D, wkDataFineD, Len.Int.WK_DATA_FINE_D);
    }

    public void setWkDataFineDFormatted(String wkDataFineD) {
        setWkDataFineD(PicParser.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).parseInt(wkDataFineD));
    }

    /**Original name: WK-DATA-FINE-D<br>*/
    public int getWkDataFineD() {
        return readPackedAsInt(Pos.WK_DATA_FINE_D, Len.Int.WK_DATA_FINE_D);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DATA_FINE = 1;
        public static final int ANNO_FINE = WK_DATA_FINE;
        public static final int MESE_FINE = ANNO_FINE + Len.ANNO_FINE;
        public static final int GG_FINE = MESE_FINE + Len.MESE_FINE;
        public static final int WK_DATA_FINE_D = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNO_FINE = 4;
        public static final int MESE_FINE = 2;
        public static final int GG_FINE = 2;
        public static final int WK_DATA_FINE = ANNO_FINE + MESE_FINE + GG_FINE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_DATA_FINE_D = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
