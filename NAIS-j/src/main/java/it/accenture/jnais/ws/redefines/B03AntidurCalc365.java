package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ANTIDUR-CALC-365<br>
 * Variable: B03-ANTIDUR-CALC-365 from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AntidurCalc365 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AntidurCalc365() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ANTIDUR_CALC365;
    }

    public void setB03AntidurCalc365(AfDecimal b03AntidurCalc365) {
        writeDecimalAsPacked(Pos.B03_ANTIDUR_CALC365, b03AntidurCalc365.copy());
    }

    public void setB03AntidurCalc365FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ANTIDUR_CALC365, Pos.B03_ANTIDUR_CALC365);
    }

    /**Original name: B03-ANTIDUR-CALC-365<br>*/
    public AfDecimal getB03AntidurCalc365() {
        return readPackedAsDecimal(Pos.B03_ANTIDUR_CALC365, Len.Int.B03_ANTIDUR_CALC365, Len.Fract.B03_ANTIDUR_CALC365);
    }

    public byte[] getB03AntidurCalc365AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ANTIDUR_CALC365, Pos.B03_ANTIDUR_CALC365);
        return buffer;
    }

    public void setB03AntidurCalc365Null(String b03AntidurCalc365Null) {
        writeString(Pos.B03_ANTIDUR_CALC365_NULL, b03AntidurCalc365Null, Len.B03_ANTIDUR_CALC365_NULL);
    }

    /**Original name: B03-ANTIDUR-CALC-365-NULL<br>*/
    public String getB03AntidurCalc365Null() {
        return readString(Pos.B03_ANTIDUR_CALC365_NULL, Len.B03_ANTIDUR_CALC365_NULL);
    }

    public String getB03AntidurCalc365NullFormatted() {
        return Functions.padBlanks(getB03AntidurCalc365Null(), Len.B03_ANTIDUR_CALC365_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ANTIDUR_CALC365 = 1;
        public static final int B03_ANTIDUR_CALC365_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ANTIDUR_CALC365 = 6;
        public static final int B03_ANTIDUR_CALC365_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_ANTIDUR_CALC365 = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ANTIDUR_CALC365 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
