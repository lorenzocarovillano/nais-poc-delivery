package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-DT-N<br>
 * Variable: WS-DT-N from program LDBM0250<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDtN {

    //==== PROPERTIES ====
    //Original name: WS-DT-N-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WS-DT-N-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: WS-DT-N-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWsDtNFormatted(String data) {
        byte[] buffer = new byte[Len.WS_DT_N];
        MarshalByte.writeString(buffer, 1, data, Len.WS_DT_N);
        setWsDtNBytes(buffer, 1);
    }

    public void setWsDtNBytes(byte[] buffer, int offset) {
        int position = offset;
        aa = MarshalByte.readFixedString(buffer, position, Len.AA);
        position += Len.AA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public String getAaFormatted() {
        return this.aa;
    }

    public String getMmFormatted() {
        return this.mm;
    }

    public String getGgFormatted() {
        return this.gg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WS_DT_N = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
