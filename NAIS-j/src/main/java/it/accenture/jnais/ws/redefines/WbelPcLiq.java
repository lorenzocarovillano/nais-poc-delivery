package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-PC-LIQ<br>
 * Variable: WBEL-PC-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelPcLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelPcLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_PC_LIQ;
    }

    public void setWbelPcLiq(AfDecimal wbelPcLiq) {
        writeDecimalAsPacked(Pos.WBEL_PC_LIQ, wbelPcLiq.copy());
    }

    public void setWbelPcLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_PC_LIQ, Pos.WBEL_PC_LIQ);
    }

    /**Original name: WBEL-PC-LIQ<br>*/
    public AfDecimal getWbelPcLiq() {
        return readPackedAsDecimal(Pos.WBEL_PC_LIQ, Len.Int.WBEL_PC_LIQ, Len.Fract.WBEL_PC_LIQ);
    }

    public byte[] getWbelPcLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_PC_LIQ, Pos.WBEL_PC_LIQ);
        return buffer;
    }

    public void initWbelPcLiqSpaces() {
        fill(Pos.WBEL_PC_LIQ, Len.WBEL_PC_LIQ, Types.SPACE_CHAR);
    }

    public void setWbelPcLiqNull(String wbelPcLiqNull) {
        writeString(Pos.WBEL_PC_LIQ_NULL, wbelPcLiqNull, Len.WBEL_PC_LIQ_NULL);
    }

    /**Original name: WBEL-PC-LIQ-NULL<br>*/
    public String getWbelPcLiqNull() {
        return readString(Pos.WBEL_PC_LIQ_NULL, Len.WBEL_PC_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_PC_LIQ = 1;
        public static final int WBEL_PC_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_PC_LIQ = 4;
        public static final int WBEL_PC_LIQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
