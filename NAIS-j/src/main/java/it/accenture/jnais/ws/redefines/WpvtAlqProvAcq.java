package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-ALQ-PROV-ACQ<br>
 * Variable: WPVT-ALQ-PROV-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtAlqProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtAlqProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ALQ_PROV_ACQ;
    }

    public void setWpvtAlqProvAcq(AfDecimal wpvtAlqProvAcq) {
        writeDecimalAsPacked(Pos.WPVT_ALQ_PROV_ACQ, wpvtAlqProvAcq.copy());
    }

    public void setWpvtAlqProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ALQ_PROV_ACQ, Pos.WPVT_ALQ_PROV_ACQ);
    }

    /**Original name: WPVT-ALQ-PROV-ACQ<br>*/
    public AfDecimal getWpvtAlqProvAcq() {
        return readPackedAsDecimal(Pos.WPVT_ALQ_PROV_ACQ, Len.Int.WPVT_ALQ_PROV_ACQ, Len.Fract.WPVT_ALQ_PROV_ACQ);
    }

    public byte[] getWpvtAlqProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ALQ_PROV_ACQ, Pos.WPVT_ALQ_PROV_ACQ);
        return buffer;
    }

    public void initWpvtAlqProvAcqSpaces() {
        fill(Pos.WPVT_ALQ_PROV_ACQ, Len.WPVT_ALQ_PROV_ACQ, Types.SPACE_CHAR);
    }

    public void setWpvtAlqProvAcqNull(String wpvtAlqProvAcqNull) {
        writeString(Pos.WPVT_ALQ_PROV_ACQ_NULL, wpvtAlqProvAcqNull, Len.WPVT_ALQ_PROV_ACQ_NULL);
    }

    /**Original name: WPVT-ALQ-PROV-ACQ-NULL<br>*/
    public String getWpvtAlqProvAcqNull() {
        return readString(Pos.WPVT_ALQ_PROV_ACQ_NULL, Len.WPVT_ALQ_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_PROV_ACQ = 1;
        public static final int WPVT_ALQ_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_PROV_ACQ = 4;
        public static final int WPVT_ALQ_PROV_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
