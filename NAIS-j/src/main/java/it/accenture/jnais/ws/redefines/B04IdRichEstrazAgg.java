package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B04-ID-RICH-ESTRAZ-AGG<br>
 * Variable: B04-ID-RICH-ESTRAZ-AGG from program LLBS0266<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B04IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B04IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B04_ID_RICH_ESTRAZ_AGG;
    }

    public void setB04IdRichEstrazAgg(int b04IdRichEstrazAgg) {
        writeIntAsPacked(Pos.B04_ID_RICH_ESTRAZ_AGG, b04IdRichEstrazAgg, Len.Int.B04_ID_RICH_ESTRAZ_AGG);
    }

    public void setB04IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B04_ID_RICH_ESTRAZ_AGG, Pos.B04_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: B04-ID-RICH-ESTRAZ-AGG<br>*/
    public int getB04IdRichEstrazAgg() {
        return readPackedAsInt(Pos.B04_ID_RICH_ESTRAZ_AGG, Len.Int.B04_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getB04IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B04_ID_RICH_ESTRAZ_AGG, Pos.B04_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setB04IdRichEstrazAggNull(String b04IdRichEstrazAggNull) {
        writeString(Pos.B04_ID_RICH_ESTRAZ_AGG_NULL, b04IdRichEstrazAggNull, Len.B04_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: B04-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getB04IdRichEstrazAggNull() {
        return readString(Pos.B04_ID_RICH_ESTRAZ_AGG_NULL, Len.B04_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getB04IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getB04IdRichEstrazAggNull(), Len.B04_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B04_ID_RICH_ESTRAZ_AGG = 1;
        public static final int B04_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B04_ID_RICH_ESTRAZ_AGG = 5;
        public static final int B04_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B04_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
