package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-PC-TFR<br>
 * Variable: DAD-PC-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadPcTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadPcTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_PC_TFR;
    }

    public void setDadPcTfr(AfDecimal dadPcTfr) {
        writeDecimalAsPacked(Pos.DAD_PC_TFR, dadPcTfr.copy());
    }

    public void setDadPcTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_PC_TFR, Pos.DAD_PC_TFR);
    }

    /**Original name: DAD-PC-TFR<br>*/
    public AfDecimal getDadPcTfr() {
        return readPackedAsDecimal(Pos.DAD_PC_TFR, Len.Int.DAD_PC_TFR, Len.Fract.DAD_PC_TFR);
    }

    public byte[] getDadPcTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_PC_TFR, Pos.DAD_PC_TFR);
        return buffer;
    }

    public void setDadPcTfrNull(String dadPcTfrNull) {
        writeString(Pos.DAD_PC_TFR_NULL, dadPcTfrNull, Len.DAD_PC_TFR_NULL);
    }

    /**Original name: DAD-PC-TFR-NULL<br>*/
    public String getDadPcTfrNull() {
        return readString(Pos.DAD_PC_TFR_NULL, Len.DAD_PC_TFR_NULL);
    }

    public String getDadPcTfrNullFormatted() {
        return Functions.padBlanks(getDadPcTfrNull(), Len.DAD_PC_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_PC_TFR = 1;
        public static final int DAD_PC_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_PC_TFR = 4;
        public static final int DAD_PC_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
