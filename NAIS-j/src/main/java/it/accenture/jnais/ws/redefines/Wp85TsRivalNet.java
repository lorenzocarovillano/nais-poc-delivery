package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-TS-RIVAL-NET<br>
 * Variable: WP85-TS-RIVAL-NET from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85TsRivalNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85TsRivalNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_TS_RIVAL_NET;
    }

    public void setWp85TsRivalNet(AfDecimal wp85TsRivalNet) {
        writeDecimalAsPacked(Pos.WP85_TS_RIVAL_NET, wp85TsRivalNet.copy());
    }

    /**Original name: WP85-TS-RIVAL-NET<br>*/
    public AfDecimal getWp85TsRivalNet() {
        return readPackedAsDecimal(Pos.WP85_TS_RIVAL_NET, Len.Int.WP85_TS_RIVAL_NET, Len.Fract.WP85_TS_RIVAL_NET);
    }

    public void setWp85TsRivalNetNull(String wp85TsRivalNetNull) {
        writeString(Pos.WP85_TS_RIVAL_NET_NULL, wp85TsRivalNetNull, Len.WP85_TS_RIVAL_NET_NULL);
    }

    /**Original name: WP85-TS-RIVAL-NET-NULL<br>*/
    public String getWp85TsRivalNetNull() {
        return readString(Pos.WP85_TS_RIVAL_NET_NULL, Len.WP85_TS_RIVAL_NET_NULL);
    }

    public String getWp85TsRivalNetNullFormatted() {
        return Functions.padBlanks(getWp85TsRivalNetNull(), Len.WP85_TS_RIVAL_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_TS_RIVAL_NET = 1;
        public static final int WP85_TS_RIVAL_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_TS_RIVAL_NET = 8;
        public static final int WP85_TS_RIVAL_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_TS_RIVAL_NET = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_TS_RIVAL_NET = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
