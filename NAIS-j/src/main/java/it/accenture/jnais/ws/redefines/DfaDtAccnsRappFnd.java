package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-DT-ACCNS-RAPP-FND<br>
 * Variable: DFA-DT-ACCNS-RAPP-FND from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaDtAccnsRappFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaDtAccnsRappFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_DT_ACCNS_RAPP_FND;
    }

    public void setDfaDtAccnsRappFnd(int dfaDtAccnsRappFnd) {
        writeIntAsPacked(Pos.DFA_DT_ACCNS_RAPP_FND, dfaDtAccnsRappFnd, Len.Int.DFA_DT_ACCNS_RAPP_FND);
    }

    public void setDfaDtAccnsRappFndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_DT_ACCNS_RAPP_FND, Pos.DFA_DT_ACCNS_RAPP_FND);
    }

    /**Original name: DFA-DT-ACCNS-RAPP-FND<br>*/
    public int getDfaDtAccnsRappFnd() {
        return readPackedAsInt(Pos.DFA_DT_ACCNS_RAPP_FND, Len.Int.DFA_DT_ACCNS_RAPP_FND);
    }

    public byte[] getDfaDtAccnsRappFndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_DT_ACCNS_RAPP_FND, Pos.DFA_DT_ACCNS_RAPP_FND);
        return buffer;
    }

    public void setDfaDtAccnsRappFndNull(String dfaDtAccnsRappFndNull) {
        writeString(Pos.DFA_DT_ACCNS_RAPP_FND_NULL, dfaDtAccnsRappFndNull, Len.DFA_DT_ACCNS_RAPP_FND_NULL);
    }

    /**Original name: DFA-DT-ACCNS-RAPP-FND-NULL<br>*/
    public String getDfaDtAccnsRappFndNull() {
        return readString(Pos.DFA_DT_ACCNS_RAPP_FND_NULL, Len.DFA_DT_ACCNS_RAPP_FND_NULL);
    }

    public String getDfaDtAccnsRappFndNullFormatted() {
        return Functions.padBlanks(getDfaDtAccnsRappFndNull(), Len.DFA_DT_ACCNS_RAPP_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_DT_ACCNS_RAPP_FND = 1;
        public static final int DFA_DT_ACCNS_RAPP_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_DT_ACCNS_RAPP_FND = 5;
        public static final int DFA_DT_ACCNS_RAPP_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_DT_ACCNS_RAPP_FND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
