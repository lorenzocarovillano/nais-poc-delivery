package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WALL-PERIODO<br>
 * Variable: WALL-PERIODO from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WallPeriodo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WallPeriodo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WALL_PERIODO;
    }

    public void setWallPeriodo(short wallPeriodo) {
        writeShortAsPacked(Pos.WALL_PERIODO, wallPeriodo, Len.Int.WALL_PERIODO);
    }

    public void setWallPeriodoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WALL_PERIODO, Pos.WALL_PERIODO);
    }

    /**Original name: WALL-PERIODO<br>*/
    public short getWallPeriodo() {
        return readPackedAsShort(Pos.WALL_PERIODO, Len.Int.WALL_PERIODO);
    }

    public byte[] getWallPeriodoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WALL_PERIODO, Pos.WALL_PERIODO);
        return buffer;
    }

    public void initWallPeriodoSpaces() {
        fill(Pos.WALL_PERIODO, Len.WALL_PERIODO, Types.SPACE_CHAR);
    }

    public void setWallPeriodoNull(String wallPeriodoNull) {
        writeString(Pos.WALL_PERIODO_NULL, wallPeriodoNull, Len.WALL_PERIODO_NULL);
    }

    /**Original name: WALL-PERIODO-NULL<br>*/
    public String getWallPeriodoNull() {
        return readString(Pos.WALL_PERIODO_NULL, Len.WALL_PERIODO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WALL_PERIODO = 1;
        public static final int WALL_PERIODO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WALL_PERIODO = 2;
        public static final int WALL_PERIODO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WALL_PERIODO = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
