package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: AREA-IVVC0223<br>
 * Variable: AREA-IVVC0223 from program IVVS0216<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class AreaIvvc0223 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VAR_GAR_MAXOCCURS = 30;
    public static final int TAB_LIVELLI_GAR_MAXOCCURS = 20;

    //==== CONSTRUCTORS ====
    public AreaIvvc0223() {
        super();
    }

    public AreaIvvc0223(byte[] data) {
        super(data);
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IVVC0223;
    }

    public void setAreaIvvc0223Bytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.AREA_IVVC0223, Pos.AREA_IVVC0223);
    }

    public byte[] getAreaIvvc0223Bytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.AREA_IVVC0223, Pos.AREA_IVVC0223);
        return buffer;
    }

    public void setEleMaxAreaGar(short eleMaxAreaGar) {
        writeBinaryShort(Pos.ELE_MAX_AREA_GAR, eleMaxAreaGar);
    }

    /**Original name: IVVC0223-ELE-MAX-AREA-GAR<br>*/
    public short getEleMaxAreaGar() {
        return readBinaryShort(Pos.ELE_MAX_AREA_GAR);
    }

    public void setIdGaranziaFormatted(int idGaranziaIdx, String idGaranzia) {
        int position = Pos.ivvc0223IdGaranzia(idGaranziaIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(idGaranzia, Len.ID_GARANZIA), Len.ID_GARANZIA);
    }

    /**Original name: IVVC0223-ID-GARANZIA<br>*/
    public int getIdGaranzia(int idGaranziaIdx) {
        int position = Pos.ivvc0223IdGaranzia(idGaranziaIdx - 1);
        return readNumDispUnsignedInt(position, Len.ID_GARANZIA);
    }

    public void setCodTari(int codTariIdx, String codTari) {
        int position = Pos.ivvc0223CodTari(codTariIdx - 1);
        writeString(position, codTari, Len.COD_TARI);
    }

    /**Original name: IVVC0223-COD-TARI<br>*/
    public String getCodTari(int codTariIdx) {
        int position = Pos.ivvc0223CodTari(codTariIdx - 1);
        return readString(position, Len.COD_TARI);
    }

    public void setEleMaxVarGar(int eleMaxVarGarIdx, short eleMaxVarGar) {
        int position = Pos.ivvc0223EleMaxVarGar(eleMaxVarGarIdx - 1);
        writeBinaryShort(position, eleMaxVarGar);
    }

    /**Original name: IVVC0223-ELE-MAX-VAR-GAR<br>*/
    public short getEleMaxVarGar(int eleMaxVarGarIdx) {
        int position = Pos.ivvc0223EleMaxVarGar(eleMaxVarGarIdx - 1);
        return readBinaryShort(position);
    }

    public void setCodVariabile(int codVariabileIdx1, int codVariabileIdx2, String codVariabile) {
        int position = Pos.ivvc0223CodVariabile(codVariabileIdx1 - 1, codVariabileIdx2 - 1);
        writeString(position, codVariabile, Len.COD_VARIABILE);
    }

    /**Original name: IVVC0223-COD-VARIABILE<br>*/
    public String getCodVariabile(int codVariabileIdx1, int codVariabileIdx2) {
        int position = Pos.ivvc0223CodVariabile(codVariabileIdx1 - 1, codVariabileIdx2 - 1);
        return readString(position, Len.COD_VARIABILE);
    }

    public void setTpDato(int tpDatoIdx1, int tpDatoIdx2, char tpDato) {
        int position = Pos.ivvc0223TpDato(tpDatoIdx1 - 1, tpDatoIdx2 - 1);
        writeChar(position, tpDato);
    }

    public void setTpDatoFormatted(int tpDatoIdx1, int tpDatoIdx2, String tpDato) {
        setTpDato(tpDatoIdx1, tpDatoIdx2, Functions.charAt(tpDato, Types.CHAR_SIZE));
    }

    /**Original name: IVVC0223-TP-DATO<br>*/
    public char getTpDato(int tpDatoIdx1, int tpDatoIdx2) {
        int position = Pos.ivvc0223TpDato(tpDatoIdx1 - 1, tpDatoIdx2 - 1);
        return readChar(position);
    }

    public void setValImp(int valImpIdx1, int valImpIdx2, AfDecimal valImp) {
        int position = Pos.ivvc0223ValImp(valImpIdx1 - 1, valImpIdx2 - 1);
        writeDecimal(position, valImp.copy());
    }

    /**Original name: IVVC0223-VAL-IMP<br>*/
    public AfDecimal getValImp(int valImpIdx1, int valImpIdx2) {
        int position = Pos.ivvc0223ValImp(valImpIdx1 - 1, valImpIdx2 - 1);
        return readDecimal(position, Len.Int.VAL_IMP, Len.Fract.VAL_IMP);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int AREA_IVVC0223 = 1;
        public static final int AREA_VARIABILI_GAR = AREA_IVVC0223;
        public static final int ELE_MAX_AREA_GAR = AREA_VARIABILI_GAR;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ivvc0223TabLivelliGar(int idx) {
            return ELE_MAX_AREA_GAR + Len.ELE_MAX_AREA_GAR + idx * Len.TAB_LIVELLI_GAR;
        }

        public static int ivvc0223IdGaranzia(int idx) {
            return ivvc0223TabLivelliGar(idx);
        }

        public static int ivvc0223CodTari(int idx) {
            return ivvc0223IdGaranzia(idx) + Len.ID_GARANZIA;
        }

        public static int ivvc0223EleMaxVarGar(int idx) {
            return ivvc0223CodTari(idx) + Len.COD_TARI;
        }

        public static int ivvc0223TabVarGar(int idx1, int idx2) {
            return ivvc0223EleMaxVarGar(idx1) + Len.ELE_MAX_VAR_GAR + idx2 * Len.TAB_VAR_GAR;
        }

        public static int ivvc0223AreaVarGar(int idx1, int idx2) {
            return ivvc0223TabVarGar(idx1, idx2);
        }

        public static int ivvc0223CodVariabile(int idx1, int idx2) {
            return ivvc0223AreaVarGar(idx1, idx2);
        }

        public static int ivvc0223TpDato(int idx1, int idx2) {
            return ivvc0223CodVariabile(idx1, idx2) + Len.COD_VARIABILE;
        }

        public static int ivvc0223ValImp(int idx1, int idx2) {
            return ivvc0223TpDato(idx1, idx2) + Len.TP_DATO;
        }

        public static int ivvc0223ValPerc(int idx1, int idx2) {
            return ivvc0223ValImp(idx1, idx2) + Len.VAL_IMP;
        }

        public static int ivvc0223ValStr(int idx1, int idx2) {
            return ivvc0223ValPerc(idx1, idx2) + Len.VAL_PERC;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_AREA_GAR = 2;
        public static final int ID_GARANZIA = 9;
        public static final int COD_TARI = 12;
        public static final int ELE_MAX_VAR_GAR = 2;
        public static final int COD_VARIABILE = 12;
        public static final int TP_DATO = 1;
        public static final int VAL_IMP = 18;
        public static final int VAL_PERC = 14;
        public static final int VAL_STR = 60;
        public static final int AREA_VAR_GAR = COD_VARIABILE + TP_DATO + VAL_IMP + VAL_PERC + VAL_STR;
        public static final int TAB_VAR_GAR = AREA_VAR_GAR;
        public static final int TAB_LIVELLI_GAR = ID_GARANZIA + COD_TARI + ELE_MAX_VAR_GAR + AreaIvvc0223.TAB_VAR_GAR_MAXOCCURS * TAB_VAR_GAR;
        public static final int AREA_VARIABILI_GAR = ELE_MAX_AREA_GAR + AreaIvvc0223.TAB_LIVELLI_GAR_MAXOCCURS * TAB_LIVELLI_GAR;
        public static final int AREA_IVVC0223 = AREA_VARIABILI_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAL_IMP = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
