package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BPA-ID-OGG-A<br>
 * Variable: BPA-ID-OGG-A from program IABS0130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BpaIdOggA extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BpaIdOggA() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BPA_ID_OGG_A;
    }

    public void setBpaIdOggA(int bpaIdOggA) {
        writeIntAsPacked(Pos.BPA_ID_OGG_A, bpaIdOggA, Len.Int.BPA_ID_OGG_A);
    }

    public void setBpaIdOggAFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BPA_ID_OGG_A, Pos.BPA_ID_OGG_A);
    }

    /**Original name: BPA-ID-OGG-A<br>*/
    public int getBpaIdOggA() {
        return readPackedAsInt(Pos.BPA_ID_OGG_A, Len.Int.BPA_ID_OGG_A);
    }

    public byte[] getBpaIdOggAAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BPA_ID_OGG_A, Pos.BPA_ID_OGG_A);
        return buffer;
    }

    public void initBpaIdOggAHighValues() {
        fill(Pos.BPA_ID_OGG_A, Len.BPA_ID_OGG_A, Types.HIGH_CHAR_VAL);
    }

    public void setBpaIdOggANull(String bpaIdOggANull) {
        writeString(Pos.BPA_ID_OGG_A_NULL, bpaIdOggANull, Len.BPA_ID_OGG_A_NULL);
    }

    /**Original name: BPA-ID-OGG-A-NULL<br>*/
    public String getBpaIdOggANull() {
        return readString(Pos.BPA_ID_OGG_A_NULL, Len.BPA_ID_OGG_A_NULL);
    }

    public String getBpaIdOggANullFormatted() {
        return Functions.padBlanks(getBpaIdOggANull(), Len.BPA_ID_OGG_A_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BPA_ID_OGG_A = 1;
        public static final int BPA_ID_OGG_A_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BPA_ID_OGG_A = 5;
        public static final int BPA_ID_OGG_A_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BPA_ID_OGG_A = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
