package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-1O-UTLZ-C-REV<br>
 * Variable: P67-DT-1O-UTLZ-C-REV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67Dt1oUtlzCRev extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67Dt1oUtlzCRev() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT1O_UTLZ_C_REV;
    }

    public void setP67Dt1oUtlzCRev(int p67Dt1oUtlzCRev) {
        writeIntAsPacked(Pos.P67_DT1O_UTLZ_C_REV, p67Dt1oUtlzCRev, Len.Int.P67_DT1O_UTLZ_C_REV);
    }

    public void setP67Dt1oUtlzCRevFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT1O_UTLZ_C_REV, Pos.P67_DT1O_UTLZ_C_REV);
    }

    /**Original name: P67-DT-1O-UTLZ-C-REV<br>*/
    public int getP67Dt1oUtlzCRev() {
        return readPackedAsInt(Pos.P67_DT1O_UTLZ_C_REV, Len.Int.P67_DT1O_UTLZ_C_REV);
    }

    public byte[] getP67Dt1oUtlzCRevAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT1O_UTLZ_C_REV, Pos.P67_DT1O_UTLZ_C_REV);
        return buffer;
    }

    public void setP67Dt1oUtlzCRevNull(String p67Dt1oUtlzCRevNull) {
        writeString(Pos.P67_DT1O_UTLZ_C_REV_NULL, p67Dt1oUtlzCRevNull, Len.P67_DT1O_UTLZ_C_REV_NULL);
    }

    /**Original name: P67-DT-1O-UTLZ-C-REV-NULL<br>*/
    public String getP67Dt1oUtlzCRevNull() {
        return readString(Pos.P67_DT1O_UTLZ_C_REV_NULL, Len.P67_DT1O_UTLZ_C_REV_NULL);
    }

    public String getP67Dt1oUtlzCRevNullFormatted() {
        return Functions.padBlanks(getP67Dt1oUtlzCRevNull(), Len.P67_DT1O_UTLZ_C_REV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT1O_UTLZ_C_REV = 1;
        public static final int P67_DT1O_UTLZ_C_REV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT1O_UTLZ_C_REV = 5;
        public static final int P67_DT1O_UTLZ_C_REV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT1O_UTLZ_C_REV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
