package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-COMPON-TAX-RIMB<br>
 * Variable: WTLI-COMPON-TAX-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliComponTaxRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliComponTaxRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_COMPON_TAX_RIMB;
    }

    public void setWtliComponTaxRimb(AfDecimal wtliComponTaxRimb) {
        writeDecimalAsPacked(Pos.WTLI_COMPON_TAX_RIMB, wtliComponTaxRimb.copy());
    }

    public void setWtliComponTaxRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_COMPON_TAX_RIMB, Pos.WTLI_COMPON_TAX_RIMB);
    }

    /**Original name: WTLI-COMPON-TAX-RIMB<br>*/
    public AfDecimal getWtliComponTaxRimb() {
        return readPackedAsDecimal(Pos.WTLI_COMPON_TAX_RIMB, Len.Int.WTLI_COMPON_TAX_RIMB, Len.Fract.WTLI_COMPON_TAX_RIMB);
    }

    public byte[] getWtliComponTaxRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_COMPON_TAX_RIMB, Pos.WTLI_COMPON_TAX_RIMB);
        return buffer;
    }

    public void initWtliComponTaxRimbSpaces() {
        fill(Pos.WTLI_COMPON_TAX_RIMB, Len.WTLI_COMPON_TAX_RIMB, Types.SPACE_CHAR);
    }

    public void setWtliComponTaxRimbNull(String wtliComponTaxRimbNull) {
        writeString(Pos.WTLI_COMPON_TAX_RIMB_NULL, wtliComponTaxRimbNull, Len.WTLI_COMPON_TAX_RIMB_NULL);
    }

    /**Original name: WTLI-COMPON-TAX-RIMB-NULL<br>*/
    public String getWtliComponTaxRimbNull() {
        return readString(Pos.WTLI_COMPON_TAX_RIMB_NULL, Len.WTLI_COMPON_TAX_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_COMPON_TAX_RIMB = 1;
        public static final int WTLI_COMPON_TAX_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_COMPON_TAX_RIMB = 8;
        public static final int WTLI_COMPON_TAX_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_COMPON_TAX_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_COMPON_TAX_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
