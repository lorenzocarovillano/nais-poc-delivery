package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-ID-RAPP-ANA<br>
 * Variable: TIT-ID-RAPP-ANA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_ID_RAPP_ANA;
    }

    public void setTitIdRappAna(int titIdRappAna) {
        writeIntAsPacked(Pos.TIT_ID_RAPP_ANA, titIdRappAna, Len.Int.TIT_ID_RAPP_ANA);
    }

    public void setTitIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_ID_RAPP_ANA, Pos.TIT_ID_RAPP_ANA);
    }

    /**Original name: TIT-ID-RAPP-ANA<br>*/
    public int getTitIdRappAna() {
        return readPackedAsInt(Pos.TIT_ID_RAPP_ANA, Len.Int.TIT_ID_RAPP_ANA);
    }

    public byte[] getTitIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_ID_RAPP_ANA, Pos.TIT_ID_RAPP_ANA);
        return buffer;
    }

    public void setTitIdRappAnaNull(String titIdRappAnaNull) {
        writeString(Pos.TIT_ID_RAPP_ANA_NULL, titIdRappAnaNull, Len.TIT_ID_RAPP_ANA_NULL);
    }

    /**Original name: TIT-ID-RAPP-ANA-NULL<br>*/
    public String getTitIdRappAnaNull() {
        return readString(Pos.TIT_ID_RAPP_ANA_NULL, Len.TIT_ID_RAPP_ANA_NULL);
    }

    public String getTitIdRappAnaNullFormatted() {
        return Functions.padBlanks(getTitIdRappAnaNull(), Len.TIT_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_ID_RAPP_ANA = 1;
        public static final int TIT_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_ID_RAPP_ANA = 5;
        public static final int TIT_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
