package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPOL-DT-APPLZ-CONV<br>
 * Variable: WPOL-DT-APPLZ-CONV from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDtApplzConv extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDtApplzConv() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DT_APPLZ_CONV;
    }

    public void setWpolDtApplzConv(int wpolDtApplzConv) {
        writeIntAsPacked(Pos.WPOL_DT_APPLZ_CONV, wpolDtApplzConv, Len.Int.WPOL_DT_APPLZ_CONV);
    }

    public void setWpolDtApplzConvFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DT_APPLZ_CONV, Pos.WPOL_DT_APPLZ_CONV);
    }

    /**Original name: WPOL-DT-APPLZ-CONV<br>*/
    public int getWpolDtApplzConv() {
        return readPackedAsInt(Pos.WPOL_DT_APPLZ_CONV, Len.Int.WPOL_DT_APPLZ_CONV);
    }

    public byte[] getWpolDtApplzConvAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DT_APPLZ_CONV, Pos.WPOL_DT_APPLZ_CONV);
        return buffer;
    }

    public void setWpolDtApplzConvNull(String wpolDtApplzConvNull) {
        writeString(Pos.WPOL_DT_APPLZ_CONV_NULL, wpolDtApplzConvNull, Len.WPOL_DT_APPLZ_CONV_NULL);
    }

    /**Original name: WPOL-DT-APPLZ-CONV-NULL<br>*/
    public String getWpolDtApplzConvNull() {
        return readString(Pos.WPOL_DT_APPLZ_CONV_NULL, Len.WPOL_DT_APPLZ_CONV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DT_APPLZ_CONV = 1;
        public static final int WPOL_DT_APPLZ_CONV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DT_APPLZ_CONV = 5;
        public static final int WPOL_DT_APPLZ_CONV_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DT_APPLZ_CONV = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
