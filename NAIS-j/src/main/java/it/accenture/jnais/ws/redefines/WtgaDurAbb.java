package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DUR-ABB<br>
 * Variable: WTGA-DUR-ABB from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDurAbb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDurAbb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DUR_ABB;
    }

    public void setWtgaDurAbb(int wtgaDurAbb) {
        writeIntAsPacked(Pos.WTGA_DUR_ABB, wtgaDurAbb, Len.Int.WTGA_DUR_ABB);
    }

    public void setWtgaDurAbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DUR_ABB, Pos.WTGA_DUR_ABB);
    }

    /**Original name: WTGA-DUR-ABB<br>*/
    public int getWtgaDurAbb() {
        return readPackedAsInt(Pos.WTGA_DUR_ABB, Len.Int.WTGA_DUR_ABB);
    }

    public byte[] getWtgaDurAbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DUR_ABB, Pos.WTGA_DUR_ABB);
        return buffer;
    }

    public void initWtgaDurAbbSpaces() {
        fill(Pos.WTGA_DUR_ABB, Len.WTGA_DUR_ABB, Types.SPACE_CHAR);
    }

    public void setWtgaDurAbbNull(String wtgaDurAbbNull) {
        writeString(Pos.WTGA_DUR_ABB_NULL, wtgaDurAbbNull, Len.WTGA_DUR_ABB_NULL);
    }

    /**Original name: WTGA-DUR-ABB-NULL<br>*/
    public String getWtgaDurAbbNull() {
        return readString(Pos.WTGA_DUR_ABB_NULL, Len.WTGA_DUR_ABB_NULL);
    }

    public String getWtgaDurAbbNullFormatted() {
        return Functions.padBlanks(getWtgaDurAbbNull(), Len.WTGA_DUR_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_ABB = 1;
        public static final int WTGA_DUR_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DUR_ABB = 4;
        public static final int WTGA_DUR_ABB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DUR_ABB = 6;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
