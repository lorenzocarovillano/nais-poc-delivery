package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ETA-AA-3O-ASSTO<br>
 * Variable: WTGA-ETA-AA-3O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaEtaAa3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaEtaAa3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ETA_AA3O_ASSTO;
    }

    public void setWtgaEtaAa3oAssto(short wtgaEtaAa3oAssto) {
        writeShortAsPacked(Pos.WTGA_ETA_AA3O_ASSTO, wtgaEtaAa3oAssto, Len.Int.WTGA_ETA_AA3O_ASSTO);
    }

    public void setWtgaEtaAa3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ETA_AA3O_ASSTO, Pos.WTGA_ETA_AA3O_ASSTO);
    }

    /**Original name: WTGA-ETA-AA-3O-ASSTO<br>*/
    public short getWtgaEtaAa3oAssto() {
        return readPackedAsShort(Pos.WTGA_ETA_AA3O_ASSTO, Len.Int.WTGA_ETA_AA3O_ASSTO);
    }

    public byte[] getWtgaEtaAa3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ETA_AA3O_ASSTO, Pos.WTGA_ETA_AA3O_ASSTO);
        return buffer;
    }

    public void initWtgaEtaAa3oAsstoSpaces() {
        fill(Pos.WTGA_ETA_AA3O_ASSTO, Len.WTGA_ETA_AA3O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWtgaEtaAa3oAsstoNull(String wtgaEtaAa3oAsstoNull) {
        writeString(Pos.WTGA_ETA_AA3O_ASSTO_NULL, wtgaEtaAa3oAsstoNull, Len.WTGA_ETA_AA3O_ASSTO_NULL);
    }

    /**Original name: WTGA-ETA-AA-3O-ASSTO-NULL<br>*/
    public String getWtgaEtaAa3oAsstoNull() {
        return readString(Pos.WTGA_ETA_AA3O_ASSTO_NULL, Len.WTGA_ETA_AA3O_ASSTO_NULL);
    }

    public String getWtgaEtaAa3oAsstoNullFormatted() {
        return Functions.padBlanks(getWtgaEtaAa3oAsstoNull(), Len.WTGA_ETA_AA3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_AA3O_ASSTO = 1;
        public static final int WTGA_ETA_AA3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ETA_AA3O_ASSTO = 2;
        public static final int WTGA_ETA_AA3O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ETA_AA3O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
