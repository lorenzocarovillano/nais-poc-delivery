package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PLI-IMP-LIQ<br>
 * Variable: PLI-IMP-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PliImpLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PliImpLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PLI_IMP_LIQ;
    }

    public void setPliImpLiq(AfDecimal pliImpLiq) {
        writeDecimalAsPacked(Pos.PLI_IMP_LIQ, pliImpLiq.copy());
    }

    public void setPliImpLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PLI_IMP_LIQ, Pos.PLI_IMP_LIQ);
    }

    /**Original name: PLI-IMP-LIQ<br>*/
    public AfDecimal getPliImpLiq() {
        return readPackedAsDecimal(Pos.PLI_IMP_LIQ, Len.Int.PLI_IMP_LIQ, Len.Fract.PLI_IMP_LIQ);
    }

    public byte[] getPliImpLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PLI_IMP_LIQ, Pos.PLI_IMP_LIQ);
        return buffer;
    }

    public void setPliImpLiqNull(String pliImpLiqNull) {
        writeString(Pos.PLI_IMP_LIQ_NULL, pliImpLiqNull, Len.PLI_IMP_LIQ_NULL);
    }

    /**Original name: PLI-IMP-LIQ-NULL<br>*/
    public String getPliImpLiqNull() {
        return readString(Pos.PLI_IMP_LIQ_NULL, Len.PLI_IMP_LIQ_NULL);
    }

    public String getPliImpLiqNullFormatted() {
        return Functions.padBlanks(getPliImpLiqNull(), Len.PLI_IMP_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PLI_IMP_LIQ = 1;
        public static final int PLI_IMP_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PLI_IMP_LIQ = 8;
        public static final int PLI_IMP_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PLI_IMP_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PLI_IMP_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
