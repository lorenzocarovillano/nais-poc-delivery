package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-RIASTO-ECC<br>
 * Variable: WB03-PRE-RIASTO-ECC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreRiastoEcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreRiastoEcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_RIASTO_ECC;
    }

    public void setWb03PreRiastoEccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_RIASTO_ECC, Pos.WB03_PRE_RIASTO_ECC);
    }

    /**Original name: WB03-PRE-RIASTO-ECC<br>*/
    public AfDecimal getWb03PreRiastoEcc() {
        return readPackedAsDecimal(Pos.WB03_PRE_RIASTO_ECC, Len.Int.WB03_PRE_RIASTO_ECC, Len.Fract.WB03_PRE_RIASTO_ECC);
    }

    public byte[] getWb03PreRiastoEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_RIASTO_ECC, Pos.WB03_PRE_RIASTO_ECC);
        return buffer;
    }

    public void setWb03PreRiastoEccNull(String wb03PreRiastoEccNull) {
        writeString(Pos.WB03_PRE_RIASTO_ECC_NULL, wb03PreRiastoEccNull, Len.WB03_PRE_RIASTO_ECC_NULL);
    }

    /**Original name: WB03-PRE-RIASTO-ECC-NULL<br>*/
    public String getWb03PreRiastoEccNull() {
        return readString(Pos.WB03_PRE_RIASTO_ECC_NULL, Len.WB03_PRE_RIASTO_ECC_NULL);
    }

    public String getWb03PreRiastoEccNullFormatted() {
        return Functions.padBlanks(getWb03PreRiastoEccNull(), Len.WB03_PRE_RIASTO_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_RIASTO_ECC = 1;
        public static final int WB03_PRE_RIASTO_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_RIASTO_ECC = 8;
        public static final int WB03_PRE_RIASTO_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_RIASTO_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_RIASTO_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
