package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-VIS<br>
 * Variable: TCL-IMPB-VIS from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_VIS;
    }

    public void setTclImpbVis(AfDecimal tclImpbVis) {
        writeDecimalAsPacked(Pos.TCL_IMPB_VIS, tclImpbVis.copy());
    }

    public void setTclImpbVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_VIS, Pos.TCL_IMPB_VIS);
    }

    /**Original name: TCL-IMPB-VIS<br>*/
    public AfDecimal getTclImpbVis() {
        return readPackedAsDecimal(Pos.TCL_IMPB_VIS, Len.Int.TCL_IMPB_VIS, Len.Fract.TCL_IMPB_VIS);
    }

    public byte[] getTclImpbVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_VIS, Pos.TCL_IMPB_VIS);
        return buffer;
    }

    public void setTclImpbVisNull(String tclImpbVisNull) {
        writeString(Pos.TCL_IMPB_VIS_NULL, tclImpbVisNull, Len.TCL_IMPB_VIS_NULL);
    }

    /**Original name: TCL-IMPB-VIS-NULL<br>*/
    public String getTclImpbVisNull() {
        return readString(Pos.TCL_IMPB_VIS_NULL, Len.TCL_IMPB_VIS_NULL);
    }

    public String getTclImpbVisNullFormatted() {
        return Functions.padBlanks(getTclImpbVisNull(), Len.TCL_IMPB_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_VIS = 1;
        public static final int TCL_IMPB_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_VIS = 8;
        public static final int TCL_IMPB_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
