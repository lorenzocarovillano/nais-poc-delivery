package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvl301;
import it.accenture.jnais.copy.Wl30Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WL30-TAB-REINVST-POLI<br>
 * Variables: WL30-TAB-REINVST-POLI from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wl30TabReinvstPoli {

    //==== PROPERTIES ====
    //Original name: LCCVL301
    private Lccvl301 lccvl301 = new Lccvl301();

    //==== METHODS ====
    public void setWl30TabReinvstPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvl301.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvl301.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvl301.Len.Int.ID_PTF, 0));
        position += Lccvl301.Len.ID_PTF;
        lccvl301.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWl30TabReinvstPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvl301.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvl301.getIdPtf(), Lccvl301.Len.Int.ID_PTF, 0);
        position += Lccvl301.Len.ID_PTF;
        lccvl301.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWl30TabReinvstPoliSpaces() {
        lccvl301.initLccvl301Spaces();
    }

    public Lccvl301 getLccvl301() {
        return lccvl301;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL30_TAB_REINVST_POLI = WpolStatus.Len.STATUS + Lccvl301.Len.ID_PTF + Wl30Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
