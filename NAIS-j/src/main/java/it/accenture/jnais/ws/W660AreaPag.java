package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.W660DatiManfeeContesto;
import it.accenture.jnais.ws.enums.W660MfCalcolato;

/**Original name: W660-AREA-PAG<br>
 * Variable: W660-AREA-PAG from program LOAS0800<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class W660AreaPag extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: W660-DATI-MANFEE-CONTESTO
    private W660DatiManfeeContesto datiManfeeContesto = new W660DatiManfeeContesto();
    //Original name: W660-PERMANFEE
    private String permanfee = DefaultValues.stringVal(Len.PERMANFEE);
    //Original name: W660-MESIDIFFMFEE
    private String mesidiffmfee = DefaultValues.stringVal(Len.MESIDIFFMFEE);
    //Original name: W660-DECADELMFEE
    private String decadelmfee = DefaultValues.stringVal(Len.DECADELMFEE);
    //Original name: W660-MF-CALCOLATO
    private W660MfCalcolato mfCalcolato = new W660MfCalcolato();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W660_AREA_PAG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setW660AreaPagBytes(buf);
    }

    public String getW660AreaPagFormatted() {
        return getAreaVarInpFormatted();
    }

    public void setW660AreaPagBytes(byte[] buffer) {
        setW660AreaPagBytes(buffer, 1);
    }

    public byte[] getW660AreaPagBytes() {
        byte[] buffer = new byte[Len.W660_AREA_PAG];
        return getW660AreaPagBytes(buffer, 1);
    }

    public void setW660AreaPagBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaVarInpBytes(buffer, position);
    }

    public byte[] getW660AreaPagBytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaVarInpBytes(buffer, position);
        return buffer;
    }

    public String getAreaVarInpFormatted() {
        return MarshalByteExt.bufferToStr(getAreaVarInpBytes());
    }

    /**Original name: W660-AREA-VAR-INP<br>
	 * <pre>----------------------------------------------------------------*
	 *    PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
	 *    AREA VARIABILI E COSTANTI DI INPUT
	 *    SERVIZIO DI RIVALUTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getAreaVarInpBytes() {
        byte[] buffer = new byte[Len.AREA_VAR_INP];
        return getAreaVarInpBytes(buffer, 1);
    }

    public void setAreaVarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        datiManfeeContesto.setDatiManfeeContesto(MarshalByte.readString(buffer, position, W660DatiManfeeContesto.Len.DATI_MANFEE_CONTESTO));
        position += W660DatiManfeeContesto.Len.DATI_MANFEE_CONTESTO;
        permanfee = MarshalByte.readFixedString(buffer, position, Len.PERMANFEE);
        position += Len.PERMANFEE;
        mesidiffmfee = MarshalByte.readFixedString(buffer, position, Len.MESIDIFFMFEE);
        position += Len.MESIDIFFMFEE;
        decadelmfee = MarshalByte.readFixedString(buffer, position, Len.DECADELMFEE);
        position += Len.DECADELMFEE;
        mfCalcolato.setMfCalcolato(MarshalByte.readString(buffer, position, W660MfCalcolato.Len.MF_CALCOLATO));
    }

    public byte[] getAreaVarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, datiManfeeContesto.getDatiManfeeContesto(), W660DatiManfeeContesto.Len.DATI_MANFEE_CONTESTO);
        position += W660DatiManfeeContesto.Len.DATI_MANFEE_CONTESTO;
        MarshalByte.writeString(buffer, position, permanfee, Len.PERMANFEE);
        position += Len.PERMANFEE;
        MarshalByte.writeString(buffer, position, mesidiffmfee, Len.MESIDIFFMFEE);
        position += Len.MESIDIFFMFEE;
        MarshalByte.writeString(buffer, position, decadelmfee, Len.DECADELMFEE);
        position += Len.DECADELMFEE;
        MarshalByte.writeString(buffer, position, mfCalcolato.getMfCalcolato(), W660MfCalcolato.Len.MF_CALCOLATO);
        return buffer;
    }

    public void setPermanfee(int permanfee) {
        this.permanfee = NumericDisplay.asString(permanfee, Len.PERMANFEE);
    }

    public int getPermanfee() {
        return NumericDisplay.asInt(this.permanfee);
    }

    public String getPermanfeeFormatted() {
        return this.permanfee;
    }

    public void setMesidiffmfee(int mesidiffmfee) {
        this.mesidiffmfee = NumericDisplay.asString(mesidiffmfee, Len.MESIDIFFMFEE);
    }

    public int getMesidiffmfee() {
        return NumericDisplay.asInt(this.mesidiffmfee);
    }

    public String getMesidiffmfeeFormatted() {
        return this.mesidiffmfee;
    }

    public void setDecadelmfee(int decadelmfee) {
        this.decadelmfee = NumericDisplay.asString(decadelmfee, Len.DECADELMFEE);
    }

    public int getDecadelmfee() {
        return NumericDisplay.asInt(this.decadelmfee);
    }

    public String getDecadelmfeeFormatted() {
        return this.decadelmfee;
    }

    public W660DatiManfeeContesto getDatiManfeeContesto() {
        return datiManfeeContesto;
    }

    public W660MfCalcolato getMfCalcolato() {
        return mfCalcolato;
    }

    @Override
    public byte[] serialize() {
        return getW660AreaPagBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PERMANFEE = 5;
        public static final int MESIDIFFMFEE = 5;
        public static final int DECADELMFEE = 5;
        public static final int AREA_VAR_INP = W660DatiManfeeContesto.Len.DATI_MANFEE_CONTESTO + PERMANFEE + MESIDIFFMFEE + DECADELMFEE + W660MfCalcolato.Len.MF_CALCOLATO;
        public static final int W660_AREA_PAG = AREA_VAR_INP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
