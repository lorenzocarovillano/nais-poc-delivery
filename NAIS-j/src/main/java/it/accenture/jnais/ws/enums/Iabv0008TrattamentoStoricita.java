package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-TRATTAMENTO-STORICITA<br>
 * Variable: IABV0008-TRATTAMENTO-STORICITA from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008TrattamentoStoricita {

    //==== PROPERTIES ====
    private String value = "CPZ";
    public static final String EFFETTO = "EFF";
    public static final String COMPETENZA = "CPZ";

    //==== METHODS ====
    public String getTrattamentoStoricita() {
        return this.value;
    }
}
