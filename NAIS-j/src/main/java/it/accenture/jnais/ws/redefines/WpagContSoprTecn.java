package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-SOPR-TECN<br>
 * Variable: WPAG-CONT-SOPR-TECN from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContSoprTecn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContSoprTecn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_SOPR_TECN;
    }

    public void setWpagContSoprTecn(AfDecimal wpagContSoprTecn) {
        writeDecimalAsPacked(Pos.WPAG_CONT_SOPR_TECN, wpagContSoprTecn.copy());
    }

    public void setWpagContSoprTecnFormatted(String wpagContSoprTecn) {
        setWpagContSoprTecn(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_SOPR_TECN + Len.Fract.WPAG_CONT_SOPR_TECN, Len.Fract.WPAG_CONT_SOPR_TECN, wpagContSoprTecn));
    }

    public void setWpagContSoprTecnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_SOPR_TECN, Pos.WPAG_CONT_SOPR_TECN);
    }

    /**Original name: WPAG-CONT-SOPR-TECN<br>*/
    public AfDecimal getWpagContSoprTecn() {
        return readPackedAsDecimal(Pos.WPAG_CONT_SOPR_TECN, Len.Int.WPAG_CONT_SOPR_TECN, Len.Fract.WPAG_CONT_SOPR_TECN);
    }

    public byte[] getWpagContSoprTecnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_SOPR_TECN, Pos.WPAG_CONT_SOPR_TECN);
        return buffer;
    }

    public void initWpagContSoprTecnSpaces() {
        fill(Pos.WPAG_CONT_SOPR_TECN, Len.WPAG_CONT_SOPR_TECN, Types.SPACE_CHAR);
    }

    public void setWpagContSoprTecnNull(String wpagContSoprTecnNull) {
        writeString(Pos.WPAG_CONT_SOPR_TECN_NULL, wpagContSoprTecnNull, Len.WPAG_CONT_SOPR_TECN_NULL);
    }

    /**Original name: WPAG-CONT-SOPR-TECN-NULL<br>*/
    public String getWpagContSoprTecnNull() {
        return readString(Pos.WPAG_CONT_SOPR_TECN_NULL, Len.WPAG_CONT_SOPR_TECN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SOPR_TECN = 1;
        public static final int WPAG_CONT_SOPR_TECN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_SOPR_TECN = 8;
        public static final int WPAG_CONT_SOPR_TECN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SOPR_TECN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_SOPR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
