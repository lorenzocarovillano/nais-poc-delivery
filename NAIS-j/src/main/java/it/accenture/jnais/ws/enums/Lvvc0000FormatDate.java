package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LVVC0000-FORMAT-DATE<br>
 * Variable: LVVC0000-FORMAT-DATE from copybook LVVC0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lvvc0000FormatDate {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char AAAA_V9999999 = '1';
    public static final char AAA_V_GGG = '2';
    public static final char AAA_V_MM = '3';

    //==== METHODS ====
    public void setFormatDate(char formatDate) {
        this.value = formatDate;
    }

    public char getFormatDate() {
        return this.value;
    }

    public void setAaaVGgg() {
        value = AAA_V_GGG;
    }

    public void setLvvc0000AaaVMm() {
        value = AAA_V_MM;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORMAT_DATE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
