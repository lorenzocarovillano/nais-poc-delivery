package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCS-ID-MOVI-CHIU<br>
 * Variable: OCS-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcsIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcsIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCS_ID_MOVI_CHIU;
    }

    public void setOcsIdMoviChiu(int ocsIdMoviChiu) {
        writeIntAsPacked(Pos.OCS_ID_MOVI_CHIU, ocsIdMoviChiu, Len.Int.OCS_ID_MOVI_CHIU);
    }

    public void setOcsIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCS_ID_MOVI_CHIU, Pos.OCS_ID_MOVI_CHIU);
    }

    /**Original name: OCS-ID-MOVI-CHIU<br>*/
    public int getOcsIdMoviChiu() {
        return readPackedAsInt(Pos.OCS_ID_MOVI_CHIU, Len.Int.OCS_ID_MOVI_CHIU);
    }

    public byte[] getOcsIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCS_ID_MOVI_CHIU, Pos.OCS_ID_MOVI_CHIU);
        return buffer;
    }

    public void setOcsIdMoviChiuNull(String ocsIdMoviChiuNull) {
        writeString(Pos.OCS_ID_MOVI_CHIU_NULL, ocsIdMoviChiuNull, Len.OCS_ID_MOVI_CHIU_NULL);
    }

    /**Original name: OCS-ID-MOVI-CHIU-NULL<br>*/
    public String getOcsIdMoviChiuNull() {
        return readString(Pos.OCS_ID_MOVI_CHIU_NULL, Len.OCS_ID_MOVI_CHIU_NULL);
    }

    public String getOcsIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getOcsIdMoviChiuNull(), Len.OCS_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCS_ID_MOVI_CHIU = 1;
        public static final int OCS_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCS_ID_MOVI_CHIU = 5;
        public static final int OCS_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCS_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
