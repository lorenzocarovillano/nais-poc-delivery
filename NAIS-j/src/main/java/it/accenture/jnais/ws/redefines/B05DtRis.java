package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B05-DT-RIS<br>
 * Variable: B05-DT-RIS from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B05DtRis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B05DtRis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B05_DT_RIS;
    }

    public void setB05DtRis(int b05DtRis) {
        writeIntAsPacked(Pos.B05_DT_RIS, b05DtRis, Len.Int.B05_DT_RIS);
    }

    public void setB05DtRisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B05_DT_RIS, Pos.B05_DT_RIS);
    }

    /**Original name: B05-DT-RIS<br>*/
    public int getB05DtRis() {
        return readPackedAsInt(Pos.B05_DT_RIS, Len.Int.B05_DT_RIS);
    }

    public byte[] getB05DtRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B05_DT_RIS, Pos.B05_DT_RIS);
        return buffer;
    }

    public void setB05DtRisNull(String b05DtRisNull) {
        writeString(Pos.B05_DT_RIS_NULL, b05DtRisNull, Len.B05_DT_RIS_NULL);
    }

    /**Original name: B05-DT-RIS-NULL<br>*/
    public String getB05DtRisNull() {
        return readString(Pos.B05_DT_RIS_NULL, Len.B05_DT_RIS_NULL);
    }

    public String getB05DtRisNullFormatted() {
        return Functions.padBlanks(getB05DtRisNull(), Len.B05_DT_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B05_DT_RIS = 1;
        public static final int B05_DT_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B05_DT_RIS = 5;
        public static final int B05_DT_RIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B05_DT_RIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
