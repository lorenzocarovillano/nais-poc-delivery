package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-MM-DIFF<br>
 * Variable: WPMO-MM-DIFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoMmDiff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoMmDiff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_MM_DIFF;
    }

    public void setWpmoMmDiff(short wpmoMmDiff) {
        writeShortAsPacked(Pos.WPMO_MM_DIFF, wpmoMmDiff, Len.Int.WPMO_MM_DIFF);
    }

    public void setWpmoMmDiffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_MM_DIFF, Pos.WPMO_MM_DIFF);
    }

    /**Original name: WPMO-MM-DIFF<br>*/
    public short getWpmoMmDiff() {
        return readPackedAsShort(Pos.WPMO_MM_DIFF, Len.Int.WPMO_MM_DIFF);
    }

    public byte[] getWpmoMmDiffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_MM_DIFF, Pos.WPMO_MM_DIFF);
        return buffer;
    }

    public void initWpmoMmDiffSpaces() {
        fill(Pos.WPMO_MM_DIFF, Len.WPMO_MM_DIFF, Types.SPACE_CHAR);
    }

    public void setWpmoMmDiffNull(String wpmoMmDiffNull) {
        writeString(Pos.WPMO_MM_DIFF_NULL, wpmoMmDiffNull, Len.WPMO_MM_DIFF_NULL);
    }

    /**Original name: WPMO-MM-DIFF-NULL<br>*/
    public String getWpmoMmDiffNull() {
        return readString(Pos.WPMO_MM_DIFF_NULL, Len.WPMO_MM_DIFF_NULL);
    }

    public String getWpmoMmDiffNullFormatted() {
        return Functions.padBlanks(getWpmoMmDiffNull(), Len.WPMO_MM_DIFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_MM_DIFF = 1;
        public static final int WPMO_MM_DIFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_MM_DIFF = 2;
        public static final int WPMO_MM_DIFF_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_MM_DIFF = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
