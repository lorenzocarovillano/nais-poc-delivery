package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-IMP-VOLO<br>
 * Variable: WDTC-IMP-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_IMP_VOLO;
    }

    public void setWdtcImpVolo(AfDecimal wdtcImpVolo) {
        writeDecimalAsPacked(Pos.WDTC_IMP_VOLO, wdtcImpVolo.copy());
    }

    public void setWdtcImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_IMP_VOLO, Pos.WDTC_IMP_VOLO);
    }

    /**Original name: WDTC-IMP-VOLO<br>*/
    public AfDecimal getWdtcImpVolo() {
        return readPackedAsDecimal(Pos.WDTC_IMP_VOLO, Len.Int.WDTC_IMP_VOLO, Len.Fract.WDTC_IMP_VOLO);
    }

    public byte[] getWdtcImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_IMP_VOLO, Pos.WDTC_IMP_VOLO);
        return buffer;
    }

    public void initWdtcImpVoloSpaces() {
        fill(Pos.WDTC_IMP_VOLO, Len.WDTC_IMP_VOLO, Types.SPACE_CHAR);
    }

    public void setWdtcImpVoloNull(String wdtcImpVoloNull) {
        writeString(Pos.WDTC_IMP_VOLO_NULL, wdtcImpVoloNull, Len.WDTC_IMP_VOLO_NULL);
    }

    /**Original name: WDTC-IMP-VOLO-NULL<br>*/
    public String getWdtcImpVoloNull() {
        return readString(Pos.WDTC_IMP_VOLO_NULL, Len.WDTC_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_VOLO = 1;
        public static final int WDTC_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_VOLO = 8;
        public static final int WDTC_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
