package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvspg3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndMoviFinrio;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1460<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1460Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-SOPR-DI-GAR
    private IndMoviFinrio indSoprDiGar = new IndMoviFinrio();
    //Original name: IDBVSPG3
    private Idbvspg3 idbvspg3 = new Idbvspg3();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvspg3 getIdbvspg3() {
        return idbvspg3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndMoviFinrio getIndSoprDiGar() {
        return indSoprDiGar;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
