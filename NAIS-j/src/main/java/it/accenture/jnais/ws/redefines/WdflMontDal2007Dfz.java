package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-DAL2007-DFZ<br>
 * Variable: WDFL-MONT-DAL2007-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontDal2007Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontDal2007Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_DAL2007_DFZ;
    }

    public void setWdflMontDal2007Dfz(AfDecimal wdflMontDal2007Dfz) {
        writeDecimalAsPacked(Pos.WDFL_MONT_DAL2007_DFZ, wdflMontDal2007Dfz.copy());
    }

    public void setWdflMontDal2007DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_DAL2007_DFZ, Pos.WDFL_MONT_DAL2007_DFZ);
    }

    /**Original name: WDFL-MONT-DAL2007-DFZ<br>*/
    public AfDecimal getWdflMontDal2007Dfz() {
        return readPackedAsDecimal(Pos.WDFL_MONT_DAL2007_DFZ, Len.Int.WDFL_MONT_DAL2007_DFZ, Len.Fract.WDFL_MONT_DAL2007_DFZ);
    }

    public byte[] getWdflMontDal2007DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_DAL2007_DFZ, Pos.WDFL_MONT_DAL2007_DFZ);
        return buffer;
    }

    public void setWdflMontDal2007DfzNull(String wdflMontDal2007DfzNull) {
        writeString(Pos.WDFL_MONT_DAL2007_DFZ_NULL, wdflMontDal2007DfzNull, Len.WDFL_MONT_DAL2007_DFZ_NULL);
    }

    /**Original name: WDFL-MONT-DAL2007-DFZ-NULL<br>*/
    public String getWdflMontDal2007DfzNull() {
        return readString(Pos.WDFL_MONT_DAL2007_DFZ_NULL, Len.WDFL_MONT_DAL2007_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_DAL2007_DFZ = 1;
        public static final int WDFL_MONT_DAL2007_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_DAL2007_DFZ = 8;
        public static final int WDFL_MONT_DAL2007_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_DAL2007_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_DAL2007_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
