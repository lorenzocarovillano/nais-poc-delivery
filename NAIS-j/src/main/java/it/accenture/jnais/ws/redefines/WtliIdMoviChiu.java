package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTLI-ID-MOVI-CHIU<br>
 * Variable: WTLI-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_ID_MOVI_CHIU;
    }

    public void setWtliIdMoviChiu(int wtliIdMoviChiu) {
        writeIntAsPacked(Pos.WTLI_ID_MOVI_CHIU, wtliIdMoviChiu, Len.Int.WTLI_ID_MOVI_CHIU);
    }

    public void setWtliIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_ID_MOVI_CHIU, Pos.WTLI_ID_MOVI_CHIU);
    }

    /**Original name: WTLI-ID-MOVI-CHIU<br>*/
    public int getWtliIdMoviChiu() {
        return readPackedAsInt(Pos.WTLI_ID_MOVI_CHIU, Len.Int.WTLI_ID_MOVI_CHIU);
    }

    public byte[] getWtliIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_ID_MOVI_CHIU, Pos.WTLI_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWtliIdMoviChiuSpaces() {
        fill(Pos.WTLI_ID_MOVI_CHIU, Len.WTLI_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWtliIdMoviChiuNull(String wtliIdMoviChiuNull) {
        writeString(Pos.WTLI_ID_MOVI_CHIU_NULL, wtliIdMoviChiuNull, Len.WTLI_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WTLI-ID-MOVI-CHIU-NULL<br>*/
    public String getWtliIdMoviChiuNull() {
        return readString(Pos.WTLI_ID_MOVI_CHIU_NULL, Len.WTLI_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_ID_MOVI_CHIU = 1;
        public static final int WTLI_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_ID_MOVI_CHIU = 5;
        public static final int WTLI_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
