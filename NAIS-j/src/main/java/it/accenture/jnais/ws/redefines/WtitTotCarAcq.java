package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-CAR-ACQ<br>
 * Variable: WTIT-TOT-CAR-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_CAR_ACQ;
    }

    public void setWtitTotCarAcq(AfDecimal wtitTotCarAcq) {
        writeDecimalAsPacked(Pos.WTIT_TOT_CAR_ACQ, wtitTotCarAcq.copy());
    }

    public void setWtitTotCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_CAR_ACQ, Pos.WTIT_TOT_CAR_ACQ);
    }

    /**Original name: WTIT-TOT-CAR-ACQ<br>*/
    public AfDecimal getWtitTotCarAcq() {
        return readPackedAsDecimal(Pos.WTIT_TOT_CAR_ACQ, Len.Int.WTIT_TOT_CAR_ACQ, Len.Fract.WTIT_TOT_CAR_ACQ);
    }

    public byte[] getWtitTotCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_CAR_ACQ, Pos.WTIT_TOT_CAR_ACQ);
        return buffer;
    }

    public void initWtitTotCarAcqSpaces() {
        fill(Pos.WTIT_TOT_CAR_ACQ, Len.WTIT_TOT_CAR_ACQ, Types.SPACE_CHAR);
    }

    public void setWtitTotCarAcqNull(String wtitTotCarAcqNull) {
        writeString(Pos.WTIT_TOT_CAR_ACQ_NULL, wtitTotCarAcqNull, Len.WTIT_TOT_CAR_ACQ_NULL);
    }

    /**Original name: WTIT-TOT-CAR-ACQ-NULL<br>*/
    public String getWtitTotCarAcqNull() {
        return readString(Pos.WTIT_TOT_CAR_ACQ_NULL, Len.WTIT_TOT_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_ACQ = 1;
        public static final int WTIT_TOT_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_ACQ = 8;
        public static final int WTIT_TOT_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
