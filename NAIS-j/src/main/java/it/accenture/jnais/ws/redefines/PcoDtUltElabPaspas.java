package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-PASPAS<br>
 * Variable: PCO-DT-ULT-ELAB-PASPAS from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabPaspas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabPaspas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_PASPAS;
    }

    public void setPcoDtUltElabPaspas(int pcoDtUltElabPaspas) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_PASPAS, pcoDtUltElabPaspas, Len.Int.PCO_DT_ULT_ELAB_PASPAS);
    }

    public void setPcoDtUltElabPaspasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PASPAS, Pos.PCO_DT_ULT_ELAB_PASPAS);
    }

    /**Original name: PCO-DT-ULT-ELAB-PASPAS<br>*/
    public int getPcoDtUltElabPaspas() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_PASPAS, Len.Int.PCO_DT_ULT_ELAB_PASPAS);
    }

    public byte[] getPcoDtUltElabPaspasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PASPAS, Pos.PCO_DT_ULT_ELAB_PASPAS);
        return buffer;
    }

    public void initPcoDtUltElabPaspasHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_PASPAS, Len.PCO_DT_ULT_ELAB_PASPAS, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabPaspasNull(String pcoDtUltElabPaspasNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_PASPAS_NULL, pcoDtUltElabPaspasNull, Len.PCO_DT_ULT_ELAB_PASPAS_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-PASPAS-NULL<br>*/
    public String getPcoDtUltElabPaspasNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_PASPAS_NULL, Len.PCO_DT_ULT_ELAB_PASPAS_NULL);
    }

    public String getPcoDtUltElabPaspasNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabPaspasNull(), Len.PCO_DT_ULT_ELAB_PASPAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PASPAS = 1;
        public static final int PCO_DT_ULT_ELAB_PASPAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PASPAS = 5;
        public static final int PCO_DT_ULT_ELAB_PASPAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_PASPAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
