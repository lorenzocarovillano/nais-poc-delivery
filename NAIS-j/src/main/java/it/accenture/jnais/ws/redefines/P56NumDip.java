package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-NUM-DIP<br>
 * Variable: P56-NUM-DIP from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56NumDip extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56NumDip() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_NUM_DIP;
    }

    public void setP56NumDip(int p56NumDip) {
        writeIntAsPacked(Pos.P56_NUM_DIP, p56NumDip, Len.Int.P56_NUM_DIP);
    }

    public void setP56NumDipFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_NUM_DIP, Pos.P56_NUM_DIP);
    }

    /**Original name: P56-NUM-DIP<br>*/
    public int getP56NumDip() {
        return readPackedAsInt(Pos.P56_NUM_DIP, Len.Int.P56_NUM_DIP);
    }

    public byte[] getP56NumDipAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_NUM_DIP, Pos.P56_NUM_DIP);
        return buffer;
    }

    public void setP56NumDipNull(String p56NumDipNull) {
        writeString(Pos.P56_NUM_DIP_NULL, p56NumDipNull, Len.P56_NUM_DIP_NULL);
    }

    /**Original name: P56-NUM-DIP-NULL<br>*/
    public String getP56NumDipNull() {
        return readString(Pos.P56_NUM_DIP_NULL, Len.P56_NUM_DIP_NULL);
    }

    public String getP56NumDipNullFormatted() {
        return Functions.padBlanks(getP56NumDipNull(), Len.P56_NUM_DIP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_NUM_DIP = 1;
        public static final int P56_NUM_DIP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_NUM_DIP = 3;
        public static final int P56_NUM_DIP_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_NUM_DIP = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
