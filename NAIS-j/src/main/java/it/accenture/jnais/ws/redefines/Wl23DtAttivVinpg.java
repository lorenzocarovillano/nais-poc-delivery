package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL23-DT-ATTIV-VINPG<br>
 * Variable: WL23-DT-ATTIV-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23DtAttivVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23DtAttivVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_DT_ATTIV_VINPG;
    }

    public void setWl23DtAttivVinpg(int wl23DtAttivVinpg) {
        writeIntAsPacked(Pos.WL23_DT_ATTIV_VINPG, wl23DtAttivVinpg, Len.Int.WL23_DT_ATTIV_VINPG);
    }

    public void setWl23DtAttivVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_DT_ATTIV_VINPG, Pos.WL23_DT_ATTIV_VINPG);
    }

    /**Original name: WL23-DT-ATTIV-VINPG<br>*/
    public int getWl23DtAttivVinpg() {
        return readPackedAsInt(Pos.WL23_DT_ATTIV_VINPG, Len.Int.WL23_DT_ATTIV_VINPG);
    }

    public byte[] getWl23DtAttivVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_DT_ATTIV_VINPG, Pos.WL23_DT_ATTIV_VINPG);
        return buffer;
    }

    public void initWl23DtAttivVinpgSpaces() {
        fill(Pos.WL23_DT_ATTIV_VINPG, Len.WL23_DT_ATTIV_VINPG, Types.SPACE_CHAR);
    }

    public void setWl23DtAttivVinpgNull(String wl23DtAttivVinpgNull) {
        writeString(Pos.WL23_DT_ATTIV_VINPG_NULL, wl23DtAttivVinpgNull, Len.WL23_DT_ATTIV_VINPG_NULL);
    }

    /**Original name: WL23-DT-ATTIV-VINPG-NULL<br>*/
    public String getWl23DtAttivVinpgNull() {
        return readString(Pos.WL23_DT_ATTIV_VINPG_NULL, Len.WL23_DT_ATTIV_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_DT_ATTIV_VINPG = 1;
        public static final int WL23_DT_ATTIV_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_DT_ATTIV_VINPG = 5;
        public static final int WL23_DT_ATTIV_VINPG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_DT_ATTIV_VINPG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
