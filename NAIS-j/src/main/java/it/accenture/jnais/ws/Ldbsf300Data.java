package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Ldbvf301;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSF300<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsf300Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-P84-IMP-LRD-CEDOLE-LIQ
    private short indP84ImpLrdCedoleLiq = DefaultValues.BIN_SHORT_VAL;
    //Original name: P84-DT-LIQ-CED-DB
    private String p84DtLiqCedDb = DefaultValues.stringVal(Len.P84_DT_LIQ_CED_DB);
    //Original name: LDBVF301
    private Ldbvf301 ldbvf301 = new Ldbvf301();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndP84ImpLrdCedoleLiq(short indP84ImpLrdCedoleLiq) {
        this.indP84ImpLrdCedoleLiq = indP84ImpLrdCedoleLiq;
    }

    public short getIndP84ImpLrdCedoleLiq() {
        return this.indP84ImpLrdCedoleLiq;
    }

    public void setP84DtLiqCedDb(String p84DtLiqCedDb) {
        this.p84DtLiqCedDb = Functions.subString(p84DtLiqCedDb, Len.P84_DT_LIQ_CED_DB);
    }

    public String getP84DtLiqCedDb() {
        return this.p84DtLiqCedDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public Ldbvf301 getLdbvf301() {
        return ldbvf301;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P84_DT_LIQ_CED_DB = 10;
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
