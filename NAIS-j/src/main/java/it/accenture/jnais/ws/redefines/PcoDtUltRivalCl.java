package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-RIVAL-CL<br>
 * Variable: PCO-DT-ULT-RIVAL-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltRivalCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltRivalCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_RIVAL_CL;
    }

    public void setPcoDtUltRivalCl(int pcoDtUltRivalCl) {
        writeIntAsPacked(Pos.PCO_DT_ULT_RIVAL_CL, pcoDtUltRivalCl, Len.Int.PCO_DT_ULT_RIVAL_CL);
    }

    public void setPcoDtUltRivalClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_RIVAL_CL, Pos.PCO_DT_ULT_RIVAL_CL);
    }

    /**Original name: PCO-DT-ULT-RIVAL-CL<br>*/
    public int getPcoDtUltRivalCl() {
        return readPackedAsInt(Pos.PCO_DT_ULT_RIVAL_CL, Len.Int.PCO_DT_ULT_RIVAL_CL);
    }

    public byte[] getPcoDtUltRivalClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_RIVAL_CL, Pos.PCO_DT_ULT_RIVAL_CL);
        return buffer;
    }

    public void initPcoDtUltRivalClHighValues() {
        fill(Pos.PCO_DT_ULT_RIVAL_CL, Len.PCO_DT_ULT_RIVAL_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltRivalClNull(String pcoDtUltRivalClNull) {
        writeString(Pos.PCO_DT_ULT_RIVAL_CL_NULL, pcoDtUltRivalClNull, Len.PCO_DT_ULT_RIVAL_CL_NULL);
    }

    /**Original name: PCO-DT-ULT-RIVAL-CL-NULL<br>*/
    public String getPcoDtUltRivalClNull() {
        return readString(Pos.PCO_DT_ULT_RIVAL_CL_NULL, Len.PCO_DT_ULT_RIVAL_CL_NULL);
    }

    public String getPcoDtUltRivalClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltRivalClNull(), Len.PCO_DT_ULT_RIVAL_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RIVAL_CL = 1;
        public static final int PCO_DT_ULT_RIVAL_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_RIVAL_CL = 5;
        public static final int PCO_DT_ULT_RIVAL_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_RIVAL_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
