package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-NUM-QUO<br>
 * Variable: RDF-NUM-QUO from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfNumQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfNumQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_NUM_QUO;
    }

    public void setRdfNumQuo(AfDecimal rdfNumQuo) {
        writeDecimalAsPacked(Pos.RDF_NUM_QUO, rdfNumQuo.copy());
    }

    public void setRdfNumQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_NUM_QUO, Pos.RDF_NUM_QUO);
    }

    /**Original name: RDF-NUM-QUO<br>*/
    public AfDecimal getRdfNumQuo() {
        return readPackedAsDecimal(Pos.RDF_NUM_QUO, Len.Int.RDF_NUM_QUO, Len.Fract.RDF_NUM_QUO);
    }

    public byte[] getRdfNumQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_NUM_QUO, Pos.RDF_NUM_QUO);
        return buffer;
    }

    public void setRdfNumQuoNull(String rdfNumQuoNull) {
        writeString(Pos.RDF_NUM_QUO_NULL, rdfNumQuoNull, Len.RDF_NUM_QUO_NULL);
    }

    /**Original name: RDF-NUM-QUO-NULL<br>*/
    public String getRdfNumQuoNull() {
        return readString(Pos.RDF_NUM_QUO_NULL, Len.RDF_NUM_QUO_NULL);
    }

    public String getRdfNumQuoNullFormatted() {
        return Functions.padBlanks(getRdfNumQuoNull(), Len.RDF_NUM_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_NUM_QUO = 1;
        public static final int RDF_NUM_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_NUM_QUO = 7;
        public static final int RDF_NUM_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_NUM_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_NUM_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
