package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WGRZ-PC-OPZ<br>
 * Variable: WGRZ-PC-OPZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzPcOpz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzPcOpz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_PC_OPZ;
    }

    public void setWgrzPcOpz(AfDecimal wgrzPcOpz) {
        writeDecimalAsPacked(Pos.WGRZ_PC_OPZ, wgrzPcOpz.copy());
    }

    public void setWgrzPcOpzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_PC_OPZ, Pos.WGRZ_PC_OPZ);
    }

    /**Original name: WGRZ-PC-OPZ<br>*/
    public AfDecimal getWgrzPcOpz() {
        return readPackedAsDecimal(Pos.WGRZ_PC_OPZ, Len.Int.WGRZ_PC_OPZ, Len.Fract.WGRZ_PC_OPZ);
    }

    public byte[] getWgrzPcOpzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_PC_OPZ, Pos.WGRZ_PC_OPZ);
        return buffer;
    }

    public void initWgrzPcOpzSpaces() {
        fill(Pos.WGRZ_PC_OPZ, Len.WGRZ_PC_OPZ, Types.SPACE_CHAR);
    }

    public void setWgrzPcOpzNull(String wgrzPcOpzNull) {
        writeString(Pos.WGRZ_PC_OPZ_NULL, wgrzPcOpzNull, Len.WGRZ_PC_OPZ_NULL);
    }

    /**Original name: WGRZ-PC-OPZ-NULL<br>*/
    public String getWgrzPcOpzNull() {
        return readString(Pos.WGRZ_PC_OPZ_NULL, Len.WGRZ_PC_OPZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_PC_OPZ = 1;
        public static final int WGRZ_PC_OPZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_PC_OPZ = 4;
        public static final int WGRZ_PC_OPZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WGRZ_PC_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_PC_OPZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
