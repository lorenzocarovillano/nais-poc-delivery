package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COEFF-OPZ-REN<br>
 * Variable: B03-COEFF-OPZ-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CoeffOpzRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CoeffOpzRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COEFF_OPZ_REN;
    }

    public void setB03CoeffOpzRen(AfDecimal b03CoeffOpzRen) {
        writeDecimalAsPacked(Pos.B03_COEFF_OPZ_REN, b03CoeffOpzRen.copy());
    }

    public void setB03CoeffOpzRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COEFF_OPZ_REN, Pos.B03_COEFF_OPZ_REN);
    }

    /**Original name: B03-COEFF-OPZ-REN<br>*/
    public AfDecimal getB03CoeffOpzRen() {
        return readPackedAsDecimal(Pos.B03_COEFF_OPZ_REN, Len.Int.B03_COEFF_OPZ_REN, Len.Fract.B03_COEFF_OPZ_REN);
    }

    public byte[] getB03CoeffOpzRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COEFF_OPZ_REN, Pos.B03_COEFF_OPZ_REN);
        return buffer;
    }

    public void setB03CoeffOpzRenNull(String b03CoeffOpzRenNull) {
        writeString(Pos.B03_COEFF_OPZ_REN_NULL, b03CoeffOpzRenNull, Len.B03_COEFF_OPZ_REN_NULL);
    }

    /**Original name: B03-COEFF-OPZ-REN-NULL<br>*/
    public String getB03CoeffOpzRenNull() {
        return readString(Pos.B03_COEFF_OPZ_REN_NULL, Len.B03_COEFF_OPZ_REN_NULL);
    }

    public String getB03CoeffOpzRenNullFormatted() {
        return Functions.padBlanks(getB03CoeffOpzRenNull(), Len.B03_COEFF_OPZ_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COEFF_OPZ_REN = 1;
        public static final int B03_COEFF_OPZ_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COEFF_OPZ_REN = 4;
        public static final int B03_COEFF_OPZ_REN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_COEFF_OPZ_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COEFF_OPZ_REN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
