package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WadeTabAdes;

/**Original name: WADE-AREA-ADESIONE<br>
 * Variable: WADE-AREA-ADESIONE from program LOAS0800<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WadeAreaAdesioneLoas0800 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_ADE_MAXOCCURS = 1;
    //Original name: WADE-ELE-ADES-MAX
    private short eleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WADE-TAB-ADE
    private WadeTabAdes[] tabAde = new WadeTabAdes[TAB_ADE_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WadeAreaAdesioneLoas0800() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_AREA_ADESIONE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWadeAreaAdesioneBytes(buf);
    }

    public void init() {
        for (int tabAdeIdx = 1; tabAdeIdx <= TAB_ADE_MAXOCCURS; tabAdeIdx++) {
            tabAde[tabAdeIdx - 1] = new WadeTabAdes();
        }
    }

    public String getWadeAreaAdesioneFormatted() {
        return MarshalByteExt.bufferToStr(getWadeAreaAdesioneBytes());
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer) {
        setWadeAreaAdesioneBytes(buffer, 1);
    }

    public byte[] getWadeAreaAdesioneBytes() {
        byte[] buffer = new byte[Len.WADE_AREA_ADESIONE];
        return getWadeAreaAdesioneBytes(buffer, 1);
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        eleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_ADE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabAde[idx - 1].setWadeTabAdesBytes(buffer, position);
                position += WadeTabAdes.Len.WADE_TAB_ADES;
            }
            else {
                tabAde[idx - 1].initWadeTabAdesSpaces();
                position += WadeTabAdes.Len.WADE_TAB_ADES;
            }
        }
    }

    public byte[] getWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleAdesMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_ADE_MAXOCCURS; idx++) {
            tabAde[idx - 1].getWadeTabAdesBytes(buffer, position);
            position += WadeTabAdes.Len.WADE_TAB_ADES;
        }
        return buffer;
    }

    public void setEleAdesMax(short eleAdesMax) {
        this.eleAdesMax = eleAdesMax;
    }

    public short getEleAdesMax() {
        return this.eleAdesMax;
    }

    public WadeTabAdes getTabAde(int idx) {
        return tabAde[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWadeAreaAdesioneBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_ADES_MAX = 2;
        public static final int WADE_AREA_ADESIONE = ELE_ADES_MAX + WadeAreaAdesioneLoas0800.TAB_ADE_MAXOCCURS * WadeTabAdes.Len.WADE_TAB_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
