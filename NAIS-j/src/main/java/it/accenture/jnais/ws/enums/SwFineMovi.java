package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-FINE-MOVI<br>
 * Variable: SW-FINE-MOVI from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwFineMovi {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char MOVI = 'S';
    public static final char MOVI_NO = 'N';

    //==== METHODS ====
    public void setSwFineMovi(char swFineMovi) {
        this.value = swFineMovi;
    }

    public char getSwFineMovi() {
        return this.value;
    }

    public void setMovi() {
        value = MOVI;
    }

    public boolean isMoviNo() {
        return value == MOVI_NO;
    }

    public void setMoviNo() {
        value = MOVI_NO;
    }
}
