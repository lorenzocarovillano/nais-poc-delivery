package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TABELLA-STATI-JOB-ELEMENTS<br>
 * Variables: TABELLA-STATI-JOB-ELEMENTS from copybook IABV0007<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TabellaStatiJobElements {

    //==== PROPERTIES ====
    //Original name: JOB-COD-ELAB-STATE
    private char codElabState = DefaultValues.CHAR_VAL;
    //Original name: JOB-DES
    private String des = DefaultValues.stringVal(Len.DES);
    //Original name: JOB-FLAG-TO-EXECUTE
    private char flagToExecute = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setCodElabState(char codElabState) {
        this.codElabState = codElabState;
    }

    public char getCodElabState() {
        return this.codElabState;
    }

    public void setDes(String des) {
        this.des = Functions.subString(des, Len.DES);
    }

    public String getDes() {
        return this.des;
    }

    public void setFlagToExecute(char flagToExecute) {
        this.flagToExecute = flagToExecute;
    }

    public char getFlagToExecute() {
        return this.flagToExecute;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DES = 50;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
