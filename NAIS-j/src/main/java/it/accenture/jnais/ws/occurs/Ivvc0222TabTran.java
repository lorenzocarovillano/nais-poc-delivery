package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.copy.Ivvc0222;

/**Original name: IVVC0222-TAB-TRAN<br>
 * Variables: IVVC0222-TAB-TRAN from copybook LCCVTGAC<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0222TabTran implements ICopyable<Ivvc0222TabTran> {

    //==== PROPERTIES ====
    //Original name: IVVC0222
    private Ivvc0222 ivvc0222 = new Ivvc0222();

    //==== CONSTRUCTORS ====
    public Ivvc0222TabTran() {
    }

    public Ivvc0222TabTran(Ivvc0222TabTran ivvc0222TabTran) {
        this();
        this.ivvc0222 = ((Ivvc0222)ivvc0222TabTran.ivvc0222.copy());
    }

    //==== METHODS ====
    public void setIvvc0222TabTranBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0222.setIdTranche(MarshalByte.readPackedAsInt(buffer, position, Ivvc0222.Len.Int.ID_TRANCHE, 0));
        position += Ivvc0222.Len.ID_TRANCHE;
        ivvc0222.setTpTrch(MarshalByte.readString(buffer, position, Ivvc0222.Len.TP_TRCH));
        position += Ivvc0222.Len.TP_TRCH;
        ivvc0222.setEleFndMax(MarshalByte.readBinaryShort(buffer, position));
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= Ivvc0222.FND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ivvc0222.getFnd(idx).setFndBytes(buffer, position);
                position += Ivvc0222Fnd.Len.FND;
            }
            else {
                ivvc0222.getFnd(idx).initFndSpaces();
                position += Ivvc0222Fnd.Len.FND;
            }
        }
    }

    public byte[] getIvvc0222TabTranBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ivvc0222.getIdTranche(), Ivvc0222.Len.Int.ID_TRANCHE, 0);
        position += Ivvc0222.Len.ID_TRANCHE;
        MarshalByte.writeString(buffer, position, ivvc0222.getTpTrch(), Ivvc0222.Len.TP_TRCH);
        position += Ivvc0222.Len.TP_TRCH;
        MarshalByte.writeBinaryShort(buffer, position, ivvc0222.getEleFndMax());
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= Ivvc0222.FND_MAXOCCURS; idx++) {
            ivvc0222.getFnd(idx).getFndBytes(buffer, position);
            position += Ivvc0222Fnd.Len.FND;
        }
        return buffer;
    }

    public Ivvc0222TabTran initIvvc0222TabTranSpaces() {
        ivvc0222.initIvvc0222Spaces();
        return this;
    }

    public Ivvc0222TabTran copy() {
        return new Ivvc0222TabTran(this);
    }

    public Ivvc0222TabTran initIvvc0222TabTran() {
        ivvc0222.setIdTranche(0);
        ivvc0222.setTpTrch("");
        ivvc0222.setEleFndMax(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0222.FND_MAXOCCURS; idx0++) {
            ivvc0222.getFnd(idx0).setCodFnd("");
            ivvc0222.getFnd(idx0).setCtrvFnd(new AfDecimal(0, 15, 3));
        }
        return this;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IVVC0222_TAB_TRAN = Ivvc0222.Len.ID_TRANCHE + Ivvc0222.Len.TP_TRCH + Ivvc0222.Len.ELE_FND_MAX + Ivvc0222.FND_MAXOCCURS * Ivvc0222Fnd.Len.FND;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
