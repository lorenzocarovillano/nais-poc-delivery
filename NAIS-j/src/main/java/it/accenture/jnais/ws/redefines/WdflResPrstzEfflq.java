package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RES-PRSTZ-EFFLQ<br>
 * Variable: WDFL-RES-PRSTZ-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflResPrstzEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflResPrstzEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RES_PRSTZ_EFFLQ;
    }

    public void setWdflResPrstzEfflq(AfDecimal wdflResPrstzEfflq) {
        writeDecimalAsPacked(Pos.WDFL_RES_PRSTZ_EFFLQ, wdflResPrstzEfflq.copy());
    }

    public void setWdflResPrstzEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RES_PRSTZ_EFFLQ, Pos.WDFL_RES_PRSTZ_EFFLQ);
    }

    /**Original name: WDFL-RES-PRSTZ-EFFLQ<br>*/
    public AfDecimal getWdflResPrstzEfflq() {
        return readPackedAsDecimal(Pos.WDFL_RES_PRSTZ_EFFLQ, Len.Int.WDFL_RES_PRSTZ_EFFLQ, Len.Fract.WDFL_RES_PRSTZ_EFFLQ);
    }

    public byte[] getWdflResPrstzEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RES_PRSTZ_EFFLQ, Pos.WDFL_RES_PRSTZ_EFFLQ);
        return buffer;
    }

    public void setWdflResPrstzEfflqNull(String wdflResPrstzEfflqNull) {
        writeString(Pos.WDFL_RES_PRSTZ_EFFLQ_NULL, wdflResPrstzEfflqNull, Len.WDFL_RES_PRSTZ_EFFLQ_NULL);
    }

    /**Original name: WDFL-RES-PRSTZ-EFFLQ-NULL<br>*/
    public String getWdflResPrstzEfflqNull() {
        return readString(Pos.WDFL_RES_PRSTZ_EFFLQ_NULL, Len.WDFL_RES_PRSTZ_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRSTZ_EFFLQ = 1;
        public static final int WDFL_RES_PRSTZ_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRSTZ_EFFLQ = 8;
        public static final int WDFL_RES_PRSTZ_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRSTZ_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRSTZ_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
