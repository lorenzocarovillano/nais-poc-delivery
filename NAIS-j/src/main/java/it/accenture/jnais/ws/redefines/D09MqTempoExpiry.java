package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: D09-MQ-TEMPO-EXPIRY<br>
 * Variable: D09-MQ-TEMPO-EXPIRY from program LDBS6730<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class D09MqTempoExpiry extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public D09MqTempoExpiry() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D09_MQ_TEMPO_EXPIRY;
    }

    public void setD09MqTempoExpiry(long d09MqTempoExpiry) {
        writeLongAsPacked(Pos.D09_MQ_TEMPO_EXPIRY, d09MqTempoExpiry, Len.Int.D09_MQ_TEMPO_EXPIRY);
    }

    public void setD09MqTempoExpiryFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.D09_MQ_TEMPO_EXPIRY, Pos.D09_MQ_TEMPO_EXPIRY);
    }

    /**Original name: D09-MQ-TEMPO-EXPIRY<br>*/
    public long getD09MqTempoExpiry() {
        return readPackedAsLong(Pos.D09_MQ_TEMPO_EXPIRY, Len.Int.D09_MQ_TEMPO_EXPIRY);
    }

    public byte[] getD09MqTempoExpiryAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.D09_MQ_TEMPO_EXPIRY, Pos.D09_MQ_TEMPO_EXPIRY);
        return buffer;
    }

    public void setD09MqTempoExpiryNull(String d09MqTempoExpiryNull) {
        writeString(Pos.D09_MQ_TEMPO_EXPIRY_NULL, d09MqTempoExpiryNull, Len.D09_MQ_TEMPO_EXPIRY_NULL);
    }

    /**Original name: D09-MQ-TEMPO-EXPIRY-NULL<br>*/
    public String getD09MqTempoExpiryNull() {
        return readString(Pos.D09_MQ_TEMPO_EXPIRY_NULL, Len.D09_MQ_TEMPO_EXPIRY_NULL);
    }

    public String getD09MqTempoExpiryNullFormatted() {
        return Functions.padBlanks(getD09MqTempoExpiryNull(), Len.D09_MQ_TEMPO_EXPIRY_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int D09_MQ_TEMPO_EXPIRY = 1;
        public static final int D09_MQ_TEMPO_EXPIRY_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int D09_MQ_TEMPO_EXPIRY = 6;
        public static final int D09_MQ_TEMPO_EXPIRY_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D09_MQ_TEMPO_EXPIRY = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
