package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-WTGA-VAL<br>
 * Variable: WK-WTGA-VAL from program LOAS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkWtgaVal {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char Y = 'Y';
    public static final char N = 'N';

    //==== METHODS ====
    public void setWkWtgaVal(char wkWtgaVal) {
        this.value = wkWtgaVal;
    }

    public char getWkWtgaVal() {
        return this.value;
    }

    public boolean isY() {
        return value == Y;
    }

    public void setY() {
        value = Y;
    }

    public void setN() {
        value = N;
    }
}
