package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-ULTIMA-LETTURA<br>
 * Variable: FLAG-ULTIMA-LETTURA from program LVVS0002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagUltimaLettura {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_ULTIMA_LETTURA = 'S';
    public static final char NO_ULTIMA_LETTURA = 'N';

    //==== METHODS ====
    public void setFlagUltimaLettura(char flagUltimaLettura) {
        this.value = flagUltimaLettura;
    }

    public char getFlagUltimaLettura() {
        return this.value;
    }

    public boolean isSiUltimaLettura() {
        return value == SI_ULTIMA_LETTURA;
    }

    public void setSiUltimaLettura() {
        value = SI_ULTIMA_LETTURA;
    }

    public void setNoUltimaLettura() {
        value = NO_ULTIMA_LETTURA;
    }
}
