package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSI0021-INITIALIZE<br>
 * Variable: IDSI0021-INITIALIZE from copybook IDSI0021<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0021Initialize {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setInitialize(char initialize) {
        this.value = initialize;
    }

    public char getInitialize() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setIdsi0021InitializeSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int INITIALIZE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
