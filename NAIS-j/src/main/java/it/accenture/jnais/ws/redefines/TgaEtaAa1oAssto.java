package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ETA-AA-1O-ASSTO<br>
 * Variable: TGA-ETA-AA-1O-ASSTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaEtaAa1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaEtaAa1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ETA_AA1O_ASSTO;
    }

    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        writeShortAsPacked(Pos.TGA_ETA_AA1O_ASSTO, tgaEtaAa1oAssto, Len.Int.TGA_ETA_AA1O_ASSTO);
    }

    public void setTgaEtaAa1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ETA_AA1O_ASSTO, Pos.TGA_ETA_AA1O_ASSTO);
    }

    /**Original name: TGA-ETA-AA-1O-ASSTO<br>*/
    public short getTgaEtaAa1oAssto() {
        return readPackedAsShort(Pos.TGA_ETA_AA1O_ASSTO, Len.Int.TGA_ETA_AA1O_ASSTO);
    }

    public byte[] getTgaEtaAa1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ETA_AA1O_ASSTO, Pos.TGA_ETA_AA1O_ASSTO);
        return buffer;
    }

    public void setTgaEtaAa1oAsstoNull(String tgaEtaAa1oAsstoNull) {
        writeString(Pos.TGA_ETA_AA1O_ASSTO_NULL, tgaEtaAa1oAsstoNull, Len.TGA_ETA_AA1O_ASSTO_NULL);
    }

    /**Original name: TGA-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getTgaEtaAa1oAsstoNull() {
        return readString(Pos.TGA_ETA_AA1O_ASSTO_NULL, Len.TGA_ETA_AA1O_ASSTO_NULL);
    }

    public String getTgaEtaAa1oAsstoNullFormatted() {
        return Functions.padBlanks(getTgaEtaAa1oAsstoNull(), Len.TGA_ETA_AA1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ETA_AA1O_ASSTO = 1;
        public static final int TGA_ETA_AA1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ETA_AA1O_ASSTO = 2;
        public static final int TGA_ETA_AA1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ETA_AA1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
