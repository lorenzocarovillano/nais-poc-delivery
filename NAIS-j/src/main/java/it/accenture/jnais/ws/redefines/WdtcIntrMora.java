package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-INTR-MORA<br>
 * Variable: WDTC-INTR-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_INTR_MORA;
    }

    public void setWdtcIntrMora(AfDecimal wdtcIntrMora) {
        writeDecimalAsPacked(Pos.WDTC_INTR_MORA, wdtcIntrMora.copy());
    }

    public void setWdtcIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_INTR_MORA, Pos.WDTC_INTR_MORA);
    }

    /**Original name: WDTC-INTR-MORA<br>*/
    public AfDecimal getWdtcIntrMora() {
        return readPackedAsDecimal(Pos.WDTC_INTR_MORA, Len.Int.WDTC_INTR_MORA, Len.Fract.WDTC_INTR_MORA);
    }

    public byte[] getWdtcIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_INTR_MORA, Pos.WDTC_INTR_MORA);
        return buffer;
    }

    public void initWdtcIntrMoraSpaces() {
        fill(Pos.WDTC_INTR_MORA, Len.WDTC_INTR_MORA, Types.SPACE_CHAR);
    }

    public void setWdtcIntrMoraNull(String wdtcIntrMoraNull) {
        writeString(Pos.WDTC_INTR_MORA_NULL, wdtcIntrMoraNull, Len.WDTC_INTR_MORA_NULL);
    }

    /**Original name: WDTC-INTR-MORA-NULL<br>*/
    public String getWdtcIntrMoraNull() {
        return readString(Pos.WDTC_INTR_MORA_NULL, Len.WDTC_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_MORA = 1;
        public static final int WDTC_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_MORA = 8;
        public static final int WDTC_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
