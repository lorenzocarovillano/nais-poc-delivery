package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-ESE-IMPST-TFR<br>
 * Variable: DFA-IMP-ESE-IMPST-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpEseImpstTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpEseImpstTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_ESE_IMPST_TFR;
    }

    public void setDfaImpEseImpstTfr(AfDecimal dfaImpEseImpstTfr) {
        writeDecimalAsPacked(Pos.DFA_IMP_ESE_IMPST_TFR, dfaImpEseImpstTfr.copy());
    }

    public void setDfaImpEseImpstTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_ESE_IMPST_TFR, Pos.DFA_IMP_ESE_IMPST_TFR);
    }

    /**Original name: DFA-IMP-ESE-IMPST-TFR<br>*/
    public AfDecimal getDfaImpEseImpstTfr() {
        return readPackedAsDecimal(Pos.DFA_IMP_ESE_IMPST_TFR, Len.Int.DFA_IMP_ESE_IMPST_TFR, Len.Fract.DFA_IMP_ESE_IMPST_TFR);
    }

    public byte[] getDfaImpEseImpstTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_ESE_IMPST_TFR, Pos.DFA_IMP_ESE_IMPST_TFR);
        return buffer;
    }

    public void setDfaImpEseImpstTfrNull(String dfaImpEseImpstTfrNull) {
        writeString(Pos.DFA_IMP_ESE_IMPST_TFR_NULL, dfaImpEseImpstTfrNull, Len.DFA_IMP_ESE_IMPST_TFR_NULL);
    }

    /**Original name: DFA-IMP-ESE-IMPST-TFR-NULL<br>*/
    public String getDfaImpEseImpstTfrNull() {
        return readString(Pos.DFA_IMP_ESE_IMPST_TFR_NULL, Len.DFA_IMP_ESE_IMPST_TFR_NULL);
    }

    public String getDfaImpEseImpstTfrNullFormatted() {
        return Functions.padBlanks(getDfaImpEseImpstTfrNull(), Len.DFA_IMP_ESE_IMPST_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_ESE_IMPST_TFR = 1;
        public static final int DFA_IMP_ESE_IMPST_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_ESE_IMPST_TFR = 8;
        public static final int DFA_IMP_ESE_IMPST_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_ESE_IMPST_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_ESE_IMPST_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
