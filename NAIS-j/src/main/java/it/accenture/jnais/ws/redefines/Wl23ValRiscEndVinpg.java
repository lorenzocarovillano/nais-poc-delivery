package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL23-VAL-RISC-END-VINPG<br>
 * Variable: WL23-VAL-RISC-END-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23ValRiscEndVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23ValRiscEndVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_VAL_RISC_END_VINPG;
    }

    public void setWl23ValRiscEndVinpg(AfDecimal wl23ValRiscEndVinpg) {
        writeDecimalAsPacked(Pos.WL23_VAL_RISC_END_VINPG, wl23ValRiscEndVinpg.copy());
    }

    public void setWl23ValRiscEndVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_VAL_RISC_END_VINPG, Pos.WL23_VAL_RISC_END_VINPG);
    }

    /**Original name: WL23-VAL-RISC-END-VINPG<br>*/
    public AfDecimal getWl23ValRiscEndVinpg() {
        return readPackedAsDecimal(Pos.WL23_VAL_RISC_END_VINPG, Len.Int.WL23_VAL_RISC_END_VINPG, Len.Fract.WL23_VAL_RISC_END_VINPG);
    }

    public byte[] getWl23ValRiscEndVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_VAL_RISC_END_VINPG, Pos.WL23_VAL_RISC_END_VINPG);
        return buffer;
    }

    public void initWl23ValRiscEndVinpgSpaces() {
        fill(Pos.WL23_VAL_RISC_END_VINPG, Len.WL23_VAL_RISC_END_VINPG, Types.SPACE_CHAR);
    }

    public void setWl23ValRiscEndVinpgNull(String wl23ValRiscEndVinpgNull) {
        writeString(Pos.WL23_VAL_RISC_END_VINPG_NULL, wl23ValRiscEndVinpgNull, Len.WL23_VAL_RISC_END_VINPG_NULL);
    }

    /**Original name: WL23-VAL-RISC-END-VINPG-NULL<br>*/
    public String getWl23ValRiscEndVinpgNull() {
        return readString(Pos.WL23_VAL_RISC_END_VINPG_NULL, Len.WL23_VAL_RISC_END_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_VAL_RISC_END_VINPG = 1;
        public static final int WL23_VAL_RISC_END_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_VAL_RISC_END_VINPG = 8;
        public static final int WL23_VAL_RISC_END_VINPG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WL23_VAL_RISC_END_VINPG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_VAL_RISC_END_VINPG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
