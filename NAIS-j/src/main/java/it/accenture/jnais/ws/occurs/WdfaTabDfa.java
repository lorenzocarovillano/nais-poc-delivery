package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.Lccvdfa1;

/**Original name: WDFA-TAB-DFA<br>
 * Variables: WDFA-TAB-DFA from program LLBS0230<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WdfaTabDfa {

    //==== PROPERTIES ====
    //Original name: LCCVDFA1
    private Lccvdfa1 lccvdfa1 = new Lccvdfa1();

    //==== METHODS ====
    public Lccvdfa1 getLccvdfa1() {
        return lccvdfa1;
    }
}
