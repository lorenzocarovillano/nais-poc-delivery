package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRV-RICOR<br>
 * Variable: WPAG-PRV-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRV_RICOR;
    }

    public void setWpagPrvRicor(AfDecimal wpagPrvRicor) {
        writeDecimalAsPacked(Pos.WPAG_PRV_RICOR, wpagPrvRicor.copy());
    }

    public void setWpagPrvRicorFormatted(String wpagPrvRicor) {
        setWpagPrvRicor(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRV_RICOR + Len.Fract.WPAG_PRV_RICOR, Len.Fract.WPAG_PRV_RICOR, wpagPrvRicor));
    }

    public void setWpagPrvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRV_RICOR, Pos.WPAG_PRV_RICOR);
    }

    /**Original name: WPAG-PRV-RICOR<br>*/
    public AfDecimal getWpagPrvRicor() {
        return readPackedAsDecimal(Pos.WPAG_PRV_RICOR, Len.Int.WPAG_PRV_RICOR, Len.Fract.WPAG_PRV_RICOR);
    }

    public byte[] getWpagPrvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRV_RICOR, Pos.WPAG_PRV_RICOR);
        return buffer;
    }

    public void initWpagPrvRicorSpaces() {
        fill(Pos.WPAG_PRV_RICOR, Len.WPAG_PRV_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagPrvRicorNull(String wpagPrvRicorNull) {
        writeString(Pos.WPAG_PRV_RICOR_NULL, wpagPrvRicorNull, Len.WPAG_PRV_RICOR_NULL);
    }

    /**Original name: WPAG-PRV-RICOR-NULL<br>*/
    public String getWpagPrvRicorNull() {
        return readString(Pos.WPAG_PRV_RICOR_NULL, Len.WPAG_PRV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_RICOR = 1;
        public static final int WPAG_PRV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_RICOR = 8;
        public static final int WPAG_PRV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
