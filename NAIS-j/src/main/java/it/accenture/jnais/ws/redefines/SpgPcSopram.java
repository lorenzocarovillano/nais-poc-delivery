package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: SPG-PC-SOPRAM<br>
 * Variable: SPG-PC-SOPRAM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SpgPcSopram extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SpgPcSopram() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SPG_PC_SOPRAM;
    }

    public void setSpgPcSopram(AfDecimal spgPcSopram) {
        writeDecimalAsPacked(Pos.SPG_PC_SOPRAM, spgPcSopram.copy());
    }

    public void setSpgPcSopramFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SPG_PC_SOPRAM, Pos.SPG_PC_SOPRAM);
    }

    /**Original name: SPG-PC-SOPRAM<br>*/
    public AfDecimal getSpgPcSopram() {
        return readPackedAsDecimal(Pos.SPG_PC_SOPRAM, Len.Int.SPG_PC_SOPRAM, Len.Fract.SPG_PC_SOPRAM);
    }

    public byte[] getSpgPcSopramAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SPG_PC_SOPRAM, Pos.SPG_PC_SOPRAM);
        return buffer;
    }

    public void setSpgPcSopramNull(String spgPcSopramNull) {
        writeString(Pos.SPG_PC_SOPRAM_NULL, spgPcSopramNull, Len.SPG_PC_SOPRAM_NULL);
    }

    /**Original name: SPG-PC-SOPRAM-NULL<br>*/
    public String getSpgPcSopramNull() {
        return readString(Pos.SPG_PC_SOPRAM_NULL, Len.SPG_PC_SOPRAM_NULL);
    }

    public String getSpgPcSopramNullFormatted() {
        return Functions.padBlanks(getSpgPcSopramNull(), Len.SPG_PC_SOPRAM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SPG_PC_SOPRAM = 1;
        public static final int SPG_PC_SOPRAM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SPG_PC_SOPRAM = 8;
        public static final int SPG_PC_SOPRAM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SPG_PC_SOPRAM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int SPG_PC_SOPRAM = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
