package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-WRITE<br>
 * Variable: WCOM-WRITE from copybook LCCC0261<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomWrite {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WRITE);
    public static final String NIENTE = "";
    public static final String OK = "OK";
    public static final String ERR = "ER";

    //==== METHODS ====
    public void setWrite(String write) {
        this.value = Functions.subString(write, Len.WRITE);
    }

    public String getWrite() {
        return this.value;
    }

    public void setWcomWriteNiente() {
        value = NIENTE;
    }

    public boolean isWcomWriteOk() {
        return value.equals(OK);
    }

    public void setWcomWriteOk() {
        value = OK;
    }

    public boolean isErr() {
        return value.equals(ERR);
    }

    public void setWcomWriteErr() {
        value = ERR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRITE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
