package it.accenture.jnais.ws;

import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AdesDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndDettTitCont;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSPOL0<br>
 * Generated as a class for rule WS.<br>*/
public class Idbspol0Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-BUFFER-TABLE
    private String wsBufferTable = "";
    //Original name: WS-ID-MOVI-CRZ
    private int wsIdMoviCrz = 0;
    //Original name: WK-ID-OGG
    private String wkIdOgg = "000000000";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-POLI
    private IndDettTitCont indPoli = new IndDettTitCont();
    //Original name: POLI-DB
    private AdesDb poliDb = new AdesDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsBufferTable(String wsBufferTable) {
        this.wsBufferTable = Functions.subString(wsBufferTable, Len.WS_BUFFER_TABLE);
    }

    public String getWsBufferTable() {
        return this.wsBufferTable;
    }

    public String getWsBufferTableFormatted() {
        return Functions.padBlanks(getWsBufferTable(), Len.WS_BUFFER_TABLE);
    }

    public void setWsIdMoviCrz(int wsIdMoviCrz) {
        this.wsIdMoviCrz = wsIdMoviCrz;
    }

    public int getWsIdMoviCrz() {
        return this.wsIdMoviCrz;
    }

    public void setWkIdOgg(int wkIdOgg) {
        this.wkIdOgg = NumericDisplay.asString(wkIdOgg, Len.WK_ID_OGG);
    }

    public void setWkIdOggFormatted(String wkIdOgg) {
        this.wkIdOgg = Trunc.toUnsignedNumeric(wkIdOgg, Len.WK_ID_OGG);
    }

    public int getWkIdOgg() {
        return NumericDisplay.asInt(this.wkIdOgg);
    }

    public String getWkIdOggFormatted() {
        return this.wkIdOgg;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndDettTitCont getIndPoli() {
        return indPoli;
    }

    public AdesDb getPoliDb() {
        return poliDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WK_ID_OGG = 9;
        public static final int WS_BUFFER_TABLE = 2000;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
