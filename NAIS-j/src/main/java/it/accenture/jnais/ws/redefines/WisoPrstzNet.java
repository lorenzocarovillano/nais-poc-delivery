package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-PRSTZ-NET<br>
 * Variable: WISO-PRSTZ-NET from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoPrstzNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoPrstzNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_PRSTZ_NET;
    }

    public void setWisoPrstzNet(AfDecimal wisoPrstzNet) {
        writeDecimalAsPacked(Pos.WISO_PRSTZ_NET, wisoPrstzNet.copy());
    }

    public void setWisoPrstzNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_PRSTZ_NET, Pos.WISO_PRSTZ_NET);
    }

    /**Original name: WISO-PRSTZ-NET<br>*/
    public AfDecimal getWisoPrstzNet() {
        return readPackedAsDecimal(Pos.WISO_PRSTZ_NET, Len.Int.WISO_PRSTZ_NET, Len.Fract.WISO_PRSTZ_NET);
    }

    public byte[] getWisoPrstzNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_PRSTZ_NET, Pos.WISO_PRSTZ_NET);
        return buffer;
    }

    public void initWisoPrstzNetSpaces() {
        fill(Pos.WISO_PRSTZ_NET, Len.WISO_PRSTZ_NET, Types.SPACE_CHAR);
    }

    public void setWisoPrstzNetNull(String wisoPrstzNetNull) {
        writeString(Pos.WISO_PRSTZ_NET_NULL, wisoPrstzNetNull, Len.WISO_PRSTZ_NET_NULL);
    }

    /**Original name: WISO-PRSTZ-NET-NULL<br>*/
    public String getWisoPrstzNetNull() {
        return readString(Pos.WISO_PRSTZ_NET_NULL, Len.WISO_PRSTZ_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_PRSTZ_NET = 1;
        public static final int WISO_PRSTZ_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_PRSTZ_NET = 8;
        public static final int WISO_PRSTZ_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_PRSTZ_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_PRSTZ_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
