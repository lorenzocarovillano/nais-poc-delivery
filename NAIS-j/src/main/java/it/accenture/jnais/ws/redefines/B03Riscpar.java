package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RISCPAR<br>
 * Variable: B03-RISCPAR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Riscpar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Riscpar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RISCPAR;
    }

    public void setB03Riscpar(AfDecimal b03Riscpar) {
        writeDecimalAsPacked(Pos.B03_RISCPAR, b03Riscpar.copy());
    }

    public void setB03RiscparFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RISCPAR, Pos.B03_RISCPAR);
    }

    /**Original name: B03-RISCPAR<br>*/
    public AfDecimal getB03Riscpar() {
        return readPackedAsDecimal(Pos.B03_RISCPAR, Len.Int.B03_RISCPAR, Len.Fract.B03_RISCPAR);
    }

    public byte[] getB03RiscparAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RISCPAR, Pos.B03_RISCPAR);
        return buffer;
    }

    public void setB03RiscparNull(String b03RiscparNull) {
        writeString(Pos.B03_RISCPAR_NULL, b03RiscparNull, Len.B03_RISCPAR_NULL);
    }

    /**Original name: B03-RISCPAR-NULL<br>*/
    public String getB03RiscparNull() {
        return readString(Pos.B03_RISCPAR_NULL, Len.B03_RISCPAR_NULL);
    }

    public String getB03RiscparNullFormatted() {
        return Functions.padBlanks(getB03RiscparNull(), Len.B03_RISCPAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RISCPAR = 1;
        public static final int B03_RISCPAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RISCPAR = 8;
        public static final int B03_RISCPAR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RISCPAR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RISCPAR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
