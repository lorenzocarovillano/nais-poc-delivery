package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-IMP-TRASFE<br>
 * Variable: WDTC-IMP-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_IMP_TRASFE;
    }

    public void setWdtcImpTrasfe(AfDecimal wdtcImpTrasfe) {
        writeDecimalAsPacked(Pos.WDTC_IMP_TRASFE, wdtcImpTrasfe.copy());
    }

    public void setWdtcImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_IMP_TRASFE, Pos.WDTC_IMP_TRASFE);
    }

    /**Original name: WDTC-IMP-TRASFE<br>*/
    public AfDecimal getWdtcImpTrasfe() {
        return readPackedAsDecimal(Pos.WDTC_IMP_TRASFE, Len.Int.WDTC_IMP_TRASFE, Len.Fract.WDTC_IMP_TRASFE);
    }

    public byte[] getWdtcImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_IMP_TRASFE, Pos.WDTC_IMP_TRASFE);
        return buffer;
    }

    public void initWdtcImpTrasfeSpaces() {
        fill(Pos.WDTC_IMP_TRASFE, Len.WDTC_IMP_TRASFE, Types.SPACE_CHAR);
    }

    public void setWdtcImpTrasfeNull(String wdtcImpTrasfeNull) {
        writeString(Pos.WDTC_IMP_TRASFE_NULL, wdtcImpTrasfeNull, Len.WDTC_IMP_TRASFE_NULL);
    }

    /**Original name: WDTC-IMP-TRASFE-NULL<br>*/
    public String getWdtcImpTrasfeNull() {
        return readString(Pos.WDTC_IMP_TRASFE_NULL, Len.WDTC_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_TRASFE = 1;
        public static final int WDTC_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_TRASFE = 8;
        public static final int WDTC_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
