package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DT-EFF-STAB<br>
 * Variable: TGA-DT-EFF-STAB from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDtEffStab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDtEffStab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DT_EFF_STAB;
    }

    public void setTgaDtEffStab(int tgaDtEffStab) {
        writeIntAsPacked(Pos.TGA_DT_EFF_STAB, tgaDtEffStab, Len.Int.TGA_DT_EFF_STAB);
    }

    public void setTgaDtEffStabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DT_EFF_STAB, Pos.TGA_DT_EFF_STAB);
    }

    /**Original name: TGA-DT-EFF-STAB<br>*/
    public int getTgaDtEffStab() {
        return readPackedAsInt(Pos.TGA_DT_EFF_STAB, Len.Int.TGA_DT_EFF_STAB);
    }

    public byte[] getTgaDtEffStabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DT_EFF_STAB, Pos.TGA_DT_EFF_STAB);
        return buffer;
    }

    public void setTgaDtEffStabNull(String tgaDtEffStabNull) {
        writeString(Pos.TGA_DT_EFF_STAB_NULL, tgaDtEffStabNull, Len.TGA_DT_EFF_STAB_NULL);
    }

    /**Original name: TGA-DT-EFF-STAB-NULL<br>*/
    public String getTgaDtEffStabNull() {
        return readString(Pos.TGA_DT_EFF_STAB_NULL, Len.TGA_DT_EFF_STAB_NULL);
    }

    public String getTgaDtEffStabNullFormatted() {
        return Functions.padBlanks(getTgaDtEffStabNull(), Len.TGA_DT_EFF_STAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DT_EFF_STAB = 1;
        public static final int TGA_DT_EFF_STAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DT_EFF_STAB = 5;
        public static final int TGA_DT_EFF_STAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DT_EFF_STAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
