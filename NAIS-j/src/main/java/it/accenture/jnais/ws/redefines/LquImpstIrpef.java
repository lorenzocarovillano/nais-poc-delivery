package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-IRPEF<br>
 * Variable: LQU-IMPST-IRPEF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstIrpef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstIrpef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_IRPEF;
    }

    public void setLquImpstIrpef(AfDecimal lquImpstIrpef) {
        writeDecimalAsPacked(Pos.LQU_IMPST_IRPEF, lquImpstIrpef.copy());
    }

    public void setLquImpstIrpefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_IRPEF, Pos.LQU_IMPST_IRPEF);
    }

    /**Original name: LQU-IMPST-IRPEF<br>*/
    public AfDecimal getLquImpstIrpef() {
        return readPackedAsDecimal(Pos.LQU_IMPST_IRPEF, Len.Int.LQU_IMPST_IRPEF, Len.Fract.LQU_IMPST_IRPEF);
    }

    public byte[] getLquImpstIrpefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_IRPEF, Pos.LQU_IMPST_IRPEF);
        return buffer;
    }

    public void setLquImpstIrpefNull(String lquImpstIrpefNull) {
        writeString(Pos.LQU_IMPST_IRPEF_NULL, lquImpstIrpefNull, Len.LQU_IMPST_IRPEF_NULL);
    }

    /**Original name: LQU-IMPST-IRPEF-NULL<br>*/
    public String getLquImpstIrpefNull() {
        return readString(Pos.LQU_IMPST_IRPEF_NULL, Len.LQU_IMPST_IRPEF_NULL);
    }

    public String getLquImpstIrpefNullFormatted() {
        return Functions.padBlanks(getLquImpstIrpefNull(), Len.LQU_IMPST_IRPEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_IRPEF = 1;
        public static final int LQU_IMPST_IRPEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_IRPEF = 8;
        public static final int LQU_IMPST_IRPEF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_IRPEF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_IRPEF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
