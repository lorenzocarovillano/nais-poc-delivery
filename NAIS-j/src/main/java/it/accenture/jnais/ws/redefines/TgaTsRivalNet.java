package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-TS-RIVAL-NET<br>
 * Variable: TGA-TS-RIVAL-NET from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaTsRivalNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaTsRivalNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_TS_RIVAL_NET;
    }

    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        writeDecimalAsPacked(Pos.TGA_TS_RIVAL_NET, tgaTsRivalNet.copy());
    }

    public void setTgaTsRivalNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_TS_RIVAL_NET, Pos.TGA_TS_RIVAL_NET);
    }

    /**Original name: TGA-TS-RIVAL-NET<br>*/
    public AfDecimal getTgaTsRivalNet() {
        return readPackedAsDecimal(Pos.TGA_TS_RIVAL_NET, Len.Int.TGA_TS_RIVAL_NET, Len.Fract.TGA_TS_RIVAL_NET);
    }

    public byte[] getTgaTsRivalNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_TS_RIVAL_NET, Pos.TGA_TS_RIVAL_NET);
        return buffer;
    }

    public void setTgaTsRivalNetNull(String tgaTsRivalNetNull) {
        writeString(Pos.TGA_TS_RIVAL_NET_NULL, tgaTsRivalNetNull, Len.TGA_TS_RIVAL_NET_NULL);
    }

    /**Original name: TGA-TS-RIVAL-NET-NULL<br>*/
    public String getTgaTsRivalNetNull() {
        return readString(Pos.TGA_TS_RIVAL_NET_NULL, Len.TGA_TS_RIVAL_NET_NULL);
    }

    public String getTgaTsRivalNetNullFormatted() {
        return Functions.padBlanks(getTgaTsRivalNetNull(), Len.TGA_TS_RIVAL_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_TS_RIVAL_NET = 1;
        public static final int TGA_TS_RIVAL_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_TS_RIVAL_NET = 8;
        public static final int TGA_TS_RIVAL_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_TS_RIVAL_NET = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_TS_RIVAL_NET = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
