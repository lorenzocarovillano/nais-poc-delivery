package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-PILDI-MM-C<br>
 * Variable: WPCO-DT-ULTC-PILDI-MM-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcPildiMmC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcPildiMmC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_PILDI_MM_C;
    }

    public void setWpcoDtUltcPildiMmC(int wpcoDtUltcPildiMmC) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_PILDI_MM_C, wpcoDtUltcPildiMmC, Len.Int.WPCO_DT_ULTC_PILDI_MM_C);
    }

    public void setDpcoDtUltcPildiMmCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_MM_C, Pos.WPCO_DT_ULTC_PILDI_MM_C);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-MM-C<br>*/
    public int getWpcoDtUltcPildiMmC() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_PILDI_MM_C, Len.Int.WPCO_DT_ULTC_PILDI_MM_C);
    }

    public byte[] getWpcoDtUltcPildiMmCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_MM_C, Pos.WPCO_DT_ULTC_PILDI_MM_C);
        return buffer;
    }

    public void setWpcoDtUltcPildiMmCNull(String wpcoDtUltcPildiMmCNull) {
        writeString(Pos.WPCO_DT_ULTC_PILDI_MM_C_NULL, wpcoDtUltcPildiMmCNull, Len.WPCO_DT_ULTC_PILDI_MM_C_NULL);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-MM-C-NULL<br>*/
    public String getWpcoDtUltcPildiMmCNull() {
        return readString(Pos.WPCO_DT_ULTC_PILDI_MM_C_NULL, Len.WPCO_DT_ULTC_PILDI_MM_C_NULL);
    }

    public String getWpcoDtUltcPildiMmCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcPildiMmCNull(), Len.WPCO_DT_ULTC_PILDI_MM_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_MM_C = 1;
        public static final int WPCO_DT_ULTC_PILDI_MM_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_MM_C = 5;
        public static final int WPCO_DT_ULTC_PILDI_MM_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_PILDI_MM_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
