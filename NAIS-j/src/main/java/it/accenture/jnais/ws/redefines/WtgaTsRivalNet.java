package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-TS-RIVAL-NET<br>
 * Variable: WTGA-TS-RIVAL-NET from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaTsRivalNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaTsRivalNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_TS_RIVAL_NET;
    }

    public void setWtgaTsRivalNet(AfDecimal wtgaTsRivalNet) {
        writeDecimalAsPacked(Pos.WTGA_TS_RIVAL_NET, wtgaTsRivalNet.copy());
    }

    public void setWtgaTsRivalNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_TS_RIVAL_NET, Pos.WTGA_TS_RIVAL_NET);
    }

    /**Original name: WTGA-TS-RIVAL-NET<br>*/
    public AfDecimal getWtgaTsRivalNet() {
        return readPackedAsDecimal(Pos.WTGA_TS_RIVAL_NET, Len.Int.WTGA_TS_RIVAL_NET, Len.Fract.WTGA_TS_RIVAL_NET);
    }

    public byte[] getWtgaTsRivalNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_TS_RIVAL_NET, Pos.WTGA_TS_RIVAL_NET);
        return buffer;
    }

    public void initWtgaTsRivalNetSpaces() {
        fill(Pos.WTGA_TS_RIVAL_NET, Len.WTGA_TS_RIVAL_NET, Types.SPACE_CHAR);
    }

    public void setWtgaTsRivalNetNull(String wtgaTsRivalNetNull) {
        writeString(Pos.WTGA_TS_RIVAL_NET_NULL, wtgaTsRivalNetNull, Len.WTGA_TS_RIVAL_NET_NULL);
    }

    /**Original name: WTGA-TS-RIVAL-NET-NULL<br>*/
    public String getWtgaTsRivalNetNull() {
        return readString(Pos.WTGA_TS_RIVAL_NET_NULL, Len.WTGA_TS_RIVAL_NET_NULL);
    }

    public String getWtgaTsRivalNetNullFormatted() {
        return Functions.padBlanks(getWtgaTsRivalNetNull(), Len.WTGA_TS_RIVAL_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_TS_RIVAL_NET = 1;
        public static final int WTGA_TS_RIVAL_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_TS_RIVAL_NET = 8;
        public static final int WTGA_TS_RIVAL_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_TS_RIVAL_NET = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_TS_RIVAL_NET = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
