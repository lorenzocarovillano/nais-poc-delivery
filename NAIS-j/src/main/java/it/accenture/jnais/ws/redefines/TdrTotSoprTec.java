package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-SOPR-TEC<br>
 * Variable: TDR-TOT-SOPR-TEC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_SOPR_TEC;
    }

    public void setTdrTotSoprTec(AfDecimal tdrTotSoprTec) {
        writeDecimalAsPacked(Pos.TDR_TOT_SOPR_TEC, tdrTotSoprTec.copy());
    }

    public void setTdrTotSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_SOPR_TEC, Pos.TDR_TOT_SOPR_TEC);
    }

    /**Original name: TDR-TOT-SOPR-TEC<br>*/
    public AfDecimal getTdrTotSoprTec() {
        return readPackedAsDecimal(Pos.TDR_TOT_SOPR_TEC, Len.Int.TDR_TOT_SOPR_TEC, Len.Fract.TDR_TOT_SOPR_TEC);
    }

    public byte[] getTdrTotSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_SOPR_TEC, Pos.TDR_TOT_SOPR_TEC);
        return buffer;
    }

    public void setTdrTotSoprTecNull(String tdrTotSoprTecNull) {
        writeString(Pos.TDR_TOT_SOPR_TEC_NULL, tdrTotSoprTecNull, Len.TDR_TOT_SOPR_TEC_NULL);
    }

    /**Original name: TDR-TOT-SOPR-TEC-NULL<br>*/
    public String getTdrTotSoprTecNull() {
        return readString(Pos.TDR_TOT_SOPR_TEC_NULL, Len.TDR_TOT_SOPR_TEC_NULL);
    }

    public String getTdrTotSoprTecNullFormatted() {
        return Functions.padBlanks(getTdrTotSoprTecNull(), Len.TDR_TOT_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_TEC = 1;
        public static final int TDR_TOT_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_SOPR_TEC = 8;
        public static final int TDR_TOT_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
