package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-ZIL-T<br>
 * Variable: W-B03-RIS-ZIL-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisZilTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisZilTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_ZIL_T;
    }

    public void setwB03RisZilT(AfDecimal wB03RisZilT) {
        writeDecimalAsPacked(Pos.W_B03_RIS_ZIL_T, wB03RisZilT.copy());
    }

    /**Original name: W-B03-RIS-ZIL-T<br>*/
    public AfDecimal getwB03RisZilT() {
        return readPackedAsDecimal(Pos.W_B03_RIS_ZIL_T, Len.Int.W_B03_RIS_ZIL_T, Len.Fract.W_B03_RIS_ZIL_T);
    }

    public byte[] getwB03RisZilTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_ZIL_T, Pos.W_B03_RIS_ZIL_T);
        return buffer;
    }

    public void setwB03RisZilTNull(String wB03RisZilTNull) {
        writeString(Pos.W_B03_RIS_ZIL_T_NULL, wB03RisZilTNull, Len.W_B03_RIS_ZIL_T_NULL);
    }

    /**Original name: W-B03-RIS-ZIL-T-NULL<br>*/
    public String getwB03RisZilTNull() {
        return readString(Pos.W_B03_RIS_ZIL_T_NULL, Len.W_B03_RIS_ZIL_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_ZIL_T = 1;
        public static final int W_B03_RIS_ZIL_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_ZIL_T = 8;
        public static final int W_B03_RIS_ZIL_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_ZIL_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_ZIL_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
