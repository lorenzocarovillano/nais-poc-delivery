package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-FLAG-FINE-ELE<br>
 * Variable: WS-FLAG-FINE-ELE from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFlagFineEle {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_FLAG_FINE_ELE);
    public static final String ALTRI_ELEMENTI = "0";
    public static final String FINE_ELEMENTI_ADESIONE = "1";
    public static final String FINE_ELEMENTI_TRANCHE = "2";

    //==== METHODS ====
    public void setWsFlagFineEle(short wsFlagFineEle) {
        this.value = NumericDisplay.asString(wsFlagFineEle, Len.WS_FLAG_FINE_ELE);
    }

    public void setWsFlagFineEleFormatted(String wsFlagFineEle) {
        this.value = Trunc.toUnsignedNumeric(wsFlagFineEle, Len.WS_FLAG_FINE_ELE);
    }

    public short getWsFlagFineEle() {
        return NumericDisplay.asShort(this.value);
    }

    public String getWsFlagFineEleFormatted() {
        return this.value;
    }

    public void setAltriElementi() {
        setWsFlagFineEleFormatted(ALTRI_ELEMENTI);
    }

    public boolean isFineElementiTranche() {
        return getWsFlagFineEleFormatted().equals(FINE_ELEMENTI_TRANCHE);
    }

    public void setFineElementiTranche() {
        setWsFlagFineEleFormatted(FINE_ELEMENTI_TRANCHE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_FLAG_FINE_ELE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
