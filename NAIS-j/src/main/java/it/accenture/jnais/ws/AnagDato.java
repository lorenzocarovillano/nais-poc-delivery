package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IAnagDato;
import it.accenture.jnais.ws.redefines.AdaLunghezzaDato;
import it.accenture.jnais.ws.redefines.AdaPrecisioneDato;

/**Original name: ANAG-DATO<br>
 * Variable: ANAG-DATO from copybook IDBVADA1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AnagDato extends SerializableParameter implements IAnagDato {

    //==== PROPERTIES ====
    //Original name: ADA-COD-COMPAGNIA-ANIA
    private int adaCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: ADA-COD-DATO
    private String adaCodDato = DefaultValues.stringVal(Len.ADA_COD_DATO);
    //Original name: ADA-DESC-DATO-LEN
    private short adaDescDatoLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: ADA-DESC-DATO
    private String adaDescDato = DefaultValues.stringVal(Len.ADA_DESC_DATO);
    //Original name: ADA-TIPO-DATO
    private String adaTipoDato = DefaultValues.stringVal(Len.ADA_TIPO_DATO);
    //Original name: ADA-LUNGHEZZA-DATO
    private AdaLunghezzaDato adaLunghezzaDato = new AdaLunghezzaDato();
    //Original name: ADA-PRECISIONE-DATO
    private AdaPrecisioneDato adaPrecisioneDato = new AdaPrecisioneDato();
    //Original name: ADA-COD-DOMINIO
    private String adaCodDominio = DefaultValues.stringVal(Len.ADA_COD_DOMINIO);
    //Original name: ADA-FORMATTAZIONE-DATO
    private String adaFormattazioneDato = DefaultValues.stringVal(Len.ADA_FORMATTAZIONE_DATO);
    //Original name: ADA-DS-UTENTE
    private String adaDsUtente = DefaultValues.stringVal(Len.ADA_DS_UTENTE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ANAG_DATO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAnagDatoBytes(buf);
    }

    public void setAnagDatoBytes(byte[] buffer) {
        setAnagDatoBytes(buffer, 1);
    }

    public byte[] getAnagDatoBytes() {
        byte[] buffer = new byte[Len.ANAG_DATO];
        return getAnagDatoBytes(buffer, 1);
    }

    public void setAnagDatoBytes(byte[] buffer, int offset) {
        int position = offset;
        adaCodCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ADA_COD_COMPAGNIA_ANIA, 0);
        position += Len.ADA_COD_COMPAGNIA_ANIA;
        adaCodDato = MarshalByte.readString(buffer, position, Len.ADA_COD_DATO);
        position += Len.ADA_COD_DATO;
        setAdaDescDatoVcharBytes(buffer, position);
        position += Len.ADA_DESC_DATO_VCHAR;
        adaTipoDato = MarshalByte.readString(buffer, position, Len.ADA_TIPO_DATO);
        position += Len.ADA_TIPO_DATO;
        adaLunghezzaDato.setAdaLunghezzaDatoFromBuffer(buffer, position);
        position += AdaLunghezzaDato.Len.ADA_LUNGHEZZA_DATO;
        adaPrecisioneDato.setAdaPrecisioneDatoFromBuffer(buffer, position);
        position += AdaPrecisioneDato.Len.ADA_PRECISIONE_DATO;
        adaCodDominio = MarshalByte.readString(buffer, position, Len.ADA_COD_DOMINIO);
        position += Len.ADA_COD_DOMINIO;
        adaFormattazioneDato = MarshalByte.readString(buffer, position, Len.ADA_FORMATTAZIONE_DATO);
        position += Len.ADA_FORMATTAZIONE_DATO;
        adaDsUtente = MarshalByte.readString(buffer, position, Len.ADA_DS_UTENTE);
    }

    public byte[] getAnagDatoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, adaCodCompagniaAnia, Len.Int.ADA_COD_COMPAGNIA_ANIA, 0);
        position += Len.ADA_COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, adaCodDato, Len.ADA_COD_DATO);
        position += Len.ADA_COD_DATO;
        getAdaDescDatoVcharBytes(buffer, position);
        position += Len.ADA_DESC_DATO_VCHAR;
        MarshalByte.writeString(buffer, position, adaTipoDato, Len.ADA_TIPO_DATO);
        position += Len.ADA_TIPO_DATO;
        adaLunghezzaDato.getAdaLunghezzaDatoAsBuffer(buffer, position);
        position += AdaLunghezzaDato.Len.ADA_LUNGHEZZA_DATO;
        adaPrecisioneDato.getAdaPrecisioneDatoAsBuffer(buffer, position);
        position += AdaPrecisioneDato.Len.ADA_PRECISIONE_DATO;
        MarshalByte.writeString(buffer, position, adaCodDominio, Len.ADA_COD_DOMINIO);
        position += Len.ADA_COD_DOMINIO;
        MarshalByte.writeString(buffer, position, adaFormattazioneDato, Len.ADA_FORMATTAZIONE_DATO);
        position += Len.ADA_FORMATTAZIONE_DATO;
        MarshalByte.writeString(buffer, position, adaDsUtente, Len.ADA_DS_UTENTE);
        return buffer;
    }

    public void setAdaCodCompagniaAnia(int adaCodCompagniaAnia) {
        this.adaCodCompagniaAnia = adaCodCompagniaAnia;
    }

    public int getAdaCodCompagniaAnia() {
        return this.adaCodCompagniaAnia;
    }

    public void setAdaCodDato(String adaCodDato) {
        this.adaCodDato = Functions.subString(adaCodDato, Len.ADA_COD_DATO);
    }

    public String getAdaCodDato() {
        return this.adaCodDato;
    }

    public void setAdaDescDatoVcharFormatted(String data) {
        byte[] buffer = new byte[Len.ADA_DESC_DATO_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.ADA_DESC_DATO_VCHAR);
        setAdaDescDatoVcharBytes(buffer, 1);
    }

    public String getAdaDescDatoVcharFormatted() {
        return MarshalByteExt.bufferToStr(getAdaDescDatoVcharBytes());
    }

    /**Original name: ADA-DESC-DATO-VCHAR<br>*/
    public byte[] getAdaDescDatoVcharBytes() {
        byte[] buffer = new byte[Len.ADA_DESC_DATO_VCHAR];
        return getAdaDescDatoVcharBytes(buffer, 1);
    }

    public void setAdaDescDatoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        adaDescDatoLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        adaDescDato = MarshalByte.readString(buffer, position, Len.ADA_DESC_DATO);
    }

    public byte[] getAdaDescDatoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, adaDescDatoLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, adaDescDato, Len.ADA_DESC_DATO);
        return buffer;
    }

    public void setAdaDescDatoLen(short adaDescDatoLen) {
        this.adaDescDatoLen = adaDescDatoLen;
    }

    public short getAdaDescDatoLen() {
        return this.adaDescDatoLen;
    }

    public void setAdaDescDato(String adaDescDato) {
        this.adaDescDato = Functions.subString(adaDescDato, Len.ADA_DESC_DATO);
    }

    public String getAdaDescDato() {
        return this.adaDescDato;
    }

    public void setAdaTipoDato(String adaTipoDato) {
        this.adaTipoDato = Functions.subString(adaTipoDato, Len.ADA_TIPO_DATO);
    }

    public String getAdaTipoDato() {
        return this.adaTipoDato;
    }

    public void setAdaCodDominio(String adaCodDominio) {
        this.adaCodDominio = Functions.subString(adaCodDominio, Len.ADA_COD_DOMINIO);
    }

    public String getAdaCodDominio() {
        return this.adaCodDominio;
    }

    public void setAdaFormattazioneDato(String adaFormattazioneDato) {
        this.adaFormattazioneDato = Functions.subString(adaFormattazioneDato, Len.ADA_FORMATTAZIONE_DATO);
    }

    public String getAdaFormattazioneDato() {
        return this.adaFormattazioneDato;
    }

    public void setAdaDsUtente(String adaDsUtente) {
        this.adaDsUtente = Functions.subString(adaDsUtente, Len.ADA_DS_UTENTE);
    }

    public String getAdaDsUtente() {
        return this.adaDsUtente;
    }

    public AdaLunghezzaDato getAdaLunghezzaDato() {
        return adaLunghezzaDato;
    }

    public AdaPrecisioneDato getAdaPrecisioneDato() {
        return adaPrecisioneDato;
    }

    @Override
    public int getCodCompagniaAnia() {
        throw new FieldNotMappedException("codCompagniaAnia");
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        throw new FieldNotMappedException("codCompagniaAnia");
    }

    @Override
    public String getCodDato() {
        throw new FieldNotMappedException("codDato");
    }

    @Override
    public void setCodDato(String codDato) {
        throw new FieldNotMappedException("codDato");
    }

    @Override
    public String getCodDominio() {
        throw new FieldNotMappedException("codDominio");
    }

    @Override
    public void setCodDominio(String codDominio) {
        throw new FieldNotMappedException("codDominio");
    }

    @Override
    public String getCodDominioObj() {
        return getCodDominio();
    }

    @Override
    public void setCodDominioObj(String codDominioObj) {
        setCodDominio(codDominioObj);
    }

    @Override
    public String getDescDatoVchar() {
        throw new FieldNotMappedException("descDatoVchar");
    }

    @Override
    public void setDescDatoVchar(String descDatoVchar) {
        throw new FieldNotMappedException("descDatoVchar");
    }

    @Override
    public String getDescDatoVcharObj() {
        return getDescDatoVchar();
    }

    @Override
    public void setDescDatoVcharObj(String descDatoVcharObj) {
        setDescDatoVchar(descDatoVcharObj);
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public String getFormattazioneDato() {
        throw new FieldNotMappedException("formattazioneDato");
    }

    @Override
    public void setFormattazioneDato(String formattazioneDato) {
        throw new FieldNotMappedException("formattazioneDato");
    }

    @Override
    public String getFormattazioneDatoObj() {
        return getFormattazioneDato();
    }

    @Override
    public void setFormattazioneDatoObj(String formattazioneDatoObj) {
        setFormattazioneDato(formattazioneDatoObj);
    }

    @Override
    public int getLunghezzaDato() {
        throw new FieldNotMappedException("lunghezzaDato");
    }

    @Override
    public void setLunghezzaDato(int lunghezzaDato) {
        throw new FieldNotMappedException("lunghezzaDato");
    }

    @Override
    public Integer getLunghezzaDatoObj() {
        return ((Integer)getLunghezzaDato());
    }

    @Override
    public void setLunghezzaDatoObj(Integer lunghezzaDatoObj) {
        setLunghezzaDato(((int)lunghezzaDatoObj));
    }

    @Override
    public short getPrecisioneDato() {
        throw new FieldNotMappedException("precisioneDato");
    }

    @Override
    public void setPrecisioneDato(short precisioneDato) {
        throw new FieldNotMappedException("precisioneDato");
    }

    @Override
    public Short getPrecisioneDatoObj() {
        return ((Short)getPrecisioneDato());
    }

    @Override
    public void setPrecisioneDatoObj(Short precisioneDatoObj) {
        setPrecisioneDato(((short)precisioneDatoObj));
    }

    @Override
    public String getTipoDato() {
        throw new FieldNotMappedException("tipoDato");
    }

    @Override
    public void setTipoDato(String tipoDato) {
        throw new FieldNotMappedException("tipoDato");
    }

    @Override
    public String getTipoDatoObj() {
        return getTipoDato();
    }

    @Override
    public void setTipoDatoObj(String tipoDatoObj) {
        setTipoDato(tipoDatoObj);
    }

    @Override
    public byte[] serialize() {
        return getAnagDatoBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ADA_COD_COMPAGNIA_ANIA = 3;
        public static final int ADA_COD_DATO = 30;
        public static final int ADA_DESC_DATO_LEN = 2;
        public static final int ADA_DESC_DATO = 250;
        public static final int ADA_DESC_DATO_VCHAR = ADA_DESC_DATO_LEN + ADA_DESC_DATO;
        public static final int ADA_TIPO_DATO = 2;
        public static final int ADA_COD_DOMINIO = 30;
        public static final int ADA_FORMATTAZIONE_DATO = 20;
        public static final int ADA_DS_UTENTE = 20;
        public static final int ANAG_DATO = ADA_COD_COMPAGNIA_ANIA + ADA_COD_DATO + ADA_DESC_DATO_VCHAR + ADA_TIPO_DATO + AdaLunghezzaDato.Len.ADA_LUNGHEZZA_DATO + AdaPrecisioneDato.Len.ADA_PRECISIONE_DATO + ADA_COD_DOMINIO + ADA_FORMATTAZIONE_DATO + ADA_DS_UTENTE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADA_COD_COMPAGNIA_ANIA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
