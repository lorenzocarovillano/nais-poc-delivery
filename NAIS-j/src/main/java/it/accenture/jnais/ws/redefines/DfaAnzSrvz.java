package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ANZ-SRVZ<br>
 * Variable: DFA-ANZ-SRVZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaAnzSrvz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaAnzSrvz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ANZ_SRVZ;
    }

    public void setDfaAnzSrvz(short dfaAnzSrvz) {
        writeShortAsPacked(Pos.DFA_ANZ_SRVZ, dfaAnzSrvz, Len.Int.DFA_ANZ_SRVZ);
    }

    public void setDfaAnzSrvzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ANZ_SRVZ, Pos.DFA_ANZ_SRVZ);
    }

    /**Original name: DFA-ANZ-SRVZ<br>*/
    public short getDfaAnzSrvz() {
        return readPackedAsShort(Pos.DFA_ANZ_SRVZ, Len.Int.DFA_ANZ_SRVZ);
    }

    public byte[] getDfaAnzSrvzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ANZ_SRVZ, Pos.DFA_ANZ_SRVZ);
        return buffer;
    }

    public void setDfaAnzSrvzNull(String dfaAnzSrvzNull) {
        writeString(Pos.DFA_ANZ_SRVZ_NULL, dfaAnzSrvzNull, Len.DFA_ANZ_SRVZ_NULL);
    }

    /**Original name: DFA-ANZ-SRVZ-NULL<br>*/
    public String getDfaAnzSrvzNull() {
        return readString(Pos.DFA_ANZ_SRVZ_NULL, Len.DFA_ANZ_SRVZ_NULL);
    }

    public String getDfaAnzSrvzNullFormatted() {
        return Functions.padBlanks(getDfaAnzSrvzNull(), Len.DFA_ANZ_SRVZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ANZ_SRVZ = 1;
        public static final int DFA_ANZ_SRVZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ANZ_SRVZ = 3;
        public static final int DFA_ANZ_SRVZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ANZ_SRVZ = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
