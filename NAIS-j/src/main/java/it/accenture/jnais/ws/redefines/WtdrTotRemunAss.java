package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-REMUN-ASS<br>
 * Variable: WTDR-TOT-REMUN-ASS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_REMUN_ASS;
    }

    public void setWtdrTotRemunAss(AfDecimal wtdrTotRemunAss) {
        writeDecimalAsPacked(Pos.WTDR_TOT_REMUN_ASS, wtdrTotRemunAss.copy());
    }

    /**Original name: WTDR-TOT-REMUN-ASS<br>*/
    public AfDecimal getWtdrTotRemunAss() {
        return readPackedAsDecimal(Pos.WTDR_TOT_REMUN_ASS, Len.Int.WTDR_TOT_REMUN_ASS, Len.Fract.WTDR_TOT_REMUN_ASS);
    }

    public void setWtdrTotRemunAssNull(String wtdrTotRemunAssNull) {
        writeString(Pos.WTDR_TOT_REMUN_ASS_NULL, wtdrTotRemunAssNull, Len.WTDR_TOT_REMUN_ASS_NULL);
    }

    /**Original name: WTDR-TOT-REMUN-ASS-NULL<br>*/
    public String getWtdrTotRemunAssNull() {
        return readString(Pos.WTDR_TOT_REMUN_ASS_NULL, Len.WTDR_TOT_REMUN_ASS_NULL);
    }

    public String getWtdrTotRemunAssNullFormatted() {
        return Functions.padBlanks(getWtdrTotRemunAssNull(), Len.WTDR_TOT_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_REMUN_ASS = 1;
        public static final int WTDR_TOT_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_REMUN_ASS = 8;
        public static final int WTDR_TOT_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
