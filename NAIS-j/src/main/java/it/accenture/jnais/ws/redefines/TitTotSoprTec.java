package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SOPR-TEC<br>
 * Variable: TIT-TOT-SOPR-TEC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SOPR_TEC;
    }

    public void setTitTotSoprTec(AfDecimal titTotSoprTec) {
        writeDecimalAsPacked(Pos.TIT_TOT_SOPR_TEC, titTotSoprTec.copy());
    }

    public void setTitTotSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SOPR_TEC, Pos.TIT_TOT_SOPR_TEC);
    }

    /**Original name: TIT-TOT-SOPR-TEC<br>*/
    public AfDecimal getTitTotSoprTec() {
        return readPackedAsDecimal(Pos.TIT_TOT_SOPR_TEC, Len.Int.TIT_TOT_SOPR_TEC, Len.Fract.TIT_TOT_SOPR_TEC);
    }

    public byte[] getTitTotSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SOPR_TEC, Pos.TIT_TOT_SOPR_TEC);
        return buffer;
    }

    public void setTitTotSoprTecNull(String titTotSoprTecNull) {
        writeString(Pos.TIT_TOT_SOPR_TEC_NULL, titTotSoprTecNull, Len.TIT_TOT_SOPR_TEC_NULL);
    }

    /**Original name: TIT-TOT-SOPR-TEC-NULL<br>*/
    public String getTitTotSoprTecNull() {
        return readString(Pos.TIT_TOT_SOPR_TEC_NULL, Len.TIT_TOT_SOPR_TEC_NULL);
    }

    public String getTitTotSoprTecNullFormatted() {
        return Functions.padBlanks(getTitTotSoprTecNull(), Len.TIT_TOT_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_TEC = 1;
        public static final int TIT_TOT_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_TEC = 8;
        public static final int TIT_TOT_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
