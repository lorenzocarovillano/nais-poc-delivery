package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-SOPR-ALT<br>
 * Variable: TIT-TOT-SOPR-ALT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotSoprAlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotSoprAlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_SOPR_ALT;
    }

    public void setTitTotSoprAlt(AfDecimal titTotSoprAlt) {
        writeDecimalAsPacked(Pos.TIT_TOT_SOPR_ALT, titTotSoprAlt.copy());
    }

    public void setTitTotSoprAltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_SOPR_ALT, Pos.TIT_TOT_SOPR_ALT);
    }

    /**Original name: TIT-TOT-SOPR-ALT<br>*/
    public AfDecimal getTitTotSoprAlt() {
        return readPackedAsDecimal(Pos.TIT_TOT_SOPR_ALT, Len.Int.TIT_TOT_SOPR_ALT, Len.Fract.TIT_TOT_SOPR_ALT);
    }

    public byte[] getTitTotSoprAltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_SOPR_ALT, Pos.TIT_TOT_SOPR_ALT);
        return buffer;
    }

    public void setTitTotSoprAltNull(String titTotSoprAltNull) {
        writeString(Pos.TIT_TOT_SOPR_ALT_NULL, titTotSoprAltNull, Len.TIT_TOT_SOPR_ALT_NULL);
    }

    /**Original name: TIT-TOT-SOPR-ALT-NULL<br>*/
    public String getTitTotSoprAltNull() {
        return readString(Pos.TIT_TOT_SOPR_ALT_NULL, Len.TIT_TOT_SOPR_ALT_NULL);
    }

    public String getTitTotSoprAltNullFormatted() {
        return Functions.padBlanks(getTitTotSoprAltNull(), Len.TIT_TOT_SOPR_ALT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_ALT = 1;
        public static final int TIT_TOT_SOPR_ALT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_SOPR_ALT = 8;
        public static final int TIT_TOT_SOPR_ALT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_ALT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_SOPR_ALT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
