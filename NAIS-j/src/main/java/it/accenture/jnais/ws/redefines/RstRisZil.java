package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-ZIL<br>
 * Variable: RST-RIS-ZIL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisZil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisZil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_ZIL;
    }

    public void setRstRisZil(AfDecimal rstRisZil) {
        writeDecimalAsPacked(Pos.RST_RIS_ZIL, rstRisZil.copy());
    }

    public void setRstRisZilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_ZIL, Pos.RST_RIS_ZIL);
    }

    /**Original name: RST-RIS-ZIL<br>*/
    public AfDecimal getRstRisZil() {
        return readPackedAsDecimal(Pos.RST_RIS_ZIL, Len.Int.RST_RIS_ZIL, Len.Fract.RST_RIS_ZIL);
    }

    public byte[] getRstRisZilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_ZIL, Pos.RST_RIS_ZIL);
        return buffer;
    }

    public void setRstRisZilNull(String rstRisZilNull) {
        writeString(Pos.RST_RIS_ZIL_NULL, rstRisZilNull, Len.RST_RIS_ZIL_NULL);
    }

    /**Original name: RST-RIS-ZIL-NULL<br>*/
    public String getRstRisZilNull() {
        return readString(Pos.RST_RIS_ZIL_NULL, Len.RST_RIS_ZIL_NULL);
    }

    public String getRstRisZilNullFormatted() {
        return Functions.padBlanks(getRstRisZilNull(), Len.RST_RIS_ZIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_ZIL = 1;
        public static final int RST_RIS_ZIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_ZIL = 8;
        public static final int RST_RIS_ZIL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_ZIL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_ZIL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
