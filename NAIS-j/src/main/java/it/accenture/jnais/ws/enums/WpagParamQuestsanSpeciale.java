package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPAG-PARAM-QUESTSAN-SPECIALE<br>
 * Variable: WPAG-PARAM-QUESTSAN-SPECIALE from copybook LVEC0268<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpagParamQuestsanSpeciale {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWpagParamQuestsanSpeciale(char wpagParamQuestsanSpeciale) {
        this.value = wpagParamQuestsanSpeciale;
    }

    public char getWpagParamQuestsanSpeciale() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PARAM_QUESTSAN_SPECIALE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
