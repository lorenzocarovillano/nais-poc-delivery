package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-COD-ACQS-CNTRT<br>
 * Variable: RRE-COD-ACQS-CNTRT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreCodAcqsCntrt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreCodAcqsCntrt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_COD_ACQS_CNTRT;
    }

    public void setRreCodAcqsCntrt(int rreCodAcqsCntrt) {
        writeIntAsPacked(Pos.RRE_COD_ACQS_CNTRT, rreCodAcqsCntrt, Len.Int.RRE_COD_ACQS_CNTRT);
    }

    public void setRreCodAcqsCntrtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_COD_ACQS_CNTRT, Pos.RRE_COD_ACQS_CNTRT);
    }

    /**Original name: RRE-COD-ACQS-CNTRT<br>*/
    public int getRreCodAcqsCntrt() {
        return readPackedAsInt(Pos.RRE_COD_ACQS_CNTRT, Len.Int.RRE_COD_ACQS_CNTRT);
    }

    public byte[] getRreCodAcqsCntrtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_COD_ACQS_CNTRT, Pos.RRE_COD_ACQS_CNTRT);
        return buffer;
    }

    public void setRreCodAcqsCntrtNull(String rreCodAcqsCntrtNull) {
        writeString(Pos.RRE_COD_ACQS_CNTRT_NULL, rreCodAcqsCntrtNull, Len.RRE_COD_ACQS_CNTRT_NULL);
    }

    /**Original name: RRE-COD-ACQS-CNTRT-NULL<br>*/
    public String getRreCodAcqsCntrtNull() {
        return readString(Pos.RRE_COD_ACQS_CNTRT_NULL, Len.RRE_COD_ACQS_CNTRT_NULL);
    }

    public String getRreCodAcqsCntrtNullFormatted() {
        return Functions.padBlanks(getRreCodAcqsCntrtNull(), Len.RRE_COD_ACQS_CNTRT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_COD_ACQS_CNTRT = 1;
        public static final int RRE_COD_ACQS_CNTRT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_COD_ACQS_CNTRT = 3;
        public static final int RRE_COD_ACQS_CNTRT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_COD_ACQS_CNTRT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
