package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-RIT-ACC-IPT<br>
 * Variable: BEL-RIT-ACC-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelRitAccIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelRitAccIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_RIT_ACC_IPT;
    }

    public void setBelRitAccIpt(AfDecimal belRitAccIpt) {
        writeDecimalAsPacked(Pos.BEL_RIT_ACC_IPT, belRitAccIpt.copy());
    }

    public void setBelRitAccIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_RIT_ACC_IPT, Pos.BEL_RIT_ACC_IPT);
    }

    /**Original name: BEL-RIT-ACC-IPT<br>*/
    public AfDecimal getBelRitAccIpt() {
        return readPackedAsDecimal(Pos.BEL_RIT_ACC_IPT, Len.Int.BEL_RIT_ACC_IPT, Len.Fract.BEL_RIT_ACC_IPT);
    }

    public byte[] getBelRitAccIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_RIT_ACC_IPT, Pos.BEL_RIT_ACC_IPT);
        return buffer;
    }

    public void setBelRitAccIptNull(String belRitAccIptNull) {
        writeString(Pos.BEL_RIT_ACC_IPT_NULL, belRitAccIptNull, Len.BEL_RIT_ACC_IPT_NULL);
    }

    /**Original name: BEL-RIT-ACC-IPT-NULL<br>*/
    public String getBelRitAccIptNull() {
        return readString(Pos.BEL_RIT_ACC_IPT_NULL, Len.BEL_RIT_ACC_IPT_NULL);
    }

    public String getBelRitAccIptNullFormatted() {
        return Functions.padBlanks(getBelRitAccIptNull(), Len.BEL_RIT_ACC_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_RIT_ACC_IPT = 1;
        public static final int BEL_RIT_ACC_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_RIT_ACC_IPT = 8;
        public static final int BEL_RIT_ACC_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_RIT_ACC_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_RIT_ACC_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
