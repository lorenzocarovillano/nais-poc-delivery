package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-TAKE-P<br>
 * Variable: PCO-DT-ULT-ELAB-TAKE-P from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabTakeP extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabTakeP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_TAKE_P;
    }

    public void setPcoDtUltElabTakeP(int pcoDtUltElabTakeP) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_TAKE_P, pcoDtUltElabTakeP, Len.Int.PCO_DT_ULT_ELAB_TAKE_P);
    }

    public void setPcoDtUltElabTakePFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_TAKE_P, Pos.PCO_DT_ULT_ELAB_TAKE_P);
    }

    /**Original name: PCO-DT-ULT-ELAB-TAKE-P<br>*/
    public int getPcoDtUltElabTakeP() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_TAKE_P, Len.Int.PCO_DT_ULT_ELAB_TAKE_P);
    }

    public byte[] getPcoDtUltElabTakePAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_TAKE_P, Pos.PCO_DT_ULT_ELAB_TAKE_P);
        return buffer;
    }

    public void initPcoDtUltElabTakePHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_TAKE_P, Len.PCO_DT_ULT_ELAB_TAKE_P, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabTakePNull(String pcoDtUltElabTakePNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_TAKE_P_NULL, pcoDtUltElabTakePNull, Len.PCO_DT_ULT_ELAB_TAKE_P_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-TAKE-P-NULL<br>*/
    public String getPcoDtUltElabTakePNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_TAKE_P_NULL, Len.PCO_DT_ULT_ELAB_TAKE_P_NULL);
    }

    public String getPcoDtUltElabTakePNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabTakePNull(), Len.PCO_DT_ULT_ELAB_TAKE_P_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_TAKE_P = 1;
        public static final int PCO_DT_ULT_ELAB_TAKE_P_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_TAKE_P = 5;
        public static final int PCO_DT_ULT_ELAB_TAKE_P_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_TAKE_P = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
