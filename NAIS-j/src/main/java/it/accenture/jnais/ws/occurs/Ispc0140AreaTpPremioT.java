package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0140CodiceTpPremioT;

/**Original name: ISPC0140-AREA-TP-PREMIO-T<br>
 * Variables: ISPC0140-AREA-TP-PREMIO-T from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140AreaTpPremioT {

    //==== PROPERTIES ====
    public static final int COMPONENTE_T_MAXOCCURS = 75;
    //Original name: ISPC0140-CODICE-TP-PREMIO-T
    private Ispc0140CodiceTpPremioT codiceTpPremioT = new Ispc0140CodiceTpPremioT();
    //Original name: ISPC0140-O-NUM-COMP-MAX-ELE-T
    private String oNumCompMaxEleT = DefaultValues.stringVal(Len.O_NUM_COMP_MAX_ELE_T);
    //Original name: ISPC0140-COMPONENTE-T
    private Ispc0140ComponenteT[] componenteT = new Ispc0140ComponenteT[COMPONENTE_T_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140AreaTpPremioT() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int componenteTIdx = 1; componenteTIdx <= COMPONENTE_T_MAXOCCURS; componenteTIdx++) {
            componenteT[componenteTIdx - 1] = new Ispc0140ComponenteT();
        }
    }

    public void setAreaTpPremioTBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceTpPremioT.setCodiceTpPremioT(MarshalByte.readString(buffer, position, Ispc0140CodiceTpPremioT.Len.CODICE_TP_PREMIO_T));
        position += Ispc0140CodiceTpPremioT.Len.CODICE_TP_PREMIO_T;
        oNumCompMaxEleT = MarshalByte.readFixedString(buffer, position, Len.O_NUM_COMP_MAX_ELE_T);
        position += Len.O_NUM_COMP_MAX_ELE_T;
        for (int idx = 1; idx <= COMPONENTE_T_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                componenteT[idx - 1].setComponenteTBytes(buffer, position);
                position += Ispc0140ComponenteT.Len.COMPONENTE_T;
            }
            else {
                componenteT[idx - 1].initComponenteTSpaces();
                position += Ispc0140ComponenteT.Len.COMPONENTE_T;
            }
        }
    }

    public byte[] getAreaTpPremioTBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codiceTpPremioT.getCodiceTpPremioT(), Ispc0140CodiceTpPremioT.Len.CODICE_TP_PREMIO_T);
        position += Ispc0140CodiceTpPremioT.Len.CODICE_TP_PREMIO_T;
        MarshalByte.writeString(buffer, position, oNumCompMaxEleT, Len.O_NUM_COMP_MAX_ELE_T);
        position += Len.O_NUM_COMP_MAX_ELE_T;
        for (int idx = 1; idx <= COMPONENTE_T_MAXOCCURS; idx++) {
            componenteT[idx - 1].getComponenteTBytes(buffer, position);
            position += Ispc0140ComponenteT.Len.COMPONENTE_T;
        }
        return buffer;
    }

    public void initAreaTpPremioTSpaces() {
        codiceTpPremioT.setCodiceTpPremioT("");
        oNumCompMaxEleT = "";
        for (int idx = 1; idx <= COMPONENTE_T_MAXOCCURS; idx++) {
            componenteT[idx - 1].initComponenteTSpaces();
        }
    }

    public short getIspc0140ONumCompMaxEleT() {
        return NumericDisplay.asShort(this.oNumCompMaxEleT);
    }

    public Ispc0140CodiceTpPremioT getCodiceTpPremioT() {
        return codiceTpPremioT;
    }

    public Ispc0140ComponenteT getComponenteT(int idx) {
        return componenteT[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int O_NUM_COMP_MAX_ELE_T = 3;
        public static final int AREA_TP_PREMIO_T = Ispc0140CodiceTpPremioT.Len.CODICE_TP_PREMIO_T + O_NUM_COMP_MAX_ELE_T + Ispc0140AreaTpPremioT.COMPONENTE_T_MAXOCCURS * Ispc0140ComponenteT.Len.COMPONENTE_T;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
