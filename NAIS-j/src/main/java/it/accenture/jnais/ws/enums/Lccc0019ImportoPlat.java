package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0019-IMPORTO-PLAT<br>
 * Variable: LCCC0019-IMPORTO-PLAT from copybook LCCC0019<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0019ImportoPlat {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IMPORTO_PLAT);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setImportoPlat(String importoPlat) {
        this.value = Functions.subString(importoPlat, Len.IMPORTO_PLAT);
    }

    public String getImportoPlat() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IMPORTO_PLAT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
