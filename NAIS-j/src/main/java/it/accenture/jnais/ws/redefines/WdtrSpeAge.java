package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SPE-AGE<br>
 * Variable: WDTR-SPE-AGE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SPE_AGE;
    }

    public void setWdtrSpeAge(AfDecimal wdtrSpeAge) {
        writeDecimalAsPacked(Pos.WDTR_SPE_AGE, wdtrSpeAge.copy());
    }

    /**Original name: WDTR-SPE-AGE<br>*/
    public AfDecimal getWdtrSpeAge() {
        return readPackedAsDecimal(Pos.WDTR_SPE_AGE, Len.Int.WDTR_SPE_AGE, Len.Fract.WDTR_SPE_AGE);
    }

    public void setWdtrSpeAgeNull(String wdtrSpeAgeNull) {
        writeString(Pos.WDTR_SPE_AGE_NULL, wdtrSpeAgeNull, Len.WDTR_SPE_AGE_NULL);
    }

    /**Original name: WDTR-SPE-AGE-NULL<br>*/
    public String getWdtrSpeAgeNull() {
        return readString(Pos.WDTR_SPE_AGE_NULL, Len.WDTR_SPE_AGE_NULL);
    }

    public String getWdtrSpeAgeNullFormatted() {
        return Functions.padBlanks(getWdtrSpeAgeNull(), Len.WDTR_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SPE_AGE = 1;
        public static final int WDTR_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SPE_AGE = 8;
        public static final int WDTR_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
