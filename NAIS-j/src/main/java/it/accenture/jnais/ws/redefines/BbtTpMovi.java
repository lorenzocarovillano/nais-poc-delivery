package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BBT-TP-MOVI<br>
 * Variable: BBT-TP-MOVI from program IABS0050<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BbtTpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BbtTpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BBT_TP_MOVI;
    }

    public void setBbtTpMovi(int bbtTpMovi) {
        writeIntAsPacked(Pos.BBT_TP_MOVI, bbtTpMovi, Len.Int.BBT_TP_MOVI);
    }

    public void setBbtTpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BBT_TP_MOVI, Pos.BBT_TP_MOVI);
    }

    /**Original name: BBT-TP-MOVI<br>*/
    public int getBbtTpMovi() {
        return readPackedAsInt(Pos.BBT_TP_MOVI, Len.Int.BBT_TP_MOVI);
    }

    public byte[] getBbtTpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BBT_TP_MOVI, Pos.BBT_TP_MOVI);
        return buffer;
    }

    public void setBbtTpMoviNull(String bbtTpMoviNull) {
        writeString(Pos.BBT_TP_MOVI_NULL, bbtTpMoviNull, Len.BBT_TP_MOVI_NULL);
    }

    /**Original name: BBT-TP-MOVI-NULL<br>*/
    public String getBbtTpMoviNull() {
        return readString(Pos.BBT_TP_MOVI_NULL, Len.BBT_TP_MOVI_NULL);
    }

    public String getBbtTpMoviNullFormatted() {
        return Functions.padBlanks(getBbtTpMoviNull(), Len.BBT_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BBT_TP_MOVI = 1;
        public static final int BBT_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BBT_TP_MOVI = 3;
        public static final int BBT_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BBT_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
