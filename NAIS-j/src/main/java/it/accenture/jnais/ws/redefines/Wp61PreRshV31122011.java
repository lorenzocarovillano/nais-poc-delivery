package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP61-PRE-RSH-V-31122011<br>
 * Variable: WP61-PRE-RSH-V-31122011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp61PreRshV31122011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp61PreRshV31122011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP61_PRE_RSH_V31122011;
    }

    public void setWp61PreRshV31122011(AfDecimal wp61PreRshV31122011) {
        writeDecimalAsPacked(Pos.WP61_PRE_RSH_V31122011, wp61PreRshV31122011.copy());
    }

    public void setWp61PreRshV31122011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP61_PRE_RSH_V31122011, Pos.WP61_PRE_RSH_V31122011);
    }

    /**Original name: WP61-PRE-RSH-V-31122011<br>*/
    public AfDecimal getWp61PreRshV31122011() {
        return readPackedAsDecimal(Pos.WP61_PRE_RSH_V31122011, Len.Int.WP61_PRE_RSH_V31122011, Len.Fract.WP61_PRE_RSH_V31122011);
    }

    public byte[] getWp61PreRshV31122011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP61_PRE_RSH_V31122011, Pos.WP61_PRE_RSH_V31122011);
        return buffer;
    }

    public void setWp61PreRshV31122011Null(String wp61PreRshV31122011Null) {
        writeString(Pos.WP61_PRE_RSH_V31122011_NULL, wp61PreRshV31122011Null, Len.WP61_PRE_RSH_V31122011_NULL);
    }

    /**Original name: WP61-PRE-RSH-V-31122011-NULL<br>*/
    public String getWp61PreRshV31122011Null() {
        return readString(Pos.WP61_PRE_RSH_V31122011_NULL, Len.WP61_PRE_RSH_V31122011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP61_PRE_RSH_V31122011 = 1;
        public static final int WP61_PRE_RSH_V31122011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP61_PRE_RSH_V31122011 = 8;
        public static final int WP61_PRE_RSH_V31122011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP61_PRE_RSH_V31122011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP61_PRE_RSH_V31122011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
