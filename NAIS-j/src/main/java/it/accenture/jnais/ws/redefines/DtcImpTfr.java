package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-IMP-TFR<br>
 * Variable: DTC-IMP-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcImpTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcImpTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_IMP_TFR;
    }

    public void setDtcImpTfr(AfDecimal dtcImpTfr) {
        writeDecimalAsPacked(Pos.DTC_IMP_TFR, dtcImpTfr.copy());
    }

    public void setDtcImpTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_IMP_TFR, Pos.DTC_IMP_TFR);
    }

    /**Original name: DTC-IMP-TFR<br>*/
    public AfDecimal getDtcImpTfr() {
        return readPackedAsDecimal(Pos.DTC_IMP_TFR, Len.Int.DTC_IMP_TFR, Len.Fract.DTC_IMP_TFR);
    }

    public byte[] getDtcImpTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_IMP_TFR, Pos.DTC_IMP_TFR);
        return buffer;
    }

    public void setDtcImpTfrNull(String dtcImpTfrNull) {
        writeString(Pos.DTC_IMP_TFR_NULL, dtcImpTfrNull, Len.DTC_IMP_TFR_NULL);
    }

    /**Original name: DTC-IMP-TFR-NULL<br>*/
    public String getDtcImpTfrNull() {
        return readString(Pos.DTC_IMP_TFR_NULL, Len.DTC_IMP_TFR_NULL);
    }

    public String getDtcImpTfrNullFormatted() {
        return Functions.padBlanks(getDtcImpTfrNull(), Len.DTC_IMP_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_IMP_TFR = 1;
        public static final int DTC_IMP_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_IMP_TFR = 8;
        public static final int DTC_IMP_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_IMP_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_IMP_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
