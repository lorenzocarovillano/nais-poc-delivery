package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDAD-PC-VOLO<br>
 * Variable: WDAD-PC-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadPcVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadPcVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_PC_VOLO;
    }

    public void setWdadPcVolo(AfDecimal wdadPcVolo) {
        writeDecimalAsPacked(Pos.WDAD_PC_VOLO, wdadPcVolo.copy());
    }

    public void setWdadPcVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_PC_VOLO, Pos.WDAD_PC_VOLO);
    }

    /**Original name: WDAD-PC-VOLO<br>*/
    public AfDecimal getWdadPcVolo() {
        return readPackedAsDecimal(Pos.WDAD_PC_VOLO, Len.Int.WDAD_PC_VOLO, Len.Fract.WDAD_PC_VOLO);
    }

    public byte[] getWdadPcVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_PC_VOLO, Pos.WDAD_PC_VOLO);
        return buffer;
    }

    public void setWdadPcVoloNull(String wdadPcVoloNull) {
        writeString(Pos.WDAD_PC_VOLO_NULL, wdadPcVoloNull, Len.WDAD_PC_VOLO_NULL);
    }

    /**Original name: WDAD-PC-VOLO-NULL<br>*/
    public String getWdadPcVoloNull() {
        return readString(Pos.WDAD_PC_VOLO_NULL, Len.WDAD_PC_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_PC_VOLO = 1;
        public static final int WDAD_PC_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_PC_VOLO = 4;
        public static final int WDAD_PC_VOLO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDAD_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
