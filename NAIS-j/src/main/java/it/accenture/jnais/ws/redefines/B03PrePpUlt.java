package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRE-PP-ULT<br>
 * Variable: B03-PRE-PP-ULT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrePpUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PrePpUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRE_PP_ULT;
    }

    public void setB03PrePpUlt(AfDecimal b03PrePpUlt) {
        writeDecimalAsPacked(Pos.B03_PRE_PP_ULT, b03PrePpUlt.copy());
    }

    public void setB03PrePpUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRE_PP_ULT, Pos.B03_PRE_PP_ULT);
    }

    /**Original name: B03-PRE-PP-ULT<br>*/
    public AfDecimal getB03PrePpUlt() {
        return readPackedAsDecimal(Pos.B03_PRE_PP_ULT, Len.Int.B03_PRE_PP_ULT, Len.Fract.B03_PRE_PP_ULT);
    }

    public byte[] getB03PrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRE_PP_ULT, Pos.B03_PRE_PP_ULT);
        return buffer;
    }

    public void setB03PrePpUltNull(String b03PrePpUltNull) {
        writeString(Pos.B03_PRE_PP_ULT_NULL, b03PrePpUltNull, Len.B03_PRE_PP_ULT_NULL);
    }

    /**Original name: B03-PRE-PP-ULT-NULL<br>*/
    public String getB03PrePpUltNull() {
        return readString(Pos.B03_PRE_PP_ULT_NULL, Len.B03_PRE_PP_ULT_NULL);
    }

    public String getB03PrePpUltNullFormatted() {
        return Functions.padBlanks(getB03PrePpUltNull(), Len.B03_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRE_PP_ULT = 1;
        public static final int B03_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRE_PP_ULT = 8;
        public static final int B03_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
