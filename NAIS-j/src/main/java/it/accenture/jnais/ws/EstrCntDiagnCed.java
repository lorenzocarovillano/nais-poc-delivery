package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P84ImpLrdCedoleLiq;

/**Original name: ESTR-CNT-DIAGN-CED<br>
 * Variable: ESTR-CNT-DIAGN-CED from copybook IDBVP841<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class EstrCntDiagnCed extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P84-COD-COMP-ANIA
    private int p84CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P84-ID-POLI
    private int p84IdPoli = DefaultValues.INT_VAL;
    //Original name: P84-DT-LIQ-CED
    private int p84DtLiqCed = DefaultValues.INT_VAL;
    //Original name: P84-COD-TARI
    private String p84CodTari = DefaultValues.stringVal(Len.P84_COD_TARI);
    //Original name: P84-IMP-LRD-CEDOLE-LIQ
    private P84ImpLrdCedoleLiq p84ImpLrdCedoleLiq = new P84ImpLrdCedoleLiq();
    //Original name: P84-IB-POLI
    private String p84IbPoli = DefaultValues.stringVal(Len.P84_IB_POLI);
    //Original name: P84-DS-OPER-SQL
    private char p84DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P84-DS-VER
    private int p84DsVer = DefaultValues.INT_VAL;
    //Original name: P84-DS-TS-CPTZ
    private long p84DsTsCptz = DefaultValues.LONG_VAL;
    //Original name: P84-DS-UTENTE
    private String p84DsUtente = DefaultValues.stringVal(Len.P84_DS_UTENTE);
    //Original name: P84-DS-STATO-ELAB
    private char p84DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ESTR_CNT_DIAGN_CED;
    }

    @Override
    public void deserialize(byte[] buf) {
        setEstrCntDiagnCedBytes(buf);
    }

    public void setEstrCntDiagnCedBytes(byte[] buffer) {
        setEstrCntDiagnCedBytes(buffer, 1);
    }

    public byte[] getEstrCntDiagnCedBytes() {
        byte[] buffer = new byte[Len.ESTR_CNT_DIAGN_CED];
        return getEstrCntDiagnCedBytes(buffer, 1);
    }

    public void setEstrCntDiagnCedBytes(byte[] buffer, int offset) {
        int position = offset;
        p84CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P84_COD_COMP_ANIA, 0);
        position += Len.P84_COD_COMP_ANIA;
        p84IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P84_ID_POLI, 0);
        position += Len.P84_ID_POLI;
        p84DtLiqCed = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P84_DT_LIQ_CED, 0);
        position += Len.P84_DT_LIQ_CED;
        p84CodTari = MarshalByte.readString(buffer, position, Len.P84_COD_TARI);
        position += Len.P84_COD_TARI;
        p84ImpLrdCedoleLiq.setP84ImpLrdCedoleLiqFromBuffer(buffer, position);
        position += P84ImpLrdCedoleLiq.Len.P84_IMP_LRD_CEDOLE_LIQ;
        p84IbPoli = MarshalByte.readString(buffer, position, Len.P84_IB_POLI);
        position += Len.P84_IB_POLI;
        p84DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p84DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P84_DS_VER, 0);
        position += Len.P84_DS_VER;
        p84DsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P84_DS_TS_CPTZ, 0);
        position += Len.P84_DS_TS_CPTZ;
        p84DsUtente = MarshalByte.readString(buffer, position, Len.P84_DS_UTENTE);
        position += Len.P84_DS_UTENTE;
        p84DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getEstrCntDiagnCedBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p84CodCompAnia, Len.Int.P84_COD_COMP_ANIA, 0);
        position += Len.P84_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, p84IdPoli, Len.Int.P84_ID_POLI, 0);
        position += Len.P84_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, p84DtLiqCed, Len.Int.P84_DT_LIQ_CED, 0);
        position += Len.P84_DT_LIQ_CED;
        MarshalByte.writeString(buffer, position, p84CodTari, Len.P84_COD_TARI);
        position += Len.P84_COD_TARI;
        p84ImpLrdCedoleLiq.getP84ImpLrdCedoleLiqAsBuffer(buffer, position);
        position += P84ImpLrdCedoleLiq.Len.P84_IMP_LRD_CEDOLE_LIQ;
        MarshalByte.writeString(buffer, position, p84IbPoli, Len.P84_IB_POLI);
        position += Len.P84_IB_POLI;
        MarshalByte.writeChar(buffer, position, p84DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p84DsVer, Len.Int.P84_DS_VER, 0);
        position += Len.P84_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p84DsTsCptz, Len.Int.P84_DS_TS_CPTZ, 0);
        position += Len.P84_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, p84DsUtente, Len.P84_DS_UTENTE);
        position += Len.P84_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p84DsStatoElab);
        return buffer;
    }

    public void setP84CodCompAnia(int p84CodCompAnia) {
        this.p84CodCompAnia = p84CodCompAnia;
    }

    public int getP84CodCompAnia() {
        return this.p84CodCompAnia;
    }

    public void setP84IdPoli(int p84IdPoli) {
        this.p84IdPoli = p84IdPoli;
    }

    public int getP84IdPoli() {
        return this.p84IdPoli;
    }

    public void setP84DtLiqCed(int p84DtLiqCed) {
        this.p84DtLiqCed = p84DtLiqCed;
    }

    public int getP84DtLiqCed() {
        return this.p84DtLiqCed;
    }

    public void setP84CodTari(String p84CodTari) {
        this.p84CodTari = Functions.subString(p84CodTari, Len.P84_COD_TARI);
    }

    public String getP84CodTari() {
        return this.p84CodTari;
    }

    public void setP84IbPoli(String p84IbPoli) {
        this.p84IbPoli = Functions.subString(p84IbPoli, Len.P84_IB_POLI);
    }

    public String getP84IbPoli() {
        return this.p84IbPoli;
    }

    public void setP84DsOperSql(char p84DsOperSql) {
        this.p84DsOperSql = p84DsOperSql;
    }

    public void setP84DsOperSqlFormatted(String p84DsOperSql) {
        setP84DsOperSql(Functions.charAt(p84DsOperSql, Types.CHAR_SIZE));
    }

    public char getP84DsOperSql() {
        return this.p84DsOperSql;
    }

    public void setP84DsVer(int p84DsVer) {
        this.p84DsVer = p84DsVer;
    }

    public int getP84DsVer() {
        return this.p84DsVer;
    }

    public void setP84DsTsCptz(long p84DsTsCptz) {
        this.p84DsTsCptz = p84DsTsCptz;
    }

    public long getP84DsTsCptz() {
        return this.p84DsTsCptz;
    }

    public void setP84DsUtente(String p84DsUtente) {
        this.p84DsUtente = Functions.subString(p84DsUtente, Len.P84_DS_UTENTE);
    }

    public String getP84DsUtente() {
        return this.p84DsUtente;
    }

    public void setP84DsStatoElab(char p84DsStatoElab) {
        this.p84DsStatoElab = p84DsStatoElab;
    }

    public void setP84DsStatoElabFormatted(String p84DsStatoElab) {
        setP84DsStatoElab(Functions.charAt(p84DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP84DsStatoElab() {
        return this.p84DsStatoElab;
    }

    public P84ImpLrdCedoleLiq getP84ImpLrdCedoleLiq() {
        return p84ImpLrdCedoleLiq;
    }

    @Override
    public byte[] serialize() {
        return getEstrCntDiagnCedBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P84_COD_COMP_ANIA = 3;
        public static final int P84_ID_POLI = 5;
        public static final int P84_DT_LIQ_CED = 5;
        public static final int P84_COD_TARI = 12;
        public static final int P84_IB_POLI = 40;
        public static final int P84_DS_OPER_SQL = 1;
        public static final int P84_DS_VER = 5;
        public static final int P84_DS_TS_CPTZ = 10;
        public static final int P84_DS_UTENTE = 20;
        public static final int P84_DS_STATO_ELAB = 1;
        public static final int ESTR_CNT_DIAGN_CED = P84_COD_COMP_ANIA + P84_ID_POLI + P84_DT_LIQ_CED + P84_COD_TARI + P84ImpLrdCedoleLiq.Len.P84_IMP_LRD_CEDOLE_LIQ + P84_IB_POLI + P84_DS_OPER_SQL + P84_DS_VER + P84_DS_TS_CPTZ + P84_DS_UTENTE + P84_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P84_COD_COMP_ANIA = 5;
            public static final int P84_ID_POLI = 9;
            public static final int P84_DT_LIQ_CED = 8;
            public static final int P84_DS_VER = 9;
            public static final int P84_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
