package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-1O-PER-MM<br>
 * Variable: W-B03-DUR-1O-PER-MM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03Dur1oPerMmLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03Dur1oPerMmLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR1O_PER_MM;
    }

    public void setwB03Dur1oPerMm(int wB03Dur1oPerMm) {
        writeIntAsPacked(Pos.W_B03_DUR1O_PER_MM, wB03Dur1oPerMm, Len.Int.W_B03_DUR1O_PER_MM);
    }

    /**Original name: W-B03-DUR-1O-PER-MM<br>*/
    public int getwB03Dur1oPerMm() {
        return readPackedAsInt(Pos.W_B03_DUR1O_PER_MM, Len.Int.W_B03_DUR1O_PER_MM);
    }

    public byte[] getwB03Dur1oPerMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR1O_PER_MM, Pos.W_B03_DUR1O_PER_MM);
        return buffer;
    }

    public void setwB03Dur1oPerMmNull(String wB03Dur1oPerMmNull) {
        writeString(Pos.W_B03_DUR1O_PER_MM_NULL, wB03Dur1oPerMmNull, Len.W_B03_DUR1O_PER_MM_NULL);
    }

    /**Original name: W-B03-DUR-1O-PER-MM-NULL<br>*/
    public String getwB03Dur1oPerMmNull() {
        return readString(Pos.W_B03_DUR1O_PER_MM_NULL, Len.W_B03_DUR1O_PER_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR1O_PER_MM = 1;
        public static final int W_B03_DUR1O_PER_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR1O_PER_MM = 3;
        public static final int W_B03_DUR1O_PER_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR1O_PER_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
