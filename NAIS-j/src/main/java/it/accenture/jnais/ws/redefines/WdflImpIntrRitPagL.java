package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-INTR-RIT-PAG-L<br>
 * Variable: WDFL-IMP-INTR-RIT-PAG-L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpIntrRitPagL extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpIntrRitPagL() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_INTR_RIT_PAG_L;
    }

    public void setWdflImpIntrRitPagL(AfDecimal wdflImpIntrRitPagL) {
        writeDecimalAsPacked(Pos.WDFL_IMP_INTR_RIT_PAG_L, wdflImpIntrRitPagL.copy());
    }

    public void setWdflImpIntrRitPagLFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_INTR_RIT_PAG_L, Pos.WDFL_IMP_INTR_RIT_PAG_L);
    }

    /**Original name: WDFL-IMP-INTR-RIT-PAG-L<br>*/
    public AfDecimal getWdflImpIntrRitPagL() {
        return readPackedAsDecimal(Pos.WDFL_IMP_INTR_RIT_PAG_L, Len.Int.WDFL_IMP_INTR_RIT_PAG_L, Len.Fract.WDFL_IMP_INTR_RIT_PAG_L);
    }

    public byte[] getWdflImpIntrRitPagLAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_INTR_RIT_PAG_L, Pos.WDFL_IMP_INTR_RIT_PAG_L);
        return buffer;
    }

    public void setWdflImpIntrRitPagLNull(String wdflImpIntrRitPagLNull) {
        writeString(Pos.WDFL_IMP_INTR_RIT_PAG_L_NULL, wdflImpIntrRitPagLNull, Len.WDFL_IMP_INTR_RIT_PAG_L_NULL);
    }

    /**Original name: WDFL-IMP-INTR-RIT-PAG-L-NULL<br>*/
    public String getWdflImpIntrRitPagLNull() {
        return readString(Pos.WDFL_IMP_INTR_RIT_PAG_L_NULL, Len.WDFL_IMP_INTR_RIT_PAG_L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_INTR_RIT_PAG_L = 1;
        public static final int WDFL_IMP_INTR_RIT_PAG_L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_INTR_RIT_PAG_L = 8;
        public static final int WDFL_IMP_INTR_RIT_PAG_L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_INTR_RIT_PAG_L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_INTR_RIT_PAG_L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
