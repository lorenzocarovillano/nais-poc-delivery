package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK1-TAB-STRINGHE-TOT<br>
 * Variables: WK1-TAB-STRINGHE-TOT from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wk1TabStringheTot {

    //==== PROPERTIES ====
    //Original name: WK1-COD-VARIABILE
    private String codVariabile = DefaultValues.stringVal(Len.COD_VARIABILE);
    //Original name: WK1-TP-STRINGA
    private char tpStringa = DefaultValues.CHAR_VAL;
    //Original name: WK1-STRINGA-TOT
    private String stringaTot = DefaultValues.stringVal(Len.STRINGA_TOT);

    //==== METHODS ====
    public void setCodVariabile(String codVariabile) {
        this.codVariabile = Functions.subString(codVariabile, Len.COD_VARIABILE);
    }

    public String getCodVariabile() {
        return this.codVariabile;
    }

    public void setTpStringa(char tpStringa) {
        this.tpStringa = tpStringa;
    }

    public char getTpStringa() {
        return this.tpStringa;
    }

    public void setStringaTot(String stringaTot) {
        this.stringaTot = Functions.subString(stringaTot, Len.STRINGA_TOT);
    }

    public String getStringaTot() {
        return this.stringaTot;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_VARIABILE = 12;
        public static final int STRINGA_TOT = 60;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
