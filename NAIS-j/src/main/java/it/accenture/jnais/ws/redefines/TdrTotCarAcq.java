package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-CAR-ACQ<br>
 * Variable: TDR-TOT-CAR-ACQ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_CAR_ACQ;
    }

    public void setTdrTotCarAcq(AfDecimal tdrTotCarAcq) {
        writeDecimalAsPacked(Pos.TDR_TOT_CAR_ACQ, tdrTotCarAcq.copy());
    }

    public void setTdrTotCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_CAR_ACQ, Pos.TDR_TOT_CAR_ACQ);
    }

    /**Original name: TDR-TOT-CAR-ACQ<br>*/
    public AfDecimal getTdrTotCarAcq() {
        return readPackedAsDecimal(Pos.TDR_TOT_CAR_ACQ, Len.Int.TDR_TOT_CAR_ACQ, Len.Fract.TDR_TOT_CAR_ACQ);
    }

    public byte[] getTdrTotCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_CAR_ACQ, Pos.TDR_TOT_CAR_ACQ);
        return buffer;
    }

    public void setTdrTotCarAcqNull(String tdrTotCarAcqNull) {
        writeString(Pos.TDR_TOT_CAR_ACQ_NULL, tdrTotCarAcqNull, Len.TDR_TOT_CAR_ACQ_NULL);
    }

    /**Original name: TDR-TOT-CAR-ACQ-NULL<br>*/
    public String getTdrTotCarAcqNull() {
        return readString(Pos.TDR_TOT_CAR_ACQ_NULL, Len.TDR_TOT_CAR_ACQ_NULL);
    }

    public String getTdrTotCarAcqNullFormatted() {
        return Functions.padBlanks(getTdrTotCarAcqNull(), Len.TDR_TOT_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_ACQ = 1;
        public static final int TDR_TOT_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_ACQ = 8;
        public static final int TDR_TOT_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
