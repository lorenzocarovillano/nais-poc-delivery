package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WL19-VAL-QUO<br>
 * Variable: WL19-VAL-QUO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl19ValQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl19ValQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_VAL_QUO;
    }

    public void setWl19ValQuo(AfDecimal wl19ValQuo) {
        writeDecimalAsPacked(Pos.WL19_VAL_QUO, wl19ValQuo.copy());
    }

    public void setWl19ValQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL19_VAL_QUO, Pos.WL19_VAL_QUO);
    }

    /**Original name: WL19-VAL-QUO<br>*/
    public AfDecimal getWl19ValQuo() {
        return readPackedAsDecimal(Pos.WL19_VAL_QUO, Len.Int.WL19_VAL_QUO, Len.Fract.WL19_VAL_QUO);
    }

    public byte[] getWl19ValQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL19_VAL_QUO, Pos.WL19_VAL_QUO);
        return buffer;
    }

    public void initWl19ValQuoSpaces() {
        fill(Pos.WL19_VAL_QUO, Len.WL19_VAL_QUO, Types.SPACE_CHAR);
    }

    public void setWl19ValQuoNull(String wl19ValQuoNull) {
        writeString(Pos.WL19_VAL_QUO_NULL, wl19ValQuoNull, Len.WL19_VAL_QUO_NULL);
    }

    /**Original name: WL19-VAL-QUO-NULL<br>*/
    public String getWl19ValQuoNull() {
        return readString(Pos.WL19_VAL_QUO_NULL, Len.WL19_VAL_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL19_VAL_QUO = 1;
        public static final int WL19_VAL_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_VAL_QUO = 7;
        public static final int WL19_VAL_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WL19_VAL_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WL19_VAL_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
