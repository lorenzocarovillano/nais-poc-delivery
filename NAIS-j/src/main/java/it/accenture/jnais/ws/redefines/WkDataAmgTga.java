package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WK-DATA-AMG-TGA<br>
 * Variable: WK-DATA-AMG-TGA from program LVVS0127<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDataAmgTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDataAmgTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DATA_AMG_TGA;
    }

    @Override
    public void init() {
        int position = 1;
        writeShort(position, ((short)0), Len.Int.AAAA_TGA, SignType.NO_SIGN);
        position += Len.AAAA_TGA;
        writeShort(position, ((short)0), Len.Int.MM_TGA, SignType.NO_SIGN);
        position += Len.MM_TGA;
        writeShort(position, ((short)0), Len.Int.GG_TGA, SignType.NO_SIGN);
    }

    public void setAaaaTgaFormatted(String aaaaTga) {
        writeString(Pos.AAAA_TGA, Trunc.toUnsignedNumeric(aaaaTga, Len.AAAA_TGA), Len.AAAA_TGA);
    }

    /**Original name: WK-AAAA-TGA<br>*/
    public short getAaaaTga() {
        return readNumDispUnsignedShort(Pos.AAAA_TGA, Len.AAAA_TGA);
    }

    public String getAaaaTgaFormatted() {
        return readFixedString(Pos.AAAA_TGA, Len.AAAA_TGA);
    }

    public void setMmTgaFormatted(String mmTga) {
        writeString(Pos.MM_TGA, Trunc.toUnsignedNumeric(mmTga, Len.MM_TGA), Len.MM_TGA);
    }

    /**Original name: WK-MM-TGA<br>*/
    public short getMmTga() {
        return readNumDispUnsignedShort(Pos.MM_TGA, Len.MM_TGA);
    }

    public void setGgTgaFormatted(String ggTga) {
        writeString(Pos.GG_TGA, Trunc.toUnsignedNumeric(ggTga, Len.GG_TGA), Len.GG_TGA);
    }

    /**Original name: WK-GG-TGA<br>*/
    public short getGgTga() {
        return readNumDispUnsignedShort(Pos.GG_TGA, Len.GG_TGA);
    }

    public void setWkDataAmgTgaRFormatted(String wkDataAmgTgaR) {
        writeString(Pos.WK_DATA_AMG_TGA_R, Trunc.toUnsignedNumeric(wkDataAmgTgaR, Len.WK_DATA_AMG_TGA_R), Len.WK_DATA_AMG_TGA_R);
    }

    /**Original name: WK-DATA-AMG-TGA-R<br>*/
    public int getWkDataAmgTgaR() {
        return readNumDispUnsignedInt(Pos.WK_DATA_AMG_TGA_R, Len.WK_DATA_AMG_TGA_R);
    }

    public String getWkDataAmgTgaRFormatted() {
        return readFixedString(Pos.WK_DATA_AMG_TGA_R, Len.WK_DATA_AMG_TGA_R);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DATA_AMG_TGA = 1;
        public static final int AAAA_TGA = WK_DATA_AMG_TGA;
        public static final int MM_TGA = AAAA_TGA + Len.AAAA_TGA;
        public static final int GG_TGA = MM_TGA + Len.MM_TGA;
        public static final int WK_DATA_AMG_TGA_R = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_TGA = 4;
        public static final int MM_TGA = 2;
        public static final int GG_TGA = 2;
        public static final int WK_DATA_AMG_TGA = AAAA_TGA + MM_TGA + GG_TGA;
        public static final int WK_DATA_AMG_TGA_R = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int AAAA_TGA = 4;
            public static final int MM_TGA = 2;
            public static final int GG_TGA = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
