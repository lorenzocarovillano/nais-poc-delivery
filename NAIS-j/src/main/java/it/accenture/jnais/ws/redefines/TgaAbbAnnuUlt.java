package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ABB-ANNU-ULT<br>
 * Variable: TGA-ABB-ANNU-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAbbAnnuUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAbbAnnuUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ABB_ANNU_ULT;
    }

    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        writeDecimalAsPacked(Pos.TGA_ABB_ANNU_ULT, tgaAbbAnnuUlt.copy());
    }

    public void setTgaAbbAnnuUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ABB_ANNU_ULT, Pos.TGA_ABB_ANNU_ULT);
    }

    /**Original name: TGA-ABB-ANNU-ULT<br>*/
    public AfDecimal getTgaAbbAnnuUlt() {
        return readPackedAsDecimal(Pos.TGA_ABB_ANNU_ULT, Len.Int.TGA_ABB_ANNU_ULT, Len.Fract.TGA_ABB_ANNU_ULT);
    }

    public byte[] getTgaAbbAnnuUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ABB_ANNU_ULT, Pos.TGA_ABB_ANNU_ULT);
        return buffer;
    }

    public void setTgaAbbAnnuUltNull(String tgaAbbAnnuUltNull) {
        writeString(Pos.TGA_ABB_ANNU_ULT_NULL, tgaAbbAnnuUltNull, Len.TGA_ABB_ANNU_ULT_NULL);
    }

    /**Original name: TGA-ABB-ANNU-ULT-NULL<br>*/
    public String getTgaAbbAnnuUltNull() {
        return readString(Pos.TGA_ABB_ANNU_ULT_NULL, Len.TGA_ABB_ANNU_ULT_NULL);
    }

    public String getTgaAbbAnnuUltNullFormatted() {
        return Functions.padBlanks(getTgaAbbAnnuUltNull(), Len.TGA_ABB_ANNU_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ABB_ANNU_ULT = 1;
        public static final int TGA_ABB_ANNU_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ABB_ANNU_ULT = 8;
        public static final int TGA_ABB_ANNU_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ABB_ANNU_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ABB_ANNU_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
