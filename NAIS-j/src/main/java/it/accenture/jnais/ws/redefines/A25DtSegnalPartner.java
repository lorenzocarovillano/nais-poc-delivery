package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-DT-SEGNAL-PARTNER<br>
 * Variable: A25-DT-SEGNAL-PARTNER from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25DtSegnalPartner extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25DtSegnalPartner() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_DT_SEGNAL_PARTNER;
    }

    public void setA25DtSegnalPartner(int a25DtSegnalPartner) {
        writeIntAsPacked(Pos.A25_DT_SEGNAL_PARTNER, a25DtSegnalPartner, Len.Int.A25_DT_SEGNAL_PARTNER);
    }

    public void setA25DtSegnalPartnerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_DT_SEGNAL_PARTNER, Pos.A25_DT_SEGNAL_PARTNER);
    }

    /**Original name: A25-DT-SEGNAL-PARTNER<br>*/
    public int getA25DtSegnalPartner() {
        return readPackedAsInt(Pos.A25_DT_SEGNAL_PARTNER, Len.Int.A25_DT_SEGNAL_PARTNER);
    }

    public byte[] getA25DtSegnalPartnerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_DT_SEGNAL_PARTNER, Pos.A25_DT_SEGNAL_PARTNER);
        return buffer;
    }

    public void setA25DtSegnalPartnerNull(String a25DtSegnalPartnerNull) {
        writeString(Pos.A25_DT_SEGNAL_PARTNER_NULL, a25DtSegnalPartnerNull, Len.A25_DT_SEGNAL_PARTNER_NULL);
    }

    /**Original name: A25-DT-SEGNAL-PARTNER-NULL<br>*/
    public String getA25DtSegnalPartnerNull() {
        return readString(Pos.A25_DT_SEGNAL_PARTNER_NULL, Len.A25_DT_SEGNAL_PARTNER_NULL);
    }

    public String getA25DtSegnalPartnerNullFormatted() {
        return Functions.padBlanks(getA25DtSegnalPartnerNull(), Len.A25_DT_SEGNAL_PARTNER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_DT_SEGNAL_PARTNER = 1;
        public static final int A25_DT_SEGNAL_PARTNER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_DT_SEGNAL_PARTNER = 5;
        public static final int A25_DT_SEGNAL_PARTNER_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_DT_SEGNAL_PARTNER = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
