package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POG-VAL-NUM<br>
 * Variable: POG-VAL-NUM from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PogValNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PogValNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POG_VAL_NUM;
    }

    public void setPogValNum(long pogValNum) {
        writeLongAsPacked(Pos.POG_VAL_NUM, pogValNum, Len.Int.POG_VAL_NUM);
    }

    public void setPogValNumFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POG_VAL_NUM, Pos.POG_VAL_NUM);
    }

    /**Original name: POG-VAL-NUM<br>*/
    public long getPogValNum() {
        return readPackedAsLong(Pos.POG_VAL_NUM, Len.Int.POG_VAL_NUM);
    }

    public byte[] getPogValNumAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POG_VAL_NUM, Pos.POG_VAL_NUM);
        return buffer;
    }

    public void setPogValNumNull(String pogValNumNull) {
        writeString(Pos.POG_VAL_NUM_NULL, pogValNumNull, Len.POG_VAL_NUM_NULL);
    }

    /**Original name: POG-VAL-NUM-NULL<br>*/
    public String getPogValNumNull() {
        return readString(Pos.POG_VAL_NUM_NULL, Len.POG_VAL_NUM_NULL);
    }

    public String getPogValNumNullFormatted() {
        return Functions.padBlanks(getPogValNumNull(), Len.POG_VAL_NUM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POG_VAL_NUM = 1;
        public static final int POG_VAL_NUM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_VAL_NUM = 8;
        public static final int POG_VAL_NUM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_VAL_NUM = 14;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
