package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-TFR-STRC<br>
 * Variable: WTGA-IMP-TFR-STRC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_TFR_STRC;
    }

    public void setWtgaImpTfrStrc(AfDecimal wtgaImpTfrStrc) {
        writeDecimalAsPacked(Pos.WTGA_IMP_TFR_STRC, wtgaImpTfrStrc.copy());
    }

    public void setWtgaImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_TFR_STRC, Pos.WTGA_IMP_TFR_STRC);
    }

    /**Original name: WTGA-IMP-TFR-STRC<br>*/
    public AfDecimal getWtgaImpTfrStrc() {
        return readPackedAsDecimal(Pos.WTGA_IMP_TFR_STRC, Len.Int.WTGA_IMP_TFR_STRC, Len.Fract.WTGA_IMP_TFR_STRC);
    }

    public byte[] getWtgaImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_TFR_STRC, Pos.WTGA_IMP_TFR_STRC);
        return buffer;
    }

    public void initWtgaImpTfrStrcSpaces() {
        fill(Pos.WTGA_IMP_TFR_STRC, Len.WTGA_IMP_TFR_STRC, Types.SPACE_CHAR);
    }

    public void setWtgaImpTfrStrcNull(String wtgaImpTfrStrcNull) {
        writeString(Pos.WTGA_IMP_TFR_STRC_NULL, wtgaImpTfrStrcNull, Len.WTGA_IMP_TFR_STRC_NULL);
    }

    /**Original name: WTGA-IMP-TFR-STRC-NULL<br>*/
    public String getWtgaImpTfrStrcNull() {
        return readString(Pos.WTGA_IMP_TFR_STRC_NULL, Len.WTGA_IMP_TFR_STRC_NULL);
    }

    public String getWtgaImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getWtgaImpTfrStrcNull(), Len.WTGA_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_TFR_STRC = 1;
        public static final int WTGA_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_TFR_STRC = 8;
        public static final int WTGA_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
