package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idbvbje3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndBtcJobExecution;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0080<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0080Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-LUNG-EFF-BLOB
    private String wsLungEffBlob = DefaultValues.stringVal(Len.WS_LUNG_EFF_BLOB);
    //Original name: LCCC0090
    private LinkArea lccc0090 = new LinkArea();
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BTC-JOB-EXECUTION
    private IndBtcJobExecution indBtcJobExecution = new IndBtcJobExecution();
    //Original name: IDBVBJE3
    private Idbvbje3 idbvbje3 = new Idbvbje3();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsBufferWhereCondFormatted(String data) {
        byte[] buffer = new byte[Len.WS_BUFFER_WHERE_COND];
        MarshalByte.writeString(buffer, 1, data, Len.WS_BUFFER_WHERE_COND);
        setWsBufferWhereCondBytes(buffer, 1);
    }

    public void setWsBufferWhereCondBytes(byte[] buffer, int offset) {
        int position = offset;
        wsLungEffBlob = MarshalByte.readFixedString(buffer, position, Len.WS_LUNG_EFF_BLOB);
    }

    public int getWsLungEffBlob() {
        return NumericDisplay.asInt(this.wsLungEffBlob);
    }

    public Idbvbje3 getIdbvbje3() {
        return idbvbje3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndBtcJobExecution getIndBtcJobExecution() {
        return indBtcJobExecution;
    }

    public LinkArea getLccc0090() {
        return lccc0090;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WS_LUNG_EFF_BLOB = 9;
        public static final int WS_BUFFER_WHERE_COND = WS_LUNG_EFF_BLOB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
