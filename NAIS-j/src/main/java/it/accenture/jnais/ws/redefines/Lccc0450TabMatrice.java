package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;

/**Original name: LCCC0450-TAB-MATRICE<br>
 * Variable: LCCC0450-TAB-MATRICE from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0450TabMatrice extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int MATRICE_MAXOCCURS = 2000;

    //==== CONSTRUCTORS ====
    public Lccc0450TabMatrice() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0450_TAB_MATRICE;
    }

    public String getLccc0450TabMatriceFormatted() {
        return readFixedString(Pos.LCCC0450_TAB_MATRICE, Len.LCCC0450_TAB_MATRICE);
    }

    public void setLccc0450TabMatriceBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0450_TAB_MATRICE, Pos.LCCC0450_TAB_MATRICE);
    }

    public byte[] getLccc0450TabMatriceBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0450_TAB_MATRICE, Pos.LCCC0450_TAB_MATRICE);
        return buffer;
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.lccc0450IdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: LCCC0450-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.lccc0450IdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setCodFondo(int codFondoIdx, String codFondo) {
        int position = Pos.lccc0450CodFondo(codFondoIdx - 1);
        writeString(position, codFondo, Len.COD_FONDO);
    }

    /**Original name: LCCC0450-COD-FONDO<br>*/
    public String getCodFondo(int codFondoIdx) {
        int position = Pos.lccc0450CodFondo(codFondoIdx - 1);
        return readString(position, Len.COD_FONDO);
    }

    public void setNumQuote(int numQuoteIdx, AfDecimal numQuote) {
        int position = Pos.lccc0450NumQuote(numQuoteIdx - 1);
        writeDecimalAsPacked(position, numQuote.copy());
    }

    /**Original name: LCCC0450-NUM-QUOTE<br>*/
    public AfDecimal getNumQuote(int numQuoteIdx) {
        int position = Pos.lccc0450NumQuote(numQuoteIdx - 1);
        return readPackedAsDecimal(position, Len.Int.NUM_QUOTE, Len.Fract.NUM_QUOTE);
    }

    public void setValQuota(int valQuotaIdx, AfDecimal valQuota) {
        int position = Pos.lccc0450ValQuota(valQuotaIdx - 1);
        writeDecimalAsPacked(position, valQuota.copy());
    }

    /**Original name: LCCC0450-VAL-QUOTA<br>*/
    public AfDecimal getValQuota(int valQuotaIdx) {
        int position = Pos.lccc0450ValQuota(valQuotaIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_QUOTA, Len.Fract.VAL_QUOTA);
    }

    public void setNumQuoteIni(int numQuoteIniIdx, AfDecimal numQuoteIni) {
        int position = Pos.lccc0450NumQuoteIni(numQuoteIniIdx - 1);
        writeDecimalAsPacked(position, numQuoteIni.copy());
    }

    /**Original name: LCCC0450-NUM-QUOTE-INI<br>*/
    public AfDecimal getNumQuoteIni(int numQuoteIniIdx) {
        int position = Pos.lccc0450NumQuoteIni(numQuoteIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.NUM_QUOTE_INI, Len.Fract.NUM_QUOTE_INI);
    }

    public void setValQuotaIni(int valQuotaIniIdx, AfDecimal valQuotaIni) {
        int position = Pos.lccc0450ValQuotaIni(valQuotaIniIdx - 1);
        writeDecimalAsPacked(position, valQuotaIni.copy());
    }

    /**Original name: LCCC0450-VAL-QUOTA-INI<br>*/
    public AfDecimal getValQuotaIni(int valQuotaIniIdx) {
        int position = Pos.lccc0450ValQuotaIni(valQuotaIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_QUOTA_INI, Len.Fract.VAL_QUOTA_INI);
    }

    public void setValQuotaT(int valQuotaTIdx, AfDecimal valQuotaT) {
        int position = Pos.lccc0450ValQuotaT(valQuotaTIdx - 1);
        writeDecimalAsPacked(position, valQuotaT.copy());
    }

    /**Original name: LCCC0450-VAL-QUOTA-T<br>*/
    public AfDecimal getValQuotaT(int valQuotaTIdx) {
        int position = Pos.lccc0450ValQuotaT(valQuotaTIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VAL_QUOTA_T, Len.Fract.VAL_QUOTA_T);
    }

    public void setPercentInv(int percentInvIdx, AfDecimal percentInv) {
        int position = Pos.lccc0450PercentInv(percentInvIdx - 1);
        writeDecimalAsPacked(position, percentInv.copy());
    }

    /**Original name: LCCC0450-PERCENT-INV<br>*/
    public AfDecimal getPercentInv(int percentInvIdx) {
        int position = Pos.lccc0450PercentInv(percentInvIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PERCENT_INV, Len.Fract.PERCENT_INV);
    }

    public void setControvalore(int controvaloreIdx, AfDecimal controvalore) {
        int position = Pos.lccc0450Controvalore(controvaloreIdx - 1);
        writeDecimalAsPacked(position, controvalore.copy());
    }

    /**Original name: LCCC0450-CONTROVALORE<br>*/
    public AfDecimal getControvalore(int controvaloreIdx) {
        int position = Pos.lccc0450Controvalore(controvaloreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CONTROVALORE, Len.Fract.CONTROVALORE);
    }

    public void setDtValQuote(int dtValQuoteIdx, int dtValQuote) {
        int position = Pos.lccc0450DtValQuote(dtValQuoteIdx - 1);
        writeInt(position, dtValQuote, Len.Int.DT_VAL_QUOTE, SignType.NO_SIGN);
    }

    public void setDtValQuoteFormatted(int dtValQuoteIdx, String dtValQuote) {
        int position = Pos.lccc0450DtValQuote(dtValQuoteIdx - 1);
        writeString(position, Trunc.toUnsignedNumeric(dtValQuote, Len.DT_VAL_QUOTE), Len.DT_VAL_QUOTE);
    }

    /**Original name: LCCC0450-DT-VAL-QUOTE<br>*/
    public int getDtValQuote(int dtValQuoteIdx) {
        int position = Pos.lccc0450DtValQuote(dtValQuoteIdx - 1);
        return readNumDispUnsignedInt(position, Len.DT_VAL_QUOTE);
    }

    public void setImpInves(int impInvesIdx, AfDecimal impInves) {
        int position = Pos.lccc0450ImpInves(impInvesIdx - 1);
        writeDecimalAsPacked(position, impInves.copy());
    }

    /**Original name: LCCC0450-IMP-INVES<br>*/
    public AfDecimal getImpInves(int impInvesIdx) {
        int position = Pos.lccc0450ImpInves(impInvesIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_INVES, Len.Fract.IMP_INVES);
    }

    public void setImpDisinv(int impDisinvIdx, AfDecimal impDisinv) {
        int position = Pos.lccc0450ImpDisinv(impDisinvIdx - 1);
        writeDecimalAsPacked(position, impDisinv.copy());
    }

    /**Original name: LCCC0450-IMP-DISINV<br>*/
    public AfDecimal getImpDisinv(int impDisinvIdx) {
        int position = Pos.lccc0450ImpDisinv(impDisinvIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_DISINV, Len.Fract.IMP_DISINV);
    }

    public void setRestoTabMatrice(String restoTabMatrice) {
        writeString(Pos.RESTO_TAB_MATRICE, restoTabMatrice, Len.RESTO_TAB_MATRICE);
    }

    /**Original name: LCCC0450-RESTO-TAB-MATRICE<br>
	 * <pre>         12 LCCC0450-RESTO-TAB-MATRICE   PIC  X(98901).
	 *          12 LCCC0450-RESTO-TAB-MATRICE   PIC  X(148401).</pre>*/
    public String getRestoTabMatrice() {
        return readString(Pos.RESTO_TAB_MATRICE, Len.RESTO_TAB_MATRICE);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0450_TAB_MATRICE = 1;
        public static final int LCCC0450_TAB_MATRICE_R = 1;
        public static final int FLR1 = LCCC0450_TAB_MATRICE_R;
        public static final int RESTO_TAB_MATRICE = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int lccc0450Matrice(int idx) {
            return LCCC0450_TAB_MATRICE + idx * Len.MATRICE;
        }

        public static int lccc0450IdTrchDiGar(int idx) {
            return lccc0450Matrice(idx);
        }

        public static int lccc0450CodFondo(int idx) {
            return lccc0450IdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }

        public static int lccc0450NumQuote(int idx) {
            return lccc0450CodFondo(idx) + Len.COD_FONDO;
        }

        public static int lccc0450ValQuota(int idx) {
            return lccc0450NumQuote(idx) + Len.NUM_QUOTE;
        }

        public static int lccc0450NumQuoteIni(int idx) {
            return lccc0450ValQuota(idx) + Len.VAL_QUOTA;
        }

        public static int lccc0450ValQuotaIni(int idx) {
            return lccc0450NumQuoteIni(idx) + Len.NUM_QUOTE_INI;
        }

        public static int lccc0450ValQuotaT(int idx) {
            return lccc0450ValQuotaIni(idx) + Len.VAL_QUOTA_INI;
        }

        public static int lccc0450PercentInv(int idx) {
            return lccc0450ValQuotaT(idx) + Len.VAL_QUOTA_T;
        }

        public static int lccc0450Controvalore(int idx) {
            return lccc0450PercentInv(idx) + Len.PERCENT_INV;
        }

        public static int lccc0450DtValQuote(int idx) {
            return lccc0450Controvalore(idx) + Len.CONTROVALORE;
        }

        public static int lccc0450ImpInves(int idx) {
            return lccc0450DtValQuote(idx) + Len.DT_VAL_QUOTE;
        }

        public static int lccc0450ImpDisinv(int idx) {
            return lccc0450ImpInves(idx) + Len.IMP_INVES;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int COD_FONDO = 20;
        public static final int NUM_QUOTE = 7;
        public static final int VAL_QUOTA = 8;
        public static final int NUM_QUOTE_INI = 7;
        public static final int VAL_QUOTA_INI = 8;
        public static final int VAL_QUOTA_T = 8;
        public static final int PERCENT_INV = 4;
        public static final int CONTROVALORE = 8;
        public static final int DT_VAL_QUOTE = 8;
        public static final int IMP_INVES = 8;
        public static final int IMP_DISINV = 8;
        public static final int MATRICE = ID_TRCH_DI_GAR + COD_FONDO + NUM_QUOTE + VAL_QUOTA + NUM_QUOTE_INI + VAL_QUOTA_INI + VAL_QUOTA_T + PERCENT_INV + CONTROVALORE + DT_VAL_QUOTE + IMP_INVES + IMP_DISINV;
        public static final int FLR1 = 99;
        public static final int LCCC0450_TAB_MATRICE = Lccc0450TabMatrice.MATRICE_MAXOCCURS * MATRICE;
        public static final int RESTO_TAB_MATRICE = 197901;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int NUM_QUOTE = 7;
            public static final int VAL_QUOTA = 12;
            public static final int NUM_QUOTE_INI = 7;
            public static final int VAL_QUOTA_INI = 12;
            public static final int VAL_QUOTA_T = 12;
            public static final int PERCENT_INV = 3;
            public static final int CONTROVALORE = 12;
            public static final int DT_VAL_QUOTE = 8;
            public static final int IMP_INVES = 12;
            public static final int IMP_DISINV = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int NUM_QUOTE = 5;
            public static final int VAL_QUOTA = 3;
            public static final int NUM_QUOTE_INI = 5;
            public static final int VAL_QUOTA_INI = 3;
            public static final int VAL_QUOTA_T = 3;
            public static final int PERCENT_INV = 3;
            public static final int CONTROVALORE = 3;
            public static final int IMP_INVES = 3;
            public static final int IMP_DISINV = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
