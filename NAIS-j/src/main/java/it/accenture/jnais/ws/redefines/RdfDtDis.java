package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-DT-DIS<br>
 * Variable: RDF-DT-DIS from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfDtDis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfDtDis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_DT_DIS;
    }

    public void setRdfDtDis(int rdfDtDis) {
        writeIntAsPacked(Pos.RDF_DT_DIS, rdfDtDis, Len.Int.RDF_DT_DIS);
    }

    public void setRdfDtDisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_DT_DIS, Pos.RDF_DT_DIS);
    }

    /**Original name: RDF-DT-DIS<br>*/
    public int getRdfDtDis() {
        return readPackedAsInt(Pos.RDF_DT_DIS, Len.Int.RDF_DT_DIS);
    }

    public byte[] getRdfDtDisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_DT_DIS, Pos.RDF_DT_DIS);
        return buffer;
    }

    public void setRdfDtDisNull(String rdfDtDisNull) {
        writeString(Pos.RDF_DT_DIS_NULL, rdfDtDisNull, Len.RDF_DT_DIS_NULL);
    }

    /**Original name: RDF-DT-DIS-NULL<br>*/
    public String getRdfDtDisNull() {
        return readString(Pos.RDF_DT_DIS_NULL, Len.RDF_DT_DIS_NULL);
    }

    public String getRdfDtDisNullFormatted() {
        return Functions.padBlanks(getRdfDtDisNull(), Len.RDF_DT_DIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_DT_DIS = 1;
        public static final int RDF_DT_DIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_DT_DIS = 5;
        public static final int RDF_DT_DIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_DT_DIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
