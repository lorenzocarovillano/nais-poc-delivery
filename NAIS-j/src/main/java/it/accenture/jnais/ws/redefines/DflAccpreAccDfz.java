package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-ACC-DFZ<br>
 * Variable: DFL-ACCPRE-ACC-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreAccDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreAccDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_ACC_DFZ;
    }

    public void setDflAccpreAccDfz(AfDecimal dflAccpreAccDfz) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_ACC_DFZ, dflAccpreAccDfz.copy());
    }

    public void setDflAccpreAccDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_ACC_DFZ, Pos.DFL_ACCPRE_ACC_DFZ);
    }

    /**Original name: DFL-ACCPRE-ACC-DFZ<br>*/
    public AfDecimal getDflAccpreAccDfz() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_ACC_DFZ, Len.Int.DFL_ACCPRE_ACC_DFZ, Len.Fract.DFL_ACCPRE_ACC_DFZ);
    }

    public byte[] getDflAccpreAccDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_ACC_DFZ, Pos.DFL_ACCPRE_ACC_DFZ);
        return buffer;
    }

    public void setDflAccpreAccDfzNull(String dflAccpreAccDfzNull) {
        writeString(Pos.DFL_ACCPRE_ACC_DFZ_NULL, dflAccpreAccDfzNull, Len.DFL_ACCPRE_ACC_DFZ_NULL);
    }

    /**Original name: DFL-ACCPRE-ACC-DFZ-NULL<br>*/
    public String getDflAccpreAccDfzNull() {
        return readString(Pos.DFL_ACCPRE_ACC_DFZ_NULL, Len.DFL_ACCPRE_ACC_DFZ_NULL);
    }

    public String getDflAccpreAccDfzNullFormatted() {
        return Functions.padBlanks(getDflAccpreAccDfzNull(), Len.DFL_ACCPRE_ACC_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_ACC_DFZ = 1;
        public static final int DFL_ACCPRE_ACC_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_ACC_DFZ = 8;
        public static final int DFL_ACCPRE_ACC_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_ACC_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_ACC_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
