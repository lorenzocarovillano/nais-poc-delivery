package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SEQUENTIAL-ESITO<br>
 * Variable: SEQUENTIAL-ESITO from copybook IABCSQ99<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SequentialEsito {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.SEQUENTIAL_ESITO);
    public static final String OK = "OK";
    public static final String KO = "KO";

    //==== METHODS ====
    public void setSequentialEsito(String sequentialEsito) {
        this.value = Functions.subString(sequentialEsito, Len.SEQUENTIAL_ESITO);
    }

    public String getSequentialEsito() {
        return this.value;
    }

    public boolean isSequentialEsitoOk() {
        return value.equals(OK);
    }

    public void setSequentialEsitoOk() {
        value = OK;
    }

    public boolean isSequentialEsitoKo() {
        return value.equals(KO);
    }

    public void setSequentialEsitoKo() {
        value = KO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SEQUENTIAL_ESITO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
