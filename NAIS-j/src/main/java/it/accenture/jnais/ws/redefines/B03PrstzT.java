package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PRSTZ-T<br>
 * Variable: B03-PRSTZ-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03PrstzT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03PrstzT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PRSTZ_T;
    }

    public void setB03PrstzT(AfDecimal b03PrstzT) {
        writeDecimalAsPacked(Pos.B03_PRSTZ_T, b03PrstzT.copy());
    }

    public void setB03PrstzTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PRSTZ_T, Pos.B03_PRSTZ_T);
    }

    /**Original name: B03-PRSTZ-T<br>*/
    public AfDecimal getB03PrstzT() {
        return readPackedAsDecimal(Pos.B03_PRSTZ_T, Len.Int.B03_PRSTZ_T, Len.Fract.B03_PRSTZ_T);
    }

    public byte[] getB03PrstzTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PRSTZ_T, Pos.B03_PRSTZ_T);
        return buffer;
    }

    public void setB03PrstzTNull(String b03PrstzTNull) {
        writeString(Pos.B03_PRSTZ_T_NULL, b03PrstzTNull, Len.B03_PRSTZ_T_NULL);
    }

    /**Original name: B03-PRSTZ-T-NULL<br>*/
    public String getB03PrstzTNull() {
        return readString(Pos.B03_PRSTZ_T_NULL, Len.B03_PRSTZ_T_NULL);
    }

    public String getB03PrstzTNullFormatted() {
        return Functions.padBlanks(getB03PrstzTNull(), Len.B03_PRSTZ_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PRSTZ_T = 1;
        public static final int B03_PRSTZ_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PRSTZ_T = 8;
        public static final int B03_PRSTZ_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PRSTZ_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PRSTZ_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
