package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RICL-RIASS<br>
 * Variable: WPCO-DT-ULT-RICL-RIASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRiclRiass extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRiclRiass() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RICL_RIASS;
    }

    public void setWpcoDtUltRiclRiass(int wpcoDtUltRiclRiass) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RICL_RIASS, wpcoDtUltRiclRiass, Len.Int.WPCO_DT_ULT_RICL_RIASS);
    }

    public void setDpcoDtUltRiclRiassFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RICL_RIASS, Pos.WPCO_DT_ULT_RICL_RIASS);
    }

    /**Original name: WPCO-DT-ULT-RICL-RIASS<br>*/
    public int getWpcoDtUltRiclRiass() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RICL_RIASS, Len.Int.WPCO_DT_ULT_RICL_RIASS);
    }

    public byte[] getWpcoDtUltRiclRiassAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RICL_RIASS, Pos.WPCO_DT_ULT_RICL_RIASS);
        return buffer;
    }

    public void setWpcoDtUltRiclRiassNull(String wpcoDtUltRiclRiassNull) {
        writeString(Pos.WPCO_DT_ULT_RICL_RIASS_NULL, wpcoDtUltRiclRiassNull, Len.WPCO_DT_ULT_RICL_RIASS_NULL);
    }

    /**Original name: WPCO-DT-ULT-RICL-RIASS-NULL<br>*/
    public String getWpcoDtUltRiclRiassNull() {
        return readString(Pos.WPCO_DT_ULT_RICL_RIASS_NULL, Len.WPCO_DT_ULT_RICL_RIASS_NULL);
    }

    public String getWpcoDtUltRiclRiassNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRiclRiassNull(), Len.WPCO_DT_ULT_RICL_RIASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RICL_RIASS = 1;
        public static final int WPCO_DT_ULT_RICL_RIASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RICL_RIASS = 5;
        public static final int WPCO_DT_ULT_RICL_RIASS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RICL_RIASS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
