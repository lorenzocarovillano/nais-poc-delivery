package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: CAO-PROG-COND<br>
 * Variable: CAO-PROG-COND from program IVVS0212<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CaoProgCond extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public CaoProgCond() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CAO_PROG_COND;
    }

    public void setCaoProgCond(short caoProgCond) {
        writeShortAsPacked(Pos.CAO_PROG_COND, caoProgCond, Len.Int.CAO_PROG_COND);
    }

    /**Original name: CAO-PROG-COND<br>*/
    public short getCaoProgCond() {
        return readPackedAsShort(Pos.CAO_PROG_COND, Len.Int.CAO_PROG_COND);
    }

    public void setCaoProgCondNull(String caoProgCondNull) {
        writeString(Pos.CAO_PROG_COND_NULL, caoProgCondNull, Len.CAO_PROG_COND_NULL);
    }

    /**Original name: CAO-PROG-COND-NULL<br>*/
    public String getCaoProgCondNull() {
        return readString(Pos.CAO_PROG_COND_NULL, Len.CAO_PROG_COND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CAO_PROG_COND = 1;
        public static final int CAO_PROG_COND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CAO_PROG_COND = 2;
        public static final int CAO_PROG_COND_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CAO_PROG_COND = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
