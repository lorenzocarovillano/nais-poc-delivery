package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-TOT-AA-GIA-PROR<br>
 * Variable: WPMO-TOT-AA-GIA-PROR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoTotAaGiaPror extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoTotAaGiaPror() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_TOT_AA_GIA_PROR;
    }

    public void setWpmoTotAaGiaPror(int wpmoTotAaGiaPror) {
        writeIntAsPacked(Pos.WPMO_TOT_AA_GIA_PROR, wpmoTotAaGiaPror, Len.Int.WPMO_TOT_AA_GIA_PROR);
    }

    public void setWpmoTotAaGiaProrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_TOT_AA_GIA_PROR, Pos.WPMO_TOT_AA_GIA_PROR);
    }

    /**Original name: WPMO-TOT-AA-GIA-PROR<br>*/
    public int getWpmoTotAaGiaPror() {
        return readPackedAsInt(Pos.WPMO_TOT_AA_GIA_PROR, Len.Int.WPMO_TOT_AA_GIA_PROR);
    }

    public byte[] getWpmoTotAaGiaProrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_TOT_AA_GIA_PROR, Pos.WPMO_TOT_AA_GIA_PROR);
        return buffer;
    }

    public void initWpmoTotAaGiaProrSpaces() {
        fill(Pos.WPMO_TOT_AA_GIA_PROR, Len.WPMO_TOT_AA_GIA_PROR, Types.SPACE_CHAR);
    }

    public void setWpmoTotAaGiaProrNull(String wpmoTotAaGiaProrNull) {
        writeString(Pos.WPMO_TOT_AA_GIA_PROR_NULL, wpmoTotAaGiaProrNull, Len.WPMO_TOT_AA_GIA_PROR_NULL);
    }

    /**Original name: WPMO-TOT-AA-GIA-PROR-NULL<br>*/
    public String getWpmoTotAaGiaProrNull() {
        return readString(Pos.WPMO_TOT_AA_GIA_PROR_NULL, Len.WPMO_TOT_AA_GIA_PROR_NULL);
    }

    public String getWpmoTotAaGiaProrNullFormatted() {
        return Functions.padBlanks(getWpmoTotAaGiaProrNull(), Len.WPMO_TOT_AA_GIA_PROR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_TOT_AA_GIA_PROR = 1;
        public static final int WPMO_TOT_AA_GIA_PROR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_TOT_AA_GIA_PROR = 3;
        public static final int WPMO_TOT_AA_GIA_PROR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_TOT_AA_GIA_PROR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
