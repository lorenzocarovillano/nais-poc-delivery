package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-TIPO-POLIZZA<br>
 * Variable: ISPC0040-TIPO-POLIZZA from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040TipoPolizza {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPO_POLIZZA);
    public static final String COLL_TFR = "01";
    public static final String COLL_PREV = "02";
    public static final String COLL_GRUPPO = "03";
    public static final String INDIV_MORTE = "04";
    public static final String INDIV_VITA = "05";
    public static final String INDIV_MISTA = "06";
    public static final String INDIV_CAPITAL = "07";
    public static final String INDIV_FINANZI = "08";
    public static final String INDIV_MULTI_GAR = "09";
    public static final String INDIV_PIP = "10";
    public static final String INDIV_FIP = "11";

    //==== METHODS ====
    public void setTipoPolizza(String tipoPolizza) {
        this.value = Functions.subString(tipoPolizza, Len.TIPO_POLIZZA);
    }

    public String getTipoPolizza() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_POLIZZA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
