package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRSTZ-INI<br>
 * Variable: WPAG-PRSTZ-INI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrstzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrstzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRSTZ_INI;
    }

    public void setWpagPrstzIni(AfDecimal wpagPrstzIni) {
        writeDecimalAsPacked(Pos.WPAG_PRSTZ_INI, wpagPrstzIni.copy());
    }

    public void setWpagPrstzIniFormatted(String wpagPrstzIni) {
        setWpagPrstzIni(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRSTZ_INI + Len.Fract.WPAG_PRSTZ_INI, Len.Fract.WPAG_PRSTZ_INI, wpagPrstzIni));
    }

    public void setWpagPrstzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRSTZ_INI, Pos.WPAG_PRSTZ_INI);
    }

    /**Original name: WPAG-PRSTZ-INI<br>*/
    public AfDecimal getWpagPrstzIni() {
        return readPackedAsDecimal(Pos.WPAG_PRSTZ_INI, Len.Int.WPAG_PRSTZ_INI, Len.Fract.WPAG_PRSTZ_INI);
    }

    public byte[] getWpagPrstzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRSTZ_INI, Pos.WPAG_PRSTZ_INI);
        return buffer;
    }

    public void initWpagPrstzIniSpaces() {
        fill(Pos.WPAG_PRSTZ_INI, Len.WPAG_PRSTZ_INI, Types.SPACE_CHAR);
    }

    public void setWpagPrstzIniNull(String wpagPrstzIniNull) {
        writeString(Pos.WPAG_PRSTZ_INI_NULL, wpagPrstzIniNull, Len.WPAG_PRSTZ_INI_NULL);
    }

    /**Original name: WPAG-PRSTZ-INI-NULL<br>*/
    public String getWpagPrstzIniNull() {
        return readString(Pos.WPAG_PRSTZ_INI_NULL, Len.WPAG_PRSTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRSTZ_INI = 1;
        public static final int WPAG_PRSTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRSTZ_INI = 8;
        public static final int WPAG_PRSTZ_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRSTZ_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRSTZ_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
