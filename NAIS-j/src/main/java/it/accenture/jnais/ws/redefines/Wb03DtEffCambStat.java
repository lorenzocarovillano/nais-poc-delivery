package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-EFF-CAMB-STAT<br>
 * Variable: WB03-DT-EFF-CAMB-STAT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtEffCambStat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtEffCambStat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_EFF_CAMB_STAT;
    }

    public void setWb03DtEffCambStatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_EFF_CAMB_STAT, Pos.WB03_DT_EFF_CAMB_STAT);
    }

    /**Original name: WB03-DT-EFF-CAMB-STAT<br>*/
    public int getWb03DtEffCambStat() {
        return readPackedAsInt(Pos.WB03_DT_EFF_CAMB_STAT, Len.Int.WB03_DT_EFF_CAMB_STAT);
    }

    public byte[] getWb03DtEffCambStatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_EFF_CAMB_STAT, Pos.WB03_DT_EFF_CAMB_STAT);
        return buffer;
    }

    public void setWb03DtEffCambStatNull(String wb03DtEffCambStatNull) {
        writeString(Pos.WB03_DT_EFF_CAMB_STAT_NULL, wb03DtEffCambStatNull, Len.WB03_DT_EFF_CAMB_STAT_NULL);
    }

    /**Original name: WB03-DT-EFF-CAMB-STAT-NULL<br>*/
    public String getWb03DtEffCambStatNull() {
        return readString(Pos.WB03_DT_EFF_CAMB_STAT_NULL, Len.WB03_DT_EFF_CAMB_STAT_NULL);
    }

    public String getWb03DtEffCambStatNullFormatted() {
        return Functions.padBlanks(getWb03DtEffCambStatNull(), Len.WB03_DT_EFF_CAMB_STAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_EFF_CAMB_STAT = 1;
        public static final int WB03_DT_EFF_CAMB_STAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_EFF_CAMB_STAT = 5;
        public static final int WB03_DT_EFF_CAMB_STAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_EFF_CAMB_STAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
