package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-TP-OBL<br>
 * Variable: FLAG-TP-OBL from program LOAS9000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTpObl {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_TP_OBL);
    public static final String POLIZZA = "PO";
    public static final String ADESIONE = "AD";

    //==== METHODS ====
    public void setFlagTpObl(String flagTpObl) {
        this.value = Functions.subString(flagTpObl, Len.FLAG_TP_OBL);
    }

    public String getFlagTpObl() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_TP_OBL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
