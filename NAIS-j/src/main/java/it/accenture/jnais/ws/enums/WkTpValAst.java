package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TP-VAL-AST<br>
 * Variable: WK-TP-VAL-AST from program LVVS0116<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTpValAst {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_TP_VAL_AST);
    public static final String VALORE_POSITIVO = "VP";
    public static final String ANNULLO_POSITIVO = "AP";
    public static final String VALORE_NEGATIVO = "VN";
    public static final String ANNULLO_NEGATIVO = "AN";
    public static final String VAS_LIQUIDAZIONE = "LQ";
    public static final String ANNULLO_LIQUIDAZIONE = "AL";

    //==== METHODS ====
    public void setWkTpValAst(String wkTpValAst) {
        this.value = Functions.subString(wkTpValAst, Len.WK_TP_VAL_AST);
    }

    public String getWkTpValAst() {
        return this.value;
    }

    public boolean isValorePositivo() {
        return value.equals(VALORE_POSITIVO);
    }

    public boolean isAnnulloPositivo() {
        return value.equals(ANNULLO_POSITIVO);
    }

    public boolean isValoreNegativo() {
        return value.equals(VALORE_NEGATIVO);
    }

    public boolean isAnnulloNegativo() {
        return value.equals(ANNULLO_NEGATIVO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TP_VAL_AST = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
