package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-INTEG-BAS-TEC<br>
 * Variable: RST-RIS-INTEG-BAS-TEC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisIntegBasTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisIntegBasTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_INTEG_BAS_TEC;
    }

    public void setRstRisIntegBasTec(AfDecimal rstRisIntegBasTec) {
        writeDecimalAsPacked(Pos.RST_RIS_INTEG_BAS_TEC, rstRisIntegBasTec.copy());
    }

    public void setRstRisIntegBasTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_INTEG_BAS_TEC, Pos.RST_RIS_INTEG_BAS_TEC);
    }

    /**Original name: RST-RIS-INTEG-BAS-TEC<br>*/
    public AfDecimal getRstRisIntegBasTec() {
        return readPackedAsDecimal(Pos.RST_RIS_INTEG_BAS_TEC, Len.Int.RST_RIS_INTEG_BAS_TEC, Len.Fract.RST_RIS_INTEG_BAS_TEC);
    }

    public byte[] getRstRisIntegBasTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_INTEG_BAS_TEC, Pos.RST_RIS_INTEG_BAS_TEC);
        return buffer;
    }

    public void setRstRisIntegBasTecNull(String rstRisIntegBasTecNull) {
        writeString(Pos.RST_RIS_INTEG_BAS_TEC_NULL, rstRisIntegBasTecNull, Len.RST_RIS_INTEG_BAS_TEC_NULL);
    }

    /**Original name: RST-RIS-INTEG-BAS-TEC-NULL<br>*/
    public String getRstRisIntegBasTecNull() {
        return readString(Pos.RST_RIS_INTEG_BAS_TEC_NULL, Len.RST_RIS_INTEG_BAS_TEC_NULL);
    }

    public String getRstRisIntegBasTecNullFormatted() {
        return Functions.padBlanks(getRstRisIntegBasTecNull(), Len.RST_RIS_INTEG_BAS_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_INTEG_BAS_TEC = 1;
        public static final int RST_RIS_INTEG_BAS_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_INTEG_BAS_TEC = 8;
        public static final int RST_RIS_INTEG_BAS_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_INTEG_BAS_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_INTEG_BAS_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
