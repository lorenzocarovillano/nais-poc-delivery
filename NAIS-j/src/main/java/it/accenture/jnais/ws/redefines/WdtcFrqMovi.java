package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-FRQ-MOVI<br>
 * Variable: WDTC-FRQ-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcFrqMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcFrqMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_FRQ_MOVI;
    }

    public void setWdtcFrqMovi(int wdtcFrqMovi) {
        writeIntAsPacked(Pos.WDTC_FRQ_MOVI, wdtcFrqMovi, Len.Int.WDTC_FRQ_MOVI);
    }

    public void setWdtcFrqMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_FRQ_MOVI, Pos.WDTC_FRQ_MOVI);
    }

    /**Original name: WDTC-FRQ-MOVI<br>*/
    public int getWdtcFrqMovi() {
        return readPackedAsInt(Pos.WDTC_FRQ_MOVI, Len.Int.WDTC_FRQ_MOVI);
    }

    public byte[] getWdtcFrqMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_FRQ_MOVI, Pos.WDTC_FRQ_MOVI);
        return buffer;
    }

    public void initWdtcFrqMoviSpaces() {
        fill(Pos.WDTC_FRQ_MOVI, Len.WDTC_FRQ_MOVI, Types.SPACE_CHAR);
    }

    public void setWdtcFrqMoviNull(String wdtcFrqMoviNull) {
        writeString(Pos.WDTC_FRQ_MOVI_NULL, wdtcFrqMoviNull, Len.WDTC_FRQ_MOVI_NULL);
    }

    /**Original name: WDTC-FRQ-MOVI-NULL<br>*/
    public String getWdtcFrqMoviNull() {
        return readString(Pos.WDTC_FRQ_MOVI_NULL, Len.WDTC_FRQ_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_FRQ_MOVI = 1;
        public static final int WDTC_FRQ_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_FRQ_MOVI = 3;
        public static final int WDTC_FRQ_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_FRQ_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
