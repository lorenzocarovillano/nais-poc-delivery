package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-DUR-MM<br>
 * Variable: POL-DUR-MM from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_DUR_MM;
    }

    public void setPolDurMm(int polDurMm) {
        writeIntAsPacked(Pos.POL_DUR_MM, polDurMm, Len.Int.POL_DUR_MM);
    }

    public void setPolDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_DUR_MM, Pos.POL_DUR_MM);
    }

    /**Original name: POL-DUR-MM<br>*/
    public int getPolDurMm() {
        return readPackedAsInt(Pos.POL_DUR_MM, Len.Int.POL_DUR_MM);
    }

    public byte[] getPolDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_DUR_MM, Pos.POL_DUR_MM);
        return buffer;
    }

    public void setPolDurMmNull(String polDurMmNull) {
        writeString(Pos.POL_DUR_MM_NULL, polDurMmNull, Len.POL_DUR_MM_NULL);
    }

    /**Original name: POL-DUR-MM-NULL<br>*/
    public String getPolDurMmNull() {
        return readString(Pos.POL_DUR_MM_NULL, Len.POL_DUR_MM_NULL);
    }

    public String getPolDurMmNullFormatted() {
        return Functions.padBlanks(getPolDurMmNull(), Len.POL_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_DUR_MM = 1;
        public static final int POL_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_DUR_MM = 3;
        public static final int POL_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
