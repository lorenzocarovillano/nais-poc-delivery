package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.Lccc0062TabRate;

/**Original name: WCOM-AREA-PAGINA<br>
 * Variable: WCOM-AREA-PAGINA from program LCCS0062<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WcomAreaPagina extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_RATE_MAXOCCURS = 12;
    //Original name: LCCC0062-DT-COMPETENZA
    private int dtCompetenza = DefaultValues.INT_VAL;
    //Original name: LCCC0062-DT-DECORRENZA
    private int dtDecorrenza = DefaultValues.INT_VAL;
    //Original name: LCCC0062-DT-ULT-QTZO
    private int dtUltQtzo = DefaultValues.INT_VAL;
    //Original name: LCCC0062-DT-ULTGZ-TRCH
    private int dtUltgzTrch = DefaultValues.INT_VAL;
    //Original name: LCCC0062-FRAZIONAMENTO
    private String frazionamento = DefaultValues.stringVal(Len.FRAZIONAMENTO);
    //Original name: LCCC0062-TOT-NUM-RATE
    private long totNumRate = DefaultValues.LONG_VAL;
    //Original name: LCCC0062-TAB-RATE
    private Lccc0062TabRate[] tabRate = new Lccc0062TabRate[TAB_RATE_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WcomAreaPagina() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCOM_AREA_PAGINA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWcomAreaPaginaBytes(buf);
    }

    public void init() {
        for (int tabRateIdx = 1; tabRateIdx <= TAB_RATE_MAXOCCURS; tabRateIdx++) {
            tabRate[tabRateIdx - 1] = new Lccc0062TabRate();
        }
    }

    public String getAreaLccc0062Formatted() {
        return MarshalByteExt.bufferToStr(getWcomAreaPaginaBytes());
    }

    public void setWcomAreaPaginaBytes(byte[] buffer) {
        setWcomAreaPaginaBytes(buffer, 1);
    }

    public byte[] getWcomAreaPaginaBytes() {
        byte[] buffer = new byte[Len.WCOM_AREA_PAGINA];
        return getWcomAreaPaginaBytes(buffer, 1);
    }

    public void setWcomAreaPaginaBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaInputBytes(buffer, position);
        position += Len.AREA_INPUT;
        setAreaOutputBytes(buffer, position);
    }

    public byte[] getWcomAreaPaginaBytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaInputBytes(buffer, position);
        position += Len.AREA_INPUT;
        getAreaOutputBytes(buffer, position);
        return buffer;
    }

    public void setAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        dtCompetenza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_COMPETENZA, 0, SignType.NO_SIGN);
        position += Len.DT_COMPETENZA;
        dtDecorrenza = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECORRENZA, 0, SignType.NO_SIGN);
        position += Len.DT_DECORRENZA;
        dtUltQtzo = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_ULT_QTZO, 0, SignType.NO_SIGN);
        position += Len.DT_ULT_QTZO;
        dtUltgzTrch = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_ULTGZ_TRCH, 0, SignType.NO_SIGN);
        position += Len.DT_ULTGZ_TRCH;
        frazionamento = MarshalByte.readFixedString(buffer, position, Len.FRAZIONAMENTO);
    }

    public byte[] getAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, dtCompetenza, Len.Int.DT_COMPETENZA, 0, SignType.NO_SIGN);
        position += Len.DT_COMPETENZA;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecorrenza, Len.Int.DT_DECORRENZA, 0, SignType.NO_SIGN);
        position += Len.DT_DECORRENZA;
        MarshalByte.writeIntAsPacked(buffer, position, dtUltQtzo, Len.Int.DT_ULT_QTZO, 0, SignType.NO_SIGN);
        position += Len.DT_ULT_QTZO;
        MarshalByte.writeIntAsPacked(buffer, position, dtUltgzTrch, Len.Int.DT_ULTGZ_TRCH, 0, SignType.NO_SIGN);
        position += Len.DT_ULTGZ_TRCH;
        MarshalByte.writeString(buffer, position, frazionamento, Len.FRAZIONAMENTO);
        return buffer;
    }

    public void setDtCompetenza(int dtCompetenza) {
        this.dtCompetenza = dtCompetenza;
    }

    public int getDtCompetenza() {
        return this.dtCompetenza;
    }

    public void setDtDecorrenza(int dtDecorrenza) {
        this.dtDecorrenza = dtDecorrenza;
    }

    public int getDtDecorrenza() {
        return this.dtDecorrenza;
    }

    public String getDtDecorrenzaFormatted() {
        return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.PACKED)).format(getDtDecorrenza()).toString();
    }

    public void setDtUltQtzo(int dtUltQtzo) {
        this.dtUltQtzo = dtUltQtzo;
    }

    public int getDtUltQtzo() {
        return this.dtUltQtzo;
    }

    public void setDtUltgzTrch(int dtUltgzTrch) {
        this.dtUltgzTrch = dtUltgzTrch;
    }

    public int getDtUltgzTrch() {
        return this.dtUltgzTrch;
    }

    public void setFrazionamentoFormatted(String frazionamento) {
        this.frazionamento = Trunc.toUnsignedNumeric(frazionamento, Len.FRAZIONAMENTO);
    }

    public int getFrazionamento() {
        return NumericDisplay.asInt(this.frazionamento);
    }

    public String getFrazionamentoFormatted() {
        return this.frazionamento;
    }

    public void setAreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        totNumRate = MarshalByte.readPackedAsLong(buffer, position, Len.Int.TOT_NUM_RATE, 0);
        position += Len.TOT_NUM_RATE;
        for (int idx = 1; idx <= TAB_RATE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabRate[idx - 1].setTabRateBytes(buffer, position);
                position += Lccc0062TabRate.Len.TAB_RATE;
            }
            else {
                tabRate[idx - 1].initTabRateSpaces();
                position += Lccc0062TabRate.Len.TAB_RATE;
            }
        }
    }

    public byte[] getAreaOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeLongAsPacked(buffer, position, totNumRate, Len.Int.TOT_NUM_RATE, 0);
        position += Len.TOT_NUM_RATE;
        for (int idx = 1; idx <= TAB_RATE_MAXOCCURS; idx++) {
            tabRate[idx - 1].getTabRateBytes(buffer, position);
            position += Lccc0062TabRate.Len.TAB_RATE;
        }
        return buffer;
    }

    public void setTotNumRate(long totNumRate) {
        this.totNumRate = totNumRate;
    }

    public long getTotNumRate() {
        return this.totNumRate;
    }

    public Lccc0062TabRate getTabRate(int idx) {
        return tabRate[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWcomAreaPaginaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_COMPETENZA = 5;
        public static final int DT_DECORRENZA = 5;
        public static final int DT_ULT_QTZO = 5;
        public static final int DT_ULTGZ_TRCH = 5;
        public static final int FRAZIONAMENTO = 5;
        public static final int AREA_INPUT = DT_COMPETENZA + DT_DECORRENZA + DT_ULT_QTZO + DT_ULTGZ_TRCH + FRAZIONAMENTO;
        public static final int TOT_NUM_RATE = 8;
        public static final int AREA_OUTPUT = TOT_NUM_RATE + WcomAreaPagina.TAB_RATE_MAXOCCURS * Lccc0062TabRate.Len.TAB_RATE;
        public static final int WCOM_AREA_PAGINA = AREA_INPUT + AREA_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DT_COMPETENZA = 8;
            public static final int DT_DECORRENZA = 8;
            public static final int DT_ULT_QTZO = 8;
            public static final int DT_ULTGZ_TRCH = 8;
            public static final int TOT_NUM_RATE = 14;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
