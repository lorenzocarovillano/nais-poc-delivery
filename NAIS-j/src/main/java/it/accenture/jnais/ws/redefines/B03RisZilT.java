package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-ZIL-T<br>
 * Variable: B03-RIS-ZIL-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisZilT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisZilT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_ZIL_T;
    }

    public void setB03RisZilT(AfDecimal b03RisZilT) {
        writeDecimalAsPacked(Pos.B03_RIS_ZIL_T, b03RisZilT.copy());
    }

    public void setB03RisZilTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_ZIL_T, Pos.B03_RIS_ZIL_T);
    }

    /**Original name: B03-RIS-ZIL-T<br>*/
    public AfDecimal getB03RisZilT() {
        return readPackedAsDecimal(Pos.B03_RIS_ZIL_T, Len.Int.B03_RIS_ZIL_T, Len.Fract.B03_RIS_ZIL_T);
    }

    public byte[] getB03RisZilTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_ZIL_T, Pos.B03_RIS_ZIL_T);
        return buffer;
    }

    public void setB03RisZilTNull(String b03RisZilTNull) {
        writeString(Pos.B03_RIS_ZIL_T_NULL, b03RisZilTNull, Len.B03_RIS_ZIL_T_NULL);
    }

    /**Original name: B03-RIS-ZIL-T-NULL<br>*/
    public String getB03RisZilTNull() {
        return readString(Pos.B03_RIS_ZIL_T_NULL, Len.B03_RIS_ZIL_T_NULL);
    }

    public String getB03RisZilTNullFormatted() {
        return Functions.padBlanks(getB03RisZilTNull(), Len.B03_RIS_ZIL_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_ZIL_T = 1;
        public static final int B03_RIS_ZIL_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_ZIL_T = 8;
        public static final int B03_RIS_ZIL_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_ZIL_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_ZIL_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
