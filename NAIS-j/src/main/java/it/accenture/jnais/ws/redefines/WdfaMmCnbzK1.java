package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-MM-CNBZ-K1<br>
 * Variable: WDFA-MM-CNBZ-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMmCnbzK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMmCnbzK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MM_CNBZ_K1;
    }

    public void setWdfaMmCnbzK1(short wdfaMmCnbzK1) {
        writeShortAsPacked(Pos.WDFA_MM_CNBZ_K1, wdfaMmCnbzK1, Len.Int.WDFA_MM_CNBZ_K1);
    }

    public void setWdfaMmCnbzK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MM_CNBZ_K1, Pos.WDFA_MM_CNBZ_K1);
    }

    /**Original name: WDFA-MM-CNBZ-K1<br>*/
    public short getWdfaMmCnbzK1() {
        return readPackedAsShort(Pos.WDFA_MM_CNBZ_K1, Len.Int.WDFA_MM_CNBZ_K1);
    }

    public byte[] getWdfaMmCnbzK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MM_CNBZ_K1, Pos.WDFA_MM_CNBZ_K1);
        return buffer;
    }

    public void setWdfaMmCnbzK1Null(String wdfaMmCnbzK1Null) {
        writeString(Pos.WDFA_MM_CNBZ_K1_NULL, wdfaMmCnbzK1Null, Len.WDFA_MM_CNBZ_K1_NULL);
    }

    /**Original name: WDFA-MM-CNBZ-K1-NULL<br>*/
    public String getWdfaMmCnbzK1Null() {
        return readString(Pos.WDFA_MM_CNBZ_K1_NULL, Len.WDFA_MM_CNBZ_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MM_CNBZ_K1 = 1;
        public static final int WDFA_MM_CNBZ_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MM_CNBZ_K1 = 3;
        public static final int WDFA_MM_CNBZ_K1_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MM_CNBZ_K1 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
