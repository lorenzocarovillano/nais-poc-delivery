package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-BOLLO-TOT-V<br>
 * Variable: BEL-IMPST-BOLLO-TOT-V from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstBolloTotV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstBolloTotV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_BOLLO_TOT_V;
    }

    public void setBelImpstBolloTotV(AfDecimal belImpstBolloTotV) {
        writeDecimalAsPacked(Pos.BEL_IMPST_BOLLO_TOT_V, belImpstBolloTotV.copy());
    }

    public void setBelImpstBolloTotVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_BOLLO_TOT_V, Pos.BEL_IMPST_BOLLO_TOT_V);
    }

    /**Original name: BEL-IMPST-BOLLO-TOT-V<br>*/
    public AfDecimal getBelImpstBolloTotV() {
        return readPackedAsDecimal(Pos.BEL_IMPST_BOLLO_TOT_V, Len.Int.BEL_IMPST_BOLLO_TOT_V, Len.Fract.BEL_IMPST_BOLLO_TOT_V);
    }

    public byte[] getBelImpstBolloTotVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_BOLLO_TOT_V, Pos.BEL_IMPST_BOLLO_TOT_V);
        return buffer;
    }

    public void setBelImpstBolloTotVNull(String belImpstBolloTotVNull) {
        writeString(Pos.BEL_IMPST_BOLLO_TOT_V_NULL, belImpstBolloTotVNull, Len.BEL_IMPST_BOLLO_TOT_V_NULL);
    }

    /**Original name: BEL-IMPST-BOLLO-TOT-V-NULL<br>*/
    public String getBelImpstBolloTotVNull() {
        return readString(Pos.BEL_IMPST_BOLLO_TOT_V_NULL, Len.BEL_IMPST_BOLLO_TOT_V_NULL);
    }

    public String getBelImpstBolloTotVNullFormatted() {
        return Functions.padBlanks(getBelImpstBolloTotVNull(), Len.BEL_IMPST_BOLLO_TOT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_BOLLO_TOT_V = 1;
        public static final int BEL_IMPST_BOLLO_TOT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_BOLLO_TOT_V = 8;
        public static final int BEL_IMPST_BOLLO_TOT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_BOLLO_TOT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_BOLLO_TOT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
