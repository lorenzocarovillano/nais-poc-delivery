package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WORK-CALCOLI<br>
 * Variable: WORK-CALCOLI from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkCalcoli {

    //==== PROPERTIES ====
    //Original name: WS-DIFF
    private AfDecimal wsDiff = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-DIFF-3
    private AfDecimal wsDiff3 = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WS-DIFF-2
    private AfDecimal wsDiff2 = new AfDecimal(DefaultValues.DEC_VAL, 14, 2);
    //Original name: WS-DIV
    private AfDecimal wsDiv = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-PERC
    private AfDecimal wsPerc = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-SOMMA
    private AfDecimal wsSomma = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-SOMMA-2
    private AfDecimal wsSomma2 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-SOMMA-3
    private AfDecimal wsSomma3 = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WS-SOMMA-4
    private AfDecimal wsSomma4 = new AfDecimal(DefaultValues.DEC_VAL, 12, 5);
    //Original name: WS-SOMMA-123
    private AfDecimal wsSomma123 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-DIFF-123
    private AfDecimal wsDiff123 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-CNTRVAL-PREC
    private AfDecimal wsCntrvalPrec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-APPO-43-1
    private AfDecimal wsAppo431 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-APPO-43-2
    private AfDecimal wsAppo432 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-APPO-1
    private AfDecimal wsAppo1 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-APPO-2
    private AfDecimal wsAppo2 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-APPO-3
    private AfDecimal wsAppo3 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-CAMPO-APPOGGIO
    private AfDecimal wsCampoAppoggio = new AfDecimal(DefaultValues.DEC_VAL, 5, 2);
    //Original name: WK-TASSO-TEC-RV
    private AfDecimal wkTassoTecRv = new AfDecimal(DefaultValues.DEC_VAL, 14, 9);

    //==== METHODS ====
    public void setWsDiff(AfDecimal wsDiff) {
        this.wsDiff.assign(wsDiff);
    }

    public AfDecimal getWsDiff() {
        return this.wsDiff.copy();
    }

    public void setWsDiff3(AfDecimal wsDiff3) {
        this.wsDiff3.assign(wsDiff3);
    }

    public AfDecimal getWsDiff3() {
        return this.wsDiff3.copy();
    }

    public void setWsDiff2(AfDecimal wsDiff2) {
        this.wsDiff2.assign(wsDiff2);
    }

    public AfDecimal getWsDiff2() {
        return this.wsDiff2.copy();
    }

    public void setWsDiv(AfDecimal wsDiv) {
        this.wsDiv.assign(wsDiv);
    }

    public AfDecimal getWsDiv() {
        return this.wsDiv.copy();
    }

    public void setWsPerc(AfDecimal wsPerc) {
        this.wsPerc.assign(wsPerc);
    }

    public AfDecimal getWsPerc() {
        return this.wsPerc.copy();
    }

    public void setWsSomma(AfDecimal wsSomma) {
        this.wsSomma.assign(wsSomma);
    }

    public AfDecimal getWsSomma() {
        return this.wsSomma.copy();
    }

    public void setWsSomma2(AfDecimal wsSomma2) {
        this.wsSomma2.assign(wsSomma2);
    }

    public AfDecimal getWsSomma2() {
        return this.wsSomma2.copy();
    }

    public void setWsSomma3(AfDecimal wsSomma3) {
        this.wsSomma3.assign(wsSomma3);
    }

    public AfDecimal getWsSomma3() {
        return this.wsSomma3.copy();
    }

    public void setWsSomma4(AfDecimal wsSomma4) {
        this.wsSomma4.assign(wsSomma4);
    }

    public AfDecimal getWsSomma4() {
        return this.wsSomma4.copy();
    }

    public void setWsSomma123(AfDecimal wsSomma123) {
        this.wsSomma123.assign(wsSomma123);
    }

    public AfDecimal getWsSomma123() {
        return this.wsSomma123.copy();
    }

    public void setWsDiff123(AfDecimal wsDiff123) {
        this.wsDiff123.assign(wsDiff123);
    }

    public AfDecimal getWsDiff123() {
        return this.wsDiff123.copy();
    }

    public void setWsCntrvalPrec(AfDecimal wsCntrvalPrec) {
        this.wsCntrvalPrec.assign(wsCntrvalPrec);
    }

    public AfDecimal getWsCntrvalPrec() {
        return this.wsCntrvalPrec.copy();
    }

    public void setWsAppo431(AfDecimal wsAppo431) {
        this.wsAppo431.assign(wsAppo431);
    }

    public AfDecimal getWsAppo431() {
        return this.wsAppo431.copy();
    }

    public void setWsAppo432(AfDecimal wsAppo432) {
        this.wsAppo432.assign(wsAppo432);
    }

    public AfDecimal getWsAppo432() {
        return this.wsAppo432.copy();
    }

    public void setWsAppo1(AfDecimal wsAppo1) {
        this.wsAppo1.assign(wsAppo1);
    }

    public AfDecimal getWsAppo1() {
        return this.wsAppo1.copy();
    }

    public void setWsAppo2(AfDecimal wsAppo2) {
        this.wsAppo2.assign(wsAppo2);
    }

    public AfDecimal getWsAppo2() {
        return this.wsAppo2.copy();
    }

    public void setWsAppo3(AfDecimal wsAppo3) {
        this.wsAppo3.assign(wsAppo3);
    }

    public AfDecimal getWsAppo3() {
        return this.wsAppo3.copy();
    }

    public void setWsCampoAppoggio(AfDecimal wsCampoAppoggio) {
        this.wsCampoAppoggio.assign(wsCampoAppoggio);
    }

    public AfDecimal getWsCampoAppoggio() {
        return this.wsCampoAppoggio.copy();
    }

    public void setWkTassoTecRv(AfDecimal wkTassoTecRv) {
        this.wkTassoTecRv.assign(wkTassoTecRv);
    }

    public AfDecimal getWkTassoTecRv() {
        return this.wkTassoTecRv.copy();
    }
}
