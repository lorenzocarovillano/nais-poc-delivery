package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTrchDiGar;
import it.accenture.jnais.copy.Ldbv0011;
import it.accenture.jnais.copy.TrchDiGarDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0130<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0130Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TRCH-DI-GAR
    private IndTrchDiGar indTrchDiGar = new IndTrchDiGar();
    //Original name: TRCH-DI-GAR-DB
    private TrchDiGarDb trchDiGarDb = new TrchDiGarDb();
    //Original name: LDBV0011
    private Ldbv0011 ldbv0011 = new Ldbv0011();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTrchDiGar getIndTrchDiGar() {
        return indTrchDiGar;
    }

    public Ldbv0011 getLdbv0011() {
        return ldbv0011;
    }

    public TrchDiGarDb getTrchDiGarDb() {
        return trchDiGarDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
