package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-IMP-TFR-STRC<br>
 * Variable: WDTC-IMP-TFR-STRC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_IMP_TFR_STRC;
    }

    public void setWdtcImpTfrStrc(AfDecimal wdtcImpTfrStrc) {
        writeDecimalAsPacked(Pos.WDTC_IMP_TFR_STRC, wdtcImpTfrStrc.copy());
    }

    public void setWdtcImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_IMP_TFR_STRC, Pos.WDTC_IMP_TFR_STRC);
    }

    /**Original name: WDTC-IMP-TFR-STRC<br>*/
    public AfDecimal getWdtcImpTfrStrc() {
        return readPackedAsDecimal(Pos.WDTC_IMP_TFR_STRC, Len.Int.WDTC_IMP_TFR_STRC, Len.Fract.WDTC_IMP_TFR_STRC);
    }

    public byte[] getWdtcImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_IMP_TFR_STRC, Pos.WDTC_IMP_TFR_STRC);
        return buffer;
    }

    public void initWdtcImpTfrStrcSpaces() {
        fill(Pos.WDTC_IMP_TFR_STRC, Len.WDTC_IMP_TFR_STRC, Types.SPACE_CHAR);
    }

    public void setWdtcImpTfrStrcNull(String wdtcImpTfrStrcNull) {
        writeString(Pos.WDTC_IMP_TFR_STRC_NULL, wdtcImpTfrStrcNull, Len.WDTC_IMP_TFR_STRC_NULL);
    }

    /**Original name: WDTC-IMP-TFR-STRC-NULL<br>*/
    public String getWdtcImpTfrStrcNull() {
        return readString(Pos.WDTC_IMP_TFR_STRC_NULL, Len.WDTC_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_TFR_STRC = 1;
        public static final int WDTC_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_TFR_STRC = 8;
        public static final int WDTC_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
