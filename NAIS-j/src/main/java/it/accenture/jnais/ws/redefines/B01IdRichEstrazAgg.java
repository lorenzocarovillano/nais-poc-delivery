package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-ID-RICH-ESTRAZ-AGG<br>
 * Variable: B01-ID-RICH-ESTRAZ-AGG from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_ID_RICH_ESTRAZ_AGG;
    }

    public void setB01IdRichEstrazAgg(int b01IdRichEstrazAgg) {
        writeIntAsPacked(Pos.B01_ID_RICH_ESTRAZ_AGG, b01IdRichEstrazAgg, Len.Int.B01_ID_RICH_ESTRAZ_AGG);
    }

    public void setB01IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_ID_RICH_ESTRAZ_AGG, Pos.B01_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: B01-ID-RICH-ESTRAZ-AGG<br>*/
    public int getB01IdRichEstrazAgg() {
        return readPackedAsInt(Pos.B01_ID_RICH_ESTRAZ_AGG, Len.Int.B01_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getB01IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_ID_RICH_ESTRAZ_AGG, Pos.B01_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setB01IdRichEstrazAggNull(String b01IdRichEstrazAggNull) {
        writeString(Pos.B01_ID_RICH_ESTRAZ_AGG_NULL, b01IdRichEstrazAggNull, Len.B01_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: B01-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getB01IdRichEstrazAggNull() {
        return readString(Pos.B01_ID_RICH_ESTRAZ_AGG_NULL, Len.B01_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getB01IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getB01IdRichEstrazAggNull(), Len.B01_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_ID_RICH_ESTRAZ_AGG = 1;
        public static final int B01_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_ID_RICH_ESTRAZ_AGG = 5;
        public static final int B01_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
