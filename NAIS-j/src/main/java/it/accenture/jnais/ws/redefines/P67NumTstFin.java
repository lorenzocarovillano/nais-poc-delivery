package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-NUM-TST-FIN<br>
 * Variable: P67-NUM-TST-FIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67NumTstFin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67NumTstFin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_NUM_TST_FIN;
    }

    public void setP67NumTstFin(int p67NumTstFin) {
        writeIntAsPacked(Pos.P67_NUM_TST_FIN, p67NumTstFin, Len.Int.P67_NUM_TST_FIN);
    }

    public void setP67NumTstFinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_NUM_TST_FIN, Pos.P67_NUM_TST_FIN);
    }

    /**Original name: P67-NUM-TST-FIN<br>*/
    public int getP67NumTstFin() {
        return readPackedAsInt(Pos.P67_NUM_TST_FIN, Len.Int.P67_NUM_TST_FIN);
    }

    public byte[] getP67NumTstFinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_NUM_TST_FIN, Pos.P67_NUM_TST_FIN);
        return buffer;
    }

    public void setP67NumTstFinNull(String p67NumTstFinNull) {
        writeString(Pos.P67_NUM_TST_FIN_NULL, p67NumTstFinNull, Len.P67_NUM_TST_FIN_NULL);
    }

    /**Original name: P67-NUM-TST-FIN-NULL<br>*/
    public String getP67NumTstFinNull() {
        return readString(Pos.P67_NUM_TST_FIN_NULL, Len.P67_NUM_TST_FIN_NULL);
    }

    public String getP67NumTstFinNullFormatted() {
        return Functions.padBlanks(getP67NumTstFinNull(), Len.P67_NUM_TST_FIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_NUM_TST_FIN = 1;
        public static final int P67_NUM_TST_FIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_NUM_TST_FIN = 3;
        public static final int P67_NUM_TST_FIN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_NUM_TST_FIN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
