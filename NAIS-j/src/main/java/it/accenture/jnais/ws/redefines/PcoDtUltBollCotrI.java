package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-COTR-I<br>
 * Variable: PCO-DT-ULT-BOLL-COTR-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollCotrI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollCotrI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_COTR_I;
    }

    public void setPcoDtUltBollCotrI(int pcoDtUltBollCotrI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_COTR_I, pcoDtUltBollCotrI, Len.Int.PCO_DT_ULT_BOLL_COTR_I);
    }

    public void setPcoDtUltBollCotrIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_COTR_I, Pos.PCO_DT_ULT_BOLL_COTR_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-COTR-I<br>*/
    public int getPcoDtUltBollCotrI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_COTR_I, Len.Int.PCO_DT_ULT_BOLL_COTR_I);
    }

    public byte[] getPcoDtUltBollCotrIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_COTR_I, Pos.PCO_DT_ULT_BOLL_COTR_I);
        return buffer;
    }

    public void initPcoDtUltBollCotrIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_COTR_I, Len.PCO_DT_ULT_BOLL_COTR_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollCotrINull(String pcoDtUltBollCotrINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_COTR_I_NULL, pcoDtUltBollCotrINull, Len.PCO_DT_ULT_BOLL_COTR_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-COTR-I-NULL<br>*/
    public String getPcoDtUltBollCotrINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_COTR_I_NULL, Len.PCO_DT_ULT_BOLL_COTR_I_NULL);
    }

    public String getPcoDtUltBollCotrINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollCotrINull(), Len.PCO_DT_ULT_BOLL_COTR_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_COTR_I = 1;
        public static final int PCO_DT_ULT_BOLL_COTR_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_COTR_I = 5;
        public static final int PCO_DT_ULT_BOLL_COTR_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_COTR_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
