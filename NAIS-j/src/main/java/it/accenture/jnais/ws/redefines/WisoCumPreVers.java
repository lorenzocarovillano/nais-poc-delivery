package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-CUM-PRE-VERS<br>
 * Variable: WISO-CUM-PRE-VERS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoCumPreVers extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoCumPreVers() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_CUM_PRE_VERS;
    }

    public void setWisoCumPreVers(AfDecimal wisoCumPreVers) {
        writeDecimalAsPacked(Pos.WISO_CUM_PRE_VERS, wisoCumPreVers.copy());
    }

    public void setWisoCumPreVersFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_CUM_PRE_VERS, Pos.WISO_CUM_PRE_VERS);
    }

    /**Original name: WISO-CUM-PRE-VERS<br>*/
    public AfDecimal getWisoCumPreVers() {
        return readPackedAsDecimal(Pos.WISO_CUM_PRE_VERS, Len.Int.WISO_CUM_PRE_VERS, Len.Fract.WISO_CUM_PRE_VERS);
    }

    public byte[] getWisoCumPreVersAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_CUM_PRE_VERS, Pos.WISO_CUM_PRE_VERS);
        return buffer;
    }

    public void initWisoCumPreVersSpaces() {
        fill(Pos.WISO_CUM_PRE_VERS, Len.WISO_CUM_PRE_VERS, Types.SPACE_CHAR);
    }

    public void setWisoCumPreVersNull(String wisoCumPreVersNull) {
        writeString(Pos.WISO_CUM_PRE_VERS_NULL, wisoCumPreVersNull, Len.WISO_CUM_PRE_VERS_NULL);
    }

    /**Original name: WISO-CUM-PRE-VERS-NULL<br>*/
    public String getWisoCumPreVersNull() {
        return readString(Pos.WISO_CUM_PRE_VERS_NULL, Len.WISO_CUM_PRE_VERS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_CUM_PRE_VERS = 1;
        public static final int WISO_CUM_PRE_VERS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_CUM_PRE_VERS = 8;
        public static final int WISO_CUM_PRE_VERS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_CUM_PRE_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_CUM_PRE_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
