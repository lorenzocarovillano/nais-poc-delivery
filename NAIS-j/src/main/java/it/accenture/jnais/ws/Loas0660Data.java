package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.WkIspcMax;
import it.accenture.jnais.ws.enums.WkIntmoraVal;
import it.accenture.jnais.ws.enums.WkManfee;
import it.accenture.jnais.ws.enums.WkWtgaVal;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpDato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0660<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0660Data {

    //==== PROPERTIES ====
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: IX-INDICI
    private IxIndiciLoas0660 ixIndici = new IxIndiciLoas0660();
    //Original name: WK-ISPC-MAX
    private WkIspcMax wkIspcMax = new WkIspcMax();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: WK-GESTIONE-MSG-ERR
    private WkGestioneMsgErr wkGestioneMsgErr = new WkGestioneMsgErr();
    //Original name: WK-DATA
    private String wkData = "00000000";
    /**Original name: WK-CURRENT-DATE<br>
	 * <pre> --> Area di accept della data dalla variabile di sistema DATE</pre>*/
    private String wkCurrentDate = "00000000";
    /**Original name: WK-MANFEE<br>
	 * <pre>----------------------------------------------------------------*
	 *      VARIABILI PER GESTIONE MANAGEMENT FEE
	 * ----------------------------------------------------------------*
	 *  --> Indicatore del calcolo del Management Fee</pre>*/
    private WkManfee wkManfee = new WkManfee();
    /**Original name: FORMATO<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO</pre>*/
    private char formato = DefaultValues.CHAR_VAL;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0490 dataInferiore = new DataInferioreLccs0490();
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0490 dataSuperiore = new DataSuperioreLccs0490();
    //Original name: GG-DIFF
    private String ggDiff = DefaultValues.stringVal(Len.GG_DIFF);
    //Original name: CODICE-RITORNO
    private char codiceRitorno = DefaultValues.CHAR_VAL;
    /**Original name: WK-WTGA-VAL<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILE INDICATORE DI MODIFICA DATO DI TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*
	 *  --> Indicatore di valorizzazione della Tranche Di Garanzia</pre>*/
    private WkWtgaVal wkWtgaVal = new WkWtgaVal();
    /**Original name: WK-INTMORA-VAL<br>
	 * <pre> --> Indicatore di valorizzazione della variabile INTMORA</pre>*/
    private WkIntmoraVal wkIntmoraVal = new WkIntmoraVal();
    //Original name: WK-PGM
    private String wkPgm = "LOAS0660";
    /**Original name: WK-PERCINDU<br>
	 * <pre>----------------------------------------------------------------*
	 *      AREA VARIABILI COSTI IDD FASE 2
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkPercindu = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WK-PRESTLORD
    private AfDecimal wkPrestlord = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-COSTO-ABS
    private AfDecimal wkCostoAbs = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-PPI
    private AfDecimal wkPpi = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: AREA-IO-ISPS0211
    private AreaIoIsps0211 areaIoIsps0211 = new AreaIoIsps0211();
    //Original name: W800-AREA-PAG
    private W800AreaPag w800AreaPag = new W800AreaPag();
    //Original name: LCCV0021
    private Lccv0021 lccv0021 = new Lccv0021();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-DATO<br>
	 * <pre>*****************************************************************
	 *     TP_D (Tipo Dato)
	 * *****************************************************************</pre>*/
    private WsTpDato wsTpDato = new WsTpDato();

    //==== CONSTRUCTORS ====
    public Loas0660Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
    }

    public void setWkData(int wkData) {
        this.wkData = NumericDisplay.asString(wkData, Len.WK_DATA);
    }

    public int getWkData() {
        return NumericDisplay.asInt(this.wkData);
    }

    public String getWkDataFormatted() {
        return this.wkData;
    }

    public void setWkCurrentDateFormatted(String wkCurrentDate) {
        this.wkCurrentDate = Trunc.toUnsignedNumeric(wkCurrentDate, Len.WK_CURRENT_DATE);
    }

    public int getWkCurrentDate() {
        return NumericDisplay.asInt(this.wkCurrentDate);
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setFormatoFormatted(String formato) {
        setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        formato = MarshalByte.readChar(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        ggDiff = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    public String getGgDiffFormatted() {
        return this.ggDiff;
    }

    public void setCodiceRitorno(char codiceRitorno) {
        this.codiceRitorno = codiceRitorno;
    }

    public void setCodiceRitornoFromBuffer(byte[] buffer) {
        codiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getCodiceRitorno() {
        return this.codiceRitorno;
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkPercindu(AfDecimal wkPercindu) {
        this.wkPercindu.assign(wkPercindu);
    }

    public AfDecimal getWkPercindu() {
        return this.wkPercindu.copy();
    }

    public void setWkPrestlord(AfDecimal wkPrestlord) {
        this.wkPrestlord.assign(wkPrestlord);
    }

    public AfDecimal getWkPrestlord() {
        return this.wkPrestlord.copy();
    }

    public void setWkCostoAbs(AfDecimal wkCostoAbs) {
        this.wkCostoAbs.assign(wkCostoAbs);
    }

    public AfDecimal getWkCostoAbs() {
        return this.wkCostoAbs.copy();
    }

    public void setWkPpi(AfDecimal wkPpi) {
        this.wkPpi.assign(wkPpi);
    }

    public AfDecimal getWkPpi() {
        return this.wkPpi.copy();
    }

    public AreaIoIsps0211 getAreaIoIsps0211() {
        return areaIoIsps0211;
    }

    public DataInferioreLccs0490 getDataInferiore() {
        return dataInferiore;
    }

    public DataSuperioreLccs0490 getDataSuperiore() {
        return dataSuperiore;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IxIndiciLoas0660 getIxIndici() {
        return ixIndici;
    }

    public Lccv0021 getLccv0021() {
        return lccv0021;
    }

    public W800AreaPag getW800AreaPag() {
        return w800AreaPag;
    }

    public WkGestioneMsgErr getWkGestioneMsgErr() {
        return wkGestioneMsgErr;
    }

    public WkIntmoraVal getWkIntmoraVal() {
        return wkIntmoraVal;
    }

    public WkIspcMax getWkIspcMax() {
        return wkIspcMax;
    }

    public WkManfee getWkManfee() {
        return wkManfee;
    }

    public WkWtgaVal getWkWtgaVal() {
        return wkWtgaVal;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpDato getWsTpDato() {
        return wsTpDato;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CURRENT_DATE = 8;
        public static final int GG_DIFF = 5;
        public static final int WK_DATA = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
