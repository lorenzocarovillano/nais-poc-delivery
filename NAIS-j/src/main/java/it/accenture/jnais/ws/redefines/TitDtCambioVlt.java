package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-CAMBIO-VLT<br>
 * Variable: TIT-DT-CAMBIO-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtCambioVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtCambioVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_CAMBIO_VLT;
    }

    public void setTitDtCambioVlt(int titDtCambioVlt) {
        writeIntAsPacked(Pos.TIT_DT_CAMBIO_VLT, titDtCambioVlt, Len.Int.TIT_DT_CAMBIO_VLT);
    }

    public void setTitDtCambioVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_CAMBIO_VLT, Pos.TIT_DT_CAMBIO_VLT);
    }

    /**Original name: TIT-DT-CAMBIO-VLT<br>*/
    public int getTitDtCambioVlt() {
        return readPackedAsInt(Pos.TIT_DT_CAMBIO_VLT, Len.Int.TIT_DT_CAMBIO_VLT);
    }

    public byte[] getTitDtCambioVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_CAMBIO_VLT, Pos.TIT_DT_CAMBIO_VLT);
        return buffer;
    }

    public void setTitDtCambioVltNull(String titDtCambioVltNull) {
        writeString(Pos.TIT_DT_CAMBIO_VLT_NULL, titDtCambioVltNull, Len.TIT_DT_CAMBIO_VLT_NULL);
    }

    /**Original name: TIT-DT-CAMBIO-VLT-NULL<br>*/
    public String getTitDtCambioVltNull() {
        return readString(Pos.TIT_DT_CAMBIO_VLT_NULL, Len.TIT_DT_CAMBIO_VLT_NULL);
    }

    public String getTitDtCambioVltNullFormatted() {
        return Functions.padBlanks(getTitDtCambioVltNull(), Len.TIT_DT_CAMBIO_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_CAMBIO_VLT = 1;
        public static final int TIT_DT_CAMBIO_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_CAMBIO_VLT = 5;
        public static final int TIT_DT_CAMBIO_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_CAMBIO_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
