package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.UnzipTab;

/**Original name: UNZIP-STRUCTURE<br>
 * Variable: UNZIP-STRUCTURE from program IVVS0212<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UnzipStructure {

    //==== PROPERTIES ====
    //Original name: UNZIP-LENGTH-STR-MAX
    private String unzipLengthStrMax = DefaultValues.stringVal(Len.UNZIP_LENGTH_STR_MAX);
    //Original name: UNZIP-LENGTH-FIELD
    private String unzipLengthField = DefaultValues.stringVal(Len.UNZIP_LENGTH_FIELD);
    //Original name: UNZIP-STRING-ZIPPED
    private String unzipStringZipped = DefaultValues.stringVal(Len.UNZIP_STRING_ZIPPED);
    //Original name: UNZIP-IDENTIFICATORE
    private char unzipIdentificatore = DefaultValues.CHAR_VAL;
    //Original name: UNZIP-ELE-VARIABILI-MAX
    private short unzipEleVariabiliMax = DefaultValues.SHORT_VAL;
    //Original name: UNZIP-TAB
    private UnzipTab unzipTab = new UnzipTab();

    //==== METHODS ====
    public void setUnzipLengthStrMax(short unzipLengthStrMax) {
        this.unzipLengthStrMax = NumericDisplay.asString(unzipLengthStrMax, Len.UNZIP_LENGTH_STR_MAX);
    }

    public void setUnzipLengthStrMaxFormatted(String unzipLengthStrMax) {
        this.unzipLengthStrMax = Trunc.toUnsignedNumeric(unzipLengthStrMax, Len.UNZIP_LENGTH_STR_MAX);
    }

    public short getUnzipLengthStrMax() {
        return NumericDisplay.asShort(this.unzipLengthStrMax);
    }

    public void setUnzipLengthField(short unzipLengthField) {
        this.unzipLengthField = NumericDisplay.asString(unzipLengthField, Len.UNZIP_LENGTH_FIELD);
    }

    public void setUnzipLengthFieldFormatted(String unzipLengthField) {
        this.unzipLengthField = Trunc.toUnsignedNumeric(unzipLengthField, Len.UNZIP_LENGTH_FIELD);
    }

    public short getUnzipLengthField() {
        return NumericDisplay.asShort(this.unzipLengthField);
    }

    public void setUnzipStringZipped(String unzipStringZipped) {
        this.unzipStringZipped = Functions.subString(unzipStringZipped, Len.UNZIP_STRING_ZIPPED);
    }

    public String getUnzipStringZipped() {
        return this.unzipStringZipped;
    }

    public String getUnzipStringZippedFormatted() {
        return Functions.padBlanks(getUnzipStringZipped(), Len.UNZIP_STRING_ZIPPED);
    }

    public void setUnzipIdentificatore(char unzipIdentificatore) {
        this.unzipIdentificatore = unzipIdentificatore;
    }

    public void setUnzipIdentificatoreFormatted(String unzipIdentificatore) {
        setUnzipIdentificatore(Functions.charAt(unzipIdentificatore, Types.CHAR_SIZE));
    }

    public char getUnzipIdentificatore() {
        return this.unzipIdentificatore;
    }

    public void setUnzipEleVariabiliMax(short unzipEleVariabiliMax) {
        this.unzipEleVariabiliMax = unzipEleVariabiliMax;
    }

    public short getUnzipEleVariabiliMax() {
        return this.unzipEleVariabiliMax;
    }

    public UnzipTab getUnzipTab() {
        return unzipTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int UNZIP_LENGTH_STR_MAX = 4;
        public static final int UNZIP_LENGTH_FIELD = 2;
        public static final int UNZIP_STRING_ZIPPED = 4000;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
