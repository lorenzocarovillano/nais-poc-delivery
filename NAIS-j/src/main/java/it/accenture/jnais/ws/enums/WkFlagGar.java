package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FLAG-GAR<br>
 * Variable: WK-FLAG-GAR from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFlagGar {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFlagGar(char wkFlagGar) {
        this.value = wkFlagGar;
    }

    public char getWkFlagGar() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
