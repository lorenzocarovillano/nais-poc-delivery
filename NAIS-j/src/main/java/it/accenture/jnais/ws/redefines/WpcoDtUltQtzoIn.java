package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-QTZO-IN<br>
 * Variable: WPCO-DT-ULT-QTZO-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltQtzoIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltQtzoIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_QTZO_IN;
    }

    public void setWpcoDtUltQtzoIn(int wpcoDtUltQtzoIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_QTZO_IN, wpcoDtUltQtzoIn, Len.Int.WPCO_DT_ULT_QTZO_IN);
    }

    public void setDpcoDtUltQtzoInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_QTZO_IN, Pos.WPCO_DT_ULT_QTZO_IN);
    }

    /**Original name: WPCO-DT-ULT-QTZO-IN<br>*/
    public int getWpcoDtUltQtzoIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_QTZO_IN, Len.Int.WPCO_DT_ULT_QTZO_IN);
    }

    public byte[] getWpcoDtUltQtzoInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_QTZO_IN, Pos.WPCO_DT_ULT_QTZO_IN);
        return buffer;
    }

    public void setWpcoDtUltQtzoInNull(String wpcoDtUltQtzoInNull) {
        writeString(Pos.WPCO_DT_ULT_QTZO_IN_NULL, wpcoDtUltQtzoInNull, Len.WPCO_DT_ULT_QTZO_IN_NULL);
    }

    /**Original name: WPCO-DT-ULT-QTZO-IN-NULL<br>*/
    public String getWpcoDtUltQtzoInNull() {
        return readString(Pos.WPCO_DT_ULT_QTZO_IN_NULL, Len.WPCO_DT_ULT_QTZO_IN_NULL);
    }

    public String getWpcoDtUltQtzoInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltQtzoInNull(), Len.WPCO_DT_ULT_QTZO_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_QTZO_IN = 1;
        public static final int WPCO_DT_ULT_QTZO_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_QTZO_IN = 5;
        public static final int WPCO_DT_ULT_QTZO_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_QTZO_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
