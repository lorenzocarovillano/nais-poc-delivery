package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-INTR-MORA<br>
 * Variable: TGA-INTR-MORA from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_INTR_MORA;
    }

    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        writeDecimalAsPacked(Pos.TGA_INTR_MORA, tgaIntrMora.copy());
    }

    public void setTgaIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_INTR_MORA, Pos.TGA_INTR_MORA);
    }

    /**Original name: TGA-INTR-MORA<br>*/
    public AfDecimal getTgaIntrMora() {
        return readPackedAsDecimal(Pos.TGA_INTR_MORA, Len.Int.TGA_INTR_MORA, Len.Fract.TGA_INTR_MORA);
    }

    public byte[] getTgaIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_INTR_MORA, Pos.TGA_INTR_MORA);
        return buffer;
    }

    public void setTgaIntrMoraNull(String tgaIntrMoraNull) {
        writeString(Pos.TGA_INTR_MORA_NULL, tgaIntrMoraNull, Len.TGA_INTR_MORA_NULL);
    }

    /**Original name: TGA-INTR-MORA-NULL<br>*/
    public String getTgaIntrMoraNull() {
        return readString(Pos.TGA_INTR_MORA_NULL, Len.TGA_INTR_MORA_NULL);
    }

    public String getTgaIntrMoraNullFormatted() {
        return Functions.padBlanks(getTgaIntrMoraNull(), Len.TGA_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_INTR_MORA = 1;
        public static final int TGA_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_INTR_MORA = 8;
        public static final int TGA_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
