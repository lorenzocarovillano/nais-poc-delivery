package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-RICH<br>
 * Variable: LQU-DT-RICH from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_RICH;
    }

    public void setLquDtRich(int lquDtRich) {
        writeIntAsPacked(Pos.LQU_DT_RICH, lquDtRich, Len.Int.LQU_DT_RICH);
    }

    public void setLquDtRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_RICH, Pos.LQU_DT_RICH);
    }

    /**Original name: LQU-DT-RICH<br>*/
    public int getLquDtRich() {
        return readPackedAsInt(Pos.LQU_DT_RICH, Len.Int.LQU_DT_RICH);
    }

    public byte[] getLquDtRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_RICH, Pos.LQU_DT_RICH);
        return buffer;
    }

    public void setLquDtRichNull(String lquDtRichNull) {
        writeString(Pos.LQU_DT_RICH_NULL, lquDtRichNull, Len.LQU_DT_RICH_NULL);
    }

    /**Original name: LQU-DT-RICH-NULL<br>*/
    public String getLquDtRichNull() {
        return readString(Pos.LQU_DT_RICH_NULL, Len.LQU_DT_RICH_NULL);
    }

    public String getLquDtRichNullFormatted() {
        return Functions.padBlanks(getLquDtRichNull(), Len.LQU_DT_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_RICH = 1;
        public static final int LQU_DT_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_RICH = 5;
        public static final int LQU_DT_RICH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_RICH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
