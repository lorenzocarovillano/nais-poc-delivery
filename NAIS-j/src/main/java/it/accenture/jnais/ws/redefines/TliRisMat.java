package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-RIS-MAT<br>
 * Variable: TLI-RIS-MAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_RIS_MAT;
    }

    public void setTliRisMat(AfDecimal tliRisMat) {
        writeDecimalAsPacked(Pos.TLI_RIS_MAT, tliRisMat.copy());
    }

    public void setTliRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_RIS_MAT, Pos.TLI_RIS_MAT);
    }

    /**Original name: TLI-RIS-MAT<br>*/
    public AfDecimal getTliRisMat() {
        return readPackedAsDecimal(Pos.TLI_RIS_MAT, Len.Int.TLI_RIS_MAT, Len.Fract.TLI_RIS_MAT);
    }

    public byte[] getTliRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_RIS_MAT, Pos.TLI_RIS_MAT);
        return buffer;
    }

    public void setTliRisMatNull(String tliRisMatNull) {
        writeString(Pos.TLI_RIS_MAT_NULL, tliRisMatNull, Len.TLI_RIS_MAT_NULL);
    }

    /**Original name: TLI-RIS-MAT-NULL<br>*/
    public String getTliRisMatNull() {
        return readString(Pos.TLI_RIS_MAT_NULL, Len.TLI_RIS_MAT_NULL);
    }

    public String getTliRisMatNullFormatted() {
        return Functions.padBlanks(getTliRisMatNull(), Len.TLI_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_RIS_MAT = 1;
        public static final int TLI_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_RIS_MAT = 8;
        public static final int TLI_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
