package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-ID-2O-ASSTO<br>
 * Variable: GRZ-ID-2O-ASSTO from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzId2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzId2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_ID2O_ASSTO;
    }

    public void setGrzId2oAssto(int grzId2oAssto) {
        writeIntAsPacked(Pos.GRZ_ID2O_ASSTO, grzId2oAssto, Len.Int.GRZ_ID2O_ASSTO);
    }

    public void setGrzId2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_ID2O_ASSTO, Pos.GRZ_ID2O_ASSTO);
    }

    /**Original name: GRZ-ID-2O-ASSTO<br>*/
    public int getGrzId2oAssto() {
        return readPackedAsInt(Pos.GRZ_ID2O_ASSTO, Len.Int.GRZ_ID2O_ASSTO);
    }

    public byte[] getGrzId2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_ID2O_ASSTO, Pos.GRZ_ID2O_ASSTO);
        return buffer;
    }

    public void setGrzId2oAsstoNull(String grzId2oAsstoNull) {
        writeString(Pos.GRZ_ID2O_ASSTO_NULL, grzId2oAsstoNull, Len.GRZ_ID2O_ASSTO_NULL);
    }

    /**Original name: GRZ-ID-2O-ASSTO-NULL<br>*/
    public String getGrzId2oAsstoNull() {
        return readString(Pos.GRZ_ID2O_ASSTO_NULL, Len.GRZ_ID2O_ASSTO_NULL);
    }

    public String getGrzId2oAsstoNullFormatted() {
        return Functions.padBlanks(getGrzId2oAsstoNull(), Len.GRZ_ID2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_ID2O_ASSTO = 1;
        public static final int GRZ_ID2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_ID2O_ASSTO = 5;
        public static final int GRZ_ID2O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_ID2O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
