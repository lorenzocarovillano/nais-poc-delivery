package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-ULT-COMMIS-TRASFE<br>
 * Variable: DFA-ULT-COMMIS-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaUltCommisTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaUltCommisTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_ULT_COMMIS_TRASFE;
    }

    public void setDfaUltCommisTrasfe(AfDecimal dfaUltCommisTrasfe) {
        writeDecimalAsPacked(Pos.DFA_ULT_COMMIS_TRASFE, dfaUltCommisTrasfe.copy());
    }

    public void setDfaUltCommisTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_ULT_COMMIS_TRASFE, Pos.DFA_ULT_COMMIS_TRASFE);
    }

    /**Original name: DFA-ULT-COMMIS-TRASFE<br>*/
    public AfDecimal getDfaUltCommisTrasfe() {
        return readPackedAsDecimal(Pos.DFA_ULT_COMMIS_TRASFE, Len.Int.DFA_ULT_COMMIS_TRASFE, Len.Fract.DFA_ULT_COMMIS_TRASFE);
    }

    public byte[] getDfaUltCommisTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_ULT_COMMIS_TRASFE, Pos.DFA_ULT_COMMIS_TRASFE);
        return buffer;
    }

    public void setDfaUltCommisTrasfeNull(String dfaUltCommisTrasfeNull) {
        writeString(Pos.DFA_ULT_COMMIS_TRASFE_NULL, dfaUltCommisTrasfeNull, Len.DFA_ULT_COMMIS_TRASFE_NULL);
    }

    /**Original name: DFA-ULT-COMMIS-TRASFE-NULL<br>*/
    public String getDfaUltCommisTrasfeNull() {
        return readString(Pos.DFA_ULT_COMMIS_TRASFE_NULL, Len.DFA_ULT_COMMIS_TRASFE_NULL);
    }

    public String getDfaUltCommisTrasfeNullFormatted() {
        return Functions.padBlanks(getDfaUltCommisTrasfeNull(), Len.DFA_ULT_COMMIS_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_ULT_COMMIS_TRASFE = 1;
        public static final int DFA_ULT_COMMIS_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_ULT_COMMIS_TRASFE = 8;
        public static final int DFA_ULT_COMMIS_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_ULT_COMMIS_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_ULT_COMMIS_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
