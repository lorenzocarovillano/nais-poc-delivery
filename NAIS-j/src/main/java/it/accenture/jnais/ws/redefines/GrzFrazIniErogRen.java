package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-FRAZ-INI-EROG-REN<br>
 * Variable: GRZ-FRAZ-INI-EROG-REN from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzFrazIniErogRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzFrazIniErogRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_FRAZ_INI_EROG_REN;
    }

    public void setGrzFrazIniErogRen(int grzFrazIniErogRen) {
        writeIntAsPacked(Pos.GRZ_FRAZ_INI_EROG_REN, grzFrazIniErogRen, Len.Int.GRZ_FRAZ_INI_EROG_REN);
    }

    public void setGrzFrazIniErogRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_FRAZ_INI_EROG_REN, Pos.GRZ_FRAZ_INI_EROG_REN);
    }

    /**Original name: GRZ-FRAZ-INI-EROG-REN<br>*/
    public int getGrzFrazIniErogRen() {
        return readPackedAsInt(Pos.GRZ_FRAZ_INI_EROG_REN, Len.Int.GRZ_FRAZ_INI_EROG_REN);
    }

    public byte[] getGrzFrazIniErogRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_FRAZ_INI_EROG_REN, Pos.GRZ_FRAZ_INI_EROG_REN);
        return buffer;
    }

    public void setGrzFrazIniErogRenNull(String grzFrazIniErogRenNull) {
        writeString(Pos.GRZ_FRAZ_INI_EROG_REN_NULL, grzFrazIniErogRenNull, Len.GRZ_FRAZ_INI_EROG_REN_NULL);
    }

    /**Original name: GRZ-FRAZ-INI-EROG-REN-NULL<br>*/
    public String getGrzFrazIniErogRenNull() {
        return readString(Pos.GRZ_FRAZ_INI_EROG_REN_NULL, Len.GRZ_FRAZ_INI_EROG_REN_NULL);
    }

    public String getGrzFrazIniErogRenNullFormatted() {
        return Functions.padBlanks(getGrzFrazIniErogRenNull(), Len.GRZ_FRAZ_INI_EROG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_FRAZ_INI_EROG_REN = 1;
        public static final int GRZ_FRAZ_INI_EROG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_FRAZ_INI_EROG_REN = 3;
        public static final int GRZ_FRAZ_INI_EROG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_FRAZ_INI_EROG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
