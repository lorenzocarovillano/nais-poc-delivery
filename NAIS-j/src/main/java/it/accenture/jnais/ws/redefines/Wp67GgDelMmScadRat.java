package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-GG-DEL-MM-SCAD-RAT<br>
 * Variable: WP67-GG-DEL-MM-SCAD-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67GgDelMmScadRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67GgDelMmScadRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_GG_DEL_MM_SCAD_RAT;
    }

    public void setWp67GgDelMmScadRat(short wp67GgDelMmScadRat) {
        writeShortAsPacked(Pos.WP67_GG_DEL_MM_SCAD_RAT, wp67GgDelMmScadRat, Len.Int.WP67_GG_DEL_MM_SCAD_RAT);
    }

    public void setWp67GgDelMmScadRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_GG_DEL_MM_SCAD_RAT, Pos.WP67_GG_DEL_MM_SCAD_RAT);
    }

    /**Original name: WP67-GG-DEL-MM-SCAD-RAT<br>*/
    public short getWp67GgDelMmScadRat() {
        return readPackedAsShort(Pos.WP67_GG_DEL_MM_SCAD_RAT, Len.Int.WP67_GG_DEL_MM_SCAD_RAT);
    }

    public byte[] getWp67GgDelMmScadRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_GG_DEL_MM_SCAD_RAT, Pos.WP67_GG_DEL_MM_SCAD_RAT);
        return buffer;
    }

    public void setWp67GgDelMmScadRatNull(String wp67GgDelMmScadRatNull) {
        writeString(Pos.WP67_GG_DEL_MM_SCAD_RAT_NULL, wp67GgDelMmScadRatNull, Len.WP67_GG_DEL_MM_SCAD_RAT_NULL);
    }

    /**Original name: WP67-GG-DEL-MM-SCAD-RAT-NULL<br>*/
    public String getWp67GgDelMmScadRatNull() {
        return readString(Pos.WP67_GG_DEL_MM_SCAD_RAT_NULL, Len.WP67_GG_DEL_MM_SCAD_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_GG_DEL_MM_SCAD_RAT = 1;
        public static final int WP67_GG_DEL_MM_SCAD_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_GG_DEL_MM_SCAD_RAT = 2;
        public static final int WP67_GG_DEL_MM_SCAD_RAT_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_GG_DEL_MM_SCAD_RAT = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
