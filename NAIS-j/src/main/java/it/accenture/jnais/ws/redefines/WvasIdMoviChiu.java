package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WVAS-ID-MOVI-CHIU<br>
 * Variable: WVAS-ID-MOVI-CHIU from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_ID_MOVI_CHIU;
    }

    public void setWvasIdMoviChiu(int wvasIdMoviChiu) {
        writeIntAsPacked(Pos.WVAS_ID_MOVI_CHIU, wvasIdMoviChiu, Len.Int.WVAS_ID_MOVI_CHIU);
    }

    /**Original name: WVAS-ID-MOVI-CHIU<br>*/
    public int getWvasIdMoviChiu() {
        return readPackedAsInt(Pos.WVAS_ID_MOVI_CHIU, Len.Int.WVAS_ID_MOVI_CHIU);
    }

    public void setWvasIdMoviChiuNull(String wvasIdMoviChiuNull) {
        writeString(Pos.WVAS_ID_MOVI_CHIU_NULL, wvasIdMoviChiuNull, Len.WVAS_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WVAS-ID-MOVI-CHIU-NULL<br>*/
    public String getWvasIdMoviChiuNull() {
        return readString(Pos.WVAS_ID_MOVI_CHIU_NULL, Len.WVAS_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_ID_MOVI_CHIU = 1;
        public static final int WVAS_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_ID_MOVI_CHIU = 5;
        public static final int WVAS_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
