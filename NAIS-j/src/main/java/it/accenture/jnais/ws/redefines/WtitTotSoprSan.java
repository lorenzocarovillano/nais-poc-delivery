package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-SOPR-SAN<br>
 * Variable: WTIT-TOT-SOPR-SAN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_SOPR_SAN;
    }

    public void setWtitTotSoprSan(AfDecimal wtitTotSoprSan) {
        writeDecimalAsPacked(Pos.WTIT_TOT_SOPR_SAN, wtitTotSoprSan.copy());
    }

    public void setWtitTotSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_SOPR_SAN, Pos.WTIT_TOT_SOPR_SAN);
    }

    /**Original name: WTIT-TOT-SOPR-SAN<br>*/
    public AfDecimal getWtitTotSoprSan() {
        return readPackedAsDecimal(Pos.WTIT_TOT_SOPR_SAN, Len.Int.WTIT_TOT_SOPR_SAN, Len.Fract.WTIT_TOT_SOPR_SAN);
    }

    public byte[] getWtitTotSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_SOPR_SAN, Pos.WTIT_TOT_SOPR_SAN);
        return buffer;
    }

    public void initWtitTotSoprSanSpaces() {
        fill(Pos.WTIT_TOT_SOPR_SAN, Len.WTIT_TOT_SOPR_SAN, Types.SPACE_CHAR);
    }

    public void setWtitTotSoprSanNull(String wtitTotSoprSanNull) {
        writeString(Pos.WTIT_TOT_SOPR_SAN_NULL, wtitTotSoprSanNull, Len.WTIT_TOT_SOPR_SAN_NULL);
    }

    /**Original name: WTIT-TOT-SOPR-SAN-NULL<br>*/
    public String getWtitTotSoprSanNull() {
        return readString(Pos.WTIT_TOT_SOPR_SAN_NULL, Len.WTIT_TOT_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_SAN = 1;
        public static final int WTIT_TOT_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_SOPR_SAN = 8;
        public static final int WTIT_TOT_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
