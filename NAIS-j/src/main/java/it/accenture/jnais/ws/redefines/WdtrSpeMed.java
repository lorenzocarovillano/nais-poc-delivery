package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-SPE-MED<br>
 * Variable: WDTR-SPE-MED from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_SPE_MED;
    }

    public void setWdtrSpeMed(AfDecimal wdtrSpeMed) {
        writeDecimalAsPacked(Pos.WDTR_SPE_MED, wdtrSpeMed.copy());
    }

    /**Original name: WDTR-SPE-MED<br>*/
    public AfDecimal getWdtrSpeMed() {
        return readPackedAsDecimal(Pos.WDTR_SPE_MED, Len.Int.WDTR_SPE_MED, Len.Fract.WDTR_SPE_MED);
    }

    public void setWdtrSpeMedNull(String wdtrSpeMedNull) {
        writeString(Pos.WDTR_SPE_MED_NULL, wdtrSpeMedNull, Len.WDTR_SPE_MED_NULL);
    }

    /**Original name: WDTR-SPE-MED-NULL<br>*/
    public String getWdtrSpeMedNull() {
        return readString(Pos.WDTR_SPE_MED_NULL, Len.WDTR_SPE_MED_NULL);
    }

    public String getWdtrSpeMedNullFormatted() {
        return Functions.padBlanks(getWdtrSpeMedNull(), Len.WDTR_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_SPE_MED = 1;
        public static final int WDTR_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_SPE_MED = 8;
        public static final int WDTR_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
