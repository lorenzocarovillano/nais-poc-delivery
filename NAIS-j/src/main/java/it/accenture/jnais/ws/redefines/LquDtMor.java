package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-MOR<br>
 * Variable: LQU-DT-MOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_MOR;
    }

    public void setLquDtMor(int lquDtMor) {
        writeIntAsPacked(Pos.LQU_DT_MOR, lquDtMor, Len.Int.LQU_DT_MOR);
    }

    public void setLquDtMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_MOR, Pos.LQU_DT_MOR);
    }

    /**Original name: LQU-DT-MOR<br>*/
    public int getLquDtMor() {
        return readPackedAsInt(Pos.LQU_DT_MOR, Len.Int.LQU_DT_MOR);
    }

    public byte[] getLquDtMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_MOR, Pos.LQU_DT_MOR);
        return buffer;
    }

    public void setLquDtMorNull(String lquDtMorNull) {
        writeString(Pos.LQU_DT_MOR_NULL, lquDtMorNull, Len.LQU_DT_MOR_NULL);
    }

    /**Original name: LQU-DT-MOR-NULL<br>*/
    public String getLquDtMorNull() {
        return readString(Pos.LQU_DT_MOR_NULL, Len.LQU_DT_MOR_NULL);
    }

    public String getLquDtMorNullFormatted() {
        return Functions.padBlanks(getLquDtMorNull(), Len.LQU_DT_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_MOR = 1;
        public static final int LQU_DT_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_MOR = 5;
        public static final int LQU_DT_MOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_MOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
