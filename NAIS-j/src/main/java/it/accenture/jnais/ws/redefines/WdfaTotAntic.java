package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-TOT-ANTIC<br>
 * Variable: WDFA-TOT-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaTotAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaTotAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_TOT_ANTIC;
    }

    public void setWdfaTotAntic(AfDecimal wdfaTotAntic) {
        writeDecimalAsPacked(Pos.WDFA_TOT_ANTIC, wdfaTotAntic.copy());
    }

    public void setWdfaTotAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_TOT_ANTIC, Pos.WDFA_TOT_ANTIC);
    }

    /**Original name: WDFA-TOT-ANTIC<br>*/
    public AfDecimal getWdfaTotAntic() {
        return readPackedAsDecimal(Pos.WDFA_TOT_ANTIC, Len.Int.WDFA_TOT_ANTIC, Len.Fract.WDFA_TOT_ANTIC);
    }

    public byte[] getWdfaTotAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_TOT_ANTIC, Pos.WDFA_TOT_ANTIC);
        return buffer;
    }

    public void setWdfaTotAnticNull(String wdfaTotAnticNull) {
        writeString(Pos.WDFA_TOT_ANTIC_NULL, wdfaTotAnticNull, Len.WDFA_TOT_ANTIC_NULL);
    }

    /**Original name: WDFA-TOT-ANTIC-NULL<br>*/
    public String getWdfaTotAnticNull() {
        return readString(Pos.WDFA_TOT_ANTIC_NULL, Len.WDFA_TOT_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_TOT_ANTIC = 1;
        public static final int WDFA_TOT_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_TOT_ANTIC = 8;
        public static final int WDFA_TOT_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_TOT_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_TOT_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
