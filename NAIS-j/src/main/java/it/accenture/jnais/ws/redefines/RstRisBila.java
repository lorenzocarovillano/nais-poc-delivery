package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-BILA<br>
 * Variable: RST-RIS-BILA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisBila extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisBila() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_BILA;
    }

    public void setRstRisBila(AfDecimal rstRisBila) {
        writeDecimalAsPacked(Pos.RST_RIS_BILA, rstRisBila.copy());
    }

    public void setRstRisBilaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_BILA, Pos.RST_RIS_BILA);
    }

    /**Original name: RST-RIS-BILA<br>*/
    public AfDecimal getRstRisBila() {
        return readPackedAsDecimal(Pos.RST_RIS_BILA, Len.Int.RST_RIS_BILA, Len.Fract.RST_RIS_BILA);
    }

    public byte[] getRstRisBilaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_BILA, Pos.RST_RIS_BILA);
        return buffer;
    }

    public void setRstRisBilaNull(String rstRisBilaNull) {
        writeString(Pos.RST_RIS_BILA_NULL, rstRisBilaNull, Len.RST_RIS_BILA_NULL);
    }

    /**Original name: RST-RIS-BILA-NULL<br>*/
    public String getRstRisBilaNull() {
        return readString(Pos.RST_RIS_BILA_NULL, Len.RST_RIS_BILA_NULL);
    }

    public String getRstRisBilaNullFormatted() {
        return Functions.padBlanks(getRstRisBilaNull(), Len.RST_RIS_BILA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_BILA = 1;
        public static final int RST_RIS_BILA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_BILA = 8;
        public static final int RST_RIS_BILA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_BILA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_BILA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
