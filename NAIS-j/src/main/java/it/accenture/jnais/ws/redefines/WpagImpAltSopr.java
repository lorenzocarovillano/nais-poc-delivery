package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-ALT-SOPR<br>
 * Variable: WPAG-IMP-ALT-SOPR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAltSopr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAltSopr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ALT_SOPR;
    }

    public void setWpagImpAltSopr(AfDecimal wpagImpAltSopr) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ALT_SOPR, wpagImpAltSopr.copy());
    }

    public void setWpagImpAltSoprFormatted(String wpagImpAltSopr) {
        setWpagImpAltSopr(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_ALT_SOPR + Len.Fract.WPAG_IMP_ALT_SOPR, Len.Fract.WPAG_IMP_ALT_SOPR, wpagImpAltSopr));
    }

    public void setWpagImpAltSoprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ALT_SOPR, Pos.WPAG_IMP_ALT_SOPR);
    }

    /**Original name: WPAG-IMP-ALT-SOPR<br>*/
    public AfDecimal getWpagImpAltSopr() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ALT_SOPR, Len.Int.WPAG_IMP_ALT_SOPR, Len.Fract.WPAG_IMP_ALT_SOPR);
    }

    public byte[] getWpagImpAltSoprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ALT_SOPR, Pos.WPAG_IMP_ALT_SOPR);
        return buffer;
    }

    public void initWpagImpAltSoprSpaces() {
        fill(Pos.WPAG_IMP_ALT_SOPR, Len.WPAG_IMP_ALT_SOPR, Types.SPACE_CHAR);
    }

    public void setWpagImpAltSoprNull(String wpagImpAltSoprNull) {
        writeString(Pos.WPAG_IMP_ALT_SOPR_NULL, wpagImpAltSoprNull, Len.WPAG_IMP_ALT_SOPR_NULL);
    }

    /**Original name: WPAG-IMP-ALT-SOPR-NULL<br>*/
    public String getWpagImpAltSoprNull() {
        return readString(Pos.WPAG_IMP_ALT_SOPR_NULL, Len.WPAG_IMP_ALT_SOPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ALT_SOPR = 1;
        public static final int WPAG_IMP_ALT_SOPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ALT_SOPR = 8;
        public static final int WPAG_IMP_ALT_SOPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ALT_SOPR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ALT_SOPR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
