package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DATA-INFERIORE<br>
 * Variable: DATA-INFERIORE from program LCCS0490<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DataInferioreLccs0490 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: AAAA-INF
    private String aaaaInf = DefaultValues.stringVal(Len.AAAA_INF);
    //Original name: MM-INF
    private String mmInf = DefaultValues.stringVal(Len.MM_INF);
    //Original name: GG-INF
    private String ggInf = DefaultValues.stringVal(Len.GG_INF);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DATA_INFERIORE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setDataInferioreBytes(buf);
    }

    public void setDataInferioreFormatted(String data) {
        byte[] buffer = new byte[Len.DATA_INFERIORE];
        MarshalByte.writeString(buffer, 1, data, Len.DATA_INFERIORE);
        setDataInferioreBytes(buffer, 1);
    }

    public String getDataInferioreFormatted() {
        return MarshalByteExt.bufferToStr(getDataInferioreBytes());
    }

    public void setDataInferioreBytes(byte[] buffer) {
        setDataInferioreBytes(buffer, 1);
    }

    public byte[] getDataInferioreBytes() {
        byte[] buffer = new byte[Len.DATA_INFERIORE];
        return getDataInferioreBytes(buffer, 1);
    }

    public void setDataInferioreBytes(byte[] buffer, int offset) {
        int position = offset;
        aaaaInf = MarshalByte.readFixedString(buffer, position, Len.AAAA_INF);
        position += Len.AAAA_INF;
        mmInf = MarshalByte.readFixedString(buffer, position, Len.MM_INF);
        position += Len.MM_INF;
        ggInf = MarshalByte.readFixedString(buffer, position, Len.GG_INF);
    }

    public byte[] getDataInferioreBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aaaaInf, Len.AAAA_INF);
        position += Len.AAAA_INF;
        MarshalByte.writeString(buffer, position, mmInf, Len.MM_INF);
        position += Len.MM_INF;
        MarshalByte.writeString(buffer, position, ggInf, Len.GG_INF);
        return buffer;
    }

    public void initDataInferioreZeroes() {
        aaaaInf = "0000";
        mmInf = "00";
        ggInf = "00";
    }

    @Override
    public byte[] serialize() {
        return getDataInferioreBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_INF = 4;
        public static final int MM_INF = 2;
        public static final int GG_INF = 2;
        public static final int DATA_INFERIORE = AAAA_INF + MM_INF + GG_INF;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
