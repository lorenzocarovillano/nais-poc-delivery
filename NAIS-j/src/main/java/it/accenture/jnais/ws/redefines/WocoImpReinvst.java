package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-IMP-REINVST<br>
 * Variable: WOCO-IMP-REINVST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoImpReinvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoImpReinvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_IMP_REINVST;
    }

    public void setWocoImpReinvst(AfDecimal wocoImpReinvst) {
        writeDecimalAsPacked(Pos.WOCO_IMP_REINVST, wocoImpReinvst.copy());
    }

    public void setWocoImpReinvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_IMP_REINVST, Pos.WOCO_IMP_REINVST);
    }

    /**Original name: WOCO-IMP-REINVST<br>*/
    public AfDecimal getWocoImpReinvst() {
        return readPackedAsDecimal(Pos.WOCO_IMP_REINVST, Len.Int.WOCO_IMP_REINVST, Len.Fract.WOCO_IMP_REINVST);
    }

    public byte[] getWocoImpReinvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_IMP_REINVST, Pos.WOCO_IMP_REINVST);
        return buffer;
    }

    public void initWocoImpReinvstSpaces() {
        fill(Pos.WOCO_IMP_REINVST, Len.WOCO_IMP_REINVST, Types.SPACE_CHAR);
    }

    public void setWocoImpReinvstNull(String wocoImpReinvstNull) {
        writeString(Pos.WOCO_IMP_REINVST_NULL, wocoImpReinvstNull, Len.WOCO_IMP_REINVST_NULL);
    }

    /**Original name: WOCO-IMP-REINVST-NULL<br>*/
    public String getWocoImpReinvstNull() {
        return readString(Pos.WOCO_IMP_REINVST_NULL, Len.WOCO_IMP_REINVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_REINVST = 1;
        public static final int WOCO_IMP_REINVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_REINVST = 8;
        public static final int WOCO_IMP_REINVST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_REINVST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_REINVST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
