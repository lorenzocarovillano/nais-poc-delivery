package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idbvb043;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndQuotzFndUnit;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSB040<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsb040Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BILA-VAR-CALC-P
    private IndQuotzFndUnit indBilaVarCalcP = new IndQuotzFndUnit();
    //Original name: IDBVB043
    private Idbvb043 idbvb043 = new Idbvb043();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idbvb043 getIdbvb043() {
        return idbvb043;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndQuotzFndUnit getIndBilaVarCalcP() {
        return indBilaVarCalcP;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
