package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-STAT-BUS<br>
 * Variable: WS-TP-STAT-BUS from copybook LCCVXSB0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpStatBus {

    //==== PROPERTIES ====
    private String value = "";
    public static final String IN_TRATTATIVA = "TR";
    public static final String PAGATA = "PA";
    public static final String IN_VIGORE = "VI";
    public static final String LIQUIDARE = "DL";
    public static final String STORNATO = "ST";
    public static final String INIZIALE = "IN";
    public static final String ATTESA_ATTIVAZIONE = "AA";
    public static final String COMPLETATA = "LI";
    public static final String PAGAMENTO_BLOCCATO = "PB";
    public static final String PARZIALMENTE_PAGATA = "PP";
    public static final String CONTABILIZZATO = "CB";
    public static final String TP_STAT_DA_AUTORIZZARE = "DA";
    public static final String TP_STAT_SOSPESO = "SO";

    //==== METHODS ====
    public void setWsTpStatBus(String wsTpStatBus) {
        this.value = Functions.subString(wsTpStatBus, Len.WS_TP_STAT_BUS);
    }

    public String getWsTpStatBus() {
        return this.value;
    }

    public boolean isInVigore() {
        return value.equals(IN_VIGORE);
    }

    public void setInVigore() {
        value = IN_VIGORE;
    }

    public boolean isStornato() {
        return value.equals(STORNATO);
    }

    public void setStornato() {
        value = STORNATO;
    }

    public boolean isCompletata() {
        return value.equals(COMPLETATA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_STAT_BUS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
