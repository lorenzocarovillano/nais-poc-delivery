package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-MONT-END2000-EFFLQ<br>
 * Variable: WDFL-MONT-END2000-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMontEnd2000Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMontEnd2000Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MONT_END2000_EFFLQ;
    }

    public void setWdflMontEnd2000Efflq(AfDecimal wdflMontEnd2000Efflq) {
        writeDecimalAsPacked(Pos.WDFL_MONT_END2000_EFFLQ, wdflMontEnd2000Efflq.copy());
    }

    public void setWdflMontEnd2000EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MONT_END2000_EFFLQ, Pos.WDFL_MONT_END2000_EFFLQ);
    }

    /**Original name: WDFL-MONT-END2000-EFFLQ<br>*/
    public AfDecimal getWdflMontEnd2000Efflq() {
        return readPackedAsDecimal(Pos.WDFL_MONT_END2000_EFFLQ, Len.Int.WDFL_MONT_END2000_EFFLQ, Len.Fract.WDFL_MONT_END2000_EFFLQ);
    }

    public byte[] getWdflMontEnd2000EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MONT_END2000_EFFLQ, Pos.WDFL_MONT_END2000_EFFLQ);
        return buffer;
    }

    public void setWdflMontEnd2000EfflqNull(String wdflMontEnd2000EfflqNull) {
        writeString(Pos.WDFL_MONT_END2000_EFFLQ_NULL, wdflMontEnd2000EfflqNull, Len.WDFL_MONT_END2000_EFFLQ_NULL);
    }

    /**Original name: WDFL-MONT-END2000-EFFLQ-NULL<br>*/
    public String getWdflMontEnd2000EfflqNull() {
        return readString(Pos.WDFL_MONT_END2000_EFFLQ_NULL, Len.WDFL_MONT_END2000_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2000_EFFLQ = 1;
        public static final int WDFL_MONT_END2000_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MONT_END2000_EFFLQ = 8;
        public static final int WDFL_MONT_END2000_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2000_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MONT_END2000_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
