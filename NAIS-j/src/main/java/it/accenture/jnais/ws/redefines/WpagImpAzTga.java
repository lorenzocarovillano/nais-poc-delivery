package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-AZ-TGA<br>
 * Variable: WPAG-IMP-AZ-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAzTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAzTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_AZ_TGA;
    }

    public void setWpagImpAzTga(AfDecimal wpagImpAzTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_AZ_TGA, wpagImpAzTga.copy());
    }

    public void setWpagImpAzTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_AZ_TGA, Pos.WPAG_IMP_AZ_TGA);
    }

    /**Original name: WPAG-IMP-AZ-TGA<br>*/
    public AfDecimal getWpagImpAzTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_AZ_TGA, Len.Int.WPAG_IMP_AZ_TGA, Len.Fract.WPAG_IMP_AZ_TGA);
    }

    public byte[] getWpagImpAzTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_AZ_TGA, Pos.WPAG_IMP_AZ_TGA);
        return buffer;
    }

    public void initWpagImpAzTgaSpaces() {
        fill(Pos.WPAG_IMP_AZ_TGA, Len.WPAG_IMP_AZ_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpAzTgaNull(String wpagImpAzTgaNull) {
        writeString(Pos.WPAG_IMP_AZ_TGA_NULL, wpagImpAzTgaNull, Len.WPAG_IMP_AZ_TGA_NULL);
    }

    /**Original name: WPAG-IMP-AZ-TGA-NULL<br>*/
    public String getWpagImpAzTgaNull() {
        return readString(Pos.WPAG_IMP_AZ_TGA_NULL, Len.WPAG_IMP_AZ_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_AZ_TGA = 1;
        public static final int WPAG_IMP_AZ_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_AZ_TGA = 8;
        public static final int WPAG_IMP_AZ_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_AZ_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_AZ_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
