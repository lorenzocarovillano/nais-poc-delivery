package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WMFZ-DT-RICH-MOVI<br>
 * Variable: WMFZ-DT-RICH-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzDtRichMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzDtRichMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_DT_RICH_MOVI;
    }

    public void setWmfzDtRichMovi(int wmfzDtRichMovi) {
        writeIntAsPacked(Pos.WMFZ_DT_RICH_MOVI, wmfzDtRichMovi, Len.Int.WMFZ_DT_RICH_MOVI);
    }

    public void setWmfzDtRichMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_DT_RICH_MOVI, Pos.WMFZ_DT_RICH_MOVI);
    }

    /**Original name: WMFZ-DT-RICH-MOVI<br>*/
    public int getWmfzDtRichMovi() {
        return readPackedAsInt(Pos.WMFZ_DT_RICH_MOVI, Len.Int.WMFZ_DT_RICH_MOVI);
    }

    public byte[] getWmfzDtRichMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_DT_RICH_MOVI, Pos.WMFZ_DT_RICH_MOVI);
        return buffer;
    }

    public void initWmfzDtRichMoviSpaces() {
        fill(Pos.WMFZ_DT_RICH_MOVI, Len.WMFZ_DT_RICH_MOVI, Types.SPACE_CHAR);
    }

    public void setWmfzDtRichMoviNull(String wmfzDtRichMoviNull) {
        writeString(Pos.WMFZ_DT_RICH_MOVI_NULL, wmfzDtRichMoviNull, Len.WMFZ_DT_RICH_MOVI_NULL);
    }

    /**Original name: WMFZ-DT-RICH-MOVI-NULL<br>*/
    public String getWmfzDtRichMoviNull() {
        return readString(Pos.WMFZ_DT_RICH_MOVI_NULL, Len.WMFZ_DT_RICH_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_DT_RICH_MOVI = 1;
        public static final int WMFZ_DT_RICH_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_DT_RICH_MOVI = 5;
        public static final int WMFZ_DT_RICH_MOVI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_DT_RICH_MOVI = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
