package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-PROG-TIT<br>
 * Variable: TDR-PROG-TIT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrProgTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrProgTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_PROG_TIT;
    }

    public void setTdrProgTit(int tdrProgTit) {
        writeIntAsPacked(Pos.TDR_PROG_TIT, tdrProgTit, Len.Int.TDR_PROG_TIT);
    }

    public void setTdrProgTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_PROG_TIT, Pos.TDR_PROG_TIT);
    }

    /**Original name: TDR-PROG-TIT<br>*/
    public int getTdrProgTit() {
        return readPackedAsInt(Pos.TDR_PROG_TIT, Len.Int.TDR_PROG_TIT);
    }

    public byte[] getTdrProgTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_PROG_TIT, Pos.TDR_PROG_TIT);
        return buffer;
    }

    public void setTdrProgTitNull(String tdrProgTitNull) {
        writeString(Pos.TDR_PROG_TIT_NULL, tdrProgTitNull, Len.TDR_PROG_TIT_NULL);
    }

    /**Original name: TDR-PROG-TIT-NULL<br>*/
    public String getTdrProgTitNull() {
        return readString(Pos.TDR_PROG_TIT_NULL, Len.TDR_PROG_TIT_NULL);
    }

    public String getTdrProgTitNullFormatted() {
        return Functions.padBlanks(getTdrProgTitNull(), Len.TDR_PROG_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_PROG_TIT = 1;
        public static final int TDR_PROG_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_PROG_TIT = 3;
        public static final int TDR_PROG_TIT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_PROG_TIT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
