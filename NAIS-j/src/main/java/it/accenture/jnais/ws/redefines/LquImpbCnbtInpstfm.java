package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPB-CNBT-INPSTFM<br>
 * Variable: LQU-IMPB-CNBT-INPSTFM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpbCnbtInpstfm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpbCnbtInpstfm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPB_CNBT_INPSTFM;
    }

    public void setLquImpbCnbtInpstfm(AfDecimal lquImpbCnbtInpstfm) {
        writeDecimalAsPacked(Pos.LQU_IMPB_CNBT_INPSTFM, lquImpbCnbtInpstfm.copy());
    }

    public void setLquImpbCnbtInpstfmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPB_CNBT_INPSTFM, Pos.LQU_IMPB_CNBT_INPSTFM);
    }

    /**Original name: LQU-IMPB-CNBT-INPSTFM<br>*/
    public AfDecimal getLquImpbCnbtInpstfm() {
        return readPackedAsDecimal(Pos.LQU_IMPB_CNBT_INPSTFM, Len.Int.LQU_IMPB_CNBT_INPSTFM, Len.Fract.LQU_IMPB_CNBT_INPSTFM);
    }

    public byte[] getLquImpbCnbtInpstfmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPB_CNBT_INPSTFM, Pos.LQU_IMPB_CNBT_INPSTFM);
        return buffer;
    }

    public void setLquImpbCnbtInpstfmNull(String lquImpbCnbtInpstfmNull) {
        writeString(Pos.LQU_IMPB_CNBT_INPSTFM_NULL, lquImpbCnbtInpstfmNull, Len.LQU_IMPB_CNBT_INPSTFM_NULL);
    }

    /**Original name: LQU-IMPB-CNBT-INPSTFM-NULL<br>*/
    public String getLquImpbCnbtInpstfmNull() {
        return readString(Pos.LQU_IMPB_CNBT_INPSTFM_NULL, Len.LQU_IMPB_CNBT_INPSTFM_NULL);
    }

    public String getLquImpbCnbtInpstfmNullFormatted() {
        return Functions.padBlanks(getLquImpbCnbtInpstfmNull(), Len.LQU_IMPB_CNBT_INPSTFM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_CNBT_INPSTFM = 1;
        public static final int LQU_IMPB_CNBT_INPSTFM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPB_CNBT_INPSTFM = 8;
        public static final int LQU_IMPB_CNBT_INPSTFM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_CNBT_INPSTFM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPB_CNBT_INPSTFM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
