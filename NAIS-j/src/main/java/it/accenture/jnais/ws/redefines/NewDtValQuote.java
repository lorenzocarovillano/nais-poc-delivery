package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: NEW-DT-VAL-QUOTE<br>
 * Variable: NEW-DT-VAL-QUOTE from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class NewDtValQuote extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public NewDtValQuote() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.NEW_DT_VAL_QUOTE;
    }

    public void setNewDtValQuote(String newDtValQuote) {
        writeString(Pos.NEW_DT_VAL_QUOTE, newDtValQuote, Len.NEW_DT_VAL_QUOTE);
    }

    /**Original name: NEW-DT-VAL-QUOTE<br>*/
    public String getNewDtValQuote() {
        return readString(Pos.NEW_DT_VAL_QUOTE, Len.NEW_DT_VAL_QUOTE);
    }

    /**Original name: NEW-DT-VAL-QUOTE-RED<br>*/
    public int getNewDtValQuoteRed() {
        return readNumDispUnsignedInt(Pos.NEW_DT_VAL_QUOTE_RED, Len.NEW_DT_VAL_QUOTE_RED);
    }

    public String getNewDtValQuoteRedFormatted() {
        return readFixedString(Pos.NEW_DT_VAL_QUOTE_RED, Len.NEW_DT_VAL_QUOTE_RED);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int NEW_DT_VAL_QUOTE = 1;
        public static final int NEW_DT_VAL_QUOTE_RED = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int NEW_DT_VAL_QUOTE = 8;
        public static final int NEW_DT_VAL_QUOTE_RED = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
