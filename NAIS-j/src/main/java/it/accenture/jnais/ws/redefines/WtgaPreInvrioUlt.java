package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-INVRIO-ULT<br>
 * Variable: WTGA-PRE-INVRIO-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreInvrioUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreInvrioUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_INVRIO_ULT;
    }

    public void setWtgaPreInvrioUlt(AfDecimal wtgaPreInvrioUlt) {
        writeDecimalAsPacked(Pos.WTGA_PRE_INVRIO_ULT, wtgaPreInvrioUlt.copy());
    }

    public void setWtgaPreInvrioUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_INVRIO_ULT, Pos.WTGA_PRE_INVRIO_ULT);
    }

    /**Original name: WTGA-PRE-INVRIO-ULT<br>*/
    public AfDecimal getWtgaPreInvrioUlt() {
        return readPackedAsDecimal(Pos.WTGA_PRE_INVRIO_ULT, Len.Int.WTGA_PRE_INVRIO_ULT, Len.Fract.WTGA_PRE_INVRIO_ULT);
    }

    public byte[] getWtgaPreInvrioUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_INVRIO_ULT, Pos.WTGA_PRE_INVRIO_ULT);
        return buffer;
    }

    public void initWtgaPreInvrioUltSpaces() {
        fill(Pos.WTGA_PRE_INVRIO_ULT, Len.WTGA_PRE_INVRIO_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaPreInvrioUltNull(String wtgaPreInvrioUltNull) {
        writeString(Pos.WTGA_PRE_INVRIO_ULT_NULL, wtgaPreInvrioUltNull, Len.WTGA_PRE_INVRIO_ULT_NULL);
    }

    /**Original name: WTGA-PRE-INVRIO-ULT-NULL<br>*/
    public String getWtgaPreInvrioUltNull() {
        return readString(Pos.WTGA_PRE_INVRIO_ULT_NULL, Len.WTGA_PRE_INVRIO_ULT_NULL);
    }

    public String getWtgaPreInvrioUltNullFormatted() {
        return Functions.padBlanks(getWtgaPreInvrioUltNull(), Len.WTGA_PRE_INVRIO_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_INVRIO_ULT = 1;
        public static final int WTGA_PRE_INVRIO_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_INVRIO_ULT = 8;
        public static final int WTGA_PRE_INVRIO_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_INVRIO_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_INVRIO_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
