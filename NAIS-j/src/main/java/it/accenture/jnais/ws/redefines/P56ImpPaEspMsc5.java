package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-PA-ESP-MSC-5<br>
 * Variable: P56-IMP-PA-ESP-MSC-5 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpPaEspMsc5 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpPaEspMsc5() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_PA_ESP_MSC5;
    }

    public void setP56ImpPaEspMsc5(AfDecimal p56ImpPaEspMsc5) {
        writeDecimalAsPacked(Pos.P56_IMP_PA_ESP_MSC5, p56ImpPaEspMsc5.copy());
    }

    public void setP56ImpPaEspMsc5FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC5, Pos.P56_IMP_PA_ESP_MSC5);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-5<br>*/
    public AfDecimal getP56ImpPaEspMsc5() {
        return readPackedAsDecimal(Pos.P56_IMP_PA_ESP_MSC5, Len.Int.P56_IMP_PA_ESP_MSC5, Len.Fract.P56_IMP_PA_ESP_MSC5);
    }

    public byte[] getP56ImpPaEspMsc5AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC5, Pos.P56_IMP_PA_ESP_MSC5);
        return buffer;
    }

    public void setP56ImpPaEspMsc5Null(String p56ImpPaEspMsc5Null) {
        writeString(Pos.P56_IMP_PA_ESP_MSC5_NULL, p56ImpPaEspMsc5Null, Len.P56_IMP_PA_ESP_MSC5_NULL);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-5-NULL<br>*/
    public String getP56ImpPaEspMsc5Null() {
        return readString(Pos.P56_IMP_PA_ESP_MSC5_NULL, Len.P56_IMP_PA_ESP_MSC5_NULL);
    }

    public String getP56ImpPaEspMsc5NullFormatted() {
        return Functions.padBlanks(getP56ImpPaEspMsc5Null(), Len.P56_IMP_PA_ESP_MSC5_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC5 = 1;
        public static final int P56_IMP_PA_ESP_MSC5_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC5 = 8;
        public static final int P56_IMP_PA_ESP_MSC5_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC5 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC5 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
