package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ODE-COD-LIV-AUT-APPRT<br>
 * Variable: ODE-COD-LIV-AUT-APPRT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OdeCodLivAutApprt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OdeCodLivAutApprt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ODE_COD_LIV_AUT_APPRT;
    }

    public void setOdeCodLivAutApprt(int odeCodLivAutApprt) {
        writeIntAsPacked(Pos.ODE_COD_LIV_AUT_APPRT, odeCodLivAutApprt, Len.Int.ODE_COD_LIV_AUT_APPRT);
    }

    public void setOdeCodLivAutApprtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ODE_COD_LIV_AUT_APPRT, Pos.ODE_COD_LIV_AUT_APPRT);
    }

    /**Original name: ODE-COD-LIV-AUT-APPRT<br>*/
    public int getOdeCodLivAutApprt() {
        return readPackedAsInt(Pos.ODE_COD_LIV_AUT_APPRT, Len.Int.ODE_COD_LIV_AUT_APPRT);
    }

    public byte[] getOdeCodLivAutApprtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ODE_COD_LIV_AUT_APPRT, Pos.ODE_COD_LIV_AUT_APPRT);
        return buffer;
    }

    public void setOdeCodLivAutApprtNull(String odeCodLivAutApprtNull) {
        writeString(Pos.ODE_COD_LIV_AUT_APPRT_NULL, odeCodLivAutApprtNull, Len.ODE_COD_LIV_AUT_APPRT_NULL);
    }

    /**Original name: ODE-COD-LIV-AUT-APPRT-NULL<br>*/
    public String getOdeCodLivAutApprtNull() {
        return readString(Pos.ODE_COD_LIV_AUT_APPRT_NULL, Len.ODE_COD_LIV_AUT_APPRT_NULL);
    }

    public String getOdeCodLivAutApprtNullFormatted() {
        return Functions.padBlanks(getOdeCodLivAutApprtNull(), Len.ODE_COD_LIV_AUT_APPRT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ODE_COD_LIV_AUT_APPRT = 1;
        public static final int ODE_COD_LIV_AUT_APPRT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ODE_COD_LIV_AUT_APPRT = 3;
        public static final int ODE_COD_LIV_AUT_APPRT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ODE_COD_LIV_AUT_APPRT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
