package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-ID-TIT-CONT<br>
 * Variable: MFZ-ID-TIT-CONT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzIdTitCont extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzIdTitCont() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_ID_TIT_CONT;
    }

    public void setMfzIdTitCont(int mfzIdTitCont) {
        writeIntAsPacked(Pos.MFZ_ID_TIT_CONT, mfzIdTitCont, Len.Int.MFZ_ID_TIT_CONT);
    }

    public void setMfzIdTitContFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_ID_TIT_CONT, Pos.MFZ_ID_TIT_CONT);
    }

    /**Original name: MFZ-ID-TIT-CONT<br>*/
    public int getMfzIdTitCont() {
        return readPackedAsInt(Pos.MFZ_ID_TIT_CONT, Len.Int.MFZ_ID_TIT_CONT);
    }

    public byte[] getMfzIdTitContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_ID_TIT_CONT, Pos.MFZ_ID_TIT_CONT);
        return buffer;
    }

    public void setMfzIdTitContNull(String mfzIdTitContNull) {
        writeString(Pos.MFZ_ID_TIT_CONT_NULL, mfzIdTitContNull, Len.MFZ_ID_TIT_CONT_NULL);
    }

    /**Original name: MFZ-ID-TIT-CONT-NULL<br>*/
    public String getMfzIdTitContNull() {
        return readString(Pos.MFZ_ID_TIT_CONT_NULL, Len.MFZ_ID_TIT_CONT_NULL);
    }

    public String getMfzIdTitContNullFormatted() {
        return Functions.padBlanks(getMfzIdTitContNull(), Len.MFZ_ID_TIT_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_ID_TIT_CONT = 1;
        public static final int MFZ_ID_TIT_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_ID_TIT_CONT = 5;
        public static final int MFZ_ID_TIT_CONT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_ID_TIT_CONT = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
