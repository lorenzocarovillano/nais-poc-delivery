package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IAS-MGG-SIN<br>
 * Variable: LQU-TOT-IAS-MGG-SIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotIasMggSin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotIasMggSin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IAS_MGG_SIN;
    }

    public void setLquTotIasMggSin(AfDecimal lquTotIasMggSin) {
        writeDecimalAsPacked(Pos.LQU_TOT_IAS_MGG_SIN, lquTotIasMggSin.copy());
    }

    public void setLquTotIasMggSinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IAS_MGG_SIN, Pos.LQU_TOT_IAS_MGG_SIN);
    }

    /**Original name: LQU-TOT-IAS-MGG-SIN<br>*/
    public AfDecimal getLquTotIasMggSin() {
        return readPackedAsDecimal(Pos.LQU_TOT_IAS_MGG_SIN, Len.Int.LQU_TOT_IAS_MGG_SIN, Len.Fract.LQU_TOT_IAS_MGG_SIN);
    }

    public byte[] getLquTotIasMggSinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IAS_MGG_SIN, Pos.LQU_TOT_IAS_MGG_SIN);
        return buffer;
    }

    public void setLquTotIasMggSinNull(String lquTotIasMggSinNull) {
        writeString(Pos.LQU_TOT_IAS_MGG_SIN_NULL, lquTotIasMggSinNull, Len.LQU_TOT_IAS_MGG_SIN_NULL);
    }

    /**Original name: LQU-TOT-IAS-MGG-SIN-NULL<br>*/
    public String getLquTotIasMggSinNull() {
        return readString(Pos.LQU_TOT_IAS_MGG_SIN_NULL, Len.LQU_TOT_IAS_MGG_SIN_NULL);
    }

    public String getLquTotIasMggSinNullFormatted() {
        return Functions.padBlanks(getLquTotIasMggSinNull(), Len.LQU_TOT_IAS_MGG_SIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_MGG_SIN = 1;
        public static final int LQU_TOT_IAS_MGG_SIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_MGG_SIN = 8;
        public static final int LQU_TOT_IAS_MGG_SIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_MGG_SIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_MGG_SIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
