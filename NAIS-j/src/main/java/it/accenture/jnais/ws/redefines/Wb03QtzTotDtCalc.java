package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-QTZ-TOT-DT-CALC<br>
 * Variable: WB03-QTZ-TOT-DT-CALC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03QtzTotDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03QtzTotDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_QTZ_TOT_DT_CALC;
    }

    public void setWb03QtzTotDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_QTZ_TOT_DT_CALC, Pos.WB03_QTZ_TOT_DT_CALC);
    }

    /**Original name: WB03-QTZ-TOT-DT-CALC<br>*/
    public AfDecimal getWb03QtzTotDtCalc() {
        return readPackedAsDecimal(Pos.WB03_QTZ_TOT_DT_CALC, Len.Int.WB03_QTZ_TOT_DT_CALC, Len.Fract.WB03_QTZ_TOT_DT_CALC);
    }

    public byte[] getWb03QtzTotDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_QTZ_TOT_DT_CALC, Pos.WB03_QTZ_TOT_DT_CALC);
        return buffer;
    }

    public void setWb03QtzTotDtCalcNull(String wb03QtzTotDtCalcNull) {
        writeString(Pos.WB03_QTZ_TOT_DT_CALC_NULL, wb03QtzTotDtCalcNull, Len.WB03_QTZ_TOT_DT_CALC_NULL);
    }

    /**Original name: WB03-QTZ-TOT-DT-CALC-NULL<br>*/
    public String getWb03QtzTotDtCalcNull() {
        return readString(Pos.WB03_QTZ_TOT_DT_CALC_NULL, Len.WB03_QTZ_TOT_DT_CALC_NULL);
    }

    public String getWb03QtzTotDtCalcNullFormatted() {
        return Functions.padBlanks(getWb03QtzTotDtCalcNull(), Len.WB03_QTZ_TOT_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_TOT_DT_CALC = 1;
        public static final int WB03_QTZ_TOT_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_TOT_DT_CALC = 7;
        public static final int WB03_QTZ_TOT_DT_CALC_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_TOT_DT_CALC = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_TOT_DT_CALC = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
