package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUAMG-X<br>
 * Variable: A2K-OUAMG-X from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class A2kOuamgX {

    //==== PROPERTIES ====
    //Original name: A2K-OUSS03
    private String ouss03 = DefaultValues.stringVal(Len.OUSS03);
    //Original name: A2K-OUAA03
    private String ouaa03 = DefaultValues.stringVal(Len.OUAA03);
    //Original name: A2K-OUMM03
    private String oumm03 = DefaultValues.stringVal(Len.OUMM03);
    //Original name: A2K-OUGG03
    private String ougg03 = DefaultValues.stringVal(Len.OUGG03);

    //==== METHODS ====
    public void setA2kOuamg(int a2kOuamg) {
        int position = 1;
        byte[] buffer = getA2kOuamgXBytes();
        MarshalByte.writeInt(buffer, position, a2kOuamg, Len.Int.A2K_OUAMG, SignType.NO_SIGN);
        setA2kOuamgXBytes(buffer);
    }

    public void setA2kOuamgFormatted(String a2kOuamg) {
        int position = 1;
        byte[] buffer = getA2kOuamgXBytes();
        MarshalByte.writeString(buffer, position, Trunc.toNumeric(a2kOuamg, Len.A2K_OUAMG), Len.A2K_OUAMG);
        setA2kOuamgXBytes(buffer);
    }

    /**Original name: A2K-OUAMG<br>
	 * <pre>                                       formato AAAAMMGG</pre>*/
    public int getA2kOuamg() {
        int position = 1;
        return MarshalByte.readInt(getA2kOuamgXBytes(), position, Len.Int.A2K_OUAMG, SignType.NO_SIGN);
    }

    public String getA2kOuamgFormatted() {
        int position = 1;
        return MarshalByte.readFixedString(getA2kOuamgXBytes(), position, Len.A2K_OUAMG);
    }

    public String getA2kOuamgXFormatted() {
        return MarshalByteExt.bufferToStr(getA2kOuamgXBytes());
    }

    public void setA2kOuamgXBytes(byte[] buffer) {
        setA2kOuamgXBytes(buffer, 1);
    }

    /**Original name: A2K-OUAMG-X<br>*/
    public byte[] getA2kOuamgXBytes() {
        byte[] buffer = new byte[Len.A2K_OUAMG_X];
        return getA2kOuamgXBytes(buffer, 1);
    }

    public void setA2kOuamgXBytes(byte[] buffer, int offset) {
        int position = offset;
        ouss03 = MarshalByte.readFixedString(buffer, position, Len.OUSS03);
        position += Len.OUSS03;
        ouaa03 = MarshalByte.readFixedString(buffer, position, Len.OUAA03);
        position += Len.OUAA03;
        oumm03 = MarshalByte.readFixedString(buffer, position, Len.OUMM03);
        position += Len.OUMM03;
        ougg03 = MarshalByte.readFixedString(buffer, position, Len.OUGG03);
    }

    public byte[] getA2kOuamgXBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ouss03, Len.OUSS03);
        position += Len.OUSS03;
        MarshalByte.writeString(buffer, position, ouaa03, Len.OUAA03);
        position += Len.OUAA03;
        MarshalByte.writeString(buffer, position, oumm03, Len.OUMM03);
        position += Len.OUMM03;
        MarshalByte.writeString(buffer, position, ougg03, Len.OUGG03);
        return buffer;
    }

    public void initA2kOuamgXSpaces() {
        ouss03 = "";
        ouaa03 = "";
        oumm03 = "";
        ougg03 = "";
    }

    public void setOuss03Formatted(String ouss03) {
        this.ouss03 = Trunc.toUnsignedNumeric(ouss03, Len.OUSS03);
    }

    public short getOuss03() {
        return NumericDisplay.asShort(this.ouss03);
    }

    public void setOuaa03Formatted(String ouaa03) {
        this.ouaa03 = Trunc.toUnsignedNumeric(ouaa03, Len.OUAA03);
    }

    public short getOuaa03() {
        return NumericDisplay.asShort(this.ouaa03);
    }

    public void setOumm03Formatted(String oumm03) {
        this.oumm03 = Trunc.toUnsignedNumeric(oumm03, Len.OUMM03);
    }

    public short getOumm03() {
        return NumericDisplay.asShort(this.oumm03);
    }

    public void setOugg03Formatted(String ougg03) {
        this.ougg03 = Trunc.toUnsignedNumeric(ougg03, Len.OUGG03);
    }

    public short getOugg03() {
        return NumericDisplay.asShort(this.ougg03);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUSS03 = 2;
        public static final int OUAA03 = 2;
        public static final int OUMM03 = 2;
        public static final int OUGG03 = 2;
        public static final int A2K_OUAMG_X = OUSS03 + OUAA03 + OUMM03 + OUGG03;
        public static final int A2K_OUAMG = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A2K_OUAMG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
