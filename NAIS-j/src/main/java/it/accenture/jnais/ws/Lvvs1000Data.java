package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvtga1Lvvs0037;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS1000<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs1000Data {

    //==== PROPERTIES ====
    /**Original name: WS-TP-TRCH-I<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private short wsTpTrchI = ((short)0);
    //Original name: DTGA-ELE-MAX-TGA
    private short dtgaEleMaxTga = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVTGA1
    private Lccvtga1Lvvs0037 lccvtga1 = new Lccvtga1Lvvs0037();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWsTpTrchI(short wsTpTrchI) {
        this.wsTpTrchI = wsTpTrchI;
    }

    public short getWsTpTrchI() {
        return this.wsTpTrchI;
    }

    public void setDtgaAreaTrchGarFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TRCH_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TRCH_GAR);
        setDtgaAreaTrchGarBytes(buffer, 1);
    }

    public void setDtgaAreaTrchGarBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleMaxTga = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDtgaTabTgaBytes(buffer, position);
    }

    public void setDtgaEleMaxTga(short dtgaEleMaxTga) {
        this.dtgaEleMaxTga = dtgaEleMaxTga;
    }

    public short getDtgaEleMaxTga() {
        return this.dtgaEleMaxTga;
    }

    public void setDtgaTabTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtga1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtga1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtga1Lvvs0037.Len.Int.ID_PTF, 0));
        position += Lccvtga1Lvvs0037.Len.ID_PTF;
        lccvtga1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvtga1Lvvs0037 getLccvtga1() {
        return lccvtga1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_TRCH_I = 2;
        public static final int DTGA_ELE_MAX_TGA = 2;
        public static final int DTGA_TAB_TGA = WpolStatus.Len.STATUS + Lccvtga1Lvvs0037.Len.ID_PTF + WtgaDati.Len.DATI;
        public static final int DTGA_AREA_TRCH_GAR = DTGA_ELE_MAX_TGA + DTGA_TAB_TGA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
