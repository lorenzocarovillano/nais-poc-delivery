package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMPB-TFR<br>
 * Variable: LQU-TOT-IMPB-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpbTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpbTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMPB_TFR;
    }

    public void setLquTotImpbTfr(AfDecimal lquTotImpbTfr) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMPB_TFR, lquTotImpbTfr.copy());
    }

    public void setLquTotImpbTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMPB_TFR, Pos.LQU_TOT_IMPB_TFR);
    }

    /**Original name: LQU-TOT-IMPB-TFR<br>*/
    public AfDecimal getLquTotImpbTfr() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMPB_TFR, Len.Int.LQU_TOT_IMPB_TFR, Len.Fract.LQU_TOT_IMPB_TFR);
    }

    public byte[] getLquTotImpbTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMPB_TFR, Pos.LQU_TOT_IMPB_TFR);
        return buffer;
    }

    public void setLquTotImpbTfrNull(String lquTotImpbTfrNull) {
        writeString(Pos.LQU_TOT_IMPB_TFR_NULL, lquTotImpbTfrNull, Len.LQU_TOT_IMPB_TFR_NULL);
    }

    /**Original name: LQU-TOT-IMPB-TFR-NULL<br>*/
    public String getLquTotImpbTfrNull() {
        return readString(Pos.LQU_TOT_IMPB_TFR_NULL, Len.LQU_TOT_IMPB_TFR_NULL);
    }

    public String getLquTotImpbTfrNullFormatted() {
        return Functions.padBlanks(getLquTotImpbTfrNull(), Len.LQU_TOT_IMPB_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMPB_TFR = 1;
        public static final int LQU_TOT_IMPB_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMPB_TFR = 8;
        public static final int LQU_TOT_IMPB_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMPB_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMPB_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
