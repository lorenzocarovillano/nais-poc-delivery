package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SPE-MED<br>
 * Variable: DTR-SPE-MED from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SPE_MED;
    }

    public void setDtrSpeMed(AfDecimal dtrSpeMed) {
        writeDecimalAsPacked(Pos.DTR_SPE_MED, dtrSpeMed.copy());
    }

    public void setDtrSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SPE_MED, Pos.DTR_SPE_MED);
    }

    /**Original name: DTR-SPE-MED<br>*/
    public AfDecimal getDtrSpeMed() {
        return readPackedAsDecimal(Pos.DTR_SPE_MED, Len.Int.DTR_SPE_MED, Len.Fract.DTR_SPE_MED);
    }

    public byte[] getDtrSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SPE_MED, Pos.DTR_SPE_MED);
        return buffer;
    }

    public void setDtrSpeMedNull(String dtrSpeMedNull) {
        writeString(Pos.DTR_SPE_MED_NULL, dtrSpeMedNull, Len.DTR_SPE_MED_NULL);
    }

    /**Original name: DTR-SPE-MED-NULL<br>*/
    public String getDtrSpeMedNull() {
        return readString(Pos.DTR_SPE_MED_NULL, Len.DTR_SPE_MED_NULL);
    }

    public String getDtrSpeMedNullFormatted() {
        return Functions.padBlanks(getDtrSpeMedNull(), Len.DTR_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SPE_MED = 1;
        public static final int DTR_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SPE_MED = 8;
        public static final int DTR_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
