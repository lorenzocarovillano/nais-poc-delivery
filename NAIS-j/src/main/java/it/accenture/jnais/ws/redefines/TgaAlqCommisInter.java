package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ALQ-COMMIS-INTER<br>
 * Variable: TGA-ALQ-COMMIS-INTER from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAlqCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAlqCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ALQ_COMMIS_INTER;
    }

    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        writeDecimalAsPacked(Pos.TGA_ALQ_COMMIS_INTER, tgaAlqCommisInter.copy());
    }

    public void setTgaAlqCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ALQ_COMMIS_INTER, Pos.TGA_ALQ_COMMIS_INTER);
    }

    /**Original name: TGA-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getTgaAlqCommisInter() {
        return readPackedAsDecimal(Pos.TGA_ALQ_COMMIS_INTER, Len.Int.TGA_ALQ_COMMIS_INTER, Len.Fract.TGA_ALQ_COMMIS_INTER);
    }

    public byte[] getTgaAlqCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ALQ_COMMIS_INTER, Pos.TGA_ALQ_COMMIS_INTER);
        return buffer;
    }

    public void setTgaAlqCommisInterNull(String tgaAlqCommisInterNull) {
        writeString(Pos.TGA_ALQ_COMMIS_INTER_NULL, tgaAlqCommisInterNull, Len.TGA_ALQ_COMMIS_INTER_NULL);
    }

    /**Original name: TGA-ALQ-COMMIS-INTER-NULL<br>*/
    public String getTgaAlqCommisInterNull() {
        return readString(Pos.TGA_ALQ_COMMIS_INTER_NULL, Len.TGA_ALQ_COMMIS_INTER_NULL);
    }

    public String getTgaAlqCommisInterNullFormatted() {
        return Functions.padBlanks(getTgaAlqCommisInterNull(), Len.TGA_ALQ_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_COMMIS_INTER = 1;
        public static final int TGA_ALQ_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_COMMIS_INTER = 4;
        public static final int TGA_ALQ_COMMIS_INTER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
