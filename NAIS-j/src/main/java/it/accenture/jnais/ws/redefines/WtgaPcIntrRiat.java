package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PC-INTR-RIAT<br>
 * Variable: WTGA-PC-INTR-RIAT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPcIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPcIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PC_INTR_RIAT;
    }

    public void setWtgaPcIntrRiat(AfDecimal wtgaPcIntrRiat) {
        writeDecimalAsPacked(Pos.WTGA_PC_INTR_RIAT, wtgaPcIntrRiat.copy());
    }

    public void setWtgaPcIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PC_INTR_RIAT, Pos.WTGA_PC_INTR_RIAT);
    }

    /**Original name: WTGA-PC-INTR-RIAT<br>*/
    public AfDecimal getWtgaPcIntrRiat() {
        return readPackedAsDecimal(Pos.WTGA_PC_INTR_RIAT, Len.Int.WTGA_PC_INTR_RIAT, Len.Fract.WTGA_PC_INTR_RIAT);
    }

    public byte[] getWtgaPcIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PC_INTR_RIAT, Pos.WTGA_PC_INTR_RIAT);
        return buffer;
    }

    public void initWtgaPcIntrRiatSpaces() {
        fill(Pos.WTGA_PC_INTR_RIAT, Len.WTGA_PC_INTR_RIAT, Types.SPACE_CHAR);
    }

    public void setWtgaPcIntrRiatNull(String wtgaPcIntrRiatNull) {
        writeString(Pos.WTGA_PC_INTR_RIAT_NULL, wtgaPcIntrRiatNull, Len.WTGA_PC_INTR_RIAT_NULL);
    }

    /**Original name: WTGA-PC-INTR-RIAT-NULL<br>*/
    public String getWtgaPcIntrRiatNull() {
        return readString(Pos.WTGA_PC_INTR_RIAT_NULL, Len.WTGA_PC_INTR_RIAT_NULL);
    }

    public String getWtgaPcIntrRiatNullFormatted() {
        return Functions.padBlanks(getWtgaPcIntrRiatNull(), Len.WTGA_PC_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PC_INTR_RIAT = 1;
        public static final int WTGA_PC_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PC_INTR_RIAT = 4;
        public static final int WTGA_PC_INTR_RIAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
