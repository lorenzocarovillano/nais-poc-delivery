package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-PC-TFR<br>
 * Variable: WDFA-PC-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaPcTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaPcTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_PC_TFR;
    }

    public void setWdfaPcTfr(AfDecimal wdfaPcTfr) {
        writeDecimalAsPacked(Pos.WDFA_PC_TFR, wdfaPcTfr.copy());
    }

    public void setWdfaPcTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_PC_TFR, Pos.WDFA_PC_TFR);
    }

    /**Original name: WDFA-PC-TFR<br>*/
    public AfDecimal getWdfaPcTfr() {
        return readPackedAsDecimal(Pos.WDFA_PC_TFR, Len.Int.WDFA_PC_TFR, Len.Fract.WDFA_PC_TFR);
    }

    public byte[] getWdfaPcTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_PC_TFR, Pos.WDFA_PC_TFR);
        return buffer;
    }

    public void setWdfaPcTfrNull(String wdfaPcTfrNull) {
        writeString(Pos.WDFA_PC_TFR_NULL, wdfaPcTfrNull, Len.WDFA_PC_TFR_NULL);
    }

    /**Original name: WDFA-PC-TFR-NULL<br>*/
    public String getWdfaPcTfrNull() {
        return readString(Pos.WDFA_PC_TFR_NULL, Len.WDFA_PC_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_PC_TFR = 1;
        public static final int WDFA_PC_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_PC_TFR = 4;
        public static final int WDFA_PC_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
