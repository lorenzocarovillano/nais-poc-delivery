package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-PC-ESP-AG-PA-MSC<br>
 * Variable: P56-PC-ESP-AG-PA-MSC from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56PcEspAgPaMsc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56PcEspAgPaMsc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_PC_ESP_AG_PA_MSC;
    }

    public void setP56PcEspAgPaMsc(AfDecimal p56PcEspAgPaMsc) {
        writeDecimalAsPacked(Pos.P56_PC_ESP_AG_PA_MSC, p56PcEspAgPaMsc.copy());
    }

    public void setP56PcEspAgPaMscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_PC_ESP_AG_PA_MSC, Pos.P56_PC_ESP_AG_PA_MSC);
    }

    /**Original name: P56-PC-ESP-AG-PA-MSC<br>*/
    public AfDecimal getP56PcEspAgPaMsc() {
        return readPackedAsDecimal(Pos.P56_PC_ESP_AG_PA_MSC, Len.Int.P56_PC_ESP_AG_PA_MSC, Len.Fract.P56_PC_ESP_AG_PA_MSC);
    }

    public byte[] getP56PcEspAgPaMscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_PC_ESP_AG_PA_MSC, Pos.P56_PC_ESP_AG_PA_MSC);
        return buffer;
    }

    public void setP56PcEspAgPaMscNull(String p56PcEspAgPaMscNull) {
        writeString(Pos.P56_PC_ESP_AG_PA_MSC_NULL, p56PcEspAgPaMscNull, Len.P56_PC_ESP_AG_PA_MSC_NULL);
    }

    /**Original name: P56-PC-ESP-AG-PA-MSC-NULL<br>*/
    public String getP56PcEspAgPaMscNull() {
        return readString(Pos.P56_PC_ESP_AG_PA_MSC_NULL, Len.P56_PC_ESP_AG_PA_MSC_NULL);
    }

    public String getP56PcEspAgPaMscNullFormatted() {
        return Functions.padBlanks(getP56PcEspAgPaMscNull(), Len.P56_PC_ESP_AG_PA_MSC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_PC_ESP_AG_PA_MSC = 1;
        public static final int P56_PC_ESP_AG_PA_MSC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_PC_ESP_AG_PA_MSC = 4;
        public static final int P56_PC_ESP_AG_PA_MSC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_PC_ESP_AG_PA_MSC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_PC_ESP_AG_PA_MSC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
