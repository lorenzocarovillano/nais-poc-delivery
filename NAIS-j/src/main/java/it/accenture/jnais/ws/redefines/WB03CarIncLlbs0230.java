package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CAR-INC<br>
 * Variable: W-B03-CAR-INC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CarIncLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CarIncLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CAR_INC;
    }

    public void setwB03CarInc(AfDecimal wB03CarInc) {
        writeDecimalAsPacked(Pos.W_B03_CAR_INC, wB03CarInc.copy());
    }

    /**Original name: W-B03-CAR-INC<br>*/
    public AfDecimal getwB03CarInc() {
        return readPackedAsDecimal(Pos.W_B03_CAR_INC, Len.Int.W_B03_CAR_INC, Len.Fract.W_B03_CAR_INC);
    }

    public byte[] getwB03CarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CAR_INC, Pos.W_B03_CAR_INC);
        return buffer;
    }

    public void setwB03CarIncNull(String wB03CarIncNull) {
        writeString(Pos.W_B03_CAR_INC_NULL, wB03CarIncNull, Len.W_B03_CAR_INC_NULL);
    }

    /**Original name: W-B03-CAR-INC-NULL<br>*/
    public String getwB03CarIncNull() {
        return readString(Pos.W_B03_CAR_INC_NULL, Len.W_B03_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_INC = 1;
        public static final int W_B03_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_INC = 8;
        public static final int W_B03_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
