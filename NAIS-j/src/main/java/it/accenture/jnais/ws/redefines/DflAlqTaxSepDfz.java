package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ALQ-TAX-SEP-DFZ<br>
 * Variable: DFL-ALQ-TAX-SEP-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAlqTaxSepDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAlqTaxSepDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ALQ_TAX_SEP_DFZ;
    }

    public void setDflAlqTaxSepDfz(AfDecimal dflAlqTaxSepDfz) {
        writeDecimalAsPacked(Pos.DFL_ALQ_TAX_SEP_DFZ, dflAlqTaxSepDfz.copy());
    }

    public void setDflAlqTaxSepDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ALQ_TAX_SEP_DFZ, Pos.DFL_ALQ_TAX_SEP_DFZ);
    }

    /**Original name: DFL-ALQ-TAX-SEP-DFZ<br>*/
    public AfDecimal getDflAlqTaxSepDfz() {
        return readPackedAsDecimal(Pos.DFL_ALQ_TAX_SEP_DFZ, Len.Int.DFL_ALQ_TAX_SEP_DFZ, Len.Fract.DFL_ALQ_TAX_SEP_DFZ);
    }

    public byte[] getDflAlqTaxSepDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ALQ_TAX_SEP_DFZ, Pos.DFL_ALQ_TAX_SEP_DFZ);
        return buffer;
    }

    public void setDflAlqTaxSepDfzNull(String dflAlqTaxSepDfzNull) {
        writeString(Pos.DFL_ALQ_TAX_SEP_DFZ_NULL, dflAlqTaxSepDfzNull, Len.DFL_ALQ_TAX_SEP_DFZ_NULL);
    }

    /**Original name: DFL-ALQ-TAX-SEP-DFZ-NULL<br>*/
    public String getDflAlqTaxSepDfzNull() {
        return readString(Pos.DFL_ALQ_TAX_SEP_DFZ_NULL, Len.DFL_ALQ_TAX_SEP_DFZ_NULL);
    }

    public String getDflAlqTaxSepDfzNullFormatted() {
        return Functions.padBlanks(getDflAlqTaxSepDfzNull(), Len.DFL_ALQ_TAX_SEP_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_TAX_SEP_DFZ = 1;
        public static final int DFL_ALQ_TAX_SEP_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_TAX_SEP_DFZ = 4;
        public static final int DFL_ALQ_TAX_SEP_DFZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
