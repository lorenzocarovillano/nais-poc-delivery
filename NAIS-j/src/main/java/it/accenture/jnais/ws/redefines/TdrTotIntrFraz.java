package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-INTR-FRAZ<br>
 * Variable: TDR-TOT-INTR-FRAZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_INTR_FRAZ;
    }

    public void setTdrTotIntrFraz(AfDecimal tdrTotIntrFraz) {
        writeDecimalAsPacked(Pos.TDR_TOT_INTR_FRAZ, tdrTotIntrFraz.copy());
    }

    public void setTdrTotIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_INTR_FRAZ, Pos.TDR_TOT_INTR_FRAZ);
    }

    /**Original name: TDR-TOT-INTR-FRAZ<br>*/
    public AfDecimal getTdrTotIntrFraz() {
        return readPackedAsDecimal(Pos.TDR_TOT_INTR_FRAZ, Len.Int.TDR_TOT_INTR_FRAZ, Len.Fract.TDR_TOT_INTR_FRAZ);
    }

    public byte[] getTdrTotIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_INTR_FRAZ, Pos.TDR_TOT_INTR_FRAZ);
        return buffer;
    }

    public void setTdrTotIntrFrazNull(String tdrTotIntrFrazNull) {
        writeString(Pos.TDR_TOT_INTR_FRAZ_NULL, tdrTotIntrFrazNull, Len.TDR_TOT_INTR_FRAZ_NULL);
    }

    /**Original name: TDR-TOT-INTR-FRAZ-NULL<br>*/
    public String getTdrTotIntrFrazNull() {
        return readString(Pos.TDR_TOT_INTR_FRAZ_NULL, Len.TDR_TOT_INTR_FRAZ_NULL);
    }

    public String getTdrTotIntrFrazNullFormatted() {
        return Functions.padBlanks(getTdrTotIntrFrazNull(), Len.TDR_TOT_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_FRAZ = 1;
        public static final int TDR_TOT_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_FRAZ = 8;
        public static final int TDR_TOT_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
