package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-RP-CL<br>
 * Variable: WPCO-DT-ULT-BOLL-RP-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollRpCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollRpCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_RP_CL;
    }

    public void setWpcoDtUltBollRpCl(int wpcoDtUltBollRpCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_RP_CL, wpcoDtUltBollRpCl, Len.Int.WPCO_DT_ULT_BOLL_RP_CL);
    }

    public void setDpcoDtUltBollRpClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RP_CL, Pos.WPCO_DT_ULT_BOLL_RP_CL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RP-CL<br>*/
    public int getWpcoDtUltBollRpCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_RP_CL, Len.Int.WPCO_DT_ULT_BOLL_RP_CL);
    }

    public byte[] getWpcoDtUltBollRpClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RP_CL, Pos.WPCO_DT_ULT_BOLL_RP_CL);
        return buffer;
    }

    public void setWpcoDtUltBollRpClNull(String wpcoDtUltBollRpClNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_RP_CL_NULL, wpcoDtUltBollRpClNull, Len.WPCO_DT_ULT_BOLL_RP_CL_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RP-CL-NULL<br>*/
    public String getWpcoDtUltBollRpClNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_RP_CL_NULL, Len.WPCO_DT_ULT_BOLL_RP_CL_NULL);
    }

    public String getWpcoDtUltBollRpClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollRpClNull(), Len.WPCO_DT_ULT_BOLL_RP_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RP_CL = 1;
        public static final int WPCO_DT_ULT_BOLL_RP_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RP_CL = 5;
        public static final int WPCO_DT_ULT_BOLL_RP_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_RP_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
