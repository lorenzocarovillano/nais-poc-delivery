package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-CALC<br>
 * Variable: WDFL-IMPST-VIS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS_CALC;
    }

    public void setWdflImpstVisCalc(AfDecimal wdflImpstVisCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS_CALC, wdflImpstVisCalc.copy());
    }

    public void setWdflImpstVisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS_CALC, Pos.WDFL_IMPST_VIS_CALC);
    }

    /**Original name: WDFL-IMPST-VIS-CALC<br>*/
    public AfDecimal getWdflImpstVisCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS_CALC, Len.Int.WDFL_IMPST_VIS_CALC, Len.Fract.WDFL_IMPST_VIS_CALC);
    }

    public byte[] getWdflImpstVisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS_CALC, Pos.WDFL_IMPST_VIS_CALC);
        return buffer;
    }

    public void setWdflImpstVisCalcNull(String wdflImpstVisCalcNull) {
        writeString(Pos.WDFL_IMPST_VIS_CALC_NULL, wdflImpstVisCalcNull, Len.WDFL_IMPST_VIS_CALC_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-CALC-NULL<br>*/
    public String getWdflImpstVisCalcNull() {
        return readString(Pos.WDFL_IMPST_VIS_CALC_NULL, Len.WDFL_IMPST_VIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS_CALC = 1;
        public static final int WDFL_IMPST_VIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS_CALC = 8;
        public static final int WDFL_IMPST_VIS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
