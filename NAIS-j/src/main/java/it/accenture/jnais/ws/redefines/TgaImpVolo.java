package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-VOLO<br>
 * Variable: TGA-IMP-VOLO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_VOLO;
    }

    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        writeDecimalAsPacked(Pos.TGA_IMP_VOLO, tgaImpVolo.copy());
    }

    public void setTgaImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_VOLO, Pos.TGA_IMP_VOLO);
    }

    /**Original name: TGA-IMP-VOLO<br>*/
    public AfDecimal getTgaImpVolo() {
        return readPackedAsDecimal(Pos.TGA_IMP_VOLO, Len.Int.TGA_IMP_VOLO, Len.Fract.TGA_IMP_VOLO);
    }

    public byte[] getTgaImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_VOLO, Pos.TGA_IMP_VOLO);
        return buffer;
    }

    public void setTgaImpVoloNull(String tgaImpVoloNull) {
        writeString(Pos.TGA_IMP_VOLO_NULL, tgaImpVoloNull, Len.TGA_IMP_VOLO_NULL);
    }

    /**Original name: TGA-IMP-VOLO-NULL<br>*/
    public String getTgaImpVoloNull() {
        return readString(Pos.TGA_IMP_VOLO_NULL, Len.TGA_IMP_VOLO_NULL);
    }

    public String getTgaImpVoloNullFormatted() {
        return Functions.padBlanks(getTgaImpVoloNull(), Len.TGA_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_VOLO = 1;
        public static final int TGA_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_VOLO = 8;
        public static final int TGA_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
