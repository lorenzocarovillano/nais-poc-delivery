package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-EMIS-RIDZ<br>
 * Variable: B03-DT-EMIS-RIDZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtEmisRidz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtEmisRidz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_EMIS_RIDZ;
    }

    public void setB03DtEmisRidz(int b03DtEmisRidz) {
        writeIntAsPacked(Pos.B03_DT_EMIS_RIDZ, b03DtEmisRidz, Len.Int.B03_DT_EMIS_RIDZ);
    }

    public void setB03DtEmisRidzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_EMIS_RIDZ, Pos.B03_DT_EMIS_RIDZ);
    }

    /**Original name: B03-DT-EMIS-RIDZ<br>*/
    public int getB03DtEmisRidz() {
        return readPackedAsInt(Pos.B03_DT_EMIS_RIDZ, Len.Int.B03_DT_EMIS_RIDZ);
    }

    public byte[] getB03DtEmisRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_EMIS_RIDZ, Pos.B03_DT_EMIS_RIDZ);
        return buffer;
    }

    public void setB03DtEmisRidzNull(String b03DtEmisRidzNull) {
        writeString(Pos.B03_DT_EMIS_RIDZ_NULL, b03DtEmisRidzNull, Len.B03_DT_EMIS_RIDZ_NULL);
    }

    /**Original name: B03-DT-EMIS-RIDZ-NULL<br>*/
    public String getB03DtEmisRidzNull() {
        return readString(Pos.B03_DT_EMIS_RIDZ_NULL, Len.B03_DT_EMIS_RIDZ_NULL);
    }

    public String getB03DtEmisRidzNullFormatted() {
        return Functions.padBlanks(getB03DtEmisRidzNull(), Len.B03_DT_EMIS_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_EMIS_RIDZ = 1;
        public static final int B03_DT_EMIS_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_EMIS_RIDZ = 5;
        public static final int B03_DT_EMIS_RIDZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_EMIS_RIDZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
