package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-IMPB-VIS-RP-P2011<br>
 * Variable: P61-IMPB-VIS-RP-P2011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61ImpbVisRpP2011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61ImpbVisRpP2011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_IMPB_VIS_RP_P2011;
    }

    public void setP61ImpbVisRpP2011(AfDecimal p61ImpbVisRpP2011) {
        writeDecimalAsPacked(Pos.P61_IMPB_VIS_RP_P2011, p61ImpbVisRpP2011.copy());
    }

    public void setP61ImpbVisRpP2011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_IMPB_VIS_RP_P2011, Pos.P61_IMPB_VIS_RP_P2011);
    }

    /**Original name: P61-IMPB-VIS-RP-P2011<br>*/
    public AfDecimal getP61ImpbVisRpP2011() {
        return readPackedAsDecimal(Pos.P61_IMPB_VIS_RP_P2011, Len.Int.P61_IMPB_VIS_RP_P2011, Len.Fract.P61_IMPB_VIS_RP_P2011);
    }

    public byte[] getP61ImpbVisRpP2011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_IMPB_VIS_RP_P2011, Pos.P61_IMPB_VIS_RP_P2011);
        return buffer;
    }

    public void setP61ImpbVisRpP2011Null(String p61ImpbVisRpP2011Null) {
        writeString(Pos.P61_IMPB_VIS_RP_P2011_NULL, p61ImpbVisRpP2011Null, Len.P61_IMPB_VIS_RP_P2011_NULL);
    }

    /**Original name: P61-IMPB-VIS-RP-P2011-NULL<br>*/
    public String getP61ImpbVisRpP2011Null() {
        return readString(Pos.P61_IMPB_VIS_RP_P2011_NULL, Len.P61_IMPB_VIS_RP_P2011_NULL);
    }

    public String getP61ImpbVisRpP2011NullFormatted() {
        return Functions.padBlanks(getP61ImpbVisRpP2011Null(), Len.P61_IMPB_VIS_RP_P2011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_IMPB_VIS_RP_P2011 = 1;
        public static final int P61_IMPB_VIS_RP_P2011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_IMPB_VIS_RP_P2011 = 8;
        public static final int P61_IMPB_VIS_RP_P2011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_IMPB_VIS_RP_P2011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_IMPB_VIS_RP_P2011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
