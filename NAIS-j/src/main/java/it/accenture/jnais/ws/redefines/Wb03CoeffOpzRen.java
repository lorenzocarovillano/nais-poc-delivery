package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COEFF-OPZ-REN<br>
 * Variable: WB03-COEFF-OPZ-REN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CoeffOpzRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CoeffOpzRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COEFF_OPZ_REN;
    }

    public void setWb03CoeffOpzRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COEFF_OPZ_REN, Pos.WB03_COEFF_OPZ_REN);
    }

    /**Original name: WB03-COEFF-OPZ-REN<br>*/
    public AfDecimal getWb03CoeffOpzRen() {
        return readPackedAsDecimal(Pos.WB03_COEFF_OPZ_REN, Len.Int.WB03_COEFF_OPZ_REN, Len.Fract.WB03_COEFF_OPZ_REN);
    }

    public byte[] getWb03CoeffOpzRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COEFF_OPZ_REN, Pos.WB03_COEFF_OPZ_REN);
        return buffer;
    }

    public void setWb03CoeffOpzRenNull(String wb03CoeffOpzRenNull) {
        writeString(Pos.WB03_COEFF_OPZ_REN_NULL, wb03CoeffOpzRenNull, Len.WB03_COEFF_OPZ_REN_NULL);
    }

    /**Original name: WB03-COEFF-OPZ-REN-NULL<br>*/
    public String getWb03CoeffOpzRenNull() {
        return readString(Pos.WB03_COEFF_OPZ_REN_NULL, Len.WB03_COEFF_OPZ_REN_NULL);
    }

    public String getWb03CoeffOpzRenNullFormatted() {
        return Functions.padBlanks(getWb03CoeffOpzRenNull(), Len.WB03_COEFF_OPZ_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_OPZ_REN = 1;
        public static final int WB03_COEFF_OPZ_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COEFF_OPZ_REN = 4;
        public static final int WB03_COEFF_OPZ_REN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_OPZ_REN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_COEFF_OPZ_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
