package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvall1;
import it.accenture.jnais.copy.WallDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WALL-TAB-ASSET-ALL<br>
 * Variables: WALL-TAB-ASSET-ALL from copybook LCCVALL7<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WallTabAssetAll {

    //==== PROPERTIES ====
    //Original name: LCCVALL1
    private Lccvall1 lccvall1 = new Lccvall1();

    //==== METHODS ====
    public void setTabAssetAllBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvall1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvall1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvall1.Len.Int.WALL_ID_PTF, 0));
        position += Lccvall1.Len.WALL_ID_PTF;
        lccvall1.getDati().setWallDatiBytes(buffer, position);
    }

    public byte[] getTabAssetAllBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvall1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvall1.getIdPtf(), Lccvall1.Len.Int.WALL_ID_PTF, 0);
        position += Lccvall1.Len.WALL_ID_PTF;
        lccvall1.getDati().getWallDatiBytes(buffer, position);
        return buffer;
    }

    public void initTabAssetAllSpaces() {
        lccvall1.initLccvall1Spaces();
    }

    public Lccvall1 getLccvall1() {
        return lccvall1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TAB_ASSET_ALL = WpolStatus.Len.STATUS + Lccvall1.Len.WALL_ID_PTF + WallDati.Len.WALL_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
