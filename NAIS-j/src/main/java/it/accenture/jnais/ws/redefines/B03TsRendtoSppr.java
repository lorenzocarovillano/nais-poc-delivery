package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-RENDTO-SPPR<br>
 * Variable: B03-TS-RENDTO-SPPR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsRendtoSppr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsRendtoSppr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_RENDTO_SPPR;
    }

    public void setB03TsRendtoSppr(AfDecimal b03TsRendtoSppr) {
        writeDecimalAsPacked(Pos.B03_TS_RENDTO_SPPR, b03TsRendtoSppr.copy());
    }

    public void setB03TsRendtoSpprFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_RENDTO_SPPR, Pos.B03_TS_RENDTO_SPPR);
    }

    /**Original name: B03-TS-RENDTO-SPPR<br>*/
    public AfDecimal getB03TsRendtoSppr() {
        return readPackedAsDecimal(Pos.B03_TS_RENDTO_SPPR, Len.Int.B03_TS_RENDTO_SPPR, Len.Fract.B03_TS_RENDTO_SPPR);
    }

    public byte[] getB03TsRendtoSpprAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_RENDTO_SPPR, Pos.B03_TS_RENDTO_SPPR);
        return buffer;
    }

    public void setB03TsRendtoSpprNull(String b03TsRendtoSpprNull) {
        writeString(Pos.B03_TS_RENDTO_SPPR_NULL, b03TsRendtoSpprNull, Len.B03_TS_RENDTO_SPPR_NULL);
    }

    /**Original name: B03-TS-RENDTO-SPPR-NULL<br>*/
    public String getB03TsRendtoSpprNull() {
        return readString(Pos.B03_TS_RENDTO_SPPR_NULL, Len.B03_TS_RENDTO_SPPR_NULL);
    }

    public String getB03TsRendtoSpprNullFormatted() {
        return Functions.padBlanks(getB03TsRendtoSpprNull(), Len.B03_TS_RENDTO_SPPR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_RENDTO_SPPR = 1;
        public static final int B03_TS_RENDTO_SPPR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_RENDTO_SPPR = 8;
        public static final int B03_TS_RENDTO_SPPR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_RENDTO_SPPR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_RENDTO_SPPR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
