package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WRIC-ID-RICH-COLLG<br>
 * Variable: WRIC-ID-RICH-COLLG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WricIdRichCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WricIdRichCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIC_ID_RICH_COLLG;
    }

    public void setWricIdRichCollg(int wricIdRichCollg) {
        writeIntAsPacked(Pos.WRIC_ID_RICH_COLLG, wricIdRichCollg, Len.Int.WRIC_ID_RICH_COLLG);
    }

    public void setWricIdRichCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIC_ID_RICH_COLLG, Pos.WRIC_ID_RICH_COLLG);
    }

    /**Original name: WRIC-ID-RICH-COLLG<br>*/
    public int getWricIdRichCollg() {
        return readPackedAsInt(Pos.WRIC_ID_RICH_COLLG, Len.Int.WRIC_ID_RICH_COLLG);
    }

    public byte[] getWricIdRichCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIC_ID_RICH_COLLG, Pos.WRIC_ID_RICH_COLLG);
        return buffer;
    }

    public void setWricIdRichCollgNull(String wricIdRichCollgNull) {
        writeString(Pos.WRIC_ID_RICH_COLLG_NULL, wricIdRichCollgNull, Len.WRIC_ID_RICH_COLLG_NULL);
    }

    /**Original name: WRIC-ID-RICH-COLLG-NULL<br>*/
    public String getWricIdRichCollgNull() {
        return readString(Pos.WRIC_ID_RICH_COLLG_NULL, Len.WRIC_ID_RICH_COLLG_NULL);
    }

    public String getWricIdRichCollgNullFormatted() {
        return Functions.padBlanks(getWricIdRichCollgNull(), Len.WRIC_ID_RICH_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIC_ID_RICH_COLLG = 1;
        public static final int WRIC_ID_RICH_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_ID_RICH_COLLG = 5;
        public static final int WRIC_ID_RICH_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIC_ID_RICH_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
