package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvtga1Lvvs0037;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3040<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3040Data {

    //==== PROPERTIES ====
    //Original name: WK-PGM-CALLED
    private String wkPgmCalled = "LDBSF110";
    //Original name: LDBVF111
    private Ldbvf111 ldbvf111 = new Ldbvf111();
    //Original name: DTGA-ELE-TRCH-MAX
    private short dtgaEleTrchMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVTGA1
    private Lccvtga1Lvvs0037 lccvtga1 = new Lccvtga1Lvvs0037();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgmCalled() {
        return this.wkPgmCalled;
    }

    public void setDtgaAreaTrancheFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TRANCHE];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TRANCHE);
        setDtgaAreaTrancheBytes(buffer, 1);
    }

    public void setDtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTrchMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDtgaTabTrchBytes(buffer, position);
    }

    public void setDtgaEleTrchMax(short dtgaEleTrchMax) {
        this.dtgaEleTrchMax = dtgaEleTrchMax;
    }

    public short getDtgaEleTrchMax() {
        return this.dtgaEleTrchMax;
    }

    public void setDtgaTabTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtga1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtga1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtga1Lvvs0037.Len.Int.ID_PTF, 0));
        position += Lccvtga1Lvvs0037.Len.ID_PTF;
        lccvtga1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvtga1Lvvs0037 getLccvtga1() {
        return lccvtga1;
    }

    public Ldbvf111 getLdbvf111() {
        return ldbvf111;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DTGA_ELE_TRCH_MAX = 2;
        public static final int DTGA_TAB_TRCH = WpolStatus.Len.STATUS + Lccvtga1Lvvs0037.Len.ID_PTF + WtgaDati.Len.DATI;
        public static final int DTGA_AREA_TRANCHE = DTGA_ELE_TRCH_MAX + DTGA_TAB_TRCH;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
