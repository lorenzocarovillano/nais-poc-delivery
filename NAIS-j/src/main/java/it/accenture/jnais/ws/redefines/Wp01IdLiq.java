package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-ID-LIQ<br>
 * Variable: WP01-ID-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01IdLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01IdLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_ID_LIQ;
    }

    public void setWp01IdLiq(int wp01IdLiq) {
        writeIntAsPacked(Pos.WP01_ID_LIQ, wp01IdLiq, Len.Int.WP01_ID_LIQ);
    }

    public void setWp01IdLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_ID_LIQ, Pos.WP01_ID_LIQ);
    }

    /**Original name: WP01-ID-LIQ<br>*/
    public int getWp01IdLiq() {
        return readPackedAsInt(Pos.WP01_ID_LIQ, Len.Int.WP01_ID_LIQ);
    }

    public byte[] getWp01IdLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_ID_LIQ, Pos.WP01_ID_LIQ);
        return buffer;
    }

    public void setWp01IdLiqNull(String wp01IdLiqNull) {
        writeString(Pos.WP01_ID_LIQ_NULL, wp01IdLiqNull, Len.WP01_ID_LIQ_NULL);
    }

    /**Original name: WP01-ID-LIQ-NULL<br>*/
    public String getWp01IdLiqNull() {
        return readString(Pos.WP01_ID_LIQ_NULL, Len.WP01_ID_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_ID_LIQ = 1;
        public static final int WP01_ID_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_ID_LIQ = 5;
        public static final int WP01_ID_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_ID_LIQ = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
