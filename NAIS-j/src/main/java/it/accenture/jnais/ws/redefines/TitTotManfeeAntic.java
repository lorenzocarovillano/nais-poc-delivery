package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-MANFEE-ANTIC<br>
 * Variable: TIT-TOT-MANFEE-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_MANFEE_ANTIC;
    }

    public void setTitTotManfeeAntic(AfDecimal titTotManfeeAntic) {
        writeDecimalAsPacked(Pos.TIT_TOT_MANFEE_ANTIC, titTotManfeeAntic.copy());
    }

    public void setTitTotManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_MANFEE_ANTIC, Pos.TIT_TOT_MANFEE_ANTIC);
    }

    /**Original name: TIT-TOT-MANFEE-ANTIC<br>*/
    public AfDecimal getTitTotManfeeAntic() {
        return readPackedAsDecimal(Pos.TIT_TOT_MANFEE_ANTIC, Len.Int.TIT_TOT_MANFEE_ANTIC, Len.Fract.TIT_TOT_MANFEE_ANTIC);
    }

    public byte[] getTitTotManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_MANFEE_ANTIC, Pos.TIT_TOT_MANFEE_ANTIC);
        return buffer;
    }

    public void setTitTotManfeeAnticNull(String titTotManfeeAnticNull) {
        writeString(Pos.TIT_TOT_MANFEE_ANTIC_NULL, titTotManfeeAnticNull, Len.TIT_TOT_MANFEE_ANTIC_NULL);
    }

    /**Original name: TIT-TOT-MANFEE-ANTIC-NULL<br>*/
    public String getTitTotManfeeAnticNull() {
        return readString(Pos.TIT_TOT_MANFEE_ANTIC_NULL, Len.TIT_TOT_MANFEE_ANTIC_NULL);
    }

    public String getTitTotManfeeAnticNullFormatted() {
        return Functions.padBlanks(getTitTotManfeeAnticNull(), Len.TIT_TOT_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_MANFEE_ANTIC = 1;
        public static final int TIT_TOT_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_MANFEE_ANTIC = 8;
        public static final int TIT_TOT_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
