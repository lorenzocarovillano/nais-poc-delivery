package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-RIT-TFR<br>
 * Variable: S089-TOT-IMP-RIT-TFR from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpRitTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpRitTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_RIT_TFR;
    }

    public void setWlquTotImpRitTfr(AfDecimal wlquTotImpRitTfr) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_RIT_TFR, wlquTotImpRitTfr.copy());
    }

    public void setWlquTotImpRitTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_RIT_TFR, Pos.S089_TOT_IMP_RIT_TFR);
    }

    /**Original name: WLQU-TOT-IMP-RIT-TFR<br>*/
    public AfDecimal getWlquTotImpRitTfr() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_RIT_TFR, Len.Int.WLQU_TOT_IMP_RIT_TFR, Len.Fract.WLQU_TOT_IMP_RIT_TFR);
    }

    public byte[] getWlquTotImpRitTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_RIT_TFR, Pos.S089_TOT_IMP_RIT_TFR);
        return buffer;
    }

    public void initWlquTotImpRitTfrSpaces() {
        fill(Pos.S089_TOT_IMP_RIT_TFR, Len.S089_TOT_IMP_RIT_TFR, Types.SPACE_CHAR);
    }

    public void setWlquTotImpRitTfrNull(String wlquTotImpRitTfrNull) {
        writeString(Pos.S089_TOT_IMP_RIT_TFR_NULL, wlquTotImpRitTfrNull, Len.WLQU_TOT_IMP_RIT_TFR_NULL);
    }

    /**Original name: WLQU-TOT-IMP-RIT-TFR-NULL<br>*/
    public String getWlquTotImpRitTfrNull() {
        return readString(Pos.S089_TOT_IMP_RIT_TFR_NULL, Len.WLQU_TOT_IMP_RIT_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIT_TFR = 1;
        public static final int S089_TOT_IMP_RIT_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIT_TFR = 8;
        public static final int WLQU_TOT_IMP_RIT_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIT_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIT_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
