package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMPB-PROV-ACQ<br>
 * Variable: TGA-IMPB-PROV-ACQ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpbProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpbProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMPB_PROV_ACQ;
    }

    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        writeDecimalAsPacked(Pos.TGA_IMPB_PROV_ACQ, tgaImpbProvAcq.copy());
    }

    public void setTgaImpbProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMPB_PROV_ACQ, Pos.TGA_IMPB_PROV_ACQ);
    }

    /**Original name: TGA-IMPB-PROV-ACQ<br>*/
    public AfDecimal getTgaImpbProvAcq() {
        return readPackedAsDecimal(Pos.TGA_IMPB_PROV_ACQ, Len.Int.TGA_IMPB_PROV_ACQ, Len.Fract.TGA_IMPB_PROV_ACQ);
    }

    public byte[] getTgaImpbProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMPB_PROV_ACQ, Pos.TGA_IMPB_PROV_ACQ);
        return buffer;
    }

    public void setTgaImpbProvAcqNull(String tgaImpbProvAcqNull) {
        writeString(Pos.TGA_IMPB_PROV_ACQ_NULL, tgaImpbProvAcqNull, Len.TGA_IMPB_PROV_ACQ_NULL);
    }

    /**Original name: TGA-IMPB-PROV-ACQ-NULL<br>*/
    public String getTgaImpbProvAcqNull() {
        return readString(Pos.TGA_IMPB_PROV_ACQ_NULL, Len.TGA_IMPB_PROV_ACQ_NULL);
    }

    public String getTgaImpbProvAcqNullFormatted() {
        return Functions.padBlanks(getTgaImpbProvAcqNull(), Len.TGA_IMPB_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_PROV_ACQ = 1;
        public static final int TGA_IMPB_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_PROV_ACQ = 8;
        public static final int TGA_IMPB_PROV_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_PROV_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
