package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.WpagDatiAltri;
import it.accenture.jnais.copy.WpagDatiParMovim;
import it.accenture.jnais.copy.WpagDatiProvvig;
import it.accenture.jnais.ws.redefines.WpagAaDiffPror;
import it.accenture.jnais.ws.redefines.WpagImpMovi;
import it.accenture.jnais.ws.redefines.WpagImpMoviNeg;
import it.accenture.jnais.ws.redefines.WpagMmDiffPror;

/**Original name: WPAG-TAB-CALCOLI<br>
 * Variables: WPAG-TAB-CALCOLI from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagTabCalcoli {

    //==== PROPERTIES ====
    public static final int WPAG_DATI_TRANCHE_MAXOCCURS = 2;
    public static final int WPAG_DATI_SOPR_GAR_MAXOCCURS = 20;
    public static final int WPAG_DATI_PAR_CALC_MAXOCCURS = 20;
    public static final int WPAG_DATI_TIT_CONT_MAXOCCURS = 12;
    public static final int WPAG_DATI_TIT_RATA_MAXOCCURS = 12;
    //Original name: WPAG-COD-GARANZIA
    private String wpagCodGaranzia = DefaultValues.stringVal(Len.WPAG_COD_GARANZIA);
    //Original name: WPAG-IMP-MOVI
    private WpagImpMovi wpagImpMovi = new WpagImpMovi();
    //Original name: WPAG-IMP-MOVI-NEG
    private WpagImpMoviNeg wpagImpMoviNeg = new WpagImpMoviNeg();
    //Original name: WPAG-AA-DIFF-PROR
    private WpagAaDiffPror wpagAaDiffPror = new WpagAaDiffPror();
    //Original name: WPAG-MM-DIFF-PROR
    private WpagMmDiffPror wpagMmDiffPror = new WpagMmDiffPror();
    /**Original name: WPAG-ELE-TRANCHE-MAX<br>
	 * <pre>--      Dati di Tranche</pre>*/
    private short wpagEleTrancheMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-DATI-TRANCHE
    private WpagDatiTranche[] wpagDatiTranche = new WpagDatiTranche[WPAG_DATI_TRANCHE_MAXOCCURS];
    //Original name: WPAG-ELE-SOPR-GAR-MAX
    private short wpagEleSoprGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-DATI-SOPR-GAR
    private WpagDatiSoprGar[] wpagDatiSoprGar = new WpagDatiSoprGar[WPAG_DATI_SOPR_GAR_MAXOCCURS];
    //Original name: WPAG-ELE-PAR-CALC-MAX
    private short wpagEleParCalcMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-DATI-PAR-CALC
    private WpagDatiParCalc[] wpagDatiParCalc = new WpagDatiParCalc[WPAG_DATI_PAR_CALC_MAXOCCURS];
    //Original name: WPAG-DATI-PROVVIG
    private WpagDatiProvvig wpagDatiProvvig = new WpagDatiProvvig();
    //Original name: WPAG-ELE-TIT-CONT-MAX
    private short wpagEleTitContMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-DATI-TIT-CONT
    private WpagDatiTitCont[] wpagDatiTitCont = new WpagDatiTitCont[WPAG_DATI_TIT_CONT_MAXOCCURS];
    //Original name: WPAG-ELE-TIT-RATA-MAX
    private short wpagEleTitRataMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WPAG-DATI-TIT-RATA
    private WpagDatiTitRata[] wpagDatiTitRata = new WpagDatiTitRata[WPAG_DATI_TIT_RATA_MAXOCCURS];
    //Original name: WPAG-DATI-ALTRI
    private WpagDatiAltri wpagDatiAltri = new WpagDatiAltri();
    //Original name: WPAG-DATI-PAR-MOVIM
    private WpagDatiParMovim wpagDatiParMovim = new WpagDatiParMovim();

    //==== CONSTRUCTORS ====
    public WpagTabCalcoli() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wpagDatiTrancheIdx = 1; wpagDatiTrancheIdx <= WPAG_DATI_TRANCHE_MAXOCCURS; wpagDatiTrancheIdx++) {
            wpagDatiTranche[wpagDatiTrancheIdx - 1] = new WpagDatiTranche();
        }
        for (int wpagDatiSoprGarIdx = 1; wpagDatiSoprGarIdx <= WPAG_DATI_SOPR_GAR_MAXOCCURS; wpagDatiSoprGarIdx++) {
            wpagDatiSoprGar[wpagDatiSoprGarIdx - 1] = new WpagDatiSoprGar();
        }
        for (int wpagDatiParCalcIdx = 1; wpagDatiParCalcIdx <= WPAG_DATI_PAR_CALC_MAXOCCURS; wpagDatiParCalcIdx++) {
            wpagDatiParCalc[wpagDatiParCalcIdx - 1] = new WpagDatiParCalc();
        }
        for (int wpagDatiTitContIdx = 1; wpagDatiTitContIdx <= WPAG_DATI_TIT_CONT_MAXOCCURS; wpagDatiTitContIdx++) {
            wpagDatiTitCont[wpagDatiTitContIdx - 1] = new WpagDatiTitCont();
        }
        for (int wpagDatiTitRataIdx = 1; wpagDatiTitRataIdx <= WPAG_DATI_TIT_RATA_MAXOCCURS; wpagDatiTitRataIdx++) {
            wpagDatiTitRata[wpagDatiTitRataIdx - 1] = new WpagDatiTitRata();
        }
    }

    public void setWpagTabCalcoliBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagCodGaranzia = MarshalByte.readString(buffer, position, Len.WPAG_COD_GARANZIA);
        position += Len.WPAG_COD_GARANZIA;
        wpagImpMovi.setWpagImpMoviFromBuffer(buffer, position);
        position += WpagImpMovi.Len.WPAG_IMP_MOVI;
        wpagImpMoviNeg.setWpagImpMoviNegFromBuffer(buffer, position);
        position += WpagImpMoviNeg.Len.WPAG_IMP_MOVI_NEG;
        wpagAaDiffPror.setWpagAaDiffProrFromBuffer(buffer, position);
        position += WpagAaDiffPror.Len.WPAG_AA_DIFF_PROR;
        wpagMmDiffPror.setWpagMmDiffProrFromBuffer(buffer, position);
        position += WpagMmDiffPror.Len.WPAG_MM_DIFF_PROR;
        wpagEleTrancheMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_TRANCHE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagDatiTranche[idx - 1].setWpagDatiTrancheBytes(buffer, position);
                position += WpagDatiTranche.Len.WPAG_DATI_TRANCHE;
            }
            else {
                wpagDatiTranche[idx - 1].initWpagDatiTrancheSpaces();
                position += WpagDatiTranche.Len.WPAG_DATI_TRANCHE;
            }
        }
        wpagEleSoprGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_SOPR_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagDatiSoprGar[idx - 1].setWpagDatiSoprGarBytes(buffer, position);
                position += WpagDatiSoprGar.Len.WPAG_DATI_SOPR_GAR;
            }
            else {
                wpagDatiSoprGar[idx - 1].initWpagDatiSoprGarSpaces();
                position += WpagDatiSoprGar.Len.WPAG_DATI_SOPR_GAR;
            }
        }
        wpagEleParCalcMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_PAR_CALC_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagDatiParCalc[idx - 1].setWpagDatiParCalcBytes(buffer, position);
                position += WpagDatiParCalc.Len.WPAG_DATI_PAR_CALC;
            }
            else {
                wpagDatiParCalc[idx - 1].initWpagDatiParCalcSpaces();
                position += WpagDatiParCalc.Len.WPAG_DATI_PAR_CALC;
            }
        }
        wpagDatiProvvig.setWpagDatiProvvigBytes(buffer, position);
        position += WpagDatiProvvig.Len.WPAG_DATI_PROVVIG;
        wpagEleTitContMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_TIT_CONT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagDatiTitCont[idx - 1].setWpagDatiTitContBytes(buffer, position);
                position += WpagDatiTitCont.Len.WPAG_DATI_TIT_CONT;
            }
            else {
                wpagDatiTitCont[idx - 1].initWpagDatiTitContSpaces();
                position += WpagDatiTitCont.Len.WPAG_DATI_TIT_CONT;
            }
        }
        wpagEleTitRataMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_TIT_RATA_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wpagDatiTitRata[idx - 1].setWpagDatiTitRataBytes(buffer, position);
                position += WpagDatiTitRata.Len.WPAG_DATI_TIT_RATA;
            }
            else {
                wpagDatiTitRata[idx - 1].initWpagDatiTitRataSpaces();
                position += WpagDatiTitRata.Len.WPAG_DATI_TIT_RATA;
            }
        }
        wpagDatiAltri.setWpagDatiAltriBytes(buffer, position);
        position += WpagDatiAltri.Len.WPAG_DATI_ALTRI;
        wpagDatiParMovim.setWpagDatiParMovimBytes(buffer, position);
    }

    public byte[] getWpagTabCalcoliBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagCodGaranzia, Len.WPAG_COD_GARANZIA);
        position += Len.WPAG_COD_GARANZIA;
        wpagImpMovi.getWpagImpMoviAsBuffer(buffer, position);
        position += WpagImpMovi.Len.WPAG_IMP_MOVI;
        wpagImpMoviNeg.getWpagImpMoviNegAsBuffer(buffer, position);
        position += WpagImpMoviNeg.Len.WPAG_IMP_MOVI_NEG;
        wpagAaDiffPror.getWpagAaDiffProrAsBuffer(buffer, position);
        position += WpagAaDiffPror.Len.WPAG_AA_DIFF_PROR;
        wpagMmDiffPror.getWpagMmDiffProrAsBuffer(buffer, position);
        position += WpagMmDiffPror.Len.WPAG_MM_DIFF_PROR;
        MarshalByte.writeBinaryShort(buffer, position, wpagEleTrancheMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_TRANCHE_MAXOCCURS; idx++) {
            wpagDatiTranche[idx - 1].getWpagDatiTrancheBytes(buffer, position);
            position += WpagDatiTranche.Len.WPAG_DATI_TRANCHE;
        }
        MarshalByte.writeBinaryShort(buffer, position, wpagEleSoprGarMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_SOPR_GAR_MAXOCCURS; idx++) {
            wpagDatiSoprGar[idx - 1].getWpagDatiSoprGarBytes(buffer, position);
            position += WpagDatiSoprGar.Len.WPAG_DATI_SOPR_GAR;
        }
        MarshalByte.writeBinaryShort(buffer, position, wpagEleParCalcMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_PAR_CALC_MAXOCCURS; idx++) {
            wpagDatiParCalc[idx - 1].getWpagDatiParCalcBytes(buffer, position);
            position += WpagDatiParCalc.Len.WPAG_DATI_PAR_CALC;
        }
        wpagDatiProvvig.getWpagDatiProvvigBytes(buffer, position);
        position += WpagDatiProvvig.Len.WPAG_DATI_PROVVIG;
        MarshalByte.writeBinaryShort(buffer, position, wpagEleTitContMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_TIT_CONT_MAXOCCURS; idx++) {
            wpagDatiTitCont[idx - 1].getWpagDatiTitContBytes(buffer, position);
            position += WpagDatiTitCont.Len.WPAG_DATI_TIT_CONT;
        }
        MarshalByte.writeBinaryShort(buffer, position, wpagEleTitRataMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WPAG_DATI_TIT_RATA_MAXOCCURS; idx++) {
            wpagDatiTitRata[idx - 1].getWpagDatiTitRataBytes(buffer, position);
            position += WpagDatiTitRata.Len.WPAG_DATI_TIT_RATA;
        }
        wpagDatiAltri.getWpagDatiAltriBytes(buffer, position);
        position += WpagDatiAltri.Len.WPAG_DATI_ALTRI;
        wpagDatiParMovim.getWpagDatiParMovimBytes(buffer, position);
        return buffer;
    }

    public void initWpagTabCalcoliSpaces() {
        wpagCodGaranzia = "";
        wpagImpMovi.initWpagImpMoviSpaces();
        wpagImpMoviNeg.initWpagImpMoviNegSpaces();
        wpagAaDiffPror.initWpagAaDiffProrSpaces();
        wpagMmDiffPror.initWpagMmDiffProrSpaces();
        wpagEleTrancheMax = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= WPAG_DATI_TRANCHE_MAXOCCURS; idx++) {
            wpagDatiTranche[idx - 1].initWpagDatiTrancheSpaces();
        }
        wpagEleSoprGarMax = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= WPAG_DATI_SOPR_GAR_MAXOCCURS; idx++) {
            wpagDatiSoprGar[idx - 1].initWpagDatiSoprGarSpaces();
        }
        wpagEleParCalcMax = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= WPAG_DATI_PAR_CALC_MAXOCCURS; idx++) {
            wpagDatiParCalc[idx - 1].initWpagDatiParCalcSpaces();
        }
        wpagDatiProvvig.initWpagDatiProvvigSpaces();
        wpagEleTitContMax = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= WPAG_DATI_TIT_CONT_MAXOCCURS; idx++) {
            wpagDatiTitCont[idx - 1].initWpagDatiTitContSpaces();
        }
        wpagEleTitRataMax = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= WPAG_DATI_TIT_RATA_MAXOCCURS; idx++) {
            wpagDatiTitRata[idx - 1].initWpagDatiTitRataSpaces();
        }
        wpagDatiAltri.initWpagDatiAltriSpaces();
        wpagDatiParMovim.initWpagDatiParMovimSpaces();
    }

    public void setWpagCodGaranzia(String wpagCodGaranzia) {
        this.wpagCodGaranzia = Functions.subString(wpagCodGaranzia, Len.WPAG_COD_GARANZIA);
    }

    public String getWpagCodGaranzia() {
        return this.wpagCodGaranzia;
    }

    public void setWpagEleTrancheMax(short wpagEleTrancheMax) {
        this.wpagEleTrancheMax = wpagEleTrancheMax;
    }

    public short getWpagEleTrancheMax() {
        return this.wpagEleTrancheMax;
    }

    public void setWpagEleSoprGarMax(short wpagEleSoprGarMax) {
        this.wpagEleSoprGarMax = wpagEleSoprGarMax;
    }

    public short getWpagEleSoprGarMax() {
        return this.wpagEleSoprGarMax;
    }

    public void setWpagEleParCalcMax(short wpagEleParCalcMax) {
        this.wpagEleParCalcMax = wpagEleParCalcMax;
    }

    public short getWpagEleParCalcMax() {
        return this.wpagEleParCalcMax;
    }

    public void setWpagEleTitContMax(short wpagEleTitContMax) {
        this.wpagEleTitContMax = wpagEleTitContMax;
    }

    public short getWpagEleTitContMax() {
        return this.wpagEleTitContMax;
    }

    public void setWpagEleTitRataMax(short wpagEleTitRataMax) {
        this.wpagEleTitRataMax = wpagEleTitRataMax;
    }

    public short getWpagEleTitRataMax() {
        return this.wpagEleTitRataMax;
    }

    public WpagAaDiffPror getWpagAaDiffPror() {
        return wpagAaDiffPror;
    }

    public WpagDatiAltri getWpagDatiAltri() {
        return wpagDatiAltri;
    }

    public WpagDatiParCalc getWpagDatiParCalc(int idx) {
        return wpagDatiParCalc[idx - 1];
    }

    public WpagDatiParMovim getWpagDatiParMovim() {
        return wpagDatiParMovim;
    }

    public WpagDatiProvvig getWpagDatiProvvig() {
        return wpagDatiProvvig;
    }

    public WpagDatiSoprGar getWpagDatiSoprGar(int idx) {
        return wpagDatiSoprGar[idx - 1];
    }

    public WpagDatiTitCont getWpagDatiTitCont(int idx) {
        return wpagDatiTitCont[idx - 1];
    }

    public WpagDatiTitRata getWpagDatiTitRata(int idx) {
        return wpagDatiTitRata[idx - 1];
    }

    public WpagDatiTranche getWpagDatiTranche(int idx) {
        return wpagDatiTranche[idx - 1];
    }

    public WpagImpMovi getWpagImpMovi() {
        return wpagImpMovi;
    }

    public WpagImpMoviNeg getWpagImpMoviNeg() {
        return wpagImpMoviNeg;
    }

    public WpagMmDiffPror getWpagMmDiffPror() {
        return wpagMmDiffPror;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_COD_GARANZIA = 12;
        public static final int WPAG_ELE_TRANCHE_MAX = 2;
        public static final int WPAG_ELE_SOPR_GAR_MAX = 2;
        public static final int WPAG_ELE_PAR_CALC_MAX = 2;
        public static final int WPAG_ELE_TIT_CONT_MAX = 2;
        public static final int WPAG_ELE_TIT_RATA_MAX = 2;
        public static final int WPAG_TAB_CALCOLI = WPAG_COD_GARANZIA + WpagImpMovi.Len.WPAG_IMP_MOVI + WpagImpMoviNeg.Len.WPAG_IMP_MOVI_NEG + WpagAaDiffPror.Len.WPAG_AA_DIFF_PROR + WpagMmDiffPror.Len.WPAG_MM_DIFF_PROR + WPAG_ELE_TRANCHE_MAX + WpagTabCalcoli.WPAG_DATI_TRANCHE_MAXOCCURS * WpagDatiTranche.Len.WPAG_DATI_TRANCHE + WPAG_ELE_SOPR_GAR_MAX + WpagTabCalcoli.WPAG_DATI_SOPR_GAR_MAXOCCURS * WpagDatiSoprGar.Len.WPAG_DATI_SOPR_GAR + WPAG_ELE_PAR_CALC_MAX + WpagTabCalcoli.WPAG_DATI_PAR_CALC_MAXOCCURS * WpagDatiParCalc.Len.WPAG_DATI_PAR_CALC + WpagDatiProvvig.Len.WPAG_DATI_PROVVIG + WPAG_ELE_TIT_CONT_MAX + WpagTabCalcoli.WPAG_DATI_TIT_CONT_MAXOCCURS * WpagDatiTitCont.Len.WPAG_DATI_TIT_CONT + WPAG_ELE_TIT_RATA_MAX + WpagTabCalcoli.WPAG_DATI_TIT_RATA_MAXOCCURS * WpagDatiTitRata.Len.WPAG_DATI_TIT_RATA + WpagDatiAltri.Len.WPAG_DATI_ALTRI + WpagDatiParMovim.Len.WPAG_DATI_PAR_MOVIM;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
