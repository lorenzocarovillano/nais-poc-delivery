package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-CAR-IAS<br>
 * Variable: WTIT-TOT-CAR-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_CAR_IAS;
    }

    public void setWtitTotCarIas(AfDecimal wtitTotCarIas) {
        writeDecimalAsPacked(Pos.WTIT_TOT_CAR_IAS, wtitTotCarIas.copy());
    }

    public void setWtitTotCarIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_CAR_IAS, Pos.WTIT_TOT_CAR_IAS);
    }

    /**Original name: WTIT-TOT-CAR-IAS<br>*/
    public AfDecimal getWtitTotCarIas() {
        return readPackedAsDecimal(Pos.WTIT_TOT_CAR_IAS, Len.Int.WTIT_TOT_CAR_IAS, Len.Fract.WTIT_TOT_CAR_IAS);
    }

    public byte[] getWtitTotCarIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_CAR_IAS, Pos.WTIT_TOT_CAR_IAS);
        return buffer;
    }

    public void initWtitTotCarIasSpaces() {
        fill(Pos.WTIT_TOT_CAR_IAS, Len.WTIT_TOT_CAR_IAS, Types.SPACE_CHAR);
    }

    public void setWtitTotCarIasNull(String wtitTotCarIasNull) {
        writeString(Pos.WTIT_TOT_CAR_IAS_NULL, wtitTotCarIasNull, Len.WTIT_TOT_CAR_IAS_NULL);
    }

    /**Original name: WTIT-TOT-CAR-IAS-NULL<br>*/
    public String getWtitTotCarIasNull() {
        return readString(Pos.WTIT_TOT_CAR_IAS_NULL, Len.WTIT_TOT_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_IAS = 1;
        public static final int WTIT_TOT_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_IAS = 8;
        public static final int WTIT_TOT_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
