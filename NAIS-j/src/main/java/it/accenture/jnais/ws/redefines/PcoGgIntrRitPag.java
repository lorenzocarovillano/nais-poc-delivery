package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-GG-INTR-RIT-PAG<br>
 * Variable: PCO-GG-INTR-RIT-PAG from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoGgIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoGgIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_GG_INTR_RIT_PAG;
    }

    public void setPcoGgIntrRitPag(int pcoGgIntrRitPag) {
        writeIntAsPacked(Pos.PCO_GG_INTR_RIT_PAG, pcoGgIntrRitPag, Len.Int.PCO_GG_INTR_RIT_PAG);
    }

    public void setPcoGgIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_GG_INTR_RIT_PAG, Pos.PCO_GG_INTR_RIT_PAG);
    }

    /**Original name: PCO-GG-INTR-RIT-PAG<br>*/
    public int getPcoGgIntrRitPag() {
        return readPackedAsInt(Pos.PCO_GG_INTR_RIT_PAG, Len.Int.PCO_GG_INTR_RIT_PAG);
    }

    public byte[] getPcoGgIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_GG_INTR_RIT_PAG, Pos.PCO_GG_INTR_RIT_PAG);
        return buffer;
    }

    public void initPcoGgIntrRitPagHighValues() {
        fill(Pos.PCO_GG_INTR_RIT_PAG, Len.PCO_GG_INTR_RIT_PAG, Types.HIGH_CHAR_VAL);
    }

    public void setPcoGgIntrRitPagNull(String pcoGgIntrRitPagNull) {
        writeString(Pos.PCO_GG_INTR_RIT_PAG_NULL, pcoGgIntrRitPagNull, Len.PCO_GG_INTR_RIT_PAG_NULL);
    }

    /**Original name: PCO-GG-INTR-RIT-PAG-NULL<br>*/
    public String getPcoGgIntrRitPagNull() {
        return readString(Pos.PCO_GG_INTR_RIT_PAG_NULL, Len.PCO_GG_INTR_RIT_PAG_NULL);
    }

    public String getPcoGgIntrRitPagNullFormatted() {
        return Functions.padBlanks(getPcoGgIntrRitPagNull(), Len.PCO_GG_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_GG_INTR_RIT_PAG = 1;
        public static final int PCO_GG_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_GG_INTR_RIT_PAG = 3;
        public static final int PCO_GG_INTR_RIT_PAG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_GG_INTR_RIT_PAG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
