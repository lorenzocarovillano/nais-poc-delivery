package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-RIT-VIS<br>
 * Variable: TCL-IMP-RIT-VIS from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpRitVis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpRitVis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_RIT_VIS;
    }

    public void setTclImpRitVis(AfDecimal tclImpRitVis) {
        writeDecimalAsPacked(Pos.TCL_IMP_RIT_VIS, tclImpRitVis.copy());
    }

    public void setTclImpRitVisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_RIT_VIS, Pos.TCL_IMP_RIT_VIS);
    }

    /**Original name: TCL-IMP-RIT-VIS<br>*/
    public AfDecimal getTclImpRitVis() {
        return readPackedAsDecimal(Pos.TCL_IMP_RIT_VIS, Len.Int.TCL_IMP_RIT_VIS, Len.Fract.TCL_IMP_RIT_VIS);
    }

    public byte[] getTclImpRitVisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_RIT_VIS, Pos.TCL_IMP_RIT_VIS);
        return buffer;
    }

    public void setTclImpRitVisNull(String tclImpRitVisNull) {
        writeString(Pos.TCL_IMP_RIT_VIS_NULL, tclImpRitVisNull, Len.TCL_IMP_RIT_VIS_NULL);
    }

    /**Original name: TCL-IMP-RIT-VIS-NULL<br>*/
    public String getTclImpRitVisNull() {
        return readString(Pos.TCL_IMP_RIT_VIS_NULL, Len.TCL_IMP_RIT_VIS_NULL);
    }

    public String getTclImpRitVisNullFormatted() {
        return Functions.padBlanks(getTclImpRitVisNull(), Len.TCL_IMP_RIT_VIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIT_VIS = 1;
        public static final int TCL_IMP_RIT_VIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIT_VIS = 8;
        public static final int TCL_IMP_RIT_VIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIT_VIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIT_VIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
