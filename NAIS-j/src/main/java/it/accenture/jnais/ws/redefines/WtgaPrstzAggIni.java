package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-AGG-INI<br>
 * Variable: WTGA-PRSTZ-AGG-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzAggIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzAggIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_AGG_INI;
    }

    public void setWtgaPrstzAggIni(AfDecimal wtgaPrstzAggIni) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_AGG_INI, wtgaPrstzAggIni.copy());
    }

    public void setWtgaPrstzAggIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_AGG_INI, Pos.WTGA_PRSTZ_AGG_INI);
    }

    /**Original name: WTGA-PRSTZ-AGG-INI<br>*/
    public AfDecimal getWtgaPrstzAggIni() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_AGG_INI, Len.Int.WTGA_PRSTZ_AGG_INI, Len.Fract.WTGA_PRSTZ_AGG_INI);
    }

    public byte[] getWtgaPrstzAggIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_AGG_INI, Pos.WTGA_PRSTZ_AGG_INI);
        return buffer;
    }

    public void initWtgaPrstzAggIniSpaces() {
        fill(Pos.WTGA_PRSTZ_AGG_INI, Len.WTGA_PRSTZ_AGG_INI, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzAggIniNull(String wtgaPrstzAggIniNull) {
        writeString(Pos.WTGA_PRSTZ_AGG_INI_NULL, wtgaPrstzAggIniNull, Len.WTGA_PRSTZ_AGG_INI_NULL);
    }

    /**Original name: WTGA-PRSTZ-AGG-INI-NULL<br>*/
    public String getWtgaPrstzAggIniNull() {
        return readString(Pos.WTGA_PRSTZ_AGG_INI_NULL, Len.WTGA_PRSTZ_AGG_INI_NULL);
    }

    public String getWtgaPrstzAggIniNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzAggIniNull(), Len.WTGA_PRSTZ_AGG_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_AGG_INI = 1;
        public static final int WTGA_PRSTZ_AGG_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_AGG_INI = 8;
        public static final int WTGA_PRSTZ_AGG_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_AGG_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_AGG_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
