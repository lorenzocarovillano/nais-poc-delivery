package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-PRSTZ-AGG-INI<br>
 * Variable: WPAG-IMP-PRSTZ-AGG-INI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpPrstzAggIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpPrstzAggIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_PRSTZ_AGG_INI;
    }

    public void setWpagImpPrstzAggIni(AfDecimal wpagImpPrstzAggIni) {
        writeDecimalAsPacked(Pos.WPAG_IMP_PRSTZ_AGG_INI, wpagImpPrstzAggIni.copy());
    }

    public void setWpagImpPrstzAggIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_PRSTZ_AGG_INI, Pos.WPAG_IMP_PRSTZ_AGG_INI);
    }

    /**Original name: WPAG-IMP-PRSTZ-AGG-INI<br>*/
    public AfDecimal getWpagImpPrstzAggIni() {
        return readPackedAsDecimal(Pos.WPAG_IMP_PRSTZ_AGG_INI, Len.Int.WPAG_IMP_PRSTZ_AGG_INI, Len.Fract.WPAG_IMP_PRSTZ_AGG_INI);
    }

    public byte[] getWpagImpPrstzAggIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_PRSTZ_AGG_INI, Pos.WPAG_IMP_PRSTZ_AGG_INI);
        return buffer;
    }

    public void initWpagImpPrstzAggIniSpaces() {
        fill(Pos.WPAG_IMP_PRSTZ_AGG_INI, Len.WPAG_IMP_PRSTZ_AGG_INI, Types.SPACE_CHAR);
    }

    public void setWpagImpPrstzAggIniNull(String wpagImpPrstzAggIniNull) {
        writeString(Pos.WPAG_IMP_PRSTZ_AGG_INI_NULL, wpagImpPrstzAggIniNull, Len.WPAG_IMP_PRSTZ_AGG_INI_NULL);
    }

    /**Original name: WPAG-IMP-PRSTZ-AGG-INI-NULL<br>*/
    public String getWpagImpPrstzAggIniNull() {
        return readString(Pos.WPAG_IMP_PRSTZ_AGG_INI_NULL, Len.WPAG_IMP_PRSTZ_AGG_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PRSTZ_AGG_INI = 1;
        public static final int WPAG_IMP_PRSTZ_AGG_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PRSTZ_AGG_INI = 8;
        public static final int WPAG_IMP_PRSTZ_AGG_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PRSTZ_AGG_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PRSTZ_AGG_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
