package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-TS-RIVAL-FIS<br>
 * Variable: L3421-TS-RIVAL-FIS from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421TsRivalFis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421TsRivalFis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_TS_RIVAL_FIS;
    }

    public void setL3421TsRivalFisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_TS_RIVAL_FIS, Pos.L3421_TS_RIVAL_FIS);
    }

    /**Original name: L3421-TS-RIVAL-FIS<br>*/
    public AfDecimal getL3421TsRivalFis() {
        return readPackedAsDecimal(Pos.L3421_TS_RIVAL_FIS, Len.Int.L3421_TS_RIVAL_FIS, Len.Fract.L3421_TS_RIVAL_FIS);
    }

    public byte[] getL3421TsRivalFisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_TS_RIVAL_FIS, Pos.L3421_TS_RIVAL_FIS);
        return buffer;
    }

    /**Original name: L3421-TS-RIVAL-FIS-NULL<br>*/
    public String getL3421TsRivalFisNull() {
        return readString(Pos.L3421_TS_RIVAL_FIS_NULL, Len.L3421_TS_RIVAL_FIS_NULL);
    }

    public String getL3421TsRivalFisNullFormatted() {
        return Functions.padBlanks(getL3421TsRivalFisNull(), Len.L3421_TS_RIVAL_FIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_TS_RIVAL_FIS = 1;
        public static final int L3421_TS_RIVAL_FIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_TS_RIVAL_FIS = 8;
        public static final int L3421_TS_RIVAL_FIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_TS_RIVAL_FIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_TS_RIVAL_FIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
