package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDCO-IMP-ARROT-PRE<br>
 * Variable: WDCO-IMP-ARROT-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoImpArrotPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoImpArrotPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_IMP_ARROT_PRE;
    }

    public void setWdcoImpArrotPre(AfDecimal wdcoImpArrotPre) {
        writeDecimalAsPacked(Pos.WDCO_IMP_ARROT_PRE, wdcoImpArrotPre.copy());
    }

    public void setWdcoImpArrotPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_IMP_ARROT_PRE, Pos.WDCO_IMP_ARROT_PRE);
    }

    /**Original name: WDCO-IMP-ARROT-PRE<br>*/
    public AfDecimal getWdcoImpArrotPre() {
        return readPackedAsDecimal(Pos.WDCO_IMP_ARROT_PRE, Len.Int.WDCO_IMP_ARROT_PRE, Len.Fract.WDCO_IMP_ARROT_PRE);
    }

    public byte[] getWdcoImpArrotPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_IMP_ARROT_PRE, Pos.WDCO_IMP_ARROT_PRE);
        return buffer;
    }

    public void setWdcoImpArrotPreNull(String wdcoImpArrotPreNull) {
        writeString(Pos.WDCO_IMP_ARROT_PRE_NULL, wdcoImpArrotPreNull, Len.WDCO_IMP_ARROT_PRE_NULL);
    }

    /**Original name: WDCO-IMP-ARROT-PRE-NULL<br>*/
    public String getWdcoImpArrotPreNull() {
        return readString(Pos.WDCO_IMP_ARROT_PRE_NULL, Len.WDCO_IMP_ARROT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_IMP_ARROT_PRE = 1;
        public static final int WDCO_IMP_ARROT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_IMP_ARROT_PRE = 8;
        public static final int WDCO_IMP_ARROT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDCO_IMP_ARROT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_IMP_ARROT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
