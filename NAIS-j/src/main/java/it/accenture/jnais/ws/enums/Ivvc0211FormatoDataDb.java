package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-FORMATO-DATA-DB<br>
 * Variable: IVVC0211-FORMATO-DATA-DB from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211FormatoDataDb {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FORMATO_DATA_DB);
    public static final String ISO = "ISO";
    public static final String EUR = "EUR";

    //==== METHODS ====
    public void setFormatoDataDb(String formatoDataDb) {
        this.value = Functions.subString(formatoDataDb, Len.FORMATO_DATA_DB);
    }

    public String getFormatoDataDb() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORMATO_DATA_DB = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
