package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-ERRORE-TROVATO<br>
 * Variable: SW-ERRORE-TROVATO from program IEAS9900<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwErroreTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setSwErroreTrovato(char swErroreTrovato) {
        this.value = swErroreTrovato;
    }

    public char getSwErroreTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
