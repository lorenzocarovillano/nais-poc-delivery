package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-INTR-TECN<br>
 * Variable: B03-INTR-TECN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03IntrTecn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03IntrTecn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_INTR_TECN;
    }

    public void setB03IntrTecn(AfDecimal b03IntrTecn) {
        writeDecimalAsPacked(Pos.B03_INTR_TECN, b03IntrTecn.copy());
    }

    public void setB03IntrTecnFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_INTR_TECN, Pos.B03_INTR_TECN);
    }

    /**Original name: B03-INTR-TECN<br>*/
    public AfDecimal getB03IntrTecn() {
        return readPackedAsDecimal(Pos.B03_INTR_TECN, Len.Int.B03_INTR_TECN, Len.Fract.B03_INTR_TECN);
    }

    public byte[] getB03IntrTecnAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_INTR_TECN, Pos.B03_INTR_TECN);
        return buffer;
    }

    public void setB03IntrTecnNull(String b03IntrTecnNull) {
        writeString(Pos.B03_INTR_TECN_NULL, b03IntrTecnNull, Len.B03_INTR_TECN_NULL);
    }

    /**Original name: B03-INTR-TECN-NULL<br>*/
    public String getB03IntrTecnNull() {
        return readString(Pos.B03_INTR_TECN_NULL, Len.B03_INTR_TECN_NULL);
    }

    public String getB03IntrTecnNullFormatted() {
        return Functions.padBlanks(getB03IntrTecnNull(), Len.B03_INTR_TECN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_INTR_TECN = 1;
        public static final int B03_INTR_TECN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_INTR_TECN = 4;
        public static final int B03_INTR_TECN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_INTR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_INTR_TECN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
