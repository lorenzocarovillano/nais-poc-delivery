package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-MIN-TRNUT<br>
 * Variable: P85-MIN-TRNUT from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85MinTrnut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85MinTrnut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_MIN_TRNUT;
    }

    public void setP85MinTrnut(AfDecimal p85MinTrnut) {
        writeDecimalAsPacked(Pos.P85_MIN_TRNUT, p85MinTrnut.copy());
    }

    public void setP85MinTrnutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_MIN_TRNUT, Pos.P85_MIN_TRNUT);
    }

    /**Original name: P85-MIN-TRNUT<br>*/
    public AfDecimal getP85MinTrnut() {
        return readPackedAsDecimal(Pos.P85_MIN_TRNUT, Len.Int.P85_MIN_TRNUT, Len.Fract.P85_MIN_TRNUT);
    }

    public byte[] getP85MinTrnutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_MIN_TRNUT, Pos.P85_MIN_TRNUT);
        return buffer;
    }

    public void setP85MinTrnutNull(String p85MinTrnutNull) {
        writeString(Pos.P85_MIN_TRNUT_NULL, p85MinTrnutNull, Len.P85_MIN_TRNUT_NULL);
    }

    /**Original name: P85-MIN-TRNUT-NULL<br>*/
    public String getP85MinTrnutNull() {
        return readString(Pos.P85_MIN_TRNUT_NULL, Len.P85_MIN_TRNUT_NULL);
    }

    public String getP85MinTrnutNullFormatted() {
        return Functions.padBlanks(getP85MinTrnutNull(), Len.P85_MIN_TRNUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_MIN_TRNUT = 1;
        public static final int P85_MIN_TRNUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_MIN_TRNUT = 8;
        public static final int P85_MIN_TRNUT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_MIN_TRNUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_MIN_TRNUT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
