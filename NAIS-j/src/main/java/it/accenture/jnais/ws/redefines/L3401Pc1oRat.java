package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-PC-1O-RAT<br>
 * Variable: L3401-PC-1O-RAT from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401Pc1oRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401Pc1oRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_PC1O_RAT;
    }

    public void setL3401Pc1oRat(AfDecimal l3401Pc1oRat) {
        writeDecimalAsPacked(Pos.L3401_PC1O_RAT, l3401Pc1oRat.copy());
    }

    public void setL3401Pc1oRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_PC1O_RAT, Pos.L3401_PC1O_RAT);
    }

    /**Original name: L3401-PC-1O-RAT<br>*/
    public AfDecimal getL3401Pc1oRat() {
        return readPackedAsDecimal(Pos.L3401_PC1O_RAT, Len.Int.L3401_PC1O_RAT, Len.Fract.L3401_PC1O_RAT);
    }

    public byte[] getL3401Pc1oRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_PC1O_RAT, Pos.L3401_PC1O_RAT);
        return buffer;
    }

    /**Original name: L3401-PC-1O-RAT-NULL<br>*/
    public String getL3401Pc1oRatNull() {
        return readString(Pos.L3401_PC1O_RAT_NULL, Len.L3401_PC1O_RAT_NULL);
    }

    public String getL3401Pc1oRatNullFormatted() {
        return Functions.padBlanks(getL3401Pc1oRatNull(), Len.L3401_PC1O_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_PC1O_RAT = 1;
        public static final int L3401_PC1O_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_PC1O_RAT = 4;
        public static final int L3401_PC1O_RAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3401_PC1O_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_PC1O_RAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
