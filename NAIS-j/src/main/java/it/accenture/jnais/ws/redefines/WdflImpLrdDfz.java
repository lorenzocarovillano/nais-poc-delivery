package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-LRD-DFZ<br>
 * Variable: WDFL-IMP-LRD-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpLrdDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpLrdDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_LRD_DFZ;
    }

    public void setWdflImpLrdDfz(AfDecimal wdflImpLrdDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMP_LRD_DFZ, wdflImpLrdDfz.copy());
    }

    public void setWdflImpLrdDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_LRD_DFZ, Pos.WDFL_IMP_LRD_DFZ);
    }

    /**Original name: WDFL-IMP-LRD-DFZ<br>*/
    public AfDecimal getWdflImpLrdDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMP_LRD_DFZ, Len.Int.WDFL_IMP_LRD_DFZ, Len.Fract.WDFL_IMP_LRD_DFZ);
    }

    public byte[] getWdflImpLrdDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_LRD_DFZ, Pos.WDFL_IMP_LRD_DFZ);
        return buffer;
    }

    public void setWdflImpLrdDfzNull(String wdflImpLrdDfzNull) {
        writeString(Pos.WDFL_IMP_LRD_DFZ_NULL, wdflImpLrdDfzNull, Len.WDFL_IMP_LRD_DFZ_NULL);
    }

    /**Original name: WDFL-IMP-LRD-DFZ-NULL<br>*/
    public String getWdflImpLrdDfzNull() {
        return readString(Pos.WDFL_IMP_LRD_DFZ_NULL, Len.WDFL_IMP_LRD_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_LRD_DFZ = 1;
        public static final int WDFL_IMP_LRD_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_LRD_DFZ = 8;
        public static final int WDFL_IMP_LRD_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_LRD_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_LRD_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
