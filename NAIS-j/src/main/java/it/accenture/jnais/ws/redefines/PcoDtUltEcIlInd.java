package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-IL-IND<br>
 * Variable: PCO-DT-ULT-EC-IL-IND from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcIlInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcIlInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_IL_IND;
    }

    public void setPcoDtUltEcIlInd(int pcoDtUltEcIlInd) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_IL_IND, pcoDtUltEcIlInd, Len.Int.PCO_DT_ULT_EC_IL_IND);
    }

    public void setPcoDtUltEcIlIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_IL_IND, Pos.PCO_DT_ULT_EC_IL_IND);
    }

    /**Original name: PCO-DT-ULT-EC-IL-IND<br>*/
    public int getPcoDtUltEcIlInd() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_IL_IND, Len.Int.PCO_DT_ULT_EC_IL_IND);
    }

    public byte[] getPcoDtUltEcIlIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_IL_IND, Pos.PCO_DT_ULT_EC_IL_IND);
        return buffer;
    }

    public void initPcoDtUltEcIlIndHighValues() {
        fill(Pos.PCO_DT_ULT_EC_IL_IND, Len.PCO_DT_ULT_EC_IL_IND, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcIlIndNull(String pcoDtUltEcIlIndNull) {
        writeString(Pos.PCO_DT_ULT_EC_IL_IND_NULL, pcoDtUltEcIlIndNull, Len.PCO_DT_ULT_EC_IL_IND_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-IL-IND-NULL<br>*/
    public String getPcoDtUltEcIlIndNull() {
        return readString(Pos.PCO_DT_ULT_EC_IL_IND_NULL, Len.PCO_DT_ULT_EC_IL_IND_NULL);
    }

    public String getPcoDtUltEcIlIndNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcIlIndNull(), Len.PCO_DT_ULT_EC_IL_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_IL_IND = 1;
        public static final int PCO_DT_ULT_EC_IL_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_IL_IND = 5;
        public static final int PCO_DT_ULT_EC_IL_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_IL_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
