package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDAD-DUR-AA-ADES-DFLT<br>
 * Variable: WDAD-DUR-AA-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdadDurAaAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdadDurAaAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDAD_DUR_AA_ADES_DFLT;
    }

    public void setWdadDurAaAdesDflt(int wdadDurAaAdesDflt) {
        writeIntAsPacked(Pos.WDAD_DUR_AA_ADES_DFLT, wdadDurAaAdesDflt, Len.Int.WDAD_DUR_AA_ADES_DFLT);
    }

    public void setWdadDurAaAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDAD_DUR_AA_ADES_DFLT, Pos.WDAD_DUR_AA_ADES_DFLT);
    }

    /**Original name: WDAD-DUR-AA-ADES-DFLT<br>*/
    public int getWdadDurAaAdesDflt() {
        return readPackedAsInt(Pos.WDAD_DUR_AA_ADES_DFLT, Len.Int.WDAD_DUR_AA_ADES_DFLT);
    }

    public byte[] getWdadDurAaAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDAD_DUR_AA_ADES_DFLT, Pos.WDAD_DUR_AA_ADES_DFLT);
        return buffer;
    }

    public void setWdadDurAaAdesDfltNull(String wdadDurAaAdesDfltNull) {
        writeString(Pos.WDAD_DUR_AA_ADES_DFLT_NULL, wdadDurAaAdesDfltNull, Len.WDAD_DUR_AA_ADES_DFLT_NULL);
    }

    /**Original name: WDAD-DUR-AA-ADES-DFLT-NULL<br>*/
    public String getWdadDurAaAdesDfltNull() {
        return readString(Pos.WDAD_DUR_AA_ADES_DFLT_NULL, Len.WDAD_DUR_AA_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDAD_DUR_AA_ADES_DFLT = 1;
        public static final int WDAD_DUR_AA_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDAD_DUR_AA_ADES_DFLT = 3;
        public static final int WDAD_DUR_AA_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDAD_DUR_AA_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
