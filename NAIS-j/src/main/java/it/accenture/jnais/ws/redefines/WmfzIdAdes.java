package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WMFZ-ID-ADES<br>
 * Variable: WMFZ-ID-ADES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzIdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzIdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_ID_ADES;
    }

    public void setWmfzIdAdes(int wmfzIdAdes) {
        writeIntAsPacked(Pos.WMFZ_ID_ADES, wmfzIdAdes, Len.Int.WMFZ_ID_ADES);
    }

    public void setWmfzIdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_ID_ADES, Pos.WMFZ_ID_ADES);
    }

    /**Original name: WMFZ-ID-ADES<br>*/
    public int getWmfzIdAdes() {
        return readPackedAsInt(Pos.WMFZ_ID_ADES, Len.Int.WMFZ_ID_ADES);
    }

    public byte[] getWmfzIdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_ID_ADES, Pos.WMFZ_ID_ADES);
        return buffer;
    }

    public void initWmfzIdAdesSpaces() {
        fill(Pos.WMFZ_ID_ADES, Len.WMFZ_ID_ADES, Types.SPACE_CHAR);
    }

    public void setWmfzIdAdesNull(String wmfzIdAdesNull) {
        writeString(Pos.WMFZ_ID_ADES_NULL, wmfzIdAdesNull, Len.WMFZ_ID_ADES_NULL);
    }

    /**Original name: WMFZ-ID-ADES-NULL<br>*/
    public String getWmfzIdAdesNull() {
        return readString(Pos.WMFZ_ID_ADES_NULL, Len.WMFZ_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_ADES = 1;
        public static final int WMFZ_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_ID_ADES = 5;
        public static final int WMFZ_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
