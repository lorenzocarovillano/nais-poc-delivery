package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ispv0000;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Ldbv1421;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpStatBus;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2760<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2760Data {

    //==== PROPERTIES ====
    public static final int IWFI0051_CAMPO_INPUT_MAXOCCURS = 20;
    public static final int DRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2760";
    //Original name: LDBS5910
    private String ldbs5910 = "LDBS5910";
    //Original name: LDBS1420
    private String ldbs1420 = "LDBS1420";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = DefaultValues.stringVal(Len.WK_CALL_PGM);
    //Original name: IWFI0051-CAMPO-INPUT
    private char[] iwfi0051CampoInput = new char[IWFI0051_CAMPO_INPUT_MAXOCCURS];
    //Original name: RAPP-ANA
    private RappAnaLdbs1240 rappAna = new RappAnaLdbs1240();
    //Original name: GAR
    private Gar gar = new Gar();
    //Original name: POLI
    private PoliIdbspol0 poli = new PoliIdbspol0();
    //Original name: LDBV1421
    private Ldbv1421 ldbv1421 = new Ldbv1421();
    //Original name: STAT-OGG-BUS
    private StatOggBusIdbsstb0 statOggBus = new StatOggBusIdbsstb0();
    //Original name: ISPV0000
    private Ispv0000 ispv0000 = new Ispv0000();
    //Original name: DRAN-ELE-RAN-MAX
    private short dranEleRanMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] dranTabRappAnag = new WranTabRappAnag[DRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    /**Original name: WS-TP-OGG<br>
	 * <pre>--
	 * *****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-TP-STAT-BUS<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
    private WsTpStatBus wsTpStatBus = new WsTpStatBus();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK-VARIABILI
    private WkVariabiliLvvs2760 wkVariabili = new WkVariabiliLvvs2760();

    //==== CONSTRUCTORS ====
    public Lvvs2760Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int iwfi0051CampoInputIdx = 1; iwfi0051CampoInputIdx <= IWFI0051_CAMPO_INPUT_MAXOCCURS; iwfi0051CampoInputIdx++) {
            setIwfi0051CampoInput(iwfi0051CampoInputIdx, DefaultValues.CHAR_VAL);
        }
        for (int dranTabRappAnagIdx = 1; dranTabRappAnagIdx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; dranTabRappAnagIdx++) {
            dranTabRappAnag[dranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs5910() {
        return this.ldbs5910;
    }

    public String getLdbs1420() {
        return this.ldbs1420;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setIwfi0051CampoInput(int iwfi0051CampoInputIdx, char iwfi0051CampoInput) {
        this.iwfi0051CampoInput[iwfi0051CampoInputIdx - 1] = iwfi0051CampoInput;
    }

    public char getIwfi0051CampoInput(int iwfi0051CampoInputIdx) {
        return this.iwfi0051CampoInput[iwfi0051CampoInputIdx - 1];
    }

    public void setDranAreaRappAnaFormatted(String data) {
        byte[] buffer = new byte[Len.DRAN_AREA_RAPP_ANA];
        MarshalByte.writeString(buffer, 1, data, Len.DRAN_AREA_RAPP_ANA);
        setDranAreaRappAnaBytes(buffer, 1);
    }

    public void setDranAreaRappAnaBytes(byte[] buffer, int offset) {
        int position = offset;
        dranEleRanMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                dranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public void setDranEleRanMax(short dranEleRanMax) {
        this.dranEleRanMax = dranEleRanMax;
    }

    public short getDranEleRanMax() {
        return this.dranEleRanMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public WranTabRappAnag getDranTabRappAnag(int idx) {
        return dranTabRappAnag[idx - 1];
    }

    public Gar getGar() {
        return gar;
    }

    public Ispv0000 getIspv0000() {
        return ispv0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv1421 getLdbv1421() {
        return ldbv1421;
    }

    public PoliIdbspol0 getPoli() {
        return poli;
    }

    public RappAnaLdbs1240 getRappAna() {
        return rappAna;
    }

    public StatOggBusIdbsstb0 getStatOggBus() {
        return statOggBus;
    }

    public WkVariabiliLvvs2760 getWkVariabili() {
        return wkVariabili;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpStatBus getWsTpStatBus() {
        return wsTpStatBus;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_CALL_PGM = 8;
        public static final int DRAN_ELE_RAN_MAX = 2;
        public static final int DRAN_AREA_RAPP_ANA = DRAN_ELE_RAN_MAX + Lvvs2760Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
