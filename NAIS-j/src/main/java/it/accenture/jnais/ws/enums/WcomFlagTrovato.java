package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-FLAG-TROVATO<br>
 * Variable: WCOM-FLAG-TROVATO from program LVVS0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomFlagTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_TROVATO = 'S';
    public static final char NO_TROVATO = 'N';

    //==== METHODS ====
    public void setWcomFlagTrovato(char wcomFlagTrovato) {
        this.value = wcomFlagTrovato;
    }

    public char getWcomFlagTrovato() {
        return this.value;
    }

    public boolean isSiTrovato() {
        return value == SI_TROVATO;
    }

    public void setSiTrovato() {
        value = SI_TROVATO;
    }

    public boolean isNoTrovato() {
        return value == NO_TROVATO;
    }

    public void setNoTrovato() {
        value = NO_TROVATO;
    }
}
