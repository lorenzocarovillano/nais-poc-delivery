package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-TASSE<br>
 * Variable: WPAG-CONT-TASSE from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContTasse extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContTasse() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_TASSE;
    }

    public void setWpagContTasse(AfDecimal wpagContTasse) {
        writeDecimalAsPacked(Pos.WPAG_CONT_TASSE, wpagContTasse.copy());
    }

    public void setWpagContTasseFormatted(String wpagContTasse) {
        setWpagContTasse(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_TASSE + Len.Fract.WPAG_CONT_TASSE, Len.Fract.WPAG_CONT_TASSE, wpagContTasse));
    }

    public void setWpagContTasseFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_TASSE, Pos.WPAG_CONT_TASSE);
    }

    /**Original name: WPAG-CONT-TASSE<br>*/
    public AfDecimal getWpagContTasse() {
        return readPackedAsDecimal(Pos.WPAG_CONT_TASSE, Len.Int.WPAG_CONT_TASSE, Len.Fract.WPAG_CONT_TASSE);
    }

    public byte[] getWpagContTasseAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_TASSE, Pos.WPAG_CONT_TASSE);
        return buffer;
    }

    public void initWpagContTasseSpaces() {
        fill(Pos.WPAG_CONT_TASSE, Len.WPAG_CONT_TASSE, Types.SPACE_CHAR);
    }

    public void setWpagContTasseNull(String wpagContTasseNull) {
        writeString(Pos.WPAG_CONT_TASSE_NULL, wpagContTasseNull, Len.WPAG_CONT_TASSE_NULL);
    }

    /**Original name: WPAG-CONT-TASSE-NULL<br>*/
    public String getWpagContTasseNull() {
        return readString(Pos.WPAG_CONT_TASSE_NULL, Len.WPAG_CONT_TASSE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_TASSE = 1;
        public static final int WPAG_CONT_TASSE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_TASSE = 8;
        public static final int WPAG_CONT_TASSE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_TASSE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_TASSE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
