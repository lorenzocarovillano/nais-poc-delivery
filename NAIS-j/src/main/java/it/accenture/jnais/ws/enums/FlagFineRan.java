package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-RAN<br>
 * Variable: FLAG-FINE-RAN from program LVVS2760<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineRan {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagFineRan(char flagFineRan) {
        this.value = flagFineRan;
    }

    public char getFlagFineRan() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
