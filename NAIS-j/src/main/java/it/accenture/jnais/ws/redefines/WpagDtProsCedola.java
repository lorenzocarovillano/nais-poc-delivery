package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-DT-PROS-CEDOLA<br>
 * Variable: WPAG-DT-PROS-CEDOLA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagDtProsCedola extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagDtProsCedola() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_DT_PROS_CEDOLA;
    }

    public void setWpagDtProsCedola(int wpagDtProsCedola) {
        writeIntAsPacked(Pos.WPAG_DT_PROS_CEDOLA, wpagDtProsCedola, Len.Int.WPAG_DT_PROS_CEDOLA);
    }

    public void setWpagDtProsCedolaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_DT_PROS_CEDOLA, Pos.WPAG_DT_PROS_CEDOLA);
    }

    /**Original name: WPAG-DT-PROS-CEDOLA<br>*/
    public int getWpagDtProsCedola() {
        return readPackedAsInt(Pos.WPAG_DT_PROS_CEDOLA, Len.Int.WPAG_DT_PROS_CEDOLA);
    }

    public byte[] getWpagDtProsCedolaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_DT_PROS_CEDOLA, Pos.WPAG_DT_PROS_CEDOLA);
        return buffer;
    }

    public void initWpagDtProsCedolaSpaces() {
        fill(Pos.WPAG_DT_PROS_CEDOLA, Len.WPAG_DT_PROS_CEDOLA, Types.SPACE_CHAR);
    }

    public void setWpagDtProsCedolaNull(String wpagDtProsCedolaNull) {
        writeString(Pos.WPAG_DT_PROS_CEDOLA_NULL, wpagDtProsCedolaNull, Len.WPAG_DT_PROS_CEDOLA_NULL);
    }

    /**Original name: WPAG-DT-PROS-CEDOLA-NULL<br>*/
    public String getWpagDtProsCedolaNull() {
        return readString(Pos.WPAG_DT_PROS_CEDOLA_NULL, Len.WPAG_DT_PROS_CEDOLA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_DT_PROS_CEDOLA = 1;
        public static final int WPAG_DT_PROS_CEDOLA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DT_PROS_CEDOLA = 5;
        public static final int WPAG_DT_PROS_CEDOLA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_DT_PROS_CEDOLA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
