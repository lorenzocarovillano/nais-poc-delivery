package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-FIND-INTERN<br>
 * Variable: WK-FIND-INTERN from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkFindIntern {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkFindIntern(char wkFindIntern) {
        this.value = wkFindIntern;
    }

    public char getWkFindIntern() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
