package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvgrl1;
import it.accenture.jnais.copy.WgrlDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WGRL-TAB-GAR-LIQ<br>
 * Variables: WGRL-TAB-GAR-LIQ from copybook LCCVGRLA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WgrlTabGarLiq {

    //==== PROPERTIES ====
    //Original name: LCCVGRL1
    private Lccvgrl1 lccvgrl1 = new Lccvgrl1();

    //==== METHODS ====
    public void setWgrlTabGarLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvgrl1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvgrl1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvgrl1.Len.Int.ID_PTF, 0));
        position += Lccvgrl1.Len.ID_PTF;
        lccvgrl1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWgrlTabGarLiqBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvgrl1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvgrl1.getIdPtf(), Lccvgrl1.Len.Int.ID_PTF, 0);
        position += Lccvgrl1.Len.ID_PTF;
        lccvgrl1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWgrlTabGarLiqSpaces() {
        lccvgrl1.initLccvgrl1Spaces();
    }

    public Lccvgrl1 getLccvgrl1() {
        return lccvgrl1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRL_TAB_GAR_LIQ = WpolStatus.Len.STATUS + Lccvgrl1.Len.ID_PTF + WgrlDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
