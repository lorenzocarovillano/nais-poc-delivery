package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: B03-MET-RISC-SPCL<br>
 * Variable: B03-MET-RISC-SPCL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03MetRiscSpcl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03MetRiscSpcl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_MET_RISC_SPCL;
    }

    public void setB03MetRiscSpcl(short b03MetRiscSpcl) {
        writeShortAsPacked(Pos.B03_MET_RISC_SPCL, b03MetRiscSpcl, Len.Int.B03_MET_RISC_SPCL);
    }

    public void setB03MetRiscSpclFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_MET_RISC_SPCL, Pos.B03_MET_RISC_SPCL);
    }

    /**Original name: B03-MET-RISC-SPCL<br>*/
    public short getB03MetRiscSpcl() {
        return readPackedAsShort(Pos.B03_MET_RISC_SPCL, Len.Int.B03_MET_RISC_SPCL);
    }

    public byte[] getB03MetRiscSpclAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_MET_RISC_SPCL, Pos.B03_MET_RISC_SPCL);
        return buffer;
    }

    public void setB03MetRiscSpclNull(char b03MetRiscSpclNull) {
        writeChar(Pos.B03_MET_RISC_SPCL_NULL, b03MetRiscSpclNull);
    }

    /**Original name: B03-MET-RISC-SPCL-NULL<br>*/
    public char getB03MetRiscSpclNull() {
        return readChar(Pos.B03_MET_RISC_SPCL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_MET_RISC_SPCL = 1;
        public static final int B03_MET_RISC_SPCL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_MET_RISC_SPCL = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_MET_RISC_SPCL = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
