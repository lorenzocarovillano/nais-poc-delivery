package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPRE-RIMB-EFF<br>
 * Variable: WPRE-RIMB-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreRimbEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreRimbEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_RIMB_EFF;
    }

    public void setWpreRimbEff(AfDecimal wpreRimbEff) {
        writeDecimalAsPacked(Pos.WPRE_RIMB_EFF, wpreRimbEff.copy());
    }

    public void setWpreRimbEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_RIMB_EFF, Pos.WPRE_RIMB_EFF);
    }

    /**Original name: WPRE-RIMB-EFF<br>*/
    public AfDecimal getWpreRimbEff() {
        return readPackedAsDecimal(Pos.WPRE_RIMB_EFF, Len.Int.WPRE_RIMB_EFF, Len.Fract.WPRE_RIMB_EFF);
    }

    public byte[] getWpreRimbEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_RIMB_EFF, Pos.WPRE_RIMB_EFF);
        return buffer;
    }

    public void initWpreRimbEffSpaces() {
        fill(Pos.WPRE_RIMB_EFF, Len.WPRE_RIMB_EFF, Types.SPACE_CHAR);
    }

    public void setWpreRimbEffNull(String wpreRimbEffNull) {
        writeString(Pos.WPRE_RIMB_EFF_NULL, wpreRimbEffNull, Len.WPRE_RIMB_EFF_NULL);
    }

    /**Original name: WPRE-RIMB-EFF-NULL<br>*/
    public String getWpreRimbEffNull() {
        return readString(Pos.WPRE_RIMB_EFF_NULL, Len.WPRE_RIMB_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_RIMB_EFF = 1;
        public static final int WPRE_RIMB_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_RIMB_EFF = 8;
        public static final int WPRE_RIMB_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPRE_RIMB_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_RIMB_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
