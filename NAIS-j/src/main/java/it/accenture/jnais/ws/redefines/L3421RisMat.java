package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-RIS-MAT<br>
 * Variable: L3421-RIS-MAT from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421RisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421RisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_RIS_MAT;
    }

    public void setL3421RisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_RIS_MAT, Pos.L3421_RIS_MAT);
    }

    /**Original name: L3421-RIS-MAT<br>*/
    public AfDecimal getL3421RisMat() {
        return readPackedAsDecimal(Pos.L3421_RIS_MAT, Len.Int.L3421_RIS_MAT, Len.Fract.L3421_RIS_MAT);
    }

    public byte[] getL3421RisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_RIS_MAT, Pos.L3421_RIS_MAT);
        return buffer;
    }

    /**Original name: L3421-RIS-MAT-NULL<br>*/
    public String getL3421RisMatNull() {
        return readString(Pos.L3421_RIS_MAT_NULL, Len.L3421_RIS_MAT_NULL);
    }

    public String getL3421RisMatNullFormatted() {
        return Functions.padBlanks(getL3421RisMatNull(), Len.L3421_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_RIS_MAT = 1;
        public static final int L3421_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_RIS_MAT = 8;
        public static final int L3421_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
