package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.enums.WsTpTrch;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0020<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0020Data {

    //==== PROPERTIES ====
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 1250;
    /**Original name: WS-TP-TRCH<br>
	 * <pre>*****************************************************************
	 *     TP_TRCH
	 * *****************************************************************</pre>*/
    private WsTpTrch wsTpTrch = new WsTpTrch();
    //Original name: DTGA-ELE-TRCH-MAX
    private short dtgaEleTrchMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> dtgaTabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, DTGA_TAB_TRAN_MAXOCCURS);
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short ixTabRan = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setDtgaAreaTrchFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TRCH];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TRCH);
        setDtgaAreaTrchBytes(buffer, 1);
    }

    public void setDtgaAreaTrchBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTrchMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTGA_TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtgaTabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_dtgaTabTran = new W1tgaTabTran();
                temp_dtgaTabTran.initW1tgaTabTranSpaces();
                getDtgaTabTranObj().fill(temp_dtgaTabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (DTGA_TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public void setDtgaEleTrchMax(short dtgaEleTrchMax) {
        this.dtgaEleTrchMax = dtgaEleTrchMax;
    }

    public short getDtgaEleTrchMax() {
        return this.dtgaEleTrchMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabRan(short ixTabRan) {
        this.ixTabRan = ixTabRan;
    }

    public short getIxTabRan() {
        return this.ixTabRan;
    }

    public W1tgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran.get(idx - 1);
    }

    public LazyArrayCopy<W1tgaTabTran> getDtgaTabTranObj() {
        return dtgaTabTran;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public WsTpTrch getWsTpTrch() {
        return wsTpTrch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DTGA_ELE_TRCH_MAX = 2;
        public static final int DTGA_AREA_TRCH = DTGA_ELE_TRCH_MAX + Lvvs0020Data.DTGA_TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
