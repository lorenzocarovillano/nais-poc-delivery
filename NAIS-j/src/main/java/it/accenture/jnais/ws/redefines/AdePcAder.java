package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PC-ADER<br>
 * Variable: ADE-PC-ADER from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePcAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePcAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PC_ADER;
    }

    public void setAdePcAder(AfDecimal adePcAder) {
        writeDecimalAsPacked(Pos.ADE_PC_ADER, adePcAder.copy());
    }

    public void setAdePcAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PC_ADER, Pos.ADE_PC_ADER);
    }

    /**Original name: ADE-PC-ADER<br>*/
    public AfDecimal getAdePcAder() {
        return readPackedAsDecimal(Pos.ADE_PC_ADER, Len.Int.ADE_PC_ADER, Len.Fract.ADE_PC_ADER);
    }

    public byte[] getAdePcAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PC_ADER, Pos.ADE_PC_ADER);
        return buffer;
    }

    public void setAdePcAderNull(String adePcAderNull) {
        writeString(Pos.ADE_PC_ADER_NULL, adePcAderNull, Len.ADE_PC_ADER_NULL);
    }

    /**Original name: ADE-PC-ADER-NULL<br>*/
    public String getAdePcAderNull() {
        return readString(Pos.ADE_PC_ADER_NULL, Len.ADE_PC_ADER_NULL);
    }

    public String getAdePcAderNullFormatted() {
        return Functions.padBlanks(getAdePcAderNull(), Len.ADE_PC_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PC_ADER = 1;
        public static final int ADE_PC_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PC_ADER = 4;
        public static final int ADE_PC_ADER_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PC_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
