package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-PROV-ACQ-1AA<br>
 * Variable: DTR-PROV-ACQ-1AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_PROV_ACQ1AA;
    }

    public void setDtrProvAcq1aa(AfDecimal dtrProvAcq1aa) {
        writeDecimalAsPacked(Pos.DTR_PROV_ACQ1AA, dtrProvAcq1aa.copy());
    }

    public void setDtrProvAcq1aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_PROV_ACQ1AA, Pos.DTR_PROV_ACQ1AA);
    }

    /**Original name: DTR-PROV-ACQ-1AA<br>*/
    public AfDecimal getDtrProvAcq1aa() {
        return readPackedAsDecimal(Pos.DTR_PROV_ACQ1AA, Len.Int.DTR_PROV_ACQ1AA, Len.Fract.DTR_PROV_ACQ1AA);
    }

    public byte[] getDtrProvAcq1aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_PROV_ACQ1AA, Pos.DTR_PROV_ACQ1AA);
        return buffer;
    }

    public void setDtrProvAcq1aaNull(String dtrProvAcq1aaNull) {
        writeString(Pos.DTR_PROV_ACQ1AA_NULL, dtrProvAcq1aaNull, Len.DTR_PROV_ACQ1AA_NULL);
    }

    /**Original name: DTR-PROV-ACQ-1AA-NULL<br>*/
    public String getDtrProvAcq1aaNull() {
        return readString(Pos.DTR_PROV_ACQ1AA_NULL, Len.DTR_PROV_ACQ1AA_NULL);
    }

    public String getDtrProvAcq1aaNullFormatted() {
        return Functions.padBlanks(getDtrProvAcq1aaNull(), Len.DTR_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_PROV_ACQ1AA = 1;
        public static final int DTR_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_PROV_ACQ1AA = 8;
        public static final int DTR_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
