package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PROV-ACQ-2AA<br>
 * Variable: TDR-TOT-PROV-ACQ-2AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotProvAcq2aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotProvAcq2aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PROV_ACQ2AA;
    }

    public void setTdrTotProvAcq2aa(AfDecimal tdrTotProvAcq2aa) {
        writeDecimalAsPacked(Pos.TDR_TOT_PROV_ACQ2AA, tdrTotProvAcq2aa.copy());
    }

    public void setTdrTotProvAcq2aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PROV_ACQ2AA, Pos.TDR_TOT_PROV_ACQ2AA);
    }

    /**Original name: TDR-TOT-PROV-ACQ-2AA<br>*/
    public AfDecimal getTdrTotProvAcq2aa() {
        return readPackedAsDecimal(Pos.TDR_TOT_PROV_ACQ2AA, Len.Int.TDR_TOT_PROV_ACQ2AA, Len.Fract.TDR_TOT_PROV_ACQ2AA);
    }

    public byte[] getTdrTotProvAcq2aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PROV_ACQ2AA, Pos.TDR_TOT_PROV_ACQ2AA);
        return buffer;
    }

    public void setTdrTotProvAcq2aaNull(String tdrTotProvAcq2aaNull) {
        writeString(Pos.TDR_TOT_PROV_ACQ2AA_NULL, tdrTotProvAcq2aaNull, Len.TDR_TOT_PROV_ACQ2AA_NULL);
    }

    /**Original name: TDR-TOT-PROV-ACQ-2AA-NULL<br>*/
    public String getTdrTotProvAcq2aaNull() {
        return readString(Pos.TDR_TOT_PROV_ACQ2AA_NULL, Len.TDR_TOT_PROV_ACQ2AA_NULL);
    }

    public String getTdrTotProvAcq2aaNullFormatted() {
        return Functions.padBlanks(getTdrTotProvAcq2aaNull(), Len.TDR_TOT_PROV_ACQ2AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_ACQ2AA = 1;
        public static final int TDR_TOT_PROV_ACQ2AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_ACQ2AA = 8;
        public static final int TDR_TOT_PROV_ACQ2AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_ACQ2AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_ACQ2AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
