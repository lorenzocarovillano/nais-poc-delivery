package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-DT-LIQ<br>
 * Variable: S089-DT-LIQ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089DtLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089DtLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_DT_LIQ;
    }

    public void setWlquDtLiq(int wlquDtLiq) {
        writeIntAsPacked(Pos.S089_DT_LIQ, wlquDtLiq, Len.Int.WLQU_DT_LIQ);
    }

    public void setWlquDtLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_DT_LIQ, Pos.S089_DT_LIQ);
    }

    /**Original name: WLQU-DT-LIQ<br>*/
    public int getWlquDtLiq() {
        return readPackedAsInt(Pos.S089_DT_LIQ, Len.Int.WLQU_DT_LIQ);
    }

    public byte[] getWlquDtLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_DT_LIQ, Pos.S089_DT_LIQ);
        return buffer;
    }

    public void initWlquDtLiqSpaces() {
        fill(Pos.S089_DT_LIQ, Len.S089_DT_LIQ, Types.SPACE_CHAR);
    }

    public void setWlquDtLiqNull(String wlquDtLiqNull) {
        writeString(Pos.S089_DT_LIQ_NULL, wlquDtLiqNull, Len.WLQU_DT_LIQ_NULL);
    }

    /**Original name: WLQU-DT-LIQ-NULL<br>*/
    public String getWlquDtLiqNull() {
        return readString(Pos.S089_DT_LIQ_NULL, Len.WLQU_DT_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_DT_LIQ = 1;
        public static final int S089_DT_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_DT_LIQ = 5;
        public static final int WLQU_DT_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_DT_LIQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
