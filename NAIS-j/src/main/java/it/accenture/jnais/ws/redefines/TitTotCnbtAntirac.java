package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-CNBT-ANTIRAC<br>
 * Variable: TIT-TOT-CNBT-ANTIRAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_CNBT_ANTIRAC;
    }

    public void setTitTotCnbtAntirac(AfDecimal titTotCnbtAntirac) {
        writeDecimalAsPacked(Pos.TIT_TOT_CNBT_ANTIRAC, titTotCnbtAntirac.copy());
    }

    public void setTitTotCnbtAntiracFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_CNBT_ANTIRAC, Pos.TIT_TOT_CNBT_ANTIRAC);
    }

    /**Original name: TIT-TOT-CNBT-ANTIRAC<br>*/
    public AfDecimal getTitTotCnbtAntirac() {
        return readPackedAsDecimal(Pos.TIT_TOT_CNBT_ANTIRAC, Len.Int.TIT_TOT_CNBT_ANTIRAC, Len.Fract.TIT_TOT_CNBT_ANTIRAC);
    }

    public byte[] getTitTotCnbtAntiracAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_CNBT_ANTIRAC, Pos.TIT_TOT_CNBT_ANTIRAC);
        return buffer;
    }

    public void setTitTotCnbtAntiracNull(String titTotCnbtAntiracNull) {
        writeString(Pos.TIT_TOT_CNBT_ANTIRAC_NULL, titTotCnbtAntiracNull, Len.TIT_TOT_CNBT_ANTIRAC_NULL);
    }

    /**Original name: TIT-TOT-CNBT-ANTIRAC-NULL<br>*/
    public String getTitTotCnbtAntiracNull() {
        return readString(Pos.TIT_TOT_CNBT_ANTIRAC_NULL, Len.TIT_TOT_CNBT_ANTIRAC_NULL);
    }

    public String getTitTotCnbtAntiracNullFormatted() {
        return Functions.padBlanks(getTitTotCnbtAntiracNull(), Len.TIT_TOT_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CNBT_ANTIRAC = 1;
        public static final int TIT_TOT_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CNBT_ANTIRAC = 8;
        public static final int TIT_TOT_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
