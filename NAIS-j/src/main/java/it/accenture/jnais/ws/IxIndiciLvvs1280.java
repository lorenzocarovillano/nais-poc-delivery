package it.accenture.jnais.ws;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LVVS1280<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLvvs1280 {

    //==== PROPERTIES ====
    //Original name: IX-DCLGEN
    private short dclgen = ((short)0);
    //Original name: IX-TAB-GRZ
    private short tabGrz = ((short)0);
    //Original name: IX-TAB-L19
    private short tabL19 = ((short)0);
    //Original name: IX-TAB-TGA
    private short tabTga = ((short)0);
    //Original name: IX-TAB-VAS
    private short tabVas = ((short)0);
    //Original name: IX-GUIDA-GRZ
    private short guidaGrz = ((short)0);
    //Original name: IX-TGA-APPO
    private short tgaAppo = ((short)0);

    //==== METHODS ====
    public void setDclgen(short dclgen) {
        this.dclgen = dclgen;
    }

    public short getDclgen() {
        return this.dclgen;
    }

    public void setTabGrz(short tabGrz) {
        this.tabGrz = tabGrz;
    }

    public short getTabGrz() {
        return this.tabGrz;
    }

    public void setTabL19(short tabL19) {
        this.tabL19 = tabL19;
    }

    public short getTabL19() {
        return this.tabL19;
    }

    public void setTabTga(short tabTga) {
        this.tabTga = tabTga;
    }

    public short getTabTga() {
        return this.tabTga;
    }

    public void setTabVas(short tabVas) {
        this.tabVas = tabVas;
    }

    public short getTabVas() {
        return this.tabVas;
    }

    public void setGuidaGrz(short guidaGrz) {
        this.guidaGrz = guidaGrz;
    }

    public short getGuidaGrz() {
        return this.guidaGrz;
    }

    public void setTgaAppo(short tgaAppo) {
        this.tgaAppo = tgaAppo;
    }

    public short getTgaAppo() {
        return this.tgaAppo;
    }
}
