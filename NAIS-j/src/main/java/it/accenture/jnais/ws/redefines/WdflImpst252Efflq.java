package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-252-EFFLQ<br>
 * Variable: WDFL-IMPST-252-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpst252Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpst252Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST252_EFFLQ;
    }

    public void setWdflImpst252Efflq(AfDecimal wdflImpst252Efflq) {
        writeDecimalAsPacked(Pos.WDFL_IMPST252_EFFLQ, wdflImpst252Efflq.copy());
    }

    public void setWdflImpst252EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST252_EFFLQ, Pos.WDFL_IMPST252_EFFLQ);
    }

    /**Original name: WDFL-IMPST-252-EFFLQ<br>*/
    public AfDecimal getWdflImpst252Efflq() {
        return readPackedAsDecimal(Pos.WDFL_IMPST252_EFFLQ, Len.Int.WDFL_IMPST252_EFFLQ, Len.Fract.WDFL_IMPST252_EFFLQ);
    }

    public byte[] getWdflImpst252EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST252_EFFLQ, Pos.WDFL_IMPST252_EFFLQ);
        return buffer;
    }

    public void setWdflImpst252EfflqNull(String wdflImpst252EfflqNull) {
        writeString(Pos.WDFL_IMPST252_EFFLQ_NULL, wdflImpst252EfflqNull, Len.WDFL_IMPST252_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMPST-252-EFFLQ-NULL<br>*/
    public String getWdflImpst252EfflqNull() {
        return readString(Pos.WDFL_IMPST252_EFFLQ_NULL, Len.WDFL_IMPST252_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST252_EFFLQ = 1;
        public static final int WDFL_IMPST252_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST252_EFFLQ = 8;
        public static final int WDFL_IMPST252_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST252_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST252_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
