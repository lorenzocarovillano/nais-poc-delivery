package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPLI-ID-MOVI-CHIU<br>
 * Variable: WPLI-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpliIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpliIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPLI_ID_MOVI_CHIU;
    }

    public void setWpliIdMoviChiu(int wpliIdMoviChiu) {
        writeIntAsPacked(Pos.WPLI_ID_MOVI_CHIU, wpliIdMoviChiu, Len.Int.WPLI_ID_MOVI_CHIU);
    }

    public void setWpliIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPLI_ID_MOVI_CHIU, Pos.WPLI_ID_MOVI_CHIU);
    }

    /**Original name: WPLI-ID-MOVI-CHIU<br>*/
    public int getWpliIdMoviChiu() {
        return readPackedAsInt(Pos.WPLI_ID_MOVI_CHIU, Len.Int.WPLI_ID_MOVI_CHIU);
    }

    public byte[] getWpliIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPLI_ID_MOVI_CHIU, Pos.WPLI_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWpliIdMoviChiuSpaces() {
        fill(Pos.WPLI_ID_MOVI_CHIU, Len.WPLI_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWpliIdMoviChiuNull(String wpliIdMoviChiuNull) {
        writeString(Pos.WPLI_ID_MOVI_CHIU_NULL, wpliIdMoviChiuNull, Len.WPLI_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WPLI-ID-MOVI-CHIU-NULL<br>*/
    public String getWpliIdMoviChiuNull() {
        return readString(Pos.WPLI_ID_MOVI_CHIU_NULL, Len.WPLI_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPLI_ID_MOVI_CHIU = 1;
        public static final int WPLI_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_ID_MOVI_CHIU = 5;
        public static final int WPLI_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPLI_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
