package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-AA-REN-CER<br>
 * Variable: B03-AA-REN-CER from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03AaRenCer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03AaRenCer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_AA_REN_CER;
    }

    public void setB03AaRenCer(int b03AaRenCer) {
        writeIntAsPacked(Pos.B03_AA_REN_CER, b03AaRenCer, Len.Int.B03_AA_REN_CER);
    }

    public void setB03AaRenCerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_AA_REN_CER, Pos.B03_AA_REN_CER);
    }

    /**Original name: B03-AA-REN-CER<br>*/
    public int getB03AaRenCer() {
        return readPackedAsInt(Pos.B03_AA_REN_CER, Len.Int.B03_AA_REN_CER);
    }

    public byte[] getB03AaRenCerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_AA_REN_CER, Pos.B03_AA_REN_CER);
        return buffer;
    }

    public void setB03AaRenCerNull(String b03AaRenCerNull) {
        writeString(Pos.B03_AA_REN_CER_NULL, b03AaRenCerNull, Len.B03_AA_REN_CER_NULL);
    }

    /**Original name: B03-AA-REN-CER-NULL<br>*/
    public String getB03AaRenCerNull() {
        return readString(Pos.B03_AA_REN_CER_NULL, Len.B03_AA_REN_CER_NULL);
    }

    public String getB03AaRenCerNullFormatted() {
        return Functions.padBlanks(getB03AaRenCerNull(), Len.B03_AA_REN_CER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_AA_REN_CER = 1;
        public static final int B03_AA_REN_CER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_AA_REN_CER = 3;
        public static final int B03_AA_REN_CER_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_AA_REN_CER = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
