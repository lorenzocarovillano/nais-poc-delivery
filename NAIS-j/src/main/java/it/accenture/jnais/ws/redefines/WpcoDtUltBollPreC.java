package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-PRE-C<br>
 * Variable: WPCO-DT-ULT-BOLL-PRE-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollPreC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollPreC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_PRE_C;
    }

    public void setWpcoDtUltBollPreC(int wpcoDtUltBollPreC) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_PRE_C, wpcoDtUltBollPreC, Len.Int.WPCO_DT_ULT_BOLL_PRE_C);
    }

    public void setDpcoDtUltBollPreCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PRE_C, Pos.WPCO_DT_ULT_BOLL_PRE_C);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PRE-C<br>*/
    public int getWpcoDtUltBollPreC() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_PRE_C, Len.Int.WPCO_DT_ULT_BOLL_PRE_C);
    }

    public byte[] getWpcoDtUltBollPreCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_PRE_C, Pos.WPCO_DT_ULT_BOLL_PRE_C);
        return buffer;
    }

    public void setWpcoDtUltBollPreCNull(String wpcoDtUltBollPreCNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_PRE_C_NULL, wpcoDtUltBollPreCNull, Len.WPCO_DT_ULT_BOLL_PRE_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-PRE-C-NULL<br>*/
    public String getWpcoDtUltBollPreCNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_PRE_C_NULL, Len.WPCO_DT_ULT_BOLL_PRE_C_NULL);
    }

    public String getWpcoDtUltBollPreCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollPreCNull(), Len.WPCO_DT_ULT_BOLL_PRE_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PRE_C = 1;
        public static final int WPCO_DT_ULT_BOLL_PRE_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_PRE_C = 5;
        public static final int WPCO_DT_ULT_BOLL_PRE_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_PRE_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
