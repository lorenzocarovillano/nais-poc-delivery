package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-PILDI-MM-C<br>
 * Variable: PCO-DT-ULTC-PILDI-MM-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcPildiMmC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcPildiMmC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_PILDI_MM_C;
    }

    public void setPcoDtUltcPildiMmC(int pcoDtUltcPildiMmC) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_PILDI_MM_C, pcoDtUltcPildiMmC, Len.Int.PCO_DT_ULTC_PILDI_MM_C);
    }

    public void setPcoDtUltcPildiMmCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_MM_C, Pos.PCO_DT_ULTC_PILDI_MM_C);
    }

    /**Original name: PCO-DT-ULTC-PILDI-MM-C<br>*/
    public int getPcoDtUltcPildiMmC() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_PILDI_MM_C, Len.Int.PCO_DT_ULTC_PILDI_MM_C);
    }

    public byte[] getPcoDtUltcPildiMmCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_MM_C, Pos.PCO_DT_ULTC_PILDI_MM_C);
        return buffer;
    }

    public void initPcoDtUltcPildiMmCHighValues() {
        fill(Pos.PCO_DT_ULTC_PILDI_MM_C, Len.PCO_DT_ULTC_PILDI_MM_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcPildiMmCNull(String pcoDtUltcPildiMmCNull) {
        writeString(Pos.PCO_DT_ULTC_PILDI_MM_C_NULL, pcoDtUltcPildiMmCNull, Len.PCO_DT_ULTC_PILDI_MM_C_NULL);
    }

    /**Original name: PCO-DT-ULTC-PILDI-MM-C-NULL<br>*/
    public String getPcoDtUltcPildiMmCNull() {
        return readString(Pos.PCO_DT_ULTC_PILDI_MM_C_NULL, Len.PCO_DT_ULTC_PILDI_MM_C_NULL);
    }

    public String getPcoDtUltcPildiMmCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcPildiMmCNull(), Len.PCO_DT_ULTC_PILDI_MM_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_MM_C = 1;
        public static final int PCO_DT_ULTC_PILDI_MM_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_MM_C = 5;
        public static final int PCO_DT_ULTC_PILDI_MM_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_PILDI_MM_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
