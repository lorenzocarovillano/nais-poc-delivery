package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-MONT-END2006<br>
 * Variable: LQU-MONT-END2006 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquMontEnd2006 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquMontEnd2006() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_MONT_END2006;
    }

    public void setLquMontEnd2006(AfDecimal lquMontEnd2006) {
        writeDecimalAsPacked(Pos.LQU_MONT_END2006, lquMontEnd2006.copy());
    }

    public void setLquMontEnd2006FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_MONT_END2006, Pos.LQU_MONT_END2006);
    }

    /**Original name: LQU-MONT-END2006<br>*/
    public AfDecimal getLquMontEnd2006() {
        return readPackedAsDecimal(Pos.LQU_MONT_END2006, Len.Int.LQU_MONT_END2006, Len.Fract.LQU_MONT_END2006);
    }

    public byte[] getLquMontEnd2006AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_MONT_END2006, Pos.LQU_MONT_END2006);
        return buffer;
    }

    public void setLquMontEnd2006Null(String lquMontEnd2006Null) {
        writeString(Pos.LQU_MONT_END2006_NULL, lquMontEnd2006Null, Len.LQU_MONT_END2006_NULL);
    }

    /**Original name: LQU-MONT-END2006-NULL<br>*/
    public String getLquMontEnd2006Null() {
        return readString(Pos.LQU_MONT_END2006_NULL, Len.LQU_MONT_END2006_NULL);
    }

    public String getLquMontEnd2006NullFormatted() {
        return Functions.padBlanks(getLquMontEnd2006Null(), Len.LQU_MONT_END2006_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_MONT_END2006 = 1;
        public static final int LQU_MONT_END2006_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_MONT_END2006 = 8;
        public static final int LQU_MONT_END2006_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_MONT_END2006 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_MONT_END2006 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
