package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-TOT-FIN-UTIL<br>
 * Variable: P56-IMP-TOT-FIN-UTIL from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpTotFinUtil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpTotFinUtil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_TOT_FIN_UTIL;
    }

    public void setP56ImpTotFinUtil(AfDecimal p56ImpTotFinUtil) {
        writeDecimalAsPacked(Pos.P56_IMP_TOT_FIN_UTIL, p56ImpTotFinUtil.copy());
    }

    public void setP56ImpTotFinUtilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_TOT_FIN_UTIL, Pos.P56_IMP_TOT_FIN_UTIL);
    }

    /**Original name: P56-IMP-TOT-FIN-UTIL<br>*/
    public AfDecimal getP56ImpTotFinUtil() {
        return readPackedAsDecimal(Pos.P56_IMP_TOT_FIN_UTIL, Len.Int.P56_IMP_TOT_FIN_UTIL, Len.Fract.P56_IMP_TOT_FIN_UTIL);
    }

    public byte[] getP56ImpTotFinUtilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_TOT_FIN_UTIL, Pos.P56_IMP_TOT_FIN_UTIL);
        return buffer;
    }

    public void setP56ImpTotFinUtilNull(String p56ImpTotFinUtilNull) {
        writeString(Pos.P56_IMP_TOT_FIN_UTIL_NULL, p56ImpTotFinUtilNull, Len.P56_IMP_TOT_FIN_UTIL_NULL);
    }

    /**Original name: P56-IMP-TOT-FIN-UTIL-NULL<br>*/
    public String getP56ImpTotFinUtilNull() {
        return readString(Pos.P56_IMP_TOT_FIN_UTIL_NULL, Len.P56_IMP_TOT_FIN_UTIL_NULL);
    }

    public String getP56ImpTotFinUtilNullFormatted() {
        return Functions.padBlanks(getP56ImpTotFinUtilNull(), Len.P56_IMP_TOT_FIN_UTIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_TOT_FIN_UTIL = 1;
        public static final int P56_IMP_TOT_FIN_UTIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_TOT_FIN_UTIL = 8;
        public static final int P56_IMP_TOT_FIN_UTIL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_TOT_FIN_UTIL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_TOT_FIN_UTIL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
