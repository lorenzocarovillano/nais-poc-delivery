package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-CAR-INC<br>
 * Variable: WTDR-TOT-CAR-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_CAR_INC;
    }

    public void setWtdrTotCarInc(AfDecimal wtdrTotCarInc) {
        writeDecimalAsPacked(Pos.WTDR_TOT_CAR_INC, wtdrTotCarInc.copy());
    }

    /**Original name: WTDR-TOT-CAR-INC<br>*/
    public AfDecimal getWtdrTotCarInc() {
        return readPackedAsDecimal(Pos.WTDR_TOT_CAR_INC, Len.Int.WTDR_TOT_CAR_INC, Len.Fract.WTDR_TOT_CAR_INC);
    }

    public void setWtdrTotCarIncNull(String wtdrTotCarIncNull) {
        writeString(Pos.WTDR_TOT_CAR_INC_NULL, wtdrTotCarIncNull, Len.WTDR_TOT_CAR_INC_NULL);
    }

    /**Original name: WTDR-TOT-CAR-INC-NULL<br>*/
    public String getWtdrTotCarIncNull() {
        return readString(Pos.WTDR_TOT_CAR_INC_NULL, Len.WTDR_TOT_CAR_INC_NULL);
    }

    public String getWtdrTotCarIncNullFormatted() {
        return Functions.padBlanks(getWtdrTotCarIncNull(), Len.WTDR_TOT_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_INC = 1;
        public static final int WTDR_TOT_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_INC = 8;
        public static final int WTDR_TOT_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
