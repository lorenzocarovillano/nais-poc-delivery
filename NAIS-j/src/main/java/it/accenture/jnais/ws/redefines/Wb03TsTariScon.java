package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-TARI-SCON<br>
 * Variable: WB03-TS-TARI-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsTariScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsTariScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_TARI_SCON;
    }

    public void setWb03TsTariSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_TARI_SCON, Pos.WB03_TS_TARI_SCON);
    }

    /**Original name: WB03-TS-TARI-SCON<br>*/
    public AfDecimal getWb03TsTariScon() {
        return readPackedAsDecimal(Pos.WB03_TS_TARI_SCON, Len.Int.WB03_TS_TARI_SCON, Len.Fract.WB03_TS_TARI_SCON);
    }

    public byte[] getWb03TsTariSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_TARI_SCON, Pos.WB03_TS_TARI_SCON);
        return buffer;
    }

    public void setWb03TsTariSconNull(String wb03TsTariSconNull) {
        writeString(Pos.WB03_TS_TARI_SCON_NULL, wb03TsTariSconNull, Len.WB03_TS_TARI_SCON_NULL);
    }

    /**Original name: WB03-TS-TARI-SCON-NULL<br>*/
    public String getWb03TsTariSconNull() {
        return readString(Pos.WB03_TS_TARI_SCON_NULL, Len.WB03_TS_TARI_SCON_NULL);
    }

    public String getWb03TsTariSconNullFormatted() {
        return Functions.padBlanks(getWb03TsTariSconNull(), Len.WB03_TS_TARI_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_TARI_SCON = 1;
        public static final int WB03_TS_TARI_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_TARI_SCON = 8;
        public static final int WB03_TS_TARI_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_TARI_SCON = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_TARI_SCON = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
