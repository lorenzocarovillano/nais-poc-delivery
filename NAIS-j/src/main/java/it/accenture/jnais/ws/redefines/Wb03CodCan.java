package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-COD-CAN<br>
 * Variable: WB03-COD-CAN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CodCan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CodCan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_COD_CAN;
    }

    public void setWb03CodCan(int wb03CodCan) {
        writeIntAsPacked(Pos.WB03_COD_CAN, wb03CodCan, Len.Int.WB03_COD_CAN);
    }

    public void setWb03CodCanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_COD_CAN, Pos.WB03_COD_CAN);
    }

    /**Original name: WB03-COD-CAN<br>*/
    public int getWb03CodCan() {
        return readPackedAsInt(Pos.WB03_COD_CAN, Len.Int.WB03_COD_CAN);
    }

    public byte[] getWb03CodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_COD_CAN, Pos.WB03_COD_CAN);
        return buffer;
    }

    public void setWb03CodCanNull(String wb03CodCanNull) {
        writeString(Pos.WB03_COD_CAN_NULL, wb03CodCanNull, Len.WB03_COD_CAN_NULL);
    }

    /**Original name: WB03-COD-CAN-NULL<br>*/
    public String getWb03CodCanNull() {
        return readString(Pos.WB03_COD_CAN_NULL, Len.WB03_COD_CAN_NULL);
    }

    public String getWb03CodCanNullFormatted() {
        return Functions.padBlanks(getWb03CodCanNull(), Len.WB03_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_COD_CAN = 1;
        public static final int WB03_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_COD_CAN = 3;
        public static final int WB03_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
