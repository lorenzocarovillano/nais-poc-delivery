package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-MM-DIFF<br>
 * Variable: PMO-MM-DIFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoMmDiff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoMmDiff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_MM_DIFF;
    }

    public void setPmoMmDiff(short pmoMmDiff) {
        writeShortAsPacked(Pos.PMO_MM_DIFF, pmoMmDiff, Len.Int.PMO_MM_DIFF);
    }

    public void setPmoMmDiffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_MM_DIFF, Pos.PMO_MM_DIFF);
    }

    /**Original name: PMO-MM-DIFF<br>*/
    public short getPmoMmDiff() {
        return readPackedAsShort(Pos.PMO_MM_DIFF, Len.Int.PMO_MM_DIFF);
    }

    public byte[] getPmoMmDiffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_MM_DIFF, Pos.PMO_MM_DIFF);
        return buffer;
    }

    public void initPmoMmDiffHighValues() {
        fill(Pos.PMO_MM_DIFF, Len.PMO_MM_DIFF, Types.HIGH_CHAR_VAL);
    }

    public void setPmoMmDiffNull(String pmoMmDiffNull) {
        writeString(Pos.PMO_MM_DIFF_NULL, pmoMmDiffNull, Len.PMO_MM_DIFF_NULL);
    }

    /**Original name: PMO-MM-DIFF-NULL<br>*/
    public String getPmoMmDiffNull() {
        return readString(Pos.PMO_MM_DIFF_NULL, Len.PMO_MM_DIFF_NULL);
    }

    public String getPmoMmDiffNullFormatted() {
        return Functions.padBlanks(getPmoMmDiffNull(), Len.PMO_MM_DIFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_MM_DIFF = 1;
        public static final int PMO_MM_DIFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_MM_DIFF = 2;
        public static final int PMO_MM_DIFF_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_MM_DIFF = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
