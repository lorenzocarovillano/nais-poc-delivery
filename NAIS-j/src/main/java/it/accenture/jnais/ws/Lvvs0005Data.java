package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.enums.WkFindLetto;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0005<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0005Data {

    //==== PROPERTIES ====
    public static final int WRAN_TAB_RAPP_ANAG_MAXOCCURS = 100;
    //Original name: WRAN-ELE-RAPP-ANAG-MAX
    private short wranEleRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] wranTabRappAnag = new WranTabRappAnag[WRAN_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: WK-FIND-LETTO
    private WkFindLetto wkFindLetto = new WkFindLetto();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-RAN
    private short ixTabRan = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0005Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wranTabRappAnagIdx = 1; wranTabRappAnagIdx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; wranTabRappAnagIdx++) {
            wranTabRappAnag[wranTabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public void setWranAreaRappAnagFormatted(String data) {
        byte[] buffer = new byte[Len.WRAN_AREA_RAPP_ANAG];
        MarshalByte.writeString(buffer, 1, data, Len.WRAN_AREA_RAPP_ANAG);
        setWranAreaRappAnagBytes(buffer, 1);
    }

    public void setWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        wranEleRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wranTabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                wranTabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public void setWranEleRappAnagMax(short wranEleRappAnagMax) {
        this.wranEleRappAnagMax = wranEleRappAnagMax;
    }

    public short getWranEleRappAnagMax() {
        return this.wranEleRappAnagMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabRan(short ixTabRan) {
        this.ixTabRan = ixTabRan;
    }

    public short getIxTabRan() {
        return this.ixTabRan;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public WkFindLetto getWkFindLetto() {
        return wkFindLetto;
    }

    public WranTabRappAnag getWranTabRappAnag(int idx) {
        return wranTabRappAnag[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_ELE_RAPP_ANAG_MAX = 2;
        public static final int WRAN_AREA_RAPP_ANAG = WRAN_ELE_RAPP_ANAG_MAX + Lvvs0005Data.WRAN_TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
