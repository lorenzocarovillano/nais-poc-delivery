package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: EOF-FILESQS4<br>
 * Variable: EOF-FILESQS4 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EofFilesqs4 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setEofFilesqs4(char eofFilesqs4) {
        this.value = eofFilesqs4;
    }

    public char getEofFilesqs4() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
