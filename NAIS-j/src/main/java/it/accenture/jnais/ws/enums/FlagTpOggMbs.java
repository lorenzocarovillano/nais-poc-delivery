package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-TP-OGG-MBS<br>
 * Variable: FLAG-TP-OGG-MBS from program LOAS9000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTpOggMbs {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_TP_OGG_MBS);
    public static final String POLIZZA = "PO";
    public static final String ADESIONE = "AD";

    //==== METHODS ====
    public void setFlagTpOggMbs(String flagTpOggMbs) {
        this.value = Functions.subString(flagTpOggMbs, Len.FLAG_TP_OGG_MBS);
    }

    public String getFlagTpOggMbs() {
        return this.value;
    }

    public boolean isAdesione() {
        return value.equals(ADESIONE);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_TP_OGG_MBS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
