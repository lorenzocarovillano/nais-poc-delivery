package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-UTI<br>
 * Variable: LQU-TOT-IMP-UTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_UTI;
    }

    public void setLquTotImpUti(AfDecimal lquTotImpUti) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_UTI, lquTotImpUti.copy());
    }

    public void setLquTotImpUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_UTI, Pos.LQU_TOT_IMP_UTI);
    }

    /**Original name: LQU-TOT-IMP-UTI<br>*/
    public AfDecimal getLquTotImpUti() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_UTI, Len.Int.LQU_TOT_IMP_UTI, Len.Fract.LQU_TOT_IMP_UTI);
    }

    public byte[] getLquTotImpUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_UTI, Pos.LQU_TOT_IMP_UTI);
        return buffer;
    }

    public void setLquTotImpUtiNull(String lquTotImpUtiNull) {
        writeString(Pos.LQU_TOT_IMP_UTI_NULL, lquTotImpUtiNull, Len.LQU_TOT_IMP_UTI_NULL);
    }

    /**Original name: LQU-TOT-IMP-UTI-NULL<br>*/
    public String getLquTotImpUtiNull() {
        return readString(Pos.LQU_TOT_IMP_UTI_NULL, Len.LQU_TOT_IMP_UTI_NULL);
    }

    public String getLquTotImpUtiNullFormatted() {
        return Functions.padBlanks(getLquTotImpUtiNull(), Len.LQU_TOT_IMP_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_UTI = 1;
        public static final int LQU_TOT_IMP_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_UTI = 8;
        public static final int LQU_TOT_IMP_UTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_UTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_UTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
