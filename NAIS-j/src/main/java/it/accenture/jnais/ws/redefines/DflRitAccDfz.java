package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RIT-ACC-DFZ<br>
 * Variable: DFL-RIT-ACC-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflRitAccDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflRitAccDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RIT_ACC_DFZ;
    }

    public void setDflRitAccDfz(AfDecimal dflRitAccDfz) {
        writeDecimalAsPacked(Pos.DFL_RIT_ACC_DFZ, dflRitAccDfz.copy());
    }

    public void setDflRitAccDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RIT_ACC_DFZ, Pos.DFL_RIT_ACC_DFZ);
    }

    /**Original name: DFL-RIT-ACC-DFZ<br>*/
    public AfDecimal getDflRitAccDfz() {
        return readPackedAsDecimal(Pos.DFL_RIT_ACC_DFZ, Len.Int.DFL_RIT_ACC_DFZ, Len.Fract.DFL_RIT_ACC_DFZ);
    }

    public byte[] getDflRitAccDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RIT_ACC_DFZ, Pos.DFL_RIT_ACC_DFZ);
        return buffer;
    }

    public void setDflRitAccDfzNull(String dflRitAccDfzNull) {
        writeString(Pos.DFL_RIT_ACC_DFZ_NULL, dflRitAccDfzNull, Len.DFL_RIT_ACC_DFZ_NULL);
    }

    /**Original name: DFL-RIT-ACC-DFZ-NULL<br>*/
    public String getDflRitAccDfzNull() {
        return readString(Pos.DFL_RIT_ACC_DFZ_NULL, Len.DFL_RIT_ACC_DFZ_NULL);
    }

    public String getDflRitAccDfzNullFormatted() {
        return Functions.padBlanks(getDflRitAccDfzNull(), Len.DFL_RIT_ACC_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RIT_ACC_DFZ = 1;
        public static final int DFL_RIT_ACC_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RIT_ACC_DFZ = 8;
        public static final int DFL_RIT_ACC_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RIT_ACC_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RIT_ACC_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
