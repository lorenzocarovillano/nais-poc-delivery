package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-COMMIS-INTER<br>
 * Variable: W-B03-COMMIS-INTER from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CommisInterLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CommisInterLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COMMIS_INTER;
    }

    public void setwB03CommisInter(AfDecimal wB03CommisInter) {
        writeDecimalAsPacked(Pos.W_B03_COMMIS_INTER, wB03CommisInter.copy());
    }

    /**Original name: W-B03-COMMIS-INTER<br>*/
    public AfDecimal getwB03CommisInter() {
        return readPackedAsDecimal(Pos.W_B03_COMMIS_INTER, Len.Int.W_B03_COMMIS_INTER, Len.Fract.W_B03_COMMIS_INTER);
    }

    public byte[] getwB03CommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COMMIS_INTER, Pos.W_B03_COMMIS_INTER);
        return buffer;
    }

    public void setwB03CommisInterNull(String wB03CommisInterNull) {
        writeString(Pos.W_B03_COMMIS_INTER_NULL, wB03CommisInterNull, Len.W_B03_COMMIS_INTER_NULL);
    }

    /**Original name: W-B03-COMMIS-INTER-NULL<br>*/
    public String getwB03CommisInterNull() {
        return readString(Pos.W_B03_COMMIS_INTER_NULL, Len.W_B03_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COMMIS_INTER = 1;
        public static final int W_B03_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COMMIS_INTER = 8;
        public static final int W_B03_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
