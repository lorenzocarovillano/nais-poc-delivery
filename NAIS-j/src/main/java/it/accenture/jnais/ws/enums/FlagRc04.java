package it.accenture.jnais.ws.enums;

/**Original name: FLAG-RC-04<br>
 * Variable: FLAG-RC-04 from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagRc04 {

    //==== PROPERTIES ====
    private char value = '0';
    public static final char RC04_NON_TROVATO = '0';
    public static final char RC04_NON_FORZATO = '1';
    public static final char RC04_FORZATO = '2';
    public static final char RC08_TROVATO = '3';

    //==== METHODS ====
    public void setFlagRc04(char flagRc04) {
        this.value = flagRc04;
    }

    public char getFlagRc04() {
        return this.value;
    }

    public boolean isRc04NonForzato() {
        return value == RC04_NON_FORZATO;
    }

    public void setRc04NonForzato() {
        value = RC04_NON_FORZATO;
    }

    public boolean isRc04Forzato() {
        return value == RC04_FORZATO;
    }

    public void setRc04Forzato() {
        value = RC04_FORZATO;
    }

    public boolean isRc08Trovato() {
        return value == RC08_TROVATO;
    }

    public void setRc08Trovato() {
        value = RC08_TROVATO;
    }
}
