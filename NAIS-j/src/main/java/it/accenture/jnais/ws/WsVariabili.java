package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program IEAS9700<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabili {

    //==== PROPERTIES ====
    //Original name: WS-PRO-LOG-ERRORE
    private int proLogErrore = DefaultValues.INT_VAL;
    //Original name: WS-DESCR-ERRORE
    private String descrErrore = DefaultValues.stringVal(Len.DESCR_ERRORE);
    //Original name: WS-SESSIONE
    private String sessione = DefaultValues.stringVal(Len.SESSIONE);
    //Original name: WS-SESSIONE-FILLER
    private String sessioneFiller = DefaultValues.stringVal(Len.SESSIONE_FILLER);

    //==== METHODS ====
    public void initWsVariabiliSpaces() {
        proLogErrore = Types.INVALID_INT_VAL;
        descrErrore = "";
        initSessioneXSpaces();
    }

    public void setProLogErrore(int proLogErrore) {
        this.proLogErrore = proLogErrore;
    }

    public int getProLogErrore() {
        return this.proLogErrore;
    }

    public void setDescrErrore(String descrErrore) {
        this.descrErrore = Functions.subString(descrErrore, Len.DESCR_ERRORE);
    }

    public String getDescrErrore() {
        return this.descrErrore;
    }

    public void setSessioneXFormatted(String data) {
        byte[] buffer = new byte[Len.SESSIONE_X];
        MarshalByte.writeString(buffer, 1, data, Len.SESSIONE_X);
        setSessioneXBytes(buffer, 1);
    }

    public void setSessioneXBytes(byte[] buffer, int offset) {
        int position = offset;
        sessione = MarshalByte.readFixedString(buffer, position, Len.SESSIONE);
        position += Len.SESSIONE;
        sessioneFiller = MarshalByte.readString(buffer, position, Len.SESSIONE_FILLER);
    }

    public void initSessioneXSpaces() {
        sessione = "";
        sessioneFiller = "";
    }

    public int getSessione() {
        return NumericDisplay.asInt(this.sessione);
    }

    public String getSessioneFormatted() {
        return this.sessione;
    }

    public void setSessioneFiller(String sessioneFiller) {
        this.sessioneFiller = Functions.subString(sessioneFiller, Len.SESSIONE_FILLER);
    }

    public String getSessioneFiller() {
        return this.sessioneFiller;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCR_ERRORE = 200;
        public static final int SESSIONE = 9;
        public static final int SESSIONE_FILLER = 11;
        public static final int SESSIONE_X = SESSIONE + SESSIONE_FILLER;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
