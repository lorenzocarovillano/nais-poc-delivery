package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-TAB-CALCOLO-KO<br>
 * Variables: IVVC0211-TAB-CALCOLO-KO from copybook IVVC0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0211TabCalcoloKo {

    //==== PROPERTIES ====
    //Original name: IVVC0211-VAR-CALCOLO-KO
    private String calcoloKo = DefaultValues.stringVal(Len.CALCOLO_KO);
    //Original name: IVVC0211-VAR-NOT-FOUND-SP
    private char notFoundSp = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTabCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        calcoloKo = MarshalByte.readString(buffer, position, Len.CALCOLO_KO);
        position += Len.CALCOLO_KO;
        notFoundSp = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTabCalcoloKoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, calcoloKo, Len.CALCOLO_KO);
        position += Len.CALCOLO_KO;
        MarshalByte.writeChar(buffer, position, notFoundSp);
        return buffer;
    }

    public void initTabCalcoloKoSpaces() {
        calcoloKo = "";
        notFoundSp = Types.SPACE_CHAR;
    }

    public void setCalcoloKo(String calcoloKo) {
        this.calcoloKo = Functions.subString(calcoloKo, Len.CALCOLO_KO);
    }

    public String getCalcoloKo() {
        return this.calcoloKo;
    }

    public void setNotFoundSp(char notFoundSp) {
        this.notFoundSp = notFoundSp;
    }

    public char getNotFoundSp() {
        return this.notFoundSp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CALCOLO_KO = 19;
        public static final int NOT_FOUND_SP = 1;
        public static final int TAB_CALCOLO_KO = CALCOLO_KO + NOT_FOUND_SP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
