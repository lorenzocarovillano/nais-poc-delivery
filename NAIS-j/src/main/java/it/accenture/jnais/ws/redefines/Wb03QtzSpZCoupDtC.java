package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-QTZ-SP-Z-COUP-DT-C<br>
 * Variable: WB03-QTZ-SP-Z-COUP-DT-C from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03QtzSpZCoupDtC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03QtzSpZCoupDtC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_QTZ_SP_Z_COUP_DT_C;
    }

    public void setWb03QtzSpZCoupDtCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_QTZ_SP_Z_COUP_DT_C, Pos.WB03_QTZ_SP_Z_COUP_DT_C);
    }

    /**Original name: WB03-QTZ-SP-Z-COUP-DT-C<br>*/
    public AfDecimal getWb03QtzSpZCoupDtC() {
        return readPackedAsDecimal(Pos.WB03_QTZ_SP_Z_COUP_DT_C, Len.Int.WB03_QTZ_SP_Z_COUP_DT_C, Len.Fract.WB03_QTZ_SP_Z_COUP_DT_C);
    }

    public byte[] getWb03QtzSpZCoupDtCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_QTZ_SP_Z_COUP_DT_C, Pos.WB03_QTZ_SP_Z_COUP_DT_C);
        return buffer;
    }

    public void setWb03QtzSpZCoupDtCNull(String wb03QtzSpZCoupDtCNull) {
        writeString(Pos.WB03_QTZ_SP_Z_COUP_DT_C_NULL, wb03QtzSpZCoupDtCNull, Len.WB03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    /**Original name: WB03-QTZ-SP-Z-COUP-DT-C-NULL<br>*/
    public String getWb03QtzSpZCoupDtCNull() {
        return readString(Pos.WB03_QTZ_SP_Z_COUP_DT_C_NULL, Len.WB03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    public String getWb03QtzSpZCoupDtCNullFormatted() {
        return Functions.padBlanks(getWb03QtzSpZCoupDtCNull(), Len.WB03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_SP_Z_COUP_DT_C = 1;
        public static final int WB03_QTZ_SP_Z_COUP_DT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_SP_Z_COUP_DT_C = 7;
        public static final int WB03_QTZ_SP_Z_COUP_DT_C_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_SP_Z_COUP_DT_C = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_SP_Z_COUP_DT_C = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
