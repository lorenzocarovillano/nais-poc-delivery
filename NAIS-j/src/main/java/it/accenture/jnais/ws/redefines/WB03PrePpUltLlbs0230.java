package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-PP-ULT<br>
 * Variable: W-B03-PRE-PP-ULT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PrePpUltLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PrePpUltLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_PP_ULT;
    }

    public void setwB03PrePpUlt(AfDecimal wB03PrePpUlt) {
        writeDecimalAsPacked(Pos.W_B03_PRE_PP_ULT, wB03PrePpUlt.copy());
    }

    /**Original name: W-B03-PRE-PP-ULT<br>*/
    public AfDecimal getwB03PrePpUlt() {
        return readPackedAsDecimal(Pos.W_B03_PRE_PP_ULT, Len.Int.W_B03_PRE_PP_ULT, Len.Fract.W_B03_PRE_PP_ULT);
    }

    public byte[] getwB03PrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_PP_ULT, Pos.W_B03_PRE_PP_ULT);
        return buffer;
    }

    public void setwB03PrePpUltNull(String wB03PrePpUltNull) {
        writeString(Pos.W_B03_PRE_PP_ULT_NULL, wB03PrePpUltNull, Len.W_B03_PRE_PP_ULT_NULL);
    }

    /**Original name: W-B03-PRE-PP-ULT-NULL<br>*/
    public String getwB03PrePpUltNull() {
        return readString(Pos.W_B03_PRE_PP_ULT_NULL, Len.W_B03_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_PP_ULT = 1;
        public static final int W_B03_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_PP_ULT = 8;
        public static final int W_B03_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
