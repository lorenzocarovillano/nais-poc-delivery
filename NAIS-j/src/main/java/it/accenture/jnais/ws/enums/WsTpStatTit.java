package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-STAT-TIT<br>
 * Variable: WS-TP-STAT-TIT from copybook LCCVXST0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpStatTit {

    //==== PROPERTIES ====
    private String value = "";
    public static final String EMESSO = "EM";
    public static final String INCASSATO = "IN";
    public static final String DA_PAGARE = "DP";
    public static final String STORNATO_EMESSO = "SE";
    public static final String STORNATO_INCASSO = "SI";
    public static final String ATTESA_EMISSIONE = "AM";
    public static final String TIT_SENZA_SEGUITO = "SS";
    public static final String SALVO_BUON_FINE = "SB";
    public static final String PAGATO = "PA";
    public static final String TP_STAT_STORNATO = "ST";

    //==== METHODS ====
    public void setWsTpStatTit(String wsTpStatTit) {
        this.value = Functions.subString(wsTpStatTit, Len.WS_TP_STAT_TIT);
    }

    public String getWsTpStatTit() {
        return this.value;
    }

    public void setEmesso() {
        value = EMESSO;
    }

    public void setIncassato() {
        value = INCASSATO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_STAT_TIT = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
