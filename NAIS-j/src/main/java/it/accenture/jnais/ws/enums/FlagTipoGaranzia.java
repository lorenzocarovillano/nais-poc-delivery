package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-TIPO-GARANZIA<br>
 * Variable: FLAG-TIPO-GARANZIA from program IVVS0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTipoGaranzia {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_TIPO_GARANZIA);
    public static final String INIT = "";
    public static final String CONTESTO = "GC";
    public static final String DB = "GD";

    //==== METHODS ====
    public void setFlagTipoGaranzia(String flagTipoGaranzia) {
        this.value = Functions.subString(flagTipoGaranzia, Len.FLAG_TIPO_GARANZIA);
    }

    public String getFlagTipoGaranzia() {
        return this.value;
    }

    public void setInit() {
        value = INIT;
    }

    public boolean isContesto() {
        return value.equals(CONTESTO);
    }

    public void setContesto() {
        value = CONTESTO;
    }

    public void setDb() {
        value = DB;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_TIPO_GARANZIA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
