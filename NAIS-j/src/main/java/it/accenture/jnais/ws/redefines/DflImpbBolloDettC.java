package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-BOLLO-DETT-C<br>
 * Variable: DFL-IMPB-BOLLO-DETT-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_BOLLO_DETT_C;
    }

    public void setDflImpbBolloDettC(AfDecimal dflImpbBolloDettC) {
        writeDecimalAsPacked(Pos.DFL_IMPB_BOLLO_DETT_C, dflImpbBolloDettC.copy());
    }

    public void setDflImpbBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_BOLLO_DETT_C, Pos.DFL_IMPB_BOLLO_DETT_C);
    }

    /**Original name: DFL-IMPB-BOLLO-DETT-C<br>*/
    public AfDecimal getDflImpbBolloDettC() {
        return readPackedAsDecimal(Pos.DFL_IMPB_BOLLO_DETT_C, Len.Int.DFL_IMPB_BOLLO_DETT_C, Len.Fract.DFL_IMPB_BOLLO_DETT_C);
    }

    public byte[] getDflImpbBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_BOLLO_DETT_C, Pos.DFL_IMPB_BOLLO_DETT_C);
        return buffer;
    }

    public void setDflImpbBolloDettCNull(String dflImpbBolloDettCNull) {
        writeString(Pos.DFL_IMPB_BOLLO_DETT_C_NULL, dflImpbBolloDettCNull, Len.DFL_IMPB_BOLLO_DETT_C_NULL);
    }

    /**Original name: DFL-IMPB-BOLLO-DETT-C-NULL<br>*/
    public String getDflImpbBolloDettCNull() {
        return readString(Pos.DFL_IMPB_BOLLO_DETT_C_NULL, Len.DFL_IMPB_BOLLO_DETT_C_NULL);
    }

    public String getDflImpbBolloDettCNullFormatted() {
        return Functions.padBlanks(getDflImpbBolloDettCNull(), Len.DFL_IMPB_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_BOLLO_DETT_C = 1;
        public static final int DFL_IMPB_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_BOLLO_DETT_C = 8;
        public static final int DFL_IMPB_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
