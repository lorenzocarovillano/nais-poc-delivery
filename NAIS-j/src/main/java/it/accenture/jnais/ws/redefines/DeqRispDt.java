package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-RISP-DT<br>
 * Variable: DEQ-RISP-DT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqRispDt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqRispDt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_RISP_DT;
    }

    public void setDeqRispDt(int deqRispDt) {
        writeIntAsPacked(Pos.DEQ_RISP_DT, deqRispDt, Len.Int.DEQ_RISP_DT);
    }

    public void setDeqRispDtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_RISP_DT, Pos.DEQ_RISP_DT);
    }

    /**Original name: DEQ-RISP-DT<br>*/
    public int getDeqRispDt() {
        return readPackedAsInt(Pos.DEQ_RISP_DT, Len.Int.DEQ_RISP_DT);
    }

    public byte[] getDeqRispDtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_RISP_DT, Pos.DEQ_RISP_DT);
        return buffer;
    }

    public void setDeqRispDtNull(String deqRispDtNull) {
        writeString(Pos.DEQ_RISP_DT_NULL, deqRispDtNull, Len.DEQ_RISP_DT_NULL);
    }

    /**Original name: DEQ-RISP-DT-NULL<br>*/
    public String getDeqRispDtNull() {
        return readString(Pos.DEQ_RISP_DT_NULL, Len.DEQ_RISP_DT_NULL);
    }

    public String getDeqRispDtNullFormatted() {
        return Functions.padBlanks(getDeqRispDtNull(), Len.DEQ_RISP_DT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_DT = 1;
        public static final int DEQ_RISP_DT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_DT = 5;
        public static final int DEQ_RISP_DT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_DT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
