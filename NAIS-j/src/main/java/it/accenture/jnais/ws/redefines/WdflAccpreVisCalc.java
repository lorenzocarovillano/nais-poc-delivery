package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-VIS-CALC<br>
 * Variable: WDFL-ACCPRE-VIS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreVisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreVisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_VIS_CALC;
    }

    public void setWdflAccpreVisCalc(AfDecimal wdflAccpreVisCalc) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_VIS_CALC, wdflAccpreVisCalc.copy());
    }

    public void setWdflAccpreVisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_VIS_CALC, Pos.WDFL_ACCPRE_VIS_CALC);
    }

    /**Original name: WDFL-ACCPRE-VIS-CALC<br>*/
    public AfDecimal getWdflAccpreVisCalc() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_VIS_CALC, Len.Int.WDFL_ACCPRE_VIS_CALC, Len.Fract.WDFL_ACCPRE_VIS_CALC);
    }

    public byte[] getWdflAccpreVisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_VIS_CALC, Pos.WDFL_ACCPRE_VIS_CALC);
        return buffer;
    }

    public void setWdflAccpreVisCalcNull(String wdflAccpreVisCalcNull) {
        writeString(Pos.WDFL_ACCPRE_VIS_CALC_NULL, wdflAccpreVisCalcNull, Len.WDFL_ACCPRE_VIS_CALC_NULL);
    }

    /**Original name: WDFL-ACCPRE-VIS-CALC-NULL<br>*/
    public String getWdflAccpreVisCalcNull() {
        return readString(Pos.WDFL_ACCPRE_VIS_CALC_NULL, Len.WDFL_ACCPRE_VIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_VIS_CALC = 1;
        public static final int WDFL_ACCPRE_VIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_VIS_CALC = 8;
        public static final int WDFL_ACCPRE_VIS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_VIS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_VIS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
