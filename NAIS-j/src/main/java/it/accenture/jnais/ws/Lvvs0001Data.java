package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.DColl;
import it.accenture.jnais.copy.GarIvvs0216;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.ws.enums.WcomFlagTrovato;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0001<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0001Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0001";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: WKS-NOME-TABELLA
    private String wksNomeTabella = DefaultValues.stringVal(Len.WKS_NOME_TABELLA);
    //Original name: WS-TP-DATO
    private WsTpDatoLves0269 wsTpDato = new WsTpDatoLves0269();
    //Original name: WS-TP-OGGETTO
    private WsTpOgg wsTpOggetto = new WsTpOgg();
    //Original name: D-COLL
    private DColl dColl = new DColl();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: GAR
    private GarIvvs0216 gar = new GarIvvs0216();
    //Original name: PARAM-MOVI
    private ParamMovi paramMovi = new ParamMovi();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: AREE-APPOGGIO
    private AreeAppoggio areeAppoggio = new AreeAppoggio();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private IxIndiciLvvs0001 ixIndici = new IxIndiciLvvs0001();
    //Original name: WCOM-FLAG-TROVATO
    private WcomFlagTrovato wcomFlagTrovato = new WcomFlagTrovato();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWksNomeTabella(String wksNomeTabella) {
        this.wksNomeTabella = Functions.subString(wksNomeTabella, Len.WKS_NOME_TABELLA);
    }

    public String getWksNomeTabella() {
        return this.wksNomeTabella;
    }

    public String getWksNomeTabellaFormatted() {
        return Functions.padBlanks(getWksNomeTabella(), Len.WKS_NOME_TABELLA);
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public AreeAppoggio getAreeAppoggio() {
        return areeAppoggio;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public GarIvvs0216 getGar() {
        return gar;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLvvs0001 getIxIndici() {
        return ixIndici;
    }

    public ParamMovi getParamMovi() {
        return paramMovi;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public WcomFlagTrovato getWcomFlagTrovato() {
        return wcomFlagTrovato;
    }

    public WsTpDatoLves0269 getWsTpDato() {
        return wsTpDato;
    }

    public WsTpOgg getWsTpOggetto() {
        return wsTpOggetto;
    }

    public DColl getdColl() {
        return dColl;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WKS_NOME_TABELLA = 30;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
