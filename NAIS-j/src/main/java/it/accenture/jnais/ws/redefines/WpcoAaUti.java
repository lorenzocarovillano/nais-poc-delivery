package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-AA-UTI<br>
 * Variable: WPCO-AA-UTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoAaUti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoAaUti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_AA_UTI;
    }

    public void setWpcoAaUti(short wpcoAaUti) {
        writeShortAsPacked(Pos.WPCO_AA_UTI, wpcoAaUti, Len.Int.WPCO_AA_UTI);
    }

    public void setDpcoAaUtiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_AA_UTI, Pos.WPCO_AA_UTI);
    }

    /**Original name: WPCO-AA-UTI<br>*/
    public short getWpcoAaUti() {
        return readPackedAsShort(Pos.WPCO_AA_UTI, Len.Int.WPCO_AA_UTI);
    }

    public byte[] getWpcoAaUtiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_AA_UTI, Pos.WPCO_AA_UTI);
        return buffer;
    }

    public void setWpcoAaUtiNull(String wpcoAaUtiNull) {
        writeString(Pos.WPCO_AA_UTI_NULL, wpcoAaUtiNull, Len.WPCO_AA_UTI_NULL);
    }

    /**Original name: WPCO-AA-UTI-NULL<br>*/
    public String getWpcoAaUtiNull() {
        return readString(Pos.WPCO_AA_UTI_NULL, Len.WPCO_AA_UTI_NULL);
    }

    public String getWpcoAaUtiNullFormatted() {
        return Functions.padBlanks(getWpcoAaUtiNull(), Len.WPCO_AA_UTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_AA_UTI = 1;
        public static final int WPCO_AA_UTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_AA_UTI = 3;
        public static final int WPCO_AA_UTI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_AA_UTI = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
