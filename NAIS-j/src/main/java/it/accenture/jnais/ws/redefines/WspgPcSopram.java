package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WSPG-PC-SOPRAM<br>
 * Variable: WSPG-PC-SOPRAM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WspgPcSopram extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WspgPcSopram() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSPG_PC_SOPRAM;
    }

    public void setWspgPcSopram(AfDecimal wspgPcSopram) {
        writeDecimalAsPacked(Pos.WSPG_PC_SOPRAM, wspgPcSopram.copy());
    }

    public void setWspgPcSopramFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WSPG_PC_SOPRAM, Pos.WSPG_PC_SOPRAM);
    }

    /**Original name: WSPG-PC-SOPRAM<br>*/
    public AfDecimal getWspgPcSopram() {
        return readPackedAsDecimal(Pos.WSPG_PC_SOPRAM, Len.Int.WSPG_PC_SOPRAM, Len.Fract.WSPG_PC_SOPRAM);
    }

    public byte[] getWspgPcSopramAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WSPG_PC_SOPRAM, Pos.WSPG_PC_SOPRAM);
        return buffer;
    }

    public void initWspgPcSopramSpaces() {
        fill(Pos.WSPG_PC_SOPRAM, Len.WSPG_PC_SOPRAM, Types.SPACE_CHAR);
    }

    public void setWspgPcSopramNull(String wspgPcSopramNull) {
        writeString(Pos.WSPG_PC_SOPRAM_NULL, wspgPcSopramNull, Len.WSPG_PC_SOPRAM_NULL);
    }

    /**Original name: WSPG-PC-SOPRAM-NULL<br>*/
    public String getWspgPcSopramNull() {
        return readString(Pos.WSPG_PC_SOPRAM_NULL, Len.WSPG_PC_SOPRAM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSPG_PC_SOPRAM = 1;
        public static final int WSPG_PC_SOPRAM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WSPG_PC_SOPRAM = 8;
        public static final int WSPG_PC_SOPRAM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WSPG_PC_SOPRAM = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WSPG_PC_SOPRAM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
