package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-IMP-TRASFE<br>
 * Variable: TIT-IMP-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_IMP_TRASFE;
    }

    public void setTitImpTrasfe(AfDecimal titImpTrasfe) {
        writeDecimalAsPacked(Pos.TIT_IMP_TRASFE, titImpTrasfe.copy());
    }

    public void setTitImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_IMP_TRASFE, Pos.TIT_IMP_TRASFE);
    }

    /**Original name: TIT-IMP-TRASFE<br>*/
    public AfDecimal getTitImpTrasfe() {
        return readPackedAsDecimal(Pos.TIT_IMP_TRASFE, Len.Int.TIT_IMP_TRASFE, Len.Fract.TIT_IMP_TRASFE);
    }

    public byte[] getTitImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_IMP_TRASFE, Pos.TIT_IMP_TRASFE);
        return buffer;
    }

    public void setTitImpTrasfeNull(String titImpTrasfeNull) {
        writeString(Pos.TIT_IMP_TRASFE_NULL, titImpTrasfeNull, Len.TIT_IMP_TRASFE_NULL);
    }

    /**Original name: TIT-IMP-TRASFE-NULL<br>*/
    public String getTitImpTrasfeNull() {
        return readString(Pos.TIT_IMP_TRASFE_NULL, Len.TIT_IMP_TRASFE_NULL);
    }

    public String getTitImpTrasfeNullFormatted() {
        return Functions.padBlanks(getTitImpTrasfeNull(), Len.TIT_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_IMP_TRASFE = 1;
        public static final int TIT_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_IMP_TRASFE = 8;
        public static final int TIT_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
