package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-CNBT-ANTIRAC<br>
 * Variable: WTDR-TOT-CNBT-ANTIRAC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotCnbtAntirac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotCnbtAntirac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_CNBT_ANTIRAC;
    }

    public void setWtdrTotCnbtAntirac(AfDecimal wtdrTotCnbtAntirac) {
        writeDecimalAsPacked(Pos.WTDR_TOT_CNBT_ANTIRAC, wtdrTotCnbtAntirac.copy());
    }

    /**Original name: WTDR-TOT-CNBT-ANTIRAC<br>*/
    public AfDecimal getWtdrTotCnbtAntirac() {
        return readPackedAsDecimal(Pos.WTDR_TOT_CNBT_ANTIRAC, Len.Int.WTDR_TOT_CNBT_ANTIRAC, Len.Fract.WTDR_TOT_CNBT_ANTIRAC);
    }

    public void setWtdrTotCnbtAntiracNull(String wtdrTotCnbtAntiracNull) {
        writeString(Pos.WTDR_TOT_CNBT_ANTIRAC_NULL, wtdrTotCnbtAntiracNull, Len.WTDR_TOT_CNBT_ANTIRAC_NULL);
    }

    /**Original name: WTDR-TOT-CNBT-ANTIRAC-NULL<br>*/
    public String getWtdrTotCnbtAntiracNull() {
        return readString(Pos.WTDR_TOT_CNBT_ANTIRAC_NULL, Len.WTDR_TOT_CNBT_ANTIRAC_NULL);
    }

    public String getWtdrTotCnbtAntiracNullFormatted() {
        return Functions.padBlanks(getWtdrTotCnbtAntiracNull(), Len.WTDR_TOT_CNBT_ANTIRAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CNBT_ANTIRAC = 1;
        public static final int WTDR_TOT_CNBT_ANTIRAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CNBT_ANTIRAC = 8;
        public static final int WTDR_TOT_CNBT_ANTIRAC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CNBT_ANTIRAC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CNBT_ANTIRAC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
