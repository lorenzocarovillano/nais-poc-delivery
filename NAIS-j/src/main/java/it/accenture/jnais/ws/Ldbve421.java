package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.AreaLdbve421I;

/**Original name: LDBVE421<br>
 * Variable: LDBVE421 from copybook LDBVE421<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbve421 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: AREA-LDBVE421-I
    private AreaLdbve421I areaLdbve421I = new AreaLdbve421I();
    //Original name: LDBVE421-IMP-BOLLO-DETT-C
    private AfDecimal ldbve421ImpBolloDettC = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBVE421-IMP-BOLLO-DETT-V
    private AfDecimal ldbve421ImpBolloDettV = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBVE421-IMP-BOLLO-TOT-V
    private AfDecimal ldbve421ImpBolloTotV = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LDBVE421-IMP-BOLLO-TOT-R
    private AfDecimal ldbve421ImpBolloTotR = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVE421;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbve421Bytes(buf);
    }

    public String getLdbve421Formatted() {
        return MarshalByteExt.bufferToStr(getLdbve421Bytes());
    }

    public void setLdbve421Bytes(byte[] buffer) {
        setLdbve421Bytes(buffer, 1);
    }

    public byte[] getLdbve421Bytes() {
        byte[] buffer = new byte[Len.LDBVE421];
        return getLdbve421Bytes(buffer, 1);
    }

    public void setLdbve421Bytes(byte[] buffer, int offset) {
        int position = offset;
        areaLdbve421I.setAreaLdbve421IBytes(buffer, position);
        position += AreaLdbve421I.Len.AREA_LDBVE421_I;
        setAreaLdbve421OBytes(buffer, position);
    }

    public byte[] getLdbve421Bytes(byte[] buffer, int offset) {
        int position = offset;
        areaLdbve421I.getAreaLdbve421IBytes(buffer, position);
        position += AreaLdbve421I.Len.AREA_LDBVE421_I;
        getAreaLdbve421OBytes(buffer, position);
        return buffer;
    }

    public void setAreaLdbve421OBytes(byte[] buffer, int offset) {
        int position = offset;
        ldbve421ImpBolloDettC.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE421_IMP_BOLLO_DETT_C, Len.Fract.LDBVE421_IMP_BOLLO_DETT_C));
        position += Len.LDBVE421_IMP_BOLLO_DETT_C;
        ldbve421ImpBolloDettV.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE421_IMP_BOLLO_DETT_V, Len.Fract.LDBVE421_IMP_BOLLO_DETT_V));
        position += Len.LDBVE421_IMP_BOLLO_DETT_V;
        ldbve421ImpBolloTotV.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE421_IMP_BOLLO_TOT_V, Len.Fract.LDBVE421_IMP_BOLLO_TOT_V));
        position += Len.LDBVE421_IMP_BOLLO_TOT_V;
        ldbve421ImpBolloTotR.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.LDBVE421_IMP_BOLLO_TOT_R, Len.Fract.LDBVE421_IMP_BOLLO_TOT_R));
    }

    public byte[] getAreaLdbve421OBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve421ImpBolloDettC.copy());
        position += Len.LDBVE421_IMP_BOLLO_DETT_C;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve421ImpBolloDettV.copy());
        position += Len.LDBVE421_IMP_BOLLO_DETT_V;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve421ImpBolloTotV.copy());
        position += Len.LDBVE421_IMP_BOLLO_TOT_V;
        MarshalByte.writeDecimalAsPacked(buffer, position, ldbve421ImpBolloTotR.copy());
        return buffer;
    }

    public void setLdbve421ImpBolloDettC(AfDecimal ldbve421ImpBolloDettC) {
        this.ldbve421ImpBolloDettC.assign(ldbve421ImpBolloDettC);
    }

    public AfDecimal getLdbve421ImpBolloDettC() {
        return this.ldbve421ImpBolloDettC.copy();
    }

    public void setLdbve421ImpBolloDettV(AfDecimal ldbve421ImpBolloDettV) {
        this.ldbve421ImpBolloDettV.assign(ldbve421ImpBolloDettV);
    }

    public AfDecimal getLdbve421ImpBolloDettV() {
        return this.ldbve421ImpBolloDettV.copy();
    }

    public void setLdbve421ImpBolloTotV(AfDecimal ldbve421ImpBolloTotV) {
        this.ldbve421ImpBolloTotV.assign(ldbve421ImpBolloTotV);
    }

    public AfDecimal getLdbve421ImpBolloTotV() {
        return this.ldbve421ImpBolloTotV.copy();
    }

    public void setLdbve421ImpBolloTotR(AfDecimal ldbve421ImpBolloTotR) {
        this.ldbve421ImpBolloTotR.assign(ldbve421ImpBolloTotR);
    }

    public AfDecimal getLdbve421ImpBolloTotR() {
        return this.ldbve421ImpBolloTotR.copy();
    }

    public AreaLdbve421I getAreaLdbve421I() {
        return areaLdbve421I;
    }

    @Override
    public byte[] serialize() {
        return getLdbve421Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBVE421_IMP_BOLLO_DETT_C = 8;
        public static final int LDBVE421_IMP_BOLLO_DETT_V = 8;
        public static final int LDBVE421_IMP_BOLLO_TOT_V = 8;
        public static final int LDBVE421_IMP_BOLLO_TOT_R = 8;
        public static final int AREA_LDBVE421_O = LDBVE421_IMP_BOLLO_DETT_C + LDBVE421_IMP_BOLLO_DETT_V + LDBVE421_IMP_BOLLO_TOT_V + LDBVE421_IMP_BOLLO_TOT_R;
        public static final int LDBVE421 = AreaLdbve421I.Len.AREA_LDBVE421_I + AREA_LDBVE421_O;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBVE421_IMP_BOLLO_DETT_C = 12;
            public static final int LDBVE421_IMP_BOLLO_DETT_V = 12;
            public static final int LDBVE421_IMP_BOLLO_TOT_V = 12;
            public static final int LDBVE421_IMP_BOLLO_TOT_R = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBVE421_IMP_BOLLO_DETT_C = 3;
            public static final int LDBVE421_IMP_BOLLO_DETT_V = 3;
            public static final int LDBVE421_IMP_BOLLO_TOT_V = 3;
            public static final int LDBVE421_IMP_BOLLO_TOT_R = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
