package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PRE-TOT<br>
 * Variable: DTC-PRE-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PRE_TOT;
    }

    public void setDtcPreTot(AfDecimal dtcPreTot) {
        writeDecimalAsPacked(Pos.DTC_PRE_TOT, dtcPreTot.copy());
    }

    public void setDtcPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PRE_TOT, Pos.DTC_PRE_TOT);
    }

    /**Original name: DTC-PRE-TOT<br>*/
    public AfDecimal getDtcPreTot() {
        return readPackedAsDecimal(Pos.DTC_PRE_TOT, Len.Int.DTC_PRE_TOT, Len.Fract.DTC_PRE_TOT);
    }

    public byte[] getDtcPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PRE_TOT, Pos.DTC_PRE_TOT);
        return buffer;
    }

    public void setDtcPreTotNull(String dtcPreTotNull) {
        writeString(Pos.DTC_PRE_TOT_NULL, dtcPreTotNull, Len.DTC_PRE_TOT_NULL);
    }

    /**Original name: DTC-PRE-TOT-NULL<br>*/
    public String getDtcPreTotNull() {
        return readString(Pos.DTC_PRE_TOT_NULL, Len.DTC_PRE_TOT_NULL);
    }

    public String getDtcPreTotNullFormatted() {
        return Functions.padBlanks(getDtcPreTotNull(), Len.DTC_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PRE_TOT = 1;
        public static final int DTC_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PRE_TOT = 8;
        public static final int DTC_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
