package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-NUM-AA-PAG-PRE<br>
 * Variable: WGRZ-NUM-AA-PAG-PRE from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzNumAaPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzNumAaPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_NUM_AA_PAG_PRE;
    }

    public void setWgrzNumAaPagPre(int wgrzNumAaPagPre) {
        writeIntAsPacked(Pos.WGRZ_NUM_AA_PAG_PRE, wgrzNumAaPagPre, Len.Int.WGRZ_NUM_AA_PAG_PRE);
    }

    public void setWgrzNumAaPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_NUM_AA_PAG_PRE, Pos.WGRZ_NUM_AA_PAG_PRE);
    }

    /**Original name: WGRZ-NUM-AA-PAG-PRE<br>*/
    public int getWgrzNumAaPagPre() {
        return readPackedAsInt(Pos.WGRZ_NUM_AA_PAG_PRE, Len.Int.WGRZ_NUM_AA_PAG_PRE);
    }

    public byte[] getWgrzNumAaPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_NUM_AA_PAG_PRE, Pos.WGRZ_NUM_AA_PAG_PRE);
        return buffer;
    }

    public void initWgrzNumAaPagPreSpaces() {
        fill(Pos.WGRZ_NUM_AA_PAG_PRE, Len.WGRZ_NUM_AA_PAG_PRE, Types.SPACE_CHAR);
    }

    public void setWgrzNumAaPagPreNull(String wgrzNumAaPagPreNull) {
        writeString(Pos.WGRZ_NUM_AA_PAG_PRE_NULL, wgrzNumAaPagPreNull, Len.WGRZ_NUM_AA_PAG_PRE_NULL);
    }

    /**Original name: WGRZ-NUM-AA-PAG-PRE-NULL<br>*/
    public String getWgrzNumAaPagPreNull() {
        return readString(Pos.WGRZ_NUM_AA_PAG_PRE_NULL, Len.WGRZ_NUM_AA_PAG_PRE_NULL);
    }

    public String getWgrzNumAaPagPreNullFormatted() {
        return Functions.padBlanks(getWgrzNumAaPagPreNull(), Len.WGRZ_NUM_AA_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_NUM_AA_PAG_PRE = 1;
        public static final int WGRZ_NUM_AA_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_NUM_AA_PAG_PRE = 3;
        public static final int WGRZ_NUM_AA_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_NUM_AA_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
