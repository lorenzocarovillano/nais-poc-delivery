package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ETA-AA-3O-ASSTO<br>
 * Variable: TGA-ETA-AA-3O-ASSTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaEtaAa3oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaEtaAa3oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ETA_AA3O_ASSTO;
    }

    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        writeShortAsPacked(Pos.TGA_ETA_AA3O_ASSTO, tgaEtaAa3oAssto, Len.Int.TGA_ETA_AA3O_ASSTO);
    }

    public void setTgaEtaAa3oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ETA_AA3O_ASSTO, Pos.TGA_ETA_AA3O_ASSTO);
    }

    /**Original name: TGA-ETA-AA-3O-ASSTO<br>*/
    public short getTgaEtaAa3oAssto() {
        return readPackedAsShort(Pos.TGA_ETA_AA3O_ASSTO, Len.Int.TGA_ETA_AA3O_ASSTO);
    }

    public byte[] getTgaEtaAa3oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ETA_AA3O_ASSTO, Pos.TGA_ETA_AA3O_ASSTO);
        return buffer;
    }

    public void setTgaEtaAa3oAsstoNull(String tgaEtaAa3oAsstoNull) {
        writeString(Pos.TGA_ETA_AA3O_ASSTO_NULL, tgaEtaAa3oAsstoNull, Len.TGA_ETA_AA3O_ASSTO_NULL);
    }

    /**Original name: TGA-ETA-AA-3O-ASSTO-NULL<br>*/
    public String getTgaEtaAa3oAsstoNull() {
        return readString(Pos.TGA_ETA_AA3O_ASSTO_NULL, Len.TGA_ETA_AA3O_ASSTO_NULL);
    }

    public String getTgaEtaAa3oAsstoNullFormatted() {
        return Functions.padBlanks(getTgaEtaAa3oAsstoNull(), Len.TGA_ETA_AA3O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ETA_AA3O_ASSTO = 1;
        public static final int TGA_ETA_AA3O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ETA_AA3O_ASSTO = 2;
        public static final int TGA_ETA_AA3O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ETA_AA3O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
