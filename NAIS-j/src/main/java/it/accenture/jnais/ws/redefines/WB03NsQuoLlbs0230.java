package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-NS-QUO<br>
 * Variable: W-B03-NS-QUO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03NsQuoLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03NsQuoLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_NS_QUO;
    }

    public void setwB03NsQuo(AfDecimal wB03NsQuo) {
        writeDecimalAsPacked(Pos.W_B03_NS_QUO, wB03NsQuo.copy());
    }

    /**Original name: W-B03-NS-QUO<br>*/
    public AfDecimal getwB03NsQuo() {
        return readPackedAsDecimal(Pos.W_B03_NS_QUO, Len.Int.W_B03_NS_QUO, Len.Fract.W_B03_NS_QUO);
    }

    public byte[] getwB03NsQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_NS_QUO, Pos.W_B03_NS_QUO);
        return buffer;
    }

    public void setwB03NsQuoNull(String wB03NsQuoNull) {
        writeString(Pos.W_B03_NS_QUO_NULL, wB03NsQuoNull, Len.W_B03_NS_QUO_NULL);
    }

    /**Original name: W-B03-NS-QUO-NULL<br>*/
    public String getwB03NsQuoNull() {
        return readString(Pos.W_B03_NS_QUO_NULL, Len.W_B03_NS_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_NS_QUO = 1;
        public static final int W_B03_NS_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_NS_QUO = 4;
        public static final int W_B03_NS_QUO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_NS_QUO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_NS_QUO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
