package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-PAG-PRE<br>
 * Variable: W-B03-DUR-PAG-PRE from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurPagPreLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurPagPreLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_PAG_PRE;
    }

    public void setwB03DurPagPre(int wB03DurPagPre) {
        writeIntAsPacked(Pos.W_B03_DUR_PAG_PRE, wB03DurPagPre, Len.Int.W_B03_DUR_PAG_PRE);
    }

    /**Original name: W-B03-DUR-PAG-PRE<br>*/
    public int getwB03DurPagPre() {
        return readPackedAsInt(Pos.W_B03_DUR_PAG_PRE, Len.Int.W_B03_DUR_PAG_PRE);
    }

    public byte[] getwB03DurPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_PAG_PRE, Pos.W_B03_DUR_PAG_PRE);
        return buffer;
    }

    public void setwB03DurPagPreNull(String wB03DurPagPreNull) {
        writeString(Pos.W_B03_DUR_PAG_PRE_NULL, wB03DurPagPreNull, Len.W_B03_DUR_PAG_PRE_NULL);
    }

    /**Original name: W-B03-DUR-PAG-PRE-NULL<br>*/
    public String getwB03DurPagPreNull() {
        return readString(Pos.W_B03_DUR_PAG_PRE_NULL, Len.W_B03_DUR_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_PAG_PRE = 1;
        public static final int W_B03_DUR_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_PAG_PRE = 3;
        public static final int W_B03_DUR_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
