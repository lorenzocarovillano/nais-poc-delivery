package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ELE-CACHE-35<br>
 * Variables: ELE-CACHE-35 from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class EleCache35 {

    //==== PROPERTIES ====
    //Original name: C35-ID-GAR
    private String idGar = DefaultValues.stringVal(Len.ID_GAR);
    //Original name: C35-CNTRVAL-PREC
    private AfDecimal cntrvalPrec = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setIdGarFormatted(String idGar) {
        this.idGar = Trunc.toUnsignedNumeric(idGar, Len.ID_GAR);
    }

    public int getIdGar() {
        return NumericDisplay.asInt(this.idGar);
    }

    public void setCntrvalPrec(AfDecimal cntrvalPrec) {
        this.cntrvalPrec.assign(cntrvalPrec);
    }

    public AfDecimal getCntrvalPrec() {
        return this.cntrvalPrec.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GAR = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
