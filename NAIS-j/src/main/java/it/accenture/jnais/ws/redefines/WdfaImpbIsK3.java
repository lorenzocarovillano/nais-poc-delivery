package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-IS-K3<br>
 * Variable: WDFA-IMPB-IS-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbIsK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbIsK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_IS_K3;
    }

    public void setWdfaImpbIsK3(AfDecimal wdfaImpbIsK3) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_IS_K3, wdfaImpbIsK3.copy());
    }

    public void setWdfaImpbIsK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_IS_K3, Pos.WDFA_IMPB_IS_K3);
    }

    /**Original name: WDFA-IMPB-IS-K3<br>*/
    public AfDecimal getWdfaImpbIsK3() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_IS_K3, Len.Int.WDFA_IMPB_IS_K3, Len.Fract.WDFA_IMPB_IS_K3);
    }

    public byte[] getWdfaImpbIsK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_IS_K3, Pos.WDFA_IMPB_IS_K3);
        return buffer;
    }

    public void setWdfaImpbIsK3Null(String wdfaImpbIsK3Null) {
        writeString(Pos.WDFA_IMPB_IS_K3_NULL, wdfaImpbIsK3Null, Len.WDFA_IMPB_IS_K3_NULL);
    }

    /**Original name: WDFA-IMPB-IS-K3-NULL<br>*/
    public String getWdfaImpbIsK3Null() {
        return readString(Pos.WDFA_IMPB_IS_K3_NULL, Len.WDFA_IMPB_IS_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K3 = 1;
        public static final int WDFA_IMPB_IS_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K3 = 8;
        public static final int WDFA_IMPB_IS_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
