package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-ID-MOVI-CHIU<br>
 * Variable: ADE-ID-MOVI-CHIU from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_ID_MOVI_CHIU;
    }

    public void setAdeIdMoviChiu(int adeIdMoviChiu) {
        writeIntAsPacked(Pos.ADE_ID_MOVI_CHIU, adeIdMoviChiu, Len.Int.ADE_ID_MOVI_CHIU);
    }

    public void setAdeIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_ID_MOVI_CHIU, Pos.ADE_ID_MOVI_CHIU);
    }

    /**Original name: ADE-ID-MOVI-CHIU<br>*/
    public int getAdeIdMoviChiu() {
        return readPackedAsInt(Pos.ADE_ID_MOVI_CHIU, Len.Int.ADE_ID_MOVI_CHIU);
    }

    public byte[] getAdeIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_ID_MOVI_CHIU, Pos.ADE_ID_MOVI_CHIU);
        return buffer;
    }

    public void setAdeIdMoviChiuNull(String adeIdMoviChiuNull) {
        writeString(Pos.ADE_ID_MOVI_CHIU_NULL, adeIdMoviChiuNull, Len.ADE_ID_MOVI_CHIU_NULL);
    }

    /**Original name: ADE-ID-MOVI-CHIU-NULL<br>*/
    public String getAdeIdMoviChiuNull() {
        return readString(Pos.ADE_ID_MOVI_CHIU_NULL, Len.ADE_ID_MOVI_CHIU_NULL);
    }

    public String getAdeIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getAdeIdMoviChiuNull(), Len.ADE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_ID_MOVI_CHIU = 1;
        public static final int ADE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_ID_MOVI_CHIU = 5;
        public static final int ADE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
