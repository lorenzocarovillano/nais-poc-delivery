package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-ONER-TRASFE<br>
 * Variable: WDFA-ONER-TRASFE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaOnerTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaOnerTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_ONER_TRASFE;
    }

    public void setWdfaOnerTrasfe(AfDecimal wdfaOnerTrasfe) {
        writeDecimalAsPacked(Pos.WDFA_ONER_TRASFE, wdfaOnerTrasfe.copy());
    }

    public void setWdfaOnerTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_ONER_TRASFE, Pos.WDFA_ONER_TRASFE);
    }

    /**Original name: WDFA-ONER-TRASFE<br>*/
    public AfDecimal getWdfaOnerTrasfe() {
        return readPackedAsDecimal(Pos.WDFA_ONER_TRASFE, Len.Int.WDFA_ONER_TRASFE, Len.Fract.WDFA_ONER_TRASFE);
    }

    public byte[] getWdfaOnerTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_ONER_TRASFE, Pos.WDFA_ONER_TRASFE);
        return buffer;
    }

    public void setWdfaOnerTrasfeNull(String wdfaOnerTrasfeNull) {
        writeString(Pos.WDFA_ONER_TRASFE_NULL, wdfaOnerTrasfeNull, Len.WDFA_ONER_TRASFE_NULL);
    }

    /**Original name: WDFA-ONER-TRASFE-NULL<br>*/
    public String getWdfaOnerTrasfeNull() {
        return readString(Pos.WDFA_ONER_TRASFE_NULL, Len.WDFA_ONER_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_ONER_TRASFE = 1;
        public static final int WDFA_ONER_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ONER_TRASFE = 8;
        public static final int WDFA_ONER_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_ONER_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_ONER_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
