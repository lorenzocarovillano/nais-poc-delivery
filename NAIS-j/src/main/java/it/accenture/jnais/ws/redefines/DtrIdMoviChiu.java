package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-ID-MOVI-CHIU<br>
 * Variable: DTR-ID-MOVI-CHIU from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_ID_MOVI_CHIU;
    }

    public void setDtrIdMoviChiu(int dtrIdMoviChiu) {
        writeIntAsPacked(Pos.DTR_ID_MOVI_CHIU, dtrIdMoviChiu, Len.Int.DTR_ID_MOVI_CHIU);
    }

    public void setDtrIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_ID_MOVI_CHIU, Pos.DTR_ID_MOVI_CHIU);
    }

    /**Original name: DTR-ID-MOVI-CHIU<br>*/
    public int getDtrIdMoviChiu() {
        return readPackedAsInt(Pos.DTR_ID_MOVI_CHIU, Len.Int.DTR_ID_MOVI_CHIU);
    }

    public byte[] getDtrIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_ID_MOVI_CHIU, Pos.DTR_ID_MOVI_CHIU);
        return buffer;
    }

    public void setDtrIdMoviChiuNull(String dtrIdMoviChiuNull) {
        writeString(Pos.DTR_ID_MOVI_CHIU_NULL, dtrIdMoviChiuNull, Len.DTR_ID_MOVI_CHIU_NULL);
    }

    /**Original name: DTR-ID-MOVI-CHIU-NULL<br>*/
    public String getDtrIdMoviChiuNull() {
        return readString(Pos.DTR_ID_MOVI_CHIU_NULL, Len.DTR_ID_MOVI_CHIU_NULL);
    }

    public String getDtrIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getDtrIdMoviChiuNull(), Len.DTR_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_ID_MOVI_CHIU = 1;
        public static final int DTR_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_ID_MOVI_CHIU = 5;
        public static final int DTR_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
