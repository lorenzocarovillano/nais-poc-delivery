package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-GAR-GG<br>
 * Variable: B03-DUR-GAR-GG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurGarGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurGarGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_GAR_GG;
    }

    public void setB03DurGarGg(int b03DurGarGg) {
        writeIntAsPacked(Pos.B03_DUR_GAR_GG, b03DurGarGg, Len.Int.B03_DUR_GAR_GG);
    }

    public void setB03DurGarGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_GAR_GG, Pos.B03_DUR_GAR_GG);
    }

    /**Original name: B03-DUR-GAR-GG<br>*/
    public int getB03DurGarGg() {
        return readPackedAsInt(Pos.B03_DUR_GAR_GG, Len.Int.B03_DUR_GAR_GG);
    }

    public byte[] getB03DurGarGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_GAR_GG, Pos.B03_DUR_GAR_GG);
        return buffer;
    }

    public void setB03DurGarGgNull(String b03DurGarGgNull) {
        writeString(Pos.B03_DUR_GAR_GG_NULL, b03DurGarGgNull, Len.B03_DUR_GAR_GG_NULL);
    }

    /**Original name: B03-DUR-GAR-GG-NULL<br>*/
    public String getB03DurGarGgNull() {
        return readString(Pos.B03_DUR_GAR_GG_NULL, Len.B03_DUR_GAR_GG_NULL);
    }

    public String getB03DurGarGgNullFormatted() {
        return Functions.padBlanks(getB03DurGarGgNull(), Len.B03_DUR_GAR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_GAR_GG = 1;
        public static final int B03_DUR_GAR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_GAR_GG = 3;
        public static final int B03_DUR_GAR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_GAR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
