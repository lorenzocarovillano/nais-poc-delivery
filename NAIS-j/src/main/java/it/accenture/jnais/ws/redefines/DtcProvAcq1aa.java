package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PROV-ACQ-1AA<br>
 * Variable: DTC-PROV-ACQ-1AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PROV_ACQ1AA;
    }

    public void setDtcProvAcq1aa(AfDecimal dtcProvAcq1aa) {
        writeDecimalAsPacked(Pos.DTC_PROV_ACQ1AA, dtcProvAcq1aa.copy());
    }

    public void setDtcProvAcq1aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PROV_ACQ1AA, Pos.DTC_PROV_ACQ1AA);
    }

    /**Original name: DTC-PROV-ACQ-1AA<br>*/
    public AfDecimal getDtcProvAcq1aa() {
        return readPackedAsDecimal(Pos.DTC_PROV_ACQ1AA, Len.Int.DTC_PROV_ACQ1AA, Len.Fract.DTC_PROV_ACQ1AA);
    }

    public byte[] getDtcProvAcq1aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PROV_ACQ1AA, Pos.DTC_PROV_ACQ1AA);
        return buffer;
    }

    public void setDtcProvAcq1aaNull(String dtcProvAcq1aaNull) {
        writeString(Pos.DTC_PROV_ACQ1AA_NULL, dtcProvAcq1aaNull, Len.DTC_PROV_ACQ1AA_NULL);
    }

    /**Original name: DTC-PROV-ACQ-1AA-NULL<br>*/
    public String getDtcProvAcq1aaNull() {
        return readString(Pos.DTC_PROV_ACQ1AA_NULL, Len.DTC_PROV_ACQ1AA_NULL);
    }

    public String getDtcProvAcq1aaNullFormatted() {
        return Functions.padBlanks(getDtcProvAcq1aaNull(), Len.DTC_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PROV_ACQ1AA = 1;
        public static final int DTC_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PROV_ACQ1AA = 8;
        public static final int DTC_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
