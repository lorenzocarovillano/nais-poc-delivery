package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CUR-VAS<br>
 * Variable: FLAG-CUR-VAS from program LCCS0450<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCurVas {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char INIT_CUR_VAS = 'S';
    public static final char FINE_CUR_VAS = 'N';

    //==== METHODS ====
    public void setFlagCurVas(char flagCurVas) {
        this.value = flagCurVas;
    }

    public char getFlagCurVas() {
        return this.value;
    }

    public void setInitCurVas() {
        value = INIT_CUR_VAS;
    }

    public boolean isFineCurVas() {
        return value == FINE_CUR_VAS;
    }

    public void setFineCurVas() {
        value = FINE_CUR_VAS;
    }
}
