package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-AT92-C<br>
 * Variable: WPCO-DT-ULT-ELAB-AT92-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabAt92C extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabAt92C() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_AT92_C;
    }

    public void setWpcoDtUltElabAt92C(int wpcoDtUltElabAt92C) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_AT92_C, wpcoDtUltElabAt92C, Len.Int.WPCO_DT_ULT_ELAB_AT92_C);
    }

    public void setDpcoDtUltElabAt92CFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT92_C, Pos.WPCO_DT_ULT_ELAB_AT92_C);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT92-C<br>*/
    public int getWpcoDtUltElabAt92C() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_AT92_C, Len.Int.WPCO_DT_ULT_ELAB_AT92_C);
    }

    public byte[] getWpcoDtUltElabAt92CAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT92_C, Pos.WPCO_DT_ULT_ELAB_AT92_C);
        return buffer;
    }

    public void setWpcoDtUltElabAt92CNull(String wpcoDtUltElabAt92CNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_AT92_C_NULL, wpcoDtUltElabAt92CNull, Len.WPCO_DT_ULT_ELAB_AT92_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT92-C-NULL<br>*/
    public String getWpcoDtUltElabAt92CNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_AT92_C_NULL, Len.WPCO_DT_ULT_ELAB_AT92_C_NULL);
    }

    public String getWpcoDtUltElabAt92CNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabAt92CNull(), Len.WPCO_DT_ULT_ELAB_AT92_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT92_C = 1;
        public static final int WPCO_DT_ULT_ELAB_AT92_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT92_C = 5;
        public static final int WPCO_DT_ULT_ELAB_AT92_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_AT92_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
