package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-FRAZ-DFLT<br>
 * Variable: WDCO-FRAZ-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoFrazDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoFrazDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_FRAZ_DFLT;
    }

    public void setWdcoFrazDflt(int wdcoFrazDflt) {
        writeIntAsPacked(Pos.WDCO_FRAZ_DFLT, wdcoFrazDflt, Len.Int.WDCO_FRAZ_DFLT);
    }

    public void setWdcoFrazDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_FRAZ_DFLT, Pos.WDCO_FRAZ_DFLT);
    }

    /**Original name: WDCO-FRAZ-DFLT<br>*/
    public int getWdcoFrazDflt() {
        return readPackedAsInt(Pos.WDCO_FRAZ_DFLT, Len.Int.WDCO_FRAZ_DFLT);
    }

    public byte[] getWdcoFrazDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_FRAZ_DFLT, Pos.WDCO_FRAZ_DFLT);
        return buffer;
    }

    public void setWdcoFrazDfltNull(String wdcoFrazDfltNull) {
        writeString(Pos.WDCO_FRAZ_DFLT_NULL, wdcoFrazDfltNull, Len.WDCO_FRAZ_DFLT_NULL);
    }

    /**Original name: WDCO-FRAZ-DFLT-NULL<br>*/
    public String getWdcoFrazDfltNull() {
        return readString(Pos.WDCO_FRAZ_DFLT_NULL, Len.WDCO_FRAZ_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_FRAZ_DFLT = 1;
        public static final int WDCO_FRAZ_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_FRAZ_DFLT = 3;
        public static final int WDCO_FRAZ_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_FRAZ_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
