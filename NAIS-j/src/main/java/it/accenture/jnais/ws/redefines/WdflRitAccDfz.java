package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-ACC-DFZ<br>
 * Variable: WDFL-RIT-ACC-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitAccDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitAccDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_ACC_DFZ;
    }

    public void setWdflRitAccDfz(AfDecimal wdflRitAccDfz) {
        writeDecimalAsPacked(Pos.WDFL_RIT_ACC_DFZ, wdflRitAccDfz.copy());
    }

    public void setWdflRitAccDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_ACC_DFZ, Pos.WDFL_RIT_ACC_DFZ);
    }

    /**Original name: WDFL-RIT-ACC-DFZ<br>*/
    public AfDecimal getWdflRitAccDfz() {
        return readPackedAsDecimal(Pos.WDFL_RIT_ACC_DFZ, Len.Int.WDFL_RIT_ACC_DFZ, Len.Fract.WDFL_RIT_ACC_DFZ);
    }

    public byte[] getWdflRitAccDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_ACC_DFZ, Pos.WDFL_RIT_ACC_DFZ);
        return buffer;
    }

    public void setWdflRitAccDfzNull(String wdflRitAccDfzNull) {
        writeString(Pos.WDFL_RIT_ACC_DFZ_NULL, wdflRitAccDfzNull, Len.WDFL_RIT_ACC_DFZ_NULL);
    }

    /**Original name: WDFL-RIT-ACC-DFZ-NULL<br>*/
    public String getWdflRitAccDfzNull() {
        return readString(Pos.WDFL_RIT_ACC_DFZ_NULL, Len.WDFL_RIT_ACC_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_ACC_DFZ = 1;
        public static final int WDFL_RIT_ACC_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_ACC_DFZ = 8;
        public static final int WDFL_RIT_ACC_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_ACC_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_ACC_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
