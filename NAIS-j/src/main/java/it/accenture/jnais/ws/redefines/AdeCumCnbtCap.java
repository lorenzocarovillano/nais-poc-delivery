package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-CUM-CNBT-CAP<br>
 * Variable: ADE-CUM-CNBT-CAP from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeCumCnbtCap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeCumCnbtCap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_CUM_CNBT_CAP;
    }

    public void setAdeCumCnbtCap(AfDecimal adeCumCnbtCap) {
        writeDecimalAsPacked(Pos.ADE_CUM_CNBT_CAP, adeCumCnbtCap.copy());
    }

    public void setAdeCumCnbtCapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_CUM_CNBT_CAP, Pos.ADE_CUM_CNBT_CAP);
    }

    /**Original name: ADE-CUM-CNBT-CAP<br>*/
    public AfDecimal getAdeCumCnbtCap() {
        return readPackedAsDecimal(Pos.ADE_CUM_CNBT_CAP, Len.Int.ADE_CUM_CNBT_CAP, Len.Fract.ADE_CUM_CNBT_CAP);
    }

    public byte[] getAdeCumCnbtCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_CUM_CNBT_CAP, Pos.ADE_CUM_CNBT_CAP);
        return buffer;
    }

    public void setAdeCumCnbtCapNull(String adeCumCnbtCapNull) {
        writeString(Pos.ADE_CUM_CNBT_CAP_NULL, adeCumCnbtCapNull, Len.ADE_CUM_CNBT_CAP_NULL);
    }

    /**Original name: ADE-CUM-CNBT-CAP-NULL<br>*/
    public String getAdeCumCnbtCapNull() {
        return readString(Pos.ADE_CUM_CNBT_CAP_NULL, Len.ADE_CUM_CNBT_CAP_NULL);
    }

    public String getAdeCumCnbtCapNullFormatted() {
        return Functions.padBlanks(getAdeCumCnbtCapNull(), Len.ADE_CUM_CNBT_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_CUM_CNBT_CAP = 1;
        public static final int ADE_CUM_CNBT_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_CUM_CNBT_CAP = 8;
        public static final int ADE_CUM_CNBT_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_CUM_CNBT_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_CUM_CNBT_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
