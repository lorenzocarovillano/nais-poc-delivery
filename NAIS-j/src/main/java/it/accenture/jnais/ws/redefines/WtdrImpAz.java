package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-IMP-AZ<br>
 * Variable: WTDR-IMP-AZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_IMP_AZ;
    }

    public void setWtdrImpAz(AfDecimal wtdrImpAz) {
        writeDecimalAsPacked(Pos.WTDR_IMP_AZ, wtdrImpAz.copy());
    }

    /**Original name: WTDR-IMP-AZ<br>*/
    public AfDecimal getWtdrImpAz() {
        return readPackedAsDecimal(Pos.WTDR_IMP_AZ, Len.Int.WTDR_IMP_AZ, Len.Fract.WTDR_IMP_AZ);
    }

    public void setWtdrImpAzNull(String wtdrImpAzNull) {
        writeString(Pos.WTDR_IMP_AZ_NULL, wtdrImpAzNull, Len.WTDR_IMP_AZ_NULL);
    }

    /**Original name: WTDR-IMP-AZ-NULL<br>*/
    public String getWtdrImpAzNull() {
        return readString(Pos.WTDR_IMP_AZ_NULL, Len.WTDR_IMP_AZ_NULL);
    }

    public String getWtdrImpAzNullFormatted() {
        return Functions.padBlanks(getWtdrImpAzNull(), Len.WTDR_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_AZ = 1;
        public static final int WTDR_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_IMP_AZ = 8;
        public static final int WTDR_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
