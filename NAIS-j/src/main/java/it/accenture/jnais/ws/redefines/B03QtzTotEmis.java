package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-QTZ-TOT-EMIS<br>
 * Variable: B03-QTZ-TOT-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03QtzTotEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03QtzTotEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_QTZ_TOT_EMIS;
    }

    public void setB03QtzTotEmis(AfDecimal b03QtzTotEmis) {
        writeDecimalAsPacked(Pos.B03_QTZ_TOT_EMIS, b03QtzTotEmis.copy());
    }

    public void setB03QtzTotEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_QTZ_TOT_EMIS, Pos.B03_QTZ_TOT_EMIS);
    }

    /**Original name: B03-QTZ-TOT-EMIS<br>*/
    public AfDecimal getB03QtzTotEmis() {
        return readPackedAsDecimal(Pos.B03_QTZ_TOT_EMIS, Len.Int.B03_QTZ_TOT_EMIS, Len.Fract.B03_QTZ_TOT_EMIS);
    }

    public byte[] getB03QtzTotEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_QTZ_TOT_EMIS, Pos.B03_QTZ_TOT_EMIS);
        return buffer;
    }

    public void setB03QtzTotEmisNull(String b03QtzTotEmisNull) {
        writeString(Pos.B03_QTZ_TOT_EMIS_NULL, b03QtzTotEmisNull, Len.B03_QTZ_TOT_EMIS_NULL);
    }

    /**Original name: B03-QTZ-TOT-EMIS-NULL<br>*/
    public String getB03QtzTotEmisNull() {
        return readString(Pos.B03_QTZ_TOT_EMIS_NULL, Len.B03_QTZ_TOT_EMIS_NULL);
    }

    public String getB03QtzTotEmisNullFormatted() {
        return Functions.padBlanks(getB03QtzTotEmisNull(), Len.B03_QTZ_TOT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_QTZ_TOT_EMIS = 1;
        public static final int B03_QTZ_TOT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_QTZ_TOT_EMIS = 7;
        public static final int B03_QTZ_TOT_EMIS_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_QTZ_TOT_EMIS = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_QTZ_TOT_EMIS = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
