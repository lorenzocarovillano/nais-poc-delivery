package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B05-VAL-PC<br>
 * Variable: W-B05-VAL-PC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB05ValPcLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB05ValPcLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B05_VAL_PC;
    }

    public void setwB05ValPc(AfDecimal wB05ValPc) {
        writeDecimalAsPacked(Pos.W_B05_VAL_PC, wB05ValPc.copy());
    }

    /**Original name: W-B05-VAL-PC<br>*/
    public AfDecimal getwB05ValPc() {
        return readPackedAsDecimal(Pos.W_B05_VAL_PC, Len.Int.W_B05_VAL_PC, Len.Fract.W_B05_VAL_PC);
    }

    public byte[] getwB05ValPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B05_VAL_PC, Pos.W_B05_VAL_PC);
        return buffer;
    }

    public void setwB05ValPcNull(String wB05ValPcNull) {
        writeString(Pos.W_B05_VAL_PC_NULL, wB05ValPcNull, Len.W_B05_VAL_PC_NULL);
    }

    /**Original name: W-B05-VAL-PC-NULL<br>*/
    public String getwB05ValPcNull() {
        return readString(Pos.W_B05_VAL_PC_NULL, Len.W_B05_VAL_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B05_VAL_PC = 1;
        public static final int W_B05_VAL_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B05_VAL_PC = 8;
        public static final int W_B05_VAL_PC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B05_VAL_PC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B05_VAL_PC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
