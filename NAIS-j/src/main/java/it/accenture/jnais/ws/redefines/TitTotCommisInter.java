package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-COMMIS-INTER<br>
 * Variable: TIT-TOT-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_COMMIS_INTER;
    }

    public void setTitTotCommisInter(AfDecimal titTotCommisInter) {
        writeDecimalAsPacked(Pos.TIT_TOT_COMMIS_INTER, titTotCommisInter.copy());
    }

    public void setTitTotCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_COMMIS_INTER, Pos.TIT_TOT_COMMIS_INTER);
    }

    /**Original name: TIT-TOT-COMMIS-INTER<br>*/
    public AfDecimal getTitTotCommisInter() {
        return readPackedAsDecimal(Pos.TIT_TOT_COMMIS_INTER, Len.Int.TIT_TOT_COMMIS_INTER, Len.Fract.TIT_TOT_COMMIS_INTER);
    }

    public byte[] getTitTotCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_COMMIS_INTER, Pos.TIT_TOT_COMMIS_INTER);
        return buffer;
    }

    public void setTitTotCommisInterNull(String titTotCommisInterNull) {
        writeString(Pos.TIT_TOT_COMMIS_INTER_NULL, titTotCommisInterNull, Len.TIT_TOT_COMMIS_INTER_NULL);
    }

    /**Original name: TIT-TOT-COMMIS-INTER-NULL<br>*/
    public String getTitTotCommisInterNull() {
        return readString(Pos.TIT_TOT_COMMIS_INTER_NULL, Len.TIT_TOT_COMMIS_INTER_NULL);
    }

    public String getTitTotCommisInterNullFormatted() {
        return Functions.padBlanks(getTitTotCommisInterNull(), Len.TIT_TOT_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_COMMIS_INTER = 1;
        public static final int TIT_TOT_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_COMMIS_INTER = 8;
        public static final int TIT_TOT_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
