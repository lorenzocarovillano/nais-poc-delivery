package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IAS-PNL<br>
 * Variable: TLI-IAS-PNL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliIasPnl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliIasPnl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IAS_PNL;
    }

    public void setTliIasPnl(AfDecimal tliIasPnl) {
        writeDecimalAsPacked(Pos.TLI_IAS_PNL, tliIasPnl.copy());
    }

    public void setTliIasPnlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IAS_PNL, Pos.TLI_IAS_PNL);
    }

    /**Original name: TLI-IAS-PNL<br>*/
    public AfDecimal getTliIasPnl() {
        return readPackedAsDecimal(Pos.TLI_IAS_PNL, Len.Int.TLI_IAS_PNL, Len.Fract.TLI_IAS_PNL);
    }

    public byte[] getTliIasPnlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IAS_PNL, Pos.TLI_IAS_PNL);
        return buffer;
    }

    public void setTliIasPnlNull(String tliIasPnlNull) {
        writeString(Pos.TLI_IAS_PNL_NULL, tliIasPnlNull, Len.TLI_IAS_PNL_NULL);
    }

    /**Original name: TLI-IAS-PNL-NULL<br>*/
    public String getTliIasPnlNull() {
        return readString(Pos.TLI_IAS_PNL_NULL, Len.TLI_IAS_PNL_NULL);
    }

    public String getTliIasPnlNullFormatted() {
        return Functions.padBlanks(getTliIasPnlNull(), Len.TLI_IAS_PNL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IAS_PNL = 1;
        public static final int TLI_IAS_PNL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IAS_PNL = 8;
        public static final int TLI_IAS_PNL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IAS_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IAS_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
