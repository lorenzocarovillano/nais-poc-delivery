package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DUR-MM<br>
 * Variable: L3401-DUR-MM from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DUR_MM;
    }

    public void setL3401DurMm(int l3401DurMm) {
        writeIntAsPacked(Pos.L3401_DUR_MM, l3401DurMm, Len.Int.L3401_DUR_MM);
    }

    public void setL3401DurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DUR_MM, Pos.L3401_DUR_MM);
    }

    /**Original name: L3401-DUR-MM<br>*/
    public int getL3401DurMm() {
        return readPackedAsInt(Pos.L3401_DUR_MM, Len.Int.L3401_DUR_MM);
    }

    public byte[] getL3401DurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DUR_MM, Pos.L3401_DUR_MM);
        return buffer;
    }

    /**Original name: L3401-DUR-MM-NULL<br>*/
    public String getL3401DurMmNull() {
        return readString(Pos.L3401_DUR_MM_NULL, Len.L3401_DUR_MM_NULL);
    }

    public String getL3401DurMmNullFormatted() {
        return Functions.padBlanks(getL3401DurMmNull(), Len.L3401_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DUR_MM = 1;
        public static final int L3401_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DUR_MM = 3;
        public static final int L3401_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
