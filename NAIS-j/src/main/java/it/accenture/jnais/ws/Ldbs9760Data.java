package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndLiq;
import it.accenture.jnais.copy.TrchDiGarDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS9760<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs9760Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-LIQ
    private IndLiq indLiq = new IndLiq();
    //Original name: LIQ-DB
    private TrchDiGarDb liqDb = new TrchDiGarDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndLiq getIndLiq() {
        return indLiq;
    }

    public TrchDiGarDb getLiqDb() {
        return liqDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
