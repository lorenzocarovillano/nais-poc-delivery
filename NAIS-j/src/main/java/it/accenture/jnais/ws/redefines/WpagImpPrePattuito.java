package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-PRE-PATTUITO<br>
 * Variable: WPAG-IMP-PRE-PATTUITO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpPrePattuito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpPrePattuito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_PRE_PATTUITO;
    }

    public void setWpagImpPrePattuito(AfDecimal wpagImpPrePattuito) {
        writeDecimalAsPacked(Pos.WPAG_IMP_PRE_PATTUITO, wpagImpPrePattuito.copy());
    }

    public void setWpagImpPrePattuitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_PRE_PATTUITO, Pos.WPAG_IMP_PRE_PATTUITO);
    }

    /**Original name: WPAG-IMP-PRE-PATTUITO<br>*/
    public AfDecimal getWpagImpPrePattuito() {
        return readPackedAsDecimal(Pos.WPAG_IMP_PRE_PATTUITO, Len.Int.WPAG_IMP_PRE_PATTUITO, Len.Fract.WPAG_IMP_PRE_PATTUITO);
    }

    public byte[] getWpagImpPrePattuitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_PRE_PATTUITO, Pos.WPAG_IMP_PRE_PATTUITO);
        return buffer;
    }

    public void initWpagImpPrePattuitoSpaces() {
        fill(Pos.WPAG_IMP_PRE_PATTUITO, Len.WPAG_IMP_PRE_PATTUITO, Types.SPACE_CHAR);
    }

    public void setWpagImpPrePattuitoNull(String wpagImpPrePattuitoNull) {
        writeString(Pos.WPAG_IMP_PRE_PATTUITO_NULL, wpagImpPrePattuitoNull, Len.WPAG_IMP_PRE_PATTUITO_NULL);
    }

    /**Original name: WPAG-IMP-PRE-PATTUITO-NULL<br>*/
    public String getWpagImpPrePattuitoNull() {
        return readString(Pos.WPAG_IMP_PRE_PATTUITO_NULL, Len.WPAG_IMP_PRE_PATTUITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PRE_PATTUITO = 1;
        public static final int WPAG_IMP_PRE_PATTUITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_PRE_PATTUITO = 8;
        public static final int WPAG_IMP_PRE_PATTUITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PRE_PATTUITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_PRE_PATTUITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
