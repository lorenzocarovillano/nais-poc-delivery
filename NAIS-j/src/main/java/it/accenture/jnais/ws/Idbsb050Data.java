package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndLogErrore;
import it.accenture.jnais.copy.RichDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSB050<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsb050Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BILA-VAR-CALC-T
    private IndLogErrore indBilaVarCalcT = new IndLogErrore();
    //Original name: BILA-VAR-CALC-T-DB
    private RichDb bilaVarCalcTDb = new RichDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public RichDb getBilaVarCalcTDb() {
        return bilaVarCalcTDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndLogErrore getIndBilaVarCalcT() {
        return indBilaVarCalcT;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
