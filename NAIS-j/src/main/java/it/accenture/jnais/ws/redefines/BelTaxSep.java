package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-TAX-SEP<br>
 * Variable: BEL-TAX-SEP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelTaxSep extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelTaxSep() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_TAX_SEP;
    }

    public void setBelTaxSep(AfDecimal belTaxSep) {
        writeDecimalAsPacked(Pos.BEL_TAX_SEP, belTaxSep.copy());
    }

    public void setBelTaxSepFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_TAX_SEP, Pos.BEL_TAX_SEP);
    }

    /**Original name: BEL-TAX-SEP<br>*/
    public AfDecimal getBelTaxSep() {
        return readPackedAsDecimal(Pos.BEL_TAX_SEP, Len.Int.BEL_TAX_SEP, Len.Fract.BEL_TAX_SEP);
    }

    public byte[] getBelTaxSepAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_TAX_SEP, Pos.BEL_TAX_SEP);
        return buffer;
    }

    public void setBelTaxSepNull(String belTaxSepNull) {
        writeString(Pos.BEL_TAX_SEP_NULL, belTaxSepNull, Len.BEL_TAX_SEP_NULL);
    }

    /**Original name: BEL-TAX-SEP-NULL<br>*/
    public String getBelTaxSepNull() {
        return readString(Pos.BEL_TAX_SEP_NULL, Len.BEL_TAX_SEP_NULL);
    }

    public String getBelTaxSepNullFormatted() {
        return Functions.padBlanks(getBelTaxSepNull(), Len.BEL_TAX_SEP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_TAX_SEP = 1;
        public static final int BEL_TAX_SEP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_TAX_SEP = 8;
        public static final int BEL_TAX_SEP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_TAX_SEP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_TAX_SEP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
