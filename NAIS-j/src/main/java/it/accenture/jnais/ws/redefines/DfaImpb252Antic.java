package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMPB-252-ANTIC<br>
 * Variable: DFA-IMPB-252-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpb252Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpb252Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMPB252_ANTIC;
    }

    public void setDfaImpb252Antic(AfDecimal dfaImpb252Antic) {
        writeDecimalAsPacked(Pos.DFA_IMPB252_ANTIC, dfaImpb252Antic.copy());
    }

    public void setDfaImpb252AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMPB252_ANTIC, Pos.DFA_IMPB252_ANTIC);
    }

    /**Original name: DFA-IMPB-252-ANTIC<br>*/
    public AfDecimal getDfaImpb252Antic() {
        return readPackedAsDecimal(Pos.DFA_IMPB252_ANTIC, Len.Int.DFA_IMPB252_ANTIC, Len.Fract.DFA_IMPB252_ANTIC);
    }

    public byte[] getDfaImpb252AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMPB252_ANTIC, Pos.DFA_IMPB252_ANTIC);
        return buffer;
    }

    public void setDfaImpb252AnticNull(String dfaImpb252AnticNull) {
        writeString(Pos.DFA_IMPB252_ANTIC_NULL, dfaImpb252AnticNull, Len.DFA_IMPB252_ANTIC_NULL);
    }

    /**Original name: DFA-IMPB-252-ANTIC-NULL<br>*/
    public String getDfaImpb252AnticNull() {
        return readString(Pos.DFA_IMPB252_ANTIC_NULL, Len.DFA_IMPB252_ANTIC_NULL);
    }

    public String getDfaImpb252AnticNullFormatted() {
        return Functions.padBlanks(getDfaImpb252AnticNull(), Len.DFA_IMPB252_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMPB252_ANTIC = 1;
        public static final int DFA_IMPB252_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMPB252_ANTIC = 8;
        public static final int DFA_IMPB252_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMPB252_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMPB252_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
