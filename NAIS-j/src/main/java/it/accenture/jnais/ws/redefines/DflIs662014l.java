package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IS-662014L<br>
 * Variable: DFL-IS-662014L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIs662014l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIs662014l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IS662014L;
    }

    public void setDflIs662014l(AfDecimal dflIs662014l) {
        writeDecimalAsPacked(Pos.DFL_IS662014L, dflIs662014l.copy());
    }

    public void setDflIs662014lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IS662014L, Pos.DFL_IS662014L);
    }

    /**Original name: DFL-IS-662014L<br>*/
    public AfDecimal getDflIs662014l() {
        return readPackedAsDecimal(Pos.DFL_IS662014L, Len.Int.DFL_IS662014L, Len.Fract.DFL_IS662014L);
    }

    public byte[] getDflIs662014lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IS662014L, Pos.DFL_IS662014L);
        return buffer;
    }

    public void setDflIs662014lNull(String dflIs662014lNull) {
        writeString(Pos.DFL_IS662014L_NULL, dflIs662014lNull, Len.DFL_IS662014L_NULL);
    }

    /**Original name: DFL-IS-662014L-NULL<br>*/
    public String getDflIs662014lNull() {
        return readString(Pos.DFL_IS662014L_NULL, Len.DFL_IS662014L_NULL);
    }

    public String getDflIs662014lNullFormatted() {
        return Functions.padBlanks(getDflIs662014lNull(), Len.DFL_IS662014L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IS662014L = 1;
        public static final int DFL_IS662014L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IS662014L = 8;
        public static final int DFL_IS662014L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IS662014L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IS662014L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
