package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PROV-DA-REC<br>
 * Variable: DTC-PROV-DA-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PROV_DA_REC;
    }

    public void setDtcProvDaRec(AfDecimal dtcProvDaRec) {
        writeDecimalAsPacked(Pos.DTC_PROV_DA_REC, dtcProvDaRec.copy());
    }

    public void setDtcProvDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PROV_DA_REC, Pos.DTC_PROV_DA_REC);
    }

    /**Original name: DTC-PROV-DA-REC<br>*/
    public AfDecimal getDtcProvDaRec() {
        return readPackedAsDecimal(Pos.DTC_PROV_DA_REC, Len.Int.DTC_PROV_DA_REC, Len.Fract.DTC_PROV_DA_REC);
    }

    public byte[] getDtcProvDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PROV_DA_REC, Pos.DTC_PROV_DA_REC);
        return buffer;
    }

    public void setDtcProvDaRecNull(String dtcProvDaRecNull) {
        writeString(Pos.DTC_PROV_DA_REC_NULL, dtcProvDaRecNull, Len.DTC_PROV_DA_REC_NULL);
    }

    /**Original name: DTC-PROV-DA-REC-NULL<br>*/
    public String getDtcProvDaRecNull() {
        return readString(Pos.DTC_PROV_DA_REC_NULL, Len.DTC_PROV_DA_REC_NULL);
    }

    public String getDtcProvDaRecNullFormatted() {
        return Functions.padBlanks(getDtcProvDaRecNull(), Len.DTC_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PROV_DA_REC = 1;
        public static final int DTC_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PROV_DA_REC = 8;
        public static final int DTC_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
