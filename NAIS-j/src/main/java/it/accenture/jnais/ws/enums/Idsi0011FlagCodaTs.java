package it.accenture.jnais.ws.enums;

/**Original name: IDSI0011-FLAG-CODA-TS<br>
 * Variable: IDSI0011-FLAG-CODA-TS from copybook IDSI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0011FlagCodaTs {

    //==== PROPERTIES ====
    private char value = 'N';
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIdsi0011FlagCodaTs(char idsi0011FlagCodaTs) {
        this.value = idsi0011FlagCodaTs;
    }

    public char getIdsi0011FlagCodaTs() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSI0011_FLAG_CODA_TS = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
