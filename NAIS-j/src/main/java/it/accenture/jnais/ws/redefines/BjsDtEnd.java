package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BJS-DT-END<br>
 * Variable: BJS-DT-END from program IABS0060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BjsDtEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BjsDtEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BJS_DT_END;
    }

    public void setBjsDtEnd(long bjsDtEnd) {
        writeLongAsPacked(Pos.BJS_DT_END, bjsDtEnd, Len.Int.BJS_DT_END);
    }

    public void setBjsDtEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BJS_DT_END, Pos.BJS_DT_END);
    }

    /**Original name: BJS-DT-END<br>*/
    public long getBjsDtEnd() {
        return readPackedAsLong(Pos.BJS_DT_END, Len.Int.BJS_DT_END);
    }

    public byte[] getBjsDtEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BJS_DT_END, Pos.BJS_DT_END);
        return buffer;
    }

    public void setBjsDtEndNull(String bjsDtEndNull) {
        writeString(Pos.BJS_DT_END_NULL, bjsDtEndNull, Len.BJS_DT_END_NULL);
    }

    /**Original name: BJS-DT-END-NULL<br>*/
    public String getBjsDtEndNull() {
        return readString(Pos.BJS_DT_END_NULL, Len.BJS_DT_END_NULL);
    }

    public String getBjsDtEndNullFormatted() {
        return Functions.padBlanks(getBjsDtEndNull(), Len.BJS_DT_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BJS_DT_END = 1;
        public static final int BJS_DT_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BJS_DT_END = 10;
        public static final int BJS_DT_END_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BJS_DT_END = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
