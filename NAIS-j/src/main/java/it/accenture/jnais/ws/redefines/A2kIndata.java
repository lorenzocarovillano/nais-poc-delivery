package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: A2K-INDATA<br>
 * Variable: A2K-INDATA from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A2kIndata extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A2kIndata() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A2K_INDATA;
    }

    public void setA2kIndata(String a2kIndata) {
        writeString(Pos.A2K_INDATA, a2kIndata, Len.A2K_INDATA);
    }

    public void setA2kIndataFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A2K_INDATA, Pos.A2K_INDATA);
    }

    /**Original name: A2K-INDATA<br>
	 * <pre>                                       data di input</pre>*/
    public String getA2kIndata() {
        return readString(Pos.A2K_INDATA, Len.A2K_INDATA);
    }

    public String getA2kIndataFormatted() {
        return Functions.padBlanks(getA2kIndata(), Len.A2K_INDATA);
    }

    public byte[] getA2kIndataAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A2K_INDATA, Pos.A2K_INDATA);
        return buffer;
    }

    public String getIngmaFormatted() {
        return readFixedString(Pos.INGMA, Len.INGMA);
    }

    /**Original name: A2K-INGMA-X<br>*/
    public byte[] getIngmaXBytes() {
        byte[] buffer = new byte[Len.INGMA_X];
        return getIngmaXBytes(buffer, 1);
    }

    public byte[] getIngmaXBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.INGMA_X, Pos.INGMA_X);
        return buffer;
    }

    public String getInssFormatted() {
        return readFixedString(Pos.INSS, Len.INSS);
    }

    public String getInaaFormatted() {
        return readFixedString(Pos.INAA, Len.INAA);
    }

    public String getIngg02Formatted() {
        return readFixedString(Pos.INGG02, Len.INGG02);
    }

    public String getInmm02Formatted() {
        return readFixedString(Pos.INMM02, Len.INMM02);
    }

    public String getInss02Formatted() {
        return readFixedString(Pos.INSS02, Len.INSS02);
    }

    public String getInaa02Formatted() {
        return readFixedString(Pos.INAA02, Len.INAA02);
    }

    public void setA2kInamg(int a2kInamg) {
        writeInt(Pos.INAMG, a2kInamg, Len.Int.A2K_INAMG, SignType.NO_SIGN);
    }

    public void setA2kInamgFormatted(String a2kInamg) {
        writeString(Pos.INAMG, Trunc.toUnsignedNumeric(a2kInamg, Len.INAMG), Len.INAMG);
    }

    /**Original name: A2K-INAMG<br>*/
    public int getA2kInamg() {
        return readNumDispUnsignedInt(Pos.INAMG, Len.INAMG);
    }

    public String getInss03Formatted() {
        return readFixedString(Pos.INSS03, Len.INSS03);
    }

    public String getInaa03Formatted() {
        return readFixedString(Pos.INAA03, Len.INAA03);
    }

    public String getInmm03Formatted() {
        return readFixedString(Pos.INMM03, Len.INMM03);
    }

    public String getIngg03Formatted() {
        return readFixedString(Pos.INGG03, Len.INGG03);
    }

    /**Original name: A2K-INAMGP<br>*/
    public int getInamgp() {
        return readPackedAsInt(Pos.INAMGP, Len.Int.INAMGP);
    }

    /**Original name: A2K-INAMG0P<br>*/
    public int getInamg0p() {
        return readPackedAsInt(Pos.INAMG0P, Len.Int.INAMG0P);
    }

    /**Original name: A2K-INPROG9<br>*/
    public int getInprog9() {
        return readPackedAsInt(Pos.INPROG9, Len.Int.INPROG9);
    }

    /**Original name: A2K-INPROG<br>*/
    public int getInprog() {
        return readPackedAsInt(Pos.INPROG, Len.Int.INPROG);
    }

    public String getInjulFormatted() {
        return readFixedString(Pos.INJUL, Len.INJUL);
    }

    /**Original name: A2K-INJUL-X<br>*/
    public byte[] getInjulXBytes() {
        byte[] buffer = new byte[Len.INJUL_X];
        return getInjulXBytes(buffer, 1);
    }

    public byte[] getInjulXBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.INJUL_X, Pos.INJUL_X);
        return buffer;
    }

    public String getInjssFormatted() {
        return readFixedString(Pos.INJSS, Len.INJSS);
    }

    public String getInjaaFormatted() {
        return readFixedString(Pos.INJAA, Len.INJAA);
    }

    /**Original name: A2K-INJGGG<br>*/
    public short getInjggg() {
        return readNumDispUnsignedShort(Pos.INJGGG, Len.INJGGG);
    }

    /**Original name: A2K-INPROGC<br>*/
    public int getInprogc() {
        return readPackedAsInt(Pos.INPROGC, Len.Int.INPROGC);
    }

    /**Original name: A2K-INPROCO<br>*/
    public int getInproco() {
        return readPackedAsInt(Pos.INPROCO, Len.Int.INPROCO);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A2K_INDATA = 1;
        public static final int FILLER_A2K_INPUT = 1;
        public static final int INGMA = FILLER_A2K_INPUT;
        public static final int INGMA_X = INGMA;
        public static final int INGG = INGMA_X;
        public static final int INMM = INGG + Len.INGG;
        public static final int INSS = INMM + Len.INMM;
        public static final int INAA = INSS + Len.INSS;
        public static final int FLR1 = INGMA + Len.INGMA;
        public static final int FILLER_A2K_INPUT2 = 1;
        public static final int INGBMBA = FILLER_A2K_INPUT2;
        public static final int INGG02 = INGBMBA;
        public static final int INS102 = INGG02 + Len.INGG02;
        public static final int INMM02 = INS102 + Len.INS102;
        public static final int INS202 = INMM02 + Len.INMM02;
        public static final int INSS02 = INS202 + Len.INS202;
        public static final int INAA02 = INSS02 + Len.INSS02;
        public static final int FILLER_A2K_INPUT3 = 1;
        public static final int INAMG = FILLER_A2K_INPUT3;
        public static final int INAMG_X = INAMG;
        public static final int INSS03 = INAMG_X;
        public static final int INAA03 = INSS03 + Len.INSS03;
        public static final int INMM03 = INAA03 + Len.INAA03;
        public static final int INGG03 = INMM03 + Len.INMM03;
        public static final int FLR2 = INAMG + Len.INAMG;
        public static final int FILLER_A2K_INPUT5 = 1;
        public static final int INAMGP = FILLER_A2K_INPUT5;
        public static final int FLR3 = INAMGP + Len.INAMGP;
        public static final int FILLER_A2K_INPUT7 = 1;
        public static final int INAMG0P = FILLER_A2K_INPUT7;
        public static final int FLR4 = INAMG0P + Len.INAMG0P;
        public static final int FILLER_A2K_INPUT9 = 1;
        public static final int INPROG9 = FILLER_A2K_INPUT9;
        public static final int FLR5 = INPROG9 + Len.INPROG9;
        public static final int FILLER_A2K_INPUT11 = 1;
        public static final int INPROG = FILLER_A2K_INPUT11;
        public static final int FLR6 = INPROG + Len.INPROG;
        public static final int FILLER_A2K_INPUT13 = 1;
        public static final int INJUL = FILLER_A2K_INPUT13;
        public static final int INJUL_X = INJUL;
        public static final int INJSS = INJUL_X;
        public static final int INJAA = INJSS + Len.INJSS;
        public static final int INJGGG = INJAA + Len.INJAA;
        public static final int FLR7 = INJUL + Len.INJUL;
        public static final int FILLER_A2K_INPUT15 = 1;
        public static final int INPROGC = FILLER_A2K_INPUT15;
        public static final int FLR8 = INPROGC + Len.INPROGC;
        public static final int FILLER_A2K_INPUT17 = 1;
        public static final int INPROCO = FILLER_A2K_INPUT17;
        public static final int FLR9 = INPROCO + Len.INPROCO;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int INGG = 2;
        public static final int INMM = 2;
        public static final int INSS = 2;
        public static final int INGMA = 8;
        public static final int INGG02 = 2;
        public static final int INS102 = 1;
        public static final int INMM02 = 2;
        public static final int INS202 = 1;
        public static final int INSS02 = 2;
        public static final int INSS03 = 2;
        public static final int INAA03 = 2;
        public static final int INMM03 = 2;
        public static final int INAMG = 8;
        public static final int INAMGP = 5;
        public static final int INAMG0P = 5;
        public static final int INPROG9 = 3;
        public static final int INPROG = 4;
        public static final int INJSS = 2;
        public static final int INJAA = 2;
        public static final int INJUL = 7;
        public static final int INPROGC = 3;
        public static final int INPROCO = 4;
        public static final int A2K_INDATA = 10;
        public static final int INAA = 2;
        public static final int INGMA_X = INGG + INMM + INSS + INAA;
        public static final int INAA02 = 2;
        public static final int INGG03 = 2;
        public static final int INJGGG = 3;
        public static final int INJUL_X = INJSS + INJAA + INJGGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A2K_INAMG = 8;
            public static final int INAMGP = 9;
            public static final int INAMG0P = 9;
            public static final int INPROG9 = 5;
            public static final int INPROG = 7;
            public static final int INPROGC = 5;
            public static final int INPROCO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
