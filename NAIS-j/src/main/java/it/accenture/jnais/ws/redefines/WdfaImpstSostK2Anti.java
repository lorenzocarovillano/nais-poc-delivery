package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-SOST-K2-ANTI<br>
 * Variable: WDFA-IMPST-SOST-K2-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstSostK2Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstSostK2Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_SOST_K2_ANTI;
    }

    public void setWdfaImpstSostK2Anti(AfDecimal wdfaImpstSostK2Anti) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_SOST_K2_ANTI, wdfaImpstSostK2Anti.copy());
    }

    public void setWdfaImpstSostK2AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_SOST_K2_ANTI, Pos.WDFA_IMPST_SOST_K2_ANTI);
    }

    /**Original name: WDFA-IMPST-SOST-K2-ANTI<br>*/
    public AfDecimal getWdfaImpstSostK2Anti() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_SOST_K2_ANTI, Len.Int.WDFA_IMPST_SOST_K2_ANTI, Len.Fract.WDFA_IMPST_SOST_K2_ANTI);
    }

    public byte[] getWdfaImpstSostK2AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_SOST_K2_ANTI, Pos.WDFA_IMPST_SOST_K2_ANTI);
        return buffer;
    }

    public void setWdfaImpstSostK2AntiNull(String wdfaImpstSostK2AntiNull) {
        writeString(Pos.WDFA_IMPST_SOST_K2_ANTI_NULL, wdfaImpstSostK2AntiNull, Len.WDFA_IMPST_SOST_K2_ANTI_NULL);
    }

    /**Original name: WDFA-IMPST-SOST-K2-ANTI-NULL<br>*/
    public String getWdfaImpstSostK2AntiNull() {
        return readString(Pos.WDFA_IMPST_SOST_K2_ANTI_NULL, Len.WDFA_IMPST_SOST_K2_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K2_ANTI = 1;
        public static final int WDFA_IMPST_SOST_K2_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K2_ANTI = 8;
        public static final int WDFA_IMPST_SOST_K2_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K2_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K2_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
