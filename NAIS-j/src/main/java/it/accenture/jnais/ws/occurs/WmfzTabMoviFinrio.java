package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvmfz1;
import it.accenture.jnais.copy.WmfzDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WMFZ-TAB-MOVI-FINRIO<br>
 * Variables: WMFZ-TAB-MOVI-FINRIO from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WmfzTabMoviFinrio {

    //==== PROPERTIES ====
    //Original name: LCCVMFZ1
    private Lccvmfz1 lccvmfz1 = new Lccvmfz1();

    //==== METHODS ====
    public void setWmfzTabMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvmfz1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvmfz1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvmfz1.Len.Int.ID_PTF, 0));
        position += Lccvmfz1.Len.ID_PTF;
        lccvmfz1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWmfzTabMoviFinrioBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvmfz1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvmfz1.getIdPtf(), Lccvmfz1.Len.Int.ID_PTF, 0);
        position += Lccvmfz1.Len.ID_PTF;
        lccvmfz1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWmfzTabMoviFinrioSpaces() {
        lccvmfz1.initLccvmfz1Spaces();
    }

    public Lccvmfz1 getLccvmfz1() {
        return lccvmfz1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_TAB_MOVI_FINRIO = WpolStatus.Len.STATUS + Lccvmfz1.Len.ID_PTF + WmfzDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
