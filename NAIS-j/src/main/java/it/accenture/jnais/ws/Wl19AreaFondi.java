package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Wl19TabFnd;

/**Original name: WL19-AREA-FONDI<br>
 * Variable: WL19-AREA-FONDI from program LCCS0450<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Wl19AreaFondi extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_FND_MAXOCCURS = 250;
    /**Original name: WL19-ELE-FND-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short eleFndMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WL19-TAB-FND
    private Wl19TabFnd[] tabFnd = new Wl19TabFnd[TAB_FND_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Wl19AreaFondi() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL19_AREA_FONDI;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWl19AreaFondiBytes(buf);
    }

    public void init() {
        for (int tabFndIdx = 1; tabFndIdx <= TAB_FND_MAXOCCURS; tabFndIdx++) {
            tabFnd[tabFndIdx - 1] = new Wl19TabFnd();
        }
    }

    public void setWl19AreaFondiBytes(byte[] buffer) {
        setWl19AreaFondiBytes(buffer, 1);
    }

    public byte[] getWl19AreaFondiBytes() {
        byte[] buffer = new byte[Len.WL19_AREA_FONDI];
        return getWl19AreaFondiBytes(buffer, 1);
    }

    public void setWl19AreaFondiBytes(byte[] buffer, int offset) {
        int position = offset;
        eleFndMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setTabellaBytes(buffer, position);
    }

    public byte[] getWl19AreaFondiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleFndMax);
        position += Types.SHORT_SIZE;
        getTabellaBytes(buffer, position);
        return buffer;
    }

    public void setEleFndMax(short eleFndMax) {
        this.eleFndMax = eleFndMax;
    }

    public short getEleFndMax() {
        return this.eleFndMax;
    }

    public void setTabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_FND_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabFnd[idx - 1].setWl19TabFndBytes(buffer, position);
                position += Wl19TabFnd.Len.WL19_TAB_FND;
            }
            else {
                tabFnd[idx - 1].initWl19TabFndSpaces();
                position += Wl19TabFnd.Len.WL19_TAB_FND;
            }
        }
    }

    public byte[] getTabellaBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= TAB_FND_MAXOCCURS; idx++) {
            tabFnd[idx - 1].getWl19TabFndBytes(buffer, position);
            position += Wl19TabFnd.Len.WL19_TAB_FND;
        }
        return buffer;
    }

    public Wl19TabFnd getTabFnd(int idx) {
        return tabFnd[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWl19AreaFondiBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_FND_MAX = 2;
        public static final int TABELLA = Wl19AreaFondi.TAB_FND_MAXOCCURS * Wl19TabFnd.Len.WL19_TAB_FND;
        public static final int WL19_AREA_FONDI = ELE_FND_MAX + TABELLA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
