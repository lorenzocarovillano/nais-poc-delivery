package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0012-STRUTT-SERVIZIO-ELEM<br>
 * Variables: IDSV0012-STRUTT-SERVIZIO-ELEM from copybook IDSV0012<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Idsv0012StruttServizioElem {

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CODICE_STR_DATO_R = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
