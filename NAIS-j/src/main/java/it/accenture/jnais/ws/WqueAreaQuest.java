package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WqueTabQuest;

/**Original name: WQUE-AREA-QUEST<br>
 * Variable: WQUE-AREA-QUEST from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WqueAreaQuest extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_QUEST_MAXOCCURS = 12;
    //Original name: WQUE-ELE-QUEST-MAX
    private short eleQuestMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WQUE-TAB-QUEST
    private WqueTabQuest[] tabQuest = new WqueTabQuest[TAB_QUEST_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WqueAreaQuest() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WQUE_AREA_QUEST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWqueAreaQuestBytes(buf);
    }

    public void init() {
        for (int tabQuestIdx = 1; tabQuestIdx <= TAB_QUEST_MAXOCCURS; tabQuestIdx++) {
            tabQuest[tabQuestIdx - 1] = new WqueTabQuest();
        }
    }

    public String getWqueAreaQuestFormatted() {
        return MarshalByteExt.bufferToStr(getWqueAreaQuestBytes());
    }

    public void setWqueAreaQuestBytes(byte[] buffer) {
        setWqueAreaQuestBytes(buffer, 1);
    }

    public byte[] getWqueAreaQuestBytes() {
        byte[] buffer = new byte[Len.WQUE_AREA_QUEST];
        return getWqueAreaQuestBytes(buffer, 1);
    }

    public void setWqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        eleQuestMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_QUEST_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabQuest[idx - 1].setWqueTabQuestBytes(buffer, position);
                position += WqueTabQuest.Len.WQUE_TAB_QUEST;
            }
            else {
                tabQuest[idx - 1].initWqueTabQuestSpaces();
                position += WqueTabQuest.Len.WQUE_TAB_QUEST;
            }
        }
    }

    public byte[] getWqueAreaQuestBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleQuestMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_QUEST_MAXOCCURS; idx++) {
            tabQuest[idx - 1].getWqueTabQuestBytes(buffer, position);
            position += WqueTabQuest.Len.WQUE_TAB_QUEST;
        }
        return buffer;
    }

    public void setEleQuestMax(short eleQuestMax) {
        this.eleQuestMax = eleQuestMax;
    }

    public short getEleQuestMax() {
        return this.eleQuestMax;
    }

    public WqueTabQuest getTabQuest(int idx) {
        return tabQuest[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWqueAreaQuestBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_QUEST_MAX = 2;
        public static final int WQUE_AREA_QUEST = ELE_QUEST_MAX + WqueAreaQuest.TAB_QUEST_MAXOCCURS * WqueTabQuest.Len.WQUE_TAB_QUEST;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
