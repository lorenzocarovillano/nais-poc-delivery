package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCC0490-PRESENZA-RATEO<br>
 * Variable: LCCC0490-PRESENZA-RATEO from copybook LCCC0490<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0490PresenzaRateo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_PRESENZA_RATEO = 'S';
    public static final char NO_PRESENZA_RATEO = 'N';

    //==== METHODS ====
    public void setPresenzaRateo(char presenzaRateo) {
        this.value = presenzaRateo;
    }

    public char getPresenzaRateo() {
        return this.value;
    }

    public boolean isLccc0490SiPresenzaRateo() {
        return value == SI_PRESENZA_RATEO;
    }

    public void setSiPresenzaRateo() {
        value = SI_PRESENZA_RATEO;
    }

    public void setNoPresenzaRateo() {
        value = NO_PRESENZA_RATEO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PRESENZA_RATEO = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
