package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-RB-CL<br>
 * Variable: WPCO-DT-ULTC-RB-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcRbCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcRbCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_RB_CL;
    }

    public void setWpcoDtUltcRbCl(int wpcoDtUltcRbCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_RB_CL, wpcoDtUltcRbCl, Len.Int.WPCO_DT_ULTC_RB_CL);
    }

    public void setDpcoDtUltcRbClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_RB_CL, Pos.WPCO_DT_ULTC_RB_CL);
    }

    /**Original name: WPCO-DT-ULTC-RB-CL<br>*/
    public int getWpcoDtUltcRbCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_RB_CL, Len.Int.WPCO_DT_ULTC_RB_CL);
    }

    public byte[] getWpcoDtUltcRbClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_RB_CL, Pos.WPCO_DT_ULTC_RB_CL);
        return buffer;
    }

    public void setWpcoDtUltcRbClNull(String wpcoDtUltcRbClNull) {
        writeString(Pos.WPCO_DT_ULTC_RB_CL_NULL, wpcoDtUltcRbClNull, Len.WPCO_DT_ULTC_RB_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTC-RB-CL-NULL<br>*/
    public String getWpcoDtUltcRbClNull() {
        return readString(Pos.WPCO_DT_ULTC_RB_CL_NULL, Len.WPCO_DT_ULTC_RB_CL_NULL);
    }

    public String getWpcoDtUltcRbClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcRbClNull(), Len.WPCO_DT_ULTC_RB_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_RB_CL = 1;
        public static final int WPCO_DT_ULTC_RB_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_RB_CL = 5;
        public static final int WPCO_DT_ULTC_RB_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_RB_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
