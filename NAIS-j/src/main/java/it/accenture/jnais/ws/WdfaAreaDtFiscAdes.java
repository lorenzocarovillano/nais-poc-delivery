package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvdfa1;
import it.accenture.jnais.copy.WdfaDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WDFA-AREA-DT-FISC-ADES<br>
 * Variable: WDFA-AREA-DT-FISC-ADES from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WdfaAreaDtFiscAdes extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WDFA-ELE-FISC-ADES-MAX
    private short wdfaEleFiscAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDFA1
    private Lccvdfa1 lccvdfa1 = new Lccvdfa1();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_AREA_DT_FISC_ADES;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWdfaAreaDtFiscAdesBytes(buf);
    }

    public String getWdfaAreaDtFiscAdesFormatted() {
        return MarshalByteExt.bufferToStr(getWdfaAreaDtFiscAdesBytes());
    }

    public void setWdfaAreaDtFiscAdesBytes(byte[] buffer) {
        setWdfaAreaDtFiscAdesBytes(buffer, 1);
    }

    public byte[] getWdfaAreaDtFiscAdesBytes() {
        byte[] buffer = new byte[Len.WDFA_AREA_DT_FISC_ADES];
        return getWdfaAreaDtFiscAdesBytes(buffer, 1);
    }

    public void setWdfaAreaDtFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        wdfaEleFiscAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWdfaTabFiscAdesBytes(buffer, position);
    }

    public byte[] getWdfaAreaDtFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wdfaEleFiscAdesMax);
        position += Types.SHORT_SIZE;
        getWdfaTabFiscAdesBytes(buffer, position);
        return buffer;
    }

    public void setWdfaEleFiscAdesMax(short wdfaEleFiscAdesMax) {
        this.wdfaEleFiscAdesMax = wdfaEleFiscAdesMax;
    }

    public short getWdfaEleFiscAdesMax() {
        return this.wdfaEleFiscAdesMax;
    }

    public void setWdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvdfa1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvdfa1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvdfa1.Len.Int.ID_PTF, 0));
        position += Lccvdfa1.Len.ID_PTF;
        lccvdfa1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWdfaTabFiscAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvdfa1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvdfa1.getIdPtf(), Lccvdfa1.Len.Int.ID_PTF, 0);
        position += Lccvdfa1.Len.ID_PTF;
        lccvdfa1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    @Override
    public byte[] serialize() {
        return getWdfaAreaDtFiscAdesBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_ELE_FISC_ADES_MAX = 2;
        public static final int WDFA_TAB_FISC_ADES = WpolStatus.Len.STATUS + Lccvdfa1.Len.ID_PTF + WdfaDati.Len.DATI;
        public static final int WDFA_AREA_DT_FISC_ADES = WDFA_ELE_FISC_ADES_MAX + WDFA_TAB_FISC_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
