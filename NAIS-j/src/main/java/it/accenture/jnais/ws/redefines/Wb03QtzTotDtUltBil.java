package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-QTZ-TOT-DT-ULT-BIL<br>
 * Variable: WB03-QTZ-TOT-DT-ULT-BIL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03QtzTotDtUltBil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03QtzTotDtUltBil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_QTZ_TOT_DT_ULT_BIL;
    }

    public void setWb03QtzTotDtUltBilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_QTZ_TOT_DT_ULT_BIL, Pos.WB03_QTZ_TOT_DT_ULT_BIL);
    }

    /**Original name: WB03-QTZ-TOT-DT-ULT-BIL<br>*/
    public AfDecimal getWb03QtzTotDtUltBil() {
        return readPackedAsDecimal(Pos.WB03_QTZ_TOT_DT_ULT_BIL, Len.Int.WB03_QTZ_TOT_DT_ULT_BIL, Len.Fract.WB03_QTZ_TOT_DT_ULT_BIL);
    }

    public byte[] getWb03QtzTotDtUltBilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_QTZ_TOT_DT_ULT_BIL, Pos.WB03_QTZ_TOT_DT_ULT_BIL);
        return buffer;
    }

    public void setWb03QtzTotDtUltBilNull(String wb03QtzTotDtUltBilNull) {
        writeString(Pos.WB03_QTZ_TOT_DT_ULT_BIL_NULL, wb03QtzTotDtUltBilNull, Len.WB03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    /**Original name: WB03-QTZ-TOT-DT-ULT-BIL-NULL<br>*/
    public String getWb03QtzTotDtUltBilNull() {
        return readString(Pos.WB03_QTZ_TOT_DT_ULT_BIL_NULL, Len.WB03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    public String getWb03QtzTotDtUltBilNullFormatted() {
        return Functions.padBlanks(getWb03QtzTotDtUltBilNull(), Len.WB03_QTZ_TOT_DT_ULT_BIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_TOT_DT_ULT_BIL = 1;
        public static final int WB03_QTZ_TOT_DT_ULT_BIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_QTZ_TOT_DT_ULT_BIL = 7;
        public static final int WB03_QTZ_TOT_DT_ULT_BIL_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_TOT_DT_ULT_BIL = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_QTZ_TOT_DT_ULT_BIL = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
