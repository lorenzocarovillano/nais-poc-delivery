package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WK-DATA-SUPERIORE<br>
 * Variable: WK-DATA-SUPERIORE from program LCCS1750<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDataSuperiore extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDataSuperiore() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DATA_SUPERIORE;
    }

    public String getWkDataSuperioreFormatted() {
        return readFixedString(Pos.WK_DATA_SUPERIORE, Len.WK_DATA_SUPERIORE);
    }

    public void setWkDataSuperioreBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WK_DATA_SUPERIORE, Pos.WK_DATA_SUPERIORE);
    }

    public byte[] getWkDataSuperioreBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WK_DATA_SUPERIORE, Pos.WK_DATA_SUPERIORE);
        return buffer;
    }

    /**Original name: WK-GG-SUP<br>*/
    public short getGgSup() {
        return readNumDispUnsignedShort(Pos.GG_SUP, Len.GG_SUP);
    }

    public void setWkDataSuperioreNFormatted(String wkDataSuperioreN) {
        writeString(Pos.WK_DATA_SUPERIORE_N, Trunc.toUnsignedNumeric(wkDataSuperioreN, Len.WK_DATA_SUPERIORE_N), Len.WK_DATA_SUPERIORE_N);
    }

    /**Original name: WK-DATA-SUPERIORE-N<br>*/
    public int getWkDataSuperioreN() {
        return readNumDispUnsignedInt(Pos.WK_DATA_SUPERIORE_N, Len.WK_DATA_SUPERIORE_N);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DATA_SUPERIORE = 1;
        public static final int AAAA_SUP = WK_DATA_SUPERIORE;
        public static final int MM_SUP = AAAA_SUP + Len.AAAA_SUP;
        public static final int GG_SUP = MM_SUP + Len.MM_SUP;
        public static final int WK_DATA_SUPERIORE_N = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_SUP = 4;
        public static final int MM_SUP = 2;
        public static final int GG_SUP = 2;
        public static final int WK_DATA_SUPERIORE = AAAA_SUP + MM_SUP + GG_SUP;
        public static final int WK_DATA_SUPERIORE_N = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
