package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-PERV-DEN<br>
 * Variable: LQU-DT-PERV-DEN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtPervDen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtPervDen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_PERV_DEN;
    }

    public void setLquDtPervDen(int lquDtPervDen) {
        writeIntAsPacked(Pos.LQU_DT_PERV_DEN, lquDtPervDen, Len.Int.LQU_DT_PERV_DEN);
    }

    public void setLquDtPervDenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_PERV_DEN, Pos.LQU_DT_PERV_DEN);
    }

    /**Original name: LQU-DT-PERV-DEN<br>*/
    public int getLquDtPervDen() {
        return readPackedAsInt(Pos.LQU_DT_PERV_DEN, Len.Int.LQU_DT_PERV_DEN);
    }

    public byte[] getLquDtPervDenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_PERV_DEN, Pos.LQU_DT_PERV_DEN);
        return buffer;
    }

    public void setLquDtPervDenNull(String lquDtPervDenNull) {
        writeString(Pos.LQU_DT_PERV_DEN_NULL, lquDtPervDenNull, Len.LQU_DT_PERV_DEN_NULL);
    }

    /**Original name: LQU-DT-PERV-DEN-NULL<br>*/
    public String getLquDtPervDenNull() {
        return readString(Pos.LQU_DT_PERV_DEN_NULL, Len.LQU_DT_PERV_DEN_NULL);
    }

    public String getLquDtPervDenNullFormatted() {
        return Functions.padBlanks(getLquDtPervDenNull(), Len.LQU_DT_PERV_DEN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_PERV_DEN = 1;
        public static final int LQU_DT_PERV_DEN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_PERV_DEN = 5;
        public static final int LQU_DT_PERV_DEN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_PERV_DEN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
