package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ANTIDUR-RICOR-PREC<br>
 * Variable: WB03-ANTIDUR-RICOR-PREC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03AntidurRicorPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03AntidurRicorPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ANTIDUR_RICOR_PREC;
    }

    public void setWb03AntidurRicorPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ANTIDUR_RICOR_PREC, Pos.WB03_ANTIDUR_RICOR_PREC);
    }

    /**Original name: WB03-ANTIDUR-RICOR-PREC<br>*/
    public int getWb03AntidurRicorPrec() {
        return readPackedAsInt(Pos.WB03_ANTIDUR_RICOR_PREC, Len.Int.WB03_ANTIDUR_RICOR_PREC);
    }

    public byte[] getWb03AntidurRicorPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ANTIDUR_RICOR_PREC, Pos.WB03_ANTIDUR_RICOR_PREC);
        return buffer;
    }

    public void setWb03AntidurRicorPrecNull(String wb03AntidurRicorPrecNull) {
        writeString(Pos.WB03_ANTIDUR_RICOR_PREC_NULL, wb03AntidurRicorPrecNull, Len.WB03_ANTIDUR_RICOR_PREC_NULL);
    }

    /**Original name: WB03-ANTIDUR-RICOR-PREC-NULL<br>*/
    public String getWb03AntidurRicorPrecNull() {
        return readString(Pos.WB03_ANTIDUR_RICOR_PREC_NULL, Len.WB03_ANTIDUR_RICOR_PREC_NULL);
    }

    public String getWb03AntidurRicorPrecNullFormatted() {
        return Functions.padBlanks(getWb03AntidurRicorPrecNull(), Len.WB03_ANTIDUR_RICOR_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ANTIDUR_RICOR_PREC = 1;
        public static final int WB03_ANTIDUR_RICOR_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ANTIDUR_RICOR_PREC = 3;
        public static final int WB03_ANTIDUR_RICOR_PREC_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ANTIDUR_RICOR_PREC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
