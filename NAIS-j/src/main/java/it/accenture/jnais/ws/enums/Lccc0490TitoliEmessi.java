package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LCCC0490-TITOLI-EMESSI<br>
 * Variable: LCCC0490-TITOLI-EMESSI from copybook LCCC0490<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0490TitoliEmessi {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_TIT_EMESSI = 'S';
    public static final char NO_TIT_EMESSI = 'N';

    //==== METHODS ====
    public void setTitoliEmessi(char titoliEmessi) {
        this.value = titoliEmessi;
    }

    public char getTitoliEmessi() {
        return this.value;
    }

    public boolean isLccc0490SiTitEmessi() {
        return value == SI_TIT_EMESSI;
    }

    public void setSiTitEmessi() {
        value = SI_TIT_EMESSI;
    }

    public void setNoTitEmessi() {
        value = NO_TIT_EMESSI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TITOLI_EMESSI = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
