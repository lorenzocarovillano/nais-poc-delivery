package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-TP-INVST<br>
 * Variable: WGRZ-TP-INVST from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzTpInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzTpInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_TP_INVST;
    }

    public void setWgrzTpInvst(short wgrzTpInvst) {
        writeShortAsPacked(Pos.WGRZ_TP_INVST, wgrzTpInvst, Len.Int.WGRZ_TP_INVST);
    }

    public void setWgrzTpInvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_TP_INVST, Pos.WGRZ_TP_INVST);
    }

    /**Original name: WGRZ-TP-INVST<br>*/
    public short getWgrzTpInvst() {
        return readPackedAsShort(Pos.WGRZ_TP_INVST, Len.Int.WGRZ_TP_INVST);
    }

    public byte[] getWgrzTpInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_TP_INVST, Pos.WGRZ_TP_INVST);
        return buffer;
    }

    public void initWgrzTpInvstSpaces() {
        fill(Pos.WGRZ_TP_INVST, Len.WGRZ_TP_INVST, Types.SPACE_CHAR);
    }

    public void setWgrzTpInvstNull(String wgrzTpInvstNull) {
        writeString(Pos.WGRZ_TP_INVST_NULL, wgrzTpInvstNull, Len.WGRZ_TP_INVST_NULL);
    }

    /**Original name: WGRZ-TP-INVST-NULL<br>*/
    public String getWgrzTpInvstNull() {
        return readString(Pos.WGRZ_TP_INVST_NULL, Len.WGRZ_TP_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_TP_INVST = 1;
        public static final int WGRZ_TP_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_TP_INVST = 2;
        public static final int WGRZ_TP_INVST_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_TP_INVST = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
