package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndOggCollg;
import it.accenture.jnais.copy.Ldbv0721;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0720<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0720Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-OGG-COLLG
    private IndOggCollg indOggCollg = new IndOggCollg();
    //Original name: OGG-COLLG-DB
    private DettTitContDb oggCollgDb = new DettTitContDb();
    //Original name: LDBV0721
    private Ldbv0721 ldbv0721 = new Ldbv0721();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndOggCollg getIndOggCollg() {
        return indOggCollg;
    }

    public Ldbv0721 getLdbv0721() {
        return ldbv0721;
    }

    public DettTitContDb getOggCollgDb() {
        return oggCollgDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
