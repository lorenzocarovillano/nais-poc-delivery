package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WK-DATA-AMG-POL<br>
 * Variable: WK-DATA-AMG-POL from program LVVS0127<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDataAmgPol extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDataAmgPol() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DATA_AMG_POL;
    }

    @Override
    public void init() {
        int position = 1;
        writeShort(position, ((short)0), Len.Int.AAAA_POL, SignType.NO_SIGN);
        position += Len.AAAA_POL;
        writeShort(position, ((short)0), Len.Int.MM_POL, SignType.NO_SIGN);
        position += Len.MM_POL;
        writeShort(position, ((short)0), Len.Int.GG_POL, SignType.NO_SIGN);
    }

    public void setAaaaPolFormatted(String aaaaPol) {
        writeString(Pos.AAAA_POL, Trunc.toUnsignedNumeric(aaaaPol, Len.AAAA_POL), Len.AAAA_POL);
    }

    /**Original name: WK-AAAA-POL<br>*/
    public short getAaaaPol() {
        return readNumDispUnsignedShort(Pos.AAAA_POL, Len.AAAA_POL);
    }

    public void setMmPolFormatted(String mmPol) {
        writeString(Pos.MM_POL, Trunc.toUnsignedNumeric(mmPol, Len.MM_POL), Len.MM_POL);
    }

    /**Original name: WK-MM-POL<br>*/
    public short getMmPol() {
        return readNumDispUnsignedShort(Pos.MM_POL, Len.MM_POL);
    }

    public void setGgPolFormatted(String ggPol) {
        writeString(Pos.GG_POL, Trunc.toUnsignedNumeric(ggPol, Len.GG_POL), Len.GG_POL);
    }

    /**Original name: WK-GG-POL<br>*/
    public short getGgPol() {
        return readNumDispUnsignedShort(Pos.GG_POL, Len.GG_POL);
    }

    public void setWkDataAmgPolRFormatted(String wkDataAmgPolR) {
        writeString(Pos.WK_DATA_AMG_POL_R, Trunc.toUnsignedNumeric(wkDataAmgPolR, Len.WK_DATA_AMG_POL_R), Len.WK_DATA_AMG_POL_R);
    }

    /**Original name: WK-DATA-AMG-POL-R<br>*/
    public int getWkDataAmgPolR() {
        return readNumDispUnsignedInt(Pos.WK_DATA_AMG_POL_R, Len.WK_DATA_AMG_POL_R);
    }

    public String getWkDataAmgPolRFormatted() {
        return readFixedString(Pos.WK_DATA_AMG_POL_R, Len.WK_DATA_AMG_POL_R);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DATA_AMG_POL = 1;
        public static final int AAAA_POL = WK_DATA_AMG_POL;
        public static final int MM_POL = AAAA_POL + Len.AAAA_POL;
        public static final int GG_POL = MM_POL + Len.MM_POL;
        public static final int WK_DATA_AMG_POL_R = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_POL = 4;
        public static final int MM_POL = 2;
        public static final int GG_POL = 2;
        public static final int WK_DATA_AMG_POL = AAAA_POL + MM_POL + GG_POL;
        public static final int WK_DATA_AMG_POL_R = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int AAAA_POL = 4;
            public static final int MM_POL = 2;
            public static final int GG_POL = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
