package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-RISTORNI-CAP<br>
 * Variable: WRST-RIS-RISTORNI-CAP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisRistorniCap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisRistorniCap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_RISTORNI_CAP;
    }

    public void setWrstRisRistorniCap(AfDecimal wrstRisRistorniCap) {
        writeDecimalAsPacked(Pos.WRST_RIS_RISTORNI_CAP, wrstRisRistorniCap.copy());
    }

    public void setWrstRisRistorniCapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_RISTORNI_CAP, Pos.WRST_RIS_RISTORNI_CAP);
    }

    /**Original name: WRST-RIS-RISTORNI-CAP<br>*/
    public AfDecimal getWrstRisRistorniCap() {
        return readPackedAsDecimal(Pos.WRST_RIS_RISTORNI_CAP, Len.Int.WRST_RIS_RISTORNI_CAP, Len.Fract.WRST_RIS_RISTORNI_CAP);
    }

    public byte[] getWrstRisRistorniCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_RISTORNI_CAP, Pos.WRST_RIS_RISTORNI_CAP);
        return buffer;
    }

    public void initWrstRisRistorniCapSpaces() {
        fill(Pos.WRST_RIS_RISTORNI_CAP, Len.WRST_RIS_RISTORNI_CAP, Types.SPACE_CHAR);
    }

    public void setWrstRisRistorniCapNull(String wrstRisRistorniCapNull) {
        writeString(Pos.WRST_RIS_RISTORNI_CAP_NULL, wrstRisRistorniCapNull, Len.WRST_RIS_RISTORNI_CAP_NULL);
    }

    /**Original name: WRST-RIS-RISTORNI-CAP-NULL<br>*/
    public String getWrstRisRistorniCapNull() {
        return readString(Pos.WRST_RIS_RISTORNI_CAP_NULL, Len.WRST_RIS_RISTORNI_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_RISTORNI_CAP = 1;
        public static final int WRST_RIS_RISTORNI_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_RISTORNI_CAP = 8;
        public static final int WRST_RIS_RISTORNI_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_RISTORNI_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_RISTORNI_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
