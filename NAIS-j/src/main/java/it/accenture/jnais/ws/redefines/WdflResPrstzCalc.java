package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RES-PRSTZ-CALC<br>
 * Variable: WDFL-RES-PRSTZ-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflResPrstzCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflResPrstzCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RES_PRSTZ_CALC;
    }

    public void setWdflResPrstzCalc(AfDecimal wdflResPrstzCalc) {
        writeDecimalAsPacked(Pos.WDFL_RES_PRSTZ_CALC, wdflResPrstzCalc.copy());
    }

    public void setWdflResPrstzCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RES_PRSTZ_CALC, Pos.WDFL_RES_PRSTZ_CALC);
    }

    /**Original name: WDFL-RES-PRSTZ-CALC<br>*/
    public AfDecimal getWdflResPrstzCalc() {
        return readPackedAsDecimal(Pos.WDFL_RES_PRSTZ_CALC, Len.Int.WDFL_RES_PRSTZ_CALC, Len.Fract.WDFL_RES_PRSTZ_CALC);
    }

    public byte[] getWdflResPrstzCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RES_PRSTZ_CALC, Pos.WDFL_RES_PRSTZ_CALC);
        return buffer;
    }

    public void setWdflResPrstzCalcNull(String wdflResPrstzCalcNull) {
        writeString(Pos.WDFL_RES_PRSTZ_CALC_NULL, wdflResPrstzCalcNull, Len.WDFL_RES_PRSTZ_CALC_NULL);
    }

    /**Original name: WDFL-RES-PRSTZ-CALC-NULL<br>*/
    public String getWdflResPrstzCalcNull() {
        return readString(Pos.WDFL_RES_PRSTZ_CALC_NULL, Len.WDFL_RES_PRSTZ_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRSTZ_CALC = 1;
        public static final int WDFL_RES_PRSTZ_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RES_PRSTZ_CALC = 8;
        public static final int WDFL_RES_PRSTZ_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRSTZ_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RES_PRSTZ_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
