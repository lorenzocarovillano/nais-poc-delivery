package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IStatOggBus;

/**Original name: LDBV1701<br>
 * Variable: LDBV1701 from copybook LDBV1701<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv1701 extends SerializableParameter implements IStatOggBus {

    //==== PROPERTIES ====
    //Original name: LDBV1701-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LDBV1701-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LDBV1701-TP-CAUS
    private String tpCaus = DefaultValues.stringVal(Len.TP_CAUS);
    //Original name: LDBV1701-TP-STAT
    private String tpStat = DefaultValues.stringVal(Len.TP_STAT);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV1701;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv1701Bytes(buf);
    }

    public String getLdbv1701Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv1701Bytes());
    }

    public void setLdbv1701Bytes(byte[] buffer) {
        setLdbv1701Bytes(buffer, 1);
    }

    public byte[] getLdbv1701Bytes() {
        byte[] buffer = new byte[Len.LDBV1701];
        return getLdbv1701Bytes(buffer, 1);
    }

    public void setLdbv1701Bytes(byte[] buffer, int offset) {
        int position = offset;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        tpCaus = MarshalByte.readString(buffer, position, Len.TP_CAUS);
        position += Len.TP_CAUS;
        tpStat = MarshalByte.readString(buffer, position, Len.TP_STAT);
    }

    public byte[] getLdbv1701Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeString(buffer, position, tpCaus, Len.TP_CAUS);
        position += Len.TP_CAUS;
        MarshalByte.writeString(buffer, position, tpStat, Len.TP_STAT);
        return buffer;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    @Override
    public void setTpCaus(String tpCaus) {
        this.tpCaus = Functions.subString(tpCaus, Len.TP_CAUS);
    }

    @Override
    public String getTpCaus() {
        return this.tpCaus;
    }

    public void setTpStat(String tpStat) {
        this.tpStat = Functions.subString(tpStat, Len.TP_STAT);
    }

    public String getTpStat() {
        return this.tpStat;
    }

    @Override
    public String getCaus() {
        return getTpCaus();
    }

    @Override
    public void setCaus(String caus) {
        this.setTpCaus(caus);
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdStatOggBus() {
        throw new FieldNotMappedException("idStatOggBus");
    }

    @Override
    public void setIdStatOggBus(int idStatOggBus) {
        throw new FieldNotMappedException("idStatOggBus");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public int getLdbv1701IdOgg() {
        throw new FieldNotMappedException("ldbv1701IdOgg");
    }

    @Override
    public void setLdbv1701IdOgg(int ldbv1701IdOgg) {
        throw new FieldNotMappedException("ldbv1701IdOgg");
    }

    @Override
    public String getLdbv1701TpOgg() {
        throw new FieldNotMappedException("ldbv1701TpOgg");
    }

    @Override
    public void setLdbv1701TpOgg(String ldbv1701TpOgg) {
        throw new FieldNotMappedException("ldbv1701TpOgg");
    }

    @Override
    public String getStat() {
        return getTpStat();
    }

    @Override
    public void setStat(String stat) {
        this.setTpStat(stat);
    }

    @Override
    public long getStbDsRiga() {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public int getStbIdOgg() {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public String getStbTpOgg() {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public String getTpStatBus() {
        throw new FieldNotMappedException("tpStatBus");
    }

    @Override
    public void setTpStatBus(String tpStatBus) {
        throw new FieldNotMappedException("tpStatBus");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public byte[] serialize() {
        return getLdbv1701Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int TP_CAUS = 2;
        public static final int TP_STAT = 2;
        public static final int LDBV1701 = ID_OGG + TP_OGG + TP_CAUS + TP_STAT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
