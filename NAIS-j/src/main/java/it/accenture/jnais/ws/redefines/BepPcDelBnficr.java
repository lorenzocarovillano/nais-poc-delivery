package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEP-PC-DEL-BNFICR<br>
 * Variable: BEP-PC-DEL-BNFICR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BepPcDelBnficr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BepPcDelBnficr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEP_PC_DEL_BNFICR;
    }

    public void setBepPcDelBnficr(AfDecimal bepPcDelBnficr) {
        writeDecimalAsPacked(Pos.BEP_PC_DEL_BNFICR, bepPcDelBnficr.copy());
    }

    public void setBepPcDelBnficrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEP_PC_DEL_BNFICR, Pos.BEP_PC_DEL_BNFICR);
    }

    /**Original name: BEP-PC-DEL-BNFICR<br>*/
    public AfDecimal getBepPcDelBnficr() {
        return readPackedAsDecimal(Pos.BEP_PC_DEL_BNFICR, Len.Int.BEP_PC_DEL_BNFICR, Len.Fract.BEP_PC_DEL_BNFICR);
    }

    public byte[] getBepPcDelBnficrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEP_PC_DEL_BNFICR, Pos.BEP_PC_DEL_BNFICR);
        return buffer;
    }

    public void setBepPcDelBnficrNull(String bepPcDelBnficrNull) {
        writeString(Pos.BEP_PC_DEL_BNFICR_NULL, bepPcDelBnficrNull, Len.BEP_PC_DEL_BNFICR_NULL);
    }

    /**Original name: BEP-PC-DEL-BNFICR-NULL<br>*/
    public String getBepPcDelBnficrNull() {
        return readString(Pos.BEP_PC_DEL_BNFICR_NULL, Len.BEP_PC_DEL_BNFICR_NULL);
    }

    public String getBepPcDelBnficrNullFormatted() {
        return Functions.padBlanks(getBepPcDelBnficrNull(), Len.BEP_PC_DEL_BNFICR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEP_PC_DEL_BNFICR = 1;
        public static final int BEP_PC_DEL_BNFICR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEP_PC_DEL_BNFICR = 4;
        public static final int BEP_PC_DEL_BNFICR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEP_PC_DEL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEP_PC_DEL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
