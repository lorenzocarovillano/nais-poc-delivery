package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-BOLLO-DETT-D<br>
 * Variable: DFL-IMPST-BOLLO-DETT-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstBolloDettD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstBolloDettD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_BOLLO_DETT_D;
    }

    public void setDflImpstBolloDettD(AfDecimal dflImpstBolloDettD) {
        writeDecimalAsPacked(Pos.DFL_IMPST_BOLLO_DETT_D, dflImpstBolloDettD.copy());
    }

    public void setDflImpstBolloDettDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_BOLLO_DETT_D, Pos.DFL_IMPST_BOLLO_DETT_D);
    }

    /**Original name: DFL-IMPST-BOLLO-DETT-D<br>*/
    public AfDecimal getDflImpstBolloDettD() {
        return readPackedAsDecimal(Pos.DFL_IMPST_BOLLO_DETT_D, Len.Int.DFL_IMPST_BOLLO_DETT_D, Len.Fract.DFL_IMPST_BOLLO_DETT_D);
    }

    public byte[] getDflImpstBolloDettDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_BOLLO_DETT_D, Pos.DFL_IMPST_BOLLO_DETT_D);
        return buffer;
    }

    public void setDflImpstBolloDettDNull(String dflImpstBolloDettDNull) {
        writeString(Pos.DFL_IMPST_BOLLO_DETT_D_NULL, dflImpstBolloDettDNull, Len.DFL_IMPST_BOLLO_DETT_D_NULL);
    }

    /**Original name: DFL-IMPST-BOLLO-DETT-D-NULL<br>*/
    public String getDflImpstBolloDettDNull() {
        return readString(Pos.DFL_IMPST_BOLLO_DETT_D_NULL, Len.DFL_IMPST_BOLLO_DETT_D_NULL);
    }

    public String getDflImpstBolloDettDNullFormatted() {
        return Functions.padBlanks(getDflImpstBolloDettDNull(), Len.DFL_IMPST_BOLLO_DETT_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_DETT_D = 1;
        public static final int DFL_IMPST_BOLLO_DETT_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_DETT_D = 8;
        public static final int DFL_IMPST_BOLLO_DETT_D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_DETT_D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_DETT_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
