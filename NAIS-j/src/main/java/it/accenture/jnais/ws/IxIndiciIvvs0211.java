package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciIvvs0211 {

    //==== PROPERTIES ====
    //Original name: IND-LIVELLO
    private short livello = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-STR
    private short str = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-UNZIP
    private short unzip = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-START-VAR
    private short startVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-END-VAR
    private short endVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-DISTINCT
    private short distinct = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TGA
    private short tga = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GRZ
    private short grz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-GOP
    private short gop = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TOP
    private short top = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-COD-VAR
    private short codVar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TIPO-OPZ
    private short tipoOpz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-OPZ
    private short opz = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-CHAR
    private short charFld = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-TAB-DIST
    private short tabDist = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-RIGA
    private short riga = DefaultValues.BIN_SHORT_VAL;
    //Original name: IND-COL
    private short col = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setLivello(short livello) {
        this.livello = livello;
    }

    public short getLivello() {
        return this.livello;
    }

    public void setStr(short str) {
        this.str = str;
    }

    public short getStr() {
        return this.str;
    }

    public void setUnzip(short unzip) {
        this.unzip = unzip;
    }

    public short getUnzip() {
        return this.unzip;
    }

    public void setStartVar(short startVar) {
        this.startVar = startVar;
    }

    public short getStartVar() {
        return this.startVar;
    }

    public void setEndVar(short endVar) {
        this.endVar = endVar;
    }

    public short getEndVar() {
        return this.endVar;
    }

    public void setDistinct(short distinct) {
        this.distinct = distinct;
    }

    public short getDistinct() {
        return this.distinct;
    }

    public void setTga(short tga) {
        this.tga = tga;
    }

    public short getTga() {
        return this.tga;
    }

    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }

    public void setGop(short gop) {
        this.gop = gop;
    }

    public short getGop() {
        return this.gop;
    }

    public void setTop(short top) {
        this.top = top;
    }

    public short getTop() {
        return this.top;
    }

    public void setCodVar(short codVar) {
        this.codVar = codVar;
    }

    public short getCodVar() {
        return this.codVar;
    }

    public void setTipoOpz(short tipoOpz) {
        this.tipoOpz = tipoOpz;
    }

    public short getTipoOpz() {
        return this.tipoOpz;
    }

    public void setOpz(short opz) {
        this.opz = opz;
    }

    public short getOpz() {
        return this.opz;
    }

    public void setCharFld(short charFld) {
        this.charFld = charFld;
    }

    public short getCharFld() {
        return this.charFld;
    }

    public void setTabDist(short tabDist) {
        this.tabDist = tabDist;
    }

    public short getTabDist() {
        return this.tabDist;
    }

    public void setRiga(short riga) {
        this.riga = riga;
    }

    public short getRiga() {
        return this.riga;
    }

    public void setCol(short col) {
        this.col = col;
    }

    public short getCol() {
        return this.col;
    }
}
