package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-NUM-RAT-PAG-PRE<br>
 * Variable: PMO-NUM-RAT-PAG-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoNumRatPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoNumRatPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_NUM_RAT_PAG_PRE;
    }

    public void setPmoNumRatPagPre(int pmoNumRatPagPre) {
        writeIntAsPacked(Pos.PMO_NUM_RAT_PAG_PRE, pmoNumRatPagPre, Len.Int.PMO_NUM_RAT_PAG_PRE);
    }

    public void setPmoNumRatPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_NUM_RAT_PAG_PRE, Pos.PMO_NUM_RAT_PAG_PRE);
    }

    /**Original name: PMO-NUM-RAT-PAG-PRE<br>*/
    public int getPmoNumRatPagPre() {
        return readPackedAsInt(Pos.PMO_NUM_RAT_PAG_PRE, Len.Int.PMO_NUM_RAT_PAG_PRE);
    }

    public byte[] getPmoNumRatPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_NUM_RAT_PAG_PRE, Pos.PMO_NUM_RAT_PAG_PRE);
        return buffer;
    }

    public void initPmoNumRatPagPreHighValues() {
        fill(Pos.PMO_NUM_RAT_PAG_PRE, Len.PMO_NUM_RAT_PAG_PRE, Types.HIGH_CHAR_VAL);
    }

    public void setPmoNumRatPagPreNull(String pmoNumRatPagPreNull) {
        writeString(Pos.PMO_NUM_RAT_PAG_PRE_NULL, pmoNumRatPagPreNull, Len.PMO_NUM_RAT_PAG_PRE_NULL);
    }

    /**Original name: PMO-NUM-RAT-PAG-PRE-NULL<br>*/
    public String getPmoNumRatPagPreNull() {
        return readString(Pos.PMO_NUM_RAT_PAG_PRE_NULL, Len.PMO_NUM_RAT_PAG_PRE_NULL);
    }

    public String getPmoNumRatPagPreNullFormatted() {
        return Functions.padBlanks(getPmoNumRatPagPreNull(), Len.PMO_NUM_RAT_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_NUM_RAT_PAG_PRE = 1;
        public static final int PMO_NUM_RAT_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_NUM_RAT_PAG_PRE = 3;
        public static final int PMO_NUM_RAT_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_NUM_RAT_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
