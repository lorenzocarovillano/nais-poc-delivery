package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-BOLLO-DETT-C<br>
 * Variable: S089-IMPB-BOLLO-DETT-C from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_BOLLO_DETT_C;
    }

    public void setWlquImpbBolloDettC(AfDecimal wlquImpbBolloDettC) {
        writeDecimalAsPacked(Pos.S089_IMPB_BOLLO_DETT_C, wlquImpbBolloDettC.copy());
    }

    public void setWlquImpbBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_BOLLO_DETT_C, Pos.S089_IMPB_BOLLO_DETT_C);
    }

    /**Original name: WLQU-IMPB-BOLLO-DETT-C<br>*/
    public AfDecimal getWlquImpbBolloDettC() {
        return readPackedAsDecimal(Pos.S089_IMPB_BOLLO_DETT_C, Len.Int.WLQU_IMPB_BOLLO_DETT_C, Len.Fract.WLQU_IMPB_BOLLO_DETT_C);
    }

    public byte[] getWlquImpbBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_BOLLO_DETT_C, Pos.S089_IMPB_BOLLO_DETT_C);
        return buffer;
    }

    public void initWlquImpbBolloDettCSpaces() {
        fill(Pos.S089_IMPB_BOLLO_DETT_C, Len.S089_IMPB_BOLLO_DETT_C, Types.SPACE_CHAR);
    }

    public void setWlquImpbBolloDettCNull(String wlquImpbBolloDettCNull) {
        writeString(Pos.S089_IMPB_BOLLO_DETT_C_NULL, wlquImpbBolloDettCNull, Len.WLQU_IMPB_BOLLO_DETT_C_NULL);
    }

    /**Original name: WLQU-IMPB-BOLLO-DETT-C-NULL<br>*/
    public String getWlquImpbBolloDettCNull() {
        return readString(Pos.S089_IMPB_BOLLO_DETT_C_NULL, Len.WLQU_IMPB_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_BOLLO_DETT_C = 1;
        public static final int S089_IMPB_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_BOLLO_DETT_C = 8;
        public static final int WLQU_IMPB_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
