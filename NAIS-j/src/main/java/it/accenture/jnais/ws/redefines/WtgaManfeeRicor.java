package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-MANFEE-RICOR<br>
 * Variable: WTGA-MANFEE-RICOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_MANFEE_RICOR;
    }

    public void setWtgaManfeeRicor(AfDecimal wtgaManfeeRicor) {
        writeDecimalAsPacked(Pos.WTGA_MANFEE_RICOR, wtgaManfeeRicor.copy());
    }

    public void setWtgaManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_MANFEE_RICOR, Pos.WTGA_MANFEE_RICOR);
    }

    /**Original name: WTGA-MANFEE-RICOR<br>*/
    public AfDecimal getWtgaManfeeRicor() {
        return readPackedAsDecimal(Pos.WTGA_MANFEE_RICOR, Len.Int.WTGA_MANFEE_RICOR, Len.Fract.WTGA_MANFEE_RICOR);
    }

    public byte[] getWtgaManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_MANFEE_RICOR, Pos.WTGA_MANFEE_RICOR);
        return buffer;
    }

    public void initWtgaManfeeRicorSpaces() {
        fill(Pos.WTGA_MANFEE_RICOR, Len.WTGA_MANFEE_RICOR, Types.SPACE_CHAR);
    }

    public void setWtgaManfeeRicorNull(String wtgaManfeeRicorNull) {
        writeString(Pos.WTGA_MANFEE_RICOR_NULL, wtgaManfeeRicorNull, Len.WTGA_MANFEE_RICOR_NULL);
    }

    /**Original name: WTGA-MANFEE-RICOR-NULL<br>*/
    public String getWtgaManfeeRicorNull() {
        return readString(Pos.WTGA_MANFEE_RICOR_NULL, Len.WTGA_MANFEE_RICOR_NULL);
    }

    public String getWtgaManfeeRicorNullFormatted() {
        return Functions.padBlanks(getWtgaManfeeRicorNull(), Len.WTGA_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_MANFEE_RICOR = 1;
        public static final int WTGA_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_MANFEE_RICOR = 8;
        public static final int WTGA_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
