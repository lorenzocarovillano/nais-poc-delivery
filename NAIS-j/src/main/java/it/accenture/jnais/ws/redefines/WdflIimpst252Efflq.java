package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IIMPST-252-EFFLQ<br>
 * Variable: WDFL-IIMPST-252-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIimpst252Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIimpst252Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IIMPST252_EFFLQ;
    }

    public void setWdflIimpst252Efflq(AfDecimal wdflIimpst252Efflq) {
        writeDecimalAsPacked(Pos.WDFL_IIMPST252_EFFLQ, wdflIimpst252Efflq.copy());
    }

    public void setWdflIimpst252EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IIMPST252_EFFLQ, Pos.WDFL_IIMPST252_EFFLQ);
    }

    /**Original name: WDFL-IIMPST-252-EFFLQ<br>*/
    public AfDecimal getWdflIimpst252Efflq() {
        return readPackedAsDecimal(Pos.WDFL_IIMPST252_EFFLQ, Len.Int.WDFL_IIMPST252_EFFLQ, Len.Fract.WDFL_IIMPST252_EFFLQ);
    }

    public byte[] getWdflIimpst252EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IIMPST252_EFFLQ, Pos.WDFL_IIMPST252_EFFLQ);
        return buffer;
    }

    public void setWdflIimpst252EfflqNull(String wdflIimpst252EfflqNull) {
        writeString(Pos.WDFL_IIMPST252_EFFLQ_NULL, wdflIimpst252EfflqNull, Len.WDFL_IIMPST252_EFFLQ_NULL);
    }

    /**Original name: WDFL-IIMPST-252-EFFLQ-NULL<br>*/
    public String getWdflIimpst252EfflqNull() {
        return readString(Pos.WDFL_IIMPST252_EFFLQ_NULL, Len.WDFL_IIMPST252_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST252_EFFLQ = 1;
        public static final int WDFL_IIMPST252_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST252_EFFLQ = 8;
        public static final int WDFL_IIMPST252_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST252_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST252_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
