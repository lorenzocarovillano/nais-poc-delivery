package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-ID-MOVI-CHIU<br>
 * Variable: RDF-ID-MOVI-CHIU from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_ID_MOVI_CHIU;
    }

    public void setRdfIdMoviChiu(int rdfIdMoviChiu) {
        writeIntAsPacked(Pos.RDF_ID_MOVI_CHIU, rdfIdMoviChiu, Len.Int.RDF_ID_MOVI_CHIU);
    }

    public void setRdfIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_ID_MOVI_CHIU, Pos.RDF_ID_MOVI_CHIU);
    }

    /**Original name: RDF-ID-MOVI-CHIU<br>*/
    public int getRdfIdMoviChiu() {
        return readPackedAsInt(Pos.RDF_ID_MOVI_CHIU, Len.Int.RDF_ID_MOVI_CHIU);
    }

    public byte[] getRdfIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_ID_MOVI_CHIU, Pos.RDF_ID_MOVI_CHIU);
        return buffer;
    }

    public void setRdfIdMoviChiuNull(String rdfIdMoviChiuNull) {
        writeString(Pos.RDF_ID_MOVI_CHIU_NULL, rdfIdMoviChiuNull, Len.RDF_ID_MOVI_CHIU_NULL);
    }

    /**Original name: RDF-ID-MOVI-CHIU-NULL<br>*/
    public String getRdfIdMoviChiuNull() {
        return readString(Pos.RDF_ID_MOVI_CHIU_NULL, Len.RDF_ID_MOVI_CHIU_NULL);
    }

    public String getRdfIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getRdfIdMoviChiuNull(), Len.RDF_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_ID_MOVI_CHIU = 1;
        public static final int RDF_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_ID_MOVI_CHIU = 5;
        public static final int RDF_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
