package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-END-FINANZ<br>
 * Variable: WP67-DT-END-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtEndFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtEndFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_END_FINANZ;
    }

    public void setWp67DtEndFinanz(int wp67DtEndFinanz) {
        writeIntAsPacked(Pos.WP67_DT_END_FINANZ, wp67DtEndFinanz, Len.Int.WP67_DT_END_FINANZ);
    }

    public void setWp67DtEndFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_END_FINANZ, Pos.WP67_DT_END_FINANZ);
    }

    /**Original name: WP67-DT-END-FINANZ<br>*/
    public int getWp67DtEndFinanz() {
        return readPackedAsInt(Pos.WP67_DT_END_FINANZ, Len.Int.WP67_DT_END_FINANZ);
    }

    public byte[] getWp67DtEndFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_END_FINANZ, Pos.WP67_DT_END_FINANZ);
        return buffer;
    }

    public void setWp67DtEndFinanzNull(String wp67DtEndFinanzNull) {
        writeString(Pos.WP67_DT_END_FINANZ_NULL, wp67DtEndFinanzNull, Len.WP67_DT_END_FINANZ_NULL);
    }

    /**Original name: WP67-DT-END-FINANZ-NULL<br>*/
    public String getWp67DtEndFinanzNull() {
        return readString(Pos.WP67_DT_END_FINANZ_NULL, Len.WP67_DT_END_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_END_FINANZ = 1;
        public static final int WP67_DT_END_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_END_FINANZ = 5;
        public static final int WP67_DT_END_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_END_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
