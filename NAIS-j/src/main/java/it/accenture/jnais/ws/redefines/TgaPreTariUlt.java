package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-TARI-ULT<br>
 * Variable: TGA-PRE-TARI-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreTariUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreTariUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_TARI_ULT;
    }

    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        writeDecimalAsPacked(Pos.TGA_PRE_TARI_ULT, tgaPreTariUlt.copy());
    }

    public void setTgaPreTariUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_TARI_ULT, Pos.TGA_PRE_TARI_ULT);
    }

    /**Original name: TGA-PRE-TARI-ULT<br>*/
    public AfDecimal getTgaPreTariUlt() {
        return readPackedAsDecimal(Pos.TGA_PRE_TARI_ULT, Len.Int.TGA_PRE_TARI_ULT, Len.Fract.TGA_PRE_TARI_ULT);
    }

    public byte[] getTgaPreTariUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_TARI_ULT, Pos.TGA_PRE_TARI_ULT);
        return buffer;
    }

    public void setTgaPreTariUltNull(String tgaPreTariUltNull) {
        writeString(Pos.TGA_PRE_TARI_ULT_NULL, tgaPreTariUltNull, Len.TGA_PRE_TARI_ULT_NULL);
    }

    /**Original name: TGA-PRE-TARI-ULT-NULL<br>*/
    public String getTgaPreTariUltNull() {
        return readString(Pos.TGA_PRE_TARI_ULT_NULL, Len.TGA_PRE_TARI_ULT_NULL);
    }

    public String getTgaPreTariUltNullFormatted() {
        return Functions.padBlanks(getTgaPreTariUltNull(), Len.TGA_PRE_TARI_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_TARI_ULT = 1;
        public static final int TGA_PRE_TARI_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_TARI_ULT = 8;
        public static final int TGA_PRE_TARI_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_TARI_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_TARI_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
