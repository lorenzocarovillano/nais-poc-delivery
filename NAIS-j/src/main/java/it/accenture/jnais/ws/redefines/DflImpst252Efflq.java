package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-252-EFFLQ<br>
 * Variable: DFL-IMPST-252-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpst252Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpst252Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST252_EFFLQ;
    }

    public void setDflImpst252Efflq(AfDecimal dflImpst252Efflq) {
        writeDecimalAsPacked(Pos.DFL_IMPST252_EFFLQ, dflImpst252Efflq.copy());
    }

    public void setDflImpst252EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST252_EFFLQ, Pos.DFL_IMPST252_EFFLQ);
    }

    /**Original name: DFL-IMPST-252-EFFLQ<br>*/
    public AfDecimal getDflImpst252Efflq() {
        return readPackedAsDecimal(Pos.DFL_IMPST252_EFFLQ, Len.Int.DFL_IMPST252_EFFLQ, Len.Fract.DFL_IMPST252_EFFLQ);
    }

    public byte[] getDflImpst252EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST252_EFFLQ, Pos.DFL_IMPST252_EFFLQ);
        return buffer;
    }

    public void setDflImpst252EfflqNull(String dflImpst252EfflqNull) {
        writeString(Pos.DFL_IMPST252_EFFLQ_NULL, dflImpst252EfflqNull, Len.DFL_IMPST252_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPST-252-EFFLQ-NULL<br>*/
    public String getDflImpst252EfflqNull() {
        return readString(Pos.DFL_IMPST252_EFFLQ_NULL, Len.DFL_IMPST252_EFFLQ_NULL);
    }

    public String getDflImpst252EfflqNullFormatted() {
        return Functions.padBlanks(getDflImpst252EfflqNull(), Len.DFL_IMPST252_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST252_EFFLQ = 1;
        public static final int DFL_IMPST252_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST252_EFFLQ = 8;
        public static final int DFL_IMPST252_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST252_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST252_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
