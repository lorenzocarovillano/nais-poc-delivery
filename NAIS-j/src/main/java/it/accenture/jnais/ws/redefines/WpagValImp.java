package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-VAL-IMP<br>
 * Variable: WPAG-VAL-IMP from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_VAL_IMP;
    }

    public void setWpagValImp(AfDecimal wpagValImp) {
        writeDecimalAsPacked(Pos.WPAG_VAL_IMP, wpagValImp.copy());
    }

    public void setWpagValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_VAL_IMP, Pos.WPAG_VAL_IMP);
    }

    /**Original name: WPAG-VAL-IMP<br>*/
    public AfDecimal getWpagValImp() {
        return readPackedAsDecimal(Pos.WPAG_VAL_IMP, Len.Int.WPAG_VAL_IMP, Len.Fract.WPAG_VAL_IMP);
    }

    public byte[] getWpagValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_VAL_IMP, Pos.WPAG_VAL_IMP);
        return buffer;
    }

    public void initWpagValImpSpaces() {
        fill(Pos.WPAG_VAL_IMP, Len.WPAG_VAL_IMP, Types.SPACE_CHAR);
    }

    public void setWpagValImpNull(String wpagValImpNull) {
        writeString(Pos.WPAG_VAL_IMP_NULL, wpagValImpNull, Len.WPAG_VAL_IMP_NULL);
    }

    /**Original name: WPAG-VAL-IMP-NULL<br>*/
    public String getWpagValImpNull() {
        return readString(Pos.WPAG_VAL_IMP_NULL, Len.WPAG_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_IMP = 1;
        public static final int WPAG_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_VAL_IMP = 8;
        public static final int WPAG_VAL_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_VAL_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_VAL_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
