package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRAN-DT-DELIBERA-CDA<br>
 * Variable: WRAN-DT-DELIBERA-CDA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WranDtDeliberaCda extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WranDtDeliberaCda() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_DT_DELIBERA_CDA;
    }

    public void setWranDtDeliberaCda(int wranDtDeliberaCda) {
        writeIntAsPacked(Pos.WRAN_DT_DELIBERA_CDA, wranDtDeliberaCda, Len.Int.WRAN_DT_DELIBERA_CDA);
    }

    public void setWranDtDeliberaCdaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRAN_DT_DELIBERA_CDA, Pos.WRAN_DT_DELIBERA_CDA);
    }

    /**Original name: WRAN-DT-DELIBERA-CDA<br>*/
    public int getWranDtDeliberaCda() {
        return readPackedAsInt(Pos.WRAN_DT_DELIBERA_CDA, Len.Int.WRAN_DT_DELIBERA_CDA);
    }

    public byte[] getWranDtDeliberaCdaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRAN_DT_DELIBERA_CDA, Pos.WRAN_DT_DELIBERA_CDA);
        return buffer;
    }

    public void initWranDtDeliberaCdaSpaces() {
        fill(Pos.WRAN_DT_DELIBERA_CDA, Len.WRAN_DT_DELIBERA_CDA, Types.SPACE_CHAR);
    }

    public void setWranDtDeliberaCdaNull(String wranDtDeliberaCdaNull) {
        writeString(Pos.WRAN_DT_DELIBERA_CDA_NULL, wranDtDeliberaCdaNull, Len.WRAN_DT_DELIBERA_CDA_NULL);
    }

    /**Original name: WRAN-DT-DELIBERA-CDA-NULL<br>*/
    public String getWranDtDeliberaCdaNull() {
        return readString(Pos.WRAN_DT_DELIBERA_CDA_NULL, Len.WRAN_DT_DELIBERA_CDA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRAN_DT_DELIBERA_CDA = 1;
        public static final int WRAN_DT_DELIBERA_CDA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_DT_DELIBERA_CDA = 5;
        public static final int WRAN_DT_DELIBERA_CDA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_DT_DELIBERA_CDA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
