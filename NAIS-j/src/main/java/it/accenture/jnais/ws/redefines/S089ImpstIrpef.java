package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-IRPEF<br>
 * Variable: S089-IMPST-IRPEF from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstIrpef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstIrpef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_IRPEF;
    }

    public void setWlquImpstIrpef(AfDecimal wlquImpstIrpef) {
        writeDecimalAsPacked(Pos.S089_IMPST_IRPEF, wlquImpstIrpef.copy());
    }

    public void setWlquImpstIrpefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_IRPEF, Pos.S089_IMPST_IRPEF);
    }

    /**Original name: WLQU-IMPST-IRPEF<br>*/
    public AfDecimal getWlquImpstIrpef() {
        return readPackedAsDecimal(Pos.S089_IMPST_IRPEF, Len.Int.WLQU_IMPST_IRPEF, Len.Fract.WLQU_IMPST_IRPEF);
    }

    public byte[] getWlquImpstIrpefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_IRPEF, Pos.S089_IMPST_IRPEF);
        return buffer;
    }

    public void initWlquImpstIrpefSpaces() {
        fill(Pos.S089_IMPST_IRPEF, Len.S089_IMPST_IRPEF, Types.SPACE_CHAR);
    }

    public void setWlquImpstIrpefNull(String wlquImpstIrpefNull) {
        writeString(Pos.S089_IMPST_IRPEF_NULL, wlquImpstIrpefNull, Len.WLQU_IMPST_IRPEF_NULL);
    }

    /**Original name: WLQU-IMPST-IRPEF-NULL<br>*/
    public String getWlquImpstIrpefNull() {
        return readString(Pos.S089_IMPST_IRPEF_NULL, Len.WLQU_IMPST_IRPEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_IRPEF = 1;
        public static final int S089_IMPST_IRPEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_IRPEF = 8;
        public static final int WLQU_IMPST_IRPEF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_IRPEF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_IRPEF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
