package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.redefines.Iabv0003BlobDataArray;

/**Original name: IABV0003<br>
 * Variable: IABV0003 from copybook IABV0003<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Iabv0003 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IABV0003-IB-OGG
    private String iabv0003IbOgg = DefaultValues.stringVal(Len.IABV0003_IB_OGG);
    //Original name: IABV0003-COD-MACROFUNCT
    private String iabv0003CodMacrofunct = DefaultValues.stringVal(Len.IABV0003_COD_MACROFUNCT);
    //Original name: IABV0003-TP-MOVI-BUSINESS
    private int iabv0003TpMoviBusiness = DefaultValues.INT_VAL;
    //Original name: IABV0003-ELE-MAX
    private short iabv0003EleMax = DefaultValues.SHORT_VAL;
    //Original name: IABV0003-BLOB-DATA-ARRAY
    private Iabv0003BlobDataArray iabv0003BlobDataArray = new Iabv0003BlobDataArray();
    //Original name: IABV0003-LUNG-EFF-BLOB
    private String iabv0003LungEffBlob = DefaultValues.stringVal(Len.IABV0003_LUNG_EFF_BLOB);
    //Original name: IABV0003-ALIAS-STR-DATO
    private String iabv0003AliasStrDato = DefaultValues.stringVal(Len.IABV0003_ALIAS_STR_DATO);
    //Original name: FILLER-IABV0003
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IABV0003;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIabv0003Bytes(buf);
    }

    public String getIabv0003Formatted() {
        return MarshalByteExt.bufferToStr(getIabv0003Bytes());
    }

    public void setIabv0003Bytes(byte[] buffer) {
        setIabv0003Bytes(buffer, 1);
    }

    public byte[] getIabv0003Bytes() {
        byte[] buffer = new byte[Len.IABV0003];
        return getIabv0003Bytes(buffer, 1);
    }

    public void setIabv0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        setIabv0003JobBytes(buffer, position);
        position += Len.IABV0003_JOB;
        setIabv0003DataJobBytes(buffer, position);
        position += Len.IABV0003_DATA_JOB;
        iabv0003AliasStrDato = MarshalByte.readString(buffer, position, Len.IABV0003_ALIAS_STR_DATO);
        position += Len.IABV0003_ALIAS_STR_DATO;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public byte[] getIabv0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        getIabv0003JobBytes(buffer, position);
        position += Len.IABV0003_JOB;
        getIabv0003DataJobBytes(buffer, position);
        position += Len.IABV0003_DATA_JOB;
        MarshalByte.writeString(buffer, position, iabv0003AliasStrDato, Len.IABV0003_ALIAS_STR_DATO);
        position += Len.IABV0003_ALIAS_STR_DATO;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setIabv0003JobBytes(byte[] buffer, int offset) {
        int position = offset;
        iabv0003IbOgg = MarshalByte.readString(buffer, position, Len.IABV0003_IB_OGG);
        position += Len.IABV0003_IB_OGG;
        iabv0003CodMacrofunct = MarshalByte.readString(buffer, position, Len.IABV0003_COD_MACROFUNCT);
        position += Len.IABV0003_COD_MACROFUNCT;
        iabv0003TpMoviBusiness = MarshalByte.readPackedAsInt(buffer, position, Len.Int.IABV0003_TP_MOVI_BUSINESS, 0);
    }

    public byte[] getIabv0003JobBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, iabv0003IbOgg, Len.IABV0003_IB_OGG);
        position += Len.IABV0003_IB_OGG;
        MarshalByte.writeString(buffer, position, iabv0003CodMacrofunct, Len.IABV0003_COD_MACROFUNCT);
        position += Len.IABV0003_COD_MACROFUNCT;
        MarshalByte.writeIntAsPacked(buffer, position, iabv0003TpMoviBusiness, Len.Int.IABV0003_TP_MOVI_BUSINESS, 0);
        return buffer;
    }

    public void setIabv0003IbOgg(String iabv0003IbOgg) {
        this.iabv0003IbOgg = Functions.subString(iabv0003IbOgg, Len.IABV0003_IB_OGG);
    }

    public String getIabv0003IbOgg() {
        return this.iabv0003IbOgg;
    }

    public void setIabv0003CodMacrofunct(String iabv0003CodMacrofunct) {
        this.iabv0003CodMacrofunct = Functions.subString(iabv0003CodMacrofunct, Len.IABV0003_COD_MACROFUNCT);
    }

    public String getIabv0003CodMacrofunct() {
        return this.iabv0003CodMacrofunct;
    }

    public void setIabv0003TpMoviBusiness(int iabv0003TpMoviBusiness) {
        this.iabv0003TpMoviBusiness = iabv0003TpMoviBusiness;
    }

    public int getIabv0003TpMoviBusiness() {
        return this.iabv0003TpMoviBusiness;
    }

    public void setIabv0003DataJobBytes(byte[] buffer, int offset) {
        int position = offset;
        iabv0003EleMax = MarshalByte.readShort(buffer, position, Len.IABV0003_ELE_MAX, SignType.NO_SIGN);
        position += Len.IABV0003_ELE_MAX;
        setIabv0003BlobDataBytes(buffer, position);
        position += Len.IABV0003_BLOB_DATA;
        iabv0003LungEffBlob = MarshalByte.readFixedString(buffer, position, Len.IABV0003_LUNG_EFF_BLOB);
    }

    public byte[] getIabv0003DataJobBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShort(buffer, position, iabv0003EleMax, Len.IABV0003_ELE_MAX, SignType.NO_SIGN);
        position += Len.IABV0003_ELE_MAX;
        getIabv0003BlobDataBytes(buffer, position);
        position += Len.IABV0003_BLOB_DATA;
        MarshalByte.writeString(buffer, position, iabv0003LungEffBlob, Len.IABV0003_LUNG_EFF_BLOB);
        return buffer;
    }

    public void setIabv0003EleMax(short iabv0003EleMax) {
        this.iabv0003EleMax = iabv0003EleMax;
    }

    public short getIabv0003EleMax() {
        return this.iabv0003EleMax;
    }

    public void setIabv0003BlobDataFormatted(String data) {
        byte[] buffer = new byte[Len.IABV0003_BLOB_DATA];
        MarshalByte.writeString(buffer, 1, data, Len.IABV0003_BLOB_DATA);
        setIabv0003BlobDataBytes(buffer, 1);
    }

    public String getIabv0003BlobDataFormatted() {
        return MarshalByteExt.bufferToStr(getIabv0003BlobDataBytes());
    }

    /**Original name: IABV0003-BLOB-DATA<br>*/
    public byte[] getIabv0003BlobDataBytes() {
        byte[] buffer = new byte[Len.IABV0003_BLOB_DATA];
        return getIabv0003BlobDataBytes(buffer, 1);
    }

    public void setIabv0003BlobDataBytes(byte[] buffer, int offset) {
        int position = offset;
        iabv0003BlobDataArray.setIabv0003BlobDataArrayFromBuffer(buffer, position);
    }

    public byte[] getIabv0003BlobDataBytes(byte[] buffer, int offset) {
        int position = offset;
        iabv0003BlobDataArray.getIabv0003BlobDataArrayAsBuffer(buffer, position);
        return buffer;
    }

    public void setIabv0003LungEffBlobFormatted(String iabv0003LungEffBlob) {
        this.iabv0003LungEffBlob = Trunc.toUnsignedNumeric(iabv0003LungEffBlob, Len.IABV0003_LUNG_EFF_BLOB);
    }

    public int getIabv0003LungEffBlob() {
        return NumericDisplay.asInt(this.iabv0003LungEffBlob);
    }

    public void setIabv0003AliasStrDato(String iabv0003AliasStrDato) {
        this.iabv0003AliasStrDato = Functions.subString(iabv0003AliasStrDato, Len.IABV0003_ALIAS_STR_DATO);
    }

    public String getIabv0003AliasStrDato() {
        return this.iabv0003AliasStrDato;
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public Iabv0003BlobDataArray getIabv0003BlobDataArray() {
        return iabv0003BlobDataArray;
    }

    @Override
    public byte[] serialize() {
        return getIabv0003Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IABV0003_IB_OGG = 100;
        public static final int IABV0003_COD_MACROFUNCT = 2;
        public static final int IABV0003_TP_MOVI_BUSINESS = 3;
        public static final int IABV0003_JOB = IABV0003_IB_OGG + IABV0003_COD_MACROFUNCT + IABV0003_TP_MOVI_BUSINESS;
        public static final int IABV0003_ELE_MAX = 3;
        public static final int IABV0003_BLOB_DATA = Iabv0003BlobDataArray.Len.IABV0003_BLOB_DATA_ARRAY;
        public static final int IABV0003_LUNG_EFF_BLOB = 9;
        public static final int IABV0003_DATA_JOB = IABV0003_ELE_MAX + IABV0003_BLOB_DATA + IABV0003_LUNG_EFF_BLOB;
        public static final int IABV0003_ALIAS_STR_DATO = 30;
        public static final int FLR1 = 70;
        public static final int IABV0003 = IABV0003_JOB + IABV0003_DATA_JOB + IABV0003_ALIAS_STR_DATO + FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IABV0003_TP_MOVI_BUSINESS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
