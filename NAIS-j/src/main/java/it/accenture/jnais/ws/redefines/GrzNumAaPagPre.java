package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-NUM-AA-PAG-PRE<br>
 * Variable: GRZ-NUM-AA-PAG-PRE from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzNumAaPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzNumAaPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_NUM_AA_PAG_PRE;
    }

    public void setGrzNumAaPagPre(int grzNumAaPagPre) {
        writeIntAsPacked(Pos.GRZ_NUM_AA_PAG_PRE, grzNumAaPagPre, Len.Int.GRZ_NUM_AA_PAG_PRE);
    }

    public void setGrzNumAaPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_NUM_AA_PAG_PRE, Pos.GRZ_NUM_AA_PAG_PRE);
    }

    /**Original name: GRZ-NUM-AA-PAG-PRE<br>*/
    public int getGrzNumAaPagPre() {
        return readPackedAsInt(Pos.GRZ_NUM_AA_PAG_PRE, Len.Int.GRZ_NUM_AA_PAG_PRE);
    }

    public byte[] getGrzNumAaPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_NUM_AA_PAG_PRE, Pos.GRZ_NUM_AA_PAG_PRE);
        return buffer;
    }

    public void setGrzNumAaPagPreNull(String grzNumAaPagPreNull) {
        writeString(Pos.GRZ_NUM_AA_PAG_PRE_NULL, grzNumAaPagPreNull, Len.GRZ_NUM_AA_PAG_PRE_NULL);
    }

    /**Original name: GRZ-NUM-AA-PAG-PRE-NULL<br>*/
    public String getGrzNumAaPagPreNull() {
        return readString(Pos.GRZ_NUM_AA_PAG_PRE_NULL, Len.GRZ_NUM_AA_PAG_PRE_NULL);
    }

    public String getGrzNumAaPagPreNullFormatted() {
        return Functions.padBlanks(getGrzNumAaPagPreNull(), Len.GRZ_NUM_AA_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_NUM_AA_PAG_PRE = 1;
        public static final int GRZ_NUM_AA_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_NUM_AA_PAG_PRE = 3;
        public static final int GRZ_NUM_AA_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_NUM_AA_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
