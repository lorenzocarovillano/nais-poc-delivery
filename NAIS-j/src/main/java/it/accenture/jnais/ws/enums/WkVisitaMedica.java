package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-VISITA-MEDICA<br>
 * Variable: WK-VISITA-MEDICA from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkVisitaMedica {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_VISITA_MEDICA);
    public static final String NON_PREVITA = "0";
    public static final String PREVISTA = "1";
    public static final String OBBLIGATORIA = "2";

    //==== METHODS ====
    public void setWkVisitaMedica(short wkVisitaMedica) {
        this.value = NumericDisplay.asString(wkVisitaMedica, Len.WK_VISITA_MEDICA);
    }

    public short getWkVisitaMedica() {
        return NumericDisplay.asShort(this.value);
    }

    public String getWkVisitaMedicaFormatted() {
        return this.value;
    }

    public boolean isObbligatoria() {
        return getWkVisitaMedicaFormatted().equals(OBBLIGATORIA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_VISITA_MEDICA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
