package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-TFR-STRC<br>
 * Variable: TDR-IMP-TFR-STRC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpTfrStrc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpTfrStrc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_TFR_STRC;
    }

    public void setTdrImpTfrStrc(AfDecimal tdrImpTfrStrc) {
        writeDecimalAsPacked(Pos.TDR_IMP_TFR_STRC, tdrImpTfrStrc.copy());
    }

    public void setTdrImpTfrStrcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_TFR_STRC, Pos.TDR_IMP_TFR_STRC);
    }

    /**Original name: TDR-IMP-TFR-STRC<br>*/
    public AfDecimal getTdrImpTfrStrc() {
        return readPackedAsDecimal(Pos.TDR_IMP_TFR_STRC, Len.Int.TDR_IMP_TFR_STRC, Len.Fract.TDR_IMP_TFR_STRC);
    }

    public byte[] getTdrImpTfrStrcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_TFR_STRC, Pos.TDR_IMP_TFR_STRC);
        return buffer;
    }

    public void setTdrImpTfrStrcNull(String tdrImpTfrStrcNull) {
        writeString(Pos.TDR_IMP_TFR_STRC_NULL, tdrImpTfrStrcNull, Len.TDR_IMP_TFR_STRC_NULL);
    }

    /**Original name: TDR-IMP-TFR-STRC-NULL<br>*/
    public String getTdrImpTfrStrcNull() {
        return readString(Pos.TDR_IMP_TFR_STRC_NULL, Len.TDR_IMP_TFR_STRC_NULL);
    }

    public String getTdrImpTfrStrcNullFormatted() {
        return Functions.padBlanks(getTdrImpTfrStrcNull(), Len.TDR_IMP_TFR_STRC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_TFR_STRC = 1;
        public static final int TDR_IMP_TFR_STRC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_TFR_STRC = 8;
        public static final int TDR_IMP_TFR_STRC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_TFR_STRC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_TFR_STRC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
