package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IVVC0223-TAB-LIVELLI-GAR<br>
 * Variables: IVVC0223-TAB-LIVELLI-GAR from copybook IVVC0223<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0223TabLivelliGar {

    //==== PROPERTIES ====
    public static final int TAB_VAR_GAR_MAXOCCURS = 30;
    //Original name: IVVC0223-ID-GARANZIA
    private String idGaranzia = DefaultValues.stringVal(Len.ID_GARANZIA);
    //Original name: IVVC0223-COD-TARI
    private String codTari = DefaultValues.stringVal(Len.COD_TARI);
    //Original name: IVVC0223-ELE-MAX-VAR-GAR
    private short eleMaxVarGar = DefaultValues.BIN_SHORT_VAL;
    //Original name: IVVC0223-TAB-VAR-GAR
    private Ivvc0223TabVarGar[] tabVarGar = new Ivvc0223TabVarGar[TAB_VAR_GAR_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ivvc0223TabLivelliGar() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int tabVarGarIdx = 1; tabVarGarIdx <= TAB_VAR_GAR_MAXOCCURS; tabVarGarIdx++) {
            tabVarGar[tabVarGarIdx - 1] = new Ivvc0223TabVarGar();
        }
    }

    public void setTabLivelliGarBytes(byte[] buffer, int offset) {
        int position = offset;
        idGaranzia = MarshalByte.readFixedString(buffer, position, Len.ID_GARANZIA);
        position += Len.ID_GARANZIA;
        codTari = MarshalByte.readString(buffer, position, Len.COD_TARI);
        position += Len.COD_TARI;
        eleMaxVarGar = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_VAR_GAR_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabVarGar[idx - 1].setTabVarGarBytes(buffer, position);
                position += Ivvc0223TabVarGar.Len.TAB_VAR_GAR;
            }
            else {
                tabVarGar[idx - 1].initTabVarGarSpaces();
                position += Ivvc0223TabVarGar.Len.TAB_VAR_GAR;
            }
        }
    }

    public byte[] getTabLivelliGarBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idGaranzia, Len.ID_GARANZIA);
        position += Len.ID_GARANZIA;
        MarshalByte.writeString(buffer, position, codTari, Len.COD_TARI);
        position += Len.COD_TARI;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxVarGar);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_VAR_GAR_MAXOCCURS; idx++) {
            tabVarGar[idx - 1].getTabVarGarBytes(buffer, position);
            position += Ivvc0223TabVarGar.Len.TAB_VAR_GAR;
        }
        return buffer;
    }

    public void initTabLivelliGarSpaces() {
        idGaranzia = "";
        codTari = "";
        eleMaxVarGar = Types.INVALID_BINARY_SHORT_VAL;
        for (int idx = 1; idx <= TAB_VAR_GAR_MAXOCCURS; idx++) {
            tabVarGar[idx - 1].initTabVarGarSpaces();
        }
    }

    public void setIdGaranziaFormatted(String idGaranzia) {
        this.idGaranzia = Trunc.toUnsignedNumeric(idGaranzia, Len.ID_GARANZIA);
    }

    public int getIdGaranzia() {
        return NumericDisplay.asInt(this.idGaranzia);
    }

    public void setCodTari(String codTari) {
        this.codTari = Functions.subString(codTari, Len.COD_TARI);
    }

    public String getCodTari() {
        return this.codTari;
    }

    public void setEleMaxVarGar(short eleMaxVarGar) {
        this.eleMaxVarGar = eleMaxVarGar;
    }

    public short getEleMaxVarGar() {
        return this.eleMaxVarGar;
    }

    public Ivvc0223TabVarGar getTabVarGar(int idx) {
        return tabVarGar[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_GARANZIA = 9;
        public static final int COD_TARI = 12;
        public static final int ELE_MAX_VAR_GAR = 2;
        public static final int TAB_LIVELLI_GAR = ID_GARANZIA + COD_TARI + ELE_MAX_VAR_GAR + Ivvc0223TabLivelliGar.TAB_VAR_GAR_MAXOCCURS * Ivvc0223TabVarGar.Len.TAB_VAR_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
