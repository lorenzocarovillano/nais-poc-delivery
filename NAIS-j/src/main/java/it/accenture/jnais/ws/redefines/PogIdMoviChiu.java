package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POG-ID-MOVI-CHIU<br>
 * Variable: POG-ID-MOVI-CHIU from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PogIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PogIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POG_ID_MOVI_CHIU;
    }

    public void setPogIdMoviChiu(int pogIdMoviChiu) {
        writeIntAsPacked(Pos.POG_ID_MOVI_CHIU, pogIdMoviChiu, Len.Int.POG_ID_MOVI_CHIU);
    }

    public void setPogIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POG_ID_MOVI_CHIU, Pos.POG_ID_MOVI_CHIU);
    }

    /**Original name: POG-ID-MOVI-CHIU<br>*/
    public int getPogIdMoviChiu() {
        return readPackedAsInt(Pos.POG_ID_MOVI_CHIU, Len.Int.POG_ID_MOVI_CHIU);
    }

    public byte[] getPogIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POG_ID_MOVI_CHIU, Pos.POG_ID_MOVI_CHIU);
        return buffer;
    }

    public void setPogIdMoviChiuNull(String pogIdMoviChiuNull) {
        writeString(Pos.POG_ID_MOVI_CHIU_NULL, pogIdMoviChiuNull, Len.POG_ID_MOVI_CHIU_NULL);
    }

    /**Original name: POG-ID-MOVI-CHIU-NULL<br>*/
    public String getPogIdMoviChiuNull() {
        return readString(Pos.POG_ID_MOVI_CHIU_NULL, Len.POG_ID_MOVI_CHIU_NULL);
    }

    public String getPogIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPogIdMoviChiuNull(), Len.POG_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POG_ID_MOVI_CHIU = 1;
        public static final int POG_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_ID_MOVI_CHIU = 5;
        public static final int POG_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
