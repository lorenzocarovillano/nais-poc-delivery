package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPOL-STATUS<br>
 * Variable: WPOL-STATUS from copybook LCCVPOL1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpolStatus {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ADD = 'A';
    public static final char MOD = 'M';
    public static final char INV = 'I';
    public static final char DEL = 'D';
    public static final char CON = 'C';

    //==== METHODS ====
    public void setStatus(char status) {
        this.value = status;
    }

    public char getStatus() {
        return this.value;
    }

    public boolean isAdd() {
        return value == ADD;
    }

    public void setWcomStAdd() {
        value = ADD;
    }

    public boolean isWcomStDeMod() {
        return value == MOD;
    }

    public void setMod() {
        value = MOD;
    }

    public boolean isInv() {
        return value == INV;
    }

    public void setInv() {
        value = INV;
    }

    public boolean isDel() {
        return value == DEL;
    }

    public void setWpmoStDel() {
        value = DEL;
    }

    public boolean isWpmoStCon() {
        return value == CON;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
