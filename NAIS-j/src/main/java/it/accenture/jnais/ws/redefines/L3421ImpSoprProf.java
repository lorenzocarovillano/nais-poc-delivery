package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-SOPR-PROF<br>
 * Variable: L3421-IMP-SOPR-PROF from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpSoprProf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpSoprProf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_SOPR_PROF;
    }

    public void setL3421ImpSoprProfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_SOPR_PROF, Pos.L3421_IMP_SOPR_PROF);
    }

    /**Original name: L3421-IMP-SOPR-PROF<br>*/
    public AfDecimal getL3421ImpSoprProf() {
        return readPackedAsDecimal(Pos.L3421_IMP_SOPR_PROF, Len.Int.L3421_IMP_SOPR_PROF, Len.Fract.L3421_IMP_SOPR_PROF);
    }

    public byte[] getL3421ImpSoprProfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_SOPR_PROF, Pos.L3421_IMP_SOPR_PROF);
        return buffer;
    }

    /**Original name: L3421-IMP-SOPR-PROF-NULL<br>*/
    public String getL3421ImpSoprProfNull() {
        return readString(Pos.L3421_IMP_SOPR_PROF_NULL, Len.L3421_IMP_SOPR_PROF_NULL);
    }

    public String getL3421ImpSoprProfNullFormatted() {
        return Functions.padBlanks(getL3421ImpSoprProfNull(), Len.L3421_IMP_SOPR_PROF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_PROF = 1;
        public static final int L3421_IMP_SOPR_PROF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_PROF = 8;
        public static final int L3421_IMP_SOPR_PROF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_PROF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_PROF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
