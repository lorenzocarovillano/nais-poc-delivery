package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ispc0040DatiInput;
import it.accenture.jnais.copy.Ispc0040DatiOutput1;
import it.accenture.jnais.copy.Ispc0040DatiOutput2;
import it.accenture.jnais.copy.Ispc0040OutCpiProtection;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;
import it.accenture.jnais.ws.redefines.Ispc0040TabFondi;

/**Original name: AREA-IO-ISPS0040<br>
 * Variable: AREA-IO-ISPS0040 from program ISPS0040<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIsps0040 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int ISPC0040_TAB_ERRORI_MAXOCCURS = 10;
    //Original name: ISPC0040-DATI-INPUT
    private Ispc0040DatiInput ispc0040DatiInput = new Ispc0040DatiInput();
    //Original name: ISPC0040-DATI-OUTPUT1
    private Ispc0040DatiOutput1 ispc0040DatiOutput1 = new Ispc0040DatiOutput1();
    //Original name: ISPC0040-TAB-FONDI
    private Ispc0040TabFondi ispc0040TabFondi = new Ispc0040TabFondi();
    //Original name: ISPC0040-DATI-OUTPUT2
    private Ispc0040DatiOutput2 ispc0040DatiOutput2 = new Ispc0040DatiOutput2();
    //Original name: ISPC0040-OUT-CPI-PROTECTION
    private Ispc0040OutCpiProtection ispc0040OutCpiProtection = new Ispc0040OutCpiProtection();
    //Original name: ISPC0040-ESITO
    private String ispc0040Esito = DefaultValues.stringVal(Len.ISPC0040_ESITO);
    //Original name: ISPC0040-ERR-NUM-ELE
    private String ispc0040ErrNumEle = DefaultValues.stringVal(Len.ISPC0040_ERR_NUM_ELE);
    //Original name: ISPC0040-TAB-ERRORI
    private Ispc0211TabErrori[] ispc0040TabErrori = new Ispc0211TabErrori[ISPC0040_TAB_ERRORI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIoIsps0040() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_ISPS0040;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoIsps0040Bytes(buf);
    }

    public void init() {
        for (int ispc0040TabErroriIdx = 1; ispc0040TabErroriIdx <= ISPC0040_TAB_ERRORI_MAXOCCURS; ispc0040TabErroriIdx++) {
            ispc0040TabErrori[ispc0040TabErroriIdx - 1] = new Ispc0211TabErrori();
        }
    }

    public String getAreaIoIsps0040Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoIsps0040Bytes());
    }

    public void setAreaIoIsps0040Bytes(byte[] buffer) {
        setAreaIoIsps0040Bytes(buffer, 1);
    }

    public byte[] getAreaIoIsps0040Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_ISPS0040];
        return getAreaIoIsps0040Bytes(buffer, 1);
    }

    public void setAreaIoIsps0040Bytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0040DatiInput.setIspc0040DatiInputBytes(buffer, position);
        position += Ispc0040DatiInput.Len.ISPC0040_DATI_INPUT;
        setIspc0040DatiOutputBytes(buffer, position);
        position += Len.ISPC0040_DATI_OUTPUT;
        setIspc0040AreaErroriBytes(buffer, position);
    }

    public byte[] getAreaIoIsps0040Bytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0040DatiInput.getIspc0040DatiInputBytes(buffer, position);
        position += Ispc0040DatiInput.Len.ISPC0040_DATI_INPUT;
        getIspc0040DatiOutputBytes(buffer, position);
        position += Len.ISPC0040_DATI_OUTPUT;
        getIspc0040AreaErroriBytes(buffer, position);
        return buffer;
    }

    public void setIspc0040DatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0040DatiOutput1.setIspc0040DatiOutput1Bytes(buffer, position);
        position += Ispc0040DatiOutput1.Len.ISPC0040_DATI_OUTPUT1;
        ispc0040TabFondi.setIspc0040TabFondiBytes(buffer, position);
        position += Ispc0040TabFondi.Len.ISPC0040_TAB_FONDI;
        ispc0040DatiOutput2.setIspc0040DatiOutput2Bytes(buffer, position);
        position += Ispc0040DatiOutput2.Len.ISPC0040_DATI_OUTPUT2;
        ispc0040OutCpiProtection.setIspc0040OutCpiProtectionBytes(buffer, position);
    }

    public byte[] getIspc0040DatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0040DatiOutput1.getIspc0040DatiOutput1Bytes(buffer, position);
        position += Ispc0040DatiOutput1.Len.ISPC0040_DATI_OUTPUT1;
        ispc0040TabFondi.getIspc0040TabFondiBytes(buffer, position);
        position += Ispc0040TabFondi.Len.ISPC0040_TAB_FONDI;
        ispc0040DatiOutput2.getIspc0040DatiOutput2Bytes(buffer, position);
        position += Ispc0040DatiOutput2.Len.ISPC0040_DATI_OUTPUT2;
        ispc0040OutCpiProtection.getIspc0040OutCpiProtectionBytes(buffer, position);
        return buffer;
    }

    /**Original name: ISPC0040-AREA-ERRORI<br>*/
    public byte[] getIspc0040AreaErroriBytes() {
        byte[] buffer = new byte[Len.ISPC0040_AREA_ERRORI];
        return getIspc0040AreaErroriBytes(buffer, 1);
    }

    public void setIspc0040AreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        ispc0040Esito = MarshalByte.readFixedString(buffer, position, Len.ISPC0040_ESITO);
        position += Len.ISPC0040_ESITO;
        ispc0040ErrNumEle = MarshalByte.readFixedString(buffer, position, Len.ISPC0040_ERR_NUM_ELE);
        position += Len.ISPC0040_ERR_NUM_ELE;
        for (int idx = 1; idx <= ISPC0040_TAB_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                ispc0040TabErrori[idx - 1].setIspc0211TabErroriBytes(buffer, position);
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
            else {
                ispc0040TabErrori[idx - 1].initIspc0211TabErroriSpaces();
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
        }
    }

    public byte[] getIspc0040AreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ispc0040Esito, Len.ISPC0040_ESITO);
        position += Len.ISPC0040_ESITO;
        MarshalByte.writeString(buffer, position, ispc0040ErrNumEle, Len.ISPC0040_ERR_NUM_ELE);
        position += Len.ISPC0040_ERR_NUM_ELE;
        for (int idx = 1; idx <= ISPC0040_TAB_ERRORI_MAXOCCURS; idx++) {
            ispc0040TabErrori[idx - 1].getIspc0211TabErroriBytes(buffer, position);
            position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        }
        return buffer;
    }

    public void setIspc0040EsitoFormatted(String ispc0040Esito) {
        this.ispc0040Esito = Trunc.toUnsignedNumeric(ispc0040Esito, Len.ISPC0040_ESITO);
    }

    public short getIspc0040Esito() {
        return NumericDisplay.asShort(this.ispc0040Esito);
    }

    public void setIspc0040ErrNumEleFormatted(String ispc0040ErrNumEle) {
        this.ispc0040ErrNumEle = Trunc.toUnsignedNumeric(ispc0040ErrNumEle, Len.ISPC0040_ERR_NUM_ELE);
    }

    public short getIspc0040ErrNumEle() {
        return NumericDisplay.asShort(this.ispc0040ErrNumEle);
    }

    public Ispc0040DatiInput getIspc0040DatiInput() {
        return ispc0040DatiInput;
    }

    public Ispc0040DatiOutput1 getIspc0040DatiOutput1() {
        return ispc0040DatiOutput1;
    }

    public Ispc0040DatiOutput2 getIspc0040DatiOutput2() {
        return ispc0040DatiOutput2;
    }

    public Ispc0040OutCpiProtection getIspc0040OutCpiProtection() {
        return ispc0040OutCpiProtection;
    }

    public Ispc0211TabErrori getIspc0040TabErrori(int idx) {
        return ispc0040TabErrori[idx - 1];
    }

    public Ispc0040TabFondi getIspc0040TabFondi() {
        return ispc0040TabFondi;
    }

    @Override
    public byte[] serialize() {
        return getAreaIoIsps0040Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ISPC0040_DATI_OUTPUT = Ispc0040DatiOutput1.Len.ISPC0040_DATI_OUTPUT1 + Ispc0040TabFondi.Len.ISPC0040_TAB_FONDI + Ispc0040DatiOutput2.Len.ISPC0040_DATI_OUTPUT2 + Ispc0040OutCpiProtection.Len.ISPC0040_OUT_CPI_PROTECTION;
        public static final int ISPC0040_ESITO = 2;
        public static final int ISPC0040_ERR_NUM_ELE = 3;
        public static final int ISPC0040_AREA_ERRORI = ISPC0040_ESITO + ISPC0040_ERR_NUM_ELE + AreaIoIsps0040.ISPC0040_TAB_ERRORI_MAXOCCURS * Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        public static final int AREA_IO_ISPS0040 = Ispc0040DatiInput.Len.ISPC0040_DATI_INPUT + ISPC0040_DATI_OUTPUT + ISPC0040_AREA_ERRORI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
