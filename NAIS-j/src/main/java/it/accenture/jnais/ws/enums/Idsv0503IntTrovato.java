package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0503-INT-TROVATO<br>
 * Variable: IDSV0503-INT-TROVATO from copybook IDSV0503<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0503IntTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIdsv0503IntTrovato(char idsv0503IntTrovato) {
        this.value = idsv0503IntTrovato;
    }

    public char getIdsv0503IntTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
