package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-EFF-VARZ-STAT-T<br>
 * Variable: ADE-DT-EFF-VARZ-STAT-T from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtEffVarzStatT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtEffVarzStatT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_EFF_VARZ_STAT_T;
    }

    public void setAdeDtEffVarzStatT(int adeDtEffVarzStatT) {
        writeIntAsPacked(Pos.ADE_DT_EFF_VARZ_STAT_T, adeDtEffVarzStatT, Len.Int.ADE_DT_EFF_VARZ_STAT_T);
    }

    public void setAdeDtEffVarzStatTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_EFF_VARZ_STAT_T, Pos.ADE_DT_EFF_VARZ_STAT_T);
    }

    /**Original name: ADE-DT-EFF-VARZ-STAT-T<br>*/
    public int getAdeDtEffVarzStatT() {
        return readPackedAsInt(Pos.ADE_DT_EFF_VARZ_STAT_T, Len.Int.ADE_DT_EFF_VARZ_STAT_T);
    }

    public byte[] getAdeDtEffVarzStatTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_EFF_VARZ_STAT_T, Pos.ADE_DT_EFF_VARZ_STAT_T);
        return buffer;
    }

    public void setAdeDtEffVarzStatTNull(String adeDtEffVarzStatTNull) {
        writeString(Pos.ADE_DT_EFF_VARZ_STAT_T_NULL, adeDtEffVarzStatTNull, Len.ADE_DT_EFF_VARZ_STAT_T_NULL);
    }

    /**Original name: ADE-DT-EFF-VARZ-STAT-T-NULL<br>*/
    public String getAdeDtEffVarzStatTNull() {
        return readString(Pos.ADE_DT_EFF_VARZ_STAT_T_NULL, Len.ADE_DT_EFF_VARZ_STAT_T_NULL);
    }

    public String getAdeDtEffVarzStatTNullFormatted() {
        return Functions.padBlanks(getAdeDtEffVarzStatTNull(), Len.ADE_DT_EFF_VARZ_STAT_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_EFF_VARZ_STAT_T = 1;
        public static final int ADE_DT_EFF_VARZ_STAT_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_EFF_VARZ_STAT_T = 5;
        public static final int ADE_DT_EFF_VARZ_STAT_T_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_EFF_VARZ_STAT_T = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
