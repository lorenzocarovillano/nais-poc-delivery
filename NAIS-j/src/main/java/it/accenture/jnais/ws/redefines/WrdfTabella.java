package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRDF-TABELLA<br>
 * Variable: WRDF-TABELLA from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrdfTabella extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_RIC_DISINV_MAXOCCURS = 1250;
    public static final char WRDF_ST_ADD = 'A';
    public static final char WRDF_ST_MOD = 'M';
    public static final char WRDF_ST_INV = 'I';
    public static final char WRDF_ST_DEL = 'D';
    public static final char WRDF_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WrdfTabella() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRDF_TABELLA;
    }

    public String getWrdfTabellaFormatted() {
        return readFixedString(Pos.WRDF_TABELLA, Len.WRDF_TABELLA);
    }

    public void setWrdfTabellaBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRDF_TABELLA, Pos.WRDF_TABELLA);
    }

    public byte[] getWrdfTabellaBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRDF_TABELLA, Pos.WRDF_TABELLA);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wrdfStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WRDF-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA RICH_DIS_FND
	 *    ALIAS RDF
	 *    ULTIMO AGG. 25 NOV 2019
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wrdfStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wrdfIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WRDF-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wrdfIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdRichDisFnd(int idRichDisFndIdx, int idRichDisFnd) {
        int position = Pos.wrdfIdRichDisFnd(idRichDisFndIdx - 1);
        writeIntAsPacked(position, idRichDisFnd, Len.Int.ID_RICH_DIS_FND);
    }

    /**Original name: WRDF-ID-RICH-DIS-FND<br>*/
    public int getIdRichDisFnd(int idRichDisFndIdx) {
        int position = Pos.wrdfIdRichDisFnd(idRichDisFndIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_RICH_DIS_FND);
    }

    public void setIdMoviFinrio(int idMoviFinrioIdx, int idMoviFinrio) {
        int position = Pos.wrdfIdMoviFinrio(idMoviFinrioIdx - 1);
        writeIntAsPacked(position, idMoviFinrio, Len.Int.ID_MOVI_FINRIO);
    }

    /**Original name: WRDF-ID-MOVI-FINRIO<br>*/
    public int getIdMoviFinrio(int idMoviFinrioIdx) {
        int position = Pos.wrdfIdMoviFinrio(idMoviFinrioIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_FINRIO);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wrdfIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WRDF-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wrdfIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wrdfIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WRDF-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wrdfIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wrdfDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WRDF-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wrdfDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wrdfDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WRDF-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wrdfDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wrdfCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WRDF-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wrdfCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodFnd(int codFndIdx, String codFnd) {
        int position = Pos.wrdfCodFnd(codFndIdx - 1);
        writeString(position, codFnd, Len.COD_FND);
    }

    /**Original name: WRDF-COD-FND<br>*/
    public String getCodFnd(int codFndIdx) {
        int position = Pos.wrdfCodFnd(codFndIdx - 1);
        return readString(position, Len.COD_FND);
    }

    public void setNumQuo(int numQuoIdx, AfDecimal numQuo) {
        int position = Pos.wrdfNumQuo(numQuoIdx - 1);
        writeDecimalAsPacked(position, numQuo.copy());
    }

    /**Original name: WRDF-NUM-QUO<br>*/
    public AfDecimal getNumQuo(int numQuoIdx) {
        int position = Pos.wrdfNumQuo(numQuoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.NUM_QUO, Len.Fract.NUM_QUO);
    }

    public void setPc(int pcIdx, AfDecimal pc) {
        int position = Pos.wrdfPc(pcIdx - 1);
        writeDecimalAsPacked(position, pc.copy());
    }

    /**Original name: WRDF-PC<br>*/
    public AfDecimal getPc(int pcIdx) {
        int position = Pos.wrdfPc(pcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC, Len.Fract.PC);
    }

    public void setImpMovto(int impMovtoIdx, AfDecimal impMovto) {
        int position = Pos.wrdfImpMovto(impMovtoIdx - 1);
        writeDecimalAsPacked(position, impMovto.copy());
    }

    /**Original name: WRDF-IMP-MOVTO<br>*/
    public AfDecimal getImpMovto(int impMovtoIdx) {
        int position = Pos.wrdfImpMovto(impMovtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_MOVTO, Len.Fract.IMP_MOVTO);
    }

    public void setDtDis(int dtDisIdx, int dtDis) {
        int position = Pos.wrdfDtDis(dtDisIdx - 1);
        writeIntAsPacked(position, dtDis, Len.Int.DT_DIS);
    }

    /**Original name: WRDF-DT-DIS<br>*/
    public int getDtDis(int dtDisIdx) {
        int position = Pos.wrdfDtDis(dtDisIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_DIS);
    }

    public void setCodTari(int codTariIdx, String codTari) {
        int position = Pos.wrdfCodTari(codTariIdx - 1);
        writeString(position, codTari, Len.COD_TARI);
    }

    /**Original name: WRDF-COD-TARI<br>*/
    public String getCodTari(int codTariIdx) {
        int position = Pos.wrdfCodTari(codTariIdx - 1);
        return readString(position, Len.COD_TARI);
    }

    public void setTpStat(int tpStatIdx, String tpStat) {
        int position = Pos.wrdfTpStat(tpStatIdx - 1);
        writeString(position, tpStat, Len.TP_STAT);
    }

    /**Original name: WRDF-TP-STAT<br>*/
    public String getTpStat(int tpStatIdx) {
        int position = Pos.wrdfTpStat(tpStatIdx - 1);
        return readString(position, Len.TP_STAT);
    }

    public void setTpModDis(int tpModDisIdx, String tpModDis) {
        int position = Pos.wrdfTpModDis(tpModDisIdx - 1);
        writeString(position, tpModDis, Len.TP_MOD_DIS);
    }

    /**Original name: WRDF-TP-MOD-DIS<br>*/
    public String getTpModDis(int tpModDisIdx) {
        int position = Pos.wrdfTpModDis(tpModDisIdx - 1);
        return readString(position, Len.TP_MOD_DIS);
    }

    public void setCodDiv(int codDivIdx, String codDiv) {
        int position = Pos.wrdfCodDiv(codDivIdx - 1);
        writeString(position, codDiv, Len.COD_DIV);
    }

    /**Original name: WRDF-COD-DIV<br>*/
    public String getCodDiv(int codDivIdx) {
        int position = Pos.wrdfCodDiv(codDivIdx - 1);
        return readString(position, Len.COD_DIV);
    }

    public void setDtCambioVlt(int dtCambioVltIdx, int dtCambioVlt) {
        int position = Pos.wrdfDtCambioVlt(dtCambioVltIdx - 1);
        writeIntAsPacked(position, dtCambioVlt, Len.Int.DT_CAMBIO_VLT);
    }

    /**Original name: WRDF-DT-CAMBIO-VLT<br>*/
    public int getDtCambioVlt(int dtCambioVltIdx) {
        int position = Pos.wrdfDtCambioVlt(dtCambioVltIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_CAMBIO_VLT);
    }

    public void setTpFnd(int tpFndIdx, char tpFnd) {
        int position = Pos.wrdfTpFnd(tpFndIdx - 1);
        writeChar(position, tpFnd);
    }

    /**Original name: WRDF-TP-FND<br>*/
    public char getTpFnd(int tpFndIdx) {
        int position = Pos.wrdfTpFnd(tpFndIdx - 1);
        return readChar(position);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wrdfDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WRDF-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wrdfDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wrdfDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WRDF-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wrdfDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wrdfDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WRDF-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wrdfDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wrdfDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WRDF-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wrdfDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wrdfDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WRDF-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wrdfDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wrdfDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WRDF-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wrdfDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wrdfDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WRDF-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wrdfDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setDtDisCalc(int dtDisCalcIdx, int dtDisCalc) {
        int position = Pos.wrdfDtDisCalc(dtDisCalcIdx - 1);
        writeIntAsPacked(position, dtDisCalc, Len.Int.DT_DIS_CALC);
    }

    /**Original name: WRDF-DT-DIS-CALC<br>*/
    public int getDtDisCalc(int dtDisCalcIdx) {
        int position = Pos.wrdfDtDisCalc(dtDisCalcIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_DIS_CALC);
    }

    public void setFlCalcDis(int flCalcDisIdx, char flCalcDis) {
        int position = Pos.wrdfFlCalcDis(flCalcDisIdx - 1);
        writeChar(position, flCalcDis);
    }

    /**Original name: WRDF-FL-CALC-DIS<br>*/
    public char getFlCalcDis(int flCalcDisIdx) {
        int position = Pos.wrdfFlCalcDis(flCalcDisIdx - 1);
        return readChar(position);
    }

    public void setCommisGest(int commisGestIdx, AfDecimal commisGest) {
        int position = Pos.wrdfCommisGest(commisGestIdx - 1);
        writeDecimalAsPacked(position, commisGest.copy());
    }

    /**Original name: WRDF-COMMIS-GEST<br>*/
    public AfDecimal getCommisGest(int commisGestIdx) {
        int position = Pos.wrdfCommisGest(commisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_GEST, Len.Fract.COMMIS_GEST);
    }

    public void setNumQuoCdgFnz(int numQuoCdgFnzIdx, AfDecimal numQuoCdgFnz) {
        int position = Pos.wrdfNumQuoCdgFnz(numQuoCdgFnzIdx - 1);
        writeDecimalAsPacked(position, numQuoCdgFnz.copy());
    }

    /**Original name: WRDF-NUM-QUO-CDG-FNZ<br>*/
    public AfDecimal getNumQuoCdgFnz(int numQuoCdgFnzIdx) {
        int position = Pos.wrdfNumQuoCdgFnz(numQuoCdgFnzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.NUM_QUO_CDG_FNZ, Len.Fract.NUM_QUO_CDG_FNZ);
    }

    public void setNumQuoCdgtotFnz(int numQuoCdgtotFnzIdx, AfDecimal numQuoCdgtotFnz) {
        int position = Pos.wrdfNumQuoCdgtotFnz(numQuoCdgtotFnzIdx - 1);
        writeDecimalAsPacked(position, numQuoCdgtotFnz.copy());
    }

    /**Original name: WRDF-NUM-QUO-CDGTOT-FNZ<br>*/
    public AfDecimal getNumQuoCdgtotFnz(int numQuoCdgtotFnzIdx) {
        int position = Pos.wrdfNumQuoCdgtotFnz(numQuoCdgtotFnzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.NUM_QUO_CDGTOT_FNZ, Len.Fract.NUM_QUO_CDGTOT_FNZ);
    }

    public void setCosRunAssvaIdc(int cosRunAssvaIdcIdx, AfDecimal cosRunAssvaIdc) {
        int position = Pos.wrdfCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        writeDecimalAsPacked(position, cosRunAssvaIdc.copy());
    }

    /**Original name: WRDF-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getCosRunAssvaIdc(int cosRunAssvaIdcIdx) {
        int position = Pos.wrdfCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA_IDC, Len.Fract.COS_RUN_ASSVA_IDC);
    }

    public void setFlSwmBp2s(int flSwmBp2sIdx, char flSwmBp2s) {
        int position = Pos.wrdfFlSwmBp2s(flSwmBp2sIdx - 1);
        writeChar(position, flSwmBp2s);
    }

    /**Original name: WRDF-FL-SWM-BP2S<br>*/
    public char getFlSwmBp2s(int flSwmBp2sIdx) {
        int position = Pos.wrdfFlSwmBp2s(flSwmBp2sIdx - 1);
        return readChar(position);
    }

    public void setRestoTabella(String restoTabella) {
        writeString(Pos.RESTO_TABELLA, restoTabella, Len.RESTO_TABELLA);
    }

    /**Original name: WRDF-RESTO-TABELLA<br>
	 * <pre>          04 WRDF-RESTO-TABELLA      PIC X(259792).
	 *           04 WRDF-RESTO-TABELLA      PIC X(269784).</pre>*/
    public String getRestoTabella() {
        return readString(Pos.RESTO_TABELLA, Len.RESTO_TABELLA);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRDF_TABELLA = 1;
        public static final int WRDF_TABELLA_R = 1;
        public static final int FLR1 = WRDF_TABELLA_R;
        public static final int RESTO_TABELLA = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wrdfTabRicDisinv(int idx) {
            return WRDF_TABELLA + idx * Len.TAB_RIC_DISINV;
        }

        public static int wrdfStatus(int idx) {
            return wrdfTabRicDisinv(idx);
        }

        public static int wrdfIdPtf(int idx) {
            return wrdfStatus(idx) + Len.STATUS;
        }

        public static int wrdfDati(int idx) {
            return wrdfIdPtf(idx) + Len.ID_PTF;
        }

        public static int wrdfIdRichDisFnd(int idx) {
            return wrdfDati(idx);
        }

        public static int wrdfIdMoviFinrio(int idx) {
            return wrdfIdRichDisFnd(idx) + Len.ID_RICH_DIS_FND;
        }

        public static int wrdfIdMoviCrz(int idx) {
            return wrdfIdMoviFinrio(idx) + Len.ID_MOVI_FINRIO;
        }

        public static int wrdfIdMoviChiu(int idx) {
            return wrdfIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wrdfDtIniEff(int idx) {
            return wrdfIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wrdfDtEndEff(int idx) {
            return wrdfDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wrdfCodCompAnia(int idx) {
            return wrdfDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wrdfCodFnd(int idx) {
            return wrdfCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wrdfNumQuo(int idx) {
            return wrdfCodFnd(idx) + Len.COD_FND;
        }

        public static int wrdfPc(int idx) {
            return wrdfNumQuo(idx) + Len.NUM_QUO;
        }

        public static int wrdfImpMovto(int idx) {
            return wrdfPc(idx) + Len.PC;
        }

        public static int wrdfDtDis(int idx) {
            return wrdfImpMovto(idx) + Len.IMP_MOVTO;
        }

        public static int wrdfCodTari(int idx) {
            return wrdfDtDis(idx) + Len.DT_DIS;
        }

        public static int wrdfTpStat(int idx) {
            return wrdfCodTari(idx) + Len.COD_TARI;
        }

        public static int wrdfTpModDis(int idx) {
            return wrdfTpStat(idx) + Len.TP_STAT;
        }

        public static int wrdfCodDiv(int idx) {
            return wrdfTpModDis(idx) + Len.TP_MOD_DIS;
        }

        public static int wrdfDtCambioVlt(int idx) {
            return wrdfCodDiv(idx) + Len.COD_DIV;
        }

        public static int wrdfTpFnd(int idx) {
            return wrdfDtCambioVlt(idx) + Len.DT_CAMBIO_VLT;
        }

        public static int wrdfDsRiga(int idx) {
            return wrdfTpFnd(idx) + Len.TP_FND;
        }

        public static int wrdfDsOperSql(int idx) {
            return wrdfDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wrdfDsVer(int idx) {
            return wrdfDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wrdfDsTsIniCptz(int idx) {
            return wrdfDsVer(idx) + Len.DS_VER;
        }

        public static int wrdfDsTsEndCptz(int idx) {
            return wrdfDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wrdfDsUtente(int idx) {
            return wrdfDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wrdfDsStatoElab(int idx) {
            return wrdfDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wrdfDtDisCalc(int idx) {
            return wrdfDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wrdfFlCalcDis(int idx) {
            return wrdfDtDisCalc(idx) + Len.DT_DIS_CALC;
        }

        public static int wrdfCommisGest(int idx) {
            return wrdfFlCalcDis(idx) + Len.FL_CALC_DIS;
        }

        public static int wrdfNumQuoCdgFnz(int idx) {
            return wrdfCommisGest(idx) + Len.COMMIS_GEST;
        }

        public static int wrdfNumQuoCdgtotFnz(int idx) {
            return wrdfNumQuoCdgFnz(idx) + Len.NUM_QUO_CDG_FNZ;
        }

        public static int wrdfCosRunAssvaIdc(int idx) {
            return wrdfNumQuoCdgtotFnz(idx) + Len.NUM_QUO_CDGTOT_FNZ;
        }

        public static int wrdfFlSwmBp2s(int idx) {
            return wrdfCosRunAssvaIdc(idx) + Len.COS_RUN_ASSVA_IDC;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_RICH_DIS_FND = 5;
        public static final int ID_MOVI_FINRIO = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_FND = 20;
        public static final int NUM_QUO = 7;
        public static final int PC = 4;
        public static final int IMP_MOVTO = 8;
        public static final int DT_DIS = 5;
        public static final int COD_TARI = 12;
        public static final int TP_STAT = 2;
        public static final int TP_MOD_DIS = 2;
        public static final int COD_DIV = 20;
        public static final int DT_CAMBIO_VLT = 5;
        public static final int TP_FND = 1;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DT_DIS_CALC = 5;
        public static final int FL_CALC_DIS = 1;
        public static final int COMMIS_GEST = 10;
        public static final int NUM_QUO_CDG_FNZ = 7;
        public static final int NUM_QUO_CDGTOT_FNZ = 7;
        public static final int COS_RUN_ASSVA_IDC = 8;
        public static final int FL_SWM_BP2S = 1;
        public static final int DATI = ID_RICH_DIS_FND + ID_MOVI_FINRIO + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_FND + NUM_QUO + PC + IMP_MOVTO + DT_DIS + COD_TARI + TP_STAT + TP_MOD_DIS + COD_DIV + DT_CAMBIO_VLT + TP_FND + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + DT_DIS_CALC + FL_CALC_DIS + COMMIS_GEST + NUM_QUO_CDG_FNZ + NUM_QUO_CDGTOT_FNZ + COS_RUN_ASSVA_IDC + FL_SWM_BP2S;
        public static final int TAB_RIC_DISINV = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 217;
        public static final int WRDF_TABELLA = WrdfTabella.TAB_RIC_DISINV_MAXOCCURS * TAB_RIC_DISINV;
        public static final int RESTO_TABELLA = 271033;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_RICH_DIS_FND = 9;
            public static final int ID_MOVI_FINRIO = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int NUM_QUO = 7;
            public static final int PC = 3;
            public static final int IMP_MOVTO = 12;
            public static final int DT_DIS = 8;
            public static final int DT_CAMBIO_VLT = 8;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int DT_DIS_CALC = 8;
            public static final int COMMIS_GEST = 11;
            public static final int NUM_QUO_CDG_FNZ = 7;
            public static final int NUM_QUO_CDGTOT_FNZ = 7;
            public static final int COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int NUM_QUO = 5;
            public static final int PC = 3;
            public static final int IMP_MOVTO = 3;
            public static final int COMMIS_GEST = 7;
            public static final int NUM_QUO_CDG_FNZ = 5;
            public static final int NUM_QUO_CDGTOT_FNZ = 5;
            public static final int COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
