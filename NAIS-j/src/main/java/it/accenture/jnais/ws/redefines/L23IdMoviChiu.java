package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-ID-MOVI-CHIU<br>
 * Variable: L23-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_ID_MOVI_CHIU;
    }

    public void setL23IdMoviChiu(int l23IdMoviChiu) {
        writeIntAsPacked(Pos.L23_ID_MOVI_CHIU, l23IdMoviChiu, Len.Int.L23_ID_MOVI_CHIU);
    }

    public void setL23IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_ID_MOVI_CHIU, Pos.L23_ID_MOVI_CHIU);
    }

    /**Original name: L23-ID-MOVI-CHIU<br>*/
    public int getL23IdMoviChiu() {
        return readPackedAsInt(Pos.L23_ID_MOVI_CHIU, Len.Int.L23_ID_MOVI_CHIU);
    }

    public byte[] getL23IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_ID_MOVI_CHIU, Pos.L23_ID_MOVI_CHIU);
        return buffer;
    }

    public void setL23IdMoviChiuNull(String l23IdMoviChiuNull) {
        writeString(Pos.L23_ID_MOVI_CHIU_NULL, l23IdMoviChiuNull, Len.L23_ID_MOVI_CHIU_NULL);
    }

    /**Original name: L23-ID-MOVI-CHIU-NULL<br>*/
    public String getL23IdMoviChiuNull() {
        return readString(Pos.L23_ID_MOVI_CHIU_NULL, Len.L23_ID_MOVI_CHIU_NULL);
    }

    public String getL23IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getL23IdMoviChiuNull(), Len.L23_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_ID_MOVI_CHIU = 1;
        public static final int L23_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_ID_MOVI_CHIU = 5;
        public static final int L23_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
