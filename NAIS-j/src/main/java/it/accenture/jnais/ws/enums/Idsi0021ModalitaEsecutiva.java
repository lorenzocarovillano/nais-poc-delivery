package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSI0021-MODALITA-ESECUTIVA<br>
 * Variable: IDSI0021-MODALITA-ESECUTIVA from copybook IDSI0021<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0021ModalitaEsecutiva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ON_LINE = 'O';
    public static final char BATCH = 'B';

    //==== METHODS ====
    public void setModalitaEsecutiva(char modalitaEsecutiva) {
        this.value = modalitaEsecutiva;
    }

    public char getModalitaEsecutiva() {
        return this.value;
    }

    public void setIdsi0021OnLine() {
        value = ON_LINE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ESECUTIVA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
