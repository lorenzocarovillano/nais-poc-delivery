package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B01-PC-INVST<br>
 * Variable: W-B01-PC-INVST from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB01PcInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB01PcInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B01_PC_INVST;
    }

    public void setwB01PcInvst(AfDecimal wB01PcInvst) {
        writeDecimalAsPacked(Pos.W_B01_PC_INVST, wB01PcInvst.copy());
    }

    /**Original name: W-B01-PC-INVST<br>*/
    public AfDecimal getwB01PcInvst() {
        return readPackedAsDecimal(Pos.W_B01_PC_INVST, Len.Int.W_B01_PC_INVST, Len.Fract.W_B01_PC_INVST);
    }

    public byte[] getwB01PcInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B01_PC_INVST, Pos.W_B01_PC_INVST);
        return buffer;
    }

    public void setwB01PcInvstNull(String wB01PcInvstNull) {
        writeString(Pos.W_B01_PC_INVST_NULL, wB01PcInvstNull, Len.W_B01_PC_INVST_NULL);
    }

    /**Original name: W-B01-PC-INVST-NULL<br>*/
    public String getwB01PcInvstNull() {
        return readString(Pos.W_B01_PC_INVST_NULL, Len.W_B01_PC_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B01_PC_INVST = 1;
        public static final int W_B01_PC_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B01_PC_INVST = 4;
        public static final int W_B01_PC_INVST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B01_PC_INVST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B01_PC_INVST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
