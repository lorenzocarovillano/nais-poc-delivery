package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-MEDIO<br>
 * Variable: B03-TS-MEDIO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsMedio extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsMedio() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_MEDIO;
    }

    public void setB03TsMedio(AfDecimal b03TsMedio) {
        writeDecimalAsPacked(Pos.B03_TS_MEDIO, b03TsMedio.copy());
    }

    public void setB03TsMedioFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_MEDIO, Pos.B03_TS_MEDIO);
    }

    /**Original name: B03-TS-MEDIO<br>*/
    public AfDecimal getB03TsMedio() {
        return readPackedAsDecimal(Pos.B03_TS_MEDIO, Len.Int.B03_TS_MEDIO, Len.Fract.B03_TS_MEDIO);
    }

    public byte[] getB03TsMedioAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_MEDIO, Pos.B03_TS_MEDIO);
        return buffer;
    }

    public void setB03TsMedioNull(String b03TsMedioNull) {
        writeString(Pos.B03_TS_MEDIO_NULL, b03TsMedioNull, Len.B03_TS_MEDIO_NULL);
    }

    /**Original name: B03-TS-MEDIO-NULL<br>*/
    public String getB03TsMedioNull() {
        return readString(Pos.B03_TS_MEDIO_NULL, Len.B03_TS_MEDIO_NULL);
    }

    public String getB03TsMedioNullFormatted() {
        return Functions.padBlanks(getB03TsMedioNull(), Len.B03_TS_MEDIO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_MEDIO = 1;
        public static final int B03_TS_MEDIO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_MEDIO = 8;
        public static final int B03_TS_MEDIO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_MEDIO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_MEDIO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
