package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.Idsv0101Addresses;

/**Original name: AREA-IDSV0101<br>
 * Variable: AREA-IDSV0101 from program IVVS0216<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIdsv0101 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int ADDRESSES_MAXOCCURS = 5;
    //Original name: IDSV0101-ADDRESSES
    private Idsv0101Addresses[] addresses = new Idsv0101Addresses[ADDRESSES_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIdsv0101() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IDSV0101;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIdsv0101Bytes(buf);
    }

    public void init() {
        for (int addressesIdx = 1; addressesIdx <= ADDRESSES_MAXOCCURS; addressesIdx++) {
            addresses[addressesIdx - 1] = new Idsv0101Addresses();
        }
    }

    public String getAreaIdsv0101Formatted() {
        return getAreaAddressesFormatted();
    }

    public void setAreaIdsv0101Bytes(byte[] buffer) {
        setAreaIdsv0101Bytes(buffer, 1);
    }

    public byte[] getAreaIdsv0101Bytes() {
        byte[] buffer = new byte[Len.AREA_IDSV0101];
        return getAreaIdsv0101Bytes(buffer, 1);
    }

    public void setAreaIdsv0101Bytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaAddressesBytes(buffer, position);
    }

    public byte[] getAreaIdsv0101Bytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaAddressesBytes(buffer, position);
        return buffer;
    }

    public String getAreaAddressesFormatted() {
        return MarshalByteExt.bufferToStr(getAreaAddressesBytes());
    }

    public void setAreaAddressesBytes(byte[] buffer) {
        setAreaAddressesBytes(buffer, 1);
    }

    /**Original name: IDSV0101-AREA-ADDRESSES<br>*/
    public byte[] getAreaAddressesBytes() {
        byte[] buffer = new byte[Len.AREA_ADDRESSES];
        return getAreaAddressesBytes(buffer, 1);
    }

    public void setAreaAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESSES_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                addresses[idx - 1].setAddressesBytes(buffer, position);
                position += Idsv0101Addresses.Len.ADDRESSES;
            }
            else {
                addresses[idx - 1].initAddressesSpaces();
                position += Idsv0101Addresses.Len.ADDRESSES;
            }
        }
    }

    public byte[] getAreaAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= ADDRESSES_MAXOCCURS; idx++) {
            addresses[idx - 1].getAddressesBytes(buffer, position);
            position += Idsv0101Addresses.Len.ADDRESSES;
        }
        return buffer;
    }

    public Idsv0101Addresses getAddresses(int idx) {
        return addresses[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaIdsv0101Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AREA_ADDRESSES = AreaIdsv0101.ADDRESSES_MAXOCCURS * Idsv0101Addresses.Len.ADDRESSES;
        public static final int AREA_IDSV0101 = AREA_ADDRESSES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
