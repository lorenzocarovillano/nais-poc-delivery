package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-PC-ESE-IMPST-TFR<br>
 * Variable: WDFA-PC-ESE-IMPST-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaPcEseImpstTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaPcEseImpstTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_PC_ESE_IMPST_TFR;
    }

    public void setWdfaPcEseImpstTfr(AfDecimal wdfaPcEseImpstTfr) {
        writeDecimalAsPacked(Pos.WDFA_PC_ESE_IMPST_TFR, wdfaPcEseImpstTfr.copy());
    }

    public void setWdfaPcEseImpstTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_PC_ESE_IMPST_TFR, Pos.WDFA_PC_ESE_IMPST_TFR);
    }

    /**Original name: WDFA-PC-ESE-IMPST-TFR<br>*/
    public AfDecimal getWdfaPcEseImpstTfr() {
        return readPackedAsDecimal(Pos.WDFA_PC_ESE_IMPST_TFR, Len.Int.WDFA_PC_ESE_IMPST_TFR, Len.Fract.WDFA_PC_ESE_IMPST_TFR);
    }

    public byte[] getWdfaPcEseImpstTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_PC_ESE_IMPST_TFR, Pos.WDFA_PC_ESE_IMPST_TFR);
        return buffer;
    }

    public void setWdfaPcEseImpstTfrNull(String wdfaPcEseImpstTfrNull) {
        writeString(Pos.WDFA_PC_ESE_IMPST_TFR_NULL, wdfaPcEseImpstTfrNull, Len.WDFA_PC_ESE_IMPST_TFR_NULL);
    }

    /**Original name: WDFA-PC-ESE-IMPST-TFR-NULL<br>*/
    public String getWdfaPcEseImpstTfrNull() {
        return readString(Pos.WDFA_PC_ESE_IMPST_TFR_NULL, Len.WDFA_PC_ESE_IMPST_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_PC_ESE_IMPST_TFR = 1;
        public static final int WDFA_PC_ESE_IMPST_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_PC_ESE_IMPST_TFR = 4;
        public static final int WDFA_PC_ESE_IMPST_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_PC_ESE_IMPST_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_PC_ESE_IMPST_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
