package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RAN-ID-RAPP-ANA-COLLG<br>
 * Variable: RAN-ID-RAPP-ANA-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RanIdRappAnaCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RanIdRappAnaCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAN_ID_RAPP_ANA_COLLG;
    }

    public void setRanIdRappAnaCollg(int ranIdRappAnaCollg) {
        writeIntAsPacked(Pos.RAN_ID_RAPP_ANA_COLLG, ranIdRappAnaCollg, Len.Int.RAN_ID_RAPP_ANA_COLLG);
    }

    public void setRanIdRappAnaCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RAN_ID_RAPP_ANA_COLLG, Pos.RAN_ID_RAPP_ANA_COLLG);
    }

    /**Original name: RAN-ID-RAPP-ANA-COLLG<br>*/
    public int getRanIdRappAnaCollg() {
        return readPackedAsInt(Pos.RAN_ID_RAPP_ANA_COLLG, Len.Int.RAN_ID_RAPP_ANA_COLLG);
    }

    public byte[] getRanIdRappAnaCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RAN_ID_RAPP_ANA_COLLG, Pos.RAN_ID_RAPP_ANA_COLLG);
        return buffer;
    }

    public void setRanIdRappAnaCollgNull(String ranIdRappAnaCollgNull) {
        writeString(Pos.RAN_ID_RAPP_ANA_COLLG_NULL, ranIdRappAnaCollgNull, Len.RAN_ID_RAPP_ANA_COLLG_NULL);
    }

    /**Original name: RAN-ID-RAPP-ANA-COLLG-NULL<br>*/
    public String getRanIdRappAnaCollgNull() {
        return readString(Pos.RAN_ID_RAPP_ANA_COLLG_NULL, Len.RAN_ID_RAPP_ANA_COLLG_NULL);
    }

    public String getRanIdRappAnaCollgNullFormatted() {
        return Functions.padBlanks(getRanIdRappAnaCollgNull(), Len.RAN_ID_RAPP_ANA_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RAN_ID_RAPP_ANA_COLLG = 1;
        public static final int RAN_ID_RAPP_ANA_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_ID_RAPP_ANA_COLLG = 5;
        public static final int RAN_ID_RAPP_ANA_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_ID_RAPP_ANA_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
