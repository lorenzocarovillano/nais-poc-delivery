package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPST-SOST-662014<br>
 * Variable: TCL-IMPST-SOST-662014 from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpstSost662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpstSost662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPST_SOST662014;
    }

    public void setTclImpstSost662014(AfDecimal tclImpstSost662014) {
        writeDecimalAsPacked(Pos.TCL_IMPST_SOST662014, tclImpstSost662014.copy());
    }

    public void setTclImpstSost662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPST_SOST662014, Pos.TCL_IMPST_SOST662014);
    }

    /**Original name: TCL-IMPST-SOST-662014<br>*/
    public AfDecimal getTclImpstSost662014() {
        return readPackedAsDecimal(Pos.TCL_IMPST_SOST662014, Len.Int.TCL_IMPST_SOST662014, Len.Fract.TCL_IMPST_SOST662014);
    }

    public byte[] getTclImpstSost662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPST_SOST662014, Pos.TCL_IMPST_SOST662014);
        return buffer;
    }

    public void setTclImpstSost662014Null(String tclImpstSost662014Null) {
        writeString(Pos.TCL_IMPST_SOST662014_NULL, tclImpstSost662014Null, Len.TCL_IMPST_SOST662014_NULL);
    }

    /**Original name: TCL-IMPST-SOST-662014-NULL<br>*/
    public String getTclImpstSost662014Null() {
        return readString(Pos.TCL_IMPST_SOST662014_NULL, Len.TCL_IMPST_SOST662014_NULL);
    }

    public String getTclImpstSost662014NullFormatted() {
        return Functions.padBlanks(getTclImpstSost662014Null(), Len.TCL_IMPST_SOST662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPST_SOST662014 = 1;
        public static final int TCL_IMPST_SOST662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPST_SOST662014 = 8;
        public static final int TCL_IMPST_SOST662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPST_SOST662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPST_SOST662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
