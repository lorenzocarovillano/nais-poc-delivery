package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-MANFEE-RICOR<br>
 * Variable: TIT-TOT-MANFEE-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotManfeeRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotManfeeRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_MANFEE_RICOR;
    }

    public void setTitTotManfeeRicor(AfDecimal titTotManfeeRicor) {
        writeDecimalAsPacked(Pos.TIT_TOT_MANFEE_RICOR, titTotManfeeRicor.copy());
    }

    public void setTitTotManfeeRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_MANFEE_RICOR, Pos.TIT_TOT_MANFEE_RICOR);
    }

    /**Original name: TIT-TOT-MANFEE-RICOR<br>*/
    public AfDecimal getTitTotManfeeRicor() {
        return readPackedAsDecimal(Pos.TIT_TOT_MANFEE_RICOR, Len.Int.TIT_TOT_MANFEE_RICOR, Len.Fract.TIT_TOT_MANFEE_RICOR);
    }

    public byte[] getTitTotManfeeRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_MANFEE_RICOR, Pos.TIT_TOT_MANFEE_RICOR);
        return buffer;
    }

    public void setTitTotManfeeRicorNull(String titTotManfeeRicorNull) {
        writeString(Pos.TIT_TOT_MANFEE_RICOR_NULL, titTotManfeeRicorNull, Len.TIT_TOT_MANFEE_RICOR_NULL);
    }

    /**Original name: TIT-TOT-MANFEE-RICOR-NULL<br>*/
    public String getTitTotManfeeRicorNull() {
        return readString(Pos.TIT_TOT_MANFEE_RICOR_NULL, Len.TIT_TOT_MANFEE_RICOR_NULL);
    }

    public String getTitTotManfeeRicorNullFormatted() {
        return Functions.padBlanks(getTitTotManfeeRicorNull(), Len.TIT_TOT_MANFEE_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_MANFEE_RICOR = 1;
        public static final int TIT_TOT_MANFEE_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_MANFEE_RICOR = 8;
        public static final int TIT_TOT_MANFEE_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_MANFEE_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_MANFEE_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
