package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCOM-VARIABILI-COND-WF<br>
 * Variables: WCOM-VARIABILI-COND-WF from copybook LCCC0001<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcomVariabiliCondWf {

    //==== PROPERTIES ====
    //Original name: WCOM-COD-OPERANDO-WF
    private String codOperandoWf = DefaultValues.stringVal(Len.COD_OPERANDO_WF);
    //Original name: WCOM-VAL-OPERANDO-WF
    private String valOperandoWf = DefaultValues.stringVal(Len.VAL_OPERANDO_WF);

    //==== METHODS ====
    public void setVariabiliCondWfBytes(byte[] buffer, int offset) {
        int position = offset;
        codOperandoWf = MarshalByte.readString(buffer, position, Len.COD_OPERANDO_WF);
        position += Len.COD_OPERANDO_WF;
        valOperandoWf = MarshalByte.readString(buffer, position, Len.VAL_OPERANDO_WF);
    }

    public byte[] getVariabiliCondWfBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codOperandoWf, Len.COD_OPERANDO_WF);
        position += Len.COD_OPERANDO_WF;
        MarshalByte.writeString(buffer, position, valOperandoWf, Len.VAL_OPERANDO_WF);
        return buffer;
    }

    public void initVariabiliCondWfSpaces() {
        codOperandoWf = "";
        valOperandoWf = "";
    }

    public void setCodOperandoWf(String codOperandoWf) {
        this.codOperandoWf = Functions.subString(codOperandoWf, Len.COD_OPERANDO_WF);
    }

    public String getCodOperandoWf() {
        return this.codOperandoWf;
    }

    public void setValOperandoWf(String valOperandoWf) {
        this.valOperandoWf = Functions.subString(valOperandoWf, Len.VAL_OPERANDO_WF);
    }

    public String getValOperandoWf() {
        return this.valOperandoWf;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_OPERANDO_WF = 30;
        public static final int VAL_OPERANDO_WF = 100;
        public static final int VARIABILI_COND_WF = COD_OPERANDO_WF + VAL_OPERANDO_WF;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
