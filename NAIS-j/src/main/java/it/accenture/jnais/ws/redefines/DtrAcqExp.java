package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-ACQ-EXP<br>
 * Variable: DTR-ACQ-EXP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_ACQ_EXP;
    }

    public void setDtrAcqExp(AfDecimal dtrAcqExp) {
        writeDecimalAsPacked(Pos.DTR_ACQ_EXP, dtrAcqExp.copy());
    }

    public void setDtrAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_ACQ_EXP, Pos.DTR_ACQ_EXP);
    }

    /**Original name: DTR-ACQ-EXP<br>*/
    public AfDecimal getDtrAcqExp() {
        return readPackedAsDecimal(Pos.DTR_ACQ_EXP, Len.Int.DTR_ACQ_EXP, Len.Fract.DTR_ACQ_EXP);
    }

    public byte[] getDtrAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_ACQ_EXP, Pos.DTR_ACQ_EXP);
        return buffer;
    }

    public void setDtrAcqExpNull(String dtrAcqExpNull) {
        writeString(Pos.DTR_ACQ_EXP_NULL, dtrAcqExpNull, Len.DTR_ACQ_EXP_NULL);
    }

    /**Original name: DTR-ACQ-EXP-NULL<br>*/
    public String getDtrAcqExpNull() {
        return readString(Pos.DTR_ACQ_EXP_NULL, Len.DTR_ACQ_EXP_NULL);
    }

    public String getDtrAcqExpNullFormatted() {
        return Functions.padBlanks(getDtrAcqExpNull(), Len.DTR_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_ACQ_EXP = 1;
        public static final int DTR_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_ACQ_EXP = 8;
        public static final int DTR_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
