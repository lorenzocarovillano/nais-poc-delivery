package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.ptr.BrsDataRecord;

/**Original name: BTC-REC-SCHEDULE<br>
 * Variable: BTC-REC-SCHEDULE from copybook IDBVBRS1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcRecScheduleLlbm0230 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: BRS-ID-BATCH
    private int idBatch = DefaultValues.BIN_INT_VAL;
    //Original name: BRS-ID-JOB
    private int idJob = DefaultValues.BIN_INT_VAL;
    //Original name: BRS-ID-RECORD
    private int idRecord = DefaultValues.BIN_INT_VAL;
    //Original name: BRS-TYPE-RECORD
    private String typeRecord = DefaultValues.stringVal(Len.TYPE_RECORD);
    //Original name: BRS-DATA-RECORD-LEN
    private short dataRecordLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BRS-DATA-RECORD
    private BrsDataRecord dataRecord = new BrsDataRecord();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_REC_SCHEDULE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcRecScheduleBytes(buf);
    }

    public String getBtcRecScheduleFormatted() {
        return MarshalByteExt.bufferToStr(getBtcRecScheduleBytes());
    }

    public void setBtcRecScheduleBytes(byte[] buffer) {
        setBtcRecScheduleBytes(buffer, 1);
    }

    public byte[] getBtcRecScheduleBytes() {
        byte[] buffer = new byte[Len.BTC_REC_SCHEDULE];
        return getBtcRecScheduleBytes(buffer, 1);
    }

    public void setBtcRecScheduleBytes(byte[] buffer, int offset) {
        int position = offset;
        idBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idJob = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idRecord = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        typeRecord = MarshalByte.readString(buffer, position, Len.TYPE_RECORD);
        position += Len.TYPE_RECORD;
        setDataRecordVcharBytes(buffer, position);
    }

    public byte[] getBtcRecScheduleBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, idBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idJob);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idRecord);
        position += Types.INT_SIZE;
        MarshalByte.writeString(buffer, position, typeRecord, Len.TYPE_RECORD);
        position += Len.TYPE_RECORD;
        getDataRecordVcharBytes(buffer, position);
        return buffer;
    }

    public void setIdBatch(int idBatch) {
        this.idBatch = idBatch;
    }

    public int getIdBatch() {
        return this.idBatch;
    }

    public void setIdJob(int idJob) {
        this.idJob = idJob;
    }

    public int getIdJob() {
        return this.idJob;
    }

    public void setIdRecord(int idRecord) {
        this.idRecord = idRecord;
    }

    public int getIdRecord() {
        return this.idRecord;
    }

    public void setTypeRecord(String typeRecord) {
        this.typeRecord = Functions.subString(typeRecord, Len.TYPE_RECORD);
    }

    public String getTypeRecord() {
        return this.typeRecord;
    }

    public void setDataRecordVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        dataRecordLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        dataRecord.setDataRecordFromBuffer(buffer, position);
    }

    public byte[] getDataRecordVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, dataRecordLen);
        position += Types.SHORT_SIZE;
        dataRecord.getDataRecordAsBuffer(buffer, position);
        return buffer;
    }

    public void setDataRecordLen(short dataRecordLen) {
        this.dataRecordLen = dataRecordLen;
    }

    public short getDataRecordLen() {
        return this.dataRecordLen;
    }

    public BrsDataRecord getDataRecord() {
        return dataRecord;
    }

    @Override
    public byte[] serialize() {
        return getBtcRecScheduleBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_BATCH = 4;
        public static final int ID_JOB = 4;
        public static final int ID_RECORD = 4;
        public static final int TYPE_RECORD = 3;
        public static final int DATA_RECORD_LEN = 2;
        public static final int DATA_RECORD_VCHAR = DATA_RECORD_LEN + BrsDataRecord.Len.DATA_RECORD;
        public static final int BTC_REC_SCHEDULE = ID_BATCH + ID_JOB + ID_RECORD + TYPE_RECORD + DATA_RECORD_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
