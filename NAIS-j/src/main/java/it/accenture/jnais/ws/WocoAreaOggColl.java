package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.ws.redefines.WocoTab;

/**Original name: WOCO-AREA-OGG-COLL<br>
 * Variable: WOCO-AREA-OGG-COLL from program IVVS0211<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WocoAreaOggColl {

    //==== PROPERTIES ====
    //Original name: WOCO-ELE-OGG-COLL-MAX
    private short wocoEleOggCollMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WOCO-TAB
    private WocoTab wocoTab = new WocoTab();

    //==== METHODS ====
    public void setWocoAreaOggCollFormatted(String data) {
        byte[] buffer = new byte[Len.WOCO_AREA_OGG_COLL];
        MarshalByte.writeString(buffer, 1, data, Len.WOCO_AREA_OGG_COLL);
        setWocoAreaOggCollBytes(buffer, 1);
    }

    public void setWocoAreaOggCollBytes(byte[] buffer, int offset) {
        int position = offset;
        wocoEleOggCollMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        wocoTab.setWocoTabBytes(buffer, position);
    }

    public byte[] getWocoAreaOggCollBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wocoEleOggCollMax);
        position += Types.SHORT_SIZE;
        wocoTab.getWocoTabBytes(buffer, position);
        return buffer;
    }

    public void setWocoEleOggCollMax(short wocoEleOggCollMax) {
        this.wocoEleOggCollMax = wocoEleOggCollMax;
    }

    public short getWocoEleOggCollMax() {
        return this.wocoEleOggCollMax;
    }

    public WocoTab getWocoTab() {
        return wocoTab;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_ELE_OGG_COLL_MAX = 2;
        public static final int WOCO_AREA_OGG_COLL = WOCO_ELE_OGG_COLL_MAX + WocoTab.Len.WOCO_TAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
