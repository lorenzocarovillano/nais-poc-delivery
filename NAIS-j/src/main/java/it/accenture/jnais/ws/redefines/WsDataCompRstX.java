package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-DATA-COMP-RST-X<br>
 * Variable: WS-DATA-COMP-RST-X from program LVVS0113<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsDataCompRstX extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsDataCompRstX() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_DATA_COMP_RST_X;
    }

    public void setDtCompRst8Formatted(String dtCompRst8) {
        writeString(Pos.DT_COMP_RST8, Trunc.toUnsignedNumeric(dtCompRst8, Len.DT_COMP_RST8), Len.DT_COMP_RST8);
    }

    /**Original name: WS-DT-COMP-RST-8<br>*/
    public int getDtCompRst8() {
        return readNumDispUnsignedInt(Pos.DT_COMP_RST8, Len.DT_COMP_RST8);
    }

    public void setLt40Rst(String lt40Rst) {
        writeString(Pos.LT40_RST, lt40Rst, Len.LT40_RST);
    }

    /**Original name: WS-LT-40-RST<br>*/
    public String getLt40Rst() {
        return readString(Pos.LT40_RST, Len.LT40_RST);
    }

    public void setLtOraCompRst8(String ltOraCompRst8) {
        writeString(Pos.LT_ORA_COMP_RST8, ltOraCompRst8, Len.LT_ORA_COMP_RST8);
    }

    /**Original name: WS-LT-ORA-COMP-RST-8<br>*/
    public String getLtOraCompRst8() {
        return readString(Pos.LT_ORA_COMP_RST8, Len.LT_ORA_COMP_RST8);
    }

    /**Original name: WS-DATA-COMP-RST-9<br>*/
    public long getWsDataCompRst9() {
        return readNumDispUnsignedLong(Pos.WS_DATA_COMP_RST9, Len.WS_DATA_COMP_RST9);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_DATA_COMP_RST_X = 1;
        public static final int DT_COMP_RST8 = WS_DATA_COMP_RST_X;
        public static final int LT40_RST = DT_COMP_RST8 + Len.DT_COMP_RST8;
        public static final int LT_ORA_COMP_RST8 = LT40_RST + Len.LT40_RST;
        public static final int WS_DATA_COMP_RST9 = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DT_COMP_RST8 = 8;
        public static final int LT40_RST = 2;
        public static final int LT_ORA_COMP_RST8 = 8;
        public static final int WS_DATA_COMP_RST_X = DT_COMP_RST8 + LT40_RST + LT_ORA_COMP_RST8;
        public static final int WS_DATA_COMP_RST9 = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
