package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-TOT-PRE<br>
 * Variable: WOCO-TOT-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoTotPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoTotPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_TOT_PRE;
    }

    public void setWocoTotPre(AfDecimal wocoTotPre) {
        writeDecimalAsPacked(Pos.WOCO_TOT_PRE, wocoTotPre.copy());
    }

    public void setWocoTotPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_TOT_PRE, Pos.WOCO_TOT_PRE);
    }

    /**Original name: WOCO-TOT-PRE<br>*/
    public AfDecimal getWocoTotPre() {
        return readPackedAsDecimal(Pos.WOCO_TOT_PRE, Len.Int.WOCO_TOT_PRE, Len.Fract.WOCO_TOT_PRE);
    }

    public byte[] getWocoTotPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_TOT_PRE, Pos.WOCO_TOT_PRE);
        return buffer;
    }

    public void initWocoTotPreSpaces() {
        fill(Pos.WOCO_TOT_PRE, Len.WOCO_TOT_PRE, Types.SPACE_CHAR);
    }

    public void setWocoTotPreNull(String wocoTotPreNull) {
        writeString(Pos.WOCO_TOT_PRE_NULL, wocoTotPreNull, Len.WOCO_TOT_PRE_NULL);
    }

    /**Original name: WOCO-TOT-PRE-NULL<br>*/
    public String getWocoTotPreNull() {
        return readString(Pos.WOCO_TOT_PRE_NULL, Len.WOCO_TOT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_TOT_PRE = 1;
        public static final int WOCO_TOT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_TOT_PRE = 8;
        public static final int WOCO_TOT_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_TOT_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_TOT_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
