package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-CAR-INC<br>
 * Variable: WTIT-TOT-CAR-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_CAR_INC;
    }

    public void setWtitTotCarInc(AfDecimal wtitTotCarInc) {
        writeDecimalAsPacked(Pos.WTIT_TOT_CAR_INC, wtitTotCarInc.copy());
    }

    public void setWtitTotCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_CAR_INC, Pos.WTIT_TOT_CAR_INC);
    }

    /**Original name: WTIT-TOT-CAR-INC<br>*/
    public AfDecimal getWtitTotCarInc() {
        return readPackedAsDecimal(Pos.WTIT_TOT_CAR_INC, Len.Int.WTIT_TOT_CAR_INC, Len.Fract.WTIT_TOT_CAR_INC);
    }

    public byte[] getWtitTotCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_CAR_INC, Pos.WTIT_TOT_CAR_INC);
        return buffer;
    }

    public void initWtitTotCarIncSpaces() {
        fill(Pos.WTIT_TOT_CAR_INC, Len.WTIT_TOT_CAR_INC, Types.SPACE_CHAR);
    }

    public void setWtitTotCarIncNull(String wtitTotCarIncNull) {
        writeString(Pos.WTIT_TOT_CAR_INC_NULL, wtitTotCarIncNull, Len.WTIT_TOT_CAR_INC_NULL);
    }

    /**Original name: WTIT-TOT-CAR-INC-NULL<br>*/
    public String getWtitTotCarIncNull() {
        return readString(Pos.WTIT_TOT_CAR_INC_NULL, Len.WTIT_TOT_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_INC = 1;
        public static final int WTIT_TOT_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_CAR_INC = 8;
        public static final int WTIT_TOT_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
