package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Lccvl711;
import it.accenture.jnais.copy.MatrElabBatch;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS1050<br>
 * Generated as a class for rule WS.<br>*/
public class Loas1050Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LOAS1050";
    //Original name: WK-TABELLA
    private String wkTabella = "";
    //Original name: WL71-ELE-MATR-ELAB-MAX
    private int wl71EleMatrElabMax = DefaultValues.BIN_INT_VAL;
    //Original name: LCCVL711
    private Lccvl711 lccvl711 = new Lccvl711();
    //Original name: IX-TAB-L71
    private short ixTabL71 = DefaultValues.BIN_SHORT_VAL;
    //Original name: AREA-IO-LCCS0090
    private LinkArea areaIoLccs0090 = new LinkArea();
    //Original name: MATR-ELAB-BATCH
    private MatrElabBatch matrElabBatch = new MatrElabBatch();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setWl71EleMatrElabMax(int wl71EleMatrElabMax) {
        this.wl71EleMatrElabMax = wl71EleMatrElabMax;
    }

    public int getWl71EleMatrElabMax() {
        return this.wl71EleMatrElabMax;
    }

    public void setIxTabL71(short ixTabL71) {
        this.ixTabL71 = ixTabL71;
    }

    public short getIxTabL71() {
        return this.ixTabL71;
    }

    public LinkArea getAreaIoLccs0090() {
        return areaIoLccs0090;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Lccvl711 getLccvl711() {
        return lccvl711;
    }

    public MatrElabBatch getMatrElabBatch() {
        return matrElabBatch;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TABELLA = 20;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
