package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-RISTORNI-CAP<br>
 * Variable: WB03-RIS-RISTORNI-CAP from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisRistorniCap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisRistorniCap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_RISTORNI_CAP;
    }

    public void setWb03RisRistorniCapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_RISTORNI_CAP, Pos.WB03_RIS_RISTORNI_CAP);
    }

    /**Original name: WB03-RIS-RISTORNI-CAP<br>*/
    public AfDecimal getWb03RisRistorniCap() {
        return readPackedAsDecimal(Pos.WB03_RIS_RISTORNI_CAP, Len.Int.WB03_RIS_RISTORNI_CAP, Len.Fract.WB03_RIS_RISTORNI_CAP);
    }

    public byte[] getWb03RisRistorniCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_RISTORNI_CAP, Pos.WB03_RIS_RISTORNI_CAP);
        return buffer;
    }

    public void setWb03RisRistorniCapNull(String wb03RisRistorniCapNull) {
        writeString(Pos.WB03_RIS_RISTORNI_CAP_NULL, wb03RisRistorniCapNull, Len.WB03_RIS_RISTORNI_CAP_NULL);
    }

    /**Original name: WB03-RIS-RISTORNI-CAP-NULL<br>*/
    public String getWb03RisRistorniCapNull() {
        return readString(Pos.WB03_RIS_RISTORNI_CAP_NULL, Len.WB03_RIS_RISTORNI_CAP_NULL);
    }

    public String getWb03RisRistorniCapNullFormatted() {
        return Functions.padBlanks(getWb03RisRistorniCapNull(), Len.WB03_RIS_RISTORNI_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_RISTORNI_CAP = 1;
        public static final int WB03_RIS_RISTORNI_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_RISTORNI_CAP = 8;
        public static final int WB03_RIS_RISTORNI_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_RISTORNI_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_RISTORNI_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
