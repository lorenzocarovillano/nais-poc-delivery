package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-INTR-FRAZ<br>
 * Variable: WDTR-INTR-FRAZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_INTR_FRAZ;
    }

    public void setWdtrIntrFraz(AfDecimal wdtrIntrFraz) {
        writeDecimalAsPacked(Pos.WDTR_INTR_FRAZ, wdtrIntrFraz.copy());
    }

    /**Original name: WDTR-INTR-FRAZ<br>*/
    public AfDecimal getWdtrIntrFraz() {
        return readPackedAsDecimal(Pos.WDTR_INTR_FRAZ, Len.Int.WDTR_INTR_FRAZ, Len.Fract.WDTR_INTR_FRAZ);
    }

    public void setWdtrIntrFrazNull(String wdtrIntrFrazNull) {
        writeString(Pos.WDTR_INTR_FRAZ_NULL, wdtrIntrFrazNull, Len.WDTR_INTR_FRAZ_NULL);
    }

    /**Original name: WDTR-INTR-FRAZ-NULL<br>*/
    public String getWdtrIntrFrazNull() {
        return readString(Pos.WDTR_INTR_FRAZ_NULL, Len.WDTR_INTR_FRAZ_NULL);
    }

    public String getWdtrIntrFrazNullFormatted() {
        return Functions.padBlanks(getWdtrIntrFrazNull(), Len.WDTR_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_FRAZ = 1;
        public static final int WDTR_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_FRAZ = 8;
        public static final int WDTR_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
