package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-PC-TFR<br>
 * Variable: WADE-PC-TFR from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadePcTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadePcTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_PC_TFR;
    }

    public void setWadePcTfr(AfDecimal wadePcTfr) {
        writeDecimalAsPacked(Pos.WADE_PC_TFR, wadePcTfr.copy());
    }

    public void setWadePcTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_PC_TFR, Pos.WADE_PC_TFR);
    }

    /**Original name: WADE-PC-TFR<br>*/
    public AfDecimal getWadePcTfr() {
        return readPackedAsDecimal(Pos.WADE_PC_TFR, Len.Int.WADE_PC_TFR, Len.Fract.WADE_PC_TFR);
    }

    public byte[] getWadePcTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_PC_TFR, Pos.WADE_PC_TFR);
        return buffer;
    }

    public void initWadePcTfrSpaces() {
        fill(Pos.WADE_PC_TFR, Len.WADE_PC_TFR, Types.SPACE_CHAR);
    }

    public void setWadePcTfrNull(String wadePcTfrNull) {
        writeString(Pos.WADE_PC_TFR_NULL, wadePcTfrNull, Len.WADE_PC_TFR_NULL);
    }

    /**Original name: WADE-PC-TFR-NULL<br>*/
    public String getWadePcTfrNull() {
        return readString(Pos.WADE_PC_TFR_NULL, Len.WADE_PC_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_PC_TFR = 1;
        public static final int WADE_PC_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_PC_TFR = 4;
        public static final int WADE_PC_TFR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_PC_TFR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
