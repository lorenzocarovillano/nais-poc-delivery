package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-CPT-FIN<br>
 * Variable: P67-CPT-FIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67CptFin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67CptFin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_CPT_FIN;
    }

    public void setP67CptFin(AfDecimal p67CptFin) {
        writeDecimalAsPacked(Pos.P67_CPT_FIN, p67CptFin.copy());
    }

    public void setP67CptFinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_CPT_FIN, Pos.P67_CPT_FIN);
    }

    /**Original name: P67-CPT-FIN<br>*/
    public AfDecimal getP67CptFin() {
        return readPackedAsDecimal(Pos.P67_CPT_FIN, Len.Int.P67_CPT_FIN, Len.Fract.P67_CPT_FIN);
    }

    public byte[] getP67CptFinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_CPT_FIN, Pos.P67_CPT_FIN);
        return buffer;
    }

    public void setP67CptFinNull(String p67CptFinNull) {
        writeString(Pos.P67_CPT_FIN_NULL, p67CptFinNull, Len.P67_CPT_FIN_NULL);
    }

    /**Original name: P67-CPT-FIN-NULL<br>*/
    public String getP67CptFinNull() {
        return readString(Pos.P67_CPT_FIN_NULL, Len.P67_CPT_FIN_NULL);
    }

    public String getP67CptFinNullFormatted() {
        return Functions.padBlanks(getP67CptFinNull(), Len.P67_CPT_FIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_CPT_FIN = 1;
        public static final int P67_CPT_FIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_CPT_FIN = 8;
        public static final int P67_CPT_FIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_CPT_FIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_CPT_FIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
