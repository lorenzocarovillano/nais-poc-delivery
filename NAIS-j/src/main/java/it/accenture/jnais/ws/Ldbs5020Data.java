package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idbvq053;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS5020<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs5020Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-Q05-DFLT-VISUAL
    private short indQ05DfltVisual = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDBVQ053
    private Idbvq053 idbvq053 = new Idbvq053();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndQ05DfltVisual(short indQ05DfltVisual) {
        this.indQ05DfltVisual = indQ05DfltVisual;
    }

    public short getIndQ05DfltVisual() {
        return this.indQ05DfltVisual;
    }

    public Idbvq053 getIdbvq053() {
        return idbvq053;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
