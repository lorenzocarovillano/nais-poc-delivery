package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV8888-STR-PERFORMANCE-DBG<br>
 * Variable: IDSV8888-STR-PERFORMANCE-DBG from copybook IDSV8888<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class Idsv8888StrPerformanceDbg extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final String ARCH_BATCH = "ARCH";
    public static final String COM_COB_JAV = "JAVA";
    public static final String BUSINESS = "BUSI";
    public static final String SONDA_S4 = "S4";
    public static final String SONDA_S5 = "S5";
    public static final String SONDA_S8 = "S8";
    public static final String SONDA_S9 = "S9";
    public static final String INIZIO = "INIZIO";
    public static final String FINE = "FINE";
    public static final char ON_LINE = 'O';
    public static final char BATCH = 'B';
    public static final char BATCH_INFR = 'I';

    //==== CONSTRUCTORS ====
    public Idsv8888StrPerformanceDbg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.STR_PERFORMANCE_DBG;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "TUNING", Len.FLR1);
        position += Len.FLR1;
        writeChar(position, '/');
        position += (Types.CHAR_SIZE + Len.FASE);
        writeChar(position, '/');
        position += (Types.CHAR_SIZE + Len.NOME_PGM);
        writeChar(position, '/');
        position += (Types.CHAR_SIZE + Len.DESC_PGM);
        writeChar(position, '/');
        position += (Types.CHAR_SIZE + Len.TIMESTAMP_FLD);
        writeChar(position, '/');
        position += (Types.CHAR_SIZE + Len.STATO);
        writeChar(position, '/');
        position += (Types.CHAR_SIZE + Types.CHAR_SIZE);
        writeChar(position, '/');
    }

    public void setFase(String fase) {
        writeString(Pos.FASE, fase, Len.FASE);
    }

    /**Original name: IDSV8888-FASE<br>*/
    public String getFase() {
        return readString(Pos.FASE, Len.FASE);
    }

    public void setArchBatch() {
        setFase(ARCH_BATCH);
    }

    public void setComCobJav() {
        setFase(COM_COB_JAV);
    }

    public void setBusiness() {
        setFase(BUSINESS);
    }

    public void setIdsv8888SondaS8() {
        setFase(SONDA_S8);
    }

    public void setSondaS9() {
        setFase(SONDA_S9);
    }

    public void setNomePgm(String nomePgm) {
        writeString(Pos.NOME_PGM, nomePgm, Len.NOME_PGM);
    }

    /**Original name: IDSV8888-NOME-PGM<br>*/
    public String getNomePgm() {
        return readString(Pos.NOME_PGM, Len.NOME_PGM);
    }

    public String getIdsv8888NomePgmFormatted() {
        return Functions.padBlanks(getNomePgm(), Len.NOME_PGM);
    }

    public void setDescPgm(String descPgm) {
        writeString(Pos.DESC_PGM, descPgm, Len.DESC_PGM);
    }

    /**Original name: IDSV8888-DESC-PGM<br>*/
    public String getDescPgm() {
        return readString(Pos.DESC_PGM, Len.DESC_PGM);
    }

    public void setTimestampFld(String timestampFld) {
        writeString(Pos.TIMESTAMP_FLD, timestampFld, Len.TIMESTAMP_FLD);
    }

    /**Original name: IDSV8888-TIMESTAMP<br>*/
    public String getTimestampFld() {
        return readString(Pos.TIMESTAMP_FLD, Len.TIMESTAMP_FLD);
    }

    public String getTimestampFldFormatted() {
        return Functions.padBlanks(getTimestampFld(), Len.TIMESTAMP_FLD);
    }

    public void setStato(String stato) {
        writeString(Pos.STATO, stato, Len.STATO);
    }

    /**Original name: IDSV8888-STATO<br>*/
    public String getStato() {
        return readString(Pos.STATO, Len.STATO);
    }

    public void setInizio() {
        setStato(INIZIO);
    }

    public void setFine() {
        setStato(FINE);
    }

    public void setModalitaEsecutiva(char modalitaEsecutiva) {
        writeChar(Pos.MODALITA_ESECUTIVA, modalitaEsecutiva);
    }

    /**Original name: IDSV8888-MODALITA-ESECUTIVA<br>*/
    public char getModalitaEsecutiva() {
        return readChar(Pos.MODALITA_ESECUTIVA);
    }

    public void setUserName(String userName) {
        writeString(Pos.USER_NAME, userName, Len.USER_NAME);
    }

    /**Original name: IDSV8888-USER-NAME<br>*/
    public String getUserName() {
        return readString(Pos.USER_NAME, Len.USER_NAME);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int STR_PERFORMANCE_DBG = 1;
        public static final int FLR1 = STR_PERFORMANCE_DBG;
        public static final int FLR2 = FLR1 + Len.FLR1;
        public static final int FASE = FLR2 + Len.FLR2;
        public static final int FLR3 = FASE + Len.FASE;
        public static final int NOME_PGM = FLR3 + Len.FLR2;
        public static final int FLR4 = NOME_PGM + Len.NOME_PGM;
        public static final int DESC_PGM = FLR4 + Len.FLR2;
        public static final int FLR5 = DESC_PGM + Len.DESC_PGM;
        public static final int TIMESTAMP_FLD = FLR5 + Len.FLR2;
        public static final int FLR6 = TIMESTAMP_FLD + Len.TIMESTAMP_FLD;
        public static final int STATO = FLR6 + Len.FLR2;
        public static final int FLR7 = STATO + Len.STATO;
        public static final int MODALITA_ESECUTIVA = FLR7 + Len.FLR2;
        public static final int FLR8 = MODALITA_ESECUTIVA + Len.MODALITA_ESECUTIVA;
        public static final int USER_NAME = FLR8 + Len.FLR2;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 6;
        public static final int FLR2 = 1;
        public static final int FASE = 4;
        public static final int NOME_PGM = 30;
        public static final int DESC_PGM = 23;
        public static final int TIMESTAMP_FLD = 26;
        public static final int STATO = 6;
        public static final int MODALITA_ESECUTIVA = 1;
        public static final int USER_NAME = 20;
        public static final int STR_PERFORMANCE_DBG = FASE + NOME_PGM + DESC_PGM + TIMESTAMP_FLD + STATO + MODALITA_ESECUTIVA + USER_NAME + FLR1 + 7 * FLR2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
