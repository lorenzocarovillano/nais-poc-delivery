package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-CAR-INC<br>
 * Variable: TDR-TOT-CAR-INC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_CAR_INC;
    }

    public void setTdrTotCarInc(AfDecimal tdrTotCarInc) {
        writeDecimalAsPacked(Pos.TDR_TOT_CAR_INC, tdrTotCarInc.copy());
    }

    public void setTdrTotCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_CAR_INC, Pos.TDR_TOT_CAR_INC);
    }

    /**Original name: TDR-TOT-CAR-INC<br>*/
    public AfDecimal getTdrTotCarInc() {
        return readPackedAsDecimal(Pos.TDR_TOT_CAR_INC, Len.Int.TDR_TOT_CAR_INC, Len.Fract.TDR_TOT_CAR_INC);
    }

    public byte[] getTdrTotCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_CAR_INC, Pos.TDR_TOT_CAR_INC);
        return buffer;
    }

    public void setTdrTotCarIncNull(String tdrTotCarIncNull) {
        writeString(Pos.TDR_TOT_CAR_INC_NULL, tdrTotCarIncNull, Len.TDR_TOT_CAR_INC_NULL);
    }

    /**Original name: TDR-TOT-CAR-INC-NULL<br>*/
    public String getTdrTotCarIncNull() {
        return readString(Pos.TDR_TOT_CAR_INC_NULL, Len.TDR_TOT_CAR_INC_NULL);
    }

    public String getTdrTotCarIncNullFormatted() {
        return Functions.padBlanks(getTdrTotCarIncNull(), Len.TDR_TOT_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_INC = 1;
        public static final int TDR_TOT_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_CAR_INC = 8;
        public static final int TDR_TOT_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
