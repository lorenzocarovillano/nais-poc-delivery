package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-REMUN-ASS<br>
 * Variable: WTIT-TOT-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_REMUN_ASS;
    }

    public void setWtitTotRemunAss(AfDecimal wtitTotRemunAss) {
        writeDecimalAsPacked(Pos.WTIT_TOT_REMUN_ASS, wtitTotRemunAss.copy());
    }

    public void setWtitTotRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_REMUN_ASS, Pos.WTIT_TOT_REMUN_ASS);
    }

    /**Original name: WTIT-TOT-REMUN-ASS<br>*/
    public AfDecimal getWtitTotRemunAss() {
        return readPackedAsDecimal(Pos.WTIT_TOT_REMUN_ASS, Len.Int.WTIT_TOT_REMUN_ASS, Len.Fract.WTIT_TOT_REMUN_ASS);
    }

    public byte[] getWtitTotRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_REMUN_ASS, Pos.WTIT_TOT_REMUN_ASS);
        return buffer;
    }

    public void initWtitTotRemunAssSpaces() {
        fill(Pos.WTIT_TOT_REMUN_ASS, Len.WTIT_TOT_REMUN_ASS, Types.SPACE_CHAR);
    }

    public void setWtitTotRemunAssNull(String wtitTotRemunAssNull) {
        writeString(Pos.WTIT_TOT_REMUN_ASS_NULL, wtitTotRemunAssNull, Len.WTIT_TOT_REMUN_ASS_NULL);
    }

    /**Original name: WTIT-TOT-REMUN-ASS-NULL<br>*/
    public String getWtitTotRemunAssNull() {
        return readString(Pos.WTIT_TOT_REMUN_ASS_NULL, Len.WTIT_TOT_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_REMUN_ASS = 1;
        public static final int WTIT_TOT_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_REMUN_ASS = 8;
        public static final int WTIT_TOT_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
