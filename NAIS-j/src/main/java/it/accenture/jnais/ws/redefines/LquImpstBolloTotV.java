package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-BOLLO-TOT-V<br>
 * Variable: LQU-IMPST-BOLLO-TOT-V from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstBolloTotV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstBolloTotV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_BOLLO_TOT_V;
    }

    public void setLquImpstBolloTotV(AfDecimal lquImpstBolloTotV) {
        writeDecimalAsPacked(Pos.LQU_IMPST_BOLLO_TOT_V, lquImpstBolloTotV.copy());
    }

    public void setLquImpstBolloTotVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_BOLLO_TOT_V, Pos.LQU_IMPST_BOLLO_TOT_V);
    }

    /**Original name: LQU-IMPST-BOLLO-TOT-V<br>*/
    public AfDecimal getLquImpstBolloTotV() {
        return readPackedAsDecimal(Pos.LQU_IMPST_BOLLO_TOT_V, Len.Int.LQU_IMPST_BOLLO_TOT_V, Len.Fract.LQU_IMPST_BOLLO_TOT_V);
    }

    public byte[] getLquImpstBolloTotVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_BOLLO_TOT_V, Pos.LQU_IMPST_BOLLO_TOT_V);
        return buffer;
    }

    public void setLquImpstBolloTotVNull(String lquImpstBolloTotVNull) {
        writeString(Pos.LQU_IMPST_BOLLO_TOT_V_NULL, lquImpstBolloTotVNull, Len.LQU_IMPST_BOLLO_TOT_V_NULL);
    }

    /**Original name: LQU-IMPST-BOLLO-TOT-V-NULL<br>*/
    public String getLquImpstBolloTotVNull() {
        return readString(Pos.LQU_IMPST_BOLLO_TOT_V_NULL, Len.LQU_IMPST_BOLLO_TOT_V_NULL);
    }

    public String getLquImpstBolloTotVNullFormatted() {
        return Functions.padBlanks(getLquImpstBolloTotVNull(), Len.LQU_IMPST_BOLLO_TOT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_TOT_V = 1;
        public static final int LQU_IMPST_BOLLO_TOT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_BOLLO_TOT_V = 8;
        public static final int LQU_IMPST_BOLLO_TOT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_TOT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_BOLLO_TOT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
