package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LDBV2911<br>
 * Variable: LDBV2911 from copybook LDBV2911<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv2911 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV2911-ID-POLI
    private int idPoli = DefaultValues.INT_VAL;
    //Original name: LDBV2911-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV2911-PRSTZ-INI-NEWFIS
    private AfDecimal prstzIniNewfis = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV2911;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv2911Bytes(buf);
    }

    public String getLdbv2911Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv2911Bytes());
    }

    public void setLdbv2911Bytes(byte[] buffer) {
        setLdbv2911Bytes(buffer, 1);
    }

    public byte[] getLdbv2911Bytes() {
        byte[] buffer = new byte[Len.LDBV2911];
        return getLdbv2911Bytes(buffer, 1);
    }

    public void setLdbv2911Bytes(byte[] buffer, int offset) {
        int position = offset;
        idPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        prstzIniNewfis.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRSTZ_INI_NEWFIS, Len.Fract.PRSTZ_INI_NEWFIS));
    }

    public byte[] getLdbv2911Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idPoli, Len.Int.ID_POLI, 0);
        position += Len.ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeDecimalAsPacked(buffer, position, prstzIniNewfis.copy());
        return buffer;
    }

    public void setIdPoli(int idPoli) {
        this.idPoli = idPoli;
    }

    public int getIdPoli() {
        return this.idPoli;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setPrstzIniNewfis(AfDecimal prstzIniNewfis) {
        this.prstzIniNewfis.assign(prstzIniNewfis);
    }

    public AfDecimal getPrstzIniNewfis() {
        return this.prstzIniNewfis.copy();
    }

    @Override
    public byte[] serialize() {
        return getLdbv2911Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_POLI = 5;
        public static final int ID_ADES = 5;
        public static final int PRSTZ_INI_NEWFIS = 8;
        public static final int LDBV2911 = ID_POLI + ID_ADES + PRSTZ_INI_NEWFIS;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_POLI = 9;
            public static final int ID_ADES = 9;
            public static final int PRSTZ_INI_NEWFIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRSTZ_INI_NEWFIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
