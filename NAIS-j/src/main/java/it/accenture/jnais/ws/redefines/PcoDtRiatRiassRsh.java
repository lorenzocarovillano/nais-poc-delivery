package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-RIAT-RIASS-RSH<br>
 * Variable: PCO-DT-RIAT-RIASS-RSH from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtRiatRiassRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtRiatRiassRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_RIAT_RIASS_RSH;
    }

    public void setPcoDtRiatRiassRsh(int pcoDtRiatRiassRsh) {
        writeIntAsPacked(Pos.PCO_DT_RIAT_RIASS_RSH, pcoDtRiatRiassRsh, Len.Int.PCO_DT_RIAT_RIASS_RSH);
    }

    public void setPcoDtRiatRiassRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_RIAT_RIASS_RSH, Pos.PCO_DT_RIAT_RIASS_RSH);
    }

    /**Original name: PCO-DT-RIAT-RIASS-RSH<br>*/
    public int getPcoDtRiatRiassRsh() {
        return readPackedAsInt(Pos.PCO_DT_RIAT_RIASS_RSH, Len.Int.PCO_DT_RIAT_RIASS_RSH);
    }

    public byte[] getPcoDtRiatRiassRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_RIAT_RIASS_RSH, Pos.PCO_DT_RIAT_RIASS_RSH);
        return buffer;
    }

    public void initPcoDtRiatRiassRshHighValues() {
        fill(Pos.PCO_DT_RIAT_RIASS_RSH, Len.PCO_DT_RIAT_RIASS_RSH, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtRiatRiassRshNull(String pcoDtRiatRiassRshNull) {
        writeString(Pos.PCO_DT_RIAT_RIASS_RSH_NULL, pcoDtRiatRiassRshNull, Len.PCO_DT_RIAT_RIASS_RSH_NULL);
    }

    /**Original name: PCO-DT-RIAT-RIASS-RSH-NULL<br>*/
    public String getPcoDtRiatRiassRshNull() {
        return readString(Pos.PCO_DT_RIAT_RIASS_RSH_NULL, Len.PCO_DT_RIAT_RIASS_RSH_NULL);
    }

    public String getPcoDtRiatRiassRshNullFormatted() {
        return Functions.padBlanks(getPcoDtRiatRiassRshNull(), Len.PCO_DT_RIAT_RIASS_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_RIAT_RIASS_RSH = 1;
        public static final int PCO_DT_RIAT_RIASS_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_RIAT_RIASS_RSH = 5;
        public static final int PCO_DT_RIAT_RIASS_RSH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_RIAT_RIASS_RSH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
