package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WOCO-DT-SCAD<br>
 * Variable: WOCO-DT-SCAD from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_DT_SCAD;
    }

    public void setWocoDtScad(int wocoDtScad) {
        writeIntAsPacked(Pos.WOCO_DT_SCAD, wocoDtScad, Len.Int.WOCO_DT_SCAD);
    }

    public void setWocoDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_DT_SCAD, Pos.WOCO_DT_SCAD);
    }

    /**Original name: WOCO-DT-SCAD<br>*/
    public int getWocoDtScad() {
        return readPackedAsInt(Pos.WOCO_DT_SCAD, Len.Int.WOCO_DT_SCAD);
    }

    public byte[] getWocoDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_DT_SCAD, Pos.WOCO_DT_SCAD);
        return buffer;
    }

    public void initWocoDtScadSpaces() {
        fill(Pos.WOCO_DT_SCAD, Len.WOCO_DT_SCAD, Types.SPACE_CHAR);
    }

    public void setWocoDtScadNull(String wocoDtScadNull) {
        writeString(Pos.WOCO_DT_SCAD_NULL, wocoDtScadNull, Len.WOCO_DT_SCAD_NULL);
    }

    /**Original name: WOCO-DT-SCAD-NULL<br>*/
    public String getWocoDtScadNull() {
        return readString(Pos.WOCO_DT_SCAD_NULL, Len.WOCO_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_DT_SCAD = 1;
        public static final int WOCO_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_DT_SCAD = 5;
        public static final int WOCO_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
