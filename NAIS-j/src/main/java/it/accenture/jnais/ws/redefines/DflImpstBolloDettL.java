package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-BOLLO-DETT-L<br>
 * Variable: DFL-IMPST-BOLLO-DETT-L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstBolloDettL extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstBolloDettL() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_BOLLO_DETT_L;
    }

    public void setDflImpstBolloDettL(AfDecimal dflImpstBolloDettL) {
        writeDecimalAsPacked(Pos.DFL_IMPST_BOLLO_DETT_L, dflImpstBolloDettL.copy());
    }

    public void setDflImpstBolloDettLFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_BOLLO_DETT_L, Pos.DFL_IMPST_BOLLO_DETT_L);
    }

    /**Original name: DFL-IMPST-BOLLO-DETT-L<br>*/
    public AfDecimal getDflImpstBolloDettL() {
        return readPackedAsDecimal(Pos.DFL_IMPST_BOLLO_DETT_L, Len.Int.DFL_IMPST_BOLLO_DETT_L, Len.Fract.DFL_IMPST_BOLLO_DETT_L);
    }

    public byte[] getDflImpstBolloDettLAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_BOLLO_DETT_L, Pos.DFL_IMPST_BOLLO_DETT_L);
        return buffer;
    }

    public void setDflImpstBolloDettLNull(String dflImpstBolloDettLNull) {
        writeString(Pos.DFL_IMPST_BOLLO_DETT_L_NULL, dflImpstBolloDettLNull, Len.DFL_IMPST_BOLLO_DETT_L_NULL);
    }

    /**Original name: DFL-IMPST-BOLLO-DETT-L-NULL<br>*/
    public String getDflImpstBolloDettLNull() {
        return readString(Pos.DFL_IMPST_BOLLO_DETT_L_NULL, Len.DFL_IMPST_BOLLO_DETT_L_NULL);
    }

    public String getDflImpstBolloDettLNullFormatted() {
        return Functions.padBlanks(getDflImpstBolloDettLNull(), Len.DFL_IMPST_BOLLO_DETT_L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_DETT_L = 1;
        public static final int DFL_IMPST_BOLLO_DETT_L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_DETT_L = 8;
        public static final int DFL_IMPST_BOLLO_DETT_L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_DETT_L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_DETT_L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
