package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-INI-COP<br>
 * Variable: WTIT-DT-INI-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_INI_COP;
    }

    public void setWtitDtIniCop(int wtitDtIniCop) {
        writeIntAsPacked(Pos.WTIT_DT_INI_COP, wtitDtIniCop, Len.Int.WTIT_DT_INI_COP);
    }

    public void setWtitDtIniCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_INI_COP, Pos.WTIT_DT_INI_COP);
    }

    /**Original name: WTIT-DT-INI-COP<br>*/
    public int getWtitDtIniCop() {
        return readPackedAsInt(Pos.WTIT_DT_INI_COP, Len.Int.WTIT_DT_INI_COP);
    }

    public byte[] getWtitDtIniCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_INI_COP, Pos.WTIT_DT_INI_COP);
        return buffer;
    }

    public void initWtitDtIniCopSpaces() {
        fill(Pos.WTIT_DT_INI_COP, Len.WTIT_DT_INI_COP, Types.SPACE_CHAR);
    }

    public void setWtitDtIniCopNull(String wtitDtIniCopNull) {
        writeString(Pos.WTIT_DT_INI_COP_NULL, wtitDtIniCopNull, Len.WTIT_DT_INI_COP_NULL);
    }

    /**Original name: WTIT-DT-INI-COP-NULL<br>*/
    public String getWtitDtIniCopNull() {
        return readString(Pos.WTIT_DT_INI_COP_NULL, Len.WTIT_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_INI_COP = 1;
        public static final int WTIT_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_INI_COP = 5;
        public static final int WTIT_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
