package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Lccv0021AreaOutput;

/**Original name: LCCV0021<br>
 * Variable: LCCV0021 from copybook LCCV0021<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Lccv0021 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LCCV0021-PGM
    private String pgm = DefaultValues.stringVal(Len.PGM);
    //Original name: LCCV0021-TP-MOV-PTF
    private String tpMovPtf = DefaultValues.stringVal(Len.TP_MOV_PTF);
    //Original name: LCCV0021-AREA-OUTPUT
    private Lccv0021AreaOutput areaOutput = new Lccv0021AreaOutput();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCV0021;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLccv0021Bytes(buf);
    }

    public String getLccv0021Formatted() {
        return MarshalByteExt.bufferToStr(getLccv0021Bytes());
    }

    public void setLccv0021Bytes(byte[] buffer) {
        setLccv0021Bytes(buffer, 1);
    }

    public byte[] getLccv0021Bytes() {
        byte[] buffer = new byte[Len.LCCV0021];
        return getLccv0021Bytes(buffer, 1);
    }

    public void setLccv0021Bytes(byte[] buffer, int offset) {
        int position = offset;
        pgm = MarshalByte.readString(buffer, position, Len.PGM);
        position += Len.PGM;
        setAreaInputBytes(buffer, position);
        position += Len.AREA_INPUT;
        areaOutput.setAreaOutputBytes(buffer, position);
    }

    public byte[] getLccv0021Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, pgm, Len.PGM);
        position += Len.PGM;
        getAreaInputBytes(buffer, position);
        position += Len.AREA_INPUT;
        areaOutput.getAreaOutputBytes(buffer, position);
        return buffer;
    }

    public void setPgm(String pgm) {
        this.pgm = Functions.subString(pgm, Len.PGM);
    }

    public String getPgm() {
        return this.pgm;
    }

    public void setAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        tpMovPtf = MarshalByte.readFixedString(buffer, position, Len.TP_MOV_PTF);
    }

    public byte[] getAreaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpMovPtf, Len.TP_MOV_PTF);
        return buffer;
    }

    public void setTpMovPtfFormatted(String tpMovPtf) {
        this.tpMovPtf = Trunc.toUnsignedNumeric(tpMovPtf, Len.TP_MOV_PTF);
    }

    public int getTpMovPtf() {
        return NumericDisplay.asInt(this.tpMovPtf);
    }

    public String getTpMovPtfFormatted() {
        return this.tpMovPtf;
    }

    public Lccv0021AreaOutput getAreaOutput() {
        return areaOutput;
    }

    @Override
    public byte[] serialize() {
        return getLccv0021Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PGM = 8;
        public static final int TP_MOV_PTF = 5;
        public static final int AREA_INPUT = TP_MOV_PTF;
        public static final int LCCV0021 = PGM + AREA_INPUT + Lccv0021AreaOutput.Len.AREA_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
