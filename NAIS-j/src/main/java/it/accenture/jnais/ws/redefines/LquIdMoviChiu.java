package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-ID-MOVI-CHIU<br>
 * Variable: LQU-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_ID_MOVI_CHIU;
    }

    public void setLquIdMoviChiu(int lquIdMoviChiu) {
        writeIntAsPacked(Pos.LQU_ID_MOVI_CHIU, lquIdMoviChiu, Len.Int.LQU_ID_MOVI_CHIU);
    }

    public void setLquIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_ID_MOVI_CHIU, Pos.LQU_ID_MOVI_CHIU);
    }

    /**Original name: LQU-ID-MOVI-CHIU<br>*/
    public int getLquIdMoviChiu() {
        return readPackedAsInt(Pos.LQU_ID_MOVI_CHIU, Len.Int.LQU_ID_MOVI_CHIU);
    }

    public byte[] getLquIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_ID_MOVI_CHIU, Pos.LQU_ID_MOVI_CHIU);
        return buffer;
    }

    public void setLquIdMoviChiuNull(String lquIdMoviChiuNull) {
        writeString(Pos.LQU_ID_MOVI_CHIU_NULL, lquIdMoviChiuNull, Len.LQU_ID_MOVI_CHIU_NULL);
    }

    /**Original name: LQU-ID-MOVI-CHIU-NULL<br>*/
    public String getLquIdMoviChiuNull() {
        return readString(Pos.LQU_ID_MOVI_CHIU_NULL, Len.LQU_ID_MOVI_CHIU_NULL);
    }

    public String getLquIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getLquIdMoviChiuNull(), Len.LQU_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_ID_MOVI_CHIU = 1;
        public static final int LQU_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_ID_MOVI_CHIU = 5;
        public static final int LQU_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
