package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPST-VIS-662014<br>
 * Variable: TCL-IMPST-VIS-662014 from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpstVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpstVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPST_VIS662014;
    }

    public void setTclImpstVis662014(AfDecimal tclImpstVis662014) {
        writeDecimalAsPacked(Pos.TCL_IMPST_VIS662014, tclImpstVis662014.copy());
    }

    public void setTclImpstVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPST_VIS662014, Pos.TCL_IMPST_VIS662014);
    }

    /**Original name: TCL-IMPST-VIS-662014<br>*/
    public AfDecimal getTclImpstVis662014() {
        return readPackedAsDecimal(Pos.TCL_IMPST_VIS662014, Len.Int.TCL_IMPST_VIS662014, Len.Fract.TCL_IMPST_VIS662014);
    }

    public byte[] getTclImpstVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPST_VIS662014, Pos.TCL_IMPST_VIS662014);
        return buffer;
    }

    public void setTclImpstVis662014Null(String tclImpstVis662014Null) {
        writeString(Pos.TCL_IMPST_VIS662014_NULL, tclImpstVis662014Null, Len.TCL_IMPST_VIS662014_NULL);
    }

    /**Original name: TCL-IMPST-VIS-662014-NULL<br>*/
    public String getTclImpstVis662014Null() {
        return readString(Pos.TCL_IMPST_VIS662014_NULL, Len.TCL_IMPST_VIS662014_NULL);
    }

    public String getTclImpstVis662014NullFormatted() {
        return Functions.padBlanks(getTclImpstVis662014Null(), Len.TCL_IMPST_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPST_VIS662014 = 1;
        public static final int TCL_IMPST_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPST_VIS662014 = 8;
        public static final int TCL_IMPST_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPST_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPST_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
