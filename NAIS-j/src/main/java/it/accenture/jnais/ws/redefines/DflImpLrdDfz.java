package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMP-LRD-DFZ<br>
 * Variable: DFL-IMP-LRD-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpLrdDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpLrdDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMP_LRD_DFZ;
    }

    public void setDflImpLrdDfz(AfDecimal dflImpLrdDfz) {
        writeDecimalAsPacked(Pos.DFL_IMP_LRD_DFZ, dflImpLrdDfz.copy());
    }

    public void setDflImpLrdDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMP_LRD_DFZ, Pos.DFL_IMP_LRD_DFZ);
    }

    /**Original name: DFL-IMP-LRD-DFZ<br>*/
    public AfDecimal getDflImpLrdDfz() {
        return readPackedAsDecimal(Pos.DFL_IMP_LRD_DFZ, Len.Int.DFL_IMP_LRD_DFZ, Len.Fract.DFL_IMP_LRD_DFZ);
    }

    public byte[] getDflImpLrdDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMP_LRD_DFZ, Pos.DFL_IMP_LRD_DFZ);
        return buffer;
    }

    public void setDflImpLrdDfzNull(String dflImpLrdDfzNull) {
        writeString(Pos.DFL_IMP_LRD_DFZ_NULL, dflImpLrdDfzNull, Len.DFL_IMP_LRD_DFZ_NULL);
    }

    /**Original name: DFL-IMP-LRD-DFZ-NULL<br>*/
    public String getDflImpLrdDfzNull() {
        return readString(Pos.DFL_IMP_LRD_DFZ_NULL, Len.DFL_IMP_LRD_DFZ_NULL);
    }

    public String getDflImpLrdDfzNullFormatted() {
        return Functions.padBlanks(getDflImpLrdDfzNull(), Len.DFL_IMP_LRD_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMP_LRD_DFZ = 1;
        public static final int DFL_IMP_LRD_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMP_LRD_DFZ = 8;
        public static final int DFL_IMP_LRD_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMP_LRD_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMP_LRD_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
