package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CAUSALE<br>
 * Variable: FLAG-CAUSALE from program LDBS1350<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCausale {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char NOT_CAUSALE = 'N';
    public static final char CAUSALE = 'S';

    //==== METHODS ====
    public void setFlagCausale(char flagCausale) {
        this.value = flagCausale;
    }

    public char getFlagCausale() {
        return this.value;
    }

    public boolean isNotCausale() {
        return value == NOT_CAUSALE;
    }

    public void setNotCausale() {
        value = NOT_CAUSALE;
    }

    public boolean isCausale() {
        return value == CAUSALE;
    }

    public void setCausale() {
        value = CAUSALE;
    }
}
