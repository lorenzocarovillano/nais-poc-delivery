package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: LDBV1351-LIVELLO-OPERAZIONI<br>
 * Variable: LDBV1351-LIVELLO-OPERAZIONI from copybook LDBV1351<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ldbv1351LivelloOperazioni {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char PRODOTTO = 'P';
    public static final char GARANZIE = 'G';
    public static final char OPZIONE = 'O';

    //==== METHODS ====
    public void setLdbv1351LivelloOperazioni(char ldbv1351LivelloOperazioni) {
        this.value = ldbv1351LivelloOperazioni;
    }

    public char getLdbv1351LivelloOperazioni() {
        return this.value;
    }

    public boolean isLdbv1361Prodotto() {
        return value == PRODOTTO;
    }

    public void setLdbv1361Prodotto() {
        value = PRODOTTO;
    }

    public void setLdbv1361Garanzie() {
        value = GARANZIE;
    }

    public boolean isLdbv1361Opzione() {
        return value == OPZIONE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV1351_LIVELLO_OPERAZIONI = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
