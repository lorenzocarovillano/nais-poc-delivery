package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMP-AZ<br>
 * Variable: TGA-IMP-AZ from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMP_AZ;
    }

    public void setTgaImpAz(AfDecimal tgaImpAz) {
        writeDecimalAsPacked(Pos.TGA_IMP_AZ, tgaImpAz.copy());
    }

    public void setTgaImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMP_AZ, Pos.TGA_IMP_AZ);
    }

    /**Original name: TGA-IMP-AZ<br>*/
    public AfDecimal getTgaImpAz() {
        return readPackedAsDecimal(Pos.TGA_IMP_AZ, Len.Int.TGA_IMP_AZ, Len.Fract.TGA_IMP_AZ);
    }

    public byte[] getTgaImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMP_AZ, Pos.TGA_IMP_AZ);
        return buffer;
    }

    public void setTgaImpAzNull(String tgaImpAzNull) {
        writeString(Pos.TGA_IMP_AZ_NULL, tgaImpAzNull, Len.TGA_IMP_AZ_NULL);
    }

    /**Original name: TGA-IMP-AZ-NULL<br>*/
    public String getTgaImpAzNull() {
        return readString(Pos.TGA_IMP_AZ_NULL, Len.TGA_IMP_AZ_NULL);
    }

    public String getTgaImpAzNullFormatted() {
        return Functions.padBlanks(getTgaImpAzNull(), Len.TGA_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMP_AZ = 1;
        public static final int TGA_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMP_AZ = 8;
        public static final int TGA_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
