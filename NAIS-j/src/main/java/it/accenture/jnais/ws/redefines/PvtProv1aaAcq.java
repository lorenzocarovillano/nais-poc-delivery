package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PVT-PROV-1AA-ACQ<br>
 * Variable: PVT-PROV-1AA-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PvtProv1aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PvtProv1aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PVT_PROV1AA_ACQ;
    }

    public void setPvtProv1aaAcq(AfDecimal pvtProv1aaAcq) {
        writeDecimalAsPacked(Pos.PVT_PROV1AA_ACQ, pvtProv1aaAcq.copy());
    }

    public void setPvtProv1aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PVT_PROV1AA_ACQ, Pos.PVT_PROV1AA_ACQ);
    }

    /**Original name: PVT-PROV-1AA-ACQ<br>*/
    public AfDecimal getPvtProv1aaAcq() {
        return readPackedAsDecimal(Pos.PVT_PROV1AA_ACQ, Len.Int.PVT_PROV1AA_ACQ, Len.Fract.PVT_PROV1AA_ACQ);
    }

    public byte[] getPvtProv1aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PVT_PROV1AA_ACQ, Pos.PVT_PROV1AA_ACQ);
        return buffer;
    }

    public void setPvtProv1aaAcqNull(String pvtProv1aaAcqNull) {
        writeString(Pos.PVT_PROV1AA_ACQ_NULL, pvtProv1aaAcqNull, Len.PVT_PROV1AA_ACQ_NULL);
    }

    /**Original name: PVT-PROV-1AA-ACQ-NULL<br>*/
    public String getPvtProv1aaAcqNull() {
        return readString(Pos.PVT_PROV1AA_ACQ_NULL, Len.PVT_PROV1AA_ACQ_NULL);
    }

    public String getPvtProv1aaAcqNullFormatted() {
        return Functions.padBlanks(getPvtProv1aaAcqNull(), Len.PVT_PROV1AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PVT_PROV1AA_ACQ = 1;
        public static final int PVT_PROV1AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PVT_PROV1AA_ACQ = 8;
        public static final int PVT_PROV1AA_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PVT_PROV1AA_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PVT_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
