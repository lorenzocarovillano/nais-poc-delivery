package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P58-IMPST-BOLLO-TOT-V<br>
 * Variable: P58-IMPST-BOLLO-TOT-V from program IDBSP580<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P58ImpstBolloTotV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P58ImpstBolloTotV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P58_IMPST_BOLLO_TOT_V;
    }

    public void setP58ImpstBolloTotV(AfDecimal p58ImpstBolloTotV) {
        writeDecimalAsPacked(Pos.P58_IMPST_BOLLO_TOT_V, p58ImpstBolloTotV.copy());
    }

    public void setP58ImpstBolloTotVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P58_IMPST_BOLLO_TOT_V, Pos.P58_IMPST_BOLLO_TOT_V);
    }

    /**Original name: P58-IMPST-BOLLO-TOT-V<br>*/
    public AfDecimal getP58ImpstBolloTotV() {
        return readPackedAsDecimal(Pos.P58_IMPST_BOLLO_TOT_V, Len.Int.P58_IMPST_BOLLO_TOT_V, Len.Fract.P58_IMPST_BOLLO_TOT_V);
    }

    public byte[] getP58ImpstBolloTotVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P58_IMPST_BOLLO_TOT_V, Pos.P58_IMPST_BOLLO_TOT_V);
        return buffer;
    }

    public void setP58ImpstBolloTotVNull(String p58ImpstBolloTotVNull) {
        writeString(Pos.P58_IMPST_BOLLO_TOT_V_NULL, p58ImpstBolloTotVNull, Len.P58_IMPST_BOLLO_TOT_V_NULL);
    }

    /**Original name: P58-IMPST-BOLLO-TOT-V-NULL<br>*/
    public String getP58ImpstBolloTotVNull() {
        return readString(Pos.P58_IMPST_BOLLO_TOT_V_NULL, Len.P58_IMPST_BOLLO_TOT_V_NULL);
    }

    public String getP58ImpstBolloTotVNullFormatted() {
        return Functions.padBlanks(getP58ImpstBolloTotVNull(), Len.P58_IMPST_BOLLO_TOT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P58_IMPST_BOLLO_TOT_V = 1;
        public static final int P58_IMPST_BOLLO_TOT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P58_IMPST_BOLLO_TOT_V = 8;
        public static final int P58_IMPST_BOLLO_TOT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P58_IMPST_BOLLO_TOT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P58_IMPST_BOLLO_TOT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
