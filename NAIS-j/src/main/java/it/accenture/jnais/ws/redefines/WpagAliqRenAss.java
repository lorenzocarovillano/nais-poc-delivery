package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-ALIQ-REN-ASS<br>
 * Variable: WPAG-ALIQ-REN-ASS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAliqRenAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAliqRenAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_ALIQ_REN_ASS;
    }

    public void setWpagAliqRenAss(AfDecimal wpagAliqRenAss) {
        writeDecimalAsPacked(Pos.WPAG_ALIQ_REN_ASS, wpagAliqRenAss.copy());
    }

    public void setWpagAliqRenAssFormatted(String wpagAliqRenAss) {
        setWpagAliqRenAss(PicParser.display(new PicParams("S9(5)V9(9)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_ALIQ_REN_ASS + Len.Fract.WPAG_ALIQ_REN_ASS, Len.Fract.WPAG_ALIQ_REN_ASS, wpagAliqRenAss));
    }

    public void setWpagAliqRenAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_ALIQ_REN_ASS, Pos.WPAG_ALIQ_REN_ASS);
    }

    /**Original name: WPAG-ALIQ-REN-ASS<br>*/
    public AfDecimal getWpagAliqRenAss() {
        return readPackedAsDecimal(Pos.WPAG_ALIQ_REN_ASS, Len.Int.WPAG_ALIQ_REN_ASS, Len.Fract.WPAG_ALIQ_REN_ASS);
    }

    public byte[] getWpagAliqRenAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_ALIQ_REN_ASS, Pos.WPAG_ALIQ_REN_ASS);
        return buffer;
    }

    public void initWpagAliqRenAssSpaces() {
        fill(Pos.WPAG_ALIQ_REN_ASS, Len.WPAG_ALIQ_REN_ASS, Types.SPACE_CHAR);
    }

    public void setWpagAliqRenAssNull(String wpagAliqRenAssNull) {
        writeString(Pos.WPAG_ALIQ_REN_ASS_NULL, wpagAliqRenAssNull, Len.WPAG_ALIQ_REN_ASS_NULL);
    }

    /**Original name: WPAG-ALIQ-REN-ASS-NULL<br>*/
    public String getWpagAliqRenAssNull() {
        return readString(Pos.WPAG_ALIQ_REN_ASS_NULL, Len.WPAG_ALIQ_REN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_REN_ASS = 1;
        public static final int WPAG_ALIQ_REN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_REN_ASS = 8;
        public static final int WPAG_ALIQ_REN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_REN_ASS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_REN_ASS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
