package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-STOR<br>
 * Variable: PCO-DT-ULT-BOLL-STOR from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollStor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollStor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_STOR;
    }

    public void setPcoDtUltBollStor(int pcoDtUltBollStor) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_STOR, pcoDtUltBollStor, Len.Int.PCO_DT_ULT_BOLL_STOR);
    }

    public void setPcoDtUltBollStorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_STOR, Pos.PCO_DT_ULT_BOLL_STOR);
    }

    /**Original name: PCO-DT-ULT-BOLL-STOR<br>*/
    public int getPcoDtUltBollStor() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_STOR, Len.Int.PCO_DT_ULT_BOLL_STOR);
    }

    public byte[] getPcoDtUltBollStorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_STOR, Pos.PCO_DT_ULT_BOLL_STOR);
        return buffer;
    }

    public void initPcoDtUltBollStorHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_STOR, Len.PCO_DT_ULT_BOLL_STOR, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollStorNull(String pcoDtUltBollStorNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_STOR_NULL, pcoDtUltBollStorNull, Len.PCO_DT_ULT_BOLL_STOR_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-STOR-NULL<br>*/
    public String getPcoDtUltBollStorNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_STOR_NULL, Len.PCO_DT_ULT_BOLL_STOR_NULL);
    }

    public String getPcoDtUltBollStorNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollStorNull(), Len.PCO_DT_ULT_BOLL_STOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_STOR = 1;
        public static final int PCO_DT_ULT_BOLL_STOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_STOR = 5;
        public static final int PCO_DT_ULT_BOLL_STOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_STOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
