package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-DT-ESI-TIT<br>
 * Variable: DTC-DT-ESI-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcDtEsiTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcDtEsiTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_DT_ESI_TIT;
    }

    public void setDtcDtEsiTit(int dtcDtEsiTit) {
        writeIntAsPacked(Pos.DTC_DT_ESI_TIT, dtcDtEsiTit, Len.Int.DTC_DT_ESI_TIT);
    }

    public void setDtcDtEsiTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_DT_ESI_TIT, Pos.DTC_DT_ESI_TIT);
    }

    /**Original name: DTC-DT-ESI-TIT<br>*/
    public int getDtcDtEsiTit() {
        return readPackedAsInt(Pos.DTC_DT_ESI_TIT, Len.Int.DTC_DT_ESI_TIT);
    }

    public byte[] getDtcDtEsiTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_DT_ESI_TIT, Pos.DTC_DT_ESI_TIT);
        return buffer;
    }

    public void setDtcDtEsiTitNull(String dtcDtEsiTitNull) {
        writeString(Pos.DTC_DT_ESI_TIT_NULL, dtcDtEsiTitNull, Len.DTC_DT_ESI_TIT_NULL);
    }

    /**Original name: DTC-DT-ESI-TIT-NULL<br>*/
    public String getDtcDtEsiTitNull() {
        return readString(Pos.DTC_DT_ESI_TIT_NULL, Len.DTC_DT_ESI_TIT_NULL);
    }

    public String getDtcDtEsiTitNullFormatted() {
        return Functions.padBlanks(getDtcDtEsiTitNull(), Len.DTC_DT_ESI_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_DT_ESI_TIT = 1;
        public static final int DTC_DT_ESI_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_DT_ESI_TIT = 5;
        public static final int DTC_DT_ESI_TIT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_DT_ESI_TIT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
