package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-ISC-K1<br>
 * Variable: DFA-IMP-CNBT-ISC-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtIscK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtIscK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_ISC_K1;
    }

    public void setDfaImpCnbtIscK1(AfDecimal dfaImpCnbtIscK1) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_ISC_K1, dfaImpCnbtIscK1.copy());
    }

    public void setDfaImpCnbtIscK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_ISC_K1, Pos.DFA_IMP_CNBT_ISC_K1);
    }

    /**Original name: DFA-IMP-CNBT-ISC-K1<br>*/
    public AfDecimal getDfaImpCnbtIscK1() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_ISC_K1, Len.Int.DFA_IMP_CNBT_ISC_K1, Len.Fract.DFA_IMP_CNBT_ISC_K1);
    }

    public byte[] getDfaImpCnbtIscK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_ISC_K1, Pos.DFA_IMP_CNBT_ISC_K1);
        return buffer;
    }

    public void setDfaImpCnbtIscK1Null(String dfaImpCnbtIscK1Null) {
        writeString(Pos.DFA_IMP_CNBT_ISC_K1_NULL, dfaImpCnbtIscK1Null, Len.DFA_IMP_CNBT_ISC_K1_NULL);
    }

    /**Original name: DFA-IMP-CNBT-ISC-K1-NULL<br>*/
    public String getDfaImpCnbtIscK1Null() {
        return readString(Pos.DFA_IMP_CNBT_ISC_K1_NULL, Len.DFA_IMP_CNBT_ISC_K1_NULL);
    }

    public String getDfaImpCnbtIscK1NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtIscK1Null(), Len.DFA_IMP_CNBT_ISC_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_ISC_K1 = 1;
        public static final int DFA_IMP_CNBT_ISC_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_ISC_K1 = 8;
        public static final int DFA_IMP_CNBT_ISC_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_ISC_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_ISC_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
