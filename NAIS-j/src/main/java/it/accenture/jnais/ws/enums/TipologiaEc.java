package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIPOLOGIA-EC<br>
 * Variable: TIPOLOGIA-EC from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TipologiaEc {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.TIPOLOGIA_EC);
    public static final String MU = "MU";
    public static final String RV = "PRI";
    public static final String UL = "PUL";
    public static final String TCM = "TC";

    //==== METHODS ====
    public void setTipologiaEc(String tipologiaEc) {
        this.value = Functions.subString(tipologiaEc, Len.TIPOLOGIA_EC);
    }

    public String getTipologiaEc() {
        return this.value;
    }

    public boolean isMu() {
        return value.equals(MU);
    }

    public boolean isRv() {
        return value.equals(RV);
    }

    public boolean isUl() {
        return value.equals(UL);
    }

    public boolean isTcm() {
        return value.equals(TCM);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPOLOGIA_EC = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
