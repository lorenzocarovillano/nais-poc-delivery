package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-ID-POLI<br>
 * Variable: WS-ID-POLI from program LDBM0170<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsIdPoli extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WS-ID-POLI
    private int wsIdPoli;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_ID_POLI;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWsIdPoliFromBuffer(buf);
    }

    public void setWsIdPoli(int wsIdPoli) {
        this.wsIdPoli = wsIdPoli;
    }

    public void setWsIdPoliFromBuffer(byte[] buffer, int offset) {
        setWsIdPoli(MarshalByte.readPackedAsInt(buffer, offset, Len.Int.WS_ID_POLI, 0));
    }

    public void setWsIdPoliFromBuffer(byte[] buffer) {
        setWsIdPoliFromBuffer(buffer, 1);
    }

    public int getWsIdPoli() {
        return this.wsIdPoli;
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.intToBufferPacked(getWsIdPoli(), Len.Int.WS_ID_POLI);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_POLI = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_ID_POLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WS_ID_POLI = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
