package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-ABB-TS-662014<br>
 * Variable: LQU-PC-ABB-TS-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcAbbTs662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcAbbTs662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_ABB_TS662014;
    }

    public void setLquPcAbbTs662014(AfDecimal lquPcAbbTs662014) {
        writeDecimalAsPacked(Pos.LQU_PC_ABB_TS662014, lquPcAbbTs662014.copy());
    }

    public void setLquPcAbbTs662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_ABB_TS662014, Pos.LQU_PC_ABB_TS662014);
    }

    /**Original name: LQU-PC-ABB-TS-662014<br>*/
    public AfDecimal getLquPcAbbTs662014() {
        return readPackedAsDecimal(Pos.LQU_PC_ABB_TS662014, Len.Int.LQU_PC_ABB_TS662014, Len.Fract.LQU_PC_ABB_TS662014);
    }

    public byte[] getLquPcAbbTs662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_ABB_TS662014, Pos.LQU_PC_ABB_TS662014);
        return buffer;
    }

    public void setLquPcAbbTs662014Null(String lquPcAbbTs662014Null) {
        writeString(Pos.LQU_PC_ABB_TS662014_NULL, lquPcAbbTs662014Null, Len.LQU_PC_ABB_TS662014_NULL);
    }

    /**Original name: LQU-PC-ABB-TS-662014-NULL<br>*/
    public String getLquPcAbbTs662014Null() {
        return readString(Pos.LQU_PC_ABB_TS662014_NULL, Len.LQU_PC_ABB_TS662014_NULL);
    }

    public String getLquPcAbbTs662014NullFormatted() {
        return Functions.padBlanks(getLquPcAbbTs662014Null(), Len.LQU_PC_ABB_TS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_ABB_TS662014 = 1;
        public static final int LQU_PC_ABB_TS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_ABB_TS662014 = 4;
        public static final int LQU_PC_ABB_TS662014_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_ABB_TS662014 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_ABB_TS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
