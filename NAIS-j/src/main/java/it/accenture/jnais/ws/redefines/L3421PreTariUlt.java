package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-TARI-ULT<br>
 * Variable: L3421-PRE-TARI-ULT from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PreTariUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PreTariUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_TARI_ULT;
    }

    public void setL3421PreTariUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_TARI_ULT, Pos.L3421_PRE_TARI_ULT);
    }

    /**Original name: L3421-PRE-TARI-ULT<br>*/
    public AfDecimal getL3421PreTariUlt() {
        return readPackedAsDecimal(Pos.L3421_PRE_TARI_ULT, Len.Int.L3421_PRE_TARI_ULT, Len.Fract.L3421_PRE_TARI_ULT);
    }

    public byte[] getL3421PreTariUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_TARI_ULT, Pos.L3421_PRE_TARI_ULT);
        return buffer;
    }

    /**Original name: L3421-PRE-TARI-ULT-NULL<br>*/
    public String getL3421PreTariUltNull() {
        return readString(Pos.L3421_PRE_TARI_ULT_NULL, Len.L3421_PRE_TARI_ULT_NULL);
    }

    public String getL3421PreTariUltNullFormatted() {
        return Functions.padBlanks(getL3421PreTariUltNull(), Len.L3421_PRE_TARI_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_TARI_ULT = 1;
        public static final int L3421_PRE_TARI_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_TARI_ULT = 8;
        public static final int L3421_PRE_TARI_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_TARI_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_TARI_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
