package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CLOSE-FILESQS1<br>
 * Variable: FLAG-CLOSE-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCloseFilesqs1 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCloseFilesqs1(char flagCloseFilesqs1) {
        this.value = flagCloseFilesqs1;
    }

    public char getFlagCloseFilesqs1() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setCloseFilesqs1No() {
        value = NO;
    }
}
