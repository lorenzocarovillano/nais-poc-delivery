package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-ID-RICH-ESTRAZ-AGG<br>
 * Variable: B03-ID-RICH-ESTRAZ-AGG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03IdRichEstrazAgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03IdRichEstrazAgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_ID_RICH_ESTRAZ_AGG;
    }

    public void setB03IdRichEstrazAgg(int b03IdRichEstrazAgg) {
        writeIntAsPacked(Pos.B03_ID_RICH_ESTRAZ_AGG, b03IdRichEstrazAgg, Len.Int.B03_ID_RICH_ESTRAZ_AGG);
    }

    public void setB03IdRichEstrazAggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_ID_RICH_ESTRAZ_AGG, Pos.B03_ID_RICH_ESTRAZ_AGG);
    }

    /**Original name: B03-ID-RICH-ESTRAZ-AGG<br>*/
    public int getB03IdRichEstrazAgg() {
        return readPackedAsInt(Pos.B03_ID_RICH_ESTRAZ_AGG, Len.Int.B03_ID_RICH_ESTRAZ_AGG);
    }

    public byte[] getB03IdRichEstrazAggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_ID_RICH_ESTRAZ_AGG, Pos.B03_ID_RICH_ESTRAZ_AGG);
        return buffer;
    }

    public void setB03IdRichEstrazAggNull(String b03IdRichEstrazAggNull) {
        writeString(Pos.B03_ID_RICH_ESTRAZ_AGG_NULL, b03IdRichEstrazAggNull, Len.B03_ID_RICH_ESTRAZ_AGG_NULL);
    }

    /**Original name: B03-ID-RICH-ESTRAZ-AGG-NULL<br>*/
    public String getB03IdRichEstrazAggNull() {
        return readString(Pos.B03_ID_RICH_ESTRAZ_AGG_NULL, Len.B03_ID_RICH_ESTRAZ_AGG_NULL);
    }

    public String getB03IdRichEstrazAggNullFormatted() {
        return Functions.padBlanks(getB03IdRichEstrazAggNull(), Len.B03_ID_RICH_ESTRAZ_AGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_ID_RICH_ESTRAZ_AGG = 1;
        public static final int B03_ID_RICH_ESTRAZ_AGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_ID_RICH_ESTRAZ_AGG = 5;
        public static final int B03_ID_RICH_ESTRAZ_AGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_ID_RICH_ESTRAZ_AGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
