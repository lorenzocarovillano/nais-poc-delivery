package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-ETA-SCAD-FEMM-DFLT<br>
 * Variable: WDCO-ETA-SCAD-FEMM-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoEtaScadFemmDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoEtaScadFemmDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_ETA_SCAD_FEMM_DFLT;
    }

    public void setWdcoEtaScadFemmDflt(int wdcoEtaScadFemmDflt) {
        writeIntAsPacked(Pos.WDCO_ETA_SCAD_FEMM_DFLT, wdcoEtaScadFemmDflt, Len.Int.WDCO_ETA_SCAD_FEMM_DFLT);
    }

    public void setWdcoEtaScadFemmDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_ETA_SCAD_FEMM_DFLT, Pos.WDCO_ETA_SCAD_FEMM_DFLT);
    }

    /**Original name: WDCO-ETA-SCAD-FEMM-DFLT<br>*/
    public int getWdcoEtaScadFemmDflt() {
        return readPackedAsInt(Pos.WDCO_ETA_SCAD_FEMM_DFLT, Len.Int.WDCO_ETA_SCAD_FEMM_DFLT);
    }

    public byte[] getWdcoEtaScadFemmDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_ETA_SCAD_FEMM_DFLT, Pos.WDCO_ETA_SCAD_FEMM_DFLT);
        return buffer;
    }

    public void setWdcoEtaScadFemmDfltNull(String wdcoEtaScadFemmDfltNull) {
        writeString(Pos.WDCO_ETA_SCAD_FEMM_DFLT_NULL, wdcoEtaScadFemmDfltNull, Len.WDCO_ETA_SCAD_FEMM_DFLT_NULL);
    }

    /**Original name: WDCO-ETA-SCAD-FEMM-DFLT-NULL<br>*/
    public String getWdcoEtaScadFemmDfltNull() {
        return readString(Pos.WDCO_ETA_SCAD_FEMM_DFLT_NULL, Len.WDCO_ETA_SCAD_FEMM_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_ETA_SCAD_FEMM_DFLT = 1;
        public static final int WDCO_ETA_SCAD_FEMM_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_ETA_SCAD_FEMM_DFLT = 3;
        public static final int WDCO_ETA_SCAD_FEMM_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_ETA_SCAD_FEMM_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
