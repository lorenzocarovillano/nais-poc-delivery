package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ALQ-MARG-C-SUBRSH<br>
 * Variable: W-B03-ALQ-MARG-C-SUBRSH from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AlqMargCSubrshLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AlqMargCSubrshLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ALQ_MARG_C_SUBRSH;
    }

    public void setwB03AlqMargCSubrsh(AfDecimal wB03AlqMargCSubrsh) {
        writeDecimalAsPacked(Pos.W_B03_ALQ_MARG_C_SUBRSH, wB03AlqMargCSubrsh.copy());
    }

    /**Original name: W-B03-ALQ-MARG-C-SUBRSH<br>*/
    public AfDecimal getwB03AlqMargCSubrsh() {
        return readPackedAsDecimal(Pos.W_B03_ALQ_MARG_C_SUBRSH, Len.Int.W_B03_ALQ_MARG_C_SUBRSH, Len.Fract.W_B03_ALQ_MARG_C_SUBRSH);
    }

    public byte[] getwB03AlqMargCSubrshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ALQ_MARG_C_SUBRSH, Pos.W_B03_ALQ_MARG_C_SUBRSH);
        return buffer;
    }

    public void setwB03AlqMargCSubrshNull(String wB03AlqMargCSubrshNull) {
        writeString(Pos.W_B03_ALQ_MARG_C_SUBRSH_NULL, wB03AlqMargCSubrshNull, Len.W_B03_ALQ_MARG_C_SUBRSH_NULL);
    }

    /**Original name: W-B03-ALQ-MARG-C-SUBRSH-NULL<br>*/
    public String getwB03AlqMargCSubrshNull() {
        return readString(Pos.W_B03_ALQ_MARG_C_SUBRSH_NULL, Len.W_B03_ALQ_MARG_C_SUBRSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ALQ_MARG_C_SUBRSH = 1;
        public static final int W_B03_ALQ_MARG_C_SUBRSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ALQ_MARG_C_SUBRSH = 4;
        public static final int W_B03_ALQ_MARG_C_SUBRSH_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ALQ_MARG_C_SUBRSH = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
