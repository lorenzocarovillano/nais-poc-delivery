package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-FRAZ-DFLT<br>
 * Variable: DCO-FRAZ-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoFrazDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoFrazDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_FRAZ_DFLT;
    }

    public void setDcoFrazDflt(int dcoFrazDflt) {
        writeIntAsPacked(Pos.DCO_FRAZ_DFLT, dcoFrazDflt, Len.Int.DCO_FRAZ_DFLT);
    }

    public void setDcoFrazDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_FRAZ_DFLT, Pos.DCO_FRAZ_DFLT);
    }

    /**Original name: DCO-FRAZ-DFLT<br>*/
    public int getDcoFrazDflt() {
        return readPackedAsInt(Pos.DCO_FRAZ_DFLT, Len.Int.DCO_FRAZ_DFLT);
    }

    public byte[] getDcoFrazDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_FRAZ_DFLT, Pos.DCO_FRAZ_DFLT);
        return buffer;
    }

    public void setDcoFrazDfltNull(String dcoFrazDfltNull) {
        writeString(Pos.DCO_FRAZ_DFLT_NULL, dcoFrazDfltNull, Len.DCO_FRAZ_DFLT_NULL);
    }

    /**Original name: DCO-FRAZ-DFLT-NULL<br>*/
    public String getDcoFrazDfltNull() {
        return readString(Pos.DCO_FRAZ_DFLT_NULL, Len.DCO_FRAZ_DFLT_NULL);
    }

    public String getDcoFrazDfltNullFormatted() {
        return Functions.padBlanks(getDcoFrazDfltNull(), Len.DCO_FRAZ_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_FRAZ_DFLT = 1;
        public static final int DCO_FRAZ_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_FRAZ_DFLT = 3;
        public static final int DCO_FRAZ_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_FRAZ_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
