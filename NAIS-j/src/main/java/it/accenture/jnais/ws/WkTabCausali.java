package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.redefines.WkTabCaus1;

/**Original name: WK-TAB-CAUSALI<br>
 * Variable: WK-TAB-CAUSALI from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkTabCausali {

    //==== PROPERTIES ====
    //Original name: WK-TAB-CAUS-ELE-MAX
    private short wkTabCausEleMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WK-TAB-CAUS1
    private WkTabCaus1 wkTabCaus1 = new WkTabCaus1();

    //==== METHODS ====
    public void setWkTabCausEleMax(short wkTabCausEleMax) {
        this.wkTabCausEleMax = wkTabCausEleMax;
    }

    public short getWkTabCausEleMax() {
        return this.wkTabCausEleMax;
    }

    public WkTabCaus1 getWkTabCaus1() {
        return wkTabCaus1;
    }
}
