package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-DT-CALC<br>
 * Variable: RST-DT-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstDtCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstDtCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_DT_CALC;
    }

    public void setRstDtCalc(int rstDtCalc) {
        writeIntAsPacked(Pos.RST_DT_CALC, rstDtCalc, Len.Int.RST_DT_CALC);
    }

    public void setRstDtCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_DT_CALC, Pos.RST_DT_CALC);
    }

    /**Original name: RST-DT-CALC<br>*/
    public int getRstDtCalc() {
        return readPackedAsInt(Pos.RST_DT_CALC, Len.Int.RST_DT_CALC);
    }

    public byte[] getRstDtCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_DT_CALC, Pos.RST_DT_CALC);
        return buffer;
    }

    public void setRstDtCalcNull(String rstDtCalcNull) {
        writeString(Pos.RST_DT_CALC_NULL, rstDtCalcNull, Len.RST_DT_CALC_NULL);
    }

    /**Original name: RST-DT-CALC-NULL<br>*/
    public String getRstDtCalcNull() {
        return readString(Pos.RST_DT_CALC_NULL, Len.RST_DT_CALC_NULL);
    }

    public String getRstDtCalcNullFormatted() {
        return Functions.padBlanks(getRstDtCalcNull(), Len.RST_DT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_DT_CALC = 1;
        public static final int RST_DT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_DT_CALC = 5;
        public static final int RST_DT_CALC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_DT_CALC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
