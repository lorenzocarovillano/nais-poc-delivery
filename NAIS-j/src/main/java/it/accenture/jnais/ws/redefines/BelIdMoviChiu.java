package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-ID-MOVI-CHIU<br>
 * Variable: BEL-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_ID_MOVI_CHIU;
    }

    public void setBelIdMoviChiu(int belIdMoviChiu) {
        writeIntAsPacked(Pos.BEL_ID_MOVI_CHIU, belIdMoviChiu, Len.Int.BEL_ID_MOVI_CHIU);
    }

    public void setBelIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_ID_MOVI_CHIU, Pos.BEL_ID_MOVI_CHIU);
    }

    /**Original name: BEL-ID-MOVI-CHIU<br>*/
    public int getBelIdMoviChiu() {
        return readPackedAsInt(Pos.BEL_ID_MOVI_CHIU, Len.Int.BEL_ID_MOVI_CHIU);
    }

    public byte[] getBelIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_ID_MOVI_CHIU, Pos.BEL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setBelIdMoviChiuNull(String belIdMoviChiuNull) {
        writeString(Pos.BEL_ID_MOVI_CHIU_NULL, belIdMoviChiuNull, Len.BEL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: BEL-ID-MOVI-CHIU-NULL<br>*/
    public String getBelIdMoviChiuNull() {
        return readString(Pos.BEL_ID_MOVI_CHIU_NULL, Len.BEL_ID_MOVI_CHIU_NULL);
    }

    public String getBelIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getBelIdMoviChiuNull(), Len.BEL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_ID_MOVI_CHIU = 1;
        public static final int BEL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_ID_MOVI_CHIU = 5;
        public static final int BEL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
