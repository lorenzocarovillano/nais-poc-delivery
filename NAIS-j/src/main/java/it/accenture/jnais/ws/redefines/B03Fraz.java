package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-FRAZ<br>
 * Variable: B03-FRAZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Fraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Fraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_FRAZ;
    }

    public void setB03Fraz(int b03Fraz) {
        writeIntAsPacked(Pos.B03_FRAZ, b03Fraz, Len.Int.B03_FRAZ);
    }

    public void setB03FrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_FRAZ, Pos.B03_FRAZ);
    }

    /**Original name: B03-FRAZ<br>*/
    public int getB03Fraz() {
        return readPackedAsInt(Pos.B03_FRAZ, Len.Int.B03_FRAZ);
    }

    public byte[] getB03FrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_FRAZ, Pos.B03_FRAZ);
        return buffer;
    }

    public void setB03FrazNull(String b03FrazNull) {
        writeString(Pos.B03_FRAZ_NULL, b03FrazNull, Len.B03_FRAZ_NULL);
    }

    /**Original name: B03-FRAZ-NULL<br>*/
    public String getB03FrazNull() {
        return readString(Pos.B03_FRAZ_NULL, Len.B03_FRAZ_NULL);
    }

    public String getB03FrazNullFormatted() {
        return Functions.padBlanks(getB03FrazNull(), Len.B03_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_FRAZ = 1;
        public static final int B03_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_FRAZ = 3;
        public static final int B03_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
