package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.modernsystems.ctu.core.SerializableParameter;

import it.accenture.jnais.copy.Lccc0019CampiEsito;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.Lccc0019Controllo;
import it.accenture.jnais.ws.enums.Lccc0019ImportoPlat;
import it.accenture.jnais.ws.enums.Lccc0019ReturnCode;
import it.accenture.jnais.ws.enums.Lccc0019Tariffa;
import it.accenture.jnais.ws.enums.Lccc0019TipoAgg;
import it.accenture.jnais.ws.enums.Lccc0019TpOperaz;
import it.accenture.jnais.ws.occurs.Lccc0019TabFondi;

/**Original name: AREA-LCCC0019<br>
 * Variable: AREA-LCCC0019 from program LVES0270<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaLccc0019 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_FONDI_MAXOCCURS = 3;
    /**Original name: LCCC0019-TP-OPERAZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     PORTAFOGLIO VITA ITALIA                                     *
	 *     CREATA : 05/02/2007                                         *
	 * ----------------------------------------------------------------*
	 *     GESTIONE INPUT/OUTPUT VERIFICA PLATFOND
	 *     VERSIONE AL 24/03/2009
	 * ----------------------------------------------------------------*</pre>*/
    private Lccc0019TpOperaz tpOperaz = new Lccc0019TpOperaz();
    //Original name: LCCC0019-TARIFFA
    private Lccc0019Tariffa tariffa = new Lccc0019Tariffa();
    //Original name: LCCC0019-CONTROLLO
    private Lccc0019Controllo controllo = new Lccc0019Controllo();
    //Original name: LCCC0019-IMPORTO-PLAT
    private Lccc0019ImportoPlat importoPlat = new Lccc0019ImportoPlat();
    //Original name: LCCC0019-TIPO-AGG
    private Lccc0019TipoAgg tipoAgg = new Lccc0019TipoAgg();
    //Original name: LCCC0019-ELE-MAX-FONDI
    private short eleMaxFondi = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC0019-TAB-FONDI
    private Lccc0019TabFondi[] tabFondi = new Lccc0019TabFondi[TAB_FONDI_MAXOCCURS];
    //Original name: LCCC0019-IMP-DA-SCALARE
    private AfDecimal impDaScalare = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: LCCC0019-IMP-DA-SCALARE-OLD
    private AfDecimal impDaScalareOld = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    /**Original name: LCCC0019-IMP-DISP-PLAFOND<br>
	 * <pre>-- IMPORTO DISPONIBILE DI PLAFOND</pre>*/
    private AfDecimal impDispPlafond = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    /**Original name: LCCC0019-RETURN-CODE<br>
	 * <pre>-- return code</pre>*/
    private Lccc0019ReturnCode returnCode = new Lccc0019ReturnCode();
    //Original name: LCCC0019-SQLCODE-SIGNED
    private Idso0011SqlcodeSigned sqlcodeSigned = new Idso0011SqlcodeSigned();
    //Original name: LCCC0019-SQLCODE
    private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
    //Original name: LCCC0019-CAMPI-ESITO
    private Lccc0019CampiEsito campiEsito = new Lccc0019CampiEsito();

    //==== CONSTRUCTORS ====
    public AreaLccc0019() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_LCCC0019;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaLccc0019Bytes(buf);
    }

    public void init() {
        for (int tabFondiIdx = 1; tabFondiIdx <= TAB_FONDI_MAXOCCURS; tabFondiIdx++) {
            tabFondi[tabFondiIdx - 1] = new Lccc0019TabFondi();
        }
    }

    public void setAreaLccc0019Bytes(byte[] buffer) {
        setAreaLccc0019Bytes(buffer, 1);
    }

    public byte[] getAreaLccc0019Bytes() {
        byte[] buffer = new byte[Len.AREA_LCCC0019];
        return getAreaLccc0019Bytes(buffer, 1);
    }

    public void setAreaLccc0019Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpOperaz.setTpOperaz(MarshalByte.readString(buffer, position, Lccc0019TpOperaz.Len.TP_OPERAZ));
        position += Lccc0019TpOperaz.Len.TP_OPERAZ;
        tariffa.setTariffa(MarshalByte.readString(buffer, position, Lccc0019Tariffa.Len.TARIFFA));
        position += Lccc0019Tariffa.Len.TARIFFA;
        controllo.setControllo(MarshalByte.readString(buffer, position, Lccc0019Controllo.Len.CONTROLLO));
        position += Lccc0019Controllo.Len.CONTROLLO;
        importoPlat.setImportoPlat(MarshalByte.readString(buffer, position, Lccc0019ImportoPlat.Len.IMPORTO_PLAT));
        position += Lccc0019ImportoPlat.Len.IMPORTO_PLAT;
        tipoAgg.setTipoAgg(MarshalByte.readString(buffer, position, Lccc0019TipoAgg.Len.TIPO_AGG));
        position += Lccc0019TipoAgg.Len.TIPO_AGG;
        setAreaFondoBytes(buffer, position);
        position += Len.AREA_FONDO;
        impDaScalare.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_DA_SCALARE, Len.Fract.IMP_DA_SCALARE));
        position += Len.IMP_DA_SCALARE;
        impDaScalareOld.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_DA_SCALARE_OLD, Len.Fract.IMP_DA_SCALARE_OLD));
        position += Len.IMP_DA_SCALARE_OLD;
        impDispPlafond.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMP_DISP_PLAFOND, Len.Fract.IMP_DISP_PLAFOND));
        position += Len.IMP_DISP_PLAFOND;
        returnCode.setReturnCode(MarshalByte.readString(buffer, position, Lccc0019ReturnCode.Len.RETURN_CODE));
        position += Lccc0019ReturnCode.Len.RETURN_CODE;
        sqlcodeSigned.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        sqlcode = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE), Len.SQLCODE);
        position += Len.SQLCODE;
        campiEsito.setCampiEsitoBytes(buffer, position);
    }

    public byte[] getAreaLccc0019Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, tpOperaz.getTpOperaz(), Lccc0019TpOperaz.Len.TP_OPERAZ);
        position += Lccc0019TpOperaz.Len.TP_OPERAZ;
        MarshalByte.writeString(buffer, position, tariffa.getTariffa(), Lccc0019Tariffa.Len.TARIFFA);
        position += Lccc0019Tariffa.Len.TARIFFA;
        MarshalByte.writeString(buffer, position, controllo.getControllo(), Lccc0019Controllo.Len.CONTROLLO);
        position += Lccc0019Controllo.Len.CONTROLLO;
        MarshalByte.writeString(buffer, position, importoPlat.getImportoPlat(), Lccc0019ImportoPlat.Len.IMPORTO_PLAT);
        position += Lccc0019ImportoPlat.Len.IMPORTO_PLAT;
        MarshalByte.writeString(buffer, position, tipoAgg.getTipoAgg(), Lccc0019TipoAgg.Len.TIPO_AGG);
        position += Lccc0019TipoAgg.Len.TIPO_AGG;
        getAreaFondoBytes(buffer, position);
        position += Len.AREA_FONDO;
        MarshalByte.writeDecimalAsPacked(buffer, position, impDaScalare.copy());
        position += Len.IMP_DA_SCALARE;
        MarshalByte.writeDecimalAsPacked(buffer, position, impDaScalareOld.copy());
        position += Len.IMP_DA_SCALARE_OLD;
        MarshalByte.writeDecimalAsPacked(buffer, position, impDispPlafond.copy());
        position += Len.IMP_DISP_PLAFOND;
        MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Lccc0019ReturnCode.Len.RETURN_CODE);
        position += Lccc0019ReturnCode.Len.RETURN_CODE;
        MarshalByte.writeInt(buffer, position, sqlcodeSigned.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
        position += Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
        MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
        position += Len.SQLCODE;
        campiEsito.getCampiEsitoBytes(buffer, position);
        return buffer;
    }

    public void setAreaFondoBytes(byte[] buffer, int offset) {
        int position = offset;
        eleMaxFondi = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_FONDI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabFondi[idx - 1].setTabFondiBytes(buffer, position);
                position += Lccc0019TabFondi.Len.TAB_FONDI;
            }
            else {
                tabFondi[idx - 1].initTabFondiSpaces();
                position += Lccc0019TabFondi.Len.TAB_FONDI;
            }
        }
    }

    public byte[] getAreaFondoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxFondi);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_FONDI_MAXOCCURS; idx++) {
            tabFondi[idx - 1].getTabFondiBytes(buffer, position);
            position += Lccc0019TabFondi.Len.TAB_FONDI;
        }
        return buffer;
    }

    public void setEleMaxFondi(short eleMaxFondi) {
        this.eleMaxFondi = eleMaxFondi;
    }

    public short getEleMaxFondi() {
        return this.eleMaxFondi;
    }

    public void setImpDaScalare(AfDecimal impDaScalare) {
        this.impDaScalare.assign(impDaScalare);
    }

    public AfDecimal getImpDaScalare() {
        return this.impDaScalare.copy();
    }

    public void setImpDaScalareOld(AfDecimal impDaScalareOld) {
        this.impDaScalareOld.assign(impDaScalareOld);
    }

    public AfDecimal getImpDaScalareOld() {
        return this.impDaScalareOld.copy();
    }

    public void setImpDispPlafond(AfDecimal impDispPlafond) {
        this.impDispPlafond.assign(impDispPlafond);
    }

    public AfDecimal getImpDispPlafond() {
        return this.impDispPlafond.copy();
    }

    public void setSqlcodeFormatted(String sqlcode) {
        this.sqlcode = PicFormatter.display("--(7)9").format(sqlcode).toString();
    }

    public long getSqlcode() {
        return PicParser.display("--(7)9").parseLong(this.sqlcode);
    }

    public Lccc0019CampiEsito getCampiEsito() {
        return campiEsito;
    }

    public Lccc0019Controllo getControllo() {
        return controllo;
    }

    public Lccc0019ImportoPlat getImportoPlat() {
        return importoPlat;
    }

    public Lccc0019ReturnCode getReturnCode() {
        return returnCode;
    }

    public Idso0011SqlcodeSigned getSqlcodeSigned() {
        return sqlcodeSigned;
    }

    public Lccc0019TabFondi getTabFondi(int idx) {
        return tabFondi[idx - 1];
    }

    public Lccc0019Tariffa getTariffa() {
        return tariffa;
    }

    public Lccc0019TipoAgg getTipoAgg() {
        return tipoAgg;
    }

    public Lccc0019TpOperaz getTpOperaz() {
        return tpOperaz;
    }

    @Override
    public byte[] serialize() {
        return getAreaLccc0019Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_MAX_FONDI = 2;
        public static final int AREA_FONDO = ELE_MAX_FONDI + AreaLccc0019.TAB_FONDI_MAXOCCURS * Lccc0019TabFondi.Len.TAB_FONDI;
        public static final int IMP_DA_SCALARE = 8;
        public static final int IMP_DA_SCALARE_OLD = 8;
        public static final int IMP_DISP_PLAFOND = 8;
        public static final int SQLCODE = 9;
        public static final int AREA_LCCC0019 = Lccc0019TpOperaz.Len.TP_OPERAZ + Lccc0019Tariffa.Len.TARIFFA + Lccc0019Controllo.Len.CONTROLLO + Lccc0019ImportoPlat.Len.IMPORTO_PLAT + Lccc0019TipoAgg.Len.TIPO_AGG + AREA_FONDO + IMP_DA_SCALARE + IMP_DA_SCALARE_OLD + IMP_DISP_PLAFOND + Lccc0019ReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + SQLCODE + Lccc0019CampiEsito.Len.CAMPI_ESITO;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IMP_DA_SCALARE = 12;
            public static final int IMP_DA_SCALARE_OLD = 12;
            public static final int IMP_DISP_PLAFOND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_DA_SCALARE = 3;
            public static final int IMP_DA_SCALARE_OLD = 3;
            public static final int IMP_DISP_PLAFOND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
