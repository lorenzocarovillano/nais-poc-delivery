package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-INCR-PRE<br>
 * Variable: L3421-INCR-PRE from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421IncrPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421IncrPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_INCR_PRE;
    }

    public void setL3421IncrPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_INCR_PRE, Pos.L3421_INCR_PRE);
    }

    /**Original name: L3421-INCR-PRE<br>*/
    public AfDecimal getL3421IncrPre() {
        return readPackedAsDecimal(Pos.L3421_INCR_PRE, Len.Int.L3421_INCR_PRE, Len.Fract.L3421_INCR_PRE);
    }

    public byte[] getL3421IncrPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_INCR_PRE, Pos.L3421_INCR_PRE);
        return buffer;
    }

    /**Original name: L3421-INCR-PRE-NULL<br>*/
    public String getL3421IncrPreNull() {
        return readString(Pos.L3421_INCR_PRE_NULL, Len.L3421_INCR_PRE_NULL);
    }

    public String getL3421IncrPreNullFormatted() {
        return Functions.padBlanks(getL3421IncrPreNull(), Len.L3421_INCR_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_INCR_PRE = 1;
        public static final int L3421_INCR_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_INCR_PRE = 8;
        public static final int L3421_INCR_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_INCR_PRE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_INCR_PRE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
