package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNDE-END2000-CALC<br>
 * Variable: WDFL-CNDE-END2000-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCndeEnd2000Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCndeEnd2000Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNDE_END2000_CALC;
    }

    public void setWdflCndeEnd2000Calc(AfDecimal wdflCndeEnd2000Calc) {
        writeDecimalAsPacked(Pos.WDFL_CNDE_END2000_CALC, wdflCndeEnd2000Calc.copy());
    }

    public void setWdflCndeEnd2000CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNDE_END2000_CALC, Pos.WDFL_CNDE_END2000_CALC);
    }

    /**Original name: WDFL-CNDE-END2000-CALC<br>*/
    public AfDecimal getWdflCndeEnd2000Calc() {
        return readPackedAsDecimal(Pos.WDFL_CNDE_END2000_CALC, Len.Int.WDFL_CNDE_END2000_CALC, Len.Fract.WDFL_CNDE_END2000_CALC);
    }

    public byte[] getWdflCndeEnd2000CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNDE_END2000_CALC, Pos.WDFL_CNDE_END2000_CALC);
        return buffer;
    }

    public void setWdflCndeEnd2000CalcNull(String wdflCndeEnd2000CalcNull) {
        writeString(Pos.WDFL_CNDE_END2000_CALC_NULL, wdflCndeEnd2000CalcNull, Len.WDFL_CNDE_END2000_CALC_NULL);
    }

    /**Original name: WDFL-CNDE-END2000-CALC-NULL<br>*/
    public String getWdflCndeEnd2000CalcNull() {
        return readString(Pos.WDFL_CNDE_END2000_CALC_NULL, Len.WDFL_CNDE_END2000_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_END2000_CALC = 1;
        public static final int WDFL_CNDE_END2000_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_END2000_CALC = 8;
        public static final int WDFL_CNDE_END2000_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_END2000_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_END2000_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
