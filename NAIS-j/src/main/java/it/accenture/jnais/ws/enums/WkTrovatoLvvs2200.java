package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TROVATO<br>
 * Variable: WK-TROVATO from program LVVS2200<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTrovatoLvvs2200 {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_TROVATO);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setWkTrovato(String wkTrovato) {
        this.value = Functions.subString(wkTrovato, Len.WK_TROVATO);
    }

    public String getWkTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value.equals(SI);
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TROVATO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
