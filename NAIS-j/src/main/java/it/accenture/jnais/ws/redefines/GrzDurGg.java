package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DUR-GG<br>
 * Variable: GRZ-DUR-GG from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DUR_GG;
    }

    public void setGrzDurGg(int grzDurGg) {
        writeIntAsPacked(Pos.GRZ_DUR_GG, grzDurGg, Len.Int.GRZ_DUR_GG);
    }

    public void setGrzDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DUR_GG, Pos.GRZ_DUR_GG);
    }

    /**Original name: GRZ-DUR-GG<br>*/
    public int getGrzDurGg() {
        return readPackedAsInt(Pos.GRZ_DUR_GG, Len.Int.GRZ_DUR_GG);
    }

    public byte[] getGrzDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DUR_GG, Pos.GRZ_DUR_GG);
        return buffer;
    }

    public void setGrzDurGgNull(String grzDurGgNull) {
        writeString(Pos.GRZ_DUR_GG_NULL, grzDurGgNull, Len.GRZ_DUR_GG_NULL);
    }

    /**Original name: GRZ-DUR-GG-NULL<br>*/
    public String getGrzDurGgNull() {
        return readString(Pos.GRZ_DUR_GG_NULL, Len.GRZ_DUR_GG_NULL);
    }

    public String getGrzDurGgNullFormatted() {
        return Functions.padBlanks(getGrzDurGgNull(), Len.GRZ_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DUR_GG = 1;
        public static final int GRZ_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DUR_GG = 3;
        public static final int GRZ_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
