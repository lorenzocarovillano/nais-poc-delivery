package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMPB-PROV-ACQ<br>
 * Variable: WTGA-IMPB-PROV-ACQ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpbProvAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpbProvAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMPB_PROV_ACQ;
    }

    public void setWtgaImpbProvAcq(AfDecimal wtgaImpbProvAcq) {
        writeDecimalAsPacked(Pos.WTGA_IMPB_PROV_ACQ, wtgaImpbProvAcq.copy());
    }

    public void setWtgaImpbProvAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMPB_PROV_ACQ, Pos.WTGA_IMPB_PROV_ACQ);
    }

    /**Original name: WTGA-IMPB-PROV-ACQ<br>*/
    public AfDecimal getWtgaImpbProvAcq() {
        return readPackedAsDecimal(Pos.WTGA_IMPB_PROV_ACQ, Len.Int.WTGA_IMPB_PROV_ACQ, Len.Fract.WTGA_IMPB_PROV_ACQ);
    }

    public byte[] getWtgaImpbProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMPB_PROV_ACQ, Pos.WTGA_IMPB_PROV_ACQ);
        return buffer;
    }

    public void initWtgaImpbProvAcqSpaces() {
        fill(Pos.WTGA_IMPB_PROV_ACQ, Len.WTGA_IMPB_PROV_ACQ, Types.SPACE_CHAR);
    }

    public void setWtgaImpbProvAcqNull(String wtgaImpbProvAcqNull) {
        writeString(Pos.WTGA_IMPB_PROV_ACQ_NULL, wtgaImpbProvAcqNull, Len.WTGA_IMPB_PROV_ACQ_NULL);
    }

    /**Original name: WTGA-IMPB-PROV-ACQ-NULL<br>*/
    public String getWtgaImpbProvAcqNull() {
        return readString(Pos.WTGA_IMPB_PROV_ACQ_NULL, Len.WTGA_IMPB_PROV_ACQ_NULL);
    }

    public String getWtgaImpbProvAcqNullFormatted() {
        return Functions.padBlanks(getWtgaImpbProvAcqNull(), Len.WTGA_IMPB_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_PROV_ACQ = 1;
        public static final int WTGA_IMPB_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_PROV_ACQ = 8;
        public static final int WTGA_IMPB_PROV_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_PROV_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
