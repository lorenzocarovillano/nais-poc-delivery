package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-ONER-LIQ<br>
 * Variable: S089-IMP-ONER-LIQ from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpOnerLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpOnerLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_ONER_LIQ;
    }

    public void setWlquImpOnerLiq(AfDecimal wlquImpOnerLiq) {
        writeDecimalAsPacked(Pos.S089_IMP_ONER_LIQ, wlquImpOnerLiq.copy());
    }

    public void setWlquImpOnerLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_ONER_LIQ, Pos.S089_IMP_ONER_LIQ);
    }

    /**Original name: WLQU-IMP-ONER-LIQ<br>*/
    public AfDecimal getWlquImpOnerLiq() {
        return readPackedAsDecimal(Pos.S089_IMP_ONER_LIQ, Len.Int.WLQU_IMP_ONER_LIQ, Len.Fract.WLQU_IMP_ONER_LIQ);
    }

    public byte[] getWlquImpOnerLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_ONER_LIQ, Pos.S089_IMP_ONER_LIQ);
        return buffer;
    }

    public void initWlquImpOnerLiqSpaces() {
        fill(Pos.S089_IMP_ONER_LIQ, Len.S089_IMP_ONER_LIQ, Types.SPACE_CHAR);
    }

    public void setWlquImpOnerLiqNull(String wlquImpOnerLiqNull) {
        writeString(Pos.S089_IMP_ONER_LIQ_NULL, wlquImpOnerLiqNull, Len.WLQU_IMP_ONER_LIQ_NULL);
    }

    /**Original name: WLQU-IMP-ONER-LIQ-NULL<br>*/
    public String getWlquImpOnerLiqNull() {
        return readString(Pos.S089_IMP_ONER_LIQ_NULL, Len.WLQU_IMP_ONER_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_ONER_LIQ = 1;
        public static final int S089_IMP_ONER_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_ONER_LIQ = 8;
        public static final int WLQU_IMP_ONER_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_ONER_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_ONER_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
