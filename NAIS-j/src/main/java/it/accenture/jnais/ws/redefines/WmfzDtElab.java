package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WMFZ-DT-ELAB<br>
 * Variable: WMFZ-DT-ELAB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmfzDtElab extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmfzDtElab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMFZ_DT_ELAB;
    }

    public void setWmfzDtElab(int wmfzDtElab) {
        writeIntAsPacked(Pos.WMFZ_DT_ELAB, wmfzDtElab, Len.Int.WMFZ_DT_ELAB);
    }

    public void setWmfzDtElabFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WMFZ_DT_ELAB, Pos.WMFZ_DT_ELAB);
    }

    /**Original name: WMFZ-DT-ELAB<br>*/
    public int getWmfzDtElab() {
        return readPackedAsInt(Pos.WMFZ_DT_ELAB, Len.Int.WMFZ_DT_ELAB);
    }

    public byte[] getWmfzDtElabAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WMFZ_DT_ELAB, Pos.WMFZ_DT_ELAB);
        return buffer;
    }

    public void initWmfzDtElabSpaces() {
        fill(Pos.WMFZ_DT_ELAB, Len.WMFZ_DT_ELAB, Types.SPACE_CHAR);
    }

    public void setWmfzDtElabNull(String wmfzDtElabNull) {
        writeString(Pos.WMFZ_DT_ELAB_NULL, wmfzDtElabNull, Len.WMFZ_DT_ELAB_NULL);
    }

    /**Original name: WMFZ-DT-ELAB-NULL<br>*/
    public String getWmfzDtElabNull() {
        return readString(Pos.WMFZ_DT_ELAB_NULL, Len.WMFZ_DT_ELAB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMFZ_DT_ELAB = 1;
        public static final int WMFZ_DT_ELAB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMFZ_DT_ELAB = 5;
        public static final int WMFZ_DT_ELAB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMFZ_DT_ELAB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
