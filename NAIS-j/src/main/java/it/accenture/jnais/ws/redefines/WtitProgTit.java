package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-PROG-TIT<br>
 * Variable: WTIT-PROG-TIT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitProgTit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitProgTit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_PROG_TIT;
    }

    public void setWtitProgTit(int wtitProgTit) {
        writeIntAsPacked(Pos.WTIT_PROG_TIT, wtitProgTit, Len.Int.WTIT_PROG_TIT);
    }

    public void setWtitProgTitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_PROG_TIT, Pos.WTIT_PROG_TIT);
    }

    /**Original name: WTIT-PROG-TIT<br>*/
    public int getWtitProgTit() {
        return readPackedAsInt(Pos.WTIT_PROG_TIT, Len.Int.WTIT_PROG_TIT);
    }

    public byte[] getWtitProgTitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_PROG_TIT, Pos.WTIT_PROG_TIT);
        return buffer;
    }

    public void initWtitProgTitSpaces() {
        fill(Pos.WTIT_PROG_TIT, Len.WTIT_PROG_TIT, Types.SPACE_CHAR);
    }

    public void setWtitProgTitNull(String wtitProgTitNull) {
        writeString(Pos.WTIT_PROG_TIT_NULL, wtitProgTitNull, Len.WTIT_PROG_TIT_NULL);
    }

    /**Original name: WTIT-PROG-TIT-NULL<br>*/
    public String getWtitProgTitNull() {
        return readString(Pos.WTIT_PROG_TIT_NULL, Len.WTIT_PROG_TIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_PROG_TIT = 1;
        public static final int WTIT_PROG_TIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_PROG_TIT = 3;
        public static final int WTIT_PROG_TIT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_PROG_TIT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
