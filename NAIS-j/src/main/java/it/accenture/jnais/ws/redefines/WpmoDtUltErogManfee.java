package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-DT-ULT-EROG-MANFEE<br>
 * Variable: WPMO-DT-ULT-EROG-MANFEE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoDtUltErogManfee extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoDtUltErogManfee() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_DT_ULT_EROG_MANFEE;
    }

    public void setWpmoDtUltErogManfee(int wpmoDtUltErogManfee) {
        writeIntAsPacked(Pos.WPMO_DT_ULT_EROG_MANFEE, wpmoDtUltErogManfee, Len.Int.WPMO_DT_ULT_EROG_MANFEE);
    }

    public void setWpmoDtUltErogManfeeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_DT_ULT_EROG_MANFEE, Pos.WPMO_DT_ULT_EROG_MANFEE);
    }

    /**Original name: WPMO-DT-ULT-EROG-MANFEE<br>*/
    public int getWpmoDtUltErogManfee() {
        return readPackedAsInt(Pos.WPMO_DT_ULT_EROG_MANFEE, Len.Int.WPMO_DT_ULT_EROG_MANFEE);
    }

    public byte[] getWpmoDtUltErogManfeeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_DT_ULT_EROG_MANFEE, Pos.WPMO_DT_ULT_EROG_MANFEE);
        return buffer;
    }

    public void initWpmoDtUltErogManfeeSpaces() {
        fill(Pos.WPMO_DT_ULT_EROG_MANFEE, Len.WPMO_DT_ULT_EROG_MANFEE, Types.SPACE_CHAR);
    }

    public void setWpmoDtUltErogManfeeNull(String wpmoDtUltErogManfeeNull) {
        writeString(Pos.WPMO_DT_ULT_EROG_MANFEE_NULL, wpmoDtUltErogManfeeNull, Len.WPMO_DT_ULT_EROG_MANFEE_NULL);
    }

    /**Original name: WPMO-DT-ULT-EROG-MANFEE-NULL<br>*/
    public String getWpmoDtUltErogManfeeNull() {
        return readString(Pos.WPMO_DT_ULT_EROG_MANFEE_NULL, Len.WPMO_DT_ULT_EROG_MANFEE_NULL);
    }

    public String getWpmoDtUltErogManfeeNullFormatted() {
        return Functions.padBlanks(getWpmoDtUltErogManfeeNull(), Len.WPMO_DT_ULT_EROG_MANFEE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_DT_ULT_EROG_MANFEE = 1;
        public static final int WPMO_DT_ULT_EROG_MANFEE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_DT_ULT_EROG_MANFEE = 5;
        public static final int WPMO_DT_ULT_EROG_MANFEE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_DT_ULT_EROG_MANFEE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
