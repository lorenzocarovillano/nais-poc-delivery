package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TABELLA-PARAMETRI<br>
 * Variables: WS-TABELLA-PARAMETRI from program IEAS9800<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsTabellaParametri {

    //==== PROPERTIES ====
    //Original name: WS-PARAMETRO
    private String wsParametro = "";

    //==== METHODS ====
    public void initTabellaParametriSpaces() {
        wsParametro = "";
    }

    public void setWsParametroSubstring(String replacement, int start, int length) {
        wsParametro = Functions.setSubstring(wsParametro, replacement, start, length);
    }

    public String getWsParametro() {
        return this.wsParametro;
    }

    public String getWsParametroFormatted() {
        return Functions.padBlanks(getWsParametro(), Len.WS_PARAMETRO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_PARAMETRO = 100;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
