package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: TAB-GIORNI<br>
 * Variable: TAB-GIORNI from program LVVS0000<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TabGiorniLvvs0000 extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int GG_MAXOCCURS = 12;

    //==== CONSTRUCTORS ====
    public TabGiorniLvvs0000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TAB_GIORNI;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "312831303130313130313031", Len.TAB_GIORNI);
    }

    public void setGg(int ggIdx, short gg) {
        int position = Pos.tabGg(ggIdx - 1);
        writeShort(position, gg, Len.Int.GG, SignType.NO_SIGN);
    }

    /**Original name: TAB-GG<br>*/
    public short getGg(int ggIdx) {
        int position = Pos.tabGg(ggIdx - 1);
        return readNumDispUnsignedShort(position, Len.GG);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TAB_GIORNI = 1;
        public static final int FILLER_WS = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int tabGg(int idx) {
            return FILLER_WS + idx * Len.GG;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GG = 2;
        public static final int TAB_GIORNI = 24;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GG = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
