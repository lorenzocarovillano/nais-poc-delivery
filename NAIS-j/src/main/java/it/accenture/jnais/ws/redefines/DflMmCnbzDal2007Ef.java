package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MM-CNBZ-DAL2007-EF<br>
 * Variable: DFL-MM-CNBZ-DAL2007-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMmCnbzDal2007Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMmCnbzDal2007Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MM_CNBZ_DAL2007_EF;
    }

    public void setDflMmCnbzDal2007Ef(int dflMmCnbzDal2007Ef) {
        writeIntAsPacked(Pos.DFL_MM_CNBZ_DAL2007_EF, dflMmCnbzDal2007Ef, Len.Int.DFL_MM_CNBZ_DAL2007_EF);
    }

    public void setDflMmCnbzDal2007EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MM_CNBZ_DAL2007_EF, Pos.DFL_MM_CNBZ_DAL2007_EF);
    }

    /**Original name: DFL-MM-CNBZ-DAL2007-EF<br>*/
    public int getDflMmCnbzDal2007Ef() {
        return readPackedAsInt(Pos.DFL_MM_CNBZ_DAL2007_EF, Len.Int.DFL_MM_CNBZ_DAL2007_EF);
    }

    public byte[] getDflMmCnbzDal2007EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MM_CNBZ_DAL2007_EF, Pos.DFL_MM_CNBZ_DAL2007_EF);
        return buffer;
    }

    public void setDflMmCnbzDal2007EfNull(String dflMmCnbzDal2007EfNull) {
        writeString(Pos.DFL_MM_CNBZ_DAL2007_EF_NULL, dflMmCnbzDal2007EfNull, Len.DFL_MM_CNBZ_DAL2007_EF_NULL);
    }

    /**Original name: DFL-MM-CNBZ-DAL2007-EF-NULL<br>*/
    public String getDflMmCnbzDal2007EfNull() {
        return readString(Pos.DFL_MM_CNBZ_DAL2007_EF_NULL, Len.DFL_MM_CNBZ_DAL2007_EF_NULL);
    }

    public String getDflMmCnbzDal2007EfNullFormatted() {
        return Functions.padBlanks(getDflMmCnbzDal2007EfNull(), Len.DFL_MM_CNBZ_DAL2007_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MM_CNBZ_DAL2007_EF = 1;
        public static final int DFL_MM_CNBZ_DAL2007_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MM_CNBZ_DAL2007_EF = 3;
        public static final int DFL_MM_CNBZ_DAL2007_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MM_CNBZ_DAL2007_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
