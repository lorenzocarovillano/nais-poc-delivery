package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: IDSV8888-LIVELLO-DEBUG<br>
 * Variable: IDSV8888-LIVELLO-DEBUG from copybook IDSV8888<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv8888LivelloDebug {

    //==== PROPERTIES ====
    private String value = "0";
    public static final String NO_DEBUG = "0";
    public static final String DEBUG_BASSO = "1";
    public static final String DEBUG_MEDIO = "2";
    public static final String DEBUG_ELEVATO = "3";
    public static final String DEBUG_ESASPERATO = "4";
    private static final String[] ANY_APPL_DBG = new String[] {"1", "2", "3", "4"};
    public static final String ARCH_BATCH_DBG = "5";
    public static final String COM_COB_JAV_DBG = "6";
    public static final String STRESS_TEST_DBG = "7";
    public static final String BUSINESS_DBG = "8";
    public static final String TOT_TUNING_DBG = "9";
    private static final String[] ANY_TUNING_DBG = new String[] {"5", "6", "7", "8", "9"};

    //==== METHODS ====
    public void setLivelloDebug(short livelloDebug) {
        this.value = NumericDisplay.asString(livelloDebug, Len.LIVELLO_DEBUG);
    }

    public void setLivelloDebugFormatted(String livelloDebug) {
        this.value = Trunc.toUnsignedNumeric(livelloDebug, Len.LIVELLO_DEBUG);
    }

    public short getLivelloDebug() {
        return NumericDisplay.asShort(this.value);
    }

    public String getLivelloDebugFormatted() {
        return this.value;
    }

    public void setNoDebug() {
        setLivelloDebugFormatted(NO_DEBUG);
    }

    public void setIdsv8888DebugBasso() {
        setLivelloDebugFormatted(DEBUG_BASSO);
    }

    public void setIdsv8888DebugMedio() {
        setLivelloDebugFormatted(DEBUG_MEDIO);
    }

    public void setIdsv8888DebugEsasperato() {
        setLivelloDebugFormatted(DEBUG_ESASPERATO);
    }

    public boolean isAnyApplDbg() {
        return ArrayUtils.contains(ANY_APPL_DBG, getLivelloDebugFormatted());
    }

    public boolean isArchBatchDbg() {
        return getLivelloDebugFormatted().equals(ARCH_BATCH_DBG);
    }

    public void setIdsv8888ArchBatchDbg() {
        setLivelloDebugFormatted(ARCH_BATCH_DBG);
    }

    public boolean isComCobJavDbg() {
        return getLivelloDebugFormatted().equals(COM_COB_JAV_DBG);
    }

    public boolean isStressTestDbg() {
        return getLivelloDebugFormatted().equals(STRESS_TEST_DBG);
    }

    public void setStressTestDbg() {
        setLivelloDebugFormatted(STRESS_TEST_DBG);
    }

    public boolean isBusinessDbg() {
        return getLivelloDebugFormatted().equals(BUSINESS_DBG);
    }

    public void setIdsv8888BusinessDbg() {
        setLivelloDebugFormatted(BUSINESS_DBG);
    }

    public boolean isTotTuningDbg() {
        return getLivelloDebugFormatted().equals(TOT_TUNING_DBG);
    }

    public boolean isAnyTuningDbg() {
        return ArrayUtils.contains(ANY_TUNING_DBG, getLivelloDebugFormatted());
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LIVELLO_DEBUG = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
