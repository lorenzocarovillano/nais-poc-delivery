package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-COEFF-OPZ-REN<br>
 * Variable: W-B03-COEFF-OPZ-REN from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CoeffOpzRenLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CoeffOpzRenLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COEFF_OPZ_REN;
    }

    public void setwB03CoeffOpzRen(AfDecimal wB03CoeffOpzRen) {
        writeDecimalAsPacked(Pos.W_B03_COEFF_OPZ_REN, wB03CoeffOpzRen.copy());
    }

    /**Original name: W-B03-COEFF-OPZ-REN<br>*/
    public AfDecimal getwB03CoeffOpzRen() {
        return readPackedAsDecimal(Pos.W_B03_COEFF_OPZ_REN, Len.Int.W_B03_COEFF_OPZ_REN, Len.Fract.W_B03_COEFF_OPZ_REN);
    }

    public byte[] getwB03CoeffOpzRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COEFF_OPZ_REN, Pos.W_B03_COEFF_OPZ_REN);
        return buffer;
    }

    public void setwB03CoeffOpzRenNull(String wB03CoeffOpzRenNull) {
        writeString(Pos.W_B03_COEFF_OPZ_REN_NULL, wB03CoeffOpzRenNull, Len.W_B03_COEFF_OPZ_REN_NULL);
    }

    /**Original name: W-B03-COEFF-OPZ-REN-NULL<br>*/
    public String getwB03CoeffOpzRenNull() {
        return readString(Pos.W_B03_COEFF_OPZ_REN_NULL, Len.W_B03_COEFF_OPZ_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_OPZ_REN = 1;
        public static final int W_B03_COEFF_OPZ_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COEFF_OPZ_REN = 4;
        public static final int W_B03_COEFF_OPZ_REN_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_OPZ_REN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COEFF_OPZ_REN = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
