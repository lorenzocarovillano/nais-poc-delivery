package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-INTR-RIAT<br>
 * Variable: DTC-INTR-RIAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_INTR_RIAT;
    }

    public void setDtcIntrRiat(AfDecimal dtcIntrRiat) {
        writeDecimalAsPacked(Pos.DTC_INTR_RIAT, dtcIntrRiat.copy());
    }

    public void setDtcIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_INTR_RIAT, Pos.DTC_INTR_RIAT);
    }

    /**Original name: DTC-INTR-RIAT<br>*/
    public AfDecimal getDtcIntrRiat() {
        return readPackedAsDecimal(Pos.DTC_INTR_RIAT, Len.Int.DTC_INTR_RIAT, Len.Fract.DTC_INTR_RIAT);
    }

    public byte[] getDtcIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_INTR_RIAT, Pos.DTC_INTR_RIAT);
        return buffer;
    }

    public void setDtcIntrRiatNull(String dtcIntrRiatNull) {
        writeString(Pos.DTC_INTR_RIAT_NULL, dtcIntrRiatNull, Len.DTC_INTR_RIAT_NULL);
    }

    /**Original name: DTC-INTR-RIAT-NULL<br>*/
    public String getDtcIntrRiatNull() {
        return readString(Pos.DTC_INTR_RIAT_NULL, Len.DTC_INTR_RIAT_NULL);
    }

    public String getDtcIntrRiatNullFormatted() {
        return Functions.padBlanks(getDtcIntrRiatNull(), Len.DTC_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_INTR_RIAT = 1;
        public static final int DTC_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_INTR_RIAT = 8;
        public static final int DTC_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
