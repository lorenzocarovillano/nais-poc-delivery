package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-MATU-RES-K1<br>
 * Variable: DFA-MATU-RES-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaMatuResK1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaMatuResK1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_MATU_RES_K1;
    }

    public void setDfaMatuResK1(AfDecimal dfaMatuResK1) {
        writeDecimalAsPacked(Pos.DFA_MATU_RES_K1, dfaMatuResK1.copy());
    }

    public void setDfaMatuResK1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_MATU_RES_K1, Pos.DFA_MATU_RES_K1);
    }

    /**Original name: DFA-MATU-RES-K1<br>*/
    public AfDecimal getDfaMatuResK1() {
        return readPackedAsDecimal(Pos.DFA_MATU_RES_K1, Len.Int.DFA_MATU_RES_K1, Len.Fract.DFA_MATU_RES_K1);
    }

    public byte[] getDfaMatuResK1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_MATU_RES_K1, Pos.DFA_MATU_RES_K1);
        return buffer;
    }

    public void setDfaMatuResK1Null(String dfaMatuResK1Null) {
        writeString(Pos.DFA_MATU_RES_K1_NULL, dfaMatuResK1Null, Len.DFA_MATU_RES_K1_NULL);
    }

    /**Original name: DFA-MATU-RES-K1-NULL<br>*/
    public String getDfaMatuResK1Null() {
        return readString(Pos.DFA_MATU_RES_K1_NULL, Len.DFA_MATU_RES_K1_NULL);
    }

    public String getDfaMatuResK1NullFormatted() {
        return Functions.padBlanks(getDfaMatuResK1Null(), Len.DFA_MATU_RES_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_MATU_RES_K1 = 1;
        public static final int DFA_MATU_RES_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_MATU_RES_K1 = 8;
        public static final int DFA_MATU_RES_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_MATU_RES_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_MATU_RES_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
