package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WCDG-FND-X-RECCDG<br>
 * Variables: WCDG-FND-X-RECCDG from copybook IVVC0224<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcdgFndXReccdg {

    //==== PROPERTIES ====
    //Original name: WCDG-COD-FND-RECCDG
    private String codFndReccdg = DefaultValues.stringVal(Len.COD_FND_RECCDG);
    //Original name: WCDG-CONTROVAL-FND-RECCDG
    private AfDecimal controvalFndReccdg = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);

    //==== METHODS ====
    public void setFndXReccdgBytes(byte[] buffer, int offset) {
        int position = offset;
        codFndReccdg = MarshalByte.readString(buffer, position, Len.COD_FND_RECCDG);
        position += Len.COD_FND_RECCDG;
        controvalFndReccdg.assign(MarshalByte.readDecimal(buffer, position, Len.Int.CONTROVAL_FND_RECCDG, Len.Fract.CONTROVAL_FND_RECCDG));
    }

    public byte[] getFndXReccdgBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codFndReccdg, Len.COD_FND_RECCDG);
        position += Len.COD_FND_RECCDG;
        MarshalByte.writeDecimal(buffer, position, controvalFndReccdg.copy());
        return buffer;
    }

    public void initFndXReccdgSpaces() {
        codFndReccdg = "";
        controvalFndReccdg.setNaN();
    }

    public void setCodFndReccdg(String codFndReccdg) {
        this.codFndReccdg = Functions.subString(codFndReccdg, Len.COD_FND_RECCDG);
    }

    public String getCodFndReccdg() {
        return this.codFndReccdg;
    }

    public void setControvalFndReccdg(AfDecimal controvalFndReccdg) {
        this.controvalFndReccdg.assign(controvalFndReccdg);
    }

    public AfDecimal getControvalFndReccdg() {
        return this.controvalFndReccdg.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND_RECCDG = 60;
        public static final int CONTROVAL_FND_RECCDG = 18;
        public static final int FND_X_RECCDG = COD_FND_RECCDG + CONTROVAL_FND_RECCDG;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CONTROVAL_FND_RECCDG = 11;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CONTROVAL_FND_RECCDG = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
