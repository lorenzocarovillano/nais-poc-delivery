package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-MATU-K3<br>
 * Variable: DFA-MATU-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaMatuK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaMatuK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_MATU_K3;
    }

    public void setDfaMatuK3(AfDecimal dfaMatuK3) {
        writeDecimalAsPacked(Pos.DFA_MATU_K3, dfaMatuK3.copy());
    }

    public void setDfaMatuK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_MATU_K3, Pos.DFA_MATU_K3);
    }

    /**Original name: DFA-MATU-K3<br>*/
    public AfDecimal getDfaMatuK3() {
        return readPackedAsDecimal(Pos.DFA_MATU_K3, Len.Int.DFA_MATU_K3, Len.Fract.DFA_MATU_K3);
    }

    public byte[] getDfaMatuK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_MATU_K3, Pos.DFA_MATU_K3);
        return buffer;
    }

    public void setDfaMatuK3Null(String dfaMatuK3Null) {
        writeString(Pos.DFA_MATU_K3_NULL, dfaMatuK3Null, Len.DFA_MATU_K3_NULL);
    }

    /**Original name: DFA-MATU-K3-NULL<br>*/
    public String getDfaMatuK3Null() {
        return readString(Pos.DFA_MATU_K3_NULL, Len.DFA_MATU_K3_NULL);
    }

    public String getDfaMatuK3NullFormatted() {
        return Functions.padBlanks(getDfaMatuK3Null(), Len.DFA_MATU_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_MATU_K3 = 1;
        public static final int DFA_MATU_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_MATU_K3 = 8;
        public static final int DFA_MATU_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_MATU_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_MATU_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
