package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WE15-ID-SEGMENTAZ-CLI<br>
 * Variable: WE15-ID-SEGMENTAZ-CLI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We15IdSegmentazCli extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We15IdSegmentazCli() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE15_ID_SEGMENTAZ_CLI;
    }

    public void setWe15IdSegmentazCli(int we15IdSegmentazCli) {
        writeIntAsPacked(Pos.WE15_ID_SEGMENTAZ_CLI, we15IdSegmentazCli, Len.Int.WE15_ID_SEGMENTAZ_CLI);
    }

    public void setWe15IdSegmentazCliFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WE15_ID_SEGMENTAZ_CLI, Pos.WE15_ID_SEGMENTAZ_CLI);
    }

    /**Original name: WE15-ID-SEGMENTAZ-CLI<br>*/
    public int getWe15IdSegmentazCli() {
        return readPackedAsInt(Pos.WE15_ID_SEGMENTAZ_CLI, Len.Int.WE15_ID_SEGMENTAZ_CLI);
    }

    public byte[] getWe15IdSegmentazCliAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE15_ID_SEGMENTAZ_CLI, Pos.WE15_ID_SEGMENTAZ_CLI);
        return buffer;
    }

    public void initWe15IdSegmentazCliSpaces() {
        fill(Pos.WE15_ID_SEGMENTAZ_CLI, Len.WE15_ID_SEGMENTAZ_CLI, Types.SPACE_CHAR);
    }

    public void setWe15IdSegmentazCliNull(String we15IdSegmentazCliNull) {
        writeString(Pos.WE15_ID_SEGMENTAZ_CLI_NULL, we15IdSegmentazCliNull, Len.WE15_ID_SEGMENTAZ_CLI_NULL);
    }

    /**Original name: WE15-ID-SEGMENTAZ-CLI-NULL<br>*/
    public String getWe15IdSegmentazCliNull() {
        return readString(Pos.WE15_ID_SEGMENTAZ_CLI_NULL, Len.WE15_ID_SEGMENTAZ_CLI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE15_ID_SEGMENTAZ_CLI = 1;
        public static final int WE15_ID_SEGMENTAZ_CLI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE15_ID_SEGMENTAZ_CLI = 5;
        public static final int WE15_ID_SEGMENTAZ_CLI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WE15_ID_SEGMENTAZ_CLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
