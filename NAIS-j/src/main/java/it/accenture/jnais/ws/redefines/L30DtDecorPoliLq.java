package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L30-DT-DECOR-POLI-LQ<br>
 * Variable: L30-DT-DECOR-POLI-LQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L30DtDecorPoliLq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L30DtDecorPoliLq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L30_DT_DECOR_POLI_LQ;
    }

    public void setL30DtDecorPoliLq(int l30DtDecorPoliLq) {
        writeIntAsPacked(Pos.L30_DT_DECOR_POLI_LQ, l30DtDecorPoliLq, Len.Int.L30_DT_DECOR_POLI_LQ);
    }

    public void setL30DtDecorPoliLqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L30_DT_DECOR_POLI_LQ, Pos.L30_DT_DECOR_POLI_LQ);
    }

    /**Original name: L30-DT-DECOR-POLI-LQ<br>*/
    public int getL30DtDecorPoliLq() {
        return readPackedAsInt(Pos.L30_DT_DECOR_POLI_LQ, Len.Int.L30_DT_DECOR_POLI_LQ);
    }

    public byte[] getL30DtDecorPoliLqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L30_DT_DECOR_POLI_LQ, Pos.L30_DT_DECOR_POLI_LQ);
        return buffer;
    }

    public void setL30DtDecorPoliLqNull(String l30DtDecorPoliLqNull) {
        writeString(Pos.L30_DT_DECOR_POLI_LQ_NULL, l30DtDecorPoliLqNull, Len.L30_DT_DECOR_POLI_LQ_NULL);
    }

    /**Original name: L30-DT-DECOR-POLI-LQ-NULL<br>*/
    public String getL30DtDecorPoliLqNull() {
        return readString(Pos.L30_DT_DECOR_POLI_LQ_NULL, Len.L30_DT_DECOR_POLI_LQ_NULL);
    }

    public String getL30DtDecorPoliLqNullFormatted() {
        return Functions.padBlanks(getL30DtDecorPoliLqNull(), Len.L30_DT_DECOR_POLI_LQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L30_DT_DECOR_POLI_LQ = 1;
        public static final int L30_DT_DECOR_POLI_LQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L30_DT_DECOR_POLI_LQ = 5;
        public static final int L30_DT_DECOR_POLI_LQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L30_DT_DECOR_POLI_LQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
