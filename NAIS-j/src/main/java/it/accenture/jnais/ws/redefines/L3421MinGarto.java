package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-MIN-GARTO<br>
 * Variable: L3421-MIN-GARTO from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421MinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421MinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_MIN_GARTO;
    }

    public void setL3421MinGartoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_MIN_GARTO, Pos.L3421_MIN_GARTO);
    }

    /**Original name: L3421-MIN-GARTO<br>*/
    public AfDecimal getL3421MinGarto() {
        return readPackedAsDecimal(Pos.L3421_MIN_GARTO, Len.Int.L3421_MIN_GARTO, Len.Fract.L3421_MIN_GARTO);
    }

    public byte[] getL3421MinGartoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_MIN_GARTO, Pos.L3421_MIN_GARTO);
        return buffer;
    }

    /**Original name: L3421-MIN-GARTO-NULL<br>*/
    public String getL3421MinGartoNull() {
        return readString(Pos.L3421_MIN_GARTO_NULL, Len.L3421_MIN_GARTO_NULL);
    }

    public String getL3421MinGartoNullFormatted() {
        return Functions.padBlanks(getL3421MinGartoNull(), Len.L3421_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_MIN_GARTO = 1;
        public static final int L3421_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_MIN_GARTO = 8;
        public static final int L3421_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_MIN_GARTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_MIN_GARTO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
