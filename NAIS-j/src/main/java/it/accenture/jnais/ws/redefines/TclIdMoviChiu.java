package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-ID-MOVI-CHIU<br>
 * Variable: TCL-ID-MOVI-CHIU from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_ID_MOVI_CHIU;
    }

    public void setTclIdMoviChiu(int tclIdMoviChiu) {
        writeIntAsPacked(Pos.TCL_ID_MOVI_CHIU, tclIdMoviChiu, Len.Int.TCL_ID_MOVI_CHIU);
    }

    public void setTclIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_ID_MOVI_CHIU, Pos.TCL_ID_MOVI_CHIU);
    }

    /**Original name: TCL-ID-MOVI-CHIU<br>*/
    public int getTclIdMoviChiu() {
        return readPackedAsInt(Pos.TCL_ID_MOVI_CHIU, Len.Int.TCL_ID_MOVI_CHIU);
    }

    public byte[] getTclIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_ID_MOVI_CHIU, Pos.TCL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setTclIdMoviChiuNull(String tclIdMoviChiuNull) {
        writeString(Pos.TCL_ID_MOVI_CHIU_NULL, tclIdMoviChiuNull, Len.TCL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: TCL-ID-MOVI-CHIU-NULL<br>*/
    public String getTclIdMoviChiuNull() {
        return readString(Pos.TCL_ID_MOVI_CHIU_NULL, Len.TCL_ID_MOVI_CHIU_NULL);
    }

    public String getTclIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getTclIdMoviChiuNull(), Len.TCL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_ID_MOVI_CHIU = 1;
        public static final int TCL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_ID_MOVI_CHIU = 5;
        public static final int TCL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
