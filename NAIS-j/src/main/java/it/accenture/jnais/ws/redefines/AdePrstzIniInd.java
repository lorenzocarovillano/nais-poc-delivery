package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PRSTZ-INI-IND<br>
 * Variable: ADE-PRSTZ-INI-IND from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePrstzIniInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePrstzIniInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PRSTZ_INI_IND;
    }

    public void setAdePrstzIniInd(AfDecimal adePrstzIniInd) {
        writeDecimalAsPacked(Pos.ADE_PRSTZ_INI_IND, adePrstzIniInd.copy());
    }

    public void setAdePrstzIniIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PRSTZ_INI_IND, Pos.ADE_PRSTZ_INI_IND);
    }

    /**Original name: ADE-PRSTZ-INI-IND<br>*/
    public AfDecimal getAdePrstzIniInd() {
        return readPackedAsDecimal(Pos.ADE_PRSTZ_INI_IND, Len.Int.ADE_PRSTZ_INI_IND, Len.Fract.ADE_PRSTZ_INI_IND);
    }

    public byte[] getAdePrstzIniIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PRSTZ_INI_IND, Pos.ADE_PRSTZ_INI_IND);
        return buffer;
    }

    public void setAdePrstzIniIndNull(String adePrstzIniIndNull) {
        writeString(Pos.ADE_PRSTZ_INI_IND_NULL, adePrstzIniIndNull, Len.ADE_PRSTZ_INI_IND_NULL);
    }

    /**Original name: ADE-PRSTZ-INI-IND-NULL<br>*/
    public String getAdePrstzIniIndNull() {
        return readString(Pos.ADE_PRSTZ_INI_IND_NULL, Len.ADE_PRSTZ_INI_IND_NULL);
    }

    public String getAdePrstzIniIndNullFormatted() {
        return Functions.padBlanks(getAdePrstzIniIndNull(), Len.ADE_PRSTZ_INI_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PRSTZ_INI_IND = 1;
        public static final int ADE_PRSTZ_INI_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PRSTZ_INI_IND = 8;
        public static final int ADE_PRSTZ_INI_IND_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PRSTZ_INI_IND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PRSTZ_INI_IND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
