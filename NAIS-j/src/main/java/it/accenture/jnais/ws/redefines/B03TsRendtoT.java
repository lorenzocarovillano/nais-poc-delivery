package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-RENDTO-T<br>
 * Variable: B03-TS-RENDTO-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsRendtoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsRendtoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_RENDTO_T;
    }

    public void setB03TsRendtoT(AfDecimal b03TsRendtoT) {
        writeDecimalAsPacked(Pos.B03_TS_RENDTO_T, b03TsRendtoT.copy());
    }

    public void setB03TsRendtoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_RENDTO_T, Pos.B03_TS_RENDTO_T);
    }

    /**Original name: B03-TS-RENDTO-T<br>*/
    public AfDecimal getB03TsRendtoT() {
        return readPackedAsDecimal(Pos.B03_TS_RENDTO_T, Len.Int.B03_TS_RENDTO_T, Len.Fract.B03_TS_RENDTO_T);
    }

    public byte[] getB03TsRendtoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_RENDTO_T, Pos.B03_TS_RENDTO_T);
        return buffer;
    }

    public void setB03TsRendtoTNull(String b03TsRendtoTNull) {
        writeString(Pos.B03_TS_RENDTO_T_NULL, b03TsRendtoTNull, Len.B03_TS_RENDTO_T_NULL);
    }

    /**Original name: B03-TS-RENDTO-T-NULL<br>*/
    public String getB03TsRendtoTNull() {
        return readString(Pos.B03_TS_RENDTO_T_NULL, Len.B03_TS_RENDTO_T_NULL);
    }

    public String getB03TsRendtoTNullFormatted() {
        return Functions.padBlanks(getB03TsRendtoTNull(), Len.B03_TS_RENDTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_RENDTO_T = 1;
        public static final int B03_TS_RENDTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_RENDTO_T = 8;
        public static final int B03_TS_RENDTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_RENDTO_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_RENDTO_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
