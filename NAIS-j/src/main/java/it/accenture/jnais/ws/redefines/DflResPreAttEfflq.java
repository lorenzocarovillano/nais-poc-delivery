package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RES-PRE-ATT-EFFLQ<br>
 * Variable: DFL-RES-PRE-ATT-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflResPreAttEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflResPreAttEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RES_PRE_ATT_EFFLQ;
    }

    public void setDflResPreAttEfflq(AfDecimal dflResPreAttEfflq) {
        writeDecimalAsPacked(Pos.DFL_RES_PRE_ATT_EFFLQ, dflResPreAttEfflq.copy());
    }

    public void setDflResPreAttEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RES_PRE_ATT_EFFLQ, Pos.DFL_RES_PRE_ATT_EFFLQ);
    }

    /**Original name: DFL-RES-PRE-ATT-EFFLQ<br>*/
    public AfDecimal getDflResPreAttEfflq() {
        return readPackedAsDecimal(Pos.DFL_RES_PRE_ATT_EFFLQ, Len.Int.DFL_RES_PRE_ATT_EFFLQ, Len.Fract.DFL_RES_PRE_ATT_EFFLQ);
    }

    public byte[] getDflResPreAttEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RES_PRE_ATT_EFFLQ, Pos.DFL_RES_PRE_ATT_EFFLQ);
        return buffer;
    }

    public void setDflResPreAttEfflqNull(String dflResPreAttEfflqNull) {
        writeString(Pos.DFL_RES_PRE_ATT_EFFLQ_NULL, dflResPreAttEfflqNull, Len.DFL_RES_PRE_ATT_EFFLQ_NULL);
    }

    /**Original name: DFL-RES-PRE-ATT-EFFLQ-NULL<br>*/
    public String getDflResPreAttEfflqNull() {
        return readString(Pos.DFL_RES_PRE_ATT_EFFLQ_NULL, Len.DFL_RES_PRE_ATT_EFFLQ_NULL);
    }

    public String getDflResPreAttEfflqNullFormatted() {
        return Functions.padBlanks(getDflResPreAttEfflqNull(), Len.DFL_RES_PRE_ATT_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRE_ATT_EFFLQ = 1;
        public static final int DFL_RES_PRE_ATT_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRE_ATT_EFFLQ = 8;
        public static final int DFL_RES_PRE_ATT_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRE_ATT_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRE_ATT_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
