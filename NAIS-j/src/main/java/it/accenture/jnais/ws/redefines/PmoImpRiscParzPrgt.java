package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-IMP-RISC-PARZ-PRGT<br>
 * Variable: PMO-IMP-RISC-PARZ-PRGT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoImpRiscParzPrgt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoImpRiscParzPrgt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_IMP_RISC_PARZ_PRGT;
    }

    public void setPmoImpRiscParzPrgt(AfDecimal pmoImpRiscParzPrgt) {
        writeDecimalAsPacked(Pos.PMO_IMP_RISC_PARZ_PRGT, pmoImpRiscParzPrgt.copy());
    }

    public void setPmoImpRiscParzPrgtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_IMP_RISC_PARZ_PRGT, Pos.PMO_IMP_RISC_PARZ_PRGT);
    }

    /**Original name: PMO-IMP-RISC-PARZ-PRGT<br>*/
    public AfDecimal getPmoImpRiscParzPrgt() {
        return readPackedAsDecimal(Pos.PMO_IMP_RISC_PARZ_PRGT, Len.Int.PMO_IMP_RISC_PARZ_PRGT, Len.Fract.PMO_IMP_RISC_PARZ_PRGT);
    }

    public byte[] getPmoImpRiscParzPrgtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_IMP_RISC_PARZ_PRGT, Pos.PMO_IMP_RISC_PARZ_PRGT);
        return buffer;
    }

    public void initPmoImpRiscParzPrgtHighValues() {
        fill(Pos.PMO_IMP_RISC_PARZ_PRGT, Len.PMO_IMP_RISC_PARZ_PRGT, Types.HIGH_CHAR_VAL);
    }

    public void setPmoImpRiscParzPrgtNull(String pmoImpRiscParzPrgtNull) {
        writeString(Pos.PMO_IMP_RISC_PARZ_PRGT_NULL, pmoImpRiscParzPrgtNull, Len.PMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    /**Original name: PMO-IMP-RISC-PARZ-PRGT-NULL<br>*/
    public String getPmoImpRiscParzPrgtNull() {
        return readString(Pos.PMO_IMP_RISC_PARZ_PRGT_NULL, Len.PMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    public String getPmoImpRiscParzPrgtNullFormatted() {
        return Functions.padBlanks(getPmoImpRiscParzPrgtNull(), Len.PMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_IMP_RISC_PARZ_PRGT = 1;
        public static final int PMO_IMP_RISC_PARZ_PRGT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_IMP_RISC_PARZ_PRGT = 8;
        public static final int PMO_IMP_RISC_PARZ_PRGT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_IMP_RISC_PARZ_PRGT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_IMP_RISC_PARZ_PRGT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
