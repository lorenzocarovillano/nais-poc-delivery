package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-SOST-CALC<br>
 * Variable: DFL-ACCPRE-SOST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreSostCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreSostCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_SOST_CALC;
    }

    public void setDflAccpreSostCalc(AfDecimal dflAccpreSostCalc) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_SOST_CALC, dflAccpreSostCalc.copy());
    }

    public void setDflAccpreSostCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_SOST_CALC, Pos.DFL_ACCPRE_SOST_CALC);
    }

    /**Original name: DFL-ACCPRE-SOST-CALC<br>*/
    public AfDecimal getDflAccpreSostCalc() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_SOST_CALC, Len.Int.DFL_ACCPRE_SOST_CALC, Len.Fract.DFL_ACCPRE_SOST_CALC);
    }

    public byte[] getDflAccpreSostCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_SOST_CALC, Pos.DFL_ACCPRE_SOST_CALC);
        return buffer;
    }

    public void setDflAccpreSostCalcNull(String dflAccpreSostCalcNull) {
        writeString(Pos.DFL_ACCPRE_SOST_CALC_NULL, dflAccpreSostCalcNull, Len.DFL_ACCPRE_SOST_CALC_NULL);
    }

    /**Original name: DFL-ACCPRE-SOST-CALC-NULL<br>*/
    public String getDflAccpreSostCalcNull() {
        return readString(Pos.DFL_ACCPRE_SOST_CALC_NULL, Len.DFL_ACCPRE_SOST_CALC_NULL);
    }

    public String getDflAccpreSostCalcNullFormatted() {
        return Functions.padBlanks(getDflAccpreSostCalcNull(), Len.DFL_ACCPRE_SOST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_SOST_CALC = 1;
        public static final int DFL_ACCPRE_SOST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_SOST_CALC = 8;
        public static final int DFL_ACCPRE_SOST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_SOST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_SOST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
