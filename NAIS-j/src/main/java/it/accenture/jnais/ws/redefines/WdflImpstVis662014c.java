package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-VIS-662014C<br>
 * Variable: WDFL-IMPST-VIS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstVis662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstVis662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_VIS662014C;
    }

    public void setWdflImpstVis662014c(AfDecimal wdflImpstVis662014c) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_VIS662014C, wdflImpstVis662014c.copy());
    }

    public void setWdflImpstVis662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_VIS662014C, Pos.WDFL_IMPST_VIS662014C);
    }

    /**Original name: WDFL-IMPST-VIS-662014C<br>*/
    public AfDecimal getWdflImpstVis662014c() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_VIS662014C, Len.Int.WDFL_IMPST_VIS662014C, Len.Fract.WDFL_IMPST_VIS662014C);
    }

    public byte[] getWdflImpstVis662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_VIS662014C, Pos.WDFL_IMPST_VIS662014C);
        return buffer;
    }

    public void setWdflImpstVis662014cNull(String wdflImpstVis662014cNull) {
        writeString(Pos.WDFL_IMPST_VIS662014C_NULL, wdflImpstVis662014cNull, Len.WDFL_IMPST_VIS662014C_NULL);
    }

    /**Original name: WDFL-IMPST-VIS-662014C-NULL<br>*/
    public String getWdflImpstVis662014cNull() {
        return readString(Pos.WDFL_IMPST_VIS662014C_NULL, Len.WDFL_IMPST_VIS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS662014C = 1;
        public static final int WDFL_IMPST_VIS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_VIS662014C = 8;
        public static final int WDFL_IMPST_VIS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_VIS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
