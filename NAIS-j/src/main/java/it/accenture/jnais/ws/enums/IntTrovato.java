package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: INT-TROVATO<br>
 * Variable: INT-TROVATO from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class IntTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setIntTrovato(char intTrovato) {
        this.value = intTrovato;
    }

    public char getIntTrovato() {
        return this.value;
    }

    public boolean isIntTrovatoSi() {
        return value == SI;
    }

    public void setIntTrovatoSi() {
        value = SI;
    }

    public boolean isIntTrovatoNo() {
        return value == NO;
    }

    public void setIntTrovatoNo() {
        value = NO;
    }
}
