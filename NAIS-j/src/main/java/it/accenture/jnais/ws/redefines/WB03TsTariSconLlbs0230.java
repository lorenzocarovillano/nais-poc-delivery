package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-TS-TARI-SCON<br>
 * Variable: W-B03-TS-TARI-SCON from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03TsTariSconLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03TsTariSconLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_TS_TARI_SCON;
    }

    public void setwB03TsTariScon(AfDecimal wB03TsTariScon) {
        writeDecimalAsPacked(Pos.W_B03_TS_TARI_SCON, wB03TsTariScon.copy());
    }

    /**Original name: W-B03-TS-TARI-SCON<br>*/
    public AfDecimal getwB03TsTariScon() {
        return readPackedAsDecimal(Pos.W_B03_TS_TARI_SCON, Len.Int.W_B03_TS_TARI_SCON, Len.Fract.W_B03_TS_TARI_SCON);
    }

    public byte[] getwB03TsTariSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_TS_TARI_SCON, Pos.W_B03_TS_TARI_SCON);
        return buffer;
    }

    public void setwB03TsTariSconNull(String wB03TsTariSconNull) {
        writeString(Pos.W_B03_TS_TARI_SCON_NULL, wB03TsTariSconNull, Len.W_B03_TS_TARI_SCON_NULL);
    }

    /**Original name: W-B03-TS-TARI-SCON-NULL<br>*/
    public String getwB03TsTariSconNull() {
        return readString(Pos.W_B03_TS_TARI_SCON_NULL, Len.W_B03_TS_TARI_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_TS_TARI_SCON = 1;
        public static final int W_B03_TS_TARI_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_TS_TARI_SCON = 8;
        public static final int W_B03_TS_TARI_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_TS_TARI_SCON = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_TS_TARI_SCON = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
