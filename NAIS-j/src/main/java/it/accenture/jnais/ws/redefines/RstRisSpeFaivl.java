package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-SPE-FAIVL<br>
 * Variable: RST-RIS-SPE-FAIVL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisSpeFaivl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisSpeFaivl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_SPE_FAIVL;
    }

    public void setRstRisSpeFaivl(AfDecimal rstRisSpeFaivl) {
        writeDecimalAsPacked(Pos.RST_RIS_SPE_FAIVL, rstRisSpeFaivl.copy());
    }

    public void setRstRisSpeFaivlFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_SPE_FAIVL, Pos.RST_RIS_SPE_FAIVL);
    }

    /**Original name: RST-RIS-SPE-FAIVL<br>*/
    public AfDecimal getRstRisSpeFaivl() {
        return readPackedAsDecimal(Pos.RST_RIS_SPE_FAIVL, Len.Int.RST_RIS_SPE_FAIVL, Len.Fract.RST_RIS_SPE_FAIVL);
    }

    public byte[] getRstRisSpeFaivlAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_SPE_FAIVL, Pos.RST_RIS_SPE_FAIVL);
        return buffer;
    }

    public void setRstRisSpeFaivlNull(String rstRisSpeFaivlNull) {
        writeString(Pos.RST_RIS_SPE_FAIVL_NULL, rstRisSpeFaivlNull, Len.RST_RIS_SPE_FAIVL_NULL);
    }

    /**Original name: RST-RIS-SPE-FAIVL-NULL<br>*/
    public String getRstRisSpeFaivlNull() {
        return readString(Pos.RST_RIS_SPE_FAIVL_NULL, Len.RST_RIS_SPE_FAIVL_NULL);
    }

    public String getRstRisSpeFaivlNullFormatted() {
        return Functions.padBlanks(getRstRisSpeFaivlNull(), Len.RST_RIS_SPE_FAIVL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_SPE_FAIVL = 1;
        public static final int RST_RIS_SPE_FAIVL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_SPE_FAIVL = 8;
        public static final int RST_RIS_SPE_FAIVL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_SPE_FAIVL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_SPE_FAIVL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
