package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-MIN-GARTO-T<br>
 * Variable: WB03-MIN-GARTO-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03MinGartoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03MinGartoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_MIN_GARTO_T;
    }

    public void setWb03MinGartoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_MIN_GARTO_T, Pos.WB03_MIN_GARTO_T);
    }

    /**Original name: WB03-MIN-GARTO-T<br>*/
    public AfDecimal getWb03MinGartoT() {
        return readPackedAsDecimal(Pos.WB03_MIN_GARTO_T, Len.Int.WB03_MIN_GARTO_T, Len.Fract.WB03_MIN_GARTO_T);
    }

    public byte[] getWb03MinGartoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_MIN_GARTO_T, Pos.WB03_MIN_GARTO_T);
        return buffer;
    }

    public void setWb03MinGartoTNull(String wb03MinGartoTNull) {
        writeString(Pos.WB03_MIN_GARTO_T_NULL, wb03MinGartoTNull, Len.WB03_MIN_GARTO_T_NULL);
    }

    /**Original name: WB03-MIN-GARTO-T-NULL<br>*/
    public String getWb03MinGartoTNull() {
        return readString(Pos.WB03_MIN_GARTO_T_NULL, Len.WB03_MIN_GARTO_T_NULL);
    }

    public String getWb03MinGartoTNullFormatted() {
        return Functions.padBlanks(getWb03MinGartoTNull(), Len.WB03_MIN_GARTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_MIN_GARTO_T = 1;
        public static final int WB03_MIN_GARTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_MIN_GARTO_T = 8;
        public static final int WB03_MIN_GARTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_MIN_GARTO_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_MIN_GARTO_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
