package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMPB-VIS-DA-REC<br>
 * Variable: ADE-IMPB-VIS-DA-REC from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpbVisDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpbVisDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMPB_VIS_DA_REC;
    }

    public void setAdeImpbVisDaRec(AfDecimal adeImpbVisDaRec) {
        writeDecimalAsPacked(Pos.ADE_IMPB_VIS_DA_REC, adeImpbVisDaRec.copy());
    }

    public void setAdeImpbVisDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMPB_VIS_DA_REC, Pos.ADE_IMPB_VIS_DA_REC);
    }

    /**Original name: ADE-IMPB-VIS-DA-REC<br>*/
    public AfDecimal getAdeImpbVisDaRec() {
        return readPackedAsDecimal(Pos.ADE_IMPB_VIS_DA_REC, Len.Int.ADE_IMPB_VIS_DA_REC, Len.Fract.ADE_IMPB_VIS_DA_REC);
    }

    public byte[] getAdeImpbVisDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMPB_VIS_DA_REC, Pos.ADE_IMPB_VIS_DA_REC);
        return buffer;
    }

    public void setAdeImpbVisDaRecNull(String adeImpbVisDaRecNull) {
        writeString(Pos.ADE_IMPB_VIS_DA_REC_NULL, adeImpbVisDaRecNull, Len.ADE_IMPB_VIS_DA_REC_NULL);
    }

    /**Original name: ADE-IMPB-VIS-DA-REC-NULL<br>*/
    public String getAdeImpbVisDaRecNull() {
        return readString(Pos.ADE_IMPB_VIS_DA_REC_NULL, Len.ADE_IMPB_VIS_DA_REC_NULL);
    }

    public String getAdeImpbVisDaRecNullFormatted() {
        return Functions.padBlanks(getAdeImpbVisDaRecNull(), Len.ADE_IMPB_VIS_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMPB_VIS_DA_REC = 1;
        public static final int ADE_IMPB_VIS_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMPB_VIS_DA_REC = 8;
        public static final int ADE_IMPB_VIS_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMPB_VIS_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMPB_VIS_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
