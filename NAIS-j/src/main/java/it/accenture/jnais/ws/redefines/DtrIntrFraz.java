package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-INTR-FRAZ<br>
 * Variable: DTR-INTR-FRAZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_INTR_FRAZ;
    }

    public void setDtrIntrFraz(AfDecimal dtrIntrFraz) {
        writeDecimalAsPacked(Pos.DTR_INTR_FRAZ, dtrIntrFraz.copy());
    }

    public void setDtrIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_INTR_FRAZ, Pos.DTR_INTR_FRAZ);
    }

    /**Original name: DTR-INTR-FRAZ<br>*/
    public AfDecimal getDtrIntrFraz() {
        return readPackedAsDecimal(Pos.DTR_INTR_FRAZ, Len.Int.DTR_INTR_FRAZ, Len.Fract.DTR_INTR_FRAZ);
    }

    public byte[] getDtrIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_INTR_FRAZ, Pos.DTR_INTR_FRAZ);
        return buffer;
    }

    public void setDtrIntrFrazNull(String dtrIntrFrazNull) {
        writeString(Pos.DTR_INTR_FRAZ_NULL, dtrIntrFrazNull, Len.DTR_INTR_FRAZ_NULL);
    }

    /**Original name: DTR-INTR-FRAZ-NULL<br>*/
    public String getDtrIntrFrazNull() {
        return readString(Pos.DTR_INTR_FRAZ_NULL, Len.DTR_INTR_FRAZ_NULL);
    }

    public String getDtrIntrFrazNullFormatted() {
        return Functions.padBlanks(getDtrIntrFrazNull(), Len.DTR_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_INTR_FRAZ = 1;
        public static final int DTR_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_INTR_FRAZ = 8;
        public static final int DTR_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
