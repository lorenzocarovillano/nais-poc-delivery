package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: OPEN-TYPE-FILESQS1<br>
 * Variable: OPEN-TYPE-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class OpenTypeFilesqs1 {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPEN_TYPE_FILESQS1);
    public static final String INPUT_FILESQS1 = "INP";
    public static final String OUTPUT_FILESQS1 = "OUT";
    public static final String I_O_FILESQS1 = "I-O";
    public static final String EXTEND_FILESQS1 = "EXT";

    //==== METHODS ====
    public void setOpenTypeFilesqsAll(String openTypeFilesqsAll) {
        this.value = Functions.subString(openTypeFilesqsAll, Len.OPEN_TYPE_FILESQS1);
    }

    public String getOpenTypeFilesqsAll() {
        return this.value;
    }

    public boolean isInputFilesqs1() {
        return value.equals(INPUT_FILESQS1);
    }

    public void setInputFilesqs5() {
        value = INPUT_FILESQS1;
    }

    public boolean isOutputFilesqs1() {
        return value.equals(OUTPUT_FILESQS1);
    }

    public void setOutputFilesqsAll() {
        value = OUTPUT_FILESQS1;
    }

    public boolean isiOFilesqs1() {
        return value.equals(I_O_FILESQS1);
    }

    public boolean isExtendFilesqs1() {
        return value.equals(EXTEND_FILESQS1);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPEN_TYPE_FILESQS1 = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
