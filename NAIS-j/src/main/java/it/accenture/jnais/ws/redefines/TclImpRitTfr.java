package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-RIT-TFR<br>
 * Variable: TCL-IMP-RIT-TFR from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpRitTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpRitTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_RIT_TFR;
    }

    public void setTclImpRitTfr(AfDecimal tclImpRitTfr) {
        writeDecimalAsPacked(Pos.TCL_IMP_RIT_TFR, tclImpRitTfr.copy());
    }

    public void setTclImpRitTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_RIT_TFR, Pos.TCL_IMP_RIT_TFR);
    }

    /**Original name: TCL-IMP-RIT-TFR<br>*/
    public AfDecimal getTclImpRitTfr() {
        return readPackedAsDecimal(Pos.TCL_IMP_RIT_TFR, Len.Int.TCL_IMP_RIT_TFR, Len.Fract.TCL_IMP_RIT_TFR);
    }

    public byte[] getTclImpRitTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_RIT_TFR, Pos.TCL_IMP_RIT_TFR);
        return buffer;
    }

    public void setTclImpRitTfrNull(String tclImpRitTfrNull) {
        writeString(Pos.TCL_IMP_RIT_TFR_NULL, tclImpRitTfrNull, Len.TCL_IMP_RIT_TFR_NULL);
    }

    /**Original name: TCL-IMP-RIT-TFR-NULL<br>*/
    public String getTclImpRitTfrNull() {
        return readString(Pos.TCL_IMP_RIT_TFR_NULL, Len.TCL_IMP_RIT_TFR_NULL);
    }

    public String getTclImpRitTfrNullFormatted() {
        return Functions.padBlanks(getTclImpRitTfrNull(), Len.TCL_IMP_RIT_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIT_TFR = 1;
        public static final int TCL_IMP_RIT_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RIT_TFR = 8;
        public static final int TCL_IMP_RIT_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIT_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RIT_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
