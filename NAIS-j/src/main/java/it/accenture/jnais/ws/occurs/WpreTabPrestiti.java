package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvpre1;
import it.accenture.jnais.copy.WpreDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WPRE-TAB-PRESTITI<br>
 * Variables: WPRE-TAB-PRESTITI from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpreTabPrestiti {

    //==== PROPERTIES ====
    //Original name: LCCVPRE1
    private Lccvpre1 lccvpre1 = new Lccvpre1();

    //==== METHODS ====
    public void setWpreTabPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpre1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpre1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpre1.Len.Int.ID_PTF, 0));
        position += Lccvpre1.Len.ID_PTF;
        lccvpre1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpreTabPrestitiBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpre1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpre1.getIdPtf(), Lccvpre1.Len.Int.ID_PTF, 0);
        position += Lccvpre1.Len.ID_PTF;
        lccvpre1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWpreTabPrestitiSpaces() {
        lccvpre1.initLccvpre1Spaces();
    }

    public Lccvpre1 getLccvpre1() {
        return lccvpre1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_TAB_PRESTITI = WpolStatus.Len.STATUS + Lccvpre1.Len.ID_PTF + WpreDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
