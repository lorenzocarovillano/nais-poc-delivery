package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPRE-TAB-AREA-RIASS<br>
 * Variables: WPRE-TAB-AREA-RIASS from copybook LOAC0560<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpreTabAreaRiass {

    //==== PROPERTIES ====
    //Original name: WPRE-COD-TRATTATI-ELA
    private String codTrattatiEla = DefaultValues.stringVal(Len.COD_TRATTATI_ELA);
    //Original name: WPRE-DES-TRATTATI-ELA
    private String desTrattatiEla = DefaultValues.stringVal(Len.DES_TRATTATI_ELA);

    //==== METHODS ====
    public void setTabAreaRiassBytes(byte[] buffer, int offset) {
        int position = offset;
        codTrattatiEla = MarshalByte.readString(buffer, position, Len.COD_TRATTATI_ELA);
        position += Len.COD_TRATTATI_ELA;
        desTrattatiEla = MarshalByte.readString(buffer, position, Len.DES_TRATTATI_ELA);
    }

    public void initTabAreaRiassSpaces() {
        codTrattatiEla = "";
        desTrattatiEla = "";
    }

    public void setCodTrattatiEla(String codTrattatiEla) {
        this.codTrattatiEla = Functions.subString(codTrattatiEla, Len.COD_TRATTATI_ELA);
    }

    public String getCodTrattatiEla() {
        return this.codTrattatiEla;
    }

    public void setDesTrattatiEla(String desTrattatiEla) {
        this.desTrattatiEla = Functions.subString(desTrattatiEla, Len.DES_TRATTATI_ELA);
    }

    public String getDesTrattatiEla() {
        return this.desTrattatiEla;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_TRATTATI_ELA = 12;
        public static final int DES_TRATTATI_ELA = 30;
        public static final int TAB_AREA_RIASS = COD_TRATTATI_ELA + DES_TRATTATI_ELA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
