package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RINN-COLL<br>
 * Variable: WPCO-DT-ULT-RINN-COLL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRinnColl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRinnColl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RINN_COLL;
    }

    public void setWpcoDtUltRinnColl(int wpcoDtUltRinnColl) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RINN_COLL, wpcoDtUltRinnColl, Len.Int.WPCO_DT_ULT_RINN_COLL);
    }

    public void setDpcoDtUltRinnCollFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RINN_COLL, Pos.WPCO_DT_ULT_RINN_COLL);
    }

    /**Original name: WPCO-DT-ULT-RINN-COLL<br>*/
    public int getWpcoDtUltRinnColl() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RINN_COLL, Len.Int.WPCO_DT_ULT_RINN_COLL);
    }

    public byte[] getWpcoDtUltRinnCollAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RINN_COLL, Pos.WPCO_DT_ULT_RINN_COLL);
        return buffer;
    }

    public void setWpcoDtUltRinnCollNull(String wpcoDtUltRinnCollNull) {
        writeString(Pos.WPCO_DT_ULT_RINN_COLL_NULL, wpcoDtUltRinnCollNull, Len.WPCO_DT_ULT_RINN_COLL_NULL);
    }

    /**Original name: WPCO-DT-ULT-RINN-COLL-NULL<br>*/
    public String getWpcoDtUltRinnCollNull() {
        return readString(Pos.WPCO_DT_ULT_RINN_COLL_NULL, Len.WPCO_DT_ULT_RINN_COLL_NULL);
    }

    public String getWpcoDtUltRinnCollNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRinnCollNull(), Len.WPCO_DT_ULT_RINN_COLL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RINN_COLL = 1;
        public static final int WPCO_DT_ULT_RINN_COLL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RINN_COLL = 5;
        public static final int WPCO_DT_ULT_RINN_COLL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RINN_COLL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
