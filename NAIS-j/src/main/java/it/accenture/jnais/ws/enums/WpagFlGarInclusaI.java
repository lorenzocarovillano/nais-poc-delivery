package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPAG-FL-GAR-INCLUSA-I<br>
 * Variable: WPAG-FL-GAR-INCLUSA-I from copybook LVEC0268<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpagFlGarInclusaI {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlGarInclusaI(char flGarInclusaI) {
        this.value = flGarInclusaI;
    }

    public char getFlGarInclusaI() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_GAR_INCLUSA_I = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
