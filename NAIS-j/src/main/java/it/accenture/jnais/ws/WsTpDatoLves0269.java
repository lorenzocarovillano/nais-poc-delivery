package it.accenture.jnais.ws;

/**Original name: WS-TP-DATO<br>
 * Variable: WS-TP-DATO from program LVES0269<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTpDatoLves0269 {

    //==== PROPERTIES ====
    //Original name: WS-TASSO
    private char tasso = 'A';
    //Original name: WS-IMPORTO
    private char importo = 'N';
    //Original name: WS-PERCENTUALE
    private char percentuale = 'P';
    //Original name: WS-MILLESIMI
    private char millesimi = 'M';
    //Original name: WS-DATA
    private char data2 = 'D';
    //Original name: WS-STRINGA
    private char stringa = 'S';
    //Original name: WS-NUMERO
    private char numero = 'I';
    //Original name: WS-FLAG
    private char flag = 'F';

    //==== METHODS ====
    public char getTasso() {
        return this.tasso;
    }

    public char getImporto() {
        return this.importo;
    }

    public char getPercentuale() {
        return this.percentuale;
    }

    public char getMillesimi() {
        return this.millesimi;
    }

    public char getData2() {
        return this.data2;
    }

    public char getStringa() {
        return this.stringa;
    }

    public char getNumero() {
        return this.numero;
    }

    public char getFlag() {
        return this.flag;
    }
}
