package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRSTZ-INI<br>
 * Variable: L3421-PRSTZ-INI from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PrstzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PrstzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRSTZ_INI;
    }

    public void setL3421PrstzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRSTZ_INI, Pos.L3421_PRSTZ_INI);
    }

    /**Original name: L3421-PRSTZ-INI<br>*/
    public AfDecimal getL3421PrstzIni() {
        return readPackedAsDecimal(Pos.L3421_PRSTZ_INI, Len.Int.L3421_PRSTZ_INI, Len.Fract.L3421_PRSTZ_INI);
    }

    public byte[] getL3421PrstzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRSTZ_INI, Pos.L3421_PRSTZ_INI);
        return buffer;
    }

    /**Original name: L3421-PRSTZ-INI-NULL<br>*/
    public String getL3421PrstzIniNull() {
        return readString(Pos.L3421_PRSTZ_INI_NULL, Len.L3421_PRSTZ_INI_NULL);
    }

    public String getL3421PrstzIniNullFormatted() {
        return Functions.padBlanks(getL3421PrstzIniNull(), Len.L3421_PRSTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRSTZ_INI = 1;
        public static final int L3421_PRSTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRSTZ_INI = 8;
        public static final int L3421_PRSTZ_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRSTZ_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRSTZ_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
