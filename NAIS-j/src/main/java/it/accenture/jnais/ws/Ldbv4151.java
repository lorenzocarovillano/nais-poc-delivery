package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbv4151DatiInput;

/**Original name: LDBV4151<br>
 * Variable: LDBV4151 from copybook LDBV4151<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv4151 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV4151-DATI-INPUT
    private Ldbv4151DatiInput datiInput = new Ldbv4151DatiInput();
    //Original name: LDBV4151-PRE-TOT
    private AfDecimal preTot = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV4151;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv4151Bytes(buf);
    }

    public void setLdbv4151Bytes(byte[] buffer) {
        setLdbv4151Bytes(buffer, 1);
    }

    public byte[] getLdbv4151Bytes() {
        byte[] buffer = new byte[Len.LDBV4151];
        return getLdbv4151Bytes(buffer, 1);
    }

    public void setLdbv4151Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.setDatiInputBytes(buffer, position);
        position += Ldbv4151DatiInput.Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getLdbv4151Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.getDatiInputBytes(buffer, position);
        position += Ldbv4151DatiInput.Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        preTot.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PRE_TOT, Len.Fract.PRE_TOT));
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, preTot.copy());
        return buffer;
    }

    public void setPreTot(AfDecimal preTot) {
        this.preTot.assign(preTot);
    }

    public AfDecimal getPreTot() {
        return this.preTot.copy();
    }

    public Ldbv4151DatiInput getDatiInput() {
        return datiInput;
    }

    @Override
    public byte[] serialize() {
        return getLdbv4151Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_TOT = 8;
        public static final int DATI_OUTPUT = PRE_TOT;
        public static final int LDBV4151 = Ldbv4151DatiInput.Len.DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
