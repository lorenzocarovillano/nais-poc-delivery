package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-RAT-FINANZ<br>
 * Variable: P67-IMP-RAT-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpRatFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpRatFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_RAT_FINANZ;
    }

    public void setP67ImpRatFinanz(AfDecimal p67ImpRatFinanz) {
        writeDecimalAsPacked(Pos.P67_IMP_RAT_FINANZ, p67ImpRatFinanz.copy());
    }

    public void setP67ImpRatFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_RAT_FINANZ, Pos.P67_IMP_RAT_FINANZ);
    }

    /**Original name: P67-IMP-RAT-FINANZ<br>*/
    public AfDecimal getP67ImpRatFinanz() {
        return readPackedAsDecimal(Pos.P67_IMP_RAT_FINANZ, Len.Int.P67_IMP_RAT_FINANZ, Len.Fract.P67_IMP_RAT_FINANZ);
    }

    public byte[] getP67ImpRatFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_RAT_FINANZ, Pos.P67_IMP_RAT_FINANZ);
        return buffer;
    }

    public void setP67ImpRatFinanzNull(String p67ImpRatFinanzNull) {
        writeString(Pos.P67_IMP_RAT_FINANZ_NULL, p67ImpRatFinanzNull, Len.P67_IMP_RAT_FINANZ_NULL);
    }

    /**Original name: P67-IMP-RAT-FINANZ-NULL<br>*/
    public String getP67ImpRatFinanzNull() {
        return readString(Pos.P67_IMP_RAT_FINANZ_NULL, Len.P67_IMP_RAT_FINANZ_NULL);
    }

    public String getP67ImpRatFinanzNullFormatted() {
        return Functions.padBlanks(getP67ImpRatFinanzNull(), Len.P67_IMP_RAT_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_RAT_FINANZ = 1;
        public static final int P67_IMP_RAT_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_RAT_FINANZ = 8;
        public static final int P67_IMP_RAT_FINANZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_RAT_FINANZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_RAT_FINANZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
