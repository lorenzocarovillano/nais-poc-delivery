package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IEAO9801-AREA<br>
 * Variable: IEAO9801-AREA from copybook IEAO9801<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ieao9801Area extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IEAO9801-DESC-ERRORE-ESTESA
    private String descErroreEstesa = DefaultValues.stringVal(Len.DESC_ERRORE_ESTESA);
    //Original name: IEAO9801-COD-ERRORE-990
    private String codErrore990 = DefaultValues.stringVal(Len.COD_ERRORE990);
    //Original name: IEAO9801-LABEL-ERR-990
    private String labelErr990 = DefaultValues.stringVal(Len.LABEL_ERR990);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IEAO9801_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIeao9801AreaBytes(buf);
    }

    public String getIeao9801AreaFormatted() {
        return MarshalByteExt.bufferToStr(getIeao9801AreaBytes());
    }

    public void setIeao9801AreaBytes(byte[] buffer) {
        setIeao9801AreaBytes(buffer, 1);
    }

    public byte[] getIeao9801AreaBytes() {
        byte[] buffer = new byte[Len.IEAO9801_AREA];
        return getIeao9801AreaBytes(buffer, 1);
    }

    public void setIeao9801AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        descErroreEstesa = MarshalByte.readString(buffer, position, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        codErrore990 = MarshalByte.readFixedString(buffer, position, Len.COD_ERRORE990);
        position += Len.COD_ERRORE990;
        labelErr990 = MarshalByte.readString(buffer, position, Len.LABEL_ERR990);
    }

    public byte[] getIeao9801AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, descErroreEstesa, Len.DESC_ERRORE_ESTESA);
        position += Len.DESC_ERRORE_ESTESA;
        MarshalByte.writeString(buffer, position, codErrore990, Len.COD_ERRORE990);
        position += Len.COD_ERRORE990;
        MarshalByte.writeString(buffer, position, labelErr990, Len.LABEL_ERR990);
        return buffer;
    }

    public void setDescErroreEstesa(String descErroreEstesa) {
        this.descErroreEstesa = Functions.subString(descErroreEstesa, Len.DESC_ERRORE_ESTESA);
    }

    public String getDescErroreEstesa() {
        return this.descErroreEstesa;
    }

    public void setCodErrore990(int codErrore990) {
        this.codErrore990 = NumericDisplay.asString(codErrore990, Len.COD_ERRORE990);
    }

    public int getCodErrore990() {
        return NumericDisplay.asInt(this.codErrore990);
    }

    public String getCodErrore990Formatted() {
        return this.codErrore990;
    }

    public void setLabelErr990(String labelErr990) {
        this.labelErr990 = Functions.subString(labelErr990, Len.LABEL_ERR990);
    }

    public String getLabelErr990() {
        return this.labelErr990;
    }

    @Override
    public byte[] serialize() {
        return getIeao9801AreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESC_ERRORE_ESTESA = 200;
        public static final int COD_ERRORE990 = 6;
        public static final int LABEL_ERR990 = 30;
        public static final int IEAO9801_AREA = DESC_ERRORE_ESTESA + COD_ERRORE990 + LABEL_ERR990;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
