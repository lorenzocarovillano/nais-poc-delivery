package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WL23-CPT-VINCTO-PIGN<br>
 * Variable: WL23-CPT-VINCTO-PIGN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23CptVinctoPign extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23CptVinctoPign() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_CPT_VINCTO_PIGN;
    }

    public void setWl23CptVinctoPign(AfDecimal wl23CptVinctoPign) {
        writeDecimalAsPacked(Pos.WL23_CPT_VINCTO_PIGN, wl23CptVinctoPign.copy());
    }

    public void setWl23CptVinctoPignFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_CPT_VINCTO_PIGN, Pos.WL23_CPT_VINCTO_PIGN);
    }

    /**Original name: WL23-CPT-VINCTO-PIGN<br>*/
    public AfDecimal getWl23CptVinctoPign() {
        return readPackedAsDecimal(Pos.WL23_CPT_VINCTO_PIGN, Len.Int.WL23_CPT_VINCTO_PIGN, Len.Fract.WL23_CPT_VINCTO_PIGN);
    }

    public byte[] getWl23CptVinctoPignAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_CPT_VINCTO_PIGN, Pos.WL23_CPT_VINCTO_PIGN);
        return buffer;
    }

    public void initWl23CptVinctoPignSpaces() {
        fill(Pos.WL23_CPT_VINCTO_PIGN, Len.WL23_CPT_VINCTO_PIGN, Types.SPACE_CHAR);
    }

    public void setWl23CptVinctoPignNull(String wl23CptVinctoPignNull) {
        writeString(Pos.WL23_CPT_VINCTO_PIGN_NULL, wl23CptVinctoPignNull, Len.WL23_CPT_VINCTO_PIGN_NULL);
    }

    /**Original name: WL23-CPT-VINCTO-PIGN-NULL<br>*/
    public String getWl23CptVinctoPignNull() {
        return readString(Pos.WL23_CPT_VINCTO_PIGN_NULL, Len.WL23_CPT_VINCTO_PIGN_NULL);
    }

    public String getWl23CptVinctoPignNullFormatted() {
        return Functions.padBlanks(getWl23CptVinctoPignNull(), Len.WL23_CPT_VINCTO_PIGN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_CPT_VINCTO_PIGN = 1;
        public static final int WL23_CPT_VINCTO_PIGN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_CPT_VINCTO_PIGN = 8;
        public static final int WL23_CPT_VINCTO_PIGN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WL23_CPT_VINCTO_PIGN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_CPT_VINCTO_PIGN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
