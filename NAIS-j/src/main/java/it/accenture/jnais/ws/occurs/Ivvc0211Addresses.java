package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;

/**Original name: IVVC0211-ADDRESSES<br>
 * Variables: IVVC0211-ADDRESSES from copybook IVVC0211<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0211Addresses {

    //==== PROPERTIES ====
    //Original name: IVVC0211-ADDRESS-TYPE
    private char type = DefaultValues.CHAR_VAL;
    //Original name: IVVC0211-ADDRESS
    private int s = DefaultValues.BIN_INT_VAL;

    //==== METHODS ====
    public void setAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        type = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        s = MarshalByte.readBinaryInt(buffer, position);
    }

    public byte[] getAddressesBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, type);
        position += Types.CHAR_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, s);
        return buffer;
    }

    public void initAddressesSpaces() {
        type = Types.SPACE_CHAR;
        s = Types.INVALID_BINARY_INT_VAL;
    }

    public void setType(char type) {
        this.type = type;
    }

    public char getType() {
        return this.type;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getS() {
        return this.s;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TYPE = 1;
        public static final int S = 4;
        public static final int ADDRESSES = TYPE + S;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
