package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-FRQ-MOVI<br>
 * Variable: DTR-FRQ-MOVI from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrFrqMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrFrqMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_FRQ_MOVI;
    }

    public void setDtrFrqMovi(int dtrFrqMovi) {
        writeIntAsPacked(Pos.DTR_FRQ_MOVI, dtrFrqMovi, Len.Int.DTR_FRQ_MOVI);
    }

    public void setDtrFrqMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_FRQ_MOVI, Pos.DTR_FRQ_MOVI);
    }

    /**Original name: DTR-FRQ-MOVI<br>*/
    public int getDtrFrqMovi() {
        return readPackedAsInt(Pos.DTR_FRQ_MOVI, Len.Int.DTR_FRQ_MOVI);
    }

    public byte[] getDtrFrqMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_FRQ_MOVI, Pos.DTR_FRQ_MOVI);
        return buffer;
    }

    public void setDtrFrqMoviNull(String dtrFrqMoviNull) {
        writeString(Pos.DTR_FRQ_MOVI_NULL, dtrFrqMoviNull, Len.DTR_FRQ_MOVI_NULL);
    }

    /**Original name: DTR-FRQ-MOVI-NULL<br>*/
    public String getDtrFrqMoviNull() {
        return readString(Pos.DTR_FRQ_MOVI_NULL, Len.DTR_FRQ_MOVI_NULL);
    }

    public String getDtrFrqMoviNullFormatted() {
        return Functions.padBlanks(getDtrFrqMoviNull(), Len.DTR_FRQ_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_FRQ_MOVI = 1;
        public static final int DTR_FRQ_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_FRQ_MOVI = 3;
        public static final int DTR_FRQ_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_FRQ_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
