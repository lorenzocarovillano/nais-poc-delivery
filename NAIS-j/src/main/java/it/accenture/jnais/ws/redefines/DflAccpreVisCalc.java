package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ACCPRE-VIS-CALC<br>
 * Variable: DFL-ACCPRE-VIS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAccpreVisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAccpreVisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ACCPRE_VIS_CALC;
    }

    public void setDflAccpreVisCalc(AfDecimal dflAccpreVisCalc) {
        writeDecimalAsPacked(Pos.DFL_ACCPRE_VIS_CALC, dflAccpreVisCalc.copy());
    }

    public void setDflAccpreVisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ACCPRE_VIS_CALC, Pos.DFL_ACCPRE_VIS_CALC);
    }

    /**Original name: DFL-ACCPRE-VIS-CALC<br>*/
    public AfDecimal getDflAccpreVisCalc() {
        return readPackedAsDecimal(Pos.DFL_ACCPRE_VIS_CALC, Len.Int.DFL_ACCPRE_VIS_CALC, Len.Fract.DFL_ACCPRE_VIS_CALC);
    }

    public byte[] getDflAccpreVisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ACCPRE_VIS_CALC, Pos.DFL_ACCPRE_VIS_CALC);
        return buffer;
    }

    public void setDflAccpreVisCalcNull(String dflAccpreVisCalcNull) {
        writeString(Pos.DFL_ACCPRE_VIS_CALC_NULL, dflAccpreVisCalcNull, Len.DFL_ACCPRE_VIS_CALC_NULL);
    }

    /**Original name: DFL-ACCPRE-VIS-CALC-NULL<br>*/
    public String getDflAccpreVisCalcNull() {
        return readString(Pos.DFL_ACCPRE_VIS_CALC_NULL, Len.DFL_ACCPRE_VIS_CALC_NULL);
    }

    public String getDflAccpreVisCalcNullFormatted() {
        return Functions.padBlanks(getDflAccpreVisCalcNull(), Len.DFL_ACCPRE_VIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_VIS_CALC = 1;
        public static final int DFL_ACCPRE_VIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ACCPRE_VIS_CALC = 8;
        public static final int DFL_ACCPRE_VIS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_VIS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ACCPRE_VIS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
