package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RES-PRSTZ-CALC<br>
 * Variable: DFL-RES-PRSTZ-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflResPrstzCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflResPrstzCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RES_PRSTZ_CALC;
    }

    public void setDflResPrstzCalc(AfDecimal dflResPrstzCalc) {
        writeDecimalAsPacked(Pos.DFL_RES_PRSTZ_CALC, dflResPrstzCalc.copy());
    }

    public void setDflResPrstzCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RES_PRSTZ_CALC, Pos.DFL_RES_PRSTZ_CALC);
    }

    /**Original name: DFL-RES-PRSTZ-CALC<br>*/
    public AfDecimal getDflResPrstzCalc() {
        return readPackedAsDecimal(Pos.DFL_RES_PRSTZ_CALC, Len.Int.DFL_RES_PRSTZ_CALC, Len.Fract.DFL_RES_PRSTZ_CALC);
    }

    public byte[] getDflResPrstzCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RES_PRSTZ_CALC, Pos.DFL_RES_PRSTZ_CALC);
        return buffer;
    }

    public void setDflResPrstzCalcNull(String dflResPrstzCalcNull) {
        writeString(Pos.DFL_RES_PRSTZ_CALC_NULL, dflResPrstzCalcNull, Len.DFL_RES_PRSTZ_CALC_NULL);
    }

    /**Original name: DFL-RES-PRSTZ-CALC-NULL<br>*/
    public String getDflResPrstzCalcNull() {
        return readString(Pos.DFL_RES_PRSTZ_CALC_NULL, Len.DFL_RES_PRSTZ_CALC_NULL);
    }

    public String getDflResPrstzCalcNullFormatted() {
        return Functions.padBlanks(getDflResPrstzCalcNull(), Len.DFL_RES_PRSTZ_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRSTZ_CALC = 1;
        public static final int DFL_RES_PRSTZ_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRSTZ_CALC = 8;
        public static final int DFL_RES_PRSTZ_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRSTZ_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRSTZ_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
