package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.L23CptVinctoPign;
import it.accenture.jnais.ws.redefines.L23DtAttivVinpg;
import it.accenture.jnais.ws.redefines.L23DtChiuVinpg;
import it.accenture.jnais.ws.redefines.L23DtNotificaBlocco;
import it.accenture.jnais.ws.redefines.L23DtProvvSeq;
import it.accenture.jnais.ws.redefines.L23IdMoviChiu;
import it.accenture.jnais.ws.redefines.L23NumProvvSeq;
import it.accenture.jnais.ws.redefines.L23SomPreVinpg;
import it.accenture.jnais.ws.redefines.L23ValRiscEndVinpg;
import it.accenture.jnais.ws.redefines.L23ValRiscIniVinpg;

/**Original name: VINC-PEG<br>
 * Variable: VINC-PEG from copybook IDBVL231<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class VincPegIdbsl230 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: L23-ID-VINC-PEG
    private int l23IdVincPeg = DefaultValues.INT_VAL;
    //Original name: L23-ID-MOVI-CRZ
    private int l23IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: L23-ID-MOVI-CHIU
    private L23IdMoviChiu l23IdMoviChiu = new L23IdMoviChiu();
    //Original name: L23-DT-INI-EFF
    private int l23DtIniEff = DefaultValues.INT_VAL;
    //Original name: L23-DT-END-EFF
    private int l23DtEndEff = DefaultValues.INT_VAL;
    //Original name: L23-COD-COMP-ANIA
    private int l23CodCompAnia = DefaultValues.INT_VAL;
    //Original name: L23-ID-RAPP-ANA
    private int l23IdRappAna = DefaultValues.INT_VAL;
    //Original name: L23-TP-VINC
    private String l23TpVinc = DefaultValues.stringVal(Len.L23_TP_VINC);
    //Original name: L23-FL-DELEGA-AL-RISC
    private char l23FlDelegaAlRisc = DefaultValues.CHAR_VAL;
    //Original name: L23-DESC-LEN
    private short l23DescLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: L23-DESC
    private String l23Desc = DefaultValues.stringVal(Len.L23_DESC);
    //Original name: L23-DT-ATTIV-VINPG
    private L23DtAttivVinpg l23DtAttivVinpg = new L23DtAttivVinpg();
    //Original name: L23-CPT-VINCTO-PIGN
    private L23CptVinctoPign l23CptVinctoPign = new L23CptVinctoPign();
    //Original name: L23-DT-CHIU-VINPG
    private L23DtChiuVinpg l23DtChiuVinpg = new L23DtChiuVinpg();
    //Original name: L23-VAL-RISC-INI-VINPG
    private L23ValRiscIniVinpg l23ValRiscIniVinpg = new L23ValRiscIniVinpg();
    //Original name: L23-VAL-RISC-END-VINPG
    private L23ValRiscEndVinpg l23ValRiscEndVinpg = new L23ValRiscEndVinpg();
    //Original name: L23-SOM-PRE-VINPG
    private L23SomPreVinpg l23SomPreVinpg = new L23SomPreVinpg();
    //Original name: L23-FL-VINPG-INT-PRSTZ
    private char l23FlVinpgIntPrstz = DefaultValues.CHAR_VAL;
    //Original name: L23-DESC-AGG-VINC-LEN
    private short l23DescAggVincLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: L23-DESC-AGG-VINC
    private String l23DescAggVinc = DefaultValues.stringVal(Len.L23_DESC_AGG_VINC);
    //Original name: L23-DS-RIGA
    private long l23DsRiga = DefaultValues.LONG_VAL;
    //Original name: L23-DS-OPER-SQL
    private char l23DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: L23-DS-VER
    private int l23DsVer = DefaultValues.INT_VAL;
    //Original name: L23-DS-TS-INI-CPTZ
    private long l23DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: L23-DS-TS-END-CPTZ
    private long l23DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: L23-DS-UTENTE
    private String l23DsUtente = DefaultValues.stringVal(Len.L23_DS_UTENTE);
    //Original name: L23-DS-STATO-ELAB
    private char l23DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: L23-TP-AUT-SEQ
    private String l23TpAutSeq = DefaultValues.stringVal(Len.L23_TP_AUT_SEQ);
    //Original name: L23-COD-UFF-SEQ
    private String l23CodUffSeq = DefaultValues.stringVal(Len.L23_COD_UFF_SEQ);
    //Original name: L23-NUM-PROVV-SEQ
    private L23NumProvvSeq l23NumProvvSeq = new L23NumProvvSeq();
    //Original name: L23-TP-PROVV-SEQ
    private String l23TpProvvSeq = DefaultValues.stringVal(Len.L23_TP_PROVV_SEQ);
    //Original name: L23-DT-PROVV-SEQ
    private L23DtProvvSeq l23DtProvvSeq = new L23DtProvvSeq();
    //Original name: L23-DT-NOTIFICA-BLOCCO
    private L23DtNotificaBlocco l23DtNotificaBlocco = new L23DtNotificaBlocco();
    //Original name: L23-NOTA-PROVV-LEN
    private short l23NotaProvvLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: L23-NOTA-PROVV
    private String l23NotaProvv = DefaultValues.stringVal(Len.L23_NOTA_PROVV);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VINC_PEG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setVincPegBytes(buf);
    }

    public void setVincPegFormatted(String data) {
        byte[] buffer = new byte[Len.VINC_PEG];
        MarshalByte.writeString(buffer, 1, data, Len.VINC_PEG);
        setVincPegBytes(buffer, 1);
    }

    public String getVincPegFormatted() {
        return MarshalByteExt.bufferToStr(getVincPegBytes());
    }

    public void setVincPegBytes(byte[] buffer) {
        setVincPegBytes(buffer, 1);
    }

    public byte[] getVincPegBytes() {
        byte[] buffer = new byte[Len.VINC_PEG];
        return getVincPegBytes(buffer, 1);
    }

    public void setVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        l23IdVincPeg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_ID_VINC_PEG, 0);
        position += Len.L23_ID_VINC_PEG;
        l23IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_ID_MOVI_CRZ, 0);
        position += Len.L23_ID_MOVI_CRZ;
        l23IdMoviChiu.setL23IdMoviChiuFromBuffer(buffer, position);
        position += L23IdMoviChiu.Len.L23_ID_MOVI_CHIU;
        l23DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_DT_INI_EFF, 0);
        position += Len.L23_DT_INI_EFF;
        l23DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_DT_END_EFF, 0);
        position += Len.L23_DT_END_EFF;
        l23CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_COD_COMP_ANIA, 0);
        position += Len.L23_COD_COMP_ANIA;
        l23IdRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_ID_RAPP_ANA, 0);
        position += Len.L23_ID_RAPP_ANA;
        l23TpVinc = MarshalByte.readString(buffer, position, Len.L23_TP_VINC);
        position += Len.L23_TP_VINC;
        l23FlDelegaAlRisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setL23DescVcharBytes(buffer, position);
        position += Len.L23_DESC_VCHAR;
        l23DtAttivVinpg.setL23DtAttivVinpgFromBuffer(buffer, position);
        position += L23DtAttivVinpg.Len.L23_DT_ATTIV_VINPG;
        l23CptVinctoPign.setL23CptVinctoPignFromBuffer(buffer, position);
        position += L23CptVinctoPign.Len.L23_CPT_VINCTO_PIGN;
        l23DtChiuVinpg.setL23DtChiuVinpgFromBuffer(buffer, position);
        position += L23DtChiuVinpg.Len.L23_DT_CHIU_VINPG;
        l23ValRiscIniVinpg.setL23ValRiscIniVinpgFromBuffer(buffer, position);
        position += L23ValRiscIniVinpg.Len.L23_VAL_RISC_INI_VINPG;
        l23ValRiscEndVinpg.setL23ValRiscEndVinpgFromBuffer(buffer, position);
        position += L23ValRiscEndVinpg.Len.L23_VAL_RISC_END_VINPG;
        l23SomPreVinpg.setL23SomPreVinpgFromBuffer(buffer, position);
        position += L23SomPreVinpg.Len.L23_SOM_PRE_VINPG;
        l23FlVinpgIntPrstz = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setL23DescAggVincVcharBytes(buffer, position);
        position += Len.L23_DESC_AGG_VINC_VCHAR;
        l23DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L23_DS_RIGA, 0);
        position += Len.L23_DS_RIGA;
        l23DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l23DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.L23_DS_VER, 0);
        position += Len.L23_DS_VER;
        l23DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L23_DS_TS_INI_CPTZ, 0);
        position += Len.L23_DS_TS_INI_CPTZ;
        l23DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.L23_DS_TS_END_CPTZ, 0);
        position += Len.L23_DS_TS_END_CPTZ;
        l23DsUtente = MarshalByte.readString(buffer, position, Len.L23_DS_UTENTE);
        position += Len.L23_DS_UTENTE;
        l23DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        l23TpAutSeq = MarshalByte.readString(buffer, position, Len.L23_TP_AUT_SEQ);
        position += Len.L23_TP_AUT_SEQ;
        l23CodUffSeq = MarshalByte.readString(buffer, position, Len.L23_COD_UFF_SEQ);
        position += Len.L23_COD_UFF_SEQ;
        l23NumProvvSeq.setL23NumProvvSeqFromBuffer(buffer, position);
        position += L23NumProvvSeq.Len.L23_NUM_PROVV_SEQ;
        l23TpProvvSeq = MarshalByte.readString(buffer, position, Len.L23_TP_PROVV_SEQ);
        position += Len.L23_TP_PROVV_SEQ;
        l23DtProvvSeq.setL23DtProvvSeqFromBuffer(buffer, position);
        position += L23DtProvvSeq.Len.L23_DT_PROVV_SEQ;
        l23DtNotificaBlocco.setL23DtNotificaBloccoFromBuffer(buffer, position);
        position += L23DtNotificaBlocco.Len.L23_DT_NOTIFICA_BLOCCO;
        setL23NotaProvvVcharBytes(buffer, position);
    }

    public byte[] getVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, l23IdVincPeg, Len.Int.L23_ID_VINC_PEG, 0);
        position += Len.L23_ID_VINC_PEG;
        MarshalByte.writeIntAsPacked(buffer, position, l23IdMoviCrz, Len.Int.L23_ID_MOVI_CRZ, 0);
        position += Len.L23_ID_MOVI_CRZ;
        l23IdMoviChiu.getL23IdMoviChiuAsBuffer(buffer, position);
        position += L23IdMoviChiu.Len.L23_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, l23DtIniEff, Len.Int.L23_DT_INI_EFF, 0);
        position += Len.L23_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, l23DtEndEff, Len.Int.L23_DT_END_EFF, 0);
        position += Len.L23_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, l23CodCompAnia, Len.Int.L23_COD_COMP_ANIA, 0);
        position += Len.L23_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, l23IdRappAna, Len.Int.L23_ID_RAPP_ANA, 0);
        position += Len.L23_ID_RAPP_ANA;
        MarshalByte.writeString(buffer, position, l23TpVinc, Len.L23_TP_VINC);
        position += Len.L23_TP_VINC;
        MarshalByte.writeChar(buffer, position, l23FlDelegaAlRisc);
        position += Types.CHAR_SIZE;
        getL23DescVcharBytes(buffer, position);
        position += Len.L23_DESC_VCHAR;
        l23DtAttivVinpg.getL23DtAttivVinpgAsBuffer(buffer, position);
        position += L23DtAttivVinpg.Len.L23_DT_ATTIV_VINPG;
        l23CptVinctoPign.getL23CptVinctoPignAsBuffer(buffer, position);
        position += L23CptVinctoPign.Len.L23_CPT_VINCTO_PIGN;
        l23DtChiuVinpg.getL23DtChiuVinpgAsBuffer(buffer, position);
        position += L23DtChiuVinpg.Len.L23_DT_CHIU_VINPG;
        l23ValRiscIniVinpg.getL23ValRiscIniVinpgAsBuffer(buffer, position);
        position += L23ValRiscIniVinpg.Len.L23_VAL_RISC_INI_VINPG;
        l23ValRiscEndVinpg.getL23ValRiscEndVinpgAsBuffer(buffer, position);
        position += L23ValRiscEndVinpg.Len.L23_VAL_RISC_END_VINPG;
        l23SomPreVinpg.getL23SomPreVinpgAsBuffer(buffer, position);
        position += L23SomPreVinpg.Len.L23_SOM_PRE_VINPG;
        MarshalByte.writeChar(buffer, position, l23FlVinpgIntPrstz);
        position += Types.CHAR_SIZE;
        getL23DescAggVincVcharBytes(buffer, position);
        position += Len.L23_DESC_AGG_VINC_VCHAR;
        MarshalByte.writeLongAsPacked(buffer, position, l23DsRiga, Len.Int.L23_DS_RIGA, 0);
        position += Len.L23_DS_RIGA;
        MarshalByte.writeChar(buffer, position, l23DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, l23DsVer, Len.Int.L23_DS_VER, 0);
        position += Len.L23_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, l23DsTsIniCptz, Len.Int.L23_DS_TS_INI_CPTZ, 0);
        position += Len.L23_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, l23DsTsEndCptz, Len.Int.L23_DS_TS_END_CPTZ, 0);
        position += Len.L23_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, l23DsUtente, Len.L23_DS_UTENTE);
        position += Len.L23_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, l23DsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, l23TpAutSeq, Len.L23_TP_AUT_SEQ);
        position += Len.L23_TP_AUT_SEQ;
        MarshalByte.writeString(buffer, position, l23CodUffSeq, Len.L23_COD_UFF_SEQ);
        position += Len.L23_COD_UFF_SEQ;
        l23NumProvvSeq.getL23NumProvvSeqAsBuffer(buffer, position);
        position += L23NumProvvSeq.Len.L23_NUM_PROVV_SEQ;
        MarshalByte.writeString(buffer, position, l23TpProvvSeq, Len.L23_TP_PROVV_SEQ);
        position += Len.L23_TP_PROVV_SEQ;
        l23DtProvvSeq.getL23DtProvvSeqAsBuffer(buffer, position);
        position += L23DtProvvSeq.Len.L23_DT_PROVV_SEQ;
        l23DtNotificaBlocco.getL23DtNotificaBloccoAsBuffer(buffer, position);
        position += L23DtNotificaBlocco.Len.L23_DT_NOTIFICA_BLOCCO;
        getL23NotaProvvVcharBytes(buffer, position);
        return buffer;
    }

    public void setL23IdVincPeg(int l23IdVincPeg) {
        this.l23IdVincPeg = l23IdVincPeg;
    }

    public int getL23IdVincPeg() {
        return this.l23IdVincPeg;
    }

    public void setL23IdMoviCrz(int l23IdMoviCrz) {
        this.l23IdMoviCrz = l23IdMoviCrz;
    }

    public int getL23IdMoviCrz() {
        return this.l23IdMoviCrz;
    }

    public void setL23DtIniEff(int l23DtIniEff) {
        this.l23DtIniEff = l23DtIniEff;
    }

    public int getL23DtIniEff() {
        return this.l23DtIniEff;
    }

    public void setL23DtEndEff(int l23DtEndEff) {
        this.l23DtEndEff = l23DtEndEff;
    }

    public int getL23DtEndEff() {
        return this.l23DtEndEff;
    }

    public void setL23CodCompAnia(int l23CodCompAnia) {
        this.l23CodCompAnia = l23CodCompAnia;
    }

    public int getL23CodCompAnia() {
        return this.l23CodCompAnia;
    }

    public void setL23IdRappAna(int l23IdRappAna) {
        this.l23IdRappAna = l23IdRappAna;
    }

    public int getL23IdRappAna() {
        return this.l23IdRappAna;
    }

    public void setL23TpVinc(String l23TpVinc) {
        this.l23TpVinc = Functions.subString(l23TpVinc, Len.L23_TP_VINC);
    }

    public String getL23TpVinc() {
        return this.l23TpVinc;
    }

    public String getL23TpVincFormatted() {
        return Functions.padBlanks(getL23TpVinc(), Len.L23_TP_VINC);
    }

    public void setL23FlDelegaAlRisc(char l23FlDelegaAlRisc) {
        this.l23FlDelegaAlRisc = l23FlDelegaAlRisc;
    }

    public char getL23FlDelegaAlRisc() {
        return this.l23FlDelegaAlRisc;
    }

    public void setL23DescVcharFormatted(String data) {
        byte[] buffer = new byte[Len.L23_DESC_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.L23_DESC_VCHAR);
        setL23DescVcharBytes(buffer, 1);
    }

    public String getL23DescVcharFormatted() {
        return MarshalByteExt.bufferToStr(getL23DescVcharBytes());
    }

    /**Original name: L23-DESC-VCHAR<br>*/
    public byte[] getL23DescVcharBytes() {
        byte[] buffer = new byte[Len.L23_DESC_VCHAR];
        return getL23DescVcharBytes(buffer, 1);
    }

    public void setL23DescVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        l23DescLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        l23Desc = MarshalByte.readString(buffer, position, Len.L23_DESC);
    }

    public byte[] getL23DescVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, l23DescLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, l23Desc, Len.L23_DESC);
        return buffer;
    }

    public void setL23DescLen(short l23DescLen) {
        this.l23DescLen = l23DescLen;
    }

    public short getL23DescLen() {
        return this.l23DescLen;
    }

    public void setL23Desc(String l23Desc) {
        this.l23Desc = Functions.subString(l23Desc, Len.L23_DESC);
    }

    public String getL23Desc() {
        return this.l23Desc;
    }

    public void setL23FlVinpgIntPrstz(char l23FlVinpgIntPrstz) {
        this.l23FlVinpgIntPrstz = l23FlVinpgIntPrstz;
    }

    public char getL23FlVinpgIntPrstz() {
        return this.l23FlVinpgIntPrstz;
    }

    public void setL23DescAggVincVcharFormatted(String data) {
        byte[] buffer = new byte[Len.L23_DESC_AGG_VINC_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.L23_DESC_AGG_VINC_VCHAR);
        setL23DescAggVincVcharBytes(buffer, 1);
    }

    public String getL23DescAggVincVcharFormatted() {
        return MarshalByteExt.bufferToStr(getL23DescAggVincVcharBytes());
    }

    /**Original name: L23-DESC-AGG-VINC-VCHAR<br>*/
    public byte[] getL23DescAggVincVcharBytes() {
        byte[] buffer = new byte[Len.L23_DESC_AGG_VINC_VCHAR];
        return getL23DescAggVincVcharBytes(buffer, 1);
    }

    public void setL23DescAggVincVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        l23DescAggVincLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        l23DescAggVinc = MarshalByte.readString(buffer, position, Len.L23_DESC_AGG_VINC);
    }

    public byte[] getL23DescAggVincVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, l23DescAggVincLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, l23DescAggVinc, Len.L23_DESC_AGG_VINC);
        return buffer;
    }

    public void setL23DescAggVincLen(short l23DescAggVincLen) {
        this.l23DescAggVincLen = l23DescAggVincLen;
    }

    public short getL23DescAggVincLen() {
        return this.l23DescAggVincLen;
    }

    public void setL23DescAggVinc(String l23DescAggVinc) {
        this.l23DescAggVinc = Functions.subString(l23DescAggVinc, Len.L23_DESC_AGG_VINC);
    }

    public String getL23DescAggVinc() {
        return this.l23DescAggVinc;
    }

    public void setL23DsRiga(long l23DsRiga) {
        this.l23DsRiga = l23DsRiga;
    }

    public long getL23DsRiga() {
        return this.l23DsRiga;
    }

    public void setL23DsOperSql(char l23DsOperSql) {
        this.l23DsOperSql = l23DsOperSql;
    }

    public void setL23DsOperSqlFormatted(String l23DsOperSql) {
        setL23DsOperSql(Functions.charAt(l23DsOperSql, Types.CHAR_SIZE));
    }

    public char getL23DsOperSql() {
        return this.l23DsOperSql;
    }

    public void setL23DsVer(int l23DsVer) {
        this.l23DsVer = l23DsVer;
    }

    public int getL23DsVer() {
        return this.l23DsVer;
    }

    public void setL23DsTsIniCptz(long l23DsTsIniCptz) {
        this.l23DsTsIniCptz = l23DsTsIniCptz;
    }

    public long getL23DsTsIniCptz() {
        return this.l23DsTsIniCptz;
    }

    public void setL23DsTsEndCptz(long l23DsTsEndCptz) {
        this.l23DsTsEndCptz = l23DsTsEndCptz;
    }

    public long getL23DsTsEndCptz() {
        return this.l23DsTsEndCptz;
    }

    public void setL23DsUtente(String l23DsUtente) {
        this.l23DsUtente = Functions.subString(l23DsUtente, Len.L23_DS_UTENTE);
    }

    public String getL23DsUtente() {
        return this.l23DsUtente;
    }

    public void setL23DsStatoElab(char l23DsStatoElab) {
        this.l23DsStatoElab = l23DsStatoElab;
    }

    public void setL23DsStatoElabFormatted(String l23DsStatoElab) {
        setL23DsStatoElab(Functions.charAt(l23DsStatoElab, Types.CHAR_SIZE));
    }

    public char getL23DsStatoElab() {
        return this.l23DsStatoElab;
    }

    public void setL23TpAutSeq(String l23TpAutSeq) {
        this.l23TpAutSeq = Functions.subString(l23TpAutSeq, Len.L23_TP_AUT_SEQ);
    }

    public String getL23TpAutSeq() {
        return this.l23TpAutSeq;
    }

    public String getL23TpAutSeqFormatted() {
        return Functions.padBlanks(getL23TpAutSeq(), Len.L23_TP_AUT_SEQ);
    }

    public void setL23CodUffSeq(String l23CodUffSeq) {
        this.l23CodUffSeq = Functions.subString(l23CodUffSeq, Len.L23_COD_UFF_SEQ);
    }

    public String getL23CodUffSeq() {
        return this.l23CodUffSeq;
    }

    public String getL23CodUffSeqFormatted() {
        return Functions.padBlanks(getL23CodUffSeq(), Len.L23_COD_UFF_SEQ);
    }

    public void setL23TpProvvSeq(String l23TpProvvSeq) {
        this.l23TpProvvSeq = Functions.subString(l23TpProvvSeq, Len.L23_TP_PROVV_SEQ);
    }

    public String getL23TpProvvSeq() {
        return this.l23TpProvvSeq;
    }

    public String getL23TpProvvSeqFormatted() {
        return Functions.padBlanks(getL23TpProvvSeq(), Len.L23_TP_PROVV_SEQ);
    }

    public void setL23NotaProvvVcharFormatted(String data) {
        byte[] buffer = new byte[Len.L23_NOTA_PROVV_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.L23_NOTA_PROVV_VCHAR);
        setL23NotaProvvVcharBytes(buffer, 1);
    }

    public String getL23NotaProvvVcharFormatted() {
        return MarshalByteExt.bufferToStr(getL23NotaProvvVcharBytes());
    }

    /**Original name: L23-NOTA-PROVV-VCHAR<br>*/
    public byte[] getL23NotaProvvVcharBytes() {
        byte[] buffer = new byte[Len.L23_NOTA_PROVV_VCHAR];
        return getL23NotaProvvVcharBytes(buffer, 1);
    }

    public void setL23NotaProvvVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        l23NotaProvvLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        l23NotaProvv = MarshalByte.readString(buffer, position, Len.L23_NOTA_PROVV);
    }

    public byte[] getL23NotaProvvVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, l23NotaProvvLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, l23NotaProvv, Len.L23_NOTA_PROVV);
        return buffer;
    }

    public void setL23NotaProvvLen(short l23NotaProvvLen) {
        this.l23NotaProvvLen = l23NotaProvvLen;
    }

    public short getL23NotaProvvLen() {
        return this.l23NotaProvvLen;
    }

    public void setL23NotaProvv(String l23NotaProvv) {
        this.l23NotaProvv = Functions.subString(l23NotaProvv, Len.L23_NOTA_PROVV);
    }

    public String getL23NotaProvv() {
        return this.l23NotaProvv;
    }

    public L23CptVinctoPign getL23CptVinctoPign() {
        return l23CptVinctoPign;
    }

    public L23DtAttivVinpg getL23DtAttivVinpg() {
        return l23DtAttivVinpg;
    }

    public L23DtChiuVinpg getL23DtChiuVinpg() {
        return l23DtChiuVinpg;
    }

    public L23DtNotificaBlocco getL23DtNotificaBlocco() {
        return l23DtNotificaBlocco;
    }

    public L23DtProvvSeq getL23DtProvvSeq() {
        return l23DtProvvSeq;
    }

    public L23IdMoviChiu getL23IdMoviChiu() {
        return l23IdMoviChiu;
    }

    public L23NumProvvSeq getL23NumProvvSeq() {
        return l23NumProvvSeq;
    }

    public L23SomPreVinpg getL23SomPreVinpg() {
        return l23SomPreVinpg;
    }

    public L23ValRiscEndVinpg getL23ValRiscEndVinpg() {
        return l23ValRiscEndVinpg;
    }

    public L23ValRiscIniVinpg getL23ValRiscIniVinpg() {
        return l23ValRiscIniVinpg;
    }

    @Override
    public byte[] serialize() {
        return getVincPegBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_ID_VINC_PEG = 5;
        public static final int L23_ID_MOVI_CRZ = 5;
        public static final int L23_DT_INI_EFF = 5;
        public static final int L23_DT_END_EFF = 5;
        public static final int L23_COD_COMP_ANIA = 3;
        public static final int L23_ID_RAPP_ANA = 5;
        public static final int L23_TP_VINC = 2;
        public static final int L23_FL_DELEGA_AL_RISC = 1;
        public static final int L23_DESC_LEN = 2;
        public static final int L23_DESC = 1000;
        public static final int L23_DESC_VCHAR = L23_DESC_LEN + L23_DESC;
        public static final int L23_FL_VINPG_INT_PRSTZ = 1;
        public static final int L23_DESC_AGG_VINC_LEN = 2;
        public static final int L23_DESC_AGG_VINC = 100;
        public static final int L23_DESC_AGG_VINC_VCHAR = L23_DESC_AGG_VINC_LEN + L23_DESC_AGG_VINC;
        public static final int L23_DS_RIGA = 6;
        public static final int L23_DS_OPER_SQL = 1;
        public static final int L23_DS_VER = 5;
        public static final int L23_DS_TS_INI_CPTZ = 10;
        public static final int L23_DS_TS_END_CPTZ = 10;
        public static final int L23_DS_UTENTE = 20;
        public static final int L23_DS_STATO_ELAB = 1;
        public static final int L23_TP_AUT_SEQ = 2;
        public static final int L23_COD_UFF_SEQ = 11;
        public static final int L23_TP_PROVV_SEQ = 2;
        public static final int L23_NOTA_PROVV_LEN = 2;
        public static final int L23_NOTA_PROVV = 300;
        public static final int L23_NOTA_PROVV_VCHAR = L23_NOTA_PROVV_LEN + L23_NOTA_PROVV;
        public static final int VINC_PEG = L23_ID_VINC_PEG + L23_ID_MOVI_CRZ + L23IdMoviChiu.Len.L23_ID_MOVI_CHIU + L23_DT_INI_EFF + L23_DT_END_EFF + L23_COD_COMP_ANIA + L23_ID_RAPP_ANA + L23_TP_VINC + L23_FL_DELEGA_AL_RISC + L23_DESC_VCHAR + L23DtAttivVinpg.Len.L23_DT_ATTIV_VINPG + L23CptVinctoPign.Len.L23_CPT_VINCTO_PIGN + L23DtChiuVinpg.Len.L23_DT_CHIU_VINPG + L23ValRiscIniVinpg.Len.L23_VAL_RISC_INI_VINPG + L23ValRiscEndVinpg.Len.L23_VAL_RISC_END_VINPG + L23SomPreVinpg.Len.L23_SOM_PRE_VINPG + L23_FL_VINPG_INT_PRSTZ + L23_DESC_AGG_VINC_VCHAR + L23_DS_RIGA + L23_DS_OPER_SQL + L23_DS_VER + L23_DS_TS_INI_CPTZ + L23_DS_TS_END_CPTZ + L23_DS_UTENTE + L23_DS_STATO_ELAB + L23_TP_AUT_SEQ + L23_COD_UFF_SEQ + L23NumProvvSeq.Len.L23_NUM_PROVV_SEQ + L23_TP_PROVV_SEQ + L23DtProvvSeq.Len.L23_DT_PROVV_SEQ + L23DtNotificaBlocco.Len.L23_DT_NOTIFICA_BLOCCO + L23_NOTA_PROVV_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_ID_VINC_PEG = 9;
            public static final int L23_ID_MOVI_CRZ = 9;
            public static final int L23_DT_INI_EFF = 8;
            public static final int L23_DT_END_EFF = 8;
            public static final int L23_COD_COMP_ANIA = 5;
            public static final int L23_ID_RAPP_ANA = 9;
            public static final int L23_DS_RIGA = 10;
            public static final int L23_DS_VER = 9;
            public static final int L23_DS_TS_INI_CPTZ = 18;
            public static final int L23_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
