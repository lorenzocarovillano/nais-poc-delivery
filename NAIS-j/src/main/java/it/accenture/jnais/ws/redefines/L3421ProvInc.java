package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PROV-INC<br>
 * Variable: L3421-PROV-INC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PROV_INC;
    }

    public void setL3421ProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PROV_INC, Pos.L3421_PROV_INC);
    }

    /**Original name: L3421-PROV-INC<br>*/
    public AfDecimal getL3421ProvInc() {
        return readPackedAsDecimal(Pos.L3421_PROV_INC, Len.Int.L3421_PROV_INC, Len.Fract.L3421_PROV_INC);
    }

    public byte[] getL3421ProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PROV_INC, Pos.L3421_PROV_INC);
        return buffer;
    }

    /**Original name: L3421-PROV-INC-NULL<br>*/
    public String getL3421ProvIncNull() {
        return readString(Pos.L3421_PROV_INC_NULL, Len.L3421_PROV_INC_NULL);
    }

    public String getL3421ProvIncNullFormatted() {
        return Functions.padBlanks(getL3421ProvIncNull(), Len.L3421_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PROV_INC = 1;
        public static final int L3421_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PROV_INC = 8;
        public static final int L3421_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
