package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPOL-ID-ACC-COMM<br>
 * Variable: WPOL-ID-ACC-COMM from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolIdAccComm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolIdAccComm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_ID_ACC_COMM;
    }

    public void setWpolIdAccComm(int wpolIdAccComm) {
        writeIntAsPacked(Pos.WPOL_ID_ACC_COMM, wpolIdAccComm, Len.Int.WPOL_ID_ACC_COMM);
    }

    public void setWpolIdAccCommFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_ID_ACC_COMM, Pos.WPOL_ID_ACC_COMM);
    }

    /**Original name: WPOL-ID-ACC-COMM<br>*/
    public int getWpolIdAccComm() {
        return readPackedAsInt(Pos.WPOL_ID_ACC_COMM, Len.Int.WPOL_ID_ACC_COMM);
    }

    public byte[] getWpolIdAccCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_ID_ACC_COMM, Pos.WPOL_ID_ACC_COMM);
        return buffer;
    }

    public void setWpolIdAccCommNull(String wpolIdAccCommNull) {
        writeString(Pos.WPOL_ID_ACC_COMM_NULL, wpolIdAccCommNull, Len.WPOL_ID_ACC_COMM_NULL);
    }

    /**Original name: WPOL-ID-ACC-COMM-NULL<br>*/
    public String getWpolIdAccCommNull() {
        return readString(Pos.WPOL_ID_ACC_COMM_NULL, Len.WPOL_ID_ACC_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_ID_ACC_COMM = 1;
        public static final int WPOL_ID_ACC_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_ID_ACC_COMM = 5;
        public static final int WPOL_ID_ACC_COMM_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_ID_ACC_COMM = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
