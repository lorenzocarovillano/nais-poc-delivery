package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-DT-CESSAZIONE<br>
 * Variable: DFA-DT-CESSAZIONE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaDtCessazione extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaDtCessazione() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_DT_CESSAZIONE;
    }

    public void setDfaDtCessazione(int dfaDtCessazione) {
        writeIntAsPacked(Pos.DFA_DT_CESSAZIONE, dfaDtCessazione, Len.Int.DFA_DT_CESSAZIONE);
    }

    public void setDfaDtCessazioneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_DT_CESSAZIONE, Pos.DFA_DT_CESSAZIONE);
    }

    /**Original name: DFA-DT-CESSAZIONE<br>*/
    public int getDfaDtCessazione() {
        return readPackedAsInt(Pos.DFA_DT_CESSAZIONE, Len.Int.DFA_DT_CESSAZIONE);
    }

    public byte[] getDfaDtCessazioneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_DT_CESSAZIONE, Pos.DFA_DT_CESSAZIONE);
        return buffer;
    }

    public void setDfaDtCessazioneNull(String dfaDtCessazioneNull) {
        writeString(Pos.DFA_DT_CESSAZIONE_NULL, dfaDtCessazioneNull, Len.DFA_DT_CESSAZIONE_NULL);
    }

    /**Original name: DFA-DT-CESSAZIONE-NULL<br>*/
    public String getDfaDtCessazioneNull() {
        return readString(Pos.DFA_DT_CESSAZIONE_NULL, Len.DFA_DT_CESSAZIONE_NULL);
    }

    public String getDfaDtCessazioneNullFormatted() {
        return Functions.padBlanks(getDfaDtCessazioneNull(), Len.DFA_DT_CESSAZIONE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_DT_CESSAZIONE = 1;
        public static final int DFA_DT_CESSAZIONE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_DT_CESSAZIONE = 5;
        public static final int DFA_DT_CESSAZIONE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_DT_CESSAZIONE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
