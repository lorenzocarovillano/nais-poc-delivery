package it.accenture.jnais.ws;

import it.accenture.jnais.ws.enums.Errore35;
import it.accenture.jnais.ws.enums.Errore37Tr;
import it.accenture.jnais.ws.enums.Flag580;
import it.accenture.jnais.ws.enums.FlagAbbinata;
import it.accenture.jnais.ws.enums.FlagCntrl12Rv;
import it.accenture.jnais.ws.enums.FlagCntrl30;
import it.accenture.jnais.ws.enums.FlagCntrl34;
import it.accenture.jnais.ws.enums.FlagCntrl37;
import it.accenture.jnais.ws.enums.FlagCntrl3Rv;
import it.accenture.jnais.ws.enums.FlagCntrlEsec34;
import it.accenture.jnais.ws.enums.FlagControllo;
import it.accenture.jnais.ws.enums.FlagErroreSi;
import it.accenture.jnais.ws.enums.FlagIntErr;
import it.accenture.jnais.ws.enums.FlagLoopCntrl25;
import it.accenture.jnais.ws.enums.FlagRecord0;
import it.accenture.jnais.ws.enums.FlagScrittura;
import it.accenture.jnais.ws.enums.FlagTrovatoMovi;
import it.accenture.jnais.ws.enums.Rec9TpMoviTrovato;

/**Original name: AREA-FLAG<br>
 * Variable: AREA-FLAG from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreaFlag {

    //==== PROPERTIES ====
    //Original name: ERRORE-35
    private Errore35 errore35 = new Errore35();
    //Original name: ERRORE-37-TR
    private Errore37Tr errore37Tr = new Errore37Tr();
    //Original name: FLAG-CONTROLLO
    private FlagControllo flagControllo = new FlagControllo();
    //Original name: FLAG-ABBINATA
    private FlagAbbinata flagAbbinata = new FlagAbbinata();
    //Original name: FLAG-SCRITTURA
    private FlagScrittura flagScrittura = new FlagScrittura();
    //Original name: FLAG-LOOP-CNTRL-25
    private FlagLoopCntrl25 flagLoopCntrl25 = new FlagLoopCntrl25();
    //Original name: FLAG-RECORD-0
    private FlagRecord0 flagRecord0 = new FlagRecord0();
    //Original name: FLAG-CNTRL-37
    private FlagCntrl37 flagCntrl37 = new FlagCntrl37();
    //Original name: FLAG-CNTRL-34
    private FlagCntrl34 flagCntrl34 = new FlagCntrl34();
    //Original name: FLAG-CNTRL-ESEC-34
    private FlagCntrlEsec34 flagCntrlEsec34 = new FlagCntrlEsec34();
    //Original name: FLAG-CNTRL-3-RV
    private FlagCntrl3Rv flagCntrl3Rv = new FlagCntrl3Rv();
    //Original name: FLAG-CNTRL-12-RV
    private FlagCntrl12Rv flagCntrl12Rv = new FlagCntrl12Rv();
    //Original name: FLAG-580
    private Flag580 flag580 = new Flag580();
    //Original name: FLAG-CNTRL-30
    private FlagCntrl30 flagCntrl30 = new FlagCntrl30();
    //Original name: FLAG-INT-ERR
    private FlagIntErr flagIntErr = new FlagIntErr();
    //Original name: FLAG-ERRORE-SI
    private FlagErroreSi flagErroreSi = new FlagErroreSi();
    //Original name: FLAG-TROVATO-MOVI
    private FlagTrovatoMovi flagTrovatoMovi = new FlagTrovatoMovi();
    //Original name: REC-9-TP-MOVI-TROVATO
    private Rec9TpMoviTrovato rec9TpMoviTrovato = new Rec9TpMoviTrovato();

    //==== METHODS ====
    public Errore35 getErrore35() {
        return errore35;
    }

    public Errore37Tr getErrore37Tr() {
        return errore37Tr;
    }

    public Flag580 getFlag580() {
        return flag580;
    }

    public FlagAbbinata getFlagAbbinata() {
        return flagAbbinata;
    }

    public FlagCntrl12Rv getFlagCntrl12Rv() {
        return flagCntrl12Rv;
    }

    public FlagCntrl30 getFlagCntrl30() {
        return flagCntrl30;
    }

    public FlagCntrl34 getFlagCntrl34() {
        return flagCntrl34;
    }

    public FlagCntrl37 getFlagCntrl37() {
        return flagCntrl37;
    }

    public FlagCntrl3Rv getFlagCntrl3Rv() {
        return flagCntrl3Rv;
    }

    public FlagCntrlEsec34 getFlagCntrlEsec34() {
        return flagCntrlEsec34;
    }

    public FlagControllo getFlagControllo() {
        return flagControllo;
    }

    public FlagErroreSi getFlagErroreSi() {
        return flagErroreSi;
    }

    public FlagIntErr getFlagIntErr() {
        return flagIntErr;
    }

    public FlagLoopCntrl25 getFlagLoopCntrl25() {
        return flagLoopCntrl25;
    }

    public FlagRecord0 getFlagRecord0() {
        return flagRecord0;
    }

    public FlagScrittura getFlagScrittura() {
        return flagScrittura;
    }

    public FlagTrovatoMovi getFlagTrovatoMovi() {
        return flagTrovatoMovi;
    }

    public Rec9TpMoviTrovato getRec9TpMoviTrovato() {
        return rec9TpMoviTrovato;
    }
}
