package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.AstAlloc;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.Ldbv0641;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.copy.RappRete;
import it.accenture.jnais.copy.StraDiInvst;
import it.accenture.jnais.ws.enums.TrovataGar;
import it.accenture.jnais.ws.enums.WsMovimentoLccs1900;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.occurs.WallTabAssetAll;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS1900<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs1900Data {

    //==== PROPERTIES ====
    public static final int WALL_TAB_ASSET_ALL_MAXOCCURS = 220;
    //Original name: WK-PGM
    private String wkPgm = "LCCS1900";
    /**Original name: LDBS0640<br>
	 * <pre>---------------------------------------------------------------*
	 *  PGM CHIAMATI
	 * ---------------------------------------------------------------*</pre>*/
    private String ldbs0640 = "LDBS0640";
    //Original name: PGM-IDBSALL0
    private String pgmIdbsall0 = "IDBSALL0";
    //Original name: PGM-IDBSRRE0
    private String pgmIdbsrre0 = "IDBSRRE0";
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    /**Original name: WALL-ELE-ASSET-ALL-MAX<br>
	 * <pre>----------------------------------------------------------------*
	 *    COPY 7 PER LA GESTIONE DELLE OCCURS
	 * ----------------------------------------------------------------*</pre>*/
    private short wallEleAssetAllMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WALL-TAB-ASSET-ALL
    private WallTabAssetAll[] wallTabAssetAll = new WallTabAssetAll[WALL_TAB_ASSET_ALL_MAXOCCURS];
    //Original name: WK-VARIABILI
    private WkVariabiliLccs1900 wkVariabili = new WkVariabiliLccs1900();
    /**Original name: TROVATA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private TrovataGar trovataGar = new TrovataGar();
    //Original name: IX-LCCC1901
    private short ixLccc1901 = DefaultValues.SHORT_VAL;
    //Original name: IX-TAB-ALL
    private short ixTabAll = DefaultValues.SHORT_VAL;
    //Original name: PARAM-MOVI
    private ParamMovi paramMovi = new ParamMovi();
    //Original name: STRA-DI-INVST
    private StraDiInvst straDiInvst = new StraDiInvst();
    //Original name: AST-ALLOC
    private AstAlloc astAlloc = new AstAlloc();
    //Original name: RAPP-RETE
    private RappRete rappRete = new RappRete();
    //Original name: LDBV0641
    private Ldbv0641 ldbv0641 = new Ldbv0641();
    /**Original name: WS-TP-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *  COPY TIPOLOGICHE
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLccs1900 wsMovimento = new WsMovimentoLccs1900();

    //==== CONSTRUCTORS ====
    public Lccs1900Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wallTabAssetAllIdx = 1; wallTabAssetAllIdx <= WALL_TAB_ASSET_ALL_MAXOCCURS; wallTabAssetAllIdx++) {
            wallTabAssetAll[wallTabAssetAllIdx - 1] = new WallTabAssetAll();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs0640() {
        return this.ldbs0640;
    }

    public String getPgmIdbsall0() {
        return this.pgmIdbsall0;
    }

    public String getPgmIdbsall0Formatted() {
        return Functions.padBlanks(getPgmIdbsall0(), Len.PGM_IDBSALL0);
    }

    public String getPgmIdbsrre0() {
        return this.pgmIdbsrre0;
    }

    public String getPgmIdbsrre0Formatted() {
        return Functions.padBlanks(getPgmIdbsrre0(), Len.PGM_IDBSRRE0);
    }

    public void setWallEleAssetAllMax(short wallEleAssetAllMax) {
        this.wallEleAssetAllMax = wallEleAssetAllMax;
    }

    public short getWallEleAssetAllMax() {
        return this.wallEleAssetAllMax;
    }

    public void setIxLccc1901(short ixLccc1901) {
        this.ixLccc1901 = ixLccc1901;
    }

    public short getIxLccc1901() {
        return this.ixLccc1901;
    }

    public void setIxTabAll(short ixTabAll) {
        this.ixTabAll = ixTabAll;
    }

    public short getIxTabAll() {
        return this.ixTabAll;
    }

    public AstAlloc getAstAlloc() {
        return astAlloc;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ldbv0641 getLdbv0641() {
        return ldbv0641;
    }

    public ParamMovi getParamMovi() {
        return paramMovi;
    }

    public RappRete getRappRete() {
        return rappRete;
    }

    public StraDiInvst getStraDiInvst() {
        return straDiInvst;
    }

    public TrovataGar getTrovataGar() {
        return trovataGar;
    }

    public WallTabAssetAll getWallTabAssetAll(int idx) {
        return wallTabAssetAll[idx - 1];
    }

    public WkVariabiliLccs1900 getWkVariabili() {
        return wkVariabili;
    }

    public WsMovimentoLccs1900 getWsMovimento() {
        return wsMovimento;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PGM_IDBSALL0 = 8;
        public static final int PGM_IDBSRRE0 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
