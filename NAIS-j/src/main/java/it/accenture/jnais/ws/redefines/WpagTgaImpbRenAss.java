package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-TGA-IMPB-REN-ASS<br>
 * Variable: WPAG-TGA-IMPB-REN-ASS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpbRenAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpbRenAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMPB_REN_ASS;
    }

    public void setWpagTgaImpbRenAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMPB_REN_ASS, Pos.WPAG_TGA_IMPB_REN_ASS);
    }

    public byte[] getWpagTgaImpbRenAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMPB_REN_ASS, Pos.WPAG_TGA_IMPB_REN_ASS);
        return buffer;
    }

    public void initWpagTgaImpbRenAssSpaces() {
        fill(Pos.WPAG_TGA_IMPB_REN_ASS, Len.WPAG_TGA_IMPB_REN_ASS, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpbRenAssNull(String wpagTgaImpbRenAssNull) {
        writeString(Pos.WPAG_TGA_IMPB_REN_ASS_NULL, wpagTgaImpbRenAssNull, Len.WPAG_TGA_IMPB_REN_ASS_NULL);
    }

    /**Original name: WPAG-TGA-IMPB-REN-ASS-NULL<br>*/
    public String getWpagTgaImpbRenAssNull() {
        return readString(Pos.WPAG_TGA_IMPB_REN_ASS_NULL, Len.WPAG_TGA_IMPB_REN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMPB_REN_ASS = 1;
        public static final int WPAG_TGA_IMPB_REN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMPB_REN_ASS = 8;
        public static final int WPAG_TGA_IMPB_REN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
