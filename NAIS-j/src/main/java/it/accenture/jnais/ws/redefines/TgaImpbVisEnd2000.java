package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMPB-VIS-END2000<br>
 * Variable: TGA-IMPB-VIS-END2000 from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpbVisEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpbVisEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMPB_VIS_END2000;
    }

    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        writeDecimalAsPacked(Pos.TGA_IMPB_VIS_END2000, tgaImpbVisEnd2000.copy());
    }

    public void setTgaImpbVisEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMPB_VIS_END2000, Pos.TGA_IMPB_VIS_END2000);
    }

    /**Original name: TGA-IMPB-VIS-END2000<br>*/
    public AfDecimal getTgaImpbVisEnd2000() {
        return readPackedAsDecimal(Pos.TGA_IMPB_VIS_END2000, Len.Int.TGA_IMPB_VIS_END2000, Len.Fract.TGA_IMPB_VIS_END2000);
    }

    public byte[] getTgaImpbVisEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMPB_VIS_END2000, Pos.TGA_IMPB_VIS_END2000);
        return buffer;
    }

    public void setTgaImpbVisEnd2000Null(String tgaImpbVisEnd2000Null) {
        writeString(Pos.TGA_IMPB_VIS_END2000_NULL, tgaImpbVisEnd2000Null, Len.TGA_IMPB_VIS_END2000_NULL);
    }

    /**Original name: TGA-IMPB-VIS-END2000-NULL<br>*/
    public String getTgaImpbVisEnd2000Null() {
        return readString(Pos.TGA_IMPB_VIS_END2000_NULL, Len.TGA_IMPB_VIS_END2000_NULL);
    }

    public String getTgaImpbVisEnd2000NullFormatted() {
        return Functions.padBlanks(getTgaImpbVisEnd2000Null(), Len.TGA_IMPB_VIS_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_VIS_END2000 = 1;
        public static final int TGA_IMPB_VIS_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_VIS_END2000 = 8;
        public static final int TGA_IMPB_VIS_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_VIS_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_VIS_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
