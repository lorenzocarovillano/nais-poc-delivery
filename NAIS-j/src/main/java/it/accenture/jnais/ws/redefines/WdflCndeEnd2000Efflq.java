package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNDE-END2000-EFFLQ<br>
 * Variable: WDFL-CNDE-END2000-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCndeEnd2000Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCndeEnd2000Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNDE_END2000_EFFLQ;
    }

    public void setWdflCndeEnd2000Efflq(AfDecimal wdflCndeEnd2000Efflq) {
        writeDecimalAsPacked(Pos.WDFL_CNDE_END2000_EFFLQ, wdflCndeEnd2000Efflq.copy());
    }

    public void setWdflCndeEnd2000EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNDE_END2000_EFFLQ, Pos.WDFL_CNDE_END2000_EFFLQ);
    }

    /**Original name: WDFL-CNDE-END2000-EFFLQ<br>*/
    public AfDecimal getWdflCndeEnd2000Efflq() {
        return readPackedAsDecimal(Pos.WDFL_CNDE_END2000_EFFLQ, Len.Int.WDFL_CNDE_END2000_EFFLQ, Len.Fract.WDFL_CNDE_END2000_EFFLQ);
    }

    public byte[] getWdflCndeEnd2000EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNDE_END2000_EFFLQ, Pos.WDFL_CNDE_END2000_EFFLQ);
        return buffer;
    }

    public void setWdflCndeEnd2000EfflqNull(String wdflCndeEnd2000EfflqNull) {
        writeString(Pos.WDFL_CNDE_END2000_EFFLQ_NULL, wdflCndeEnd2000EfflqNull, Len.WDFL_CNDE_END2000_EFFLQ_NULL);
    }

    /**Original name: WDFL-CNDE-END2000-EFFLQ-NULL<br>*/
    public String getWdflCndeEnd2000EfflqNull() {
        return readString(Pos.WDFL_CNDE_END2000_EFFLQ_NULL, Len.WDFL_CNDE_END2000_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_END2000_EFFLQ = 1;
        public static final int WDFL_CNDE_END2000_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_END2000_EFFLQ = 8;
        public static final int WDFL_CNDE_END2000_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_END2000_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_END2000_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
