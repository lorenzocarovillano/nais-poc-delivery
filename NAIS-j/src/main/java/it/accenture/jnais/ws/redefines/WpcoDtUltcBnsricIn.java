package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-BNSRIC-IN<br>
 * Variable: WPCO-DT-ULTC-BNSRIC-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcBnsricIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcBnsricIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_BNSRIC_IN;
    }

    public void setWpcoDtUltcBnsricIn(int wpcoDtUltcBnsricIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_BNSRIC_IN, wpcoDtUltcBnsricIn, Len.Int.WPCO_DT_ULTC_BNSRIC_IN);
    }

    public void setDpcoDtUltcBnsricInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSRIC_IN, Pos.WPCO_DT_ULTC_BNSRIC_IN);
    }

    /**Original name: WPCO-DT-ULTC-BNSRIC-IN<br>*/
    public int getWpcoDtUltcBnsricIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_BNSRIC_IN, Len.Int.WPCO_DT_ULTC_BNSRIC_IN);
    }

    public byte[] getWpcoDtUltcBnsricInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSRIC_IN, Pos.WPCO_DT_ULTC_BNSRIC_IN);
        return buffer;
    }

    public void setWpcoDtUltcBnsricInNull(String wpcoDtUltcBnsricInNull) {
        writeString(Pos.WPCO_DT_ULTC_BNSRIC_IN_NULL, wpcoDtUltcBnsricInNull, Len.WPCO_DT_ULTC_BNSRIC_IN_NULL);
    }

    /**Original name: WPCO-DT-ULTC-BNSRIC-IN-NULL<br>*/
    public String getWpcoDtUltcBnsricInNull() {
        return readString(Pos.WPCO_DT_ULTC_BNSRIC_IN_NULL, Len.WPCO_DT_ULTC_BNSRIC_IN_NULL);
    }

    public String getWpcoDtUltcBnsricInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcBnsricInNull(), Len.WPCO_DT_ULTC_BNSRIC_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSRIC_IN = 1;
        public static final int WPCO_DT_ULTC_BNSRIC_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSRIC_IN = 5;
        public static final int WPCO_DT_ULTC_BNSRIC_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_BNSRIC_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
