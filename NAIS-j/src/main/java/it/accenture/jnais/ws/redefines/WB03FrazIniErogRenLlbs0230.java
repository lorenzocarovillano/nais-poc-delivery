package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-FRAZ-INI-EROG-REN<br>
 * Variable: W-B03-FRAZ-INI-EROG-REN from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03FrazIniErogRenLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03FrazIniErogRenLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_FRAZ_INI_EROG_REN;
    }

    public void setwB03FrazIniErogRen(int wB03FrazIniErogRen) {
        writeIntAsPacked(Pos.W_B03_FRAZ_INI_EROG_REN, wB03FrazIniErogRen, Len.Int.W_B03_FRAZ_INI_EROG_REN);
    }

    /**Original name: W-B03-FRAZ-INI-EROG-REN<br>*/
    public int getwB03FrazIniErogRen() {
        return readPackedAsInt(Pos.W_B03_FRAZ_INI_EROG_REN, Len.Int.W_B03_FRAZ_INI_EROG_REN);
    }

    public byte[] getwB03FrazIniErogRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_FRAZ_INI_EROG_REN, Pos.W_B03_FRAZ_INI_EROG_REN);
        return buffer;
    }

    public void setwB03FrazIniErogRenNull(String wB03FrazIniErogRenNull) {
        writeString(Pos.W_B03_FRAZ_INI_EROG_REN_NULL, wB03FrazIniErogRenNull, Len.W_B03_FRAZ_INI_EROG_REN_NULL);
    }

    /**Original name: W-B03-FRAZ-INI-EROG-REN-NULL<br>*/
    public String getwB03FrazIniErogRenNull() {
        return readString(Pos.W_B03_FRAZ_INI_EROG_REN_NULL, Len.W_B03_FRAZ_INI_EROG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_FRAZ_INI_EROG_REN = 1;
        public static final int W_B03_FRAZ_INI_EROG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_FRAZ_INI_EROG_REN = 3;
        public static final int W_B03_FRAZ_INI_EROG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_FRAZ_INI_EROG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
