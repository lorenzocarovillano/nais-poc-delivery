package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: SDI-FRQ-VALUT<br>
 * Variable: SDI-FRQ-VALUT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SdiFrqValut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SdiFrqValut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SDI_FRQ_VALUT;
    }

    public void setSdiFrqValut(int sdiFrqValut) {
        writeIntAsPacked(Pos.SDI_FRQ_VALUT, sdiFrqValut, Len.Int.SDI_FRQ_VALUT);
    }

    public void setSdiFrqValutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SDI_FRQ_VALUT, Pos.SDI_FRQ_VALUT);
    }

    /**Original name: SDI-FRQ-VALUT<br>*/
    public int getSdiFrqValut() {
        return readPackedAsInt(Pos.SDI_FRQ_VALUT, Len.Int.SDI_FRQ_VALUT);
    }

    public byte[] getSdiFrqValutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SDI_FRQ_VALUT, Pos.SDI_FRQ_VALUT);
        return buffer;
    }

    public void setSdiFrqValutNull(String sdiFrqValutNull) {
        writeString(Pos.SDI_FRQ_VALUT_NULL, sdiFrqValutNull, Len.SDI_FRQ_VALUT_NULL);
    }

    /**Original name: SDI-FRQ-VALUT-NULL<br>*/
    public String getSdiFrqValutNull() {
        return readString(Pos.SDI_FRQ_VALUT_NULL, Len.SDI_FRQ_VALUT_NULL);
    }

    public String getSdiFrqValutNullFormatted() {
        return Functions.padBlanks(getSdiFrqValutNull(), Len.SDI_FRQ_VALUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SDI_FRQ_VALUT = 1;
        public static final int SDI_FRQ_VALUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_FRQ_VALUT = 3;
        public static final int SDI_FRQ_VALUT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SDI_FRQ_VALUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
