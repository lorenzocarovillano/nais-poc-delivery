package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DUR-GG<br>
 * Variable: TGA-DUR-GG from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDurGg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDurGg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DUR_GG;
    }

    public void setTgaDurGg(int tgaDurGg) {
        writeIntAsPacked(Pos.TGA_DUR_GG, tgaDurGg, Len.Int.TGA_DUR_GG);
    }

    public void setTgaDurGgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DUR_GG, Pos.TGA_DUR_GG);
    }

    /**Original name: TGA-DUR-GG<br>*/
    public int getTgaDurGg() {
        return readPackedAsInt(Pos.TGA_DUR_GG, Len.Int.TGA_DUR_GG);
    }

    public byte[] getTgaDurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DUR_GG, Pos.TGA_DUR_GG);
        return buffer;
    }

    public void setTgaDurGgNull(String tgaDurGgNull) {
        writeString(Pos.TGA_DUR_GG_NULL, tgaDurGgNull, Len.TGA_DUR_GG_NULL);
    }

    /**Original name: TGA-DUR-GG-NULL<br>*/
    public String getTgaDurGgNull() {
        return readString(Pos.TGA_DUR_GG_NULL, Len.TGA_DUR_GG_NULL);
    }

    public String getTgaDurGgNullFormatted() {
        return Functions.padBlanks(getTgaDurGgNull(), Len.TGA_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DUR_GG = 1;
        public static final int TGA_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DUR_GG = 3;
        public static final int TGA_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
