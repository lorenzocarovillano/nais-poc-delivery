package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;
import it.accenture.jnais.copy.Ispc0040DatiInput;

/**Original name: WK-0040-TABELLA<br>
 * Variables: WK-0040-TABELLA from program LOAS0310<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wk0040Tabella implements ICopyable<Wk0040Tabella> {

    //==== PROPERTIES ====
    //Original name: WK-0040-DATI-INPUT
    private Ispc0040DatiInput datiInput = new Ispc0040DatiInput();
    //Original name: WK-0040-DATA-VERSIONE-PROD
    private String dataVersioneProd = DefaultValues.stringVal(Len.DATA_VERSIONE_PROD);

    //==== CONSTRUCTORS ====
    public Wk0040Tabella() {
    }

    public Wk0040Tabella(Wk0040Tabella wk0040Tabella) {
        this();
        this.datiInput = wk0040Tabella.datiInput.copy();
        this.dataVersioneProd = wk0040Tabella.dataVersioneProd;
    }

    //==== METHODS ====
    public void setDataVersioneProd(String dataVersioneProd) {
        this.dataVersioneProd = Functions.subString(dataVersioneProd, Len.DATA_VERSIONE_PROD);
    }

    public String getDataVersioneProd() {
        return this.dataVersioneProd;
    }

    public String getDataVersioneProdFormatted() {
        return Functions.padBlanks(getDataVersioneProd(), Len.DATA_VERSIONE_PROD);
    }

    public Ispc0040DatiInput getDatiInput() {
        return datiInput;
    }

    public Wk0040Tabella copy() {
        return new Wk0040Tabella(this);
    }

    public Wk0040Tabella initWk0040Tabella() {
        datiInput.setCodCompagniaFormatted("00000");
        datiInput.setCodProdotto("");
        datiInput.setCodConvenzione("");
        datiInput.setDataInizValidConv("");
        datiInput.setDataRiferimento("");
        datiInput.setDataEmissione("");
        datiInput.setDataProposta("");
        datiInput.setLivelloUtenteFormatted("00");
        datiInput.setFunzionalitaFormatted("00000");
        datiInput.setSessionId("");
        dataVersioneProd = "";
        return this;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATA_VERSIONE_PROD = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
