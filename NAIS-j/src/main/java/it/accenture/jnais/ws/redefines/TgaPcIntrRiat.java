package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PC-INTR-RIAT<br>
 * Variable: TGA-PC-INTR-RIAT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPcIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPcIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PC_INTR_RIAT;
    }

    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        writeDecimalAsPacked(Pos.TGA_PC_INTR_RIAT, tgaPcIntrRiat.copy());
    }

    public void setTgaPcIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PC_INTR_RIAT, Pos.TGA_PC_INTR_RIAT);
    }

    /**Original name: TGA-PC-INTR-RIAT<br>*/
    public AfDecimal getTgaPcIntrRiat() {
        return readPackedAsDecimal(Pos.TGA_PC_INTR_RIAT, Len.Int.TGA_PC_INTR_RIAT, Len.Fract.TGA_PC_INTR_RIAT);
    }

    public byte[] getTgaPcIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PC_INTR_RIAT, Pos.TGA_PC_INTR_RIAT);
        return buffer;
    }

    public void setTgaPcIntrRiatNull(String tgaPcIntrRiatNull) {
        writeString(Pos.TGA_PC_INTR_RIAT_NULL, tgaPcIntrRiatNull, Len.TGA_PC_INTR_RIAT_NULL);
    }

    /**Original name: TGA-PC-INTR-RIAT-NULL<br>*/
    public String getTgaPcIntrRiatNull() {
        return readString(Pos.TGA_PC_INTR_RIAT_NULL, Len.TGA_PC_INTR_RIAT_NULL);
    }

    public String getTgaPcIntrRiatNullFormatted() {
        return Functions.padBlanks(getTgaPcIntrRiatNull(), Len.TGA_PC_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PC_INTR_RIAT = 1;
        public static final int TGA_PC_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PC_INTR_RIAT = 4;
        public static final int TGA_PC_INTR_RIAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
