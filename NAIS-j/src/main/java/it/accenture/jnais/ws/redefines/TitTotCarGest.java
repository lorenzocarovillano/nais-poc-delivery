package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-CAR-GEST<br>
 * Variable: TIT-TOT-CAR-GEST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_CAR_GEST;
    }

    public void setTitTotCarGest(AfDecimal titTotCarGest) {
        writeDecimalAsPacked(Pos.TIT_TOT_CAR_GEST, titTotCarGest.copy());
    }

    public void setTitTotCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_CAR_GEST, Pos.TIT_TOT_CAR_GEST);
    }

    /**Original name: TIT-TOT-CAR-GEST<br>*/
    public AfDecimal getTitTotCarGest() {
        return readPackedAsDecimal(Pos.TIT_TOT_CAR_GEST, Len.Int.TIT_TOT_CAR_GEST, Len.Fract.TIT_TOT_CAR_GEST);
    }

    public byte[] getTitTotCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_CAR_GEST, Pos.TIT_TOT_CAR_GEST);
        return buffer;
    }

    public void setTitTotCarGestNull(String titTotCarGestNull) {
        writeString(Pos.TIT_TOT_CAR_GEST_NULL, titTotCarGestNull, Len.TIT_TOT_CAR_GEST_NULL);
    }

    /**Original name: TIT-TOT-CAR-GEST-NULL<br>*/
    public String getTitTotCarGestNull() {
        return readString(Pos.TIT_TOT_CAR_GEST_NULL, Len.TIT_TOT_CAR_GEST_NULL);
    }

    public String getTitTotCarGestNullFormatted() {
        return Functions.padBlanks(getTitTotCarGestNull(), Len.TIT_TOT_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_GEST = 1;
        public static final int TIT_TOT_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_CAR_GEST = 8;
        public static final int TIT_TOT_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
