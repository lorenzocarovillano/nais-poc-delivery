package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PROV-ACQ-1AA<br>
 * Variable: WTIT-TOT-PROV-ACQ-1AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PROV_ACQ1AA;
    }

    public void setWtitTotProvAcq1aa(AfDecimal wtitTotProvAcq1aa) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PROV_ACQ1AA, wtitTotProvAcq1aa.copy());
    }

    public void setWtitTotProvAcq1aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PROV_ACQ1AA, Pos.WTIT_TOT_PROV_ACQ1AA);
    }

    /**Original name: WTIT-TOT-PROV-ACQ-1AA<br>*/
    public AfDecimal getWtitTotProvAcq1aa() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PROV_ACQ1AA, Len.Int.WTIT_TOT_PROV_ACQ1AA, Len.Fract.WTIT_TOT_PROV_ACQ1AA);
    }

    public byte[] getWtitTotProvAcq1aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PROV_ACQ1AA, Pos.WTIT_TOT_PROV_ACQ1AA);
        return buffer;
    }

    public void initWtitTotProvAcq1aaSpaces() {
        fill(Pos.WTIT_TOT_PROV_ACQ1AA, Len.WTIT_TOT_PROV_ACQ1AA, Types.SPACE_CHAR);
    }

    public void setWtitTotProvAcq1aaNull(String wtitTotProvAcq1aaNull) {
        writeString(Pos.WTIT_TOT_PROV_ACQ1AA_NULL, wtitTotProvAcq1aaNull, Len.WTIT_TOT_PROV_ACQ1AA_NULL);
    }

    /**Original name: WTIT-TOT-PROV-ACQ-1AA-NULL<br>*/
    public String getWtitTotProvAcq1aaNull() {
        return readString(Pos.WTIT_TOT_PROV_ACQ1AA_NULL, Len.WTIT_TOT_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_ACQ1AA = 1;
        public static final int WTIT_TOT_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PROV_ACQ1AA = 8;
        public static final int WTIT_TOT_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
