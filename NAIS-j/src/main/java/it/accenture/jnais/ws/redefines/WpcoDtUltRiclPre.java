package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RICL-PRE<br>
 * Variable: WPCO-DT-ULT-RICL-PRE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRiclPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRiclPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RICL_PRE;
    }

    public void setWpcoDtUltRiclPre(int wpcoDtUltRiclPre) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RICL_PRE, wpcoDtUltRiclPre, Len.Int.WPCO_DT_ULT_RICL_PRE);
    }

    public void setDpcoDtUltRiclPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RICL_PRE, Pos.WPCO_DT_ULT_RICL_PRE);
    }

    /**Original name: WPCO-DT-ULT-RICL-PRE<br>*/
    public int getWpcoDtUltRiclPre() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RICL_PRE, Len.Int.WPCO_DT_ULT_RICL_PRE);
    }

    public byte[] getWpcoDtUltRiclPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RICL_PRE, Pos.WPCO_DT_ULT_RICL_PRE);
        return buffer;
    }

    public void setWpcoDtUltRiclPreNull(String wpcoDtUltRiclPreNull) {
        writeString(Pos.WPCO_DT_ULT_RICL_PRE_NULL, wpcoDtUltRiclPreNull, Len.WPCO_DT_ULT_RICL_PRE_NULL);
    }

    /**Original name: WPCO-DT-ULT-RICL-PRE-NULL<br>*/
    public String getWpcoDtUltRiclPreNull() {
        return readString(Pos.WPCO_DT_ULT_RICL_PRE_NULL, Len.WPCO_DT_ULT_RICL_PRE_NULL);
    }

    public String getWpcoDtUltRiclPreNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRiclPreNull(), Len.WPCO_DT_ULT_RICL_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RICL_PRE = 1;
        public static final int WPCO_DT_ULT_RICL_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RICL_PRE = 5;
        public static final int WPCO_DT_ULT_RICL_PRE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RICL_PRE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
