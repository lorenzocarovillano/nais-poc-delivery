package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-TS-NET-T<br>
 * Variable: B03-TS-NET-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03TsNetT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03TsNetT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_TS_NET_T;
    }

    public void setB03TsNetT(AfDecimal b03TsNetT) {
        writeDecimalAsPacked(Pos.B03_TS_NET_T, b03TsNetT.copy());
    }

    public void setB03TsNetTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_TS_NET_T, Pos.B03_TS_NET_T);
    }

    /**Original name: B03-TS-NET-T<br>*/
    public AfDecimal getB03TsNetT() {
        return readPackedAsDecimal(Pos.B03_TS_NET_T, Len.Int.B03_TS_NET_T, Len.Fract.B03_TS_NET_T);
    }

    public byte[] getB03TsNetTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_TS_NET_T, Pos.B03_TS_NET_T);
        return buffer;
    }

    public void setB03TsNetTNull(String b03TsNetTNull) {
        writeString(Pos.B03_TS_NET_T_NULL, b03TsNetTNull, Len.B03_TS_NET_T_NULL);
    }

    /**Original name: B03-TS-NET-T-NULL<br>*/
    public String getB03TsNetTNull() {
        return readString(Pos.B03_TS_NET_T_NULL, Len.B03_TS_NET_T_NULL);
    }

    public String getB03TsNetTNullFormatted() {
        return Functions.padBlanks(getB03TsNetTNull(), Len.B03_TS_NET_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_TS_NET_T = 1;
        public static final int B03_TS_NET_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_TS_NET_T = 8;
        public static final int B03_TS_NET_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_TS_NET_T = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_TS_NET_T = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
