package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RES-PRE-ATT-CALC<br>
 * Variable: DFL-RES-PRE-ATT-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflResPreAttCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflResPreAttCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RES_PRE_ATT_CALC;
    }

    public void setDflResPreAttCalc(AfDecimal dflResPreAttCalc) {
        writeDecimalAsPacked(Pos.DFL_RES_PRE_ATT_CALC, dflResPreAttCalc.copy());
    }

    public void setDflResPreAttCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RES_PRE_ATT_CALC, Pos.DFL_RES_PRE_ATT_CALC);
    }

    /**Original name: DFL-RES-PRE-ATT-CALC<br>*/
    public AfDecimal getDflResPreAttCalc() {
        return readPackedAsDecimal(Pos.DFL_RES_PRE_ATT_CALC, Len.Int.DFL_RES_PRE_ATT_CALC, Len.Fract.DFL_RES_PRE_ATT_CALC);
    }

    public byte[] getDflResPreAttCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RES_PRE_ATT_CALC, Pos.DFL_RES_PRE_ATT_CALC);
        return buffer;
    }

    public void setDflResPreAttCalcNull(String dflResPreAttCalcNull) {
        writeString(Pos.DFL_RES_PRE_ATT_CALC_NULL, dflResPreAttCalcNull, Len.DFL_RES_PRE_ATT_CALC_NULL);
    }

    /**Original name: DFL-RES-PRE-ATT-CALC-NULL<br>*/
    public String getDflResPreAttCalcNull() {
        return readString(Pos.DFL_RES_PRE_ATT_CALC_NULL, Len.DFL_RES_PRE_ATT_CALC_NULL);
    }

    public String getDflResPreAttCalcNullFormatted() {
        return Functions.padBlanks(getDflResPreAttCalcNull(), Len.DFL_RES_PRE_ATT_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRE_ATT_CALC = 1;
        public static final int DFL_RES_PRE_ATT_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRE_ATT_CALC = 8;
        public static final int DFL_RES_PRE_ATT_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRE_ATT_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRE_ATT_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
