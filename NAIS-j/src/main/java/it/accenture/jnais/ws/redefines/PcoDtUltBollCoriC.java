package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-CORI-C<br>
 * Variable: PCO-DT-ULT-BOLL-CORI-C from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollCoriC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollCoriC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_CORI_C;
    }

    public void setPcoDtUltBollCoriC(int pcoDtUltBollCoriC) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_CORI_C, pcoDtUltBollCoriC, Len.Int.PCO_DT_ULT_BOLL_CORI_C);
    }

    public void setPcoDtUltBollCoriCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_CORI_C, Pos.PCO_DT_ULT_BOLL_CORI_C);
    }

    /**Original name: PCO-DT-ULT-BOLL-CORI-C<br>*/
    public int getPcoDtUltBollCoriC() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_CORI_C, Len.Int.PCO_DT_ULT_BOLL_CORI_C);
    }

    public byte[] getPcoDtUltBollCoriCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_CORI_C, Pos.PCO_DT_ULT_BOLL_CORI_C);
        return buffer;
    }

    public void initPcoDtUltBollCoriCHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_CORI_C, Len.PCO_DT_ULT_BOLL_CORI_C, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollCoriCNull(String pcoDtUltBollCoriCNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_CORI_C_NULL, pcoDtUltBollCoriCNull, Len.PCO_DT_ULT_BOLL_CORI_C_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-CORI-C-NULL<br>*/
    public String getPcoDtUltBollCoriCNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_CORI_C_NULL, Len.PCO_DT_ULT_BOLL_CORI_C_NULL);
    }

    public String getPcoDtUltBollCoriCNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollCoriCNull(), Len.PCO_DT_ULT_BOLL_CORI_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_CORI_C = 1;
        public static final int PCO_DT_ULT_BOLL_CORI_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_CORI_C = 5;
        public static final int PCO_DT_ULT_BOLL_CORI_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_CORI_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
