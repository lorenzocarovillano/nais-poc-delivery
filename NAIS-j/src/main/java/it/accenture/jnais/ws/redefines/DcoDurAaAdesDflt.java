package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-DUR-AA-ADES-DFLT<br>
 * Variable: DCO-DUR-AA-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoDurAaAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoDurAaAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_DUR_AA_ADES_DFLT;
    }

    public void setDcoDurAaAdesDflt(int dcoDurAaAdesDflt) {
        writeIntAsPacked(Pos.DCO_DUR_AA_ADES_DFLT, dcoDurAaAdesDflt, Len.Int.DCO_DUR_AA_ADES_DFLT);
    }

    public void setDcoDurAaAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_DUR_AA_ADES_DFLT, Pos.DCO_DUR_AA_ADES_DFLT);
    }

    /**Original name: DCO-DUR-AA-ADES-DFLT<br>*/
    public int getDcoDurAaAdesDflt() {
        return readPackedAsInt(Pos.DCO_DUR_AA_ADES_DFLT, Len.Int.DCO_DUR_AA_ADES_DFLT);
    }

    public byte[] getDcoDurAaAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_DUR_AA_ADES_DFLT, Pos.DCO_DUR_AA_ADES_DFLT);
        return buffer;
    }

    public void setDcoDurAaAdesDfltNull(String dcoDurAaAdesDfltNull) {
        writeString(Pos.DCO_DUR_AA_ADES_DFLT_NULL, dcoDurAaAdesDfltNull, Len.DCO_DUR_AA_ADES_DFLT_NULL);
    }

    /**Original name: DCO-DUR-AA-ADES-DFLT-NULL<br>*/
    public String getDcoDurAaAdesDfltNull() {
        return readString(Pos.DCO_DUR_AA_ADES_DFLT_NULL, Len.DCO_DUR_AA_ADES_DFLT_NULL);
    }

    public String getDcoDurAaAdesDfltNullFormatted() {
        return Functions.padBlanks(getDcoDurAaAdesDfltNull(), Len.DCO_DUR_AA_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_DUR_AA_ADES_DFLT = 1;
        public static final int DCO_DUR_AA_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_DUR_AA_ADES_DFLT = 3;
        public static final int DCO_DUR_AA_ADES_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_DUR_AA_ADES_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
