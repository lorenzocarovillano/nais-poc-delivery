package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-RIASTA<br>
 * Variable: WB03-RIS-RIASTA from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisRiasta extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisRiasta() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_RIASTA;
    }

    public void setWb03RisRiastaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_RIASTA, Pos.WB03_RIS_RIASTA);
    }

    /**Original name: WB03-RIS-RIASTA<br>*/
    public AfDecimal getWb03RisRiasta() {
        return readPackedAsDecimal(Pos.WB03_RIS_RIASTA, Len.Int.WB03_RIS_RIASTA, Len.Fract.WB03_RIS_RIASTA);
    }

    public byte[] getWb03RisRiastaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_RIASTA, Pos.WB03_RIS_RIASTA);
        return buffer;
    }

    public void setWb03RisRiastaNull(String wb03RisRiastaNull) {
        writeString(Pos.WB03_RIS_RIASTA_NULL, wb03RisRiastaNull, Len.WB03_RIS_RIASTA_NULL);
    }

    /**Original name: WB03-RIS-RIASTA-NULL<br>*/
    public String getWb03RisRiastaNull() {
        return readString(Pos.WB03_RIS_RIASTA_NULL, Len.WB03_RIS_RIASTA_NULL);
    }

    public String getWb03RisRiastaNullFormatted() {
        return Functions.padBlanks(getWb03RisRiastaNull(), Len.WB03_RIS_RIASTA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_RIASTA = 1;
        public static final int WB03_RIS_RIASTA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_RIASTA = 8;
        public static final int WB03_RIS_RIASTA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_RIASTA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_RIASTA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
