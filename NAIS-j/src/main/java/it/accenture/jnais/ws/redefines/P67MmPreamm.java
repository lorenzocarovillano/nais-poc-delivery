package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-MM-PREAMM<br>
 * Variable: P67-MM-PREAMM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67MmPreamm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67MmPreamm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_MM_PREAMM;
    }

    public void setP67MmPreamm(int p67MmPreamm) {
        writeIntAsPacked(Pos.P67_MM_PREAMM, p67MmPreamm, Len.Int.P67_MM_PREAMM);
    }

    public void setP67MmPreammFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_MM_PREAMM, Pos.P67_MM_PREAMM);
    }

    /**Original name: P67-MM-PREAMM<br>*/
    public int getP67MmPreamm() {
        return readPackedAsInt(Pos.P67_MM_PREAMM, Len.Int.P67_MM_PREAMM);
    }

    public byte[] getP67MmPreammAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_MM_PREAMM, Pos.P67_MM_PREAMM);
        return buffer;
    }

    public void setP67MmPreammNull(String p67MmPreammNull) {
        writeString(Pos.P67_MM_PREAMM_NULL, p67MmPreammNull, Len.P67_MM_PREAMM_NULL);
    }

    /**Original name: P67-MM-PREAMM-NULL<br>*/
    public String getP67MmPreammNull() {
        return readString(Pos.P67_MM_PREAMM_NULL, Len.P67_MM_PREAMM_NULL);
    }

    public String getP67MmPreammNullFormatted() {
        return Functions.padBlanks(getP67MmPreammNull(), Len.P67_MM_PREAMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_MM_PREAMM = 1;
        public static final int P67_MM_PREAMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_MM_PREAMM = 3;
        public static final int P67_MM_PREAMM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_MM_PREAMM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
