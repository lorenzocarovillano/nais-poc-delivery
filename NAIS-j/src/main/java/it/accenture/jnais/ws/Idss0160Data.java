package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.CompNumOgg;
import it.accenture.jnais.copy.Idsi0021Area;
import it.accenture.jnais.copy.Idso0021;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.Ivvc0218;
import it.accenture.jnais.copy.Lccvade1;
import it.accenture.jnais.copy.Lccvpol1;
import it.accenture.jnais.copy.NumOgg;
import it.accenture.jnais.copy.ProgrNumOgg;
import it.accenture.jnais.copy.RappRete;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WkFindCampo;
import it.accenture.jnais.ws.enums.WksTrovatoTabb;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsFlagFineValore;
import it.accenture.jnais.ws.enums.WsLettura;
import it.accenture.jnais.ws.occurs.WrreTabRappRete;
import it.accenture.jnais.ws.redefines.WksTabStrDato;
import it.accenture.jnais.ws.redefines.WsValoreApp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDSS0160<br>
 * Generated as a class for rule WS.<br>*/
public class Idss0160Data {

    //==== PROPERTIES ====
    public static final int WRRE_TAB_RAPP_RETE_MAXOCCURS = 20;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "IDSS0160";
    //Original name: WS-VARIABILI
    private WsVariabiliIdss0160 wsVariabili = new WsVariabiliIdss0160();
    //Original name: WS-VALORE-APP
    private WsValoreApp wsValoreApp = new WsValoreApp();
    //Original name: IX-INDICI
    private IxIndici ixIndici = new IxIndici();
    /**Original name: WS-FLAG-FINE-VALORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private WsFlagFineValore wsFlagFineValore = new WsFlagFineValore();
    //Original name: WK-FIND-CAMPO
    private WkFindCampo wkFindCampo = new WkFindCampo();
    //Original name: WS-LETTURA
    private WsLettura wsLettura = new WsLettura();
    //Original name: WKS-TROVATO-TABB
    private WksTrovatoTabb wksTrovatoTabb = new WksTrovatoTabb();
    //Original name: WKS-ELE-MAX-TABB
    private short wksEleMaxTabb = DefaultValues.SHORT_VAL;
    //Original name: WKS-TAB-STR-DATO
    private WksTabStrDato wksTabStrDato = new WksTabStrDato();
    //Original name: WKS-APPO-IDSI0011
    private String wksAppoIdsi0011 = "";
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: AREA-CALL-IWFS0050
    private AreaCallIwfs0050 areaCallIwfs0050 = new AreaCallIwfs0050();
    //Original name: AREA-IDSV0141
    private Idss0140Link areaIdsv0141 = new Idss0140Link();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: COMP-NUM-OGG
    private CompNumOgg compNumOgg = new CompNumOgg();
    //Original name: NUM-OGG
    private NumOgg numOgg = new NumOgg();
    //Original name: PROGR-NUM-OGG
    private ProgrNumOgg progrNumOgg = new ProgrNumOgg();
    //Original name: RAPP-RETE
    private RappRete rappRete = new RappRete();
    //Original name: IVVC0218
    private Ivvc0218 ivvc0218 = new Ivvc0218();
    //Original name: AREA-CALL
    private AreaCall areaCall = new AreaCall();
    //Original name: IDSI0021-AREA
    private Idsi0021Area idsi0021Area = new Idsi0021Area();
    //Original name: IDSO0021
    private Idso0021 idso0021 = new Idso0021();
    //Original name: WPOL-ELE-POLI-MAX
    private short wpolElePoliMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1 lccvpol1 = new Lccvpol1();
    //Original name: WADE-ELE-ADES-MAX
    private short wadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1 lccvade1 = new Lccvade1();
    //Original name: WRRE-ELE-RAPP-RETE-MAX
    private short wrreEleRappReteMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRRE-TAB-RAPP-RETE
    private WrreTabRappRete[] wrreTabRappRete = new WrreTabRappRete[WRRE_TAB_RAPP_RETE_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Idss0160Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wrreTabRappReteIdx = 1; wrreTabRappReteIdx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; wrreTabRappReteIdx++) {
            wrreTabRappRete[wrreTabRappReteIdx - 1] = new WrreTabRappRete();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWksEleMaxTabb(short wksEleMaxTabb) {
        this.wksEleMaxTabb = wksEleMaxTabb;
    }

    public short getWksEleMaxTabb() {
        return this.wksEleMaxTabb;
    }

    public void setWksAppoIdsi0011(String wksAppoIdsi0011) {
        this.wksAppoIdsi0011 = Functions.subString(wksAppoIdsi0011, Len.WKS_APPO_IDSI0011);
    }

    public String getWksAppoIdsi0011() {
        return this.wksAppoIdsi0011;
    }

    public String getWksAppoIdsi0011Formatted() {
        return Functions.padBlanks(getWksAppoIdsi0011(), Len.WKS_APPO_IDSI0011);
    }

    /**Original name: INPUT-IDSS0020<br>*/
    public byte[] getInputIdss0020Bytes() {
        byte[] buffer = new byte[Len.INPUT_IDSS0020];
        return getInputIdss0020Bytes(buffer, 1);
    }

    public byte[] getInputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0021Area.getIdsi0021AreaBytes(buffer, position);
        return buffer;
    }

    public void initInputIdss0020Spaces() {
        idsi0021Area.initIdsi0021AreaSpaces();
    }

    public void setOutputIdss0020Bytes(byte[] buffer) {
        setOutputIdss0020Bytes(buffer, 1);
    }

    public void setOutputIdss0020Bytes(byte[] buffer, int offset) {
        int position = offset;
        idso0021.setIdso0021AreaBytes(buffer, position);
    }

    public void setWpolAreaPolizzaFormatted(String data) {
        byte[] buffer = new byte[Len.WPOL_AREA_POLIZZA];
        MarshalByte.writeString(buffer, 1, data, Len.WPOL_AREA_POLIZZA);
        setWpolAreaPolizzaBytes(buffer, 1);
    }

    public void setWpolAreaPolizzaBytes(byte[] buffer, int offset) {
        int position = offset;
        wpolElePoliMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWpolTabPoliBytes(buffer, position);
    }

    public void setWpolElePoliMax(short wpolElePoliMax) {
        this.wpolElePoliMax = wpolElePoliMax;
    }

    public short getWpolElePoliMax() {
        return this.wpolElePoliMax;
    }

    public void setWpolTabPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1.Len.Int.ID_PTF, 0));
        position += Lccvpol1.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setWadeAreaAdesioneFormatted(String data) {
        byte[] buffer = new byte[Len.WADE_AREA_ADESIONE];
        MarshalByte.writeString(buffer, 1, data, Len.WADE_AREA_ADESIONE);
        setWadeAreaAdesioneBytes(buffer, 1);
    }

    public void setWadeAreaAdesioneBytes(byte[] buffer, int offset) {
        int position = offset;
        wadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWadeTabAdesBytes(buffer, position);
    }

    public void setWadeEleAdesMax(short wadeEleAdesMax) {
        this.wadeEleAdesMax = wadeEleAdesMax;
    }

    public short getWadeEleAdesMax() {
        return this.wadeEleAdesMax;
    }

    public void setWadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1.Len.Int.ID_PTF, 0));
        position += Lccvade1.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public void setWrreAreaRappReteFormatted(String data) {
        byte[] buffer = new byte[Len.WRRE_AREA_RAPP_RETE];
        MarshalByte.writeString(buffer, 1, data, Len.WRRE_AREA_RAPP_RETE);
        setWrreAreaRappReteBytes(buffer, 1);
    }

    public void setWrreAreaRappReteBytes(byte[] buffer, int offset) {
        int position = offset;
        wrreEleRappReteMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= WRRE_TAB_RAPP_RETE_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                wrreTabRappRete[idx - 1].setWrreTabRappReteBytes(buffer, position);
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
            else {
                wrreTabRappRete[idx - 1].initWrreTabRappReteSpaces();
                position += WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;
            }
        }
    }

    public void setWrreEleRappReteMax(short wrreEleRappReteMax) {
        this.wrreEleRappReteMax = wrreEleRappReteMax;
    }

    public short getWrreEleRappReteMax() {
        return this.wrreEleRappReteMax;
    }

    public AreaCall getAreaCall() {
        return areaCall;
    }

    public AreaCallIwfs0050 getAreaCallIwfs0050() {
        return areaCallIwfs0050;
    }

    public Idss0140Link getAreaIdsv0141() {
        return areaIdsv0141;
    }

    public CompNumOgg getCompNumOgg() {
        return compNumOgg;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsi0021Area getIdsi0021Area() {
        return idsi0021Area;
    }

    public Idso0021 getIdso0021() {
        return idso0021;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ivvc0218 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndici getIxIndici() {
        return ixIndici;
    }

    public Lccvade1 getLccvade1() {
        return lccvade1;
    }

    public Lccvpol1 getLccvpol1() {
        return lccvpol1;
    }

    public NumOgg getNumOgg() {
        return numOgg;
    }

    public ProgrNumOgg getProgrNumOgg() {
        return progrNumOgg;
    }

    public RappRete getRappRete() {
        return rappRete;
    }

    public WkFindCampo getWkFindCampo() {
        return wkFindCampo;
    }

    public WksTabStrDato getWksTabStrDato() {
        return wksTabStrDato;
    }

    public WksTrovatoTabb getWksTrovatoTabb() {
        return wksTrovatoTabb;
    }

    public WrreTabRappRete getWrreTabRappRete(int idx) {
        return wrreTabRappRete[idx - 1];
    }

    public WsFlagFineValore getWsFlagFineValore() {
        return wsFlagFineValore;
    }

    public WsLettura getWsLettura() {
        return wsLettura;
    }

    public WsValoreApp getWsValoreApp() {
        return wsValoreApp;
    }

    public WsVariabiliIdss0160 getWsVariabili() {
        return wsVariabili;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WKS_APPO_IDSI0011 = 30462;
        public static final int INPUT_IDSS0020 = Idsi0021Area.Len.IDSI0021_AREA;
        public static final int WADE_ELE_ADES_MAX = 2;
        public static final int WADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int WADE_AREA_ADESIONE = WADE_ELE_ADES_MAX + WADE_TAB_ADES;
        public static final int WPOL_ELE_POLI_MAX = 2;
        public static final int WPOL_TAB_POLI = WpolStatus.Len.STATUS + Lccvpol1.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int WPOL_AREA_POLIZZA = WPOL_ELE_POLI_MAX + WPOL_TAB_POLI;
        public static final int WRRE_ELE_RAPP_RETE_MAX = 2;
        public static final int WRRE_AREA_RAPP_RETE = WRRE_ELE_RAPP_RETE_MAX + Idss0160Data.WRRE_TAB_RAPP_RETE_MAXOCCURS * WrreTabRappRete.Len.WRRE_TAB_RAPP_RETE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
