package it.accenture.jnais.ws;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program ISPS0580<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciIsps0580 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-ERR
    private short tabErr = ((short)0);

    //==== METHODS ====
    public void setTabErr(short tabErr) {
        this.tabErr = tabErr;
    }

    public short getTabErr() {
        return this.tabErr;
    }
}
