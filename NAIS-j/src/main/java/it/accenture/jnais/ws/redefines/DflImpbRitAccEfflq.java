package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-RIT-ACC-EFFLQ<br>
 * Variable: DFL-IMPB-RIT-ACC-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbRitAccEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbRitAccEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_RIT_ACC_EFFLQ;
    }

    public void setDflImpbRitAccEfflq(AfDecimal dflImpbRitAccEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPB_RIT_ACC_EFFLQ, dflImpbRitAccEfflq.copy());
    }

    public void setDflImpbRitAccEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_RIT_ACC_EFFLQ, Pos.DFL_IMPB_RIT_ACC_EFFLQ);
    }

    /**Original name: DFL-IMPB-RIT-ACC-EFFLQ<br>*/
    public AfDecimal getDflImpbRitAccEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPB_RIT_ACC_EFFLQ, Len.Int.DFL_IMPB_RIT_ACC_EFFLQ, Len.Fract.DFL_IMPB_RIT_ACC_EFFLQ);
    }

    public byte[] getDflImpbRitAccEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_RIT_ACC_EFFLQ, Pos.DFL_IMPB_RIT_ACC_EFFLQ);
        return buffer;
    }

    public void setDflImpbRitAccEfflqNull(String dflImpbRitAccEfflqNull) {
        writeString(Pos.DFL_IMPB_RIT_ACC_EFFLQ_NULL, dflImpbRitAccEfflqNull, Len.DFL_IMPB_RIT_ACC_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPB-RIT-ACC-EFFLQ-NULL<br>*/
    public String getDflImpbRitAccEfflqNull() {
        return readString(Pos.DFL_IMPB_RIT_ACC_EFFLQ_NULL, Len.DFL_IMPB_RIT_ACC_EFFLQ_NULL);
    }

    public String getDflImpbRitAccEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpbRitAccEfflqNull(), Len.DFL_IMPB_RIT_ACC_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_RIT_ACC_EFFLQ = 1;
        public static final int DFL_IMPB_RIT_ACC_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_RIT_ACC_EFFLQ = 8;
        public static final int DFL_IMPB_RIT_ACC_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_RIT_ACC_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_RIT_ACC_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
