package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-CASO-MOR<br>
 * Variable: TGA-PRE-CASO-MOR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPreCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPreCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_CASO_MOR;
    }

    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        writeDecimalAsPacked(Pos.TGA_PRE_CASO_MOR, tgaPreCasoMor.copy());
    }

    public void setTgaPreCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_CASO_MOR, Pos.TGA_PRE_CASO_MOR);
    }

    /**Original name: TGA-PRE-CASO-MOR<br>*/
    public AfDecimal getTgaPreCasoMor() {
        return readPackedAsDecimal(Pos.TGA_PRE_CASO_MOR, Len.Int.TGA_PRE_CASO_MOR, Len.Fract.TGA_PRE_CASO_MOR);
    }

    public byte[] getTgaPreCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_CASO_MOR, Pos.TGA_PRE_CASO_MOR);
        return buffer;
    }

    public void setTgaPreCasoMorNull(String tgaPreCasoMorNull) {
        writeString(Pos.TGA_PRE_CASO_MOR_NULL, tgaPreCasoMorNull, Len.TGA_PRE_CASO_MOR_NULL);
    }

    /**Original name: TGA-PRE-CASO-MOR-NULL<br>*/
    public String getTgaPreCasoMorNull() {
        return readString(Pos.TGA_PRE_CASO_MOR_NULL, Len.TGA_PRE_CASO_MOR_NULL);
    }

    public String getTgaPreCasoMorNullFormatted() {
        return Functions.padBlanks(getTgaPreCasoMorNull(), Len.TGA_PRE_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_CASO_MOR = 1;
        public static final int TGA_PRE_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_CASO_MOR = 8;
        public static final int TGA_PRE_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
