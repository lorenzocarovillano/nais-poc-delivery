package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-SDO-INTR<br>
 * Variable: PRE-SDO-INTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreSdoIntr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreSdoIntr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_SDO_INTR;
    }

    public void setPreSdoIntr(AfDecimal preSdoIntr) {
        writeDecimalAsPacked(Pos.PRE_SDO_INTR, preSdoIntr.copy());
    }

    public void setPreSdoIntrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_SDO_INTR, Pos.PRE_SDO_INTR);
    }

    /**Original name: PRE-SDO-INTR<br>*/
    public AfDecimal getPreSdoIntr() {
        return readPackedAsDecimal(Pos.PRE_SDO_INTR, Len.Int.PRE_SDO_INTR, Len.Fract.PRE_SDO_INTR);
    }

    public byte[] getPreSdoIntrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_SDO_INTR, Pos.PRE_SDO_INTR);
        return buffer;
    }

    public void setPreSdoIntrNull(String preSdoIntrNull) {
        writeString(Pos.PRE_SDO_INTR_NULL, preSdoIntrNull, Len.PRE_SDO_INTR_NULL);
    }

    /**Original name: PRE-SDO-INTR-NULL<br>*/
    public String getPreSdoIntrNull() {
        return readString(Pos.PRE_SDO_INTR_NULL, Len.PRE_SDO_INTR_NULL);
    }

    public String getPreSdoIntrNullFormatted() {
        return Functions.padBlanks(getPreSdoIntrNull(), Len.PRE_SDO_INTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_SDO_INTR = 1;
        public static final int PRE_SDO_INTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_SDO_INTR = 8;
        public static final int PRE_SDO_INTR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_SDO_INTR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_SDO_INTR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
