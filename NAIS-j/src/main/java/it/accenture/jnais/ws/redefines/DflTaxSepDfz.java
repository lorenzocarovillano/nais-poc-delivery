package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-TAX-SEP-DFZ<br>
 * Variable: DFL-TAX-SEP-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflTaxSepDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflTaxSepDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_TAX_SEP_DFZ;
    }

    public void setDflTaxSepDfz(AfDecimal dflTaxSepDfz) {
        writeDecimalAsPacked(Pos.DFL_TAX_SEP_DFZ, dflTaxSepDfz.copy());
    }

    public void setDflTaxSepDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_TAX_SEP_DFZ, Pos.DFL_TAX_SEP_DFZ);
    }

    /**Original name: DFL-TAX-SEP-DFZ<br>*/
    public AfDecimal getDflTaxSepDfz() {
        return readPackedAsDecimal(Pos.DFL_TAX_SEP_DFZ, Len.Int.DFL_TAX_SEP_DFZ, Len.Fract.DFL_TAX_SEP_DFZ);
    }

    public byte[] getDflTaxSepDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_TAX_SEP_DFZ, Pos.DFL_TAX_SEP_DFZ);
        return buffer;
    }

    public void setDflTaxSepDfzNull(String dflTaxSepDfzNull) {
        writeString(Pos.DFL_TAX_SEP_DFZ_NULL, dflTaxSepDfzNull, Len.DFL_TAX_SEP_DFZ_NULL);
    }

    /**Original name: DFL-TAX-SEP-DFZ-NULL<br>*/
    public String getDflTaxSepDfzNull() {
        return readString(Pos.DFL_TAX_SEP_DFZ_NULL, Len.DFL_TAX_SEP_DFZ_NULL);
    }

    public String getDflTaxSepDfzNullFormatted() {
        return Functions.padBlanks(getDflTaxSepDfzNull(), Len.DFL_TAX_SEP_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_TAX_SEP_DFZ = 1;
        public static final int DFL_TAX_SEP_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_TAX_SEP_DFZ = 8;
        public static final int DFL_TAX_SEP_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_TAX_SEP_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
