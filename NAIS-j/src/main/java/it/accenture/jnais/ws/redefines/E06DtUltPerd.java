package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E06-DT-ULT-PERD<br>
 * Variable: E06-DT-ULT-PERD from program IDBSE060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E06DtUltPerd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E06DtUltPerd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E06_DT_ULT_PERD;
    }

    public void setE06DtUltPerd(int e06DtUltPerd) {
        writeIntAsPacked(Pos.E06_DT_ULT_PERD, e06DtUltPerd, Len.Int.E06_DT_ULT_PERD);
    }

    public void setE06DtUltPerdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E06_DT_ULT_PERD, Pos.E06_DT_ULT_PERD);
    }

    /**Original name: E06-DT-ULT-PERD<br>*/
    public int getE06DtUltPerd() {
        return readPackedAsInt(Pos.E06_DT_ULT_PERD, Len.Int.E06_DT_ULT_PERD);
    }

    public byte[] getE06DtUltPerdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E06_DT_ULT_PERD, Pos.E06_DT_ULT_PERD);
        return buffer;
    }

    public void setE06DtUltPerdNull(String e06DtUltPerdNull) {
        writeString(Pos.E06_DT_ULT_PERD_NULL, e06DtUltPerdNull, Len.E06_DT_ULT_PERD_NULL);
    }

    /**Original name: E06-DT-ULT-PERD-NULL<br>*/
    public String getE06DtUltPerdNull() {
        return readString(Pos.E06_DT_ULT_PERD_NULL, Len.E06_DT_ULT_PERD_NULL);
    }

    public String getE06DtUltPerdNullFormatted() {
        return Functions.padBlanks(getE06DtUltPerdNull(), Len.E06_DT_ULT_PERD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E06_DT_ULT_PERD = 1;
        public static final int E06_DT_ULT_PERD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E06_DT_ULT_PERD = 5;
        public static final int E06_DT_ULT_PERD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E06_DT_ULT_PERD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
