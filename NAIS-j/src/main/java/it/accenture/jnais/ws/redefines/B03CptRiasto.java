package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CPT-RIASTO<br>
 * Variable: B03-CPT-RIASTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CptRiasto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CptRiasto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CPT_RIASTO;
    }

    public void setB03CptRiasto(AfDecimal b03CptRiasto) {
        writeDecimalAsPacked(Pos.B03_CPT_RIASTO, b03CptRiasto.copy());
    }

    public void setB03CptRiastoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CPT_RIASTO, Pos.B03_CPT_RIASTO);
    }

    /**Original name: B03-CPT-RIASTO<br>*/
    public AfDecimal getB03CptRiasto() {
        return readPackedAsDecimal(Pos.B03_CPT_RIASTO, Len.Int.B03_CPT_RIASTO, Len.Fract.B03_CPT_RIASTO);
    }

    public byte[] getB03CptRiastoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CPT_RIASTO, Pos.B03_CPT_RIASTO);
        return buffer;
    }

    public void setB03CptRiastoNull(String b03CptRiastoNull) {
        writeString(Pos.B03_CPT_RIASTO_NULL, b03CptRiastoNull, Len.B03_CPT_RIASTO_NULL);
    }

    /**Original name: B03-CPT-RIASTO-NULL<br>*/
    public String getB03CptRiastoNull() {
        return readString(Pos.B03_CPT_RIASTO_NULL, Len.B03_CPT_RIASTO_NULL);
    }

    public String getB03CptRiastoNullFormatted() {
        return Functions.padBlanks(getB03CptRiastoNull(), Len.B03_CPT_RIASTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CPT_RIASTO = 1;
        public static final int B03_CPT_RIASTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CPT_RIASTO = 8;
        public static final int B03_CPT_RIASTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CPT_RIASTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CPT_RIASTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
