package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.redefines.WpagAlqScon;
import it.accenture.jnais.ws.redefines.WpagCptInOpzRivto;
import it.accenture.jnais.ws.redefines.WpagCptRshMor;
import it.accenture.jnais.ws.redefines.WpagDurAbbAnni;
import it.accenture.jnais.ws.redefines.WpagImpAbbAnnoUlt;
import it.accenture.jnais.ws.redefines.WpagImpAbbTotIni;
import it.accenture.jnais.ws.redefines.WpagImpAderTga;
import it.accenture.jnais.ws.redefines.WpagImpAltSopr;
import it.accenture.jnais.ws.redefines.WpagImpAzTga;
import it.accenture.jnais.ws.redefines.WpagImpBnsAntic;
import it.accenture.jnais.ws.redefines.WpagImpCarAcqTga;
import it.accenture.jnais.ws.redefines.WpagImpCarGestTga;
import it.accenture.jnais.ws.redefines.WpagImpCarIncTga;
import it.accenture.jnais.ws.redefines.WpagImpPrePattuito;
import it.accenture.jnais.ws.redefines.WpagImpPreRival;
import it.accenture.jnais.ws.redefines.WpagImpPrestUlt;
import it.accenture.jnais.ws.redefines.WpagImpPrstzAggIni;
import it.accenture.jnais.ws.redefines.WpagImpScon;
import it.accenture.jnais.ws.redefines.WpagImpSoprProf;
import it.accenture.jnais.ws.redefines.WpagImpSoprSan;
import it.accenture.jnais.ws.redefines.WpagImpSoprSpo;
import it.accenture.jnais.ws.redefines.WpagImpSoprTec;
import it.accenture.jnais.ws.redefines.WpagImpTfrTga;
import it.accenture.jnais.ws.redefines.WpagImpVoloTga;
import it.accenture.jnais.ws.redefines.WpagManfeeAntic;
import it.accenture.jnais.ws.redefines.WpagPreCasoMor;
import it.accenture.jnais.ws.redefines.WpagPreIniNet;
import it.accenture.jnais.ws.redefines.WpagPreInvrioIni;
import it.accenture.jnais.ws.redefines.WpagPreInvrioUlt;
import it.accenture.jnais.ws.redefines.WpagPreLrd;
import it.accenture.jnais.ws.redefines.WpagPrePpIni;
import it.accenture.jnais.ws.redefines.WpagPrePpUlt;
import it.accenture.jnais.ws.redefines.WpagPreTariIni;
import it.accenture.jnais.ws.redefines.WpagPreTariUlt;
import it.accenture.jnais.ws.redefines.WpagPrstzIni;
import it.accenture.jnais.ws.redefines.WpagPrstzUlt;
import it.accenture.jnais.ws.redefines.WpagRatLrd;
import it.accenture.jnais.ws.redefines.WpagTgaAliqAcq;
import it.accenture.jnais.ws.redefines.WpagTgaAliqCommisInt;
import it.accenture.jnais.ws.redefines.WpagTgaAliqIncas;
import it.accenture.jnais.ws.redefines.WpagTgaAliqRenAss;
import it.accenture.jnais.ws.redefines.WpagTgaAliqRicor;
import it.accenture.jnais.ws.redefines.WpagTgaImpAcq;
import it.accenture.jnais.ws.redefines.WpagTgaImpAcqExp;
import it.accenture.jnais.ws.redefines.WpagTgaImpbCommisInt;
import it.accenture.jnais.ws.redefines.WpagTgaImpbRenAss;
import it.accenture.jnais.ws.redefines.WpagTgaImpCommisInt;
import it.accenture.jnais.ws.redefines.WpagTgaImpIncas;
import it.accenture.jnais.ws.redefines.WpagTgaImpRenAss;
import it.accenture.jnais.ws.redefines.WpagTgaImpRicor;
import it.accenture.jnais.ws.redefines.WpagTsRivalFis;

/**Original name: WPAG-DATI-TRANCHE<br>
 * Variables: WPAG-DATI-TRANCHE from copybook LVEC0268<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpagDatiTranche {

    //==== PROPERTIES ====
    //Original name: WPAG-TP-RGM-FISC
    private String wpagTpRgmFisc = DefaultValues.stringVal(Len.WPAG_TP_RGM_FISC);
    //Original name: WPAG-TP-TRCH
    private String wpagTpTrch = DefaultValues.stringVal(Len.WPAG_TP_TRCH);
    //Original name: WPAG-PRE-CASO-MOR
    private WpagPreCasoMor wpagPreCasoMor = new WpagPreCasoMor();
    //Original name: WPAG-IMP-BNS-ANTIC
    private WpagImpBnsAntic wpagImpBnsAntic = new WpagImpBnsAntic();
    //Original name: WPAG-PRE-INI-NET
    private WpagPreIniNet wpagPreIniNet = new WpagPreIniNet();
    //Original name: WPAG-PRE-PP-INI
    private WpagPrePpIni wpagPrePpIni = new WpagPrePpIni();
    //Original name: WPAG-PRE-PP-ULT
    private WpagPrePpUlt wpagPrePpUlt = new WpagPrePpUlt();
    //Original name: WPAG-PRE-TARI-INI
    private WpagPreTariIni wpagPreTariIni = new WpagPreTariIni();
    //Original name: WPAG-PRE-TARI-ULT
    private WpagPreTariUlt wpagPreTariUlt = new WpagPreTariUlt();
    //Original name: WPAG-PRE-INVRIO-INI
    private WpagPreInvrioIni wpagPreInvrioIni = new WpagPreInvrioIni();
    //Original name: WPAG-PRE-INVRIO-ULT
    private WpagPreInvrioUlt wpagPreInvrioUlt = new WpagPreInvrioUlt();
    //Original name: WPAG-IMP-SOPR-PROF
    private WpagImpSoprProf wpagImpSoprProf = new WpagImpSoprProf();
    //Original name: WPAG-IMP-SOPR-SAN
    private WpagImpSoprSan wpagImpSoprSan = new WpagImpSoprSan();
    //Original name: WPAG-IMP-SOPR-SPO
    private WpagImpSoprSpo wpagImpSoprSpo = new WpagImpSoprSpo();
    //Original name: WPAG-IMP-SOPR-TEC
    private WpagImpSoprTec wpagImpSoprTec = new WpagImpSoprTec();
    //Original name: WPAG-IMP-ALT-SOPR
    private WpagImpAltSopr wpagImpAltSopr = new WpagImpAltSopr();
    //Original name: WPAG-TS-RIVAL-FIS
    private WpagTsRivalFis wpagTsRivalFis = new WpagTsRivalFis();
    //Original name: WPAG-RAT-LRD
    private WpagRatLrd wpagRatLrd = new WpagRatLrd();
    //Original name: WPAG-PRE-LRD
    private WpagPreLrd wpagPreLrd = new WpagPreLrd();
    //Original name: WPAG-PRSTZ-INI
    private WpagPrstzIni wpagPrstzIni = new WpagPrstzIni();
    //Original name: WPAG-PRSTZ-ULT
    private WpagPrstzUlt wpagPrstzUlt = new WpagPrstzUlt();
    //Original name: WPAG-CPT-IN-OPZ-RIVTO
    private WpagCptInOpzRivto wpagCptInOpzRivto = new WpagCptInOpzRivto();
    //Original name: WPAG-CPT-RSH-MOR
    private WpagCptRshMor wpagCptRshMor = new WpagCptRshMor();
    //Original name: WPAG-IMP-SCON
    private WpagImpScon wpagImpScon = new WpagImpScon();
    //Original name: WPAG-ALQ-SCON
    private WpagAlqScon wpagAlqScon = new WpagAlqScon();
    //Original name: WPAG-IMP-CAR-ACQ-TGA
    private WpagImpCarAcqTga wpagImpCarAcqTga = new WpagImpCarAcqTga();
    //Original name: WPAG-IMP-CAR-INC-TGA
    private WpagImpCarIncTga wpagImpCarIncTga = new WpagImpCarIncTga();
    //Original name: WPAG-IMP-CAR-GEST-TGA
    private WpagImpCarGestTga wpagImpCarGestTga = new WpagImpCarGestTga();
    //Original name: WPAG-IMP-AZ-TGA
    private WpagImpAzTga wpagImpAzTga = new WpagImpAzTga();
    //Original name: WPAG-IMP-ADER-TGA
    private WpagImpAderTga wpagImpAderTga = new WpagImpAderTga();
    //Original name: WPAG-IMP-TFR-TGA
    private WpagImpTfrTga wpagImpTfrTga = new WpagImpTfrTga();
    //Original name: WPAG-IMP-VOLO-TGA
    private WpagImpVoloTga wpagImpVoloTga = new WpagImpVoloTga();
    //Original name: WPAG-IMP-ABB-ANNO-ULT
    private WpagImpAbbAnnoUlt wpagImpAbbAnnoUlt = new WpagImpAbbAnnoUlt();
    //Original name: WPAG-IMP-ABB-TOT-INI
    private WpagImpAbbTotIni wpagImpAbbTotIni = new WpagImpAbbTotIni();
    //Original name: WPAG-DUR-ABB-ANNI
    private WpagDurAbbAnni wpagDurAbbAnni = new WpagDurAbbAnni();
    //Original name: WPAG-IMP-PRE-PATTUITO
    private WpagImpPrePattuito wpagImpPrePattuito = new WpagImpPrePattuito();
    //Original name: WPAG-IMP-PREST-ULT
    private WpagImpPrestUlt wpagImpPrestUlt = new WpagImpPrestUlt();
    //Original name: WPAG-IMP-PRE-RIVAL
    private WpagImpPreRival wpagImpPreRival = new WpagImpPreRival();
    //Original name: WPAG-TGA-ALIQ-INCAS
    private WpagTgaAliqIncas wpagTgaAliqIncas = new WpagTgaAliqIncas();
    //Original name: WPAG-TGA-ALIQ-ACQ
    private WpagTgaAliqAcq wpagTgaAliqAcq = new WpagTgaAliqAcq();
    //Original name: WPAG-TGA-ALIQ-RICOR
    private WpagTgaAliqRicor wpagTgaAliqRicor = new WpagTgaAliqRicor();
    //Original name: WPAG-TGA-IMP-INCAS
    private WpagTgaImpIncas wpagTgaImpIncas = new WpagTgaImpIncas();
    //Original name: WPAG-TGA-IMP-ACQ
    private WpagTgaImpAcq wpagTgaImpAcq = new WpagTgaImpAcq();
    //Original name: WPAG-TGA-IMP-RICOR
    private WpagTgaImpRicor wpagTgaImpRicor = new WpagTgaImpRicor();
    //Original name: WPAG-IMP-PRSTZ-AGG-INI
    private WpagImpPrstzAggIni wpagImpPrstzAggIni = new WpagImpPrstzAggIni();
    //Original name: WPAG-FL-PREL-RIS
    private String wpagFlPrelRis = DefaultValues.stringVal(Len.WPAG_FL_PREL_RIS);
    //Original name: WPAG-FL-RICALCOLO
    private String wpagFlRicalcolo = DefaultValues.stringVal(Len.WPAG_FL_RICALCOLO);
    //Original name: WPAG-MANFEE-ANTIC
    private WpagManfeeAntic wpagManfeeAntic = new WpagManfeeAntic();
    //Original name: WPAG-TGA-INT-FRAZ
    private AfDecimal wpagTgaIntFraz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WPAG-REN-INI-TS-TEC-0
    private AfDecimal wpagRenIniTsTec0 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WPAG-TGA-IMP-ACQ-EXP
    private WpagTgaImpAcqExp wpagTgaImpAcqExp = new WpagTgaImpAcqExp();
    //Original name: WPAG-TGA-IMP-REN-ASS
    private WpagTgaImpRenAss wpagTgaImpRenAss = new WpagTgaImpRenAss();
    //Original name: WPAG-TGA-IMP-COMMIS-INT
    private WpagTgaImpCommisInt wpagTgaImpCommisInt = new WpagTgaImpCommisInt();
    //Original name: WPAG-TGA-ALIQ-REN-ASS
    private WpagTgaAliqRenAss wpagTgaAliqRenAss = new WpagTgaAliqRenAss();
    //Original name: WPAG-TGA-ALIQ-COMMIS-INT
    private WpagTgaAliqCommisInt wpagTgaAliqCommisInt = new WpagTgaAliqCommisInt();
    //Original name: WPAG-TGA-IMPB-REN-ASS
    private WpagTgaImpbRenAss wpagTgaImpbRenAss = new WpagTgaImpbRenAss();
    //Original name: WPAG-TGA-IMPB-COMMIS-INT
    private WpagTgaImpbCommisInt wpagTgaImpbCommisInt = new WpagTgaImpbCommisInt();

    //==== METHODS ====
    public void setWpagDatiTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        wpagTpRgmFisc = MarshalByte.readString(buffer, position, Len.WPAG_TP_RGM_FISC);
        position += Len.WPAG_TP_RGM_FISC;
        wpagTpTrch = MarshalByte.readString(buffer, position, Len.WPAG_TP_TRCH);
        position += Len.WPAG_TP_TRCH;
        wpagPreCasoMor.setWpagPreCasoMorFromBuffer(buffer, position);
        position += WpagPreCasoMor.Len.WPAG_PRE_CASO_MOR;
        wpagImpBnsAntic.setWpagImpBnsAnticFromBuffer(buffer, position);
        position += WpagImpBnsAntic.Len.WPAG_IMP_BNS_ANTIC;
        wpagPreIniNet.setWpagPreIniNetFromBuffer(buffer, position);
        position += WpagPreIniNet.Len.WPAG_PRE_INI_NET;
        wpagPrePpIni.setWpagPrePpIniFromBuffer(buffer, position);
        position += WpagPrePpIni.Len.WPAG_PRE_PP_INI;
        wpagPrePpUlt.setWpagPrePpUltFromBuffer(buffer, position);
        position += WpagPrePpUlt.Len.WPAG_PRE_PP_ULT;
        wpagPreTariIni.setWpagPreTariIniFromBuffer(buffer, position);
        position += WpagPreTariIni.Len.WPAG_PRE_TARI_INI;
        wpagPreTariUlt.setWpagPreTariUltFromBuffer(buffer, position);
        position += WpagPreTariUlt.Len.WPAG_PRE_TARI_ULT;
        wpagPreInvrioIni.setWpagPreInvrioIniFromBuffer(buffer, position);
        position += WpagPreInvrioIni.Len.WPAG_PRE_INVRIO_INI;
        wpagPreInvrioUlt.setWpagPreInvrioUltFromBuffer(buffer, position);
        position += WpagPreInvrioUlt.Len.WPAG_PRE_INVRIO_ULT;
        wpagImpSoprProf.setWpagImpSoprProfFromBuffer(buffer, position);
        position += WpagImpSoprProf.Len.WPAG_IMP_SOPR_PROF;
        wpagImpSoprSan.setWpagImpSoprSanFromBuffer(buffer, position);
        position += WpagImpSoprSan.Len.WPAG_IMP_SOPR_SAN;
        wpagImpSoprSpo.setWpagImpSoprSpoFromBuffer(buffer, position);
        position += WpagImpSoprSpo.Len.WPAG_IMP_SOPR_SPO;
        wpagImpSoprTec.setWpagImpSoprTecFromBuffer(buffer, position);
        position += WpagImpSoprTec.Len.WPAG_IMP_SOPR_TEC;
        wpagImpAltSopr.setWpagImpAltSoprFromBuffer(buffer, position);
        position += WpagImpAltSopr.Len.WPAG_IMP_ALT_SOPR;
        wpagTsRivalFis.setWpagTsRivalFisFromBuffer(buffer, position);
        position += WpagTsRivalFis.Len.WPAG_TS_RIVAL_FIS;
        wpagRatLrd.setWpagRatLrdFromBuffer(buffer, position);
        position += WpagRatLrd.Len.WPAG_RAT_LRD;
        wpagPreLrd.setWpagPreLrdFromBuffer(buffer, position);
        position += WpagPreLrd.Len.WPAG_PRE_LRD;
        wpagPrstzIni.setWpagPrstzIniFromBuffer(buffer, position);
        position += WpagPrstzIni.Len.WPAG_PRSTZ_INI;
        wpagPrstzUlt.setWpagPrstzUltFromBuffer(buffer, position);
        position += WpagPrstzUlt.Len.WPAG_PRSTZ_ULT;
        wpagCptInOpzRivto.setWpagCptInOpzRivtoFromBuffer(buffer, position);
        position += WpagCptInOpzRivto.Len.WPAG_CPT_IN_OPZ_RIVTO;
        wpagCptRshMor.setWpagCptRshMorFromBuffer(buffer, position);
        position += WpagCptRshMor.Len.WPAG_CPT_RSH_MOR;
        wpagImpScon.setWpagImpSconFromBuffer(buffer, position);
        position += WpagImpScon.Len.WPAG_IMP_SCON;
        wpagAlqScon.setWpagAlqSconFromBuffer(buffer, position);
        position += WpagAlqScon.Len.WPAG_ALQ_SCON;
        wpagImpCarAcqTga.setWpagImpCarAcqTgaFromBuffer(buffer, position);
        position += WpagImpCarAcqTga.Len.WPAG_IMP_CAR_ACQ_TGA;
        wpagImpCarIncTga.setWpagImpCarIncTgaFromBuffer(buffer, position);
        position += WpagImpCarIncTga.Len.WPAG_IMP_CAR_INC_TGA;
        wpagImpCarGestTga.setWpagImpCarGestTgaFromBuffer(buffer, position);
        position += WpagImpCarGestTga.Len.WPAG_IMP_CAR_GEST_TGA;
        wpagImpAzTga.setWpagImpAzTgaFromBuffer(buffer, position);
        position += WpagImpAzTga.Len.WPAG_IMP_AZ_TGA;
        wpagImpAderTga.setWpagImpAderTgaFromBuffer(buffer, position);
        position += WpagImpAderTga.Len.WPAG_IMP_ADER_TGA;
        wpagImpTfrTga.setWpagImpTfrTgaFromBuffer(buffer, position);
        position += WpagImpTfrTga.Len.WPAG_IMP_TFR_TGA;
        wpagImpVoloTga.setWpagImpVoloTgaFromBuffer(buffer, position);
        position += WpagImpVoloTga.Len.WPAG_IMP_VOLO_TGA;
        wpagImpAbbAnnoUlt.setWpagImpAbbAnnoUltFromBuffer(buffer, position);
        position += WpagImpAbbAnnoUlt.Len.WPAG_IMP_ABB_ANNO_ULT;
        wpagImpAbbTotIni.setWpagImpAbbTotIniFromBuffer(buffer, position);
        position += WpagImpAbbTotIni.Len.WPAG_IMP_ABB_TOT_INI;
        wpagDurAbbAnni.setWpagDurAbbAnniFromBuffer(buffer, position);
        position += WpagDurAbbAnni.Len.WPAG_DUR_ABB_ANNI;
        wpagImpPrePattuito.setWpagImpPrePattuitoFromBuffer(buffer, position);
        position += WpagImpPrePattuito.Len.WPAG_IMP_PRE_PATTUITO;
        wpagImpPrestUlt.setWpagImpPrestUltFromBuffer(buffer, position);
        position += WpagImpPrestUlt.Len.WPAG_IMP_PREST_ULT;
        wpagImpPreRival.setWpagImpPreRivalFromBuffer(buffer, position);
        position += WpagImpPreRival.Len.WPAG_IMP_PRE_RIVAL;
        wpagTgaAliqIncas.setWpagTgaAliqIncasFromBuffer(buffer, position);
        position += WpagTgaAliqIncas.Len.WPAG_TGA_ALIQ_INCAS;
        wpagTgaAliqAcq.setWpagTgaAliqAcqFromBuffer(buffer, position);
        position += WpagTgaAliqAcq.Len.WPAG_TGA_ALIQ_ACQ;
        wpagTgaAliqRicor.setWpagTgaAliqRicorFromBuffer(buffer, position);
        position += WpagTgaAliqRicor.Len.WPAG_TGA_ALIQ_RICOR;
        wpagTgaImpIncas.setWpagTgaImpIncasFromBuffer(buffer, position);
        position += WpagTgaImpIncas.Len.WPAG_TGA_IMP_INCAS;
        wpagTgaImpAcq.setWpagTgaImpAcqFromBuffer(buffer, position);
        position += WpagTgaImpAcq.Len.WPAG_TGA_IMP_ACQ;
        wpagTgaImpRicor.setWpagTgaImpRicorFromBuffer(buffer, position);
        position += WpagTgaImpRicor.Len.WPAG_TGA_IMP_RICOR;
        wpagImpPrstzAggIni.setWpagImpPrstzAggIniFromBuffer(buffer, position);
        position += WpagImpPrstzAggIni.Len.WPAG_IMP_PRSTZ_AGG_INI;
        wpagFlPrelRis = MarshalByte.readString(buffer, position, Len.WPAG_FL_PREL_RIS);
        position += Len.WPAG_FL_PREL_RIS;
        wpagFlRicalcolo = MarshalByte.readString(buffer, position, Len.WPAG_FL_RICALCOLO);
        position += Len.WPAG_FL_RICALCOLO;
        wpagManfeeAntic.setWpagManfeeAnticFromBuffer(buffer, position);
        position += WpagManfeeAntic.Len.WPAG_MANFEE_ANTIC;
        wpagTgaIntFraz.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WPAG_TGA_INT_FRAZ, Len.Fract.WPAG_TGA_INT_FRAZ));
        position += Len.WPAG_TGA_INT_FRAZ;
        wpagRenIniTsTec0.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.WPAG_REN_INI_TS_TEC0, Len.Fract.WPAG_REN_INI_TS_TEC0));
        position += Len.WPAG_REN_INI_TS_TEC0;
        wpagTgaImpAcqExp.setWpagTgaImpAcqExpFromBuffer(buffer, position);
        position += WpagTgaImpAcqExp.Len.WPAG_TGA_IMP_ACQ_EXP;
        wpagTgaImpRenAss.setWpagTgaImpRenAssFromBuffer(buffer, position);
        position += WpagTgaImpRenAss.Len.WPAG_TGA_IMP_REN_ASS;
        wpagTgaImpCommisInt.setWpagTgaImpCommisIntFromBuffer(buffer, position);
        position += WpagTgaImpCommisInt.Len.WPAG_TGA_IMP_COMMIS_INT;
        wpagTgaAliqRenAss.setWpagTgaAliqRenAssFromBuffer(buffer, position);
        position += WpagTgaAliqRenAss.Len.WPAG_TGA_ALIQ_REN_ASS;
        wpagTgaAliqCommisInt.setWpagTgaAliqCommisIntFromBuffer(buffer, position);
        position += WpagTgaAliqCommisInt.Len.WPAG_TGA_ALIQ_COMMIS_INT;
        wpagTgaImpbRenAss.setWpagTgaImpbRenAssFromBuffer(buffer, position);
        position += WpagTgaImpbRenAss.Len.WPAG_TGA_IMPB_REN_ASS;
        wpagTgaImpbCommisInt.setWpagTgaImpbCommisIntFromBuffer(buffer, position);
    }

    public byte[] getWpagDatiTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wpagTpRgmFisc, Len.WPAG_TP_RGM_FISC);
        position += Len.WPAG_TP_RGM_FISC;
        MarshalByte.writeString(buffer, position, wpagTpTrch, Len.WPAG_TP_TRCH);
        position += Len.WPAG_TP_TRCH;
        wpagPreCasoMor.getWpagPreCasoMorAsBuffer(buffer, position);
        position += WpagPreCasoMor.Len.WPAG_PRE_CASO_MOR;
        wpagImpBnsAntic.getWpagImpBnsAnticAsBuffer(buffer, position);
        position += WpagImpBnsAntic.Len.WPAG_IMP_BNS_ANTIC;
        wpagPreIniNet.getWpagPreIniNetAsBuffer(buffer, position);
        position += WpagPreIniNet.Len.WPAG_PRE_INI_NET;
        wpagPrePpIni.getWpagPrePpIniAsBuffer(buffer, position);
        position += WpagPrePpIni.Len.WPAG_PRE_PP_INI;
        wpagPrePpUlt.getWpagPrePpUltAsBuffer(buffer, position);
        position += WpagPrePpUlt.Len.WPAG_PRE_PP_ULT;
        wpagPreTariIni.getWpagPreTariIniAsBuffer(buffer, position);
        position += WpagPreTariIni.Len.WPAG_PRE_TARI_INI;
        wpagPreTariUlt.getWpagPreTariUltAsBuffer(buffer, position);
        position += WpagPreTariUlt.Len.WPAG_PRE_TARI_ULT;
        wpagPreInvrioIni.getWpagPreInvrioIniAsBuffer(buffer, position);
        position += WpagPreInvrioIni.Len.WPAG_PRE_INVRIO_INI;
        wpagPreInvrioUlt.getWpagPreInvrioUltAsBuffer(buffer, position);
        position += WpagPreInvrioUlt.Len.WPAG_PRE_INVRIO_ULT;
        wpagImpSoprProf.getWpagImpSoprProfAsBuffer(buffer, position);
        position += WpagImpSoprProf.Len.WPAG_IMP_SOPR_PROF;
        wpagImpSoprSan.getWpagImpSoprSanAsBuffer(buffer, position);
        position += WpagImpSoprSan.Len.WPAG_IMP_SOPR_SAN;
        wpagImpSoprSpo.getWpagImpSoprSpoAsBuffer(buffer, position);
        position += WpagImpSoprSpo.Len.WPAG_IMP_SOPR_SPO;
        wpagImpSoprTec.getWpagImpSoprTecAsBuffer(buffer, position);
        position += WpagImpSoprTec.Len.WPAG_IMP_SOPR_TEC;
        wpagImpAltSopr.getWpagImpAltSoprAsBuffer(buffer, position);
        position += WpagImpAltSopr.Len.WPAG_IMP_ALT_SOPR;
        wpagTsRivalFis.getWpagTsRivalFisAsBuffer(buffer, position);
        position += WpagTsRivalFis.Len.WPAG_TS_RIVAL_FIS;
        wpagRatLrd.getWpagRatLrdAsBuffer(buffer, position);
        position += WpagRatLrd.Len.WPAG_RAT_LRD;
        wpagPreLrd.getWpagPreLrdAsBuffer(buffer, position);
        position += WpagPreLrd.Len.WPAG_PRE_LRD;
        wpagPrstzIni.getWpagPrstzIniAsBuffer(buffer, position);
        position += WpagPrstzIni.Len.WPAG_PRSTZ_INI;
        wpagPrstzUlt.getWpagPrstzUltAsBuffer(buffer, position);
        position += WpagPrstzUlt.Len.WPAG_PRSTZ_ULT;
        wpagCptInOpzRivto.getWpagCptInOpzRivtoAsBuffer(buffer, position);
        position += WpagCptInOpzRivto.Len.WPAG_CPT_IN_OPZ_RIVTO;
        wpagCptRshMor.getWpagCptRshMorAsBuffer(buffer, position);
        position += WpagCptRshMor.Len.WPAG_CPT_RSH_MOR;
        wpagImpScon.getWpagImpSconAsBuffer(buffer, position);
        position += WpagImpScon.Len.WPAG_IMP_SCON;
        wpagAlqScon.getWpagAlqSconAsBuffer(buffer, position);
        position += WpagAlqScon.Len.WPAG_ALQ_SCON;
        wpagImpCarAcqTga.getWpagImpCarAcqTgaAsBuffer(buffer, position);
        position += WpagImpCarAcqTga.Len.WPAG_IMP_CAR_ACQ_TGA;
        wpagImpCarIncTga.getWpagImpCarIncTgaAsBuffer(buffer, position);
        position += WpagImpCarIncTga.Len.WPAG_IMP_CAR_INC_TGA;
        wpagImpCarGestTga.getWpagImpCarGestTgaAsBuffer(buffer, position);
        position += WpagImpCarGestTga.Len.WPAG_IMP_CAR_GEST_TGA;
        wpagImpAzTga.getWpagImpAzTgaAsBuffer(buffer, position);
        position += WpagImpAzTga.Len.WPAG_IMP_AZ_TGA;
        wpagImpAderTga.getWpagImpAderTgaAsBuffer(buffer, position);
        position += WpagImpAderTga.Len.WPAG_IMP_ADER_TGA;
        wpagImpTfrTga.getWpagImpTfrTgaAsBuffer(buffer, position);
        position += WpagImpTfrTga.Len.WPAG_IMP_TFR_TGA;
        wpagImpVoloTga.getWpagImpVoloTgaAsBuffer(buffer, position);
        position += WpagImpVoloTga.Len.WPAG_IMP_VOLO_TGA;
        wpagImpAbbAnnoUlt.getWpagImpAbbAnnoUltAsBuffer(buffer, position);
        position += WpagImpAbbAnnoUlt.Len.WPAG_IMP_ABB_ANNO_ULT;
        wpagImpAbbTotIni.getWpagImpAbbTotIniAsBuffer(buffer, position);
        position += WpagImpAbbTotIni.Len.WPAG_IMP_ABB_TOT_INI;
        wpagDurAbbAnni.getWpagDurAbbAnniAsBuffer(buffer, position);
        position += WpagDurAbbAnni.Len.WPAG_DUR_ABB_ANNI;
        wpagImpPrePattuito.getWpagImpPrePattuitoAsBuffer(buffer, position);
        position += WpagImpPrePattuito.Len.WPAG_IMP_PRE_PATTUITO;
        wpagImpPrestUlt.getWpagImpPrestUltAsBuffer(buffer, position);
        position += WpagImpPrestUlt.Len.WPAG_IMP_PREST_ULT;
        wpagImpPreRival.getWpagImpPreRivalAsBuffer(buffer, position);
        position += WpagImpPreRival.Len.WPAG_IMP_PRE_RIVAL;
        wpagTgaAliqIncas.getWpagTgaAliqIncasAsBuffer(buffer, position);
        position += WpagTgaAliqIncas.Len.WPAG_TGA_ALIQ_INCAS;
        wpagTgaAliqAcq.getWpagTgaAliqAcqAsBuffer(buffer, position);
        position += WpagTgaAliqAcq.Len.WPAG_TGA_ALIQ_ACQ;
        wpagTgaAliqRicor.getWpagTgaAliqRicorAsBuffer(buffer, position);
        position += WpagTgaAliqRicor.Len.WPAG_TGA_ALIQ_RICOR;
        wpagTgaImpIncas.getWpagTgaImpIncasAsBuffer(buffer, position);
        position += WpagTgaImpIncas.Len.WPAG_TGA_IMP_INCAS;
        wpagTgaImpAcq.getWpagTgaImpAcqAsBuffer(buffer, position);
        position += WpagTgaImpAcq.Len.WPAG_TGA_IMP_ACQ;
        wpagTgaImpRicor.getWpagTgaImpRicorAsBuffer(buffer, position);
        position += WpagTgaImpRicor.Len.WPAG_TGA_IMP_RICOR;
        wpagImpPrstzAggIni.getWpagImpPrstzAggIniAsBuffer(buffer, position);
        position += WpagImpPrstzAggIni.Len.WPAG_IMP_PRSTZ_AGG_INI;
        MarshalByte.writeString(buffer, position, wpagFlPrelRis, Len.WPAG_FL_PREL_RIS);
        position += Len.WPAG_FL_PREL_RIS;
        MarshalByte.writeString(buffer, position, wpagFlRicalcolo, Len.WPAG_FL_RICALCOLO);
        position += Len.WPAG_FL_RICALCOLO;
        wpagManfeeAntic.getWpagManfeeAnticAsBuffer(buffer, position);
        position += WpagManfeeAntic.Len.WPAG_MANFEE_ANTIC;
        MarshalByte.writeDecimalAsPacked(buffer, position, wpagTgaIntFraz.copy());
        position += Len.WPAG_TGA_INT_FRAZ;
        MarshalByte.writeDecimalAsPacked(buffer, position, wpagRenIniTsTec0.copy());
        position += Len.WPAG_REN_INI_TS_TEC0;
        wpagTgaImpAcqExp.getWpagTgaImpAcqExpAsBuffer(buffer, position);
        position += WpagTgaImpAcqExp.Len.WPAG_TGA_IMP_ACQ_EXP;
        wpagTgaImpRenAss.getWpagTgaImpRenAssAsBuffer(buffer, position);
        position += WpagTgaImpRenAss.Len.WPAG_TGA_IMP_REN_ASS;
        wpagTgaImpCommisInt.getWpagTgaImpCommisIntAsBuffer(buffer, position);
        position += WpagTgaImpCommisInt.Len.WPAG_TGA_IMP_COMMIS_INT;
        wpagTgaAliqRenAss.getWpagTgaAliqRenAssAsBuffer(buffer, position);
        position += WpagTgaAliqRenAss.Len.WPAG_TGA_ALIQ_REN_ASS;
        wpagTgaAliqCommisInt.getWpagTgaAliqCommisIntAsBuffer(buffer, position);
        position += WpagTgaAliqCommisInt.Len.WPAG_TGA_ALIQ_COMMIS_INT;
        wpagTgaImpbRenAss.getWpagTgaImpbRenAssAsBuffer(buffer, position);
        position += WpagTgaImpbRenAss.Len.WPAG_TGA_IMPB_REN_ASS;
        wpagTgaImpbCommisInt.getWpagTgaImpbCommisIntAsBuffer(buffer, position);
        return buffer;
    }

    public void initWpagDatiTrancheSpaces() {
        wpagTpRgmFisc = "";
        wpagTpTrch = "";
        wpagPreCasoMor.initWpagPreCasoMorSpaces();
        wpagImpBnsAntic.initWpagImpBnsAnticSpaces();
        wpagPreIniNet.initWpagPreIniNetSpaces();
        wpagPrePpIni.initWpagPrePpIniSpaces();
        wpagPrePpUlt.initWpagPrePpUltSpaces();
        wpagPreTariIni.initWpagPreTariIniSpaces();
        wpagPreTariUlt.initWpagPreTariUltSpaces();
        wpagPreInvrioIni.initWpagPreInvrioIniSpaces();
        wpagPreInvrioUlt.initWpagPreInvrioUltSpaces();
        wpagImpSoprProf.initWpagImpSoprProfSpaces();
        wpagImpSoprSan.initWpagImpSoprSanSpaces();
        wpagImpSoprSpo.initWpagImpSoprSpoSpaces();
        wpagImpSoprTec.initWpagImpSoprTecSpaces();
        wpagImpAltSopr.initWpagImpAltSoprSpaces();
        wpagTsRivalFis.initWpagTsRivalFisSpaces();
        wpagRatLrd.initWpagRatLrdSpaces();
        wpagPreLrd.initWpagPreLrdSpaces();
        wpagPrstzIni.initWpagPrstzIniSpaces();
        wpagPrstzUlt.initWpagPrstzUltSpaces();
        wpagCptInOpzRivto.initWpagCptInOpzRivtoSpaces();
        wpagCptRshMor.initWpagCptRshMorSpaces();
        wpagImpScon.initWpagImpSconSpaces();
        wpagAlqScon.initWpagAlqSconSpaces();
        wpagImpCarAcqTga.initWpagImpCarAcqTgaSpaces();
        wpagImpCarIncTga.initWpagImpCarIncTgaSpaces();
        wpagImpCarGestTga.initWpagImpCarGestTgaSpaces();
        wpagImpAzTga.initWpagImpAzTgaSpaces();
        wpagImpAderTga.initWpagImpAderTgaSpaces();
        wpagImpTfrTga.initWpagImpTfrTgaSpaces();
        wpagImpVoloTga.initWpagImpVoloTgaSpaces();
        wpagImpAbbAnnoUlt.initWpagImpAbbAnnoUltSpaces();
        wpagImpAbbTotIni.initWpagImpAbbTotIniSpaces();
        wpagDurAbbAnni.initWpagDurAbbAnniSpaces();
        wpagImpPrePattuito.initWpagImpPrePattuitoSpaces();
        wpagImpPrestUlt.initWpagImpPrestUltSpaces();
        wpagImpPreRival.initWpagImpPreRivalSpaces();
        wpagTgaAliqIncas.initWpagTgaAliqIncasSpaces();
        wpagTgaAliqAcq.initWpagTgaAliqAcqSpaces();
        wpagTgaAliqRicor.initWpagTgaAliqRicorSpaces();
        wpagTgaImpIncas.initWpagTgaImpIncasSpaces();
        wpagTgaImpAcq.initWpagTgaImpAcqSpaces();
        wpagTgaImpRicor.initWpagTgaImpRicorSpaces();
        wpagImpPrstzAggIni.initWpagImpPrstzAggIniSpaces();
        wpagFlPrelRis = "";
        wpagFlRicalcolo = "";
        wpagManfeeAntic.initWpagManfeeAnticSpaces();
        wpagTgaIntFraz.setNaN();
        wpagRenIniTsTec0.setNaN();
        wpagTgaImpAcqExp.initWpagTgaImpAcqExpSpaces();
        wpagTgaImpRenAss.initWpagTgaImpRenAssSpaces();
        wpagTgaImpCommisInt.initWpagTgaImpCommisIntSpaces();
        wpagTgaAliqRenAss.initWpagTgaAliqRenAssSpaces();
        wpagTgaAliqCommisInt.initWpagTgaAliqCommisIntSpaces();
        wpagTgaImpbRenAss.initWpagTgaImpbRenAssSpaces();
        wpagTgaImpbCommisInt.initWpagTgaImpbCommisIntSpaces();
    }

    public void setWpagTpRgmFisc(String wpagTpRgmFisc) {
        this.wpagTpRgmFisc = Functions.subString(wpagTpRgmFisc, Len.WPAG_TP_RGM_FISC);
    }

    public String getWpagTpRgmFisc() {
        return this.wpagTpRgmFisc;
    }

    public void setWpagTpTrch(String wpagTpTrch) {
        this.wpagTpTrch = Functions.subString(wpagTpTrch, Len.WPAG_TP_TRCH);
    }

    public String getWpagTpTrch() {
        return this.wpagTpTrch;
    }

    public void setWpagFlPrelRis(String wpagFlPrelRis) {
        this.wpagFlPrelRis = Functions.subString(wpagFlPrelRis, Len.WPAG_FL_PREL_RIS);
    }

    public String getWpagFlPrelRis() {
        return this.wpagFlPrelRis;
    }

    public void setWpagFlRicalcolo(String wpagFlRicalcolo) {
        this.wpagFlRicalcolo = Functions.subString(wpagFlRicalcolo, Len.WPAG_FL_RICALCOLO);
    }

    public String getWpagFlRicalcolo() {
        return this.wpagFlRicalcolo;
    }

    public void setWpagTgaIntFraz(AfDecimal wpagTgaIntFraz) {
        this.wpagTgaIntFraz.assign(wpagTgaIntFraz);
    }

    public AfDecimal getWpagTgaIntFraz() {
        return this.wpagTgaIntFraz.copy();
    }

    public void setWpagRenIniTsTec0(AfDecimal wpagRenIniTsTec0) {
        this.wpagRenIniTsTec0.assign(wpagRenIniTsTec0);
    }

    public AfDecimal getWpagRenIniTsTec0() {
        return this.wpagRenIniTsTec0.copy();
    }

    public WpagAlqScon getWpagAlqScon() {
        return wpagAlqScon;
    }

    public WpagCptInOpzRivto getWpagCptInOpzRivto() {
        return wpagCptInOpzRivto;
    }

    public WpagCptRshMor getWpagCptRshMor() {
        return wpagCptRshMor;
    }

    public WpagDurAbbAnni getWpagDurAbbAnni() {
        return wpagDurAbbAnni;
    }

    public WpagImpAbbAnnoUlt getWpagImpAbbAnnoUlt() {
        return wpagImpAbbAnnoUlt;
    }

    public WpagImpAbbTotIni getWpagImpAbbTotIni() {
        return wpagImpAbbTotIni;
    }

    public WpagImpAderTga getWpagImpAderTga() {
        return wpagImpAderTga;
    }

    public WpagImpAltSopr getWpagImpAltSopr() {
        return wpagImpAltSopr;
    }

    public WpagImpAzTga getWpagImpAzTga() {
        return wpagImpAzTga;
    }

    public WpagImpBnsAntic getWpagImpBnsAntic() {
        return wpagImpBnsAntic;
    }

    public WpagImpCarAcqTga getWpagImpCarAcqTga() {
        return wpagImpCarAcqTga;
    }

    public WpagImpCarGestTga getWpagImpCarGestTga() {
        return wpagImpCarGestTga;
    }

    public WpagImpCarIncTga getWpagImpCarIncTga() {
        return wpagImpCarIncTga;
    }

    public WpagImpPrePattuito getWpagImpPrePattuito() {
        return wpagImpPrePattuito;
    }

    public WpagImpPreRival getWpagImpPreRival() {
        return wpagImpPreRival;
    }

    public WpagImpPrestUlt getWpagImpPrestUlt() {
        return wpagImpPrestUlt;
    }

    public WpagImpPrstzAggIni getWpagImpPrstzAggIni() {
        return wpagImpPrstzAggIni;
    }

    public WpagImpScon getWpagImpScon() {
        return wpagImpScon;
    }

    public WpagImpSoprProf getWpagImpSoprProf() {
        return wpagImpSoprProf;
    }

    public WpagImpSoprSan getWpagImpSoprSan() {
        return wpagImpSoprSan;
    }

    public WpagImpSoprSpo getWpagImpSoprSpo() {
        return wpagImpSoprSpo;
    }

    public WpagImpSoprTec getWpagImpSoprTec() {
        return wpagImpSoprTec;
    }

    public WpagImpTfrTga getWpagImpTfrTga() {
        return wpagImpTfrTga;
    }

    public WpagImpVoloTga getWpagImpVoloTga() {
        return wpagImpVoloTga;
    }

    public WpagManfeeAntic getWpagManfeeAntic() {
        return wpagManfeeAntic;
    }

    public WpagPreCasoMor getWpagPreCasoMor() {
        return wpagPreCasoMor;
    }

    public WpagPreIniNet getWpagPreIniNet() {
        return wpagPreIniNet;
    }

    public WpagPreInvrioIni getWpagPreInvrioIni() {
        return wpagPreInvrioIni;
    }

    public WpagPreInvrioUlt getWpagPreInvrioUlt() {
        return wpagPreInvrioUlt;
    }

    public WpagPreLrd getWpagPreLrd() {
        return wpagPreLrd;
    }

    public WpagPrePpIni getWpagPrePpIni() {
        return wpagPrePpIni;
    }

    public WpagPrePpUlt getWpagPrePpUlt() {
        return wpagPrePpUlt;
    }

    public WpagPreTariIni getWpagPreTariIni() {
        return wpagPreTariIni;
    }

    public WpagPreTariUlt getWpagPreTariUlt() {
        return wpagPreTariUlt;
    }

    public WpagPrstzIni getWpagPrstzIni() {
        return wpagPrstzIni;
    }

    public WpagPrstzUlt getWpagPrstzUlt() {
        return wpagPrstzUlt;
    }

    public WpagRatLrd getWpagRatLrd() {
        return wpagRatLrd;
    }

    public WpagTgaAliqAcq getWpagTgaAliqAcq() {
        return wpagTgaAliqAcq;
    }

    public WpagTgaAliqCommisInt getWpagTgaAliqCommisInt() {
        return wpagTgaAliqCommisInt;
    }

    public WpagTgaAliqIncas getWpagTgaAliqIncas() {
        return wpagTgaAliqIncas;
    }

    public WpagTgaAliqRenAss getWpagTgaAliqRenAss() {
        return wpagTgaAliqRenAss;
    }

    public WpagTgaAliqRicor getWpagTgaAliqRicor() {
        return wpagTgaAliqRicor;
    }

    public WpagTgaImpAcq getWpagTgaImpAcq() {
        return wpagTgaImpAcq;
    }

    public WpagTgaImpAcqExp getWpagTgaImpAcqExp() {
        return wpagTgaImpAcqExp;
    }

    public WpagTgaImpCommisInt getWpagTgaImpCommisInt() {
        return wpagTgaImpCommisInt;
    }

    public WpagTgaImpIncas getWpagTgaImpIncas() {
        return wpagTgaImpIncas;
    }

    public WpagTgaImpRenAss getWpagTgaImpRenAss() {
        return wpagTgaImpRenAss;
    }

    public WpagTgaImpRicor getWpagTgaImpRicor() {
        return wpagTgaImpRicor;
    }

    public WpagTgaImpbCommisInt getWpagTgaImpbCommisInt() {
        return wpagTgaImpbCommisInt;
    }

    public WpagTgaImpbRenAss getWpagTgaImpbRenAss() {
        return wpagTgaImpbRenAss;
    }

    public WpagTsRivalFis getWpagTsRivalFis() {
        return wpagTsRivalFis;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TP_RGM_FISC = 2;
        public static final int WPAG_TP_TRCH = 2;
        public static final int WPAG_FL_PREL_RIS = 2;
        public static final int WPAG_FL_RICALCOLO = 2;
        public static final int WPAG_TGA_INT_FRAZ = 8;
        public static final int WPAG_REN_INI_TS_TEC0 = 8;
        public static final int WPAG_DATI_TRANCHE = WPAG_TP_RGM_FISC + WPAG_TP_TRCH + WpagPreCasoMor.Len.WPAG_PRE_CASO_MOR + WpagImpBnsAntic.Len.WPAG_IMP_BNS_ANTIC + WpagPreIniNet.Len.WPAG_PRE_INI_NET + WpagPrePpIni.Len.WPAG_PRE_PP_INI + WpagPrePpUlt.Len.WPAG_PRE_PP_ULT + WpagPreTariIni.Len.WPAG_PRE_TARI_INI + WpagPreTariUlt.Len.WPAG_PRE_TARI_ULT + WpagPreInvrioIni.Len.WPAG_PRE_INVRIO_INI + WpagPreInvrioUlt.Len.WPAG_PRE_INVRIO_ULT + WpagImpSoprProf.Len.WPAG_IMP_SOPR_PROF + WpagImpSoprSan.Len.WPAG_IMP_SOPR_SAN + WpagImpSoprSpo.Len.WPAG_IMP_SOPR_SPO + WpagImpSoprTec.Len.WPAG_IMP_SOPR_TEC + WpagImpAltSopr.Len.WPAG_IMP_ALT_SOPR + WpagTsRivalFis.Len.WPAG_TS_RIVAL_FIS + WpagRatLrd.Len.WPAG_RAT_LRD + WpagPreLrd.Len.WPAG_PRE_LRD + WpagPrstzIni.Len.WPAG_PRSTZ_INI + WpagPrstzUlt.Len.WPAG_PRSTZ_ULT + WpagCptInOpzRivto.Len.WPAG_CPT_IN_OPZ_RIVTO + WpagCptRshMor.Len.WPAG_CPT_RSH_MOR + WpagImpScon.Len.WPAG_IMP_SCON + WpagAlqScon.Len.WPAG_ALQ_SCON + WpagImpCarAcqTga.Len.WPAG_IMP_CAR_ACQ_TGA + WpagImpCarIncTga.Len.WPAG_IMP_CAR_INC_TGA + WpagImpCarGestTga.Len.WPAG_IMP_CAR_GEST_TGA + WpagImpAzTga.Len.WPAG_IMP_AZ_TGA + WpagImpAderTga.Len.WPAG_IMP_ADER_TGA + WpagImpTfrTga.Len.WPAG_IMP_TFR_TGA + WpagImpVoloTga.Len.WPAG_IMP_VOLO_TGA + WpagImpAbbAnnoUlt.Len.WPAG_IMP_ABB_ANNO_ULT + WpagImpAbbTotIni.Len.WPAG_IMP_ABB_TOT_INI + WpagDurAbbAnni.Len.WPAG_DUR_ABB_ANNI + WpagImpPrePattuito.Len.WPAG_IMP_PRE_PATTUITO + WpagImpPrestUlt.Len.WPAG_IMP_PREST_ULT + WpagImpPreRival.Len.WPAG_IMP_PRE_RIVAL + WpagTgaAliqIncas.Len.WPAG_TGA_ALIQ_INCAS + WpagTgaAliqAcq.Len.WPAG_TGA_ALIQ_ACQ + WpagTgaAliqRicor.Len.WPAG_TGA_ALIQ_RICOR + WpagTgaImpIncas.Len.WPAG_TGA_IMP_INCAS + WpagTgaImpAcq.Len.WPAG_TGA_IMP_ACQ + WpagTgaImpRicor.Len.WPAG_TGA_IMP_RICOR + WpagImpPrstzAggIni.Len.WPAG_IMP_PRSTZ_AGG_INI + WPAG_FL_PREL_RIS + WPAG_FL_RICALCOLO + WpagManfeeAntic.Len.WPAG_MANFEE_ANTIC + WPAG_TGA_INT_FRAZ + WPAG_REN_INI_TS_TEC0 + WpagTgaImpAcqExp.Len.WPAG_TGA_IMP_ACQ_EXP + WpagTgaImpRenAss.Len.WPAG_TGA_IMP_REN_ASS + WpagTgaImpCommisInt.Len.WPAG_TGA_IMP_COMMIS_INT + WpagTgaAliqRenAss.Len.WPAG_TGA_ALIQ_REN_ASS + WpagTgaAliqCommisInt.Len.WPAG_TGA_ALIQ_COMMIS_INT + WpagTgaImpbRenAss.Len.WPAG_TGA_IMPB_REN_ASS + WpagTgaImpbCommisInt.Len.WPAG_TGA_IMPB_COMMIS_INT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_INT_FRAZ = 12;
            public static final int WPAG_REN_INI_TS_TEC0 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_INT_FRAZ = 3;
            public static final int WPAG_REN_INI_TS_TEC0 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
