package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-BOLLO-DETT-D<br>
 * Variable: WDFL-IMPB-BOLLO-DETT-D from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbBolloDettD extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbBolloDettD() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_BOLLO_DETT_D;
    }

    public void setWdflImpbBolloDettD(AfDecimal wdflImpbBolloDettD) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_BOLLO_DETT_D, wdflImpbBolloDettD.copy());
    }

    public void setWdflImpbBolloDettDFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_BOLLO_DETT_D, Pos.WDFL_IMPB_BOLLO_DETT_D);
    }

    /**Original name: WDFL-IMPB-BOLLO-DETT-D<br>*/
    public AfDecimal getWdflImpbBolloDettD() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_BOLLO_DETT_D, Len.Int.WDFL_IMPB_BOLLO_DETT_D, Len.Fract.WDFL_IMPB_BOLLO_DETT_D);
    }

    public byte[] getWdflImpbBolloDettDAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_BOLLO_DETT_D, Pos.WDFL_IMPB_BOLLO_DETT_D);
        return buffer;
    }

    public void setWdflImpbBolloDettDNull(String wdflImpbBolloDettDNull) {
        writeString(Pos.WDFL_IMPB_BOLLO_DETT_D_NULL, wdflImpbBolloDettDNull, Len.WDFL_IMPB_BOLLO_DETT_D_NULL);
    }

    /**Original name: WDFL-IMPB-BOLLO-DETT-D-NULL<br>*/
    public String getWdflImpbBolloDettDNull() {
        return readString(Pos.WDFL_IMPB_BOLLO_DETT_D_NULL, Len.WDFL_IMPB_BOLLO_DETT_D_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_BOLLO_DETT_D = 1;
        public static final int WDFL_IMPB_BOLLO_DETT_D_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_BOLLO_DETT_D = 8;
        public static final int WDFL_IMPB_BOLLO_DETT_D_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_BOLLO_DETT_D = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_BOLLO_DETT_D = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
