package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PRE-ATT-DI-TRCH<br>
 * Variable: L3421-PRE-ATT-DI-TRCH from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PreAttDiTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PreAttDiTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PRE_ATT_DI_TRCH;
    }

    public void setL3421PreAttDiTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PRE_ATT_DI_TRCH, Pos.L3421_PRE_ATT_DI_TRCH);
    }

    /**Original name: L3421-PRE-ATT-DI-TRCH<br>*/
    public AfDecimal getL3421PreAttDiTrch() {
        return readPackedAsDecimal(Pos.L3421_PRE_ATT_DI_TRCH, Len.Int.L3421_PRE_ATT_DI_TRCH, Len.Fract.L3421_PRE_ATT_DI_TRCH);
    }

    public byte[] getL3421PreAttDiTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PRE_ATT_DI_TRCH, Pos.L3421_PRE_ATT_DI_TRCH);
        return buffer;
    }

    /**Original name: L3421-PRE-ATT-DI-TRCH-NULL<br>*/
    public String getL3421PreAttDiTrchNull() {
        return readString(Pos.L3421_PRE_ATT_DI_TRCH_NULL, Len.L3421_PRE_ATT_DI_TRCH_NULL);
    }

    public String getL3421PreAttDiTrchNullFormatted() {
        return Functions.padBlanks(getL3421PreAttDiTrchNull(), Len.L3421_PRE_ATT_DI_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PRE_ATT_DI_TRCH = 1;
        public static final int L3421_PRE_ATT_DI_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PRE_ATT_DI_TRCH = 8;
        public static final int L3421_PRE_ATT_DI_TRCH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PRE_ATT_DI_TRCH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PRE_ATT_DI_TRCH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
