package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IAS-RST-DPST<br>
 * Variable: TLI-IAS-RST-DPST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliIasRstDpst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliIasRstDpst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IAS_RST_DPST;
    }

    public void setTliIasRstDpst(AfDecimal tliIasRstDpst) {
        writeDecimalAsPacked(Pos.TLI_IAS_RST_DPST, tliIasRstDpst.copy());
    }

    public void setTliIasRstDpstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IAS_RST_DPST, Pos.TLI_IAS_RST_DPST);
    }

    /**Original name: TLI-IAS-RST-DPST<br>*/
    public AfDecimal getTliIasRstDpst() {
        return readPackedAsDecimal(Pos.TLI_IAS_RST_DPST, Len.Int.TLI_IAS_RST_DPST, Len.Fract.TLI_IAS_RST_DPST);
    }

    public byte[] getTliIasRstDpstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IAS_RST_DPST, Pos.TLI_IAS_RST_DPST);
        return buffer;
    }

    public void setTliIasRstDpstNull(String tliIasRstDpstNull) {
        writeString(Pos.TLI_IAS_RST_DPST_NULL, tliIasRstDpstNull, Len.TLI_IAS_RST_DPST_NULL);
    }

    /**Original name: TLI-IAS-RST-DPST-NULL<br>*/
    public String getTliIasRstDpstNull() {
        return readString(Pos.TLI_IAS_RST_DPST_NULL, Len.TLI_IAS_RST_DPST_NULL);
    }

    public String getTliIasRstDpstNullFormatted() {
        return Functions.padBlanks(getTliIasRstDpstNull(), Len.TLI_IAS_RST_DPST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IAS_RST_DPST = 1;
        public static final int TLI_IAS_RST_DPST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IAS_RST_DPST = 8;
        public static final int TLI_IAS_RST_DPST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IAS_RST_DPST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IAS_RST_DPST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
