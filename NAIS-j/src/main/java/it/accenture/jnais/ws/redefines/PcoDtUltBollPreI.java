package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-PRE-I<br>
 * Variable: PCO-DT-ULT-BOLL-PRE-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollPreI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollPreI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_PRE_I;
    }

    public void setPcoDtUltBollPreI(int pcoDtUltBollPreI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_PRE_I, pcoDtUltBollPreI, Len.Int.PCO_DT_ULT_BOLL_PRE_I);
    }

    public void setPcoDtUltBollPreIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PRE_I, Pos.PCO_DT_ULT_BOLL_PRE_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-PRE-I<br>*/
    public int getPcoDtUltBollPreI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_PRE_I, Len.Int.PCO_DT_ULT_BOLL_PRE_I);
    }

    public byte[] getPcoDtUltBollPreIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_PRE_I, Pos.PCO_DT_ULT_BOLL_PRE_I);
        return buffer;
    }

    public void initPcoDtUltBollPreIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_PRE_I, Len.PCO_DT_ULT_BOLL_PRE_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollPreINull(String pcoDtUltBollPreINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_PRE_I_NULL, pcoDtUltBollPreINull, Len.PCO_DT_ULT_BOLL_PRE_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-PRE-I-NULL<br>*/
    public String getPcoDtUltBollPreINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_PRE_I_NULL, Len.PCO_DT_ULT_BOLL_PRE_I_NULL);
    }

    public String getPcoDtUltBollPreINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollPreINull(), Len.PCO_DT_ULT_BOLL_PRE_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PRE_I = 1;
        public static final int PCO_DT_ULT_BOLL_PRE_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_PRE_I = 5;
        public static final int PCO_DT_ULT_BOLL_PRE_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_PRE_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
