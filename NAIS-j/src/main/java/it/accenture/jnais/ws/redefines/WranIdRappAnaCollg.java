package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRAN-ID-RAPP-ANA-COLLG<br>
 * Variable: WRAN-ID-RAPP-ANA-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WranIdRappAnaCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WranIdRappAnaCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_ID_RAPP_ANA_COLLG;
    }

    public void setWranIdRappAnaCollg(int wranIdRappAnaCollg) {
        writeIntAsPacked(Pos.WRAN_ID_RAPP_ANA_COLLG, wranIdRappAnaCollg, Len.Int.WRAN_ID_RAPP_ANA_COLLG);
    }

    public void setWranIdRappAnaCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRAN_ID_RAPP_ANA_COLLG, Pos.WRAN_ID_RAPP_ANA_COLLG);
    }

    /**Original name: WRAN-ID-RAPP-ANA-COLLG<br>*/
    public int getWranIdRappAnaCollg() {
        return readPackedAsInt(Pos.WRAN_ID_RAPP_ANA_COLLG, Len.Int.WRAN_ID_RAPP_ANA_COLLG);
    }

    public byte[] getWranIdRappAnaCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRAN_ID_RAPP_ANA_COLLG, Pos.WRAN_ID_RAPP_ANA_COLLG);
        return buffer;
    }

    public void initWranIdRappAnaCollgSpaces() {
        fill(Pos.WRAN_ID_RAPP_ANA_COLLG, Len.WRAN_ID_RAPP_ANA_COLLG, Types.SPACE_CHAR);
    }

    public void setWranIdRappAnaCollgNull(String wranIdRappAnaCollgNull) {
        writeString(Pos.WRAN_ID_RAPP_ANA_COLLG_NULL, wranIdRappAnaCollgNull, Len.WRAN_ID_RAPP_ANA_COLLG_NULL);
    }

    /**Original name: WRAN-ID-RAPP-ANA-COLLG-NULL<br>*/
    public String getWranIdRappAnaCollgNull() {
        return readString(Pos.WRAN_ID_RAPP_ANA_COLLG_NULL, Len.WRAN_ID_RAPP_ANA_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRAN_ID_RAPP_ANA_COLLG = 1;
        public static final int WRAN_ID_RAPP_ANA_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRAN_ID_RAPP_ANA_COLLG = 5;
        public static final int WRAN_ID_RAPP_ANA_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRAN_ID_RAPP_ANA_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
