package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CAR-GEST<br>
 * Variable: W-B03-CAR-GEST from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CarGestLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CarGestLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CAR_GEST;
    }

    public void setwB03CarGest(AfDecimal wB03CarGest) {
        writeDecimalAsPacked(Pos.W_B03_CAR_GEST, wB03CarGest.copy());
    }

    /**Original name: W-B03-CAR-GEST<br>*/
    public AfDecimal getwB03CarGest() {
        return readPackedAsDecimal(Pos.W_B03_CAR_GEST, Len.Int.W_B03_CAR_GEST, Len.Fract.W_B03_CAR_GEST);
    }

    public byte[] getwB03CarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CAR_GEST, Pos.W_B03_CAR_GEST);
        return buffer;
    }

    public void setwB03CarGestNull(String wB03CarGestNull) {
        writeString(Pos.W_B03_CAR_GEST_NULL, wB03CarGestNull, Len.W_B03_CAR_GEST_NULL);
    }

    /**Original name: W-B03-CAR-GEST-NULL<br>*/
    public String getwB03CarGestNull() {
        return readString(Pos.W_B03_CAR_GEST_NULL, Len.W_B03_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_GEST = 1;
        public static final int W_B03_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CAR_GEST = 8;
        public static final int W_B03_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
