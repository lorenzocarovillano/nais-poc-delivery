package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-INTR-PREST-CALC<br>
 * Variable: WDFL-INTR-PREST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIntrPrestCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIntrPrestCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_INTR_PREST_CALC;
    }

    public void setWdflIntrPrestCalc(AfDecimal wdflIntrPrestCalc) {
        writeDecimalAsPacked(Pos.WDFL_INTR_PREST_CALC, wdflIntrPrestCalc.copy());
    }

    public void setWdflIntrPrestCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_INTR_PREST_CALC, Pos.WDFL_INTR_PREST_CALC);
    }

    /**Original name: WDFL-INTR-PREST-CALC<br>*/
    public AfDecimal getWdflIntrPrestCalc() {
        return readPackedAsDecimal(Pos.WDFL_INTR_PREST_CALC, Len.Int.WDFL_INTR_PREST_CALC, Len.Fract.WDFL_INTR_PREST_CALC);
    }

    public byte[] getWdflIntrPrestCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_INTR_PREST_CALC, Pos.WDFL_INTR_PREST_CALC);
        return buffer;
    }

    public void setWdflIntrPrestCalcNull(String wdflIntrPrestCalcNull) {
        writeString(Pos.WDFL_INTR_PREST_CALC_NULL, wdflIntrPrestCalcNull, Len.WDFL_INTR_PREST_CALC_NULL);
    }

    /**Original name: WDFL-INTR-PREST-CALC-NULL<br>*/
    public String getWdflIntrPrestCalcNull() {
        return readString(Pos.WDFL_INTR_PREST_CALC_NULL, Len.WDFL_INTR_PREST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_INTR_PREST_CALC = 1;
        public static final int WDFL_INTR_PREST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_INTR_PREST_CALC = 8;
        public static final int WDFL_INTR_PREST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_INTR_PREST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_INTR_PREST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
