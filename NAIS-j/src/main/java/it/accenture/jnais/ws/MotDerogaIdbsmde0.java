package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.MdeIdMoviChiu;

/**Original name: MOT-DEROGA<br>
 * Variable: MOT-DEROGA from copybook IDBVMDE1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class MotDerogaIdbsmde0 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: MDE-ID-MOT-DEROGA
    private int mdeIdMotDeroga = DefaultValues.INT_VAL;
    //Original name: MDE-ID-OGG-DEROGA
    private int mdeIdOggDeroga = DefaultValues.INT_VAL;
    //Original name: MDE-ID-MOVI-CRZ
    private int mdeIdMoviCrz = DefaultValues.INT_VAL;
    //Original name: MDE-ID-MOVI-CHIU
    private MdeIdMoviChiu mdeIdMoviChiu = new MdeIdMoviChiu();
    //Original name: MDE-DT-INI-EFF
    private int mdeDtIniEff = DefaultValues.INT_VAL;
    //Original name: MDE-DT-END-EFF
    private int mdeDtEndEff = DefaultValues.INT_VAL;
    //Original name: MDE-COD-COMP-ANIA
    private int mdeCodCompAnia = DefaultValues.INT_VAL;
    //Original name: MDE-TP-MOT-DEROGA
    private String mdeTpMotDeroga = DefaultValues.stringVal(Len.MDE_TP_MOT_DEROGA);
    //Original name: MDE-COD-LIV-AUT
    private int mdeCodLivAut = DefaultValues.INT_VAL;
    //Original name: MDE-COD-ERR
    private String mdeCodErr = DefaultValues.stringVal(Len.MDE_COD_ERR);
    //Original name: MDE-TP-ERR
    private String mdeTpErr = DefaultValues.stringVal(Len.MDE_TP_ERR);
    //Original name: MDE-DESC-ERR-BREVE-LEN
    private short mdeDescErrBreveLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: MDE-DESC-ERR-BREVE
    private String mdeDescErrBreve = DefaultValues.stringVal(Len.MDE_DESC_ERR_BREVE);
    //Original name: MDE-DESC-ERR-EST-LEN
    private short mdeDescErrEstLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: MDE-DESC-ERR-EST
    private String mdeDescErrEst = DefaultValues.stringVal(Len.MDE_DESC_ERR_EST);
    //Original name: MDE-DS-RIGA
    private long mdeDsRiga = DefaultValues.LONG_VAL;
    //Original name: MDE-DS-OPER-SQL
    private char mdeDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: MDE-DS-VER
    private int mdeDsVer = DefaultValues.INT_VAL;
    //Original name: MDE-DS-TS-INI-CPTZ
    private long mdeDsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: MDE-DS-TS-END-CPTZ
    private long mdeDsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: MDE-DS-UTENTE
    private String mdeDsUtente = DefaultValues.stringVal(Len.MDE_DS_UTENTE);
    //Original name: MDE-DS-STATO-ELAB
    private char mdeDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOT_DEROGA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMotDerogaBytes(buf);
    }

    public void setMotDerogaFormatted(String data) {
        byte[] buffer = new byte[Len.MOT_DEROGA];
        MarshalByte.writeString(buffer, 1, data, Len.MOT_DEROGA);
        setMotDerogaBytes(buffer, 1);
    }

    public String getMotDerogaFormatted() {
        return MarshalByteExt.bufferToStr(getMotDerogaBytes());
    }

    public void setMotDerogaBytes(byte[] buffer) {
        setMotDerogaBytes(buffer, 1);
    }

    public byte[] getMotDerogaBytes() {
        byte[] buffer = new byte[Len.MOT_DEROGA];
        return getMotDerogaBytes(buffer, 1);
    }

    public void setMotDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        mdeIdMotDeroga = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_ID_MOT_DEROGA, 0);
        position += Len.MDE_ID_MOT_DEROGA;
        mdeIdOggDeroga = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_ID_OGG_DEROGA, 0);
        position += Len.MDE_ID_OGG_DEROGA;
        mdeIdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_ID_MOVI_CRZ, 0);
        position += Len.MDE_ID_MOVI_CRZ;
        mdeIdMoviChiu.setMdeIdMoviChiuFromBuffer(buffer, position);
        position += MdeIdMoviChiu.Len.MDE_ID_MOVI_CHIU;
        mdeDtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_DT_INI_EFF, 0);
        position += Len.MDE_DT_INI_EFF;
        mdeDtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_DT_END_EFF, 0);
        position += Len.MDE_DT_END_EFF;
        mdeCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_COD_COMP_ANIA, 0);
        position += Len.MDE_COD_COMP_ANIA;
        mdeTpMotDeroga = MarshalByte.readString(buffer, position, Len.MDE_TP_MOT_DEROGA);
        position += Len.MDE_TP_MOT_DEROGA;
        mdeCodLivAut = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_COD_LIV_AUT, 0);
        position += Len.MDE_COD_LIV_AUT;
        mdeCodErr = MarshalByte.readString(buffer, position, Len.MDE_COD_ERR);
        position += Len.MDE_COD_ERR;
        mdeTpErr = MarshalByte.readString(buffer, position, Len.MDE_TP_ERR);
        position += Len.MDE_TP_ERR;
        setMdeDescErrBreveVcharBytes(buffer, position);
        position += Len.MDE_DESC_ERR_BREVE_VCHAR;
        setMdeDescErrEstVcharBytes(buffer, position);
        position += Len.MDE_DESC_ERR_EST_VCHAR;
        mdeDsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MDE_DS_RIGA, 0);
        position += Len.MDE_DS_RIGA;
        mdeDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mdeDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MDE_DS_VER, 0);
        position += Len.MDE_DS_VER;
        mdeDsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MDE_DS_TS_INI_CPTZ, 0);
        position += Len.MDE_DS_TS_INI_CPTZ;
        mdeDsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MDE_DS_TS_END_CPTZ, 0);
        position += Len.MDE_DS_TS_END_CPTZ;
        mdeDsUtente = MarshalByte.readString(buffer, position, Len.MDE_DS_UTENTE);
        position += Len.MDE_DS_UTENTE;
        mdeDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getMotDerogaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, mdeIdMotDeroga, Len.Int.MDE_ID_MOT_DEROGA, 0);
        position += Len.MDE_ID_MOT_DEROGA;
        MarshalByte.writeIntAsPacked(buffer, position, mdeIdOggDeroga, Len.Int.MDE_ID_OGG_DEROGA, 0);
        position += Len.MDE_ID_OGG_DEROGA;
        MarshalByte.writeIntAsPacked(buffer, position, mdeIdMoviCrz, Len.Int.MDE_ID_MOVI_CRZ, 0);
        position += Len.MDE_ID_MOVI_CRZ;
        mdeIdMoviChiu.getMdeIdMoviChiuAsBuffer(buffer, position);
        position += MdeIdMoviChiu.Len.MDE_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, mdeDtIniEff, Len.Int.MDE_DT_INI_EFF, 0);
        position += Len.MDE_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, mdeDtEndEff, Len.Int.MDE_DT_END_EFF, 0);
        position += Len.MDE_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, mdeCodCompAnia, Len.Int.MDE_COD_COMP_ANIA, 0);
        position += Len.MDE_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, mdeTpMotDeroga, Len.MDE_TP_MOT_DEROGA);
        position += Len.MDE_TP_MOT_DEROGA;
        MarshalByte.writeIntAsPacked(buffer, position, mdeCodLivAut, Len.Int.MDE_COD_LIV_AUT, 0);
        position += Len.MDE_COD_LIV_AUT;
        MarshalByte.writeString(buffer, position, mdeCodErr, Len.MDE_COD_ERR);
        position += Len.MDE_COD_ERR;
        MarshalByte.writeString(buffer, position, mdeTpErr, Len.MDE_TP_ERR);
        position += Len.MDE_TP_ERR;
        getMdeDescErrBreveVcharBytes(buffer, position);
        position += Len.MDE_DESC_ERR_BREVE_VCHAR;
        getMdeDescErrEstVcharBytes(buffer, position);
        position += Len.MDE_DESC_ERR_EST_VCHAR;
        MarshalByte.writeLongAsPacked(buffer, position, mdeDsRiga, Len.Int.MDE_DS_RIGA, 0);
        position += Len.MDE_DS_RIGA;
        MarshalByte.writeChar(buffer, position, mdeDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, mdeDsVer, Len.Int.MDE_DS_VER, 0);
        position += Len.MDE_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, mdeDsTsIniCptz, Len.Int.MDE_DS_TS_INI_CPTZ, 0);
        position += Len.MDE_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, mdeDsTsEndCptz, Len.Int.MDE_DS_TS_END_CPTZ, 0);
        position += Len.MDE_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, mdeDsUtente, Len.MDE_DS_UTENTE);
        position += Len.MDE_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, mdeDsStatoElab);
        return buffer;
    }

    public void setMdeIdMotDeroga(int mdeIdMotDeroga) {
        this.mdeIdMotDeroga = mdeIdMotDeroga;
    }

    public int getMdeIdMotDeroga() {
        return this.mdeIdMotDeroga;
    }

    public void setMdeIdOggDeroga(int mdeIdOggDeroga) {
        this.mdeIdOggDeroga = mdeIdOggDeroga;
    }

    public int getMdeIdOggDeroga() {
        return this.mdeIdOggDeroga;
    }

    public void setMdeIdMoviCrz(int mdeIdMoviCrz) {
        this.mdeIdMoviCrz = mdeIdMoviCrz;
    }

    public int getMdeIdMoviCrz() {
        return this.mdeIdMoviCrz;
    }

    public void setMdeDtIniEff(int mdeDtIniEff) {
        this.mdeDtIniEff = mdeDtIniEff;
    }

    public int getMdeDtIniEff() {
        return this.mdeDtIniEff;
    }

    public void setMdeDtEndEff(int mdeDtEndEff) {
        this.mdeDtEndEff = mdeDtEndEff;
    }

    public int getMdeDtEndEff() {
        return this.mdeDtEndEff;
    }

    public void setMdeCodCompAnia(int mdeCodCompAnia) {
        this.mdeCodCompAnia = mdeCodCompAnia;
    }

    public int getMdeCodCompAnia() {
        return this.mdeCodCompAnia;
    }

    public void setMdeTpMotDeroga(String mdeTpMotDeroga) {
        this.mdeTpMotDeroga = Functions.subString(mdeTpMotDeroga, Len.MDE_TP_MOT_DEROGA);
    }

    public String getMdeTpMotDeroga() {
        return this.mdeTpMotDeroga;
    }

    public void setMdeCodLivAut(int mdeCodLivAut) {
        this.mdeCodLivAut = mdeCodLivAut;
    }

    public int getMdeCodLivAut() {
        return this.mdeCodLivAut;
    }

    public void setMdeCodErr(String mdeCodErr) {
        this.mdeCodErr = Functions.subString(mdeCodErr, Len.MDE_COD_ERR);
    }

    public String getMdeCodErr() {
        return this.mdeCodErr;
    }

    public void setMdeTpErr(String mdeTpErr) {
        this.mdeTpErr = Functions.subString(mdeTpErr, Len.MDE_TP_ERR);
    }

    public String getMdeTpErr() {
        return this.mdeTpErr;
    }

    public void setMdeDescErrBreveVcharFormatted(String data) {
        byte[] buffer = new byte[Len.MDE_DESC_ERR_BREVE_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.MDE_DESC_ERR_BREVE_VCHAR);
        setMdeDescErrBreveVcharBytes(buffer, 1);
    }

    public String getMdeDescErrBreveVcharFormatted() {
        return MarshalByteExt.bufferToStr(getMdeDescErrBreveVcharBytes());
    }

    /**Original name: MDE-DESC-ERR-BREVE-VCHAR<br>*/
    public byte[] getMdeDescErrBreveVcharBytes() {
        byte[] buffer = new byte[Len.MDE_DESC_ERR_BREVE_VCHAR];
        return getMdeDescErrBreveVcharBytes(buffer, 1);
    }

    public void setMdeDescErrBreveVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        mdeDescErrBreveLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        mdeDescErrBreve = MarshalByte.readString(buffer, position, Len.MDE_DESC_ERR_BREVE);
    }

    public byte[] getMdeDescErrBreveVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, mdeDescErrBreveLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, mdeDescErrBreve, Len.MDE_DESC_ERR_BREVE);
        return buffer;
    }

    public void setMdeDescErrBreveLen(short mdeDescErrBreveLen) {
        this.mdeDescErrBreveLen = mdeDescErrBreveLen;
    }

    public short getMdeDescErrBreveLen() {
        return this.mdeDescErrBreveLen;
    }

    public void setMdeDescErrBreve(String mdeDescErrBreve) {
        this.mdeDescErrBreve = Functions.subString(mdeDescErrBreve, Len.MDE_DESC_ERR_BREVE);
    }

    public String getMdeDescErrBreve() {
        return this.mdeDescErrBreve;
    }

    public void setMdeDescErrEstVcharFormatted(String data) {
        byte[] buffer = new byte[Len.MDE_DESC_ERR_EST_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.MDE_DESC_ERR_EST_VCHAR);
        setMdeDescErrEstVcharBytes(buffer, 1);
    }

    public String getMdeDescErrEstVcharFormatted() {
        return MarshalByteExt.bufferToStr(getMdeDescErrEstVcharBytes());
    }

    /**Original name: MDE-DESC-ERR-EST-VCHAR<br>*/
    public byte[] getMdeDescErrEstVcharBytes() {
        byte[] buffer = new byte[Len.MDE_DESC_ERR_EST_VCHAR];
        return getMdeDescErrEstVcharBytes(buffer, 1);
    }

    public void setMdeDescErrEstVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        mdeDescErrEstLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        mdeDescErrEst = MarshalByte.readString(buffer, position, Len.MDE_DESC_ERR_EST);
    }

    public byte[] getMdeDescErrEstVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, mdeDescErrEstLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, mdeDescErrEst, Len.MDE_DESC_ERR_EST);
        return buffer;
    }

    public void setMdeDescErrEstLen(short mdeDescErrEstLen) {
        this.mdeDescErrEstLen = mdeDescErrEstLen;
    }

    public short getMdeDescErrEstLen() {
        return this.mdeDescErrEstLen;
    }

    public void setMdeDescErrEst(String mdeDescErrEst) {
        this.mdeDescErrEst = Functions.subString(mdeDescErrEst, Len.MDE_DESC_ERR_EST);
    }

    public String getMdeDescErrEst() {
        return this.mdeDescErrEst;
    }

    public void setMdeDsRiga(long mdeDsRiga) {
        this.mdeDsRiga = mdeDsRiga;
    }

    public long getMdeDsRiga() {
        return this.mdeDsRiga;
    }

    public void setMdeDsOperSql(char mdeDsOperSql) {
        this.mdeDsOperSql = mdeDsOperSql;
    }

    public void setMdeDsOperSqlFormatted(String mdeDsOperSql) {
        setMdeDsOperSql(Functions.charAt(mdeDsOperSql, Types.CHAR_SIZE));
    }

    public char getMdeDsOperSql() {
        return this.mdeDsOperSql;
    }

    public void setMdeDsVer(int mdeDsVer) {
        this.mdeDsVer = mdeDsVer;
    }

    public int getMdeDsVer() {
        return this.mdeDsVer;
    }

    public void setMdeDsTsIniCptz(long mdeDsTsIniCptz) {
        this.mdeDsTsIniCptz = mdeDsTsIniCptz;
    }

    public long getMdeDsTsIniCptz() {
        return this.mdeDsTsIniCptz;
    }

    public void setMdeDsTsEndCptz(long mdeDsTsEndCptz) {
        this.mdeDsTsEndCptz = mdeDsTsEndCptz;
    }

    public long getMdeDsTsEndCptz() {
        return this.mdeDsTsEndCptz;
    }

    public void setMdeDsUtente(String mdeDsUtente) {
        this.mdeDsUtente = Functions.subString(mdeDsUtente, Len.MDE_DS_UTENTE);
    }

    public String getMdeDsUtente() {
        return this.mdeDsUtente;
    }

    public void setMdeDsStatoElab(char mdeDsStatoElab) {
        this.mdeDsStatoElab = mdeDsStatoElab;
    }

    public void setMdeDsStatoElabFormatted(String mdeDsStatoElab) {
        setMdeDsStatoElab(Functions.charAt(mdeDsStatoElab, Types.CHAR_SIZE));
    }

    public char getMdeDsStatoElab() {
        return this.mdeDsStatoElab;
    }

    public MdeIdMoviChiu getMdeIdMoviChiu() {
        return mdeIdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getMotDerogaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MDE_ID_MOT_DEROGA = 5;
        public static final int MDE_ID_OGG_DEROGA = 5;
        public static final int MDE_ID_MOVI_CRZ = 5;
        public static final int MDE_DT_INI_EFF = 5;
        public static final int MDE_DT_END_EFF = 5;
        public static final int MDE_COD_COMP_ANIA = 3;
        public static final int MDE_TP_MOT_DEROGA = 2;
        public static final int MDE_COD_LIV_AUT = 3;
        public static final int MDE_COD_ERR = 12;
        public static final int MDE_TP_ERR = 2;
        public static final int MDE_DESC_ERR_BREVE_LEN = 2;
        public static final int MDE_DESC_ERR_BREVE = 100;
        public static final int MDE_DESC_ERR_BREVE_VCHAR = MDE_DESC_ERR_BREVE_LEN + MDE_DESC_ERR_BREVE;
        public static final int MDE_DESC_ERR_EST_LEN = 2;
        public static final int MDE_DESC_ERR_EST = 250;
        public static final int MDE_DESC_ERR_EST_VCHAR = MDE_DESC_ERR_EST_LEN + MDE_DESC_ERR_EST;
        public static final int MDE_DS_RIGA = 6;
        public static final int MDE_DS_OPER_SQL = 1;
        public static final int MDE_DS_VER = 5;
        public static final int MDE_DS_TS_INI_CPTZ = 10;
        public static final int MDE_DS_TS_END_CPTZ = 10;
        public static final int MDE_DS_UTENTE = 20;
        public static final int MDE_DS_STATO_ELAB = 1;
        public static final int MOT_DEROGA = MDE_ID_MOT_DEROGA + MDE_ID_OGG_DEROGA + MDE_ID_MOVI_CRZ + MdeIdMoviChiu.Len.MDE_ID_MOVI_CHIU + MDE_DT_INI_EFF + MDE_DT_END_EFF + MDE_COD_COMP_ANIA + MDE_TP_MOT_DEROGA + MDE_COD_LIV_AUT + MDE_COD_ERR + MDE_TP_ERR + MDE_DESC_ERR_BREVE_VCHAR + MDE_DESC_ERR_EST_VCHAR + MDE_DS_RIGA + MDE_DS_OPER_SQL + MDE_DS_VER + MDE_DS_TS_INI_CPTZ + MDE_DS_TS_END_CPTZ + MDE_DS_UTENTE + MDE_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MDE_ID_MOT_DEROGA = 9;
            public static final int MDE_ID_OGG_DEROGA = 9;
            public static final int MDE_ID_MOVI_CRZ = 9;
            public static final int MDE_DT_INI_EFF = 8;
            public static final int MDE_DT_END_EFF = 8;
            public static final int MDE_COD_COMP_ANIA = 5;
            public static final int MDE_COD_LIV_AUT = 5;
            public static final int MDE_DS_RIGA = 10;
            public static final int MDE_DS_VER = 9;
            public static final int MDE_DS_TS_INI_CPTZ = 18;
            public static final int MDE_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
