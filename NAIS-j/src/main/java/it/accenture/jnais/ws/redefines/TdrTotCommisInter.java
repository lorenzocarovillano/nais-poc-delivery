package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-COMMIS-INTER<br>
 * Variable: TDR-TOT-COMMIS-INTER from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_COMMIS_INTER;
    }

    public void setTdrTotCommisInter(AfDecimal tdrTotCommisInter) {
        writeDecimalAsPacked(Pos.TDR_TOT_COMMIS_INTER, tdrTotCommisInter.copy());
    }

    public void setTdrTotCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_COMMIS_INTER, Pos.TDR_TOT_COMMIS_INTER);
    }

    /**Original name: TDR-TOT-COMMIS-INTER<br>*/
    public AfDecimal getTdrTotCommisInter() {
        return readPackedAsDecimal(Pos.TDR_TOT_COMMIS_INTER, Len.Int.TDR_TOT_COMMIS_INTER, Len.Fract.TDR_TOT_COMMIS_INTER);
    }

    public byte[] getTdrTotCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_COMMIS_INTER, Pos.TDR_TOT_COMMIS_INTER);
        return buffer;
    }

    public void setTdrTotCommisInterNull(String tdrTotCommisInterNull) {
        writeString(Pos.TDR_TOT_COMMIS_INTER_NULL, tdrTotCommisInterNull, Len.TDR_TOT_COMMIS_INTER_NULL);
    }

    /**Original name: TDR-TOT-COMMIS-INTER-NULL<br>*/
    public String getTdrTotCommisInterNull() {
        return readString(Pos.TDR_TOT_COMMIS_INTER_NULL, Len.TDR_TOT_COMMIS_INTER_NULL);
    }

    public String getTdrTotCommisInterNullFormatted() {
        return Functions.padBlanks(getTdrTotCommisInterNull(), Len.TDR_TOT_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_COMMIS_INTER = 1;
        public static final int TDR_TOT_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_COMMIS_INTER = 8;
        public static final int TDR_TOT_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
