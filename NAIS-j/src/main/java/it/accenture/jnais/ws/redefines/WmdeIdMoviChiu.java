package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WMDE-ID-MOVI-CHIU<br>
 * Variable: WMDE-ID-MOVI-CHIU from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WmdeIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WmdeIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WMDE_ID_MOVI_CHIU;
    }

    public void setWmdeIdMoviChiu(int wmdeIdMoviChiu) {
        writeIntAsPacked(Pos.WMDE_ID_MOVI_CHIU, wmdeIdMoviChiu, Len.Int.WMDE_ID_MOVI_CHIU);
    }

    /**Original name: WMDE-ID-MOVI-CHIU<br>*/
    public int getWmdeIdMoviChiu() {
        return readPackedAsInt(Pos.WMDE_ID_MOVI_CHIU, Len.Int.WMDE_ID_MOVI_CHIU);
    }

    /**Original name: WMDE-ID-MOVI-CHIU-NULL<br>*/
    public String getWmdeIdMoviChiuNull() {
        return readString(Pos.WMDE_ID_MOVI_CHIU_NULL, Len.WMDE_ID_MOVI_CHIU_NULL);
    }

    public String getWmdeIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getWmdeIdMoviChiuNull(), Len.WMDE_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WMDE_ID_MOVI_CHIU = 1;
        public static final int WMDE_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WMDE_ID_MOVI_CHIU = 5;
        public static final int WMDE_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WMDE_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
