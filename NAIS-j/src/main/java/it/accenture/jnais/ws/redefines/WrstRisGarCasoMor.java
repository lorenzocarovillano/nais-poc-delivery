package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-GAR-CASO-MOR<br>
 * Variable: WRST-RIS-GAR-CASO-MOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisGarCasoMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisGarCasoMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_GAR_CASO_MOR;
    }

    public void setWrstRisGarCasoMor(AfDecimal wrstRisGarCasoMor) {
        writeDecimalAsPacked(Pos.WRST_RIS_GAR_CASO_MOR, wrstRisGarCasoMor.copy());
    }

    public void setWrstRisGarCasoMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_GAR_CASO_MOR, Pos.WRST_RIS_GAR_CASO_MOR);
    }

    /**Original name: WRST-RIS-GAR-CASO-MOR<br>*/
    public AfDecimal getWrstRisGarCasoMor() {
        return readPackedAsDecimal(Pos.WRST_RIS_GAR_CASO_MOR, Len.Int.WRST_RIS_GAR_CASO_MOR, Len.Fract.WRST_RIS_GAR_CASO_MOR);
    }

    public byte[] getWrstRisGarCasoMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_GAR_CASO_MOR, Pos.WRST_RIS_GAR_CASO_MOR);
        return buffer;
    }

    public void initWrstRisGarCasoMorSpaces() {
        fill(Pos.WRST_RIS_GAR_CASO_MOR, Len.WRST_RIS_GAR_CASO_MOR, Types.SPACE_CHAR);
    }

    public void setWrstRisGarCasoMorNull(String wrstRisGarCasoMorNull) {
        writeString(Pos.WRST_RIS_GAR_CASO_MOR_NULL, wrstRisGarCasoMorNull, Len.WRST_RIS_GAR_CASO_MOR_NULL);
    }

    /**Original name: WRST-RIS-GAR-CASO-MOR-NULL<br>*/
    public String getWrstRisGarCasoMorNull() {
        return readString(Pos.WRST_RIS_GAR_CASO_MOR_NULL, Len.WRST_RIS_GAR_CASO_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_GAR_CASO_MOR = 1;
        public static final int WRST_RIS_GAR_CASO_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_GAR_CASO_MOR = 8;
        public static final int WRST_RIS_GAR_CASO_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_GAR_CASO_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_GAR_CASO_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
