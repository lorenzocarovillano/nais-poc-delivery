package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-MEDIO<br>
 * Variable: WB03-TS-MEDIO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsMedio extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsMedio() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_MEDIO;
    }

    public void setWb03TsMedioFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_MEDIO, Pos.WB03_TS_MEDIO);
    }

    /**Original name: WB03-TS-MEDIO<br>*/
    public AfDecimal getWb03TsMedio() {
        return readPackedAsDecimal(Pos.WB03_TS_MEDIO, Len.Int.WB03_TS_MEDIO, Len.Fract.WB03_TS_MEDIO);
    }

    public byte[] getWb03TsMedioAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_MEDIO, Pos.WB03_TS_MEDIO);
        return buffer;
    }

    public void setWb03TsMedioNull(String wb03TsMedioNull) {
        writeString(Pos.WB03_TS_MEDIO_NULL, wb03TsMedioNull, Len.WB03_TS_MEDIO_NULL);
    }

    /**Original name: WB03-TS-MEDIO-NULL<br>*/
    public String getWb03TsMedioNull() {
        return readString(Pos.WB03_TS_MEDIO_NULL, Len.WB03_TS_MEDIO_NULL);
    }

    public String getWb03TsMedioNullFormatted() {
        return Functions.padBlanks(getWb03TsMedioNull(), Len.WB03_TS_MEDIO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_MEDIO = 1;
        public static final int WB03_TS_MEDIO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_MEDIO = 8;
        public static final int WB03_TS_MEDIO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_MEDIO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_MEDIO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
