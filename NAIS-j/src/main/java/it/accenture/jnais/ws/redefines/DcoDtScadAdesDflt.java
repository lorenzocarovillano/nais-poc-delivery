package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCO-DT-SCAD-ADES-DFLT<br>
 * Variable: DCO-DT-SCAD-ADES-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DcoDtScadAdesDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DcoDtScadAdesDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DCO_DT_SCAD_ADES_DFLT;
    }

    public void setDcoDtScadAdesDflt(int dcoDtScadAdesDflt) {
        writeIntAsPacked(Pos.DCO_DT_SCAD_ADES_DFLT, dcoDtScadAdesDflt, Len.Int.DCO_DT_SCAD_ADES_DFLT);
    }

    public void setDcoDtScadAdesDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DCO_DT_SCAD_ADES_DFLT, Pos.DCO_DT_SCAD_ADES_DFLT);
    }

    /**Original name: DCO-DT-SCAD-ADES-DFLT<br>*/
    public int getDcoDtScadAdesDflt() {
        return readPackedAsInt(Pos.DCO_DT_SCAD_ADES_DFLT, Len.Int.DCO_DT_SCAD_ADES_DFLT);
    }

    public byte[] getDcoDtScadAdesDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DCO_DT_SCAD_ADES_DFLT, Pos.DCO_DT_SCAD_ADES_DFLT);
        return buffer;
    }

    public void setDcoDtScadAdesDfltNull(String dcoDtScadAdesDfltNull) {
        writeString(Pos.DCO_DT_SCAD_ADES_DFLT_NULL, dcoDtScadAdesDfltNull, Len.DCO_DT_SCAD_ADES_DFLT_NULL);
    }

    /**Original name: DCO-DT-SCAD-ADES-DFLT-NULL<br>*/
    public String getDcoDtScadAdesDfltNull() {
        return readString(Pos.DCO_DT_SCAD_ADES_DFLT_NULL, Len.DCO_DT_SCAD_ADES_DFLT_NULL);
    }

    public String getDcoDtScadAdesDfltNullFormatted() {
        return Functions.padBlanks(getDcoDtScadAdesDfltNull(), Len.DCO_DT_SCAD_ADES_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DCO_DT_SCAD_ADES_DFLT = 1;
        public static final int DCO_DT_SCAD_ADES_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DCO_DT_SCAD_ADES_DFLT = 5;
        public static final int DCO_DT_SCAD_ADES_DFLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DCO_DT_SCAD_ADES_DFLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
