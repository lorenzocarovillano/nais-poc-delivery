package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-SCAD-TRCH<br>
 * Variable: WB03-DT-SCAD-TRCH from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtScadTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtScadTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_SCAD_TRCH;
    }

    public void setWb03DtScadTrch(int wb03DtScadTrch) {
        writeIntAsPacked(Pos.WB03_DT_SCAD_TRCH, wb03DtScadTrch, Len.Int.WB03_DT_SCAD_TRCH);
    }

    public void setWb03DtScadTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_SCAD_TRCH, Pos.WB03_DT_SCAD_TRCH);
    }

    /**Original name: WB03-DT-SCAD-TRCH<br>*/
    public int getWb03DtScadTrch() {
        return readPackedAsInt(Pos.WB03_DT_SCAD_TRCH, Len.Int.WB03_DT_SCAD_TRCH);
    }

    public byte[] getWb03DtScadTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_SCAD_TRCH, Pos.WB03_DT_SCAD_TRCH);
        return buffer;
    }

    public void setWb03DtScadTrchNull(String wb03DtScadTrchNull) {
        writeString(Pos.WB03_DT_SCAD_TRCH_NULL, wb03DtScadTrchNull, Len.WB03_DT_SCAD_TRCH_NULL);
    }

    /**Original name: WB03-DT-SCAD-TRCH-NULL<br>*/
    public String getWb03DtScadTrchNull() {
        return readString(Pos.WB03_DT_SCAD_TRCH_NULL, Len.WB03_DT_SCAD_TRCH_NULL);
    }

    public String getWb03DtScadTrchNullFormatted() {
        return Functions.padBlanks(getWb03DtScadTrchNull(), Len.WB03_DT_SCAD_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_SCAD_TRCH = 1;
        public static final int WB03_DT_SCAD_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_SCAD_TRCH = 5;
        public static final int WB03_DT_SCAD_TRCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_SCAD_TRCH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
