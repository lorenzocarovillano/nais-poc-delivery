package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: FORMATO<br>
 * Variable: FORMATO from program LCCS0010<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Formato extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: FORMATO
    private char formato;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.FORMATO;
    }

    @Override
    public void deserialize(byte[] buf) {
        setFormatoFromBuffer(buf);
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setCodiceRitornoFormatted(String codiceRitorno) {
        setFormato(Functions.charAt(codiceRitorno, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer, int offset) {
        setFormato(MarshalByte.readChar(buffer, offset));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        setFormatoFromBuffer(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    @Override
    public byte[] serialize() {
        return MarshalByteExt.chToBuffer(getFormato());
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FORMATO = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int FORMATO = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int FORMATO = 0;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
