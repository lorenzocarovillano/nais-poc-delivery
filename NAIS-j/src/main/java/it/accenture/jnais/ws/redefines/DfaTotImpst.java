package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-TOT-IMPST<br>
 * Variable: DFA-TOT-IMPST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaTotImpst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaTotImpst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_TOT_IMPST;
    }

    public void setDfaTotImpst(AfDecimal dfaTotImpst) {
        writeDecimalAsPacked(Pos.DFA_TOT_IMPST, dfaTotImpst.copy());
    }

    public void setDfaTotImpstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_TOT_IMPST, Pos.DFA_TOT_IMPST);
    }

    /**Original name: DFA-TOT-IMPST<br>*/
    public AfDecimal getDfaTotImpst() {
        return readPackedAsDecimal(Pos.DFA_TOT_IMPST, Len.Int.DFA_TOT_IMPST, Len.Fract.DFA_TOT_IMPST);
    }

    public byte[] getDfaTotImpstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_TOT_IMPST, Pos.DFA_TOT_IMPST);
        return buffer;
    }

    public void setDfaTotImpstNull(String dfaTotImpstNull) {
        writeString(Pos.DFA_TOT_IMPST_NULL, dfaTotImpstNull, Len.DFA_TOT_IMPST_NULL);
    }

    /**Original name: DFA-TOT-IMPST-NULL<br>*/
    public String getDfaTotImpstNull() {
        return readString(Pos.DFA_TOT_IMPST_NULL, Len.DFA_TOT_IMPST_NULL);
    }

    public String getDfaTotImpstNullFormatted() {
        return Functions.padBlanks(getDfaTotImpstNull(), Len.DFA_TOT_IMPST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_TOT_IMPST = 1;
        public static final int DFA_TOT_IMPST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_TOT_IMPST = 8;
        public static final int DFA_TOT_IMPST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_TOT_IMPST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_TOT_IMPST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
