package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-PC-ABB-TIT-STAT<br>
 * Variable: LQU-PC-ABB-TIT-STAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquPcAbbTitStat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquPcAbbTitStat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_PC_ABB_TIT_STAT;
    }

    public void setLquPcAbbTitStat(AfDecimal lquPcAbbTitStat) {
        writeDecimalAsPacked(Pos.LQU_PC_ABB_TIT_STAT, lquPcAbbTitStat.copy());
    }

    public void setLquPcAbbTitStatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_PC_ABB_TIT_STAT, Pos.LQU_PC_ABB_TIT_STAT);
    }

    /**Original name: LQU-PC-ABB-TIT-STAT<br>*/
    public AfDecimal getLquPcAbbTitStat() {
        return readPackedAsDecimal(Pos.LQU_PC_ABB_TIT_STAT, Len.Int.LQU_PC_ABB_TIT_STAT, Len.Fract.LQU_PC_ABB_TIT_STAT);
    }

    public byte[] getLquPcAbbTitStatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_PC_ABB_TIT_STAT, Pos.LQU_PC_ABB_TIT_STAT);
        return buffer;
    }

    public void setLquPcAbbTitStatNull(String lquPcAbbTitStatNull) {
        writeString(Pos.LQU_PC_ABB_TIT_STAT_NULL, lquPcAbbTitStatNull, Len.LQU_PC_ABB_TIT_STAT_NULL);
    }

    /**Original name: LQU-PC-ABB-TIT-STAT-NULL<br>*/
    public String getLquPcAbbTitStatNull() {
        return readString(Pos.LQU_PC_ABB_TIT_STAT_NULL, Len.LQU_PC_ABB_TIT_STAT_NULL);
    }

    public String getLquPcAbbTitStatNullFormatted() {
        return Functions.padBlanks(getLquPcAbbTitStatNull(), Len.LQU_PC_ABB_TIT_STAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_PC_ABB_TIT_STAT = 1;
        public static final int LQU_PC_ABB_TIT_STAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_PC_ABB_TIT_STAT = 4;
        public static final int LQU_PC_ABB_TIT_STAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_PC_ABB_TIT_STAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_PC_ABB_TIT_STAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
