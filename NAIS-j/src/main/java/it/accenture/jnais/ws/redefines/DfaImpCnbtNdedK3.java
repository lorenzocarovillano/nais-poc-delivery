package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-NDED-K3<br>
 * Variable: DFA-IMP-CNBT-NDED-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtNdedK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtNdedK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_NDED_K3;
    }

    public void setDfaImpCnbtNdedK3(AfDecimal dfaImpCnbtNdedK3) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_NDED_K3, dfaImpCnbtNdedK3.copy());
    }

    public void setDfaImpCnbtNdedK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_NDED_K3, Pos.DFA_IMP_CNBT_NDED_K3);
    }

    /**Original name: DFA-IMP-CNBT-NDED-K3<br>*/
    public AfDecimal getDfaImpCnbtNdedK3() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_NDED_K3, Len.Int.DFA_IMP_CNBT_NDED_K3, Len.Fract.DFA_IMP_CNBT_NDED_K3);
    }

    public byte[] getDfaImpCnbtNdedK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_NDED_K3, Pos.DFA_IMP_CNBT_NDED_K3);
        return buffer;
    }

    public void setDfaImpCnbtNdedK3Null(String dfaImpCnbtNdedK3Null) {
        writeString(Pos.DFA_IMP_CNBT_NDED_K3_NULL, dfaImpCnbtNdedK3Null, Len.DFA_IMP_CNBT_NDED_K3_NULL);
    }

    /**Original name: DFA-IMP-CNBT-NDED-K3-NULL<br>*/
    public String getDfaImpCnbtNdedK3Null() {
        return readString(Pos.DFA_IMP_CNBT_NDED_K3_NULL, Len.DFA_IMP_CNBT_NDED_K3_NULL);
    }

    public String getDfaImpCnbtNdedK3NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtNdedK3Null(), Len.DFA_IMP_CNBT_NDED_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_NDED_K3 = 1;
        public static final int DFA_IMP_CNBT_NDED_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_NDED_K3 = 8;
        public static final int DFA_IMP_CNBT_NDED_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_NDED_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_NDED_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
