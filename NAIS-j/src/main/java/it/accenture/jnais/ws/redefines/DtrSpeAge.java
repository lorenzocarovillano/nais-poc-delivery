package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SPE-AGE<br>
 * Variable: DTR-SPE-AGE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SPE_AGE;
    }

    public void setDtrSpeAge(AfDecimal dtrSpeAge) {
        writeDecimalAsPacked(Pos.DTR_SPE_AGE, dtrSpeAge.copy());
    }

    public void setDtrSpeAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SPE_AGE, Pos.DTR_SPE_AGE);
    }

    /**Original name: DTR-SPE-AGE<br>*/
    public AfDecimal getDtrSpeAge() {
        return readPackedAsDecimal(Pos.DTR_SPE_AGE, Len.Int.DTR_SPE_AGE, Len.Fract.DTR_SPE_AGE);
    }

    public byte[] getDtrSpeAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SPE_AGE, Pos.DTR_SPE_AGE);
        return buffer;
    }

    public void setDtrSpeAgeNull(String dtrSpeAgeNull) {
        writeString(Pos.DTR_SPE_AGE_NULL, dtrSpeAgeNull, Len.DTR_SPE_AGE_NULL);
    }

    /**Original name: DTR-SPE-AGE-NULL<br>*/
    public String getDtrSpeAgeNull() {
        return readString(Pos.DTR_SPE_AGE_NULL, Len.DTR_SPE_AGE_NULL);
    }

    public String getDtrSpeAgeNullFormatted() {
        return Functions.padBlanks(getDtrSpeAgeNull(), Len.DTR_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SPE_AGE = 1;
        public static final int DTR_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SPE_AGE = 8;
        public static final int DTR_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
