package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-ID-OGG<br>
 * Variable: RRE-ID-OGG from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_ID_OGG;
    }

    public void setRreIdOgg(int rreIdOgg) {
        writeIntAsPacked(Pos.RRE_ID_OGG, rreIdOgg, Len.Int.RRE_ID_OGG);
    }

    public void setRreIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_ID_OGG, Pos.RRE_ID_OGG);
    }

    /**Original name: RRE-ID-OGG<br>*/
    public int getRreIdOgg() {
        return readPackedAsInt(Pos.RRE_ID_OGG, Len.Int.RRE_ID_OGG);
    }

    public byte[] getRreIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_ID_OGG, Pos.RRE_ID_OGG);
        return buffer;
    }

    public void setRreIdOggNull(String rreIdOggNull) {
        writeString(Pos.RRE_ID_OGG_NULL, rreIdOggNull, Len.RRE_ID_OGG_NULL);
    }

    /**Original name: RRE-ID-OGG-NULL<br>*/
    public String getRreIdOggNull() {
        return readString(Pos.RRE_ID_OGG_NULL, Len.RRE_ID_OGG_NULL);
    }

    public String getRreIdOggNullFormatted() {
        return Functions.padBlanks(getRreIdOggNull(), Len.RRE_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_ID_OGG = 1;
        public static final int RRE_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_ID_OGG = 5;
        public static final int RRE_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
