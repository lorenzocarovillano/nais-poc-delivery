package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-INTR-RETDT<br>
 * Variable: DTR-INTR-RETDT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_INTR_RETDT;
    }

    public void setDtrIntrRetdt(AfDecimal dtrIntrRetdt) {
        writeDecimalAsPacked(Pos.DTR_INTR_RETDT, dtrIntrRetdt.copy());
    }

    public void setDtrIntrRetdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_INTR_RETDT, Pos.DTR_INTR_RETDT);
    }

    /**Original name: DTR-INTR-RETDT<br>*/
    public AfDecimal getDtrIntrRetdt() {
        return readPackedAsDecimal(Pos.DTR_INTR_RETDT, Len.Int.DTR_INTR_RETDT, Len.Fract.DTR_INTR_RETDT);
    }

    public byte[] getDtrIntrRetdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_INTR_RETDT, Pos.DTR_INTR_RETDT);
        return buffer;
    }

    public void setDtrIntrRetdtNull(String dtrIntrRetdtNull) {
        writeString(Pos.DTR_INTR_RETDT_NULL, dtrIntrRetdtNull, Len.DTR_INTR_RETDT_NULL);
    }

    /**Original name: DTR-INTR-RETDT-NULL<br>*/
    public String getDtrIntrRetdtNull() {
        return readString(Pos.DTR_INTR_RETDT_NULL, Len.DTR_INTR_RETDT_NULL);
    }

    public String getDtrIntrRetdtNullFormatted() {
        return Functions.padBlanks(getDtrIntrRetdtNull(), Len.DTR_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_INTR_RETDT = 1;
        public static final int DTR_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_INTR_RETDT = 8;
        public static final int DTR_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
