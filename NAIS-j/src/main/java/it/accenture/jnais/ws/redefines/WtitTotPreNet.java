package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-PRE-NET<br>
 * Variable: WTIT-TOT-PRE-NET from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_PRE_NET;
    }

    public void setWtitTotPreNet(AfDecimal wtitTotPreNet) {
        writeDecimalAsPacked(Pos.WTIT_TOT_PRE_NET, wtitTotPreNet.copy());
    }

    public void setWtitTotPreNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_PRE_NET, Pos.WTIT_TOT_PRE_NET);
    }

    /**Original name: WTIT-TOT-PRE-NET<br>*/
    public AfDecimal getWtitTotPreNet() {
        return readPackedAsDecimal(Pos.WTIT_TOT_PRE_NET, Len.Int.WTIT_TOT_PRE_NET, Len.Fract.WTIT_TOT_PRE_NET);
    }

    public byte[] getWtitTotPreNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_PRE_NET, Pos.WTIT_TOT_PRE_NET);
        return buffer;
    }

    public void initWtitTotPreNetSpaces() {
        fill(Pos.WTIT_TOT_PRE_NET, Len.WTIT_TOT_PRE_NET, Types.SPACE_CHAR);
    }

    public void setWtitTotPreNetNull(String wtitTotPreNetNull) {
        writeString(Pos.WTIT_TOT_PRE_NET_NULL, wtitTotPreNetNull, Len.WTIT_TOT_PRE_NET_NULL);
    }

    /**Original name: WTIT-TOT-PRE-NET-NULL<br>*/
    public String getWtitTotPreNetNull() {
        return readString(Pos.WTIT_TOT_PRE_NET_NULL, Len.WTIT_TOT_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_NET = 1;
        public static final int WTIT_TOT_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_PRE_NET = 8;
        public static final int WTIT_TOT_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
