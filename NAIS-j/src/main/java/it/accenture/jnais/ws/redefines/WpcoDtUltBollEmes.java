package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-EMES<br>
 * Variable: WPCO-DT-ULT-BOLL-EMES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollEmes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollEmes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_EMES;
    }

    public void setWpcoDtUltBollEmes(int wpcoDtUltBollEmes) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_EMES, wpcoDtUltBollEmes, Len.Int.WPCO_DT_ULT_BOLL_EMES);
    }

    public void setDpcoDtUltBollEmesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_EMES, Pos.WPCO_DT_ULT_BOLL_EMES);
    }

    /**Original name: WPCO-DT-ULT-BOLL-EMES<br>*/
    public int getWpcoDtUltBollEmes() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_EMES, Len.Int.WPCO_DT_ULT_BOLL_EMES);
    }

    public byte[] getWpcoDtUltBollEmesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_EMES, Pos.WPCO_DT_ULT_BOLL_EMES);
        return buffer;
    }

    public void setWpcoDtUltBollEmesNull(String wpcoDtUltBollEmesNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_EMES_NULL, wpcoDtUltBollEmesNull, Len.WPCO_DT_ULT_BOLL_EMES_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-EMES-NULL<br>*/
    public String getWpcoDtUltBollEmesNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_EMES_NULL, Len.WPCO_DT_ULT_BOLL_EMES_NULL);
    }

    public String getWpcoDtUltBollEmesNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollEmesNull(), Len.WPCO_DT_ULT_BOLL_EMES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_EMES = 1;
        public static final int WPCO_DT_ULT_BOLL_EMES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_EMES = 5;
        public static final int WPCO_DT_ULT_BOLL_EMES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_EMES = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
