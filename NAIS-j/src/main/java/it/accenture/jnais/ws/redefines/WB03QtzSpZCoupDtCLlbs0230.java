package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-QTZ-SP-Z-COUP-DT-C<br>
 * Variable: W-B03-QTZ-SP-Z-COUP-DT-C from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03QtzSpZCoupDtCLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03QtzSpZCoupDtCLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_QTZ_SP_Z_COUP_DT_C;
    }

    public void setwB03QtzSpZCoupDtC(AfDecimal wB03QtzSpZCoupDtC) {
        writeDecimalAsPacked(Pos.W_B03_QTZ_SP_Z_COUP_DT_C, wB03QtzSpZCoupDtC.copy());
    }

    /**Original name: W-B03-QTZ-SP-Z-COUP-DT-C<br>*/
    public AfDecimal getwB03QtzSpZCoupDtC() {
        return readPackedAsDecimal(Pos.W_B03_QTZ_SP_Z_COUP_DT_C, Len.Int.W_B03_QTZ_SP_Z_COUP_DT_C, Len.Fract.W_B03_QTZ_SP_Z_COUP_DT_C);
    }

    public byte[] getwB03QtzSpZCoupDtCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_QTZ_SP_Z_COUP_DT_C, Pos.W_B03_QTZ_SP_Z_COUP_DT_C);
        return buffer;
    }

    public void setwB03QtzSpZCoupDtCNull(String wB03QtzSpZCoupDtCNull) {
        writeString(Pos.W_B03_QTZ_SP_Z_COUP_DT_C_NULL, wB03QtzSpZCoupDtCNull, Len.W_B03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    /**Original name: W-B03-QTZ-SP-Z-COUP-DT-C-NULL<br>*/
    public String getwB03QtzSpZCoupDtCNull() {
        return readString(Pos.W_B03_QTZ_SP_Z_COUP_DT_C_NULL, Len.W_B03_QTZ_SP_Z_COUP_DT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_SP_Z_COUP_DT_C = 1;
        public static final int W_B03_QTZ_SP_Z_COUP_DT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_QTZ_SP_Z_COUP_DT_C = 7;
        public static final int W_B03_QTZ_SP_Z_COUP_DT_C_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_SP_Z_COUP_DT_C = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_QTZ_SP_Z_COUP_DT_C = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
