package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbv5141TpTrch;

/**Original name: LDBV5141<br>
 * Variable: LDBV5141 from copybook LDBV5141<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv5141 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV5141-TP-TRCH
    private Ldbv5141TpTrch tpTrch = new Ldbv5141TpTrch();
    //Original name: LDBV5141-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBV5141-CUM-PRE-ATT
    private AfDecimal cumPreAtt = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV5141;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv5141Bytes(buf);
    }

    public String getLdbv5141Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5141Bytes());
    }

    public void setLdbv5141Bytes(byte[] buffer) {
        setLdbv5141Bytes(buffer, 1);
    }

    public byte[] getLdbv5141Bytes() {
        byte[] buffer = new byte[Len.LDBV5141];
        return getLdbv5141Bytes(buffer, 1);
    }

    public void setLdbv5141Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpTrch.setTpTrchBytes(buffer, position);
        position += Ldbv5141TpTrch.Len.TP_TRCH;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        cumPreAtt.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CUM_PRE_ATT, Len.Fract.CUM_PRE_ATT));
    }

    public byte[] getLdbv5141Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpTrch.getTpTrchBytes(buffer, position);
        position += Ldbv5141TpTrch.Len.TP_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeDecimalAsPacked(buffer, position, cumPreAtt.copy());
        return buffer;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setCumPreAtt(AfDecimal cumPreAtt) {
        this.cumPreAtt.assign(cumPreAtt);
    }

    public AfDecimal getCumPreAtt() {
        return this.cumPreAtt.copy();
    }

    public Ldbv5141TpTrch getTpTrch() {
        return tpTrch;
    }

    @Override
    public byte[] serialize() {
        return getLdbv5141Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ADES = 5;
        public static final int CUM_PRE_ATT = 8;
        public static final int LDBV5141 = Ldbv5141TpTrch.Len.TP_TRCH + ID_ADES + CUM_PRE_ATT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ADES = 9;
            public static final int CUM_PRE_ATT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CUM_PRE_ATT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
