package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-RES-PRE-ATT-DFZ<br>
 * Variable: DFL-RES-PRE-ATT-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflResPreAttDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflResPreAttDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_RES_PRE_ATT_DFZ;
    }

    public void setDflResPreAttDfz(AfDecimal dflResPreAttDfz) {
        writeDecimalAsPacked(Pos.DFL_RES_PRE_ATT_DFZ, dflResPreAttDfz.copy());
    }

    public void setDflResPreAttDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_RES_PRE_ATT_DFZ, Pos.DFL_RES_PRE_ATT_DFZ);
    }

    /**Original name: DFL-RES-PRE-ATT-DFZ<br>*/
    public AfDecimal getDflResPreAttDfz() {
        return readPackedAsDecimal(Pos.DFL_RES_PRE_ATT_DFZ, Len.Int.DFL_RES_PRE_ATT_DFZ, Len.Fract.DFL_RES_PRE_ATT_DFZ);
    }

    public byte[] getDflResPreAttDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_RES_PRE_ATT_DFZ, Pos.DFL_RES_PRE_ATT_DFZ);
        return buffer;
    }

    public void setDflResPreAttDfzNull(String dflResPreAttDfzNull) {
        writeString(Pos.DFL_RES_PRE_ATT_DFZ_NULL, dflResPreAttDfzNull, Len.DFL_RES_PRE_ATT_DFZ_NULL);
    }

    /**Original name: DFL-RES-PRE-ATT-DFZ-NULL<br>*/
    public String getDflResPreAttDfzNull() {
        return readString(Pos.DFL_RES_PRE_ATT_DFZ_NULL, Len.DFL_RES_PRE_ATT_DFZ_NULL);
    }

    public String getDflResPreAttDfzNullFormatted() {
        return Functions.padBlanks(getDflResPreAttDfzNull(), Len.DFL_RES_PRE_ATT_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRE_ATT_DFZ = 1;
        public static final int DFL_RES_PRE_ATT_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_RES_PRE_ATT_DFZ = 8;
        public static final int DFL_RES_PRE_ATT_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRE_ATT_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_RES_PRE_ATT_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
