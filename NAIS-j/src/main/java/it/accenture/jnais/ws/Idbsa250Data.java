package it.accenture.jnais.ws;

import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndRisDiTrch;
import it.accenture.jnais.copy.PersDb;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSA250<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsa250Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-PERS
    private IndRisDiTrch indPers = new IndRisDiTrch();
    //Original name: PERS-DB
    private PersDb persDb = new PersDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndRisDiTrch getIndPers() {
        return indPers;
    }

    public PersDb getPersDb() {
        return persDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
