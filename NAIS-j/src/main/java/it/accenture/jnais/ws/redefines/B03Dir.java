package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DIR<br>
 * Variable: B03-DIR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03Dir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03Dir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DIR;
    }

    public void setB03Dir(AfDecimal b03Dir) {
        writeDecimalAsPacked(Pos.B03_DIR, b03Dir.copy());
    }

    public void setB03DirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DIR, Pos.B03_DIR);
    }

    /**Original name: B03-DIR<br>*/
    public AfDecimal getB03Dir() {
        return readPackedAsDecimal(Pos.B03_DIR, Len.Int.B03_DIR, Len.Fract.B03_DIR);
    }

    public byte[] getB03DirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DIR, Pos.B03_DIR);
        return buffer;
    }

    public void setB03DirNull(String b03DirNull) {
        writeString(Pos.B03_DIR_NULL, b03DirNull, Len.B03_DIR_NULL);
    }

    /**Original name: B03-DIR-NULL<br>*/
    public String getB03DirNull() {
        return readString(Pos.B03_DIR_NULL, Len.B03_DIR_NULL);
    }

    public String getB03DirNullFormatted() {
        return Functions.padBlanks(getB03DirNull(), Len.B03_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DIR = 1;
        public static final int B03_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DIR = 8;
        public static final int B03_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
