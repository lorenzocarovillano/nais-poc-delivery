package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-ACQ-EXP<br>
 * Variable: WDTC-ACQ-EXP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_ACQ_EXP;
    }

    public void setWdtcAcqExp(AfDecimal wdtcAcqExp) {
        writeDecimalAsPacked(Pos.WDTC_ACQ_EXP, wdtcAcqExp.copy());
    }

    public void setWdtcAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_ACQ_EXP, Pos.WDTC_ACQ_EXP);
    }

    /**Original name: WDTC-ACQ-EXP<br>*/
    public AfDecimal getWdtcAcqExp() {
        return readPackedAsDecimal(Pos.WDTC_ACQ_EXP, Len.Int.WDTC_ACQ_EXP, Len.Fract.WDTC_ACQ_EXP);
    }

    public byte[] getWdtcAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_ACQ_EXP, Pos.WDTC_ACQ_EXP);
        return buffer;
    }

    public void initWdtcAcqExpSpaces() {
        fill(Pos.WDTC_ACQ_EXP, Len.WDTC_ACQ_EXP, Types.SPACE_CHAR);
    }

    public void setWdtcAcqExpNull(String wdtcAcqExpNull) {
        writeString(Pos.WDTC_ACQ_EXP_NULL, wdtcAcqExpNull, Len.WDTC_ACQ_EXP_NULL);
    }

    /**Original name: WDTC-ACQ-EXP-NULL<br>*/
    public String getWdtcAcqExpNull() {
        return readString(Pos.WDTC_ACQ_EXP_NULL, Len.WDTC_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_ACQ_EXP = 1;
        public static final int WDTC_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_ACQ_EXP = 8;
        public static final int WDTC_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
