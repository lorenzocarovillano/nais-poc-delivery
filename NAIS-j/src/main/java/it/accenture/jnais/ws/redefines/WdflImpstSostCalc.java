package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-SOST-CALC<br>
 * Variable: WDFL-IMPST-SOST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstSostCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstSostCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_SOST_CALC;
    }

    public void setWdflImpstSostCalc(AfDecimal wdflImpstSostCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_SOST_CALC, wdflImpstSostCalc.copy());
    }

    public void setWdflImpstSostCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_SOST_CALC, Pos.WDFL_IMPST_SOST_CALC);
    }

    /**Original name: WDFL-IMPST-SOST-CALC<br>*/
    public AfDecimal getWdflImpstSostCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_SOST_CALC, Len.Int.WDFL_IMPST_SOST_CALC, Len.Fract.WDFL_IMPST_SOST_CALC);
    }

    public byte[] getWdflImpstSostCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_SOST_CALC, Pos.WDFL_IMPST_SOST_CALC);
        return buffer;
    }

    public void setWdflImpstSostCalcNull(String wdflImpstSostCalcNull) {
        writeString(Pos.WDFL_IMPST_SOST_CALC_NULL, wdflImpstSostCalcNull, Len.WDFL_IMPST_SOST_CALC_NULL);
    }

    /**Original name: WDFL-IMPST-SOST-CALC-NULL<br>*/
    public String getWdflImpstSostCalcNull() {
        return readString(Pos.WDFL_IMPST_SOST_CALC_NULL, Len.WDFL_IMPST_SOST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_SOST_CALC = 1;
        public static final int WDFL_IMPST_SOST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_SOST_CALC = 8;
        public static final int WDFL_IMPST_SOST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_SOST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_SOST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
