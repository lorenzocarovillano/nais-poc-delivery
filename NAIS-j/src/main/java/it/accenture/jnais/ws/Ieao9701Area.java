package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: IEAO9701-AREA<br>
 * Variable: IEAO9701-AREA from copybook IEAO9701<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ieao9701Area extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IEAO9701-ID-LOG-ERR
    private String idLogErr = DefaultValues.stringVal(Len.ID_LOG_ERR);
    //Original name: IEAO9701-PROG-LOG-ERR
    private String progLogErr = DefaultValues.stringVal(Len.PROG_LOG_ERR);
    //Original name: IEAO9701-COD-ERRORE-990
    private String codErrore990 = DefaultValues.stringVal(Len.COD_ERRORE990);
    //Original name: IEAO9701-LABEL-ERR-990
    private String labelErr990 = DefaultValues.stringVal(Len.LABEL_ERR990);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IEAO9701_AREA;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIeao9701AreaBytes(buf);
    }

    public String getIeao9701AreaFormatted() {
        return MarshalByteExt.bufferToStr(getIeao9701AreaBytes());
    }

    public void setIeao9701AreaBytes(byte[] buffer) {
        setIeao9701AreaBytes(buffer, 1);
    }

    public byte[] getIeao9701AreaBytes() {
        byte[] buffer = new byte[Len.IEAO9701_AREA];
        return getIeao9701AreaBytes(buffer, 1);
    }

    public void setIeao9701AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        idLogErr = MarshalByte.readFixedString(buffer, position, Len.ID_LOG_ERR);
        position += Len.ID_LOG_ERR;
        progLogErr = MarshalByte.readFixedString(buffer, position, Len.PROG_LOG_ERR);
        position += Len.PROG_LOG_ERR;
        codErrore990 = MarshalByte.readFixedString(buffer, position, Len.COD_ERRORE990);
        position += Len.COD_ERRORE990;
        labelErr990 = MarshalByte.readString(buffer, position, Len.LABEL_ERR990);
    }

    public byte[] getIeao9701AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idLogErr, Len.ID_LOG_ERR);
        position += Len.ID_LOG_ERR;
        MarshalByte.writeString(buffer, position, progLogErr, Len.PROG_LOG_ERR);
        position += Len.PROG_LOG_ERR;
        MarshalByte.writeString(buffer, position, codErrore990, Len.COD_ERRORE990);
        position += Len.COD_ERRORE990;
        MarshalByte.writeString(buffer, position, labelErr990, Len.LABEL_ERR990);
        return buffer;
    }

    public void initIeao9701AreaSpaces() {
        idLogErr = "";
        progLogErr = "";
        codErrore990 = "";
        labelErr990 = "";
    }

    public void setIdLogErrFormatted(String idLogErr) {
        this.idLogErr = Trunc.toUnsignedNumeric(idLogErr, Len.ID_LOG_ERR);
    }

    public int getIdLogErr() {
        return NumericDisplay.asInt(this.idLogErr);
    }

    public void setProgLogErr(int progLogErr) {
        this.progLogErr = NumericDisplay.asString(progLogErr, Len.PROG_LOG_ERR);
    }

    public int getProgLogErr() {
        return NumericDisplay.asInt(this.progLogErr);
    }

    public void setCodErrore990(int codErrore990) {
        this.codErrore990 = NumericDisplay.asString(codErrore990, Len.COD_ERRORE990);
    }

    public int getCodErrore990() {
        return NumericDisplay.asInt(this.codErrore990);
    }

    public String getCodErrore990Formatted() {
        return this.codErrore990;
    }

    public void setLabelErr990(String labelErr990) {
        this.labelErr990 = Functions.subString(labelErr990, Len.LABEL_ERR990);
    }

    public String getLabelErr990() {
        return this.labelErr990;
    }

    @Override
    public byte[] serialize() {
        return getIeao9701AreaBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_LOG_ERR = 9;
        public static final int PROG_LOG_ERR = 5;
        public static final int COD_ERRORE990 = 6;
        public static final int LABEL_ERR990 = 30;
        public static final int IEAO9701_AREA = ID_LOG_ERR + PROG_LOG_ERR + COD_ERRORE990 + LABEL_ERR990;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
