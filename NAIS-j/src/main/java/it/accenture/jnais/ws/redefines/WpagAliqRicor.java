package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-ALIQ-RICOR<br>
 * Variable: WPAG-ALIQ-RICOR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagAliqRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagAliqRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_ALIQ_RICOR;
    }

    public void setWpagAliqRicor(AfDecimal wpagAliqRicor) {
        writeDecimalAsPacked(Pos.WPAG_ALIQ_RICOR, wpagAliqRicor.copy());
    }

    public void setWpagAliqRicorFormatted(String wpagAliqRicor) {
        setWpagAliqRicor(PicParser.display(new PicParams("S9(5)V9(9)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_ALIQ_RICOR + Len.Fract.WPAG_ALIQ_RICOR, Len.Fract.WPAG_ALIQ_RICOR, wpagAliqRicor));
    }

    public void setWpagAliqRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_ALIQ_RICOR, Pos.WPAG_ALIQ_RICOR);
    }

    /**Original name: WPAG-ALIQ-RICOR<br>*/
    public AfDecimal getWpagAliqRicor() {
        return readPackedAsDecimal(Pos.WPAG_ALIQ_RICOR, Len.Int.WPAG_ALIQ_RICOR, Len.Fract.WPAG_ALIQ_RICOR);
    }

    public byte[] getWpagAliqRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_ALIQ_RICOR, Pos.WPAG_ALIQ_RICOR);
        return buffer;
    }

    public void initWpagAliqRicorSpaces() {
        fill(Pos.WPAG_ALIQ_RICOR, Len.WPAG_ALIQ_RICOR, Types.SPACE_CHAR);
    }

    public void setWpagAliqRicorNull(String wpagAliqRicorNull) {
        writeString(Pos.WPAG_ALIQ_RICOR_NULL, wpagAliqRicorNull, Len.WPAG_ALIQ_RICOR_NULL);
    }

    /**Original name: WPAG-ALIQ-RICOR-NULL<br>*/
    public String getWpagAliqRicorNull() {
        return readString(Pos.WPAG_ALIQ_RICOR_NULL, Len.WPAG_ALIQ_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_RICOR = 1;
        public static final int WPAG_ALIQ_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_ALIQ_RICOR = 8;
        public static final int WPAG_ALIQ_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_RICOR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_ALIQ_RICOR = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
