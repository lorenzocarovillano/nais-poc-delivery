package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SOPR-TEC<br>
 * Variable: WTDR-TOT-SOPR-TEC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SOPR_TEC;
    }

    public void setWtdrTotSoprTec(AfDecimal wtdrTotSoprTec) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SOPR_TEC, wtdrTotSoprTec.copy());
    }

    /**Original name: WTDR-TOT-SOPR-TEC<br>*/
    public AfDecimal getWtdrTotSoprTec() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SOPR_TEC, Len.Int.WTDR_TOT_SOPR_TEC, Len.Fract.WTDR_TOT_SOPR_TEC);
    }

    public void setWtdrTotSoprTecNull(String wtdrTotSoprTecNull) {
        writeString(Pos.WTDR_TOT_SOPR_TEC_NULL, wtdrTotSoprTecNull, Len.WTDR_TOT_SOPR_TEC_NULL);
    }

    /**Original name: WTDR-TOT-SOPR-TEC-NULL<br>*/
    public String getWtdrTotSoprTecNull() {
        return readString(Pos.WTDR_TOT_SOPR_TEC_NULL, Len.WTDR_TOT_SOPR_TEC_NULL);
    }

    public String getWtdrTotSoprTecNullFormatted() {
        return Functions.padBlanks(getWtdrTotSoprTecNull(), Len.WTDR_TOT_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_TEC = 1;
        public static final int WTDR_TOT_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SOPR_TEC = 8;
        public static final int WTDR_TOT_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
