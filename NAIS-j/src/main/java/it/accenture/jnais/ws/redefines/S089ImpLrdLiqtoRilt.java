package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMP-LRD-LIQTO-RILT<br>
 * Variable: S089-IMP-LRD-LIQTO-RILT from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpLrdLiqtoRilt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpLrdLiqtoRilt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMP_LRD_LIQTO_RILT;
    }

    public void setWlquImpLrdLiqtoRilt(AfDecimal wlquImpLrdLiqtoRilt) {
        writeDecimalAsPacked(Pos.S089_IMP_LRD_LIQTO_RILT, wlquImpLrdLiqtoRilt.copy());
    }

    public void setWlquImpLrdLiqtoRiltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMP_LRD_LIQTO_RILT, Pos.S089_IMP_LRD_LIQTO_RILT);
    }

    /**Original name: WLQU-IMP-LRD-LIQTO-RILT<br>*/
    public AfDecimal getWlquImpLrdLiqtoRilt() {
        return readPackedAsDecimal(Pos.S089_IMP_LRD_LIQTO_RILT, Len.Int.WLQU_IMP_LRD_LIQTO_RILT, Len.Fract.WLQU_IMP_LRD_LIQTO_RILT);
    }

    public byte[] getWlquImpLrdLiqtoRiltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMP_LRD_LIQTO_RILT, Pos.S089_IMP_LRD_LIQTO_RILT);
        return buffer;
    }

    public void initWlquImpLrdLiqtoRiltSpaces() {
        fill(Pos.S089_IMP_LRD_LIQTO_RILT, Len.S089_IMP_LRD_LIQTO_RILT, Types.SPACE_CHAR);
    }

    public void setWlquImpLrdLiqtoRiltNull(String wlquImpLrdLiqtoRiltNull) {
        writeString(Pos.S089_IMP_LRD_LIQTO_RILT_NULL, wlquImpLrdLiqtoRiltNull, Len.WLQU_IMP_LRD_LIQTO_RILT_NULL);
    }

    /**Original name: WLQU-IMP-LRD-LIQTO-RILT-NULL<br>*/
    public String getWlquImpLrdLiqtoRiltNull() {
        return readString(Pos.S089_IMP_LRD_LIQTO_RILT_NULL, Len.WLQU_IMP_LRD_LIQTO_RILT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMP_LRD_LIQTO_RILT = 1;
        public static final int S089_IMP_LRD_LIQTO_RILT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMP_LRD_LIQTO_RILT = 8;
        public static final int WLQU_IMP_LRD_LIQTO_RILT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_LRD_LIQTO_RILT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMP_LRD_LIQTO_RILT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
