package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CUM-RISCPAR<br>
 * Variable: B03-CUM-RISCPAR from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CumRiscpar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CumRiscpar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CUM_RISCPAR;
    }

    public void setB03CumRiscpar(AfDecimal b03CumRiscpar) {
        writeDecimalAsPacked(Pos.B03_CUM_RISCPAR, b03CumRiscpar.copy());
    }

    public void setB03CumRiscparFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CUM_RISCPAR, Pos.B03_CUM_RISCPAR);
    }

    /**Original name: B03-CUM-RISCPAR<br>*/
    public AfDecimal getB03CumRiscpar() {
        return readPackedAsDecimal(Pos.B03_CUM_RISCPAR, Len.Int.B03_CUM_RISCPAR, Len.Fract.B03_CUM_RISCPAR);
    }

    public byte[] getB03CumRiscparAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CUM_RISCPAR, Pos.B03_CUM_RISCPAR);
        return buffer;
    }

    public void setB03CumRiscparNull(String b03CumRiscparNull) {
        writeString(Pos.B03_CUM_RISCPAR_NULL, b03CumRiscparNull, Len.B03_CUM_RISCPAR_NULL);
    }

    /**Original name: B03-CUM-RISCPAR-NULL<br>*/
    public String getB03CumRiscparNull() {
        return readString(Pos.B03_CUM_RISCPAR_NULL, Len.B03_CUM_RISCPAR_NULL);
    }

    public String getB03CumRiscparNullFormatted() {
        return Functions.padBlanks(getB03CumRiscparNull(), Len.B03_CUM_RISCPAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CUM_RISCPAR = 1;
        public static final int B03_CUM_RISCPAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CUM_RISCPAR = 8;
        public static final int B03_CUM_RISCPAR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CUM_RISCPAR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CUM_RISCPAR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
