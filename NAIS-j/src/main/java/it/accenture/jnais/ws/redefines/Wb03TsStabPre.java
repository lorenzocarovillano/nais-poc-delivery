package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-TS-STAB-PRE<br>
 * Variable: WB03-TS-STAB-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03TsStabPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03TsStabPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_TS_STAB_PRE;
    }

    public void setWb03TsStabPre(AfDecimal wb03TsStabPre) {
        writeDecimalAsPacked(Pos.WB03_TS_STAB_PRE, wb03TsStabPre.copy());
    }

    public void setWb03TsStabPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_TS_STAB_PRE, Pos.WB03_TS_STAB_PRE);
    }

    /**Original name: WB03-TS-STAB-PRE<br>*/
    public AfDecimal getWb03TsStabPre() {
        return readPackedAsDecimal(Pos.WB03_TS_STAB_PRE, Len.Int.WB03_TS_STAB_PRE, Len.Fract.WB03_TS_STAB_PRE);
    }

    public byte[] getWb03TsStabPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_TS_STAB_PRE, Pos.WB03_TS_STAB_PRE);
        return buffer;
    }

    public void setWb03TsStabPreNull(String wb03TsStabPreNull) {
        writeString(Pos.WB03_TS_STAB_PRE_NULL, wb03TsStabPreNull, Len.WB03_TS_STAB_PRE_NULL);
    }

    /**Original name: WB03-TS-STAB-PRE-NULL<br>*/
    public String getWb03TsStabPreNull() {
        return readString(Pos.WB03_TS_STAB_PRE_NULL, Len.WB03_TS_STAB_PRE_NULL);
    }

    public String getWb03TsStabPreNullFormatted() {
        return Functions.padBlanks(getWb03TsStabPreNull(), Len.WB03_TS_STAB_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_TS_STAB_PRE = 1;
        public static final int WB03_TS_STAB_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_TS_STAB_PRE = 8;
        public static final int WB03_TS_STAB_PRE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_TS_STAB_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_TS_STAB_PRE = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
