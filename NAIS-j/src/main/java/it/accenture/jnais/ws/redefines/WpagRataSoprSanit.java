package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-RATA-SOPR-SANIT<br>
 * Variable: WPAG-RATA-SOPR-SANIT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataSoprSanit extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataSoprSanit() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_SOPR_SANIT;
    }

    public void setWpagRataSoprSanit(AfDecimal wpagRataSoprSanit) {
        writeDecimalAsPacked(Pos.WPAG_RATA_SOPR_SANIT, wpagRataSoprSanit.copy());
    }

    public void setWpagRataSoprSanitFormatted(String wpagRataSoprSanit) {
        setWpagRataSoprSanit(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_RATA_SOPR_SANIT + Len.Fract.WPAG_RATA_SOPR_SANIT, Len.Fract.WPAG_RATA_SOPR_SANIT, wpagRataSoprSanit));
    }

    public void setWpagRataSoprSanitFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_SOPR_SANIT, Pos.WPAG_RATA_SOPR_SANIT);
    }

    /**Original name: WPAG-RATA-SOPR-SANIT<br>*/
    public AfDecimal getWpagRataSoprSanit() {
        return readPackedAsDecimal(Pos.WPAG_RATA_SOPR_SANIT, Len.Int.WPAG_RATA_SOPR_SANIT, Len.Fract.WPAG_RATA_SOPR_SANIT);
    }

    public byte[] getWpagRataSoprSanitAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_SOPR_SANIT, Pos.WPAG_RATA_SOPR_SANIT);
        return buffer;
    }

    public void initWpagRataSoprSanitSpaces() {
        fill(Pos.WPAG_RATA_SOPR_SANIT, Len.WPAG_RATA_SOPR_SANIT, Types.SPACE_CHAR);
    }

    public void setWpagRataSoprSanitNull(String wpagRataSoprSanitNull) {
        writeString(Pos.WPAG_RATA_SOPR_SANIT_NULL, wpagRataSoprSanitNull, Len.WPAG_RATA_SOPR_SANIT_NULL);
    }

    /**Original name: WPAG-RATA-SOPR-SANIT-NULL<br>*/
    public String getWpagRataSoprSanitNull() {
        return readString(Pos.WPAG_RATA_SOPR_SANIT_NULL, Len.WPAG_RATA_SOPR_SANIT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SOPR_SANIT = 1;
        public static final int WPAG_RATA_SOPR_SANIT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_SOPR_SANIT = 8;
        public static final int WPAG_RATA_SOPR_SANIT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SOPR_SANIT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_SOPR_SANIT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
