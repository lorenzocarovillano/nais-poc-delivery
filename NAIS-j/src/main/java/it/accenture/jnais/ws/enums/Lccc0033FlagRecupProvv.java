package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0033-FLAG-RECUP-PROVV<br>
 * Variable: LCCC0033-FLAG-RECUP-PROVV from copybook LCCC0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc0033FlagRecupProvv {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_RECUP_PROVV);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setFlagRecupProvv(String flagRecupProvv) {
        this.value = Functions.subString(flagRecupProvv, Len.FLAG_RECUP_PROVV);
    }

    public String getFlagRecupProvv() {
        return this.value;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_RECUP_PROVV = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
