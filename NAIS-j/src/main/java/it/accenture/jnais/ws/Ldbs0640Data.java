package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamMovi;
import it.accenture.jnais.copy.Ldbv0641;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0640<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0640Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-PARAM-MOVI
    private IndParamMovi indParamMovi = new IndParamMovi();
    //Original name: PARAM-MOVI-DB
    private DettTitContDb paramMoviDb = new DettTitContDb();
    //Original name: LDBV0641
    private Ldbv0641 ldbv0641 = new Ldbv0641();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndParamMovi getIndParamMovi() {
        return indParamMovi;
    }

    public Ldbv0641 getLdbv0641() {
        return ldbv0641;
    }

    public DettTitContDb getParamMoviDb() {
        return paramMoviDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
