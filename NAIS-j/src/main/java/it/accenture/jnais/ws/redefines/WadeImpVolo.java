package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMP-VOLO<br>
 * Variable: WADE-IMP-VOLO from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMP_VOLO;
    }

    public void setWadeImpVolo(AfDecimal wadeImpVolo) {
        writeDecimalAsPacked(Pos.WADE_IMP_VOLO, wadeImpVolo.copy());
    }

    public void setWadeImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMP_VOLO, Pos.WADE_IMP_VOLO);
    }

    /**Original name: WADE-IMP-VOLO<br>*/
    public AfDecimal getWadeImpVolo() {
        return readPackedAsDecimal(Pos.WADE_IMP_VOLO, Len.Int.WADE_IMP_VOLO, Len.Fract.WADE_IMP_VOLO);
    }

    public byte[] getWadeImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMP_VOLO, Pos.WADE_IMP_VOLO);
        return buffer;
    }

    public void initWadeImpVoloSpaces() {
        fill(Pos.WADE_IMP_VOLO, Len.WADE_IMP_VOLO, Types.SPACE_CHAR);
    }

    public void setWadeImpVoloNull(String wadeImpVoloNull) {
        writeString(Pos.WADE_IMP_VOLO_NULL, wadeImpVoloNull, Len.WADE_IMP_VOLO_NULL);
    }

    /**Original name: WADE-IMP-VOLO-NULL<br>*/
    public String getWadeImpVoloNull() {
        return readString(Pos.WADE_IMP_VOLO_NULL, Len.WADE_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMP_VOLO = 1;
        public static final int WADE_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMP_VOLO = 8;
        public static final int WADE_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
