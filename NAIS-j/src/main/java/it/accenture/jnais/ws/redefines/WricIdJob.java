package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WRIC-ID-JOB<br>
 * Variable: WRIC-ID-JOB from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WricIdJob extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WricIdJob() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIC_ID_JOB;
    }

    public void setWricIdJob(int wricIdJob) {
        writeIntAsPacked(Pos.WRIC_ID_JOB, wricIdJob, Len.Int.WRIC_ID_JOB);
    }

    public void setWricIdJobFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIC_ID_JOB, Pos.WRIC_ID_JOB);
    }

    /**Original name: WRIC-ID-JOB<br>*/
    public int getWricIdJob() {
        return readPackedAsInt(Pos.WRIC_ID_JOB, Len.Int.WRIC_ID_JOB);
    }

    public byte[] getWricIdJobAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIC_ID_JOB, Pos.WRIC_ID_JOB);
        return buffer;
    }

    public void setWricIdJobNull(String wricIdJobNull) {
        writeString(Pos.WRIC_ID_JOB_NULL, wricIdJobNull, Len.WRIC_ID_JOB_NULL);
    }

    /**Original name: WRIC-ID-JOB-NULL<br>*/
    public String getWricIdJobNull() {
        return readString(Pos.WRIC_ID_JOB_NULL, Len.WRIC_ID_JOB_NULL);
    }

    public String getWricIdJobNullFormatted() {
        return Functions.padBlanks(getWricIdJobNull(), Len.WRIC_ID_JOB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIC_ID_JOB = 1;
        public static final int WRIC_ID_JOB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_ID_JOB = 5;
        public static final int WRIC_ID_JOB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIC_ID_JOB = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
