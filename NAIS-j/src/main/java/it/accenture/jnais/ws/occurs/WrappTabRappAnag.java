package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WRAPP-TAB-RAPP-ANAG<br>
 * Variables: WRAPP-TAB-RAPP-ANAG from copybook LCCVRANA<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WrappTabRappAnag {

    //==== PROPERTIES ====
    //Original name: WRAN-ID-RAPP-ANA
    private int idRappAna = DefaultValues.INT_VAL;
    //Original name: WRAN-ID-RAPP-ANA-COLLG
    private int idRappAnaCollg = DefaultValues.INT_VAL;
    //Original name: WRAN-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: WRAN-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: WRAN-ID-MOVI-CRZ
    private int idMoviCrz = DefaultValues.INT_VAL;
    //Original name: WRAN-ID-MOVI-CHIU
    private int idMoviChiu = DefaultValues.INT_VAL;
    //Original name: WRAN-DT-INI-EFF
    private int dtIniEff = DefaultValues.INT_VAL;
    //Original name: WRAN-DT-END-EFF
    private int dtEndEff = DefaultValues.INT_VAL;
    //Original name: WRAN-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: WRAN-COD-SOGG
    private String codSogg = DefaultValues.stringVal(Len.COD_SOGG);
    //Original name: WRAN-TP-RAPP-ANA
    private String tpRappAna = DefaultValues.stringVal(Len.TP_RAPP_ANA);
    //Original name: WRAN-TP-PERS
    private char tpPers = DefaultValues.CHAR_VAL;
    //Original name: WRAN-SEX
    private char sex = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DT-NASC
    private int dtNasc = DefaultValues.INT_VAL;
    //Original name: WRAN-FL-ESTAS
    private char flEstas = DefaultValues.CHAR_VAL;
    //Original name: WRAN-INDIR-1
    private String indir1 = DefaultValues.stringVal(Len.INDIR1);
    //Original name: WRAN-INDIR-2
    private String indir2 = DefaultValues.stringVal(Len.INDIR2);
    //Original name: WRAN-INDIR-3
    private String indir3 = DefaultValues.stringVal(Len.INDIR3);
    //Original name: WRAN-TP-UTLZ-INDIR-1
    private String tpUtlzIndir1 = DefaultValues.stringVal(Len.TP_UTLZ_INDIR1);
    //Original name: WRAN-TP-UTLZ-INDIR-2
    private String tpUtlzIndir2 = DefaultValues.stringVal(Len.TP_UTLZ_INDIR2);
    //Original name: WRAN-TP-UTLZ-INDIR-3
    private String tpUtlzIndir3 = DefaultValues.stringVal(Len.TP_UTLZ_INDIR3);
    //Original name: WRAN-ESTR-CNT-CORR-ACCR
    private String estrCntCorrAccr = DefaultValues.stringVal(Len.ESTR_CNT_CORR_ACCR);
    //Original name: WRAN-ESTR-CNT-CORR-ADD
    private String estrCntCorrAdd = DefaultValues.stringVal(Len.ESTR_CNT_CORR_ADD);
    //Original name: WRAN-ESTR-DOCTO
    private String estrDocto = DefaultValues.stringVal(Len.ESTR_DOCTO);
    //Original name: WRAN-PC-NEL-RAPP
    private AfDecimal pcNelRapp = new AfDecimal(DefaultValues.DEC_VAL, 6, 3);
    //Original name: WRAN-TP-MEZ-PAG-ADD
    private String tpMezPagAdd = DefaultValues.stringVal(Len.TP_MEZ_PAG_ADD);
    //Original name: WRAN-TP-MEZ-PAG-ACCR
    private String tpMezPagAccr = DefaultValues.stringVal(Len.TP_MEZ_PAG_ACCR);
    //Original name: WRAN-COD-MATR
    private String codMatr = DefaultValues.stringVal(Len.COD_MATR);
    //Original name: WRAN-TP-ADEGZ
    private String tpAdegz = DefaultValues.stringVal(Len.TP_ADEGZ);
    //Original name: WRAN-FL-TST-RSH
    private char flTstRsh = DefaultValues.CHAR_VAL;
    //Original name: WRAN-COD-AZ
    private String codAz = DefaultValues.stringVal(Len.COD_AZ);
    //Original name: WRAN-IND-PRINC
    private String indPrinc = DefaultValues.stringVal(Len.IND_PRINC);
    //Original name: WRAN-DT-DELIBERA-CDA
    private int dtDeliberaCda = DefaultValues.INT_VAL;
    //Original name: WRAN-DLG-AL-RISC
    private char dlgAlRisc = DefaultValues.CHAR_VAL;
    //Original name: WRAN-LEGALE-RAPPR-PRINC
    private char legaleRapprPrinc = DefaultValues.CHAR_VAL;
    //Original name: WRAN-TP-LEGALE-RAPPR
    private String tpLegaleRappr = DefaultValues.stringVal(Len.TP_LEGALE_RAPPR);
    //Original name: WRAN-TP-IND-PRINC
    private String tpIndPrinc = DefaultValues.stringVal(Len.TP_IND_PRINC);
    //Original name: WRAN-TP-STAT-RID
    private String tpStatRid = DefaultValues.stringVal(Len.TP_STAT_RID);
    //Original name: WRAN-NOME-INT-RID-LEN
    private short nomeIntRidLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-NOME-INT-RID
    private String nomeIntRid = DefaultValues.stringVal(Len.NOME_INT_RID);
    //Original name: WRAN-COGN-INT-RID-LEN
    private short cognIntRidLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-COGN-INT-RID
    private String cognIntRid = DefaultValues.stringVal(Len.COGN_INT_RID);
    //Original name: WRAN-COGN-INT-TRATT-LEN
    private short cognIntTrattLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-COGN-INT-TRATT
    private String cognIntTratt = DefaultValues.stringVal(Len.COGN_INT_TRATT);
    //Original name: WRAN-NOME-INT-TRATT-LEN
    private short nomeIntTrattLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-NOME-INT-TRATT
    private String nomeIntTratt = DefaultValues.stringVal(Len.NOME_INT_TRATT);
    //Original name: WRAN-CF-INT-RID
    private String cfIntRid = DefaultValues.stringVal(Len.CF_INT_RID);
    //Original name: WRAN-FL-COINC-DIP-CNTR
    private char flCoincDipCntr = DefaultValues.CHAR_VAL;
    //Original name: WRAN-FL-COINC-INT-CNTR
    private char flCoincIntCntr = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DT-DECES
    private int dtDeces = DefaultValues.INT_VAL;
    //Original name: WRAN-FL-FUMATORE
    private char flFumatore = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DS-RIGA
    private long dsRiga = DefaultValues.LONG_VAL;
    //Original name: WRAN-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: WRAN-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: WRAN-DS-TS-INI-CPTZ
    private long dsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: WRAN-DS-TS-END-CPTZ
    private long dsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: WRAN-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: WRAN-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: WRAN-FL-LAV-DIP
    private char flLavDip = DefaultValues.CHAR_VAL;
    //Original name: WRAN-TP-VARZ-PAGAT
    private String tpVarzPagat = DefaultValues.stringVal(Len.TP_VARZ_PAGAT);
    //Original name: WRAN-COD-RID
    private String codRid = DefaultValues.stringVal(Len.COD_RID);
    //Original name: WRAN-TP-CAUS-RID
    private String tpCausRid = DefaultValues.stringVal(Len.TP_CAUS_RID);

    //==== METHODS ====
    public void setWrappTabRappAnagBytes(byte[] buffer) {
        setWrappTabRappAnagBytes(buffer, 1);
    }

    public byte[] getWrappTabRappAnagBytes() {
        byte[] buffer = new byte[Len.WRAPP_TAB_RAPP_ANAG];
        return getWrappTabRappAnagBytes(buffer, 1);
    }

    public void setWrappTabRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        idRappAna = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RAPP_ANA, 0);
        position += Len.ID_RAPP_ANA;
        idRappAnaCollg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_RAPP_ANA_COLLG, 0);
        position += Len.ID_RAPP_ANA_COLLG;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        idMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        idMoviChiu = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        dtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        dtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        codSogg = MarshalByte.readString(buffer, position, Len.COD_SOGG);
        position += Len.COD_SOGG;
        tpRappAna = MarshalByte.readString(buffer, position, Len.TP_RAPP_ANA);
        position += Len.TP_RAPP_ANA;
        tpPers = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        sex = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtNasc = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_NASC, 0);
        position += Len.DT_NASC;
        flEstas = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        indir1 = MarshalByte.readString(buffer, position, Len.INDIR1);
        position += Len.INDIR1;
        indir2 = MarshalByte.readString(buffer, position, Len.INDIR2);
        position += Len.INDIR2;
        indir3 = MarshalByte.readString(buffer, position, Len.INDIR3);
        position += Len.INDIR3;
        tpUtlzIndir1 = MarshalByte.readString(buffer, position, Len.TP_UTLZ_INDIR1);
        position += Len.TP_UTLZ_INDIR1;
        tpUtlzIndir2 = MarshalByte.readString(buffer, position, Len.TP_UTLZ_INDIR2);
        position += Len.TP_UTLZ_INDIR2;
        tpUtlzIndir3 = MarshalByte.readString(buffer, position, Len.TP_UTLZ_INDIR3);
        position += Len.TP_UTLZ_INDIR3;
        estrCntCorrAccr = MarshalByte.readString(buffer, position, Len.ESTR_CNT_CORR_ACCR);
        position += Len.ESTR_CNT_CORR_ACCR;
        estrCntCorrAdd = MarshalByte.readString(buffer, position, Len.ESTR_CNT_CORR_ADD);
        position += Len.ESTR_CNT_CORR_ADD;
        estrDocto = MarshalByte.readString(buffer, position, Len.ESTR_DOCTO);
        position += Len.ESTR_DOCTO;
        pcNelRapp.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.PC_NEL_RAPP, Len.Fract.PC_NEL_RAPP));
        position += Len.PC_NEL_RAPP;
        tpMezPagAdd = MarshalByte.readString(buffer, position, Len.TP_MEZ_PAG_ADD);
        position += Len.TP_MEZ_PAG_ADD;
        tpMezPagAccr = MarshalByte.readString(buffer, position, Len.TP_MEZ_PAG_ACCR);
        position += Len.TP_MEZ_PAG_ACCR;
        codMatr = MarshalByte.readString(buffer, position, Len.COD_MATR);
        position += Len.COD_MATR;
        tpAdegz = MarshalByte.readString(buffer, position, Len.TP_ADEGZ);
        position += Len.TP_ADEGZ;
        flTstRsh = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        codAz = MarshalByte.readString(buffer, position, Len.COD_AZ);
        position += Len.COD_AZ;
        indPrinc = MarshalByte.readString(buffer, position, Len.IND_PRINC);
        position += Len.IND_PRINC;
        dtDeliberaCda = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DELIBERA_CDA, 0);
        position += Len.DT_DELIBERA_CDA;
        dlgAlRisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        legaleRapprPrinc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpLegaleRappr = MarshalByte.readString(buffer, position, Len.TP_LEGALE_RAPPR);
        position += Len.TP_LEGALE_RAPPR;
        tpIndPrinc = MarshalByte.readString(buffer, position, Len.TP_IND_PRINC);
        position += Len.TP_IND_PRINC;
        tpStatRid = MarshalByte.readString(buffer, position, Len.TP_STAT_RID);
        position += Len.TP_STAT_RID;
        setNomeIntRidVcharBytes(buffer, position);
        position += Len.NOME_INT_RID_VCHAR;
        setCognIntRidVcharBytes(buffer, position);
        position += Len.COGN_INT_RID_VCHAR;
        setCognIntTrattVcharBytes(buffer, position);
        position += Len.COGN_INT_TRATT_VCHAR;
        setNomeIntTrattVcharBytes(buffer, position);
        position += Len.NOME_INT_TRATT_VCHAR;
        cfIntRid = MarshalByte.readString(buffer, position, Len.CF_INT_RID);
        position += Len.CF_INT_RID;
        flCoincDipCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flCoincIntCntr = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dtDeces = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECES, 0);
        position += Len.DT_DECES;
        flFumatore = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        dsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        flLavDip = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        tpVarzPagat = MarshalByte.readString(buffer, position, Len.TP_VARZ_PAGAT);
        position += Len.TP_VARZ_PAGAT;
        codRid = MarshalByte.readString(buffer, position, Len.COD_RID);
        position += Len.COD_RID;
        tpCausRid = MarshalByte.readString(buffer, position, Len.TP_CAUS_RID);
    }

    public byte[] getWrappTabRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idRappAna, Len.Int.ID_RAPP_ANA, 0);
        position += Len.ID_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, idRappAnaCollg, Len.Int.ID_RAPP_ANA_COLLG, 0);
        position += Len.ID_RAPP_ANA_COLLG;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviCrz, Len.Int.ID_MOVI_CRZ, 0);
        position += Len.ID_MOVI_CRZ;
        MarshalByte.writeIntAsPacked(buffer, position, idMoviChiu, Len.Int.ID_MOVI_CHIU, 0);
        position += Len.ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniEff, Len.Int.DT_INI_EFF, 0);
        position += Len.DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndEff, Len.Int.DT_END_EFF, 0);
        position += Len.DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, codSogg, Len.COD_SOGG);
        position += Len.COD_SOGG;
        MarshalByte.writeString(buffer, position, tpRappAna, Len.TP_RAPP_ANA);
        position += Len.TP_RAPP_ANA;
        MarshalByte.writeChar(buffer, position, tpPers);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, sex);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtNasc, Len.Int.DT_NASC, 0);
        position += Len.DT_NASC;
        MarshalByte.writeChar(buffer, position, flEstas);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, indir1, Len.INDIR1);
        position += Len.INDIR1;
        MarshalByte.writeString(buffer, position, indir2, Len.INDIR2);
        position += Len.INDIR2;
        MarshalByte.writeString(buffer, position, indir3, Len.INDIR3);
        position += Len.INDIR3;
        MarshalByte.writeString(buffer, position, tpUtlzIndir1, Len.TP_UTLZ_INDIR1);
        position += Len.TP_UTLZ_INDIR1;
        MarshalByte.writeString(buffer, position, tpUtlzIndir2, Len.TP_UTLZ_INDIR2);
        position += Len.TP_UTLZ_INDIR2;
        MarshalByte.writeString(buffer, position, tpUtlzIndir3, Len.TP_UTLZ_INDIR3);
        position += Len.TP_UTLZ_INDIR3;
        MarshalByte.writeString(buffer, position, estrCntCorrAccr, Len.ESTR_CNT_CORR_ACCR);
        position += Len.ESTR_CNT_CORR_ACCR;
        MarshalByte.writeString(buffer, position, estrCntCorrAdd, Len.ESTR_CNT_CORR_ADD);
        position += Len.ESTR_CNT_CORR_ADD;
        MarshalByte.writeString(buffer, position, estrDocto, Len.ESTR_DOCTO);
        position += Len.ESTR_DOCTO;
        MarshalByte.writeDecimalAsPacked(buffer, position, pcNelRapp.copy());
        position += Len.PC_NEL_RAPP;
        MarshalByte.writeString(buffer, position, tpMezPagAdd, Len.TP_MEZ_PAG_ADD);
        position += Len.TP_MEZ_PAG_ADD;
        MarshalByte.writeString(buffer, position, tpMezPagAccr, Len.TP_MEZ_PAG_ACCR);
        position += Len.TP_MEZ_PAG_ACCR;
        MarshalByte.writeString(buffer, position, codMatr, Len.COD_MATR);
        position += Len.COD_MATR;
        MarshalByte.writeString(buffer, position, tpAdegz, Len.TP_ADEGZ);
        position += Len.TP_ADEGZ;
        MarshalByte.writeChar(buffer, position, flTstRsh);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, codAz, Len.COD_AZ);
        position += Len.COD_AZ;
        MarshalByte.writeString(buffer, position, indPrinc, Len.IND_PRINC);
        position += Len.IND_PRINC;
        MarshalByte.writeIntAsPacked(buffer, position, dtDeliberaCda, Len.Int.DT_DELIBERA_CDA, 0);
        position += Len.DT_DELIBERA_CDA;
        MarshalByte.writeChar(buffer, position, dlgAlRisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, legaleRapprPrinc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpLegaleRappr, Len.TP_LEGALE_RAPPR);
        position += Len.TP_LEGALE_RAPPR;
        MarshalByte.writeString(buffer, position, tpIndPrinc, Len.TP_IND_PRINC);
        position += Len.TP_IND_PRINC;
        MarshalByte.writeString(buffer, position, tpStatRid, Len.TP_STAT_RID);
        position += Len.TP_STAT_RID;
        getNomeIntRidVcharBytes(buffer, position);
        position += Len.NOME_INT_RID_VCHAR;
        getCognIntRidVcharBytes(buffer, position);
        position += Len.COGN_INT_RID_VCHAR;
        getCognIntTrattVcharBytes(buffer, position);
        position += Len.COGN_INT_TRATT_VCHAR;
        getNomeIntTrattVcharBytes(buffer, position);
        position += Len.NOME_INT_TRATT_VCHAR;
        MarshalByte.writeString(buffer, position, cfIntRid, Len.CF_INT_RID);
        position += Len.CF_INT_RID;
        MarshalByte.writeChar(buffer, position, flCoincDipCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flCoincIntCntr);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dtDeces, Len.Int.DT_DECES, 0);
        position += Len.DT_DECES;
        MarshalByte.writeChar(buffer, position, flFumatore);
        position += Types.CHAR_SIZE;
        MarshalByte.writeLongAsPacked(buffer, position, dsRiga, Len.Int.DS_RIGA, 0);
        position += Len.DS_RIGA;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ, 0);
        position += Len.DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ, 0);
        position += Len.DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, flLavDip);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, tpVarzPagat, Len.TP_VARZ_PAGAT);
        position += Len.TP_VARZ_PAGAT;
        MarshalByte.writeString(buffer, position, codRid, Len.COD_RID);
        position += Len.COD_RID;
        MarshalByte.writeString(buffer, position, tpCausRid, Len.TP_CAUS_RID);
        return buffer;
    }

    public void setIdRappAna(int idRappAna) {
        this.idRappAna = idRappAna;
    }

    public int getIdRappAna() {
        return this.idRappAna;
    }

    public void setIdRappAnaCollg(int idRappAnaCollg) {
        this.idRappAnaCollg = idRappAnaCollg;
    }

    public int getIdRappAnaCollg() {
        return this.idRappAnaCollg;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIdMoviCrz(int idMoviCrz) {
        this.idMoviCrz = idMoviCrz;
    }

    public int getIdMoviCrz() {
        return this.idMoviCrz;
    }

    public void setIdMoviChiu(int idMoviChiu) {
        this.idMoviChiu = idMoviChiu;
    }

    public int getIdMoviChiu() {
        return this.idMoviChiu;
    }

    public void setDtIniEff(int dtIniEff) {
        this.dtIniEff = dtIniEff;
    }

    public int getDtIniEff() {
        return this.dtIniEff;
    }

    public void setDtEndEff(int dtEndEff) {
        this.dtEndEff = dtEndEff;
    }

    public int getDtEndEff() {
        return this.dtEndEff;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setCodSogg(String codSogg) {
        this.codSogg = Functions.subString(codSogg, Len.COD_SOGG);
    }

    public String getCodSogg() {
        return this.codSogg;
    }

    public void setTpRappAna(String tpRappAna) {
        this.tpRappAna = Functions.subString(tpRappAna, Len.TP_RAPP_ANA);
    }

    public String getTpRappAna() {
        return this.tpRappAna;
    }

    public void setTpPers(char tpPers) {
        this.tpPers = tpPers;
    }

    public char getTpPers() {
        return this.tpPers;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public char getSex() {
        return this.sex;
    }

    public void setDtNasc(int dtNasc) {
        this.dtNasc = dtNasc;
    }

    public int getDtNasc() {
        return this.dtNasc;
    }

    public void setFlEstas(char flEstas) {
        this.flEstas = flEstas;
    }

    public char getFlEstas() {
        return this.flEstas;
    }

    public void setIndir1(String indir1) {
        this.indir1 = Functions.subString(indir1, Len.INDIR1);
    }

    public String getIndir1() {
        return this.indir1;
    }

    public void setIndir2(String indir2) {
        this.indir2 = Functions.subString(indir2, Len.INDIR2);
    }

    public String getIndir2() {
        return this.indir2;
    }

    public void setIndir3(String indir3) {
        this.indir3 = Functions.subString(indir3, Len.INDIR3);
    }

    public String getIndir3() {
        return this.indir3;
    }

    public void setTpUtlzIndir1(String tpUtlzIndir1) {
        this.tpUtlzIndir1 = Functions.subString(tpUtlzIndir1, Len.TP_UTLZ_INDIR1);
    }

    public String getTpUtlzIndir1() {
        return this.tpUtlzIndir1;
    }

    public void setTpUtlzIndir2(String tpUtlzIndir2) {
        this.tpUtlzIndir2 = Functions.subString(tpUtlzIndir2, Len.TP_UTLZ_INDIR2);
    }

    public String getTpUtlzIndir2() {
        return this.tpUtlzIndir2;
    }

    public void setTpUtlzIndir3(String tpUtlzIndir3) {
        this.tpUtlzIndir3 = Functions.subString(tpUtlzIndir3, Len.TP_UTLZ_INDIR3);
    }

    public String getTpUtlzIndir3() {
        return this.tpUtlzIndir3;
    }

    public void setEstrCntCorrAccr(String estrCntCorrAccr) {
        this.estrCntCorrAccr = Functions.subString(estrCntCorrAccr, Len.ESTR_CNT_CORR_ACCR);
    }

    public String getEstrCntCorrAccr() {
        return this.estrCntCorrAccr;
    }

    public void setEstrCntCorrAdd(String estrCntCorrAdd) {
        this.estrCntCorrAdd = Functions.subString(estrCntCorrAdd, Len.ESTR_CNT_CORR_ADD);
    }

    public String getEstrCntCorrAdd() {
        return this.estrCntCorrAdd;
    }

    public void setEstrDocto(String estrDocto) {
        this.estrDocto = Functions.subString(estrDocto, Len.ESTR_DOCTO);
    }

    public String getEstrDocto() {
        return this.estrDocto;
    }

    public void setPcNelRapp(AfDecimal pcNelRapp) {
        this.pcNelRapp.assign(pcNelRapp);
    }

    public AfDecimal getPcNelRapp() {
        return this.pcNelRapp.copy();
    }

    public void setTpMezPagAdd(String tpMezPagAdd) {
        this.tpMezPagAdd = Functions.subString(tpMezPagAdd, Len.TP_MEZ_PAG_ADD);
    }

    public String getTpMezPagAdd() {
        return this.tpMezPagAdd;
    }

    public void setTpMezPagAccr(String tpMezPagAccr) {
        this.tpMezPagAccr = Functions.subString(tpMezPagAccr, Len.TP_MEZ_PAG_ACCR);
    }

    public String getTpMezPagAccr() {
        return this.tpMezPagAccr;
    }

    public void setCodMatr(String codMatr) {
        this.codMatr = Functions.subString(codMatr, Len.COD_MATR);
    }

    public String getCodMatr() {
        return this.codMatr;
    }

    public void setTpAdegz(String tpAdegz) {
        this.tpAdegz = Functions.subString(tpAdegz, Len.TP_ADEGZ);
    }

    public String getTpAdegz() {
        return this.tpAdegz;
    }

    public void setFlTstRsh(char flTstRsh) {
        this.flTstRsh = flTstRsh;
    }

    public char getFlTstRsh() {
        return this.flTstRsh;
    }

    public void setCodAz(String codAz) {
        this.codAz = Functions.subString(codAz, Len.COD_AZ);
    }

    public String getCodAz() {
        return this.codAz;
    }

    public void setIndPrinc(String indPrinc) {
        this.indPrinc = Functions.subString(indPrinc, Len.IND_PRINC);
    }

    public String getIndPrinc() {
        return this.indPrinc;
    }

    public void setDtDeliberaCda(int dtDeliberaCda) {
        this.dtDeliberaCda = dtDeliberaCda;
    }

    public int getDtDeliberaCda() {
        return this.dtDeliberaCda;
    }

    public void setDlgAlRisc(char dlgAlRisc) {
        this.dlgAlRisc = dlgAlRisc;
    }

    public char getDlgAlRisc() {
        return this.dlgAlRisc;
    }

    public void setLegaleRapprPrinc(char legaleRapprPrinc) {
        this.legaleRapprPrinc = legaleRapprPrinc;
    }

    public char getLegaleRapprPrinc() {
        return this.legaleRapprPrinc;
    }

    public void setTpLegaleRappr(String tpLegaleRappr) {
        this.tpLegaleRappr = Functions.subString(tpLegaleRappr, Len.TP_LEGALE_RAPPR);
    }

    public String getTpLegaleRappr() {
        return this.tpLegaleRappr;
    }

    public void setTpIndPrinc(String tpIndPrinc) {
        this.tpIndPrinc = Functions.subString(tpIndPrinc, Len.TP_IND_PRINC);
    }

    public String getTpIndPrinc() {
        return this.tpIndPrinc;
    }

    public void setTpStatRid(String tpStatRid) {
        this.tpStatRid = Functions.subString(tpStatRid, Len.TP_STAT_RID);
    }

    public String getTpStatRid() {
        return this.tpStatRid;
    }

    public void setNomeIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        nomeIntRidLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        nomeIntRid = MarshalByte.readString(buffer, position, Len.NOME_INT_RID);
    }

    public byte[] getNomeIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, nomeIntRidLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, nomeIntRid, Len.NOME_INT_RID);
        return buffer;
    }

    public void setNomeIntRidLen(short nomeIntRidLen) {
        this.nomeIntRidLen = nomeIntRidLen;
    }

    public short getNomeIntRidLen() {
        return this.nomeIntRidLen;
    }

    public void setNomeIntRid(String nomeIntRid) {
        this.nomeIntRid = Functions.subString(nomeIntRid, Len.NOME_INT_RID);
    }

    public String getNomeIntRid() {
        return this.nomeIntRid;
    }

    public void setCognIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        cognIntRidLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        cognIntRid = MarshalByte.readString(buffer, position, Len.COGN_INT_RID);
    }

    public byte[] getCognIntRidVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, cognIntRidLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, cognIntRid, Len.COGN_INT_RID);
        return buffer;
    }

    public void setCognIntRidLen(short cognIntRidLen) {
        this.cognIntRidLen = cognIntRidLen;
    }

    public short getCognIntRidLen() {
        return this.cognIntRidLen;
    }

    public void setCognIntRid(String cognIntRid) {
        this.cognIntRid = Functions.subString(cognIntRid, Len.COGN_INT_RID);
    }

    public String getCognIntRid() {
        return this.cognIntRid;
    }

    public void setCognIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        cognIntTrattLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        cognIntTratt = MarshalByte.readString(buffer, position, Len.COGN_INT_TRATT);
    }

    public byte[] getCognIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, cognIntTrattLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, cognIntTratt, Len.COGN_INT_TRATT);
        return buffer;
    }

    public void setCognIntTrattLen(short cognIntTrattLen) {
        this.cognIntTrattLen = cognIntTrattLen;
    }

    public short getCognIntTrattLen() {
        return this.cognIntTrattLen;
    }

    public void setCognIntTratt(String cognIntTratt) {
        this.cognIntTratt = Functions.subString(cognIntTratt, Len.COGN_INT_TRATT);
    }

    public String getCognIntTratt() {
        return this.cognIntTratt;
    }

    public void setNomeIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        nomeIntTrattLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        nomeIntTratt = MarshalByte.readString(buffer, position, Len.NOME_INT_TRATT);
    }

    public byte[] getNomeIntTrattVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, nomeIntTrattLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, nomeIntTratt, Len.NOME_INT_TRATT);
        return buffer;
    }

    public void setNomeIntTrattLen(short nomeIntTrattLen) {
        this.nomeIntTrattLen = nomeIntTrattLen;
    }

    public short getNomeIntTrattLen() {
        return this.nomeIntTrattLen;
    }

    public void setNomeIntTratt(String nomeIntTratt) {
        this.nomeIntTratt = Functions.subString(nomeIntTratt, Len.NOME_INT_TRATT);
    }

    public String getNomeIntTratt() {
        return this.nomeIntTratt;
    }

    public void setCfIntRid(String cfIntRid) {
        this.cfIntRid = Functions.subString(cfIntRid, Len.CF_INT_RID);
    }

    public String getCfIntRid() {
        return this.cfIntRid;
    }

    public void setFlCoincDipCntr(char flCoincDipCntr) {
        this.flCoincDipCntr = flCoincDipCntr;
    }

    public char getFlCoincDipCntr() {
        return this.flCoincDipCntr;
    }

    public void setFlCoincIntCntr(char flCoincIntCntr) {
        this.flCoincIntCntr = flCoincIntCntr;
    }

    public char getFlCoincIntCntr() {
        return this.flCoincIntCntr;
    }

    public void setDtDeces(int dtDeces) {
        this.dtDeces = dtDeces;
    }

    public int getDtDeces() {
        return this.dtDeces;
    }

    public void setFlFumatore(char flFumatore) {
        this.flFumatore = flFumatore;
    }

    public char getFlFumatore() {
        return this.flFumatore;
    }

    public void setDsRiga(long dsRiga) {
        this.dsRiga = dsRiga;
    }

    public long getDsRiga() {
        return this.dsRiga;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dsTsIniCptz = dsTsIniCptz;
    }

    public long getDsTsIniCptz() {
        return this.dsTsIniCptz;
    }

    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dsTsEndCptz = dsTsEndCptz;
    }

    public long getDsTsEndCptz() {
        return this.dsTsEndCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setFlLavDip(char flLavDip) {
        this.flLavDip = flLavDip;
    }

    public char getFlLavDip() {
        return this.flLavDip;
    }

    public void setTpVarzPagat(String tpVarzPagat) {
        this.tpVarzPagat = Functions.subString(tpVarzPagat, Len.TP_VARZ_PAGAT);
    }

    public String getTpVarzPagat() {
        return this.tpVarzPagat;
    }

    public void setCodRid(String codRid) {
        this.codRid = Functions.subString(codRid, Len.COD_RID);
    }

    public String getCodRid() {
        return this.codRid;
    }

    public void setTpCausRid(String tpCausRid) {
        this.tpCausRid = Functions.subString(tpCausRid, Len.TP_CAUS_RID);
    }

    public String getTpCausRid() {
        return this.tpCausRid;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_OGG = 2;
        public static final int COD_SOGG = 20;
        public static final int TP_RAPP_ANA = 2;
        public static final int INDIR1 = 20;
        public static final int INDIR2 = 20;
        public static final int INDIR3 = 20;
        public static final int TP_UTLZ_INDIR1 = 2;
        public static final int TP_UTLZ_INDIR2 = 2;
        public static final int TP_UTLZ_INDIR3 = 2;
        public static final int ESTR_CNT_CORR_ACCR = 20;
        public static final int ESTR_CNT_CORR_ADD = 20;
        public static final int ESTR_DOCTO = 20;
        public static final int TP_MEZ_PAG_ADD = 2;
        public static final int TP_MEZ_PAG_ACCR = 2;
        public static final int COD_MATR = 20;
        public static final int TP_ADEGZ = 2;
        public static final int COD_AZ = 30;
        public static final int IND_PRINC = 2;
        public static final int TP_LEGALE_RAPPR = 2;
        public static final int TP_IND_PRINC = 2;
        public static final int TP_STAT_RID = 2;
        public static final int NOME_INT_RID = 100;
        public static final int COGN_INT_RID = 100;
        public static final int COGN_INT_TRATT = 100;
        public static final int NOME_INT_TRATT = 100;
        public static final int CF_INT_RID = 16;
        public static final int DS_UTENTE = 20;
        public static final int TP_VARZ_PAGAT = 2;
        public static final int COD_RID = 11;
        public static final int TP_CAUS_RID = 2;
        public static final int ID_RAPP_ANA = 5;
        public static final int ID_RAPP_ANA_COLLG = 5;
        public static final int ID_OGG = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_NASC = 5;
        public static final int PC_NEL_RAPP = 4;
        public static final int DT_DELIBERA_CDA = 5;
        public static final int NOME_INT_RID_LEN = 2;
        public static final int NOME_INT_RID_VCHAR = NOME_INT_RID_LEN + NOME_INT_RID;
        public static final int COGN_INT_RID_LEN = 2;
        public static final int COGN_INT_RID_VCHAR = COGN_INT_RID_LEN + COGN_INT_RID;
        public static final int COGN_INT_TRATT_LEN = 2;
        public static final int COGN_INT_TRATT_VCHAR = COGN_INT_TRATT_LEN + COGN_INT_TRATT;
        public static final int NOME_INT_TRATT_LEN = 2;
        public static final int NOME_INT_TRATT_VCHAR = NOME_INT_TRATT_LEN + NOME_INT_TRATT;
        public static final int DT_DECES = 5;
        public static final int DS_RIGA = 6;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int TP_PERS = 1;
        public static final int SEX = 1;
        public static final int FL_ESTAS = 1;
        public static final int FL_TST_RSH = 1;
        public static final int DLG_AL_RISC = 1;
        public static final int LEGALE_RAPPR_PRINC = 1;
        public static final int FL_COINC_DIP_CNTR = 1;
        public static final int FL_COINC_INT_CNTR = 1;
        public static final int FL_FUMATORE = 1;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_STATO_ELAB = 1;
        public static final int FL_LAV_DIP = 1;
        public static final int WRAPP_TAB_RAPP_ANAG = ID_RAPP_ANA + ID_RAPP_ANA_COLLG + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_SOGG + TP_RAPP_ANA + TP_PERS + SEX + DT_NASC + FL_ESTAS + INDIR1 + INDIR2 + INDIR3 + TP_UTLZ_INDIR1 + TP_UTLZ_INDIR2 + TP_UTLZ_INDIR3 + ESTR_CNT_CORR_ACCR + ESTR_CNT_CORR_ADD + ESTR_DOCTO + PC_NEL_RAPP + TP_MEZ_PAG_ADD + TP_MEZ_PAG_ACCR + COD_MATR + TP_ADEGZ + FL_TST_RSH + COD_AZ + IND_PRINC + DT_DELIBERA_CDA + DLG_AL_RISC + LEGALE_RAPPR_PRINC + TP_LEGALE_RAPPR + TP_IND_PRINC + TP_STAT_RID + NOME_INT_RID_VCHAR + COGN_INT_RID_VCHAR + COGN_INT_TRATT_VCHAR + NOME_INT_TRATT_VCHAR + CF_INT_RID + FL_COINC_DIP_CNTR + FL_COINC_INT_CNTR + DT_DECES + FL_FUMATORE + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + FL_LAV_DIP + TP_VARZ_PAGAT + COD_RID + TP_CAUS_RID;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_RAPP_ANA = 9;
            public static final int ID_RAPP_ANA_COLLG = 9;
            public static final int ID_OGG = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_NASC = 8;
            public static final int PC_NEL_RAPP = 3;
            public static final int DT_DELIBERA_CDA = 8;
            public static final int DT_DECES = 8;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PC_NEL_RAPP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
