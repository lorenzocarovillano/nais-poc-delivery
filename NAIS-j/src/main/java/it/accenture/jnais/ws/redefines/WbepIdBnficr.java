package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WBEP-ID-BNFICR<br>
 * Variable: WBEP-ID-BNFICR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbepIdBnficr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbepIdBnficr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEP_ID_BNFICR;
    }

    public void setWbepIdBnficr(int wbepIdBnficr) {
        writeIntAsPacked(Pos.WBEP_ID_BNFICR, wbepIdBnficr, Len.Int.WBEP_ID_BNFICR);
    }

    public void setWbepIdBnficrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEP_ID_BNFICR, Pos.WBEP_ID_BNFICR);
    }

    /**Original name: WBEP-ID-BNFICR<br>*/
    public int getWbepIdBnficr() {
        return readPackedAsInt(Pos.WBEP_ID_BNFICR, Len.Int.WBEP_ID_BNFICR);
    }

    public byte[] getWbepIdBnficrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEP_ID_BNFICR, Pos.WBEP_ID_BNFICR);
        return buffer;
    }

    public void initWbepIdBnficrSpaces() {
        fill(Pos.WBEP_ID_BNFICR, Len.WBEP_ID_BNFICR, Types.SPACE_CHAR);
    }

    public void setWbepIdBnficrNull(String wbepIdBnficrNull) {
        writeString(Pos.WBEP_ID_BNFICR_NULL, wbepIdBnficrNull, Len.WBEP_ID_BNFICR_NULL);
    }

    /**Original name: WBEP-ID-BNFICR-NULL<br>*/
    public String getWbepIdBnficrNull() {
        return readString(Pos.WBEP_ID_BNFICR_NULL, Len.WBEP_ID_BNFICR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEP_ID_BNFICR = 1;
        public static final int WBEP_ID_BNFICR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEP_ID_BNFICR = 5;
        public static final int WBEP_ID_BNFICR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEP_ID_BNFICR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
