package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-CANONE-ANTIC<br>
 * Variable: WP67-IMP-CANONE-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpCanoneAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpCanoneAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_CANONE_ANTIC;
    }

    public void setWp67ImpCanoneAntic(AfDecimal wp67ImpCanoneAntic) {
        writeDecimalAsPacked(Pos.WP67_IMP_CANONE_ANTIC, wp67ImpCanoneAntic.copy());
    }

    public void setWp67ImpCanoneAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_CANONE_ANTIC, Pos.WP67_IMP_CANONE_ANTIC);
    }

    /**Original name: WP67-IMP-CANONE-ANTIC<br>*/
    public AfDecimal getWp67ImpCanoneAntic() {
        return readPackedAsDecimal(Pos.WP67_IMP_CANONE_ANTIC, Len.Int.WP67_IMP_CANONE_ANTIC, Len.Fract.WP67_IMP_CANONE_ANTIC);
    }

    public byte[] getWp67ImpCanoneAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_CANONE_ANTIC, Pos.WP67_IMP_CANONE_ANTIC);
        return buffer;
    }

    public void setWp67ImpCanoneAnticNull(String wp67ImpCanoneAnticNull) {
        writeString(Pos.WP67_IMP_CANONE_ANTIC_NULL, wp67ImpCanoneAnticNull, Len.WP67_IMP_CANONE_ANTIC_NULL);
    }

    /**Original name: WP67-IMP-CANONE-ANTIC-NULL<br>*/
    public String getWp67ImpCanoneAnticNull() {
        return readString(Pos.WP67_IMP_CANONE_ANTIC_NULL, Len.WP67_IMP_CANONE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_CANONE_ANTIC = 1;
        public static final int WP67_IMP_CANONE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_CANONE_ANTIC = 8;
        public static final int WP67_IMP_CANONE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_CANONE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_CANONE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
