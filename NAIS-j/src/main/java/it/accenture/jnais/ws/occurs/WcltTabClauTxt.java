package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvclt1;
import it.accenture.jnais.copy.WcltDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WCLT-TAB-CLAU-TXT<br>
 * Variables: WCLT-TAB-CLAU-TXT from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WcltTabClauTxt {

    //==== PROPERTIES ====
    //Original name: LCCVCLT1
    private Lccvclt1 lccvclt1 = new Lccvclt1();

    //==== METHODS ====
    public void setWcltTabClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvclt1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvclt1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvclt1.Len.Int.ID_PTF, 0));
        position += Lccvclt1.Len.ID_PTF;
        lccvclt1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWcltTabClauTxtBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvclt1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvclt1.getIdPtf(), Lccvclt1.Len.Int.ID_PTF, 0);
        position += Lccvclt1.Len.ID_PTF;
        lccvclt1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWcltTabClauTxtSpaces() {
        lccvclt1.initLccvclt1Spaces();
    }

    public Lccvclt1 getLccvclt1() {
        return lccvclt1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WCLT_TAB_CLAU_TXT = WpolStatus.Len.STATUS + Lccvclt1.Len.ID_PTF + WcltDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
