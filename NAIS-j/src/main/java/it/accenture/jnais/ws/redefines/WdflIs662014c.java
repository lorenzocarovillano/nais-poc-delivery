package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IS-662014C<br>
 * Variable: WDFL-IS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIs662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIs662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IS662014C;
    }

    public void setWdflIs662014c(AfDecimal wdflIs662014c) {
        writeDecimalAsPacked(Pos.WDFL_IS662014C, wdflIs662014c.copy());
    }

    public void setWdflIs662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IS662014C, Pos.WDFL_IS662014C);
    }

    /**Original name: WDFL-IS-662014C<br>*/
    public AfDecimal getWdflIs662014c() {
        return readPackedAsDecimal(Pos.WDFL_IS662014C, Len.Int.WDFL_IS662014C, Len.Fract.WDFL_IS662014C);
    }

    public byte[] getWdflIs662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IS662014C, Pos.WDFL_IS662014C);
        return buffer;
    }

    public void setWdflIs662014cNull(String wdflIs662014cNull) {
        writeString(Pos.WDFL_IS662014C_NULL, wdflIs662014cNull, Len.WDFL_IS662014C_NULL);
    }

    /**Original name: WDFL-IS-662014C-NULL<br>*/
    public String getWdflIs662014cNull() {
        return readString(Pos.WDFL_IS662014C_NULL, Len.WDFL_IS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IS662014C = 1;
        public static final int WDFL_IS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IS662014C = 8;
        public static final int WDFL_IS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
