package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-DT-VALZZ-QUO-DT-CA<br>
 * Variable: B01-DT-VALZZ-QUO-DT-CA from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01DtValzzQuoDtCa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01DtValzzQuoDtCa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_DT_VALZZ_QUO_DT_CA;
    }

    public void setB01DtValzzQuoDtCa(int b01DtValzzQuoDtCa) {
        writeIntAsPacked(Pos.B01_DT_VALZZ_QUO_DT_CA, b01DtValzzQuoDtCa, Len.Int.B01_DT_VALZZ_QUO_DT_CA);
    }

    public void setB01DtValzzQuoDtCaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_DT_VALZZ_QUO_DT_CA, Pos.B01_DT_VALZZ_QUO_DT_CA);
    }

    /**Original name: B01-DT-VALZZ-QUO-DT-CA<br>*/
    public int getB01DtValzzQuoDtCa() {
        return readPackedAsInt(Pos.B01_DT_VALZZ_QUO_DT_CA, Len.Int.B01_DT_VALZZ_QUO_DT_CA);
    }

    public byte[] getB01DtValzzQuoDtCaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_DT_VALZZ_QUO_DT_CA, Pos.B01_DT_VALZZ_QUO_DT_CA);
        return buffer;
    }

    public void setB01DtValzzQuoDtCaNull(String b01DtValzzQuoDtCaNull) {
        writeString(Pos.B01_DT_VALZZ_QUO_DT_CA_NULL, b01DtValzzQuoDtCaNull, Len.B01_DT_VALZZ_QUO_DT_CA_NULL);
    }

    /**Original name: B01-DT-VALZZ-QUO-DT-CA-NULL<br>*/
    public String getB01DtValzzQuoDtCaNull() {
        return readString(Pos.B01_DT_VALZZ_QUO_DT_CA_NULL, Len.B01_DT_VALZZ_QUO_DT_CA_NULL);
    }

    public String getB01DtValzzQuoDtCaNullFormatted() {
        return Functions.padBlanks(getB01DtValzzQuoDtCaNull(), Len.B01_DT_VALZZ_QUO_DT_CA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_DT_VALZZ_QUO_DT_CA = 1;
        public static final int B01_DT_VALZZ_QUO_DT_CA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_DT_VALZZ_QUO_DT_CA = 5;
        public static final int B01_DT_VALZZ_QUO_DT_CA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_DT_VALZZ_QUO_DT_CA = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
