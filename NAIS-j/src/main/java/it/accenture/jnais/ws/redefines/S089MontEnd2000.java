package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-MONT-END2000<br>
 * Variable: S089-MONT-END2000 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089MontEnd2000 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089MontEnd2000() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_MONT_END2000;
    }

    public void setWlquMontEnd2000(AfDecimal wlquMontEnd2000) {
        writeDecimalAsPacked(Pos.S089_MONT_END2000, wlquMontEnd2000.copy());
    }

    public void setWlquMontEnd2000FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_MONT_END2000, Pos.S089_MONT_END2000);
    }

    /**Original name: WLQU-MONT-END2000<br>*/
    public AfDecimal getWlquMontEnd2000() {
        return readPackedAsDecimal(Pos.S089_MONT_END2000, Len.Int.WLQU_MONT_END2000, Len.Fract.WLQU_MONT_END2000);
    }

    public byte[] getWlquMontEnd2000AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_MONT_END2000, Pos.S089_MONT_END2000);
        return buffer;
    }

    public void initWlquMontEnd2000Spaces() {
        fill(Pos.S089_MONT_END2000, Len.S089_MONT_END2000, Types.SPACE_CHAR);
    }

    public void setWlquMontEnd2000Null(String wlquMontEnd2000Null) {
        writeString(Pos.S089_MONT_END2000_NULL, wlquMontEnd2000Null, Len.WLQU_MONT_END2000_NULL);
    }

    /**Original name: WLQU-MONT-END2000-NULL<br>*/
    public String getWlquMontEnd2000Null() {
        return readString(Pos.S089_MONT_END2000_NULL, Len.WLQU_MONT_END2000_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_MONT_END2000 = 1;
        public static final int S089_MONT_END2000_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_MONT_END2000 = 8;
        public static final int WLQU_MONT_END2000_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_MONT_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_MONT_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
