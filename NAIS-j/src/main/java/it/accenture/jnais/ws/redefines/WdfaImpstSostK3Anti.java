package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-SOST-K3-ANTI<br>
 * Variable: WDFA-IMPST-SOST-K3-ANTI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstSostK3Anti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstSostK3Anti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_SOST_K3_ANTI;
    }

    public void setWdfaImpstSostK3Anti(AfDecimal wdfaImpstSostK3Anti) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_SOST_K3_ANTI, wdfaImpstSostK3Anti.copy());
    }

    public void setWdfaImpstSostK3AntiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_SOST_K3_ANTI, Pos.WDFA_IMPST_SOST_K3_ANTI);
    }

    /**Original name: WDFA-IMPST-SOST-K3-ANTI<br>*/
    public AfDecimal getWdfaImpstSostK3Anti() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_SOST_K3_ANTI, Len.Int.WDFA_IMPST_SOST_K3_ANTI, Len.Fract.WDFA_IMPST_SOST_K3_ANTI);
    }

    public byte[] getWdfaImpstSostK3AntiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_SOST_K3_ANTI, Pos.WDFA_IMPST_SOST_K3_ANTI);
        return buffer;
    }

    public void setWdfaImpstSostK3AntiNull(String wdfaImpstSostK3AntiNull) {
        writeString(Pos.WDFA_IMPST_SOST_K3_ANTI_NULL, wdfaImpstSostK3AntiNull, Len.WDFA_IMPST_SOST_K3_ANTI_NULL);
    }

    /**Original name: WDFA-IMPST-SOST-K3-ANTI-NULL<br>*/
    public String getWdfaImpstSostK3AntiNull() {
        return readString(Pos.WDFA_IMPST_SOST_K3_ANTI_NULL, Len.WDFA_IMPST_SOST_K3_ANTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K3_ANTI = 1;
        public static final int WDFA_IMPST_SOST_K3_ANTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_SOST_K3_ANTI = 8;
        public static final int WDFA_IMPST_SOST_K3_ANTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K3_ANTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_SOST_K3_ANTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
