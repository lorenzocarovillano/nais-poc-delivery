package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-BNSRIC<br>
 * Variable: RST-RIS-BNSRIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisBnsric extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisBnsric() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_BNSRIC;
    }

    public void setRstRisBnsric(AfDecimal rstRisBnsric) {
        writeDecimalAsPacked(Pos.RST_RIS_BNSRIC, rstRisBnsric.copy());
    }

    public void setRstRisBnsricFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_BNSRIC, Pos.RST_RIS_BNSRIC);
    }

    /**Original name: RST-RIS-BNSRIC<br>*/
    public AfDecimal getRstRisBnsric() {
        return readPackedAsDecimal(Pos.RST_RIS_BNSRIC, Len.Int.RST_RIS_BNSRIC, Len.Fract.RST_RIS_BNSRIC);
    }

    public byte[] getRstRisBnsricAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_BNSRIC, Pos.RST_RIS_BNSRIC);
        return buffer;
    }

    public void setRstRisBnsricNull(String rstRisBnsricNull) {
        writeString(Pos.RST_RIS_BNSRIC_NULL, rstRisBnsricNull, Len.RST_RIS_BNSRIC_NULL);
    }

    /**Original name: RST-RIS-BNSRIC-NULL<br>*/
    public String getRstRisBnsricNull() {
        return readString(Pos.RST_RIS_BNSRIC_NULL, Len.RST_RIS_BNSRIC_NULL);
    }

    public String getRstRisBnsricNullFormatted() {
        return Functions.padBlanks(getRstRisBnsricNull(), Len.RST_RIS_BNSRIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_BNSRIC = 1;
        public static final int RST_RIS_BNSRIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_BNSRIC = 8;
        public static final int RST_RIS_BNSRIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_BNSRIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_BNSRIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
