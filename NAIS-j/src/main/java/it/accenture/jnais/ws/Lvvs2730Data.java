package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagFineLetturaP58;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0037;
import it.accenture.jnais.ws.occurs.Wp58TabImpstBol;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS2730<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs2730Data {

    //==== PROPERTIES ====
    public static final int DP58_TAB_P58_MAXOCCURS = 75;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS2730";
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    //Original name: LDBSE590
    private String ldbse590 = "LDBSE590";
    //Original name: WK-DATA-SEPARATA
    private WkDataSeparata wkDataSeparata = new WkDataSeparata();
    //Original name: WK-DATA-INIZIO
    private WkDataInizio wkDataInizio = new WkDataInizio();
    //Original name: WK-DATA-INIZIO-D
    private int wkDataInizioD = DefaultValues.INT_VAL;
    //Original name: WK-DATA-FINE
    private WkDataFine wkDataFine = new WkDataFine();
    //Original name: WK-DATA-FINE-D
    private int wkDataFineD = DefaultValues.INT_VAL;
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: FLAG-CUR-MOV
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: FLAG-FINE-LETTURA-P58
    private FlagFineLetturaP58 flagFineLetturaP58 = new FlagFineLetturaP58();
    //Original name: WK-VAR-MOVI-COMUN
    private WkVarMoviComun wkVarMoviComun = new WkVarMoviComun();
    //Original name: IX-TAB-P58
    private int ixTabP58 = 0;
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: IMPST-BOLLO
    private ImpstBollo impstBollo = new ImpstBollo();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>*****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0037 wsMovimento = new WsMovimentoLvvs0037();
    //Original name: DP58-ELE-P58-MAX
    private short dp58EleP58Max = DefaultValues.BIN_SHORT_VAL;
    //Original name: DP58-TAB-P58
    private Wp58TabImpstBol[] dp58TabP58 = new Wp58TabImpstBol[DP58_TAB_P58_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ISO
    private short ixTabIso = DefaultValues.BIN_SHORT_VAL;
    //Original name: INT-REGISTER1
    private int intRegister1 = DefaultValues.INT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs2730Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dp58TabP58Idx = 1; dp58TabP58Idx <= DP58_TAB_P58_MAXOCCURS; dp58TabP58Idx++) {
            dp58TabP58[dp58TabP58Idx - 1] = new Wp58TabImpstBol();
        }
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public String getLdbse590() {
        return this.ldbse590;
    }

    public void setWkDataInizioD(int wkDataInizioD) {
        this.wkDataInizioD = wkDataInizioD;
    }

    public int getWkDataInizioD() {
        return this.wkDataInizioD;
    }

    public void setWkDataFineD(int wkDataFineD) {
        this.wkDataFineD = wkDataFineD;
    }

    public int getWkDataFineD() {
        return this.wkDataFineD;
    }

    public void setIxTabP58(int ixTabP58) {
        this.ixTabP58 = ixTabP58;
    }

    public int getIxTabP58() {
        return this.ixTabP58;
    }

    public void setDp58AreaP58Formatted(String data) {
        byte[] buffer = new byte[Len.DP58_AREA_P58];
        MarshalByte.writeString(buffer, 1, data, Len.DP58_AREA_P58);
        setDp58AreaP58Bytes(buffer, 1);
    }

    public void setDp58AreaP58Bytes(byte[] buffer, int offset) {
        int position = offset;
        dp58EleP58Max = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DP58_TAB_P58_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dp58TabP58[idx - 1].setWp58TabImpstBolBytes(buffer, position);
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
            else {
                dp58TabP58[idx - 1].initWp58TabImpstBolSpaces();
                position += Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;
            }
        }
    }

    public void setDp58EleP58Max(short dp58EleP58Max) {
        this.dp58EleP58Max = dp58EleP58Max;
    }

    public short getDp58EleP58Max() {
        return this.dp58EleP58Max;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabIso(short ixTabIso) {
        this.ixTabIso = ixTabIso;
    }

    public short getIxTabIso() {
        return this.ixTabIso;
    }

    public void setIntRegister1(int intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public int getIntRegister1() {
        return this.intRegister1;
    }

    public Wp58TabImpstBol getDp58TabP58(int idx) {
        return dp58TabP58[idx - 1];
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagFineLetturaP58 getFlagFineLetturaP58() {
        return flagFineLetturaP58;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public ImpstBollo getImpstBollo() {
        return impstBollo;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WkDataFine getWkDataFine() {
        return wkDataFine;
    }

    public WkDataInizio getWkDataInizio() {
        return wkDataInizio;
    }

    public WkDataSeparata getWkDataSeparata() {
        return wkDataSeparata;
    }

    public WkVarMoviComun getWkVarMoviComun() {
        return wkVarMoviComun;
    }

    public WsMovimentoLvvs0037 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DP58_ELE_P58_MAX = 2;
        public static final int DP58_AREA_P58 = DP58_ELE_P58_MAX + Lvvs2730Data.DP58_TAB_P58_MAXOCCURS * Wp58TabImpstBol.Len.WP58_TAB_IMPST_BOL;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
