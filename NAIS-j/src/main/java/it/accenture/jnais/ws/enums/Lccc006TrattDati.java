package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC006-TRATT-DATI<br>
 * Variable: LCCC006-TRATT-DATI from copybook LCCC0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccc006TrattDati {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char AMMESSO_MOD = '1';
    public static final char AMMESSO_NMOD = '2';
    public static final char NON_AMMESSO = '3';
    public static final char OBBLIGATORIO = '4';

    //==== METHODS ====
    public void setFlagModCalcPreP(char flagModCalcPreP) {
        this.value = flagModCalcPreP;
    }

    public void setIspc0140FlagModCalcPrePFormatted(String ispc0140FlagModCalcPreP) {
        setFlagModCalcPreP(Functions.charAt(ispc0140FlagModCalcPreP, Types.CHAR_SIZE));
    }

    public char getFlagModCalcPreP() {
        return this.value;
    }

    public boolean isUno() {
        return value == AMMESSO_MOD;
    }

    public void setStd() {
        value = AMMESSO_MOD;
    }

    public boolean isMcrfnct() {
        return value == AMMESSO_NMOD;
    }

    public void setMcrfnct() {
        value = AMMESSO_NMOD;
    }

    public boolean isTre() {
        return value == NON_AMMESSO;
    }

    public void setTpmv() {
        value = NON_AMMESSO;
    }

    public boolean isQuattro() {
        return value == OBBLIGATORIO;
    }

    public void setMcrfnctTpmv() {
        value = OBBLIGATORIO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_MOD_CALC_PRE_P = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
