package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-TAX-SEP-EFFLQ<br>
 * Variable: DFL-IMPB-TAX-SEP-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbTaxSepEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbTaxSepEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_TAX_SEP_EFFLQ;
    }

    public void setDflImpbTaxSepEfflq(AfDecimal dflImpbTaxSepEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPB_TAX_SEP_EFFLQ, dflImpbTaxSepEfflq.copy());
    }

    public void setDflImpbTaxSepEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_TAX_SEP_EFFLQ, Pos.DFL_IMPB_TAX_SEP_EFFLQ);
    }

    /**Original name: DFL-IMPB-TAX-SEP-EFFLQ<br>*/
    public AfDecimal getDflImpbTaxSepEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPB_TAX_SEP_EFFLQ, Len.Int.DFL_IMPB_TAX_SEP_EFFLQ, Len.Fract.DFL_IMPB_TAX_SEP_EFFLQ);
    }

    public byte[] getDflImpbTaxSepEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_TAX_SEP_EFFLQ, Pos.DFL_IMPB_TAX_SEP_EFFLQ);
        return buffer;
    }

    public void setDflImpbTaxSepEfflqNull(String dflImpbTaxSepEfflqNull) {
        writeString(Pos.DFL_IMPB_TAX_SEP_EFFLQ_NULL, dflImpbTaxSepEfflqNull, Len.DFL_IMPB_TAX_SEP_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPB-TAX-SEP-EFFLQ-NULL<br>*/
    public String getDflImpbTaxSepEfflqNull() {
        return readString(Pos.DFL_IMPB_TAX_SEP_EFFLQ_NULL, Len.DFL_IMPB_TAX_SEP_EFFLQ_NULL);
    }

    public String getDflImpbTaxSepEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpbTaxSepEfflqNull(), Len.DFL_IMPB_TAX_SEP_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TAX_SEP_EFFLQ = 1;
        public static final int DFL_IMPB_TAX_SEP_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TAX_SEP_EFFLQ = 8;
        public static final int DFL_IMPB_TAX_SEP_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TAX_SEP_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TAX_SEP_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
