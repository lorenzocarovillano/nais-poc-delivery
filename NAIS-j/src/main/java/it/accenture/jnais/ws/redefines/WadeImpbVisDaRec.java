package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-IMPB-VIS-DA-REC<br>
 * Variable: WADE-IMPB-VIS-DA-REC from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeImpbVisDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeImpbVisDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_IMPB_VIS_DA_REC;
    }

    public void setWadeImpbVisDaRec(AfDecimal wadeImpbVisDaRec) {
        writeDecimalAsPacked(Pos.WADE_IMPB_VIS_DA_REC, wadeImpbVisDaRec.copy());
    }

    public void setWadeImpbVisDaRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_IMPB_VIS_DA_REC, Pos.WADE_IMPB_VIS_DA_REC);
    }

    /**Original name: WADE-IMPB-VIS-DA-REC<br>*/
    public AfDecimal getWadeImpbVisDaRec() {
        return readPackedAsDecimal(Pos.WADE_IMPB_VIS_DA_REC, Len.Int.WADE_IMPB_VIS_DA_REC, Len.Fract.WADE_IMPB_VIS_DA_REC);
    }

    public byte[] getWadeImpbVisDaRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_IMPB_VIS_DA_REC, Pos.WADE_IMPB_VIS_DA_REC);
        return buffer;
    }

    public void initWadeImpbVisDaRecSpaces() {
        fill(Pos.WADE_IMPB_VIS_DA_REC, Len.WADE_IMPB_VIS_DA_REC, Types.SPACE_CHAR);
    }

    public void setWadeImpbVisDaRecNull(String wadeImpbVisDaRecNull) {
        writeString(Pos.WADE_IMPB_VIS_DA_REC_NULL, wadeImpbVisDaRecNull, Len.WADE_IMPB_VIS_DA_REC_NULL);
    }

    /**Original name: WADE-IMPB-VIS-DA-REC-NULL<br>*/
    public String getWadeImpbVisDaRecNull() {
        return readString(Pos.WADE_IMPB_VIS_DA_REC_NULL, Len.WADE_IMPB_VIS_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_IMPB_VIS_DA_REC = 1;
        public static final int WADE_IMPB_VIS_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_IMPB_VIS_DA_REC = 8;
        public static final int WADE_IMPB_VIS_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_IMPB_VIS_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_IMPB_VIS_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
