package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-IMP-ADER<br>
 * Variable: WDTC-IMP-ADER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcImpAder extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcImpAder() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_IMP_ADER;
    }

    public void setWdtcImpAder(AfDecimal wdtcImpAder) {
        writeDecimalAsPacked(Pos.WDTC_IMP_ADER, wdtcImpAder.copy());
    }

    public void setWdtcImpAderFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_IMP_ADER, Pos.WDTC_IMP_ADER);
    }

    /**Original name: WDTC-IMP-ADER<br>*/
    public AfDecimal getWdtcImpAder() {
        return readPackedAsDecimal(Pos.WDTC_IMP_ADER, Len.Int.WDTC_IMP_ADER, Len.Fract.WDTC_IMP_ADER);
    }

    public byte[] getWdtcImpAderAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_IMP_ADER, Pos.WDTC_IMP_ADER);
        return buffer;
    }

    public void initWdtcImpAderSpaces() {
        fill(Pos.WDTC_IMP_ADER, Len.WDTC_IMP_ADER, Types.SPACE_CHAR);
    }

    public void setWdtcImpAderNull(String wdtcImpAderNull) {
        writeString(Pos.WDTC_IMP_ADER_NULL, wdtcImpAderNull, Len.WDTC_IMP_ADER_NULL);
    }

    /**Original name: WDTC-IMP-ADER-NULL<br>*/
    public String getWdtcImpAderNull() {
        return readString(Pos.WDTC_IMP_ADER_NULL, Len.WDTC_IMP_ADER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_ADER = 1;
        public static final int WDTC_IMP_ADER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_IMP_ADER = 8;
        public static final int WDTC_IMP_ADER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_ADER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_IMP_ADER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
