package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-MINUS-VALENZA<br>
 * Variable: WVAS-MINUS-VALENZA from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasMinusValenza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasMinusValenza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_MINUS_VALENZA;
    }

    public void setWvasMinusValenza(AfDecimal wvasMinusValenza) {
        writeDecimalAsPacked(Pos.WVAS_MINUS_VALENZA, wvasMinusValenza.copy());
    }

    /**Original name: WVAS-MINUS-VALENZA<br>*/
    public AfDecimal getWvasMinusValenza() {
        return readPackedAsDecimal(Pos.WVAS_MINUS_VALENZA, Len.Int.WVAS_MINUS_VALENZA, Len.Fract.WVAS_MINUS_VALENZA);
    }

    public void setWvasMinusValenzaNull(String wvasMinusValenzaNull) {
        writeString(Pos.WVAS_MINUS_VALENZA_NULL, wvasMinusValenzaNull, Len.WVAS_MINUS_VALENZA_NULL);
    }

    /**Original name: WVAS-MINUS-VALENZA-NULL<br>*/
    public String getWvasMinusValenzaNull() {
        return readString(Pos.WVAS_MINUS_VALENZA_NULL, Len.WVAS_MINUS_VALENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_MINUS_VALENZA = 1;
        public static final int WVAS_MINUS_VALENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_MINUS_VALENZA = 8;
        public static final int WVAS_MINUS_VALENZA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_MINUS_VALENZA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_MINUS_VALENZA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
