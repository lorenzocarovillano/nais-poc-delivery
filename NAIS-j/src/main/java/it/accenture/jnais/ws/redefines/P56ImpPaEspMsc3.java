package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-IMP-PA-ESP-MSC-3<br>
 * Variable: P56-IMP-PA-ESP-MSC-3 from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56ImpPaEspMsc3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56ImpPaEspMsc3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_IMP_PA_ESP_MSC3;
    }

    public void setP56ImpPaEspMsc3(AfDecimal p56ImpPaEspMsc3) {
        writeDecimalAsPacked(Pos.P56_IMP_PA_ESP_MSC3, p56ImpPaEspMsc3.copy());
    }

    public void setP56ImpPaEspMsc3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC3, Pos.P56_IMP_PA_ESP_MSC3);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-3<br>*/
    public AfDecimal getP56ImpPaEspMsc3() {
        return readPackedAsDecimal(Pos.P56_IMP_PA_ESP_MSC3, Len.Int.P56_IMP_PA_ESP_MSC3, Len.Fract.P56_IMP_PA_ESP_MSC3);
    }

    public byte[] getP56ImpPaEspMsc3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_IMP_PA_ESP_MSC3, Pos.P56_IMP_PA_ESP_MSC3);
        return buffer;
    }

    public void setP56ImpPaEspMsc3Null(String p56ImpPaEspMsc3Null) {
        writeString(Pos.P56_IMP_PA_ESP_MSC3_NULL, p56ImpPaEspMsc3Null, Len.P56_IMP_PA_ESP_MSC3_NULL);
    }

    /**Original name: P56-IMP-PA-ESP-MSC-3-NULL<br>*/
    public String getP56ImpPaEspMsc3Null() {
        return readString(Pos.P56_IMP_PA_ESP_MSC3_NULL, Len.P56_IMP_PA_ESP_MSC3_NULL);
    }

    public String getP56ImpPaEspMsc3NullFormatted() {
        return Functions.padBlanks(getP56ImpPaEspMsc3Null(), Len.P56_IMP_PA_ESP_MSC3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC3 = 1;
        public static final int P56_IMP_PA_ESP_MSC3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_IMP_PA_ESP_MSC3 = 8;
        public static final int P56_IMP_PA_ESP_MSC3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_IMP_PA_ESP_MSC3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
