package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-RIS-MAT-POST-TAX<br>
 * Variable: ISO-RIS-MAT-POST-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoRisMatPostTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoRisMatPostTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_RIS_MAT_POST_TAX;
    }

    public void setIsoRisMatPostTax(AfDecimal isoRisMatPostTax) {
        writeDecimalAsPacked(Pos.ISO_RIS_MAT_POST_TAX, isoRisMatPostTax.copy());
    }

    public void setIsoRisMatPostTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_RIS_MAT_POST_TAX, Pos.ISO_RIS_MAT_POST_TAX);
    }

    /**Original name: ISO-RIS-MAT-POST-TAX<br>*/
    public AfDecimal getIsoRisMatPostTax() {
        return readPackedAsDecimal(Pos.ISO_RIS_MAT_POST_TAX, Len.Int.ISO_RIS_MAT_POST_TAX, Len.Fract.ISO_RIS_MAT_POST_TAX);
    }

    public byte[] getIsoRisMatPostTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_RIS_MAT_POST_TAX, Pos.ISO_RIS_MAT_POST_TAX);
        return buffer;
    }

    public void setIsoRisMatPostTaxNull(String isoRisMatPostTaxNull) {
        writeString(Pos.ISO_RIS_MAT_POST_TAX_NULL, isoRisMatPostTaxNull, Len.ISO_RIS_MAT_POST_TAX_NULL);
    }

    /**Original name: ISO-RIS-MAT-POST-TAX-NULL<br>*/
    public String getIsoRisMatPostTaxNull() {
        return readString(Pos.ISO_RIS_MAT_POST_TAX_NULL, Len.ISO_RIS_MAT_POST_TAX_NULL);
    }

    public String getIsoRisMatPostTaxNullFormatted() {
        return Functions.padBlanks(getIsoRisMatPostTaxNull(), Len.ISO_RIS_MAT_POST_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_RIS_MAT_POST_TAX = 1;
        public static final int ISO_RIS_MAT_POST_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_RIS_MAT_POST_TAX = 8;
        public static final int ISO_RIS_MAT_POST_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_RIS_MAT_POST_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ISO_RIS_MAT_POST_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
