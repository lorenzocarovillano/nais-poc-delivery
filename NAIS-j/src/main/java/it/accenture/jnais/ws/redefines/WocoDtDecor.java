package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WOCO-DT-DECOR<br>
 * Variable: WOCO-DT-DECOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoDtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoDtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_DT_DECOR;
    }

    public void setWocoDtDecor(int wocoDtDecor) {
        writeIntAsPacked(Pos.WOCO_DT_DECOR, wocoDtDecor, Len.Int.WOCO_DT_DECOR);
    }

    public void setWocoDtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_DT_DECOR, Pos.WOCO_DT_DECOR);
    }

    /**Original name: WOCO-DT-DECOR<br>*/
    public int getWocoDtDecor() {
        return readPackedAsInt(Pos.WOCO_DT_DECOR, Len.Int.WOCO_DT_DECOR);
    }

    public byte[] getWocoDtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_DT_DECOR, Pos.WOCO_DT_DECOR);
        return buffer;
    }

    public void initWocoDtDecorSpaces() {
        fill(Pos.WOCO_DT_DECOR, Len.WOCO_DT_DECOR, Types.SPACE_CHAR);
    }

    public void setWocoDtDecorNull(String wocoDtDecorNull) {
        writeString(Pos.WOCO_DT_DECOR_NULL, wocoDtDecorNull, Len.WOCO_DT_DECOR_NULL);
    }

    /**Original name: WOCO-DT-DECOR-NULL<br>*/
    public String getWocoDtDecorNull() {
        return readString(Pos.WOCO_DT_DECOR_NULL, Len.WOCO_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_DT_DECOR = 1;
        public static final int WOCO_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_DT_DECOR = 5;
        public static final int WOCO_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
