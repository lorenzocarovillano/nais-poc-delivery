package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-ALQ-PROV-RICOR<br>
 * Variable: WTGA-ALQ-PROV-RICOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaAlqProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaAlqProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_ALQ_PROV_RICOR;
    }

    public void setWtgaAlqProvRicor(AfDecimal wtgaAlqProvRicor) {
        writeDecimalAsPacked(Pos.WTGA_ALQ_PROV_RICOR, wtgaAlqProvRicor.copy());
    }

    public void setWtgaAlqProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_ALQ_PROV_RICOR, Pos.WTGA_ALQ_PROV_RICOR);
    }

    /**Original name: WTGA-ALQ-PROV-RICOR<br>*/
    public AfDecimal getWtgaAlqProvRicor() {
        return readPackedAsDecimal(Pos.WTGA_ALQ_PROV_RICOR, Len.Int.WTGA_ALQ_PROV_RICOR, Len.Fract.WTGA_ALQ_PROV_RICOR);
    }

    public byte[] getWtgaAlqProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_ALQ_PROV_RICOR, Pos.WTGA_ALQ_PROV_RICOR);
        return buffer;
    }

    public void initWtgaAlqProvRicorSpaces() {
        fill(Pos.WTGA_ALQ_PROV_RICOR, Len.WTGA_ALQ_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWtgaAlqProvRicorNull(String wtgaAlqProvRicorNull) {
        writeString(Pos.WTGA_ALQ_PROV_RICOR_NULL, wtgaAlqProvRicorNull, Len.WTGA_ALQ_PROV_RICOR_NULL);
    }

    /**Original name: WTGA-ALQ-PROV-RICOR-NULL<br>*/
    public String getWtgaAlqProvRicorNull() {
        return readString(Pos.WTGA_ALQ_PROV_RICOR_NULL, Len.WTGA_ALQ_PROV_RICOR_NULL);
    }

    public String getWtgaAlqProvRicorNullFormatted() {
        return Functions.padBlanks(getWtgaAlqProvRicorNull(), Len.WTGA_ALQ_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_PROV_RICOR = 1;
        public static final int WTGA_ALQ_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_ALQ_PROV_RICOR = 4;
        public static final int WTGA_ALQ_PROV_RICOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_ALQ_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
