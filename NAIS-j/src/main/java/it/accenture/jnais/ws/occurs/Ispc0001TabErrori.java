package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0001GravitaErr;

/**Original name: ISPC0001-TAB-ERRORI<br>
 * Variables: ISPC0001-TAB-ERRORI from copybook IEAV9903<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0001TabErrori {

    //==== PROPERTIES ====
    //Original name: ISPC0001-COD-ERR
    private String codErr = DefaultValues.stringVal(Len.COD_ERR);
    //Original name: ISPC0001-DESCRIZIONE-ERR
    private String descrizioneErr = DefaultValues.stringVal(Len.DESCRIZIONE_ERR);
    //Original name: ISPC0001-GRAVITA-ERR
    private Ispc0001GravitaErr gravitaErr = new Ispc0001GravitaErr();
    //Original name: ISPC0001-LIVELLO-DEROGA
    private String livelloDeroga = DefaultValues.stringVal(Len.LIVELLO_DEROGA);

    //==== METHODS ====
    public void setIspc0001TabErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        codErr = MarshalByte.readString(buffer, position, Len.COD_ERR);
        position += Len.COD_ERR;
        descrizioneErr = MarshalByte.readString(buffer, position, Len.DESCRIZIONE_ERR);
        position += Len.DESCRIZIONE_ERR;
        gravitaErr.value = MarshalByte.readFixedString(buffer, position, Ispc0001GravitaErr.Len.GRAVITA_ERR);
        position += Ispc0001GravitaErr.Len.GRAVITA_ERR;
        livelloDeroga = MarshalByte.readFixedString(buffer, position, Len.LIVELLO_DEROGA);
    }

    public void initIspc0001TabErroriSpaces() {
        codErr = "";
        descrizioneErr = "";
        gravitaErr.setIspc0001GravitaErr(Types.INVALID_SHORT_VAL);
        livelloDeroga = "";
    }

    public void setIspc0001CodErr(String ispc0001CodErr) {
        this.codErr = Functions.subString(ispc0001CodErr, Len.COD_ERR);
    }

    public String getIspc0001CodErr() {
        return this.codErr;
    }

    public String getIspc0001CodErrFormatted() {
        return Functions.padBlanks(getIspc0001CodErr(), Len.COD_ERR);
    }

    public void setIspc0001DescrizioneErr(String ispc0001DescrizioneErr) {
        this.descrizioneErr = Functions.subString(ispc0001DescrizioneErr, Len.DESCRIZIONE_ERR);
    }

    public String getIspc0001DescrizioneErr() {
        return this.descrizioneErr;
    }

    public String getIspc0001DescrizioneErrFormatted() {
        return Functions.padBlanks(getIspc0001DescrizioneErr(), Len.DESCRIZIONE_ERR);
    }

    public short getIspc0001LivelloDeroga() {
        return NumericDisplay.asShort(this.livelloDeroga);
    }

    public Ispc0001GravitaErr getGravitaErr() {
        return gravitaErr;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_ERR = 12;
        public static final int DESCRIZIONE_ERR = 50;
        public static final int LIVELLO_DEROGA = 2;
        public static final int ISPC0001_TAB_ERRORI = COD_ERR + DESCRIZIONE_ERR + Ispc0001GravitaErr.Len.GRAVITA_ERR + LIVELLO_DEROGA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
