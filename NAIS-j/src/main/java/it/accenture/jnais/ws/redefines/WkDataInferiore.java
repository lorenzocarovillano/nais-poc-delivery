package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WK-DATA-INFERIORE<br>
 * Variable: WK-DATA-INFERIORE from program LCCS1750<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDataInferiore extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDataInferiore() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DATA_INFERIORE;
    }

    public void setWkDataInferioreBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WK_DATA_INFERIORE, Pos.WK_DATA_INFERIORE);
    }

    public byte[] getWkDataInferioreBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WK_DATA_INFERIORE, Pos.WK_DATA_INFERIORE);
        return buffer;
    }

    /**Original name: WK-GG-INF<br>*/
    public short getGgInf() {
        return readNumDispUnsignedShort(Pos.GG_INF, Len.GG_INF);
    }

    public void setWkDataInferioreNFormatted(String wkDataInferioreN) {
        writeString(Pos.WK_DATA_INFERIORE_N, Trunc.toUnsignedNumeric(wkDataInferioreN, Len.WK_DATA_INFERIORE_N), Len.WK_DATA_INFERIORE_N);
    }

    /**Original name: WK-DATA-INFERIORE-N<br>*/
    public int getWkDataInferioreN() {
        return readNumDispUnsignedInt(Pos.WK_DATA_INFERIORE_N, Len.WK_DATA_INFERIORE_N);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DATA_INFERIORE = 1;
        public static final int AAAA_INF = WK_DATA_INFERIORE;
        public static final int MM_INF = AAAA_INF + Len.AAAA_INF;
        public static final int GG_INF = MM_INF + Len.MM_INF;
        public static final int WK_DATA_INFERIORE_N = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int AAAA_INF = 4;
        public static final int MM_INF = 2;
        public static final int GG_INF = 2;
        public static final int WK_DATA_INFERIORE = AAAA_INF + MM_INF + GG_INF;
        public static final int WK_DATA_INFERIORE_N = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
