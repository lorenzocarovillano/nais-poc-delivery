package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-MOVI<br>
 * Variable: WPAG-IMP-MOVI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_MOVI;
    }

    public void setWpagImpMovi(AfDecimal wpagImpMovi) {
        writeDecimalAsPacked(Pos.WPAG_IMP_MOVI, wpagImpMovi.copy());
    }

    public void setWpagImpMoviFormatted(String wpagImpMovi) {
        setWpagImpMovi(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_MOVI + Len.Fract.WPAG_IMP_MOVI, Len.Fract.WPAG_IMP_MOVI, wpagImpMovi));
    }

    public void setWpagImpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_MOVI, Pos.WPAG_IMP_MOVI);
    }

    /**Original name: WPAG-IMP-MOVI<br>*/
    public AfDecimal getWpagImpMovi() {
        return readPackedAsDecimal(Pos.WPAG_IMP_MOVI, Len.Int.WPAG_IMP_MOVI, Len.Fract.WPAG_IMP_MOVI);
    }

    public byte[] getWpagImpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_MOVI, Pos.WPAG_IMP_MOVI);
        return buffer;
    }

    public void initWpagImpMoviSpaces() {
        fill(Pos.WPAG_IMP_MOVI, Len.WPAG_IMP_MOVI, Types.SPACE_CHAR);
    }

    public void setWpagImpMoviNull(String wpagImpMoviNull) {
        writeString(Pos.WPAG_IMP_MOVI_NULL, wpagImpMoviNull, Len.WPAG_IMP_MOVI_NULL);
    }

    /**Original name: WPAG-IMP-MOVI-NULL<br>*/
    public String getWpagImpMoviNull() {
        return readString(Pos.WPAG_IMP_MOVI_NULL, Len.WPAG_IMP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_MOVI = 1;
        public static final int WPAG_IMP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_MOVI = 8;
        public static final int WPAG_IMP_MOVI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_MOVI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_MOVI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
