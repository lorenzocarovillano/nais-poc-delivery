package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNDE-END2006-CALC<br>
 * Variable: DFL-CNDE-END2006-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCndeEnd2006Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCndeEnd2006Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNDE_END2006_CALC;
    }

    public void setDflCndeEnd2006Calc(AfDecimal dflCndeEnd2006Calc) {
        writeDecimalAsPacked(Pos.DFL_CNDE_END2006_CALC, dflCndeEnd2006Calc.copy());
    }

    public void setDflCndeEnd2006CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNDE_END2006_CALC, Pos.DFL_CNDE_END2006_CALC);
    }

    /**Original name: DFL-CNDE-END2006-CALC<br>*/
    public AfDecimal getDflCndeEnd2006Calc() {
        return readPackedAsDecimal(Pos.DFL_CNDE_END2006_CALC, Len.Int.DFL_CNDE_END2006_CALC, Len.Fract.DFL_CNDE_END2006_CALC);
    }

    public byte[] getDflCndeEnd2006CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNDE_END2006_CALC, Pos.DFL_CNDE_END2006_CALC);
        return buffer;
    }

    public void setDflCndeEnd2006CalcNull(String dflCndeEnd2006CalcNull) {
        writeString(Pos.DFL_CNDE_END2006_CALC_NULL, dflCndeEnd2006CalcNull, Len.DFL_CNDE_END2006_CALC_NULL);
    }

    /**Original name: DFL-CNDE-END2006-CALC-NULL<br>*/
    public String getDflCndeEnd2006CalcNull() {
        return readString(Pos.DFL_CNDE_END2006_CALC_NULL, Len.DFL_CNDE_END2006_CALC_NULL);
    }

    public String getDflCndeEnd2006CalcNullFormatted() {
        return Functions.padBlanks(getDflCndeEnd2006CalcNull(), Len.DFL_CNDE_END2006_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_END2006_CALC = 1;
        public static final int DFL_CNDE_END2006_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNDE_END2006_CALC = 8;
        public static final int DFL_CNDE_END2006_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_END2006_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNDE_END2006_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
