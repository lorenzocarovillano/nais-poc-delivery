package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DT-ULT-PRE-PAG<br>
 * Variable: WB03-DT-ULT-PRE-PAG from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DtUltPrePag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DtUltPrePag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DT_ULT_PRE_PAG;
    }

    public void setWb03DtUltPrePag(int wb03DtUltPrePag) {
        writeIntAsPacked(Pos.WB03_DT_ULT_PRE_PAG, wb03DtUltPrePag, Len.Int.WB03_DT_ULT_PRE_PAG);
    }

    public void setWb03DtUltPrePagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DT_ULT_PRE_PAG, Pos.WB03_DT_ULT_PRE_PAG);
    }

    /**Original name: WB03-DT-ULT-PRE-PAG<br>*/
    public int getWb03DtUltPrePag() {
        return readPackedAsInt(Pos.WB03_DT_ULT_PRE_PAG, Len.Int.WB03_DT_ULT_PRE_PAG);
    }

    public byte[] getWb03DtUltPrePagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DT_ULT_PRE_PAG, Pos.WB03_DT_ULT_PRE_PAG);
        return buffer;
    }

    public void setWb03DtUltPrePagNull(String wb03DtUltPrePagNull) {
        writeString(Pos.WB03_DT_ULT_PRE_PAG_NULL, wb03DtUltPrePagNull, Len.WB03_DT_ULT_PRE_PAG_NULL);
    }

    /**Original name: WB03-DT-ULT-PRE-PAG-NULL<br>*/
    public String getWb03DtUltPrePagNull() {
        return readString(Pos.WB03_DT_ULT_PRE_PAG_NULL, Len.WB03_DT_ULT_PRE_PAG_NULL);
    }

    public String getWb03DtUltPrePagNullFormatted() {
        return Functions.padBlanks(getWb03DtUltPrePagNull(), Len.WB03_DT_ULT_PRE_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DT_ULT_PRE_PAG = 1;
        public static final int WB03_DT_ULT_PRE_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DT_ULT_PRE_PAG = 5;
        public static final int WB03_DT_ULT_PRE_PAG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DT_ULT_PRE_PAG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
