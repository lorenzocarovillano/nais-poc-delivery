package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WB03-MET-RISC-SPCL<br>
 * Variable: WB03-MET-RISC-SPCL from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03MetRiscSpcl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03MetRiscSpcl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_MET_RISC_SPCL;
    }

    public void setWb03MetRiscSpclFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_MET_RISC_SPCL, Pos.WB03_MET_RISC_SPCL);
    }

    /**Original name: WB03-MET-RISC-SPCL<br>*/
    public short getWb03MetRiscSpcl() {
        return readPackedAsShort(Pos.WB03_MET_RISC_SPCL, Len.Int.WB03_MET_RISC_SPCL);
    }

    public byte[] getWb03MetRiscSpclAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_MET_RISC_SPCL, Pos.WB03_MET_RISC_SPCL);
        return buffer;
    }

    public void setWb03MetRiscSpclNull(char wb03MetRiscSpclNull) {
        writeChar(Pos.WB03_MET_RISC_SPCL_NULL, wb03MetRiscSpclNull);
    }

    /**Original name: WB03-MET-RISC-SPCL-NULL<br>*/
    public char getWb03MetRiscSpclNull() {
        return readChar(Pos.WB03_MET_RISC_SPCL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_MET_RISC_SPCL = 1;
        public static final int WB03_MET_RISC_SPCL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_MET_RISC_SPCL = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_MET_RISC_SPCL = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
