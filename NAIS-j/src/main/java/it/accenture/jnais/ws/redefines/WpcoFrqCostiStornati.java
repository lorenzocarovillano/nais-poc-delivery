package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-FRQ-COSTI-STORNATI<br>
 * Variable: WPCO-FRQ-COSTI-STORNATI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoFrqCostiStornati extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoFrqCostiStornati() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_FRQ_COSTI_STORNATI;
    }

    public void setWpcoFrqCostiStornati(int wpcoFrqCostiStornati) {
        writeIntAsPacked(Pos.WPCO_FRQ_COSTI_STORNATI, wpcoFrqCostiStornati, Len.Int.WPCO_FRQ_COSTI_STORNATI);
    }

    public void setDpcoFrqCostiStornatiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_FRQ_COSTI_STORNATI, Pos.WPCO_FRQ_COSTI_STORNATI);
    }

    /**Original name: WPCO-FRQ-COSTI-STORNATI<br>*/
    public int getWpcoFrqCostiStornati() {
        return readPackedAsInt(Pos.WPCO_FRQ_COSTI_STORNATI, Len.Int.WPCO_FRQ_COSTI_STORNATI);
    }

    public byte[] getWpcoFrqCostiStornatiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_FRQ_COSTI_STORNATI, Pos.WPCO_FRQ_COSTI_STORNATI);
        return buffer;
    }

    public void setWpcoFrqCostiStornatiNull(String wpcoFrqCostiStornatiNull) {
        writeString(Pos.WPCO_FRQ_COSTI_STORNATI_NULL, wpcoFrqCostiStornatiNull, Len.WPCO_FRQ_COSTI_STORNATI_NULL);
    }

    /**Original name: WPCO-FRQ-COSTI-STORNATI-NULL<br>*/
    public String getWpcoFrqCostiStornatiNull() {
        return readString(Pos.WPCO_FRQ_COSTI_STORNATI_NULL, Len.WPCO_FRQ_COSTI_STORNATI_NULL);
    }

    public String getWpcoFrqCostiStornatiNullFormatted() {
        return Functions.padBlanks(getWpcoFrqCostiStornatiNull(), Len.WPCO_FRQ_COSTI_STORNATI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_FRQ_COSTI_STORNATI = 1;
        public static final int WPCO_FRQ_COSTI_STORNATI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_FRQ_COSTI_STORNATI = 3;
        public static final int WPCO_FRQ_COSTI_STORNATI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_FRQ_COSTI_STORNATI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
