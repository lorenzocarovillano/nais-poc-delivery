package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-OGG<br>
 * Variable: WS-TP-OGG from copybook LCCVXOG0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpOggLccs0024 {

    //==== PROPERTIES ====
    private String value = "";
    public static final String POLIZZA = "PO";
    public static final String ADESIONE = "AD";
    public static final String GARANZIA = "GA";
    public static final String TRANCHE = "TG";
    public static final String RIPARTO_FONDI = "RF";
    public static final String LIQUIDAZIONE = "LI";
    public static final String PRESTITO = "PR";
    public static final String TITOLO_CONTABILE = "TC";
    public static final String GARANZIA_LIQUID = "GL";
    public static final String DEROGA = "DE";
    public static final String MOVIMENTO = "MO";
    public static final String PREVENTIVO = "PV";
    public static final String VINCPEG = "VP";
    public static final String PERCIP_LIQUID = "PL";

    //==== METHODS ====
    public void setWsTpOgg(String wsTpOgg) {
        this.value = Functions.subString(wsTpOgg, Len.WS_TP_OGG);
    }

    public String getWsTpOgg() {
        return this.value;
    }

    public boolean isPolizza() {
        return value.equals(POLIZZA);
    }

    public void setPolizza() {
        value = POLIZZA;
    }

    public boolean isAdesione() {
        return value.equals(ADESIONE);
    }

    public void setAdesione() {
        value = ADESIONE;
    }

    public void setGaranzia() {
        value = GARANZIA;
    }

    public boolean isTranche() {
        return value.equals(TRANCHE);
    }

    public void setTranche() {
        value = TRANCHE;
    }

    public boolean isPreventivo() {
        return value.equals(PREVENTIVO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_OGG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
