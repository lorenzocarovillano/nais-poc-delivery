package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-BNSFDT-IN<br>
 * Variable: PCO-DT-ULTC-BNSFDT-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcBnsfdtIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcBnsfdtIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_BNSFDT_IN;
    }

    public void setPcoDtUltcBnsfdtIn(int pcoDtUltcBnsfdtIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_BNSFDT_IN, pcoDtUltcBnsfdtIn, Len.Int.PCO_DT_ULTC_BNSFDT_IN);
    }

    public void setPcoDtUltcBnsfdtInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_BNSFDT_IN, Pos.PCO_DT_ULTC_BNSFDT_IN);
    }

    /**Original name: PCO-DT-ULTC-BNSFDT-IN<br>*/
    public int getPcoDtUltcBnsfdtIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_BNSFDT_IN, Len.Int.PCO_DT_ULTC_BNSFDT_IN);
    }

    public byte[] getPcoDtUltcBnsfdtInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_BNSFDT_IN, Pos.PCO_DT_ULTC_BNSFDT_IN);
        return buffer;
    }

    public void initPcoDtUltcBnsfdtInHighValues() {
        fill(Pos.PCO_DT_ULTC_BNSFDT_IN, Len.PCO_DT_ULTC_BNSFDT_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcBnsfdtInNull(String pcoDtUltcBnsfdtInNull) {
        writeString(Pos.PCO_DT_ULTC_BNSFDT_IN_NULL, pcoDtUltcBnsfdtInNull, Len.PCO_DT_ULTC_BNSFDT_IN_NULL);
    }

    /**Original name: PCO-DT-ULTC-BNSFDT-IN-NULL<br>*/
    public String getPcoDtUltcBnsfdtInNull() {
        return readString(Pos.PCO_DT_ULTC_BNSFDT_IN_NULL, Len.PCO_DT_ULTC_BNSFDT_IN_NULL);
    }

    public String getPcoDtUltcBnsfdtInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcBnsfdtInNull(), Len.PCO_DT_ULTC_BNSFDT_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSFDT_IN = 1;
        public static final int PCO_DT_ULTC_BNSFDT_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_BNSFDT_IN = 5;
        public static final int PCO_DT_ULTC_BNSFDT_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_BNSFDT_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
