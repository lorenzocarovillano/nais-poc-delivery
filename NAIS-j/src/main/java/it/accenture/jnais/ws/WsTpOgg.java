package it.accenture.jnais.ws;

/**Original name: WS-TP-OGG<br>
 * Variable: WS-TP-OGG from program LCCS0005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTpOgg {

    //==== PROPERTIES ====
    //Original name: WS-POLIZZA
    private String polizza = "PO";
    //Original name: WS-ADESIONE
    private String adesione = "AD";
    //Original name: WS-GARANZIA
    private String garanzia = "GA";
    //Original name: WS-TRANCHE
    private String tranche = "TG";

    //==== METHODS ====
    public String getPolizza() {
        return this.polizza;
    }

    public String getAdesione() {
        return this.adesione;
    }

    public String getGaranzia() {
        return this.garanzia;
    }

    public String getTranche() {
        return this.tranche;
    }
}
