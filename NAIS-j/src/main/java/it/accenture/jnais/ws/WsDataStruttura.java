package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-DATA-STRUTTURA<br>
 * Variable: WS-DATA-STRUTTURA from program LDBM0170<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsDataStruttura {

    //==== PROPERTIES ====
    //Original name: WS-DATA-STRUT-AA
    private String aa = DefaultValues.stringVal(Len.AA);
    //Original name: WS-DATA-STRUT-MM
    private String mm = DefaultValues.stringVal(Len.MM);
    //Original name: WS-DATA-STRUT-GG
    private String gg = DefaultValues.stringVal(Len.GG);

    //==== METHODS ====
    public void setWsDataStrutturaFormatted(String data) {
        byte[] buffer = new byte[Len.WS_DATA_STRUTTURA];
        MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_STRUTTURA);
        setWsDataStrutturaBytes(buffer, 1);
    }

    public byte[] getWsDataStrutturaBytes() {
        byte[] buffer = new byte[Len.WS_DATA_STRUTTURA];
        return getWsDataStrutturaBytes(buffer, 1);
    }

    public void setWsDataStrutturaBytes(byte[] buffer, int offset) {
        int position = offset;
        aa = MarshalByte.readFixedString(buffer, position, Len.AA);
        position += Len.AA;
        mm = MarshalByte.readFixedString(buffer, position, Len.MM);
        position += Len.MM;
        gg = MarshalByte.readFixedString(buffer, position, Len.GG);
    }

    public byte[] getWsDataStrutturaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, aa, Len.AA);
        position += Len.AA;
        MarshalByte.writeString(buffer, position, mm, Len.MM);
        position += Len.MM;
        MarshalByte.writeString(buffer, position, gg, Len.GG);
        return buffer;
    }

    public void setMm(short mm) {
        this.mm = NumericDisplay.asString(mm, Len.MM);
    }

    public short getMm() {
        return NumericDisplay.asShort(this.mm);
    }

    public void setGg(short gg) {
        this.gg = NumericDisplay.asString(gg, Len.GG);
    }

    public short getGg() {
        return NumericDisplay.asShort(this.gg);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AA = 4;
        public static final int MM = 2;
        public static final int GG = 2;
        public static final int WS_DATA_STRUTTURA = AA + MM + GG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
