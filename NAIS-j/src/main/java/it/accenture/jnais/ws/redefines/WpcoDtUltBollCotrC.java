package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-COTR-C<br>
 * Variable: WPCO-DT-ULT-BOLL-COTR-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollCotrC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollCotrC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_COTR_C;
    }

    public void setWpcoDtUltBollCotrC(int wpcoDtUltBollCotrC) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_COTR_C, wpcoDtUltBollCotrC, Len.Int.WPCO_DT_ULT_BOLL_COTR_C);
    }

    public void setDpcoDtUltBollCotrCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_COTR_C, Pos.WPCO_DT_ULT_BOLL_COTR_C);
    }

    /**Original name: WPCO-DT-ULT-BOLL-COTR-C<br>*/
    public int getWpcoDtUltBollCotrC() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_COTR_C, Len.Int.WPCO_DT_ULT_BOLL_COTR_C);
    }

    public byte[] getWpcoDtUltBollCotrCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_COTR_C, Pos.WPCO_DT_ULT_BOLL_COTR_C);
        return buffer;
    }

    public void setWpcoDtUltBollCotrCNull(String wpcoDtUltBollCotrCNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_COTR_C_NULL, wpcoDtUltBollCotrCNull, Len.WPCO_DT_ULT_BOLL_COTR_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-COTR-C-NULL<br>*/
    public String getWpcoDtUltBollCotrCNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_COTR_C_NULL, Len.WPCO_DT_ULT_BOLL_COTR_C_NULL);
    }

    public String getWpcoDtUltBollCotrCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollCotrCNull(), Len.WPCO_DT_ULT_BOLL_COTR_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_COTR_C = 1;
        public static final int WPCO_DT_ULT_BOLL_COTR_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_COTR_C = 5;
        public static final int WPCO_DT_ULT_BOLL_COTR_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_COTR_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
