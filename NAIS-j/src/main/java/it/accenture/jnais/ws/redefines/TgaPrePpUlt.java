package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PRE-PP-ULT<br>
 * Variable: TGA-PRE-PP-ULT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPrePpUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPrePpUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PRE_PP_ULT;
    }

    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        writeDecimalAsPacked(Pos.TGA_PRE_PP_ULT, tgaPrePpUlt.copy());
    }

    public void setTgaPrePpUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PRE_PP_ULT, Pos.TGA_PRE_PP_ULT);
    }

    /**Original name: TGA-PRE-PP-ULT<br>*/
    public AfDecimal getTgaPrePpUlt() {
        return readPackedAsDecimal(Pos.TGA_PRE_PP_ULT, Len.Int.TGA_PRE_PP_ULT, Len.Fract.TGA_PRE_PP_ULT);
    }

    public byte[] getTgaPrePpUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PRE_PP_ULT, Pos.TGA_PRE_PP_ULT);
        return buffer;
    }

    public void setTgaPrePpUltNull(String tgaPrePpUltNull) {
        writeString(Pos.TGA_PRE_PP_ULT_NULL, tgaPrePpUltNull, Len.TGA_PRE_PP_ULT_NULL);
    }

    /**Original name: TGA-PRE-PP-ULT-NULL<br>*/
    public String getTgaPrePpUltNull() {
        return readString(Pos.TGA_PRE_PP_ULT_NULL, Len.TGA_PRE_PP_ULT_NULL);
    }

    public String getTgaPrePpUltNullFormatted() {
        return Functions.padBlanks(getTgaPrePpUltNull(), Len.TGA_PRE_PP_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PRE_PP_ULT = 1;
        public static final int TGA_PRE_PP_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PRE_PP_ULT = 8;
        public static final int TGA_PRE_PP_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PRE_PP_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PRE_PP_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
