package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ALQ-TAX-SEP-DFZ<br>
 * Variable: WDFL-ALQ-TAX-SEP-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAlqTaxSepDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAlqTaxSepDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ALQ_TAX_SEP_DFZ;
    }

    public void setWdflAlqTaxSepDfz(AfDecimal wdflAlqTaxSepDfz) {
        writeDecimalAsPacked(Pos.WDFL_ALQ_TAX_SEP_DFZ, wdflAlqTaxSepDfz.copy());
    }

    public void setWdflAlqTaxSepDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ALQ_TAX_SEP_DFZ, Pos.WDFL_ALQ_TAX_SEP_DFZ);
    }

    /**Original name: WDFL-ALQ-TAX-SEP-DFZ<br>*/
    public AfDecimal getWdflAlqTaxSepDfz() {
        return readPackedAsDecimal(Pos.WDFL_ALQ_TAX_SEP_DFZ, Len.Int.WDFL_ALQ_TAX_SEP_DFZ, Len.Fract.WDFL_ALQ_TAX_SEP_DFZ);
    }

    public byte[] getWdflAlqTaxSepDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ALQ_TAX_SEP_DFZ, Pos.WDFL_ALQ_TAX_SEP_DFZ);
        return buffer;
    }

    public void setWdflAlqTaxSepDfzNull(String wdflAlqTaxSepDfzNull) {
        writeString(Pos.WDFL_ALQ_TAX_SEP_DFZ_NULL, wdflAlqTaxSepDfzNull, Len.WDFL_ALQ_TAX_SEP_DFZ_NULL);
    }

    /**Original name: WDFL-ALQ-TAX-SEP-DFZ-NULL<br>*/
    public String getWdflAlqTaxSepDfzNull() {
        return readString(Pos.WDFL_ALQ_TAX_SEP_DFZ_NULL, Len.WDFL_ALQ_TAX_SEP_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_TAX_SEP_DFZ = 1;
        public static final int WDFL_ALQ_TAX_SEP_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ALQ_TAX_SEP_DFZ = 4;
        public static final int WDFL_ALQ_TAX_SEP_DFZ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ALQ_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
