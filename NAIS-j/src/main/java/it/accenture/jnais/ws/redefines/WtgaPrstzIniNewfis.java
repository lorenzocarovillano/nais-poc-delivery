package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-INI-NEWFIS<br>
 * Variable: WTGA-PRSTZ-INI-NEWFIS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzIniNewfis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzIniNewfis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_INI_NEWFIS;
    }

    public void setWtgaPrstzIniNewfis(AfDecimal wtgaPrstzIniNewfis) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_INI_NEWFIS, wtgaPrstzIniNewfis.copy());
    }

    public void setWtgaPrstzIniNewfisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_INI_NEWFIS, Pos.WTGA_PRSTZ_INI_NEWFIS);
    }

    /**Original name: WTGA-PRSTZ-INI-NEWFIS<br>*/
    public AfDecimal getWtgaPrstzIniNewfis() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_INI_NEWFIS, Len.Int.WTGA_PRSTZ_INI_NEWFIS, Len.Fract.WTGA_PRSTZ_INI_NEWFIS);
    }

    public byte[] getWtgaPrstzIniNewfisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_INI_NEWFIS, Pos.WTGA_PRSTZ_INI_NEWFIS);
        return buffer;
    }

    public void initWtgaPrstzIniNewfisSpaces() {
        fill(Pos.WTGA_PRSTZ_INI_NEWFIS, Len.WTGA_PRSTZ_INI_NEWFIS, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzIniNewfisNull(String wtgaPrstzIniNewfisNull) {
        writeString(Pos.WTGA_PRSTZ_INI_NEWFIS_NULL, wtgaPrstzIniNewfisNull, Len.WTGA_PRSTZ_INI_NEWFIS_NULL);
    }

    /**Original name: WTGA-PRSTZ-INI-NEWFIS-NULL<br>*/
    public String getWtgaPrstzIniNewfisNull() {
        return readString(Pos.WTGA_PRSTZ_INI_NEWFIS_NULL, Len.WTGA_PRSTZ_INI_NEWFIS_NULL);
    }

    public String getWtgaPrstzIniNewfisNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzIniNewfisNull(), Len.WTGA_PRSTZ_INI_NEWFIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI_NEWFIS = 1;
        public static final int WTGA_PRSTZ_INI_NEWFIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_INI_NEWFIS = 8;
        public static final int WTGA_PRSTZ_INI_NEWFIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI_NEWFIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_INI_NEWFIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
