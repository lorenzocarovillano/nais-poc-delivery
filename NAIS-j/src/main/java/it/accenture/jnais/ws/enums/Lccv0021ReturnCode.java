package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCV0021-RETURN-CODE<br>
 * Variable: LCCV0021-RETURN-CODE from copybook LCCV0021<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Lccv0021ReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.RETURN_CODE);
    public static final String SUCCESSFUL_RC = "00";
    public static final String SQL_ERROR = "D3";

    //==== METHODS ====
    public void setReturnCode(String returnCode) {
        this.value = Functions.subString(returnCode, Len.RETURN_CODE);
    }

    public String getReturnCode() {
        return this.value;
    }

    public boolean isSuccessfulRc() {
        return value.equals(SUCCESSFUL_RC);
    }

    public void setSqlError() {
        value = SQL_ERROR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
