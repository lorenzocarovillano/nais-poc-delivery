package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-RIS-ACQ-T<br>
 * Variable: WB03-RIS-ACQ-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03RisAcqT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03RisAcqT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_RIS_ACQ_T;
    }

    public void setWb03RisAcqT(AfDecimal wb03RisAcqT) {
        writeDecimalAsPacked(Pos.WB03_RIS_ACQ_T, wb03RisAcqT.copy());
    }

    public void setWb03RisAcqTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_RIS_ACQ_T, Pos.WB03_RIS_ACQ_T);
    }

    /**Original name: WB03-RIS-ACQ-T<br>*/
    public AfDecimal getWb03RisAcqT() {
        return readPackedAsDecimal(Pos.WB03_RIS_ACQ_T, Len.Int.WB03_RIS_ACQ_T, Len.Fract.WB03_RIS_ACQ_T);
    }

    public byte[] getWb03RisAcqTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_RIS_ACQ_T, Pos.WB03_RIS_ACQ_T);
        return buffer;
    }

    public void setWb03RisAcqTNull(String wb03RisAcqTNull) {
        writeString(Pos.WB03_RIS_ACQ_T_NULL, wb03RisAcqTNull, Len.WB03_RIS_ACQ_T_NULL);
    }

    /**Original name: WB03-RIS-ACQ-T-NULL<br>*/
    public String getWb03RisAcqTNull() {
        return readString(Pos.WB03_RIS_ACQ_T_NULL, Len.WB03_RIS_ACQ_T_NULL);
    }

    public String getWb03RisAcqTNullFormatted() {
        return Functions.padBlanks(getWb03RisAcqTNull(), Len.WB03_RIS_ACQ_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_RIS_ACQ_T = 1;
        public static final int WB03_RIS_ACQ_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_RIS_ACQ_T = 8;
        public static final int WB03_RIS_ACQ_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_RIS_ACQ_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_RIS_ACQ_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
