package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbv3401DatiInput;
import it.accenture.jnais.copy.Ldbv3401GarOutput;

/**Original name: LDBV3401<br>
 * Variable: LDBV3401 from copybook LDBV3401<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbv3401 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBV3401-DATI-INPUT
    private Ldbv3401DatiInput ldbv3401DatiInput = new Ldbv3401DatiInput();
    //Original name: LDBV3401-GAR-OUTPUT
    private Ldbv3401GarOutput ldbv3401GarOutput = new Ldbv3401GarOutput();
    //Original name: L3401-TP-STAT-BUS
    private String l3401TpStatBus = DefaultValues.stringVal(Len.L3401_TP_STAT_BUS);
    //Original name: L3401-TP-CAUS
    private String l3401TpCaus = DefaultValues.stringVal(Len.L3401_TP_CAUS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV3401;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbv3401Bytes(buf);
    }

    public void setLdbv3401Bytes(byte[] buffer) {
        setLdbv3401Bytes(buffer, 1);
    }

    public byte[] getLdbv3401Bytes() {
        byte[] buffer = new byte[Len.LDBV3401];
        return getLdbv3401Bytes(buffer, 1);
    }

    public void setLdbv3401Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv3401DatiInput.setLdbv3401DatiInputBytes(buffer, position);
        position += Ldbv3401DatiInput.Len.LDBV3401_DATI_INPUT;
        ldbv3401GarOutput.setLdbv3401GarOutputBytes(buffer, position);
        position += Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT;
        setLdbv3401StbOutputBytes(buffer, position);
    }

    public byte[] getLdbv3401Bytes(byte[] buffer, int offset) {
        int position = offset;
        ldbv3401DatiInput.getLdbv3401DatiInputBytes(buffer, position);
        position += Ldbv3401DatiInput.Len.LDBV3401_DATI_INPUT;
        ldbv3401GarOutput.getLdbv3401GarOutputBytes(buffer, position);
        position += Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT;
        getLdbv3401StbOutputBytes(buffer, position);
        return buffer;
    }

    public void setLdbv3401StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        l3401TpStatBus = MarshalByte.readString(buffer, position, Len.L3401_TP_STAT_BUS);
        position += Len.L3401_TP_STAT_BUS;
        l3401TpCaus = MarshalByte.readString(buffer, position, Len.L3401_TP_CAUS);
    }

    public byte[] getLdbv3401StbOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, l3401TpStatBus, Len.L3401_TP_STAT_BUS);
        position += Len.L3401_TP_STAT_BUS;
        MarshalByte.writeString(buffer, position, l3401TpCaus, Len.L3401_TP_CAUS);
        return buffer;
    }

    public void setL3401TpStatBus(String l3401TpStatBus) {
        this.l3401TpStatBus = Functions.subString(l3401TpStatBus, Len.L3401_TP_STAT_BUS);
    }

    public String getL3401TpStatBus() {
        return this.l3401TpStatBus;
    }

    public void setL3401TpCaus(String l3401TpCaus) {
        this.l3401TpCaus = Functions.subString(l3401TpCaus, Len.L3401_TP_CAUS);
    }

    public String getL3401TpCaus() {
        return this.l3401TpCaus;
    }

    public Ldbv3401DatiInput getLdbv3401DatiInput() {
        return ldbv3401DatiInput;
    }

    public Ldbv3401GarOutput getLdbv3401GarOutput() {
        return ldbv3401GarOutput;
    }

    @Override
    public byte[] serialize() {
        return getLdbv3401Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_TP_STAT_BUS = 2;
        public static final int L3401_TP_CAUS = 2;
        public static final int LDBV3401_STB_OUTPUT = L3401_TP_STAT_BUS + L3401_TP_CAUS;
        public static final int LDBV3401 = Ldbv3401DatiInput.Len.LDBV3401_DATI_INPUT + Ldbv3401GarOutput.Len.LDBV3401_GAR_OUTPUT + LDBV3401_STB_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
