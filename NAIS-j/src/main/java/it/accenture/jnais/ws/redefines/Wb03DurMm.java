package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-MM<br>
 * Variable: WB03-DUR-MM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_MM;
    }

    public void setWb03DurMm(int wb03DurMm) {
        writeIntAsPacked(Pos.WB03_DUR_MM, wb03DurMm, Len.Int.WB03_DUR_MM);
    }

    public void setWb03DurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_MM, Pos.WB03_DUR_MM);
    }

    /**Original name: WB03-DUR-MM<br>*/
    public int getWb03DurMm() {
        return readPackedAsInt(Pos.WB03_DUR_MM, Len.Int.WB03_DUR_MM);
    }

    public byte[] getWb03DurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_MM, Pos.WB03_DUR_MM);
        return buffer;
    }

    public void setWb03DurMmNull(String wb03DurMmNull) {
        writeString(Pos.WB03_DUR_MM_NULL, wb03DurMmNull, Len.WB03_DUR_MM_NULL);
    }

    /**Original name: WB03-DUR-MM-NULL<br>*/
    public String getWb03DurMmNull() {
        return readString(Pos.WB03_DUR_MM_NULL, Len.WB03_DUR_MM_NULL);
    }

    public String getWb03DurMmNullFormatted() {
        return Functions.padBlanks(getWb03DurMmNull(), Len.WB03_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_MM = 1;
        public static final int WB03_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_MM = 3;
        public static final int WB03_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
