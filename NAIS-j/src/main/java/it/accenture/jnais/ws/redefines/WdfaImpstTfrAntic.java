package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPST-TFR-ANTIC<br>
 * Variable: WDFA-IMPST-TFR-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpstTfrAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpstTfrAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPST_TFR_ANTIC;
    }

    public void setWdfaImpstTfrAntic(AfDecimal wdfaImpstTfrAntic) {
        writeDecimalAsPacked(Pos.WDFA_IMPST_TFR_ANTIC, wdfaImpstTfrAntic.copy());
    }

    public void setWdfaImpstTfrAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPST_TFR_ANTIC, Pos.WDFA_IMPST_TFR_ANTIC);
    }

    /**Original name: WDFA-IMPST-TFR-ANTIC<br>*/
    public AfDecimal getWdfaImpstTfrAntic() {
        return readPackedAsDecimal(Pos.WDFA_IMPST_TFR_ANTIC, Len.Int.WDFA_IMPST_TFR_ANTIC, Len.Fract.WDFA_IMPST_TFR_ANTIC);
    }

    public byte[] getWdfaImpstTfrAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPST_TFR_ANTIC, Pos.WDFA_IMPST_TFR_ANTIC);
        return buffer;
    }

    public void setWdfaImpstTfrAnticNull(String wdfaImpstTfrAnticNull) {
        writeString(Pos.WDFA_IMPST_TFR_ANTIC_NULL, wdfaImpstTfrAnticNull, Len.WDFA_IMPST_TFR_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPST-TFR-ANTIC-NULL<br>*/
    public String getWdfaImpstTfrAnticNull() {
        return readString(Pos.WDFA_IMPST_TFR_ANTIC_NULL, Len.WDFA_IMPST_TFR_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_TFR_ANTIC = 1;
        public static final int WDFA_IMPST_TFR_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPST_TFR_ANTIC = 8;
        public static final int WDFA_IMPST_TFR_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_TFR_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPST_TFR_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
