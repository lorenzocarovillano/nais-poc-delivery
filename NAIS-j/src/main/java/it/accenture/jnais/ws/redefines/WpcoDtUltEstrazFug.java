package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ESTRAZ-FUG<br>
 * Variable: WPCO-DT-ULT-ESTRAZ-FUG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltEstrazFug extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltEstrazFug() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ESTRAZ_FUG;
    }

    public void setWpcoDtUltEstrazFug(int wpcoDtUltEstrazFug) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ESTRAZ_FUG, wpcoDtUltEstrazFug, Len.Int.WPCO_DT_ULT_ESTRAZ_FUG);
    }

    public void setDpcoDtUltEstrazFugFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ESTRAZ_FUG, Pos.WPCO_DT_ULT_ESTRAZ_FUG);
    }

    /**Original name: WPCO-DT-ULT-ESTRAZ-FUG<br>*/
    public int getWpcoDtUltEstrazFug() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ESTRAZ_FUG, Len.Int.WPCO_DT_ULT_ESTRAZ_FUG);
    }

    public byte[] getWpcoDtUltEstrazFugAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ESTRAZ_FUG, Pos.WPCO_DT_ULT_ESTRAZ_FUG);
        return buffer;
    }

    public void setWpcoDtUltEstrazFugNull(String wpcoDtUltEstrazFugNull) {
        writeString(Pos.WPCO_DT_ULT_ESTRAZ_FUG_NULL, wpcoDtUltEstrazFugNull, Len.WPCO_DT_ULT_ESTRAZ_FUG_NULL);
    }

    /**Original name: WPCO-DT-ULT-ESTRAZ-FUG-NULL<br>*/
    public String getWpcoDtUltEstrazFugNull() {
        return readString(Pos.WPCO_DT_ULT_ESTRAZ_FUG_NULL, Len.WPCO_DT_ULT_ESTRAZ_FUG_NULL);
    }

    public String getWpcoDtUltEstrazFugNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltEstrazFugNull(), Len.WPCO_DT_ULT_ESTRAZ_FUG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ESTRAZ_FUG = 1;
        public static final int WPCO_DT_ULT_ESTRAZ_FUG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ESTRAZ_FUG = 5;
        public static final int WPCO_DT_ULT_ESTRAZ_FUG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ESTRAZ_FUG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
