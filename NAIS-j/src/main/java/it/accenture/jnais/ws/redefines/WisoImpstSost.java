package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-IMPST-SOST<br>
 * Variable: WISO-IMPST-SOST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoImpstSost extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoImpstSost() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_IMPST_SOST;
    }

    public void setWisoImpstSost(AfDecimal wisoImpstSost) {
        writeDecimalAsPacked(Pos.WISO_IMPST_SOST, wisoImpstSost.copy());
    }

    public void setWisoImpstSostFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_IMPST_SOST, Pos.WISO_IMPST_SOST);
    }

    /**Original name: WISO-IMPST-SOST<br>*/
    public AfDecimal getWisoImpstSost() {
        return readPackedAsDecimal(Pos.WISO_IMPST_SOST, Len.Int.WISO_IMPST_SOST, Len.Fract.WISO_IMPST_SOST);
    }

    public byte[] getWisoImpstSostAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_IMPST_SOST, Pos.WISO_IMPST_SOST);
        return buffer;
    }

    public void initWisoImpstSostSpaces() {
        fill(Pos.WISO_IMPST_SOST, Len.WISO_IMPST_SOST, Types.SPACE_CHAR);
    }

    public void setWisoImpstSostNull(String wisoImpstSostNull) {
        writeString(Pos.WISO_IMPST_SOST_NULL, wisoImpstSostNull, Len.WISO_IMPST_SOST_NULL);
    }

    /**Original name: WISO-IMPST-SOST-NULL<br>*/
    public String getWisoImpstSostNull() {
        return readString(Pos.WISO_IMPST_SOST_NULL, Len.WISO_IMPST_SOST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_IMPST_SOST = 1;
        public static final int WISO_IMPST_SOST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_IMPST_SOST = 8;
        public static final int WISO_IMPST_SOST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_IMPST_SOST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_IMPST_SOST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
