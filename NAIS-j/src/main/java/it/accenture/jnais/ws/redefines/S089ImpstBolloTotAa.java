package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPST-BOLLO-TOT-AA<br>
 * Variable: S089-IMPST-BOLLO-TOT-AA from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpstBolloTotAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpstBolloTotAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPST_BOLLO_TOT_AA;
    }

    public void setWlquImpstBolloTotAa(AfDecimal wlquImpstBolloTotAa) {
        writeDecimalAsPacked(Pos.S089_IMPST_BOLLO_TOT_AA, wlquImpstBolloTotAa.copy());
    }

    public void setWlquImpstBolloTotAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPST_BOLLO_TOT_AA, Pos.S089_IMPST_BOLLO_TOT_AA);
    }

    /**Original name: WLQU-IMPST-BOLLO-TOT-AA<br>*/
    public AfDecimal getWlquImpstBolloTotAa() {
        return readPackedAsDecimal(Pos.S089_IMPST_BOLLO_TOT_AA, Len.Int.WLQU_IMPST_BOLLO_TOT_AA, Len.Fract.WLQU_IMPST_BOLLO_TOT_AA);
    }

    public byte[] getWlquImpstBolloTotAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPST_BOLLO_TOT_AA, Pos.S089_IMPST_BOLLO_TOT_AA);
        return buffer;
    }

    public void initWlquImpstBolloTotAaSpaces() {
        fill(Pos.S089_IMPST_BOLLO_TOT_AA, Len.S089_IMPST_BOLLO_TOT_AA, Types.SPACE_CHAR);
    }

    public void setWlquImpstBolloTotAaNull(String wlquImpstBolloTotAaNull) {
        writeString(Pos.S089_IMPST_BOLLO_TOT_AA_NULL, wlquImpstBolloTotAaNull, Len.WLQU_IMPST_BOLLO_TOT_AA_NULL);
    }

    /**Original name: WLQU-IMPST-BOLLO-TOT-AA-NULL<br>*/
    public String getWlquImpstBolloTotAaNull() {
        return readString(Pos.S089_IMPST_BOLLO_TOT_AA_NULL, Len.WLQU_IMPST_BOLLO_TOT_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_TOT_AA = 1;
        public static final int S089_IMPST_BOLLO_TOT_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPST_BOLLO_TOT_AA = 8;
        public static final int WLQU_IMPST_BOLLO_TOT_AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_TOT_AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPST_BOLLO_TOT_AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
