package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPB-TAX-SEP-CALC<br>
 * Variable: WDFL-IMPB-TAX-SEP-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpbTaxSepCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpbTaxSepCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPB_TAX_SEP_CALC;
    }

    public void setWdflImpbTaxSepCalc(AfDecimal wdflImpbTaxSepCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMPB_TAX_SEP_CALC, wdflImpbTaxSepCalc.copy());
    }

    public void setWdflImpbTaxSepCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPB_TAX_SEP_CALC, Pos.WDFL_IMPB_TAX_SEP_CALC);
    }

    /**Original name: WDFL-IMPB-TAX-SEP-CALC<br>*/
    public AfDecimal getWdflImpbTaxSepCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMPB_TAX_SEP_CALC, Len.Int.WDFL_IMPB_TAX_SEP_CALC, Len.Fract.WDFL_IMPB_TAX_SEP_CALC);
    }

    public byte[] getWdflImpbTaxSepCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPB_TAX_SEP_CALC, Pos.WDFL_IMPB_TAX_SEP_CALC);
        return buffer;
    }

    public void setWdflImpbTaxSepCalcNull(String wdflImpbTaxSepCalcNull) {
        writeString(Pos.WDFL_IMPB_TAX_SEP_CALC_NULL, wdflImpbTaxSepCalcNull, Len.WDFL_IMPB_TAX_SEP_CALC_NULL);
    }

    /**Original name: WDFL-IMPB-TAX-SEP-CALC-NULL<br>*/
    public String getWdflImpbTaxSepCalcNull() {
        return readString(Pos.WDFL_IMPB_TAX_SEP_CALC_NULL, Len.WDFL_IMPB_TAX_SEP_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TAX_SEP_CALC = 1;
        public static final int WDFL_IMPB_TAX_SEP_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPB_TAX_SEP_CALC = 8;
        public static final int WDFL_IMPB_TAX_SEP_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPB_TAX_SEP_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
