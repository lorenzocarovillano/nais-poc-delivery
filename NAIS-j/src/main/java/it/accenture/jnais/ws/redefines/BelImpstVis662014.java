package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-VIS-662014<br>
 * Variable: BEL-IMPST-VIS-662014 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_VIS662014;
    }

    public void setBelImpstVis662014(AfDecimal belImpstVis662014) {
        writeDecimalAsPacked(Pos.BEL_IMPST_VIS662014, belImpstVis662014.copy());
    }

    public void setBelImpstVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_VIS662014, Pos.BEL_IMPST_VIS662014);
    }

    /**Original name: BEL-IMPST-VIS-662014<br>*/
    public AfDecimal getBelImpstVis662014() {
        return readPackedAsDecimal(Pos.BEL_IMPST_VIS662014, Len.Int.BEL_IMPST_VIS662014, Len.Fract.BEL_IMPST_VIS662014);
    }

    public byte[] getBelImpstVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_VIS662014, Pos.BEL_IMPST_VIS662014);
        return buffer;
    }

    public void setBelImpstVis662014Null(String belImpstVis662014Null) {
        writeString(Pos.BEL_IMPST_VIS662014_NULL, belImpstVis662014Null, Len.BEL_IMPST_VIS662014_NULL);
    }

    /**Original name: BEL-IMPST-VIS-662014-NULL<br>*/
    public String getBelImpstVis662014Null() {
        return readString(Pos.BEL_IMPST_VIS662014_NULL, Len.BEL_IMPST_VIS662014_NULL);
    }

    public String getBelImpstVis662014NullFormatted() {
        return Functions.padBlanks(getBelImpstVis662014Null(), Len.BEL_IMPST_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_VIS662014 = 1;
        public static final int BEL_IMPST_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_VIS662014 = 8;
        public static final int BEL_IMPST_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
