package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISO-ID-MOVI-CHIU<br>
 * Variable: ISO-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class IsoIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public IsoIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ISO_ID_MOVI_CHIU;
    }

    public void setIsoIdMoviChiu(int isoIdMoviChiu) {
        writeIntAsPacked(Pos.ISO_ID_MOVI_CHIU, isoIdMoviChiu, Len.Int.ISO_ID_MOVI_CHIU);
    }

    public void setIsoIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ISO_ID_MOVI_CHIU, Pos.ISO_ID_MOVI_CHIU);
    }

    /**Original name: ISO-ID-MOVI-CHIU<br>*/
    public int getIsoIdMoviChiu() {
        return readPackedAsInt(Pos.ISO_ID_MOVI_CHIU, Len.Int.ISO_ID_MOVI_CHIU);
    }

    public byte[] getIsoIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ISO_ID_MOVI_CHIU, Pos.ISO_ID_MOVI_CHIU);
        return buffer;
    }

    public void setIsoIdMoviChiuNull(String isoIdMoviChiuNull) {
        writeString(Pos.ISO_ID_MOVI_CHIU_NULL, isoIdMoviChiuNull, Len.ISO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: ISO-ID-MOVI-CHIU-NULL<br>*/
    public String getIsoIdMoviChiuNull() {
        return readString(Pos.ISO_ID_MOVI_CHIU_NULL, Len.ISO_ID_MOVI_CHIU_NULL);
    }

    public String getIsoIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getIsoIdMoviChiuNull(), Len.ISO_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ISO_ID_MOVI_CHIU = 1;
        public static final int ISO_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ISO_ID_MOVI_CHIU = 5;
        public static final int ISO_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ISO_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
