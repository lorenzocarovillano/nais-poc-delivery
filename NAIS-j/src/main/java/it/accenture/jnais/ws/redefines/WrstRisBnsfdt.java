package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-BNSFDT<br>
 * Variable: WRST-RIS-BNSFDT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisBnsfdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisBnsfdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_BNSFDT;
    }

    public void setWrstRisBnsfdt(AfDecimal wrstRisBnsfdt) {
        writeDecimalAsPacked(Pos.WRST_RIS_BNSFDT, wrstRisBnsfdt.copy());
    }

    public void setWrstRisBnsfdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_BNSFDT, Pos.WRST_RIS_BNSFDT);
    }

    /**Original name: WRST-RIS-BNSFDT<br>*/
    public AfDecimal getWrstRisBnsfdt() {
        return readPackedAsDecimal(Pos.WRST_RIS_BNSFDT, Len.Int.WRST_RIS_BNSFDT, Len.Fract.WRST_RIS_BNSFDT);
    }

    public byte[] getWrstRisBnsfdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_BNSFDT, Pos.WRST_RIS_BNSFDT);
        return buffer;
    }

    public void initWrstRisBnsfdtSpaces() {
        fill(Pos.WRST_RIS_BNSFDT, Len.WRST_RIS_BNSFDT, Types.SPACE_CHAR);
    }

    public void setWrstRisBnsfdtNull(String wrstRisBnsfdtNull) {
        writeString(Pos.WRST_RIS_BNSFDT_NULL, wrstRisBnsfdtNull, Len.WRST_RIS_BNSFDT_NULL);
    }

    /**Original name: WRST-RIS-BNSFDT-NULL<br>*/
    public String getWrstRisBnsfdtNull() {
        return readString(Pos.WRST_RIS_BNSFDT_NULL, Len.WRST_RIS_BNSFDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_BNSFDT = 1;
        public static final int WRST_RIS_BNSFDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_BNSFDT = 8;
        public static final int WRST_RIS_BNSFDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_BNSFDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_BNSFDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
