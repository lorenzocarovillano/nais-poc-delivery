package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSD-RICORRENZA<br>
 * Variable: CSD-RICORRENZA from program IDSS0020<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class CsdRicorrenza extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public CsdRicorrenza() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.CSD_RICORRENZA;
    }

    public void setCsdRicorrenza(int csdRicorrenza) {
        writeIntAsPacked(Pos.CSD_RICORRENZA, csdRicorrenza, Len.Int.CSD_RICORRENZA);
    }

    /**Original name: CSD-RICORRENZA<br>*/
    public int getCsdRicorrenza() {
        return readPackedAsInt(Pos.CSD_RICORRENZA, Len.Int.CSD_RICORRENZA);
    }

    public void initCsdRicorrenzaSpaces() {
        fill(Pos.CSD_RICORRENZA, Len.CSD_RICORRENZA, Types.SPACE_CHAR);
    }

    public void setCsdRicorrenzaNull(String csdRicorrenzaNull) {
        writeString(Pos.CSD_RICORRENZA_NULL, csdRicorrenzaNull, Len.CSD_RICORRENZA_NULL);
    }

    /**Original name: CSD-RICORRENZA-NULL<br>*/
    public String getCsdRicorrenzaNull() {
        return readString(Pos.CSD_RICORRENZA_NULL, Len.CSD_RICORRENZA_NULL);
    }

    public String getCsdRicorrenzaNullFormatted() {
        return Functions.padBlanks(getCsdRicorrenzaNull(), Len.CSD_RICORRENZA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int CSD_RICORRENZA = 1;
        public static final int CSD_RICORRENZA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int CSD_RICORRENZA = 3;
        public static final int CSD_RICORRENZA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CSD_RICORRENZA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
