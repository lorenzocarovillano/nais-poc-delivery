package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-BATCH-STATE<br>
 * Variable: FLAG-BATCH-STATE from program IABS0130<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagBatchState {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_BATCH_STATE);
    public static final String OK = "OK";
    public static final String KO = "KO";

    //==== METHODS ====
    public void setFlagBatchState(String flagBatchState) {
        this.value = Functions.subString(flagBatchState, Len.FLAG_BATCH_STATE);
    }

    public String getFlagBatchState() {
        return this.value;
    }

    public boolean isBatchStateOk() {
        return value.equals(OK);
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_BATCH_STATE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
