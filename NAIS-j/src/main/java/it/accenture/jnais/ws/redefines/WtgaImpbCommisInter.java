package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMPB-COMMIS-INTER<br>
 * Variable: WTGA-IMPB-COMMIS-INTER from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpbCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpbCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMPB_COMMIS_INTER;
    }

    public void setWtgaImpbCommisInter(AfDecimal wtgaImpbCommisInter) {
        writeDecimalAsPacked(Pos.WTGA_IMPB_COMMIS_INTER, wtgaImpbCommisInter.copy());
    }

    public void setWtgaImpbCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMPB_COMMIS_INTER, Pos.WTGA_IMPB_COMMIS_INTER);
    }

    /**Original name: WTGA-IMPB-COMMIS-INTER<br>*/
    public AfDecimal getWtgaImpbCommisInter() {
        return readPackedAsDecimal(Pos.WTGA_IMPB_COMMIS_INTER, Len.Int.WTGA_IMPB_COMMIS_INTER, Len.Fract.WTGA_IMPB_COMMIS_INTER);
    }

    public byte[] getWtgaImpbCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMPB_COMMIS_INTER, Pos.WTGA_IMPB_COMMIS_INTER);
        return buffer;
    }

    public void initWtgaImpbCommisInterSpaces() {
        fill(Pos.WTGA_IMPB_COMMIS_INTER, Len.WTGA_IMPB_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWtgaImpbCommisInterNull(String wtgaImpbCommisInterNull) {
        writeString(Pos.WTGA_IMPB_COMMIS_INTER_NULL, wtgaImpbCommisInterNull, Len.WTGA_IMPB_COMMIS_INTER_NULL);
    }

    /**Original name: WTGA-IMPB-COMMIS-INTER-NULL<br>*/
    public String getWtgaImpbCommisInterNull() {
        return readString(Pos.WTGA_IMPB_COMMIS_INTER_NULL, Len.WTGA_IMPB_COMMIS_INTER_NULL);
    }

    public String getWtgaImpbCommisInterNullFormatted() {
        return Functions.padBlanks(getWtgaImpbCommisInterNull(), Len.WTGA_IMPB_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_COMMIS_INTER = 1;
        public static final int WTGA_IMPB_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMPB_COMMIS_INTER = 8;
        public static final int WTGA_IMPB_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMPB_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
