package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSI0011-FORMATO-DATA-DB<br>
 * Variable: IDSI0011-FORMATO-DATA-DB from copybook IDSI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0011FormatoDataDb {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IDSI0011_FORMATO_DATA_DB);
    public static final String ISO = "ISO";
    public static final String EUR = "EUR";

    //==== METHODS ====
    public void setIdsi0011FormatoDataDb(String idsi0011FormatoDataDb) {
        this.value = Functions.subString(idsi0011FormatoDataDb, Len.IDSI0011_FORMATO_DATA_DB);
    }

    public String getIdsi0011FormatoDataDb() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSI0011_FORMATO_DATA_DB = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
