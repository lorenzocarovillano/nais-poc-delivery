package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ICNB-INPSTFM-DFZ<br>
 * Variable: WDFL-ICNB-INPSTFM-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIcnbInpstfmDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIcnbInpstfmDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ICNB_INPSTFM_DFZ;
    }

    public void setWdflIcnbInpstfmDfz(AfDecimal wdflIcnbInpstfmDfz) {
        writeDecimalAsPacked(Pos.WDFL_ICNB_INPSTFM_DFZ, wdflIcnbInpstfmDfz.copy());
    }

    public void setWdflIcnbInpstfmDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ICNB_INPSTFM_DFZ, Pos.WDFL_ICNB_INPSTFM_DFZ);
    }

    /**Original name: WDFL-ICNB-INPSTFM-DFZ<br>*/
    public AfDecimal getWdflIcnbInpstfmDfz() {
        return readPackedAsDecimal(Pos.WDFL_ICNB_INPSTFM_DFZ, Len.Int.WDFL_ICNB_INPSTFM_DFZ, Len.Fract.WDFL_ICNB_INPSTFM_DFZ);
    }

    public byte[] getWdflIcnbInpstfmDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ICNB_INPSTFM_DFZ, Pos.WDFL_ICNB_INPSTFM_DFZ);
        return buffer;
    }

    public void setWdflIcnbInpstfmDfzNull(String wdflIcnbInpstfmDfzNull) {
        writeString(Pos.WDFL_ICNB_INPSTFM_DFZ_NULL, wdflIcnbInpstfmDfzNull, Len.WDFL_ICNB_INPSTFM_DFZ_NULL);
    }

    /**Original name: WDFL-ICNB-INPSTFM-DFZ-NULL<br>*/
    public String getWdflIcnbInpstfmDfzNull() {
        return readString(Pos.WDFL_ICNB_INPSTFM_DFZ_NULL, Len.WDFL_ICNB_INPSTFM_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ICNB_INPSTFM_DFZ = 1;
        public static final int WDFL_ICNB_INPSTFM_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ICNB_INPSTFM_DFZ = 8;
        public static final int WDFL_ICNB_INPSTFM_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ICNB_INPSTFM_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ICNB_INPSTFM_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
