package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDCO-DT-ULT-RINN-TAC<br>
 * Variable: WDCO-DT-ULT-RINN-TAC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdcoDtUltRinnTac extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdcoDtUltRinnTac() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDCO_DT_ULT_RINN_TAC;
    }

    public void setWdcoDtUltRinnTac(int wdcoDtUltRinnTac) {
        writeIntAsPacked(Pos.WDCO_DT_ULT_RINN_TAC, wdcoDtUltRinnTac, Len.Int.WDCO_DT_ULT_RINN_TAC);
    }

    public void setWdcoDtUltRinnTacFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDCO_DT_ULT_RINN_TAC, Pos.WDCO_DT_ULT_RINN_TAC);
    }

    /**Original name: WDCO-DT-ULT-RINN-TAC<br>*/
    public int getWdcoDtUltRinnTac() {
        return readPackedAsInt(Pos.WDCO_DT_ULT_RINN_TAC, Len.Int.WDCO_DT_ULT_RINN_TAC);
    }

    public byte[] getWdcoDtUltRinnTacAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDCO_DT_ULT_RINN_TAC, Pos.WDCO_DT_ULT_RINN_TAC);
        return buffer;
    }

    public void setWdcoDtUltRinnTacNull(String wdcoDtUltRinnTacNull) {
        writeString(Pos.WDCO_DT_ULT_RINN_TAC_NULL, wdcoDtUltRinnTacNull, Len.WDCO_DT_ULT_RINN_TAC_NULL);
    }

    /**Original name: WDCO-DT-ULT-RINN-TAC-NULL<br>*/
    public String getWdcoDtUltRinnTacNull() {
        return readString(Pos.WDCO_DT_ULT_RINN_TAC_NULL, Len.WDCO_DT_ULT_RINN_TAC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDCO_DT_ULT_RINN_TAC = 1;
        public static final int WDCO_DT_ULT_RINN_TAC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDCO_DT_ULT_RINN_TAC = 5;
        public static final int WDCO_DT_ULT_RINN_TAC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDCO_DT_ULT_RINN_TAC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
