package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WTIT-DT-CERT-FISC<br>
 * Variable: WTIT-DT-CERT-FISC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitDtCertFisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitDtCertFisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_DT_CERT_FISC;
    }

    public void setWtitDtCertFisc(int wtitDtCertFisc) {
        writeIntAsPacked(Pos.WTIT_DT_CERT_FISC, wtitDtCertFisc, Len.Int.WTIT_DT_CERT_FISC);
    }

    public void setWtitDtCertFiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_DT_CERT_FISC, Pos.WTIT_DT_CERT_FISC);
    }

    /**Original name: WTIT-DT-CERT-FISC<br>*/
    public int getWtitDtCertFisc() {
        return readPackedAsInt(Pos.WTIT_DT_CERT_FISC, Len.Int.WTIT_DT_CERT_FISC);
    }

    public byte[] getWtitDtCertFiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_DT_CERT_FISC, Pos.WTIT_DT_CERT_FISC);
        return buffer;
    }

    public void initWtitDtCertFiscSpaces() {
        fill(Pos.WTIT_DT_CERT_FISC, Len.WTIT_DT_CERT_FISC, Types.SPACE_CHAR);
    }

    public void setWtitDtCertFiscNull(String wtitDtCertFiscNull) {
        writeString(Pos.WTIT_DT_CERT_FISC_NULL, wtitDtCertFiscNull, Len.WTIT_DT_CERT_FISC_NULL);
    }

    /**Original name: WTIT-DT-CERT-FISC-NULL<br>*/
    public String getWtitDtCertFiscNull() {
        return readString(Pos.WTIT_DT_CERT_FISC_NULL, Len.WTIT_DT_CERT_FISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_DT_CERT_FISC = 1;
        public static final int WTIT_DT_CERT_FISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_DT_CERT_FISC = 5;
        public static final int WTIT_DT_CERT_FISC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_DT_CERT_FISC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
