package it.accenture.jnais.ws;

/**Original name: WK-AREA-CONTATORI<br>
 * Variable: WK-AREA-CONTATORI from program LRGM0380<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAreaContatori {

    //==== PROPERTIES ====
    //Original name: DESC-CTR-1
    private String descCtr1 = "   POLIZZE LETTE";
    //Original name: REC-ELABORATI
    private short recElaborati = ((short)1);
    //Original name: DESC-CTR-2
    private String descCtr2 = "   POLIZZE DA SCARTARE";
    //Original name: REC-SCARTATI
    private short recScartati = ((short)2);
    //Original name: DESC-CTR-3
    private String descCtr3 = "TOTALE PERCENTUALE INVIABILI  %";
    //Original name: REC-PERC
    private short recPerc = ((short)3);

    //==== METHODS ====
    public String getDescCtr1() {
        return this.descCtr1;
    }

    public short getRecElaborati() {
        return this.recElaborati;
    }

    public String getDescCtr2() {
        return this.descCtr2;
    }

    public short getRecScartati() {
        return this.recScartati;
    }

    public String getDescCtr3() {
        return this.descCtr3;
    }

    public short getRecPerc() {
        return this.recPerc;
    }
}
