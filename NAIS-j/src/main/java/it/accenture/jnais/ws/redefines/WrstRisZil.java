package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-ZIL<br>
 * Variable: WRST-RIS-ZIL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisZil extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisZil() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_ZIL;
    }

    public void setWrstRisZil(AfDecimal wrstRisZil) {
        writeDecimalAsPacked(Pos.WRST_RIS_ZIL, wrstRisZil.copy());
    }

    public void setWrstRisZilFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_ZIL, Pos.WRST_RIS_ZIL);
    }

    /**Original name: WRST-RIS-ZIL<br>*/
    public AfDecimal getWrstRisZil() {
        return readPackedAsDecimal(Pos.WRST_RIS_ZIL, Len.Int.WRST_RIS_ZIL, Len.Fract.WRST_RIS_ZIL);
    }

    public byte[] getWrstRisZilAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_ZIL, Pos.WRST_RIS_ZIL);
        return buffer;
    }

    public void initWrstRisZilSpaces() {
        fill(Pos.WRST_RIS_ZIL, Len.WRST_RIS_ZIL, Types.SPACE_CHAR);
    }

    public void setWrstRisZilNull(String wrstRisZilNull) {
        writeString(Pos.WRST_RIS_ZIL_NULL, wrstRisZilNull, Len.WRST_RIS_ZIL_NULL);
    }

    /**Original name: WRST-RIS-ZIL-NULL<br>*/
    public String getWrstRisZilNull() {
        return readString(Pos.WRST_RIS_ZIL_NULL, Len.WRST_RIS_ZIL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_ZIL = 1;
        public static final int WRST_RIS_ZIL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_ZIL = 8;
        public static final int WRST_RIS_ZIL_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_ZIL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_ZIL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
