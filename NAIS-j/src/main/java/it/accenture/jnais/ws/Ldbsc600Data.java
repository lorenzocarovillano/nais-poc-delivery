package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idbvstb3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndTrchDiGar;
import it.accenture.jnais.copy.StatOggBus;
import it.accenture.jnais.copy.TrchDiGarDb;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSC600<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsc600Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-TRCH-DI-GAR
    private IndTrchDiGar indTrchDiGar = new IndTrchDiGar();
    //Original name: IND-STB-ID-MOVI-CHIU
    private short indStbIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: TRCH-DI-GAR-DB
    private TrchDiGarDb trchDiGarDb = new TrchDiGarDb();
    //Original name: IDBVSTB3
    private Idbvstb3 idbvstb3 = new Idbvstb3();
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndStbIdMoviChiu(short indStbIdMoviChiu) {
        this.indStbIdMoviChiu = indStbIdMoviChiu;
    }

    public short getIndStbIdMoviChiu() {
        return this.indStbIdMoviChiu;
    }

    public Idbvstb3 getIdbvstb3() {
        return idbvstb3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndTrchDiGar getIndTrchDiGar() {
        return indTrchDiGar;
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public TrchDiGarDb getTrchDiGarDb() {
        return trchDiGarDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
