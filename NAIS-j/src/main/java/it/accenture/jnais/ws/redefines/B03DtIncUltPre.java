package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-INC-ULT-PRE<br>
 * Variable: B03-DT-INC-ULT-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtIncUltPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtIncUltPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_INC_ULT_PRE;
    }

    public void setB03DtIncUltPre(int b03DtIncUltPre) {
        writeIntAsPacked(Pos.B03_DT_INC_ULT_PRE, b03DtIncUltPre, Len.Int.B03_DT_INC_ULT_PRE);
    }

    public void setB03DtIncUltPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_INC_ULT_PRE, Pos.B03_DT_INC_ULT_PRE);
    }

    /**Original name: B03-DT-INC-ULT-PRE<br>*/
    public int getB03DtIncUltPre() {
        return readPackedAsInt(Pos.B03_DT_INC_ULT_PRE, Len.Int.B03_DT_INC_ULT_PRE);
    }

    public byte[] getB03DtIncUltPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_INC_ULT_PRE, Pos.B03_DT_INC_ULT_PRE);
        return buffer;
    }

    public void setB03DtIncUltPreNull(String b03DtIncUltPreNull) {
        writeString(Pos.B03_DT_INC_ULT_PRE_NULL, b03DtIncUltPreNull, Len.B03_DT_INC_ULT_PRE_NULL);
    }

    /**Original name: B03-DT-INC-ULT-PRE-NULL<br>*/
    public String getB03DtIncUltPreNull() {
        return readString(Pos.B03_DT_INC_ULT_PRE_NULL, Len.B03_DT_INC_ULT_PRE_NULL);
    }

    public String getB03DtIncUltPreNullFormatted() {
        return Functions.padBlanks(getB03DtIncUltPreNull(), Len.B03_DT_INC_ULT_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_INC_ULT_PRE = 1;
        public static final int B03_DT_INC_ULT_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_INC_ULT_PRE = 5;
        public static final int B03_DT_INC_ULT_PRE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_INC_ULT_PRE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
