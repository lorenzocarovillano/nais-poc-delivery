package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-PREST<br>
 * Variable: LQU-TOT-IMP-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_PREST;
    }

    public void setLquTotImpPrest(AfDecimal lquTotImpPrest) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_PREST, lquTotImpPrest.copy());
    }

    public void setLquTotImpPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_PREST, Pos.LQU_TOT_IMP_PREST);
    }

    /**Original name: LQU-TOT-IMP-PREST<br>*/
    public AfDecimal getLquTotImpPrest() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_PREST, Len.Int.LQU_TOT_IMP_PREST, Len.Fract.LQU_TOT_IMP_PREST);
    }

    public byte[] getLquTotImpPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_PREST, Pos.LQU_TOT_IMP_PREST);
        return buffer;
    }

    public void setLquTotImpPrestNull(String lquTotImpPrestNull) {
        writeString(Pos.LQU_TOT_IMP_PREST_NULL, lquTotImpPrestNull, Len.LQU_TOT_IMP_PREST_NULL);
    }

    /**Original name: LQU-TOT-IMP-PREST-NULL<br>*/
    public String getLquTotImpPrestNull() {
        return readString(Pos.LQU_TOT_IMP_PREST_NULL, Len.LQU_TOT_IMP_PREST_NULL);
    }

    public String getLquTotImpPrestNullFormatted() {
        return Functions.padBlanks(getLquTotImpPrestNull(), Len.LQU_TOT_IMP_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_PREST = 1;
        public static final int LQU_TOT_IMP_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_PREST = 8;
        public static final int LQU_TOT_IMP_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
