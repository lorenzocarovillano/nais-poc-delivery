package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.WkFrazMm;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program LVES0245<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabiliLves0245 {

    //==== PROPERTIES ====
    //Original name: WK-FRQ-MOVI
    private String frqMovi = DefaultValues.stringVal(Len.FRQ_MOVI);
    //Original name: WK-FRAZ-MM
    private WkFrazMm frazMm = new WkFrazMm();
    /**Original name: WK-RATE-ANTIC<br>
	 * <pre>--  numero rate anticipate</pre>*/
    private long rateAntic = DefaultValues.LONG_VAL;
    //Original name: WK-PARAM-OGG
    private String paramOgg = DefaultValues.stringVal(Len.PARAM_OGG);
    public static final String PR_RATEANTIC = "RATEANTIC";

    //==== METHODS ====
    public void setFrqMovi(int frqMovi) {
        this.frqMovi = NumericDisplay.asString(frqMovi, Len.FRQ_MOVI);
    }

    public void setFrqMoviFormatted(String frqMovi) {
        this.frqMovi = Trunc.toUnsignedNumeric(frqMovi, Len.FRQ_MOVI);
    }

    public int getFrqMovi() {
        return NumericDisplay.asInt(this.frqMovi);
    }

    public void setRateAntic(long rateAntic) {
        this.rateAntic = rateAntic;
    }

    public long getRateAntic() {
        return this.rateAntic;
    }

    public void setParamOgg(String paramOgg) {
        this.paramOgg = Functions.subString(paramOgg, Len.PARAM_OGG);
    }

    public String getParamOgg() {
        return this.paramOgg;
    }

    public void setPrRateantic() {
        paramOgg = PR_RATEANTIC;
    }

    public WkFrazMm getFrazMm() {
        return frazMm;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FRQ_MOVI = 5;
        public static final int PARAM_OGG = 12;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
