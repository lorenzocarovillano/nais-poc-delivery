package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-CNTRL-30<br>
 * Variable: FLAG-CNTRL-30 from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCntrl30 {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCntrl30(char flagCntrl30) {
        this.value = flagCntrl30;
    }

    public char getFlagCntrl30() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
