package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.commons.data.to.IBtcExeMessage;

/**Original name: BTC-EXE-MESSAGE<br>
 * Variable: BTC-EXE-MESSAGE from copybook IDBVBEM1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcExeMessage extends SerializableParameter implements IBtcExeMessage {

    //==== PROPERTIES ====
    //Original name: BEM-ID-BATCH
    private int idBatch = DefaultValues.BIN_INT_VAL;
    //Original name: BEM-ID-JOB
    private int idJob = DefaultValues.BIN_INT_VAL;
    //Original name: BEM-ID-EXECUTION
    private int idExecution = DefaultValues.BIN_INT_VAL;
    //Original name: BEM-ID-MESSAGE
    private int idMessage = DefaultValues.BIN_INT_VAL;
    //Original name: BEM-COD-MESSAGE
    private int codMessage = DefaultValues.BIN_INT_VAL;
    //Original name: BEM-MESSAGE-LEN
    private short messageLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BEM-MESSAGE
    private String message = DefaultValues.stringVal(Len.MESSAGE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_EXE_MESSAGE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcExeMessageBytes(buf);
    }

    public String getBtcExeMessageFormatted() {
        return MarshalByteExt.bufferToStr(getBtcExeMessageBytes());
    }

    public void setBtcExeMessageBytes(byte[] buffer) {
        setBtcExeMessageBytes(buffer, 1);
    }

    public byte[] getBtcExeMessageBytes() {
        byte[] buffer = new byte[Len.BTC_EXE_MESSAGE];
        return getBtcExeMessageBytes(buffer, 1);
    }

    public void setBtcExeMessageBytes(byte[] buffer, int offset) {
        int position = offset;
        idBatch = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idJob = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idExecution = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        idMessage = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        codMessage = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        setMessageVcharBytes(buffer, position);
    }

    public byte[] getBtcExeMessageBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, idBatch);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idJob);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idExecution);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, idMessage);
        position += Types.INT_SIZE;
        MarshalByte.writeBinaryInt(buffer, position, codMessage);
        position += Types.INT_SIZE;
        getMessageVcharBytes(buffer, position);
        return buffer;
    }

    @Override
    public void setIdBatch(int idBatch) {
        this.idBatch = idBatch;
    }

    @Override
    public int getIdBatch() {
        return this.idBatch;
    }

    @Override
    public void setIdJob(int idJob) {
        this.idJob = idJob;
    }

    @Override
    public int getIdJob() {
        return this.idJob;
    }

    @Override
    public void setIdExecution(int idExecution) {
        this.idExecution = idExecution;
    }

    @Override
    public int getIdExecution() {
        return this.idExecution;
    }

    @Override
    public void setIdMessage(int idMessage) {
        this.idMessage = idMessage;
    }

    @Override
    public int getIdMessage() {
        return this.idMessage;
    }

    @Override
    public void setCodMessage(int codMessage) {
        this.codMessage = codMessage;
    }

    @Override
    public int getCodMessage() {
        return this.codMessage;
    }

    public void setMessageVcharFormatted(String data) {
        byte[] buffer = new byte[Len.MESSAGE_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.MESSAGE_VCHAR);
        setMessageVcharBytes(buffer, 1);
    }

    public String getMessageVcharFormatted() {
        return MarshalByteExt.bufferToStr(getMessageVcharBytes());
    }

    /**Original name: BEM-MESSAGE-VCHAR<br>*/
    public byte[] getMessageVcharBytes() {
        byte[] buffer = new byte[Len.MESSAGE_VCHAR];
        return getMessageVcharBytes(buffer, 1);
    }

    public void setMessageVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        messageLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        message = MarshalByte.readString(buffer, position, Len.MESSAGE);
    }

    public byte[] getMessageVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, messageLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, message, Len.MESSAGE);
        return buffer;
    }

    public void setMessageLen(short messageLen) {
        this.messageLen = messageLen;
    }

    public short getMessageLen() {
        return this.messageLen;
    }

    public void setMessage(String message) {
        this.message = Functions.subString(message, Len.MESSAGE);
    }

    public String getMessage() {
        return this.message;
    }

    public String getMessageFormatted() {
        return Functions.padBlanks(getMessage(), Len.MESSAGE);
    }

    @Override
    public String getMessageVchar() {
        return getMessageVcharFormatted();
    }

    @Override
    public void setMessageVchar(String messageVchar) {
        this.setMessageVcharFormatted(messageVchar);
    }

    @Override
    public byte[] serialize() {
        return getBtcExeMessageBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_BATCH = 4;
        public static final int ID_JOB = 4;
        public static final int ID_EXECUTION = 4;
        public static final int ID_MESSAGE = 4;
        public static final int COD_MESSAGE = 4;
        public static final int MESSAGE_LEN = 2;
        public static final int MESSAGE = 1024;
        public static final int MESSAGE_VCHAR = MESSAGE_LEN + MESSAGE;
        public static final int BTC_EXE_MESSAGE = ID_BATCH + ID_JOB + ID_EXECUTION + ID_MESSAGE + COD_MESSAGE + MESSAGE_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
