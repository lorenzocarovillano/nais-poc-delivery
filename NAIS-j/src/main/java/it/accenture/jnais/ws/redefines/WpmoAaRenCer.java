package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-AA-REN-CER<br>
 * Variable: WPMO-AA-REN-CER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoAaRenCer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoAaRenCer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_AA_REN_CER;
    }

    public void setWpmoAaRenCer(int wpmoAaRenCer) {
        writeIntAsPacked(Pos.WPMO_AA_REN_CER, wpmoAaRenCer, Len.Int.WPMO_AA_REN_CER);
    }

    public void setWpmoAaRenCerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_AA_REN_CER, Pos.WPMO_AA_REN_CER);
    }

    /**Original name: WPMO-AA-REN-CER<br>*/
    public int getWpmoAaRenCer() {
        return readPackedAsInt(Pos.WPMO_AA_REN_CER, Len.Int.WPMO_AA_REN_CER);
    }

    public byte[] getWpmoAaRenCerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_AA_REN_CER, Pos.WPMO_AA_REN_CER);
        return buffer;
    }

    public void initWpmoAaRenCerSpaces() {
        fill(Pos.WPMO_AA_REN_CER, Len.WPMO_AA_REN_CER, Types.SPACE_CHAR);
    }

    public void setWpmoAaRenCerNull(String wpmoAaRenCerNull) {
        writeString(Pos.WPMO_AA_REN_CER_NULL, wpmoAaRenCerNull, Len.WPMO_AA_REN_CER_NULL);
    }

    /**Original name: WPMO-AA-REN-CER-NULL<br>*/
    public String getWpmoAaRenCerNull() {
        return readString(Pos.WPMO_AA_REN_CER_NULL, Len.WPMO_AA_REN_CER_NULL);
    }

    public String getWpmoAaRenCerNullFormatted() {
        return Functions.padBlanks(getWpmoAaRenCerNull(), Len.WPMO_AA_REN_CER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_AA_REN_CER = 1;
        public static final int WPMO_AA_REN_CER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_AA_REN_CER = 3;
        public static final int WPMO_AA_REN_CER_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_AA_REN_CER = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
