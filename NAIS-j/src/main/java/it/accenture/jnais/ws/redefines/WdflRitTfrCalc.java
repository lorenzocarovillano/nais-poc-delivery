package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-TFR-CALC<br>
 * Variable: WDFL-RIT-TFR-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitTfrCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitTfrCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_TFR_CALC;
    }

    public void setWdflRitTfrCalc(AfDecimal wdflRitTfrCalc) {
        writeDecimalAsPacked(Pos.WDFL_RIT_TFR_CALC, wdflRitTfrCalc.copy());
    }

    public void setWdflRitTfrCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_TFR_CALC, Pos.WDFL_RIT_TFR_CALC);
    }

    /**Original name: WDFL-RIT-TFR-CALC<br>*/
    public AfDecimal getWdflRitTfrCalc() {
        return readPackedAsDecimal(Pos.WDFL_RIT_TFR_CALC, Len.Int.WDFL_RIT_TFR_CALC, Len.Fract.WDFL_RIT_TFR_CALC);
    }

    public byte[] getWdflRitTfrCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_TFR_CALC, Pos.WDFL_RIT_TFR_CALC);
        return buffer;
    }

    public void setWdflRitTfrCalcNull(String wdflRitTfrCalcNull) {
        writeString(Pos.WDFL_RIT_TFR_CALC_NULL, wdflRitTfrCalcNull, Len.WDFL_RIT_TFR_CALC_NULL);
    }

    /**Original name: WDFL-RIT-TFR-CALC-NULL<br>*/
    public String getWdflRitTfrCalcNull() {
        return readString(Pos.WDFL_RIT_TFR_CALC_NULL, Len.WDFL_RIT_TFR_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_TFR_CALC = 1;
        public static final int WDFL_RIT_TFR_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_TFR_CALC = 8;
        public static final int WDFL_RIT_TFR_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_TFR_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_TFR_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
