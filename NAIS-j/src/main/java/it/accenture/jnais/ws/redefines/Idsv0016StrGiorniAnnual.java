package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: IDSV0016-STR-GIORNI-ANNUAL<br>
 * Variable: IDSV0016-STR-GIORNI-ANNUAL from program LLBM0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Idsv0016StrGiorniAnnual extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int IDSV0016_STR_GIORNI_ANNUAL_ELE_MAXOCCURS = 12;

    //==== CONSTRUCTORS ====
    public Idsv0016StrGiorniAnnual() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0016_STR_GIORNI_ANNUAL;
    }

    @Override
    public void init() {
        int position = 1;
        writeShort(position, ((short)31), Len.Int.GENNAIO, SignType.NO_SIGN);
        position += Len.GENNAIO;
        writeShort(position, ((short)28), Len.Int.FEBBRAIO, SignType.NO_SIGN);
        position += Len.FEBBRAIO;
        writeShort(position, ((short)31), Len.Int.MARZO, SignType.NO_SIGN);
        position += Len.MARZO;
        writeShort(position, ((short)30), Len.Int.APRILE, SignType.NO_SIGN);
        position += Len.APRILE;
        writeShort(position, ((short)31), Len.Int.MAGGIO, SignType.NO_SIGN);
        position += Len.MAGGIO;
        writeShort(position, ((short)30), Len.Int.GIUGNO, SignType.NO_SIGN);
        position += Len.GIUGNO;
        writeShort(position, ((short)31), Len.Int.LUGLIO, SignType.NO_SIGN);
        position += Len.LUGLIO;
        writeShort(position, ((short)31), Len.Int.AGOSTO, SignType.NO_SIGN);
        position += Len.AGOSTO;
        writeShort(position, ((short)30), Len.Int.SETTEMBRE, SignType.NO_SIGN);
        position += Len.SETTEMBRE;
        writeShort(position, ((short)31), Len.Int.OTTOBRE, SignType.NO_SIGN);
        position += Len.OTTOBRE;
        writeShort(position, ((short)30), Len.Int.NOVEMBRE, SignType.NO_SIGN);
        position += Len.NOVEMBRE;
        writeShort(position, ((short)31), Len.Int.DICEMBRE, SignType.NO_SIGN);
    }

    public void setGgAnnual(int ggAnnualIdx, short ggAnnual) {
        int position = Pos.idsv0016StrGgAnnual(ggAnnualIdx - 1);
        writeShort(position, ggAnnual, Len.Int.GG_ANNUAL, SignType.NO_SIGN);
    }

    /**Original name: IDSV0016-STR-GG-ANNUAL<br>*/
    public short getGgAnnual(int ggAnnualIdx) {
        int position = Pos.idsv0016StrGgAnnual(ggAnnualIdx - 1);
        return readNumDispUnsignedShort(position, Len.GG_ANNUAL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSV0016_STR_GIORNI_ANNUAL = 1;
        public static final int GENNAIO = IDSV0016_STR_GIORNI_ANNUAL;
        public static final int FEBBRAIO = GENNAIO + Len.GENNAIO;
        public static final int MARZO = FEBBRAIO + Len.FEBBRAIO;
        public static final int APRILE = MARZO + Len.MARZO;
        public static final int MAGGIO = APRILE + Len.APRILE;
        public static final int GIUGNO = MAGGIO + Len.MAGGIO;
        public static final int LUGLIO = GIUGNO + Len.GIUGNO;
        public static final int AGOSTO = LUGLIO + Len.LUGLIO;
        public static final int SETTEMBRE = AGOSTO + Len.AGOSTO;
        public static final int OTTOBRE = SETTEMBRE + Len.SETTEMBRE;
        public static final int NOVEMBRE = OTTOBRE + Len.OTTOBRE;
        public static final int DICEMBRE = NOVEMBRE + Len.NOVEMBRE;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int idsv0016StrGiorniAnnualEle(int idx) {
            return 1 + idx * Len.IDSV0016_STR_GIORNI_ANNUAL_ELE;
        }

        public static int idsv0016StrGgAnnual(int idx) {
            return idsv0016StrGiorniAnnualEle(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GENNAIO = 2;
        public static final int FEBBRAIO = 2;
        public static final int MARZO = 2;
        public static final int APRILE = 2;
        public static final int MAGGIO = 2;
        public static final int GIUGNO = 2;
        public static final int LUGLIO = 2;
        public static final int AGOSTO = 2;
        public static final int SETTEMBRE = 2;
        public static final int OTTOBRE = 2;
        public static final int NOVEMBRE = 2;
        public static final int GG_ANNUAL = 2;
        public static final int IDSV0016_STR_GIORNI_ANNUAL_ELE = GG_ANNUAL;
        public static final int DICEMBRE = 2;
        public static final int IDSV0016_STR_GIORNI_ANNUAL = GENNAIO + FEBBRAIO + MARZO + APRILE + MAGGIO + GIUGNO + LUGLIO + AGOSTO + SETTEMBRE + OTTOBRE + NOVEMBRE + DICEMBRE;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GENNAIO = 2;
            public static final int FEBBRAIO = 2;
            public static final int MARZO = 2;
            public static final int APRILE = 2;
            public static final int MAGGIO = 2;
            public static final int GIUGNO = 2;
            public static final int LUGLIO = 2;
            public static final int AGOSTO = 2;
            public static final int SETTEMBRE = 2;
            public static final int OTTOBRE = 2;
            public static final int NOVEMBRE = 2;
            public static final int DICEMBRE = 2;
            public static final int GG_ANNUAL = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
