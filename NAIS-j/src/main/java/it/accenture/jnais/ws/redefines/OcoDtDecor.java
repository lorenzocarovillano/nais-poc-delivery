package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: OCO-DT-DECOR<br>
 * Variable: OCO-DT-DECOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class OcoDtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public OcoDtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.OCO_DT_DECOR;
    }

    public void setOcoDtDecor(int ocoDtDecor) {
        writeIntAsPacked(Pos.OCO_DT_DECOR, ocoDtDecor, Len.Int.OCO_DT_DECOR);
    }

    public void setOcoDtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.OCO_DT_DECOR, Pos.OCO_DT_DECOR);
    }

    /**Original name: OCO-DT-DECOR<br>*/
    public int getOcoDtDecor() {
        return readPackedAsInt(Pos.OCO_DT_DECOR, Len.Int.OCO_DT_DECOR);
    }

    public byte[] getOcoDtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.OCO_DT_DECOR, Pos.OCO_DT_DECOR);
        return buffer;
    }

    public void setOcoDtDecorNull(String ocoDtDecorNull) {
        writeString(Pos.OCO_DT_DECOR_NULL, ocoDtDecorNull, Len.OCO_DT_DECOR_NULL);
    }

    /**Original name: OCO-DT-DECOR-NULL<br>*/
    public String getOcoDtDecorNull() {
        return readString(Pos.OCO_DT_DECOR_NULL, Len.OCO_DT_DECOR_NULL);
    }

    public String getOcoDtDecorNullFormatted() {
        return Functions.padBlanks(getOcoDtDecorNull(), Len.OCO_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int OCO_DT_DECOR = 1;
        public static final int OCO_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int OCO_DT_DECOR = 5;
        public static final int OCO_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int OCO_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
