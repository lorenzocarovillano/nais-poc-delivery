package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-RISTORNI-CAP<br>
 * Variable: RST-RIS-RISTORNI-CAP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisRistorniCap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisRistorniCap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_RISTORNI_CAP;
    }

    public void setRstRisRistorniCap(AfDecimal rstRisRistorniCap) {
        writeDecimalAsPacked(Pos.RST_RIS_RISTORNI_CAP, rstRisRistorniCap.copy());
    }

    public void setRstRisRistorniCapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_RISTORNI_CAP, Pos.RST_RIS_RISTORNI_CAP);
    }

    /**Original name: RST-RIS-RISTORNI-CAP<br>*/
    public AfDecimal getRstRisRistorniCap() {
        return readPackedAsDecimal(Pos.RST_RIS_RISTORNI_CAP, Len.Int.RST_RIS_RISTORNI_CAP, Len.Fract.RST_RIS_RISTORNI_CAP);
    }

    public byte[] getRstRisRistorniCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_RISTORNI_CAP, Pos.RST_RIS_RISTORNI_CAP);
        return buffer;
    }

    public void setRstRisRistorniCapNull(String rstRisRistorniCapNull) {
        writeString(Pos.RST_RIS_RISTORNI_CAP_NULL, rstRisRistorniCapNull, Len.RST_RIS_RISTORNI_CAP_NULL);
    }

    /**Original name: RST-RIS-RISTORNI-CAP-NULL<br>*/
    public String getRstRisRistorniCapNull() {
        return readString(Pos.RST_RIS_RISTORNI_CAP_NULL, Len.RST_RIS_RISTORNI_CAP_NULL);
    }

    public String getRstRisRistorniCapNullFormatted() {
        return Functions.padBlanks(getRstRisRistorniCapNull(), Len.RST_RIS_RISTORNI_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_RISTORNI_CAP = 1;
        public static final int RST_RIS_RISTORNI_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_RISTORNI_CAP = 8;
        public static final int RST_RIS_RISTORNI_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_RISTORNI_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_RISTORNI_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
