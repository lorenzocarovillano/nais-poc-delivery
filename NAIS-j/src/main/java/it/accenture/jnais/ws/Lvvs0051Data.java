package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvtga1Lvvs0037;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0051<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0051Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0051";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVTGA1
    private Lccvtga1Lvvs0037 lccvtga1 = new Lccvtga1Lvvs0037();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TGA
    private short ixTabTga = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDtgaTabTgaBytes(buffer, position);
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDtgaTabTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtga1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtga1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtga1Lvvs0037.Len.Int.ID_PTF, 0));
        position += Lccvtga1Lvvs0037.Len.ID_PTF;
        lccvtga1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabTga(short ixTabTga) {
        this.ixTabTga = ixTabTga;
    }

    public short getIxTabTga() {
        return this.ixTabTga;
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvtga1Lvvs0037 getLccvtga1() {
        return lccvtga1;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GG_DIFF = 5;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_TAB_TGA = WpolStatus.Len.STATUS + Lccvtga1Lvvs0037.Len.ID_PTF + WtgaDati.Len.DATI;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + DTGA_TAB_TGA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
