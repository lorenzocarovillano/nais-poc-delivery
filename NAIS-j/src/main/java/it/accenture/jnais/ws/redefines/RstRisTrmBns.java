package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-TRM-BNS<br>
 * Variable: RST-RIS-TRM-BNS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisTrmBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisTrmBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_TRM_BNS;
    }

    public void setRstRisTrmBns(AfDecimal rstRisTrmBns) {
        writeDecimalAsPacked(Pos.RST_RIS_TRM_BNS, rstRisTrmBns.copy());
    }

    public void setRstRisTrmBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_TRM_BNS, Pos.RST_RIS_TRM_BNS);
    }

    /**Original name: RST-RIS-TRM-BNS<br>*/
    public AfDecimal getRstRisTrmBns() {
        return readPackedAsDecimal(Pos.RST_RIS_TRM_BNS, Len.Int.RST_RIS_TRM_BNS, Len.Fract.RST_RIS_TRM_BNS);
    }

    public byte[] getRstRisTrmBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_TRM_BNS, Pos.RST_RIS_TRM_BNS);
        return buffer;
    }

    public void setRstRisTrmBnsNull(String rstRisTrmBnsNull) {
        writeString(Pos.RST_RIS_TRM_BNS_NULL, rstRisTrmBnsNull, Len.RST_RIS_TRM_BNS_NULL);
    }

    /**Original name: RST-RIS-TRM-BNS-NULL<br>*/
    public String getRstRisTrmBnsNull() {
        return readString(Pos.RST_RIS_TRM_BNS_NULL, Len.RST_RIS_TRM_BNS_NULL);
    }

    public String getRstRisTrmBnsNullFormatted() {
        return Functions.padBlanks(getRstRisTrmBnsNull(), Len.RST_RIS_TRM_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_TRM_BNS = 1;
        public static final int RST_RIS_TRM_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_TRM_BNS = 8;
        public static final int RST_RIS_TRM_BNS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_TRM_BNS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_TRM_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
