package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-COD-LIVELLO-FLAG<br>
 * Variable: WK-COD-LIVELLO-FLAG from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkCodLivelloFlag {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setWkCodLivelloFlag(char wkCodLivelloFlag) {
        this.value = wkCodLivelloFlag;
    }

    public char getWkCodLivelloFlag() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
