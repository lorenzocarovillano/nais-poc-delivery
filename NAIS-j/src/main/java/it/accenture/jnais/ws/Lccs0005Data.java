package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ivvc0218;
import it.accenture.jnais.copy.MotDeroga;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.NoteOgg;
import it.accenture.jnais.copy.OggDeroga;
import it.accenture.jnais.copy.Rich;
import it.accenture.jnais.copy.RichEst;
import it.accenture.jnais.copy.StatOggWf;
import it.accenture.jnais.copy.StatRichEst;
import it.accenture.jnais.copy.WkVarLvec0202;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.enums.WsTpCausScarto;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0005<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0005Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LCCS0005";
    //Original name: PGM-IDSS0150
    private String pgmIdss0150 = "IDSS0150";
    //Original name: IDBSP040
    private String idbsp040 = "IDBSP040";
    //Original name: LDBS8170
    private String ldbs8170 = "LDBS8170";
    //Original name: IDBSP010
    private String idbsp010 = "IDBSP010";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    /**Original name: WK-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkTabella = "";
    //Original name: WK-ID-OGG
    private int wkIdOgg = 0;
    //Original name: WK-TP-OGG
    private String wkTpOgg = "";
    //Original name: WK-STAT-OGG-WF
    private String wkStatOggWf = "";
    //Original name: WK-STAT-END-WF
    private char wkStatEndWf = Types.SPACE_CHAR;
    //Original name: TP-INDIVIDUALE
    private String tpIndividuale = "IN";
    //Original name: WK-COD-LIV-AUT-SUP
    private int wkCodLivAutSup = DefaultValues.INT_VAL;
    //Original name: WK-COD-GR-AUT-SUP
    private long wkCodGrAutSup = DefaultValues.LONG_VAL;
    //Original name: WK-LIVELLO-DEBUG
    private String wkLivelloDebug = DefaultValues.stringVal(Len.WK_LIVELLO_DEBUG);
    //Original name: WS-TP-OGG
    private WsTpOgg wsTpOgg = new WsTpOgg();
    //Original name: WK-VARIABILI
    private WkVariabili wkVariabili = new WkVariabili();
    //Original name: WS-INDICI
    private WsIndiciLccs0005 wsIndici = new WsIndiciLccs0005();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Gestioned delle Tipo Logiche
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimento wsMovimento = new WsMovimento();
    /**Original name: WS-TP-CAUS-SCARTO<br>
	 * <pre>****************************************************************
	 *     TP_CAUS_SCARTO (Tipo Causale scarto)
	 * ****************************************************************</pre>*/
    private WsTpCausScarto wsTpCausScarto = new WsTpCausScarto();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IO-LCCS0090
    private LinkArea areaIoLccs0090 = new LinkArea();
    //Original name: S234-DATI-INPUT
    private S234DatiInput s234DatiInput = new S234DatiInput();
    //Original name: S234-DATI-OUTPUT
    private S234DatiOutput s234DatiOutput = new S234DatiOutput();
    //Original name: LCCC0025-AREA-COMUNICAZ
    private Lccc0025AreaComunicaz lccc0025AreaComunicaz = new Lccc0025AreaComunicaz();
    //Original name: AREA-IO-LCCS0070
    private AreaIoLccs0070 areaIoLccs0070 = new AreaIoLccs0070();
    //Original name: WK-VAR-LVEC0202
    private WkVarLvec0202 wkVarLvec0202 = new WkVarLvec0202();
    //Original name: AREA-ESTRAZIONE-IB
    private WpagAreaIo areaEstrazioneIb = new WpagAreaIo();
    //Original name: IVVC0218
    private Ivvc0218 ivvc0218 = new Ivvc0218();
    //Original name: RICH
    private Rich rich = new Rich();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: STAT-OGG-WF
    private StatOggWf statOggWf = new StatOggWf();
    //Original name: MOT-DEROGA
    private MotDeroga motDeroga = new MotDeroga();
    //Original name: OGG-DEROGA
    private OggDeroga oggDeroga = new OggDeroga();
    //Original name: STAT-RICH-EST
    private StatRichEst statRichEst = new StatRichEst();
    //Original name: RICH-EST
    private RichEst richEst = new RichEst();
    //Original name: NOTE-OGG
    private NoteOgg noteOgg = new NoteOgg();

    //==== CONSTRUCTORS ====
    public Lccs0005Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getPgmIdss0150() {
        return this.pgmIdss0150;
    }

    public String getPgmIdss0150Formatted() {
        return Functions.padBlanks(getPgmIdss0150(), Len.PGM_IDSS0150);
    }

    public String getIdbsp040() {
        return this.idbsp040;
    }

    public String getIdbsp040Formatted() {
        return Functions.padBlanks(getIdbsp040(), Len.IDBSP040);
    }

    public String getLdbs8170() {
        return this.ldbs8170;
    }

    public String getIdbsp010() {
        return this.idbsp010;
    }

    public String getIdbsp010Formatted() {
        return Functions.padBlanks(getIdbsp010(), Len.IDBSP010);
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setWkIdOgg(int wkIdOgg) {
        this.wkIdOgg = wkIdOgg;
    }

    public int getWkIdOgg() {
        return this.wkIdOgg;
    }

    public void setWkTpOgg(String wkTpOgg) {
        this.wkTpOgg = Functions.subString(wkTpOgg, Len.WK_TP_OGG);
    }

    public String getWkTpOgg() {
        return this.wkTpOgg;
    }

    public void setWkStatOggWf(String wkStatOggWf) {
        this.wkStatOggWf = Functions.subString(wkStatOggWf, Len.WK_STAT_OGG_WF);
    }

    public String getWkStatOggWf() {
        return this.wkStatOggWf;
    }

    public void setWkStatEndWf(char wkStatEndWf) {
        this.wkStatEndWf = wkStatEndWf;
    }

    public char getWkStatEndWf() {
        return this.wkStatEndWf;
    }

    public String getTpIndividuale() {
        return this.tpIndividuale;
    }

    public void setWkCodLivAutSup(int wkCodLivAutSup) {
        this.wkCodLivAutSup = wkCodLivAutSup;
    }

    public int getWkCodLivAutSup() {
        return this.wkCodLivAutSup;
    }

    public void setWkCodGrAutSup(long wkCodGrAutSup) {
        this.wkCodGrAutSup = wkCodGrAutSup;
    }

    public long getWkCodGrAutSup() {
        return this.wkCodGrAutSup;
    }

    public void setWkLivelloDebugFormatted(String wkLivelloDebug) {
        this.wkLivelloDebug = Trunc.toUnsignedNumeric(wkLivelloDebug, Len.WK_LIVELLO_DEBUG);
    }

    public short getWkLivelloDebug() {
        return NumericDisplay.asShort(this.wkLivelloDebug);
    }

    public String getWkLivelloDebugFormatted() {
        return this.wkLivelloDebug;
    }

    public WpagAreaIo getAreaEstrazioneIb() {
        return areaEstrazioneIb;
    }

    public AreaIoLccs0070 getAreaIoLccs0070() {
        return areaIoLccs0070;
    }

    public LinkArea getAreaIoLccs0090() {
        return areaIoLccs0090;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ivvc0218 getIvvc0218() {
        return ivvc0218;
    }

    public Lccc0025AreaComunicaz getLccc0025AreaComunicaz() {
        return lccc0025AreaComunicaz;
    }

    public MotDeroga getMotDeroga() {
        return motDeroga;
    }

    public Movi getMovi() {
        return movi;
    }

    public NoteOgg getNoteOgg() {
        return noteOgg;
    }

    public OggDeroga getOggDeroga() {
        return oggDeroga;
    }

    public Rich getRich() {
        return rich;
    }

    public RichEst getRichEst() {
        return richEst;
    }

    public S234DatiInput getS234DatiInput() {
        return s234DatiInput;
    }

    public S234DatiOutput getS234DatiOutput() {
        return s234DatiOutput;
    }

    public StatOggWf getStatOggWf() {
        return statOggWf;
    }

    public StatRichEst getStatRichEst() {
        return statRichEst;
    }

    public WkVarLvec0202 getWkVarLvec0202() {
        return wkVarLvec0202;
    }

    public WkVariabili getWkVariabili() {
        return wkVariabili;
    }

    public WsIndiciLccs0005 getWsIndici() {
        return wsIndici;
    }

    public WsMovimento getWsMovimento() {
        return wsMovimento;
    }

    public WsTpCausScarto getWsTpCausScarto() {
        return wsTpCausScarto;
    }

    public WsTpOgg getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LIVELLO_DEBUG = 1;
        public static final int WK_STAT_OGG_WF = 2;
        public static final int WK_TP_OGG = 2;
        public static final int WK_TABELLA = 30;
        public static final int IDBSP040 = 8;
        public static final int IDBSP010 = 8;
        public static final int PGM_IDSS0150 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
