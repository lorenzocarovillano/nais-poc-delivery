package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BTC-ID-RICH<br>
 * Variable: BTC-ID-RICH from program IABS0040<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BtcIdRich extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BtcIdRich() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_ID_RICH;
    }

    public void setBtcIdRich(int btcIdRich) {
        writeBinaryInt(Pos.BTC_ID_RICH, btcIdRich);
    }

    public void setBtcIdRichFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Types.INT_SIZE, Pos.BTC_ID_RICH);
    }

    /**Original name: BTC-ID-RICH<br>*/
    public int getBtcIdRich() {
        return readBinaryInt(Pos.BTC_ID_RICH);
    }

    public byte[] getBtcIdRichAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Types.INT_SIZE, Pos.BTC_ID_RICH);
        return buffer;
    }

    public void setBtcIdRichNull(String btcIdRichNull) {
        writeString(Pos.BTC_ID_RICH_NULL, btcIdRichNull, Len.BTC_ID_RICH_NULL);
    }

    /**Original name: BTC-ID-RICH-NULL<br>*/
    public String getBtcIdRichNull() {
        return readString(Pos.BTC_ID_RICH_NULL, Len.BTC_ID_RICH_NULL);
    }

    public String getBtcIdRichNullFormatted() {
        return Functions.padBlanks(getBtcIdRichNull(), Len.BTC_ID_RICH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BTC_ID_RICH = 1;
        public static final int BTC_ID_RICH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BTC_ID_RICH = 4;
        public static final int BTC_ID_RICH_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
