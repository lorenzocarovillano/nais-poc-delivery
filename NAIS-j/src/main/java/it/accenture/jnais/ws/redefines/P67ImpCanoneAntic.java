package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-CANONE-ANTIC<br>
 * Variable: P67-IMP-CANONE-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpCanoneAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpCanoneAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_CANONE_ANTIC;
    }

    public void setP67ImpCanoneAntic(AfDecimal p67ImpCanoneAntic) {
        writeDecimalAsPacked(Pos.P67_IMP_CANONE_ANTIC, p67ImpCanoneAntic.copy());
    }

    public void setP67ImpCanoneAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_CANONE_ANTIC, Pos.P67_IMP_CANONE_ANTIC);
    }

    /**Original name: P67-IMP-CANONE-ANTIC<br>*/
    public AfDecimal getP67ImpCanoneAntic() {
        return readPackedAsDecimal(Pos.P67_IMP_CANONE_ANTIC, Len.Int.P67_IMP_CANONE_ANTIC, Len.Fract.P67_IMP_CANONE_ANTIC);
    }

    public byte[] getP67ImpCanoneAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_CANONE_ANTIC, Pos.P67_IMP_CANONE_ANTIC);
        return buffer;
    }

    public void setP67ImpCanoneAnticNull(String p67ImpCanoneAnticNull) {
        writeString(Pos.P67_IMP_CANONE_ANTIC_NULL, p67ImpCanoneAnticNull, Len.P67_IMP_CANONE_ANTIC_NULL);
    }

    /**Original name: P67-IMP-CANONE-ANTIC-NULL<br>*/
    public String getP67ImpCanoneAnticNull() {
        return readString(Pos.P67_IMP_CANONE_ANTIC_NULL, Len.P67_IMP_CANONE_ANTIC_NULL);
    }

    public String getP67ImpCanoneAnticNullFormatted() {
        return Functions.padBlanks(getP67ImpCanoneAnticNull(), Len.P67_IMP_CANONE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_CANONE_ANTIC = 1;
        public static final int P67_IMP_CANONE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_CANONE_ANTIC = 8;
        public static final int P67_IMP_CANONE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_CANONE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_CANONE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
