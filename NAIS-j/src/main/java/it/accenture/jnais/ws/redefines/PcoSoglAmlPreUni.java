package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-SOGL-AML-PRE-UNI<br>
 * Variable: PCO-SOGL-AML-PRE-UNI from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoSoglAmlPreUni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoSoglAmlPreUni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_SOGL_AML_PRE_UNI;
    }

    public void setPcoSoglAmlPreUni(AfDecimal pcoSoglAmlPreUni) {
        writeDecimalAsPacked(Pos.PCO_SOGL_AML_PRE_UNI, pcoSoglAmlPreUni.copy());
    }

    public void setPcoSoglAmlPreUniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_SOGL_AML_PRE_UNI, Pos.PCO_SOGL_AML_PRE_UNI);
    }

    /**Original name: PCO-SOGL-AML-PRE-UNI<br>*/
    public AfDecimal getPcoSoglAmlPreUni() {
        return readPackedAsDecimal(Pos.PCO_SOGL_AML_PRE_UNI, Len.Int.PCO_SOGL_AML_PRE_UNI, Len.Fract.PCO_SOGL_AML_PRE_UNI);
    }

    public byte[] getPcoSoglAmlPreUniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_SOGL_AML_PRE_UNI, Pos.PCO_SOGL_AML_PRE_UNI);
        return buffer;
    }

    public void initPcoSoglAmlPreUniHighValues() {
        fill(Pos.PCO_SOGL_AML_PRE_UNI, Len.PCO_SOGL_AML_PRE_UNI, Types.HIGH_CHAR_VAL);
    }

    public void setPcoSoglAmlPreUniNull(String pcoSoglAmlPreUniNull) {
        writeString(Pos.PCO_SOGL_AML_PRE_UNI_NULL, pcoSoglAmlPreUniNull, Len.PCO_SOGL_AML_PRE_UNI_NULL);
    }

    /**Original name: PCO-SOGL-AML-PRE-UNI-NULL<br>*/
    public String getPcoSoglAmlPreUniNull() {
        return readString(Pos.PCO_SOGL_AML_PRE_UNI_NULL, Len.PCO_SOGL_AML_PRE_UNI_NULL);
    }

    public String getPcoSoglAmlPreUniNullFormatted() {
        return Functions.padBlanks(getPcoSoglAmlPreUniNull(), Len.PCO_SOGL_AML_PRE_UNI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_SOGL_AML_PRE_UNI = 1;
        public static final int PCO_SOGL_AML_PRE_UNI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_SOGL_AML_PRE_UNI = 8;
        public static final int PCO_SOGL_AML_PRE_UNI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_SOGL_AML_PRE_UNI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_SOGL_AML_PRE_UNI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
