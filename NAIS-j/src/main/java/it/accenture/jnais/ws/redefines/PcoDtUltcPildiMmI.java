package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-PILDI-MM-I<br>
 * Variable: PCO-DT-ULTC-PILDI-MM-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcPildiMmI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcPildiMmI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_PILDI_MM_I;
    }

    public void setPcoDtUltcPildiMmI(int pcoDtUltcPildiMmI) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_PILDI_MM_I, pcoDtUltcPildiMmI, Len.Int.PCO_DT_ULTC_PILDI_MM_I);
    }

    public void setPcoDtUltcPildiMmIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_MM_I, Pos.PCO_DT_ULTC_PILDI_MM_I);
    }

    /**Original name: PCO-DT-ULTC-PILDI-MM-I<br>*/
    public int getPcoDtUltcPildiMmI() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_PILDI_MM_I, Len.Int.PCO_DT_ULTC_PILDI_MM_I);
    }

    public byte[] getPcoDtUltcPildiMmIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_MM_I, Pos.PCO_DT_ULTC_PILDI_MM_I);
        return buffer;
    }

    public void initPcoDtUltcPildiMmIHighValues() {
        fill(Pos.PCO_DT_ULTC_PILDI_MM_I, Len.PCO_DT_ULTC_PILDI_MM_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcPildiMmINull(String pcoDtUltcPildiMmINull) {
        writeString(Pos.PCO_DT_ULTC_PILDI_MM_I_NULL, pcoDtUltcPildiMmINull, Len.PCO_DT_ULTC_PILDI_MM_I_NULL);
    }

    /**Original name: PCO-DT-ULTC-PILDI-MM-I-NULL<br>*/
    public String getPcoDtUltcPildiMmINull() {
        return readString(Pos.PCO_DT_ULTC_PILDI_MM_I_NULL, Len.PCO_DT_ULTC_PILDI_MM_I_NULL);
    }

    public String getPcoDtUltcPildiMmINullFormatted() {
        return Functions.padBlanks(getPcoDtUltcPildiMmINull(), Len.PCO_DT_ULTC_PILDI_MM_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_MM_I = 1;
        public static final int PCO_DT_ULTC_PILDI_MM_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_MM_I = 5;
        public static final int PCO_DT_ULTC_PILDI_MM_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_PILDI_MM_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
