package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WocoTabOggColl;

/**Original name: WOCO-AREA-OGG-COLLG<br>
 * Variable: WOCO-AREA-OGG-COLLG from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WocoAreaOggCollg extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_OGG_COLLG_MAXOCCURS = 60;
    //Original name: WOCO-ELE-OGG-COLLG-MAX
    private short eleOggCollgMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WOCO-TAB-OGG-COLLG
    private WocoTabOggColl[] tabOggCollg = new WocoTabOggColl[TAB_OGG_COLLG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WocoAreaOggCollg() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_AREA_OGG_COLLG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWocoAreaOggCollgBytes(buf);
    }

    public void init() {
        for (int tabOggCollgIdx = 1; tabOggCollgIdx <= TAB_OGG_COLLG_MAXOCCURS; tabOggCollgIdx++) {
            tabOggCollg[tabOggCollgIdx - 1] = new WocoTabOggColl();
        }
    }

    public String getWocoAreaOggCollgFormatted() {
        return MarshalByteExt.bufferToStr(getWocoAreaOggCollgBytes());
    }

    public void setWocoAreaOggCollgBytes(byte[] buffer) {
        setWocoAreaOggCollgBytes(buffer, 1);
    }

    public byte[] getWocoAreaOggCollgBytes() {
        byte[] buffer = new byte[Len.WOCO_AREA_OGG_COLLG];
        return getWocoAreaOggCollgBytes(buffer, 1);
    }

    public void setWocoAreaOggCollgBytes(byte[] buffer, int offset) {
        int position = offset;
        eleOggCollgMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_OGG_COLLG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabOggCollg[idx - 1].setWocoTabOggCollBytes(buffer, position);
                position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
            }
            else {
                tabOggCollg[idx - 1].initWocoTabOggCollSpaces();
                position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
            }
        }
    }

    public byte[] getWocoAreaOggCollgBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleOggCollgMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_OGG_COLLG_MAXOCCURS; idx++) {
            tabOggCollg[idx - 1].getWocoTabOggCollBytes(buffer, position);
            position += WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;
        }
        return buffer;
    }

    public void setEleOggCollgMax(short eleOggCollgMax) {
        this.eleOggCollgMax = eleOggCollgMax;
    }

    public short getEleOggCollgMax() {
        return this.eleOggCollgMax;
    }

    public WocoTabOggColl getTabOggCollg(int idx) {
        return tabOggCollg[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWocoAreaOggCollgBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_OGG_COLLG_MAX = 2;
        public static final int WOCO_AREA_OGG_COLLG = ELE_OGG_COLLG_MAX + WocoAreaOggCollg.TAB_OGG_COLLG_MAXOCCURS * WocoTabOggColl.Len.WOCO_TAB_OGG_COLL;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
