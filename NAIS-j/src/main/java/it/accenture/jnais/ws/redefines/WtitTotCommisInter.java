package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-COMMIS-INTER<br>
 * Variable: WTIT-TOT-COMMIS-INTER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_COMMIS_INTER;
    }

    public void setWtitTotCommisInter(AfDecimal wtitTotCommisInter) {
        writeDecimalAsPacked(Pos.WTIT_TOT_COMMIS_INTER, wtitTotCommisInter.copy());
    }

    public void setWtitTotCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_COMMIS_INTER, Pos.WTIT_TOT_COMMIS_INTER);
    }

    /**Original name: WTIT-TOT-COMMIS-INTER<br>*/
    public AfDecimal getWtitTotCommisInter() {
        return readPackedAsDecimal(Pos.WTIT_TOT_COMMIS_INTER, Len.Int.WTIT_TOT_COMMIS_INTER, Len.Fract.WTIT_TOT_COMMIS_INTER);
    }

    public byte[] getWtitTotCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_COMMIS_INTER, Pos.WTIT_TOT_COMMIS_INTER);
        return buffer;
    }

    public void initWtitTotCommisInterSpaces() {
        fill(Pos.WTIT_TOT_COMMIS_INTER, Len.WTIT_TOT_COMMIS_INTER, Types.SPACE_CHAR);
    }

    public void setWtitTotCommisInterNull(String wtitTotCommisInterNull) {
        writeString(Pos.WTIT_TOT_COMMIS_INTER_NULL, wtitTotCommisInterNull, Len.WTIT_TOT_COMMIS_INTER_NULL);
    }

    /**Original name: WTIT-TOT-COMMIS-INTER-NULL<br>*/
    public String getWtitTotCommisInterNull() {
        return readString(Pos.WTIT_TOT_COMMIS_INTER_NULL, Len.WTIT_TOT_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_COMMIS_INTER = 1;
        public static final int WTIT_TOT_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_COMMIS_INTER = 8;
        public static final int WTIT_TOT_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
