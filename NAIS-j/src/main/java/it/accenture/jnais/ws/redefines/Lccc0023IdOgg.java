package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: LCCC0023-ID-OGG<br>
 * Variable: LCCC0023-ID-OGG from program LCCS0023<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0023IdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0023IdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0023_ID_OGG;
    }

    public void setLccc0023IdOgg(int lccc0023IdOgg) {
        writeIntAsPacked(Pos.LCCC0023_ID_OGG, lccc0023IdOgg, Len.Int.LCCC0023_ID_OGG);
    }

    public void setLccc0023IdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0023_ID_OGG, Pos.LCCC0023_ID_OGG);
    }

    /**Original name: LCCC0023-ID-OGG<br>*/
    public int getLccc0023IdOgg() {
        return readPackedAsInt(Pos.LCCC0023_ID_OGG, Len.Int.LCCC0023_ID_OGG);
    }

    public byte[] getLccc0023IdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0023_ID_OGG, Pos.LCCC0023_ID_OGG);
        return buffer;
    }

    public void initLccc0023IdOggSpaces() {
        fill(Pos.LCCC0023_ID_OGG, Len.LCCC0023_ID_OGG, Types.SPACE_CHAR);
    }

    public void setLccc0023IdOggNull(String lccc0023IdOggNull) {
        writeString(Pos.LCCC0023_ID_OGG_NULL, lccc0023IdOggNull, Len.LCCC0023_ID_OGG_NULL);
    }

    /**Original name: LCCC0023-ID-OGG-NULL<br>*/
    public String getLccc0023IdOggNull() {
        return readString(Pos.LCCC0023_ID_OGG_NULL, Len.LCCC0023_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_OGG = 1;
        public static final int LCCC0023_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0023_ID_OGG = 5;
        public static final int LCCC0023_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0023_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
