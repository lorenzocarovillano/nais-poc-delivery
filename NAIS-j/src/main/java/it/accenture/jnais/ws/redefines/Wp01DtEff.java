package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP01-DT-EFF<br>
 * Variable: WP01-DT-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp01DtEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp01DtEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP01_DT_EFF;
    }

    public void setWp01DtEff(int wp01DtEff) {
        writeIntAsPacked(Pos.WP01_DT_EFF, wp01DtEff, Len.Int.WP01_DT_EFF);
    }

    public void setWp01DtEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP01_DT_EFF, Pos.WP01_DT_EFF);
    }

    /**Original name: WP01-DT-EFF<br>*/
    public int getWp01DtEff() {
        return readPackedAsInt(Pos.WP01_DT_EFF, Len.Int.WP01_DT_EFF);
    }

    public byte[] getWp01DtEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP01_DT_EFF, Pos.WP01_DT_EFF);
        return buffer;
    }

    public void setWp01DtEffNull(String wp01DtEffNull) {
        writeString(Pos.WP01_DT_EFF_NULL, wp01DtEffNull, Len.WP01_DT_EFF_NULL);
    }

    /**Original name: WP01-DT-EFF-NULL<br>*/
    public String getWp01DtEffNull() {
        return readString(Pos.WP01_DT_EFF_NULL, Len.WP01_DT_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP01_DT_EFF = 1;
        public static final int WP01_DT_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP01_DT_EFF = 5;
        public static final int WP01_DT_EFF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP01_DT_EFF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
