package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-CAR-GEST<br>
 * Variable: L3421-IMP-CAR-GEST from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_CAR_GEST;
    }

    public void setL3421ImpCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_CAR_GEST, Pos.L3421_IMP_CAR_GEST);
    }

    /**Original name: L3421-IMP-CAR-GEST<br>*/
    public AfDecimal getL3421ImpCarGest() {
        return readPackedAsDecimal(Pos.L3421_IMP_CAR_GEST, Len.Int.L3421_IMP_CAR_GEST, Len.Fract.L3421_IMP_CAR_GEST);
    }

    public byte[] getL3421ImpCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_CAR_GEST, Pos.L3421_IMP_CAR_GEST);
        return buffer;
    }

    /**Original name: L3421-IMP-CAR-GEST-NULL<br>*/
    public String getL3421ImpCarGestNull() {
        return readString(Pos.L3421_IMP_CAR_GEST_NULL, Len.L3421_IMP_CAR_GEST_NULL);
    }

    public String getL3421ImpCarGestNullFormatted() {
        return Functions.padBlanks(getL3421ImpCarGestNull(), Len.L3421_IMP_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_CAR_GEST = 1;
        public static final int L3421_IMP_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_CAR_GEST = 8;
        public static final int L3421_IMP_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
