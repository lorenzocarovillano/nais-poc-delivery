package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-STST-X-REGIONE<br>
 * Variable: WPCO-STST-X-REGIONE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoStstXRegione extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoStstXRegione() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_STST_X_REGIONE;
    }

    public void setWpcoStstXRegione(int wpcoStstXRegione) {
        writeIntAsPacked(Pos.WPCO_STST_X_REGIONE, wpcoStstXRegione, Len.Int.WPCO_STST_X_REGIONE);
    }

    public void setDpcoStstXRegioneFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_STST_X_REGIONE, Pos.WPCO_STST_X_REGIONE);
    }

    /**Original name: WPCO-STST-X-REGIONE<br>*/
    public int getWpcoStstXRegione() {
        return readPackedAsInt(Pos.WPCO_STST_X_REGIONE, Len.Int.WPCO_STST_X_REGIONE);
    }

    public byte[] getWpcoStstXRegioneAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_STST_X_REGIONE, Pos.WPCO_STST_X_REGIONE);
        return buffer;
    }

    public void setWpcoStstXRegioneNull(String wpcoStstXRegioneNull) {
        writeString(Pos.WPCO_STST_X_REGIONE_NULL, wpcoStstXRegioneNull, Len.WPCO_STST_X_REGIONE_NULL);
    }

    /**Original name: WPCO-STST-X-REGIONE-NULL<br>*/
    public String getWpcoStstXRegioneNull() {
        return readString(Pos.WPCO_STST_X_REGIONE_NULL, Len.WPCO_STST_X_REGIONE_NULL);
    }

    public String getWpcoStstXRegioneNullFormatted() {
        return Functions.padBlanks(getWpcoStstXRegioneNull(), Len.WPCO_STST_X_REGIONE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_STST_X_REGIONE = 1;
        public static final int WPCO_STST_X_REGIONE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_STST_X_REGIONE = 5;
        public static final int WPCO_STST_X_REGIONE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_STST_X_REGIONE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
