package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: D09-MQ-TEMPO-ATTESA-1<br>
 * Variable: D09-MQ-TEMPO-ATTESA-1 from program LDBS6730<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class D09MqTempoAttesa1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public D09MqTempoAttesa1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D09_MQ_TEMPO_ATTESA1;
    }

    public void setD09MqTempoAttesa1(long d09MqTempoAttesa1) {
        writeLongAsPacked(Pos.D09_MQ_TEMPO_ATTESA1, d09MqTempoAttesa1, Len.Int.D09_MQ_TEMPO_ATTESA1);
    }

    public void setD09MqTempoAttesa1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.D09_MQ_TEMPO_ATTESA1, Pos.D09_MQ_TEMPO_ATTESA1);
    }

    /**Original name: D09-MQ-TEMPO-ATTESA-1<br>*/
    public long getD09MqTempoAttesa1() {
        return readPackedAsLong(Pos.D09_MQ_TEMPO_ATTESA1, Len.Int.D09_MQ_TEMPO_ATTESA1);
    }

    public byte[] getD09MqTempoAttesa1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.D09_MQ_TEMPO_ATTESA1, Pos.D09_MQ_TEMPO_ATTESA1);
        return buffer;
    }

    public void setD09MqTempoAttesa1Null(String d09MqTempoAttesa1Null) {
        writeString(Pos.D09_MQ_TEMPO_ATTESA1_NULL, d09MqTempoAttesa1Null, Len.D09_MQ_TEMPO_ATTESA1_NULL);
    }

    /**Original name: D09-MQ-TEMPO-ATTESA-1-NULL<br>*/
    public String getD09MqTempoAttesa1Null() {
        return readString(Pos.D09_MQ_TEMPO_ATTESA1_NULL, Len.D09_MQ_TEMPO_ATTESA1_NULL);
    }

    public String getD09MqTempoAttesa1NullFormatted() {
        return Functions.padBlanks(getD09MqTempoAttesa1Null(), Len.D09_MQ_TEMPO_ATTESA1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int D09_MQ_TEMPO_ATTESA1 = 1;
        public static final int D09_MQ_TEMPO_ATTESA1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int D09_MQ_TEMPO_ATTESA1 = 6;
        public static final int D09_MQ_TEMPO_ATTESA1_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D09_MQ_TEMPO_ATTESA1 = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
