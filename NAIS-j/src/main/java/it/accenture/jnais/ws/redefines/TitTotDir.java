package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-DIR<br>
 * Variable: TIT-TOT-DIR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_DIR;
    }

    public void setTitTotDir(AfDecimal titTotDir) {
        writeDecimalAsPacked(Pos.TIT_TOT_DIR, titTotDir.copy());
    }

    public void setTitTotDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_DIR, Pos.TIT_TOT_DIR);
    }

    /**Original name: TIT-TOT-DIR<br>*/
    public AfDecimal getTitTotDir() {
        return readPackedAsDecimal(Pos.TIT_TOT_DIR, Len.Int.TIT_TOT_DIR, Len.Fract.TIT_TOT_DIR);
    }

    public byte[] getTitTotDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_DIR, Pos.TIT_TOT_DIR);
        return buffer;
    }

    public void setTitTotDirNull(String titTotDirNull) {
        writeString(Pos.TIT_TOT_DIR_NULL, titTotDirNull, Len.TIT_TOT_DIR_NULL);
    }

    /**Original name: TIT-TOT-DIR-NULL<br>*/
    public String getTitTotDirNull() {
        return readString(Pos.TIT_TOT_DIR_NULL, Len.TIT_TOT_DIR_NULL);
    }

    public String getTitTotDirNullFormatted() {
        return Functions.padBlanks(getTitTotDirNull(), Len.TIT_TOT_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_DIR = 1;
        public static final int TIT_TOT_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_DIR = 8;
        public static final int TIT_TOT_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
