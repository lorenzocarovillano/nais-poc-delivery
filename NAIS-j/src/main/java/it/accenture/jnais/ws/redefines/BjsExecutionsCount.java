package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: BJS-EXECUTIONS-COUNT<br>
 * Variable: BJS-EXECUTIONS-COUNT from program IABS0060<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BjsExecutionsCount extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BjsExecutionsCount() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BJS_EXECUTIONS_COUNT;
    }

    public void setBjsExecutionsCount(int bjsExecutionsCount) {
        writeBinaryInt(Pos.BJS_EXECUTIONS_COUNT, bjsExecutionsCount);
    }

    public void setBjsExecutionsCountFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Types.INT_SIZE, Pos.BJS_EXECUTIONS_COUNT);
    }

    /**Original name: BJS-EXECUTIONS-COUNT<br>*/
    public int getBjsExecutionsCount() {
        return readBinaryInt(Pos.BJS_EXECUTIONS_COUNT);
    }

    public byte[] getBjsExecutionsCountAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Types.INT_SIZE, Pos.BJS_EXECUTIONS_COUNT);
        return buffer;
    }

    public void setBjsExecutionsCountNull(String bjsExecutionsCountNull) {
        writeString(Pos.BJS_EXECUTIONS_COUNT_NULL, bjsExecutionsCountNull, Len.BJS_EXECUTIONS_COUNT_NULL);
    }

    /**Original name: BJS-EXECUTIONS-COUNT-NULL<br>*/
    public String getBjsExecutionsCountNull() {
        return readString(Pos.BJS_EXECUTIONS_COUNT_NULL, Len.BJS_EXECUTIONS_COUNT_NULL);
    }

    public String getBjsExecutionsCountNullFormatted() {
        return Functions.padBlanks(getBjsExecutionsCountNull(), Len.BJS_EXECUTIONS_COUNT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BJS_EXECUTIONS_COUNT = 1;
        public static final int BJS_EXECUTIONS_COUNT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BJS_EXECUTIONS_COUNT = 4;
        public static final int BJS_EXECUTIONS_COUNT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
