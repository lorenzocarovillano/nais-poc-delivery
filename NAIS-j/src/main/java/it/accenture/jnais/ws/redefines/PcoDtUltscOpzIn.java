package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTSC-OPZ-IN<br>
 * Variable: PCO-DT-ULTSC-OPZ-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltscOpzIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltscOpzIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTSC_OPZ_IN;
    }

    public void setPcoDtUltscOpzIn(int pcoDtUltscOpzIn) {
        writeIntAsPacked(Pos.PCO_DT_ULTSC_OPZ_IN, pcoDtUltscOpzIn, Len.Int.PCO_DT_ULTSC_OPZ_IN);
    }

    public void setPcoDtUltscOpzInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTSC_OPZ_IN, Pos.PCO_DT_ULTSC_OPZ_IN);
    }

    /**Original name: PCO-DT-ULTSC-OPZ-IN<br>*/
    public int getPcoDtUltscOpzIn() {
        return readPackedAsInt(Pos.PCO_DT_ULTSC_OPZ_IN, Len.Int.PCO_DT_ULTSC_OPZ_IN);
    }

    public byte[] getPcoDtUltscOpzInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTSC_OPZ_IN, Pos.PCO_DT_ULTSC_OPZ_IN);
        return buffer;
    }

    public void initPcoDtUltscOpzInHighValues() {
        fill(Pos.PCO_DT_ULTSC_OPZ_IN, Len.PCO_DT_ULTSC_OPZ_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltscOpzInNull(String pcoDtUltscOpzInNull) {
        writeString(Pos.PCO_DT_ULTSC_OPZ_IN_NULL, pcoDtUltscOpzInNull, Len.PCO_DT_ULTSC_OPZ_IN_NULL);
    }

    /**Original name: PCO-DT-ULTSC-OPZ-IN-NULL<br>*/
    public String getPcoDtUltscOpzInNull() {
        return readString(Pos.PCO_DT_ULTSC_OPZ_IN_NULL, Len.PCO_DT_ULTSC_OPZ_IN_NULL);
    }

    public String getPcoDtUltscOpzInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltscOpzInNull(), Len.PCO_DT_ULTSC_OPZ_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_OPZ_IN = 1;
        public static final int PCO_DT_ULTSC_OPZ_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_OPZ_IN = 5;
        public static final int PCO_DT_ULTSC_OPZ_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTSC_OPZ_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
