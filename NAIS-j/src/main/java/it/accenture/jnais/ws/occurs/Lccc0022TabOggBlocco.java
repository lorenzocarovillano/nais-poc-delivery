package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: LCCC0022-TAB-OGG-BLOCCO<br>
 * Variables: LCCC0022-TAB-OGG-BLOCCO from copybook LCCC0022<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Lccc0022TabOggBlocco {

    //==== PROPERTIES ====
    //Original name: LCCC0022-OGB-ID-OGG-BLOCCO
    private int idOggBlocco = DefaultValues.INT_VAL;
    //Original name: LCCC0022-OGB-ID-OGG-1RIO
    private int idOgg1rio = DefaultValues.INT_VAL;
    //Original name: LCCC0022-OGB-TP-OGG-1RIO
    private String tpOgg1rio = DefaultValues.stringVal(Len.TP_OGG1RIO);
    //Original name: LCCC0022-OGB-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: LCCC0022-OGB-TP-MOVI
    private int tpMovi = DefaultValues.INT_VAL;
    //Original name: LCCC0022-OGB-TP-OGG
    private String tpOgg = DefaultValues.stringVal(Len.TP_OGG);
    //Original name: LCCC0022-OGB-ID-OGG
    private int idOgg = DefaultValues.INT_VAL;
    //Original name: LCCC0022-OGB-DT-EFFETTO
    private int dtEffetto = DefaultValues.INT_VAL;
    //Original name: LCCC0022-OGB-TP-STAT-BLOCCO
    private String tpStatBlocco = DefaultValues.stringVal(Len.TP_STAT_BLOCCO);
    //Original name: LCCC0022-OGB-COD-BLOCCO
    private String codBlocco = DefaultValues.stringVal(Len.COD_BLOCCO);
    //Original name: LCCC0022-OGB-DESCRIZIONE
    private String descrizione = DefaultValues.stringVal(Len.DESCRIZIONE);
    //Original name: LCCC0022-OGB-GRAVITA
    private char gravita = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setTabOggBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        idOggBlocco = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG_BLOCCO, 0);
        position += Len.ID_OGG_BLOCCO;
        idOgg1rio = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG1RIO, 0);
        position += Len.ID_OGG1RIO;
        tpOgg1rio = MarshalByte.readString(buffer, position, Len.TP_OGG1RIO);
        position += Len.TP_OGG1RIO;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        tpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        tpOgg = MarshalByte.readString(buffer, position, Len.TP_OGG);
        position += Len.TP_OGG;
        idOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        dtEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_EFFETTO, 0);
        position += Len.DT_EFFETTO;
        tpStatBlocco = MarshalByte.readString(buffer, position, Len.TP_STAT_BLOCCO);
        position += Len.TP_STAT_BLOCCO;
        codBlocco = MarshalByte.readString(buffer, position, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        descrizione = MarshalByte.readString(buffer, position, Len.DESCRIZIONE);
        position += Len.DESCRIZIONE;
        gravita = MarshalByte.readChar(buffer, position);
    }

    public byte[] getTabOggBloccoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idOggBlocco, Len.Int.ID_OGG_BLOCCO, 0);
        position += Len.ID_OGG_BLOCCO;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg1rio, Len.Int.ID_OGG1RIO, 0);
        position += Len.ID_OGG1RIO;
        MarshalByte.writeString(buffer, position, tpOgg1rio, Len.TP_OGG1RIO);
        position += Len.TP_OGG1RIO;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, tpMovi, Len.Int.TP_MOVI, 0);
        position += Len.TP_MOVI;
        MarshalByte.writeString(buffer, position, tpOgg, Len.TP_OGG);
        position += Len.TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, idOgg, Len.Int.ID_OGG, 0);
        position += Len.ID_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, dtEffetto, Len.Int.DT_EFFETTO, 0);
        position += Len.DT_EFFETTO;
        MarshalByte.writeString(buffer, position, tpStatBlocco, Len.TP_STAT_BLOCCO);
        position += Len.TP_STAT_BLOCCO;
        MarshalByte.writeString(buffer, position, codBlocco, Len.COD_BLOCCO);
        position += Len.COD_BLOCCO;
        MarshalByte.writeString(buffer, position, descrizione, Len.DESCRIZIONE);
        position += Len.DESCRIZIONE;
        MarshalByte.writeChar(buffer, position, gravita);
        return buffer;
    }

    public void initTabOggBloccoSpaces() {
        idOggBlocco = Types.INVALID_INT_VAL;
        idOgg1rio = Types.INVALID_INT_VAL;
        tpOgg1rio = "";
        codCompAnia = Types.INVALID_INT_VAL;
        tpMovi = Types.INVALID_INT_VAL;
        tpOgg = "";
        idOgg = Types.INVALID_INT_VAL;
        dtEffetto = Types.INVALID_INT_VAL;
        tpStatBlocco = "";
        codBlocco = "";
        descrizione = "";
        gravita = Types.SPACE_CHAR;
    }

    public void setIdOggBlocco(int idOggBlocco) {
        this.idOggBlocco = idOggBlocco;
    }

    public int getIdOggBlocco() {
        return this.idOggBlocco;
    }

    public void setIdOgg1rio(int idOgg1rio) {
        this.idOgg1rio = idOgg1rio;
    }

    public int getIdOgg1rio() {
        return this.idOgg1rio;
    }

    public void setTpOgg1rio(String tpOgg1rio) {
        this.tpOgg1rio = Functions.subString(tpOgg1rio, Len.TP_OGG1RIO);
    }

    public String getTpOgg1rio() {
        return this.tpOgg1rio;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setTpMovi(int tpMovi) {
        this.tpMovi = tpMovi;
    }

    public int getTpMovi() {
        return this.tpMovi;
    }

    public void setTpOgg(String tpOgg) {
        this.tpOgg = Functions.subString(tpOgg, Len.TP_OGG);
    }

    public String getTpOgg() {
        return this.tpOgg;
    }

    public void setIdOgg(int idOgg) {
        this.idOgg = idOgg;
    }

    public int getIdOgg() {
        return this.idOgg;
    }

    public void setDtEffetto(int dtEffetto) {
        this.dtEffetto = dtEffetto;
    }

    public int getDtEffetto() {
        return this.dtEffetto;
    }

    public void setTpStatBlocco(String tpStatBlocco) {
        this.tpStatBlocco = Functions.subString(tpStatBlocco, Len.TP_STAT_BLOCCO);
    }

    public String getTpStatBlocco() {
        return this.tpStatBlocco;
    }

    public void setCodBlocco(String codBlocco) {
        this.codBlocco = Functions.subString(codBlocco, Len.COD_BLOCCO);
    }

    public String getCodBlocco() {
        return this.codBlocco;
    }

    public String getLccc0022OgbCodBloccoFormatted() {
        return Functions.padBlanks(getCodBlocco(), Len.COD_BLOCCO);
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = Functions.subString(descrizione, Len.DESCRIZIONE);
    }

    public String getDescrizione() {
        return this.descrizione;
    }

    public void setGravita(char gravita) {
        this.gravita = gravita;
    }

    public char getGravita() {
        return this.gravita;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_OGG_BLOCCO = 5;
        public static final int ID_OGG1RIO = 5;
        public static final int TP_OGG1RIO = 2;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_MOVI = 3;
        public static final int TP_OGG = 2;
        public static final int ID_OGG = 5;
        public static final int DT_EFFETTO = 5;
        public static final int TP_STAT_BLOCCO = 2;
        public static final int COD_BLOCCO = 5;
        public static final int DESCRIZIONE = 100;
        public static final int GRAVITA = 1;
        public static final int TAB_OGG_BLOCCO = ID_OGG_BLOCCO + ID_OGG1RIO + TP_OGG1RIO + COD_COMP_ANIA + TP_MOVI + TP_OGG + ID_OGG + DT_EFFETTO + TP_STAT_BLOCCO + COD_BLOCCO + DESCRIZIONE + GRAVITA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_OGG_BLOCCO = 9;
            public static final int ID_OGG1RIO = 9;
            public static final int COD_COMP_ANIA = 5;
            public static final int TP_MOVI = 5;
            public static final int ID_OGG = 9;
            public static final int DT_EFFETTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
