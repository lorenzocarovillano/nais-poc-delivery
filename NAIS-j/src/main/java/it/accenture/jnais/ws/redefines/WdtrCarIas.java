package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-CAR-IAS<br>
 * Variable: WDTR-CAR-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_CAR_IAS;
    }

    public void setWdtrCarIas(AfDecimal wdtrCarIas) {
        writeDecimalAsPacked(Pos.WDTR_CAR_IAS, wdtrCarIas.copy());
    }

    /**Original name: WDTR-CAR-IAS<br>*/
    public AfDecimal getWdtrCarIas() {
        return readPackedAsDecimal(Pos.WDTR_CAR_IAS, Len.Int.WDTR_CAR_IAS, Len.Fract.WDTR_CAR_IAS);
    }

    public void setWdtrCarIasNull(String wdtrCarIasNull) {
        writeString(Pos.WDTR_CAR_IAS_NULL, wdtrCarIasNull, Len.WDTR_CAR_IAS_NULL);
    }

    /**Original name: WDTR-CAR-IAS-NULL<br>*/
    public String getWdtrCarIasNull() {
        return readString(Pos.WDTR_CAR_IAS_NULL, Len.WDTR_CAR_IAS_NULL);
    }

    public String getWdtrCarIasNullFormatted() {
        return Functions.padBlanks(getWdtrCarIasNull(), Len.WDTR_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_IAS = 1;
        public static final int WDTR_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_CAR_IAS = 8;
        public static final int WDTR_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
