package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PROV-INC<br>
 * Variable: WB03-PROV-INC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03ProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03ProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PROV_INC;
    }

    public void setWb03ProvInc(AfDecimal wb03ProvInc) {
        writeDecimalAsPacked(Pos.WB03_PROV_INC, wb03ProvInc.copy());
    }

    public void setWb03ProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PROV_INC, Pos.WB03_PROV_INC);
    }

    /**Original name: WB03-PROV-INC<br>*/
    public AfDecimal getWb03ProvInc() {
        return readPackedAsDecimal(Pos.WB03_PROV_INC, Len.Int.WB03_PROV_INC, Len.Fract.WB03_PROV_INC);
    }

    public byte[] getWb03ProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PROV_INC, Pos.WB03_PROV_INC);
        return buffer;
    }

    public void setWb03ProvIncNull(String wb03ProvIncNull) {
        writeString(Pos.WB03_PROV_INC_NULL, wb03ProvIncNull, Len.WB03_PROV_INC_NULL);
    }

    /**Original name: WB03-PROV-INC-NULL<br>*/
    public String getWb03ProvIncNull() {
        return readString(Pos.WB03_PROV_INC_NULL, Len.WB03_PROV_INC_NULL);
    }

    public String getWb03ProvIncNullFormatted() {
        return Functions.padBlanks(getWb03ProvIncNull(), Len.WB03_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PROV_INC = 1;
        public static final int WB03_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PROV_INC = 8;
        public static final int WB03_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
