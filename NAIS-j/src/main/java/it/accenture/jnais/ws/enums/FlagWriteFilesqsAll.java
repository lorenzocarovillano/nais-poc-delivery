package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-WRITE-FILESQS-ALL<br>
 * Variable: FLAG-WRITE-FILESQS-ALL from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagWriteFilesqsAll {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagWriteFilesqsAll(char flagWriteFilesqsAll) {
        this.value = flagWriteFilesqsAll;
    }

    public char getFlagWriteFilesqsAll() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setNo() {
        value = NO;
    }
}
