package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPOL-DIR-EMIS<br>
 * Variable: WPOL-DIR-EMIS from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDirEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDirEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DIR_EMIS;
    }

    public void setWpolDirEmis(AfDecimal wpolDirEmis) {
        writeDecimalAsPacked(Pos.WPOL_DIR_EMIS, wpolDirEmis.copy());
    }

    public void setWpolDirEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DIR_EMIS, Pos.WPOL_DIR_EMIS);
    }

    /**Original name: WPOL-DIR-EMIS<br>*/
    public AfDecimal getWpolDirEmis() {
        return readPackedAsDecimal(Pos.WPOL_DIR_EMIS, Len.Int.WPOL_DIR_EMIS, Len.Fract.WPOL_DIR_EMIS);
    }

    public byte[] getWpolDirEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DIR_EMIS, Pos.WPOL_DIR_EMIS);
        return buffer;
    }

    public void setWpolDirEmisNull(String wpolDirEmisNull) {
        writeString(Pos.WPOL_DIR_EMIS_NULL, wpolDirEmisNull, Len.WPOL_DIR_EMIS_NULL);
    }

    /**Original name: WPOL-DIR-EMIS-NULL<br>*/
    public String getWpolDirEmisNull() {
        return readString(Pos.WPOL_DIR_EMIS_NULL, Len.WPOL_DIR_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DIR_EMIS = 1;
        public static final int WPOL_DIR_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DIR_EMIS = 8;
        public static final int WPOL_DIR_EMIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOL_DIR_EMIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DIR_EMIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
