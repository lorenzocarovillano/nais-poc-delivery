package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-DECOR<br>
 * Variable: ADE-DT-DECOR from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_DECOR;
    }

    public void setAdeDtDecor(int adeDtDecor) {
        writeIntAsPacked(Pos.ADE_DT_DECOR, adeDtDecor, Len.Int.ADE_DT_DECOR);
    }

    public void setAdeDtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_DECOR, Pos.ADE_DT_DECOR);
    }

    /**Original name: ADE-DT-DECOR<br>*/
    public int getAdeDtDecor() {
        return readPackedAsInt(Pos.ADE_DT_DECOR, Len.Int.ADE_DT_DECOR);
    }

    public String getAdeDtDecorFormatted() {
        return PicFormatter.display(new PicParams("S9(8)V").setUsage(PicUsage.PACKED)).format(getAdeDtDecor()).toString();
    }

    public byte[] getAdeDtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_DECOR, Pos.ADE_DT_DECOR);
        return buffer;
    }

    public void setAdeDtDecorNull(String adeDtDecorNull) {
        writeString(Pos.ADE_DT_DECOR_NULL, adeDtDecorNull, Len.ADE_DT_DECOR_NULL);
    }

    /**Original name: ADE-DT-DECOR-NULL<br>*/
    public String getAdeDtDecorNull() {
        return readString(Pos.ADE_DT_DECOR_NULL, Len.ADE_DT_DECOR_NULL);
    }

    public String getAdeDtDecorNullFormatted() {
        return Functions.padBlanks(getAdeDtDecorNull(), Len.ADE_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_DECOR = 1;
        public static final int ADE_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_DECOR = 5;
        public static final int ADE_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
