package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CAR-GEST<br>
 * Variable: B03-CAR-GEST from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CAR_GEST;
    }

    public void setB03CarGest(AfDecimal b03CarGest) {
        writeDecimalAsPacked(Pos.B03_CAR_GEST, b03CarGest.copy());
    }

    public void setB03CarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CAR_GEST, Pos.B03_CAR_GEST);
    }

    /**Original name: B03-CAR-GEST<br>*/
    public AfDecimal getB03CarGest() {
        return readPackedAsDecimal(Pos.B03_CAR_GEST, Len.Int.B03_CAR_GEST, Len.Fract.B03_CAR_GEST);
    }

    public byte[] getB03CarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CAR_GEST, Pos.B03_CAR_GEST);
        return buffer;
    }

    public void setB03CarGestNull(String b03CarGestNull) {
        writeString(Pos.B03_CAR_GEST_NULL, b03CarGestNull, Len.B03_CAR_GEST_NULL);
    }

    /**Original name: B03-CAR-GEST-NULL<br>*/
    public String getB03CarGestNull() {
        return readString(Pos.B03_CAR_GEST_NULL, Len.B03_CAR_GEST_NULL);
    }

    public String getB03CarGestNullFormatted() {
        return Functions.padBlanks(getB03CarGestNull(), Len.B03_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CAR_GEST = 1;
        public static final int B03_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CAR_GEST = 8;
        public static final int B03_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
