package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-DT-PROS-BNS-RIC<br>
 * Variable: WPAG-DT-PROS-BNS-RIC from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagDtProsBnsRic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagDtProsBnsRic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_DT_PROS_BNS_RIC;
    }

    public void setWpagDtProsBnsRic(int wpagDtProsBnsRic) {
        writeIntAsPacked(Pos.WPAG_DT_PROS_BNS_RIC, wpagDtProsBnsRic, Len.Int.WPAG_DT_PROS_BNS_RIC);
    }

    public void setWpagDtProsBnsRicFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_DT_PROS_BNS_RIC, Pos.WPAG_DT_PROS_BNS_RIC);
    }

    /**Original name: WPAG-DT-PROS-BNS-RIC<br>*/
    public int getWpagDtProsBnsRic() {
        return readPackedAsInt(Pos.WPAG_DT_PROS_BNS_RIC, Len.Int.WPAG_DT_PROS_BNS_RIC);
    }

    public byte[] getWpagDtProsBnsRicAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_DT_PROS_BNS_RIC, Pos.WPAG_DT_PROS_BNS_RIC);
        return buffer;
    }

    public void initWpagDtProsBnsRicSpaces() {
        fill(Pos.WPAG_DT_PROS_BNS_RIC, Len.WPAG_DT_PROS_BNS_RIC, Types.SPACE_CHAR);
    }

    public void setWpagDtProsBnsRicNull(String wpagDtProsBnsRicNull) {
        writeString(Pos.WPAG_DT_PROS_BNS_RIC_NULL, wpagDtProsBnsRicNull, Len.WPAG_DT_PROS_BNS_RIC_NULL);
    }

    /**Original name: WPAG-DT-PROS-BNS-RIC-NULL<br>*/
    public String getWpagDtProsBnsRicNull() {
        return readString(Pos.WPAG_DT_PROS_BNS_RIC_NULL, Len.WPAG_DT_PROS_BNS_RIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_DT_PROS_BNS_RIC = 1;
        public static final int WPAG_DT_PROS_BNS_RIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_DT_PROS_BNS_RIC = 5;
        public static final int WPAG_DT_PROS_BNS_RIC_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_DT_PROS_BNS_RIC = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
