package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L19-VAL-QUO<br>
 * Variable: L19-VAL-QUO from program IDBSL190<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L19ValQuo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L19ValQuo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L19_VAL_QUO;
    }

    public void setL19ValQuo(AfDecimal l19ValQuo) {
        writeDecimalAsPacked(Pos.L19_VAL_QUO, l19ValQuo.copy());
    }

    public void setL19ValQuoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L19_VAL_QUO, Pos.L19_VAL_QUO);
    }

    /**Original name: L19-VAL-QUO<br>*/
    public AfDecimal getL19ValQuo() {
        return readPackedAsDecimal(Pos.L19_VAL_QUO, Len.Int.L19_VAL_QUO, Len.Fract.L19_VAL_QUO);
    }

    public byte[] getL19ValQuoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L19_VAL_QUO, Pos.L19_VAL_QUO);
        return buffer;
    }

    public void setL19ValQuoNull(String l19ValQuoNull) {
        writeString(Pos.L19_VAL_QUO_NULL, l19ValQuoNull, Len.L19_VAL_QUO_NULL);
    }

    /**Original name: L19-VAL-QUO-NULL<br>*/
    public String getL19ValQuoNull() {
        return readString(Pos.L19_VAL_QUO_NULL, Len.L19_VAL_QUO_NULL);
    }

    public String getL19ValQuoNullFormatted() {
        return Functions.padBlanks(getL19ValQuoNull(), Len.L19_VAL_QUO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L19_VAL_QUO = 1;
        public static final int L19_VAL_QUO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L19_VAL_QUO = 7;
        public static final int L19_VAL_QUO_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L19_VAL_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L19_VAL_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
