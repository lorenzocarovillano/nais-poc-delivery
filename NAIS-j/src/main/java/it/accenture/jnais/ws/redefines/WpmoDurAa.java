package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-DUR-AA<br>
 * Variable: WPMO-DUR-AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoDurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoDurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_DUR_AA;
    }

    public void setWpmoDurAa(int wpmoDurAa) {
        writeIntAsPacked(Pos.WPMO_DUR_AA, wpmoDurAa, Len.Int.WPMO_DUR_AA);
    }

    public void setWpmoDurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_DUR_AA, Pos.WPMO_DUR_AA);
    }

    /**Original name: WPMO-DUR-AA<br>*/
    public int getWpmoDurAa() {
        return readPackedAsInt(Pos.WPMO_DUR_AA, Len.Int.WPMO_DUR_AA);
    }

    public byte[] getWpmoDurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_DUR_AA, Pos.WPMO_DUR_AA);
        return buffer;
    }

    public void initWpmoDurAaSpaces() {
        fill(Pos.WPMO_DUR_AA, Len.WPMO_DUR_AA, Types.SPACE_CHAR);
    }

    public void setWpmoDurAaNull(String wpmoDurAaNull) {
        writeString(Pos.WPMO_DUR_AA_NULL, wpmoDurAaNull, Len.WPMO_DUR_AA_NULL);
    }

    /**Original name: WPMO-DUR-AA-NULL<br>*/
    public String getWpmoDurAaNull() {
        return readString(Pos.WPMO_DUR_AA_NULL, Len.WPMO_DUR_AA_NULL);
    }

    public String getWpmoDurAaNullFormatted() {
        return Functions.padBlanks(getWpmoDurAaNull(), Len.WPMO_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_DUR_AA = 1;
        public static final int WPMO_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_DUR_AA = 3;
        public static final int WPMO_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
