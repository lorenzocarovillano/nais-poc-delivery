package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-UTIL-C-REV<br>
 * Variable: P67-IMP-UTIL-C-REV from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpUtilCRev extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpUtilCRev() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_UTIL_C_REV;
    }

    public void setP67ImpUtilCRev(AfDecimal p67ImpUtilCRev) {
        writeDecimalAsPacked(Pos.P67_IMP_UTIL_C_REV, p67ImpUtilCRev.copy());
    }

    public void setP67ImpUtilCRevFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_UTIL_C_REV, Pos.P67_IMP_UTIL_C_REV);
    }

    /**Original name: P67-IMP-UTIL-C-REV<br>*/
    public AfDecimal getP67ImpUtilCRev() {
        return readPackedAsDecimal(Pos.P67_IMP_UTIL_C_REV, Len.Int.P67_IMP_UTIL_C_REV, Len.Fract.P67_IMP_UTIL_C_REV);
    }

    public byte[] getP67ImpUtilCRevAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_UTIL_C_REV, Pos.P67_IMP_UTIL_C_REV);
        return buffer;
    }

    public void setP67ImpUtilCRevNull(String p67ImpUtilCRevNull) {
        writeString(Pos.P67_IMP_UTIL_C_REV_NULL, p67ImpUtilCRevNull, Len.P67_IMP_UTIL_C_REV_NULL);
    }

    /**Original name: P67-IMP-UTIL-C-REV-NULL<br>*/
    public String getP67ImpUtilCRevNull() {
        return readString(Pos.P67_IMP_UTIL_C_REV_NULL, Len.P67_IMP_UTIL_C_REV_NULL);
    }

    public String getP67ImpUtilCRevNullFormatted() {
        return Functions.padBlanks(getP67ImpUtilCRevNull(), Len.P67_IMP_UTIL_C_REV_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_UTIL_C_REV = 1;
        public static final int P67_IMP_UTIL_C_REV_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_UTIL_C_REV = 8;
        public static final int P67_IMP_UTIL_C_REV_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_UTIL_C_REV = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_UTIL_C_REV = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
