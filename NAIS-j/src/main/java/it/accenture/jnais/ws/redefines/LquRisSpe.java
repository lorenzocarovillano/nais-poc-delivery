package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-RIS-SPE<br>
 * Variable: LQU-RIS-SPE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquRisSpe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquRisSpe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_RIS_SPE;
    }

    public void setLquRisSpe(AfDecimal lquRisSpe) {
        writeDecimalAsPacked(Pos.LQU_RIS_SPE, lquRisSpe.copy());
    }

    public void setLquRisSpeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_RIS_SPE, Pos.LQU_RIS_SPE);
    }

    /**Original name: LQU-RIS-SPE<br>*/
    public AfDecimal getLquRisSpe() {
        return readPackedAsDecimal(Pos.LQU_RIS_SPE, Len.Int.LQU_RIS_SPE, Len.Fract.LQU_RIS_SPE);
    }

    public byte[] getLquRisSpeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_RIS_SPE, Pos.LQU_RIS_SPE);
        return buffer;
    }

    public void setLquRisSpeNull(String lquRisSpeNull) {
        writeString(Pos.LQU_RIS_SPE_NULL, lquRisSpeNull, Len.LQU_RIS_SPE_NULL);
    }

    /**Original name: LQU-RIS-SPE-NULL<br>*/
    public String getLquRisSpeNull() {
        return readString(Pos.LQU_RIS_SPE_NULL, Len.LQU_RIS_SPE_NULL);
    }

    public String getLquRisSpeNullFormatted() {
        return Functions.padBlanks(getLquRisSpeNull(), Len.LQU_RIS_SPE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_RIS_SPE = 1;
        public static final int LQU_RIS_SPE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_RIS_SPE = 8;
        public static final int LQU_RIS_SPE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_RIS_SPE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_RIS_SPE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
