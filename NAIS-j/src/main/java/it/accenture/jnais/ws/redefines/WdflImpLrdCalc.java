package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-LRD-CALC<br>
 * Variable: WDFL-IMP-LRD-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpLrdCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpLrdCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_LRD_CALC;
    }

    public void setWdflImpLrdCalc(AfDecimal wdflImpLrdCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMP_LRD_CALC, wdflImpLrdCalc.copy());
    }

    public void setWdflImpLrdCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_LRD_CALC, Pos.WDFL_IMP_LRD_CALC);
    }

    /**Original name: WDFL-IMP-LRD-CALC<br>*/
    public AfDecimal getWdflImpLrdCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMP_LRD_CALC, Len.Int.WDFL_IMP_LRD_CALC, Len.Fract.WDFL_IMP_LRD_CALC);
    }

    public byte[] getWdflImpLrdCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_LRD_CALC, Pos.WDFL_IMP_LRD_CALC);
        return buffer;
    }

    public void setWdflImpLrdCalcNull(String wdflImpLrdCalcNull) {
        writeString(Pos.WDFL_IMP_LRD_CALC_NULL, wdflImpLrdCalcNull, Len.WDFL_IMP_LRD_CALC_NULL);
    }

    /**Original name: WDFL-IMP-LRD-CALC-NULL<br>*/
    public String getWdflImpLrdCalcNull() {
        return readString(Pos.WDFL_IMP_LRD_CALC_NULL, Len.WDFL_IMP_LRD_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_LRD_CALC = 1;
        public static final int WDFL_IMP_LRD_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_LRD_CALC = 8;
        public static final int WDFL_IMP_LRD_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_LRD_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_LRD_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
