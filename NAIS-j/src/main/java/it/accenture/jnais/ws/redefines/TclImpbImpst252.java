package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-IMPST-252<br>
 * Variable: TCL-IMPB-IMPST-252 from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbImpst252 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbImpst252() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_IMPST252;
    }

    public void setTclImpbImpst252(AfDecimal tclImpbImpst252) {
        writeDecimalAsPacked(Pos.TCL_IMPB_IMPST252, tclImpbImpst252.copy());
    }

    public void setTclImpbImpst252FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_IMPST252, Pos.TCL_IMPB_IMPST252);
    }

    /**Original name: TCL-IMPB-IMPST-252<br>*/
    public AfDecimal getTclImpbImpst252() {
        return readPackedAsDecimal(Pos.TCL_IMPB_IMPST252, Len.Int.TCL_IMPB_IMPST252, Len.Fract.TCL_IMPB_IMPST252);
    }

    public byte[] getTclImpbImpst252AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_IMPST252, Pos.TCL_IMPB_IMPST252);
        return buffer;
    }

    public void setTclImpbImpst252Null(String tclImpbImpst252Null) {
        writeString(Pos.TCL_IMPB_IMPST252_NULL, tclImpbImpst252Null, Len.TCL_IMPB_IMPST252_NULL);
    }

    /**Original name: TCL-IMPB-IMPST-252-NULL<br>*/
    public String getTclImpbImpst252Null() {
        return readString(Pos.TCL_IMPB_IMPST252_NULL, Len.TCL_IMPB_IMPST252_NULL);
    }

    public String getTclImpbImpst252NullFormatted() {
        return Functions.padBlanks(getTclImpbImpst252Null(), Len.TCL_IMPB_IMPST252_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_IMPST252 = 1;
        public static final int TCL_IMPB_IMPST252_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_IMPST252 = 8;
        public static final int TCL_IMPB_IMPST252_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_IMPST252 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_IMPST252 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
