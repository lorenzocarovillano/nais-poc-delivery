package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-RISP-PC<br>
 * Variable: DEQ-RISP-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqRispPc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqRispPc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_RISP_PC;
    }

    public void setDeqRispPc(AfDecimal deqRispPc) {
        writeDecimalAsPacked(Pos.DEQ_RISP_PC, deqRispPc.copy());
    }

    public void setDeqRispPcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_RISP_PC, Pos.DEQ_RISP_PC);
    }

    /**Original name: DEQ-RISP-PC<br>*/
    public AfDecimal getDeqRispPc() {
        return readPackedAsDecimal(Pos.DEQ_RISP_PC, Len.Int.DEQ_RISP_PC, Len.Fract.DEQ_RISP_PC);
    }

    public byte[] getDeqRispPcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_RISP_PC, Pos.DEQ_RISP_PC);
        return buffer;
    }

    public void setDeqRispPcNull(String deqRispPcNull) {
        writeString(Pos.DEQ_RISP_PC_NULL, deqRispPcNull, Len.DEQ_RISP_PC_NULL);
    }

    /**Original name: DEQ-RISP-PC-NULL<br>*/
    public String getDeqRispPcNull() {
        return readString(Pos.DEQ_RISP_PC_NULL, Len.DEQ_RISP_PC_NULL);
    }

    public String getDeqRispPcNullFormatted() {
        return Functions.padBlanks(getDeqRispPcNull(), Len.DEQ_RISP_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_PC = 1;
        public static final int DEQ_RISP_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_RISP_PC = 4;
        public static final int DEQ_RISP_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DEQ_RISP_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
