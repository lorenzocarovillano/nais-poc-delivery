package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: S089-TP-MET-RISC<br>
 * Variable: S089-TP-MET-RISC from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TpMetRisc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TpMetRisc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TP_MET_RISC;
    }

    public void setWlquTpMetRisc(short wlquTpMetRisc) {
        writeShortAsPacked(Pos.S089_TP_MET_RISC, wlquTpMetRisc, Len.Int.WLQU_TP_MET_RISC);
    }

    public void setWlquTpMetRiscFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TP_MET_RISC, Pos.S089_TP_MET_RISC);
    }

    /**Original name: WLQU-TP-MET-RISC<br>*/
    public short getWlquTpMetRisc() {
        return readPackedAsShort(Pos.S089_TP_MET_RISC, Len.Int.WLQU_TP_MET_RISC);
    }

    public byte[] getWlquTpMetRiscAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TP_MET_RISC, Pos.S089_TP_MET_RISC);
        return buffer;
    }

    public void initWlquTpMetRiscSpaces() {
        fill(Pos.S089_TP_MET_RISC, Len.S089_TP_MET_RISC, Types.SPACE_CHAR);
    }

    public void setWlquTpMetRiscNull(String wlquTpMetRiscNull) {
        writeString(Pos.S089_TP_MET_RISC_NULL, wlquTpMetRiscNull, Len.WLQU_TP_MET_RISC_NULL);
    }

    /**Original name: WLQU-TP-MET-RISC-NULL<br>*/
    public String getWlquTpMetRiscNull() {
        return readString(Pos.S089_TP_MET_RISC_NULL, Len.WLQU_TP_MET_RISC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TP_MET_RISC = 1;
        public static final int S089_TP_MET_RISC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TP_MET_RISC = 2;
        public static final int WLQU_TP_MET_RISC_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TP_MET_RISC = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
