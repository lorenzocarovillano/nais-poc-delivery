package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IPers;
import it.accenture.jnais.ws.redefines.A25CodPersSecond;
import it.accenture.jnais.ws.redefines.A25Dt1aAtvt;
import it.accenture.jnais.ws.redefines.A25DtAcqsPers;
import it.accenture.jnais.ws.redefines.A25DtBlocCli;
import it.accenture.jnais.ws.redefines.A25DtDeadPers;
import it.accenture.jnais.ws.redefines.A25DtEndVldtPers;
import it.accenture.jnais.ws.redefines.A25DtNascCli;
import it.accenture.jnais.ws.redefines.A25DtSegnalPartner;
import it.accenture.jnais.ws.redefines.A25IdSegmentazCli;
import it.accenture.jnais.ws.redefines.A25TstamAggmRiga;
import it.accenture.jnais.ws.redefines.A25TstamEndVldt;

/**Original name: PERS<br>
 * Variable: PERS from copybook IDBVA251<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Pers extends SerializableParameter implements IPers {

    //==== PROPERTIES ====
    //Original name: A25-ID-PERS
    private int a25IdPers = DefaultValues.INT_VAL;
    //Original name: A25-TSTAM-INI-VLDT
    private long a25TstamIniVldt = DefaultValues.LONG_VAL;
    //Original name: A25-TSTAM-END-VLDT
    private A25TstamEndVldt a25TstamEndVldt = new A25TstamEndVldt();
    //Original name: A25-COD-PERS
    private long a25CodPers = DefaultValues.LONG_VAL;
    //Original name: A25-RIFTO-RETE
    private String a25RiftoRete = DefaultValues.stringVal(Len.A25_RIFTO_RETE);
    //Original name: A25-COD-PRT-IVA
    private String a25CodPrtIva = DefaultValues.stringVal(Len.A25_COD_PRT_IVA);
    //Original name: A25-IND-PVCY-PRSNL
    private char a25IndPvcyPrsnl = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-PVCY-CMMRC
    private char a25IndPvcyCmmrc = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-PVCY-INDST
    private char a25IndPvcyIndst = DefaultValues.CHAR_VAL;
    //Original name: A25-DT-NASC-CLI
    private A25DtNascCli a25DtNascCli = new A25DtNascCli();
    //Original name: A25-DT-ACQS-PERS
    private A25DtAcqsPers a25DtAcqsPers = new A25DtAcqsPers();
    //Original name: A25-IND-CLI
    private char a25IndCli = DefaultValues.CHAR_VAL;
    //Original name: A25-COD-CMN
    private String a25CodCmn = DefaultValues.stringVal(Len.A25_COD_CMN);
    //Original name: A25-COD-FRM-GIURD
    private String a25CodFrmGiurd = DefaultValues.stringVal(Len.A25_COD_FRM_GIURD);
    //Original name: A25-COD-ENTE-PUBB
    private String a25CodEntePubb = DefaultValues.stringVal(Len.A25_COD_ENTE_PUBB);
    //Original name: A25-DEN-RGN-SOC-LEN
    private short a25DenRgnSocLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-RGN-SOC
    private String a25DenRgnSoc = DefaultValues.stringVal(Len.A25_DEN_RGN_SOC);
    //Original name: A25-DEN-SIG-RGN-SOC-LEN
    private short a25DenSigRgnSocLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-SIG-RGN-SOC
    private String a25DenSigRgnSoc = DefaultValues.stringVal(Len.A25_DEN_SIG_RGN_SOC);
    //Original name: A25-IND-ESE-FISC
    private char a25IndEseFisc = DefaultValues.CHAR_VAL;
    //Original name: A25-COD-STAT-CVL
    private String a25CodStatCvl = DefaultValues.stringVal(Len.A25_COD_STAT_CVL);
    //Original name: A25-DEN-NOME-LEN
    private short a25DenNomeLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-NOME
    private String a25DenNome = DefaultValues.stringVal(Len.A25_DEN_NOME);
    //Original name: A25-DEN-COGN-LEN
    private short a25DenCognLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-COGN
    private String a25DenCogn = DefaultValues.stringVal(Len.A25_DEN_COGN);
    //Original name: A25-COD-FISC
    private String a25CodFisc = DefaultValues.stringVal(Len.A25_COD_FISC);
    //Original name: A25-IND-SEX
    private char a25IndSex = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-CPCT-GIURD
    private char a25IndCpctGiurd = DefaultValues.CHAR_VAL;
    //Original name: A25-IND-PORT-HDCP
    private char a25IndPortHdcp = DefaultValues.CHAR_VAL;
    //Original name: A25-COD-USER-INS
    private String a25CodUserIns = DefaultValues.stringVal(Len.A25_COD_USER_INS);
    //Original name: A25-TSTAM-INS-RIGA
    private long a25TstamInsRiga = DefaultValues.LONG_VAL;
    //Original name: A25-COD-USER-AGGM
    private String a25CodUserAggm = DefaultValues.stringVal(Len.A25_COD_USER_AGGM);
    //Original name: A25-TSTAM-AGGM-RIGA
    private A25TstamAggmRiga a25TstamAggmRiga = new A25TstamAggmRiga();
    //Original name: A25-DEN-CMN-NASC-STRN-LEN
    private short a25DenCmnNascStrnLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: A25-DEN-CMN-NASC-STRN
    private String a25DenCmnNascStrn = DefaultValues.stringVal(Len.A25_DEN_CMN_NASC_STRN);
    //Original name: A25-COD-RAMO-STGR
    private String a25CodRamoStgr = DefaultValues.stringVal(Len.A25_COD_RAMO_STGR);
    //Original name: A25-COD-STGR-ATVT-UIC
    private String a25CodStgrAtvtUic = DefaultValues.stringVal(Len.A25_COD_STGR_ATVT_UIC);
    //Original name: A25-COD-RAMO-ATVT-UIC
    private String a25CodRamoAtvtUic = DefaultValues.stringVal(Len.A25_COD_RAMO_ATVT_UIC);
    //Original name: A25-DT-END-VLDT-PERS
    private A25DtEndVldtPers a25DtEndVldtPers = new A25DtEndVldtPers();
    //Original name: A25-DT-DEAD-PERS
    private A25DtDeadPers a25DtDeadPers = new A25DtDeadPers();
    //Original name: A25-TP-STAT-CLI
    private String a25TpStatCli = DefaultValues.stringVal(Len.A25_TP_STAT_CLI);
    //Original name: A25-DT-BLOC-CLI
    private A25DtBlocCli a25DtBlocCli = new A25DtBlocCli();
    //Original name: A25-COD-PERS-SECOND
    private A25CodPersSecond a25CodPersSecond = new A25CodPersSecond();
    //Original name: A25-ID-SEGMENTAZ-CLI
    private A25IdSegmentazCli a25IdSegmentazCli = new A25IdSegmentazCli();
    //Original name: A25-DT-1A-ATVT
    private A25Dt1aAtvt a25Dt1aAtvt = new A25Dt1aAtvt();
    //Original name: A25-DT-SEGNAL-PARTNER
    private A25DtSegnalPartner a25DtSegnalPartner = new A25DtSegnalPartner();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PERS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setPersBytes(buf);
    }

    public String getPersFormatted() {
        return MarshalByteExt.bufferToStr(getPersBytes());
    }

    public void setPersBytes(byte[] buffer) {
        setPersBytes(buffer, 1);
    }

    public byte[] getPersBytes() {
        byte[] buffer = new byte[Len.PERS];
        return getPersBytes(buffer, 1);
    }

    public void setPersBytes(byte[] buffer, int offset) {
        int position = offset;
        a25IdPers = MarshalByte.readPackedAsInt(buffer, position, Len.Int.A25_ID_PERS, 0);
        position += Len.A25_ID_PERS;
        a25TstamIniVldt = MarshalByte.readPackedAsLong(buffer, position, Len.Int.A25_TSTAM_INI_VLDT, 0);
        position += Len.A25_TSTAM_INI_VLDT;
        a25TstamEndVldt.setA25TstamEndVldtFromBuffer(buffer, position);
        position += A25TstamEndVldt.Len.A25_TSTAM_END_VLDT;
        a25CodPers = MarshalByte.readPackedAsLong(buffer, position, Len.Int.A25_COD_PERS, 0);
        position += Len.A25_COD_PERS;
        a25RiftoRete = MarshalByte.readString(buffer, position, Len.A25_RIFTO_RETE);
        position += Len.A25_RIFTO_RETE;
        a25CodPrtIva = MarshalByte.readString(buffer, position, Len.A25_COD_PRT_IVA);
        position += Len.A25_COD_PRT_IVA;
        a25IndPvcyPrsnl = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndPvcyCmmrc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndPvcyIndst = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25DtNascCli.setA25DtNascCliFromBuffer(buffer, position);
        position += A25DtNascCli.Len.A25_DT_NASC_CLI;
        a25DtAcqsPers.setA25DtAcqsPersFromBuffer(buffer, position);
        position += A25DtAcqsPers.Len.A25_DT_ACQS_PERS;
        a25IndCli = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25CodCmn = MarshalByte.readString(buffer, position, Len.A25_COD_CMN);
        position += Len.A25_COD_CMN;
        a25CodFrmGiurd = MarshalByte.readString(buffer, position, Len.A25_COD_FRM_GIURD);
        position += Len.A25_COD_FRM_GIURD;
        a25CodEntePubb = MarshalByte.readString(buffer, position, Len.A25_COD_ENTE_PUBB);
        position += Len.A25_COD_ENTE_PUBB;
        setA25DenRgnSocVcharBytes(buffer, position);
        position += Len.A25_DEN_RGN_SOC_VCHAR;
        setA25DenSigRgnSocVcharBytes(buffer, position);
        position += Len.A25_DEN_SIG_RGN_SOC_VCHAR;
        a25IndEseFisc = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25CodStatCvl = MarshalByte.readString(buffer, position, Len.A25_COD_STAT_CVL);
        position += Len.A25_COD_STAT_CVL;
        setA25DenNomeVcharBytes(buffer, position);
        position += Len.A25_DEN_NOME_VCHAR;
        setA25DenCognVcharBytes(buffer, position);
        position += Len.A25_DEN_COGN_VCHAR;
        a25CodFisc = MarshalByte.readString(buffer, position, Len.A25_COD_FISC);
        position += Len.A25_COD_FISC;
        a25IndSex = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndCpctGiurd = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25IndPortHdcp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        a25CodUserIns = MarshalByte.readString(buffer, position, Len.A25_COD_USER_INS);
        position += Len.A25_COD_USER_INS;
        a25TstamInsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.A25_TSTAM_INS_RIGA, 0);
        position += Len.A25_TSTAM_INS_RIGA;
        a25CodUserAggm = MarshalByte.readString(buffer, position, Len.A25_COD_USER_AGGM);
        position += Len.A25_COD_USER_AGGM;
        a25TstamAggmRiga.setA25TstamAggmRigaFromBuffer(buffer, position);
        position += A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA;
        setA25DenCmnNascStrnVcharBytes(buffer, position);
        position += Len.A25_DEN_CMN_NASC_STRN_VCHAR;
        a25CodRamoStgr = MarshalByte.readString(buffer, position, Len.A25_COD_RAMO_STGR);
        position += Len.A25_COD_RAMO_STGR;
        a25CodStgrAtvtUic = MarshalByte.readString(buffer, position, Len.A25_COD_STGR_ATVT_UIC);
        position += Len.A25_COD_STGR_ATVT_UIC;
        a25CodRamoAtvtUic = MarshalByte.readString(buffer, position, Len.A25_COD_RAMO_ATVT_UIC);
        position += Len.A25_COD_RAMO_ATVT_UIC;
        a25DtEndVldtPers.setA25DtEndVldtPersFromBuffer(buffer, position);
        position += A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS;
        a25DtDeadPers.setA25DtDeadPersFromBuffer(buffer, position);
        position += A25DtDeadPers.Len.A25_DT_DEAD_PERS;
        a25TpStatCli = MarshalByte.readString(buffer, position, Len.A25_TP_STAT_CLI);
        position += Len.A25_TP_STAT_CLI;
        a25DtBlocCli.setA25DtBlocCliFromBuffer(buffer, position);
        position += A25DtBlocCli.Len.A25_DT_BLOC_CLI;
        a25CodPersSecond.setA25CodPersSecondFromBuffer(buffer, position);
        position += A25CodPersSecond.Len.A25_COD_PERS_SECOND;
        a25IdSegmentazCli.setA25IdSegmentazCliFromBuffer(buffer, position);
        position += A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI;
        a25Dt1aAtvt.setA25Dt1aAtvtFromBuffer(buffer, position);
        position += A25Dt1aAtvt.Len.A25_DT1A_ATVT;
        a25DtSegnalPartner.setA25DtSegnalPartnerFromBuffer(buffer, position);
    }

    public byte[] getPersBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, a25IdPers, Len.Int.A25_ID_PERS, 0);
        position += Len.A25_ID_PERS;
        MarshalByte.writeLongAsPacked(buffer, position, a25TstamIniVldt, Len.Int.A25_TSTAM_INI_VLDT, 0);
        position += Len.A25_TSTAM_INI_VLDT;
        a25TstamEndVldt.getA25TstamEndVldtAsBuffer(buffer, position);
        position += A25TstamEndVldt.Len.A25_TSTAM_END_VLDT;
        MarshalByte.writeLongAsPacked(buffer, position, a25CodPers, Len.Int.A25_COD_PERS, 0);
        position += Len.A25_COD_PERS;
        MarshalByte.writeString(buffer, position, a25RiftoRete, Len.A25_RIFTO_RETE);
        position += Len.A25_RIFTO_RETE;
        MarshalByte.writeString(buffer, position, a25CodPrtIva, Len.A25_COD_PRT_IVA);
        position += Len.A25_COD_PRT_IVA;
        MarshalByte.writeChar(buffer, position, a25IndPvcyPrsnl);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a25IndPvcyCmmrc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a25IndPvcyIndst);
        position += Types.CHAR_SIZE;
        a25DtNascCli.getA25DtNascCliAsBuffer(buffer, position);
        position += A25DtNascCli.Len.A25_DT_NASC_CLI;
        a25DtAcqsPers.getA25DtAcqsPersAsBuffer(buffer, position);
        position += A25DtAcqsPers.Len.A25_DT_ACQS_PERS;
        MarshalByte.writeChar(buffer, position, a25IndCli);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, a25CodCmn, Len.A25_COD_CMN);
        position += Len.A25_COD_CMN;
        MarshalByte.writeString(buffer, position, a25CodFrmGiurd, Len.A25_COD_FRM_GIURD);
        position += Len.A25_COD_FRM_GIURD;
        MarshalByte.writeString(buffer, position, a25CodEntePubb, Len.A25_COD_ENTE_PUBB);
        position += Len.A25_COD_ENTE_PUBB;
        getA25DenRgnSocVcharBytes(buffer, position);
        position += Len.A25_DEN_RGN_SOC_VCHAR;
        getA25DenSigRgnSocVcharBytes(buffer, position);
        position += Len.A25_DEN_SIG_RGN_SOC_VCHAR;
        MarshalByte.writeChar(buffer, position, a25IndEseFisc);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, a25CodStatCvl, Len.A25_COD_STAT_CVL);
        position += Len.A25_COD_STAT_CVL;
        getA25DenNomeVcharBytes(buffer, position);
        position += Len.A25_DEN_NOME_VCHAR;
        getA25DenCognVcharBytes(buffer, position);
        position += Len.A25_DEN_COGN_VCHAR;
        MarshalByte.writeString(buffer, position, a25CodFisc, Len.A25_COD_FISC);
        position += Len.A25_COD_FISC;
        MarshalByte.writeChar(buffer, position, a25IndSex);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a25IndCpctGiurd);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, a25IndPortHdcp);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, a25CodUserIns, Len.A25_COD_USER_INS);
        position += Len.A25_COD_USER_INS;
        MarshalByte.writeLongAsPacked(buffer, position, a25TstamInsRiga, Len.Int.A25_TSTAM_INS_RIGA, 0);
        position += Len.A25_TSTAM_INS_RIGA;
        MarshalByte.writeString(buffer, position, a25CodUserAggm, Len.A25_COD_USER_AGGM);
        position += Len.A25_COD_USER_AGGM;
        a25TstamAggmRiga.getA25TstamAggmRigaAsBuffer(buffer, position);
        position += A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA;
        getA25DenCmnNascStrnVcharBytes(buffer, position);
        position += Len.A25_DEN_CMN_NASC_STRN_VCHAR;
        MarshalByte.writeString(buffer, position, a25CodRamoStgr, Len.A25_COD_RAMO_STGR);
        position += Len.A25_COD_RAMO_STGR;
        MarshalByte.writeString(buffer, position, a25CodStgrAtvtUic, Len.A25_COD_STGR_ATVT_UIC);
        position += Len.A25_COD_STGR_ATVT_UIC;
        MarshalByte.writeString(buffer, position, a25CodRamoAtvtUic, Len.A25_COD_RAMO_ATVT_UIC);
        position += Len.A25_COD_RAMO_ATVT_UIC;
        a25DtEndVldtPers.getA25DtEndVldtPersAsBuffer(buffer, position);
        position += A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS;
        a25DtDeadPers.getA25DtDeadPersAsBuffer(buffer, position);
        position += A25DtDeadPers.Len.A25_DT_DEAD_PERS;
        MarshalByte.writeString(buffer, position, a25TpStatCli, Len.A25_TP_STAT_CLI);
        position += Len.A25_TP_STAT_CLI;
        a25DtBlocCli.getA25DtBlocCliAsBuffer(buffer, position);
        position += A25DtBlocCli.Len.A25_DT_BLOC_CLI;
        a25CodPersSecond.getA25CodPersSecondAsBuffer(buffer, position);
        position += A25CodPersSecond.Len.A25_COD_PERS_SECOND;
        a25IdSegmentazCli.getA25IdSegmentazCliAsBuffer(buffer, position);
        position += A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI;
        a25Dt1aAtvt.getA25Dt1aAtvtAsBuffer(buffer, position);
        position += A25Dt1aAtvt.Len.A25_DT1A_ATVT;
        a25DtSegnalPartner.getA25DtSegnalPartnerAsBuffer(buffer, position);
        return buffer;
    }

    public void setA25IdPers(int a25IdPers) {
        this.a25IdPers = a25IdPers;
    }

    public int getA25IdPers() {
        return this.a25IdPers;
    }

    public void setA25TstamIniVldt(long a25TstamIniVldt) {
        this.a25TstamIniVldt = a25TstamIniVldt;
    }

    public long getA25TstamIniVldt() {
        return this.a25TstamIniVldt;
    }

    public void setA25CodPers(long a25CodPers) {
        this.a25CodPers = a25CodPers;
    }

    public long getA25CodPers() {
        return this.a25CodPers;
    }

    public void setA25RiftoRete(String a25RiftoRete) {
        this.a25RiftoRete = Functions.subString(a25RiftoRete, Len.A25_RIFTO_RETE);
    }

    public String getA25RiftoRete() {
        return this.a25RiftoRete;
    }

    public String getA25RiftoReteFormatted() {
        return Functions.padBlanks(getA25RiftoRete(), Len.A25_RIFTO_RETE);
    }

    public void setA25CodPrtIva(String a25CodPrtIva) {
        this.a25CodPrtIva = Functions.subString(a25CodPrtIva, Len.A25_COD_PRT_IVA);
    }

    public String getA25CodPrtIva() {
        return this.a25CodPrtIva;
    }

    public String getA25CodPrtIvaFormatted() {
        return Functions.padBlanks(getA25CodPrtIva(), Len.A25_COD_PRT_IVA);
    }

    public void setA25IndPvcyPrsnl(char a25IndPvcyPrsnl) {
        this.a25IndPvcyPrsnl = a25IndPvcyPrsnl;
    }

    public char getA25IndPvcyPrsnl() {
        return this.a25IndPvcyPrsnl;
    }

    public void setA25IndPvcyCmmrc(char a25IndPvcyCmmrc) {
        this.a25IndPvcyCmmrc = a25IndPvcyCmmrc;
    }

    public char getA25IndPvcyCmmrc() {
        return this.a25IndPvcyCmmrc;
    }

    public void setA25IndPvcyIndst(char a25IndPvcyIndst) {
        this.a25IndPvcyIndst = a25IndPvcyIndst;
    }

    public char getA25IndPvcyIndst() {
        return this.a25IndPvcyIndst;
    }

    public void setA25IndCli(char a25IndCli) {
        this.a25IndCli = a25IndCli;
    }

    public char getA25IndCli() {
        return this.a25IndCli;
    }

    public void setA25CodCmn(String a25CodCmn) {
        this.a25CodCmn = Functions.subString(a25CodCmn, Len.A25_COD_CMN);
    }

    public String getA25CodCmn() {
        return this.a25CodCmn;
    }

    public String getA25CodCmnFormatted() {
        return Functions.padBlanks(getA25CodCmn(), Len.A25_COD_CMN);
    }

    public void setA25CodFrmGiurd(String a25CodFrmGiurd) {
        this.a25CodFrmGiurd = Functions.subString(a25CodFrmGiurd, Len.A25_COD_FRM_GIURD);
    }

    public String getA25CodFrmGiurd() {
        return this.a25CodFrmGiurd;
    }

    public String getA25CodFrmGiurdFormatted() {
        return Functions.padBlanks(getA25CodFrmGiurd(), Len.A25_COD_FRM_GIURD);
    }

    public void setA25CodEntePubb(String a25CodEntePubb) {
        this.a25CodEntePubb = Functions.subString(a25CodEntePubb, Len.A25_COD_ENTE_PUBB);
    }

    public String getA25CodEntePubb() {
        return this.a25CodEntePubb;
    }

    public String getA25CodEntePubbFormatted() {
        return Functions.padBlanks(getA25CodEntePubb(), Len.A25_COD_ENTE_PUBB);
    }

    public void setA25DenRgnSocVcharFormatted(String data) {
        byte[] buffer = new byte[Len.A25_DEN_RGN_SOC_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.A25_DEN_RGN_SOC_VCHAR);
        setA25DenRgnSocVcharBytes(buffer, 1);
    }

    public String getA25DenRgnSocVcharFormatted() {
        return MarshalByteExt.bufferToStr(getA25DenRgnSocVcharBytes());
    }

    /**Original name: A25-DEN-RGN-SOC-VCHAR<br>*/
    public byte[] getA25DenRgnSocVcharBytes() {
        byte[] buffer = new byte[Len.A25_DEN_RGN_SOC_VCHAR];
        return getA25DenRgnSocVcharBytes(buffer, 1);
    }

    public void setA25DenRgnSocVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenRgnSocLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenRgnSoc = MarshalByte.readString(buffer, position, Len.A25_DEN_RGN_SOC);
    }

    public byte[] getA25DenRgnSocVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, a25DenRgnSocLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, a25DenRgnSoc, Len.A25_DEN_RGN_SOC);
        return buffer;
    }

    public void setA25DenRgnSocLen(short a25DenRgnSocLen) {
        this.a25DenRgnSocLen = a25DenRgnSocLen;
    }

    public short getA25DenRgnSocLen() {
        return this.a25DenRgnSocLen;
    }

    public void setA25DenRgnSoc(String a25DenRgnSoc) {
        this.a25DenRgnSoc = Functions.subString(a25DenRgnSoc, Len.A25_DEN_RGN_SOC);
    }

    public String getA25DenRgnSoc() {
        return this.a25DenRgnSoc;
    }

    public void setA25DenSigRgnSocVcharFormatted(String data) {
        byte[] buffer = new byte[Len.A25_DEN_SIG_RGN_SOC_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.A25_DEN_SIG_RGN_SOC_VCHAR);
        setA25DenSigRgnSocVcharBytes(buffer, 1);
    }

    public String getA25DenSigRgnSocVcharFormatted() {
        return MarshalByteExt.bufferToStr(getA25DenSigRgnSocVcharBytes());
    }

    /**Original name: A25-DEN-SIG-RGN-SOC-VCHAR<br>*/
    public byte[] getA25DenSigRgnSocVcharBytes() {
        byte[] buffer = new byte[Len.A25_DEN_SIG_RGN_SOC_VCHAR];
        return getA25DenSigRgnSocVcharBytes(buffer, 1);
    }

    public void setA25DenSigRgnSocVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenSigRgnSocLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenSigRgnSoc = MarshalByte.readString(buffer, position, Len.A25_DEN_SIG_RGN_SOC);
    }

    public byte[] getA25DenSigRgnSocVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, a25DenSigRgnSocLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, a25DenSigRgnSoc, Len.A25_DEN_SIG_RGN_SOC);
        return buffer;
    }

    public void setA25DenSigRgnSocLen(short a25DenSigRgnSocLen) {
        this.a25DenSigRgnSocLen = a25DenSigRgnSocLen;
    }

    public short getA25DenSigRgnSocLen() {
        return this.a25DenSigRgnSocLen;
    }

    public void setA25DenSigRgnSoc(String a25DenSigRgnSoc) {
        this.a25DenSigRgnSoc = Functions.subString(a25DenSigRgnSoc, Len.A25_DEN_SIG_RGN_SOC);
    }

    public String getA25DenSigRgnSoc() {
        return this.a25DenSigRgnSoc;
    }

    public void setA25IndEseFisc(char a25IndEseFisc) {
        this.a25IndEseFisc = a25IndEseFisc;
    }

    public char getA25IndEseFisc() {
        return this.a25IndEseFisc;
    }

    public void setA25CodStatCvl(String a25CodStatCvl) {
        this.a25CodStatCvl = Functions.subString(a25CodStatCvl, Len.A25_COD_STAT_CVL);
    }

    public String getA25CodStatCvl() {
        return this.a25CodStatCvl;
    }

    public String getA25CodStatCvlFormatted() {
        return Functions.padBlanks(getA25CodStatCvl(), Len.A25_COD_STAT_CVL);
    }

    public void setA25DenNomeVcharFormatted(String data) {
        byte[] buffer = new byte[Len.A25_DEN_NOME_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.A25_DEN_NOME_VCHAR);
        setA25DenNomeVcharBytes(buffer, 1);
    }

    public String getA25DenNomeVcharFormatted() {
        return MarshalByteExt.bufferToStr(getA25DenNomeVcharBytes());
    }

    /**Original name: A25-DEN-NOME-VCHAR<br>*/
    public byte[] getA25DenNomeVcharBytes() {
        byte[] buffer = new byte[Len.A25_DEN_NOME_VCHAR];
        return getA25DenNomeVcharBytes(buffer, 1);
    }

    public void setA25DenNomeVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenNomeLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenNome = MarshalByte.readString(buffer, position, Len.A25_DEN_NOME);
    }

    public byte[] getA25DenNomeVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, a25DenNomeLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, a25DenNome, Len.A25_DEN_NOME);
        return buffer;
    }

    public void setA25DenNomeLen(short a25DenNomeLen) {
        this.a25DenNomeLen = a25DenNomeLen;
    }

    public short getA25DenNomeLen() {
        return this.a25DenNomeLen;
    }

    public void setA25DenNome(String a25DenNome) {
        this.a25DenNome = Functions.subString(a25DenNome, Len.A25_DEN_NOME);
    }

    public String getA25DenNome() {
        return this.a25DenNome;
    }

    public void setA25DenCognVcharFormatted(String data) {
        byte[] buffer = new byte[Len.A25_DEN_COGN_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.A25_DEN_COGN_VCHAR);
        setA25DenCognVcharBytes(buffer, 1);
    }

    public String getA25DenCognVcharFormatted() {
        return MarshalByteExt.bufferToStr(getA25DenCognVcharBytes());
    }

    /**Original name: A25-DEN-COGN-VCHAR<br>*/
    public byte[] getA25DenCognVcharBytes() {
        byte[] buffer = new byte[Len.A25_DEN_COGN_VCHAR];
        return getA25DenCognVcharBytes(buffer, 1);
    }

    public void setA25DenCognVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenCognLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenCogn = MarshalByte.readString(buffer, position, Len.A25_DEN_COGN);
    }

    public byte[] getA25DenCognVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, a25DenCognLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, a25DenCogn, Len.A25_DEN_COGN);
        return buffer;
    }

    public void setA25DenCognLen(short a25DenCognLen) {
        this.a25DenCognLen = a25DenCognLen;
    }

    public short getA25DenCognLen() {
        return this.a25DenCognLen;
    }

    public void setA25DenCogn(String a25DenCogn) {
        this.a25DenCogn = Functions.subString(a25DenCogn, Len.A25_DEN_COGN);
    }

    public String getA25DenCogn() {
        return this.a25DenCogn;
    }

    public void setA25CodFisc(String a25CodFisc) {
        this.a25CodFisc = Functions.subString(a25CodFisc, Len.A25_COD_FISC);
    }

    public String getA25CodFisc() {
        return this.a25CodFisc;
    }

    public String getA25CodFiscFormatted() {
        return Functions.padBlanks(getA25CodFisc(), Len.A25_COD_FISC);
    }

    public void setA25IndSex(char a25IndSex) {
        this.a25IndSex = a25IndSex;
    }

    public char getA25IndSex() {
        return this.a25IndSex;
    }

    public void setA25IndCpctGiurd(char a25IndCpctGiurd) {
        this.a25IndCpctGiurd = a25IndCpctGiurd;
    }

    public char getA25IndCpctGiurd() {
        return this.a25IndCpctGiurd;
    }

    public void setA25IndPortHdcp(char a25IndPortHdcp) {
        this.a25IndPortHdcp = a25IndPortHdcp;
    }

    public char getA25IndPortHdcp() {
        return this.a25IndPortHdcp;
    }

    public void setA25CodUserIns(String a25CodUserIns) {
        this.a25CodUserIns = Functions.subString(a25CodUserIns, Len.A25_COD_USER_INS);
    }

    public String getA25CodUserIns() {
        return this.a25CodUserIns;
    }

    public void setA25TstamInsRiga(long a25TstamInsRiga) {
        this.a25TstamInsRiga = a25TstamInsRiga;
    }

    public long getA25TstamInsRiga() {
        return this.a25TstamInsRiga;
    }

    public void setA25CodUserAggm(String a25CodUserAggm) {
        this.a25CodUserAggm = Functions.subString(a25CodUserAggm, Len.A25_COD_USER_AGGM);
    }

    public String getA25CodUserAggm() {
        return this.a25CodUserAggm;
    }

    public String getA25CodUserAggmFormatted() {
        return Functions.padBlanks(getA25CodUserAggm(), Len.A25_COD_USER_AGGM);
    }

    public void setA25DenCmnNascStrnVcharFormatted(String data) {
        byte[] buffer = new byte[Len.A25_DEN_CMN_NASC_STRN_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.A25_DEN_CMN_NASC_STRN_VCHAR);
        setA25DenCmnNascStrnVcharBytes(buffer, 1);
    }

    public String getA25DenCmnNascStrnVcharFormatted() {
        return MarshalByteExt.bufferToStr(getA25DenCmnNascStrnVcharBytes());
    }

    /**Original name: A25-DEN-CMN-NASC-STRN-VCHAR<br>*/
    public byte[] getA25DenCmnNascStrnVcharBytes() {
        byte[] buffer = new byte[Len.A25_DEN_CMN_NASC_STRN_VCHAR];
        return getA25DenCmnNascStrnVcharBytes(buffer, 1);
    }

    public void setA25DenCmnNascStrnVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        a25DenCmnNascStrnLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        a25DenCmnNascStrn = MarshalByte.readString(buffer, position, Len.A25_DEN_CMN_NASC_STRN);
    }

    public byte[] getA25DenCmnNascStrnVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, a25DenCmnNascStrnLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, a25DenCmnNascStrn, Len.A25_DEN_CMN_NASC_STRN);
        return buffer;
    }

    public void setA25DenCmnNascStrnLen(short a25DenCmnNascStrnLen) {
        this.a25DenCmnNascStrnLen = a25DenCmnNascStrnLen;
    }

    public short getA25DenCmnNascStrnLen() {
        return this.a25DenCmnNascStrnLen;
    }

    public void setA25DenCmnNascStrn(String a25DenCmnNascStrn) {
        this.a25DenCmnNascStrn = Functions.subString(a25DenCmnNascStrn, Len.A25_DEN_CMN_NASC_STRN);
    }

    public String getA25DenCmnNascStrn() {
        return this.a25DenCmnNascStrn;
    }

    public void setA25CodRamoStgr(String a25CodRamoStgr) {
        this.a25CodRamoStgr = Functions.subString(a25CodRamoStgr, Len.A25_COD_RAMO_STGR);
    }

    public String getA25CodRamoStgr() {
        return this.a25CodRamoStgr;
    }

    public String getA25CodRamoStgrFormatted() {
        return Functions.padBlanks(getA25CodRamoStgr(), Len.A25_COD_RAMO_STGR);
    }

    public void setA25CodStgrAtvtUic(String a25CodStgrAtvtUic) {
        this.a25CodStgrAtvtUic = Functions.subString(a25CodStgrAtvtUic, Len.A25_COD_STGR_ATVT_UIC);
    }

    public String getA25CodStgrAtvtUic() {
        return this.a25CodStgrAtvtUic;
    }

    public String getA25CodStgrAtvtUicFormatted() {
        return Functions.padBlanks(getA25CodStgrAtvtUic(), Len.A25_COD_STGR_ATVT_UIC);
    }

    public void setA25CodRamoAtvtUic(String a25CodRamoAtvtUic) {
        this.a25CodRamoAtvtUic = Functions.subString(a25CodRamoAtvtUic, Len.A25_COD_RAMO_ATVT_UIC);
    }

    public String getA25CodRamoAtvtUic() {
        return this.a25CodRamoAtvtUic;
    }

    public String getA25CodRamoAtvtUicFormatted() {
        return Functions.padBlanks(getA25CodRamoAtvtUic(), Len.A25_COD_RAMO_ATVT_UIC);
    }

    public void setA25TpStatCli(String a25TpStatCli) {
        this.a25TpStatCli = Functions.subString(a25TpStatCli, Len.A25_TP_STAT_CLI);
    }

    public String getA25TpStatCli() {
        return this.a25TpStatCli;
    }

    public String getA25TpStatCliFormatted() {
        return Functions.padBlanks(getA25TpStatCli(), Len.A25_TP_STAT_CLI);
    }

    public A25CodPersSecond getA25CodPersSecond() {
        return a25CodPersSecond;
    }

    public A25Dt1aAtvt getA25Dt1aAtvt() {
        return a25Dt1aAtvt;
    }

    public A25DtAcqsPers getA25DtAcqsPers() {
        return a25DtAcqsPers;
    }

    public A25DtBlocCli getA25DtBlocCli() {
        return a25DtBlocCli;
    }

    public A25DtDeadPers getA25DtDeadPers() {
        return a25DtDeadPers;
    }

    public A25DtEndVldtPers getA25DtEndVldtPers() {
        return a25DtEndVldtPers;
    }

    public A25DtNascCli getA25DtNascCli() {
        return a25DtNascCli;
    }

    public A25DtSegnalPartner getA25DtSegnalPartner() {
        return a25DtSegnalPartner;
    }

    public A25IdSegmentazCli getA25IdSegmentazCli() {
        return a25IdSegmentazCli;
    }

    public A25TstamAggmRiga getA25TstamAggmRiga() {
        return a25TstamAggmRiga;
    }

    public A25TstamEndVldt getA25TstamEndVldt() {
        return a25TstamEndVldt;
    }

    @Override
    public String getCodCmn() {
        throw new FieldNotMappedException("codCmn");
    }

    @Override
    public void setCodCmn(String codCmn) {
        throw new FieldNotMappedException("codCmn");
    }

    @Override
    public String getCodCmnObj() {
        return getCodCmn();
    }

    @Override
    public void setCodCmnObj(String codCmnObj) {
        setCodCmn(codCmnObj);
    }

    @Override
    public String getCodEntePubb() {
        throw new FieldNotMappedException("codEntePubb");
    }

    @Override
    public void setCodEntePubb(String codEntePubb) {
        throw new FieldNotMappedException("codEntePubb");
    }

    @Override
    public String getCodEntePubbObj() {
        return getCodEntePubb();
    }

    @Override
    public void setCodEntePubbObj(String codEntePubbObj) {
        setCodEntePubb(codEntePubbObj);
    }

    @Override
    public String getCodFisc() {
        throw new FieldNotMappedException("codFisc");
    }

    @Override
    public void setCodFisc(String codFisc) {
        throw new FieldNotMappedException("codFisc");
    }

    @Override
    public String getCodFiscObj() {
        return getCodFisc();
    }

    @Override
    public void setCodFiscObj(String codFiscObj) {
        setCodFisc(codFiscObj);
    }

    @Override
    public String getCodFrmGiurd() {
        throw new FieldNotMappedException("codFrmGiurd");
    }

    @Override
    public void setCodFrmGiurd(String codFrmGiurd) {
        throw new FieldNotMappedException("codFrmGiurd");
    }

    @Override
    public String getCodFrmGiurdObj() {
        return getCodFrmGiurd();
    }

    @Override
    public void setCodFrmGiurdObj(String codFrmGiurdObj) {
        setCodFrmGiurd(codFrmGiurdObj);
    }

    @Override
    public long getCodPers() {
        throw new FieldNotMappedException("codPers");
    }

    @Override
    public void setCodPers(long codPers) {
        throw new FieldNotMappedException("codPers");
    }

    @Override
    public long getCodPersSecond() {
        throw new FieldNotMappedException("codPersSecond");
    }

    @Override
    public void setCodPersSecond(long codPersSecond) {
        throw new FieldNotMappedException("codPersSecond");
    }

    @Override
    public Long getCodPersSecondObj() {
        return ((Long)getCodPersSecond());
    }

    @Override
    public void setCodPersSecondObj(Long codPersSecondObj) {
        setCodPersSecond(((long)codPersSecondObj));
    }

    @Override
    public String getCodPrtIva() {
        throw new FieldNotMappedException("codPrtIva");
    }

    @Override
    public void setCodPrtIva(String codPrtIva) {
        throw new FieldNotMappedException("codPrtIva");
    }

    @Override
    public String getCodPrtIvaObj() {
        return getCodPrtIva();
    }

    @Override
    public void setCodPrtIvaObj(String codPrtIvaObj) {
        setCodPrtIva(codPrtIvaObj);
    }

    @Override
    public String getCodRamoAtvtUic() {
        throw new FieldNotMappedException("codRamoAtvtUic");
    }

    @Override
    public void setCodRamoAtvtUic(String codRamoAtvtUic) {
        throw new FieldNotMappedException("codRamoAtvtUic");
    }

    @Override
    public String getCodRamoAtvtUicObj() {
        return getCodRamoAtvtUic();
    }

    @Override
    public void setCodRamoAtvtUicObj(String codRamoAtvtUicObj) {
        setCodRamoAtvtUic(codRamoAtvtUicObj);
    }

    @Override
    public String getCodRamoStgr() {
        throw new FieldNotMappedException("codRamoStgr");
    }

    @Override
    public void setCodRamoStgr(String codRamoStgr) {
        throw new FieldNotMappedException("codRamoStgr");
    }

    @Override
    public String getCodRamoStgrObj() {
        return getCodRamoStgr();
    }

    @Override
    public void setCodRamoStgrObj(String codRamoStgrObj) {
        setCodRamoStgr(codRamoStgrObj);
    }

    @Override
    public String getCodStatCvl() {
        throw new FieldNotMappedException("codStatCvl");
    }

    @Override
    public void setCodStatCvl(String codStatCvl) {
        throw new FieldNotMappedException("codStatCvl");
    }

    @Override
    public String getCodStatCvlObj() {
        return getCodStatCvl();
    }

    @Override
    public void setCodStatCvlObj(String codStatCvlObj) {
        setCodStatCvl(codStatCvlObj);
    }

    @Override
    public String getCodStgrAtvtUic() {
        throw new FieldNotMappedException("codStgrAtvtUic");
    }

    @Override
    public void setCodStgrAtvtUic(String codStgrAtvtUic) {
        throw new FieldNotMappedException("codStgrAtvtUic");
    }

    @Override
    public String getCodStgrAtvtUicObj() {
        return getCodStgrAtvtUic();
    }

    @Override
    public void setCodStgrAtvtUicObj(String codStgrAtvtUicObj) {
        setCodStgrAtvtUic(codStgrAtvtUicObj);
    }

    @Override
    public String getCodUserAggm() {
        throw new FieldNotMappedException("codUserAggm");
    }

    @Override
    public void setCodUserAggm(String codUserAggm) {
        throw new FieldNotMappedException("codUserAggm");
    }

    @Override
    public String getCodUserAggmObj() {
        return getCodUserAggm();
    }

    @Override
    public void setCodUserAggmObj(String codUserAggmObj) {
        setCodUserAggm(codUserAggmObj);
    }

    @Override
    public String getCodUserIns() {
        throw new FieldNotMappedException("codUserIns");
    }

    @Override
    public void setCodUserIns(String codUserIns) {
        throw new FieldNotMappedException("codUserIns");
    }

    @Override
    public String getDenCmnNascStrnVchar() {
        throw new FieldNotMappedException("denCmnNascStrnVchar");
    }

    @Override
    public void setDenCmnNascStrnVchar(String denCmnNascStrnVchar) {
        throw new FieldNotMappedException("denCmnNascStrnVchar");
    }

    @Override
    public String getDenCmnNascStrnVcharObj() {
        return getDenCmnNascStrnVchar();
    }

    @Override
    public void setDenCmnNascStrnVcharObj(String denCmnNascStrnVcharObj) {
        setDenCmnNascStrnVchar(denCmnNascStrnVcharObj);
    }

    @Override
    public String getDenCognVchar() {
        throw new FieldNotMappedException("denCognVchar");
    }

    @Override
    public void setDenCognVchar(String denCognVchar) {
        throw new FieldNotMappedException("denCognVchar");
    }

    @Override
    public String getDenCognVcharObj() {
        return getDenCognVchar();
    }

    @Override
    public void setDenCognVcharObj(String denCognVcharObj) {
        setDenCognVchar(denCognVcharObj);
    }

    @Override
    public String getDenNomeVchar() {
        throw new FieldNotMappedException("denNomeVchar");
    }

    @Override
    public void setDenNomeVchar(String denNomeVchar) {
        throw new FieldNotMappedException("denNomeVchar");
    }

    @Override
    public String getDenNomeVcharObj() {
        return getDenNomeVchar();
    }

    @Override
    public void setDenNomeVcharObj(String denNomeVcharObj) {
        setDenNomeVchar(denNomeVcharObj);
    }

    @Override
    public String getDenRgnSocVchar() {
        throw new FieldNotMappedException("denRgnSocVchar");
    }

    @Override
    public void setDenRgnSocVchar(String denRgnSocVchar) {
        throw new FieldNotMappedException("denRgnSocVchar");
    }

    @Override
    public String getDenRgnSocVcharObj() {
        return getDenRgnSocVchar();
    }

    @Override
    public void setDenRgnSocVcharObj(String denRgnSocVcharObj) {
        setDenRgnSocVchar(denRgnSocVcharObj);
    }

    @Override
    public String getDenSigRgnSocVchar() {
        throw new FieldNotMappedException("denSigRgnSocVchar");
    }

    @Override
    public void setDenSigRgnSocVchar(String denSigRgnSocVchar) {
        throw new FieldNotMappedException("denSigRgnSocVchar");
    }

    @Override
    public String getDenSigRgnSocVcharObj() {
        return getDenSigRgnSocVchar();
    }

    @Override
    public void setDenSigRgnSocVcharObj(String denSigRgnSocVcharObj) {
        setDenSigRgnSocVchar(denSigRgnSocVcharObj);
    }

    @Override
    public String getDt1aAtvtDb() {
        throw new FieldNotMappedException("dt1aAtvtDb");
    }

    @Override
    public void setDt1aAtvtDb(String dt1aAtvtDb) {
        throw new FieldNotMappedException("dt1aAtvtDb");
    }

    @Override
    public String getDt1aAtvtDbObj() {
        return getDt1aAtvtDb();
    }

    @Override
    public void setDt1aAtvtDbObj(String dt1aAtvtDbObj) {
        setDt1aAtvtDb(dt1aAtvtDbObj);
    }

    @Override
    public String getDtAcqsPersDb() {
        throw new FieldNotMappedException("dtAcqsPersDb");
    }

    @Override
    public void setDtAcqsPersDb(String dtAcqsPersDb) {
        throw new FieldNotMappedException("dtAcqsPersDb");
    }

    @Override
    public String getDtAcqsPersDbObj() {
        return getDtAcqsPersDb();
    }

    @Override
    public void setDtAcqsPersDbObj(String dtAcqsPersDbObj) {
        setDtAcqsPersDb(dtAcqsPersDbObj);
    }

    @Override
    public String getDtBlocCliDb() {
        throw new FieldNotMappedException("dtBlocCliDb");
    }

    @Override
    public void setDtBlocCliDb(String dtBlocCliDb) {
        throw new FieldNotMappedException("dtBlocCliDb");
    }

    @Override
    public String getDtBlocCliDbObj() {
        return getDtBlocCliDb();
    }

    @Override
    public void setDtBlocCliDbObj(String dtBlocCliDbObj) {
        setDtBlocCliDb(dtBlocCliDbObj);
    }

    @Override
    public String getDtDeadPersDb() {
        throw new FieldNotMappedException("dtDeadPersDb");
    }

    @Override
    public void setDtDeadPersDb(String dtDeadPersDb) {
        throw new FieldNotMappedException("dtDeadPersDb");
    }

    @Override
    public String getDtDeadPersDbObj() {
        return getDtDeadPersDb();
    }

    @Override
    public void setDtDeadPersDbObj(String dtDeadPersDbObj) {
        setDtDeadPersDb(dtDeadPersDbObj);
    }

    @Override
    public String getDtEndVldtPersDb() {
        throw new FieldNotMappedException("dtEndVldtPersDb");
    }

    @Override
    public void setDtEndVldtPersDb(String dtEndVldtPersDb) {
        throw new FieldNotMappedException("dtEndVldtPersDb");
    }

    @Override
    public String getDtEndVldtPersDbObj() {
        return getDtEndVldtPersDb();
    }

    @Override
    public void setDtEndVldtPersDbObj(String dtEndVldtPersDbObj) {
        setDtEndVldtPersDb(dtEndVldtPersDbObj);
    }

    @Override
    public String getDtNascCliDb() {
        throw new FieldNotMappedException("dtNascCliDb");
    }

    @Override
    public void setDtNascCliDb(String dtNascCliDb) {
        throw new FieldNotMappedException("dtNascCliDb");
    }

    @Override
    public String getDtNascCliDbObj() {
        return getDtNascCliDb();
    }

    @Override
    public void setDtNascCliDbObj(String dtNascCliDbObj) {
        setDtNascCliDb(dtNascCliDbObj);
    }

    @Override
    public String getDtSegnalPartnerDb() {
        throw new FieldNotMappedException("dtSegnalPartnerDb");
    }

    @Override
    public void setDtSegnalPartnerDb(String dtSegnalPartnerDb) {
        throw new FieldNotMappedException("dtSegnalPartnerDb");
    }

    @Override
    public String getDtSegnalPartnerDbObj() {
        return getDtSegnalPartnerDb();
    }

    @Override
    public void setDtSegnalPartnerDbObj(String dtSegnalPartnerDbObj) {
        setDtSegnalPartnerDb(dtSegnalPartnerDbObj);
    }

    @Override
    public int getIdPers() {
        throw new FieldNotMappedException("idPers");
    }

    @Override
    public void setIdPers(int idPers) {
        throw new FieldNotMappedException("idPers");
    }

    @Override
    public int getIdSegmentazCli() {
        throw new FieldNotMappedException("idSegmentazCli");
    }

    @Override
    public void setIdSegmentazCli(int idSegmentazCli) {
        throw new FieldNotMappedException("idSegmentazCli");
    }

    @Override
    public Integer getIdSegmentazCliObj() {
        return ((Integer)getIdSegmentazCli());
    }

    @Override
    public void setIdSegmentazCliObj(Integer idSegmentazCliObj) {
        setIdSegmentazCli(((int)idSegmentazCliObj));
    }

    @Override
    public char getIndCli() {
        throw new FieldNotMappedException("indCli");
    }

    @Override
    public void setIndCli(char indCli) {
        throw new FieldNotMappedException("indCli");
    }

    @Override
    public Character getIndCliObj() {
        return ((Character)getIndCli());
    }

    @Override
    public void setIndCliObj(Character indCliObj) {
        setIndCli(((char)indCliObj));
    }

    @Override
    public char getIndCpctGiurd() {
        throw new FieldNotMappedException("indCpctGiurd");
    }

    @Override
    public void setIndCpctGiurd(char indCpctGiurd) {
        throw new FieldNotMappedException("indCpctGiurd");
    }

    @Override
    public Character getIndCpctGiurdObj() {
        return ((Character)getIndCpctGiurd());
    }

    @Override
    public void setIndCpctGiurdObj(Character indCpctGiurdObj) {
        setIndCpctGiurd(((char)indCpctGiurdObj));
    }

    @Override
    public char getIndEseFisc() {
        throw new FieldNotMappedException("indEseFisc");
    }

    @Override
    public void setIndEseFisc(char indEseFisc) {
        throw new FieldNotMappedException("indEseFisc");
    }

    @Override
    public Character getIndEseFiscObj() {
        return ((Character)getIndEseFisc());
    }

    @Override
    public void setIndEseFiscObj(Character indEseFiscObj) {
        setIndEseFisc(((char)indEseFiscObj));
    }

    @Override
    public char getIndPortHdcp() {
        throw new FieldNotMappedException("indPortHdcp");
    }

    @Override
    public void setIndPortHdcp(char indPortHdcp) {
        throw new FieldNotMappedException("indPortHdcp");
    }

    @Override
    public Character getIndPortHdcpObj() {
        return ((Character)getIndPortHdcp());
    }

    @Override
    public void setIndPortHdcpObj(Character indPortHdcpObj) {
        setIndPortHdcp(((char)indPortHdcpObj));
    }

    @Override
    public char getIndPvcyCmmrc() {
        throw new FieldNotMappedException("indPvcyCmmrc");
    }

    @Override
    public void setIndPvcyCmmrc(char indPvcyCmmrc) {
        throw new FieldNotMappedException("indPvcyCmmrc");
    }

    @Override
    public Character getIndPvcyCmmrcObj() {
        return ((Character)getIndPvcyCmmrc());
    }

    @Override
    public void setIndPvcyCmmrcObj(Character indPvcyCmmrcObj) {
        setIndPvcyCmmrc(((char)indPvcyCmmrcObj));
    }

    @Override
    public char getIndPvcyIndst() {
        throw new FieldNotMappedException("indPvcyIndst");
    }

    @Override
    public void setIndPvcyIndst(char indPvcyIndst) {
        throw new FieldNotMappedException("indPvcyIndst");
    }

    @Override
    public Character getIndPvcyIndstObj() {
        return ((Character)getIndPvcyIndst());
    }

    @Override
    public void setIndPvcyIndstObj(Character indPvcyIndstObj) {
        setIndPvcyIndst(((char)indPvcyIndstObj));
    }

    @Override
    public char getIndPvcyPrsnl() {
        throw new FieldNotMappedException("indPvcyPrsnl");
    }

    @Override
    public void setIndPvcyPrsnl(char indPvcyPrsnl) {
        throw new FieldNotMappedException("indPvcyPrsnl");
    }

    @Override
    public Character getIndPvcyPrsnlObj() {
        return ((Character)getIndPvcyPrsnl());
    }

    @Override
    public void setIndPvcyPrsnlObj(Character indPvcyPrsnlObj) {
        setIndPvcyPrsnl(((char)indPvcyPrsnlObj));
    }

    @Override
    public char getIndSex() {
        throw new FieldNotMappedException("indSex");
    }

    @Override
    public void setIndSex(char indSex) {
        throw new FieldNotMappedException("indSex");
    }

    @Override
    public Character getIndSexObj() {
        return ((Character)getIndSex());
    }

    @Override
    public void setIndSexObj(Character indSexObj) {
        setIndSex(((char)indSexObj));
    }

    @Override
    public String getRiftoRete() {
        throw new FieldNotMappedException("riftoRete");
    }

    @Override
    public void setRiftoRete(String riftoRete) {
        throw new FieldNotMappedException("riftoRete");
    }

    @Override
    public String getRiftoReteObj() {
        return getRiftoRete();
    }

    @Override
    public void setRiftoReteObj(String riftoReteObj) {
        setRiftoRete(riftoReteObj);
    }

    @Override
    public String getTpStatCli() {
        throw new FieldNotMappedException("tpStatCli");
    }

    @Override
    public void setTpStatCli(String tpStatCli) {
        throw new FieldNotMappedException("tpStatCli");
    }

    @Override
    public String getTpStatCliObj() {
        return getTpStatCli();
    }

    @Override
    public void setTpStatCliObj(String tpStatCliObj) {
        setTpStatCli(tpStatCliObj);
    }

    @Override
    public long getTstamAggmRiga() {
        throw new FieldNotMappedException("tstamAggmRiga");
    }

    @Override
    public void setTstamAggmRiga(long tstamAggmRiga) {
        throw new FieldNotMappedException("tstamAggmRiga");
    }

    @Override
    public Long getTstamAggmRigaObj() {
        return ((Long)getTstamAggmRiga());
    }

    @Override
    public void setTstamAggmRigaObj(Long tstamAggmRigaObj) {
        setTstamAggmRiga(((long)tstamAggmRigaObj));
    }

    @Override
    public long getTstamEndVldt() {
        throw new FieldNotMappedException("tstamEndVldt");
    }

    @Override
    public void setTstamEndVldt(long tstamEndVldt) {
        throw new FieldNotMappedException("tstamEndVldt");
    }

    @Override
    public Long getTstamEndVldtObj() {
        return ((Long)getTstamEndVldt());
    }

    @Override
    public void setTstamEndVldtObj(Long tstamEndVldtObj) {
        setTstamEndVldt(((long)tstamEndVldtObj));
    }

    @Override
    public long getTstamIniVldt() {
        throw new FieldNotMappedException("tstamIniVldt");
    }

    @Override
    public void setTstamIniVldt(long tstamIniVldt) {
        throw new FieldNotMappedException("tstamIniVldt");
    }

    @Override
    public long getTstamInsRiga() {
        throw new FieldNotMappedException("tstamInsRiga");
    }

    @Override
    public void setTstamInsRiga(long tstamInsRiga) {
        throw new FieldNotMappedException("tstamInsRiga");
    }

    @Override
    public byte[] serialize() {
        return getPersBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_ID_PERS = 5;
        public static final int A25_TSTAM_INI_VLDT = 10;
        public static final int A25_COD_PERS = 6;
        public static final int A25_RIFTO_RETE = 20;
        public static final int A25_COD_PRT_IVA = 11;
        public static final int A25_IND_PVCY_PRSNL = 1;
        public static final int A25_IND_PVCY_CMMRC = 1;
        public static final int A25_IND_PVCY_INDST = 1;
        public static final int A25_IND_CLI = 1;
        public static final int A25_COD_CMN = 4;
        public static final int A25_COD_FRM_GIURD = 4;
        public static final int A25_COD_ENTE_PUBB = 4;
        public static final int A25_DEN_RGN_SOC_LEN = 2;
        public static final int A25_DEN_RGN_SOC = 100;
        public static final int A25_DEN_RGN_SOC_VCHAR = A25_DEN_RGN_SOC_LEN + A25_DEN_RGN_SOC;
        public static final int A25_DEN_SIG_RGN_SOC_LEN = 2;
        public static final int A25_DEN_SIG_RGN_SOC = 100;
        public static final int A25_DEN_SIG_RGN_SOC_VCHAR = A25_DEN_SIG_RGN_SOC_LEN + A25_DEN_SIG_RGN_SOC;
        public static final int A25_IND_ESE_FISC = 1;
        public static final int A25_COD_STAT_CVL = 4;
        public static final int A25_DEN_NOME_LEN = 2;
        public static final int A25_DEN_NOME = 100;
        public static final int A25_DEN_NOME_VCHAR = A25_DEN_NOME_LEN + A25_DEN_NOME;
        public static final int A25_DEN_COGN_LEN = 2;
        public static final int A25_DEN_COGN = 100;
        public static final int A25_DEN_COGN_VCHAR = A25_DEN_COGN_LEN + A25_DEN_COGN;
        public static final int A25_COD_FISC = 16;
        public static final int A25_IND_SEX = 1;
        public static final int A25_IND_CPCT_GIURD = 1;
        public static final int A25_IND_PORT_HDCP = 1;
        public static final int A25_COD_USER_INS = 20;
        public static final int A25_TSTAM_INS_RIGA = 10;
        public static final int A25_COD_USER_AGGM = 20;
        public static final int A25_DEN_CMN_NASC_STRN_LEN = 2;
        public static final int A25_DEN_CMN_NASC_STRN = 100;
        public static final int A25_DEN_CMN_NASC_STRN_VCHAR = A25_DEN_CMN_NASC_STRN_LEN + A25_DEN_CMN_NASC_STRN;
        public static final int A25_COD_RAMO_STGR = 4;
        public static final int A25_COD_STGR_ATVT_UIC = 4;
        public static final int A25_COD_RAMO_ATVT_UIC = 4;
        public static final int A25_TP_STAT_CLI = 2;
        public static final int PERS = A25_ID_PERS + A25_TSTAM_INI_VLDT + A25TstamEndVldt.Len.A25_TSTAM_END_VLDT + A25_COD_PERS + A25_RIFTO_RETE + A25_COD_PRT_IVA + A25_IND_PVCY_PRSNL + A25_IND_PVCY_CMMRC + A25_IND_PVCY_INDST + A25DtNascCli.Len.A25_DT_NASC_CLI + A25DtAcqsPers.Len.A25_DT_ACQS_PERS + A25_IND_CLI + A25_COD_CMN + A25_COD_FRM_GIURD + A25_COD_ENTE_PUBB + A25_DEN_RGN_SOC_VCHAR + A25_DEN_SIG_RGN_SOC_VCHAR + A25_IND_ESE_FISC + A25_COD_STAT_CVL + A25_DEN_NOME_VCHAR + A25_DEN_COGN_VCHAR + A25_COD_FISC + A25_IND_SEX + A25_IND_CPCT_GIURD + A25_IND_PORT_HDCP + A25_COD_USER_INS + A25_TSTAM_INS_RIGA + A25_COD_USER_AGGM + A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA + A25_DEN_CMN_NASC_STRN_VCHAR + A25_COD_RAMO_STGR + A25_COD_STGR_ATVT_UIC + A25_COD_RAMO_ATVT_UIC + A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS + A25DtDeadPers.Len.A25_DT_DEAD_PERS + A25_TP_STAT_CLI + A25DtBlocCli.Len.A25_DT_BLOC_CLI + A25CodPersSecond.Len.A25_COD_PERS_SECOND + A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI + A25Dt1aAtvt.Len.A25_DT1A_ATVT + A25DtSegnalPartner.Len.A25_DT_SEGNAL_PARTNER;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_ID_PERS = 9;
            public static final int A25_TSTAM_INI_VLDT = 18;
            public static final int A25_COD_PERS = 11;
            public static final int A25_TSTAM_INS_RIGA = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
