package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-VIS-ANTIC<br>
 * Variable: WDFA-IMPB-VIS-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbVisAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbVisAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_VIS_ANTIC;
    }

    public void setWdfaImpbVisAntic(AfDecimal wdfaImpbVisAntic) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_VIS_ANTIC, wdfaImpbVisAntic.copy());
    }

    public void setWdfaImpbVisAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_VIS_ANTIC, Pos.WDFA_IMPB_VIS_ANTIC);
    }

    /**Original name: WDFA-IMPB-VIS-ANTIC<br>*/
    public AfDecimal getWdfaImpbVisAntic() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_VIS_ANTIC, Len.Int.WDFA_IMPB_VIS_ANTIC, Len.Fract.WDFA_IMPB_VIS_ANTIC);
    }

    public byte[] getWdfaImpbVisAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_VIS_ANTIC, Pos.WDFA_IMPB_VIS_ANTIC);
        return buffer;
    }

    public void setWdfaImpbVisAnticNull(String wdfaImpbVisAnticNull) {
        writeString(Pos.WDFA_IMPB_VIS_ANTIC_NULL, wdfaImpbVisAnticNull, Len.WDFA_IMPB_VIS_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPB-VIS-ANTIC-NULL<br>*/
    public String getWdfaImpbVisAnticNull() {
        return readString(Pos.WDFA_IMPB_VIS_ANTIC_NULL, Len.WDFA_IMPB_VIS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_VIS_ANTIC = 1;
        public static final int WDFA_IMPB_VIS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_VIS_ANTIC = 8;
        public static final int WDFA_IMPB_VIS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_VIS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_VIS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
