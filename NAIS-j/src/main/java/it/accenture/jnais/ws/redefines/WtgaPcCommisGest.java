package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PC-COMMIS-GEST<br>
 * Variable: WTGA-PC-COMMIS-GEST from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPcCommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPcCommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PC_COMMIS_GEST;
    }

    public void setWtgaPcCommisGest(AfDecimal wtgaPcCommisGest) {
        writeDecimalAsPacked(Pos.WTGA_PC_COMMIS_GEST, wtgaPcCommisGest.copy());
    }

    public void setWtgaPcCommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PC_COMMIS_GEST, Pos.WTGA_PC_COMMIS_GEST);
    }

    /**Original name: WTGA-PC-COMMIS-GEST<br>*/
    public AfDecimal getWtgaPcCommisGest() {
        return readPackedAsDecimal(Pos.WTGA_PC_COMMIS_GEST, Len.Int.WTGA_PC_COMMIS_GEST, Len.Fract.WTGA_PC_COMMIS_GEST);
    }

    public byte[] getWtgaPcCommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PC_COMMIS_GEST, Pos.WTGA_PC_COMMIS_GEST);
        return buffer;
    }

    public void initWtgaPcCommisGestSpaces() {
        fill(Pos.WTGA_PC_COMMIS_GEST, Len.WTGA_PC_COMMIS_GEST, Types.SPACE_CHAR);
    }

    public void setWtgaPcCommisGestNull(String wtgaPcCommisGestNull) {
        writeString(Pos.WTGA_PC_COMMIS_GEST_NULL, wtgaPcCommisGestNull, Len.WTGA_PC_COMMIS_GEST_NULL);
    }

    /**Original name: WTGA-PC-COMMIS-GEST-NULL<br>*/
    public String getWtgaPcCommisGestNull() {
        return readString(Pos.WTGA_PC_COMMIS_GEST_NULL, Len.WTGA_PC_COMMIS_GEST_NULL);
    }

    public String getWtgaPcCommisGestNullFormatted() {
        return Functions.padBlanks(getWtgaPcCommisGestNull(), Len.WTGA_PC_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PC_COMMIS_GEST = 1;
        public static final int WTGA_PC_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PC_COMMIS_GEST = 4;
        public static final int WTGA_PC_COMMIS_GEST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
