package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-1O-PER-GG<br>
 * Variable: W-B03-DUR-1O-PER-GG from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03Dur1oPerGgLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03Dur1oPerGgLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR1O_PER_GG;
    }

    public void setwB03Dur1oPerGg(int wB03Dur1oPerGg) {
        writeIntAsPacked(Pos.W_B03_DUR1O_PER_GG, wB03Dur1oPerGg, Len.Int.W_B03_DUR1O_PER_GG);
    }

    /**Original name: W-B03-DUR-1O-PER-GG<br>*/
    public int getwB03Dur1oPerGg() {
        return readPackedAsInt(Pos.W_B03_DUR1O_PER_GG, Len.Int.W_B03_DUR1O_PER_GG);
    }

    public byte[] getwB03Dur1oPerGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR1O_PER_GG, Pos.W_B03_DUR1O_PER_GG);
        return buffer;
    }

    public void setwB03Dur1oPerGgNull(String wB03Dur1oPerGgNull) {
        writeString(Pos.W_B03_DUR1O_PER_GG_NULL, wB03Dur1oPerGgNull, Len.W_B03_DUR1O_PER_GG_NULL);
    }

    /**Original name: W-B03-DUR-1O-PER-GG-NULL<br>*/
    public String getwB03Dur1oPerGgNull() {
        return readString(Pos.W_B03_DUR1O_PER_GG_NULL, Len.W_B03_DUR1O_PER_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR1O_PER_GG = 1;
        public static final int W_B03_DUR1O_PER_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR1O_PER_GG = 3;
        public static final int W_B03_DUR1O_PER_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR1O_PER_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
