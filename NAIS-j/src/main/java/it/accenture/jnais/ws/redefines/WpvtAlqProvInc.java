package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPVT-ALQ-PROV-INC<br>
 * Variable: WPVT-ALQ-PROV-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpvtAlqProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpvtAlqProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPVT_ALQ_PROV_INC;
    }

    public void setWpvtAlqProvInc(AfDecimal wpvtAlqProvInc) {
        writeDecimalAsPacked(Pos.WPVT_ALQ_PROV_INC, wpvtAlqProvInc.copy());
    }

    public void setWpvtAlqProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPVT_ALQ_PROV_INC, Pos.WPVT_ALQ_PROV_INC);
    }

    /**Original name: WPVT-ALQ-PROV-INC<br>*/
    public AfDecimal getWpvtAlqProvInc() {
        return readPackedAsDecimal(Pos.WPVT_ALQ_PROV_INC, Len.Int.WPVT_ALQ_PROV_INC, Len.Fract.WPVT_ALQ_PROV_INC);
    }

    public byte[] getWpvtAlqProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPVT_ALQ_PROV_INC, Pos.WPVT_ALQ_PROV_INC);
        return buffer;
    }

    public void initWpvtAlqProvIncSpaces() {
        fill(Pos.WPVT_ALQ_PROV_INC, Len.WPVT_ALQ_PROV_INC, Types.SPACE_CHAR);
    }

    public void setWpvtAlqProvIncNull(String wpvtAlqProvIncNull) {
        writeString(Pos.WPVT_ALQ_PROV_INC_NULL, wpvtAlqProvIncNull, Len.WPVT_ALQ_PROV_INC_NULL);
    }

    /**Original name: WPVT-ALQ-PROV-INC-NULL<br>*/
    public String getWpvtAlqProvIncNull() {
        return readString(Pos.WPVT_ALQ_PROV_INC_NULL, Len.WPVT_ALQ_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_PROV_INC = 1;
        public static final int WPVT_ALQ_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPVT_ALQ_PROV_INC = 4;
        public static final int WPVT_ALQ_PROV_INC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPVT_ALQ_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
