package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WRIN-TP-RECORD-DATI<br>
 * Variable: WRIN-TP-RECORD-DATI from program LRGM0380<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrinTpRecordDati extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TASSI_MIN_GAR_MAXOCCURS = 12;
    public static final char WRIN_COINCID_SI = 'S';
    public static final char WRIN_COINCID_NO = 'N';

    //==== CONSTRUCTORS ====
    public WrinTpRecordDati() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIN_TP_RECORD_DATI;
    }

    public void setWrinTpRecordDati(String wrinTpRecordDati) {
        writeString(Pos.WRIN_TP_RECORD_DATI, wrinTpRecordDati, Len.WRIN_TP_RECORD_DATI);
    }

    public void setWrinTpRecordDatiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIN_TP_RECORD_DATI, Pos.WRIN_TP_RECORD_DATI);
    }

    /**Original name: WRIN-TP-RECORD-DATI<br>*/
    public String getWrinTpRecordDati() {
        return readString(Pos.WRIN_TP_RECORD_DATI, Len.WRIN_TP_RECORD_DATI);
    }

    public byte[] getWrinTpRecordDatiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIN_TP_RECORD_DATI, Pos.WRIN_TP_RECORD_DATI);
        return buffer;
    }

    /**Original name: WRIN-ANNO-RIF<br>*/
    public String getWrinAnnoRif() {
        return readString(Pos.ANNO_RIF, Len.ANNO_RIF);
    }

    /**Original name: WRIN-DATA-RICH-DA<br>*/
    public String getWrinDataRichDa() {
        return readString(Pos.DATA_RICH_DA, Len.DATA_RICH_DA);
    }

    /**Original name: WRIN-DATA-RICH-A<br>*/
    public String getWrinDataRichA() {
        return readString(Pos.DATA_RICH_A, Len.DATA_RICH_A);
    }

    /**Original name: WRIN-DT-DECOR-POLI<br>*/
    public String getWrinDtDecorPoli() {
        return readString(Pos.DT_DECOR_POLI, Len.DT_DECOR_POLI);
    }

    /**Original name: WRIN-DT-RIF-DA<br>*/
    public String getWrinDtRifDa() {
        return readString(Pos.DT_RIF_DA, Len.DT_RIF_DA);
    }

    public String getWrinDtRifDaFormatted() {
        return Functions.padBlanks(getWrinDtRifDa(), Len.DT_RIF_DA);
    }

    /**Original name: WRIN-DT-RIF-A<br>*/
    public String getWrinDtRifA() {
        return readString(Pos.DT_RIF_A, Len.DT_RIF_A);
    }

    public String getWrinDtRifAFormatted() {
        return Functions.padBlanks(getWrinDtRifA(), Len.DT_RIF_A);
    }

    /**Original name: WRIN-RISC-TOT-DT-RIF-EST-CC<br>
	 * <pre> --       DATI CALCOLATI</pre>*/
    public AfDecimal getWrinRiscTotDtRifEstCc() {
        return readDecimal(Pos.RISC_TOT_DT_RIF_EST_CC, Len.Int.WRIN_RISC_TOT_DT_RIF_EST_CC, Len.Fract.WRIN_RISC_TOT_DT_RIF_EST_CC, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CAP-LIQ-DT-RIF-EST-CC<br>*/
    public AfDecimal getWrinCapLiqDtRifEstCc() {
        return readDecimal(Pos.CAP_LIQ_DT_RIF_EST_CC, Len.Int.WRIN_CAP_LIQ_DT_RIF_EST_CC, Len.Fract.WRIN_CAP_LIQ_DT_RIF_EST_CC, SignType.NO_SIGN);
    }

    /**Original name: WRIN-FLAG-PROD-CON-CEDOLE<br>
	 * <pre>          07 (SF)-RISCAT-PARZ-RIMB          PIC 9(0012)V9(03).
	 *  --       DATI CALCOLATI PER PRODOTTI CEDOLA</pre>*/
    public String getWrinFlagProdConCedole() {
        return readString(Pos.FLAG_PROD_CON_CEDOLE, Len.FLAG_PROD_CON_CEDOLE);
    }

    /**Original name: WRIN-IMP-LOR-CED-LIQU<br>*/
    public AfDecimal getWrinImpLorCedLiqu() {
        return readDecimal(Pos.IMP_LOR_CED_LIQU, Len.Int.WRIN_IMP_LOR_CED_LIQU, Len.Fract.WRIN_IMP_LOR_CED_LIQU, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CC-PAESE<br>*/
    public String getWrinCcPaese() {
        return readString(Pos.CC_PAESE, Len.CC_PAESE);
    }

    public String getWrinCcPaeseFormatted() {
        return Functions.padBlanks(getWrinCcPaese(), Len.CC_PAESE);
    }

    /**Original name: WRIN-CC-CHECK-DIGIT<br>*/
    public String getWrinCcCheckDigit() {
        return readString(Pos.CC_CHECK_DIGIT, Len.CC_CHECK_DIGIT);
    }

    public String getWrinCcCheckDigitFormatted() {
        return Functions.padBlanks(getWrinCcCheckDigit(), Len.CC_CHECK_DIGIT);
    }

    /**Original name: WRIN-CC-CIN<br>*/
    public char getWrinCcCin() {
        return readChar(Pos.CC_CIN);
    }

    /**Original name: WRIN-CC-ABI<br>*/
    public String getWrinCcAbi() {
        return readString(Pos.CC_ABI, Len.CC_ABI);
    }

    public String getWrinCcAbiFormatted() {
        return Functions.padBlanks(getWrinCcAbi(), Len.CC_ABI);
    }

    /**Original name: WRIN-CC-CAB<br>*/
    public String getWrinCcCab() {
        return readString(Pos.CC_CAB, Len.CC_CAB);
    }

    public String getWrinCcCabFormatted() {
        return Functions.padBlanks(getWrinCcCab(), Len.CC_CAB);
    }

    /**Original name: WRIN-CC-NUM-CONTO<br>*/
    public String getWrinCcNumConto() {
        return readString(Pos.CC_NUM_CONTO, Len.CC_NUM_CONTO);
    }

    public String getWrinCcNumContoFormatted() {
        return Functions.padBlanks(getWrinCcNumConto(), Len.CC_NUM_CONTO);
    }

    /**Original name: WRIN-TOT-CUM-PREMI-VERS<br>*/
    public AfDecimal getWrinTotCumPremiVers() {
        return readDecimal(Pos.TOT_CUM_PREMI_VERS, Len.Int.WRIN_TOT_CUM_PREMI_VERS, Len.Fract.WRIN_TOT_CUM_PREMI_VERS, SignType.NO_SIGN);
    }

    public void setWrinTotCumPremiVersAp(AfDecimal wrinTotCumPremiVersAp) {
        writeDecimal(Pos.TOT_CUM_PREMI_VERS_AP, wrinTotCumPremiVersAp.copy(), SignType.NO_SIGN);
    }

    /**Original name: WRIN-TOT-CUM-PREMI-VERS-AP<br>*/
    public AfDecimal getWrinTotCumPremiVersAp() {
        return readDecimal(Pos.TOT_CUM_PREMI_VERS_AP, Len.Int.WRIN_TOT_CUM_PREMI_VERS_AP, Len.Fract.WRIN_TOT_CUM_PREMI_VERS_AP, SignType.NO_SIGN);
    }

    /**Original name: WRIN-TOT-CUM-PRE-INVST-AA-RIF<br>*/
    public AfDecimal getWrinTotCumPreInvstAaRif() {
        return readDecimal(Pos.TOT_CUM_PRE_INVST_AA_RIF, Len.Int.WRIN_TOT_CUM_PRE_INVST_AA_RIF, Len.Fract.WRIN_TOT_CUM_PRE_INVST_AA_RIF, SignType.NO_SIGN);
    }

    /**Original name: WRIN-PRESTAZIONE-AP-POL<br>*/
    public AfDecimal getWrinPrestazioneApPol() {
        return readDecimal(Pos.PRESTAZIONE_AP_POL, Len.Int.WRIN_PRESTAZIONE_AP_POL, Len.Fract.WRIN_PRESTAZIONE_AP_POL, SignType.NO_SIGN);
    }

    /**Original name: WRIN-PRESTAZIONE-AC-POL<br>*/
    public AfDecimal getWrinPrestazioneAcPol() {
        return readDecimal(Pos.PRESTAZIONE_AC_POL, Len.Int.WRIN_PRESTAZIONE_AC_POL, Len.Fract.WRIN_PRESTAZIONE_AC_POL, SignType.NO_SIGN);
    }

    /**Original name: WRIN-IMP-LRD-RISC-PARZ-AP-POL<br>*/
    public AfDecimal getWrinImpLrdRiscParzApPol() {
        return readDecimal(Pos.IMP_LRD_RISC_PARZ_AP_POL, Len.Int.WRIN_IMP_LRD_RISC_PARZ_AP_POL, Len.Fract.WRIN_IMP_LRD_RISC_PARZ_AP_POL, SignType.NO_SIGN);
    }

    /**Original name: WRIN-IMP-LRD-RISC-PARZ-AC-POL<br>*/
    public AfDecimal getWrinImpLrdRiscParzAcPol() {
        return readDecimal(Pos.IMP_LRD_RISC_PARZ_AC_POL, Len.Int.WRIN_IMP_LRD_RISC_PARZ_AC_POL, Len.Fract.WRIN_IMP_LRD_RISC_PARZ_AC_POL, SignType.NO_SIGN);
    }

    /**Original name: WRIN-ID-POLI<br>
	 * <pre> --       ID PER LETTURA TABELLE ESTRATTO CONTO</pre>*/
    public int getWrinIdPoli() {
        return readNumDispUnsignedInt(Pos.ID_POLI, Len.ID_POLI);
    }

    public String getWrinIdPoliFormatted() {
        return readFixedString(Pos.ID_POLI, Len.ID_POLI);
    }

    public String getWrinIdPoliAsString() {
        return getWrinIdPoliFormatted();
    }

    public String getWrinIdAdesFormatted() {
        return readFixedString(Pos.ID_ADES, Len.ID_ADES);
    }

    /**Original name: WRIN-COD-PROD-ACT<br>*/
    public String getWrinCodProdAct() {
        return readString(Pos.COD_PROD_ACT, Len.COD_PROD_ACT);
    }

    /**Original name: WRIN-NOME-ASSICU<br>*/
    public String getWrinNomeAssicu() {
        return readString(Pos.NOME_ASSICU, Len.NOME_ASSICU);
    }

    /**Original name: WRIN-COGNOME-ASSICU<br>*/
    public String getWrinCognomeAssicu() {
        return readString(Pos.COGNOME_ASSICU, Len.COGNOME_ASSICU);
    }

    public String getWrinIdGarFormatted() {
        return readFixedString(Pos.ID_GAR, Len.ID_GAR);
    }

    /**Original name: WRIN-COD-GAR<br>*/
    public String getWrinCodGar() {
        return readString(Pos.COD_GAR, Len.COD_GAR);
    }

    /**Original name: WRIN-TP-GAR<br>*/
    public short getWrinTpGar() {
        return readNumDispUnsignedShort(Pos.TP_GAR, Len.TP_GAR);
    }

    public String getWrinTpGarFormatted() {
        return readFixedString(Pos.TP_GAR, Len.TP_GAR);
    }

    /**Original name: WRIN-TP-INVST-GAR<br>*/
    public short getWrinTpInvstGar() {
        return readNumDispUnsignedShort(Pos.TP_INVST_GAR, Len.TP_INVST_GAR);
    }

    public String getWrinTpInvstGarFormatted() {
        return readFixedString(Pos.TP_INVST_GAR, Len.TP_INVST_GAR);
    }

    public String getWrinFrazionamentoFormatted() {
        return readFixedString(Pos.FRAZIONAMENTO, Len.FRAZIONAMENTO);
    }

    /**Original name: WRIN-TASSO-TECNICO<br>*/
    public AfDecimal getWrinTassoTecnico() {
        return readDecimal(Pos.TASSO_TECNICO, Len.Int.WRIN_TASSO_TECNICO, Len.Fract.WRIN_TASSO_TECNICO, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CUM-PREM-INVST-AA-RIF<br>
	 * <pre> --       PREMI ALLA DATA DI RIFERIMENTO ESTRATTO CONTO</pre>*/
    public AfDecimal getWrinCumPremInvstAaRif() {
        return readDecimal(Pos.CUM_PREM_INVST_AA_RIF, Len.Int.WRIN_CUM_PREM_INVST_AA_RIF, Len.Fract.WRIN_CUM_PREM_INVST_AA_RIF, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CUM-PREM-VERS-AA-RIF<br>*/
    public AfDecimal getWrinCumPremVersAaRif() {
        return readDecimal(Pos.CUM_PREM_VERS_AA_RIF, Len.Int.WRIN_CUM_PREM_VERS_AA_RIF, Len.Fract.WRIN_CUM_PREM_VERS_AA_RIF, SignType.NO_SIGN);
    }

    /**Original name: WRIN-PREST-MATUR-AA-RIF<br>*/
    public AfDecimal getWrinPrestMaturAaRif() {
        return readDecimal(Pos.PREST_MATUR_AA_RIF, Len.Int.WRIN_PREST_MATUR_AA_RIF, Len.Fract.WRIN_PREST_MATUR_AA_RIF, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CAP-LIQ-GA<br>*/
    public AfDecimal getWrinCapLiqGa() {
        return readDecimal(Pos.CAP_LIQ_GA, Len.Int.WRIN_CAP_LIQ_GA, Len.Fract.WRIN_CAP_LIQ_GA, SignType.NO_SIGN);
    }

    /**Original name: WRIN-RISC-TOT-GA<br>*/
    public AfDecimal getWrinRiscTotGa() {
        return readDecimal(Pos.RISC_TOT_GA, Len.Int.WRIN_RISC_TOT_GA, Len.Fract.WRIN_RISC_TOT_GA, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CUM-PREM-VERS-EC-PREC<br>
	 * <pre> --       PREMI ALLA DATA DELL'ESTRATTO CONTO PRECEDENTE</pre>*/
    public AfDecimal getWrinCumPremVersEcPrec() {
        return readDecimal(Pos.CUM_PREM_VERS_EC_PREC, Len.Int.WRIN_CUM_PREM_VERS_EC_PREC, Len.Fract.WRIN_CUM_PREM_VERS_EC_PREC, SignType.NO_SIGN);
    }

    /**Original name: WRIN-PREST-MATUR-EC-PREC<br>*/
    public AfDecimal getWrinPrestMaturEcPrec() {
        return readDecimal(Pos.PREST_MATUR_EC_PREC, Len.Int.WRIN_PREST_MATUR_EC_PREC, Len.Fract.WRIN_PREST_MATUR_EC_PREC, SignType.NO_SIGN);
    }

    /**Original name: WRIN-REND-LOR-GEST-SEP-GAR<br>
	 * <pre> --       TASSI RENDIMENTO ALLA DATA DI RIFERIM.ESTRATTO CONTO</pre>*/
    public AfDecimal getWrinRendLorGestSepGar() {
        return readDecimal(Pos.REND_LOR_GEST_SEP_GAR, Len.Int.WRIN_REND_LOR_GEST_SEP_GAR, Len.Fract.WRIN_REND_LOR_GEST_SEP_GAR, SignType.NO_SIGN);
    }

    /**Original name: WRIN-ALIQ-RETROC-RICON-GAR<br>*/
    public AfDecimal getWrinAliqRetrocRiconGar() {
        return readDecimal(Pos.ALIQ_RETROC_RICON_GAR, Len.Int.WRIN_ALIQ_RETROC_RICON_GAR, Len.Fract.WRIN_ALIQ_RETROC_RICON_GAR, SignType.NO_SIGN);
    }

    /**Original name: WRIN-TASSO-ANN-REND-RETROC-GAR<br>*/
    public AfDecimal getWrinTassoAnnRendRetrocGar() {
        return readDecimal(Pos.TASSO_ANN_REND_RETROC_GAR, Len.Int.WRIN_TASSO_ANN_REND_RETROC_GAR, Len.Fract.WRIN_TASSO_ANN_REND_RETROC_GAR, SignType.NO_SIGN);
    }

    /**Original name: WRIN-PC-COMM-GEST-GAR<br>*/
    public AfDecimal getWrinPcCommGestGar() {
        return readDecimal(Pos.PC_COMM_GEST_GAR, Len.Int.WRIN_PC_COMM_GEST_GAR, Len.Fract.WRIN_PC_COMM_GEST_GAR, SignType.NO_SIGN);
    }

    /**Original name: WRIN-TASSO-ANN-RIVAL-PREST-GAR<br>*/
    public AfDecimal getWrinTassoAnnRivalPrestGar() {
        return readDecimal(Pos.TASSO_ANN_RIVAL_PREST_GAR, Len.Int.WRIN_TASSO_ANN_RIVAL_PREST_GAR, Len.Fract.WRIN_TASSO_ANN_RIVAL_PREST_GAR, SignType.NO_SIGN);
    }

    /**Original name: WRIN-IMP-LRD-RISC-PARZ-AP-GA<br>
	 * <pre> --       DATI RISCATTO PARZIALE DI GARANZIA</pre>*/
    public AfDecimal getWrinImpLrdRiscParzApGa() {
        return readDecimal(Pos.IMP_LRD_RISC_PARZ_AP_GA, Len.Int.WRIN_IMP_LRD_RISC_PARZ_AP_GA, Len.Fract.WRIN_IMP_LRD_RISC_PARZ_AP_GA, SignType.NO_SIGN);
    }

    /**Original name: WRIN-IMP-LRD-RISC-PARZ-AC-GA<br>*/
    public AfDecimal getWrinImpLrdRiscParzAcGa() {
        return readDecimal(Pos.IMP_LRD_RISC_PARZ_AC_GA, Len.Int.WRIN_IMP_LRD_RISC_PARZ_AC_GA, Len.Fract.WRIN_IMP_LRD_RISC_PARZ_AC_GA, SignType.NO_SIGN);
    }

    /**Original name: WRIN-DT-INI-PER-INV<br>*/
    public String getWrinDtIniPerInv(int wrinDtIniPerInvIdx) {
        int position = Pos.wrinDtIniPerInv(wrinDtIniPerInvIdx - 1);
        return readString(position, Len.DT_INI_PER_INV);
    }

    public String getWrinDtIniPerInvFormatted(int wrinDtIniPerInvIdx) {
        return Functions.padBlanks(getWrinDtIniPerInv(wrinDtIniPerInvIdx), Len.DT_INI_PER_INV);
    }

    /**Original name: WRIN-DT-FIN-PER-INV<br>*/
    public String getWrinDtFinPerInv(int wrinDtFinPerInvIdx) {
        int position = Pos.wrinDtFinPerInv(wrinDtFinPerInvIdx - 1);
        return readString(position, Len.DT_FIN_PER_INV);
    }

    /**Original name: WRIN-REN-MIN-TRNUT<br>*/
    public AfDecimal getWrinRenMinTrnut() {
        return readDecimal(Pos.REN_MIN_TRNUT, Len.Int.WRIN_REN_MIN_TRNUT, Len.Fract.WRIN_REN_MIN_TRNUT, SignType.NO_SIGN);
    }

    /**Original name: WRIN-REN-MIN-GARTO<br>*/
    public AfDecimal getWrinRenMinGarto() {
        return readDecimal(Pos.REN_MIN_GARTO, Len.Int.WRIN_REN_MIN_GARTO, Len.Fract.WRIN_REN_MIN_GARTO, SignType.NO_SIGN);
    }

    /**Original name: WRIN-FL-STORNATE-ANN<br>*/
    public char getWrinFlStornateAnn() {
        return readChar(Pos.FL_STORNATE_ANN);
    }

    /**Original name: WRIN-COD-FND<br>
	 * <pre> --       DATI FONDI</pre>*/
    public String getWrinCodFnd() {
        return readString(Pos.COD_FND, Len.COD_FND);
    }

    /**Original name: WRIN-NUM-QUO-DISINV-RISC-PARZ<br>*/
    public AfDecimal getWrinNumQuoDisinvRiscParz() {
        return readDecimal(Pos.NUM_QUO_DISINV_RISC_PARZ, Len.Int.WRIN_NUM_QUO_DISINV_RISC_PARZ, Len.Fract.WRIN_NUM_QUO_DISINV_RISC_PARZ, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-DIS-RISC-PARZ-PRG<br>*/
    public AfDecimal getWrinNumQuoDisRiscParzPrg() {
        return readDecimal(Pos.NUM_QUO_DIS_RISC_PARZ_PRG, Len.Int.WRIN_NUM_QUO_DIS_RISC_PARZ_PRG, Len.Fract.WRIN_NUM_QUO_DIS_RISC_PARZ_PRG, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-DISINV-IMPOS-SOST<br>*/
    public AfDecimal getWrinNumQuoDisinvImposSost() {
        return readDecimal(Pos.NUM_QUO_DISINV_IMPOS_SOST, Len.Int.WRIN_NUM_QUO_DISINV_IMPOS_SOST, Len.Fract.WRIN_NUM_QUO_DISINV_IMPOS_SOST, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-DISINV-COMMGES<br>*/
    public AfDecimal getWrinNumQuoDisinvCommges() {
        return readDecimal(Pos.NUM_QUO_DISINV_COMMGES, Len.Int.WRIN_NUM_QUO_DISINV_COMMGES, Len.Fract.WRIN_NUM_QUO_DISINV_COMMGES, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-DISINV-PREL-COSTI<br>*/
    public AfDecimal getWrinNumQuoDisinvPrelCosti() {
        return readDecimal(Pos.NUM_QUO_DISINV_PREL_COSTI, Len.Int.WRIN_NUM_QUO_DISINV_PREL_COSTI, Len.Fract.WRIN_NUM_QUO_DISINV_PREL_COSTI, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-DISINV-SWITCH<br>*/
    public AfDecimal getWrinNumQuoDisinvSwitch() {
        return readDecimal(Pos.NUM_QUO_DISINV_SWITCH, Len.Int.WRIN_NUM_QUO_DISINV_SWITCH, Len.Fract.WRIN_NUM_QUO_DISINV_SWITCH, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-INVST-SWITCH<br>*/
    public AfDecimal getWrinNumQuoInvstSwitch() {
        return readDecimal(Pos.NUM_QUO_INVST_SWITCH, Len.Int.WRIN_NUM_QUO_INVST_SWITCH, Len.Fract.WRIN_NUM_QUO_INVST_SWITCH, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-INVST-VERS<br>*/
    public AfDecimal getWrinNumQuoInvstVers() {
        return readDecimal(Pos.NUM_QUO_INVST_VERS, Len.Int.WRIN_NUM_QUO_INVST_VERS, Len.Fract.WRIN_NUM_QUO_INVST_VERS, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-INVST-REBATE<br>*/
    public AfDecimal getWrinNumQuoInvstRebate() {
        return readDecimal(Pos.NUM_QUO_INVST_REBATE, Len.Int.WRIN_NUM_QUO_INVST_REBATE, Len.Fract.WRIN_NUM_QUO_INVST_REBATE, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-INVST-PURO-RISC<br>*/
    public AfDecimal getWrinNumQuoInvstPuroRisc() {
        return readDecimal(Pos.NUM_QUO_INVST_PURO_RISC, Len.Int.WRIN_NUM_QUO_INVST_PURO_RISC, Len.Fract.WRIN_NUM_QUO_INVST_PURO_RISC, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-ATTUALE<br>*/
    public AfDecimal getWrinNumQuoAttuale() {
        return readDecimal(Pos.NUM_QUO_ATTUALE, Len.Int.WRIN_NUM_QUO_ATTUALE, Len.Fract.WRIN_NUM_QUO_ATTUALE, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CNTRVAL-ATTUALE<br>*/
    public AfDecimal getWrinCntrvalAttuale() {
        return readDecimal(Pos.CNTRVAL_ATTUALE, Len.Int.WRIN_CNTRVAL_ATTUALE, Len.Fract.WRIN_CNTRVAL_ATTUALE, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-PREC<br>*/
    public AfDecimal getWrinNumQuoPrec() {
        return readDecimal(Pos.NUM_QUO_PREC, Len.Int.WRIN_NUM_QUO_PREC, Len.Fract.WRIN_NUM_QUO_PREC, SignType.NO_SIGN);
    }

    /**Original name: WRIN-CNTRVAL-PREC<br>*/
    public AfDecimal getWrinCntrvalPrec() {
        return readDecimal(Pos.CNTRVAL_PREC, Len.Int.WRIN_CNTRVAL_PREC, Len.Fract.WRIN_CNTRVAL_PREC, SignType.NO_SIGN);
    }

    public String getWrinIdGarFndFormatted() {
        return readFixedString(Pos.ID_GAR_FND, Len.ID_GAR_FND);
    }

    /**Original name: WRIN-NUM-QUO-DISINV-COMP-NAV<br>*/
    public AfDecimal getWrinNumQuoDisinvCompNav() {
        return readDecimal(Pos.NUM_QUO_DISINV_COMP_NAV, Len.Int.WRIN_NUM_QUO_DISINV_COMP_NAV, Len.Fract.WRIN_NUM_QUO_DISINV_COMP_NAV, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUM-QUO-INVST-COMP-NAV<br>*/
    public AfDecimal getWrinNumQuoInvstCompNav() {
        return readDecimal(Pos.NUM_QUO_INVST_COMP_NAV, Len.Int.WRIN_NUM_QUO_INVST_COMP_NAV, Len.Fract.WRIN_NUM_QUO_INVST_COMP_NAV, SignType.NO_SIGN);
    }

    /**Original name: WRIN-IMP-LORDO-RISC-PARZ<br>
	 * <pre> --       DATI RISCATTI PARZIALI</pre>*/
    public AfDecimal getWrinImpLordoRiscParz() {
        return readDecimal(Pos.IMP_LORDO_RISC_PARZ, Len.Int.WRIN_IMP_LORDO_RISC_PARZ, Len.Fract.WRIN_IMP_LORDO_RISC_PARZ, SignType.NO_SIGN);
    }

    /**Original name: WRIN-DT-EFFETTO-LIQ<br>*/
    public String getWrinDtEffettoLiq() {
        return readString(Pos.DT_EFFETTO_LIQ, Len.DT_EFFETTO_LIQ);
    }

    /**Original name: WRIN-RP-TP-MOVI<br>*/
    public int getWrinRpTpMovi() {
        return readNumDispUnsignedInt(Pos.RP_TP_MOVI, Len.RP_TP_MOVI);
    }

    public String getWrinRpTpMoviFormatted() {
        return readFixedString(Pos.RP_TP_MOVI, Len.RP_TP_MOVI);
    }

    public String getWrinIdGaranziaFormatted() {
        return readFixedString(Pos.ID_GARANZIA, Len.ID_GARANZIA);
    }

    public String getWrinTpMoviFormatted() {
        return readFixedString(Pos.TP_MOVI, Len.TP_MOVI);
    }

    /**Original name: WRIN-DESC-TP-MOVI<br>*/
    public String getWrinDescTpMovi() {
        return readString(Pos.DESC_TP_MOVI, Len.DESC_TP_MOVI);
    }

    /**Original name: WRIN-DT-EFF-MOVI<br>*/
    public String getWrinDtEffMovi() {
        return readString(Pos.DT_EFF_MOVI, Len.DT_EFF_MOVI);
    }

    /**Original name: WRIN-IMP-VERS<br>*/
    public AfDecimal getWrinImpVers() {
        return readDecimal(Pos.IMP_VERS, Len.Int.WRIN_IMP_VERS, Len.Fract.WRIN_IMP_VERS, SignType.NO_SIGN);
    }

    /**Original name: WRIN-IMP-OPER<br>*/
    public AfDecimal getWrinImpOper() {
        return readDecimal(Pos.IMP_OPER, Len.Int.WRIN_IMP_OPER, Len.Fract.WRIN_IMP_OPER, SignType.NO_SIGN);
    }

    /**Original name: WRIN-NUMERO-QUO<br>*/
    public AfDecimal getWrinNumeroQuo() {
        return readDecimal(Pos.NUMERO_QUO, Len.Int.WRIN_NUMERO_QUO, Len.Fract.WRIN_NUMERO_QUO, SignType.NO_SIGN);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIN_TP_RECORD_DATI = 1;
        public static final int FILLER_WRIN_REC_OUTPUT = 1;
        public static final int ANNO_RIF = FILLER_WRIN_REC_OUTPUT;
        public static final int DATA_ELAB = ANNO_RIF + Len.ANNO_RIF;
        public static final int DATA_RICH_DA = DATA_ELAB + Len.DATA_ELAB;
        public static final int DATA_RICH_A = DATA_RICH_DA + Len.DATA_RICH_DA;
        public static final int FLR1 = DATA_RICH_A + Len.DATA_RICH_A;
        public static final int FILLER_WRIN_REC_OUTPUT2 = 1;
        public static final int COD_AG_GEST = FILLER_WRIN_REC_OUTPUT2;
        public static final int TP_ACQ_CONTR_GEST = COD_AG_GEST + Len.COD_AG_GEST;
        public static final int COD_ACQ_CONTR_GEST = TP_ACQ_CONTR_GEST + Len.TP_ACQ_CONTR_GEST;
        public static final int COD_PUNTO_RETE_FIN_GEST = COD_ACQ_CONTR_GEST + Len.COD_ACQ_CONTR_GEST;
        public static final int COD_CAN_GEST = COD_PUNTO_RETE_FIN_GEST + Len.COD_PUNTO_RETE_FIN_GEST;
        public static final int COD_AG_ACQ = COD_CAN_GEST + Len.COD_CAN_GEST;
        public static final int TP_ACQ_CONTR_ACQ = COD_AG_ACQ + Len.COD_AG_ACQ;
        public static final int COD_ACQ_CONTR_ACQ = TP_ACQ_CONTR_ACQ + Len.TP_ACQ_CONTR_ACQ;
        public static final int COD_PUNTO_RETE_FIN_ACQ = COD_ACQ_CONTR_ACQ + Len.COD_ACQ_CONTR_ACQ;
        public static final int COD_CAN_ACQ = COD_PUNTO_RETE_FIN_ACQ + Len.COD_PUNTO_RETE_FIN_ACQ;
        public static final int TP_POLI = COD_CAN_ACQ + Len.COD_CAN_ACQ;
        public static final int DT_EMIS_POLI = TP_POLI + Len.TP_POLI;
        public static final int DT_DECOR_POLI = DT_EMIS_POLI + Len.DT_EMIS_POLI;
        public static final int DT_SCAD_POLI = DT_DECOR_POLI + Len.DT_DECOR_POLI;
        public static final int TP_STAT_POLI = DT_SCAD_POLI + Len.DT_SCAD_POLI;
        public static final int TP_CAUS_POLI = TP_STAT_POLI + Len.TP_STAT_POLI;
        public static final int DUR_ANNI_POLI = TP_CAUS_POLI + Len.TP_CAUS_POLI;
        public static final int DUR_MESI_POLI = DUR_ANNI_POLI + Len.DUR_ANNI_POLI;
        public static final int DUR_GIORNI_POLI = DUR_MESI_POLI + Len.DUR_MESI_POLI;
        public static final int COD_CONVENZ_POLI = DUR_GIORNI_POLI + Len.DUR_GIORNI_POLI;
        public static final int DENOMINAZ_PRODOTTO = COD_CONVENZ_POLI + Len.COD_CONVENZ_POLI;
        public static final int DT_RIF_DA = DENOMINAZ_PRODOTTO + Len.DENOMINAZ_PRODOTTO;
        public static final int DT_RIF_A = DT_RIF_DA + Len.DT_RIF_DA;
        public static final int DT_EMIS_ADES = DT_RIF_A + Len.DT_RIF_A;
        public static final int DT_DECOR_ADES = DT_EMIS_ADES + Len.DT_EMIS_ADES;
        public static final int TP_STAT_ADES = DT_DECOR_ADES + Len.DT_DECOR_ADES;
        public static final int TP_CAUS_ADES = TP_STAT_ADES + Len.TP_STAT_ADES;
        public static final int DUR_ANNI_ADES = TP_CAUS_ADES + Len.TP_CAUS_ADES;
        public static final int DUR_MESI_ADES = DUR_ANNI_ADES + Len.DUR_ANNI_ADES;
        public static final int DUR_GIORNI_ADES = DUR_MESI_ADES + Len.DUR_MESI_ADES;
        public static final int CAPITALE_VINCOL = DUR_GIORNI_ADES + Len.DUR_GIORNI_ADES;
        public static final int DT_VINCOL = CAPITALE_VINCOL + Len.CAPITALE_VINCOL;
        public static final int RISC_TOT_DT_RIF_EST_CC = DT_VINCOL + Len.DT_VINCOL;
        public static final int CAP_LIQ_DT_RIF_EST_CC = RISC_TOT_DT_RIF_EST_CC + Len.RISC_TOT_DT_RIF_EST_CC;
        public static final int RISCAT_PER_QUIESC = CAP_LIQ_DT_RIF_EST_CC + Len.CAP_LIQ_DT_RIF_EST_CC;
        public static final int VAL_RIDUZ = RISCAT_PER_QUIESC + Len.RISCAT_PER_QUIESC;
        public static final int VALORE_IMPOSTA_AA_RIF_EC = VAL_RIDUZ + Len.VAL_RIDUZ;
        public static final int FLAG_PROD_CON_CEDOLE = VALORE_IMPOSTA_AA_RIF_EC + Len.VALORE_IMPOSTA_AA_RIF_EC;
        public static final int IMP_LOR_CED_LIQU_AP = FLAG_PROD_CON_CEDOLE + Len.FLAG_PROD_CON_CEDOLE;
        public static final int IMP_LOR_CED_LIQU = IMP_LOR_CED_LIQU_AP + Len.IMP_LOR_CED_LIQU_AP;
        public static final int SPESE_CED_LIQU = IMP_LOR_CED_LIQU + Len.IMP_LOR_CED_LIQU;
        public static final int IMPOST_SOST_CED_LIQU = SPESE_CED_LIQU + Len.SPESE_CED_LIQU;
        public static final int IMP_NET_CED_LIQU = IMPOST_SOST_CED_LIQU + Len.IMPOST_SOST_CED_LIQU;
        public static final int DT_ULT_LIQU_CED = IMP_NET_CED_LIQU + Len.IMP_NET_CED_LIQU;
        public static final int ESTREMI_CONTO_CORR = DT_ULT_LIQU_CED + Len.DT_ULT_LIQU_CED;
        public static final int CC_PAESE = ESTREMI_CONTO_CORR;
        public static final int CC_CHECK_DIGIT = CC_PAESE + Len.CC_PAESE;
        public static final int CC_CIN = CC_CHECK_DIGIT + Len.CC_CHECK_DIGIT;
        public static final int CC_ABI = CC_CIN + Len.CC_CIN;
        public static final int CC_CAB = CC_ABI + Len.CC_ABI;
        public static final int CC_NUM_CONTO = CC_CAB + Len.CC_CAB;
        public static final int IMP_CED_CAPIT = CC_NUM_CONTO + Len.CC_NUM_CONTO;
        public static final int DT_ULT_CAPIT_CED = IMP_CED_CAPIT + Len.IMP_CED_CAPIT;
        public static final int DT_ULT_TIT_CONT_INC = DT_ULT_CAPIT_CED + Len.DT_ULT_CAPIT_CED;
        public static final int TOT_CUM_PREMI_VERS = DT_ULT_TIT_CONT_INC + Len.DT_ULT_TIT_CONT_INC;
        public static final int TOT_CUM_PREMI_VERS_AP = TOT_CUM_PREMI_VERS + Len.TOT_CUM_PREMI_VERS;
        public static final int TOT_CUM_PRE_INVST_AA_RIF = TOT_CUM_PREMI_VERS_AP + Len.TOT_CUM_PREMI_VERS_AP;
        public static final int IMPOST_SOSTIT_TOTALE = TOT_CUM_PRE_INVST_AA_RIF + Len.TOT_CUM_PRE_INVST_AA_RIF;
        public static final int PRESTAZIONE_AP_POL = IMPOST_SOSTIT_TOTALE + Len.IMPOST_SOSTIT_TOTALE;
        public static final int PRESTAZIONE_AC_POL = PRESTAZIONE_AP_POL + Len.PRESTAZIONE_AP_POL;
        public static final int IMP_LRD_RISC_PARZ_AP_POL = PRESTAZIONE_AC_POL + Len.PRESTAZIONE_AC_POL;
        public static final int IMP_LRD_RISC_PARZ_AC_POL = IMP_LRD_RISC_PARZ_AP_POL + Len.IMP_LRD_RISC_PARZ_AP_POL;
        public static final int ID_POLI = IMP_LRD_RISC_PARZ_AC_POL + Len.IMP_LRD_RISC_PARZ_AC_POL;
        public static final int ID_ADES = ID_POLI + Len.ID_POLI;
        public static final int COD_PROD_PARTNER = ID_ADES + Len.ID_ADES;
        public static final int COD_PROD_ACT = COD_PROD_PARTNER + Len.COD_PROD_PARTNER;
        public static final int CAPITALE_PROTETTO = COD_PROD_ACT + Len.COD_PROD_ACT;
        public static final int COS_RUN_ASSVA = CAPITALE_PROTETTO + Len.CAPITALE_PROTETTO;
        public static final int COS_RUN_ASSVA_IDC = COS_RUN_ASSVA + Len.COS_RUN_ASSVA;
        public static final int COS_RUN_AMS_FNDINT = COS_RUN_ASSVA_IDC + Len.COS_RUN_ASSVA_IDC;
        public static final int FLR2 = COS_RUN_AMS_FNDINT + Len.COS_RUN_AMS_FNDINT;
        public static final int TOT_IMP_BOLLO_RESIDUO = FLR2 + Len.FLR2;
        public static final int FILLER_WRIN_REC_OUTPUT4 = 1;
        public static final int COD_SOGG_CONTRA = FILLER_WRIN_REC_OUTPUT4;
        public static final int TP_PERS_CONTRA = COD_SOGG_CONTRA + Len.COD_SOGG_CONTRA;
        public static final int NOME_CONTRA = TP_PERS_CONTRA + Len.TP_PERS_CONTRA;
        public static final int COGNOME_CONTRA = NOME_CONTRA + Len.NOME_CONTRA;
        public static final int RAG_SOC_CONTRA = COGNOME_CONTRA + Len.COGNOME_CONTRA;
        public static final int INDIRIZZO_CONTRA = RAG_SOC_CONTRA + Len.RAG_SOC_CONTRA;
        public static final int LOCALITA_CONTRA = INDIRIZZO_CONTRA + Len.INDIRIZZO_CONTRA;
        public static final int CAP_CONTRA = LOCALITA_CONTRA + Len.LOCALITA_CONTRA;
        public static final int PROVINCIA_CONTRA = CAP_CONTRA + Len.CAP_CONTRA;
        public static final int PAESE_CONTRA = PROVINCIA_CONTRA + Len.PROVINCIA_CONTRA;
        public static final int SESSO_CONTRA = PAESE_CONTRA + Len.PAESE_CONTRA;
        public static final int COD_SOGG_ADEREN = SESSO_CONTRA + Len.SESSO_CONTRA;
        public static final int TP_PERS_ADEREN = COD_SOGG_ADEREN + Len.COD_SOGG_ADEREN;
        public static final int NOME_ADEREN = TP_PERS_ADEREN + Len.TP_PERS_ADEREN;
        public static final int COGNOME_ADEREN = NOME_ADEREN + Len.NOME_ADEREN;
        public static final int RAG_SOC_ADEREN = COGNOME_ADEREN + Len.COGNOME_ADEREN;
        public static final int INDIRIZZO_ADEREN = RAG_SOC_ADEREN + Len.RAG_SOC_ADEREN;
        public static final int LOCALITA_ADEREN = INDIRIZZO_ADEREN + Len.INDIRIZZO_ADEREN;
        public static final int CAP_ADEREN = LOCALITA_ADEREN + Len.LOCALITA_ADEREN;
        public static final int PROVINCIA_ADEREN = CAP_ADEREN + Len.CAP_ADEREN;
        public static final int PAESE_ADEREN = PROVINCIA_ADEREN + Len.PROVINCIA_ADEREN;
        public static final int SESSO_ADEREN = PAESE_ADEREN + Len.PAESE_ADEREN;
        public static final int COD_SOGG_ASSICU = SESSO_ADEREN + Len.SESSO_ADEREN;
        public static final int TP_PERS_ASSICU = COD_SOGG_ASSICU + Len.COD_SOGG_ASSICU;
        public static final int NOME_ASSICU = TP_PERS_ASSICU + Len.TP_PERS_ASSICU;
        public static final int COGNOME_ASSICU = NOME_ASSICU + Len.NOME_ASSICU;
        public static final int RAG_SOC_ASSICU = COGNOME_ASSICU + Len.COGNOME_ASSICU;
        public static final int FLAG_COINCID_ASSICU = RAG_SOC_ASSICU + Len.RAG_SOC_ASSICU;
        public static final int SESSO_ASSICU = FLAG_COINCID_ASSICU + Len.FLAG_COINCID_ASSICU;
        public static final int COD_SOGG_VINCOL = SESSO_ASSICU + Len.SESSO_ASSICU;
        public static final int TP_PERS_VINCOL = COD_SOGG_VINCOL + Len.COD_SOGG_VINCOL;
        public static final int NOME_VINCOL = TP_PERS_VINCOL + Len.TP_PERS_VINCOL;
        public static final int COGNOME_VINCOL = NOME_VINCOL + Len.NOME_VINCOL;
        public static final int RAG_SOC_VINCOL = COGNOME_VINCOL + Len.COGNOME_VINCOL;
        public static final int SESSO_VINCOL = RAG_SOC_VINCOL + Len.RAG_SOC_VINCOL;
        public static final int BENEFIC_MORTE = SESSO_VINCOL + Len.SESSO_VINCOL;
        public static final int BENEFIC_VITA = BENEFIC_MORTE + Len.BENEFIC_MORTE;
        public static final int FL_BEN_NORMAL = BENEFIC_VITA + Len.BENEFIC_VITA;
        public static final int FLR3 = FL_BEN_NORMAL + Len.FL_BEN_NORMAL;
        public static final int FILLER_WRIN_REC_OUTPUT6 = 1;
        public static final int ID_GAR = FILLER_WRIN_REC_OUTPUT6;
        public static final int COD_GAR = ID_GAR + Len.ID_GAR;
        public static final int DENOMINAZ_GAR = COD_GAR + Len.COD_GAR;
        public static final int TP_GAR = DENOMINAZ_GAR + Len.DENOMINAZ_GAR;
        public static final int TP_INVST_GAR = TP_GAR + Len.TP_GAR;
        public static final int TP_PERIOD_PREMIO = TP_INVST_GAR + Len.TP_INVST_GAR;
        public static final int FRAZIONAMENTO = TP_PERIOD_PREMIO + Len.TP_PERIOD_PREMIO;
        public static final int DUR_PAGAM_PREMI = FRAZIONAMENTO + Len.FRAZIONAMENTO;
        public static final int TASSO_TECNICO = DUR_PAGAM_PREMI + Len.DUR_PAGAM_PREMI;
        public static final int CUM_PREM_INVST_AA_RIF = TASSO_TECNICO + Len.TASSO_TECNICO;
        public static final int CUM_PREM_VERS_AA_RIF = CUM_PREM_INVST_AA_RIF + Len.CUM_PREM_INVST_AA_RIF;
        public static final int TASSE_PREM_VERS_AA_RIF = CUM_PREM_VERS_AA_RIF + Len.CUM_PREM_VERS_AA_RIF;
        public static final int PREST_MATUR_AA_RIF = TASSE_PREM_VERS_AA_RIF + Len.TASSE_PREM_VERS_AA_RIF;
        public static final int CAP_LIQ_GA = PREST_MATUR_AA_RIF + Len.PREST_MATUR_AA_RIF;
        public static final int CAP_LIQ_GA_ACC = CAP_LIQ_GA + Len.CAP_LIQ_GA;
        public static final int RISC_TOT_GA = CAP_LIQ_GA_ACC + Len.CAP_LIQ_GA_ACC;
        public static final int PRE_ATT_GAR = RISC_TOT_GA + Len.RISC_TOT_GA;
        public static final int PREST_INIZ_GAR = PRE_ATT_GAR + Len.PRE_ATT_GAR;
        public static final int CUM_PREM_VERS_EC_PREC = PREST_INIZ_GAR + Len.PREST_INIZ_GAR;
        public static final int TASSE_PREM_VERS_EC_PREC = CUM_PREM_VERS_EC_PREC + Len.CUM_PREM_VERS_EC_PREC;
        public static final int PREST_MATUR_EC_PREC = TASSE_PREM_VERS_EC_PREC + Len.TASSE_PREM_VERS_EC_PREC;
        public static final int REND_LOR_GEST_SEP_GAR = PREST_MATUR_EC_PREC + Len.PREST_MATUR_EC_PREC;
        public static final int ALIQ_RETROC_RICON_GAR = REND_LOR_GEST_SEP_GAR + Len.REND_LOR_GEST_SEP_GAR;
        public static final int TASSO_ANN_REND_RETROC_GAR = ALIQ_RETROC_RICON_GAR + Len.ALIQ_RETROC_RICON_GAR;
        public static final int PC_COMM_GEST_GAR = TASSO_ANN_REND_RETROC_GAR + Len.TASSO_ANN_REND_RETROC_GAR;
        public static final int TASSO_ANN_RIVAL_PREST_GAR = PC_COMM_GEST_GAR + Len.PC_COMM_GEST_GAR;
        public static final int IMP_LRD_RISC_PARZ_AP_GA = TASSO_ANN_RIVAL_PREST_GAR + Len.TASSO_ANN_RIVAL_PREST_GAR;
        public static final int IMP_LRD_RISC_PARZ_AC_GA = IMP_LRD_RISC_PARZ_AP_GA + Len.IMP_LRD_RISC_PARZ_AP_GA;
        public static final int CAP_MIN_LIQU_SCAD_GAR = wrinDtAttrib(TAB_TASSI_MIN_GAR_MAXOCCURS - 1) + Len.DT_ATTRIB;
        public static final int CAP_AGG_COMPL_INF = CAP_MIN_LIQU_SCAD_GAR + Len.CAP_MIN_LIQU_SCAD_GAR;
        public static final int CAP_AGG_GAR_FAM = CAP_AGG_COMPL_INF + Len.CAP_AGG_COMPL_INF;
        public static final int REN_MIN_TRNUT = CAP_AGG_GAR_FAM + Len.CAP_AGG_GAR_FAM;
        public static final int REN_MIN_GARTO = REN_MIN_TRNUT + Len.REN_MIN_TRNUT;
        public static final int COD_FND_GAR = REN_MIN_GARTO + Len.REN_MIN_GARTO;
        public static final int DESC_FND_GAR = COD_FND_GAR + Len.COD_FND_GAR;
        public static final int VAL_RIDUZ_PRE_PAG = DESC_FND_GAR + Len.DESC_FND_GAR;
        public static final int VAL_RISC_PRE_PAG = VAL_RIDUZ_PRE_PAG + Len.VAL_RIDUZ_PRE_PAG;
        public static final int FL_STORNATE_ANN = VAL_RISC_PRE_PAG + Len.VAL_RISC_PRE_PAG;
        public static final int FL_CUM_GAR_STORNATE = FL_STORNATE_ANN + Len.FL_STORNATE_ANN;
        public static final int FLR4 = FL_CUM_GAR_STORNATE + Len.FL_CUM_GAR_STORNATE;
        public static final int FILLER_WRIN_REC_OUTPUT16 = 1;
        public static final int COD_FND = FILLER_WRIN_REC_OUTPUT16;
        public static final int DENOMINAZ_FONDO = COD_FND + Len.COD_FND;
        public static final int COD_GAR_FND = DENOMINAZ_FONDO + Len.DENOMINAZ_FONDO;
        public static final int NUM_QUO_DISINV_RISC_PARZ = COD_GAR_FND + Len.COD_GAR_FND;
        public static final int VAL_QUO_DISINV_RISC_PARZ = NUM_QUO_DISINV_RISC_PARZ + Len.NUM_QUO_DISINV_RISC_PARZ;
        public static final int NUM_QUO_DIS_RISC_PARZ_PRG = VAL_QUO_DISINV_RISC_PARZ + Len.VAL_QUO_DISINV_RISC_PARZ;
        public static final int VAL_QUO_DIS_RISC_PARZ_PRG = NUM_QUO_DIS_RISC_PARZ_PRG + Len.NUM_QUO_DIS_RISC_PARZ_PRG;
        public static final int NUM_QUO_DISINV_IMPOS_SOST = VAL_QUO_DIS_RISC_PARZ_PRG + Len.VAL_QUO_DIS_RISC_PARZ_PRG;
        public static final int VAL_QUO_DISINV_IMPOS_SOST = NUM_QUO_DISINV_IMPOS_SOST + Len.NUM_QUO_DISINV_IMPOS_SOST;
        public static final int NUM_QUO_DISINV_COMMGES = VAL_QUO_DISINV_IMPOS_SOST + Len.VAL_QUO_DISINV_IMPOS_SOST;
        public static final int VAL_QUO_DISINV_COMMGES = NUM_QUO_DISINV_COMMGES + Len.NUM_QUO_DISINV_COMMGES;
        public static final int NUM_QUO_DISINV_PREL_COSTI = VAL_QUO_DISINV_COMMGES + Len.VAL_QUO_DISINV_COMMGES;
        public static final int VAL_QUO_DISINV_PREL_COSTI = NUM_QUO_DISINV_PREL_COSTI + Len.NUM_QUO_DISINV_PREL_COSTI;
        public static final int NUM_QUO_DISINV_SWITCH = VAL_QUO_DISINV_PREL_COSTI + Len.VAL_QUO_DISINV_PREL_COSTI;
        public static final int VAL_QUO_DISINV_SWITCH = NUM_QUO_DISINV_SWITCH + Len.NUM_QUO_DISINV_SWITCH;
        public static final int NUM_QUO_INVST_SWITCH = VAL_QUO_DISINV_SWITCH + Len.VAL_QUO_DISINV_SWITCH;
        public static final int VAL_QUO_INVST_SWITCH = NUM_QUO_INVST_SWITCH + Len.NUM_QUO_INVST_SWITCH;
        public static final int NUM_QUO_INVST_VERS = VAL_QUO_INVST_SWITCH + Len.VAL_QUO_INVST_SWITCH;
        public static final int VAL_QUO_INVST_VERS = NUM_QUO_INVST_VERS + Len.NUM_QUO_INVST_VERS;
        public static final int NUM_QUO_INVST_REBATE = VAL_QUO_INVST_VERS + Len.VAL_QUO_INVST_VERS;
        public static final int VAL_QUO_INVST_REBATE = NUM_QUO_INVST_REBATE + Len.NUM_QUO_INVST_REBATE;
        public static final int NUM_QUO_INVST_PURO_RISC = VAL_QUO_INVST_REBATE + Len.VAL_QUO_INVST_REBATE;
        public static final int VAL_QUO_INVST_PURO_RISC = NUM_QUO_INVST_PURO_RISC + Len.NUM_QUO_INVST_PURO_RISC;
        public static final int NUM_QUO_ATTUALE = VAL_QUO_INVST_PURO_RISC + Len.VAL_QUO_INVST_PURO_RISC;
        public static final int CNTRVAL_ATTUALE = NUM_QUO_ATTUALE + Len.NUM_QUO_ATTUALE;
        public static final int DT_VALOR_QUO_ATTUALE = CNTRVAL_ATTUALE + Len.CNTRVAL_ATTUALE;
        public static final int VAL_QUO_ATTUALE = DT_VALOR_QUO_ATTUALE + Len.DT_VALOR_QUO_ATTUALE;
        public static final int NUM_QUO_PREC = VAL_QUO_ATTUALE + Len.VAL_QUO_ATTUALE;
        public static final int CNTRVAL_PREC = NUM_QUO_PREC + Len.NUM_QUO_PREC;
        public static final int DT_VALOR_QUO_PREC = CNTRVAL_PREC + Len.CNTRVAL_PREC;
        public static final int VAL_QUO_PREC = DT_VALOR_QUO_PREC + Len.DT_VALOR_QUO_PREC;
        public static final int ID_GAR_FND = VAL_QUO_PREC + Len.VAL_QUO_PREC;
        public static final int NUM_QUO_DISINV_COMP_NAV = ID_GAR_FND + Len.ID_GAR_FND;
        public static final int VAL_QUO_DISINV_COMP_NAV = NUM_QUO_DISINV_COMP_NAV + Len.NUM_QUO_DISINV_COMP_NAV;
        public static final int NUM_QUO_INVST_COMP_NAV = VAL_QUO_DISINV_COMP_NAV + Len.VAL_QUO_DISINV_COMP_NAV;
        public static final int VAL_QUO_INVST_COMP_NAV = NUM_QUO_INVST_COMP_NAV + Len.NUM_QUO_INVST_COMP_NAV;
        public static final int FLR5 = VAL_QUO_INVST_COMP_NAV + Len.VAL_QUO_INVST_COMP_NAV;
        public static final int FILLER_WRIN_REC_OUTPUT18 = 1;
        public static final int IMP_LORDO_RISC_PARZ = FILLER_WRIN_REC_OUTPUT18;
        public static final int PREMI_ATTIVI_PRE_RISC = IMP_LORDO_RISC_PARZ + Len.IMP_LORDO_RISC_PARZ;
        public static final int PREMI_ATTIVI_POST_RISC = PREMI_ATTIVI_PRE_RISC + Len.PREMI_ATTIVI_PRE_RISC;
        public static final int DELTA_PREMI_ATTIVI = PREMI_ATTIVI_POST_RISC + Len.PREMI_ATTIVI_POST_RISC;
        public static final int DT_EFFETTO_LIQ = DELTA_PREMI_ATTIVI + Len.DELTA_PREMI_ATTIVI;
        public static final int RP_TP_MOVI = DT_EFFETTO_LIQ + Len.DT_EFFETTO_LIQ;
        public static final int FLR6 = RP_TP_MOVI + Len.RP_TP_MOVI;
        public static final int FILLER_WRIN_REC_OUTPUT20 = 1;
        public static final int ID_GARANZIA = FILLER_WRIN_REC_OUTPUT20;
        public static final int CODICE_GARANZIA = ID_GARANZIA + Len.ID_GARANZIA;
        public static final int DESC_GARANZIA = CODICE_GARANZIA + Len.CODICE_GARANZIA;
        public static final int CODICE_FND = DESC_GARANZIA + Len.DESC_GARANZIA;
        public static final int DESCR_FND = CODICE_FND + Len.CODICE_FND;
        public static final int COMPETENZA_MOVI = DESCR_FND + Len.DESCR_FND;
        public static final int ID_MOVI = COMPETENZA_MOVI + Len.COMPETENZA_MOVI;
        public static final int TP_MOVI = ID_MOVI + Len.ID_MOVI;
        public static final int DESC_TP_MOVI = TP_MOVI + Len.TP_MOVI;
        public static final int VERSO_MOVI = DESC_TP_MOVI + Len.DESC_TP_MOVI;
        public static final int DT_EFF_MOVI = VERSO_MOVI + Len.VERSO_MOVI;
        public static final int IMP_VERS = DT_EFF_MOVI + Len.DT_EFF_MOVI;
        public static final int IMP_OPER = IMP_VERS + Len.IMP_VERS;
        public static final int DATA_NAV = IMP_OPER + Len.IMP_OPER;
        public static final int VAL_QUO_NAV = DATA_NAV + Len.DATA_NAV;
        public static final int NUMERO_QUO = VAL_QUO_NAV + Len.VAL_QUO_NAV;
        public static final int FLR7 = NUMERO_QUO + Len.NUMERO_QUO;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wrinTabTassiMinGar(int idx) {
            return IMP_LRD_RISC_PARZ_AC_GA + Len.IMP_LRD_RISC_PARZ_AC_GA + idx * Len.TAB_TASSI_MIN_GAR;
        }

        public static int wrinDtIniPerInv(int idx) {
            return wrinTabTassiMinGar(idx);
        }

        public static int wrinDtFinPerInv(int idx) {
            return wrinDtIniPerInv(idx) + Len.DT_INI_PER_INV;
        }

        public static int wrinTassoMinGar(int idx) {
            return wrinDtFinPerInv(idx) + Len.DT_FIN_PER_INV;
        }

        public static int wrinDtAttrib(int idx) {
            return wrinTassoMinGar(idx) + Len.TASSO_MIN_GAR;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ANNO_RIF = 4;
        public static final int DATA_ELAB = 8;
        public static final int DATA_RICH_DA = 8;
        public static final int DATA_RICH_A = 8;
        public static final int COD_AG_GEST = 5;
        public static final int TP_ACQ_CONTR_GEST = 5;
        public static final int COD_ACQ_CONTR_GEST = 5;
        public static final int COD_PUNTO_RETE_FIN_GEST = 5;
        public static final int COD_CAN_GEST = 5;
        public static final int COD_AG_ACQ = 5;
        public static final int TP_ACQ_CONTR_ACQ = 5;
        public static final int COD_ACQ_CONTR_ACQ = 5;
        public static final int COD_PUNTO_RETE_FIN_ACQ = 5;
        public static final int COD_CAN_ACQ = 5;
        public static final int TP_POLI = 2;
        public static final int DT_EMIS_POLI = 8;
        public static final int DT_DECOR_POLI = 8;
        public static final int DT_SCAD_POLI = 8;
        public static final int TP_STAT_POLI = 2;
        public static final int TP_CAUS_POLI = 2;
        public static final int DUR_ANNI_POLI = 5;
        public static final int DUR_MESI_POLI = 5;
        public static final int DUR_GIORNI_POLI = 5;
        public static final int COD_CONVENZ_POLI = 12;
        public static final int DENOMINAZ_PRODOTTO = 50;
        public static final int DT_RIF_DA = 8;
        public static final int DT_RIF_A = 8;
        public static final int DT_EMIS_ADES = 8;
        public static final int DT_DECOR_ADES = 8;
        public static final int TP_STAT_ADES = 2;
        public static final int TP_CAUS_ADES = 2;
        public static final int DUR_ANNI_ADES = 5;
        public static final int DUR_MESI_ADES = 5;
        public static final int DUR_GIORNI_ADES = 5;
        public static final int CAPITALE_VINCOL = 15;
        public static final int DT_VINCOL = 8;
        public static final int RISC_TOT_DT_RIF_EST_CC = 15;
        public static final int CAP_LIQ_DT_RIF_EST_CC = 15;
        public static final int RISCAT_PER_QUIESC = 15;
        public static final int VAL_RIDUZ = 15;
        public static final int VALORE_IMPOSTA_AA_RIF_EC = 15;
        public static final int FLAG_PROD_CON_CEDOLE = 2;
        public static final int IMP_LOR_CED_LIQU_AP = 15;
        public static final int IMP_LOR_CED_LIQU = 15;
        public static final int SPESE_CED_LIQU = 15;
        public static final int IMPOST_SOST_CED_LIQU = 15;
        public static final int IMP_NET_CED_LIQU = 15;
        public static final int DT_ULT_LIQU_CED = 8;
        public static final int CC_PAESE = 2;
        public static final int CC_CHECK_DIGIT = 2;
        public static final int CC_CIN = 1;
        public static final int CC_ABI = 5;
        public static final int CC_CAB = 5;
        public static final int CC_NUM_CONTO = 20;
        public static final int IMP_CED_CAPIT = 15;
        public static final int DT_ULT_CAPIT_CED = 8;
        public static final int DT_ULT_TIT_CONT_INC = 8;
        public static final int TOT_CUM_PREMI_VERS = 15;
        public static final int TOT_CUM_PREMI_VERS_AP = 15;
        public static final int TOT_CUM_PRE_INVST_AA_RIF = 15;
        public static final int IMPOST_SOSTIT_TOTALE = 15;
        public static final int PRESTAZIONE_AP_POL = 15;
        public static final int PRESTAZIONE_AC_POL = 15;
        public static final int IMP_LRD_RISC_PARZ_AP_POL = 15;
        public static final int IMP_LRD_RISC_PARZ_AC_POL = 15;
        public static final int ID_POLI = 9;
        public static final int ID_ADES = 9;
        public static final int COD_PROD_PARTNER = 4;
        public static final int COD_PROD_ACT = 12;
        public static final int CAPITALE_PROTETTO = 15;
        public static final int COS_RUN_ASSVA = 15;
        public static final int COS_RUN_ASSVA_IDC = 15;
        public static final int COS_RUN_AMS_FNDINT = 15;
        public static final int FLR2 = 1596;
        public static final int COD_SOGG_CONTRA = 20;
        public static final int TP_PERS_CONTRA = 1;
        public static final int NOME_CONTRA = 50;
        public static final int COGNOME_CONTRA = 50;
        public static final int RAG_SOC_CONTRA = 100;
        public static final int INDIRIZZO_CONTRA = 100;
        public static final int LOCALITA_CONTRA = 50;
        public static final int CAP_CONTRA = 10;
        public static final int PROVINCIA_CONTRA = 4;
        public static final int PAESE_CONTRA = 50;
        public static final int SESSO_CONTRA = 1;
        public static final int COD_SOGG_ADEREN = 20;
        public static final int TP_PERS_ADEREN = 1;
        public static final int NOME_ADEREN = 50;
        public static final int COGNOME_ADEREN = 50;
        public static final int RAG_SOC_ADEREN = 100;
        public static final int INDIRIZZO_ADEREN = 100;
        public static final int LOCALITA_ADEREN = 50;
        public static final int CAP_ADEREN = 10;
        public static final int PROVINCIA_ADEREN = 4;
        public static final int PAESE_ADEREN = 50;
        public static final int SESSO_ADEREN = 1;
        public static final int COD_SOGG_ASSICU = 20;
        public static final int TP_PERS_ASSICU = 1;
        public static final int NOME_ASSICU = 50;
        public static final int COGNOME_ASSICU = 50;
        public static final int RAG_SOC_ASSICU = 100;
        public static final int FLAG_COINCID_ASSICU = 1;
        public static final int SESSO_ASSICU = 1;
        public static final int COD_SOGG_VINCOL = 20;
        public static final int TP_PERS_VINCOL = 1;
        public static final int NOME_VINCOL = 50;
        public static final int COGNOME_VINCOL = 50;
        public static final int RAG_SOC_VINCOL = 100;
        public static final int SESSO_VINCOL = 1;
        public static final int BENEFIC_MORTE = 400;
        public static final int BENEFIC_VITA = 400;
        public static final int FL_BEN_NORMAL = 2;
        public static final int ID_GAR = 9;
        public static final int COD_GAR = 12;
        public static final int DENOMINAZ_GAR = 50;
        public static final int TP_GAR = 2;
        public static final int TP_INVST_GAR = 2;
        public static final int TP_PERIOD_PREMIO = 2;
        public static final int FRAZIONAMENTO = 5;
        public static final int DUR_PAGAM_PREMI = 5;
        public static final int TASSO_TECNICO = 14;
        public static final int CUM_PREM_INVST_AA_RIF = 15;
        public static final int CUM_PREM_VERS_AA_RIF = 15;
        public static final int TASSE_PREM_VERS_AA_RIF = 15;
        public static final int PREST_MATUR_AA_RIF = 15;
        public static final int CAP_LIQ_GA = 15;
        public static final int CAP_LIQ_GA_ACC = 15;
        public static final int RISC_TOT_GA = 15;
        public static final int PRE_ATT_GAR = 15;
        public static final int PREST_INIZ_GAR = 15;
        public static final int CUM_PREM_VERS_EC_PREC = 15;
        public static final int TASSE_PREM_VERS_EC_PREC = 15;
        public static final int PREST_MATUR_EC_PREC = 15;
        public static final int REND_LOR_GEST_SEP_GAR = 14;
        public static final int ALIQ_RETROC_RICON_GAR = 6;
        public static final int TASSO_ANN_REND_RETROC_GAR = 14;
        public static final int PC_COMM_GEST_GAR = 6;
        public static final int TASSO_ANN_RIVAL_PREST_GAR = 14;
        public static final int IMP_LRD_RISC_PARZ_AP_GA = 15;
        public static final int IMP_LRD_RISC_PARZ_AC_GA = 15;
        public static final int DT_INI_PER_INV = 8;
        public static final int DT_FIN_PER_INV = 8;
        public static final int TASSO_MIN_GAR = 14;
        public static final int DT_ATTRIB = 8;
        public static final int TAB_TASSI_MIN_GAR = DT_INI_PER_INV + DT_FIN_PER_INV + TASSO_MIN_GAR + DT_ATTRIB;
        public static final int CAP_MIN_LIQU_SCAD_GAR = 15;
        public static final int CAP_AGG_COMPL_INF = 15;
        public static final int CAP_AGG_GAR_FAM = 15;
        public static final int REN_MIN_TRNUT = 14;
        public static final int REN_MIN_GARTO = 14;
        public static final int COD_FND_GAR = 20;
        public static final int DESC_FND_GAR = 50;
        public static final int VAL_RIDUZ_PRE_PAG = 15;
        public static final int VAL_RISC_PRE_PAG = 15;
        public static final int FL_STORNATE_ANN = 1;
        public static final int FL_CUM_GAR_STORNATE = 1;
        public static final int COD_FND = 20;
        public static final int DENOMINAZ_FONDO = 50;
        public static final int COD_GAR_FND = 12;
        public static final int NUM_QUO_DISINV_RISC_PARZ = 12;
        public static final int VAL_QUO_DISINV_RISC_PARZ = 15;
        public static final int NUM_QUO_DIS_RISC_PARZ_PRG = 12;
        public static final int VAL_QUO_DIS_RISC_PARZ_PRG = 15;
        public static final int NUM_QUO_DISINV_IMPOS_SOST = 12;
        public static final int VAL_QUO_DISINV_IMPOS_SOST = 15;
        public static final int NUM_QUO_DISINV_COMMGES = 12;
        public static final int VAL_QUO_DISINV_COMMGES = 15;
        public static final int NUM_QUO_DISINV_PREL_COSTI = 12;
        public static final int VAL_QUO_DISINV_PREL_COSTI = 15;
        public static final int NUM_QUO_DISINV_SWITCH = 12;
        public static final int VAL_QUO_DISINV_SWITCH = 15;
        public static final int NUM_QUO_INVST_SWITCH = 12;
        public static final int VAL_QUO_INVST_SWITCH = 15;
        public static final int NUM_QUO_INVST_VERS = 12;
        public static final int VAL_QUO_INVST_VERS = 15;
        public static final int NUM_QUO_INVST_REBATE = 12;
        public static final int VAL_QUO_INVST_REBATE = 15;
        public static final int NUM_QUO_INVST_PURO_RISC = 12;
        public static final int VAL_QUO_INVST_PURO_RISC = 15;
        public static final int NUM_QUO_ATTUALE = 12;
        public static final int CNTRVAL_ATTUALE = 15;
        public static final int DT_VALOR_QUO_ATTUALE = 8;
        public static final int VAL_QUO_ATTUALE = 12;
        public static final int NUM_QUO_PREC = 12;
        public static final int CNTRVAL_PREC = 15;
        public static final int DT_VALOR_QUO_PREC = 8;
        public static final int VAL_QUO_PREC = 12;
        public static final int ID_GAR_FND = 9;
        public static final int NUM_QUO_DISINV_COMP_NAV = 12;
        public static final int VAL_QUO_DISINV_COMP_NAV = 15;
        public static final int NUM_QUO_INVST_COMP_NAV = 12;
        public static final int VAL_QUO_INVST_COMP_NAV = 15;
        public static final int IMP_LORDO_RISC_PARZ = 15;
        public static final int PREMI_ATTIVI_PRE_RISC = 15;
        public static final int PREMI_ATTIVI_POST_RISC = 15;
        public static final int DELTA_PREMI_ATTIVI = 15;
        public static final int DT_EFFETTO_LIQ = 8;
        public static final int RP_TP_MOVI = 5;
        public static final int ID_GARANZIA = 9;
        public static final int CODICE_GARANZIA = 12;
        public static final int DESC_GARANZIA = 50;
        public static final int CODICE_FND = 20;
        public static final int DESCR_FND = 50;
        public static final int COMPETENZA_MOVI = 8;
        public static final int ID_MOVI = 9;
        public static final int TP_MOVI = 5;
        public static final int DESC_TP_MOVI = 50;
        public static final int VERSO_MOVI = 2;
        public static final int DT_EFF_MOVI = 8;
        public static final int IMP_VERS = 15;
        public static final int IMP_OPER = 15;
        public static final int DATA_NAV = 8;
        public static final int VAL_QUO_NAV = 15;
        public static final int NUMERO_QUO = 12;
        public static final int WRIN_TP_RECORD_DATI = 2282;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIN_RISC_TOT_DT_RIF_EST_CC = 12;
            public static final int WRIN_CAP_LIQ_DT_RIF_EST_CC = 12;
            public static final int WRIN_IMP_LOR_CED_LIQU = 12;
            public static final int WRIN_TOT_CUM_PREMI_VERS = 12;
            public static final int WRIN_TOT_CUM_PREMI_VERS_AP = 12;
            public static final int WRIN_TOT_CUM_PRE_INVST_AA_RIF = 12;
            public static final int WRIN_PRESTAZIONE_AP_POL = 12;
            public static final int WRIN_PRESTAZIONE_AC_POL = 12;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AP_POL = 12;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AC_POL = 12;
            public static final int WRIN_TASSO_TECNICO = 5;
            public static final int WRIN_CUM_PREM_INVST_AA_RIF = 12;
            public static final int WRIN_CUM_PREM_VERS_AA_RIF = 12;
            public static final int WRIN_PREST_MATUR_AA_RIF = 12;
            public static final int WRIN_CAP_LIQ_GA = 12;
            public static final int WRIN_RISC_TOT_GA = 12;
            public static final int WRIN_CUM_PREM_VERS_EC_PREC = 12;
            public static final int WRIN_PREST_MATUR_EC_PREC = 12;
            public static final int WRIN_REND_LOR_GEST_SEP_GAR = 5;
            public static final int WRIN_ALIQ_RETROC_RICON_GAR = 3;
            public static final int WRIN_TASSO_ANN_REND_RETROC_GAR = 5;
            public static final int WRIN_PC_COMM_GEST_GAR = 3;
            public static final int WRIN_TASSO_ANN_RIVAL_PREST_GAR = 5;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AP_GA = 12;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AC_GA = 12;
            public static final int WRIN_REN_MIN_TRNUT = 5;
            public static final int WRIN_REN_MIN_GARTO = 5;
            public static final int WRIN_NUM_QUO_DISINV_RISC_PARZ = 7;
            public static final int WRIN_NUM_QUO_DIS_RISC_PARZ_PRG = 7;
            public static final int WRIN_NUM_QUO_DISINV_IMPOS_SOST = 7;
            public static final int WRIN_NUM_QUO_DISINV_COMMGES = 7;
            public static final int WRIN_NUM_QUO_DISINV_PREL_COSTI = 7;
            public static final int WRIN_NUM_QUO_DISINV_SWITCH = 7;
            public static final int WRIN_NUM_QUO_INVST_SWITCH = 7;
            public static final int WRIN_NUM_QUO_INVST_VERS = 7;
            public static final int WRIN_NUM_QUO_INVST_REBATE = 7;
            public static final int WRIN_NUM_QUO_INVST_PURO_RISC = 7;
            public static final int WRIN_NUM_QUO_ATTUALE = 7;
            public static final int WRIN_CNTRVAL_ATTUALE = 12;
            public static final int WRIN_NUM_QUO_PREC = 7;
            public static final int WRIN_CNTRVAL_PREC = 12;
            public static final int WRIN_NUM_QUO_DISINV_COMP_NAV = 7;
            public static final int WRIN_NUM_QUO_INVST_COMP_NAV = 7;
            public static final int WRIN_IMP_LORDO_RISC_PARZ = 12;
            public static final int WRIN_IMP_VERS = 12;
            public static final int WRIN_IMP_OPER = 12;
            public static final int WRIN_NUMERO_QUO = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRIN_RISC_TOT_DT_RIF_EST_CC = 3;
            public static final int WRIN_CAP_LIQ_DT_RIF_EST_CC = 3;
            public static final int WRIN_IMP_LOR_CED_LIQU = 3;
            public static final int WRIN_TOT_CUM_PREMI_VERS = 3;
            public static final int WRIN_TOT_CUM_PREMI_VERS_AP = 3;
            public static final int WRIN_TOT_CUM_PRE_INVST_AA_RIF = 3;
            public static final int WRIN_PRESTAZIONE_AP_POL = 3;
            public static final int WRIN_PRESTAZIONE_AC_POL = 3;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AP_POL = 3;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AC_POL = 3;
            public static final int WRIN_TASSO_TECNICO = 9;
            public static final int WRIN_CUM_PREM_INVST_AA_RIF = 3;
            public static final int WRIN_CUM_PREM_VERS_AA_RIF = 3;
            public static final int WRIN_PREST_MATUR_AA_RIF = 3;
            public static final int WRIN_CAP_LIQ_GA = 3;
            public static final int WRIN_RISC_TOT_GA = 3;
            public static final int WRIN_CUM_PREM_VERS_EC_PREC = 3;
            public static final int WRIN_PREST_MATUR_EC_PREC = 3;
            public static final int WRIN_REND_LOR_GEST_SEP_GAR = 9;
            public static final int WRIN_ALIQ_RETROC_RICON_GAR = 3;
            public static final int WRIN_TASSO_ANN_REND_RETROC_GAR = 9;
            public static final int WRIN_PC_COMM_GEST_GAR = 3;
            public static final int WRIN_TASSO_ANN_RIVAL_PREST_GAR = 9;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AP_GA = 3;
            public static final int WRIN_IMP_LRD_RISC_PARZ_AC_GA = 3;
            public static final int WRIN_REN_MIN_TRNUT = 9;
            public static final int WRIN_REN_MIN_GARTO = 9;
            public static final int WRIN_NUM_QUO_DISINV_RISC_PARZ = 5;
            public static final int WRIN_NUM_QUO_DIS_RISC_PARZ_PRG = 5;
            public static final int WRIN_NUM_QUO_DISINV_IMPOS_SOST = 5;
            public static final int WRIN_NUM_QUO_DISINV_COMMGES = 5;
            public static final int WRIN_NUM_QUO_DISINV_PREL_COSTI = 5;
            public static final int WRIN_NUM_QUO_DISINV_SWITCH = 5;
            public static final int WRIN_NUM_QUO_INVST_SWITCH = 5;
            public static final int WRIN_NUM_QUO_INVST_VERS = 5;
            public static final int WRIN_NUM_QUO_INVST_REBATE = 5;
            public static final int WRIN_NUM_QUO_INVST_PURO_RISC = 5;
            public static final int WRIN_NUM_QUO_ATTUALE = 5;
            public static final int WRIN_CNTRVAL_ATTUALE = 3;
            public static final int WRIN_NUM_QUO_PREC = 5;
            public static final int WRIN_CNTRVAL_PREC = 3;
            public static final int WRIN_NUM_QUO_DISINV_COMP_NAV = 5;
            public static final int WRIN_NUM_QUO_INVST_COMP_NAV = 5;
            public static final int WRIN_IMP_LORDO_RISC_PARZ = 3;
            public static final int WRIN_IMP_VERS = 3;
            public static final int WRIN_IMP_OPER = 3;
            public static final int WRIN_NUMERO_QUO = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
