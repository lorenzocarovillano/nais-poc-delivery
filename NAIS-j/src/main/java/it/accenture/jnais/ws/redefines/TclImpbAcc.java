package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-ACC<br>
 * Variable: TCL-IMPB-ACC from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbAcc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbAcc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_ACC;
    }

    public void setTclImpbAcc(AfDecimal tclImpbAcc) {
        writeDecimalAsPacked(Pos.TCL_IMPB_ACC, tclImpbAcc.copy());
    }

    public void setTclImpbAccFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_ACC, Pos.TCL_IMPB_ACC);
    }

    /**Original name: TCL-IMPB-ACC<br>*/
    public AfDecimal getTclImpbAcc() {
        return readPackedAsDecimal(Pos.TCL_IMPB_ACC, Len.Int.TCL_IMPB_ACC, Len.Fract.TCL_IMPB_ACC);
    }

    public byte[] getTclImpbAccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_ACC, Pos.TCL_IMPB_ACC);
        return buffer;
    }

    public void setTclImpbAccNull(String tclImpbAccNull) {
        writeString(Pos.TCL_IMPB_ACC_NULL, tclImpbAccNull, Len.TCL_IMPB_ACC_NULL);
    }

    /**Original name: TCL-IMPB-ACC-NULL<br>*/
    public String getTclImpbAccNull() {
        return readString(Pos.TCL_IMPB_ACC_NULL, Len.TCL_IMPB_ACC_NULL);
    }

    public String getTclImpbAccNullFormatted() {
        return Functions.padBlanks(getTclImpbAccNull(), Len.TCL_IMPB_ACC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_ACC = 1;
        public static final int TCL_IMPB_ACC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_ACC = 8;
        public static final int TCL_IMPB_ACC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_ACC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_ACC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
