package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: SDI-ID-MOVI-CHIU<br>
 * Variable: SDI-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class SdiIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public SdiIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.SDI_ID_MOVI_CHIU;
    }

    public void setSdiIdMoviChiu(int sdiIdMoviChiu) {
        writeIntAsPacked(Pos.SDI_ID_MOVI_CHIU, sdiIdMoviChiu, Len.Int.SDI_ID_MOVI_CHIU);
    }

    public void setSdiIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.SDI_ID_MOVI_CHIU, Pos.SDI_ID_MOVI_CHIU);
    }

    /**Original name: SDI-ID-MOVI-CHIU<br>*/
    public int getSdiIdMoviChiu() {
        return readPackedAsInt(Pos.SDI_ID_MOVI_CHIU, Len.Int.SDI_ID_MOVI_CHIU);
    }

    public byte[] getSdiIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.SDI_ID_MOVI_CHIU, Pos.SDI_ID_MOVI_CHIU);
        return buffer;
    }

    public void setSdiIdMoviChiuNull(String sdiIdMoviChiuNull) {
        writeString(Pos.SDI_ID_MOVI_CHIU_NULL, sdiIdMoviChiuNull, Len.SDI_ID_MOVI_CHIU_NULL);
    }

    /**Original name: SDI-ID-MOVI-CHIU-NULL<br>*/
    public String getSdiIdMoviChiuNull() {
        return readString(Pos.SDI_ID_MOVI_CHIU_NULL, Len.SDI_ID_MOVI_CHIU_NULL);
    }

    public String getSdiIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getSdiIdMoviChiuNull(), Len.SDI_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int SDI_ID_MOVI_CHIU = 1;
        public static final int SDI_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int SDI_ID_MOVI_CHIU = 5;
        public static final int SDI_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int SDI_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
