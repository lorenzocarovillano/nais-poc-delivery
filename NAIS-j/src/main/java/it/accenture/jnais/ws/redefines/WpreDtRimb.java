package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPRE-DT-RIMB<br>
 * Variable: WPRE-DT-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreDtRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreDtRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_DT_RIMB;
    }

    public void setWpreDtRimb(int wpreDtRimb) {
        writeIntAsPacked(Pos.WPRE_DT_RIMB, wpreDtRimb, Len.Int.WPRE_DT_RIMB);
    }

    public void setWpreDtRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_DT_RIMB, Pos.WPRE_DT_RIMB);
    }

    /**Original name: WPRE-DT-RIMB<br>*/
    public int getWpreDtRimb() {
        return readPackedAsInt(Pos.WPRE_DT_RIMB, Len.Int.WPRE_DT_RIMB);
    }

    public byte[] getWpreDtRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_DT_RIMB, Pos.WPRE_DT_RIMB);
        return buffer;
    }

    public void initWpreDtRimbSpaces() {
        fill(Pos.WPRE_DT_RIMB, Len.WPRE_DT_RIMB, Types.SPACE_CHAR);
    }

    public void setWpreDtRimbNull(String wpreDtRimbNull) {
        writeString(Pos.WPRE_DT_RIMB_NULL, wpreDtRimbNull, Len.WPRE_DT_RIMB_NULL);
    }

    /**Original name: WPRE-DT-RIMB-NULL<br>*/
    public String getWpreDtRimbNull() {
        return readString(Pos.WPRE_DT_RIMB_NULL, Len.WPRE_DT_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_DT_RIMB = 1;
        public static final int WPRE_DT_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_DT_RIMB = 5;
        public static final int WPRE_DT_RIMB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_DT_RIMB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
