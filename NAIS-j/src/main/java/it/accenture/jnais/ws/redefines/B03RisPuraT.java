package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-RIS-PURA-T<br>
 * Variable: B03-RIS-PURA-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03RisPuraT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03RisPuraT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_RIS_PURA_T;
    }

    public void setB03RisPuraT(AfDecimal b03RisPuraT) {
        writeDecimalAsPacked(Pos.B03_RIS_PURA_T, b03RisPuraT.copy());
    }

    public void setB03RisPuraTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_RIS_PURA_T, Pos.B03_RIS_PURA_T);
    }

    /**Original name: B03-RIS-PURA-T<br>*/
    public AfDecimal getB03RisPuraT() {
        return readPackedAsDecimal(Pos.B03_RIS_PURA_T, Len.Int.B03_RIS_PURA_T, Len.Fract.B03_RIS_PURA_T);
    }

    public byte[] getB03RisPuraTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_RIS_PURA_T, Pos.B03_RIS_PURA_T);
        return buffer;
    }

    public void setB03RisPuraTNull(String b03RisPuraTNull) {
        writeString(Pos.B03_RIS_PURA_T_NULL, b03RisPuraTNull, Len.B03_RIS_PURA_T_NULL);
    }

    /**Original name: B03-RIS-PURA-T-NULL<br>*/
    public String getB03RisPuraTNull() {
        return readString(Pos.B03_RIS_PURA_T_NULL, Len.B03_RIS_PURA_T_NULL);
    }

    public String getB03RisPuraTNullFormatted() {
        return Functions.padBlanks(getB03RisPuraTNull(), Len.B03_RIS_PURA_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_RIS_PURA_T = 1;
        public static final int B03_RIS_PURA_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_RIS_PURA_T = 8;
        public static final int B03_RIS_PURA_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_RIS_PURA_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_RIS_PURA_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
