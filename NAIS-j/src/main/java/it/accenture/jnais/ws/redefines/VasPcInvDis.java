package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-PC-INV-DIS<br>
 * Variable: VAS-PC-INV-DIS from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasPcInvDis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasPcInvDis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_PC_INV_DIS;
    }

    public void setVasPcInvDis(AfDecimal vasPcInvDis) {
        writeDecimalAsPacked(Pos.VAS_PC_INV_DIS, vasPcInvDis.copy());
    }

    public void setVasPcInvDisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_PC_INV_DIS, Pos.VAS_PC_INV_DIS);
    }

    /**Original name: VAS-PC-INV-DIS<br>*/
    public AfDecimal getVasPcInvDis() {
        return readPackedAsDecimal(Pos.VAS_PC_INV_DIS, Len.Int.VAS_PC_INV_DIS, Len.Fract.VAS_PC_INV_DIS);
    }

    public byte[] getVasPcInvDisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_PC_INV_DIS, Pos.VAS_PC_INV_DIS);
        return buffer;
    }

    public void setVasPcInvDisNull(String vasPcInvDisNull) {
        writeString(Pos.VAS_PC_INV_DIS_NULL, vasPcInvDisNull, Len.VAS_PC_INV_DIS_NULL);
    }

    /**Original name: VAS-PC-INV-DIS-NULL<br>*/
    public String getVasPcInvDisNull() {
        return readString(Pos.VAS_PC_INV_DIS_NULL, Len.VAS_PC_INV_DIS_NULL);
    }

    public String getVasPcInvDisNullFormatted() {
        return Functions.padBlanks(getVasPcInvDisNull(), Len.VAS_PC_INV_DIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_PC_INV_DIS = 1;
        public static final int VAS_PC_INV_DIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_PC_INV_DIS = 8;
        public static final int VAS_PC_INV_DIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_PC_INV_DIS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_PC_INV_DIS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
