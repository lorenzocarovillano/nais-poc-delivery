package it.accenture.jnais.ws;

import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndRappAna;
import it.accenture.jnais.copy.Ldbv1241;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1240<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1240Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-RAPP-ANA
    private IndRappAna indRappAna = new IndRappAna();
    //Original name: RAPP-ANA-DB
    private DettTitContDb rappAnaDb = new DettTitContDb();
    //Original name: LDBV1241
    private Ldbv1241 ldbv1241 = new Ldbv1241();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndRappAna getIndRappAna() {
        return indRappAna;
    }

    public Ldbv1241 getLdbv1241() {
        return ldbv1241;
    }

    public DettTitContDb getRappAnaDb() {
        return rappAnaDb;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
