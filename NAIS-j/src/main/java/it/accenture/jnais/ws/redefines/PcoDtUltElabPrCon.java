package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-PR-CON<br>
 * Variable: PCO-DT-ULT-ELAB-PR-CON from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabPrCon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabPrCon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_PR_CON;
    }

    public void setPcoDtUltElabPrCon(int pcoDtUltElabPrCon) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_PR_CON, pcoDtUltElabPrCon, Len.Int.PCO_DT_ULT_ELAB_PR_CON);
    }

    public void setPcoDtUltElabPrConFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PR_CON, Pos.PCO_DT_ULT_ELAB_PR_CON);
    }

    /**Original name: PCO-DT-ULT-ELAB-PR-CON<br>*/
    public int getPcoDtUltElabPrCon() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_PR_CON, Len.Int.PCO_DT_ULT_ELAB_PR_CON);
    }

    public byte[] getPcoDtUltElabPrConAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_PR_CON, Pos.PCO_DT_ULT_ELAB_PR_CON);
        return buffer;
    }

    public void initPcoDtUltElabPrConHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_PR_CON, Len.PCO_DT_ULT_ELAB_PR_CON, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabPrConNull(String pcoDtUltElabPrConNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_PR_CON_NULL, pcoDtUltElabPrConNull, Len.PCO_DT_ULT_ELAB_PR_CON_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-PR-CON-NULL<br>*/
    public String getPcoDtUltElabPrConNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_PR_CON_NULL, Len.PCO_DT_ULT_ELAB_PR_CON_NULL);
    }

    public String getPcoDtUltElabPrConNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabPrConNull(), Len.PCO_DT_ULT_ELAB_PR_CON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PR_CON = 1;
        public static final int PCO_DT_ULT_ELAB_PR_CON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_PR_CON = 5;
        public static final int PCO_DT_ULT_ELAB_PR_CON_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_PR_CON = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
