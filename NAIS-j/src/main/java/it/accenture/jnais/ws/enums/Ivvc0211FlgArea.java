package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IVVC0211-FLG-AREA<br>
 * Variable: IVVC0211-FLG-AREA from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211FlgArea {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLG_AREA);
    public static final String VE = "VE";
    public static final String PV = "PV";

    //==== METHODS ====
    public void setFlgArea(String flgArea) {
        this.value = Functions.subString(flgArea, Len.FLG_AREA);
    }

    public String getFlgArea() {
        return this.value;
    }

    public boolean isVe() {
        return value.equals(VE);
    }

    public void setS211AreaVe() {
        value = VE;
    }

    public boolean isPv() {
        return value.equals(PV);
    }

    public void setS211AreaPv() {
        value = PV;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLG_AREA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
