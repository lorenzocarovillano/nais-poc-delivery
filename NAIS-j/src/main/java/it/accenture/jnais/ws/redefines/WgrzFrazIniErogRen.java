package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-FRAZ-INI-EROG-REN<br>
 * Variable: WGRZ-FRAZ-INI-EROG-REN from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzFrazIniErogRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzFrazIniErogRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_FRAZ_INI_EROG_REN;
    }

    public void setWgrzFrazIniErogRen(int wgrzFrazIniErogRen) {
        writeIntAsPacked(Pos.WGRZ_FRAZ_INI_EROG_REN, wgrzFrazIniErogRen, Len.Int.WGRZ_FRAZ_INI_EROG_REN);
    }

    public void setWgrzFrazIniErogRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_FRAZ_INI_EROG_REN, Pos.WGRZ_FRAZ_INI_EROG_REN);
    }

    /**Original name: WGRZ-FRAZ-INI-EROG-REN<br>*/
    public int getWgrzFrazIniErogRen() {
        return readPackedAsInt(Pos.WGRZ_FRAZ_INI_EROG_REN, Len.Int.WGRZ_FRAZ_INI_EROG_REN);
    }

    public byte[] getWgrzFrazIniErogRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_FRAZ_INI_EROG_REN, Pos.WGRZ_FRAZ_INI_EROG_REN);
        return buffer;
    }

    public void initWgrzFrazIniErogRenSpaces() {
        fill(Pos.WGRZ_FRAZ_INI_EROG_REN, Len.WGRZ_FRAZ_INI_EROG_REN, Types.SPACE_CHAR);
    }

    public void setWgrzFrazIniErogRenNull(String wgrzFrazIniErogRenNull) {
        writeString(Pos.WGRZ_FRAZ_INI_EROG_REN_NULL, wgrzFrazIniErogRenNull, Len.WGRZ_FRAZ_INI_EROG_REN_NULL);
    }

    /**Original name: WGRZ-FRAZ-INI-EROG-REN-NULL<br>*/
    public String getWgrzFrazIniErogRenNull() {
        return readString(Pos.WGRZ_FRAZ_INI_EROG_REN_NULL, Len.WGRZ_FRAZ_INI_EROG_REN_NULL);
    }

    public String getWgrzFrazIniErogRenNullFormatted() {
        return Functions.padBlanks(getWgrzFrazIniErogRenNull(), Len.WGRZ_FRAZ_INI_EROG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_FRAZ_INI_EROG_REN = 1;
        public static final int WGRZ_FRAZ_INI_EROG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_FRAZ_INI_EROG_REN = 3;
        public static final int WGRZ_FRAZ_INI_EROG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_FRAZ_INI_EROG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
