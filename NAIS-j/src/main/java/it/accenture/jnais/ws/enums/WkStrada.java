package it.accenture.jnais.ws.enums;

/**Original name: WK-STRADA<br>
 * Variable: WK-STRADA from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkStrada {

    //==== PROPERTIES ====
    private char value = 'S';
    public static final char SEQ = 'S';
    public static final char TAB = 'T';

    //==== METHODS ====
    public char getWkStrada() {
        return this.value;
    }

    public boolean isTab() {
        return value == TAB;
    }
}
