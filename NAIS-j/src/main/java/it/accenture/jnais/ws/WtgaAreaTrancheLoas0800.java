package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: WTGA-AREA-TRANCHE<br>
 * Variable: WTGA-AREA-TRANCHE from program LOAS0800<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WtgaAreaTrancheLoas0800 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 1250;
    //Original name: WTGA-ELE-TRAN-MAX
    private short eleTranMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGA-TAB-TRAN
    private LazyArrayCopy<W1tgaTabTran> tabTran = new LazyArrayCopy<W1tgaTabTran>(new W1tgaTabTran(), 1, TAB_TRAN_MAXOCCURS);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_AREA_TRANCHE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWtgaAreaTrancheBytes(buf);
    }

    public String getWtgaAreaTrancheFormatted() {
        return MarshalByteExt.bufferToStr(getWtgaAreaTrancheBytes());
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer) {
        setWtgaAreaTrancheBytes(buffer, 1);
    }

    public byte[] getWtgaAreaTrancheBytes() {
        byte[] buffer = new byte[Len.WTGA_AREA_TRANCHE];
        return getWtgaAreaTrancheBytes(buffer, 1);
    }

    public void setWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        eleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabTran.get(idx - 1).setW1tgaTabTranBytes(buffer, position);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
            }
            else {
                W1tgaTabTran temp_tabTran = new W1tgaTabTran();
                temp_tabTran.initW1tgaTabTranSpaces();
                getTabTranObj().fill(temp_tabTran);
                position += W1tgaTabTran.Len.W1TGA_TAB_TRAN * (TAB_TRAN_MAXOCCURS - idx + 1);
                break;
            }
        }
    }

    public byte[] getWtgaAreaTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleTranMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_TRAN_MAXOCCURS; idx++) {
            tabTran.get(idx - 1).getW1tgaTabTranBytes(buffer, position);
            position += W1tgaTabTran.Len.W1TGA_TAB_TRAN;
        }
        return buffer;
    }

    public void setEleTranMax(short eleTranMax) {
        this.eleTranMax = eleTranMax;
    }

    public short getEleTranMax() {
        return this.eleTranMax;
    }

    public W1tgaTabTran getTabTran(int idx) {
        return tabTran.get(idx - 1);
    }

    public LazyArrayCopy<W1tgaTabTran> getTabTranObj() {
        return tabTran;
    }

    @Override
    public byte[] serialize() {
        return getWtgaAreaTrancheBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_TRANCHE = ELE_TRAN_MAX + WtgaAreaTrancheLoas0800.TAB_TRAN_MAXOCCURS * W1tgaTabTran.Len.W1TGA_TAB_TRAN;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
