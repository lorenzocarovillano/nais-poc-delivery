package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvdco1Lvvs0001;
import it.accenture.jnais.ws.occurs.WadeTabAdes;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.occurs.WpmoTabParamMov;
import it.accenture.jnais.ws.occurs.WpogTabParamOgg;
import it.accenture.jnais.ws.occurs.WtgaTabTran;

/**Original name: AREE-APPOGGIO<br>
 * Variable: AREE-APPOGGIO from program LVVS0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class AreeAppoggio {

    //==== PROPERTIES ====
    public static final int DGRZ_TAB_GAR_MAXOCCURS = 20;
    public static final int DADE_TAB_ADES_MAXOCCURS = 1;
    public static final int DPMO_TAB_PARAM_MOV_MAXOCCURS = 100;
    public static final int DTGA_TAB_TRAN_MAXOCCURS = 20;
    public static final int DPOG_TAB_PARAM_OGG_MAXOCCURS = 200;
    //Original name: DDCO-ELE-DCO-MAX
    private short ddcoEleDcoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVDCO1
    private Lccvdco1Lvvs0001 lccvdco1 = new Lccvdco1Lvvs0001();
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DGRZ-TAB-GAR
    private WgrzTabGar[] dgrzTabGar = new WgrzTabGar[DGRZ_TAB_GAR_MAXOCCURS];
    //Original name: DADE-ELE-ADES-MAX
    private short dadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DADE-TAB-ADES
    private WadeTabAdes[] dadeTabAdes = new WadeTabAdes[DADE_TAB_ADES_MAXOCCURS];
    //Original name: DPMO-ELE-PARAM-MOVI-MAX
    private short dpmoEleParamMoviMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DPMO-TAB-PARAM-MOV
    private WpmoTabParamMov[] dpmoTabParamMov = new WpmoTabParamMov[DPMO_TAB_PARAM_MOV_MAXOCCURS];
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTGA-TAB-TRAN
    private WtgaTabTran[] dtgaTabTran = new WtgaTabTran[DTGA_TAB_TRAN_MAXOCCURS];
    //Original name: DPOG-ELE-PARAM-OGG-MAX
    private short dpogEleParamOggMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DPOG-TAB-PARAM-OGG
    private WpogTabParamOgg[] dpogTabParamOgg = new WpogTabParamOgg[DPOG_TAB_PARAM_OGG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreeAppoggio() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dgrzTabGarIdx = 1; dgrzTabGarIdx <= DGRZ_TAB_GAR_MAXOCCURS; dgrzTabGarIdx++) {
            dgrzTabGar[dgrzTabGarIdx - 1] = new WgrzTabGar();
        }
        for (int dadeTabAdesIdx = 1; dadeTabAdesIdx <= DADE_TAB_ADES_MAXOCCURS; dadeTabAdesIdx++) {
            dadeTabAdes[dadeTabAdesIdx - 1] = new WadeTabAdes();
        }
        for (int dpmoTabParamMovIdx = 1; dpmoTabParamMovIdx <= DPMO_TAB_PARAM_MOV_MAXOCCURS; dpmoTabParamMovIdx++) {
            dpmoTabParamMov[dpmoTabParamMovIdx - 1] = new WpmoTabParamMov();
        }
        for (int dtgaTabTranIdx = 1; dtgaTabTranIdx <= DTGA_TAB_TRAN_MAXOCCURS; dtgaTabTranIdx++) {
            dtgaTabTran[dtgaTabTranIdx - 1] = new WtgaTabTran();
        }
        for (int dpogTabParamOggIdx = 1; dpogTabParamOggIdx <= DPOG_TAB_PARAM_OGG_MAXOCCURS; dpogTabParamOggIdx++) {
            dpogTabParamOgg[dpogTabParamOggIdx - 1] = new WpogTabParamOgg();
        }
    }

    public void setDdcoEleDcoMax(short ddcoEleDcoMax) {
        this.ddcoEleDcoMax = ddcoEleDcoMax;
    }

    public short getDdcoEleDcoMax() {
        return this.ddcoEleDcoMax;
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setDadeEleAdesMax(short dadeEleAdesMax) {
        this.dadeEleAdesMax = dadeEleAdesMax;
    }

    public short getDadeEleAdesMax() {
        return this.dadeEleAdesMax;
    }

    public void setDpmoEleParamMoviMax(short dpmoEleParamMoviMax) {
        this.dpmoEleParamMoviMax = dpmoEleParamMoviMax;
    }

    public short getDpmoEleParamMoviMax() {
        return this.dpmoEleParamMoviMax;
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDpogAreaParamOggFormatted(String data) {
        byte[] buffer = new byte[Len.DPOG_AREA_PARAM_OGG];
        MarshalByte.writeString(buffer, 1, data, Len.DPOG_AREA_PARAM_OGG);
        setDpogAreaParamOggBytes(buffer, 1);
    }

    public void setDpogAreaParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        dpogEleParamOggMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DPOG_TAB_PARAM_OGG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dpogTabParamOgg[idx - 1].setWpogTabParamOggBytes(buffer, position);
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
            else {
                dpogTabParamOgg[idx - 1].initWpogTabParamOggSpaces();
                position += WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;
            }
        }
    }

    public void setDpogEleParamOggMax(short dpogEleParamOggMax) {
        this.dpogEleParamOggMax = dpogEleParamOggMax;
    }

    public short getDpogEleParamOggMax() {
        return this.dpogEleParamOggMax;
    }

    public WadeTabAdes getDadeTabAdes(int idx) {
        return dadeTabAdes[idx - 1];
    }

    public WgrzTabGar getDgrzTabGar(int idx) {
        return dgrzTabGar[idx - 1];
    }

    public WpmoTabParamMov getDpmoTabParamMov(int idx) {
        return dpmoTabParamMov[idx - 1];
    }

    public WpogTabParamOgg getDpogTabParamOgg(int idx) {
        return dpogTabParamOgg[idx - 1];
    }

    public WtgaTabTran getDtgaTabTran(int idx) {
        return dtgaTabTran[idx - 1];
    }

    public Lccvdco1Lvvs0001 getLccvdco1() {
        return lccvdco1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DPOG_ELE_PARAM_OGG_MAX = 2;
        public static final int DPOG_AREA_PARAM_OGG = DPOG_ELE_PARAM_OGG_MAX + AreeAppoggio.DPOG_TAB_PARAM_OGG_MAXOCCURS * WpogTabParamOgg.Len.WPOG_TAB_PARAM_OGG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
