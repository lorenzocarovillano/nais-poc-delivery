package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ALQ-SCON<br>
 * Variable: TGA-ALQ-SCON from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaAlqScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaAlqScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ALQ_SCON;
    }

    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        writeDecimalAsPacked(Pos.TGA_ALQ_SCON, tgaAlqScon.copy());
    }

    public void setTgaAlqSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ALQ_SCON, Pos.TGA_ALQ_SCON);
    }

    /**Original name: TGA-ALQ-SCON<br>*/
    public AfDecimal getTgaAlqScon() {
        return readPackedAsDecimal(Pos.TGA_ALQ_SCON, Len.Int.TGA_ALQ_SCON, Len.Fract.TGA_ALQ_SCON);
    }

    public byte[] getTgaAlqSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ALQ_SCON, Pos.TGA_ALQ_SCON);
        return buffer;
    }

    public void setTgaAlqSconNull(String tgaAlqSconNull) {
        writeString(Pos.TGA_ALQ_SCON_NULL, tgaAlqSconNull, Len.TGA_ALQ_SCON_NULL);
    }

    /**Original name: TGA-ALQ-SCON-NULL<br>*/
    public String getTgaAlqSconNull() {
        return readString(Pos.TGA_ALQ_SCON_NULL, Len.TGA_ALQ_SCON_NULL);
    }

    public String getTgaAlqSconNullFormatted() {
        return Functions.padBlanks(getTgaAlqSconNull(), Len.TGA_ALQ_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_SCON = 1;
        public static final int TGA_ALQ_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ALQ_SCON = 4;
        public static final int TGA_ALQ_SCON_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_ALQ_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
