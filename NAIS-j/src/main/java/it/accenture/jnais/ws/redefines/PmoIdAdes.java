package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-ID-ADES<br>
 * Variable: PMO-ID-ADES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoIdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoIdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_ID_ADES;
    }

    public void setPmoIdAdes(int pmoIdAdes) {
        writeIntAsPacked(Pos.PMO_ID_ADES, pmoIdAdes, Len.Int.PMO_ID_ADES);
    }

    public void setPmoIdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_ID_ADES, Pos.PMO_ID_ADES);
    }

    /**Original name: PMO-ID-ADES<br>*/
    public int getPmoIdAdes() {
        return readPackedAsInt(Pos.PMO_ID_ADES, Len.Int.PMO_ID_ADES);
    }

    public byte[] getPmoIdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_ID_ADES, Pos.PMO_ID_ADES);
        return buffer;
    }

    public void initPmoIdAdesHighValues() {
        fill(Pos.PMO_ID_ADES, Len.PMO_ID_ADES, Types.HIGH_CHAR_VAL);
    }

    public void setPmoIdAdesNull(String pmoIdAdesNull) {
        writeString(Pos.PMO_ID_ADES_NULL, pmoIdAdesNull, Len.PMO_ID_ADES_NULL);
    }

    /**Original name: PMO-ID-ADES-NULL<br>*/
    public String getPmoIdAdesNull() {
        return readString(Pos.PMO_ID_ADES_NULL, Len.PMO_ID_ADES_NULL);
    }

    public String getPmoIdAdesNullFormatted() {
        return Functions.padBlanks(getPmoIdAdesNull(), Len.PMO_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_ID_ADES = 1;
        public static final int PMO_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_ID_ADES = 5;
        public static final int PMO_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
