package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-TAX<br>
 * Variable: WTDR-TOT-TAX from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_TAX;
    }

    public void setWtdrTotTax(AfDecimal wtdrTotTax) {
        writeDecimalAsPacked(Pos.WTDR_TOT_TAX, wtdrTotTax.copy());
    }

    /**Original name: WTDR-TOT-TAX<br>*/
    public AfDecimal getWtdrTotTax() {
        return readPackedAsDecimal(Pos.WTDR_TOT_TAX, Len.Int.WTDR_TOT_TAX, Len.Fract.WTDR_TOT_TAX);
    }

    public void setWtdrTotTaxNull(String wtdrTotTaxNull) {
        writeString(Pos.WTDR_TOT_TAX_NULL, wtdrTotTaxNull, Len.WTDR_TOT_TAX_NULL);
    }

    /**Original name: WTDR-TOT-TAX-NULL<br>*/
    public String getWtdrTotTaxNull() {
        return readString(Pos.WTDR_TOT_TAX_NULL, Len.WTDR_TOT_TAX_NULL);
    }

    public String getWtdrTotTaxNullFormatted() {
        return Functions.padBlanks(getWtdrTotTaxNull(), Len.WTDR_TOT_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_TAX = 1;
        public static final int WTDR_TOT_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_TAX = 8;
        public static final int WTDR_TOT_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
