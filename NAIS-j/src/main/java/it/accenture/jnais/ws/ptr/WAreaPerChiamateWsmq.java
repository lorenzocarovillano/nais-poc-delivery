package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: W-AREA-PER-CHIAMATE-WSMQ<br>
 * Variable: W-AREA-PER-CHIAMATE-WSMQ from copybook IJCC0060<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class WAreaPerChiamateWsmq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WAreaPerChiamateWsmq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_AREA_PER_CHIAMATE_WSMQ;
    }

    public void setQmName(String qmName) {
        writeString(Pos.QM_NAME, qmName, Len.QM_NAME);
    }

    /**Original name: WSMQ-QM-NAME<br>*/
    public String getQmName() {
        return readString(Pos.QM_NAME, Len.QM_NAME);
    }

    public void setHconn(int hconn) {
        writeBinaryInt(Pos.HCONN, hconn);
    }

    /**Original name: WSMQ-HCONN<br>*/
    public int getHconn() {
        return readBinaryInt(Pos.HCONN);
    }

    public void setHconnJcall(int hconnJcall) {
        writeBinaryInt(Pos.HCONN_JCALL, hconnJcall);
    }

    /**Original name: WSMQ-HCONN-JCALL<br>*/
    public int getHconnJcall() {
        return readBinaryInt(Pos.HCONN_JCALL);
    }

    public void setqHandleJcall(int qHandleJcall) {
        writeBinaryInt(Pos.Q_HANDLE_JCALL, qHandleJcall);
    }

    /**Original name: WSMQ-Q-HANDLE-JCALL<br>*/
    public int getqHandleJcall() {
        return readBinaryInt(Pos.Q_HANDLE_JCALL);
    }

    public void setOptionsJcall(int optionsJcall) {
        writeBinaryInt(Pos.OPTIONS_JCALL, optionsJcall);
    }

    /**Original name: WSMQ-OPTIONS-JCALL<br>*/
    public int getOptionsJcall() {
        return readBinaryInt(Pos.OPTIONS_JCALL);
    }

    public void setCompletionCodeJcall(int completionCodeJcall) {
        writeBinaryInt(Pos.COMPLETION_CODE_JCALL, completionCodeJcall);
    }

    /**Original name: WSMQ-COMPLETION-CODE-JCALL<br>*/
    public int getCompletionCodeJcall() {
        return readBinaryInt(Pos.COMPLETION_CODE_JCALL);
    }

    public void setOpenCodeJcall(int openCodeJcall) {
        writeBinaryInt(Pos.OPEN_CODE_JCALL, openCodeJcall);
    }

    /**Original name: WSMQ-OPEN-CODE-JCALL<br>*/
    public int getOpenCodeJcall() {
        return readBinaryInt(Pos.OPEN_CODE_JCALL);
    }

    public void setConReasonJcall(int conReasonJcall) {
        writeBinaryInt(Pos.CON_REASON_JCALL, conReasonJcall);
    }

    /**Original name: WSMQ-CON-REASON-JCALL<br>*/
    public int getConReasonJcall() {
        return readBinaryInt(Pos.CON_REASON_JCALL);
    }

    public void setReasonJcall(int reasonJcall) {
        writeBinaryInt(Pos.REASON_JCALL, reasonJcall);
    }

    /**Original name: WSMQ-REASON-JCALL<br>*/
    public int getReasonJcall() {
        return readBinaryInt(Pos.REASON_JCALL);
    }

    public void setHconnJret(int hconnJret) {
        writeBinaryInt(Pos.HCONN_JRET, hconnJret);
    }

    /**Original name: WSMQ-HCONN-JRET<br>*/
    public int getHconnJret() {
        return readBinaryInt(Pos.HCONN_JRET);
    }

    public void setqHandleJret(int qHandleJret) {
        writeBinaryInt(Pos.Q_HANDLE_JRET, qHandleJret);
    }

    /**Original name: WSMQ-Q-HANDLE-JRET<br>*/
    public int getqHandleJret() {
        return readBinaryInt(Pos.Q_HANDLE_JRET);
    }

    public void setOptionsJret(int optionsJret) {
        writeBinaryInt(Pos.OPTIONS_JRET, optionsJret);
    }

    /**Original name: WSMQ-OPTIONS-JRET<br>*/
    public int getOptionsJret() {
        return readBinaryInt(Pos.OPTIONS_JRET);
    }

    public void setCompletionCodeJret(int completionCodeJret) {
        writeBinaryInt(Pos.COMPLETION_CODE_JRET, completionCodeJret);
    }

    /**Original name: WSMQ-COMPLETION-CODE-JRET<br>*/
    public int getCompletionCodeJret() {
        return readBinaryInt(Pos.COMPLETION_CODE_JRET);
    }

    public void setOpenCodeJret(int openCodeJret) {
        writeBinaryInt(Pos.OPEN_CODE_JRET, openCodeJret);
    }

    /**Original name: WSMQ-OPEN-CODE-JRET<br>*/
    public int getOpenCodeJret() {
        return readBinaryInt(Pos.OPEN_CODE_JRET);
    }

    public void setConReasonJret(int conReasonJret) {
        writeBinaryInt(Pos.CON_REASON_JRET, conReasonJret);
    }

    /**Original name: WSMQ-CON-REASON-JRET<br>*/
    public int getConReasonJret() {
        return readBinaryInt(Pos.CON_REASON_JRET);
    }

    public void setReasonJret(int reasonJret) {
        writeBinaryInt(Pos.REASON_JRET, reasonJret);
    }

    /**Original name: WSMQ-REASON-JRET<br>*/
    public int getReasonJret() {
        return readBinaryInt(Pos.REASON_JRET);
    }

    public void setBufferLengthJcall(int bufferLengthJcall) {
        writeBinaryInt(Pos.BUFFER_LENGTH_JCALL, bufferLengthJcall);
    }

    /**Original name: WSMQ-BUFFER-LENGTH-JCALL<br>*/
    public int getBufferLengthJcall() {
        return readBinaryInt(Pos.BUFFER_LENGTH_JCALL);
    }

    public void setBufferLengthJret(int bufferLengthJret) {
        writeBinaryInt(Pos.BUFFER_LENGTH_JRET, bufferLengthJret);
    }

    /**Original name: WSMQ-BUFFER-LENGTH-JRET<br>*/
    public int getBufferLengthJret() {
        return readBinaryInt(Pos.BUFFER_LENGTH_JRET);
    }

    public void setDataLength(int dataLength) {
        writeBinaryInt(Pos.DATA_LENGTH, dataLength);
    }

    /**Original name: WSMQ-DATA-LENGTH<br>*/
    public int getDataLength() {
        return readBinaryInt(Pos.DATA_LENGTH);
    }

    public void setTipoOperazione(String tipoOperazione) {
        writeString(Pos.TIPO_OPERAZIONE, tipoOperazione, Len.TIPO_OPERAZIONE);
    }

    /**Original name: WSMQ-TIPO-OPERAZIONE<br>*/
    public String getTipoOperazione() {
        return readString(Pos.TIPO_OPERAZIONE, Len.TIPO_OPERAZIONE);
    }

    public void setNomeCoda(String nomeCoda) {
        writeString(Pos.NOME_CODA, nomeCoda, Len.NOME_CODA);
    }

    /**Original name: WSMQ-NOME-CODA<br>*/
    public String getNomeCoda() {
        return readString(Pos.NOME_CODA, Len.NOME_CODA);
    }

    public void setTpUtilizzoApi(String tpUtilizzoApi) {
        writeString(Pos.TP_UTILIZZO_API, tpUtilizzoApi, Len.TP_UTILIZZO_API);
    }

    /**Original name: WSMQ-TP-UTILIZZO-API<br>*/
    public String getTpUtilizzoApi() {
        return readString(Pos.TP_UTILIZZO_API, Len.TP_UTILIZZO_API);
    }

    public void setMessaggioInterno(String messaggioInterno) {
        writeString(Pos.MESSAGGIO_INTERNO, messaggioInterno, Len.MESSAGGIO_INTERNO);
    }

    /**Original name: WSMQ-MESSAGGIO-INTERNO<br>*/
    public String getMessaggioInterno() {
        return readString(Pos.MESSAGGIO_INTERNO, Len.MESSAGGIO_INTERNO);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_AREA_PER_CHIAMATE_WSMQ = 1;
        public static final int QM_NAME = W_AREA_PER_CHIAMATE_WSMQ;
        public static final int HCONN = QM_NAME + Len.QM_NAME;
        public static final int HCONN_JCALL = HCONN + Len.HCONN;
        public static final int Q_HANDLE_JCALL = HCONN_JCALL + Len.HCONN_JCALL;
        public static final int OPTIONS_JCALL = Q_HANDLE_JCALL + Len.Q_HANDLE_JCALL;
        public static final int COMPLETION_CODE_JCALL = OPTIONS_JCALL + Len.OPTIONS_JCALL;
        public static final int OPEN_CODE_JCALL = COMPLETION_CODE_JCALL + Len.COMPLETION_CODE_JCALL;
        public static final int CON_REASON_JCALL = OPEN_CODE_JCALL + Len.OPEN_CODE_JCALL;
        public static final int REASON_JCALL = CON_REASON_JCALL + Len.CON_REASON_JCALL;
        public static final int HCONN_JRET = REASON_JCALL + Len.REASON_JCALL;
        public static final int Q_HANDLE_JRET = HCONN_JRET + Len.HCONN_JRET;
        public static final int OPTIONS_JRET = Q_HANDLE_JRET + Len.Q_HANDLE_JRET;
        public static final int COMPLETION_CODE_JRET = OPTIONS_JRET + Len.OPTIONS_JRET;
        public static final int OPEN_CODE_JRET = COMPLETION_CODE_JRET + Len.COMPLETION_CODE_JRET;
        public static final int CON_REASON_JRET = OPEN_CODE_JRET + Len.OPEN_CODE_JRET;
        public static final int REASON_JRET = CON_REASON_JRET + Len.CON_REASON_JRET;
        public static final int BUFFER_LENGTH_JCALL = REASON_JRET + Len.REASON_JRET;
        public static final int BUFFER_LENGTH_JRET = BUFFER_LENGTH_JCALL + Len.BUFFER_LENGTH_JCALL;
        public static final int DATA_LENGTH = BUFFER_LENGTH_JRET + Len.BUFFER_LENGTH_JRET;
        public static final int TIPO_CHIAMATA = DATA_LENGTH + Len.DATA_LENGTH;
        public static final int TIPO_OPERAZIONE = TIPO_CHIAMATA;
        public static final int NOME_CODA = TIPO_OPERAZIONE + Len.TIPO_OPERAZIONE;
        public static final int TP_UTILIZZO_API = NOME_CODA + Len.NOME_CODA;
        public static final int MESSAGGIO_INTERNO = TP_UTILIZZO_API + Len.TP_UTILIZZO_API;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int QM_NAME = 48;
        public static final int HCONN = 4;
        public static final int HCONN_JCALL = 4;
        public static final int Q_HANDLE_JCALL = 4;
        public static final int OPTIONS_JCALL = 4;
        public static final int COMPLETION_CODE_JCALL = 4;
        public static final int OPEN_CODE_JCALL = 4;
        public static final int CON_REASON_JCALL = 4;
        public static final int REASON_JCALL = 4;
        public static final int HCONN_JRET = 4;
        public static final int Q_HANDLE_JRET = 4;
        public static final int OPTIONS_JRET = 4;
        public static final int COMPLETION_CODE_JRET = 4;
        public static final int OPEN_CODE_JRET = 4;
        public static final int CON_REASON_JRET = 4;
        public static final int REASON_JRET = 4;
        public static final int BUFFER_LENGTH_JCALL = 4;
        public static final int BUFFER_LENGTH_JRET = 4;
        public static final int DATA_LENGTH = 4;
        public static final int TIPO_OPERAZIONE = 12;
        public static final int NOME_CODA = 48;
        public static final int TP_UTILIZZO_API = 3;
        public static final int MESSAGGIO_INTERNO = 50;
        public static final int TIPO_CHIAMATA = TIPO_OPERAZIONE + NOME_CODA + TP_UTILIZZO_API + MESSAGGIO_INTERNO;
        public static final int W_AREA_PER_CHIAMATE_WSMQ = QM_NAME + HCONN + HCONN_JCALL + Q_HANDLE_JCALL + OPTIONS_JCALL + COMPLETION_CODE_JCALL + OPEN_CODE_JCALL + CON_REASON_JCALL + REASON_JCALL + HCONN_JRET + Q_HANDLE_JRET + OPTIONS_JRET + COMPLETION_CODE_JRET + OPEN_CODE_JRET + CON_REASON_JRET + REASON_JRET + BUFFER_LENGTH_JCALL + BUFFER_LENGTH_JRET + DATA_LENGTH + TIPO_CHIAMATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
