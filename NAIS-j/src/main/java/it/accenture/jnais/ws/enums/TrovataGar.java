package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TROVATA-GAR<br>
 * Variable: TROVATA-GAR from program LCCS1900<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TrovataGar {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setTrovataGar(char trovataGar) {
        this.value = trovataGar;
    }

    public char getTrovataGar() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
