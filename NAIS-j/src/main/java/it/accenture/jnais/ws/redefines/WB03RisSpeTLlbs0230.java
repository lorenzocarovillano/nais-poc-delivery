package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-SPE-T<br>
 * Variable: W-B03-RIS-SPE-T from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisSpeTLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisSpeTLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_SPE_T;
    }

    public void setwB03RisSpeT(AfDecimal wB03RisSpeT) {
        writeDecimalAsPacked(Pos.W_B03_RIS_SPE_T, wB03RisSpeT.copy());
    }

    /**Original name: W-B03-RIS-SPE-T<br>*/
    public AfDecimal getwB03RisSpeT() {
        return readPackedAsDecimal(Pos.W_B03_RIS_SPE_T, Len.Int.W_B03_RIS_SPE_T, Len.Fract.W_B03_RIS_SPE_T);
    }

    public byte[] getwB03RisSpeTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_SPE_T, Pos.W_B03_RIS_SPE_T);
        return buffer;
    }

    public void setwB03RisSpeTNull(String wB03RisSpeTNull) {
        writeString(Pos.W_B03_RIS_SPE_T_NULL, wB03RisSpeTNull, Len.W_B03_RIS_SPE_T_NULL);
    }

    /**Original name: W-B03-RIS-SPE-T-NULL<br>*/
    public String getwB03RisSpeTNull() {
        return readString(Pos.W_B03_RIS_SPE_T_NULL, Len.W_B03_RIS_SPE_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_SPE_T = 1;
        public static final int W_B03_RIS_SPE_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_SPE_T = 8;
        public static final int W_B03_RIS_SPE_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_SPE_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_SPE_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
