package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-VARIABILI<br>
 * Variable: WS-VARIABILI from program LRGS0660<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsVariabiliLrgs0660 {

    //==== PROPERTIES ====
    //Original name: REC2-COGNOME-ASS
    private String rec2CognomeAss = DefaultValues.stringVal(Len.REC2_COGNOME_ASS);
    //Original name: REC2-NOME-ASS
    private String rec2NomeAss = DefaultValues.stringVal(Len.REC2_NOME_ASS);
    //Original name: WK-TABELLA
    private String wkTabella = DefaultValues.stringVal(Len.WK_TABELLA);
    //Original name: WK-LABEL
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);
    //Original name: WS-ERRORE
    private String wsErrore = DefaultValues.stringVal(Len.WS_ERRORE);
    //Original name: WK-LABEL-ERR
    private String wkLabelErr = DefaultValues.stringVal(Len.WK_LABEL_ERR);
    //Original name: TMP-IMP-OPER
    private String tmpImpOper = DefaultValues.stringVal(Len.TMP_IMP_OPER);
    //Original name: WK-CNTRL-37
    private AfDecimal wkCntrl37 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WK-RCODE
    private String wkRcode = DefaultValues.stringVal(Len.WK_RCODE);
    //Original name: WK-DATA-IN
    private String wkDataIn = DefaultValues.stringVal(Len.WK_DATA_IN);
    //Original name: WK-DATA-OUT
    private String wkDataOut = DefaultValues.stringVal(Len.WK_DATA_OUT);
    //Original name: WK-CONTROLLO
    private String wkControllo = DefaultValues.stringVal(Len.WK_CONTROLLO);
    //Original name: WK-FRAZ
    private String wkFraz = DefaultValues.stringVal(Len.WK_FRAZ);
    //Original name: WK-DT-DECOR
    private String wkDtDecor = DefaultValues.stringVal(Len.WK_DT_DECOR);
    //Original name: WK-DT-DECOR-AA
    private String wkDtDecorAa = "0000";
    //Original name: WK-DT-INF
    private String wkDtInf = DefaultValues.stringVal(Len.WK_DT_INF);
    //Original name: WK-DT-SUP
    private String wkDtSup = DefaultValues.stringVal(Len.WK_DT_SUP);
    //Original name: REC9-IMP-LORDO-RISC-PARZ
    private AfDecimal rec9ImpLordoRiscParz = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    public void setRec2CognomeAss(String rec2CognomeAss) {
        this.rec2CognomeAss = Functions.subString(rec2CognomeAss, Len.REC2_COGNOME_ASS);
    }

    public String getRec2CognomeAss() {
        return this.rec2CognomeAss;
    }

    public void setRec2NomeAss(String rec2NomeAss) {
        this.rec2NomeAss = Functions.subString(rec2NomeAss, Len.REC2_NOME_ASS);
    }

    public String getRec2NomeAss() {
        return this.rec2NomeAss;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public void setWsErrore(String wsErrore) {
        this.wsErrore = Functions.subString(wsErrore, Len.WS_ERRORE);
    }

    public String getWsErrore() {
        return this.wsErrore;
    }

    public String getWsErroreFormatted() {
        return Functions.padBlanks(getWsErrore(), Len.WS_ERRORE);
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public void setTmpImpOper(AfDecimal tmpImpOper) {
        this.tmpImpOper = PicFormatter.display("-9(12)V9(3)").format(tmpImpOper.copy()).toString();
    }

    public AfDecimal getTmpImpOper() {
        return PicParser.display("-9(12)V9(3)").parseDecimal(Len.Int.TMP_IMP_OPER + Len.Fract.TMP_IMP_OPER, Len.Fract.TMP_IMP_OPER, this.tmpImpOper);
    }

    public void setWkCntrl37(AfDecimal wkCntrl37) {
        this.wkCntrl37.assign(wkCntrl37);
    }

    public AfDecimal getWkCntrl37() {
        return this.wkCntrl37.copy();
    }

    public void setWkRcode(String wkRcode) {
        this.wkRcode = Functions.subString(wkRcode, Len.WK_RCODE);
    }

    public String getWkRcode() {
        return this.wkRcode;
    }

    public String getWkRcodeFormatted() {
        return Functions.padBlanks(getWkRcode(), Len.WK_RCODE);
    }

    public void setWkDataInFormatted(String wkDataIn) {
        this.wkDataIn = Trunc.toUnsignedNumeric(wkDataIn, Len.WK_DATA_IN);
    }

    public int getWkDataIn() {
        return NumericDisplay.asInt(this.wkDataIn);
    }

    public String getWkDataInFormatted() {
        return this.wkDataIn;
    }

    public void setWkDataOutFormatted(String wkDataOut) {
        this.wkDataOut = Trunc.toUnsignedNumeric(wkDataOut, Len.WK_DATA_OUT);
    }

    public int getWkDataOut() {
        return NumericDisplay.asInt(this.wkDataOut);
    }

    public String getWkDataOutFormatted() {
        return this.wkDataOut;
    }

    public void setWkControllo(int wkControllo) {
        this.wkControllo = NumericDisplay.asString(wkControllo, Len.WK_CONTROLLO);
    }

    public void setWkControlloFormatted(String wkControllo) {
        this.wkControllo = Trunc.toUnsignedNumeric(wkControllo, Len.WK_CONTROLLO);
    }

    public int getWkControllo() {
        return NumericDisplay.asInt(this.wkControllo);
    }

    public void setWkFrazFormatted(String wkFraz) {
        this.wkFraz = Trunc.toUnsignedNumeric(wkFraz, Len.WK_FRAZ);
    }

    public int getWkFraz() {
        return NumericDisplay.asInt(this.wkFraz);
    }

    public void setWkDtDecor(String wkDtDecor) {
        this.wkDtDecor = Functions.subString(wkDtDecor, Len.WK_DT_DECOR);
    }

    public String getWkDtDecor() {
        return this.wkDtDecor;
    }

    public String getWkDtDecorFormatted() {
        return Functions.padBlanks(getWkDtDecor(), Len.WK_DT_DECOR);
    }

    public void setWkDtDecorAaFormatted(String wkDtDecorAa) {
        this.wkDtDecorAa = Trunc.toUnsignedNumeric(wkDtDecorAa, Len.WK_DT_DECOR_AA);
    }

    public short getWkDtDecorAa() {
        return NumericDisplay.asShort(this.wkDtDecorAa);
    }

    public void setWkDtInfFormatted(String wkDtInf) {
        this.wkDtInf = Trunc.toUnsignedNumeric(wkDtInf, Len.WK_DT_INF);
    }

    public int getWkDtInf() {
        return NumericDisplay.asInt(this.wkDtInf);
    }

    public String getWkDtInfFormatted() {
        return this.wkDtInf;
    }

    public void setWkDtSupFormatted(String wkDtSup) {
        this.wkDtSup = Trunc.toUnsignedNumeric(wkDtSup, Len.WK_DT_SUP);
    }

    public int getWkDtSup() {
        return NumericDisplay.asInt(this.wkDtSup);
    }

    public String getWkDtSupFormatted() {
        return this.wkDtSup;
    }

    public void setRec9ImpLordoRiscParz(AfDecimal rec9ImpLordoRiscParz) {
        this.rec9ImpLordoRiscParz.assign(rec9ImpLordoRiscParz);
    }

    public AfDecimal getRec9ImpLordoRiscParz() {
        return this.rec9ImpLordoRiscParz.copy();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int REC2_COGNOME_ASS = 50;
        public static final int REC2_NOME_ASS = 50;
        public static final int WK_TABELLA = 20;
        public static final int WK_LABEL = 40;
        public static final int WS_ERRORE = 100;
        public static final int WK_LABEL_ERR = 30;
        public static final int TMP_IMP_OPER = 16;
        public static final int WK_RCODE = 2;
        public static final int WK_DATA_IN = 8;
        public static final int WK_DATA_OUT = 8;
        public static final int WK_CONTROLLO = 8;
        public static final int WK_FRAZ = 5;
        public static final int WK_DT_DECOR = 8;
        public static final int WK_DT_DECOR_AA = 4;
        public static final int WK_DT_INF = 8;
        public static final int WK_DT_SUP = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TMP_IMP_OPER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TMP_IMP_OPER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
