package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-ALQ-MARG-RIS<br>
 * Variable: W-B03-ALQ-MARG-RIS from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AlqMargRisLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AlqMargRisLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ALQ_MARG_RIS;
    }

    public void setwB03AlqMargRis(AfDecimal wB03AlqMargRis) {
        writeDecimalAsPacked(Pos.W_B03_ALQ_MARG_RIS, wB03AlqMargRis.copy());
    }

    /**Original name: W-B03-ALQ-MARG-RIS<br>*/
    public AfDecimal getwB03AlqMargRis() {
        return readPackedAsDecimal(Pos.W_B03_ALQ_MARG_RIS, Len.Int.W_B03_ALQ_MARG_RIS, Len.Fract.W_B03_ALQ_MARG_RIS);
    }

    public byte[] getwB03AlqMargRisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ALQ_MARG_RIS, Pos.W_B03_ALQ_MARG_RIS);
        return buffer;
    }

    public void setwB03AlqMargRisNull(String wB03AlqMargRisNull) {
        writeString(Pos.W_B03_ALQ_MARG_RIS_NULL, wB03AlqMargRisNull, Len.W_B03_ALQ_MARG_RIS_NULL);
    }

    /**Original name: W-B03-ALQ-MARG-RIS-NULL<br>*/
    public String getwB03AlqMargRisNull() {
        return readString(Pos.W_B03_ALQ_MARG_RIS_NULL, Len.W_B03_ALQ_MARG_RIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ALQ_MARG_RIS = 1;
        public static final int W_B03_ALQ_MARG_RIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ALQ_MARG_RIS = 4;
        public static final int W_B03_ALQ_MARG_RIS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ALQ_MARG_RIS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
