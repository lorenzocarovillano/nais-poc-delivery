package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PROV-RICOR<br>
 * Variable: WDTC-PROV-RICOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PROV_RICOR;
    }

    public void setWdtcProvRicor(AfDecimal wdtcProvRicor) {
        writeDecimalAsPacked(Pos.WDTC_PROV_RICOR, wdtcProvRicor.copy());
    }

    public void setWdtcProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PROV_RICOR, Pos.WDTC_PROV_RICOR);
    }

    /**Original name: WDTC-PROV-RICOR<br>*/
    public AfDecimal getWdtcProvRicor() {
        return readPackedAsDecimal(Pos.WDTC_PROV_RICOR, Len.Int.WDTC_PROV_RICOR, Len.Fract.WDTC_PROV_RICOR);
    }

    public byte[] getWdtcProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PROV_RICOR, Pos.WDTC_PROV_RICOR);
        return buffer;
    }

    public void initWdtcProvRicorSpaces() {
        fill(Pos.WDTC_PROV_RICOR, Len.WDTC_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWdtcProvRicorNull(String wdtcProvRicorNull) {
        writeString(Pos.WDTC_PROV_RICOR_NULL, wdtcProvRicorNull, Len.WDTC_PROV_RICOR_NULL);
    }

    /**Original name: WDTC-PROV-RICOR-NULL<br>*/
    public String getWdtcProvRicorNull() {
        return readString(Pos.WDTC_PROV_RICOR_NULL, Len.WDTC_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_RICOR = 1;
        public static final int WDTC_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PROV_RICOR = 8;
        public static final int WDTC_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
