package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SPE-MED<br>
 * Variable: WTDR-TOT-SPE-MED from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SPE_MED;
    }

    public void setWtdrTotSpeMed(AfDecimal wtdrTotSpeMed) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SPE_MED, wtdrTotSpeMed.copy());
    }

    /**Original name: WTDR-TOT-SPE-MED<br>*/
    public AfDecimal getWtdrTotSpeMed() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SPE_MED, Len.Int.WTDR_TOT_SPE_MED, Len.Fract.WTDR_TOT_SPE_MED);
    }

    public void setWtdrTotSpeMedNull(String wtdrTotSpeMedNull) {
        writeString(Pos.WTDR_TOT_SPE_MED_NULL, wtdrTotSpeMedNull, Len.WTDR_TOT_SPE_MED_NULL);
    }

    /**Original name: WTDR-TOT-SPE-MED-NULL<br>*/
    public String getWtdrTotSpeMedNull() {
        return readString(Pos.WTDR_TOT_SPE_MED_NULL, Len.WTDR_TOT_SPE_MED_NULL);
    }

    public String getWtdrTotSpeMedNullFormatted() {
        return Functions.padBlanks(getWtdrTotSpeMedNull(), Len.WTDR_TOT_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SPE_MED = 1;
        public static final int WTDR_TOT_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SPE_MED = 8;
        public static final int WTDR_TOT_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
