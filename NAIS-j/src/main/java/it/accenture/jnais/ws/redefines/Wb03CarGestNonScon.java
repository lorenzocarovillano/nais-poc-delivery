package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CAR-GEST-NON-SCON<br>
 * Variable: WB03-CAR-GEST-NON-SCON from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CarGestNonScon extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CarGestNonScon() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CAR_GEST_NON_SCON;
    }

    public void setWb03CarGestNonSconFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CAR_GEST_NON_SCON, Pos.WB03_CAR_GEST_NON_SCON);
    }

    /**Original name: WB03-CAR-GEST-NON-SCON<br>*/
    public AfDecimal getWb03CarGestNonScon() {
        return readPackedAsDecimal(Pos.WB03_CAR_GEST_NON_SCON, Len.Int.WB03_CAR_GEST_NON_SCON, Len.Fract.WB03_CAR_GEST_NON_SCON);
    }

    public byte[] getWb03CarGestNonSconAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CAR_GEST_NON_SCON, Pos.WB03_CAR_GEST_NON_SCON);
        return buffer;
    }

    public void setWb03CarGestNonSconNull(String wb03CarGestNonSconNull) {
        writeString(Pos.WB03_CAR_GEST_NON_SCON_NULL, wb03CarGestNonSconNull, Len.WB03_CAR_GEST_NON_SCON_NULL);
    }

    /**Original name: WB03-CAR-GEST-NON-SCON-NULL<br>*/
    public String getWb03CarGestNonSconNull() {
        return readString(Pos.WB03_CAR_GEST_NON_SCON_NULL, Len.WB03_CAR_GEST_NON_SCON_NULL);
    }

    public String getWb03CarGestNonSconNullFormatted() {
        return Functions.padBlanks(getWb03CarGestNonSconNull(), Len.WB03_CAR_GEST_NON_SCON_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CAR_GEST_NON_SCON = 1;
        public static final int WB03_CAR_GEST_NON_SCON_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CAR_GEST_NON_SCON = 8;
        public static final int WB03_CAR_GEST_NON_SCON_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CAR_GEST_NON_SCON = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CAR_GEST_NON_SCON = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
