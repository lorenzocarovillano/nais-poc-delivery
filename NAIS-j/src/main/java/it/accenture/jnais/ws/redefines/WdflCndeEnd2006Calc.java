package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNDE-END2006-CALC<br>
 * Variable: WDFL-CNDE-END2006-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCndeEnd2006Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCndeEnd2006Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNDE_END2006_CALC;
    }

    public void setWdflCndeEnd2006Calc(AfDecimal wdflCndeEnd2006Calc) {
        writeDecimalAsPacked(Pos.WDFL_CNDE_END2006_CALC, wdflCndeEnd2006Calc.copy());
    }

    public void setWdflCndeEnd2006CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNDE_END2006_CALC, Pos.WDFL_CNDE_END2006_CALC);
    }

    /**Original name: WDFL-CNDE-END2006-CALC<br>*/
    public AfDecimal getWdflCndeEnd2006Calc() {
        return readPackedAsDecimal(Pos.WDFL_CNDE_END2006_CALC, Len.Int.WDFL_CNDE_END2006_CALC, Len.Fract.WDFL_CNDE_END2006_CALC);
    }

    public byte[] getWdflCndeEnd2006CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNDE_END2006_CALC, Pos.WDFL_CNDE_END2006_CALC);
        return buffer;
    }

    public void setWdflCndeEnd2006CalcNull(String wdflCndeEnd2006CalcNull) {
        writeString(Pos.WDFL_CNDE_END2006_CALC_NULL, wdflCndeEnd2006CalcNull, Len.WDFL_CNDE_END2006_CALC_NULL);
    }

    /**Original name: WDFL-CNDE-END2006-CALC-NULL<br>*/
    public String getWdflCndeEnd2006CalcNull() {
        return readString(Pos.WDFL_CNDE_END2006_CALC_NULL, Len.WDFL_CNDE_END2006_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_END2006_CALC = 1;
        public static final int WDFL_CNDE_END2006_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_END2006_CALC = 8;
        public static final int WDFL_CNDE_END2006_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_END2006_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_END2006_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
