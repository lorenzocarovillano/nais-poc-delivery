package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-SOST-IPT<br>
 * Variable: WBEL-IMPST-SOST-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstSostIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstSostIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_SOST_IPT;
    }

    public void setWbelImpstSostIpt(AfDecimal wbelImpstSostIpt) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_SOST_IPT, wbelImpstSostIpt.copy());
    }

    public void setWbelImpstSostIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_SOST_IPT, Pos.WBEL_IMPST_SOST_IPT);
    }

    /**Original name: WBEL-IMPST-SOST-IPT<br>*/
    public AfDecimal getWbelImpstSostIpt() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_SOST_IPT, Len.Int.WBEL_IMPST_SOST_IPT, Len.Fract.WBEL_IMPST_SOST_IPT);
    }

    public byte[] getWbelImpstSostIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_SOST_IPT, Pos.WBEL_IMPST_SOST_IPT);
        return buffer;
    }

    public void initWbelImpstSostIptSpaces() {
        fill(Pos.WBEL_IMPST_SOST_IPT, Len.WBEL_IMPST_SOST_IPT, Types.SPACE_CHAR);
    }

    public void setWbelImpstSostIptNull(String wbelImpstSostIptNull) {
        writeString(Pos.WBEL_IMPST_SOST_IPT_NULL, wbelImpstSostIptNull, Len.WBEL_IMPST_SOST_IPT_NULL);
    }

    /**Original name: WBEL-IMPST-SOST-IPT-NULL<br>*/
    public String getWbelImpstSostIptNull() {
        return readString(Pos.WBEL_IMPST_SOST_IPT_NULL, Len.WBEL_IMPST_SOST_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_SOST_IPT = 1;
        public static final int WBEL_IMPST_SOST_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_SOST_IPT = 8;
        public static final int WBEL_IMPST_SOST_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_SOST_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_SOST_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
