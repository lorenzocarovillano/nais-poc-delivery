package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MONT-DAL2007-CALC<br>
 * Variable: DFL-MONT-DAL2007-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMontDal2007Calc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMontDal2007Calc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MONT_DAL2007_CALC;
    }

    public void setDflMontDal2007Calc(AfDecimal dflMontDal2007Calc) {
        writeDecimalAsPacked(Pos.DFL_MONT_DAL2007_CALC, dflMontDal2007Calc.copy());
    }

    public void setDflMontDal2007CalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MONT_DAL2007_CALC, Pos.DFL_MONT_DAL2007_CALC);
    }

    /**Original name: DFL-MONT-DAL2007-CALC<br>*/
    public AfDecimal getDflMontDal2007Calc() {
        return readPackedAsDecimal(Pos.DFL_MONT_DAL2007_CALC, Len.Int.DFL_MONT_DAL2007_CALC, Len.Fract.DFL_MONT_DAL2007_CALC);
    }

    public byte[] getDflMontDal2007CalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MONT_DAL2007_CALC, Pos.DFL_MONT_DAL2007_CALC);
        return buffer;
    }

    public void setDflMontDal2007CalcNull(String dflMontDal2007CalcNull) {
        writeString(Pos.DFL_MONT_DAL2007_CALC_NULL, dflMontDal2007CalcNull, Len.DFL_MONT_DAL2007_CALC_NULL);
    }

    /**Original name: DFL-MONT-DAL2007-CALC-NULL<br>*/
    public String getDflMontDal2007CalcNull() {
        return readString(Pos.DFL_MONT_DAL2007_CALC_NULL, Len.DFL_MONT_DAL2007_CALC_NULL);
    }

    public String getDflMontDal2007CalcNullFormatted() {
        return Functions.padBlanks(getDflMontDal2007CalcNull(), Len.DFL_MONT_DAL2007_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MONT_DAL2007_CALC = 1;
        public static final int DFL_MONT_DAL2007_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MONT_DAL2007_CALC = 8;
        public static final int DFL_MONT_DAL2007_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MONT_DAL2007_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_MONT_DAL2007_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
