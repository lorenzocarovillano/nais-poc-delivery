package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRST-RIS-TOT<br>
 * Variable: WRST-RIS-TOT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrstRisTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrstRisTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRST_RIS_TOT;
    }

    public void setWrstRisTot(AfDecimal wrstRisTot) {
        writeDecimalAsPacked(Pos.WRST_RIS_TOT, wrstRisTot.copy());
    }

    public void setWrstRisTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRST_RIS_TOT, Pos.WRST_RIS_TOT);
    }

    /**Original name: WRST-RIS-TOT<br>*/
    public AfDecimal getWrstRisTot() {
        return readPackedAsDecimal(Pos.WRST_RIS_TOT, Len.Int.WRST_RIS_TOT, Len.Fract.WRST_RIS_TOT);
    }

    public byte[] getWrstRisTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRST_RIS_TOT, Pos.WRST_RIS_TOT);
        return buffer;
    }

    public void initWrstRisTotSpaces() {
        fill(Pos.WRST_RIS_TOT, Len.WRST_RIS_TOT, Types.SPACE_CHAR);
    }

    public void setWrstRisTotNull(String wrstRisTotNull) {
        writeString(Pos.WRST_RIS_TOT_NULL, wrstRisTotNull, Len.WRST_RIS_TOT_NULL);
    }

    /**Original name: WRST-RIS-TOT-NULL<br>*/
    public String getWrstRisTotNull() {
        return readString(Pos.WRST_RIS_TOT_NULL, Len.WRST_RIS_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRST_RIS_TOT = 1;
        public static final int WRST_RIS_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRST_RIS_TOT = 8;
        public static final int WRST_RIS_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRST_RIS_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRST_RIS_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
