package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PROV-RICOR<br>
 * Variable: WDTR-PROV-RICOR from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PROV_RICOR;
    }

    public void setWdtrProvRicor(AfDecimal wdtrProvRicor) {
        writeDecimalAsPacked(Pos.WDTR_PROV_RICOR, wdtrProvRicor.copy());
    }

    /**Original name: WDTR-PROV-RICOR<br>*/
    public AfDecimal getWdtrProvRicor() {
        return readPackedAsDecimal(Pos.WDTR_PROV_RICOR, Len.Int.WDTR_PROV_RICOR, Len.Fract.WDTR_PROV_RICOR);
    }

    public void setWdtrProvRicorNull(String wdtrProvRicorNull) {
        writeString(Pos.WDTR_PROV_RICOR_NULL, wdtrProvRicorNull, Len.WDTR_PROV_RICOR_NULL);
    }

    /**Original name: WDTR-PROV-RICOR-NULL<br>*/
    public String getWdtrProvRicorNull() {
        return readString(Pos.WDTR_PROV_RICOR_NULL, Len.WDTR_PROV_RICOR_NULL);
    }

    public String getWdtrProvRicorNullFormatted() {
        return Functions.padBlanks(getWdtrProvRicorNull(), Len.WDTR_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_RICOR = 1;
        public static final int WDTR_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_RICOR = 8;
        public static final int WDTR_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
