package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.ws.redefines.TabGiorniLccs0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0010<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0010Data {

    //==== PROPERTIES ====
    /**Original name: TAB-GIORNI<br>
	 * <pre>¹   MODIFICA 09/02/89</pre>*/
    private TabGiorniLccs0010 tabGiorni = new TabGiorniLccs0010();
    //Original name: AMGINF
    private Amginf amginf = new Amginf();
    //Original name: AMGSUP
    private Amgsup amgsup = new Amgsup();
    //Original name: AAAAMMGG
    private Aaaammgg aaaammgg = new Aaaammgg();

    //==== METHODS ====
    public Aaaammgg getAaaammgg() {
        return aaaammgg;
    }

    public Amginf getAmginf() {
        return amginf;
    }

    public Amgsup getAmgsup() {
        return amgsup;
    }

    public TabGiorniLccs0010 getTabGiorni() {
        return tabGiorni;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RISUL = 3;
        public static final int RESTO = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
