package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: NOG-ULT-PROGR<br>
 * Variable: NOG-ULT-PROGR from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class NogUltProgr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public NogUltProgr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.NOG_ULT_PROGR;
    }

    public void setNogUltProgr(long nogUltProgr) {
        writeLongAsPacked(Pos.NOG_ULT_PROGR, nogUltProgr, Len.Int.NOG_ULT_PROGR);
    }

    public void setNogUltProgrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.NOG_ULT_PROGR, Pos.NOG_ULT_PROGR);
    }

    /**Original name: NOG-ULT-PROGR<br>*/
    public long getNogUltProgr() {
        return readPackedAsLong(Pos.NOG_ULT_PROGR, Len.Int.NOG_ULT_PROGR);
    }

    public byte[] getNogUltProgrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.NOG_ULT_PROGR, Pos.NOG_ULT_PROGR);
        return buffer;
    }

    public void setNogUltProgrNull(String nogUltProgrNull) {
        writeString(Pos.NOG_ULT_PROGR_NULL, nogUltProgrNull, Len.NOG_ULT_PROGR_NULL);
    }

    /**Original name: NOG-ULT-PROGR-NULL<br>*/
    public String getNogUltProgrNull() {
        return readString(Pos.NOG_ULT_PROGR_NULL, Len.NOG_ULT_PROGR_NULL);
    }

    public String getNogUltProgrNullFormatted() {
        return Functions.padBlanks(getNogUltProgrNull(), Len.NOG_ULT_PROGR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int NOG_ULT_PROGR = 1;
        public static final int NOG_ULT_PROGR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int NOG_ULT_PROGR = 10;
        public static final int NOG_ULT_PROGR_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NOG_ULT_PROGR = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
