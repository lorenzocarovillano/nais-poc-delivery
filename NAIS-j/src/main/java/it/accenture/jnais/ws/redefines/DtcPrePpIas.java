package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-PRE-PP-IAS<br>
 * Variable: DTC-PRE-PP-IAS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_PRE_PP_IAS;
    }

    public void setDtcPrePpIas(AfDecimal dtcPrePpIas) {
        writeDecimalAsPacked(Pos.DTC_PRE_PP_IAS, dtcPrePpIas.copy());
    }

    public void setDtcPrePpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_PRE_PP_IAS, Pos.DTC_PRE_PP_IAS);
    }

    /**Original name: DTC-PRE-PP-IAS<br>*/
    public AfDecimal getDtcPrePpIas() {
        return readPackedAsDecimal(Pos.DTC_PRE_PP_IAS, Len.Int.DTC_PRE_PP_IAS, Len.Fract.DTC_PRE_PP_IAS);
    }

    public byte[] getDtcPrePpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_PRE_PP_IAS, Pos.DTC_PRE_PP_IAS);
        return buffer;
    }

    public void setDtcPrePpIasNull(String dtcPrePpIasNull) {
        writeString(Pos.DTC_PRE_PP_IAS_NULL, dtcPrePpIasNull, Len.DTC_PRE_PP_IAS_NULL);
    }

    /**Original name: DTC-PRE-PP-IAS-NULL<br>*/
    public String getDtcPrePpIasNull() {
        return readString(Pos.DTC_PRE_PP_IAS_NULL, Len.DTC_PRE_PP_IAS_NULL);
    }

    public String getDtcPrePpIasNullFormatted() {
        return Functions.padBlanks(getDtcPrePpIasNull(), Len.DTC_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_PRE_PP_IAS = 1;
        public static final int DTC_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_PRE_PP_IAS = 8;
        public static final int DTC_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
