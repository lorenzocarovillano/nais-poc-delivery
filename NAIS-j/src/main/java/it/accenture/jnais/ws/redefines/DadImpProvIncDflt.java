package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-IMP-PROV-INC-DFLT<br>
 * Variable: DAD-IMP-PROV-INC-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadImpProvIncDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadImpProvIncDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_IMP_PROV_INC_DFLT;
    }

    public void setDadImpProvIncDflt(AfDecimal dadImpProvIncDflt) {
        writeDecimalAsPacked(Pos.DAD_IMP_PROV_INC_DFLT, dadImpProvIncDflt.copy());
    }

    public void setDadImpProvIncDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_IMP_PROV_INC_DFLT, Pos.DAD_IMP_PROV_INC_DFLT);
    }

    /**Original name: DAD-IMP-PROV-INC-DFLT<br>*/
    public AfDecimal getDadImpProvIncDflt() {
        return readPackedAsDecimal(Pos.DAD_IMP_PROV_INC_DFLT, Len.Int.DAD_IMP_PROV_INC_DFLT, Len.Fract.DAD_IMP_PROV_INC_DFLT);
    }

    public byte[] getDadImpProvIncDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_IMP_PROV_INC_DFLT, Pos.DAD_IMP_PROV_INC_DFLT);
        return buffer;
    }

    public void setDadImpProvIncDfltNull(String dadImpProvIncDfltNull) {
        writeString(Pos.DAD_IMP_PROV_INC_DFLT_NULL, dadImpProvIncDfltNull, Len.DAD_IMP_PROV_INC_DFLT_NULL);
    }

    /**Original name: DAD-IMP-PROV-INC-DFLT-NULL<br>*/
    public String getDadImpProvIncDfltNull() {
        return readString(Pos.DAD_IMP_PROV_INC_DFLT_NULL, Len.DAD_IMP_PROV_INC_DFLT_NULL);
    }

    public String getDadImpProvIncDfltNullFormatted() {
        return Functions.padBlanks(getDadImpProvIncDfltNull(), Len.DAD_IMP_PROV_INC_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_IMP_PROV_INC_DFLT = 1;
        public static final int DAD_IMP_PROV_INC_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_IMP_PROV_INC_DFLT = 8;
        public static final int DAD_IMP_PROV_INC_DFLT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_IMP_PROV_INC_DFLT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DAD_IMP_PROV_INC_DFLT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
