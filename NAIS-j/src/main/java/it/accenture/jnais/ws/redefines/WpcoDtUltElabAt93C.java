package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-AT93-C<br>
 * Variable: WPCO-DT-ULT-ELAB-AT93-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabAt93C extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabAt93C() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_AT93_C;
    }

    public void setWpcoDtUltElabAt93C(int wpcoDtUltElabAt93C) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_AT93_C, wpcoDtUltElabAt93C, Len.Int.WPCO_DT_ULT_ELAB_AT93_C);
    }

    public void setDpcoDtUltElabAt93CFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT93_C, Pos.WPCO_DT_ULT_ELAB_AT93_C);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT93-C<br>*/
    public int getWpcoDtUltElabAt93C() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_AT93_C, Len.Int.WPCO_DT_ULT_ELAB_AT93_C);
    }

    public byte[] getWpcoDtUltElabAt93CAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_AT93_C, Pos.WPCO_DT_ULT_ELAB_AT93_C);
        return buffer;
    }

    public void setWpcoDtUltElabAt93CNull(String wpcoDtUltElabAt93CNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_AT93_C_NULL, wpcoDtUltElabAt93CNull, Len.WPCO_DT_ULT_ELAB_AT93_C_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-AT93-C-NULL<br>*/
    public String getWpcoDtUltElabAt93CNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_AT93_C_NULL, Len.WPCO_DT_ULT_ELAB_AT93_C_NULL);
    }

    public String getWpcoDtUltElabAt93CNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabAt93CNull(), Len.WPCO_DT_ULT_ELAB_AT93_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT93_C = 1;
        public static final int WPCO_DT_ULT_ELAB_AT93_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_AT93_C = 5;
        public static final int WPCO_DT_ULT_ELAB_AT93_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_AT93_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
