package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-SPE-IN<br>
 * Variable: PCO-DT-ULT-ELAB-SPE-IN from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabSpeIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabSpeIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_SPE_IN;
    }

    public void setPcoDtUltElabSpeIn(int pcoDtUltElabSpeIn) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_SPE_IN, pcoDtUltElabSpeIn, Len.Int.PCO_DT_ULT_ELAB_SPE_IN);
    }

    public void setPcoDtUltElabSpeInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_SPE_IN, Pos.PCO_DT_ULT_ELAB_SPE_IN);
    }

    /**Original name: PCO-DT-ULT-ELAB-SPE-IN<br>*/
    public int getPcoDtUltElabSpeIn() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_SPE_IN, Len.Int.PCO_DT_ULT_ELAB_SPE_IN);
    }

    public byte[] getPcoDtUltElabSpeInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_SPE_IN, Pos.PCO_DT_ULT_ELAB_SPE_IN);
        return buffer;
    }

    public void initPcoDtUltElabSpeInHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_SPE_IN, Len.PCO_DT_ULT_ELAB_SPE_IN, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabSpeInNull(String pcoDtUltElabSpeInNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_SPE_IN_NULL, pcoDtUltElabSpeInNull, Len.PCO_DT_ULT_ELAB_SPE_IN_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-SPE-IN-NULL<br>*/
    public String getPcoDtUltElabSpeInNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_SPE_IN_NULL, Len.PCO_DT_ULT_ELAB_SPE_IN_NULL);
    }

    public String getPcoDtUltElabSpeInNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabSpeInNull(), Len.PCO_DT_ULT_ELAB_SPE_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_SPE_IN = 1;
        public static final int PCO_DT_ULT_ELAB_SPE_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_SPE_IN = 5;
        public static final int PCO_DT_ULT_ELAB_SPE_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_SPE_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
