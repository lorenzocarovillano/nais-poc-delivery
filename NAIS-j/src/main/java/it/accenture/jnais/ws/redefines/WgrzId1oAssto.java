package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-ID-1O-ASSTO<br>
 * Variable: WGRZ-ID-1O-ASSTO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzId1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzId1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ID1O_ASSTO;
    }

    public void setWgrzId1oAssto(int wgrzId1oAssto) {
        writeIntAsPacked(Pos.WGRZ_ID1O_ASSTO, wgrzId1oAssto, Len.Int.WGRZ_ID1O_ASSTO);
    }

    public void setWgrzId1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ID1O_ASSTO, Pos.WGRZ_ID1O_ASSTO);
    }

    /**Original name: WGRZ-ID-1O-ASSTO<br>*/
    public int getWgrzId1oAssto() {
        return readPackedAsInt(Pos.WGRZ_ID1O_ASSTO, Len.Int.WGRZ_ID1O_ASSTO);
    }

    public byte[] getWgrzId1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ID1O_ASSTO, Pos.WGRZ_ID1O_ASSTO);
        return buffer;
    }

    public void initWgrzId1oAsstoSpaces() {
        fill(Pos.WGRZ_ID1O_ASSTO, Len.WGRZ_ID1O_ASSTO, Types.SPACE_CHAR);
    }

    public void setWgrzId1oAsstoNull(String wgrzId1oAsstoNull) {
        writeString(Pos.WGRZ_ID1O_ASSTO_NULL, wgrzId1oAsstoNull, Len.WGRZ_ID1O_ASSTO_NULL);
    }

    /**Original name: WGRZ-ID-1O-ASSTO-NULL<br>*/
    public String getWgrzId1oAsstoNull() {
        return readString(Pos.WGRZ_ID1O_ASSTO_NULL, Len.WGRZ_ID1O_ASSTO_NULL);
    }

    public String getWgrzId1oAsstoNullFormatted() {
        return Functions.padBlanks(getWgrzId1oAsstoNull(), Len.WGRZ_ID1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ID1O_ASSTO = 1;
        public static final int WGRZ_ID1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ID1O_ASSTO = 5;
        public static final int WGRZ_ID1O_ASSTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ID1O_ASSTO = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
