package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-DT-END-COP<br>
 * Variable: TDR-DT-END-COP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_DT_END_COP;
    }

    public void setTdrDtEndCop(int tdrDtEndCop) {
        writeIntAsPacked(Pos.TDR_DT_END_COP, tdrDtEndCop, Len.Int.TDR_DT_END_COP);
    }

    public void setTdrDtEndCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_DT_END_COP, Pos.TDR_DT_END_COP);
    }

    /**Original name: TDR-DT-END-COP<br>*/
    public int getTdrDtEndCop() {
        return readPackedAsInt(Pos.TDR_DT_END_COP, Len.Int.TDR_DT_END_COP);
    }

    public byte[] getTdrDtEndCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_DT_END_COP, Pos.TDR_DT_END_COP);
        return buffer;
    }

    public void setTdrDtEndCopNull(String tdrDtEndCopNull) {
        writeString(Pos.TDR_DT_END_COP_NULL, tdrDtEndCopNull, Len.TDR_DT_END_COP_NULL);
    }

    /**Original name: TDR-DT-END-COP-NULL<br>*/
    public String getTdrDtEndCopNull() {
        return readString(Pos.TDR_DT_END_COP_NULL, Len.TDR_DT_END_COP_NULL);
    }

    public String getTdrDtEndCopNullFormatted() {
        return Functions.padBlanks(getTdrDtEndCopNull(), Len.TDR_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_DT_END_COP = 1;
        public static final int TDR_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_DT_END_COP = 5;
        public static final int TDR_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
