package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-NUM-GG-RIVAL<br>
 * Variable: WDTC-NUM-GG-RIVAL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcNumGgRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcNumGgRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_NUM_GG_RIVAL;
    }

    public void setWdtcNumGgRival(int wdtcNumGgRival) {
        writeIntAsPacked(Pos.WDTC_NUM_GG_RIVAL, wdtcNumGgRival, Len.Int.WDTC_NUM_GG_RIVAL);
    }

    public void setWdtcNumGgRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_NUM_GG_RIVAL, Pos.WDTC_NUM_GG_RIVAL);
    }

    /**Original name: WDTC-NUM-GG-RIVAL<br>*/
    public int getWdtcNumGgRival() {
        return readPackedAsInt(Pos.WDTC_NUM_GG_RIVAL, Len.Int.WDTC_NUM_GG_RIVAL);
    }

    public byte[] getWdtcNumGgRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_NUM_GG_RIVAL, Pos.WDTC_NUM_GG_RIVAL);
        return buffer;
    }

    public void initWdtcNumGgRivalSpaces() {
        fill(Pos.WDTC_NUM_GG_RIVAL, Len.WDTC_NUM_GG_RIVAL, Types.SPACE_CHAR);
    }

    public void setWdtcNumGgRivalNull(String wdtcNumGgRivalNull) {
        writeString(Pos.WDTC_NUM_GG_RIVAL_NULL, wdtcNumGgRivalNull, Len.WDTC_NUM_GG_RIVAL_NULL);
    }

    /**Original name: WDTC-NUM-GG-RIVAL-NULL<br>*/
    public String getWdtcNumGgRivalNull() {
        return readString(Pos.WDTC_NUM_GG_RIVAL_NULL, Len.WDTC_NUM_GG_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_NUM_GG_RIVAL = 1;
        public static final int WDTC_NUM_GG_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_NUM_GG_RIVAL = 3;
        public static final int WDTC_NUM_GG_RIVAL_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_NUM_GG_RIVAL = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
