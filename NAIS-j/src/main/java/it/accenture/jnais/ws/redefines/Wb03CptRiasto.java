package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-CPT-RIASTO<br>
 * Variable: WB03-CPT-RIASTO from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03CptRiasto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03CptRiasto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_CPT_RIASTO;
    }

    public void setWb03CptRiastoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_CPT_RIASTO, Pos.WB03_CPT_RIASTO);
    }

    /**Original name: WB03-CPT-RIASTO<br>*/
    public AfDecimal getWb03CptRiasto() {
        return readPackedAsDecimal(Pos.WB03_CPT_RIASTO, Len.Int.WB03_CPT_RIASTO, Len.Fract.WB03_CPT_RIASTO);
    }

    public byte[] getWb03CptRiastoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_CPT_RIASTO, Pos.WB03_CPT_RIASTO);
        return buffer;
    }

    public void setWb03CptRiastoNull(String wb03CptRiastoNull) {
        writeString(Pos.WB03_CPT_RIASTO_NULL, wb03CptRiastoNull, Len.WB03_CPT_RIASTO_NULL);
    }

    /**Original name: WB03-CPT-RIASTO-NULL<br>*/
    public String getWb03CptRiastoNull() {
        return readString(Pos.WB03_CPT_RIASTO_NULL, Len.WB03_CPT_RIASTO_NULL);
    }

    public String getWb03CptRiastoNullFormatted() {
        return Functions.padBlanks(getWb03CptRiastoNull(), Len.WB03_CPT_RIASTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_CPT_RIASTO = 1;
        public static final int WB03_CPT_RIASTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_CPT_RIASTO = 8;
        public static final int WB03_CPT_RIASTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_CPT_RIASTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_CPT_RIASTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
