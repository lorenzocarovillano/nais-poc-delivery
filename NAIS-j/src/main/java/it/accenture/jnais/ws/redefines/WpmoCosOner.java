package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-COS-ONER<br>
 * Variable: WPMO-COS-ONER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoCosOner extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoCosOner() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_COS_ONER;
    }

    public void setWpmoCosOner(AfDecimal wpmoCosOner) {
        writeDecimalAsPacked(Pos.WPMO_COS_ONER, wpmoCosOner.copy());
    }

    public void setWpmoCosOnerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_COS_ONER, Pos.WPMO_COS_ONER);
    }

    /**Original name: WPMO-COS-ONER<br>*/
    public AfDecimal getWpmoCosOner() {
        return readPackedAsDecimal(Pos.WPMO_COS_ONER, Len.Int.WPMO_COS_ONER, Len.Fract.WPMO_COS_ONER);
    }

    public byte[] getWpmoCosOnerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_COS_ONER, Pos.WPMO_COS_ONER);
        return buffer;
    }

    public void initWpmoCosOnerSpaces() {
        fill(Pos.WPMO_COS_ONER, Len.WPMO_COS_ONER, Types.SPACE_CHAR);
    }

    public void setWpmoCosOnerNull(String wpmoCosOnerNull) {
        writeString(Pos.WPMO_COS_ONER_NULL, wpmoCosOnerNull, Len.WPMO_COS_ONER_NULL);
    }

    /**Original name: WPMO-COS-ONER-NULL<br>*/
    public String getWpmoCosOnerNull() {
        return readString(Pos.WPMO_COS_ONER_NULL, Len.WPMO_COS_ONER_NULL);
    }

    public String getWpmoCosOnerNullFormatted() {
        return Functions.padBlanks(getWpmoCosOnerNull(), Len.WPMO_COS_ONER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_COS_ONER = 1;
        public static final int WPMO_COS_ONER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_COS_ONER = 8;
        public static final int WPMO_COS_ONER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_COS_ONER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_COS_ONER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
