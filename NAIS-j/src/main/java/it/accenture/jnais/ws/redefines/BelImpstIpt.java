package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-IMPST-IPT<br>
 * Variable: BEL-IMPST-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelImpstIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelImpstIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_IMPST_IPT;
    }

    public void setBelImpstIpt(AfDecimal belImpstIpt) {
        writeDecimalAsPacked(Pos.BEL_IMPST_IPT, belImpstIpt.copy());
    }

    public void setBelImpstIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_IMPST_IPT, Pos.BEL_IMPST_IPT);
    }

    /**Original name: BEL-IMPST-IPT<br>*/
    public AfDecimal getBelImpstIpt() {
        return readPackedAsDecimal(Pos.BEL_IMPST_IPT, Len.Int.BEL_IMPST_IPT, Len.Fract.BEL_IMPST_IPT);
    }

    public byte[] getBelImpstIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_IMPST_IPT, Pos.BEL_IMPST_IPT);
        return buffer;
    }

    public void setBelImpstIptNull(String belImpstIptNull) {
        writeString(Pos.BEL_IMPST_IPT_NULL, belImpstIptNull, Len.BEL_IMPST_IPT_NULL);
    }

    /**Original name: BEL-IMPST-IPT-NULL<br>*/
    public String getBelImpstIptNull() {
        return readString(Pos.BEL_IMPST_IPT_NULL, Len.BEL_IMPST_IPT_NULL);
    }

    public String getBelImpstIptNullFormatted() {
        return Functions.padBlanks(getBelImpstIptNull(), Len.BEL_IMPST_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_IPT = 1;
        public static final int BEL_IMPST_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_IMPST_IPT = 8;
        public static final int BEL_IMPST_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int BEL_IMPST_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
