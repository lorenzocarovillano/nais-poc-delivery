package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMPB-COMMIS-INTER<br>
 * Variable: TGA-IMPB-COMMIS-INTER from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpbCommisInter extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpbCommisInter() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMPB_COMMIS_INTER;
    }

    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        writeDecimalAsPacked(Pos.TGA_IMPB_COMMIS_INTER, tgaImpbCommisInter.copy());
    }

    public void setTgaImpbCommisInterFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMPB_COMMIS_INTER, Pos.TGA_IMPB_COMMIS_INTER);
    }

    /**Original name: TGA-IMPB-COMMIS-INTER<br>*/
    public AfDecimal getTgaImpbCommisInter() {
        return readPackedAsDecimal(Pos.TGA_IMPB_COMMIS_INTER, Len.Int.TGA_IMPB_COMMIS_INTER, Len.Fract.TGA_IMPB_COMMIS_INTER);
    }

    public byte[] getTgaImpbCommisInterAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMPB_COMMIS_INTER, Pos.TGA_IMPB_COMMIS_INTER);
        return buffer;
    }

    public void setTgaImpbCommisInterNull(String tgaImpbCommisInterNull) {
        writeString(Pos.TGA_IMPB_COMMIS_INTER_NULL, tgaImpbCommisInterNull, Len.TGA_IMPB_COMMIS_INTER_NULL);
    }

    /**Original name: TGA-IMPB-COMMIS-INTER-NULL<br>*/
    public String getTgaImpbCommisInterNull() {
        return readString(Pos.TGA_IMPB_COMMIS_INTER_NULL, Len.TGA_IMPB_COMMIS_INTER_NULL);
    }

    public String getTgaImpbCommisInterNullFormatted() {
        return Functions.padBlanks(getTgaImpbCommisInterNull(), Len.TGA_IMPB_COMMIS_INTER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_COMMIS_INTER = 1;
        public static final int TGA_IMPB_COMMIS_INTER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_COMMIS_INTER = 8;
        public static final int TGA_IMPB_COMMIS_INTER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_COMMIS_INTER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_COMMIS_INTER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
