package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Idsi0011FlagCodaTs;
import it.accenture.jnais.ws.enums.Idsi0011FormatoDataDb;
import it.accenture.jnais.ws.enums.Idsi0011IdentitaChiamante;
import it.accenture.jnais.ws.enums.Idsi0011ModalitaEsecutiva;
import it.accenture.jnais.ws.enums.Idsv0001LivelloDebug;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.enums.Idsv0003Operazione;
import it.accenture.jnais.ws.enums.Idsv0003TrattamentoStoricita;

/**Original name: IN-IDSS0010<br>
 * Variable: IN-IDSS0010 from program IDSS0010<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class InIdss0010 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: IDSI0011-PGM
    private String idsi0011Pgm = DefaultValues.stringVal(Len.IDSI0011_PGM);
    /**Original name: IDSI0011-MODALITA-ESECUTIVA<br>
	 * <pre> --   CAMPI SETTAGGI MODULI I-O
	 *       COPY IDSV0007 REPLACING ==IDSI0011== BY ==IDSI0011==.</pre>*/
    private Idsi0011ModalitaEsecutiva idsi0011ModalitaEsecutiva = new Idsi0011ModalitaEsecutiva();
    //Original name: IDSI0011-CODICE-COMPAGNIA-ANIA
    private int idsi0011CodiceCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: IDSI0011-COD-MAIN-BATCH
    private String idsi0011CodMainBatch = DefaultValues.stringVal(Len.IDSI0011_COD_MAIN_BATCH);
    //Original name: IDSI0011-TIPO-MOVIMENTO
    private String idsi0011TipoMovimento = DefaultValues.stringVal(Len.IDSI0011_TIPO_MOVIMENTO);
    //Original name: IDSI0011-SESSIONE
    private String idsi0011Sessione = DefaultValues.stringVal(Len.IDSI0011_SESSIONE);
    //Original name: IDSI0011-USER-NAME
    private String idsi0011UserName = DefaultValues.stringVal(Len.IDSI0011_USER_NAME);
    //Original name: IDSI0011-DATA-INIZIO-EFFETTO
    private int idsi0011DataInizioEffetto = DefaultValues.INT_VAL;
    //Original name: IDSI0011-DATA-FINE-EFFETTO
    private int idsi0011DataFineEffetto = DefaultValues.INT_VAL;
    //Original name: IDSI0011-DATA-COMPETENZA
    private long idsi0011DataCompetenza = DefaultValues.LONG_VAL;
    //Original name: IDSI0011-DATA-COMP-AGG-STOR
    private long idsi0011DataCompAggStor = DefaultValues.LONG_VAL;
    //Original name: IDSI0011-TRATTAMENTO-STORICITA
    private Idsv0003TrattamentoStoricita idsi0011TrattamentoStoricita = new Idsv0003TrattamentoStoricita();
    //Original name: IDSI0011-FORMATO-DATA-DB
    private Idsi0011FormatoDataDb idsi0011FormatoDataDb = new Idsi0011FormatoDataDb();
    //Original name: IDSI0011-ID-MOVI-ANNULLATO
    private String idsi0011IdMoviAnnullato = DefaultValues.stringVal(Len.IDSI0011_ID_MOVI_ANNULLATO);
    //Original name: IDSI0011-IDENTITA-CHIAMANTE
    private Idsi0011IdentitaChiamante idsi0011IdentitaChiamante = new Idsi0011IdentitaChiamante();
    //Original name: IDSI0011-LIVELLO-OPERAZIONE
    private Idsv0003LivelloOperazione idsi0011LivelloOperazione = new Idsv0003LivelloOperazione();
    /**Original name: IDSI0011-OPERAZIONE<br>
	 * <pre> -- CAMPI OPERAZIONE
	 *     COPY IDSV0008.</pre>*/
    private Idsv0003Operazione idsi0011Operazione = new Idsv0003Operazione();
    /**Original name: IDSI0011-FLAG-CODA-TS<br>
	 * <pre>    fine COPY IDSV0008.</pre>*/
    private Idsi0011FlagCodaTs idsi0011FlagCodaTs = new Idsi0011FlagCodaTs();
    /**Original name: IDSI0011-CODICE-STR-DATO<br>
	 * <pre>    fine COPY IDSV0007</pre>*/
    private String idsi0011CodiceStrDato = DefaultValues.stringVal(Len.IDSI0011_CODICE_STR_DATO);
    //Original name: IDSI0011-BUFFER-DATI
    private String idsi0011BufferDati = DefaultValues.stringVal(Len.IDSI0011_BUFFER_DATI);
    //Original name: IDSI0011-BUFFER-WHERE-COND
    private String idsi0011BufferWhereCond = DefaultValues.stringVal(Len.IDSI0011_BUFFER_WHERE_COND);
    //Original name: IDSI0011-LIVELLO-DEBUG
    private Idsv0001LivelloDebug idsi0011LivelloDebug = new Idsv0001LivelloDebug();
    //Original name: FILLER-IDSI0011-AREA
    private String flr1 = DefaultValues.stringVal(Len.FLR1);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IN_IDSS0010;
    }

    @Override
    public void deserialize(byte[] buf) {
        setInIdss0010Bytes(buf);
    }

    public void setInIdss0010Bytes(byte[] buffer) {
        setInIdss0010Bytes(buffer, 1);
    }

    public byte[] getInIdss0010Bytes() {
        byte[] buffer = new byte[Len.IN_IDSS0010];
        return getInIdss0010Bytes(buffer, 1);
    }

    public void setInIdss0010Bytes(byte[] buffer, int offset) {
        int position = offset;
        setIdsi0011AreaBytes(buffer, position);
    }

    public byte[] getInIdss0010Bytes(byte[] buffer, int offset) {
        int position = offset;
        getIdsi0011AreaBytes(buffer, position);
        return buffer;
    }

    public void setIdsi0011AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        idsi0011Pgm = MarshalByte.readString(buffer, position, Len.IDSI0011_PGM);
        position += Len.IDSI0011_PGM;
        idsi0011ModalitaEsecutiva.setIdsi0011ModalitaEsecutiva(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        idsi0011CodiceCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.IDSI0011_CODICE_COMPAGNIA_ANIA, 0);
        position += Len.IDSI0011_CODICE_COMPAGNIA_ANIA;
        idsi0011CodMainBatch = MarshalByte.readString(buffer, position, Len.IDSI0011_COD_MAIN_BATCH);
        position += Len.IDSI0011_COD_MAIN_BATCH;
        idsi0011TipoMovimento = MarshalByte.readFixedString(buffer, position, Len.IDSI0011_TIPO_MOVIMENTO);
        position += Len.IDSI0011_TIPO_MOVIMENTO;
        idsi0011Sessione = MarshalByte.readString(buffer, position, Len.IDSI0011_SESSIONE);
        position += Len.IDSI0011_SESSIONE;
        idsi0011UserName = MarshalByte.readString(buffer, position, Len.IDSI0011_USER_NAME);
        position += Len.IDSI0011_USER_NAME;
        idsi0011DataInizioEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.IDSI0011_DATA_INIZIO_EFFETTO, 0);
        position += Len.IDSI0011_DATA_INIZIO_EFFETTO;
        idsi0011DataFineEffetto = MarshalByte.readPackedAsInt(buffer, position, Len.Int.IDSI0011_DATA_FINE_EFFETTO, 0);
        position += Len.IDSI0011_DATA_FINE_EFFETTO;
        idsi0011DataCompetenza = MarshalByte.readPackedAsLong(buffer, position, Len.Int.IDSI0011_DATA_COMPETENZA, 0);
        position += Len.IDSI0011_DATA_COMPETENZA;
        idsi0011DataCompAggStor = MarshalByte.readPackedAsLong(buffer, position, Len.Int.IDSI0011_DATA_COMP_AGG_STOR, 0);
        position += Len.IDSI0011_DATA_COMP_AGG_STOR;
        idsi0011TrattamentoStoricita.setTrattamentoStoricita(MarshalByte.readString(buffer, position, Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA));
        position += Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA;
        idsi0011FormatoDataDb.setIdsi0011FormatoDataDb(MarshalByte.readString(buffer, position, Idsi0011FormatoDataDb.Len.IDSI0011_FORMATO_DATA_DB));
        position += Idsi0011FormatoDataDb.Len.IDSI0011_FORMATO_DATA_DB;
        idsi0011IdMoviAnnullato = MarshalByte.readFixedString(buffer, position, Len.IDSI0011_ID_MOVI_ANNULLATO);
        position += Len.IDSI0011_ID_MOVI_ANNULLATO;
        idsi0011IdentitaChiamante.setIdsi0011IdentitaChiamante(MarshalByte.readString(buffer, position, Idsi0011IdentitaChiamante.Len.IDSI0011_IDENTITA_CHIAMANTE));
        position += Idsi0011IdentitaChiamante.Len.IDSI0011_IDENTITA_CHIAMANTE;
        idsi0011LivelloOperazione.setLivelloOperazione(MarshalByte.readString(buffer, position, Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE));
        position += Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE;
        idsi0011Operazione.setOperazione(MarshalByte.readString(buffer, position, Idsv0003Operazione.Len.OPERAZIONE));
        position += Idsv0003Operazione.Len.OPERAZIONE;
        idsi0011FlagCodaTs.setIdsi0011FlagCodaTs(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        idsi0011CodiceStrDato = MarshalByte.readString(buffer, position, Len.IDSI0011_CODICE_STR_DATO);
        position += Len.IDSI0011_CODICE_STR_DATO;
        idsi0011BufferDati = MarshalByte.readString(buffer, position, Len.IDSI0011_BUFFER_DATI);
        position += Len.IDSI0011_BUFFER_DATI;
        idsi0011BufferWhereCond = MarshalByte.readString(buffer, position, Len.IDSI0011_BUFFER_WHERE_COND);
        position += Len.IDSI0011_BUFFER_WHERE_COND;
        idsi0011LivelloDebug.value = MarshalByte.readFixedString(buffer, position, Idsv0001LivelloDebug.Len.LIVELLO_DEBUG);
        position += Idsv0001LivelloDebug.Len.LIVELLO_DEBUG;
        flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
    }

    public byte[] getIdsi0011AreaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, idsi0011Pgm, Len.IDSI0011_PGM);
        position += Len.IDSI0011_PGM;
        MarshalByte.writeChar(buffer, position, idsi0011ModalitaEsecutiva.getIdsi0011ModalitaEsecutiva());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, idsi0011CodiceCompagniaAnia, Len.Int.IDSI0011_CODICE_COMPAGNIA_ANIA, 0);
        position += Len.IDSI0011_CODICE_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, idsi0011CodMainBatch, Len.IDSI0011_COD_MAIN_BATCH);
        position += Len.IDSI0011_COD_MAIN_BATCH;
        MarshalByte.writeString(buffer, position, idsi0011TipoMovimento, Len.IDSI0011_TIPO_MOVIMENTO);
        position += Len.IDSI0011_TIPO_MOVIMENTO;
        MarshalByte.writeString(buffer, position, idsi0011Sessione, Len.IDSI0011_SESSIONE);
        position += Len.IDSI0011_SESSIONE;
        MarshalByte.writeString(buffer, position, idsi0011UserName, Len.IDSI0011_USER_NAME);
        position += Len.IDSI0011_USER_NAME;
        MarshalByte.writeIntAsPacked(buffer, position, idsi0011DataInizioEffetto, Len.Int.IDSI0011_DATA_INIZIO_EFFETTO, 0);
        position += Len.IDSI0011_DATA_INIZIO_EFFETTO;
        MarshalByte.writeIntAsPacked(buffer, position, idsi0011DataFineEffetto, Len.Int.IDSI0011_DATA_FINE_EFFETTO, 0);
        position += Len.IDSI0011_DATA_FINE_EFFETTO;
        MarshalByte.writeLongAsPacked(buffer, position, idsi0011DataCompetenza, Len.Int.IDSI0011_DATA_COMPETENZA, 0);
        position += Len.IDSI0011_DATA_COMPETENZA;
        MarshalByte.writeLongAsPacked(buffer, position, idsi0011DataCompAggStor, Len.Int.IDSI0011_DATA_COMP_AGG_STOR, 0);
        position += Len.IDSI0011_DATA_COMP_AGG_STOR;
        MarshalByte.writeString(buffer, position, idsi0011TrattamentoStoricita.getTrattamentoStoricita(), Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA);
        position += Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA;
        MarshalByte.writeString(buffer, position, idsi0011FormatoDataDb.getIdsi0011FormatoDataDb(), Idsi0011FormatoDataDb.Len.IDSI0011_FORMATO_DATA_DB);
        position += Idsi0011FormatoDataDb.Len.IDSI0011_FORMATO_DATA_DB;
        MarshalByte.writeString(buffer, position, idsi0011IdMoviAnnullato, Len.IDSI0011_ID_MOVI_ANNULLATO);
        position += Len.IDSI0011_ID_MOVI_ANNULLATO;
        MarshalByte.writeString(buffer, position, idsi0011IdentitaChiamante.getIdsi0011IdentitaChiamante(), Idsi0011IdentitaChiamante.Len.IDSI0011_IDENTITA_CHIAMANTE);
        position += Idsi0011IdentitaChiamante.Len.IDSI0011_IDENTITA_CHIAMANTE;
        MarshalByte.writeString(buffer, position, idsi0011LivelloOperazione.getLivelloOperazione(), Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE);
        position += Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE;
        MarshalByte.writeString(buffer, position, idsi0011Operazione.getOperazione(), Idsv0003Operazione.Len.OPERAZIONE);
        position += Idsv0003Operazione.Len.OPERAZIONE;
        MarshalByte.writeChar(buffer, position, idsi0011FlagCodaTs.getIdsi0011FlagCodaTs());
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, idsi0011CodiceStrDato, Len.IDSI0011_CODICE_STR_DATO);
        position += Len.IDSI0011_CODICE_STR_DATO;
        MarshalByte.writeString(buffer, position, idsi0011BufferDati, Len.IDSI0011_BUFFER_DATI);
        position += Len.IDSI0011_BUFFER_DATI;
        MarshalByte.writeString(buffer, position, idsi0011BufferWhereCond, Len.IDSI0011_BUFFER_WHERE_COND);
        position += Len.IDSI0011_BUFFER_WHERE_COND;
        MarshalByte.writeString(buffer, position, idsi0011LivelloDebug.value, Idsv0001LivelloDebug.Len.LIVELLO_DEBUG);
        position += Idsv0001LivelloDebug.Len.LIVELLO_DEBUG;
        MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
        return buffer;
    }

    public void setIdsi0011Pgm(String idsi0011Pgm) {
        this.idsi0011Pgm = Functions.subString(idsi0011Pgm, Len.IDSI0011_PGM);
    }

    public String getIdsi0011Pgm() {
        return this.idsi0011Pgm;
    }

    public void setIdsi0011CodiceCompagniaAnia(int idsi0011CodiceCompagniaAnia) {
        this.idsi0011CodiceCompagniaAnia = idsi0011CodiceCompagniaAnia;
    }

    public int getIdsi0011CodiceCompagniaAnia() {
        return this.idsi0011CodiceCompagniaAnia;
    }

    public void setIdsi0011CodMainBatch(String idsi0011CodMainBatch) {
        this.idsi0011CodMainBatch = Functions.subString(idsi0011CodMainBatch, Len.IDSI0011_COD_MAIN_BATCH);
    }

    public String getIdsi0011CodMainBatch() {
        return this.idsi0011CodMainBatch;
    }

    public String getIdsi0011TipoMovimentoFormatted() {
        return this.idsi0011TipoMovimento;
    }

    public void setIdsi0011Sessione(String idsi0011Sessione) {
        this.idsi0011Sessione = Functions.subString(idsi0011Sessione, Len.IDSI0011_SESSIONE);
    }

    public String getIdsi0011Sessione() {
        return this.idsi0011Sessione;
    }

    public void setIdsi0011UserName(String idsi0011UserName) {
        this.idsi0011UserName = Functions.subString(idsi0011UserName, Len.IDSI0011_USER_NAME);
    }

    public String getIdsi0011UserName() {
        return this.idsi0011UserName;
    }

    public void setIdsi0011DataInizioEffetto(int idsi0011DataInizioEffetto) {
        this.idsi0011DataInizioEffetto = idsi0011DataInizioEffetto;
    }

    public int getIdsi0011DataInizioEffetto() {
        return this.idsi0011DataInizioEffetto;
    }

    public void setIdsi0011DataFineEffetto(int idsi0011DataFineEffetto) {
        this.idsi0011DataFineEffetto = idsi0011DataFineEffetto;
    }

    public int getIdsi0011DataFineEffetto() {
        return this.idsi0011DataFineEffetto;
    }

    public void setIdsi0011DataCompetenza(long idsi0011DataCompetenza) {
        this.idsi0011DataCompetenza = idsi0011DataCompetenza;
    }

    public long getIdsi0011DataCompetenza() {
        return this.idsi0011DataCompetenza;
    }

    public void setIdsi0011DataCompAggStor(long idsi0011DataCompAggStor) {
        this.idsi0011DataCompAggStor = idsi0011DataCompAggStor;
    }

    public long getIdsi0011DataCompAggStor() {
        return this.idsi0011DataCompAggStor;
    }

    public String getIdsi0011IdMoviAnnullatoFormatted() {
        return this.idsi0011IdMoviAnnullato;
    }

    public void setIdsi0011CodiceStrDato(String idsi0011CodiceStrDato) {
        this.idsi0011CodiceStrDato = Functions.subString(idsi0011CodiceStrDato, Len.IDSI0011_CODICE_STR_DATO);
    }

    public String getIdsi0011CodiceStrDato() {
        return this.idsi0011CodiceStrDato;
    }

    public String getIdsi0011CodiceStrDatoFormatted() {
        return Functions.padBlanks(getIdsi0011CodiceStrDato(), Len.IDSI0011_CODICE_STR_DATO);
    }

    public void setIdsi0011BufferDati(String idsi0011BufferDati) {
        this.idsi0011BufferDati = Functions.subString(idsi0011BufferDati, Len.IDSI0011_BUFFER_DATI);
    }

    public String getIdsi0011BufferDati() {
        return this.idsi0011BufferDati;
    }

    public String getIdsi0011BufferDatiFormatted() {
        return Functions.padBlanks(getIdsi0011BufferDati(), Len.IDSI0011_BUFFER_DATI);
    }

    public void setIdsi0011BufferWhereCond(String idsi0011BufferWhereCond) {
        this.idsi0011BufferWhereCond = Functions.subString(idsi0011BufferWhereCond, Len.IDSI0011_BUFFER_WHERE_COND);
    }

    public String getIdsi0011BufferWhereCond() {
        return this.idsi0011BufferWhereCond;
    }

    public void setFlr1(String flr1) {
        this.flr1 = Functions.subString(flr1, Len.FLR1);
    }

    public String getFlr1() {
        return this.flr1;
    }

    public Idsi0011FlagCodaTs getIdsi0011FlagCodaTs() {
        return idsi0011FlagCodaTs;
    }

    public Idsi0011FormatoDataDb getIdsi0011FormatoDataDb() {
        return idsi0011FormatoDataDb;
    }

    public Idsi0011IdentitaChiamante getIdsi0011IdentitaChiamante() {
        return idsi0011IdentitaChiamante;
    }

    public Idsv0001LivelloDebug getIdsi0011LivelloDebug() {
        return idsi0011LivelloDebug;
    }

    public Idsv0003LivelloOperazione getIdsi0011LivelloOperazione() {
        return idsi0011LivelloOperazione;
    }

    public Idsi0011ModalitaEsecutiva getIdsi0011ModalitaEsecutiva() {
        return idsi0011ModalitaEsecutiva;
    }

    public Idsv0003Operazione getIdsi0011Operazione() {
        return idsi0011Operazione;
    }

    public Idsv0003TrattamentoStoricita getIdsi0011TrattamentoStoricita() {
        return idsi0011TrattamentoStoricita;
    }

    @Override
    public byte[] serialize() {
        return getInIdss0010Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IDSI0011_PGM = 8;
        public static final int IDSI0011_CODICE_COMPAGNIA_ANIA = 3;
        public static final int IDSI0011_COD_MAIN_BATCH = 8;
        public static final int IDSI0011_TIPO_MOVIMENTO = 5;
        public static final int IDSI0011_SESSIONE = 20;
        public static final int IDSI0011_USER_NAME = 20;
        public static final int IDSI0011_DATA_INIZIO_EFFETTO = 5;
        public static final int IDSI0011_DATA_FINE_EFFETTO = 5;
        public static final int IDSI0011_DATA_COMPETENZA = 10;
        public static final int IDSI0011_DATA_COMP_AGG_STOR = 10;
        public static final int IDSI0011_ID_MOVI_ANNULLATO = 9;
        public static final int IDSI0011_CODICE_STR_DATO = 30;
        public static final int IDSI0011_BUFFER_DATI = 30000;
        public static final int IDSI0011_BUFFER_WHERE_COND = 300;
        public static final int FLR1 = 100;
        public static final int IDSI0011_AREA = IDSI0011_PGM + Idsi0011ModalitaEsecutiva.Len.IDSI0011_MODALITA_ESECUTIVA + IDSI0011_CODICE_COMPAGNIA_ANIA + IDSI0011_COD_MAIN_BATCH + IDSI0011_TIPO_MOVIMENTO + IDSI0011_SESSIONE + IDSI0011_USER_NAME + IDSI0011_DATA_INIZIO_EFFETTO + IDSI0011_DATA_FINE_EFFETTO + IDSI0011_DATA_COMPETENZA + IDSI0011_DATA_COMP_AGG_STOR + Idsv0003TrattamentoStoricita.Len.TRATTAMENTO_STORICITA + Idsi0011FormatoDataDb.Len.IDSI0011_FORMATO_DATA_DB + IDSI0011_ID_MOVI_ANNULLATO + Idsi0011IdentitaChiamante.Len.IDSI0011_IDENTITA_CHIAMANTE + Idsv0003LivelloOperazione.Len.LIVELLO_OPERAZIONE + Idsv0003Operazione.Len.OPERAZIONE + Idsi0011FlagCodaTs.Len.IDSI0011_FLAG_CODA_TS + IDSI0011_CODICE_STR_DATO + IDSI0011_BUFFER_DATI + IDSI0011_BUFFER_WHERE_COND + Idsv0001LivelloDebug.Len.LIVELLO_DEBUG + FLR1;
        public static final int IN_IDSS0010 = IDSI0011_AREA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IDSI0011_CODICE_COMPAGNIA_ANIA = 5;
            public static final int IDSI0011_DATA_INIZIO_EFFETTO = 8;
            public static final int IDSI0011_DATA_FINE_EFFETTO = 8;
            public static final int IDSI0011_DATA_COMPETENZA = 18;
            public static final int IDSI0011_DATA_COMP_AGG_STOR = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
