package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0003-MODALITA-ESECUTIVA<br>
 * Variable: IDSV0003-MODALITA-ESECUTIVA from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003ModalitaEsecutiva {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char ON_LINE = 'O';
    public static final char BATCH = 'B';
    public static final char BATCH_INFR = 'I';

    //==== METHODS ====
    public void setModalitaEsecutiva(char modalitaEsecutiva) {
        this.value = modalitaEsecutiva;
    }

    public char getModalitaEsecutiva() {
        return this.value;
    }

    public boolean isIdsv0003BatchInfr() {
        return value == BATCH_INFR;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MODALITA_ESECUTIVA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
