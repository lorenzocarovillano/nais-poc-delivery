package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MM-CNBZ-END2000-EF<br>
 * Variable: DFL-MM-CNBZ-END2000-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMmCnbzEnd2000Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMmCnbzEnd2000Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MM_CNBZ_END2000_EF;
    }

    public void setDflMmCnbzEnd2000Ef(int dflMmCnbzEnd2000Ef) {
        writeIntAsPacked(Pos.DFL_MM_CNBZ_END2000_EF, dflMmCnbzEnd2000Ef, Len.Int.DFL_MM_CNBZ_END2000_EF);
    }

    public void setDflMmCnbzEnd2000EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MM_CNBZ_END2000_EF, Pos.DFL_MM_CNBZ_END2000_EF);
    }

    /**Original name: DFL-MM-CNBZ-END2000-EF<br>*/
    public int getDflMmCnbzEnd2000Ef() {
        return readPackedAsInt(Pos.DFL_MM_CNBZ_END2000_EF, Len.Int.DFL_MM_CNBZ_END2000_EF);
    }

    public byte[] getDflMmCnbzEnd2000EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MM_CNBZ_END2000_EF, Pos.DFL_MM_CNBZ_END2000_EF);
        return buffer;
    }

    public void setDflMmCnbzEnd2000EfNull(String dflMmCnbzEnd2000EfNull) {
        writeString(Pos.DFL_MM_CNBZ_END2000_EF_NULL, dflMmCnbzEnd2000EfNull, Len.DFL_MM_CNBZ_END2000_EF_NULL);
    }

    /**Original name: DFL-MM-CNBZ-END2000-EF-NULL<br>*/
    public String getDflMmCnbzEnd2000EfNull() {
        return readString(Pos.DFL_MM_CNBZ_END2000_EF_NULL, Len.DFL_MM_CNBZ_END2000_EF_NULL);
    }

    public String getDflMmCnbzEnd2000EfNullFormatted() {
        return Functions.padBlanks(getDflMmCnbzEnd2000EfNull(), Len.DFL_MM_CNBZ_END2000_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MM_CNBZ_END2000_EF = 1;
        public static final int DFL_MM_CNBZ_END2000_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MM_CNBZ_END2000_EF = 3;
        public static final int DFL_MM_CNBZ_END2000_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MM_CNBZ_END2000_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
