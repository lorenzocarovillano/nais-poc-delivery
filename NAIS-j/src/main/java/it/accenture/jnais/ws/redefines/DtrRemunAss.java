package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-REMUN-ASS<br>
 * Variable: DTR-REMUN-ASS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_REMUN_ASS;
    }

    public void setDtrRemunAss(AfDecimal dtrRemunAss) {
        writeDecimalAsPacked(Pos.DTR_REMUN_ASS, dtrRemunAss.copy());
    }

    public void setDtrRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_REMUN_ASS, Pos.DTR_REMUN_ASS);
    }

    /**Original name: DTR-REMUN-ASS<br>*/
    public AfDecimal getDtrRemunAss() {
        return readPackedAsDecimal(Pos.DTR_REMUN_ASS, Len.Int.DTR_REMUN_ASS, Len.Fract.DTR_REMUN_ASS);
    }

    public byte[] getDtrRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_REMUN_ASS, Pos.DTR_REMUN_ASS);
        return buffer;
    }

    public void setDtrRemunAssNull(String dtrRemunAssNull) {
        writeString(Pos.DTR_REMUN_ASS_NULL, dtrRemunAssNull, Len.DTR_REMUN_ASS_NULL);
    }

    /**Original name: DTR-REMUN-ASS-NULL<br>*/
    public String getDtrRemunAssNull() {
        return readString(Pos.DTR_REMUN_ASS_NULL, Len.DTR_REMUN_ASS_NULL);
    }

    public String getDtrRemunAssNullFormatted() {
        return Functions.padBlanks(getDtrRemunAssNull(), Len.DTR_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_REMUN_ASS = 1;
        public static final int DTR_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_REMUN_ASS = 8;
        public static final int DTR_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
