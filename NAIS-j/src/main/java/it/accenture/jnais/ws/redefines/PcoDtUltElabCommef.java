package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-COMMEF<br>
 * Variable: PCO-DT-ULT-ELAB-COMMEF from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabCommef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabCommef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_COMMEF;
    }

    public void setPcoDtUltElabCommef(int pcoDtUltElabCommef) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_COMMEF, pcoDtUltElabCommef, Len.Int.PCO_DT_ULT_ELAB_COMMEF);
    }

    public void setPcoDtUltElabCommefFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_COMMEF, Pos.PCO_DT_ULT_ELAB_COMMEF);
    }

    /**Original name: PCO-DT-ULT-ELAB-COMMEF<br>*/
    public int getPcoDtUltElabCommef() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_COMMEF, Len.Int.PCO_DT_ULT_ELAB_COMMEF);
    }

    public byte[] getPcoDtUltElabCommefAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_COMMEF, Pos.PCO_DT_ULT_ELAB_COMMEF);
        return buffer;
    }

    public void initPcoDtUltElabCommefHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_COMMEF, Len.PCO_DT_ULT_ELAB_COMMEF, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabCommefNull(String pcoDtUltElabCommefNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_COMMEF_NULL, pcoDtUltElabCommefNull, Len.PCO_DT_ULT_ELAB_COMMEF_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-COMMEF-NULL<br>*/
    public String getPcoDtUltElabCommefNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_COMMEF_NULL, Len.PCO_DT_ULT_ELAB_COMMEF_NULL);
    }

    public String getPcoDtUltElabCommefNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabCommefNull(), Len.PCO_DT_ULT_ELAB_COMMEF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_COMMEF = 1;
        public static final int PCO_DT_ULT_ELAB_COMMEF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_COMMEF = 5;
        public static final int PCO_DT_ULT_ELAB_COMMEF_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_COMMEF = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
