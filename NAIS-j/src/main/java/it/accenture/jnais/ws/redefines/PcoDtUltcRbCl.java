package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-RB-CL<br>
 * Variable: PCO-DT-ULTC-RB-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcRbCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcRbCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_RB_CL;
    }

    public void setPcoDtUltcRbCl(int pcoDtUltcRbCl) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_RB_CL, pcoDtUltcRbCl, Len.Int.PCO_DT_ULTC_RB_CL);
    }

    public void setPcoDtUltcRbClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_RB_CL, Pos.PCO_DT_ULTC_RB_CL);
    }

    /**Original name: PCO-DT-ULTC-RB-CL<br>*/
    public int getPcoDtUltcRbCl() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_RB_CL, Len.Int.PCO_DT_ULTC_RB_CL);
    }

    public byte[] getPcoDtUltcRbClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_RB_CL, Pos.PCO_DT_ULTC_RB_CL);
        return buffer;
    }

    public void initPcoDtUltcRbClHighValues() {
        fill(Pos.PCO_DT_ULTC_RB_CL, Len.PCO_DT_ULTC_RB_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcRbClNull(String pcoDtUltcRbClNull) {
        writeString(Pos.PCO_DT_ULTC_RB_CL_NULL, pcoDtUltcRbClNull, Len.PCO_DT_ULTC_RB_CL_NULL);
    }

    /**Original name: PCO-DT-ULTC-RB-CL-NULL<br>*/
    public String getPcoDtUltcRbClNull() {
        return readString(Pos.PCO_DT_ULTC_RB_CL_NULL, Len.PCO_DT_ULTC_RB_CL_NULL);
    }

    public String getPcoDtUltcRbClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltcRbClNull(), Len.PCO_DT_ULTC_RB_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_RB_CL = 1;
        public static final int PCO_DT_ULTC_RB_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_RB_CL = 5;
        public static final int PCO_DT_ULTC_RB_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_RB_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
