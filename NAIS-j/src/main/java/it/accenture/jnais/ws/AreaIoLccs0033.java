package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.collection.creation.CollectionCreator;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.Lccc0033FlagRecupProvv;
import it.accenture.jnais.ws.enums.Lccc0033MacroFunzione;
import it.accenture.jnais.ws.enums.Lccc0033ModalitaServizio;
import it.accenture.jnais.ws.occurs.Lccc0033RecuperoProvv;

/**Original name: AREA-IO-LCCS0033<br>
 * Variable: AREA-IO-LCCS0033 from program LCCS0033<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoLccs0033 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int RECUPERO_PROVV_MAXOCCURS = 15;
    //Original name: LCCC0033-MACRO-FUNZIONE
    private Lccc0033MacroFunzione macroFunzione = new Lccc0033MacroFunzione();
    //Original name: LCCC0033-MODALITA-SERVIZIO
    private Lccc0033ModalitaServizio modalitaServizio = new Lccc0033ModalitaServizio();
    //Original name: LCCC0033-COD-SOGG
    private String codSogg = DefaultValues.stringVal(Len.COD_SOGG);
    //Original name: LCCC0033-TP-RAPP-ANA
    private String tpRappAna = DefaultValues.stringVal(Len.TP_RAPP_ANA);
    //Original name: LCCC0033-DT-DECOR-POL
    private int dtDecorPol = DefaultValues.INT_VAL;
    //Original name: LCCC0033-DT-STORNO-POL
    private int dtStornoPol = DefaultValues.INT_VAL;
    //Original name: LCCC0033-FLAG-RECUP-PROVV
    private Lccc0033FlagRecupProvv flagRecupProvv = new Lccc0033FlagRecupProvv();
    /**Original name: LCCC0033-ELE-MAX-RECUP-PROVV<br>
	 * <pre>-- lista polizze stesso contraente per il recupero provv.</pre>*/
    private short eleMaxRecupProvv = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCC0033-RECUPERO-PROVV
    private Lccc0033RecuperoProvv[] recuperoProvv = new Lccc0033RecuperoProvv[RECUPERO_PROVV_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIoLccs0033() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_LCCS0033;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoLccs0033Bytes(buf);
    }

    public void init() {
        for (int recuperoProvvIdx = 1; recuperoProvvIdx <= RECUPERO_PROVV_MAXOCCURS; recuperoProvvIdx++) {
            recuperoProvv[recuperoProvvIdx - 1] = new Lccc0033RecuperoProvv();
        }
    }

    public String getAreaIoLccs0033Formatted() {
        return MarshalByteExt.bufferToStr(getAreaIoLccs0033Bytes());
    }

    public void setAreaIoLccs0033Bytes(byte[] buffer) {
        setAreaIoLccs0033Bytes(buffer, 1);
    }

    public byte[] getAreaIoLccs0033Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_LCCS0033];
        return getAreaIoLccs0033Bytes(buffer, 1);
    }

    public void setAreaIoLccs0033Bytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
    }

    public byte[] getAreaIoLccs0033Bytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        position += Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        macroFunzione.setMacroFunzione(MarshalByte.readString(buffer, position, Lccc0033MacroFunzione.Len.MACRO_FUNZIONE));
        position += Lccc0033MacroFunzione.Len.MACRO_FUNZIONE;
        modalitaServizio.setModalitaServizio(MarshalByte.readString(buffer, position, Lccc0033ModalitaServizio.Len.MODALITA_SERVIZIO));
        position += Lccc0033ModalitaServizio.Len.MODALITA_SERVIZIO;
        codSogg = MarshalByte.readString(buffer, position, Len.COD_SOGG);
        position += Len.COD_SOGG;
        tpRappAna = MarshalByte.readString(buffer, position, Len.TP_RAPP_ANA);
        position += Len.TP_RAPP_ANA;
        dtDecorPol = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_DECOR_POL, 0);
        position += Len.DT_DECOR_POL;
        dtStornoPol = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_STORNO_POL, 0);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, macroFunzione.getMacroFunzione(), Lccc0033MacroFunzione.Len.MACRO_FUNZIONE);
        position += Lccc0033MacroFunzione.Len.MACRO_FUNZIONE;
        MarshalByte.writeString(buffer, position, modalitaServizio.getModalitaServizio(), Lccc0033ModalitaServizio.Len.MODALITA_SERVIZIO);
        position += Lccc0033ModalitaServizio.Len.MODALITA_SERVIZIO;
        MarshalByte.writeString(buffer, position, codSogg, Len.COD_SOGG);
        position += Len.COD_SOGG;
        MarshalByte.writeString(buffer, position, tpRappAna, Len.TP_RAPP_ANA);
        position += Len.TP_RAPP_ANA;
        MarshalByte.writeIntAsPacked(buffer, position, dtDecorPol, Len.Int.DT_DECOR_POL, 0);
        position += Len.DT_DECOR_POL;
        MarshalByte.writeIntAsPacked(buffer, position, dtStornoPol, Len.Int.DT_STORNO_POL, 0);
        return buffer;
    }

    public void setCodSogg(String codSogg) {
        this.codSogg = Functions.subString(codSogg, Len.COD_SOGG);
    }

    public String getCodSogg() {
        return this.codSogg;
    }

    public void setTpRappAna(String tpRappAna) {
        this.tpRappAna = Functions.subString(tpRappAna, Len.TP_RAPP_ANA);
    }

    public String getTpRappAna() {
        return this.tpRappAna;
    }

    public void setDtDecorPol(int dtDecorPol) {
        this.dtDecorPol = dtDecorPol;
    }

    public int getDtDecorPol() {
        return this.dtDecorPol;
    }

    public String getDtDecorPolFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getDtDecorPol()).toString();
    }

    public void setDtStornoPol(int dtStornoPol) {
        this.dtStornoPol = dtStornoPol;
    }

    public int getDtStornoPol() {
        return this.dtStornoPol;
    }

    public String getDtStornoPolFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getDtStornoPol()).toString();
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        flagRecupProvv.setFlagRecupProvv(MarshalByte.readString(buffer, position, Lccc0033FlagRecupProvv.Len.FLAG_RECUP_PROVV));
        position += Lccc0033FlagRecupProvv.Len.FLAG_RECUP_PROVV;
        eleMaxRecupProvv = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= RECUPERO_PROVV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                recuperoProvv[idx - 1].setRecuperoProvvBytes(buffer, position);
                position += Lccc0033RecuperoProvv.Len.RECUPERO_PROVV;
            }
            else {
                recuperoProvv[idx - 1].initRecuperoProvvSpaces();
                position += Lccc0033RecuperoProvv.Len.RECUPERO_PROVV;
            }
        }
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flagRecupProvv.getFlagRecupProvv(), Lccc0033FlagRecupProvv.Len.FLAG_RECUP_PROVV);
        position += Lccc0033FlagRecupProvv.Len.FLAG_RECUP_PROVV;
        MarshalByte.writeBinaryShort(buffer, position, eleMaxRecupProvv);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= RECUPERO_PROVV_MAXOCCURS; idx++) {
            recuperoProvv[idx - 1].getRecuperoProvvBytes(buffer, position);
            position += Lccc0033RecuperoProvv.Len.RECUPERO_PROVV;
        }
        return buffer;
    }

    public AfDecimal[] getAllRecuperoProvvLccc0033ImpPremioAnnuo(int recuperoProvvIdx) {
        int min_recuperoProvv = ((recuperoProvvIdx == -1) ? 1 : recuperoProvvIdx);
        int max_recuperoProvv = ((recuperoProvvIdx == -1) ? RECUPERO_PROVV_MAXOCCURS : recuperoProvvIdx);
        AfDecimal[] arr_ = CollectionCreator.getDecimalFactory(15, 3).createArray(((recuperoProvvIdx != -1) ? 1 : RECUPERO_PROVV_MAXOCCURS));
        int pos_ = 1;
        for (int recuperoProvv_tmp = min_recuperoProvv; recuperoProvv_tmp <= max_recuperoProvv; recuperoProvv_tmp++) {
            arr_[pos_++ - 1].assign(recuperoProvv[recuperoProvv_tmp - 1].getImpPremioAnnuo());
        }
        return arr_;
    }

    public void setEleMaxRecupProvv(short eleMaxRecupProvv) {
        this.eleMaxRecupProvv = eleMaxRecupProvv;
    }

    public short getEleMaxRecupProvv() {
        return this.eleMaxRecupProvv;
    }

    public Lccc0033FlagRecupProvv getFlagRecupProvv() {
        return flagRecupProvv;
    }

    public Lccc0033MacroFunzione getMacroFunzione() {
        return macroFunzione;
    }

    public Lccc0033ModalitaServizio getModalitaServizio() {
        return modalitaServizio;
    }

    public Lccc0033RecuperoProvv getRecuperoProvv(int idx) {
        return recuperoProvv[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getAreaIoLccs0033Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_SOGG = 20;
        public static final int TP_RAPP_ANA = 2;
        public static final int DT_DECOR_POL = 5;
        public static final int DT_STORNO_POL = 5;
        public static final int DATI_INPUT = Lccc0033MacroFunzione.Len.MACRO_FUNZIONE + Lccc0033ModalitaServizio.Len.MODALITA_SERVIZIO + COD_SOGG + TP_RAPP_ANA + DT_DECOR_POL + DT_STORNO_POL;
        public static final int ELE_MAX_RECUP_PROVV = 2;
        public static final int DATI_OUTPUT = Lccc0033FlagRecupProvv.Len.FLAG_RECUP_PROVV + ELE_MAX_RECUP_PROVV + AreaIoLccs0033.RECUPERO_PROVV_MAXOCCURS * Lccc0033RecuperoProvv.Len.RECUPERO_PROVV;
        public static final int AREA_IO_LCCS0033 = DATI_INPUT + DATI_OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DT_DECOR_POL = 8;
            public static final int DT_STORNO_POL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
