package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-CAUS-SCARTO<br>
 * Variable: WS-TP-CAUS-SCARTO from copybook LCCVXCS0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpCausScarto {

    //==== PROPERTIES ====
    private String value = "";
    public static final String POLIZZA_NON_PRESENTE = "01";
    public static final String OPERAZIONE_IN_CORSO = "02";
    public static final String CONTR_FORMALI = "03";
    public static final String CONTR_INDEROGABILI = "04";
    public static final String POLIZZA_NON_VIGORE = "05";
    public static final String ERRORE_GESTIONE = "06";
    public static final String SCARTO_DEROGHE = "07";
    public static final String RICHIESTA_IN_ATTESA = "08";
    public static final String SCADENZA_ANTICIPO = "09";
    public static final String RICHIESTA_SENZA_QDD = "95";
    public static final String DEROGA_RIFIUTATA = "96";
    public static final String RICHIESTA_ANNULLATA = "97";
    public static final String RICHIESTA_EVASA_ONLINE = "98";
    public static final String RICHIESTA_DUPLICATA = "99";

    //==== METHODS ====
    public void setWsTpCausScarto(String wsTpCausScarto) {
        this.value = Functions.subString(wsTpCausScarto, Len.WS_TP_CAUS_SCARTO);
    }

    public String getWsTpCausScarto() {
        return this.value;
    }

    public void setScartoDeroghe() {
        value = SCARTO_DEROGHE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_CAUS_SCARTO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
