package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-FG-DEFAULT-GRAVITA<br>
 * Variable: WS-FG-DEFAULT-GRAVITA from program LCCS0023<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFgDefaultGravita {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char BLOCCANTE = 'B';
    public static final char AMMISSIBILE = 'A';

    //==== METHODS ====
    public void setWsFgDefaultGravita(char wsFgDefaultGravita) {
        this.value = wsFgDefaultGravita;
    }

    public char getWsFgDefaultGravita() {
        return this.value;
    }

    public void setBloccante() {
        value = BLOCCANTE;
    }

    public void setAmmissibile() {
        value = AMMISSIBILE;
    }
}
