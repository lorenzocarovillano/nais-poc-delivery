package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: E15-ID-RAPP-ANA-COLLG<br>
 * Variable: E15-ID-RAPP-ANA-COLLG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E15IdRappAnaCollg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E15IdRappAnaCollg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E15_ID_RAPP_ANA_COLLG;
    }

    public void setE15IdRappAnaCollg(int e15IdRappAnaCollg) {
        writeIntAsPacked(Pos.E15_ID_RAPP_ANA_COLLG, e15IdRappAnaCollg, Len.Int.E15_ID_RAPP_ANA_COLLG);
    }

    public void setE15IdRappAnaCollgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E15_ID_RAPP_ANA_COLLG, Pos.E15_ID_RAPP_ANA_COLLG);
    }

    /**Original name: E15-ID-RAPP-ANA-COLLG<br>*/
    public int getE15IdRappAnaCollg() {
        return readPackedAsInt(Pos.E15_ID_RAPP_ANA_COLLG, Len.Int.E15_ID_RAPP_ANA_COLLG);
    }

    public byte[] getE15IdRappAnaCollgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E15_ID_RAPP_ANA_COLLG, Pos.E15_ID_RAPP_ANA_COLLG);
        return buffer;
    }

    public void setE15IdRappAnaCollgNull(String e15IdRappAnaCollgNull) {
        writeString(Pos.E15_ID_RAPP_ANA_COLLG_NULL, e15IdRappAnaCollgNull, Len.E15_ID_RAPP_ANA_COLLG_NULL);
    }

    /**Original name: E15-ID-RAPP-ANA-COLLG-NULL<br>*/
    public String getE15IdRappAnaCollgNull() {
        return readString(Pos.E15_ID_RAPP_ANA_COLLG_NULL, Len.E15_ID_RAPP_ANA_COLLG_NULL);
    }

    public String getE15IdRappAnaCollgNullFormatted() {
        return Functions.padBlanks(getE15IdRappAnaCollgNull(), Len.E15_ID_RAPP_ANA_COLLG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E15_ID_RAPP_ANA_COLLG = 1;
        public static final int E15_ID_RAPP_ANA_COLLG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E15_ID_RAPP_ANA_COLLG = 5;
        public static final int E15_ID_RAPP_ANA_COLLG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E15_ID_RAPP_ANA_COLLG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
