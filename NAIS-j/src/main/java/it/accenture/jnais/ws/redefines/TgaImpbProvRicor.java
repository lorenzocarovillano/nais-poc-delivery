package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-IMPB-PROV-RICOR<br>
 * Variable: TGA-IMPB-PROV-RICOR from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaImpbProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaImpbProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_IMPB_PROV_RICOR;
    }

    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        writeDecimalAsPacked(Pos.TGA_IMPB_PROV_RICOR, tgaImpbProvRicor.copy());
    }

    public void setTgaImpbProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_IMPB_PROV_RICOR, Pos.TGA_IMPB_PROV_RICOR);
    }

    /**Original name: TGA-IMPB-PROV-RICOR<br>*/
    public AfDecimal getTgaImpbProvRicor() {
        return readPackedAsDecimal(Pos.TGA_IMPB_PROV_RICOR, Len.Int.TGA_IMPB_PROV_RICOR, Len.Fract.TGA_IMPB_PROV_RICOR);
    }

    public byte[] getTgaImpbProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_IMPB_PROV_RICOR, Pos.TGA_IMPB_PROV_RICOR);
        return buffer;
    }

    public void setTgaImpbProvRicorNull(String tgaImpbProvRicorNull) {
        writeString(Pos.TGA_IMPB_PROV_RICOR_NULL, tgaImpbProvRicorNull, Len.TGA_IMPB_PROV_RICOR_NULL);
    }

    /**Original name: TGA-IMPB-PROV-RICOR-NULL<br>*/
    public String getTgaImpbProvRicorNull() {
        return readString(Pos.TGA_IMPB_PROV_RICOR_NULL, Len.TGA_IMPB_PROV_RICOR_NULL);
    }

    public String getTgaImpbProvRicorNullFormatted() {
        return Functions.padBlanks(getTgaImpbProvRicorNull(), Len.TGA_IMPB_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_PROV_RICOR = 1;
        public static final int TGA_IMPB_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_IMPB_PROV_RICOR = 8;
        public static final int TGA_IMPB_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_IMPB_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
