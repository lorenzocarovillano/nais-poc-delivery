package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PROV-ACQ-1AA<br>
 * Variable: TDR-TOT-PROV-ACQ-1AA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PROV_ACQ1AA;
    }

    public void setTdrTotProvAcq1aa(AfDecimal tdrTotProvAcq1aa) {
        writeDecimalAsPacked(Pos.TDR_TOT_PROV_ACQ1AA, tdrTotProvAcq1aa.copy());
    }

    public void setTdrTotProvAcq1aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PROV_ACQ1AA, Pos.TDR_TOT_PROV_ACQ1AA);
    }

    /**Original name: TDR-TOT-PROV-ACQ-1AA<br>*/
    public AfDecimal getTdrTotProvAcq1aa() {
        return readPackedAsDecimal(Pos.TDR_TOT_PROV_ACQ1AA, Len.Int.TDR_TOT_PROV_ACQ1AA, Len.Fract.TDR_TOT_PROV_ACQ1AA);
    }

    public byte[] getTdrTotProvAcq1aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PROV_ACQ1AA, Pos.TDR_TOT_PROV_ACQ1AA);
        return buffer;
    }

    public void setTdrTotProvAcq1aaNull(String tdrTotProvAcq1aaNull) {
        writeString(Pos.TDR_TOT_PROV_ACQ1AA_NULL, tdrTotProvAcq1aaNull, Len.TDR_TOT_PROV_ACQ1AA_NULL);
    }

    /**Original name: TDR-TOT-PROV-ACQ-1AA-NULL<br>*/
    public String getTdrTotProvAcq1aaNull() {
        return readString(Pos.TDR_TOT_PROV_ACQ1AA_NULL, Len.TDR_TOT_PROV_ACQ1AA_NULL);
    }

    public String getTdrTotProvAcq1aaNullFormatted() {
        return Functions.padBlanks(getTdrTotProvAcq1aaNull(), Len.TDR_TOT_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_ACQ1AA = 1;
        public static final int TDR_TOT_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PROV_ACQ1AA = 8;
        public static final int TDR_TOT_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
