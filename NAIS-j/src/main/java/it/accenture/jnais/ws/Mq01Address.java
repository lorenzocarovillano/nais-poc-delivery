package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: MQ01-ADDRESS<br>
 * Variable: MQ01-ADDRESS from program ISPS0211<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Mq01Address extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WS-ADDRESS
    private int wsAddress = DefaultValues.BIN_INT_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MQ01_ADDRESS;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMq01AddressBytes(buf);
    }

    public String getMq01AddressFormatted() {
        return MarshalByteExt.bufferToStr(getMq01AddressBytes());
    }

    public void setMq01AddressBytes(byte[] buffer) {
        setMq01AddressBytes(buffer, 1);
    }

    public byte[] getMq01AddressBytes() {
        byte[] buffer = new byte[Len.MQ01_ADDRESS];
        return getMq01AddressBytes(buffer, 1);
    }

    public void setMq01AddressBytes(byte[] buffer, int offset) {
        int position = offset;
        wsAddress = MarshalByte.readBinaryInt(buffer, position);
    }

    public byte[] getMq01AddressBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, wsAddress);
        return buffer;
    }

    public void setWsAddress(int wsAddress) {
        this.wsAddress = wsAddress;
    }

    public int getWsAddress() {
        return this.wsAddress;
    }

    @Override
    public byte[] serialize() {
        return getMq01AddressBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ADDRESS = 4;
        public static final int MQ01_ADDRESS = WS_ADDRESS;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
