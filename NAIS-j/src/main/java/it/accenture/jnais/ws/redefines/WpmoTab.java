package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-TAB<br>
 * Variable: WPMO-TAB from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_PARAM_MOV_MAXOCCURS = 100;
    public static final char WPMO_ST_ADD = 'A';
    public static final char WPMO_ST_MOD = 'M';
    public static final char WPMO_ST_INV = 'I';
    public static final char WPMO_ST_DEL = 'D';
    public static final char WPMO_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WpmoTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_TAB;
    }

    public String getWpmoTabFormatted() {
        return readFixedString(Pos.WPMO_TAB, Len.WPMO_TAB);
    }

    public void setWpmoTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_TAB, Pos.WPMO_TAB);
    }

    public byte[] getWpmoTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_TAB, Pos.WPMO_TAB);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wpmoStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WPMO-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA PARAM_MOVI
	 *    ALIAS PMO
	 *    ULTIMO AGG. 17 FEB 2015
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wpmoStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wpmoIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WPMO-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wpmoIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdParamMovi(int idParamMoviIdx, int idParamMovi) {
        int position = Pos.wpmoIdParamMovi(idParamMoviIdx - 1);
        writeIntAsPacked(position, idParamMovi, Len.Int.ID_PARAM_MOVI);
    }

    /**Original name: WPMO-ID-PARAM-MOVI<br>*/
    public int getIdParamMovi(int idParamMoviIdx) {
        int position = Pos.wpmoIdParamMovi(idParamMoviIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PARAM_MOVI);
    }

    public void setIdOgg(int idOggIdx, int idOgg) {
        int position = Pos.wpmoIdOgg(idOggIdx - 1);
        writeIntAsPacked(position, idOgg, Len.Int.ID_OGG);
    }

    /**Original name: WPMO-ID-OGG<br>*/
    public int getIdOgg(int idOggIdx) {
        int position = Pos.wpmoIdOgg(idOggIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_OGG);
    }

    public void setTpOgg(int tpOggIdx, String tpOgg) {
        int position = Pos.wpmoTpOgg(tpOggIdx - 1);
        writeString(position, tpOgg, Len.TP_OGG);
    }

    /**Original name: WPMO-TP-OGG<br>*/
    public String getTpOgg(int tpOggIdx) {
        int position = Pos.wpmoTpOgg(tpOggIdx - 1);
        return readString(position, Len.TP_OGG);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wpmoIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WPMO-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wpmoIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wpmoIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WPMO-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wpmoIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setWpmoIdMoviChiuNull(int wpmoIdMoviChiuNullIdx, String wpmoIdMoviChiuNull) {
        int position = Pos.wpmoIdMoviChiuNull(wpmoIdMoviChiuNullIdx - 1);
        writeString(position, wpmoIdMoviChiuNull, Len.WPMO_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WPMO-ID-MOVI-CHIU-NULL<br>*/
    public String getWpmoIdMoviChiuNull(int wpmoIdMoviChiuNullIdx) {
        int position = Pos.wpmoIdMoviChiuNull(wpmoIdMoviChiuNullIdx - 1);
        return readString(position, Len.WPMO_ID_MOVI_CHIU_NULL);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wpmoDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WPMO-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wpmoDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wpmoDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WPMO-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wpmoDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wpmoCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WPMO-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wpmoCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setTpMovi(int tpMoviIdx, int tpMovi) {
        int position = Pos.wpmoTpMovi(tpMoviIdx - 1);
        writeIntAsPacked(position, tpMovi, Len.Int.TP_MOVI);
    }

    /**Original name: WPMO-TP-MOVI<br>*/
    public int getTpMovi(int tpMoviIdx) {
        int position = Pos.wpmoTpMovi(tpMoviIdx - 1);
        return readPackedAsInt(position, Len.Int.TP_MOVI);
    }

    public void setWpmoTpMoviNull(int wpmoTpMoviNullIdx, String wpmoTpMoviNull) {
        int position = Pos.wpmoTpMoviNull(wpmoTpMoviNullIdx - 1);
        writeString(position, wpmoTpMoviNull, Len.WPMO_TP_MOVI_NULL);
    }

    /**Original name: WPMO-TP-MOVI-NULL<br>*/
    public String getWpmoTpMoviNull(int wpmoTpMoviNullIdx) {
        int position = Pos.wpmoTpMoviNull(wpmoTpMoviNullIdx - 1);
        return readString(position, Len.WPMO_TP_MOVI_NULL);
    }

    public void setFrqMovi(int frqMoviIdx, int frqMovi) {
        int position = Pos.wpmoFrqMovi(frqMoviIdx - 1);
        writeIntAsPacked(position, frqMovi, Len.Int.FRQ_MOVI);
    }

    /**Original name: WPMO-FRQ-MOVI<br>*/
    public int getFrqMovi(int frqMoviIdx) {
        int position = Pos.wpmoFrqMovi(frqMoviIdx - 1);
        return readPackedAsInt(position, Len.Int.FRQ_MOVI);
    }

    public void setWpmoFrqMoviNull(int wpmoFrqMoviNullIdx, String wpmoFrqMoviNull) {
        int position = Pos.wpmoFrqMoviNull(wpmoFrqMoviNullIdx - 1);
        writeString(position, wpmoFrqMoviNull, Len.WPMO_FRQ_MOVI_NULL);
    }

    /**Original name: WPMO-FRQ-MOVI-NULL<br>*/
    public String getWpmoFrqMoviNull(int wpmoFrqMoviNullIdx) {
        int position = Pos.wpmoFrqMoviNull(wpmoFrqMoviNullIdx - 1);
        return readString(position, Len.WPMO_FRQ_MOVI_NULL);
    }

    public String getWpmoFrqMoviNullFormatted(int wpmoFrqMoviNullIdx) {
        return Functions.padBlanks(getWpmoFrqMoviNull(wpmoFrqMoviNullIdx), Len.WPMO_FRQ_MOVI_NULL);
    }

    public void setDurAa(int durAaIdx, int durAa) {
        int position = Pos.wpmoDurAa(durAaIdx - 1);
        writeIntAsPacked(position, durAa, Len.Int.DUR_AA);
    }

    /**Original name: WPMO-DUR-AA<br>*/
    public int getDurAa(int durAaIdx) {
        int position = Pos.wpmoDurAa(durAaIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_AA);
    }

    public void setWpmoDurAaNull(int wpmoDurAaNullIdx, String wpmoDurAaNull) {
        int position = Pos.wpmoDurAaNull(wpmoDurAaNullIdx - 1);
        writeString(position, wpmoDurAaNull, Len.WPMO_DUR_AA_NULL);
    }

    /**Original name: WPMO-DUR-AA-NULL<br>*/
    public String getWpmoDurAaNull(int wpmoDurAaNullIdx) {
        int position = Pos.wpmoDurAaNull(wpmoDurAaNullIdx - 1);
        return readString(position, Len.WPMO_DUR_AA_NULL);
    }

    public void setDurMm(int durMmIdx, int durMm) {
        int position = Pos.wpmoDurMm(durMmIdx - 1);
        writeIntAsPacked(position, durMm, Len.Int.DUR_MM);
    }

    /**Original name: WPMO-DUR-MM<br>*/
    public int getDurMm(int durMmIdx) {
        int position = Pos.wpmoDurMm(durMmIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_MM);
    }

    public void setWpmoDurMmNull(int wpmoDurMmNullIdx, String wpmoDurMmNull) {
        int position = Pos.wpmoDurMmNull(wpmoDurMmNullIdx - 1);
        writeString(position, wpmoDurMmNull, Len.WPMO_DUR_MM_NULL);
    }

    /**Original name: WPMO-DUR-MM-NULL<br>*/
    public String getWpmoDurMmNull(int wpmoDurMmNullIdx) {
        int position = Pos.wpmoDurMmNull(wpmoDurMmNullIdx - 1);
        return readString(position, Len.WPMO_DUR_MM_NULL);
    }

    public void setDurGg(int durGgIdx, int durGg) {
        int position = Pos.wpmoDurGg(durGgIdx - 1);
        writeIntAsPacked(position, durGg, Len.Int.DUR_GG);
    }

    /**Original name: WPMO-DUR-GG<br>*/
    public int getDurGg(int durGgIdx) {
        int position = Pos.wpmoDurGg(durGgIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_GG);
    }

    public void setWpmoDurGgNull(int wpmoDurGgNullIdx, String wpmoDurGgNull) {
        int position = Pos.wpmoDurGgNull(wpmoDurGgNullIdx - 1);
        writeString(position, wpmoDurGgNull, Len.WPMO_DUR_GG_NULL);
    }

    /**Original name: WPMO-DUR-GG-NULL<br>*/
    public String getWpmoDurGgNull(int wpmoDurGgNullIdx) {
        int position = Pos.wpmoDurGgNull(wpmoDurGgNullIdx - 1);
        return readString(position, Len.WPMO_DUR_GG_NULL);
    }

    public void setDtRicorPrec(int dtRicorPrecIdx, int dtRicorPrec) {
        int position = Pos.wpmoDtRicorPrec(dtRicorPrecIdx - 1);
        writeIntAsPacked(position, dtRicorPrec, Len.Int.DT_RICOR_PREC);
    }

    /**Original name: WPMO-DT-RICOR-PREC<br>*/
    public int getDtRicorPrec(int dtRicorPrecIdx) {
        int position = Pos.wpmoDtRicorPrec(dtRicorPrecIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_RICOR_PREC);
    }

    public void setWpmoDtRicorPrecNull(int wpmoDtRicorPrecNullIdx, String wpmoDtRicorPrecNull) {
        int position = Pos.wpmoDtRicorPrecNull(wpmoDtRicorPrecNullIdx - 1);
        writeString(position, wpmoDtRicorPrecNull, Len.WPMO_DT_RICOR_PREC_NULL);
    }

    /**Original name: WPMO-DT-RICOR-PREC-NULL<br>*/
    public String getWpmoDtRicorPrecNull(int wpmoDtRicorPrecNullIdx) {
        int position = Pos.wpmoDtRicorPrecNull(wpmoDtRicorPrecNullIdx - 1);
        return readString(position, Len.WPMO_DT_RICOR_PREC_NULL);
    }

    public void setDtRicorSucc(int dtRicorSuccIdx, int dtRicorSucc) {
        int position = Pos.wpmoDtRicorSucc(dtRicorSuccIdx - 1);
        writeIntAsPacked(position, dtRicorSucc, Len.Int.DT_RICOR_SUCC);
    }

    /**Original name: WPMO-DT-RICOR-SUCC<br>*/
    public int getDtRicorSucc(int dtRicorSuccIdx) {
        int position = Pos.wpmoDtRicorSucc(dtRicorSuccIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_RICOR_SUCC);
    }

    public void setWpmoDtRicorSuccNull(int wpmoDtRicorSuccNullIdx, String wpmoDtRicorSuccNull) {
        int position = Pos.wpmoDtRicorSuccNull(wpmoDtRicorSuccNullIdx - 1);
        writeString(position, wpmoDtRicorSuccNull, Len.WPMO_DT_RICOR_SUCC_NULL);
    }

    /**Original name: WPMO-DT-RICOR-SUCC-NULL<br>*/
    public String getWpmoDtRicorSuccNull(int wpmoDtRicorSuccNullIdx) {
        int position = Pos.wpmoDtRicorSuccNull(wpmoDtRicorSuccNullIdx - 1);
        return readString(position, Len.WPMO_DT_RICOR_SUCC_NULL);
    }

    public void setPcIntrFraz(int pcIntrFrazIdx, AfDecimal pcIntrFraz) {
        int position = Pos.wpmoPcIntrFraz(pcIntrFrazIdx - 1);
        writeDecimalAsPacked(position, pcIntrFraz.copy());
    }

    /**Original name: WPMO-PC-INTR-FRAZ<br>*/
    public AfDecimal getPcIntrFraz(int pcIntrFrazIdx) {
        int position = Pos.wpmoPcIntrFraz(pcIntrFrazIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_INTR_FRAZ, Len.Fract.PC_INTR_FRAZ);
    }

    public void setWpmoPcIntrFrazNull(int wpmoPcIntrFrazNullIdx, String wpmoPcIntrFrazNull) {
        int position = Pos.wpmoPcIntrFrazNull(wpmoPcIntrFrazNullIdx - 1);
        writeString(position, wpmoPcIntrFrazNull, Len.WPMO_PC_INTR_FRAZ_NULL);
    }

    /**Original name: WPMO-PC-INTR-FRAZ-NULL<br>*/
    public String getWpmoPcIntrFrazNull(int wpmoPcIntrFrazNullIdx) {
        int position = Pos.wpmoPcIntrFrazNull(wpmoPcIntrFrazNullIdx - 1);
        return readString(position, Len.WPMO_PC_INTR_FRAZ_NULL);
    }

    public void setImpBnsDaScoTot(int impBnsDaScoTotIdx, AfDecimal impBnsDaScoTot) {
        int position = Pos.wpmoImpBnsDaScoTot(impBnsDaScoTotIdx - 1);
        writeDecimalAsPacked(position, impBnsDaScoTot.copy());
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO-TOT<br>*/
    public AfDecimal getImpBnsDaScoTot(int impBnsDaScoTotIdx) {
        int position = Pos.wpmoImpBnsDaScoTot(impBnsDaScoTotIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS_DA_SCO_TOT, Len.Fract.IMP_BNS_DA_SCO_TOT);
    }

    public void setWpmoImpBnsDaScoTotNull(int wpmoImpBnsDaScoTotNullIdx, String wpmoImpBnsDaScoTotNull) {
        int position = Pos.wpmoImpBnsDaScoTotNull(wpmoImpBnsDaScoTotNullIdx - 1);
        writeString(position, wpmoImpBnsDaScoTotNull, Len.WPMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO-TOT-NULL<br>*/
    public String getWpmoImpBnsDaScoTotNull(int wpmoImpBnsDaScoTotNullIdx) {
        int position = Pos.wpmoImpBnsDaScoTotNull(wpmoImpBnsDaScoTotNullIdx - 1);
        return readString(position, Len.WPMO_IMP_BNS_DA_SCO_TOT_NULL);
    }

    public void setImpBnsDaSco(int impBnsDaScoIdx, AfDecimal impBnsDaSco) {
        int position = Pos.wpmoImpBnsDaSco(impBnsDaScoIdx - 1);
        writeDecimalAsPacked(position, impBnsDaSco.copy());
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO<br>*/
    public AfDecimal getImpBnsDaSco(int impBnsDaScoIdx) {
        int position = Pos.wpmoImpBnsDaSco(impBnsDaScoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS_DA_SCO, Len.Fract.IMP_BNS_DA_SCO);
    }

    public void setWpmoImpBnsDaScoNull(int wpmoImpBnsDaScoNullIdx, String wpmoImpBnsDaScoNull) {
        int position = Pos.wpmoImpBnsDaScoNull(wpmoImpBnsDaScoNullIdx - 1);
        writeString(position, wpmoImpBnsDaScoNull, Len.WPMO_IMP_BNS_DA_SCO_NULL);
    }

    /**Original name: WPMO-IMP-BNS-DA-SCO-NULL<br>*/
    public String getWpmoImpBnsDaScoNull(int wpmoImpBnsDaScoNullIdx) {
        int position = Pos.wpmoImpBnsDaScoNull(wpmoImpBnsDaScoNullIdx - 1);
        return readString(position, Len.WPMO_IMP_BNS_DA_SCO_NULL);
    }

    public void setPcAnticBns(int pcAnticBnsIdx, AfDecimal pcAnticBns) {
        int position = Pos.wpmoPcAnticBns(pcAnticBnsIdx - 1);
        writeDecimalAsPacked(position, pcAnticBns.copy());
    }

    /**Original name: WPMO-PC-ANTIC-BNS<br>*/
    public AfDecimal getPcAnticBns(int pcAnticBnsIdx) {
        int position = Pos.wpmoPcAnticBns(pcAnticBnsIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_ANTIC_BNS, Len.Fract.PC_ANTIC_BNS);
    }

    public void setWpmoPcAnticBnsNull(int wpmoPcAnticBnsNullIdx, String wpmoPcAnticBnsNull) {
        int position = Pos.wpmoPcAnticBnsNull(wpmoPcAnticBnsNullIdx - 1);
        writeString(position, wpmoPcAnticBnsNull, Len.WPMO_PC_ANTIC_BNS_NULL);
    }

    /**Original name: WPMO-PC-ANTIC-BNS-NULL<br>*/
    public String getWpmoPcAnticBnsNull(int wpmoPcAnticBnsNullIdx) {
        int position = Pos.wpmoPcAnticBnsNull(wpmoPcAnticBnsNullIdx - 1);
        return readString(position, Len.WPMO_PC_ANTIC_BNS_NULL);
    }

    public void setTpRinnColl(int tpRinnCollIdx, String tpRinnColl) {
        int position = Pos.wpmoTpRinnColl(tpRinnCollIdx - 1);
        writeString(position, tpRinnColl, Len.TP_RINN_COLL);
    }

    /**Original name: WPMO-TP-RINN-COLL<br>*/
    public String getTpRinnColl(int tpRinnCollIdx) {
        int position = Pos.wpmoTpRinnColl(tpRinnCollIdx - 1);
        return readString(position, Len.TP_RINN_COLL);
    }

    public void setWpmoTpRinnCollNull(int wpmoTpRinnCollNullIdx, String wpmoTpRinnCollNull) {
        int position = Pos.wpmoTpRinnCollNull(wpmoTpRinnCollNullIdx - 1);
        writeString(position, wpmoTpRinnCollNull, Len.WPMO_TP_RINN_COLL_NULL);
    }

    /**Original name: WPMO-TP-RINN-COLL-NULL<br>*/
    public String getWpmoTpRinnCollNull(int wpmoTpRinnCollNullIdx) {
        int position = Pos.wpmoTpRinnCollNull(wpmoTpRinnCollNullIdx - 1);
        return readString(position, Len.WPMO_TP_RINN_COLL_NULL);
    }

    public void setTpRivalPre(int tpRivalPreIdx, String tpRivalPre) {
        int position = Pos.wpmoTpRivalPre(tpRivalPreIdx - 1);
        writeString(position, tpRivalPre, Len.TP_RIVAL_PRE);
    }

    /**Original name: WPMO-TP-RIVAL-PRE<br>*/
    public String getTpRivalPre(int tpRivalPreIdx) {
        int position = Pos.wpmoTpRivalPre(tpRivalPreIdx - 1);
        return readString(position, Len.TP_RIVAL_PRE);
    }

    public void setWpmoTpRivalPreNull(int wpmoTpRivalPreNullIdx, String wpmoTpRivalPreNull) {
        int position = Pos.wpmoTpRivalPreNull(wpmoTpRivalPreNullIdx - 1);
        writeString(position, wpmoTpRivalPreNull, Len.WPMO_TP_RIVAL_PRE_NULL);
    }

    /**Original name: WPMO-TP-RIVAL-PRE-NULL<br>*/
    public String getWpmoTpRivalPreNull(int wpmoTpRivalPreNullIdx) {
        int position = Pos.wpmoTpRivalPreNull(wpmoTpRivalPreNullIdx - 1);
        return readString(position, Len.WPMO_TP_RIVAL_PRE_NULL);
    }

    public void setTpRivalPrstz(int tpRivalPrstzIdx, String tpRivalPrstz) {
        int position = Pos.wpmoTpRivalPrstz(tpRivalPrstzIdx - 1);
        writeString(position, tpRivalPrstz, Len.TP_RIVAL_PRSTZ);
    }

    /**Original name: WPMO-TP-RIVAL-PRSTZ<br>*/
    public String getTpRivalPrstz(int tpRivalPrstzIdx) {
        int position = Pos.wpmoTpRivalPrstz(tpRivalPrstzIdx - 1);
        return readString(position, Len.TP_RIVAL_PRSTZ);
    }

    public void setWpmoTpRivalPrstzNull(int wpmoTpRivalPrstzNullIdx, String wpmoTpRivalPrstzNull) {
        int position = Pos.wpmoTpRivalPrstzNull(wpmoTpRivalPrstzNullIdx - 1);
        writeString(position, wpmoTpRivalPrstzNull, Len.WPMO_TP_RIVAL_PRSTZ_NULL);
    }

    /**Original name: WPMO-TP-RIVAL-PRSTZ-NULL<br>*/
    public String getWpmoTpRivalPrstzNull(int wpmoTpRivalPrstzNullIdx) {
        int position = Pos.wpmoTpRivalPrstzNull(wpmoTpRivalPrstzNullIdx - 1);
        return readString(position, Len.WPMO_TP_RIVAL_PRSTZ_NULL);
    }

    public void setFlEvidRival(int flEvidRivalIdx, char flEvidRival) {
        int position = Pos.wpmoFlEvidRival(flEvidRivalIdx - 1);
        writeChar(position, flEvidRival);
    }

    /**Original name: WPMO-FL-EVID-RIVAL<br>*/
    public char getFlEvidRival(int flEvidRivalIdx) {
        int position = Pos.wpmoFlEvidRival(flEvidRivalIdx - 1);
        return readChar(position);
    }

    public void setWpmoFlEvidRivalNull(int wpmoFlEvidRivalNullIdx, char wpmoFlEvidRivalNull) {
        int position = Pos.wpmoFlEvidRivalNull(wpmoFlEvidRivalNullIdx - 1);
        writeChar(position, wpmoFlEvidRivalNull);
    }

    /**Original name: WPMO-FL-EVID-RIVAL-NULL<br>*/
    public char getWpmoFlEvidRivalNull(int wpmoFlEvidRivalNullIdx) {
        int position = Pos.wpmoFlEvidRivalNull(wpmoFlEvidRivalNullIdx - 1);
        return readChar(position);
    }

    public void setUltPcPerd(int ultPcPerdIdx, AfDecimal ultPcPerd) {
        int position = Pos.wpmoUltPcPerd(ultPcPerdIdx - 1);
        writeDecimalAsPacked(position, ultPcPerd.copy());
    }

    /**Original name: WPMO-ULT-PC-PERD<br>*/
    public AfDecimal getUltPcPerd(int ultPcPerdIdx) {
        int position = Pos.wpmoUltPcPerd(ultPcPerdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ULT_PC_PERD, Len.Fract.ULT_PC_PERD);
    }

    public void setWpmoUltPcPerdNull(int wpmoUltPcPerdNullIdx, String wpmoUltPcPerdNull) {
        int position = Pos.wpmoUltPcPerdNull(wpmoUltPcPerdNullIdx - 1);
        writeString(position, wpmoUltPcPerdNull, Len.WPMO_ULT_PC_PERD_NULL);
    }

    /**Original name: WPMO-ULT-PC-PERD-NULL<br>*/
    public String getWpmoUltPcPerdNull(int wpmoUltPcPerdNullIdx) {
        int position = Pos.wpmoUltPcPerdNull(wpmoUltPcPerdNullIdx - 1);
        return readString(position, Len.WPMO_ULT_PC_PERD_NULL);
    }

    public void setTotAaGiaPror(int totAaGiaProrIdx, int totAaGiaPror) {
        int position = Pos.wpmoTotAaGiaPror(totAaGiaProrIdx - 1);
        writeIntAsPacked(position, totAaGiaPror, Len.Int.TOT_AA_GIA_PROR);
    }

    /**Original name: WPMO-TOT-AA-GIA-PROR<br>*/
    public int getTotAaGiaPror(int totAaGiaProrIdx) {
        int position = Pos.wpmoTotAaGiaPror(totAaGiaProrIdx - 1);
        return readPackedAsInt(position, Len.Int.TOT_AA_GIA_PROR);
    }

    public void setWpmoTotAaGiaProrNull(int wpmoTotAaGiaProrNullIdx, String wpmoTotAaGiaProrNull) {
        int position = Pos.wpmoTotAaGiaProrNull(wpmoTotAaGiaProrNullIdx - 1);
        writeString(position, wpmoTotAaGiaProrNull, Len.WPMO_TOT_AA_GIA_PROR_NULL);
    }

    /**Original name: WPMO-TOT-AA-GIA-PROR-NULL<br>*/
    public String getWpmoTotAaGiaProrNull(int wpmoTotAaGiaProrNullIdx) {
        int position = Pos.wpmoTotAaGiaProrNull(wpmoTotAaGiaProrNullIdx - 1);
        return readString(position, Len.WPMO_TOT_AA_GIA_PROR_NULL);
    }

    public void setTpOpz(int tpOpzIdx, String tpOpz) {
        int position = Pos.wpmoTpOpz(tpOpzIdx - 1);
        writeString(position, tpOpz, Len.TP_OPZ);
    }

    /**Original name: WPMO-TP-OPZ<br>*/
    public String getTpOpz(int tpOpzIdx) {
        int position = Pos.wpmoTpOpz(tpOpzIdx - 1);
        return readString(position, Len.TP_OPZ);
    }

    public void setWpmoTpOpzNull(int wpmoTpOpzNullIdx, String wpmoTpOpzNull) {
        int position = Pos.wpmoTpOpzNull(wpmoTpOpzNullIdx - 1);
        writeString(position, wpmoTpOpzNull, Len.WPMO_TP_OPZ_NULL);
    }

    /**Original name: WPMO-TP-OPZ-NULL<br>*/
    public String getWpmoTpOpzNull(int wpmoTpOpzNullIdx) {
        int position = Pos.wpmoTpOpzNull(wpmoTpOpzNullIdx - 1);
        return readString(position, Len.WPMO_TP_OPZ_NULL);
    }

    public void setAaRenCer(int aaRenCerIdx, int aaRenCer) {
        int position = Pos.wpmoAaRenCer(aaRenCerIdx - 1);
        writeIntAsPacked(position, aaRenCer, Len.Int.AA_REN_CER);
    }

    /**Original name: WPMO-AA-REN-CER<br>*/
    public int getAaRenCer(int aaRenCerIdx) {
        int position = Pos.wpmoAaRenCer(aaRenCerIdx - 1);
        return readPackedAsInt(position, Len.Int.AA_REN_CER);
    }

    public void setWpmoAaRenCerNull(int wpmoAaRenCerNullIdx, String wpmoAaRenCerNull) {
        int position = Pos.wpmoAaRenCerNull(wpmoAaRenCerNullIdx - 1);
        writeString(position, wpmoAaRenCerNull, Len.WPMO_AA_REN_CER_NULL);
    }

    /**Original name: WPMO-AA-REN-CER-NULL<br>*/
    public String getWpmoAaRenCerNull(int wpmoAaRenCerNullIdx) {
        int position = Pos.wpmoAaRenCerNull(wpmoAaRenCerNullIdx - 1);
        return readString(position, Len.WPMO_AA_REN_CER_NULL);
    }

    public String getWpmoAaRenCerNullFormatted(int wpmoAaRenCerNullIdx) {
        return Functions.padBlanks(getWpmoAaRenCerNull(wpmoAaRenCerNullIdx), Len.WPMO_AA_REN_CER_NULL);
    }

    public void setPcRevrsb(int pcRevrsbIdx, AfDecimal pcRevrsb) {
        int position = Pos.wpmoPcRevrsb(pcRevrsbIdx - 1);
        writeDecimalAsPacked(position, pcRevrsb.copy());
    }

    /**Original name: WPMO-PC-REVRSB<br>*/
    public AfDecimal getPcRevrsb(int pcRevrsbIdx) {
        int position = Pos.wpmoPcRevrsb(pcRevrsbIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_REVRSB, Len.Fract.PC_REVRSB);
    }

    public void setWpmoPcRevrsbNull(int wpmoPcRevrsbNullIdx, String wpmoPcRevrsbNull) {
        int position = Pos.wpmoPcRevrsbNull(wpmoPcRevrsbNullIdx - 1);
        writeString(position, wpmoPcRevrsbNull, Len.WPMO_PC_REVRSB_NULL);
    }

    /**Original name: WPMO-PC-REVRSB-NULL<br>*/
    public String getWpmoPcRevrsbNull(int wpmoPcRevrsbNullIdx) {
        int position = Pos.wpmoPcRevrsbNull(wpmoPcRevrsbNullIdx - 1);
        return readString(position, Len.WPMO_PC_REVRSB_NULL);
    }

    public void setImpRiscParzPrgt(int impRiscParzPrgtIdx, AfDecimal impRiscParzPrgt) {
        int position = Pos.wpmoImpRiscParzPrgt(impRiscParzPrgtIdx - 1);
        writeDecimalAsPacked(position, impRiscParzPrgt.copy());
    }

    /**Original name: WPMO-IMP-RISC-PARZ-PRGT<br>*/
    public AfDecimal getImpRiscParzPrgt(int impRiscParzPrgtIdx) {
        int position = Pos.wpmoImpRiscParzPrgt(impRiscParzPrgtIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_RISC_PARZ_PRGT, Len.Fract.IMP_RISC_PARZ_PRGT);
    }

    public void setWpmoImpRiscParzPrgtNull(int wpmoImpRiscParzPrgtNullIdx, String wpmoImpRiscParzPrgtNull) {
        int position = Pos.wpmoImpRiscParzPrgtNull(wpmoImpRiscParzPrgtNullIdx - 1);
        writeString(position, wpmoImpRiscParzPrgtNull, Len.WPMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    /**Original name: WPMO-IMP-RISC-PARZ-PRGT-NULL<br>*/
    public String getWpmoImpRiscParzPrgtNull(int wpmoImpRiscParzPrgtNullIdx) {
        int position = Pos.wpmoImpRiscParzPrgtNull(wpmoImpRiscParzPrgtNullIdx - 1);
        return readString(position, Len.WPMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    public void setImpLrdDiRat(int impLrdDiRatIdx, AfDecimal impLrdDiRat) {
        int position = Pos.wpmoImpLrdDiRat(impLrdDiRatIdx - 1);
        writeDecimalAsPacked(position, impLrdDiRat.copy());
    }

    /**Original name: WPMO-IMP-LRD-DI-RAT<br>*/
    public AfDecimal getImpLrdDiRat(int impLrdDiRatIdx) {
        int position = Pos.wpmoImpLrdDiRat(impLrdDiRatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_LRD_DI_RAT, Len.Fract.IMP_LRD_DI_RAT);
    }

    public void setWpmoImpLrdDiRatNull(int wpmoImpLrdDiRatNullIdx, String wpmoImpLrdDiRatNull) {
        int position = Pos.wpmoImpLrdDiRatNull(wpmoImpLrdDiRatNullIdx - 1);
        writeString(position, wpmoImpLrdDiRatNull, Len.WPMO_IMP_LRD_DI_RAT_NULL);
    }

    /**Original name: WPMO-IMP-LRD-DI-RAT-NULL<br>*/
    public String getWpmoImpLrdDiRatNull(int wpmoImpLrdDiRatNullIdx) {
        int position = Pos.wpmoImpLrdDiRatNull(wpmoImpLrdDiRatNullIdx - 1);
        return readString(position, Len.WPMO_IMP_LRD_DI_RAT_NULL);
    }

    public void setIbOgg(int ibOggIdx, String ibOgg) {
        int position = Pos.wpmoIbOgg(ibOggIdx - 1);
        writeString(position, ibOgg, Len.IB_OGG);
    }

    /**Original name: WPMO-IB-OGG<br>*/
    public String getIbOgg(int ibOggIdx) {
        int position = Pos.wpmoIbOgg(ibOggIdx - 1);
        return readString(position, Len.IB_OGG);
    }

    public void setWpmoIbOggNull(int wpmoIbOggNullIdx, String wpmoIbOggNull) {
        int position = Pos.wpmoIbOggNull(wpmoIbOggNullIdx - 1);
        writeString(position, wpmoIbOggNull, Len.WPMO_IB_OGG_NULL);
    }

    /**Original name: WPMO-IB-OGG-NULL<br>*/
    public String getWpmoIbOggNull(int wpmoIbOggNullIdx) {
        int position = Pos.wpmoIbOggNull(wpmoIbOggNullIdx - 1);
        return readString(position, Len.WPMO_IB_OGG_NULL);
    }

    public void setCosOner(int cosOnerIdx, AfDecimal cosOner) {
        int position = Pos.wpmoCosOner(cosOnerIdx - 1);
        writeDecimalAsPacked(position, cosOner.copy());
    }

    /**Original name: WPMO-COS-ONER<br>*/
    public AfDecimal getCosOner(int cosOnerIdx) {
        int position = Pos.wpmoCosOner(cosOnerIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_ONER, Len.Fract.COS_ONER);
    }

    public void setWpmoCosOnerNull(int wpmoCosOnerNullIdx, String wpmoCosOnerNull) {
        int position = Pos.wpmoCosOnerNull(wpmoCosOnerNullIdx - 1);
        writeString(position, wpmoCosOnerNull, Len.WPMO_COS_ONER_NULL);
    }

    /**Original name: WPMO-COS-ONER-NULL<br>*/
    public String getWpmoCosOnerNull(int wpmoCosOnerNullIdx) {
        int position = Pos.wpmoCosOnerNull(wpmoCosOnerNullIdx - 1);
        return readString(position, Len.WPMO_COS_ONER_NULL);
    }

    public void setSpePc(int spePcIdx, AfDecimal spePc) {
        int position = Pos.wpmoSpePc(spePcIdx - 1);
        writeDecimalAsPacked(position, spePc.copy());
    }

    /**Original name: WPMO-SPE-PC<br>*/
    public AfDecimal getSpePc(int spePcIdx) {
        int position = Pos.wpmoSpePc(spePcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.SPE_PC, Len.Fract.SPE_PC);
    }

    public void setWpmoSpePcNull(int wpmoSpePcNullIdx, String wpmoSpePcNull) {
        int position = Pos.wpmoSpePcNull(wpmoSpePcNullIdx - 1);
        writeString(position, wpmoSpePcNull, Len.WPMO_SPE_PC_NULL);
    }

    /**Original name: WPMO-SPE-PC-NULL<br>*/
    public String getWpmoSpePcNull(int wpmoSpePcNullIdx) {
        int position = Pos.wpmoSpePcNull(wpmoSpePcNullIdx - 1);
        return readString(position, Len.WPMO_SPE_PC_NULL);
    }

    public void setFlAttivGar(int flAttivGarIdx, char flAttivGar) {
        int position = Pos.wpmoFlAttivGar(flAttivGarIdx - 1);
        writeChar(position, flAttivGar);
    }

    /**Original name: WPMO-FL-ATTIV-GAR<br>*/
    public char getFlAttivGar(int flAttivGarIdx) {
        int position = Pos.wpmoFlAttivGar(flAttivGarIdx - 1);
        return readChar(position);
    }

    public void setWpmoFlAttivGarNull(int wpmoFlAttivGarNullIdx, char wpmoFlAttivGarNull) {
        int position = Pos.wpmoFlAttivGarNull(wpmoFlAttivGarNullIdx - 1);
        writeChar(position, wpmoFlAttivGarNull);
    }

    /**Original name: WPMO-FL-ATTIV-GAR-NULL<br>*/
    public char getWpmoFlAttivGarNull(int wpmoFlAttivGarNullIdx) {
        int position = Pos.wpmoFlAttivGarNull(wpmoFlAttivGarNullIdx - 1);
        return readChar(position);
    }

    public void setCambioVerProd(int cambioVerProdIdx, char cambioVerProd) {
        int position = Pos.wpmoCambioVerProd(cambioVerProdIdx - 1);
        writeChar(position, cambioVerProd);
    }

    /**Original name: WPMO-CAMBIO-VER-PROD<br>*/
    public char getCambioVerProd(int cambioVerProdIdx) {
        int position = Pos.wpmoCambioVerProd(cambioVerProdIdx - 1);
        return readChar(position);
    }

    public void setWpmoCambioVerProdNull(int wpmoCambioVerProdNullIdx, char wpmoCambioVerProdNull) {
        int position = Pos.wpmoCambioVerProdNull(wpmoCambioVerProdNullIdx - 1);
        writeChar(position, wpmoCambioVerProdNull);
    }

    /**Original name: WPMO-CAMBIO-VER-PROD-NULL<br>*/
    public char getWpmoCambioVerProdNull(int wpmoCambioVerProdNullIdx) {
        int position = Pos.wpmoCambioVerProdNull(wpmoCambioVerProdNullIdx - 1);
        return readChar(position);
    }

    public void setMmDiff(int mmDiffIdx, short mmDiff) {
        int position = Pos.wpmoMmDiff(mmDiffIdx - 1);
        writeShortAsPacked(position, mmDiff, Len.Int.MM_DIFF);
    }

    /**Original name: WPMO-MM-DIFF<br>*/
    public short getMmDiff(int mmDiffIdx) {
        int position = Pos.wpmoMmDiff(mmDiffIdx - 1);
        return readPackedAsShort(position, Len.Int.MM_DIFF);
    }

    public void setWpmoMmDiffNull(int wpmoMmDiffNullIdx, String wpmoMmDiffNull) {
        int position = Pos.wpmoMmDiffNull(wpmoMmDiffNullIdx - 1);
        writeString(position, wpmoMmDiffNull, Len.WPMO_MM_DIFF_NULL);
    }

    /**Original name: WPMO-MM-DIFF-NULL<br>*/
    public String getWpmoMmDiffNull(int wpmoMmDiffNullIdx) {
        int position = Pos.wpmoMmDiffNull(wpmoMmDiffNullIdx - 1);
        return readString(position, Len.WPMO_MM_DIFF_NULL);
    }

    public void setImpRatManfee(int impRatManfeeIdx, AfDecimal impRatManfee) {
        int position = Pos.wpmoImpRatManfee(impRatManfeeIdx - 1);
        writeDecimalAsPacked(position, impRatManfee.copy());
    }

    /**Original name: WPMO-IMP-RAT-MANFEE<br>*/
    public AfDecimal getImpRatManfee(int impRatManfeeIdx) {
        int position = Pos.wpmoImpRatManfee(impRatManfeeIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_RAT_MANFEE, Len.Fract.IMP_RAT_MANFEE);
    }

    public void setWpmoImpRatManfeeNull(int wpmoImpRatManfeeNullIdx, String wpmoImpRatManfeeNull) {
        int position = Pos.wpmoImpRatManfeeNull(wpmoImpRatManfeeNullIdx - 1);
        writeString(position, wpmoImpRatManfeeNull, Len.WPMO_IMP_RAT_MANFEE_NULL);
    }

    /**Original name: WPMO-IMP-RAT-MANFEE-NULL<br>*/
    public String getWpmoImpRatManfeeNull(int wpmoImpRatManfeeNullIdx) {
        int position = Pos.wpmoImpRatManfeeNull(wpmoImpRatManfeeNullIdx - 1);
        return readString(position, Len.WPMO_IMP_RAT_MANFEE_NULL);
    }

    public void setDtUltErogManfee(int dtUltErogManfeeIdx, int dtUltErogManfee) {
        int position = Pos.wpmoDtUltErogManfee(dtUltErogManfeeIdx - 1);
        writeIntAsPacked(position, dtUltErogManfee, Len.Int.DT_ULT_EROG_MANFEE);
    }

    /**Original name: WPMO-DT-ULT-EROG-MANFEE<br>*/
    public int getDtUltErogManfee(int dtUltErogManfeeIdx) {
        int position = Pos.wpmoDtUltErogManfee(dtUltErogManfeeIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_ULT_EROG_MANFEE);
    }

    public void setWpmoDtUltErogManfeeNull(int wpmoDtUltErogManfeeNullIdx, String wpmoDtUltErogManfeeNull) {
        int position = Pos.wpmoDtUltErogManfeeNull(wpmoDtUltErogManfeeNullIdx - 1);
        writeString(position, wpmoDtUltErogManfeeNull, Len.WPMO_DT_ULT_EROG_MANFEE_NULL);
    }

    /**Original name: WPMO-DT-ULT-EROG-MANFEE-NULL<br>*/
    public String getWpmoDtUltErogManfeeNull(int wpmoDtUltErogManfeeNullIdx) {
        int position = Pos.wpmoDtUltErogManfeeNull(wpmoDtUltErogManfeeNullIdx - 1);
        return readString(position, Len.WPMO_DT_ULT_EROG_MANFEE_NULL);
    }

    public void setTpOggRival(int tpOggRivalIdx, String tpOggRival) {
        int position = Pos.wpmoTpOggRival(tpOggRivalIdx - 1);
        writeString(position, tpOggRival, Len.TP_OGG_RIVAL);
    }

    /**Original name: WPMO-TP-OGG-RIVAL<br>*/
    public String getTpOggRival(int tpOggRivalIdx) {
        int position = Pos.wpmoTpOggRival(tpOggRivalIdx - 1);
        return readString(position, Len.TP_OGG_RIVAL);
    }

    public void setWpmoTpOggRivalNull(int wpmoTpOggRivalNullIdx, String wpmoTpOggRivalNull) {
        int position = Pos.wpmoTpOggRivalNull(wpmoTpOggRivalNullIdx - 1);
        writeString(position, wpmoTpOggRivalNull, Len.WPMO_TP_OGG_RIVAL_NULL);
    }

    /**Original name: WPMO-TP-OGG-RIVAL-NULL<br>*/
    public String getWpmoTpOggRivalNull(int wpmoTpOggRivalNullIdx) {
        int position = Pos.wpmoTpOggRivalNull(wpmoTpOggRivalNullIdx - 1);
        return readString(position, Len.WPMO_TP_OGG_RIVAL_NULL);
    }

    public void setSomAsstaGarac(int somAsstaGaracIdx, AfDecimal somAsstaGarac) {
        int position = Pos.wpmoSomAsstaGarac(somAsstaGaracIdx - 1);
        writeDecimalAsPacked(position, somAsstaGarac.copy());
    }

    /**Original name: WPMO-SOM-ASSTA-GARAC<br>*/
    public AfDecimal getSomAsstaGarac(int somAsstaGaracIdx) {
        int position = Pos.wpmoSomAsstaGarac(somAsstaGaracIdx - 1);
        return readPackedAsDecimal(position, Len.Int.SOM_ASSTA_GARAC, Len.Fract.SOM_ASSTA_GARAC);
    }

    public void setWpmoSomAsstaGaracNull(int wpmoSomAsstaGaracNullIdx, String wpmoSomAsstaGaracNull) {
        int position = Pos.wpmoSomAsstaGaracNull(wpmoSomAsstaGaracNullIdx - 1);
        writeString(position, wpmoSomAsstaGaracNull, Len.WPMO_SOM_ASSTA_GARAC_NULL);
    }

    /**Original name: WPMO-SOM-ASSTA-GARAC-NULL<br>*/
    public String getWpmoSomAsstaGaracNull(int wpmoSomAsstaGaracNullIdx) {
        int position = Pos.wpmoSomAsstaGaracNull(wpmoSomAsstaGaracNullIdx - 1);
        return readString(position, Len.WPMO_SOM_ASSTA_GARAC_NULL);
    }

    public void setPcApplzOpz(int pcApplzOpzIdx, AfDecimal pcApplzOpz) {
        int position = Pos.wpmoPcApplzOpz(pcApplzOpzIdx - 1);
        writeDecimalAsPacked(position, pcApplzOpz.copy());
    }

    /**Original name: WPMO-PC-APPLZ-OPZ<br>*/
    public AfDecimal getPcApplzOpz(int pcApplzOpzIdx) {
        int position = Pos.wpmoPcApplzOpz(pcApplzOpzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_APPLZ_OPZ, Len.Fract.PC_APPLZ_OPZ);
    }

    public void setWpmoPcApplzOpzNull(int wpmoPcApplzOpzNullIdx, String wpmoPcApplzOpzNull) {
        int position = Pos.wpmoPcApplzOpzNull(wpmoPcApplzOpzNullIdx - 1);
        writeString(position, wpmoPcApplzOpzNull, Len.WPMO_PC_APPLZ_OPZ_NULL);
    }

    /**Original name: WPMO-PC-APPLZ-OPZ-NULL<br>*/
    public String getWpmoPcApplzOpzNull(int wpmoPcApplzOpzNullIdx) {
        int position = Pos.wpmoPcApplzOpzNull(wpmoPcApplzOpzNullIdx - 1);
        return readString(position, Len.WPMO_PC_APPLZ_OPZ_NULL);
    }

    public void setIdAdes(int idAdesIdx, int idAdes) {
        int position = Pos.wpmoIdAdes(idAdesIdx - 1);
        writeIntAsPacked(position, idAdes, Len.Int.ID_ADES);
    }

    /**Original name: WPMO-ID-ADES<br>*/
    public int getIdAdes(int idAdesIdx) {
        int position = Pos.wpmoIdAdes(idAdesIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_ADES);
    }

    public void setWpmoIdAdesNull(int wpmoIdAdesNullIdx, String wpmoIdAdesNull) {
        int position = Pos.wpmoIdAdesNull(wpmoIdAdesNullIdx - 1);
        writeString(position, wpmoIdAdesNull, Len.WPMO_ID_ADES_NULL);
    }

    /**Original name: WPMO-ID-ADES-NULL<br>*/
    public String getWpmoIdAdesNull(int wpmoIdAdesNullIdx) {
        int position = Pos.wpmoIdAdesNull(wpmoIdAdesNullIdx - 1);
        return readString(position, Len.WPMO_ID_ADES_NULL);
    }

    public void setIdPoli(int idPoliIdx, int idPoli) {
        int position = Pos.wpmoIdPoli(idPoliIdx - 1);
        writeIntAsPacked(position, idPoli, Len.Int.ID_POLI);
    }

    /**Original name: WPMO-ID-POLI<br>*/
    public int getIdPoli(int idPoliIdx) {
        int position = Pos.wpmoIdPoli(idPoliIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_POLI);
    }

    public void setTpFrmAssva(int tpFrmAssvaIdx, String tpFrmAssva) {
        int position = Pos.wpmoTpFrmAssva(tpFrmAssvaIdx - 1);
        writeString(position, tpFrmAssva, Len.TP_FRM_ASSVA);
    }

    /**Original name: WPMO-TP-FRM-ASSVA<br>*/
    public String getTpFrmAssva(int tpFrmAssvaIdx) {
        int position = Pos.wpmoTpFrmAssva(tpFrmAssvaIdx - 1);
        return readString(position, Len.TP_FRM_ASSVA);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wpmoDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WPMO-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wpmoDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wpmoDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WPMO-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wpmoDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wpmoDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WPMO-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wpmoDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wpmoDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WPMO-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wpmoDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wpmoDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WPMO-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wpmoDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wpmoDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WPMO-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wpmoDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wpmoDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WPMO-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wpmoDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setTpEstrCnt(int tpEstrCntIdx, String tpEstrCnt) {
        int position = Pos.wpmoTpEstrCnt(tpEstrCntIdx - 1);
        writeString(position, tpEstrCnt, Len.TP_ESTR_CNT);
    }

    /**Original name: WPMO-TP-ESTR-CNT<br>*/
    public String getTpEstrCnt(int tpEstrCntIdx) {
        int position = Pos.wpmoTpEstrCnt(tpEstrCntIdx - 1);
        return readString(position, Len.TP_ESTR_CNT);
    }

    public void setWpmoTpEstrCntNull(int wpmoTpEstrCntNullIdx, String wpmoTpEstrCntNull) {
        int position = Pos.wpmoTpEstrCntNull(wpmoTpEstrCntNullIdx - 1);
        writeString(position, wpmoTpEstrCntNull, Len.WPMO_TP_ESTR_CNT_NULL);
    }

    /**Original name: WPMO-TP-ESTR-CNT-NULL<br>*/
    public String getWpmoTpEstrCntNull(int wpmoTpEstrCntNullIdx) {
        int position = Pos.wpmoTpEstrCntNull(wpmoTpEstrCntNullIdx - 1);
        return readString(position, Len.WPMO_TP_ESTR_CNT_NULL);
    }

    public void setCodRamo(int codRamoIdx, String codRamo) {
        int position = Pos.wpmoCodRamo(codRamoIdx - 1);
        writeString(position, codRamo, Len.COD_RAMO);
    }

    /**Original name: WPMO-COD-RAMO<br>*/
    public String getCodRamo(int codRamoIdx) {
        int position = Pos.wpmoCodRamo(codRamoIdx - 1);
        return readString(position, Len.COD_RAMO);
    }

    public void setWpmoCodRamoNull(int wpmoCodRamoNullIdx, String wpmoCodRamoNull) {
        int position = Pos.wpmoCodRamoNull(wpmoCodRamoNullIdx - 1);
        writeString(position, wpmoCodRamoNull, Len.WPMO_COD_RAMO_NULL);
    }

    /**Original name: WPMO-COD-RAMO-NULL<br>*/
    public String getWpmoCodRamoNull(int wpmoCodRamoNullIdx) {
        int position = Pos.wpmoCodRamoNull(wpmoCodRamoNullIdx - 1);
        return readString(position, Len.WPMO_COD_RAMO_NULL);
    }

    public void setGenDaSin(int genDaSinIdx, char genDaSin) {
        int position = Pos.wpmoGenDaSin(genDaSinIdx - 1);
        writeChar(position, genDaSin);
    }

    /**Original name: WPMO-GEN-DA-SIN<br>*/
    public char getGenDaSin(int genDaSinIdx) {
        int position = Pos.wpmoGenDaSin(genDaSinIdx - 1);
        return readChar(position);
    }

    public void setWpmoGenDaSinNull(int wpmoGenDaSinNullIdx, char wpmoGenDaSinNull) {
        int position = Pos.wpmoGenDaSinNull(wpmoGenDaSinNullIdx - 1);
        writeChar(position, wpmoGenDaSinNull);
    }

    /**Original name: WPMO-GEN-DA-SIN-NULL<br>*/
    public char getWpmoGenDaSinNull(int wpmoGenDaSinNullIdx) {
        int position = Pos.wpmoGenDaSinNull(wpmoGenDaSinNullIdx - 1);
        return readChar(position);
    }

    public void setCodTari(int codTariIdx, String codTari) {
        int position = Pos.wpmoCodTari(codTariIdx - 1);
        writeString(position, codTari, Len.COD_TARI);
    }

    /**Original name: WPMO-COD-TARI<br>*/
    public String getCodTari(int codTariIdx) {
        int position = Pos.wpmoCodTari(codTariIdx - 1);
        return readString(position, Len.COD_TARI);
    }

    public void setWpmoCodTariNull(int wpmoCodTariNullIdx, String wpmoCodTariNull) {
        int position = Pos.wpmoCodTariNull(wpmoCodTariNullIdx - 1);
        writeString(position, wpmoCodTariNull, Len.WPMO_COD_TARI_NULL);
    }

    /**Original name: WPMO-COD-TARI-NULL<br>*/
    public String getWpmoCodTariNull(int wpmoCodTariNullIdx) {
        int position = Pos.wpmoCodTariNull(wpmoCodTariNullIdx - 1);
        return readString(position, Len.WPMO_COD_TARI_NULL);
    }

    public void setNumRatPagPre(int numRatPagPreIdx, int numRatPagPre) {
        int position = Pos.wpmoNumRatPagPre(numRatPagPreIdx - 1);
        writeIntAsPacked(position, numRatPagPre, Len.Int.NUM_RAT_PAG_PRE);
    }

    /**Original name: WPMO-NUM-RAT-PAG-PRE<br>*/
    public int getNumRatPagPre(int numRatPagPreIdx) {
        int position = Pos.wpmoNumRatPagPre(numRatPagPreIdx - 1);
        return readPackedAsInt(position, Len.Int.NUM_RAT_PAG_PRE);
    }

    public void setWpmoNumRatPagPreNull(int wpmoNumRatPagPreNullIdx, String wpmoNumRatPagPreNull) {
        int position = Pos.wpmoNumRatPagPreNull(wpmoNumRatPagPreNullIdx - 1);
        writeString(position, wpmoNumRatPagPreNull, Len.WPMO_NUM_RAT_PAG_PRE_NULL);
    }

    /**Original name: WPMO-NUM-RAT-PAG-PRE-NULL<br>*/
    public String getWpmoNumRatPagPreNull(int wpmoNumRatPagPreNullIdx) {
        int position = Pos.wpmoNumRatPagPreNull(wpmoNumRatPagPreNullIdx - 1);
        return readString(position, Len.WPMO_NUM_RAT_PAG_PRE_NULL);
    }

    public void setPcServVal(int pcServValIdx, AfDecimal pcServVal) {
        int position = Pos.wpmoPcServVal(pcServValIdx - 1);
        writeDecimalAsPacked(position, pcServVal.copy());
    }

    /**Original name: WPMO-PC-SERV-VAL<br>*/
    public AfDecimal getPcServVal(int pcServValIdx) {
        int position = Pos.wpmoPcServVal(pcServValIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_SERV_VAL, Len.Fract.PC_SERV_VAL);
    }

    public void setWpmoPcServValNull(int wpmoPcServValNullIdx, String wpmoPcServValNull) {
        int position = Pos.wpmoPcServValNull(wpmoPcServValNullIdx - 1);
        writeString(position, wpmoPcServValNull, Len.WPMO_PC_SERV_VAL_NULL);
    }

    /**Original name: WPMO-PC-SERV-VAL-NULL<br>*/
    public String getWpmoPcServValNull(int wpmoPcServValNullIdx) {
        int position = Pos.wpmoPcServValNull(wpmoPcServValNullIdx - 1);
        return readString(position, Len.WPMO_PC_SERV_VAL_NULL);
    }

    public void setEtaAaSoglBnficr(int etaAaSoglBnficrIdx, short etaAaSoglBnficr) {
        int position = Pos.wpmoEtaAaSoglBnficr(etaAaSoglBnficrIdx - 1);
        writeShortAsPacked(position, etaAaSoglBnficr, Len.Int.ETA_AA_SOGL_BNFICR);
    }

    /**Original name: WPMO-ETA-AA-SOGL-BNFICR<br>*/
    public short getEtaAaSoglBnficr(int etaAaSoglBnficrIdx) {
        int position = Pos.wpmoEtaAaSoglBnficr(etaAaSoglBnficrIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA_SOGL_BNFICR);
    }

    public void setWpmoEtaAaSoglBnficrNull(int wpmoEtaAaSoglBnficrNullIdx, String wpmoEtaAaSoglBnficrNull) {
        int position = Pos.wpmoEtaAaSoglBnficrNull(wpmoEtaAaSoglBnficrNullIdx - 1);
        writeString(position, wpmoEtaAaSoglBnficrNull, Len.WPMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    /**Original name: WPMO-ETA-AA-SOGL-BNFICR-NULL<br>*/
    public String getWpmoEtaAaSoglBnficrNull(int wpmoEtaAaSoglBnficrNullIdx) {
        int position = Pos.wpmoEtaAaSoglBnficrNull(wpmoEtaAaSoglBnficrNullIdx - 1);
        return readString(position, Len.WPMO_ETA_AA_SOGL_BNFICR_NULL);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WPMO-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_TAB = 1;
        public static final int WPMO_TAB_R = 1;
        public static final int FLR1 = WPMO_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wpmoTabParamMov(int idx) {
            return WPMO_TAB + idx * Len.TAB_PARAM_MOV;
        }

        public static int wpmoStatus(int idx) {
            return wpmoTabParamMov(idx);
        }

        public static int wpmoIdPtf(int idx) {
            return wpmoStatus(idx) + Len.STATUS;
        }

        public static int wpmoDati(int idx) {
            return wpmoIdPtf(idx) + Len.ID_PTF;
        }

        public static int wpmoIdParamMovi(int idx) {
            return wpmoDati(idx);
        }

        public static int wpmoIdOgg(int idx) {
            return wpmoIdParamMovi(idx) + Len.ID_PARAM_MOVI;
        }

        public static int wpmoTpOgg(int idx) {
            return wpmoIdOgg(idx) + Len.ID_OGG;
        }

        public static int wpmoIdMoviCrz(int idx) {
            return wpmoTpOgg(idx) + Len.TP_OGG;
        }

        public static int wpmoIdMoviChiu(int idx) {
            return wpmoIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wpmoIdMoviChiuNull(int idx) {
            return wpmoIdMoviChiu(idx);
        }

        public static int wpmoDtIniEff(int idx) {
            return wpmoIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wpmoDtEndEff(int idx) {
            return wpmoDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wpmoCodCompAnia(int idx) {
            return wpmoDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wpmoTpMovi(int idx) {
            return wpmoCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wpmoTpMoviNull(int idx) {
            return wpmoTpMovi(idx);
        }

        public static int wpmoFrqMovi(int idx) {
            return wpmoTpMovi(idx) + Len.TP_MOVI;
        }

        public static int wpmoFrqMoviNull(int idx) {
            return wpmoFrqMovi(idx);
        }

        public static int wpmoDurAa(int idx) {
            return wpmoFrqMovi(idx) + Len.FRQ_MOVI;
        }

        public static int wpmoDurAaNull(int idx) {
            return wpmoDurAa(idx);
        }

        public static int wpmoDurMm(int idx) {
            return wpmoDurAa(idx) + Len.DUR_AA;
        }

        public static int wpmoDurMmNull(int idx) {
            return wpmoDurMm(idx);
        }

        public static int wpmoDurGg(int idx) {
            return wpmoDurMm(idx) + Len.DUR_MM;
        }

        public static int wpmoDurGgNull(int idx) {
            return wpmoDurGg(idx);
        }

        public static int wpmoDtRicorPrec(int idx) {
            return wpmoDurGg(idx) + Len.DUR_GG;
        }

        public static int wpmoDtRicorPrecNull(int idx) {
            return wpmoDtRicorPrec(idx);
        }

        public static int wpmoDtRicorSucc(int idx) {
            return wpmoDtRicorPrec(idx) + Len.DT_RICOR_PREC;
        }

        public static int wpmoDtRicorSuccNull(int idx) {
            return wpmoDtRicorSucc(idx);
        }

        public static int wpmoPcIntrFraz(int idx) {
            return wpmoDtRicorSucc(idx) + Len.DT_RICOR_SUCC;
        }

        public static int wpmoPcIntrFrazNull(int idx) {
            return wpmoPcIntrFraz(idx);
        }

        public static int wpmoImpBnsDaScoTot(int idx) {
            return wpmoPcIntrFraz(idx) + Len.PC_INTR_FRAZ;
        }

        public static int wpmoImpBnsDaScoTotNull(int idx) {
            return wpmoImpBnsDaScoTot(idx);
        }

        public static int wpmoImpBnsDaSco(int idx) {
            return wpmoImpBnsDaScoTot(idx) + Len.IMP_BNS_DA_SCO_TOT;
        }

        public static int wpmoImpBnsDaScoNull(int idx) {
            return wpmoImpBnsDaSco(idx);
        }

        public static int wpmoPcAnticBns(int idx) {
            return wpmoImpBnsDaSco(idx) + Len.IMP_BNS_DA_SCO;
        }

        public static int wpmoPcAnticBnsNull(int idx) {
            return wpmoPcAnticBns(idx);
        }

        public static int wpmoTpRinnColl(int idx) {
            return wpmoPcAnticBns(idx) + Len.PC_ANTIC_BNS;
        }

        public static int wpmoTpRinnCollNull(int idx) {
            return wpmoTpRinnColl(idx);
        }

        public static int wpmoTpRivalPre(int idx) {
            return wpmoTpRinnColl(idx) + Len.TP_RINN_COLL;
        }

        public static int wpmoTpRivalPreNull(int idx) {
            return wpmoTpRivalPre(idx);
        }

        public static int wpmoTpRivalPrstz(int idx) {
            return wpmoTpRivalPre(idx) + Len.TP_RIVAL_PRE;
        }

        public static int wpmoTpRivalPrstzNull(int idx) {
            return wpmoTpRivalPrstz(idx);
        }

        public static int wpmoFlEvidRival(int idx) {
            return wpmoTpRivalPrstz(idx) + Len.TP_RIVAL_PRSTZ;
        }

        public static int wpmoFlEvidRivalNull(int idx) {
            return wpmoFlEvidRival(idx);
        }

        public static int wpmoUltPcPerd(int idx) {
            return wpmoFlEvidRival(idx) + Len.FL_EVID_RIVAL;
        }

        public static int wpmoUltPcPerdNull(int idx) {
            return wpmoUltPcPerd(idx);
        }

        public static int wpmoTotAaGiaPror(int idx) {
            return wpmoUltPcPerd(idx) + Len.ULT_PC_PERD;
        }

        public static int wpmoTotAaGiaProrNull(int idx) {
            return wpmoTotAaGiaPror(idx);
        }

        public static int wpmoTpOpz(int idx) {
            return wpmoTotAaGiaPror(idx) + Len.TOT_AA_GIA_PROR;
        }

        public static int wpmoTpOpzNull(int idx) {
            return wpmoTpOpz(idx);
        }

        public static int wpmoAaRenCer(int idx) {
            return wpmoTpOpz(idx) + Len.TP_OPZ;
        }

        public static int wpmoAaRenCerNull(int idx) {
            return wpmoAaRenCer(idx);
        }

        public static int wpmoPcRevrsb(int idx) {
            return wpmoAaRenCer(idx) + Len.AA_REN_CER;
        }

        public static int wpmoPcRevrsbNull(int idx) {
            return wpmoPcRevrsb(idx);
        }

        public static int wpmoImpRiscParzPrgt(int idx) {
            return wpmoPcRevrsb(idx) + Len.PC_REVRSB;
        }

        public static int wpmoImpRiscParzPrgtNull(int idx) {
            return wpmoImpRiscParzPrgt(idx);
        }

        public static int wpmoImpLrdDiRat(int idx) {
            return wpmoImpRiscParzPrgt(idx) + Len.IMP_RISC_PARZ_PRGT;
        }

        public static int wpmoImpLrdDiRatNull(int idx) {
            return wpmoImpLrdDiRat(idx);
        }

        public static int wpmoIbOgg(int idx) {
            return wpmoImpLrdDiRat(idx) + Len.IMP_LRD_DI_RAT;
        }

        public static int wpmoIbOggNull(int idx) {
            return wpmoIbOgg(idx);
        }

        public static int wpmoCosOner(int idx) {
            return wpmoIbOgg(idx) + Len.IB_OGG;
        }

        public static int wpmoCosOnerNull(int idx) {
            return wpmoCosOner(idx);
        }

        public static int wpmoSpePc(int idx) {
            return wpmoCosOner(idx) + Len.COS_ONER;
        }

        public static int wpmoSpePcNull(int idx) {
            return wpmoSpePc(idx);
        }

        public static int wpmoFlAttivGar(int idx) {
            return wpmoSpePc(idx) + Len.SPE_PC;
        }

        public static int wpmoFlAttivGarNull(int idx) {
            return wpmoFlAttivGar(idx);
        }

        public static int wpmoCambioVerProd(int idx) {
            return wpmoFlAttivGar(idx) + Len.FL_ATTIV_GAR;
        }

        public static int wpmoCambioVerProdNull(int idx) {
            return wpmoCambioVerProd(idx);
        }

        public static int wpmoMmDiff(int idx) {
            return wpmoCambioVerProd(idx) + Len.CAMBIO_VER_PROD;
        }

        public static int wpmoMmDiffNull(int idx) {
            return wpmoMmDiff(idx);
        }

        public static int wpmoImpRatManfee(int idx) {
            return wpmoMmDiff(idx) + Len.MM_DIFF;
        }

        public static int wpmoImpRatManfeeNull(int idx) {
            return wpmoImpRatManfee(idx);
        }

        public static int wpmoDtUltErogManfee(int idx) {
            return wpmoImpRatManfee(idx) + Len.IMP_RAT_MANFEE;
        }

        public static int wpmoDtUltErogManfeeNull(int idx) {
            return wpmoDtUltErogManfee(idx);
        }

        public static int wpmoTpOggRival(int idx) {
            return wpmoDtUltErogManfee(idx) + Len.DT_ULT_EROG_MANFEE;
        }

        public static int wpmoTpOggRivalNull(int idx) {
            return wpmoTpOggRival(idx);
        }

        public static int wpmoSomAsstaGarac(int idx) {
            return wpmoTpOggRival(idx) + Len.TP_OGG_RIVAL;
        }

        public static int wpmoSomAsstaGaracNull(int idx) {
            return wpmoSomAsstaGarac(idx);
        }

        public static int wpmoPcApplzOpz(int idx) {
            return wpmoSomAsstaGarac(idx) + Len.SOM_ASSTA_GARAC;
        }

        public static int wpmoPcApplzOpzNull(int idx) {
            return wpmoPcApplzOpz(idx);
        }

        public static int wpmoIdAdes(int idx) {
            return wpmoPcApplzOpz(idx) + Len.PC_APPLZ_OPZ;
        }

        public static int wpmoIdAdesNull(int idx) {
            return wpmoIdAdes(idx);
        }

        public static int wpmoIdPoli(int idx) {
            return wpmoIdAdes(idx) + Len.ID_ADES;
        }

        public static int wpmoTpFrmAssva(int idx) {
            return wpmoIdPoli(idx) + Len.ID_POLI;
        }

        public static int wpmoDsRiga(int idx) {
            return wpmoTpFrmAssva(idx) + Len.TP_FRM_ASSVA;
        }

        public static int wpmoDsOperSql(int idx) {
            return wpmoDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wpmoDsVer(int idx) {
            return wpmoDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wpmoDsTsIniCptz(int idx) {
            return wpmoDsVer(idx) + Len.DS_VER;
        }

        public static int wpmoDsTsEndCptz(int idx) {
            return wpmoDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wpmoDsUtente(int idx) {
            return wpmoDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wpmoDsStatoElab(int idx) {
            return wpmoDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wpmoTpEstrCnt(int idx) {
            return wpmoDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wpmoTpEstrCntNull(int idx) {
            return wpmoTpEstrCnt(idx);
        }

        public static int wpmoCodRamo(int idx) {
            return wpmoTpEstrCnt(idx) + Len.TP_ESTR_CNT;
        }

        public static int wpmoCodRamoNull(int idx) {
            return wpmoCodRamo(idx);
        }

        public static int wpmoGenDaSin(int idx) {
            return wpmoCodRamo(idx) + Len.COD_RAMO;
        }

        public static int wpmoGenDaSinNull(int idx) {
            return wpmoGenDaSin(idx);
        }

        public static int wpmoCodTari(int idx) {
            return wpmoGenDaSin(idx) + Len.GEN_DA_SIN;
        }

        public static int wpmoCodTariNull(int idx) {
            return wpmoCodTari(idx);
        }

        public static int wpmoNumRatPagPre(int idx) {
            return wpmoCodTari(idx) + Len.COD_TARI;
        }

        public static int wpmoNumRatPagPreNull(int idx) {
            return wpmoNumRatPagPre(idx);
        }

        public static int wpmoPcServVal(int idx) {
            return wpmoNumRatPagPre(idx) + Len.NUM_RAT_PAG_PRE;
        }

        public static int wpmoPcServValNull(int idx) {
            return wpmoPcServVal(idx);
        }

        public static int wpmoEtaAaSoglBnficr(int idx) {
            return wpmoPcServVal(idx) + Len.PC_SERV_VAL;
        }

        public static int wpmoEtaAaSoglBnficrNull(int idx) {
            return wpmoEtaAaSoglBnficr(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_PARAM_MOVI = 5;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_MOVI = 3;
        public static final int FRQ_MOVI = 3;
        public static final int DUR_AA = 3;
        public static final int DUR_MM = 3;
        public static final int DUR_GG = 3;
        public static final int DT_RICOR_PREC = 5;
        public static final int DT_RICOR_SUCC = 5;
        public static final int PC_INTR_FRAZ = 4;
        public static final int IMP_BNS_DA_SCO_TOT = 8;
        public static final int IMP_BNS_DA_SCO = 8;
        public static final int PC_ANTIC_BNS = 4;
        public static final int TP_RINN_COLL = 2;
        public static final int TP_RIVAL_PRE = 2;
        public static final int TP_RIVAL_PRSTZ = 2;
        public static final int FL_EVID_RIVAL = 1;
        public static final int ULT_PC_PERD = 4;
        public static final int TOT_AA_GIA_PROR = 3;
        public static final int TP_OPZ = 12;
        public static final int AA_REN_CER = 3;
        public static final int PC_REVRSB = 4;
        public static final int IMP_RISC_PARZ_PRGT = 8;
        public static final int IMP_LRD_DI_RAT = 8;
        public static final int IB_OGG = 40;
        public static final int COS_ONER = 8;
        public static final int SPE_PC = 4;
        public static final int FL_ATTIV_GAR = 1;
        public static final int CAMBIO_VER_PROD = 1;
        public static final int MM_DIFF = 2;
        public static final int IMP_RAT_MANFEE = 8;
        public static final int DT_ULT_EROG_MANFEE = 5;
        public static final int TP_OGG_RIVAL = 2;
        public static final int SOM_ASSTA_GARAC = 8;
        public static final int PC_APPLZ_OPZ = 4;
        public static final int ID_ADES = 5;
        public static final int ID_POLI = 5;
        public static final int TP_FRM_ASSVA = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int TP_ESTR_CNT = 2;
        public static final int COD_RAMO = 12;
        public static final int GEN_DA_SIN = 1;
        public static final int COD_TARI = 12;
        public static final int NUM_RAT_PAG_PRE = 3;
        public static final int PC_SERV_VAL = 4;
        public static final int ETA_AA_SOGL_BNFICR = 2;
        public static final int DATI = ID_PARAM_MOVI + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + TP_MOVI + FRQ_MOVI + DUR_AA + DUR_MM + DUR_GG + DT_RICOR_PREC + DT_RICOR_SUCC + PC_INTR_FRAZ + IMP_BNS_DA_SCO_TOT + IMP_BNS_DA_SCO + PC_ANTIC_BNS + TP_RINN_COLL + TP_RIVAL_PRE + TP_RIVAL_PRSTZ + FL_EVID_RIVAL + ULT_PC_PERD + TOT_AA_GIA_PROR + TP_OPZ + AA_REN_CER + PC_REVRSB + IMP_RISC_PARZ_PRGT + IMP_LRD_DI_RAT + IB_OGG + COS_ONER + SPE_PC + FL_ATTIV_GAR + CAMBIO_VER_PROD + MM_DIFF + IMP_RAT_MANFEE + DT_ULT_EROG_MANFEE + TP_OGG_RIVAL + SOM_ASSTA_GARAC + PC_APPLZ_OPZ + ID_ADES + ID_POLI + TP_FRM_ASSVA + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + TP_ESTR_CNT + COD_RAMO + GEN_DA_SIN + COD_TARI + NUM_RAT_PAG_PRE + PC_SERV_VAL + ETA_AA_SOGL_BNFICR;
        public static final int TAB_PARAM_MOV = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 323;
        public static final int WPMO_TAB = WpmoTab.TAB_PARAM_MOV_MAXOCCURS * TAB_PARAM_MOV;
        public static final int RESTO_TAB = 31977;
        public static final int WPMO_FRQ_MOVI_NULL = 3;
        public static final int WPMO_AA_REN_CER_NULL = 3;
        public static final int WPMO_ID_MOVI_CHIU_NULL = 5;
        public static final int WPMO_TP_MOVI_NULL = 3;
        public static final int WPMO_DUR_AA_NULL = 3;
        public static final int WPMO_DUR_MM_NULL = 3;
        public static final int WPMO_DUR_GG_NULL = 3;
        public static final int WPMO_DT_RICOR_PREC_NULL = 5;
        public static final int WPMO_DT_RICOR_SUCC_NULL = 5;
        public static final int WPMO_PC_INTR_FRAZ_NULL = 4;
        public static final int WPMO_IMP_BNS_DA_SCO_TOT_NULL = 8;
        public static final int WPMO_IMP_BNS_DA_SCO_NULL = 8;
        public static final int WPMO_PC_ANTIC_BNS_NULL = 4;
        public static final int WPMO_TP_RINN_COLL_NULL = 2;
        public static final int WPMO_TP_RIVAL_PRE_NULL = 2;
        public static final int WPMO_TP_RIVAL_PRSTZ_NULL = 2;
        public static final int WPMO_ULT_PC_PERD_NULL = 4;
        public static final int WPMO_TOT_AA_GIA_PROR_NULL = 3;
        public static final int WPMO_TP_OPZ_NULL = 12;
        public static final int WPMO_PC_REVRSB_NULL = 4;
        public static final int WPMO_IMP_RISC_PARZ_PRGT_NULL = 8;
        public static final int WPMO_IMP_LRD_DI_RAT_NULL = 8;
        public static final int WPMO_IB_OGG_NULL = 40;
        public static final int WPMO_COS_ONER_NULL = 8;
        public static final int WPMO_SPE_PC_NULL = 4;
        public static final int WPMO_MM_DIFF_NULL = 2;
        public static final int WPMO_IMP_RAT_MANFEE_NULL = 8;
        public static final int WPMO_DT_ULT_EROG_MANFEE_NULL = 5;
        public static final int WPMO_TP_OGG_RIVAL_NULL = 2;
        public static final int WPMO_SOM_ASSTA_GARAC_NULL = 8;
        public static final int WPMO_PC_APPLZ_OPZ_NULL = 4;
        public static final int WPMO_ID_ADES_NULL = 5;
        public static final int WPMO_TP_ESTR_CNT_NULL = 2;
        public static final int WPMO_COD_RAMO_NULL = 12;
        public static final int WPMO_COD_TARI_NULL = 12;
        public static final int WPMO_NUM_RAT_PAG_PRE_NULL = 3;
        public static final int WPMO_PC_SERV_VAL_NULL = 4;
        public static final int WPMO_ETA_AA_SOGL_BNFICR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_PARAM_MOVI = 9;
            public static final int ID_OGG = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int TP_MOVI = 5;
            public static final int FRQ_MOVI = 5;
            public static final int DUR_AA = 5;
            public static final int DUR_MM = 5;
            public static final int DUR_GG = 5;
            public static final int DT_RICOR_PREC = 8;
            public static final int DT_RICOR_SUCC = 8;
            public static final int PC_INTR_FRAZ = 3;
            public static final int IMP_BNS_DA_SCO_TOT = 12;
            public static final int IMP_BNS_DA_SCO = 12;
            public static final int PC_ANTIC_BNS = 3;
            public static final int ULT_PC_PERD = 3;
            public static final int TOT_AA_GIA_PROR = 5;
            public static final int AA_REN_CER = 5;
            public static final int PC_REVRSB = 3;
            public static final int IMP_RISC_PARZ_PRGT = 12;
            public static final int IMP_LRD_DI_RAT = 12;
            public static final int COS_ONER = 12;
            public static final int SPE_PC = 3;
            public static final int MM_DIFF = 2;
            public static final int IMP_RAT_MANFEE = 12;
            public static final int DT_ULT_EROG_MANFEE = 8;
            public static final int SOM_ASSTA_GARAC = 12;
            public static final int PC_APPLZ_OPZ = 3;
            public static final int ID_ADES = 9;
            public static final int ID_POLI = 9;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int NUM_RAT_PAG_PRE = 5;
            public static final int PC_SERV_VAL = 3;
            public static final int ETA_AA_SOGL_BNFICR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PC_INTR_FRAZ = 3;
            public static final int IMP_BNS_DA_SCO_TOT = 3;
            public static final int IMP_BNS_DA_SCO = 3;
            public static final int PC_ANTIC_BNS = 3;
            public static final int ULT_PC_PERD = 3;
            public static final int PC_REVRSB = 3;
            public static final int IMP_RISC_PARZ_PRGT = 3;
            public static final int IMP_LRD_DI_RAT = 3;
            public static final int COS_ONER = 3;
            public static final int SPE_PC = 3;
            public static final int IMP_RAT_MANFEE = 3;
            public static final int SOM_ASSTA_GARAC = 3;
            public static final int PC_APPLZ_OPZ = 3;
            public static final int PC_SERV_VAL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
