package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-IMP-PREST<br>
 * Variable: PRE-IMP-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreImpPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreImpPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_IMP_PREST;
    }

    public void setPreImpPrest(AfDecimal preImpPrest) {
        writeDecimalAsPacked(Pos.PRE_IMP_PREST, preImpPrest.copy());
    }

    public void setPreImpPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_IMP_PREST, Pos.PRE_IMP_PREST);
    }

    /**Original name: PRE-IMP-PREST<br>*/
    public AfDecimal getPreImpPrest() {
        return readPackedAsDecimal(Pos.PRE_IMP_PREST, Len.Int.PRE_IMP_PREST, Len.Fract.PRE_IMP_PREST);
    }

    public byte[] getPreImpPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_IMP_PREST, Pos.PRE_IMP_PREST);
        return buffer;
    }

    public void setPreImpPrestNull(String preImpPrestNull) {
        writeString(Pos.PRE_IMP_PREST_NULL, preImpPrestNull, Len.PRE_IMP_PREST_NULL);
    }

    /**Original name: PRE-IMP-PREST-NULL<br>*/
    public String getPreImpPrestNull() {
        return readString(Pos.PRE_IMP_PREST_NULL, Len.PRE_IMP_PREST_NULL);
    }

    public String getPreImpPrestNullFormatted() {
        return Functions.padBlanks(getPreImpPrestNull(), Len.PRE_IMP_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_IMP_PREST = 1;
        public static final int PRE_IMP_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_IMP_PREST = 8;
        public static final int PRE_IMP_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_IMP_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_IMP_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
