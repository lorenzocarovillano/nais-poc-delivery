package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.INumOgg;
import it.accenture.jnais.ws.redefines.NogUltProgr;

/**Original name: NUM-OGG<br>
 * Variable: NUM-OGG from copybook IDBVNOG1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class NumOggIdbsnog0 extends SerializableParameter implements INumOgg {

    //==== PROPERTIES ====
    //Original name: NOG-COD-COMPAGNIA-ANIA
    private int nogCodCompagniaAnia = DefaultValues.INT_VAL;
    //Original name: NOG-FORMA-ASSICURATIVA
    private String nogFormaAssicurativa = DefaultValues.stringVal(Len.NOG_FORMA_ASSICURATIVA);
    //Original name: NOG-COD-OGGETTO
    private String nogCodOggetto = DefaultValues.stringVal(Len.NOG_COD_OGGETTO);
    //Original name: NOG-TIPO-OGGETTO
    private String nogTipoOggetto = DefaultValues.stringVal(Len.NOG_TIPO_OGGETTO);
    //Original name: NOG-ULT-PROGR
    private NogUltProgr nogUltProgr = new NogUltProgr();
    //Original name: NOG-DESC-OGGETTO-LEN
    private short nogDescOggettoLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: NOG-DESC-OGGETTO
    private String nogDescOggetto = DefaultValues.stringVal(Len.NOG_DESC_OGGETTO);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.NUM_OGG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setNumOggBytes(buf);
    }

    public void setNumOggBytes(byte[] buffer) {
        setNumOggBytes(buffer, 1);
    }

    public byte[] getNumOggBytes() {
        byte[] buffer = new byte[Len.NUM_OGG];
        return getNumOggBytes(buffer, 1);
    }

    public void setNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        nogCodCompagniaAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.NOG_COD_COMPAGNIA_ANIA, 0);
        position += Len.NOG_COD_COMPAGNIA_ANIA;
        nogFormaAssicurativa = MarshalByte.readString(buffer, position, Len.NOG_FORMA_ASSICURATIVA);
        position += Len.NOG_FORMA_ASSICURATIVA;
        nogCodOggetto = MarshalByte.readString(buffer, position, Len.NOG_COD_OGGETTO);
        position += Len.NOG_COD_OGGETTO;
        nogTipoOggetto = MarshalByte.readString(buffer, position, Len.NOG_TIPO_OGGETTO);
        position += Len.NOG_TIPO_OGGETTO;
        nogUltProgr.setNogUltProgrFromBuffer(buffer, position);
        position += NogUltProgr.Len.NOG_ULT_PROGR;
        setNogDescOggettoVcharBytes(buffer, position);
    }

    public byte[] getNumOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, nogCodCompagniaAnia, Len.Int.NOG_COD_COMPAGNIA_ANIA, 0);
        position += Len.NOG_COD_COMPAGNIA_ANIA;
        MarshalByte.writeString(buffer, position, nogFormaAssicurativa, Len.NOG_FORMA_ASSICURATIVA);
        position += Len.NOG_FORMA_ASSICURATIVA;
        MarshalByte.writeString(buffer, position, nogCodOggetto, Len.NOG_COD_OGGETTO);
        position += Len.NOG_COD_OGGETTO;
        MarshalByte.writeString(buffer, position, nogTipoOggetto, Len.NOG_TIPO_OGGETTO);
        position += Len.NOG_TIPO_OGGETTO;
        nogUltProgr.getNogUltProgrAsBuffer(buffer, position);
        position += NogUltProgr.Len.NOG_ULT_PROGR;
        getNogDescOggettoVcharBytes(buffer, position);
        return buffer;
    }

    public void setNogCodCompagniaAnia(int nogCodCompagniaAnia) {
        this.nogCodCompagniaAnia = nogCodCompagniaAnia;
    }

    public int getNogCodCompagniaAnia() {
        return this.nogCodCompagniaAnia;
    }

    public void setNogFormaAssicurativa(String nogFormaAssicurativa) {
        this.nogFormaAssicurativa = Functions.subString(nogFormaAssicurativa, Len.NOG_FORMA_ASSICURATIVA);
    }

    public String getNogFormaAssicurativa() {
        return this.nogFormaAssicurativa;
    }

    public void setNogCodOggetto(String nogCodOggetto) {
        this.nogCodOggetto = Functions.subString(nogCodOggetto, Len.NOG_COD_OGGETTO);
    }

    public String getNogCodOggetto() {
        return this.nogCodOggetto;
    }

    public void setNogTipoOggetto(String nogTipoOggetto) {
        this.nogTipoOggetto = Functions.subString(nogTipoOggetto, Len.NOG_TIPO_OGGETTO);
    }

    public String getNogTipoOggetto() {
        return this.nogTipoOggetto;
    }

    public void setNogDescOggettoVcharFormatted(String data) {
        byte[] buffer = new byte[Len.NOG_DESC_OGGETTO_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.NOG_DESC_OGGETTO_VCHAR);
        setNogDescOggettoVcharBytes(buffer, 1);
    }

    public String getNogDescOggettoVcharFormatted() {
        return MarshalByteExt.bufferToStr(getNogDescOggettoVcharBytes());
    }

    /**Original name: NOG-DESC-OGGETTO-VCHAR<br>*/
    public byte[] getNogDescOggettoVcharBytes() {
        byte[] buffer = new byte[Len.NOG_DESC_OGGETTO_VCHAR];
        return getNogDescOggettoVcharBytes(buffer, 1);
    }

    public void setNogDescOggettoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        nogDescOggettoLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        nogDescOggetto = MarshalByte.readString(buffer, position, Len.NOG_DESC_OGGETTO);
    }

    public byte[] getNogDescOggettoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, nogDescOggettoLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, nogDescOggetto, Len.NOG_DESC_OGGETTO);
        return buffer;
    }

    public void setNogDescOggettoLen(short nogDescOggettoLen) {
        this.nogDescOggettoLen = nogDescOggettoLen;
    }

    public short getNogDescOggettoLen() {
        return this.nogDescOggettoLen;
    }

    public void setNogDescOggetto(String nogDescOggetto) {
        this.nogDescOggetto = Functions.subString(nogDescOggetto, Len.NOG_DESC_OGGETTO);
    }

    public String getNogDescOggetto() {
        return this.nogDescOggetto;
    }

    @Override
    public int getCodCompagniaAnia() {
        return getNogCodCompagniaAnia();
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.setNogCodCompagniaAnia(codCompagniaAnia);
    }

    @Override
    public String getCodOggetto() {
        return getNogCodOggetto();
    }

    @Override
    public void setCodOggetto(String codOggetto) {
        this.setNogCodOggetto(codOggetto);
    }

    @Override
    public String getDescOggettoVchar() {
        throw new FieldNotMappedException("descOggettoVchar");
    }

    @Override
    public void setDescOggettoVchar(String descOggettoVchar) {
        throw new FieldNotMappedException("descOggettoVchar");
    }

    @Override
    public String getFormaAssicurativa() {
        return getNogFormaAssicurativa();
    }

    @Override
    public void setFormaAssicurativa(String formaAssicurativa) {
        this.setNogFormaAssicurativa(formaAssicurativa);
    }

    public NogUltProgr getNogUltProgr() {
        return nogUltProgr;
    }

    @Override
    public String getTipoOggetto() {
        return getNogTipoOggetto();
    }

    @Override
    public void setTipoOggetto(String tipoOggetto) {
        this.setNogTipoOggetto(tipoOggetto);
    }

    @Override
    public long getUltProgr() {
        return nogUltProgr.getNogUltProgr();
    }

    @Override
    public void setUltProgr(long ultProgr) {
        this.nogUltProgr.setNogUltProgr(ultProgr);
    }

    @Override
    public Long getUltProgrObj() {
        return ((Long)getUltProgr());
    }

    @Override
    public void setUltProgrObj(Long ultProgrObj) {
        setUltProgr(((long)ultProgrObj));
    }

    @Override
    public byte[] serialize() {
        return getNumOggBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NOG_COD_COMPAGNIA_ANIA = 3;
        public static final int NOG_FORMA_ASSICURATIVA = 2;
        public static final int NOG_COD_OGGETTO = 30;
        public static final int NOG_TIPO_OGGETTO = 2;
        public static final int NOG_DESC_OGGETTO_LEN = 2;
        public static final int NOG_DESC_OGGETTO = 250;
        public static final int NOG_DESC_OGGETTO_VCHAR = NOG_DESC_OGGETTO_LEN + NOG_DESC_OGGETTO;
        public static final int NUM_OGG = NOG_COD_COMPAGNIA_ANIA + NOG_FORMA_ASSICURATIVA + NOG_COD_OGGETTO + NOG_TIPO_OGGETTO + NogUltProgr.Len.NOG_ULT_PROGR + NOG_DESC_OGGETTO_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NOG_COD_COMPAGNIA_ANIA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
