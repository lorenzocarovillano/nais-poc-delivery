package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPAG-RATA-DIRITTI<br>
 * Variable: WPAG-RATA-DIRITTI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagRataDiritti extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagRataDiritti() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_RATA_DIRITTI;
    }

    public void setWpagRataDiritti(AfDecimal wpagRataDiritti) {
        writeDecimalAsPacked(Pos.WPAG_RATA_DIRITTI, wpagRataDiritti.copy());
    }

    public void setWpagRataDirittiFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_RATA_DIRITTI, Pos.WPAG_RATA_DIRITTI);
    }

    /**Original name: WPAG-RATA-DIRITTI<br>*/
    public AfDecimal getWpagRataDiritti() {
        return readPackedAsDecimal(Pos.WPAG_RATA_DIRITTI, Len.Int.WPAG_RATA_DIRITTI, Len.Fract.WPAG_RATA_DIRITTI);
    }

    public byte[] getWpagRataDirittiAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_RATA_DIRITTI, Pos.WPAG_RATA_DIRITTI);
        return buffer;
    }

    public void initWpagRataDirittiSpaces() {
        fill(Pos.WPAG_RATA_DIRITTI, Len.WPAG_RATA_DIRITTI, Types.SPACE_CHAR);
    }

    public void setWpagRataDirittiNull(String wpagRataDirittiNull) {
        writeString(Pos.WPAG_RATA_DIRITTI_NULL, wpagRataDirittiNull, Len.WPAG_RATA_DIRITTI_NULL);
    }

    /**Original name: WPAG-RATA-DIRITTI-NULL<br>*/
    public String getWpagRataDirittiNull() {
        return readString(Pos.WPAG_RATA_DIRITTI_NULL, Len.WPAG_RATA_DIRITTI_NULL);
    }

    public String getWpagRataDirittiNullFormatted() {
        return Functions.padBlanks(getWpagRataDirittiNull(), Len.WPAG_RATA_DIRITTI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_DIRITTI = 1;
        public static final int WPAG_RATA_DIRITTI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_RATA_DIRITTI = 8;
        public static final int WPAG_RATA_DIRITTI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_DIRITTI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_RATA_DIRITTI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
