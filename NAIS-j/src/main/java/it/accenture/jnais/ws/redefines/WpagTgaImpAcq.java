package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-TGA-IMP-ACQ<br>
 * Variable: WPAG-TGA-IMP-ACQ from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMP_ACQ;
    }

    public void setWpagTgaImpAcq(AfDecimal wpagTgaImpAcq) {
        writeDecimalAsPacked(Pos.WPAG_TGA_IMP_ACQ, wpagTgaImpAcq.copy());
    }

    public void setWpagTgaImpAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMP_ACQ, Pos.WPAG_TGA_IMP_ACQ);
    }

    /**Original name: WPAG-TGA-IMP-ACQ<br>*/
    public AfDecimal getWpagTgaImpAcq() {
        return readPackedAsDecimal(Pos.WPAG_TGA_IMP_ACQ, Len.Int.WPAG_TGA_IMP_ACQ, Len.Fract.WPAG_TGA_IMP_ACQ);
    }

    public byte[] getWpagTgaImpAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMP_ACQ, Pos.WPAG_TGA_IMP_ACQ);
        return buffer;
    }

    public void initWpagTgaImpAcqSpaces() {
        fill(Pos.WPAG_TGA_IMP_ACQ, Len.WPAG_TGA_IMP_ACQ, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpAcqNull(String wpagTgaImpAcqNull) {
        writeString(Pos.WPAG_TGA_IMP_ACQ_NULL, wpagTgaImpAcqNull, Len.WPAG_TGA_IMP_ACQ_NULL);
    }

    /**Original name: WPAG-TGA-IMP-ACQ-NULL<br>*/
    public String getWpagTgaImpAcqNull() {
        return readString(Pos.WPAG_TGA_IMP_ACQ_NULL, Len.WPAG_TGA_IMP_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_ACQ = 1;
        public static final int WPAG_TGA_IMP_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_ACQ = 8;
        public static final int WPAG_TGA_IMP_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
