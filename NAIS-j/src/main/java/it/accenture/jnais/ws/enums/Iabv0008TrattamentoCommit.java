package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-TRATTAMENTO-COMMIT<br>
 * Variable: IABV0008-TRATTAMENTO-COMMIT from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008TrattamentoCommit {

    //==== PROPERTIES ====
    private String value = "PARTIAL";
    public static final String PARTIAL = "PARTIAL";
    public static final String FULL = "FULL";

    //==== METHODS ====
    public String getTrattamentoCommit() {
        return this.value;
    }
}
