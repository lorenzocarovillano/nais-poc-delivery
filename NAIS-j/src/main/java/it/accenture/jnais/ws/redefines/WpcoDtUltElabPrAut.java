package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-PR-AUT<br>
 * Variable: WPCO-DT-ULT-ELAB-PR-AUT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabPrAut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabPrAut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_PR_AUT;
    }

    public void setWpcoDtUltElabPrAut(int wpcoDtUltElabPrAut) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_PR_AUT, wpcoDtUltElabPrAut, Len.Int.WPCO_DT_ULT_ELAB_PR_AUT);
    }

    public void setDpcoDtUltElabPrAutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PR_AUT, Pos.WPCO_DT_ULT_ELAB_PR_AUT);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PR-AUT<br>*/
    public int getWpcoDtUltElabPrAut() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_PR_AUT, Len.Int.WPCO_DT_ULT_ELAB_PR_AUT);
    }

    public byte[] getWpcoDtUltElabPrAutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_PR_AUT, Pos.WPCO_DT_ULT_ELAB_PR_AUT);
        return buffer;
    }

    public void setWpcoDtUltElabPrAutNull(String wpcoDtUltElabPrAutNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_PR_AUT_NULL, wpcoDtUltElabPrAutNull, Len.WPCO_DT_ULT_ELAB_PR_AUT_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-PR-AUT-NULL<br>*/
    public String getWpcoDtUltElabPrAutNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_PR_AUT_NULL, Len.WPCO_DT_ULT_ELAB_PR_AUT_NULL);
    }

    public String getWpcoDtUltElabPrAutNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabPrAutNull(), Len.WPCO_DT_ULT_ELAB_PR_AUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PR_AUT = 1;
        public static final int WPCO_DT_ULT_ELAB_PR_AUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_PR_AUT = 5;
        public static final int WPCO_DT_ULT_ELAB_PR_AUT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_PR_AUT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
