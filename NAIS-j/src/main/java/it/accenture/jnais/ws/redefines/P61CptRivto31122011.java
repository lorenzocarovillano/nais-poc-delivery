package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-CPT-RIVTO-31122011<br>
 * Variable: P61-CPT-RIVTO-31122011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61CptRivto31122011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61CptRivto31122011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_CPT_RIVTO31122011;
    }

    public void setP61CptRivto31122011(AfDecimal p61CptRivto31122011) {
        writeDecimalAsPacked(Pos.P61_CPT_RIVTO31122011, p61CptRivto31122011.copy());
    }

    public void setP61CptRivto31122011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_CPT_RIVTO31122011, Pos.P61_CPT_RIVTO31122011);
    }

    /**Original name: P61-CPT-RIVTO-31122011<br>*/
    public AfDecimal getP61CptRivto31122011() {
        return readPackedAsDecimal(Pos.P61_CPT_RIVTO31122011, Len.Int.P61_CPT_RIVTO31122011, Len.Fract.P61_CPT_RIVTO31122011);
    }

    public byte[] getP61CptRivto31122011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_CPT_RIVTO31122011, Pos.P61_CPT_RIVTO31122011);
        return buffer;
    }

    public void setP61CptRivto31122011Null(String p61CptRivto31122011Null) {
        writeString(Pos.P61_CPT_RIVTO31122011_NULL, p61CptRivto31122011Null, Len.P61_CPT_RIVTO31122011_NULL);
    }

    /**Original name: P61-CPT-RIVTO-31122011-NULL<br>*/
    public String getP61CptRivto31122011Null() {
        return readString(Pos.P61_CPT_RIVTO31122011_NULL, Len.P61_CPT_RIVTO31122011_NULL);
    }

    public String getP61CptRivto31122011NullFormatted() {
        return Functions.padBlanks(getP61CptRivto31122011Null(), Len.P61_CPT_RIVTO31122011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_CPT_RIVTO31122011 = 1;
        public static final int P61_CPT_RIVTO31122011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_CPT_RIVTO31122011 = 8;
        public static final int P61_CPT_RIVTO31122011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_CPT_RIVTO31122011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_CPT_RIVTO31122011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
