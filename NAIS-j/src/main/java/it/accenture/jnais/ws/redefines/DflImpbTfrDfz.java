package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-TFR-DFZ<br>
 * Variable: DFL-IMPB-TFR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbTfrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbTfrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_TFR_DFZ;
    }

    public void setDflImpbTfrDfz(AfDecimal dflImpbTfrDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPB_TFR_DFZ, dflImpbTfrDfz.copy());
    }

    public void setDflImpbTfrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_TFR_DFZ, Pos.DFL_IMPB_TFR_DFZ);
    }

    /**Original name: DFL-IMPB-TFR-DFZ<br>*/
    public AfDecimal getDflImpbTfrDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPB_TFR_DFZ, Len.Int.DFL_IMPB_TFR_DFZ, Len.Fract.DFL_IMPB_TFR_DFZ);
    }

    public byte[] getDflImpbTfrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_TFR_DFZ, Pos.DFL_IMPB_TFR_DFZ);
        return buffer;
    }

    public void setDflImpbTfrDfzNull(String dflImpbTfrDfzNull) {
        writeString(Pos.DFL_IMPB_TFR_DFZ_NULL, dflImpbTfrDfzNull, Len.DFL_IMPB_TFR_DFZ_NULL);
    }

    /**Original name: DFL-IMPB-TFR-DFZ-NULL<br>*/
    public String getDflImpbTfrDfzNull() {
        return readString(Pos.DFL_IMPB_TFR_DFZ_NULL, Len.DFL_IMPB_TFR_DFZ_NULL);
    }

    public String getDflImpbTfrDfzNullFormatted() {
        return Functions.padBlanks(getDflImpbTfrDfzNull(), Len.DFL_IMPB_TFR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TFR_DFZ = 1;
        public static final int DFL_IMPB_TFR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TFR_DFZ = 8;
        public static final int DFL_IMPB_TFR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TFR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TFR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
