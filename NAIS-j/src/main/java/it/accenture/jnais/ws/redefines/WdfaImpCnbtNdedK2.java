package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMP-CNBT-NDED-K2<br>
 * Variable: WDFA-IMP-CNBT-NDED-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpCnbtNdedK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpCnbtNdedK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMP_CNBT_NDED_K2;
    }

    public void setWdfaImpCnbtNdedK2(AfDecimal wdfaImpCnbtNdedK2) {
        writeDecimalAsPacked(Pos.WDFA_IMP_CNBT_NDED_K2, wdfaImpCnbtNdedK2.copy());
    }

    public void setWdfaImpCnbtNdedK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMP_CNBT_NDED_K2, Pos.WDFA_IMP_CNBT_NDED_K2);
    }

    /**Original name: WDFA-IMP-CNBT-NDED-K2<br>*/
    public AfDecimal getWdfaImpCnbtNdedK2() {
        return readPackedAsDecimal(Pos.WDFA_IMP_CNBT_NDED_K2, Len.Int.WDFA_IMP_CNBT_NDED_K2, Len.Fract.WDFA_IMP_CNBT_NDED_K2);
    }

    public byte[] getWdfaImpCnbtNdedK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMP_CNBT_NDED_K2, Pos.WDFA_IMP_CNBT_NDED_K2);
        return buffer;
    }

    public void setWdfaImpCnbtNdedK2Null(String wdfaImpCnbtNdedK2Null) {
        writeString(Pos.WDFA_IMP_CNBT_NDED_K2_NULL, wdfaImpCnbtNdedK2Null, Len.WDFA_IMP_CNBT_NDED_K2_NULL);
    }

    /**Original name: WDFA-IMP-CNBT-NDED-K2-NULL<br>*/
    public String getWdfaImpCnbtNdedK2Null() {
        return readString(Pos.WDFA_IMP_CNBT_NDED_K2_NULL, Len.WDFA_IMP_CNBT_NDED_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_NDED_K2 = 1;
        public static final int WDFA_IMP_CNBT_NDED_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMP_CNBT_NDED_K2 = 8;
        public static final int WDFA_IMP_CNBT_NDED_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_NDED_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMP_CNBT_NDED_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
