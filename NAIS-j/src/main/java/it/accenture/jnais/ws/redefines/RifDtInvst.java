package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIF-DT-INVST<br>
 * Variable: RIF-DT-INVST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RifDtInvst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RifDtInvst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIF_DT_INVST;
    }

    public void setRifDtInvst(int rifDtInvst) {
        writeIntAsPacked(Pos.RIF_DT_INVST, rifDtInvst, Len.Int.RIF_DT_INVST);
    }

    public void setRifDtInvstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIF_DT_INVST, Pos.RIF_DT_INVST);
    }

    /**Original name: RIF-DT-INVST<br>*/
    public int getRifDtInvst() {
        return readPackedAsInt(Pos.RIF_DT_INVST, Len.Int.RIF_DT_INVST);
    }

    public byte[] getRifDtInvstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIF_DT_INVST, Pos.RIF_DT_INVST);
        return buffer;
    }

    public void setRifDtInvstNull(String rifDtInvstNull) {
        writeString(Pos.RIF_DT_INVST_NULL, rifDtInvstNull, Len.RIF_DT_INVST_NULL);
    }

    /**Original name: RIF-DT-INVST-NULL<br>*/
    public String getRifDtInvstNull() {
        return readString(Pos.RIF_DT_INVST_NULL, Len.RIF_DT_INVST_NULL);
    }

    public String getRifDtInvstNullFormatted() {
        return Functions.padBlanks(getRifDtInvstNull(), Len.RIF_DT_INVST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIF_DT_INVST = 1;
        public static final int RIF_DT_INVST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIF_DT_INVST = 5;
        public static final int RIF_DT_INVST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIF_DT_INVST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
