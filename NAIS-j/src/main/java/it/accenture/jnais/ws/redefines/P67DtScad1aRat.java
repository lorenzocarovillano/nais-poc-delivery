package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-SCAD-1A-RAT<br>
 * Variable: P67-DT-SCAD-1A-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtScad1aRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtScad1aRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_SCAD1A_RAT;
    }

    public void setP67DtScad1aRat(int p67DtScad1aRat) {
        writeIntAsPacked(Pos.P67_DT_SCAD1A_RAT, p67DtScad1aRat, Len.Int.P67_DT_SCAD1A_RAT);
    }

    public void setP67DtScad1aRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_SCAD1A_RAT, Pos.P67_DT_SCAD1A_RAT);
    }

    /**Original name: P67-DT-SCAD-1A-RAT<br>*/
    public int getP67DtScad1aRat() {
        return readPackedAsInt(Pos.P67_DT_SCAD1A_RAT, Len.Int.P67_DT_SCAD1A_RAT);
    }

    public byte[] getP67DtScad1aRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_SCAD1A_RAT, Pos.P67_DT_SCAD1A_RAT);
        return buffer;
    }

    public void setP67DtScad1aRatNull(String p67DtScad1aRatNull) {
        writeString(Pos.P67_DT_SCAD1A_RAT_NULL, p67DtScad1aRatNull, Len.P67_DT_SCAD1A_RAT_NULL);
    }

    /**Original name: P67-DT-SCAD-1A-RAT-NULL<br>*/
    public String getP67DtScad1aRatNull() {
        return readString(Pos.P67_DT_SCAD1A_RAT_NULL, Len.P67_DT_SCAD1A_RAT_NULL);
    }

    public String getP67DtScad1aRatNullFormatted() {
        return Functions.padBlanks(getP67DtScad1aRatNull(), Len.P67_DT_SCAD1A_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_SCAD1A_RAT = 1;
        public static final int P67_DT_SCAD1A_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_SCAD1A_RAT = 5;
        public static final int P67_DT_SCAD1A_RAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_SCAD1A_RAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
