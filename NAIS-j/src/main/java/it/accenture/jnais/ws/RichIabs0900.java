package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IRich;
import it.accenture.jnais.ws.redefines.RicIdBatch;
import it.accenture.jnais.ws.redefines.RicIdJob;
import it.accenture.jnais.ws.redefines.RicIdOgg;
import it.accenture.jnais.ws.redefines.RicIdRichCollg;
import it.accenture.jnais.ws.redefines.RicTsEffEsecRich;

/**Original name: RICH<br>
 * Variable: RICH from copybook IDBVRIC1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class RichIabs0900 extends SerializableParameter implements IRich {

    //==== PROPERTIES ====
    //Original name: RIC-ID-RICH
    private int ricIdRich = DefaultValues.INT_VAL;
    //Original name: RIC-COD-COMP-ANIA
    private int ricCodCompAnia = DefaultValues.INT_VAL;
    //Original name: RIC-TP-RICH
    private String ricTpRich = DefaultValues.stringVal(Len.RIC_TP_RICH);
    //Original name: RIC-COD-MACROFUNCT
    private String ricCodMacrofunct = DefaultValues.stringVal(Len.RIC_COD_MACROFUNCT);
    //Original name: RIC-TP-MOVI
    private int ricTpMovi = DefaultValues.INT_VAL;
    //Original name: RIC-IB-RICH
    private String ricIbRich = DefaultValues.stringVal(Len.RIC_IB_RICH);
    //Original name: RIC-DT-EFF
    private int ricDtEff = DefaultValues.INT_VAL;
    //Original name: RIC-DT-RGSTRZ-RICH
    private int ricDtRgstrzRich = DefaultValues.INT_VAL;
    //Original name: RIC-DT-PERV-RICH
    private int ricDtPervRich = DefaultValues.INT_VAL;
    //Original name: RIC-DT-ESEC-RICH
    private int ricDtEsecRich = DefaultValues.INT_VAL;
    //Original name: RIC-TS-EFF-ESEC-RICH
    private RicTsEffEsecRich ricTsEffEsecRich = new RicTsEffEsecRich();
    //Original name: RIC-ID-OGG
    private RicIdOgg ricIdOgg = new RicIdOgg();
    //Original name: RIC-TP-OGG
    private String ricTpOgg = DefaultValues.stringVal(Len.RIC_TP_OGG);
    //Original name: RIC-IB-POLI
    private String ricIbPoli = DefaultValues.stringVal(Len.RIC_IB_POLI);
    //Original name: RIC-IB-ADES
    private String ricIbAdes = DefaultValues.stringVal(Len.RIC_IB_ADES);
    //Original name: RIC-IB-GAR
    private String ricIbGar = DefaultValues.stringVal(Len.RIC_IB_GAR);
    //Original name: RIC-IB-TRCH-DI-GAR
    private String ricIbTrchDiGar = DefaultValues.stringVal(Len.RIC_IB_TRCH_DI_GAR);
    //Original name: RIC-ID-BATCH
    private RicIdBatch ricIdBatch = new RicIdBatch();
    //Original name: RIC-ID-JOB
    private RicIdJob ricIdJob = new RicIdJob();
    //Original name: RIC-FL-SIMULAZIONE
    private char ricFlSimulazione = DefaultValues.CHAR_VAL;
    //Original name: RIC-KEY-ORDINAMENTO-LEN
    private short ricKeyOrdinamentoLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: RIC-KEY-ORDINAMENTO
    private String ricKeyOrdinamento = DefaultValues.stringVal(Len.RIC_KEY_ORDINAMENTO);
    //Original name: RIC-ID-RICH-COLLG
    private RicIdRichCollg ricIdRichCollg = new RicIdRichCollg();
    //Original name: RIC-TP-RAMO-BILA
    private String ricTpRamoBila = DefaultValues.stringVal(Len.RIC_TP_RAMO_BILA);
    //Original name: RIC-TP-FRM-ASSVA
    private String ricTpFrmAssva = DefaultValues.stringVal(Len.RIC_TP_FRM_ASSVA);
    //Original name: RIC-TP-CALC-RIS
    private String ricTpCalcRis = DefaultValues.stringVal(Len.RIC_TP_CALC_RIS);
    //Original name: RIC-DS-OPER-SQL
    private char ricDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: RIC-DS-VER
    private int ricDsVer = DefaultValues.INT_VAL;
    //Original name: RIC-DS-TS-CPTZ
    private long ricDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: RIC-DS-UTENTE
    private String ricDsUtente = DefaultValues.stringVal(Len.RIC_DS_UTENTE);
    //Original name: RIC-DS-STATO-ELAB
    private char ricDsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: RIC-RAMO-BILA
    private String ricRamoBila = DefaultValues.stringVal(Len.RIC_RAMO_BILA);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RICH;
    }

    @Override
    public void deserialize(byte[] buf) {
        setRichBytes(buf);
    }

    public String getRichFormatted() {
        return MarshalByteExt.bufferToStr(getRichBytes());
    }

    public void setRichBytes(byte[] buffer) {
        setRichBytes(buffer, 1);
    }

    public byte[] getRichBytes() {
        byte[] buffer = new byte[Len.RICH];
        return getRichBytes(buffer, 1);
    }

    public void setRichBytes(byte[] buffer, int offset) {
        int position = offset;
        ricIdRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_ID_RICH, 0);
        position += Len.RIC_ID_RICH;
        ricCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_COD_COMP_ANIA, 0);
        position += Len.RIC_COD_COMP_ANIA;
        ricTpRich = MarshalByte.readString(buffer, position, Len.RIC_TP_RICH);
        position += Len.RIC_TP_RICH;
        ricCodMacrofunct = MarshalByte.readString(buffer, position, Len.RIC_COD_MACROFUNCT);
        position += Len.RIC_COD_MACROFUNCT;
        ricTpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_TP_MOVI, 0);
        position += Len.RIC_TP_MOVI;
        ricIbRich = MarshalByte.readString(buffer, position, Len.RIC_IB_RICH);
        position += Len.RIC_IB_RICH;
        ricDtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_EFF, 0);
        position += Len.RIC_DT_EFF;
        ricDtRgstrzRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_RGSTRZ_RICH, 0);
        position += Len.RIC_DT_RGSTRZ_RICH;
        ricDtPervRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_PERV_RICH, 0);
        position += Len.RIC_DT_PERV_RICH;
        ricDtEsecRich = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DT_ESEC_RICH, 0);
        position += Len.RIC_DT_ESEC_RICH;
        ricTsEffEsecRich.setRicTsEffEsecRichFromBuffer(buffer, position);
        position += RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH;
        ricIdOgg.setRicIdOggFromBuffer(buffer, position);
        position += RicIdOgg.Len.RIC_ID_OGG;
        ricTpOgg = MarshalByte.readString(buffer, position, Len.RIC_TP_OGG);
        position += Len.RIC_TP_OGG;
        ricIbPoli = MarshalByte.readString(buffer, position, Len.RIC_IB_POLI);
        position += Len.RIC_IB_POLI;
        ricIbAdes = MarshalByte.readString(buffer, position, Len.RIC_IB_ADES);
        position += Len.RIC_IB_ADES;
        ricIbGar = MarshalByte.readString(buffer, position, Len.RIC_IB_GAR);
        position += Len.RIC_IB_GAR;
        ricIbTrchDiGar = MarshalByte.readString(buffer, position, Len.RIC_IB_TRCH_DI_GAR);
        position += Len.RIC_IB_TRCH_DI_GAR;
        ricIdBatch.setRicIdBatchFromBuffer(buffer, position);
        position += RicIdBatch.Len.RIC_ID_BATCH;
        ricIdJob.setRicIdJobFromBuffer(buffer, position);
        position += RicIdJob.Len.RIC_ID_JOB;
        ricFlSimulazione = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setRicKeyOrdinamentoVcharBytes(buffer, position);
        position += Len.RIC_KEY_ORDINAMENTO_VCHAR;
        ricIdRichCollg.setRicIdRichCollgFromBuffer(buffer, position);
        position += RicIdRichCollg.Len.RIC_ID_RICH_COLLG;
        ricTpRamoBila = MarshalByte.readString(buffer, position, Len.RIC_TP_RAMO_BILA);
        position += Len.RIC_TP_RAMO_BILA;
        ricTpFrmAssva = MarshalByte.readString(buffer, position, Len.RIC_TP_FRM_ASSVA);
        position += Len.RIC_TP_FRM_ASSVA;
        ricTpCalcRis = MarshalByte.readString(buffer, position, Len.RIC_TP_CALC_RIS);
        position += Len.RIC_TP_CALC_RIS;
        ricDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ricDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.RIC_DS_VER, 0);
        position += Len.RIC_DS_VER;
        ricDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.RIC_DS_TS_CPTZ, 0);
        position += Len.RIC_DS_TS_CPTZ;
        ricDsUtente = MarshalByte.readString(buffer, position, Len.RIC_DS_UTENTE);
        position += Len.RIC_DS_UTENTE;
        ricDsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        ricRamoBila = MarshalByte.readString(buffer, position, Len.RIC_RAMO_BILA);
    }

    public byte[] getRichBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ricIdRich, Len.Int.RIC_ID_RICH, 0);
        position += Len.RIC_ID_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricCodCompAnia, Len.Int.RIC_COD_COMP_ANIA, 0);
        position += Len.RIC_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, ricTpRich, Len.RIC_TP_RICH);
        position += Len.RIC_TP_RICH;
        MarshalByte.writeString(buffer, position, ricCodMacrofunct, Len.RIC_COD_MACROFUNCT);
        position += Len.RIC_COD_MACROFUNCT;
        MarshalByte.writeIntAsPacked(buffer, position, ricTpMovi, Len.Int.RIC_TP_MOVI, 0);
        position += Len.RIC_TP_MOVI;
        MarshalByte.writeString(buffer, position, ricIbRich, Len.RIC_IB_RICH);
        position += Len.RIC_IB_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtEff, Len.Int.RIC_DT_EFF, 0);
        position += Len.RIC_DT_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtRgstrzRich, Len.Int.RIC_DT_RGSTRZ_RICH, 0);
        position += Len.RIC_DT_RGSTRZ_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtPervRich, Len.Int.RIC_DT_PERV_RICH, 0);
        position += Len.RIC_DT_PERV_RICH;
        MarshalByte.writeIntAsPacked(buffer, position, ricDtEsecRich, Len.Int.RIC_DT_ESEC_RICH, 0);
        position += Len.RIC_DT_ESEC_RICH;
        ricTsEffEsecRich.getRicTsEffEsecRichAsBuffer(buffer, position);
        position += RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH;
        ricIdOgg.getRicIdOggAsBuffer(buffer, position);
        position += RicIdOgg.Len.RIC_ID_OGG;
        MarshalByte.writeString(buffer, position, ricTpOgg, Len.RIC_TP_OGG);
        position += Len.RIC_TP_OGG;
        MarshalByte.writeString(buffer, position, ricIbPoli, Len.RIC_IB_POLI);
        position += Len.RIC_IB_POLI;
        MarshalByte.writeString(buffer, position, ricIbAdes, Len.RIC_IB_ADES);
        position += Len.RIC_IB_ADES;
        MarshalByte.writeString(buffer, position, ricIbGar, Len.RIC_IB_GAR);
        position += Len.RIC_IB_GAR;
        MarshalByte.writeString(buffer, position, ricIbTrchDiGar, Len.RIC_IB_TRCH_DI_GAR);
        position += Len.RIC_IB_TRCH_DI_GAR;
        ricIdBatch.getRicIdBatchAsBuffer(buffer, position);
        position += RicIdBatch.Len.RIC_ID_BATCH;
        ricIdJob.getRicIdJobAsBuffer(buffer, position);
        position += RicIdJob.Len.RIC_ID_JOB;
        MarshalByte.writeChar(buffer, position, ricFlSimulazione);
        position += Types.CHAR_SIZE;
        getRicKeyOrdinamentoVcharBytes(buffer, position);
        position += Len.RIC_KEY_ORDINAMENTO_VCHAR;
        ricIdRichCollg.getRicIdRichCollgAsBuffer(buffer, position);
        position += RicIdRichCollg.Len.RIC_ID_RICH_COLLG;
        MarshalByte.writeString(buffer, position, ricTpRamoBila, Len.RIC_TP_RAMO_BILA);
        position += Len.RIC_TP_RAMO_BILA;
        MarshalByte.writeString(buffer, position, ricTpFrmAssva, Len.RIC_TP_FRM_ASSVA);
        position += Len.RIC_TP_FRM_ASSVA;
        MarshalByte.writeString(buffer, position, ricTpCalcRis, Len.RIC_TP_CALC_RIS);
        position += Len.RIC_TP_CALC_RIS;
        MarshalByte.writeChar(buffer, position, ricDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, ricDsVer, Len.Int.RIC_DS_VER, 0);
        position += Len.RIC_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, ricDsTsCptz, Len.Int.RIC_DS_TS_CPTZ, 0);
        position += Len.RIC_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, ricDsUtente, Len.RIC_DS_UTENTE);
        position += Len.RIC_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, ricDsStatoElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeString(buffer, position, ricRamoBila, Len.RIC_RAMO_BILA);
        return buffer;
    }

    @Override
    public void setRicIdRich(int ricIdRich) {
        this.ricIdRich = ricIdRich;
    }

    @Override
    public int getRicIdRich() {
        return this.ricIdRich;
    }

    public void setRicCodCompAnia(int ricCodCompAnia) {
        this.ricCodCompAnia = ricCodCompAnia;
    }

    public int getRicCodCompAnia() {
        return this.ricCodCompAnia;
    }

    public void setRicTpRich(String ricTpRich) {
        this.ricTpRich = Functions.subString(ricTpRich, Len.RIC_TP_RICH);
    }

    public String getRicTpRich() {
        return this.ricTpRich;
    }

    @Override
    public void setRicCodMacrofunct(String ricCodMacrofunct) {
        this.ricCodMacrofunct = Functions.subString(ricCodMacrofunct, Len.RIC_COD_MACROFUNCT);
    }

    @Override
    public String getRicCodMacrofunct() {
        return this.ricCodMacrofunct;
    }

    public String getRicCodMacrofunctFormatted() {
        return Functions.padBlanks(getRicCodMacrofunct(), Len.RIC_COD_MACROFUNCT);
    }

    @Override
    public void setRicTpMovi(int ricTpMovi) {
        this.ricTpMovi = ricTpMovi;
    }

    @Override
    public int getRicTpMovi() {
        return this.ricTpMovi;
    }

    public void setRicIbRich(String ricIbRich) {
        this.ricIbRich = Functions.subString(ricIbRich, Len.RIC_IB_RICH);
    }

    public String getRicIbRich() {
        return this.ricIbRich;
    }

    public void setRicDtEff(int ricDtEff) {
        this.ricDtEff = ricDtEff;
    }

    public int getRicDtEff() {
        return this.ricDtEff;
    }

    public void setRicDtRgstrzRich(int ricDtRgstrzRich) {
        this.ricDtRgstrzRich = ricDtRgstrzRich;
    }

    public int getRicDtRgstrzRich() {
        return this.ricDtRgstrzRich;
    }

    public void setRicDtPervRich(int ricDtPervRich) {
        this.ricDtPervRich = ricDtPervRich;
    }

    public int getRicDtPervRich() {
        return this.ricDtPervRich;
    }

    public void setRicDtEsecRich(int ricDtEsecRich) {
        this.ricDtEsecRich = ricDtEsecRich;
    }

    public int getRicDtEsecRich() {
        return this.ricDtEsecRich;
    }

    public void setRicTpOgg(String ricTpOgg) {
        this.ricTpOgg = Functions.subString(ricTpOgg, Len.RIC_TP_OGG);
    }

    public String getRicTpOgg() {
        return this.ricTpOgg;
    }

    public String getRicTpOggFormatted() {
        return Functions.padBlanks(getRicTpOgg(), Len.RIC_TP_OGG);
    }

    public void setRicIbPoli(String ricIbPoli) {
        this.ricIbPoli = Functions.subString(ricIbPoli, Len.RIC_IB_POLI);
    }

    public String getRicIbPoli() {
        return this.ricIbPoli;
    }

    public void setRicIbAdes(String ricIbAdes) {
        this.ricIbAdes = Functions.subString(ricIbAdes, Len.RIC_IB_ADES);
    }

    public String getRicIbAdes() {
        return this.ricIbAdes;
    }

    public void setRicIbGar(String ricIbGar) {
        this.ricIbGar = Functions.subString(ricIbGar, Len.RIC_IB_GAR);
    }

    public String getRicIbGar() {
        return this.ricIbGar;
    }

    public void setRicIbTrchDiGar(String ricIbTrchDiGar) {
        this.ricIbTrchDiGar = Functions.subString(ricIbTrchDiGar, Len.RIC_IB_TRCH_DI_GAR);
    }

    public String getRicIbTrchDiGar() {
        return this.ricIbTrchDiGar;
    }

    public void setRicFlSimulazione(char ricFlSimulazione) {
        this.ricFlSimulazione = ricFlSimulazione;
    }

    public char getRicFlSimulazione() {
        return this.ricFlSimulazione;
    }

    public void setRicKeyOrdinamentoVcharFormatted(String data) {
        byte[] buffer = new byte[Len.RIC_KEY_ORDINAMENTO_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.RIC_KEY_ORDINAMENTO_VCHAR);
        setRicKeyOrdinamentoVcharBytes(buffer, 1);
    }

    public String getRicKeyOrdinamentoVcharFormatted() {
        return MarshalByteExt.bufferToStr(getRicKeyOrdinamentoVcharBytes());
    }

    /**Original name: RIC-KEY-ORDINAMENTO-VCHAR<br>*/
    public byte[] getRicKeyOrdinamentoVcharBytes() {
        byte[] buffer = new byte[Len.RIC_KEY_ORDINAMENTO_VCHAR];
        return getRicKeyOrdinamentoVcharBytes(buffer, 1);
    }

    public void setRicKeyOrdinamentoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        ricKeyOrdinamentoLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        ricKeyOrdinamento = MarshalByte.readString(buffer, position, Len.RIC_KEY_ORDINAMENTO);
    }

    public byte[] getRicKeyOrdinamentoVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, ricKeyOrdinamentoLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, ricKeyOrdinamento, Len.RIC_KEY_ORDINAMENTO);
        return buffer;
    }

    public void setRicKeyOrdinamentoLen(short ricKeyOrdinamentoLen) {
        this.ricKeyOrdinamentoLen = ricKeyOrdinamentoLen;
    }

    public short getRicKeyOrdinamentoLen() {
        return this.ricKeyOrdinamentoLen;
    }

    public void setRicKeyOrdinamento(String ricKeyOrdinamento) {
        this.ricKeyOrdinamento = Functions.subString(ricKeyOrdinamento, Len.RIC_KEY_ORDINAMENTO);
    }

    public String getRicKeyOrdinamento() {
        return this.ricKeyOrdinamento;
    }

    public void setRicTpRamoBila(String ricTpRamoBila) {
        this.ricTpRamoBila = Functions.subString(ricTpRamoBila, Len.RIC_TP_RAMO_BILA);
    }

    public String getRicTpRamoBila() {
        return this.ricTpRamoBila;
    }

    public String getRicTpRamoBilaFormatted() {
        return Functions.padBlanks(getRicTpRamoBila(), Len.RIC_TP_RAMO_BILA);
    }

    public void setRicTpFrmAssva(String ricTpFrmAssva) {
        this.ricTpFrmAssva = Functions.subString(ricTpFrmAssva, Len.RIC_TP_FRM_ASSVA);
    }

    public String getRicTpFrmAssva() {
        return this.ricTpFrmAssva;
    }

    public String getRicTpFrmAssvaFormatted() {
        return Functions.padBlanks(getRicTpFrmAssva(), Len.RIC_TP_FRM_ASSVA);
    }

    public void setRicTpCalcRis(String ricTpCalcRis) {
        this.ricTpCalcRis = Functions.subString(ricTpCalcRis, Len.RIC_TP_CALC_RIS);
    }

    public String getRicTpCalcRis() {
        return this.ricTpCalcRis;
    }

    public String getRicTpCalcRisFormatted() {
        return Functions.padBlanks(getRicTpCalcRis(), Len.RIC_TP_CALC_RIS);
    }

    public void setRicDsOperSql(char ricDsOperSql) {
        this.ricDsOperSql = ricDsOperSql;
    }

    public void setRicDsOperSqlFormatted(String ricDsOperSql) {
        setRicDsOperSql(Functions.charAt(ricDsOperSql, Types.CHAR_SIZE));
    }

    public char getRicDsOperSql() {
        return this.ricDsOperSql;
    }

    public void setRicDsVer(int ricDsVer) {
        this.ricDsVer = ricDsVer;
    }

    public int getRicDsVer() {
        return this.ricDsVer;
    }

    public void setRicDsTsCptz(long ricDsTsCptz) {
        this.ricDsTsCptz = ricDsTsCptz;
    }

    public long getRicDsTsCptz() {
        return this.ricDsTsCptz;
    }

    public void setRicDsUtente(String ricDsUtente) {
        this.ricDsUtente = Functions.subString(ricDsUtente, Len.RIC_DS_UTENTE);
    }

    public String getRicDsUtente() {
        return this.ricDsUtente;
    }

    public void setRicDsStatoElab(char ricDsStatoElab) {
        this.ricDsStatoElab = ricDsStatoElab;
    }

    public void setRicDsStatoElabFormatted(String ricDsStatoElab) {
        setRicDsStatoElab(Functions.charAt(ricDsStatoElab, Types.CHAR_SIZE));
    }

    public char getRicDsStatoElab() {
        return this.ricDsStatoElab;
    }

    public void setRicRamoBila(String ricRamoBila) {
        this.ricRamoBila = Functions.subString(ricRamoBila, Len.RIC_RAMO_BILA);
    }

    public String getRicRamoBila() {
        return this.ricRamoBila;
    }

    public String getRicRamoBilaFormatted() {
        return Functions.padBlanks(getRicRamoBila(), Len.RIC_RAMO_BILA);
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsCptz() {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        throw new FieldNotMappedException("dsTsCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtEffDb() {
        throw new FieldNotMappedException("dtEffDb");
    }

    @Override
    public void setDtEffDb(String dtEffDb) {
        throw new FieldNotMappedException("dtEffDb");
    }

    @Override
    public String getDtEsecRichDb() {
        throw new FieldNotMappedException("dtEsecRichDb");
    }

    @Override
    public void setDtEsecRichDb(String dtEsecRichDb) {
        throw new FieldNotMappedException("dtEsecRichDb");
    }

    @Override
    public String getDtPervRichDb() {
        throw new FieldNotMappedException("dtPervRichDb");
    }

    @Override
    public void setDtPervRichDb(String dtPervRichDb) {
        throw new FieldNotMappedException("dtPervRichDb");
    }

    @Override
    public String getDtRgstrzRichDb() {
        throw new FieldNotMappedException("dtRgstrzRichDb");
    }

    @Override
    public void setDtRgstrzRichDb(String dtRgstrzRichDb) {
        throw new FieldNotMappedException("dtRgstrzRichDb");
    }

    @Override
    public char getFlSimulazione() {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        throw new FieldNotMappedException("flSimulazione");
    }

    @Override
    public Character getFlSimulazioneObj() {
        return ((Character)getFlSimulazione());
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        setFlSimulazione(((char)flSimulazioneObj));
    }

    @Override
    public char getIabv0002State01() {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public char getIabv0002State02() {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public char getIabv0002State03() {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public char getIabv0002State04() {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public char getIabv0002State05() {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public char getIabv0002State06() {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public char getIabv0002State07() {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public char getIabv0002State08() {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public char getIabv0002State09() {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public char getIabv0002State10() {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public char getIabv0002StateCurrent() {
        throw new FieldNotMappedException("iabv0002StateCurrent");
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        throw new FieldNotMappedException("iabv0002StateCurrent");
    }

    @Override
    public String getIbAdes() {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public void setIbAdes(String ibAdes) {
        throw new FieldNotMappedException("ibAdes");
    }

    @Override
    public String getIbAdesObj() {
        return getIbAdes();
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        setIbAdes(ibAdesObj);
    }

    @Override
    public String getIbGar() {
        throw new FieldNotMappedException("ibGar");
    }

    @Override
    public void setIbGar(String ibGar) {
        throw new FieldNotMappedException("ibGar");
    }

    @Override
    public String getIbGarObj() {
        return getIbGar();
    }

    @Override
    public void setIbGarObj(String ibGarObj) {
        setIbGar(ibGarObj);
    }

    @Override
    public String getIbPoli() {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public void setIbPoli(String ibPoli) {
        throw new FieldNotMappedException("ibPoli");
    }

    @Override
    public String getIbPoliObj() {
        return getIbPoli();
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        setIbPoli(ibPoliObj);
    }

    @Override
    public String getIbRich() {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public void setIbRich(String ibRich) {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public String getIbRichObj() {
        return getIbRich();
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        setIbRich(ibRichObj);
    }

    @Override
    public String getIbTrchDiGar() {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        throw new FieldNotMappedException("ibTrchDiGar");
    }

    @Override
    public String getIbTrchDiGarObj() {
        return getIbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        setIbTrchDiGar(ibTrchDiGarObj);
    }

    @Override
    public int getIdJob() {
        throw new FieldNotMappedException("idJob");
    }

    @Override
    public void setIdJob(int idJob) {
        throw new FieldNotMappedException("idJob");
    }

    @Override
    public Integer getIdJobObj() {
        return ((Integer)getIdJob());
    }

    @Override
    public void setIdJobObj(Integer idJobObj) {
        setIdJob(((int)idJobObj));
    }

    @Override
    public int getIdOgg() {
        throw new FieldNotMappedException("idOgg");
    }

    @Override
    public void setIdOgg(int idOgg) {
        throw new FieldNotMappedException("idOgg");
    }

    @Override
    public Integer getIdOggObj() {
        return ((Integer)getIdOgg());
    }

    @Override
    public void setIdOggObj(Integer idOggObj) {
        setIdOgg(((int)idOggObj));
    }

    @Override
    public int getIdRichCollg() {
        throw new FieldNotMappedException("idRichCollg");
    }

    @Override
    public void setIdRichCollg(int idRichCollg) {
        throw new FieldNotMappedException("idRichCollg");
    }

    @Override
    public Integer getIdRichCollgObj() {
        return ((Integer)getIdRichCollg());
    }

    @Override
    public void setIdRichCollgObj(Integer idRichCollgObj) {
        setIdRichCollg(((int)idRichCollgObj));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public String getKeyOrdinamentoVchar() {
        throw new FieldNotMappedException("keyOrdinamentoVchar");
    }

    @Override
    public void setKeyOrdinamentoVchar(String keyOrdinamentoVchar) {
        throw new FieldNotMappedException("keyOrdinamentoVchar");
    }

    @Override
    public String getKeyOrdinamentoVcharObj() {
        return getKeyOrdinamentoVchar();
    }

    @Override
    public void setKeyOrdinamentoVcharObj(String keyOrdinamentoVcharObj) {
        setKeyOrdinamentoVchar(keyOrdinamentoVcharObj);
    }

    @Override
    public char getLdbv4511StatoElab00() {
        throw new FieldNotMappedException("ldbv4511StatoElab00");
    }

    @Override
    public void setLdbv4511StatoElab00(char ldbv4511StatoElab00) {
        throw new FieldNotMappedException("ldbv4511StatoElab00");
    }

    @Override
    public char getLdbv4511StatoElab01() {
        throw new FieldNotMappedException("ldbv4511StatoElab01");
    }

    @Override
    public void setLdbv4511StatoElab01(char ldbv4511StatoElab01) {
        throw new FieldNotMappedException("ldbv4511StatoElab01");
    }

    @Override
    public char getLdbv4511StatoElab02() {
        throw new FieldNotMappedException("ldbv4511StatoElab02");
    }

    @Override
    public void setLdbv4511StatoElab02(char ldbv4511StatoElab02) {
        throw new FieldNotMappedException("ldbv4511StatoElab02");
    }

    @Override
    public char getLdbv4511StatoElab03() {
        throw new FieldNotMappedException("ldbv4511StatoElab03");
    }

    @Override
    public void setLdbv4511StatoElab03(char ldbv4511StatoElab03) {
        throw new FieldNotMappedException("ldbv4511StatoElab03");
    }

    @Override
    public char getLdbv4511StatoElab04() {
        throw new FieldNotMappedException("ldbv4511StatoElab04");
    }

    @Override
    public void setLdbv4511StatoElab04(char ldbv4511StatoElab04) {
        throw new FieldNotMappedException("ldbv4511StatoElab04");
    }

    @Override
    public char getLdbv4511StatoElab05() {
        throw new FieldNotMappedException("ldbv4511StatoElab05");
    }

    @Override
    public void setLdbv4511StatoElab05(char ldbv4511StatoElab05) {
        throw new FieldNotMappedException("ldbv4511StatoElab05");
    }

    @Override
    public char getLdbv4511StatoElab06() {
        throw new FieldNotMappedException("ldbv4511StatoElab06");
    }

    @Override
    public void setLdbv4511StatoElab06(char ldbv4511StatoElab06) {
        throw new FieldNotMappedException("ldbv4511StatoElab06");
    }

    @Override
    public char getLdbv4511StatoElab07() {
        throw new FieldNotMappedException("ldbv4511StatoElab07");
    }

    @Override
    public void setLdbv4511StatoElab07(char ldbv4511StatoElab07) {
        throw new FieldNotMappedException("ldbv4511StatoElab07");
    }

    @Override
    public char getLdbv4511StatoElab08() {
        throw new FieldNotMappedException("ldbv4511StatoElab08");
    }

    @Override
    public void setLdbv4511StatoElab08(char ldbv4511StatoElab08) {
        throw new FieldNotMappedException("ldbv4511StatoElab08");
    }

    @Override
    public char getLdbv4511StatoElab09() {
        throw new FieldNotMappedException("ldbv4511StatoElab09");
    }

    @Override
    public void setLdbv4511StatoElab09(char ldbv4511StatoElab09) {
        throw new FieldNotMappedException("ldbv4511StatoElab09");
    }

    @Override
    public String getLdbv4511TpRich00() {
        throw new FieldNotMappedException("ldbv4511TpRich00");
    }

    @Override
    public void setLdbv4511TpRich00(String ldbv4511TpRich00) {
        throw new FieldNotMappedException("ldbv4511TpRich00");
    }

    @Override
    public String getLdbv4511TpRich01() {
        throw new FieldNotMappedException("ldbv4511TpRich01");
    }

    @Override
    public void setLdbv4511TpRich01(String ldbv4511TpRich01) {
        throw new FieldNotMappedException("ldbv4511TpRich01");
    }

    @Override
    public String getLdbv4511TpRich02() {
        throw new FieldNotMappedException("ldbv4511TpRich02");
    }

    @Override
    public void setLdbv4511TpRich02(String ldbv4511TpRich02) {
        throw new FieldNotMappedException("ldbv4511TpRich02");
    }

    @Override
    public String getLdbv4511TpRich03() {
        throw new FieldNotMappedException("ldbv4511TpRich03");
    }

    @Override
    public void setLdbv4511TpRich03(String ldbv4511TpRich03) {
        throw new FieldNotMappedException("ldbv4511TpRich03");
    }

    @Override
    public String getLdbv4511TpRich04() {
        throw new FieldNotMappedException("ldbv4511TpRich04");
    }

    @Override
    public void setLdbv4511TpRich04(String ldbv4511TpRich04) {
        throw new FieldNotMappedException("ldbv4511TpRich04");
    }

    @Override
    public String getLdbv4511TpRich05() {
        throw new FieldNotMappedException("ldbv4511TpRich05");
    }

    @Override
    public void setLdbv4511TpRich05(String ldbv4511TpRich05) {
        throw new FieldNotMappedException("ldbv4511TpRich05");
    }

    @Override
    public String getLdbv4511TpRich06() {
        throw new FieldNotMappedException("ldbv4511TpRich06");
    }

    @Override
    public void setLdbv4511TpRich06(String ldbv4511TpRich06) {
        throw new FieldNotMappedException("ldbv4511TpRich06");
    }

    @Override
    public String getLdbv4511TpRich07() {
        throw new FieldNotMappedException("ldbv4511TpRich07");
    }

    @Override
    public void setLdbv4511TpRich07(String ldbv4511TpRich07) {
        throw new FieldNotMappedException("ldbv4511TpRich07");
    }

    @Override
    public String getLdbv4511TpRich08() {
        throw new FieldNotMappedException("ldbv4511TpRich08");
    }

    @Override
    public void setLdbv4511TpRich08(String ldbv4511TpRich08) {
        throw new FieldNotMappedException("ldbv4511TpRich08");
    }

    @Override
    public String getLdbv4511TpRich09() {
        throw new FieldNotMappedException("ldbv4511TpRich09");
    }

    @Override
    public void setLdbv4511TpRich09(String ldbv4511TpRich09) {
        throw new FieldNotMappedException("ldbv4511TpRich09");
    }

    @Override
    public String getPrenotazione() {
        throw new FieldNotMappedException("prenotazione");
    }

    @Override
    public void setPrenotazione(String prenotazione) {
        throw new FieldNotMappedException("prenotazione");
    }

    @Override
    public String getRamoBila() {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public void setRamoBila(String ramoBila) {
        throw new FieldNotMappedException("ramoBila");
    }

    @Override
    public String getRamoBilaObj() {
        return getRamoBila();
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        setRamoBila(ramoBilaObj);
    }

    public RicIdBatch getRicIdBatchRP() {
        return ricIdBatch;
    }

    @Override
    public int getRicIdBatch() {
        throw new FieldNotMappedException("ricIdBatch");
    }

    @Override
    public void setRicIdBatch(int ricIdBatch) {
        throw new FieldNotMappedException("ricIdBatch");
    }

    @Override
    public Integer getRicIdBatchObj() {
        return ((Integer)getRicIdBatch());
    }

    @Override
    public void setRicIdBatchObj(Integer ricIdBatchObj) {
        setRicIdBatch(((int)ricIdBatchObj));
    }

    public RicIdJob getRicIdJob() {
        return ricIdJob;
    }

    public RicIdOgg getRicIdOgg() {
        return ricIdOgg;
    }

    public RicIdRichCollg getRicIdRichCollg() {
        return ricIdRichCollg;
    }

    public RicTsEffEsecRich getRicTsEffEsecRich() {
        return ricTsEffEsecRich;
    }

    @Override
    public String getTpCalcRis() {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        throw new FieldNotMappedException("tpCalcRis");
    }

    @Override
    public String getTpCalcRisObj() {
        return getTpCalcRis();
    }

    @Override
    public void setTpCalcRisObj(String tpCalcRisObj) {
        setTpCalcRis(tpCalcRisObj);
    }

    @Override
    public String getTpFrmAssva() {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        throw new FieldNotMappedException("tpFrmAssva");
    }

    @Override
    public String getTpFrmAssvaObj() {
        return getTpFrmAssva();
    }

    @Override
    public void setTpFrmAssvaObj(String tpFrmAssvaObj) {
        setTpFrmAssva(tpFrmAssvaObj);
    }

    @Override
    public String getTpOgg() {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public void setTpOgg(String tpOgg) {
        throw new FieldNotMappedException("tpOgg");
    }

    @Override
    public String getTpOggObj() {
        return getTpOgg();
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        setTpOgg(tpOggObj);
    }

    @Override
    public String getTpRamoBila() {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        throw new FieldNotMappedException("tpRamoBila");
    }

    @Override
    public String getTpRamoBilaObj() {
        return getTpRamoBila();
    }

    @Override
    public void setTpRamoBilaObj(String tpRamoBilaObj) {
        setTpRamoBila(tpRamoBilaObj);
    }

    @Override
    public String getTpRich() {
        throw new FieldNotMappedException("tpRich");
    }

    @Override
    public void setTpRich(String tpRich) {
        throw new FieldNotMappedException("tpRich");
    }

    @Override
    public long getTsEffEsecRich() {
        throw new FieldNotMappedException("tsEffEsecRich");
    }

    @Override
    public void setTsEffEsecRich(long tsEffEsecRich) {
        throw new FieldNotMappedException("tsEffEsecRich");
    }

    @Override
    public Long getTsEffEsecRichObj() {
        return ((Long)getTsEffEsecRich());
    }

    @Override
    public void setTsEffEsecRichObj(Long tsEffEsecRichObj) {
        setTsEffEsecRich(((long)tsEffEsecRichObj));
    }

    @Override
    public byte[] serialize() {
        return getRichBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_ID_RICH = 5;
        public static final int RIC_COD_COMP_ANIA = 3;
        public static final int RIC_TP_RICH = 2;
        public static final int RIC_COD_MACROFUNCT = 2;
        public static final int RIC_TP_MOVI = 3;
        public static final int RIC_IB_RICH = 40;
        public static final int RIC_DT_EFF = 5;
        public static final int RIC_DT_RGSTRZ_RICH = 5;
        public static final int RIC_DT_PERV_RICH = 5;
        public static final int RIC_DT_ESEC_RICH = 5;
        public static final int RIC_TP_OGG = 2;
        public static final int RIC_IB_POLI = 40;
        public static final int RIC_IB_ADES = 40;
        public static final int RIC_IB_GAR = 40;
        public static final int RIC_IB_TRCH_DI_GAR = 40;
        public static final int RIC_FL_SIMULAZIONE = 1;
        public static final int RIC_KEY_ORDINAMENTO_LEN = 2;
        public static final int RIC_KEY_ORDINAMENTO = 300;
        public static final int RIC_KEY_ORDINAMENTO_VCHAR = RIC_KEY_ORDINAMENTO_LEN + RIC_KEY_ORDINAMENTO;
        public static final int RIC_TP_RAMO_BILA = 2;
        public static final int RIC_TP_FRM_ASSVA = 2;
        public static final int RIC_TP_CALC_RIS = 2;
        public static final int RIC_DS_OPER_SQL = 1;
        public static final int RIC_DS_VER = 5;
        public static final int RIC_DS_TS_CPTZ = 10;
        public static final int RIC_DS_UTENTE = 20;
        public static final int RIC_DS_STATO_ELAB = 1;
        public static final int RIC_RAMO_BILA = 12;
        public static final int RICH = RIC_ID_RICH + RIC_COD_COMP_ANIA + RIC_TP_RICH + RIC_COD_MACROFUNCT + RIC_TP_MOVI + RIC_IB_RICH + RIC_DT_EFF + RIC_DT_RGSTRZ_RICH + RIC_DT_PERV_RICH + RIC_DT_ESEC_RICH + RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH + RicIdOgg.Len.RIC_ID_OGG + RIC_TP_OGG + RIC_IB_POLI + RIC_IB_ADES + RIC_IB_GAR + RIC_IB_TRCH_DI_GAR + RicIdBatch.Len.RIC_ID_BATCH + RicIdJob.Len.RIC_ID_JOB + RIC_FL_SIMULAZIONE + RIC_KEY_ORDINAMENTO_VCHAR + RicIdRichCollg.Len.RIC_ID_RICH_COLLG + RIC_TP_RAMO_BILA + RIC_TP_FRM_ASSVA + RIC_TP_CALC_RIS + RIC_DS_OPER_SQL + RIC_DS_VER + RIC_DS_TS_CPTZ + RIC_DS_UTENTE + RIC_DS_STATO_ELAB + RIC_RAMO_BILA;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_ID_RICH = 9;
            public static final int RIC_COD_COMP_ANIA = 5;
            public static final int RIC_TP_MOVI = 5;
            public static final int RIC_DT_EFF = 8;
            public static final int RIC_DT_RGSTRZ_RICH = 8;
            public static final int RIC_DT_PERV_RICH = 8;
            public static final int RIC_DT_ESEC_RICH = 8;
            public static final int RIC_DS_VER = 9;
            public static final int RIC_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
