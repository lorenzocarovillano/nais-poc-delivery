package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-IMP-FIN-REVOLVING<br>
 * Variable: WP67-IMP-FIN-REVOLVING from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67ImpFinRevolving extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67ImpFinRevolving() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_IMP_FIN_REVOLVING;
    }

    public void setWp67ImpFinRevolving(AfDecimal wp67ImpFinRevolving) {
        writeDecimalAsPacked(Pos.WP67_IMP_FIN_REVOLVING, wp67ImpFinRevolving.copy());
    }

    public void setWp67ImpFinRevolvingFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_IMP_FIN_REVOLVING, Pos.WP67_IMP_FIN_REVOLVING);
    }

    /**Original name: WP67-IMP-FIN-REVOLVING<br>*/
    public AfDecimal getWp67ImpFinRevolving() {
        return readPackedAsDecimal(Pos.WP67_IMP_FIN_REVOLVING, Len.Int.WP67_IMP_FIN_REVOLVING, Len.Fract.WP67_IMP_FIN_REVOLVING);
    }

    public byte[] getWp67ImpFinRevolvingAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_IMP_FIN_REVOLVING, Pos.WP67_IMP_FIN_REVOLVING);
        return buffer;
    }

    public void setWp67ImpFinRevolvingNull(String wp67ImpFinRevolvingNull) {
        writeString(Pos.WP67_IMP_FIN_REVOLVING_NULL, wp67ImpFinRevolvingNull, Len.WP67_IMP_FIN_REVOLVING_NULL);
    }

    /**Original name: WP67-IMP-FIN-REVOLVING-NULL<br>*/
    public String getWp67ImpFinRevolvingNull() {
        return readString(Pos.WP67_IMP_FIN_REVOLVING_NULL, Len.WP67_IMP_FIN_REVOLVING_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_IMP_FIN_REVOLVING = 1;
        public static final int WP67_IMP_FIN_REVOLVING_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_IMP_FIN_REVOLVING = 8;
        public static final int WP67_IMP_FIN_REVOLVING_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_IMP_FIN_REVOLVING = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_IMP_FIN_REVOLVING = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
