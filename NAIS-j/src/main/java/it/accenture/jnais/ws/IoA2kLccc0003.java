package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOutput;

/**Original name: IO-A2K-LCCC0003<br>
 * Variable: IO-A2K-LCCC0003 from copybook LCCC0003<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class IoA2kLccc0003 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: A2K-INPUT
    private A2kInput input = new A2kInput();
    //Original name: A2K-OUTPUT
    private A2kOutput output = new A2kOutput();
    /**Original name: A2K-RCODE<br>
	 * <pre>                                       return code</pre>*/
    private String rcode = DefaultValues.stringVal(Len.RCODE);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IO_A2K_LCCC0003;
    }

    @Override
    public void deserialize(byte[] buf) {
        setIoA2kLccc0003Bytes(buf);
    }

    public String getIoA2kLccc0003Formatted() {
        return MarshalByteExt.bufferToStr(getIoA2kLccc0003Bytes());
    }

    public void setIoA2kLccc0003Bytes(byte[] buffer) {
        setIoA2kLccc0003Bytes(buffer, 1);
    }

    public byte[] getIoA2kLccc0003Bytes() {
        byte[] buffer = new byte[Len.IO_A2K_LCCC0003];
        return getIoA2kLccc0003Bytes(buffer, 1);
    }

    public void setIoA2kLccc0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLccc0003Bytes(buffer, position);
        position += Len.LCCC0003;
        rcode = MarshalByte.readString(buffer, position, Len.RCODE);
    }

    public byte[] getIoA2kLccc0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        getLccc0003Bytes(buffer, position);
        position += Len.LCCC0003;
        MarshalByte.writeString(buffer, position, rcode, Len.RCODE);
        return buffer;
    }

    public void setLccc0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        input.setInputBytes(buffer, position);
        position += A2kInput.Len.INPUT;
        output.setOutputBytes(buffer, position);
    }

    public byte[] getLccc0003Bytes(byte[] buffer, int offset) {
        int position = offset;
        input.getInputBytes(buffer, position);
        position += A2kInput.Len.INPUT;
        output.getOutputBytes(buffer, position);
        return buffer;
    }

    public void setRcode(String rcode) {
        this.rcode = Functions.subString(rcode, Len.RCODE);
    }

    public String getRcode() {
        return this.rcode;
    }

    public A2kInput getInput() {
        return input;
    }

    public A2kOutput getOutput() {
        return output;
    }

    @Override
    public byte[] serialize() {
        return getIoA2kLccc0003Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0003 = A2kInput.Len.INPUT + A2kOutput.Len.OUTPUT;
        public static final int RCODE = 2;
        public static final int IO_A2K_LCCC0003 = LCCC0003 + RCODE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
