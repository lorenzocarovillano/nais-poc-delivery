package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-TAB<br>
 * Variable: WTLI-TAB from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TRCH_LIQ_MAXOCCURS = 1250;
    public static final char WTLI_ST_ADD = 'A';
    public static final char WTLI_ST_MOD = 'M';
    public static final char WTLI_ST_INV = 'I';
    public static final char WTLI_ST_DEL = 'D';
    public static final char WTLI_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WtliTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_TAB;
    }

    public String getWtliTabFormatted() {
        return readFixedString(Pos.WTLI_TAB, Len.WTLI_TAB);
    }

    public void setWtliTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_TAB, Pos.WTLI_TAB);
    }

    public byte[] getWtliTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_TAB, Pos.WTLI_TAB);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wtliStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WTLI-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_LIQ
	 *    ALIAS TLI
	 *    ULTIMO AGG. 09 LUG 2009
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wtliStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wtliIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WTLI-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wtliIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdTrchLiq(int idTrchLiqIdx, int idTrchLiq) {
        int position = Pos.wtliIdTrchLiq(idTrchLiqIdx - 1);
        writeIntAsPacked(position, idTrchLiq, Len.Int.ID_TRCH_LIQ);
    }

    /**Original name: WTLI-ID-TRCH-LIQ<br>*/
    public int getIdTrchLiq(int idTrchLiqIdx) {
        int position = Pos.wtliIdTrchLiq(idTrchLiqIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_LIQ);
    }

    public void setIdGarLiq(int idGarLiqIdx, int idGarLiq) {
        int position = Pos.wtliIdGarLiq(idGarLiqIdx - 1);
        writeIntAsPacked(position, idGarLiq, Len.Int.ID_GAR_LIQ);
    }

    /**Original name: WTLI-ID-GAR-LIQ<br>*/
    public int getIdGarLiq(int idGarLiqIdx) {
        int position = Pos.wtliIdGarLiq(idGarLiqIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_GAR_LIQ);
    }

    public void setIdLiq(int idLiqIdx, int idLiq) {
        int position = Pos.wtliIdLiq(idLiqIdx - 1);
        writeIntAsPacked(position, idLiq, Len.Int.ID_LIQ);
    }

    /**Original name: WTLI-ID-LIQ<br>*/
    public int getIdLiq(int idLiqIdx) {
        int position = Pos.wtliIdLiq(idLiqIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_LIQ);
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.wtliIdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: WTLI-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.wtliIdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wtliIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WTLI-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wtliIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wtliIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WTLI-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wtliIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wtliDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WTLI-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wtliDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wtliDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WTLI-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wtliDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wtliCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WTLI-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wtliCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setImpLrdCalc(int impLrdCalcIdx, AfDecimal impLrdCalc) {
        int position = Pos.wtliImpLrdCalc(impLrdCalcIdx - 1);
        writeDecimalAsPacked(position, impLrdCalc.copy());
    }

    /**Original name: WTLI-IMP-LRD-CALC<br>*/
    public AfDecimal getImpLrdCalc(int impLrdCalcIdx) {
        int position = Pos.wtliImpLrdCalc(impLrdCalcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_LRD_CALC, Len.Fract.IMP_LRD_CALC);
    }

    public void setImpLrdDfz(int impLrdDfzIdx, AfDecimal impLrdDfz) {
        int position = Pos.wtliImpLrdDfz(impLrdDfzIdx - 1);
        writeDecimalAsPacked(position, impLrdDfz.copy());
    }

    /**Original name: WTLI-IMP-LRD-DFZ<br>*/
    public AfDecimal getImpLrdDfz(int impLrdDfzIdx) {
        int position = Pos.wtliImpLrdDfz(impLrdDfzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_LRD_DFZ, Len.Fract.IMP_LRD_DFZ);
    }

    public void setImpLrdEfflq(int impLrdEfflqIdx, AfDecimal impLrdEfflq) {
        int position = Pos.wtliImpLrdEfflq(impLrdEfflqIdx - 1);
        writeDecimalAsPacked(position, impLrdEfflq.copy());
    }

    /**Original name: WTLI-IMP-LRD-EFFLQ<br>*/
    public AfDecimal getImpLrdEfflq(int impLrdEfflqIdx) {
        int position = Pos.wtliImpLrdEfflq(impLrdEfflqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_LRD_EFFLQ, Len.Fract.IMP_LRD_EFFLQ);
    }

    public void setImpNetCalc(int impNetCalcIdx, AfDecimal impNetCalc) {
        int position = Pos.wtliImpNetCalc(impNetCalcIdx - 1);
        writeDecimalAsPacked(position, impNetCalc.copy());
    }

    /**Original name: WTLI-IMP-NET-CALC<br>*/
    public AfDecimal getImpNetCalc(int impNetCalcIdx) {
        int position = Pos.wtliImpNetCalc(impNetCalcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_NET_CALC, Len.Fract.IMP_NET_CALC);
    }

    public void setImpNetDfz(int impNetDfzIdx, AfDecimal impNetDfz) {
        int position = Pos.wtliImpNetDfz(impNetDfzIdx - 1);
        writeDecimalAsPacked(position, impNetDfz.copy());
    }

    /**Original name: WTLI-IMP-NET-DFZ<br>*/
    public AfDecimal getImpNetDfz(int impNetDfzIdx) {
        int position = Pos.wtliImpNetDfz(impNetDfzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_NET_DFZ, Len.Fract.IMP_NET_DFZ);
    }

    public void setImpNetEfflq(int impNetEfflqIdx, AfDecimal impNetEfflq) {
        int position = Pos.wtliImpNetEfflq(impNetEfflqIdx - 1);
        writeDecimalAsPacked(position, impNetEfflq.copy());
    }

    /**Original name: WTLI-IMP-NET-EFFLQ<br>*/
    public AfDecimal getImpNetEfflq(int impNetEfflqIdx) {
        int position = Pos.wtliImpNetEfflq(impNetEfflqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_NET_EFFLQ, Len.Fract.IMP_NET_EFFLQ);
    }

    public void setImpUti(int impUtiIdx, AfDecimal impUti) {
        int position = Pos.wtliImpUti(impUtiIdx - 1);
        writeDecimalAsPacked(position, impUti.copy());
    }

    /**Original name: WTLI-IMP-UTI<br>*/
    public AfDecimal getImpUti(int impUtiIdx) {
        int position = Pos.wtliImpUti(impUtiIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_UTI, Len.Fract.IMP_UTI);
    }

    public void setCodTari(int codTariIdx, String codTari) {
        int position = Pos.wtliCodTari(codTariIdx - 1);
        writeString(position, codTari, Len.COD_TARI);
    }

    /**Original name: WTLI-COD-TARI<br>*/
    public String getCodTari(int codTariIdx) {
        int position = Pos.wtliCodTari(codTariIdx - 1);
        return readString(position, Len.COD_TARI);
    }

    public void setCodDvs(int codDvsIdx, String codDvs) {
        int position = Pos.wtliCodDvs(codDvsIdx - 1);
        writeString(position, codDvs, Len.COD_DVS);
    }

    /**Original name: WTLI-COD-DVS<br>*/
    public String getCodDvs(int codDvsIdx) {
        int position = Pos.wtliCodDvs(codDvsIdx - 1);
        return readString(position, Len.COD_DVS);
    }

    public void setIasOnerPrvntFin(int iasOnerPrvntFinIdx, AfDecimal iasOnerPrvntFin) {
        int position = Pos.wtliIasOnerPrvntFin(iasOnerPrvntFinIdx - 1);
        writeDecimalAsPacked(position, iasOnerPrvntFin.copy());
    }

    /**Original name: WTLI-IAS-ONER-PRVNT-FIN<br>*/
    public AfDecimal getIasOnerPrvntFin(int iasOnerPrvntFinIdx) {
        int position = Pos.wtliIasOnerPrvntFin(iasOnerPrvntFinIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IAS_ONER_PRVNT_FIN, Len.Fract.IAS_ONER_PRVNT_FIN);
    }

    public void setIasMggSin(int iasMggSinIdx, AfDecimal iasMggSin) {
        int position = Pos.wtliIasMggSin(iasMggSinIdx - 1);
        writeDecimalAsPacked(position, iasMggSin.copy());
    }

    /**Original name: WTLI-IAS-MGG-SIN<br>*/
    public AfDecimal getIasMggSin(int iasMggSinIdx) {
        int position = Pos.wtliIasMggSin(iasMggSinIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IAS_MGG_SIN, Len.Fract.IAS_MGG_SIN);
    }

    public void setIasRstDpst(int iasRstDpstIdx, AfDecimal iasRstDpst) {
        int position = Pos.wtliIasRstDpst(iasRstDpstIdx - 1);
        writeDecimalAsPacked(position, iasRstDpst.copy());
    }

    /**Original name: WTLI-IAS-RST-DPST<br>*/
    public AfDecimal getIasRstDpst(int iasRstDpstIdx) {
        int position = Pos.wtliIasRstDpst(iasRstDpstIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IAS_RST_DPST, Len.Fract.IAS_RST_DPST);
    }

    public void setImpRimb(int impRimbIdx, AfDecimal impRimb) {
        int position = Pos.wtliImpRimb(impRimbIdx - 1);
        writeDecimalAsPacked(position, impRimb.copy());
    }

    /**Original name: WTLI-IMP-RIMB<br>*/
    public AfDecimal getImpRimb(int impRimbIdx) {
        int position = Pos.wtliImpRimb(impRimbIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_RIMB, Len.Fract.IMP_RIMB);
    }

    public void setComponTaxRimb(int componTaxRimbIdx, AfDecimal componTaxRimb) {
        int position = Pos.wtliComponTaxRimb(componTaxRimbIdx - 1);
        writeDecimalAsPacked(position, componTaxRimb.copy());
    }

    /**Original name: WTLI-COMPON-TAX-RIMB<br>*/
    public AfDecimal getComponTaxRimb(int componTaxRimbIdx) {
        int position = Pos.wtliComponTaxRimb(componTaxRimbIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMPON_TAX_RIMB, Len.Fract.COMPON_TAX_RIMB);
    }

    public void setRisMat(int risMatIdx, AfDecimal risMat) {
        int position = Pos.wtliRisMat(risMatIdx - 1);
        writeDecimalAsPacked(position, risMat.copy());
    }

    /**Original name: WTLI-RIS-MAT<br>*/
    public AfDecimal getRisMat(int risMatIdx) {
        int position = Pos.wtliRisMat(risMatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_MAT, Len.Fract.RIS_MAT);
    }

    public void setRisSpe(int risSpeIdx, AfDecimal risSpe) {
        int position = Pos.wtliRisSpe(risSpeIdx - 1);
        writeDecimalAsPacked(position, risSpe.copy());
    }

    /**Original name: WTLI-RIS-SPE<br>*/
    public AfDecimal getRisSpe(int risSpeIdx) {
        int position = Pos.wtliRisSpe(risSpeIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_SPE, Len.Fract.RIS_SPE);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wtliDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WTLI-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wtliDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wtliDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WTLI-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wtliDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wtliDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WTLI-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wtliDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wtliDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WTLI-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wtliDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wtliDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WTLI-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wtliDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wtliDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WTLI-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wtliDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wtliDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WTLI-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wtliDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setIasPnl(int iasPnlIdx, AfDecimal iasPnl) {
        int position = Pos.wtliIasPnl(iasPnlIdx - 1);
        writeDecimalAsPacked(position, iasPnl.copy());
    }

    /**Original name: WTLI-IAS-PNL<br>*/
    public AfDecimal getIasPnl(int iasPnlIdx) {
        int position = Pos.wtliIasPnl(iasPnlIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IAS_PNL, Len.Fract.IAS_PNL);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WTLI-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_TAB = 1;
        public static final int WTLI_TAB_R = 1;
        public static final int FLR1 = WTLI_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wtliTabTrchLiq(int idx) {
            return WTLI_TAB + idx * Len.TAB_TRCH_LIQ;
        }

        public static int wtliStatus(int idx) {
            return wtliTabTrchLiq(idx);
        }

        public static int wtliIdPtf(int idx) {
            return wtliStatus(idx) + Len.STATUS;
        }

        public static int wtliDati(int idx) {
            return wtliIdPtf(idx) + Len.ID_PTF;
        }

        public static int wtliIdTrchLiq(int idx) {
            return wtliDati(idx);
        }

        public static int wtliIdGarLiq(int idx) {
            return wtliIdTrchLiq(idx) + Len.ID_TRCH_LIQ;
        }

        public static int wtliIdGarLiqNull(int idx) {
            return wtliIdGarLiq(idx);
        }

        public static int wtliIdLiq(int idx) {
            return wtliIdGarLiq(idx) + Len.ID_GAR_LIQ;
        }

        public static int wtliIdTrchDiGar(int idx) {
            return wtliIdLiq(idx) + Len.ID_LIQ;
        }

        public static int wtliIdMoviCrz(int idx) {
            return wtliIdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }

        public static int wtliIdMoviChiu(int idx) {
            return wtliIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wtliIdMoviChiuNull(int idx) {
            return wtliIdMoviChiu(idx);
        }

        public static int wtliDtIniEff(int idx) {
            return wtliIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wtliDtEndEff(int idx) {
            return wtliDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wtliCodCompAnia(int idx) {
            return wtliDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wtliImpLrdCalc(int idx) {
            return wtliCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wtliImpLrdDfz(int idx) {
            return wtliImpLrdCalc(idx) + Len.IMP_LRD_CALC;
        }

        public static int wtliImpLrdDfzNull(int idx) {
            return wtliImpLrdDfz(idx);
        }

        public static int wtliImpLrdEfflq(int idx) {
            return wtliImpLrdDfz(idx) + Len.IMP_LRD_DFZ;
        }

        public static int wtliImpNetCalc(int idx) {
            return wtliImpLrdEfflq(idx) + Len.IMP_LRD_EFFLQ;
        }

        public static int wtliImpNetDfz(int idx) {
            return wtliImpNetCalc(idx) + Len.IMP_NET_CALC;
        }

        public static int wtliImpNetDfzNull(int idx) {
            return wtliImpNetDfz(idx);
        }

        public static int wtliImpNetEfflq(int idx) {
            return wtliImpNetDfz(idx) + Len.IMP_NET_DFZ;
        }

        public static int wtliImpUti(int idx) {
            return wtliImpNetEfflq(idx) + Len.IMP_NET_EFFLQ;
        }

        public static int wtliImpUtiNull(int idx) {
            return wtliImpUti(idx);
        }

        public static int wtliCodTari(int idx) {
            return wtliImpUti(idx) + Len.IMP_UTI;
        }

        public static int wtliCodTariNull(int idx) {
            return wtliCodTari(idx);
        }

        public static int wtliCodDvs(int idx) {
            return wtliCodTari(idx) + Len.COD_TARI;
        }

        public static int wtliCodDvsNull(int idx) {
            return wtliCodDvs(idx);
        }

        public static int wtliIasOnerPrvntFin(int idx) {
            return wtliCodDvs(idx) + Len.COD_DVS;
        }

        public static int wtliIasOnerPrvntFinNull(int idx) {
            return wtliIasOnerPrvntFin(idx);
        }

        public static int wtliIasMggSin(int idx) {
            return wtliIasOnerPrvntFin(idx) + Len.IAS_ONER_PRVNT_FIN;
        }

        public static int wtliIasMggSinNull(int idx) {
            return wtliIasMggSin(idx);
        }

        public static int wtliIasRstDpst(int idx) {
            return wtliIasMggSin(idx) + Len.IAS_MGG_SIN;
        }

        public static int wtliIasRstDpstNull(int idx) {
            return wtliIasRstDpst(idx);
        }

        public static int wtliImpRimb(int idx) {
            return wtliIasRstDpst(idx) + Len.IAS_RST_DPST;
        }

        public static int wtliImpRimbNull(int idx) {
            return wtliImpRimb(idx);
        }

        public static int wtliComponTaxRimb(int idx) {
            return wtliImpRimb(idx) + Len.IMP_RIMB;
        }

        public static int wtliComponTaxRimbNull(int idx) {
            return wtliComponTaxRimb(idx);
        }

        public static int wtliRisMat(int idx) {
            return wtliComponTaxRimb(idx) + Len.COMPON_TAX_RIMB;
        }

        public static int wtliRisMatNull(int idx) {
            return wtliRisMat(idx);
        }

        public static int wtliRisSpe(int idx) {
            return wtliRisMat(idx) + Len.RIS_MAT;
        }

        public static int wtliRisSpeNull(int idx) {
            return wtliRisSpe(idx);
        }

        public static int wtliDsRiga(int idx) {
            return wtliRisSpe(idx) + Len.RIS_SPE;
        }

        public static int wtliDsOperSql(int idx) {
            return wtliDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wtliDsVer(int idx) {
            return wtliDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wtliDsTsIniCptz(int idx) {
            return wtliDsVer(idx) + Len.DS_VER;
        }

        public static int wtliDsTsEndCptz(int idx) {
            return wtliDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wtliDsUtente(int idx) {
            return wtliDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wtliDsStatoElab(int idx) {
            return wtliDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int wtliIasPnl(int idx) {
            return wtliDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int wtliIasPnlNull(int idx) {
            return wtliIasPnl(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_TRCH_LIQ = 5;
        public static final int ID_GAR_LIQ = 5;
        public static final int ID_LIQ = 5;
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int IMP_LRD_CALC = 8;
        public static final int IMP_LRD_DFZ = 8;
        public static final int IMP_LRD_EFFLQ = 8;
        public static final int IMP_NET_CALC = 8;
        public static final int IMP_NET_DFZ = 8;
        public static final int IMP_NET_EFFLQ = 8;
        public static final int IMP_UTI = 8;
        public static final int COD_TARI = 12;
        public static final int COD_DVS = 20;
        public static final int IAS_ONER_PRVNT_FIN = 8;
        public static final int IAS_MGG_SIN = 8;
        public static final int IAS_RST_DPST = 8;
        public static final int IMP_RIMB = 8;
        public static final int COMPON_TAX_RIMB = 8;
        public static final int RIS_MAT = 8;
        public static final int RIS_SPE = 8;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int IAS_PNL = 8;
        public static final int DATI = ID_TRCH_LIQ + ID_GAR_LIQ + ID_LIQ + ID_TRCH_DI_GAR + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + IMP_LRD_CALC + IMP_LRD_DFZ + IMP_LRD_EFFLQ + IMP_NET_CALC + IMP_NET_DFZ + IMP_NET_EFFLQ + IMP_UTI + COD_TARI + COD_DVS + IAS_ONER_PRVNT_FIN + IAS_MGG_SIN + IAS_RST_DPST + IMP_RIMB + COMPON_TAX_RIMB + RIS_MAT + RIS_SPE + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + IAS_PNL;
        public static final int TAB_TRCH_LIQ = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 254;
        public static final int WTLI_TAB = WtliTab.TAB_TRCH_LIQ_MAXOCCURS * TAB_TRCH_LIQ;
        public static final int RESTO_TAB = 317246;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_TRCH_LIQ = 9;
            public static final int ID_GAR_LIQ = 9;
            public static final int ID_LIQ = 9;
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int IMP_LRD_CALC = 12;
            public static final int IMP_LRD_DFZ = 12;
            public static final int IMP_LRD_EFFLQ = 12;
            public static final int IMP_NET_CALC = 12;
            public static final int IMP_NET_DFZ = 12;
            public static final int IMP_NET_EFFLQ = 12;
            public static final int IMP_UTI = 12;
            public static final int IAS_ONER_PRVNT_FIN = 12;
            public static final int IAS_MGG_SIN = 12;
            public static final int IAS_RST_DPST = 12;
            public static final int IMP_RIMB = 12;
            public static final int COMPON_TAX_RIMB = 12;
            public static final int RIS_MAT = 12;
            public static final int RIS_SPE = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int IAS_PNL = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMP_LRD_CALC = 3;
            public static final int IMP_LRD_DFZ = 3;
            public static final int IMP_LRD_EFFLQ = 3;
            public static final int IMP_NET_CALC = 3;
            public static final int IMP_NET_DFZ = 3;
            public static final int IMP_NET_EFFLQ = 3;
            public static final int IMP_UTI = 3;
            public static final int IAS_ONER_PRVNT_FIN = 3;
            public static final int IAS_MGG_SIN = 3;
            public static final int IAS_RST_DPST = 3;
            public static final int IMP_RIMB = 3;
            public static final int COMPON_TAX_RIMB = 3;
            public static final int RIS_MAT = 3;
            public static final int RIS_SPE = 3;
            public static final int IAS_PNL = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
