package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Lvvs0032;
import it.accenture.jnais.copy.Lccvade1Lvvs0010;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.ws.enums.FlagStrutturaTrovata;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0032<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0032Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     PROGRAMMI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0032";
    //Original name: PGM-LVVS0000
    private String pgmLvvs0000 = "LVVS0000";
    //Original name: WK-DATA-INPUT
    private WkDataInput wkDataInput = new WkDataInput();
    //Original name: IND-STR
    private short indStr = DefaultValues.BIN_SHORT_VAL;
    //Original name: ALIAS-POLI
    private String aliasPoli = "POL";
    //Original name: FLAG-STRUTTURA-TROVATA
    private FlagStrutturaTrovata flagStrutturaTrovata = new FlagStrutturaTrovata();
    //Original name: DADE-ELE-ADES-MAX
    private short dadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1Lvvs0010 lccvade1 = new Lccvade1Lvvs0010();
    //Original name: INPUT-LVVS0000
    private InputLvvs0000 inputLvvs0000 = new InputLvvs0000();
    //Original name: IVVC0218
    private Ivvc0218Lvvs0032 ivvc0218 = new Ivvc0218Lvvs0032();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getPgmLvvs0000() {
        return this.pgmLvvs0000;
    }

    public void setIndStr(short indStr) {
        this.indStr = indStr;
    }

    public short getIndStr() {
        return this.indStr;
    }

    public String getAliasPoli() {
        return this.aliasPoli;
    }

    public String getAliasPoliFormatted() {
        return Functions.padBlanks(getAliasPoli(), Len.ALIAS_POLI);
    }

    public void setDadeAreaAdesFormatted(String data) {
        byte[] buffer = new byte[Len.DADE_AREA_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.DADE_AREA_ADES);
        setDadeAreaAdesBytes(buffer, 1);
    }

    public void setDadeAreaAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDadeTabAdesBytes(buffer, position);
    }

    public void setDadeEleAdesMax(short dadeEleAdesMax) {
        this.dadeEleAdesMax = dadeEleAdesMax;
    }

    public short getDadeEleAdesMax() {
        return this.dadeEleAdesMax;
    }

    public void setDadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1Lvvs0010.Len.Int.ID_PTF, 0));
        position += Lccvade1Lvvs0010.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public FlagStrutturaTrovata getFlagStrutturaTrovata() {
        return flagStrutturaTrovata;
    }

    public InputLvvs0000 getInputLvvs0000() {
        return inputLvvs0000;
    }

    public Ivvc0218Lvvs0032 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvade1Lvvs0010 getLccvade1() {
        return lccvade1;
    }

    public WkDataInput getWkDataInput() {
        return wkDataInput;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ALIAS_POLI = 3;
        public static final int DADE_ELE_ADES_MAX = 2;
        public static final int DADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1Lvvs0010.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int DADE_AREA_ADES = DADE_ELE_ADES_MAX + DADE_TAB_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
