package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbvc821Ades;
import it.accenture.jnais.copy.Ldbvc821Poli;
import it.accenture.jnais.copy.Ldbvc821StatOggBus;

/**Original name: LDBVC821<br>
 * Variable: LDBVC821 from copybook LDBVC821<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbvc821 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LC821-ID-POLI
    private int lc821IdPoli = DefaultValues.INT_VAL;
    //Original name: LC821-TP-FRM-ASSVA1
    private String lc821TpFrmAssva1 = DefaultValues.stringVal(Len.LC821_TP_FRM_ASSVA1);
    //Original name: LC821-TP-FRM-ASSVA2
    private String lc821TpFrmAssva2 = DefaultValues.stringVal(Len.LC821_TP_FRM_ASSVA2);
    //Original name: LDBVC821-ADES
    private Ldbvc821Ades ldbvc821Ades = new Ldbvc821Ades();
    //Original name: LDBVC821-POLI
    private Ldbvc821Poli ldbvc821Poli = new Ldbvc821Poli();
    //Original name: LDBVC821-STAT-OGG-BUS
    private Ldbvc821StatOggBus ldbvc821StatOggBus = new Ldbvc821StatOggBus();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVC821;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbvc821Bytes(buf);
    }

    public void setLdbvc821Bytes(byte[] buffer) {
        setLdbvc821Bytes(buffer, 1);
    }

    public byte[] getLdbvc821Bytes() {
        byte[] buffer = new byte[Len.LDBVC821];
        return getLdbvc821Bytes(buffer, 1);
    }

    public void setLdbvc821Bytes(byte[] buffer, int offset) {
        int position = offset;
        setLdbvc821InputBytes(buffer, position);
        position += Len.LDBVC821_INPUT;
        ldbvc821Ades.setLdbvc821AdesBytes(buffer, position);
        position += Ldbvc821Ades.Len.LDBVC821_ADES;
        ldbvc821Poli.setLdbvc821PoliBytes(buffer, position);
        position += Ldbvc821Poli.Len.LDBVC821_POLI;
        ldbvc821StatOggBus.setLdbvc821StatOggBusBytes(buffer, position);
    }

    public byte[] getLdbvc821Bytes(byte[] buffer, int offset) {
        int position = offset;
        getLdbvc821InputBytes(buffer, position);
        position += Len.LDBVC821_INPUT;
        ldbvc821Ades.getLdbvc821AdesBytes(buffer, position);
        position += Ldbvc821Ades.Len.LDBVC821_ADES;
        ldbvc821Poli.getLdbvc821PoliBytes(buffer, position);
        position += Ldbvc821Poli.Len.LDBVC821_POLI;
        ldbvc821StatOggBus.getLdbvc821StatOggBusBytes(buffer, position);
        return buffer;
    }

    public void setLdbvc821InputBytes(byte[] buffer, int offset) {
        int position = offset;
        lc821IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.LC821_ID_POLI, 0);
        position += Len.LC821_ID_POLI;
        lc821TpFrmAssva1 = MarshalByte.readString(buffer, position, Len.LC821_TP_FRM_ASSVA1);
        position += Len.LC821_TP_FRM_ASSVA1;
        lc821TpFrmAssva2 = MarshalByte.readString(buffer, position, Len.LC821_TP_FRM_ASSVA2);
    }

    public byte[] getLdbvc821InputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, lc821IdPoli, Len.Int.LC821_ID_POLI, 0);
        position += Len.LC821_ID_POLI;
        MarshalByte.writeString(buffer, position, lc821TpFrmAssva1, Len.LC821_TP_FRM_ASSVA1);
        position += Len.LC821_TP_FRM_ASSVA1;
        MarshalByte.writeString(buffer, position, lc821TpFrmAssva2, Len.LC821_TP_FRM_ASSVA2);
        return buffer;
    }

    public void setLc821IdPoli(int lc821IdPoli) {
        this.lc821IdPoli = lc821IdPoli;
    }

    public int getLc821IdPoli() {
        return this.lc821IdPoli;
    }

    public void setLc821TpFrmAssva1(String lc821TpFrmAssva1) {
        this.lc821TpFrmAssva1 = Functions.subString(lc821TpFrmAssva1, Len.LC821_TP_FRM_ASSVA1);
    }

    public String getLc821TpFrmAssva1() {
        return this.lc821TpFrmAssva1;
    }

    public void setLc821TpFrmAssva2(String lc821TpFrmAssva2) {
        this.lc821TpFrmAssva2 = Functions.subString(lc821TpFrmAssva2, Len.LC821_TP_FRM_ASSVA2);
    }

    public String getLc821TpFrmAssva2() {
        return this.lc821TpFrmAssva2;
    }

    public Ldbvc821Ades getLdbvc821Ades() {
        return ldbvc821Ades;
    }

    public Ldbvc821Poli getLdbvc821Poli() {
        return ldbvc821Poli;
    }

    public Ldbvc821StatOggBus getLdbvc821StatOggBus() {
        return ldbvc821StatOggBus;
    }

    @Override
    public byte[] serialize() {
        return getLdbvc821Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int LC821_ID_POLI = 5;
        public static final int LC821_TP_FRM_ASSVA1 = 2;
        public static final int LC821_TP_FRM_ASSVA2 = 2;
        public static final int LDBVC821_INPUT = LC821_ID_POLI + LC821_TP_FRM_ASSVA1 + LC821_TP_FRM_ASSVA2;
        public static final int LDBVC821 = LDBVC821_INPUT + Ldbvc821Ades.Len.LDBVC821_ADES + Ldbvc821Poli.Len.LDBVC821_POLI + Ldbvc821StatOggBus.Len.LDBVC821_STAT_OGG_BUS;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LC821_ID_POLI = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
