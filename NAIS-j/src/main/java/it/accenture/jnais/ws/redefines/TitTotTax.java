package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-TAX<br>
 * Variable: TIT-TOT-TAX from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotTax extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotTax() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_TAX;
    }

    public void setTitTotTax(AfDecimal titTotTax) {
        writeDecimalAsPacked(Pos.TIT_TOT_TAX, titTotTax.copy());
    }

    public void setTitTotTaxFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_TAX, Pos.TIT_TOT_TAX);
    }

    /**Original name: TIT-TOT-TAX<br>*/
    public AfDecimal getTitTotTax() {
        return readPackedAsDecimal(Pos.TIT_TOT_TAX, Len.Int.TIT_TOT_TAX, Len.Fract.TIT_TOT_TAX);
    }

    public byte[] getTitTotTaxAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_TAX, Pos.TIT_TOT_TAX);
        return buffer;
    }

    public void setTitTotTaxNull(String titTotTaxNull) {
        writeString(Pos.TIT_TOT_TAX_NULL, titTotTaxNull, Len.TIT_TOT_TAX_NULL);
    }

    /**Original name: TIT-TOT-TAX-NULL<br>*/
    public String getTitTotTaxNull() {
        return readString(Pos.TIT_TOT_TAX_NULL, Len.TIT_TOT_TAX_NULL);
    }

    public String getTitTotTaxNullFormatted() {
        return Functions.padBlanks(getTitTotTaxNull(), Len.TIT_TOT_TAX_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_TAX = 1;
        public static final int TIT_TOT_TAX_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_TAX = 8;
        public static final int TIT_TOT_TAX_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_TAX = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_TAX = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
