package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRSTZ-RID-INI<br>
 * Variable: WTGA-PRSTZ-RID-INI from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrstzRidIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrstzRidIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRSTZ_RID_INI;
    }

    public void setWtgaPrstzRidIni(AfDecimal wtgaPrstzRidIni) {
        writeDecimalAsPacked(Pos.WTGA_PRSTZ_RID_INI, wtgaPrstzRidIni.copy());
    }

    public void setWtgaPrstzRidIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRSTZ_RID_INI, Pos.WTGA_PRSTZ_RID_INI);
    }

    /**Original name: WTGA-PRSTZ-RID-INI<br>*/
    public AfDecimal getWtgaPrstzRidIni() {
        return readPackedAsDecimal(Pos.WTGA_PRSTZ_RID_INI, Len.Int.WTGA_PRSTZ_RID_INI, Len.Fract.WTGA_PRSTZ_RID_INI);
    }

    public byte[] getWtgaPrstzRidIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRSTZ_RID_INI, Pos.WTGA_PRSTZ_RID_INI);
        return buffer;
    }

    public void initWtgaPrstzRidIniSpaces() {
        fill(Pos.WTGA_PRSTZ_RID_INI, Len.WTGA_PRSTZ_RID_INI, Types.SPACE_CHAR);
    }

    public void setWtgaPrstzRidIniNull(String wtgaPrstzRidIniNull) {
        writeString(Pos.WTGA_PRSTZ_RID_INI_NULL, wtgaPrstzRidIniNull, Len.WTGA_PRSTZ_RID_INI_NULL);
    }

    /**Original name: WTGA-PRSTZ-RID-INI-NULL<br>*/
    public String getWtgaPrstzRidIniNull() {
        return readString(Pos.WTGA_PRSTZ_RID_INI_NULL, Len.WTGA_PRSTZ_RID_INI_NULL);
    }

    public String getWtgaPrstzRidIniNullFormatted() {
        return Functions.padBlanks(getWtgaPrstzRidIniNull(), Len.WTGA_PRSTZ_RID_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_RID_INI = 1;
        public static final int WTGA_PRSTZ_RID_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRSTZ_RID_INI = 8;
        public static final int WTGA_PRSTZ_RID_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_RID_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRSTZ_RID_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
