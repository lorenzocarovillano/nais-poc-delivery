package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-PROV-INC<br>
 * Variable: B03-PROV-INC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03ProvInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03ProvInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_PROV_INC;
    }

    public void setB03ProvInc(AfDecimal b03ProvInc) {
        writeDecimalAsPacked(Pos.B03_PROV_INC, b03ProvInc.copy());
    }

    public void setB03ProvIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_PROV_INC, Pos.B03_PROV_INC);
    }

    /**Original name: B03-PROV-INC<br>*/
    public AfDecimal getB03ProvInc() {
        return readPackedAsDecimal(Pos.B03_PROV_INC, Len.Int.B03_PROV_INC, Len.Fract.B03_PROV_INC);
    }

    public byte[] getB03ProvIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_PROV_INC, Pos.B03_PROV_INC);
        return buffer;
    }

    public void setB03ProvIncNull(String b03ProvIncNull) {
        writeString(Pos.B03_PROV_INC_NULL, b03ProvIncNull, Len.B03_PROV_INC_NULL);
    }

    /**Original name: B03-PROV-INC-NULL<br>*/
    public String getB03ProvIncNull() {
        return readString(Pos.B03_PROV_INC_NULL, Len.B03_PROV_INC_NULL);
    }

    public String getB03ProvIncNullFormatted() {
        return Functions.padBlanks(getB03ProvIncNull(), Len.B03_PROV_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_PROV_INC = 1;
        public static final int B03_PROV_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_PROV_INC = 8;
        public static final int B03_PROV_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_PROV_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_PROV_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
