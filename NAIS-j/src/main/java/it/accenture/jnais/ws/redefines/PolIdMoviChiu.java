package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: POL-ID-MOVI-CHIU<br>
 * Variable: POL-ID-MOVI-CHIU from program LCCS0025<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PolIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PolIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POL_ID_MOVI_CHIU;
    }

    public void setPolIdMoviChiu(int polIdMoviChiu) {
        writeIntAsPacked(Pos.POL_ID_MOVI_CHIU, polIdMoviChiu, Len.Int.POL_ID_MOVI_CHIU);
    }

    public void setPolIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POL_ID_MOVI_CHIU, Pos.POL_ID_MOVI_CHIU);
    }

    /**Original name: POL-ID-MOVI-CHIU<br>*/
    public int getPolIdMoviChiu() {
        return readPackedAsInt(Pos.POL_ID_MOVI_CHIU, Len.Int.POL_ID_MOVI_CHIU);
    }

    public byte[] getPolIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POL_ID_MOVI_CHIU, Pos.POL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setPolIdMoviChiuNull(String polIdMoviChiuNull) {
        writeString(Pos.POL_ID_MOVI_CHIU_NULL, polIdMoviChiuNull, Len.POL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: POL-ID-MOVI-CHIU-NULL<br>*/
    public String getPolIdMoviChiuNull() {
        return readString(Pos.POL_ID_MOVI_CHIU_NULL, Len.POL_ID_MOVI_CHIU_NULL);
    }

    public String getPolIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getPolIdMoviChiuNull(), Len.POL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POL_ID_MOVI_CHIU = 1;
        public static final int POL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POL_ID_MOVI_CHIU = 5;
        public static final int POL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
