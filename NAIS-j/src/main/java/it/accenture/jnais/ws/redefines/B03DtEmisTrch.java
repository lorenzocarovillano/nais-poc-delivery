package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-EMIS-TRCH<br>
 * Variable: B03-DT-EMIS-TRCH from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtEmisTrch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtEmisTrch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_EMIS_TRCH;
    }

    public void setB03DtEmisTrch(int b03DtEmisTrch) {
        writeIntAsPacked(Pos.B03_DT_EMIS_TRCH, b03DtEmisTrch, Len.Int.B03_DT_EMIS_TRCH);
    }

    public void setB03DtEmisTrchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_EMIS_TRCH, Pos.B03_DT_EMIS_TRCH);
    }

    /**Original name: B03-DT-EMIS-TRCH<br>*/
    public int getB03DtEmisTrch() {
        return readPackedAsInt(Pos.B03_DT_EMIS_TRCH, Len.Int.B03_DT_EMIS_TRCH);
    }

    public byte[] getB03DtEmisTrchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_EMIS_TRCH, Pos.B03_DT_EMIS_TRCH);
        return buffer;
    }

    public void setB03DtEmisTrchNull(String b03DtEmisTrchNull) {
        writeString(Pos.B03_DT_EMIS_TRCH_NULL, b03DtEmisTrchNull, Len.B03_DT_EMIS_TRCH_NULL);
    }

    /**Original name: B03-DT-EMIS-TRCH-NULL<br>*/
    public String getB03DtEmisTrchNull() {
        return readString(Pos.B03_DT_EMIS_TRCH_NULL, Len.B03_DT_EMIS_TRCH_NULL);
    }

    public String getB03DtEmisTrchNullFormatted() {
        return Functions.padBlanks(getB03DtEmisTrchNull(), Len.B03_DT_EMIS_TRCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_EMIS_TRCH = 1;
        public static final int B03_DT_EMIS_TRCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_EMIS_TRCH = 5;
        public static final int B03_DT_EMIS_TRCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_EMIS_TRCH = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
