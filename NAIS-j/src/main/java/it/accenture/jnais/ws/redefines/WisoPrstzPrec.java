package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WISO-PRSTZ-PREC<br>
 * Variable: WISO-PRSTZ-PREC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoPrstzPrec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoPrstzPrec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_PRSTZ_PREC;
    }

    public void setWisoPrstzPrec(AfDecimal wisoPrstzPrec) {
        writeDecimalAsPacked(Pos.WISO_PRSTZ_PREC, wisoPrstzPrec.copy());
    }

    public void setWisoPrstzPrecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_PRSTZ_PREC, Pos.WISO_PRSTZ_PREC);
    }

    /**Original name: WISO-PRSTZ-PREC<br>*/
    public AfDecimal getWisoPrstzPrec() {
        return readPackedAsDecimal(Pos.WISO_PRSTZ_PREC, Len.Int.WISO_PRSTZ_PREC, Len.Fract.WISO_PRSTZ_PREC);
    }

    public byte[] getWisoPrstzPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_PRSTZ_PREC, Pos.WISO_PRSTZ_PREC);
        return buffer;
    }

    public void initWisoPrstzPrecSpaces() {
        fill(Pos.WISO_PRSTZ_PREC, Len.WISO_PRSTZ_PREC, Types.SPACE_CHAR);
    }

    public void setWisoPrstzPrecNull(String wisoPrstzPrecNull) {
        writeString(Pos.WISO_PRSTZ_PREC_NULL, wisoPrstzPrecNull, Len.WISO_PRSTZ_PREC_NULL);
    }

    /**Original name: WISO-PRSTZ-PREC-NULL<br>*/
    public String getWisoPrstzPrecNull() {
        return readString(Pos.WISO_PRSTZ_PREC_NULL, Len.WISO_PRSTZ_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_PRSTZ_PREC = 1;
        public static final int WISO_PRSTZ_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_PRSTZ_PREC = 8;
        public static final int WISO_PRSTZ_PREC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WISO_PRSTZ_PREC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_PRSTZ_PREC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
