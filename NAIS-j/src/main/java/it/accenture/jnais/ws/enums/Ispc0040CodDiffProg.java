package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-COD-DIFF-PROG<br>
 * Variable: ISPC0040-COD-DIFF-PROG from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040CodDiffProg {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.COD_DIFF_PROG);
    public static final String DIFFERIMENTO = "05";
    public static final String PROROGA = "06";

    //==== METHODS ====
    public void setCodDiffProg(String codDiffProg) {
        this.value = Functions.subString(codDiffProg, Len.COD_DIFF_PROG);
    }

    public String getCodDiffProg() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_DIFF_PROG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
