package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-FRAZ-DECR-CPT<br>
 * Variable: WB03-FRAZ-DECR-CPT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03FrazDecrCpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03FrazDecrCpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_FRAZ_DECR_CPT;
    }

    public void setWb03FrazDecrCpt(int wb03FrazDecrCpt) {
        writeIntAsPacked(Pos.WB03_FRAZ_DECR_CPT, wb03FrazDecrCpt, Len.Int.WB03_FRAZ_DECR_CPT);
    }

    public void setWb03FrazDecrCptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_FRAZ_DECR_CPT, Pos.WB03_FRAZ_DECR_CPT);
    }

    /**Original name: WB03-FRAZ-DECR-CPT<br>*/
    public int getWb03FrazDecrCpt() {
        return readPackedAsInt(Pos.WB03_FRAZ_DECR_CPT, Len.Int.WB03_FRAZ_DECR_CPT);
    }

    public byte[] getWb03FrazDecrCptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_FRAZ_DECR_CPT, Pos.WB03_FRAZ_DECR_CPT);
        return buffer;
    }

    public void setWb03FrazDecrCptNull(String wb03FrazDecrCptNull) {
        writeString(Pos.WB03_FRAZ_DECR_CPT_NULL, wb03FrazDecrCptNull, Len.WB03_FRAZ_DECR_CPT_NULL);
    }

    /**Original name: WB03-FRAZ-DECR-CPT-NULL<br>*/
    public String getWb03FrazDecrCptNull() {
        return readString(Pos.WB03_FRAZ_DECR_CPT_NULL, Len.WB03_FRAZ_DECR_CPT_NULL);
    }

    public String getWb03FrazDecrCptNullFormatted() {
        return Functions.padBlanks(getWb03FrazDecrCptNull(), Len.WB03_FRAZ_DECR_CPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_FRAZ_DECR_CPT = 1;
        public static final int WB03_FRAZ_DECR_CPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_FRAZ_DECR_CPT = 3;
        public static final int WB03_FRAZ_DECR_CPT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_FRAZ_DECR_CPT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
