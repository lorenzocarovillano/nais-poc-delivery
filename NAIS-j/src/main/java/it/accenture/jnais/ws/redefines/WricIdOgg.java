package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WRIC-ID-OGG<br>
 * Variable: WRIC-ID-OGG from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WricIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WricIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIC_ID_OGG;
    }

    public void setWricIdOgg(int wricIdOgg) {
        writeIntAsPacked(Pos.WRIC_ID_OGG, wricIdOgg, Len.Int.WRIC_ID_OGG);
    }

    public void setWricIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIC_ID_OGG, Pos.WRIC_ID_OGG);
    }

    /**Original name: WRIC-ID-OGG<br>*/
    public int getWricIdOgg() {
        return readPackedAsInt(Pos.WRIC_ID_OGG, Len.Int.WRIC_ID_OGG);
    }

    public byte[] getWricIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIC_ID_OGG, Pos.WRIC_ID_OGG);
        return buffer;
    }

    public void setWricIdOggNull(String wricIdOggNull) {
        writeString(Pos.WRIC_ID_OGG_NULL, wricIdOggNull, Len.WRIC_ID_OGG_NULL);
    }

    /**Original name: WRIC-ID-OGG-NULL<br>*/
    public String getWricIdOggNull() {
        return readString(Pos.WRIC_ID_OGG_NULL, Len.WRIC_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIC_ID_OGG = 1;
        public static final int WRIC_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIC_ID_OGG = 5;
        public static final int WRIC_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIC_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
