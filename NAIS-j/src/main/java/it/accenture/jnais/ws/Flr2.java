package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FILLER-WS-6<br>
 * Variable: FILLER-WS-6 from program LCCS0003<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Flr2 {

    //==== PROPERTIES ====
    /**Original name: WS-PROD<br>
	 * <pre>-----------</pre>*/
    private int prod = DefaultValues.INT_VAL;
    //Original name: WS-COMO
    private int como = DefaultValues.INT_VAL;
    //Original name: WS-RESTOA
    private String restoa = DefaultValues.stringVal(Len.RESTOA);
    //Original name: WS-DELTB
    private String deltb = DefaultValues.stringVal(Len.DELTB);
    //Original name: WS-DELTAA
    private String deltaa = DefaultValues.stringVal(Len.DELTAA);

    //==== METHODS ====
    public void setProd(int prod) {
        this.prod = prod;
    }

    public int getProd() {
        return this.prod;
    }

    public void setComo(int como) {
        this.como = como;
    }

    public int getComo() {
        return this.como;
    }

    public void setRestoa(short restoa) {
        this.restoa = NumericDisplay.asString(restoa, Len.RESTOA);
    }

    public short getRestoa() {
        return NumericDisplay.asShort(this.restoa);
    }

    public String getRestoaFormatted() {
        return this.restoa;
    }

    public void setDeltb(short deltb) {
        this.deltb = NumericDisplay.asString(deltb, Len.DELTB);
    }

    public short getDeltb() {
        return NumericDisplay.asShort(this.deltb);
    }

    public String getDeltbFormatted() {
        return this.deltb;
    }

    public void setDeltaaFormatted(String deltaa) {
        this.deltaa = Trunc.toUnsignedNumeric(deltaa, Len.DELTAA);
    }

    public short getDeltaa() {
        return NumericDisplay.asShort(this.deltaa);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RESTOA = 2;
        public static final int DELTB = 2;
        public static final int DELTAA = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
