package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP58-IMPST-BOLLO-DETT-V<br>
 * Variable: WP58-IMPST-BOLLO-DETT-V from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp58ImpstBolloDettV extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp58ImpstBolloDettV() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP58_IMPST_BOLLO_DETT_V;
    }

    public void setWp58ImpstBolloDettV(AfDecimal wp58ImpstBolloDettV) {
        writeDecimalAsPacked(Pos.WP58_IMPST_BOLLO_DETT_V, wp58ImpstBolloDettV.copy());
    }

    public void setWp58ImpstBolloDettVFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP58_IMPST_BOLLO_DETT_V, Pos.WP58_IMPST_BOLLO_DETT_V);
    }

    /**Original name: WP58-IMPST-BOLLO-DETT-V<br>*/
    public AfDecimal getWp58ImpstBolloDettV() {
        return readPackedAsDecimal(Pos.WP58_IMPST_BOLLO_DETT_V, Len.Int.WP58_IMPST_BOLLO_DETT_V, Len.Fract.WP58_IMPST_BOLLO_DETT_V);
    }

    public byte[] getWp58ImpstBolloDettVAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP58_IMPST_BOLLO_DETT_V, Pos.WP58_IMPST_BOLLO_DETT_V);
        return buffer;
    }

    public void initWp58ImpstBolloDettVSpaces() {
        fill(Pos.WP58_IMPST_BOLLO_DETT_V, Len.WP58_IMPST_BOLLO_DETT_V, Types.SPACE_CHAR);
    }

    public void setDp58ImpstBolloDettVNull(String dp58ImpstBolloDettVNull) {
        writeString(Pos.WP58_IMPST_BOLLO_DETT_V_NULL, dp58ImpstBolloDettVNull, Len.DP58_IMPST_BOLLO_DETT_V_NULL);
    }

    /**Original name: DP58-IMPST-BOLLO-DETT-V-NULL<br>*/
    public String getDp58ImpstBolloDettVNull() {
        return readString(Pos.WP58_IMPST_BOLLO_DETT_V_NULL, Len.DP58_IMPST_BOLLO_DETT_V_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP58_IMPST_BOLLO_DETT_V = 1;
        public static final int WP58_IMPST_BOLLO_DETT_V_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP58_IMPST_BOLLO_DETT_V = 8;
        public static final int DP58_IMPST_BOLLO_DETT_V_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP58_IMPST_BOLLO_DETT_V = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP58_IMPST_BOLLO_DETT_V = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
