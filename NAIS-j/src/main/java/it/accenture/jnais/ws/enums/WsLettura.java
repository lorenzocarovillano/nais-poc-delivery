package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-LETTURA<br>
 * Variable: WS-LETTURA from program IDSS0160<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsLettura {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char PRIMA_LETTURA = 'P';
    public static final char SECONDA_LETTURA = 'S';

    //==== METHODS ====
    public void setWsLettura(char wsLettura) {
        this.value = wsLettura;
    }

    public char getWsLettura() {
        return this.value;
    }

    public boolean isPrimaLettura() {
        return value == PRIMA_LETTURA;
    }

    public void setPrimaLettura() {
        value = PRIMA_LETTURA;
    }

    public boolean isSecondaLettura() {
        return value == SECONDA_LETTURA;
    }

    public void setSecondaLettura() {
        value = SECONDA_LETTURA;
    }
}
