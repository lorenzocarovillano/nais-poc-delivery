package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-DT-END-ISTR<br>
 * Variable: LQU-DT-END-ISTR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquDtEndIstr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquDtEndIstr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_DT_END_ISTR;
    }

    public void setLquDtEndIstr(int lquDtEndIstr) {
        writeIntAsPacked(Pos.LQU_DT_END_ISTR, lquDtEndIstr, Len.Int.LQU_DT_END_ISTR);
    }

    public void setLquDtEndIstrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_DT_END_ISTR, Pos.LQU_DT_END_ISTR);
    }

    /**Original name: LQU-DT-END-ISTR<br>*/
    public int getLquDtEndIstr() {
        return readPackedAsInt(Pos.LQU_DT_END_ISTR, Len.Int.LQU_DT_END_ISTR);
    }

    public byte[] getLquDtEndIstrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_DT_END_ISTR, Pos.LQU_DT_END_ISTR);
        return buffer;
    }

    public void setLquDtEndIstrNull(String lquDtEndIstrNull) {
        writeString(Pos.LQU_DT_END_ISTR_NULL, lquDtEndIstrNull, Len.LQU_DT_END_ISTR_NULL);
    }

    /**Original name: LQU-DT-END-ISTR-NULL<br>*/
    public String getLquDtEndIstrNull() {
        return readString(Pos.LQU_DT_END_ISTR_NULL, Len.LQU_DT_END_ISTR_NULL);
    }

    public String getLquDtEndIstrNullFormatted() {
        return Functions.padBlanks(getLquDtEndIstrNull(), Len.LQU_DT_END_ISTR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_DT_END_ISTR = 1;
        public static final int LQU_DT_END_ISTR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_DT_END_ISTR = 5;
        public static final int LQU_DT_END_ISTR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_DT_END_ISTR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
