package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-PATTUITO<br>
 * Variable: WTGA-PRE-PATTUITO from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPrePattuito extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPrePattuito() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_PATTUITO;
    }

    public void setWtgaPrePattuito(AfDecimal wtgaPrePattuito) {
        writeDecimalAsPacked(Pos.WTGA_PRE_PATTUITO, wtgaPrePattuito.copy());
    }

    public void setWtgaPrePattuitoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_PATTUITO, Pos.WTGA_PRE_PATTUITO);
    }

    /**Original name: WTGA-PRE-PATTUITO<br>*/
    public AfDecimal getWtgaPrePattuito() {
        return readPackedAsDecimal(Pos.WTGA_PRE_PATTUITO, Len.Int.WTGA_PRE_PATTUITO, Len.Fract.WTGA_PRE_PATTUITO);
    }

    public byte[] getWtgaPrePattuitoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_PATTUITO, Pos.WTGA_PRE_PATTUITO);
        return buffer;
    }

    public void initWtgaPrePattuitoSpaces() {
        fill(Pos.WTGA_PRE_PATTUITO, Len.WTGA_PRE_PATTUITO, Types.SPACE_CHAR);
    }

    public void setWtgaPrePattuitoNull(String wtgaPrePattuitoNull) {
        writeString(Pos.WTGA_PRE_PATTUITO_NULL, wtgaPrePattuitoNull, Len.WTGA_PRE_PATTUITO_NULL);
    }

    /**Original name: WTGA-PRE-PATTUITO-NULL<br>*/
    public String getWtgaPrePattuitoNull() {
        return readString(Pos.WTGA_PRE_PATTUITO_NULL, Len.WTGA_PRE_PATTUITO_NULL);
    }

    public String getWtgaPrePattuitoNullFormatted() {
        return Functions.padBlanks(getWtgaPrePattuitoNull(), Len.WTGA_PRE_PATTUITO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_PATTUITO = 1;
        public static final int WTGA_PRE_PATTUITO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_PATTUITO = 8;
        public static final int WTGA_PRE_PATTUITO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_PATTUITO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_PATTUITO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
