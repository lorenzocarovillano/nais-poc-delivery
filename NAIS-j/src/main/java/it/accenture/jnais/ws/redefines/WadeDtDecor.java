package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WADE-DT-DECOR<br>
 * Variable: WADE-DT-DECOR from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_DECOR;
    }

    public void setWadeDtDecor(int wadeDtDecor) {
        writeIntAsPacked(Pos.WADE_DT_DECOR, wadeDtDecor, Len.Int.WADE_DT_DECOR);
    }

    public void setWadeDtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_DECOR, Pos.WADE_DT_DECOR);
    }

    /**Original name: WADE-DT-DECOR<br>*/
    public int getWadeDtDecor() {
        return readPackedAsInt(Pos.WADE_DT_DECOR, Len.Int.WADE_DT_DECOR);
    }

    public String getWadeDtDecorFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWadeDtDecor()).toString();
    }

    public byte[] getWadeDtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_DECOR, Pos.WADE_DT_DECOR);
        return buffer;
    }

    public void initWadeDtDecorSpaces() {
        fill(Pos.WADE_DT_DECOR, Len.WADE_DT_DECOR, Types.SPACE_CHAR);
    }

    public void setWadeDtDecorNull(String wadeDtDecorNull) {
        writeString(Pos.WADE_DT_DECOR_NULL, wadeDtDecorNull, Len.WADE_DT_DECOR_NULL);
    }

    /**Original name: WADE-DT-DECOR-NULL<br>*/
    public String getWadeDtDecorNull() {
        return readString(Pos.WADE_DT_DECOR_NULL, Len.WADE_DT_DECOR_NULL);
    }

    public String getWadeDtDecorNullFormatted() {
        return Functions.padBlanks(getWadeDtDecorNull(), Len.WADE_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_DECOR = 1;
        public static final int WADE_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_DECOR = 5;
        public static final int WADE_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
