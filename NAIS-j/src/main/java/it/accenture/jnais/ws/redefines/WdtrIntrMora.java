package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-INTR-MORA<br>
 * Variable: WDTR-INTR-MORA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_INTR_MORA;
    }

    public void setWdtrIntrMora(AfDecimal wdtrIntrMora) {
        writeDecimalAsPacked(Pos.WDTR_INTR_MORA, wdtrIntrMora.copy());
    }

    /**Original name: WDTR-INTR-MORA<br>*/
    public AfDecimal getWdtrIntrMora() {
        return readPackedAsDecimal(Pos.WDTR_INTR_MORA, Len.Int.WDTR_INTR_MORA, Len.Fract.WDTR_INTR_MORA);
    }

    public void setWdtrIntrMoraNull(String wdtrIntrMoraNull) {
        writeString(Pos.WDTR_INTR_MORA_NULL, wdtrIntrMoraNull, Len.WDTR_INTR_MORA_NULL);
    }

    /**Original name: WDTR-INTR-MORA-NULL<br>*/
    public String getWdtrIntrMoraNull() {
        return readString(Pos.WDTR_INTR_MORA_NULL, Len.WDTR_INTR_MORA_NULL);
    }

    public String getWdtrIntrMoraNullFormatted() {
        return Functions.padBlanks(getWdtrIntrMoraNull(), Len.WDTR_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_MORA = 1;
        public static final int WDTR_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_MORA = 8;
        public static final int WDTR_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
