package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WE12-DT-EMIS<br>
 * Variable: WE12-DT-EMIS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class We12DtEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public We12DtEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WE12_DT_EMIS;
    }

    public void setWe12DtEmis(int we12DtEmis) {
        writeIntAsPacked(Pos.WE12_DT_EMIS, we12DtEmis, Len.Int.WE12_DT_EMIS);
    }

    /**Original name: WE12-DT-EMIS<br>*/
    public int getWe12DtEmis() {
        return readPackedAsInt(Pos.WE12_DT_EMIS, Len.Int.WE12_DT_EMIS);
    }

    public byte[] getWe12DtEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WE12_DT_EMIS, Pos.WE12_DT_EMIS);
        return buffer;
    }

    public void setWe12DtEmisNull(String we12DtEmisNull) {
        writeString(Pos.WE12_DT_EMIS_NULL, we12DtEmisNull, Len.WE12_DT_EMIS_NULL);
    }

    /**Original name: WE12-DT-EMIS-NULL<br>*/
    public String getWe12DtEmisNull() {
        return readString(Pos.WE12_DT_EMIS_NULL, Len.WE12_DT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WE12_DT_EMIS = 1;
        public static final int WE12_DT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WE12_DT_EMIS = 5;
        public static final int WE12_DT_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WE12_DT_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
