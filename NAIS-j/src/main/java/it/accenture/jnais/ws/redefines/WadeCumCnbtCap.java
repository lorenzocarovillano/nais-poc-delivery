package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WADE-CUM-CNBT-CAP<br>
 * Variable: WADE-CUM-CNBT-CAP from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeCumCnbtCap extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeCumCnbtCap() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_CUM_CNBT_CAP;
    }

    public void setWadeCumCnbtCap(AfDecimal wadeCumCnbtCap) {
        writeDecimalAsPacked(Pos.WADE_CUM_CNBT_CAP, wadeCumCnbtCap.copy());
    }

    public void setWadeCumCnbtCapFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_CUM_CNBT_CAP, Pos.WADE_CUM_CNBT_CAP);
    }

    /**Original name: WADE-CUM-CNBT-CAP<br>*/
    public AfDecimal getWadeCumCnbtCap() {
        return readPackedAsDecimal(Pos.WADE_CUM_CNBT_CAP, Len.Int.WADE_CUM_CNBT_CAP, Len.Fract.WADE_CUM_CNBT_CAP);
    }

    public byte[] getWadeCumCnbtCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_CUM_CNBT_CAP, Pos.WADE_CUM_CNBT_CAP);
        return buffer;
    }

    public void initWadeCumCnbtCapSpaces() {
        fill(Pos.WADE_CUM_CNBT_CAP, Len.WADE_CUM_CNBT_CAP, Types.SPACE_CHAR);
    }

    public void setWadeCumCnbtCapNull(String wadeCumCnbtCapNull) {
        writeString(Pos.WADE_CUM_CNBT_CAP_NULL, wadeCumCnbtCapNull, Len.WADE_CUM_CNBT_CAP_NULL);
    }

    /**Original name: WADE-CUM-CNBT-CAP-NULL<br>*/
    public String getWadeCumCnbtCapNull() {
        return readString(Pos.WADE_CUM_CNBT_CAP_NULL, Len.WADE_CUM_CNBT_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_CUM_CNBT_CAP = 1;
        public static final int WADE_CUM_CNBT_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_CUM_CNBT_CAP = 8;
        public static final int WADE_CUM_CNBT_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WADE_CUM_CNBT_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_CUM_CNBT_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
