package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-INTR-RIT-PAG<br>
 * Variable: LQU-IMP-INTR-RIT-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpIntrRitPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpIntrRitPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_INTR_RIT_PAG;
    }

    public void setLquImpIntrRitPag(AfDecimal lquImpIntrRitPag) {
        writeDecimalAsPacked(Pos.LQU_IMP_INTR_RIT_PAG, lquImpIntrRitPag.copy());
    }

    public void setLquImpIntrRitPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_INTR_RIT_PAG, Pos.LQU_IMP_INTR_RIT_PAG);
    }

    /**Original name: LQU-IMP-INTR-RIT-PAG<br>*/
    public AfDecimal getLquImpIntrRitPag() {
        return readPackedAsDecimal(Pos.LQU_IMP_INTR_RIT_PAG, Len.Int.LQU_IMP_INTR_RIT_PAG, Len.Fract.LQU_IMP_INTR_RIT_PAG);
    }

    public byte[] getLquImpIntrRitPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_INTR_RIT_PAG, Pos.LQU_IMP_INTR_RIT_PAG);
        return buffer;
    }

    public void setLquImpIntrRitPagNull(String lquImpIntrRitPagNull) {
        writeString(Pos.LQU_IMP_INTR_RIT_PAG_NULL, lquImpIntrRitPagNull, Len.LQU_IMP_INTR_RIT_PAG_NULL);
    }

    /**Original name: LQU-IMP-INTR-RIT-PAG-NULL<br>*/
    public String getLquImpIntrRitPagNull() {
        return readString(Pos.LQU_IMP_INTR_RIT_PAG_NULL, Len.LQU_IMP_INTR_RIT_PAG_NULL);
    }

    public String getLquImpIntrRitPagNullFormatted() {
        return Functions.padBlanks(getLquImpIntrRitPagNull(), Len.LQU_IMP_INTR_RIT_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_INTR_RIT_PAG = 1;
        public static final int LQU_IMP_INTR_RIT_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_INTR_RIT_PAG = 8;
        public static final int LQU_IMP_INTR_RIT_PAG_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_INTR_RIT_PAG = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_INTR_RIT_PAG = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
