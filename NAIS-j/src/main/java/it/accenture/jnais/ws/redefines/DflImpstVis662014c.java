package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-662014C<br>
 * Variable: DFL-IMPST-VIS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVis662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVis662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS662014C;
    }

    public void setDflImpstVis662014c(AfDecimal dflImpstVis662014c) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS662014C, dflImpstVis662014c.copy());
    }

    public void setDflImpstVis662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS662014C, Pos.DFL_IMPST_VIS662014C);
    }

    /**Original name: DFL-IMPST-VIS-662014C<br>*/
    public AfDecimal getDflImpstVis662014c() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS662014C, Len.Int.DFL_IMPST_VIS662014C, Len.Fract.DFL_IMPST_VIS662014C);
    }

    public byte[] getDflImpstVis662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS662014C, Pos.DFL_IMPST_VIS662014C);
        return buffer;
    }

    public void setDflImpstVis662014cNull(String dflImpstVis662014cNull) {
        writeString(Pos.DFL_IMPST_VIS662014C_NULL, dflImpstVis662014cNull, Len.DFL_IMPST_VIS662014C_NULL);
    }

    /**Original name: DFL-IMPST-VIS-662014C-NULL<br>*/
    public String getDflImpstVis662014cNull() {
        return readString(Pos.DFL_IMPST_VIS662014C_NULL, Len.DFL_IMPST_VIS662014C_NULL);
    }

    public String getDflImpstVis662014cNullFormatted() {
        return Functions.padBlanks(getDflImpstVis662014cNull(), Len.DFL_IMPST_VIS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS662014C = 1;
        public static final int DFL_IMPST_VIS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS662014C = 8;
        public static final int DFL_IMPST_VIS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
