package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B05-PROG-SCHEDA-VALOR<br>
 * Variable: B05-PROG-SCHEDA-VALOR from program LLBS0250<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B05ProgSchedaValor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B05ProgSchedaValor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B05_PROG_SCHEDA_VALOR;
    }

    public void setB05ProgSchedaValor(int b05ProgSchedaValor) {
        writeIntAsPacked(Pos.B05_PROG_SCHEDA_VALOR, b05ProgSchedaValor, Len.Int.B05_PROG_SCHEDA_VALOR);
    }

    public void setB05ProgSchedaValorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B05_PROG_SCHEDA_VALOR, Pos.B05_PROG_SCHEDA_VALOR);
    }

    /**Original name: B05-PROG-SCHEDA-VALOR<br>*/
    public int getB05ProgSchedaValor() {
        return readPackedAsInt(Pos.B05_PROG_SCHEDA_VALOR, Len.Int.B05_PROG_SCHEDA_VALOR);
    }

    public byte[] getB05ProgSchedaValorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B05_PROG_SCHEDA_VALOR, Pos.B05_PROG_SCHEDA_VALOR);
        return buffer;
    }

    public void setB05ProgSchedaValorNull(String b05ProgSchedaValorNull) {
        writeString(Pos.B05_PROG_SCHEDA_VALOR_NULL, b05ProgSchedaValorNull, Len.B05_PROG_SCHEDA_VALOR_NULL);
    }

    /**Original name: B05-PROG-SCHEDA-VALOR-NULL<br>*/
    public String getB05ProgSchedaValorNull() {
        return readString(Pos.B05_PROG_SCHEDA_VALOR_NULL, Len.B05_PROG_SCHEDA_VALOR_NULL);
    }

    public String getB05ProgSchedaValorNullFormatted() {
        return Functions.padBlanks(getB05ProgSchedaValorNull(), Len.B05_PROG_SCHEDA_VALOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B05_PROG_SCHEDA_VALOR = 1;
        public static final int B05_PROG_SCHEDA_VALOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B05_PROG_SCHEDA_VALOR = 5;
        public static final int B05_PROG_SCHEDA_VALOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B05_PROG_SCHEDA_VALOR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
