package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B01-DT-QTZ-INI<br>
 * Variable: B01-DT-QTZ-INI from program IDBSB010<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B01DtQtzIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B01DtQtzIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B01_DT_QTZ_INI;
    }

    public void setB01DtQtzIni(int b01DtQtzIni) {
        writeIntAsPacked(Pos.B01_DT_QTZ_INI, b01DtQtzIni, Len.Int.B01_DT_QTZ_INI);
    }

    public void setB01DtQtzIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B01_DT_QTZ_INI, Pos.B01_DT_QTZ_INI);
    }

    /**Original name: B01-DT-QTZ-INI<br>*/
    public int getB01DtQtzIni() {
        return readPackedAsInt(Pos.B01_DT_QTZ_INI, Len.Int.B01_DT_QTZ_INI);
    }

    public byte[] getB01DtQtzIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B01_DT_QTZ_INI, Pos.B01_DT_QTZ_INI);
        return buffer;
    }

    public void setB01DtQtzIniNull(String b01DtQtzIniNull) {
        writeString(Pos.B01_DT_QTZ_INI_NULL, b01DtQtzIniNull, Len.B01_DT_QTZ_INI_NULL);
    }

    /**Original name: B01-DT-QTZ-INI-NULL<br>*/
    public String getB01DtQtzIniNull() {
        return readString(Pos.B01_DT_QTZ_INI_NULL, Len.B01_DT_QTZ_INI_NULL);
    }

    public String getB01DtQtzIniNullFormatted() {
        return Functions.padBlanks(getB01DtQtzIniNull(), Len.B01_DT_QTZ_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B01_DT_QTZ_INI = 1;
        public static final int B01_DT_QTZ_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B01_DT_QTZ_INI = 5;
        public static final int B01_DT_QTZ_INI_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B01_DT_QTZ_INI = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
