package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TAX-TOT<br>
 * Variable: WS-TAX-TOT from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsTaxTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WsTaxTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WS_TAX_TOT;
    }

    public void setWsTaxTot(AfDecimal wsTaxTot) {
        writeDecimalAsPacked(Pos.WS_TAX_TOT, wsTaxTot.copy());
    }

    /**Original name: WS-TAX-TOT<br>
	 * <pre>-- TAX TOTALE (DETTAGLIO TITOLO CONTABILE)</pre>*/
    public AfDecimal getWsTaxTot() {
        return readPackedAsDecimal(Pos.WS_TAX_TOT, Len.Int.WS_TAX_TOT, Len.Fract.WS_TAX_TOT);
    }

    /**Original name: WS-TAX-TOT-NULL<br>*/
    public String getWsTaxTotNull() {
        return readString(Pos.WS_TAX_TOT_NULL, Len.WS_TAX_TOT_NULL);
    }

    public String getWsTaxTotNullFormatted() {
        return Functions.padBlanks(getWsTaxTotNull(), Len.WS_TAX_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WS_TAX_TOT = 1;
        public static final int WS_TAX_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TAX_TOT = 8;
        public static final int WS_TAX_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WS_TAX_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WS_TAX_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
