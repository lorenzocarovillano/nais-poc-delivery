package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: NUM-ULT-NUM-LIN<br>
 * Variable: NUM-ULT-NUM-LIN from program LCCS0070<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class NumUltNumLin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public NumUltNumLin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.NUM_ULT_NUM_LIN;
    }

    public void setNumUltNumLin(int numUltNumLin) {
        writeIntAsPacked(Pos.NUM_ULT_NUM_LIN, numUltNumLin, Len.Int.NUM_ULT_NUM_LIN);
    }

    public void setNumUltNumLinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.NUM_ULT_NUM_LIN, Pos.NUM_ULT_NUM_LIN);
    }

    /**Original name: NUM-ULT-NUM-LIN<br>*/
    public int getNumUltNumLin() {
        return readPackedAsInt(Pos.NUM_ULT_NUM_LIN, Len.Int.NUM_ULT_NUM_LIN);
    }

    public byte[] getNumUltNumLinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.NUM_ULT_NUM_LIN, Pos.NUM_ULT_NUM_LIN);
        return buffer;
    }

    public void setNumUltNumLinNull(String numUltNumLinNull) {
        writeString(Pos.NUM_ULT_NUM_LIN_NULL, numUltNumLinNull, Len.NUM_ULT_NUM_LIN_NULL);
    }

    /**Original name: NUM-ULT-NUM-LIN-NULL<br>*/
    public String getNumUltNumLinNull() {
        return readString(Pos.NUM_ULT_NUM_LIN_NULL, Len.NUM_ULT_NUM_LIN_NULL);
    }

    public String getNumUltNumLinNullFormatted() {
        return Functions.padBlanks(getNumUltNumLinNull(), Len.NUM_ULT_NUM_LIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int NUM_ULT_NUM_LIN = 1;
        public static final int NUM_ULT_NUM_LIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_ULT_NUM_LIN = 3;
        public static final int NUM_ULT_NUM_LIN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int NUM_ULT_NUM_LIN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
