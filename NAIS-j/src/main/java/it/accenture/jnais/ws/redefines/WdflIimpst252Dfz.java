package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IIMPST-252-DFZ<br>
 * Variable: WDFL-IIMPST-252-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIimpst252Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIimpst252Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IIMPST252_DFZ;
    }

    public void setWdflIimpst252Dfz(AfDecimal wdflIimpst252Dfz) {
        writeDecimalAsPacked(Pos.WDFL_IIMPST252_DFZ, wdflIimpst252Dfz.copy());
    }

    public void setWdflIimpst252DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IIMPST252_DFZ, Pos.WDFL_IIMPST252_DFZ);
    }

    /**Original name: WDFL-IIMPST-252-DFZ<br>*/
    public AfDecimal getWdflIimpst252Dfz() {
        return readPackedAsDecimal(Pos.WDFL_IIMPST252_DFZ, Len.Int.WDFL_IIMPST252_DFZ, Len.Fract.WDFL_IIMPST252_DFZ);
    }

    public byte[] getWdflIimpst252DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IIMPST252_DFZ, Pos.WDFL_IIMPST252_DFZ);
        return buffer;
    }

    public void setWdflIimpst252DfzNull(String wdflIimpst252DfzNull) {
        writeString(Pos.WDFL_IIMPST252_DFZ_NULL, wdflIimpst252DfzNull, Len.WDFL_IIMPST252_DFZ_NULL);
    }

    /**Original name: WDFL-IIMPST-252-DFZ-NULL<br>*/
    public String getWdflIimpst252DfzNull() {
        return readString(Pos.WDFL_IIMPST252_DFZ_NULL, Len.WDFL_IIMPST252_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST252_DFZ = 1;
        public static final int WDFL_IIMPST252_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST252_DFZ = 8;
        public static final int WDFL_IIMPST252_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST252_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST252_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
