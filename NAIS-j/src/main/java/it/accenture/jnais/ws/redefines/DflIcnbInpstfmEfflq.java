package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ICNB-INPSTFM-EFFLQ<br>
 * Variable: DFL-ICNB-INPSTFM-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIcnbInpstfmEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIcnbInpstfmEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ICNB_INPSTFM_EFFLQ;
    }

    public void setDflIcnbInpstfmEfflq(AfDecimal dflIcnbInpstfmEfflq) {
        writeDecimalAsPacked(Pos.DFL_ICNB_INPSTFM_EFFLQ, dflIcnbInpstfmEfflq.copy());
    }

    public void setDflIcnbInpstfmEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ICNB_INPSTFM_EFFLQ, Pos.DFL_ICNB_INPSTFM_EFFLQ);
    }

    /**Original name: DFL-ICNB-INPSTFM-EFFLQ<br>*/
    public AfDecimal getDflIcnbInpstfmEfflq() {
        return readPackedAsDecimal(Pos.DFL_ICNB_INPSTFM_EFFLQ, Len.Int.DFL_ICNB_INPSTFM_EFFLQ, Len.Fract.DFL_ICNB_INPSTFM_EFFLQ);
    }

    public byte[] getDflIcnbInpstfmEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ICNB_INPSTFM_EFFLQ, Pos.DFL_ICNB_INPSTFM_EFFLQ);
        return buffer;
    }

    public void setDflIcnbInpstfmEfflqNull(String dflIcnbInpstfmEfflqNull) {
        writeString(Pos.DFL_ICNB_INPSTFM_EFFLQ_NULL, dflIcnbInpstfmEfflqNull, Len.DFL_ICNB_INPSTFM_EFFLQ_NULL);
    }

    /**Original name: DFL-ICNB-INPSTFM-EFFLQ-NULL<br>*/
    public String getDflIcnbInpstfmEfflqNull() {
        return readString(Pos.DFL_ICNB_INPSTFM_EFFLQ_NULL, Len.DFL_ICNB_INPSTFM_EFFLQ_NULL);
    }

    public String getDflIcnbInpstfmEfflqNullFormatted() {
        return Functions.padBlanks(getDflIcnbInpstfmEfflqNull(), Len.DFL_ICNB_INPSTFM_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ICNB_INPSTFM_EFFLQ = 1;
        public static final int DFL_ICNB_INPSTFM_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ICNB_INPSTFM_EFFLQ = 8;
        public static final int DFL_ICNB_INPSTFM_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ICNB_INPSTFM_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ICNB_INPSTFM_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
