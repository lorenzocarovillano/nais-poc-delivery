package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: FL-LIQUI<br>
 * Variable: FL-LIQUI from program LVVS3420<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlLiqui {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FL_LIQUI);
    public static final String SI = "1";
    public static final String NO = "0";

    //==== METHODS ====
    public void setFlLiqui(short flLiqui) {
        this.value = NumericDisplay.asString(flLiqui, Len.FL_LIQUI);
    }

    public void setFlLiquiFormatted(String flLiqui) {
        this.value = Trunc.toUnsignedNumeric(flLiqui, Len.FL_LIQUI);
    }

    public short getFlLiqui() {
        return NumericDisplay.asShort(this.value);
    }

    public void setSi() {
        setFlLiquiFormatted(SI);
    }

    public void setNo() {
        setFlLiquiFormatted(NO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_LIQUI = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
