package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-RISC-PARZ<br>
 * Variable: WK-RISC-PARZ from program LOAS0820<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkRiscParz {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkRiscParz(char wkRiscParz) {
        this.value = wkRiscParz;
    }

    public char getWkRiscParz() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
