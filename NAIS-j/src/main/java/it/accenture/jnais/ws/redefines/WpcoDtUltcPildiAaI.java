package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-PILDI-AA-I<br>
 * Variable: WPCO-DT-ULTC-PILDI-AA-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcPildiAaI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcPildiAaI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_PILDI_AA_I;
    }

    public void setWpcoDtUltcPildiAaI(int wpcoDtUltcPildiAaI) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_PILDI_AA_I, wpcoDtUltcPildiAaI, Len.Int.WPCO_DT_ULTC_PILDI_AA_I);
    }

    public void setDpcoDtUltcPildiAaIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_AA_I, Pos.WPCO_DT_ULTC_PILDI_AA_I);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-AA-I<br>*/
    public int getWpcoDtUltcPildiAaI() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_PILDI_AA_I, Len.Int.WPCO_DT_ULTC_PILDI_AA_I);
    }

    public byte[] getWpcoDtUltcPildiAaIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_AA_I, Pos.WPCO_DT_ULTC_PILDI_AA_I);
        return buffer;
    }

    public void setWpcoDtUltcPildiAaINull(String wpcoDtUltcPildiAaINull) {
        writeString(Pos.WPCO_DT_ULTC_PILDI_AA_I_NULL, wpcoDtUltcPildiAaINull, Len.WPCO_DT_ULTC_PILDI_AA_I_NULL);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-AA-I-NULL<br>*/
    public String getWpcoDtUltcPildiAaINull() {
        return readString(Pos.WPCO_DT_ULTC_PILDI_AA_I_NULL, Len.WPCO_DT_ULTC_PILDI_AA_I_NULL);
    }

    public String getWpcoDtUltcPildiAaINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcPildiAaINull(), Len.WPCO_DT_ULTC_PILDI_AA_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_AA_I = 1;
        public static final int WPCO_DT_ULTC_PILDI_AA_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_AA_I = 5;
        public static final int WPCO_DT_ULTC_PILDI_AA_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_PILDI_AA_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
