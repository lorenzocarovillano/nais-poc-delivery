package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-COS-ONER<br>
 * Variable: PMO-COS-ONER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoCosOner extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoCosOner() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_COS_ONER;
    }

    public void setPmoCosOner(AfDecimal pmoCosOner) {
        writeDecimalAsPacked(Pos.PMO_COS_ONER, pmoCosOner.copy());
    }

    public void setPmoCosOnerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_COS_ONER, Pos.PMO_COS_ONER);
    }

    /**Original name: PMO-COS-ONER<br>*/
    public AfDecimal getPmoCosOner() {
        return readPackedAsDecimal(Pos.PMO_COS_ONER, Len.Int.PMO_COS_ONER, Len.Fract.PMO_COS_ONER);
    }

    public byte[] getPmoCosOnerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_COS_ONER, Pos.PMO_COS_ONER);
        return buffer;
    }

    public void initPmoCosOnerHighValues() {
        fill(Pos.PMO_COS_ONER, Len.PMO_COS_ONER, Types.HIGH_CHAR_VAL);
    }

    public void setPmoCosOnerNull(String pmoCosOnerNull) {
        writeString(Pos.PMO_COS_ONER_NULL, pmoCosOnerNull, Len.PMO_COS_ONER_NULL);
    }

    /**Original name: PMO-COS-ONER-NULL<br>*/
    public String getPmoCosOnerNull() {
        return readString(Pos.PMO_COS_ONER_NULL, Len.PMO_COS_ONER_NULL);
    }

    public String getPmoCosOnerNullFormatted() {
        return Functions.padBlanks(getPmoCosOnerNull(), Len.PMO_COS_ONER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_COS_ONER = 1;
        public static final int PMO_COS_ONER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_COS_ONER = 8;
        public static final int PMO_COS_ONER_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_COS_ONER = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_COS_ONER = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
