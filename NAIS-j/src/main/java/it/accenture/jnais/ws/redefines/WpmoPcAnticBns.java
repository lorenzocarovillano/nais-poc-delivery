package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-PC-ANTIC-BNS<br>
 * Variable: WPMO-PC-ANTIC-BNS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoPcAnticBns extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoPcAnticBns() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_PC_ANTIC_BNS;
    }

    public void setWpmoPcAnticBns(AfDecimal wpmoPcAnticBns) {
        writeDecimalAsPacked(Pos.WPMO_PC_ANTIC_BNS, wpmoPcAnticBns.copy());
    }

    public void setWpmoPcAnticBnsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_PC_ANTIC_BNS, Pos.WPMO_PC_ANTIC_BNS);
    }

    /**Original name: WPMO-PC-ANTIC-BNS<br>*/
    public AfDecimal getWpmoPcAnticBns() {
        return readPackedAsDecimal(Pos.WPMO_PC_ANTIC_BNS, Len.Int.WPMO_PC_ANTIC_BNS, Len.Fract.WPMO_PC_ANTIC_BNS);
    }

    public byte[] getWpmoPcAnticBnsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_PC_ANTIC_BNS, Pos.WPMO_PC_ANTIC_BNS);
        return buffer;
    }

    public void initWpmoPcAnticBnsSpaces() {
        fill(Pos.WPMO_PC_ANTIC_BNS, Len.WPMO_PC_ANTIC_BNS, Types.SPACE_CHAR);
    }

    public void setWpmoPcAnticBnsNull(String wpmoPcAnticBnsNull) {
        writeString(Pos.WPMO_PC_ANTIC_BNS_NULL, wpmoPcAnticBnsNull, Len.WPMO_PC_ANTIC_BNS_NULL);
    }

    /**Original name: WPMO-PC-ANTIC-BNS-NULL<br>*/
    public String getWpmoPcAnticBnsNull() {
        return readString(Pos.WPMO_PC_ANTIC_BNS_NULL, Len.WPMO_PC_ANTIC_BNS_NULL);
    }

    public String getWpmoPcAnticBnsNullFormatted() {
        return Functions.padBlanks(getWpmoPcAnticBnsNull(), Len.WPMO_PC_ANTIC_BNS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_PC_ANTIC_BNS = 1;
        public static final int WPMO_PC_ANTIC_BNS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_PC_ANTIC_BNS = 4;
        public static final int WPMO_PC_ANTIC_BNS_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_PC_ANTIC_BNS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_PC_ANTIC_BNS = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
