package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-INTR-RETDT<br>
 * Variable: WTDR-TOT-INTR-RETDT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_INTR_RETDT;
    }

    public void setWtdrTotIntrRetdt(AfDecimal wtdrTotIntrRetdt) {
        writeDecimalAsPacked(Pos.WTDR_TOT_INTR_RETDT, wtdrTotIntrRetdt.copy());
    }

    /**Original name: WTDR-TOT-INTR-RETDT<br>*/
    public AfDecimal getWtdrTotIntrRetdt() {
        return readPackedAsDecimal(Pos.WTDR_TOT_INTR_RETDT, Len.Int.WTDR_TOT_INTR_RETDT, Len.Fract.WTDR_TOT_INTR_RETDT);
    }

    public void setWtdrTotIntrRetdtNull(String wtdrTotIntrRetdtNull) {
        writeString(Pos.WTDR_TOT_INTR_RETDT_NULL, wtdrTotIntrRetdtNull, Len.WTDR_TOT_INTR_RETDT_NULL);
    }

    /**Original name: WTDR-TOT-INTR-RETDT-NULL<br>*/
    public String getWtdrTotIntrRetdtNull() {
        return readString(Pos.WTDR_TOT_INTR_RETDT_NULL, Len.WTDR_TOT_INTR_RETDT_NULL);
    }

    public String getWtdrTotIntrRetdtNullFormatted() {
        return Functions.padBlanks(getWtdrTotIntrRetdtNull(), Len.WTDR_TOT_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_RETDT = 1;
        public static final int WTDR_TOT_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_RETDT = 8;
        public static final int WTDR_TOT_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
