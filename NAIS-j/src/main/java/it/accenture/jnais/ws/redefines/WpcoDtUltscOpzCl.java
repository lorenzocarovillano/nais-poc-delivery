package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTSC-OPZ-CL<br>
 * Variable: WPCO-DT-ULTSC-OPZ-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltscOpzCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltscOpzCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTSC_OPZ_CL;
    }

    public void setWpcoDtUltscOpzCl(int wpcoDtUltscOpzCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTSC_OPZ_CL, wpcoDtUltscOpzCl, Len.Int.WPCO_DT_ULTSC_OPZ_CL);
    }

    public void setDpcoDtUltscOpzClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTSC_OPZ_CL, Pos.WPCO_DT_ULTSC_OPZ_CL);
    }

    /**Original name: WPCO-DT-ULTSC-OPZ-CL<br>*/
    public int getWpcoDtUltscOpzCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTSC_OPZ_CL, Len.Int.WPCO_DT_ULTSC_OPZ_CL);
    }

    public byte[] getWpcoDtUltscOpzClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTSC_OPZ_CL, Pos.WPCO_DT_ULTSC_OPZ_CL);
        return buffer;
    }

    public void setWpcoDtUltscOpzClNull(String wpcoDtUltscOpzClNull) {
        writeString(Pos.WPCO_DT_ULTSC_OPZ_CL_NULL, wpcoDtUltscOpzClNull, Len.WPCO_DT_ULTSC_OPZ_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTSC-OPZ-CL-NULL<br>*/
    public String getWpcoDtUltscOpzClNull() {
        return readString(Pos.WPCO_DT_ULTSC_OPZ_CL_NULL, Len.WPCO_DT_ULTSC_OPZ_CL_NULL);
    }

    public String getWpcoDtUltscOpzClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltscOpzClNull(), Len.WPCO_DT_ULTSC_OPZ_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_OPZ_CL = 1;
        public static final int WPCO_DT_ULTSC_OPZ_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTSC_OPZ_CL = 5;
        public static final int WPCO_DT_ULTSC_OPZ_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTSC_OPZ_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
