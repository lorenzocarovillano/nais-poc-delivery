package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: ISPC0040-FLAG-COPERTURA-FIN<br>
 * Variable: ISPC0040-FLAG-COPERTURA-FIN from copybook ISPC0040<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispc0040FlagCoperturaFin {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = '1';
    public static final char NO = '0';

    //==== METHODS ====
    public void setFlagCoperturaFin(char flagCoperturaFin) {
        this.value = flagCoperturaFin;
    }

    public char getFlagCoperturaFin() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_COPERTURA_FIN = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
