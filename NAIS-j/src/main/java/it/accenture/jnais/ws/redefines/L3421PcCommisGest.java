package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-PC-COMMIS-GEST<br>
 * Variable: L3421-PC-COMMIS-GEST from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421PcCommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421PcCommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_PC_COMMIS_GEST;
    }

    public void setL3421PcCommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_PC_COMMIS_GEST, Pos.L3421_PC_COMMIS_GEST);
    }

    /**Original name: L3421-PC-COMMIS-GEST<br>*/
    public AfDecimal getL3421PcCommisGest() {
        return readPackedAsDecimal(Pos.L3421_PC_COMMIS_GEST, Len.Int.L3421_PC_COMMIS_GEST, Len.Fract.L3421_PC_COMMIS_GEST);
    }

    public byte[] getL3421PcCommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_PC_COMMIS_GEST, Pos.L3421_PC_COMMIS_GEST);
        return buffer;
    }

    /**Original name: L3421-PC-COMMIS-GEST-NULL<br>*/
    public String getL3421PcCommisGestNull() {
        return readString(Pos.L3421_PC_COMMIS_GEST_NULL, Len.L3421_PC_COMMIS_GEST_NULL);
    }

    public String getL3421PcCommisGestNullFormatted() {
        return Functions.padBlanks(getL3421PcCommisGestNull(), Len.L3421_PC_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_PC_COMMIS_GEST = 1;
        public static final int L3421_PC_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_PC_COMMIS_GEST = 4;
        public static final int L3421_PC_COMMIS_GEST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
