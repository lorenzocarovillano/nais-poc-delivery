package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-DUR-MM<br>
 * Variable: WPMO-DUR-MM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoDurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoDurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_DUR_MM;
    }

    public void setWpmoDurMm(int wpmoDurMm) {
        writeIntAsPacked(Pos.WPMO_DUR_MM, wpmoDurMm, Len.Int.WPMO_DUR_MM);
    }

    public void setWpmoDurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_DUR_MM, Pos.WPMO_DUR_MM);
    }

    /**Original name: WPMO-DUR-MM<br>*/
    public int getWpmoDurMm() {
        return readPackedAsInt(Pos.WPMO_DUR_MM, Len.Int.WPMO_DUR_MM);
    }

    public byte[] getWpmoDurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_DUR_MM, Pos.WPMO_DUR_MM);
        return buffer;
    }

    public void initWpmoDurMmSpaces() {
        fill(Pos.WPMO_DUR_MM, Len.WPMO_DUR_MM, Types.SPACE_CHAR);
    }

    public void setWpmoDurMmNull(String wpmoDurMmNull) {
        writeString(Pos.WPMO_DUR_MM_NULL, wpmoDurMmNull, Len.WPMO_DUR_MM_NULL);
    }

    /**Original name: WPMO-DUR-MM-NULL<br>*/
    public String getWpmoDurMmNull() {
        return readString(Pos.WPMO_DUR_MM_NULL, Len.WPMO_DUR_MM_NULL);
    }

    public String getWpmoDurMmNullFormatted() {
        return Functions.padBlanks(getWpmoDurMmNull(), Len.WPMO_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_DUR_MM = 1;
        public static final int WPMO_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_DUR_MM = 3;
        public static final int WPMO_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
