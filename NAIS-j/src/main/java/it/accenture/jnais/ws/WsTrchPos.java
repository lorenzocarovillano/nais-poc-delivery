package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: WS-TRCH-POS<br>
 * Variable: WS-TRCH-POS from program LVVS0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTrchPos {

    //==== PROPERTIES ====
    //Original name: WS-TP-TRCH-POS-1
    private String pos1 = "1";
    //Original name: WS-TP-TRCH-POS-2
    private String pos2 = "2";
    //Original name: WS-TP-TRCH-POS-3
    private String pos3 = "3";
    //Original name: WS-TP-TRCH-POS-4
    private String pos4 = "4";
    //Original name: WS-TP-TRCH-POS-5
    private String pos5 = "5";
    //Original name: WS-TP-TRCH-POS-6
    private String pos6 = "6";
    //Original name: WS-TP-TRCH-POS-7
    private String pos7 = "7";
    //Original name: WS-TP-TRCH-POS-8
    private String pos8 = "8";
    //Original name: WS-TP-TRCH-POS-12
    private String pos12 = "12";
    //Original name: WS-TP-TRCH-POS-16
    private String pos16 = "16";
    //Original name: WS-TP-TRCH-POS-17
    private String pos17 = "17";

    //==== METHODS ====
    public byte[] getWsTrchPosBytes() {
        byte[] buffer = new byte[Len.WS_TRCH_POS];
        return getWsTrchPosBytes(buffer, 1);
    }

    public byte[] getWsTrchPosBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, pos1, Len.POS1);
        position += Len.POS1;
        MarshalByte.writeString(buffer, position, pos2, Len.POS2);
        position += Len.POS2;
        MarshalByte.writeString(buffer, position, pos3, Len.POS3);
        position += Len.POS3;
        MarshalByte.writeString(buffer, position, pos4, Len.POS4);
        position += Len.POS4;
        MarshalByte.writeString(buffer, position, pos5, Len.POS5);
        position += Len.POS5;
        MarshalByte.writeString(buffer, position, pos6, Len.POS6);
        position += Len.POS6;
        MarshalByte.writeString(buffer, position, pos7, Len.POS7);
        position += Len.POS7;
        MarshalByte.writeString(buffer, position, pos8, Len.POS8);
        position += Len.POS8;
        MarshalByte.writeString(buffer, position, pos12, Len.POS12);
        position += Len.POS12;
        MarshalByte.writeString(buffer, position, pos16, Len.POS16);
        position += Len.POS16;
        MarshalByte.writeString(buffer, position, pos17, Len.POS17);
        return buffer;
    }

    public String getPos1() {
        return this.pos1;
    }

    public String getPos2() {
        return this.pos2;
    }

    public String getPos3() {
        return this.pos3;
    }

    public String getPos4() {
        return this.pos4;
    }

    public String getPos5() {
        return this.pos5;
    }

    public String getPos6() {
        return this.pos6;
    }

    public String getPos7() {
        return this.pos7;
    }

    public String getPos8() {
        return this.pos8;
    }

    public String getPos12() {
        return this.pos12;
    }

    public String getPos16() {
        return this.pos16;
    }

    public String getPos17() {
        return this.pos17;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int POS1 = 2;
        public static final int POS2 = 2;
        public static final int POS3 = 2;
        public static final int POS4 = 2;
        public static final int POS5 = 2;
        public static final int POS6 = 2;
        public static final int POS7 = 2;
        public static final int POS8 = 2;
        public static final int POS12 = 2;
        public static final int POS16 = 2;
        public static final int POS17 = 2;
        public static final int WS_TRCH_POS = POS1 + POS2 + POS3 + POS4 + POS5 + POS6 + POS7 + POS8 + POS12 + POS16 + POS17;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
