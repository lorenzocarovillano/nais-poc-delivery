package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.ws.enums.Ispc0040CodDurata;

/**Original name: ISPC0040-MODALITA-DURATA<br>
 * Variables: ISPC0040-MODALITA-DURATA from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040ModalitaDurata {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-DURATA
    private Ispc0040CodDurata codDurata = new Ispc0040CodDurata();
    //Original name: ISPC0040-DESCR-DURATA
    private String descrDurata = DefaultValues.stringVal(Len.DESCR_DURATA);

    //==== METHODS ====
    public void setModalitaDurataBytes(byte[] buffer, int offset) {
        int position = offset;
        codDurata.setCodDurata(MarshalByte.readString(buffer, position, Ispc0040CodDurata.Len.COD_DURATA));
        position += Ispc0040CodDurata.Len.COD_DURATA;
        descrDurata = MarshalByte.readString(buffer, position, Len.DESCR_DURATA);
    }

    public byte[] getModalitaDurataBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codDurata.getCodDurata(), Ispc0040CodDurata.Len.COD_DURATA);
        position += Ispc0040CodDurata.Len.COD_DURATA;
        MarshalByte.writeString(buffer, position, descrDurata, Len.DESCR_DURATA);
        return buffer;
    }

    public void initModalitaDurataSpaces() {
        codDurata.setCodDurata("");
        descrDurata = "";
    }

    public void setDescrDurata(String descrDurata) {
        this.descrDurata = Functions.subString(descrDurata, Len.DESCR_DURATA);
    }

    public String getDescrDurata() {
        return this.descrDurata;
    }

    public Ispc0040CodDurata getCodDurata() {
        return codDurata;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DESCR_DURATA = 30;
        public static final int MODALITA_DURATA = Ispc0040CodDurata.Len.COD_DURATA + DESCR_DURATA;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
