package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbvf111Input;

/**Original name: LDBVF111<br>
 * Variable: LDBVF111 from copybook LDBVF111<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbvf111 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBVF111-INPUT
    private Ldbvf111Input input = new Ldbvf111Input();
    //Original name: LDBVF111-CUM-PRE-VERS
    private AfDecimal cumPreVers = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVF111;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbvf111Bytes(buf);
    }

    public String getLdbvf111Formatted() {
        return MarshalByteExt.bufferToStr(getLdbvf111Bytes());
    }

    public void setLdbvf111Bytes(byte[] buffer) {
        setLdbvf111Bytes(buffer, 1);
    }

    public byte[] getLdbvf111Bytes() {
        byte[] buffer = new byte[Len.LDBVF111];
        return getLdbvf111Bytes(buffer, 1);
    }

    public void setLdbvf111Bytes(byte[] buffer, int offset) {
        int position = offset;
        input.setInputBytes(buffer, position);
        position += Ldbvf111Input.Len.INPUT;
        setOutputBytes(buffer, position);
    }

    public byte[] getLdbvf111Bytes(byte[] buffer, int offset) {
        int position = offset;
        input.getInputBytes(buffer, position);
        position += Ldbvf111Input.Len.INPUT;
        getOutputBytes(buffer, position);
        return buffer;
    }

    public void setOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        cumPreVers.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CUM_PRE_VERS, Len.Fract.CUM_PRE_VERS));
    }

    public byte[] getOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeDecimalAsPacked(buffer, position, cumPreVers.copy());
        return buffer;
    }

    public void setCumPreVers(AfDecimal cumPreVers) {
        this.cumPreVers.assign(cumPreVers);
    }

    public AfDecimal getCumPreVers() {
        return this.cumPreVers.copy();
    }

    public Ldbvf111Input getInput() {
        return input;
    }

    @Override
    public byte[] serialize() {
        return getLdbvf111Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int CUM_PRE_VERS = 8;
        public static final int OUTPUT = CUM_PRE_VERS;
        public static final int LDBVF111 = Ldbvf111Input.Len.INPUT + OUTPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CUM_PRE_VERS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CUM_PRE_VERS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
