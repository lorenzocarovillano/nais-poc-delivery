package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-COD-AGE<br>
 * Variable: W-B03-COD-AGE from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CodAgeLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CodAgeLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_COD_AGE;
    }

    public void setwB03CodAge(int wB03CodAge) {
        writeIntAsPacked(Pos.W_B03_COD_AGE, wB03CodAge, Len.Int.W_B03_COD_AGE);
    }

    /**Original name: W-B03-COD-AGE<br>*/
    public int getwB03CodAge() {
        return readPackedAsInt(Pos.W_B03_COD_AGE, Len.Int.W_B03_COD_AGE);
    }

    public byte[] getwB03CodAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_COD_AGE, Pos.W_B03_COD_AGE);
        return buffer;
    }

    public void setwB03CodAgeNull(String wB03CodAgeNull) {
        writeString(Pos.W_B03_COD_AGE_NULL, wB03CodAgeNull, Len.W_B03_COD_AGE_NULL);
    }

    /**Original name: W-B03-COD-AGE-NULL<br>*/
    public String getwB03CodAgeNull() {
        return readString(Pos.W_B03_COD_AGE_NULL, Len.W_B03_COD_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_COD_AGE = 1;
        public static final int W_B03_COD_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_COD_AGE = 3;
        public static final int W_B03_COD_AGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_COD_AGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
