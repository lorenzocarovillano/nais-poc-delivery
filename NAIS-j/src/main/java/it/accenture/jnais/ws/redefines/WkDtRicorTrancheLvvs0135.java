package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WK-DT-RICOR-TRANCHE<br>
 * Variable: WK-DT-RICOR-TRANCHE from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkDtRicorTrancheLvvs0135 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkDtRicorTrancheLvvs0135() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_DT_RICOR_TRANCHE;
    }

    public void setWkDtRicorTrancheFormatted(String data) {
        writeString(Pos.WK_DT_RICOR_TRANCHE, data, Len.WK_DT_RICOR_TRANCHE);
    }

    /**Original name: WK-DT-RICOR-TRANCHE<br>*/
    public byte[] getWkDtRicorTrancheBytes() {
        byte[] buffer = new byte[Len.WK_DT_RICOR_TRANCHE];
        return getWkDtRicorTrancheBytes(buffer, 1);
    }

    public byte[] getWkDtRicorTrancheBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WK_DT_RICOR_TRANCHE, Pos.WK_DT_RICOR_TRANCHE);
        return buffer;
    }

    public void initWkDtRicorTrancheZeroes() {
        fill(Pos.WK_DT_RICOR_TRANCHE, Len.WK_DT_RICOR_TRANCHE, '0');
    }

    public void setRicorTrancheAaFormatted(String ricorTrancheAa) {
        writeString(Pos.RICOR_TRANCHE_AA, Trunc.toUnsignedNumeric(ricorTrancheAa, Len.RICOR_TRANCHE_AA), Len.RICOR_TRANCHE_AA);
    }

    /**Original name: WK-DT-RICOR-TRANCHE-AA<br>*/
    public short getRicorTrancheAa() {
        return readNumDispUnsignedShort(Pos.RICOR_TRANCHE_AA, Len.RICOR_TRANCHE_AA);
    }

    public String getRicorTrancheAaFormatted() {
        return readFixedString(Pos.RICOR_TRANCHE_AA, Len.RICOR_TRANCHE_AA);
    }

    public void setRicorTrancheMmFormatted(String ricorTrancheMm) {
        writeString(Pos.RICOR_TRANCHE_MM, Trunc.toUnsignedNumeric(ricorTrancheMm, Len.RICOR_TRANCHE_MM), Len.RICOR_TRANCHE_MM);
    }

    /**Original name: WK-DT-RICOR-TRANCHE-MM<br>*/
    public short getRicorTrancheMm() {
        return readNumDispUnsignedShort(Pos.RICOR_TRANCHE_MM, Len.RICOR_TRANCHE_MM);
    }

    public void setRicorTrancheGgFormatted(String ricorTrancheGg) {
        writeString(Pos.RICOR_TRANCHE_GG, Trunc.toUnsignedNumeric(ricorTrancheGg, Len.RICOR_TRANCHE_GG), Len.RICOR_TRANCHE_GG);
    }

    /**Original name: WK-DT-RICOR-TRANCHE-GG<br>*/
    public short getRicorTrancheGg() {
        return readNumDispUnsignedShort(Pos.RICOR_TRANCHE_GG, Len.RICOR_TRANCHE_GG);
    }

    /**Original name: WK-DT-DECOR-TRCH-RED<br>*/
    public int getWkDtDecorTrchRed() {
        return readNumDispInt(Pos.WK_DT_DECOR_TRCH_RED, Len.WK_DT_DECOR_TRCH_RED);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_DT_RICOR_TRANCHE = 1;
        public static final int RICOR_TRANCHE_AA = WK_DT_RICOR_TRANCHE;
        public static final int RICOR_TRANCHE_MM = RICOR_TRANCHE_AA + Len.RICOR_TRANCHE_AA;
        public static final int RICOR_TRANCHE_GG = RICOR_TRANCHE_MM + Len.RICOR_TRANCHE_MM;
        public static final int WK_DT_DECOR_TRCH_RED = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RICOR_TRANCHE_AA = 4;
        public static final int RICOR_TRANCHE_MM = 2;
        public static final int RICOR_TRANCHE_GG = 2;
        public static final int WK_DT_RICOR_TRANCHE = RICOR_TRANCHE_AA + RICOR_TRANCHE_MM + RICOR_TRANCHE_GG;
        public static final int WK_DT_DECOR_TRCH_RED = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
