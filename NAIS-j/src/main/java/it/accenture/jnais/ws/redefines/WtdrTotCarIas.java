package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-CAR-IAS<br>
 * Variable: WTDR-TOT-CAR-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotCarIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotCarIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_CAR_IAS;
    }

    public void setWtdrTotCarIas(AfDecimal wtdrTotCarIas) {
        writeDecimalAsPacked(Pos.WTDR_TOT_CAR_IAS, wtdrTotCarIas.copy());
    }

    /**Original name: WTDR-TOT-CAR-IAS<br>*/
    public AfDecimal getWtdrTotCarIas() {
        return readPackedAsDecimal(Pos.WTDR_TOT_CAR_IAS, Len.Int.WTDR_TOT_CAR_IAS, Len.Fract.WTDR_TOT_CAR_IAS);
    }

    public void setWtdrTotCarIasNull(String wtdrTotCarIasNull) {
        writeString(Pos.WTDR_TOT_CAR_IAS_NULL, wtdrTotCarIasNull, Len.WTDR_TOT_CAR_IAS_NULL);
    }

    /**Original name: WTDR-TOT-CAR-IAS-NULL<br>*/
    public String getWtdrTotCarIasNull() {
        return readString(Pos.WTDR_TOT_CAR_IAS_NULL, Len.WTDR_TOT_CAR_IAS_NULL);
    }

    public String getWtdrTotCarIasNullFormatted() {
        return Functions.padBlanks(getWtdrTotCarIasNull(), Len.WTDR_TOT_CAR_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_IAS = 1;
        public static final int WTDR_TOT_CAR_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_CAR_IAS = 8;
        public static final int WTDR_TOT_CAR_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_CAR_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
