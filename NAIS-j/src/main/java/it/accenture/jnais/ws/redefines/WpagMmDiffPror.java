package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WPAG-MM-DIFF-PROR<br>
 * Variable: WPAG-MM-DIFF-PROR from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagMmDiffPror extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagMmDiffPror() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_MM_DIFF_PROR;
    }

    public void setWpagMmDiffProrFormatted(String wpagMmDiffPror) {
        writeString(Pos.WPAG_MM_DIFF_PROR, Trunc.toUnsignedNumeric(wpagMmDiffPror, Len.WPAG_MM_DIFF_PROR), Len.WPAG_MM_DIFF_PROR);
    }

    public void setWpagMmDiffProrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_MM_DIFF_PROR, Pos.WPAG_MM_DIFF_PROR);
    }

    /**Original name: WPAG-MM-DIFF-PROR<br>*/
    public short getWpagMmDiffPror() {
        return readNumDispUnsignedShort(Pos.WPAG_MM_DIFF_PROR, Len.WPAG_MM_DIFF_PROR);
    }

    public byte[] getWpagMmDiffProrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_MM_DIFF_PROR, Pos.WPAG_MM_DIFF_PROR);
        return buffer;
    }

    public void initWpagMmDiffProrSpaces() {
        fill(Pos.WPAG_MM_DIFF_PROR, Len.WPAG_MM_DIFF_PROR, Types.SPACE_CHAR);
    }

    public void setWpagMmDiffProrNull(String wpagMmDiffProrNull) {
        writeString(Pos.WPAG_MM_DIFF_PROR_NULL, wpagMmDiffProrNull, Len.WPAG_MM_DIFF_PROR_NULL);
    }

    /**Original name: WPAG-MM-DIFF-PROR-NULL<br>*/
    public String getWpagMmDiffProrNull() {
        return readString(Pos.WPAG_MM_DIFF_PROR_NULL, Len.WPAG_MM_DIFF_PROR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_MM_DIFF_PROR = 1;
        public static final int WPAG_MM_DIFF_PROR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_MM_DIFF_PROR = 2;
        public static final int WPAG_MM_DIFF_PROR_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
