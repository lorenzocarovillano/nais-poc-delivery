package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-ACQ-EXP<br>
 * Variable: WTDR-TOT-ACQ-EXP from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_ACQ_EXP;
    }

    public void setWtdrTotAcqExp(AfDecimal wtdrTotAcqExp) {
        writeDecimalAsPacked(Pos.WTDR_TOT_ACQ_EXP, wtdrTotAcqExp.copy());
    }

    /**Original name: WTDR-TOT-ACQ-EXP<br>*/
    public AfDecimal getWtdrTotAcqExp() {
        return readPackedAsDecimal(Pos.WTDR_TOT_ACQ_EXP, Len.Int.WTDR_TOT_ACQ_EXP, Len.Fract.WTDR_TOT_ACQ_EXP);
    }

    public void setWtdrTotAcqExpNull(String wtdrTotAcqExpNull) {
        writeString(Pos.WTDR_TOT_ACQ_EXP_NULL, wtdrTotAcqExpNull, Len.WTDR_TOT_ACQ_EXP_NULL);
    }

    /**Original name: WTDR-TOT-ACQ-EXP-NULL<br>*/
    public String getWtdrTotAcqExpNull() {
        return readString(Pos.WTDR_TOT_ACQ_EXP_NULL, Len.WTDR_TOT_ACQ_EXP_NULL);
    }

    public String getWtdrTotAcqExpNullFormatted() {
        return Functions.padBlanks(getWtdrTotAcqExpNull(), Len.WTDR_TOT_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_ACQ_EXP = 1;
        public static final int WTDR_TOT_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_ACQ_EXP = 8;
        public static final int WTDR_TOT_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
