package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-QTZ-AGG<br>
 * Variable: FLAG-QTZ-AGG from program LCCS0450<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagQtzAgg {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setFlagQtzAgg(char flagQtzAgg) {
        this.value = flagQtzAgg;
    }

    public char getFlagQtzAgg() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }
}
