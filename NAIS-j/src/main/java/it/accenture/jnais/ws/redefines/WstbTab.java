package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WSTB-TAB<br>
 * Variable: WSTB-TAB from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WstbTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_STAT_OGG_MAXOCCURS = 1300;
    public static final char WSTB_ST_ADD = 'A';
    public static final char WSTB_ST_MOD = 'M';
    public static final char WSTB_ST_INV = 'I';
    public static final char WSTB_ST_DEL = 'D';
    public static final char WSTB_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WstbTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSTB_TAB;
    }

    public String getWstbTabFormatted() {
        return readFixedString(Pos.WSTB_TAB, Len.WSTB_TAB);
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wstbStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WSTB-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA STAT_OGG_BUS
	 *    ALIAS STB
	 *    ULTIMO AGG. 02 SET 2008
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wstbStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wstbIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WSTB-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wstbIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdStatOggBus(int idStatOggBusIdx, int idStatOggBus) {
        int position = Pos.wstbIdStatOggBus(idStatOggBusIdx - 1);
        writeIntAsPacked(position, idStatOggBus, Len.Int.ID_STAT_OGG_BUS);
    }

    /**Original name: WSTB-ID-STAT-OGG-BUS<br>*/
    public int getIdStatOggBus(int idStatOggBusIdx) {
        int position = Pos.wstbIdStatOggBus(idStatOggBusIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_STAT_OGG_BUS);
    }

    public void setIdOgg(int idOggIdx, int idOgg) {
        int position = Pos.wstbIdOgg(idOggIdx - 1);
        writeIntAsPacked(position, idOgg, Len.Int.ID_OGG);
    }

    /**Original name: WSTB-ID-OGG<br>*/
    public int getIdOgg(int idOggIdx) {
        int position = Pos.wstbIdOgg(idOggIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_OGG);
    }

    public void setTpOgg(int tpOggIdx, String tpOgg) {
        int position = Pos.wstbTpOgg(tpOggIdx - 1);
        writeString(position, tpOgg, Len.TP_OGG);
    }

    /**Original name: WSTB-TP-OGG<br>*/
    public String getTpOgg(int tpOggIdx) {
        int position = Pos.wstbTpOgg(tpOggIdx - 1);
        return readString(position, Len.TP_OGG);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wstbIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WSTB-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wstbIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wstbIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WSTB-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wstbIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setIdMoviChiuNull(int idMoviChiuNullIdx, String idMoviChiuNull) {
        int position = Pos.wstbIdMoviChiuNull(idMoviChiuNullIdx - 1);
        writeString(position, idMoviChiuNull, Len.ID_MOVI_CHIU_NULL);
    }

    /**Original name: WSTB-ID-MOVI-CHIU-NULL<br>*/
    public String getIdMoviChiuNull(int idMoviChiuNullIdx) {
        int position = Pos.wstbIdMoviChiuNull(idMoviChiuNullIdx - 1);
        return readString(position, Len.ID_MOVI_CHIU_NULL);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wstbDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WSTB-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wstbDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wstbDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WSTB-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wstbDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wstbCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WSTB-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wstbCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setTpStatBus(int tpStatBusIdx, String tpStatBus) {
        int position = Pos.wstbTpStatBus(tpStatBusIdx - 1);
        writeString(position, tpStatBus, Len.TP_STAT_BUS);
    }

    /**Original name: WSTB-TP-STAT-BUS<br>*/
    public String getTpStatBus(int tpStatBusIdx) {
        int position = Pos.wstbTpStatBus(tpStatBusIdx - 1);
        return readString(position, Len.TP_STAT_BUS);
    }

    public void setTpCaus(int tpCausIdx, String tpCaus) {
        int position = Pos.wstbTpCaus(tpCausIdx - 1);
        writeString(position, tpCaus, Len.TP_CAUS);
    }

    /**Original name: WSTB-TP-CAUS<br>*/
    public String getTpCaus(int tpCausIdx) {
        int position = Pos.wstbTpCaus(tpCausIdx - 1);
        return readString(position, Len.TP_CAUS);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wstbDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WSTB-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wstbDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wstbDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WSTB-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wstbDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wstbDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WSTB-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wstbDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wstbDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WSTB-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wstbDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wstbDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WSTB-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wstbDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wstbDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WSTB-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wstbDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wstbDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WSTB-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wstbDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WSTB-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WSTB_TAB = 1;
        public static final int WSTB_TAB_R = 1;
        public static final int FLR1 = WSTB_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wstbTabStatOgg(int idx) {
            return WSTB_TAB + idx * Len.TAB_STAT_OGG;
        }

        public static int wstbStatus(int idx) {
            return wstbTabStatOgg(idx);
        }

        public static int wstbIdPtf(int idx) {
            return wstbStatus(idx) + Len.STATUS;
        }

        public static int wstbDati(int idx) {
            return wstbIdPtf(idx) + Len.ID_PTF;
        }

        public static int wstbIdStatOggBus(int idx) {
            return wstbDati(idx);
        }

        public static int wstbIdOgg(int idx) {
            return wstbIdStatOggBus(idx) + Len.ID_STAT_OGG_BUS;
        }

        public static int wstbTpOgg(int idx) {
            return wstbIdOgg(idx) + Len.ID_OGG;
        }

        public static int wstbIdMoviCrz(int idx) {
            return wstbTpOgg(idx) + Len.TP_OGG;
        }

        public static int wstbIdMoviChiu(int idx) {
            return wstbIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wstbIdMoviChiuNull(int idx) {
            return wstbIdMoviChiu(idx);
        }

        public static int wstbDtIniEff(int idx) {
            return wstbIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wstbDtEndEff(int idx) {
            return wstbDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wstbCodCompAnia(int idx) {
            return wstbDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wstbTpStatBus(int idx) {
            return wstbCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wstbTpCaus(int idx) {
            return wstbTpStatBus(idx) + Len.TP_STAT_BUS;
        }

        public static int wstbDsRiga(int idx) {
            return wstbTpCaus(idx) + Len.TP_CAUS;
        }

        public static int wstbDsOperSql(int idx) {
            return wstbDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wstbDsVer(int idx) {
            return wstbDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wstbDsTsIniCptz(int idx) {
            return wstbDsVer(idx) + Len.DS_VER;
        }

        public static int wstbDsTsEndCptz(int idx) {
            return wstbDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wstbDsUtente(int idx) {
            return wstbDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wstbDsStatoElab(int idx) {
            return wstbDsUtente(idx) + Len.DS_UTENTE;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_STAT_OGG_BUS = 5;
        public static final int ID_OGG = 5;
        public static final int TP_OGG = 2;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int TP_STAT_BUS = 2;
        public static final int TP_CAUS = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DATI = ID_STAT_OGG_BUS + ID_OGG + TP_OGG + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + TP_STAT_BUS + TP_CAUS + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;
        public static final int TAB_STAT_OGG = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 98;
        public static final int WSTB_TAB = WstbTab.TAB_STAT_OGG_MAXOCCURS * TAB_STAT_OGG;
        public static final int RESTO_TAB = 127302;
        public static final int ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_STAT_OGG_BUS = 9;
            public static final int ID_OGG = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
