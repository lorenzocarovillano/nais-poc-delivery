package it.accenture.jnais.ws.enums;

/**Original name: IABV0006-COUNTER-STD<br>
 * Variable: IABV0006-COUNTER-STD from copybook IABV0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0006CounterStd {

    //==== PROPERTIES ====
    private char value = 'Y';
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setCounterStd(char counterStd) {
        this.value = counterStd;
    }

    public char getCounterStd() {
        return this.value;
    }

    public boolean isIabv0006CounterStdYes() {
        return value == YES;
    }

    public void setIabv0006CounterStdYes() {
        value = YES;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COUNTER_STD = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
