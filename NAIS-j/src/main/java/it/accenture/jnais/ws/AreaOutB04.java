package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Lccvb041;
import it.accenture.jnais.copy.Wb04Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: AREA-OUT-B04<br>
 * Variable: AREA-OUT-B04 from program LLBS0230<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaOutB04 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WB04-ELE-B04-MAX
    private short wb04EleB04Max = ((short)0);
    //Original name: LCCVB041
    private Lccvb041 lccvb041 = new Lccvb041();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_OUT_B04;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaOutB04Bytes(buf);
    }

    public String getAreaOutB04Formatted() {
        return getWb04AreaB04Formatted();
    }

    public void setAreaOutB04Bytes(byte[] buffer) {
        setAreaOutB04Bytes(buffer, 1);
    }

    public byte[] getAreaOutB04Bytes() {
        byte[] buffer = new byte[Len.AREA_OUT_B04];
        return getAreaOutB04Bytes(buffer, 1);
    }

    public void setAreaOutB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        setWb04AreaB04Bytes(buffer, position);
    }

    public byte[] getAreaOutB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        getWb04AreaB04Bytes(buffer, position);
        return buffer;
    }

    public String getWb04AreaB04Formatted() {
        return MarshalByteExt.bufferToStr(getWb04AreaB04Bytes());
    }

    /**Original name: WB04-AREA-B04<br>*/
    public byte[] getWb04AreaB04Bytes() {
        byte[] buffer = new byte[Len.WB04_AREA_B04];
        return getWb04AreaB04Bytes(buffer, 1);
    }

    public void setWb04AreaB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        wb04EleB04Max = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setWb04TabB04Bytes(buffer, position);
    }

    public byte[] getWb04AreaB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, wb04EleB04Max);
        position += Types.SHORT_SIZE;
        getWb04TabB04Bytes(buffer, position);
        return buffer;
    }

    public void setWb04EleB04Max(short wb04EleB04Max) {
        this.wb04EleB04Max = wb04EleB04Max;
    }

    public short getWb04EleB04Max() {
        return this.wb04EleB04Max;
    }

    public void setWb04TabB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        lccvb041.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvb041.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvb041.Len.Int.ID_PTF, 0));
        position += Lccvb041.Len.ID_PTF;
        lccvb041.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWb04TabB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvb041.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvb041.getIdPtf(), Lccvb041.Len.Int.ID_PTF, 0);
        position += Lccvb041.Len.ID_PTF;
        lccvb041.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public Lccvb041 getLccvb041() {
        return lccvb041;
    }

    @Override
    public byte[] serialize() {
        return getAreaOutB04Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WB04_ELE_B04_MAX = 2;
        public static final int WB04_TAB_B04 = WpolStatus.Len.STATUS + Lccvb041.Len.ID_PTF + Wb04Dati.Len.DATI;
        public static final int WB04_AREA_B04 = WB04_ELE_B04_MAX + WB04_TAB_B04;
        public static final int AREA_OUT_B04 = WB04_AREA_B04;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
