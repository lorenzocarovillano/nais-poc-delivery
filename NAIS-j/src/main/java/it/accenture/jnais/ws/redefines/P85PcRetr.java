package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-PC-RETR<br>
 * Variable: P85-PC-RETR from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85PcRetr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85PcRetr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_PC_RETR;
    }

    public void setP85PcRetr(AfDecimal p85PcRetr) {
        writeDecimalAsPacked(Pos.P85_PC_RETR, p85PcRetr.copy());
    }

    public void setP85PcRetrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_PC_RETR, Pos.P85_PC_RETR);
    }

    /**Original name: P85-PC-RETR<br>*/
    public AfDecimal getP85PcRetr() {
        return readPackedAsDecimal(Pos.P85_PC_RETR, Len.Int.P85_PC_RETR, Len.Fract.P85_PC_RETR);
    }

    public byte[] getP85PcRetrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_PC_RETR, Pos.P85_PC_RETR);
        return buffer;
    }

    public void setP85PcRetrNull(String p85PcRetrNull) {
        writeString(Pos.P85_PC_RETR_NULL, p85PcRetrNull, Len.P85_PC_RETR_NULL);
    }

    /**Original name: P85-PC-RETR-NULL<br>*/
    public String getP85PcRetrNull() {
        return readString(Pos.P85_PC_RETR_NULL, Len.P85_PC_RETR_NULL);
    }

    public String getP85PcRetrNullFormatted() {
        return Functions.padBlanks(getP85PcRetrNull(), Len.P85_PC_RETR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_PC_RETR = 1;
        public static final int P85_PC_RETR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_PC_RETR = 4;
        public static final int P85_PC_RETR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_PC_RETR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
