package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-TROVATO<br>
 * Variable: SW-TROVATO from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char TROVATO_NO = 'N';

    //==== METHODS ====
    public void setSwTrovato(char swTrovato) {
        this.value = swTrovato;
    }

    public char getSwTrovato() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public boolean isTrovatoNo() {
        return value == TROVATO_NO;
    }

    public void setTrovatoNo() {
        value = TROVATO_NO;
    }
}
