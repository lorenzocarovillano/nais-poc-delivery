package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-WRITE-FILESQS3<br>
 * Variable: FLAG-WRITE-FILESQS3 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagWriteFilesqs3 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagWriteFilesqs3(char flagWriteFilesqs3) {
        this.value = flagWriteFilesqs3;
    }

    public char getFlagWriteFilesqs3() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
