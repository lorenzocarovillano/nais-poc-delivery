package it.accenture.jnais.ws.enums;

/**Original name: IDSV0301-ACTION-TYPE<br>
 * Variable: IDSV0301-ACTION-TYPE from copybook IDSV0301<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0301ActionType {

    //==== PROPERTIES ====
    private char value = 'E';
    public static final char DELETE_ACTION = 'D';
    public static final char APPEND_ACTION = 'E';

    //==== METHODS ====
    public void setActionType(char actionType) {
        this.value = actionType;
    }

    public char getActionType() {
        return this.value;
    }

    public boolean isDeleteAction() {
        return value == DELETE_ACTION;
    }

    public void setIdsv0301DeleteAction() {
        value = DELETE_ACTION;
    }

    public void setAppendAction() {
        value = APPEND_ACTION;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ACTION_TYPE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
