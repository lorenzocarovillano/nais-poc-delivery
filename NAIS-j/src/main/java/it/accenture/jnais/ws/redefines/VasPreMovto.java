package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: VAS-PRE-MOVTO<br>
 * Variable: VAS-PRE-MOVTO from program LCCS0450<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VasPreMovto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public VasPreMovto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VAS_PRE_MOVTO;
    }

    public void setVasPreMovto(AfDecimal vasPreMovto) {
        writeDecimalAsPacked(Pos.VAS_PRE_MOVTO, vasPreMovto.copy());
    }

    public void setVasPreMovtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VAS_PRE_MOVTO, Pos.VAS_PRE_MOVTO);
    }

    /**Original name: VAS-PRE-MOVTO<br>*/
    public AfDecimal getVasPreMovto() {
        return readPackedAsDecimal(Pos.VAS_PRE_MOVTO, Len.Int.VAS_PRE_MOVTO, Len.Fract.VAS_PRE_MOVTO);
    }

    public byte[] getVasPreMovtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VAS_PRE_MOVTO, Pos.VAS_PRE_MOVTO);
        return buffer;
    }

    public void setVasPreMovtoNull(String vasPreMovtoNull) {
        writeString(Pos.VAS_PRE_MOVTO_NULL, vasPreMovtoNull, Len.VAS_PRE_MOVTO_NULL);
    }

    /**Original name: VAS-PRE-MOVTO-NULL<br>*/
    public String getVasPreMovtoNull() {
        return readString(Pos.VAS_PRE_MOVTO_NULL, Len.VAS_PRE_MOVTO_NULL);
    }

    public String getVasPreMovtoNullFormatted() {
        return Functions.padBlanks(getVasPreMovtoNull(), Len.VAS_PRE_MOVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VAS_PRE_MOVTO = 1;
        public static final int VAS_PRE_MOVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int VAS_PRE_MOVTO = 8;
        public static final int VAS_PRE_MOVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int VAS_PRE_MOVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int VAS_PRE_MOVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
