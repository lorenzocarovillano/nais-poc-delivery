package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L30-ID-MOVI-CHIU<br>
 * Variable: L30-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L30IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L30IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L30_ID_MOVI_CHIU;
    }

    public void setL30IdMoviChiu(int l30IdMoviChiu) {
        writeIntAsPacked(Pos.L30_ID_MOVI_CHIU, l30IdMoviChiu, Len.Int.L30_ID_MOVI_CHIU);
    }

    public void setL30IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L30_ID_MOVI_CHIU, Pos.L30_ID_MOVI_CHIU);
    }

    /**Original name: L30-ID-MOVI-CHIU<br>*/
    public int getL30IdMoviChiu() {
        return readPackedAsInt(Pos.L30_ID_MOVI_CHIU, Len.Int.L30_ID_MOVI_CHIU);
    }

    public byte[] getL30IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L30_ID_MOVI_CHIU, Pos.L30_ID_MOVI_CHIU);
        return buffer;
    }

    public void setL30IdMoviChiuNull(String l30IdMoviChiuNull) {
        writeString(Pos.L30_ID_MOVI_CHIU_NULL, l30IdMoviChiuNull, Len.L30_ID_MOVI_CHIU_NULL);
    }

    /**Original name: L30-ID-MOVI-CHIU-NULL<br>*/
    public String getL30IdMoviChiuNull() {
        return readString(Pos.L30_ID_MOVI_CHIU_NULL, Len.L30_ID_MOVI_CHIU_NULL);
    }

    public String getL30IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getL30IdMoviChiuNull(), Len.L30_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L30_ID_MOVI_CHIU = 1;
        public static final int L30_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L30_ID_MOVI_CHIU = 5;
        public static final int L30_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L30_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
