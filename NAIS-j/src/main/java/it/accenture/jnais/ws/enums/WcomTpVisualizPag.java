package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WCOM-TP-VISUALIZ-PAG<br>
 * Variable: WCOM-TP-VISUALIZ-PAG from copybook LCCC0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WcomTpVisualizPag {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char MODIFICA = 'M';
    public static final char CONSULTA = 'C';

    //==== METHODS ====
    public void setTpVisualizPag(char tpVisualizPag) {
        this.value = tpVisualizPag;
    }

    public char getTpVisualizPag() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TP_VISUALIZ_PAG = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
