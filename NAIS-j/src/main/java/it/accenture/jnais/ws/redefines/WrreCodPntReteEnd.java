package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-COD-PNT-RETE-END<br>
 * Variable: WRRE-COD-PNT-RETE-END from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreCodPntReteEnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreCodPntReteEnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_COD_PNT_RETE_END;
    }

    public void setWrreCodPntReteEnd(long wrreCodPntReteEnd) {
        writeLongAsPacked(Pos.WRRE_COD_PNT_RETE_END, wrreCodPntReteEnd, Len.Int.WRRE_COD_PNT_RETE_END);
    }

    public void setWrreCodPntReteEndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_COD_PNT_RETE_END, Pos.WRRE_COD_PNT_RETE_END);
    }

    /**Original name: WRRE-COD-PNT-RETE-END<br>*/
    public long getWrreCodPntReteEnd() {
        return readPackedAsLong(Pos.WRRE_COD_PNT_RETE_END, Len.Int.WRRE_COD_PNT_RETE_END);
    }

    public byte[] getWrreCodPntReteEndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_COD_PNT_RETE_END, Pos.WRRE_COD_PNT_RETE_END);
        return buffer;
    }

    public void initWrreCodPntReteEndSpaces() {
        fill(Pos.WRRE_COD_PNT_RETE_END, Len.WRRE_COD_PNT_RETE_END, Types.SPACE_CHAR);
    }

    public void setWrreCodPntReteEndNull(String wrreCodPntReteEndNull) {
        writeString(Pos.WRRE_COD_PNT_RETE_END_NULL, wrreCodPntReteEndNull, Len.WRRE_COD_PNT_RETE_END_NULL);
    }

    /**Original name: WRRE-COD-PNT-RETE-END-NULL<br>*/
    public String getWrreCodPntReteEndNull() {
        return readString(Pos.WRRE_COD_PNT_RETE_END_NULL, Len.WRRE_COD_PNT_RETE_END_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_COD_PNT_RETE_END = 1;
        public static final int WRRE_COD_PNT_RETE_END_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_COD_PNT_RETE_END = 6;
        public static final int WRRE_COD_PNT_RETE_END_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_COD_PNT_RETE_END = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
