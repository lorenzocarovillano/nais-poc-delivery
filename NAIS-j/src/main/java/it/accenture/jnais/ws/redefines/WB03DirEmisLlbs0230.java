package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-DIR-EMIS<br>
 * Variable: W-B03-DIR-EMIS from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DirEmisLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DirEmisLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DIR_EMIS;
    }

    public void setwB03DirEmis(AfDecimal wB03DirEmis) {
        writeDecimalAsPacked(Pos.W_B03_DIR_EMIS, wB03DirEmis.copy());
    }

    /**Original name: W-B03-DIR-EMIS<br>*/
    public AfDecimal getwB03DirEmis() {
        return readPackedAsDecimal(Pos.W_B03_DIR_EMIS, Len.Int.W_B03_DIR_EMIS, Len.Fract.W_B03_DIR_EMIS);
    }

    public byte[] getwB03DirEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DIR_EMIS, Pos.W_B03_DIR_EMIS);
        return buffer;
    }

    public void setwB03DirEmisNull(String wB03DirEmisNull) {
        writeString(Pos.W_B03_DIR_EMIS_NULL, wB03DirEmisNull, Len.W_B03_DIR_EMIS_NULL);
    }

    /**Original name: W-B03-DIR-EMIS-NULL<br>*/
    public String getwB03DirEmisNull() {
        return readString(Pos.W_B03_DIR_EMIS_NULL, Len.W_B03_DIR_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DIR_EMIS = 1;
        public static final int W_B03_DIR_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DIR_EMIS = 8;
        public static final int W_B03_DIR_EMIS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_DIR_EMIS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DIR_EMIS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
