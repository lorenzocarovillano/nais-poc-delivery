package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL30-ID-MOVI-CHIU<br>
 * Variable: WL30-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl30IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl30IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL30_ID_MOVI_CHIU;
    }

    public void setWl30IdMoviChiu(int wl30IdMoviChiu) {
        writeIntAsPacked(Pos.WL30_ID_MOVI_CHIU, wl30IdMoviChiu, Len.Int.WL30_ID_MOVI_CHIU);
    }

    public void setWl30IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL30_ID_MOVI_CHIU, Pos.WL30_ID_MOVI_CHIU);
    }

    /**Original name: WL30-ID-MOVI-CHIU<br>*/
    public int getWl30IdMoviChiu() {
        return readPackedAsInt(Pos.WL30_ID_MOVI_CHIU, Len.Int.WL30_ID_MOVI_CHIU);
    }

    public byte[] getWl30IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL30_ID_MOVI_CHIU, Pos.WL30_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWl30IdMoviChiuSpaces() {
        fill(Pos.WL30_ID_MOVI_CHIU, Len.WL30_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWl30IdMoviChiuNull(String wl30IdMoviChiuNull) {
        writeString(Pos.WL30_ID_MOVI_CHIU_NULL, wl30IdMoviChiuNull, Len.WL30_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WL30-ID-MOVI-CHIU-NULL<br>*/
    public String getWl30IdMoviChiuNull() {
        return readString(Pos.WL30_ID_MOVI_CHIU_NULL, Len.WL30_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL30_ID_MOVI_CHIU = 1;
        public static final int WL30_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL30_ID_MOVI_CHIU = 5;
        public static final int WL30_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL30_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
