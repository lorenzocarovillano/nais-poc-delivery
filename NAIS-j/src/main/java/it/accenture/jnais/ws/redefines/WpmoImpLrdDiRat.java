package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-IMP-LRD-DI-RAT<br>
 * Variable: WPMO-IMP-LRD-DI-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoImpLrdDiRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoImpLrdDiRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_IMP_LRD_DI_RAT;
    }

    public void setWpmoImpLrdDiRat(AfDecimal wpmoImpLrdDiRat) {
        writeDecimalAsPacked(Pos.WPMO_IMP_LRD_DI_RAT, wpmoImpLrdDiRat.copy());
    }

    public void setWpmoImpLrdDiRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_IMP_LRD_DI_RAT, Pos.WPMO_IMP_LRD_DI_RAT);
    }

    /**Original name: WPMO-IMP-LRD-DI-RAT<br>*/
    public AfDecimal getWpmoImpLrdDiRat() {
        return readPackedAsDecimal(Pos.WPMO_IMP_LRD_DI_RAT, Len.Int.WPMO_IMP_LRD_DI_RAT, Len.Fract.WPMO_IMP_LRD_DI_RAT);
    }

    public byte[] getWpmoImpLrdDiRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_IMP_LRD_DI_RAT, Pos.WPMO_IMP_LRD_DI_RAT);
        return buffer;
    }

    public void initWpmoImpLrdDiRatSpaces() {
        fill(Pos.WPMO_IMP_LRD_DI_RAT, Len.WPMO_IMP_LRD_DI_RAT, Types.SPACE_CHAR);
    }

    public void setWpmoImpLrdDiRatNull(String wpmoImpLrdDiRatNull) {
        writeString(Pos.WPMO_IMP_LRD_DI_RAT_NULL, wpmoImpLrdDiRatNull, Len.WPMO_IMP_LRD_DI_RAT_NULL);
    }

    /**Original name: WPMO-IMP-LRD-DI-RAT-NULL<br>*/
    public String getWpmoImpLrdDiRatNull() {
        return readString(Pos.WPMO_IMP_LRD_DI_RAT_NULL, Len.WPMO_IMP_LRD_DI_RAT_NULL);
    }

    public String getWpmoImpLrdDiRatNullFormatted() {
        return Functions.padBlanks(getWpmoImpLrdDiRatNull(), Len.WPMO_IMP_LRD_DI_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_LRD_DI_RAT = 1;
        public static final int WPMO_IMP_LRD_DI_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_LRD_DI_RAT = 8;
        public static final int WPMO_IMP_LRD_DI_RAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_LRD_DI_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_LRD_DI_RAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
