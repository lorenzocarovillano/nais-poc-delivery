package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCA-VAL-TS<br>
 * Variable: PCA-VAL-TS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcaValTs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcaValTs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCA_VAL_TS;
    }

    public void setPcaValTs(AfDecimal pcaValTs) {
        writeDecimalAsPacked(Pos.PCA_VAL_TS, pcaValTs.copy());
    }

    public void setPcaValTsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCA_VAL_TS, Pos.PCA_VAL_TS);
    }

    /**Original name: PCA-VAL-TS<br>*/
    public AfDecimal getPcaValTs() {
        return readPackedAsDecimal(Pos.PCA_VAL_TS, Len.Int.PCA_VAL_TS, Len.Fract.PCA_VAL_TS);
    }

    public byte[] getPcaValTsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCA_VAL_TS, Pos.PCA_VAL_TS);
        return buffer;
    }

    public void setPcaValTsNull(String pcaValTsNull) {
        writeString(Pos.PCA_VAL_TS_NULL, pcaValTsNull, Len.PCA_VAL_TS_NULL);
    }

    /**Original name: PCA-VAL-TS-NULL<br>*/
    public String getPcaValTsNull() {
        return readString(Pos.PCA_VAL_TS_NULL, Len.PCA_VAL_TS_NULL);
    }

    public String getPcaValTsNullFormatted() {
        return Functions.padBlanks(getPcaValTsNull(), Len.PCA_VAL_TS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCA_VAL_TS = 1;
        public static final int PCA_VAL_TS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCA_VAL_TS = 8;
        public static final int PCA_VAL_TS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCA_VAL_TS = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCA_VAL_TS = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
