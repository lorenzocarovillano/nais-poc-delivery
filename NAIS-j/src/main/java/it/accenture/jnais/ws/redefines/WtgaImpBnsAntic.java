package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-BNS-ANTIC<br>
 * Variable: WTGA-IMP-BNS-ANTIC from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpBnsAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpBnsAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_BNS_ANTIC;
    }

    public void setWtgaImpBnsAntic(AfDecimal wtgaImpBnsAntic) {
        writeDecimalAsPacked(Pos.WTGA_IMP_BNS_ANTIC, wtgaImpBnsAntic.copy());
    }

    public void setWtgaImpBnsAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_BNS_ANTIC, Pos.WTGA_IMP_BNS_ANTIC);
    }

    /**Original name: WTGA-IMP-BNS-ANTIC<br>*/
    public AfDecimal getWtgaImpBnsAntic() {
        return readPackedAsDecimal(Pos.WTGA_IMP_BNS_ANTIC, Len.Int.WTGA_IMP_BNS_ANTIC, Len.Fract.WTGA_IMP_BNS_ANTIC);
    }

    public byte[] getWtgaImpBnsAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_BNS_ANTIC, Pos.WTGA_IMP_BNS_ANTIC);
        return buffer;
    }

    public void initWtgaImpBnsAnticSpaces() {
        fill(Pos.WTGA_IMP_BNS_ANTIC, Len.WTGA_IMP_BNS_ANTIC, Types.SPACE_CHAR);
    }

    public void setWtgaImpBnsAnticNull(String wtgaImpBnsAnticNull) {
        writeString(Pos.WTGA_IMP_BNS_ANTIC_NULL, wtgaImpBnsAnticNull, Len.WTGA_IMP_BNS_ANTIC_NULL);
    }

    /**Original name: WTGA-IMP-BNS-ANTIC-NULL<br>*/
    public String getWtgaImpBnsAnticNull() {
        return readString(Pos.WTGA_IMP_BNS_ANTIC_NULL, Len.WTGA_IMP_BNS_ANTIC_NULL);
    }

    public String getWtgaImpBnsAnticNullFormatted() {
        return Functions.padBlanks(getWtgaImpBnsAnticNull(), Len.WTGA_IMP_BNS_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_BNS_ANTIC = 1;
        public static final int WTGA_IMP_BNS_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_BNS_ANTIC = 8;
        public static final int WTGA_IMP_BNS_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_BNS_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_BNS_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
