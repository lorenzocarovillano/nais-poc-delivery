package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WPOL-DT-PROP<br>
 * Variable: WPOL-DT-PROP from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolDtProp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolDtProp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_DT_PROP;
    }

    public void setWpolDtProp(int wpolDtProp) {
        writeIntAsPacked(Pos.WPOL_DT_PROP, wpolDtProp, Len.Int.WPOL_DT_PROP);
    }

    public void setWpolDtPropFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_DT_PROP, Pos.WPOL_DT_PROP);
    }

    /**Original name: WPOL-DT-PROP<br>*/
    public int getWpolDtProp() {
        return readPackedAsInt(Pos.WPOL_DT_PROP, Len.Int.WPOL_DT_PROP);
    }

    public byte[] getWpolDtPropAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_DT_PROP, Pos.WPOL_DT_PROP);
        return buffer;
    }

    public void setWpolDtPropNull(String wpolDtPropNull) {
        writeString(Pos.WPOL_DT_PROP_NULL, wpolDtPropNull, Len.WPOL_DT_PROP_NULL);
    }

    /**Original name: WPOL-DT-PROP-NULL<br>*/
    public String getWpolDtPropNull() {
        return readString(Pos.WPOL_DT_PROP_NULL, Len.WPOL_DT_PROP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_DT_PROP = 1;
        public static final int WPOL_DT_PROP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_DT_PROP = 5;
        public static final int WPOL_DT_PROP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_DT_PROP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
