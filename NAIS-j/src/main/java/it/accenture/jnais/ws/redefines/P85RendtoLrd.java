package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P85-RENDTO-LRD<br>
 * Variable: P85-RENDTO-LRD from program IDBSP850<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P85RendtoLrd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P85RendtoLrd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P85_RENDTO_LRD;
    }

    public void setP85RendtoLrd(AfDecimal p85RendtoLrd) {
        writeDecimalAsPacked(Pos.P85_RENDTO_LRD, p85RendtoLrd.copy());
    }

    public void setP85RendtoLrdFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P85_RENDTO_LRD, Pos.P85_RENDTO_LRD);
    }

    /**Original name: P85-RENDTO-LRD<br>*/
    public AfDecimal getP85RendtoLrd() {
        return readPackedAsDecimal(Pos.P85_RENDTO_LRD, Len.Int.P85_RENDTO_LRD, Len.Fract.P85_RENDTO_LRD);
    }

    public byte[] getP85RendtoLrdAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P85_RENDTO_LRD, Pos.P85_RENDTO_LRD);
        return buffer;
    }

    public void setP85RendtoLrdNull(String p85RendtoLrdNull) {
        writeString(Pos.P85_RENDTO_LRD_NULL, p85RendtoLrdNull, Len.P85_RENDTO_LRD_NULL);
    }

    /**Original name: P85-RENDTO-LRD-NULL<br>*/
    public String getP85RendtoLrdNull() {
        return readString(Pos.P85_RENDTO_LRD_NULL, Len.P85_RENDTO_LRD_NULL);
    }

    public String getP85RendtoLrdNullFormatted() {
        return Functions.padBlanks(getP85RendtoLrdNull(), Len.P85_RENDTO_LRD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P85_RENDTO_LRD = 1;
        public static final int P85_RENDTO_LRD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P85_RENDTO_LRD = 8;
        public static final int P85_RENDTO_LRD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P85_RENDTO_LRD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P85_RENDTO_LRD = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
