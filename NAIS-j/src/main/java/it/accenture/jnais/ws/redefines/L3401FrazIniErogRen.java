package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-FRAZ-INI-EROG-REN<br>
 * Variable: L3401-FRAZ-INI-EROG-REN from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401FrazIniErogRen extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401FrazIniErogRen() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_FRAZ_INI_EROG_REN;
    }

    public void setL3401FrazIniErogRen(int l3401FrazIniErogRen) {
        writeIntAsPacked(Pos.L3401_FRAZ_INI_EROG_REN, l3401FrazIniErogRen, Len.Int.L3401_FRAZ_INI_EROG_REN);
    }

    public void setL3401FrazIniErogRenFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_FRAZ_INI_EROG_REN, Pos.L3401_FRAZ_INI_EROG_REN);
    }

    /**Original name: L3401-FRAZ-INI-EROG-REN<br>*/
    public int getL3401FrazIniErogRen() {
        return readPackedAsInt(Pos.L3401_FRAZ_INI_EROG_REN, Len.Int.L3401_FRAZ_INI_EROG_REN);
    }

    public byte[] getL3401FrazIniErogRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_FRAZ_INI_EROG_REN, Pos.L3401_FRAZ_INI_EROG_REN);
        return buffer;
    }

    /**Original name: L3401-FRAZ-INI-EROG-REN-NULL<br>*/
    public String getL3401FrazIniErogRenNull() {
        return readString(Pos.L3401_FRAZ_INI_EROG_REN_NULL, Len.L3401_FRAZ_INI_EROG_REN_NULL);
    }

    public String getL3401FrazIniErogRenNullFormatted() {
        return Functions.padBlanks(getL3401FrazIniErogRenNull(), Len.L3401_FRAZ_INI_EROG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_FRAZ_INI_EROG_REN = 1;
        public static final int L3401_FRAZ_INI_EROG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_FRAZ_INI_EROG_REN = 3;
        public static final int L3401_FRAZ_INI_EROG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_FRAZ_INI_EROG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
