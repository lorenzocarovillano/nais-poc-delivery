package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-NUM-QUO-CDG-FNZ<br>
 * Variable: RDF-NUM-QUO-CDG-FNZ from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfNumQuoCdgFnz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfNumQuoCdgFnz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_NUM_QUO_CDG_FNZ;
    }

    public void setRdfNumQuoCdgFnz(AfDecimal rdfNumQuoCdgFnz) {
        writeDecimalAsPacked(Pos.RDF_NUM_QUO_CDG_FNZ, rdfNumQuoCdgFnz.copy());
    }

    public void setRdfNumQuoCdgFnzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_NUM_QUO_CDG_FNZ, Pos.RDF_NUM_QUO_CDG_FNZ);
    }

    /**Original name: RDF-NUM-QUO-CDG-FNZ<br>*/
    public AfDecimal getRdfNumQuoCdgFnz() {
        return readPackedAsDecimal(Pos.RDF_NUM_QUO_CDG_FNZ, Len.Int.RDF_NUM_QUO_CDG_FNZ, Len.Fract.RDF_NUM_QUO_CDG_FNZ);
    }

    public byte[] getRdfNumQuoCdgFnzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_NUM_QUO_CDG_FNZ, Pos.RDF_NUM_QUO_CDG_FNZ);
        return buffer;
    }

    public void setRdfNumQuoCdgFnzNull(String rdfNumQuoCdgFnzNull) {
        writeString(Pos.RDF_NUM_QUO_CDG_FNZ_NULL, rdfNumQuoCdgFnzNull, Len.RDF_NUM_QUO_CDG_FNZ_NULL);
    }

    /**Original name: RDF-NUM-QUO-CDG-FNZ-NULL<br>*/
    public String getRdfNumQuoCdgFnzNull() {
        return readString(Pos.RDF_NUM_QUO_CDG_FNZ_NULL, Len.RDF_NUM_QUO_CDG_FNZ_NULL);
    }

    public String getRdfNumQuoCdgFnzNullFormatted() {
        return Functions.padBlanks(getRdfNumQuoCdgFnzNull(), Len.RDF_NUM_QUO_CDG_FNZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_NUM_QUO_CDG_FNZ = 1;
        public static final int RDF_NUM_QUO_CDG_FNZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_NUM_QUO_CDG_FNZ = 7;
        public static final int RDF_NUM_QUO_CDG_FNZ_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_NUM_QUO_CDG_FNZ = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RDF_NUM_QUO_CDG_FNZ = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
