package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WVAS-NUM-QUO-LORDE<br>
 * Variable: WVAS-NUM-QUO-LORDE from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasNumQuoLorde extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasNumQuoLorde() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_NUM_QUO_LORDE;
    }

    public void setWvasNumQuoLorde(AfDecimal wvasNumQuoLorde) {
        writeDecimalAsPacked(Pos.WVAS_NUM_QUO_LORDE, wvasNumQuoLorde.copy());
    }

    /**Original name: WVAS-NUM-QUO-LORDE<br>*/
    public AfDecimal getWvasNumQuoLorde() {
        return readPackedAsDecimal(Pos.WVAS_NUM_QUO_LORDE, Len.Int.WVAS_NUM_QUO_LORDE, Len.Fract.WVAS_NUM_QUO_LORDE);
    }

    public void setWvasNumQuoLordeNull(String wvasNumQuoLordeNull) {
        writeString(Pos.WVAS_NUM_QUO_LORDE_NULL, wvasNumQuoLordeNull, Len.WVAS_NUM_QUO_LORDE_NULL);
    }

    /**Original name: WVAS-NUM-QUO-LORDE-NULL<br>*/
    public String getWvasNumQuoLordeNull() {
        return readString(Pos.WVAS_NUM_QUO_LORDE_NULL, Len.WVAS_NUM_QUO_LORDE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_NUM_QUO_LORDE = 1;
        public static final int WVAS_NUM_QUO_LORDE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_NUM_QUO_LORDE = 7;
        public static final int WVAS_NUM_QUO_LORDE_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WVAS_NUM_QUO_LORDE = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_NUM_QUO_LORDE = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
