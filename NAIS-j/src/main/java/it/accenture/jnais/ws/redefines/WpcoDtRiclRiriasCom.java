package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-RICL-RIRIAS-COM<br>
 * Variable: WPCO-DT-RICL-RIRIAS-COM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtRiclRiriasCom extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtRiclRiriasCom() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_RICL_RIRIAS_COM;
    }

    public void setWpcoDtRiclRiriasCom(int wpcoDtRiclRiriasCom) {
        writeIntAsPacked(Pos.WPCO_DT_RICL_RIRIAS_COM, wpcoDtRiclRiriasCom, Len.Int.WPCO_DT_RICL_RIRIAS_COM);
    }

    public void setDpcoDtRiclRiriasComFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_RICL_RIRIAS_COM, Pos.WPCO_DT_RICL_RIRIAS_COM);
    }

    /**Original name: WPCO-DT-RICL-RIRIAS-COM<br>*/
    public int getWpcoDtRiclRiriasCom() {
        return readPackedAsInt(Pos.WPCO_DT_RICL_RIRIAS_COM, Len.Int.WPCO_DT_RICL_RIRIAS_COM);
    }

    public byte[] getWpcoDtRiclRiriasComAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_RICL_RIRIAS_COM, Pos.WPCO_DT_RICL_RIRIAS_COM);
        return buffer;
    }

    public void setWpcoDtRiclRiriasComNull(String wpcoDtRiclRiriasComNull) {
        writeString(Pos.WPCO_DT_RICL_RIRIAS_COM_NULL, wpcoDtRiclRiriasComNull, Len.WPCO_DT_RICL_RIRIAS_COM_NULL);
    }

    /**Original name: WPCO-DT-RICL-RIRIAS-COM-NULL<br>*/
    public String getWpcoDtRiclRiriasComNull() {
        return readString(Pos.WPCO_DT_RICL_RIRIAS_COM_NULL, Len.WPCO_DT_RICL_RIRIAS_COM_NULL);
    }

    public String getWpcoDtRiclRiriasComNullFormatted() {
        return Functions.padBlanks(getWpcoDtRiclRiriasComNull(), Len.WPCO_DT_RICL_RIRIAS_COM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_RICL_RIRIAS_COM = 1;
        public static final int WPCO_DT_RICL_RIRIAS_COM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_RICL_RIRIAS_COM = 5;
        public static final int WPCO_DT_RICL_RIRIAS_COM_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_RICL_RIRIAS_COM = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
