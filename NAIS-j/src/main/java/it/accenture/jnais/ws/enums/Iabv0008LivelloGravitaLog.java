package it.accenture.jnais.ws.enums;

/**Original name: IABV0008-LIVELLO-GRAVITA-LOG<br>
 * Variable: IABV0008-LIVELLO-GRAVITA-LOG from copybook IABV0008<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0008LivelloGravitaLog {

    //==== PROPERTIES ====
    private char value = '1';
    public static final char UNO = '1';
    public static final char DUE = '2';
    public static final char TRE = '3';
    public static final char QUATTRO = '4';

    //==== METHODS ====
    public char getLivelloGravitaLog() {
        return this.value;
    }
}
