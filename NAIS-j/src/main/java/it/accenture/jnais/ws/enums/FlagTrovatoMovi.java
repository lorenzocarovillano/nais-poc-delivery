package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: FLAG-TROVATO-MOVI<br>
 * Variable: FLAG-TROVATO-MOVI from program LRGS0660<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTrovatoMovi {

    //==== PROPERTIES ====
    private char value = Types.SPACE_CHAR;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagTrovatoMovi(char flagTrovatoMovi) {
        this.value = flagTrovatoMovi;
    }

    public char getFlagTrovatoMovi() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
