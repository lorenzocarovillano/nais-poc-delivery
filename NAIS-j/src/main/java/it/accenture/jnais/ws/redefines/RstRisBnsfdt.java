package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-BNSFDT<br>
 * Variable: RST-RIS-BNSFDT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisBnsfdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisBnsfdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_BNSFDT;
    }

    public void setRstRisBnsfdt(AfDecimal rstRisBnsfdt) {
        writeDecimalAsPacked(Pos.RST_RIS_BNSFDT, rstRisBnsfdt.copy());
    }

    public void setRstRisBnsfdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_BNSFDT, Pos.RST_RIS_BNSFDT);
    }

    /**Original name: RST-RIS-BNSFDT<br>*/
    public AfDecimal getRstRisBnsfdt() {
        return readPackedAsDecimal(Pos.RST_RIS_BNSFDT, Len.Int.RST_RIS_BNSFDT, Len.Fract.RST_RIS_BNSFDT);
    }

    public byte[] getRstRisBnsfdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_BNSFDT, Pos.RST_RIS_BNSFDT);
        return buffer;
    }

    public void setRstRisBnsfdtNull(String rstRisBnsfdtNull) {
        writeString(Pos.RST_RIS_BNSFDT_NULL, rstRisBnsfdtNull, Len.RST_RIS_BNSFDT_NULL);
    }

    /**Original name: RST-RIS-BNSFDT-NULL<br>*/
    public String getRstRisBnsfdtNull() {
        return readString(Pos.RST_RIS_BNSFDT_NULL, Len.RST_RIS_BNSFDT_NULL);
    }

    public String getRstRisBnsfdtNullFormatted() {
        return Functions.padBlanks(getRstRisBnsfdtNull(), Len.RST_RIS_BNSFDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_BNSFDT = 1;
        public static final int RST_RIS_BNSFDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_BNSFDT = 8;
        public static final int RST_RIS_BNSFDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_BNSFDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_BNSFDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
