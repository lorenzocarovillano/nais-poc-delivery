package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DT-VARZ-TP-IAS<br>
 * Variable: GRZ-DT-VARZ-TP-IAS from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDtVarzTpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDtVarzTpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DT_VARZ_TP_IAS;
    }

    public void setGrzDtVarzTpIas(int grzDtVarzTpIas) {
        writeIntAsPacked(Pos.GRZ_DT_VARZ_TP_IAS, grzDtVarzTpIas, Len.Int.GRZ_DT_VARZ_TP_IAS);
    }

    public void setGrzDtVarzTpIasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DT_VARZ_TP_IAS, Pos.GRZ_DT_VARZ_TP_IAS);
    }

    /**Original name: GRZ-DT-VARZ-TP-IAS<br>*/
    public int getGrzDtVarzTpIas() {
        return readPackedAsInt(Pos.GRZ_DT_VARZ_TP_IAS, Len.Int.GRZ_DT_VARZ_TP_IAS);
    }

    public byte[] getGrzDtVarzTpIasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DT_VARZ_TP_IAS, Pos.GRZ_DT_VARZ_TP_IAS);
        return buffer;
    }

    public void setGrzDtVarzTpIasNull(String grzDtVarzTpIasNull) {
        writeString(Pos.GRZ_DT_VARZ_TP_IAS_NULL, grzDtVarzTpIasNull, Len.GRZ_DT_VARZ_TP_IAS_NULL);
    }

    /**Original name: GRZ-DT-VARZ-TP-IAS-NULL<br>*/
    public String getGrzDtVarzTpIasNull() {
        return readString(Pos.GRZ_DT_VARZ_TP_IAS_NULL, Len.GRZ_DT_VARZ_TP_IAS_NULL);
    }

    public String getGrzDtVarzTpIasNullFormatted() {
        return Functions.padBlanks(getGrzDtVarzTpIasNull(), Len.GRZ_DT_VARZ_TP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DT_VARZ_TP_IAS = 1;
        public static final int GRZ_DT_VARZ_TP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DT_VARZ_TP_IAS = 5;
        public static final int GRZ_DT_VARZ_TP_IAS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DT_VARZ_TP_IAS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
