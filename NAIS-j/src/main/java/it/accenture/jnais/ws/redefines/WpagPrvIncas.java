package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRV-INCAS<br>
 * Variable: WPAG-PRV-INCAS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrvIncas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrvIncas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRV_INCAS;
    }

    public void setWpagPrvIncas(AfDecimal wpagPrvIncas) {
        writeDecimalAsPacked(Pos.WPAG_PRV_INCAS, wpagPrvIncas.copy());
    }

    public void setWpagPrvIncasFormatted(String wpagPrvIncas) {
        setWpagPrvIncas(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRV_INCAS + Len.Fract.WPAG_PRV_INCAS, Len.Fract.WPAG_PRV_INCAS, wpagPrvIncas));
    }

    public void setWpagPrvIncasFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRV_INCAS, Pos.WPAG_PRV_INCAS);
    }

    /**Original name: WPAG-PRV-INCAS<br>*/
    public AfDecimal getWpagPrvIncas() {
        return readPackedAsDecimal(Pos.WPAG_PRV_INCAS, Len.Int.WPAG_PRV_INCAS, Len.Fract.WPAG_PRV_INCAS);
    }

    public byte[] getWpagPrvIncasAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRV_INCAS, Pos.WPAG_PRV_INCAS);
        return buffer;
    }

    public void initWpagPrvIncasSpaces() {
        fill(Pos.WPAG_PRV_INCAS, Len.WPAG_PRV_INCAS, Types.SPACE_CHAR);
    }

    public void setWpagPrvIncasNull(String wpagPrvIncasNull) {
        writeString(Pos.WPAG_PRV_INCAS_NULL, wpagPrvIncasNull, Len.WPAG_PRV_INCAS_NULL);
    }

    /**Original name: WPAG-PRV-INCAS-NULL<br>*/
    public String getWpagPrvIncasNull() {
        return readString(Pos.WPAG_PRV_INCAS_NULL, Len.WPAG_PRV_INCAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_INCAS = 1;
        public static final int WPAG_PRV_INCAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_INCAS = 8;
        public static final int WPAG_PRV_INCAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_INCAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_INCAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
