package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPRE-DT-CONCS-PREST<br>
 * Variable: WPRE-DT-CONCS-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpreDtConcsPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpreDtConcsPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPRE_DT_CONCS_PREST;
    }

    public void setWpreDtConcsPrest(int wpreDtConcsPrest) {
        writeIntAsPacked(Pos.WPRE_DT_CONCS_PREST, wpreDtConcsPrest, Len.Int.WPRE_DT_CONCS_PREST);
    }

    public void setWpreDtConcsPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPRE_DT_CONCS_PREST, Pos.WPRE_DT_CONCS_PREST);
    }

    /**Original name: WPRE-DT-CONCS-PREST<br>*/
    public int getWpreDtConcsPrest() {
        return readPackedAsInt(Pos.WPRE_DT_CONCS_PREST, Len.Int.WPRE_DT_CONCS_PREST);
    }

    public byte[] getWpreDtConcsPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPRE_DT_CONCS_PREST, Pos.WPRE_DT_CONCS_PREST);
        return buffer;
    }

    public void initWpreDtConcsPrestSpaces() {
        fill(Pos.WPRE_DT_CONCS_PREST, Len.WPRE_DT_CONCS_PREST, Types.SPACE_CHAR);
    }

    public void setWpreDtConcsPrestNull(String wpreDtConcsPrestNull) {
        writeString(Pos.WPRE_DT_CONCS_PREST_NULL, wpreDtConcsPrestNull, Len.WPRE_DT_CONCS_PREST_NULL);
    }

    /**Original name: WPRE-DT-CONCS-PREST-NULL<br>*/
    public String getWpreDtConcsPrestNull() {
        return readString(Pos.WPRE_DT_CONCS_PREST_NULL, Len.WPRE_DT_CONCS_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPRE_DT_CONCS_PREST = 1;
        public static final int WPRE_DT_CONCS_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPRE_DT_CONCS_PREST = 5;
        public static final int WPRE_DT_CONCS_PREST_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPRE_DT_CONCS_PREST = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
