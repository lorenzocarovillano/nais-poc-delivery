package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-TP-ACQS-CNTRT<br>
 * Variable: RRE-TP-ACQS-CNTRT from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreTpAcqsCntrt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreTpAcqsCntrt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_TP_ACQS_CNTRT;
    }

    public void setRreTpAcqsCntrt(int rreTpAcqsCntrt) {
        writeIntAsPacked(Pos.RRE_TP_ACQS_CNTRT, rreTpAcqsCntrt, Len.Int.RRE_TP_ACQS_CNTRT);
    }

    public void setRreTpAcqsCntrtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_TP_ACQS_CNTRT, Pos.RRE_TP_ACQS_CNTRT);
    }

    /**Original name: RRE-TP-ACQS-CNTRT<br>*/
    public int getRreTpAcqsCntrt() {
        return readPackedAsInt(Pos.RRE_TP_ACQS_CNTRT, Len.Int.RRE_TP_ACQS_CNTRT);
    }

    public byte[] getRreTpAcqsCntrtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_TP_ACQS_CNTRT, Pos.RRE_TP_ACQS_CNTRT);
        return buffer;
    }

    public void setRreTpAcqsCntrtNull(String rreTpAcqsCntrtNull) {
        writeString(Pos.RRE_TP_ACQS_CNTRT_NULL, rreTpAcqsCntrtNull, Len.RRE_TP_ACQS_CNTRT_NULL);
    }

    /**Original name: RRE-TP-ACQS-CNTRT-NULL<br>*/
    public String getRreTpAcqsCntrtNull() {
        return readString(Pos.RRE_TP_ACQS_CNTRT_NULL, Len.RRE_TP_ACQS_CNTRT_NULL);
    }

    public String getRreTpAcqsCntrtNullFormatted() {
        return Functions.padBlanks(getRreTpAcqsCntrtNull(), Len.RRE_TP_ACQS_CNTRT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_TP_ACQS_CNTRT = 1;
        public static final int RRE_TP_ACQS_CNTRT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_TP_ACQS_CNTRT = 3;
        public static final int RRE_TP_ACQS_CNTRT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_TP_ACQS_CNTRT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
