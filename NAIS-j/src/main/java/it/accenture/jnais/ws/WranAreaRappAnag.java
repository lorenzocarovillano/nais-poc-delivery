package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WranTabRappAnag;

/**Original name: WRAN-AREA-RAPP-ANAG<br>
 * Variable: WRAN-AREA-RAPP-ANAG from program LVES0245<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WranAreaRappAnag extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_RAPP_ANAG_MAXOCCURS = 100;
    //Original name: WRAN-ELE-RAPP-ANAG-MAX
    private short eleRappAnagMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WRAN-TAB-RAPP-ANAG
    private WranTabRappAnag[] tabRappAnag = new WranTabRappAnag[TAB_RAPP_ANAG_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WranAreaRappAnag() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRAN_AREA_RAPP_ANAG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWranAreaRappAnagBytes(buf);
    }

    public void init() {
        for (int tabRappAnagIdx = 1; tabRappAnagIdx <= TAB_RAPP_ANAG_MAXOCCURS; tabRappAnagIdx++) {
            tabRappAnag[tabRappAnagIdx - 1] = new WranTabRappAnag();
        }
    }

    public String getWranAreaRappAnagFormatted() {
        return MarshalByteExt.bufferToStr(getWranAreaRappAnagBytes());
    }

    public void setWranAreaRappAnagBytes(byte[] buffer) {
        setWranAreaRappAnagBytes(buffer, 1);
    }

    public byte[] getWranAreaRappAnagBytes() {
        byte[] buffer = new byte[Len.WRAN_AREA_RAPP_ANAG];
        return getWranAreaRappAnagBytes(buffer, 1);
    }

    public void setWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        eleRappAnagMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabRappAnag[idx - 1].setWranTabRappAnagBytes(buffer, position);
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
            else {
                tabRappAnag[idx - 1].initWranTabRappAnagSpaces();
                position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
            }
        }
    }

    public byte[] getWranAreaRappAnagBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleRappAnagMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_RAPP_ANAG_MAXOCCURS; idx++) {
            tabRappAnag[idx - 1].getWranTabRappAnagBytes(buffer, position);
            position += WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;
        }
        return buffer;
    }

    public void setEleRappAnagMax(short eleRappAnagMax) {
        this.eleRappAnagMax = eleRappAnagMax;
    }

    public short getEleRappAnagMax() {
        return this.eleRappAnagMax;
    }

    public WranTabRappAnag getTabRappAnag(int idx) {
        return tabRappAnag[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWranAreaRappAnagBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_RAPP_ANAG_MAX = 2;
        public static final int WRAN_AREA_RAPP_ANAG = ELE_RAPP_ANAG_MAX + WranAreaRappAnag.TAB_RAPP_ANAG_MAXOCCURS * WranTabRappAnag.Len.WRAN_TAB_RAPP_ANAG;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
