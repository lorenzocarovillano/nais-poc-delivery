package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-INTR-FRAZ<br>
 * Variable: WTDR-TOT-INTR-FRAZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_INTR_FRAZ;
    }

    public void setWtdrTotIntrFraz(AfDecimal wtdrTotIntrFraz) {
        writeDecimalAsPacked(Pos.WTDR_TOT_INTR_FRAZ, wtdrTotIntrFraz.copy());
    }

    /**Original name: WTDR-TOT-INTR-FRAZ<br>*/
    public AfDecimal getWtdrTotIntrFraz() {
        return readPackedAsDecimal(Pos.WTDR_TOT_INTR_FRAZ, Len.Int.WTDR_TOT_INTR_FRAZ, Len.Fract.WTDR_TOT_INTR_FRAZ);
    }

    public void setWtdrTotIntrFrazNull(String wtdrTotIntrFrazNull) {
        writeString(Pos.WTDR_TOT_INTR_FRAZ_NULL, wtdrTotIntrFrazNull, Len.WTDR_TOT_INTR_FRAZ_NULL);
    }

    /**Original name: WTDR-TOT-INTR-FRAZ-NULL<br>*/
    public String getWtdrTotIntrFrazNull() {
        return readString(Pos.WTDR_TOT_INTR_FRAZ_NULL, Len.WTDR_TOT_INTR_FRAZ_NULL);
    }

    public String getWtdrTotIntrFrazNullFormatted() {
        return Functions.padBlanks(getWtdrTotIntrFrazNull(), Len.WTDR_TOT_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_FRAZ = 1;
        public static final int WTDR_TOT_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_INTR_FRAZ = 8;
        public static final int WTDR_TOT_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
