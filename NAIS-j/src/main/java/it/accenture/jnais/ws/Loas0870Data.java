package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv0015;
import it.accenture.jnais.copy.Ldbv7141;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.ws.enums.WkFineFetch;
import it.accenture.jnais.ws.enums.WkGrzTrovata;
import it.accenture.jnais.ws.enums.WkPmoTrovata;
import it.accenture.jnais.ws.enums.WkTitTrovato;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import it.accenture.jnais.ws.enums.WsTpPerPremio;
import it.accenture.jnais.ws.occurs.WsFg;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0870<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0870Data {

    //==== PROPERTIES ====
    public static final int WS_FG_MAXOCCURS = 300;
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: IDSV0015
    private Idsv0015 idsv0015 = new Idsv0015();
    //Original name: TIT-CONT
    private TitCont titCont = new TitCont();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: LDBV7141
    private Ldbv7141 ldbv7141 = new Ldbv7141();
    //Original name: DETT-TIT-CONT
    private DettTitCont dettTitCont = new DettTitCont();
    /**Original name: WS-TP-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *   AREE TIPOLOGICHE OGGETTO
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    //Original name: IX-INDICI
    private IxIndiciLoas0870 ixIndici = new IxIndiciLoas0870();
    //Original name: WK-INTERVALLO
    private String wkIntervallo = "00";
    //Original name: WK-ID-TRCH-DI-GAR
    private int wkIdTrchDiGar = 0;
    /**Original name: WK-TIT-TROVATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     AREE DI COMODO
	 * ----------------------------------------------------------------*
	 *  --> Ricerca occorrenza Titolo Contabile</pre>*/
    private WkTitTrovato wkTitTrovato = new WkTitTrovato();
    /**Original name: WK-GRZ-TROVATA<br>
	 * <pre> --> Ricerca occorrenza Garanzia</pre>*/
    private WkGrzTrovata wkGrzTrovata = new WkGrzTrovata();
    /**Original name: WK-PMO-TROVATA<br>
	 * <pre> --> Ricerca occorrenza Parametro Garanzia</pre>*/
    private WkPmoTrovata wkPmoTrovata = new WkPmoTrovata();
    /**Original name: WK-FINE-FETCH<br>
	 * <pre> -->  Flag fine fetch</pre>*/
    private WkFineFetch wkFineFetch = new WkFineFetch();
    //Original name: WK-GESTIONE-MSG-ERR
    private WkGestioneMsgErr wkGestioneMsgErr = new WkGestioneMsgErr();
    //Original name: WK-DT-INIZIO-CALCOLO
    private String wkDtInizioCalcolo = "00000000";
    //Original name: WK-DT-A
    private String wkDtA = "00000000";
    //Original name: WK-DT-DA
    private String wkDtDa = "00000000";
    //Original name: WMAX-TAB-TIT
    private short wmaxTabTit = ((short)12);
    /**Original name: WK-PGM<br>
	 * <pre> --> Nome del programma Businesss Service</pre>*/
    private String wkPgm = "LOAS0870";
    /**Original name: WS-TP-PER-PREMIO<br>
	 * <pre> --> Area di appoggio per gestione Management Fee
	 * --> Tipo periodo premio (Garanzia)</pre>*/
    private WsTpPerPremio wsTpPerPremio = new WsTpPerPremio();
    /**Original name: WS-NUM-AA-PAG-PRE<br>
	 * <pre>--> NUMERO ANNI PAGAMENTO PREMIO (GARANZIA)</pre>*/
    private int wsNumAaPagPre = DefaultValues.INT_VAL;
    //Original name: WS-NUM-AA
    private int wsNumAa = DefaultValues.INT_VAL;
    //Original name: WK-TGA-INC
    private char wkTgaInc = DefaultValues.CHAR_VAL;
    public static final char TIT_INCASSATO = 'S';
    public static final char TIT_NON_INCASSATO = 'N';
    //Original name: WS-FG
    private WsFg[] wsFg = new WsFg[WS_FG_MAXOCCURS];
    /**Original name: SW-CUR-DTC<br>
	 * <pre> --> Flag fine cursore dettaglio titolo contabile</pre>*/
    private char swCurDtc = DefaultValues.CHAR_VAL;
    public static final char INIT_CUR_DTC = 'S';
    public static final char FINE_CUR_DTC = 'N';
    /**Original name: SW-TROVATO-DTC<br>
	 * <pre> --> Flag fine cursore dettaglio titolo contabile</pre>*/
    private char swTrovatoDtc = DefaultValues.CHAR_VAL;
    public static final char DETTAGLIO_TROVATO = 'S';
    public static final char DETTAGLIO_NON_TROVATO = 'N';
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = DefaultValues.stringVal(Len.IN_RCODE);
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Loas0870Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wsFgIdx = 1; wsFgIdx <= WS_FG_MAXOCCURS; wsFgIdx++) {
            wsFg[wsFgIdx - 1] = new WsFg();
        }
    }

    public void setWkIntervallo(short wkIntervallo) {
        this.wkIntervallo = NumericDisplay.asString(wkIntervallo, Len.WK_INTERVALLO);
    }

    public short getWkIntervallo() {
        return NumericDisplay.asShort(this.wkIntervallo);
    }

    public String getWkIntervalloFormatted() {
        return this.wkIntervallo;
    }

    public void setWkIdTrchDiGar(int wkIdTrchDiGar) {
        this.wkIdTrchDiGar = wkIdTrchDiGar;
    }

    public int getWkIdTrchDiGar() {
        return this.wkIdTrchDiGar;
    }

    public void setWkDtInizioCalcolo(int wkDtInizioCalcolo) {
        this.wkDtInizioCalcolo = NumericDisplay.asString(wkDtInizioCalcolo, Len.WK_DT_INIZIO_CALCOLO);
    }

    public int getWkDtInizioCalcolo() {
        return NumericDisplay.asInt(this.wkDtInizioCalcolo);
    }

    public String getWkDtInizioCalcoloFormatted() {
        return this.wkDtInizioCalcolo;
    }

    public void setWkDtA(int wkDtA) {
        this.wkDtA = NumericDisplay.asString(wkDtA, Len.WK_DT_A);
    }

    public void setWkDtAFormatted(String wkDtA) {
        this.wkDtA = Trunc.toUnsignedNumeric(wkDtA, Len.WK_DT_A);
    }

    public int getWkDtA() {
        return NumericDisplay.asInt(this.wkDtA);
    }

    public void setWkDtDa(int wkDtDa) {
        this.wkDtDa = NumericDisplay.asString(wkDtDa, Len.WK_DT_DA);
    }

    public void setWkDtDaFormatted(String wkDtDa) {
        this.wkDtDa = Trunc.toUnsignedNumeric(wkDtDa, Len.WK_DT_DA);
    }

    public int getWkDtDa() {
        return NumericDisplay.asInt(this.wkDtDa);
    }

    public short getWmaxTabTit() {
        return this.wmaxTabTit;
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWsNumAaPagPre(int wsNumAaPagPre) {
        this.wsNumAaPagPre = wsNumAaPagPre;
    }

    public int getWsNumAaPagPre() {
        return this.wsNumAaPagPre;
    }

    public void setWsNumAa(int wsNumAa) {
        this.wsNumAa = wsNumAa;
    }

    public int getWsNumAa() {
        return this.wsNumAa;
    }

    public void setWkTgaInc(char wkTgaInc) {
        this.wkTgaInc = wkTgaInc;
    }

    public char getWkTgaInc() {
        return this.wkTgaInc;
    }

    public boolean isTitNonIncassato() {
        return wkTgaInc == TIT_NON_INCASSATO;
    }

    public void setSwCurDtc(char swCurDtc) {
        this.swCurDtc = swCurDtc;
    }

    public char getSwCurDtc() {
        return this.swCurDtc;
    }

    public void setInitCurDtc() {
        swCurDtc = INIT_CUR_DTC;
    }

    public boolean isFineCurDtc() {
        return swCurDtc == FINE_CUR_DTC;
    }

    public void setSwTrovatoDtc(char swTrovatoDtc) {
        this.swTrovatoDtc = swTrovatoDtc;
    }

    public char getSwTrovatoDtc() {
        return this.swTrovatoDtc;
    }

    public boolean isDettaglioTrovato() {
        return swTrovatoDtc == DETTAGLIO_TROVATO;
    }

    public void setDettaglioTrovato() {
        swTrovatoDtc = DETTAGLIO_TROVATO;
    }

    public void setDettaglioNonTrovato() {
        swTrovatoDtc = DETTAGLIO_NON_TROVATO;
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public DettTitCont getDettTitCont() {
        return dettTitCont;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv0015 getIdsv0015() {
        return idsv0015;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public IxIndiciLoas0870 getIxIndici() {
        return ixIndici;
    }

    public Ldbv7141 getLdbv7141() {
        return ldbv7141;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public TitCont getTitCont() {
        return titCont;
    }

    public WkFineFetch getWkFineFetch() {
        return wkFineFetch;
    }

    public WkGestioneMsgErr getWkGestioneMsgErr() {
        return wkGestioneMsgErr;
    }

    public WkGrzTrovata getWkGrzTrovata() {
        return wkGrzTrovata;
    }

    public WkPmoTrovata getWkPmoTrovata() {
        return wkPmoTrovata;
    }

    public WkTitTrovato getWkTitTrovato() {
        return wkTitTrovato;
    }

    public WsFg getWsFg(int idx) {
        return wsFg[idx - 1];
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    public WsTpPerPremio getWsTpPerPremio() {
        return wsTpPerPremio;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_INTERVALLO = 2;
        public static final int WK_TIT_Z = 9;
        public static final int WK_TGA_Z = 9;
        public static final int WK_DT_INIZIO_CALCOLO = 8;
        public static final int WK_DT_A = 8;
        public static final int WK_DT_DA = 8;
        public static final int WK_APPO_DT_NUM = 8;
        public static final int WK_CURRENT_DATE = 8;
        public static final int WS_NUM_AA = 5;
        public static final int WK_NUM_AA_PAG = 2;
        public static final int GG_DIFF = 5;
        public static final int WK_COM_AA = 4;
        public static final int WK_COM_MM = 2;
        public static final int IN_RCODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
