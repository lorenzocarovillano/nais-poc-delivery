package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-IMPB-ADDIZ-REGION<br>
 * Variable: S089-IMPB-ADDIZ-REGION from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089ImpbAddizRegion extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089ImpbAddizRegion() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_IMPB_ADDIZ_REGION;
    }

    public void setWlquImpbAddizRegion(AfDecimal wlquImpbAddizRegion) {
        writeDecimalAsPacked(Pos.S089_IMPB_ADDIZ_REGION, wlquImpbAddizRegion.copy());
    }

    public void setWlquImpbAddizRegionFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_IMPB_ADDIZ_REGION, Pos.S089_IMPB_ADDIZ_REGION);
    }

    /**Original name: WLQU-IMPB-ADDIZ-REGION<br>*/
    public AfDecimal getWlquImpbAddizRegion() {
        return readPackedAsDecimal(Pos.S089_IMPB_ADDIZ_REGION, Len.Int.WLQU_IMPB_ADDIZ_REGION, Len.Fract.WLQU_IMPB_ADDIZ_REGION);
    }

    public byte[] getWlquImpbAddizRegionAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_IMPB_ADDIZ_REGION, Pos.S089_IMPB_ADDIZ_REGION);
        return buffer;
    }

    public void initWlquImpbAddizRegionSpaces() {
        fill(Pos.S089_IMPB_ADDIZ_REGION, Len.S089_IMPB_ADDIZ_REGION, Types.SPACE_CHAR);
    }

    public void setWlquImpbAddizRegionNull(String wlquImpbAddizRegionNull) {
        writeString(Pos.S089_IMPB_ADDIZ_REGION_NULL, wlquImpbAddizRegionNull, Len.WLQU_IMPB_ADDIZ_REGION_NULL);
    }

    /**Original name: WLQU-IMPB-ADDIZ-REGION-NULL<br>*/
    public String getWlquImpbAddizRegionNull() {
        return readString(Pos.S089_IMPB_ADDIZ_REGION_NULL, Len.WLQU_IMPB_ADDIZ_REGION_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_IMPB_ADDIZ_REGION = 1;
        public static final int S089_IMPB_ADDIZ_REGION_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_IMPB_ADDIZ_REGION = 8;
        public static final int WLQU_IMPB_ADDIZ_REGION_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_ADDIZ_REGION = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_IMPB_ADDIZ_REGION = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
