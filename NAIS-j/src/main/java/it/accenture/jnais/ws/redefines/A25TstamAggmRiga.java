package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: A25-TSTAM-AGGM-RIGA<br>
 * Variable: A25-TSTAM-AGGM-RIGA from program LDBS1300<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class A25TstamAggmRiga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public A25TstamAggmRiga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.A25_TSTAM_AGGM_RIGA;
    }

    public void setA25TstamAggmRiga(long a25TstamAggmRiga) {
        writeLongAsPacked(Pos.A25_TSTAM_AGGM_RIGA, a25TstamAggmRiga, Len.Int.A25_TSTAM_AGGM_RIGA);
    }

    public void setA25TstamAggmRigaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.A25_TSTAM_AGGM_RIGA, Pos.A25_TSTAM_AGGM_RIGA);
    }

    /**Original name: A25-TSTAM-AGGM-RIGA<br>*/
    public long getA25TstamAggmRiga() {
        return readPackedAsLong(Pos.A25_TSTAM_AGGM_RIGA, Len.Int.A25_TSTAM_AGGM_RIGA);
    }

    public byte[] getA25TstamAggmRigaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.A25_TSTAM_AGGM_RIGA, Pos.A25_TSTAM_AGGM_RIGA);
        return buffer;
    }

    public void setA25TstamAggmRigaNull(String a25TstamAggmRigaNull) {
        writeString(Pos.A25_TSTAM_AGGM_RIGA_NULL, a25TstamAggmRigaNull, Len.A25_TSTAM_AGGM_RIGA_NULL);
    }

    /**Original name: A25-TSTAM-AGGM-RIGA-NULL<br>*/
    public String getA25TstamAggmRigaNull() {
        return readString(Pos.A25_TSTAM_AGGM_RIGA_NULL, Len.A25_TSTAM_AGGM_RIGA_NULL);
    }

    public String getA25TstamAggmRigaNullFormatted() {
        return Functions.padBlanks(getA25TstamAggmRigaNull(), Len.A25_TSTAM_AGGM_RIGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int A25_TSTAM_AGGM_RIGA = 1;
        public static final int A25_TSTAM_AGGM_RIGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int A25_TSTAM_AGGM_RIGA = 10;
        public static final int A25_TSTAM_AGGM_RIGA_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A25_TSTAM_AGGM_RIGA = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
