package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABV0002-TIPO-ORDER-BY<br>
 * Variable: IABV0002-TIPO-ORDER-BY from copybook IABV0002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabv0002TipoOrderBy {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.IABV0002_TIPO_ORDER_BY);
    public static final String IB_OGG = "IBO";
    public static final String ID_JOB = "JOB";

    //==== METHODS ====
    public void setIabv0002TipoOrderBy(String iabv0002TipoOrderBy) {
        this.value = Functions.subString(iabv0002TipoOrderBy, Len.IABV0002_TIPO_ORDER_BY);
    }

    public String getIabv0002TipoOrderBy() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IABV0002_TIPO_ORDER_BY = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
