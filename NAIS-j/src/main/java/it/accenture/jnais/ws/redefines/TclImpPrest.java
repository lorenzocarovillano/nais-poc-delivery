package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-PREST<br>
 * Variable: TCL-IMP-PREST from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_PREST;
    }

    public void setTclImpPrest(AfDecimal tclImpPrest) {
        writeDecimalAsPacked(Pos.TCL_IMP_PREST, tclImpPrest.copy());
    }

    public void setTclImpPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_PREST, Pos.TCL_IMP_PREST);
    }

    /**Original name: TCL-IMP-PREST<br>*/
    public AfDecimal getTclImpPrest() {
        return readPackedAsDecimal(Pos.TCL_IMP_PREST, Len.Int.TCL_IMP_PREST, Len.Fract.TCL_IMP_PREST);
    }

    public byte[] getTclImpPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_PREST, Pos.TCL_IMP_PREST);
        return buffer;
    }

    public void setTclImpPrestNull(String tclImpPrestNull) {
        writeString(Pos.TCL_IMP_PREST_NULL, tclImpPrestNull, Len.TCL_IMP_PREST_NULL);
    }

    /**Original name: TCL-IMP-PREST-NULL<br>*/
    public String getTclImpPrestNull() {
        return readString(Pos.TCL_IMP_PREST_NULL, Len.TCL_IMP_PREST_NULL);
    }

    public String getTclImpPrestNullFormatted() {
        return Functions.padBlanks(getTclImpPrestNull(), Len.TCL_IMP_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_PREST = 1;
        public static final int TCL_IMP_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_PREST = 8;
        public static final int TCL_IMP_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
