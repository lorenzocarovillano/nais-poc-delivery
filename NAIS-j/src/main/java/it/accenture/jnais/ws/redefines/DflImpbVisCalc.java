package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-VIS-CALC<br>
 * Variable: DFL-IMPB-VIS-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbVisCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbVisCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_VIS_CALC;
    }

    public void setDflImpbVisCalc(AfDecimal dflImpbVisCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPB_VIS_CALC, dflImpbVisCalc.copy());
    }

    public void setDflImpbVisCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_VIS_CALC, Pos.DFL_IMPB_VIS_CALC);
    }

    /**Original name: DFL-IMPB-VIS-CALC<br>*/
    public AfDecimal getDflImpbVisCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPB_VIS_CALC, Len.Int.DFL_IMPB_VIS_CALC, Len.Fract.DFL_IMPB_VIS_CALC);
    }

    public byte[] getDflImpbVisCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_VIS_CALC, Pos.DFL_IMPB_VIS_CALC);
        return buffer;
    }

    public void setDflImpbVisCalcNull(String dflImpbVisCalcNull) {
        writeString(Pos.DFL_IMPB_VIS_CALC_NULL, dflImpbVisCalcNull, Len.DFL_IMPB_VIS_CALC_NULL);
    }

    /**Original name: DFL-IMPB-VIS-CALC-NULL<br>*/
    public String getDflImpbVisCalcNull() {
        return readString(Pos.DFL_IMPB_VIS_CALC_NULL, Len.DFL_IMPB_VIS_CALC_NULL);
    }

    public String getDflImpbVisCalcNullFormatted() {
        return Functions.padBlanks(getDflImpbVisCalcNull(), Len.DFL_IMPB_VIS_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS_CALC = 1;
        public static final int DFL_IMPB_VIS_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_VIS_CALC = 8;
        public static final int DFL_IMPB_VIS_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_VIS_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
