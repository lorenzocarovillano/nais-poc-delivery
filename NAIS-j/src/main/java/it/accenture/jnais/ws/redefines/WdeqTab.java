package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDEQ-TAB<br>
 * Variable: WDEQ-TAB from program IVVS0211<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_DETT_QUEST_MAXOCCURS = 180;
    public static final char WDEQ_ST_ADD = 'A';
    public static final char WDEQ_ST_MOD = 'M';
    public static final char WDEQ_ST_INV = 'I';
    public static final char WDEQ_ST_DEL = 'D';
    public static final char WDEQ_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public WdeqTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_TAB;
    }

    public String getWdeqTabFormatted() {
        return readFixedString(Pos.WDEQ_TAB, Len.WDEQ_TAB);
    }

    public void setWdeqTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_TAB, Pos.WDEQ_TAB);
    }

    public byte[] getWdeqTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_TAB, Pos.WDEQ_TAB);
        return buffer;
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.wdeqStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: WDEQ-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA DETT_QUEST
	 *    ALIAS DEQ
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.wdeqStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.wdeqIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: WDEQ-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.wdeqIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdDettQuest(int idDettQuestIdx, int idDettQuest) {
        int position = Pos.wdeqIdDettQuest(idDettQuestIdx - 1);
        writeIntAsPacked(position, idDettQuest, Len.Int.ID_DETT_QUEST);
    }

    /**Original name: WDEQ-ID-DETT-QUEST<br>*/
    public int getIdDettQuest(int idDettQuestIdx) {
        int position = Pos.wdeqIdDettQuest(idDettQuestIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_DETT_QUEST);
    }

    public void setIdQuest(int idQuestIdx, int idQuest) {
        int position = Pos.wdeqIdQuest(idQuestIdx - 1);
        writeIntAsPacked(position, idQuest, Len.Int.ID_QUEST);
    }

    /**Original name: WDEQ-ID-QUEST<br>*/
    public int getIdQuest(int idQuestIdx) {
        int position = Pos.wdeqIdQuest(idQuestIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_QUEST);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.wdeqIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: WDEQ-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.wdeqIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.wdeqIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: WDEQ-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.wdeqIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.wdeqDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: WDEQ-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.wdeqDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.wdeqDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: WDEQ-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.wdeqDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.wdeqCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: WDEQ-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.wdeqCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setCodQuest(int codQuestIdx, String codQuest) {
        int position = Pos.wdeqCodQuest(codQuestIdx - 1);
        writeString(position, codQuest, Len.COD_QUEST);
    }

    /**Original name: WDEQ-COD-QUEST<br>*/
    public String getCodQuest(int codQuestIdx) {
        int position = Pos.wdeqCodQuest(codQuestIdx - 1);
        return readString(position, Len.COD_QUEST);
    }

    public void setCodDom(int codDomIdx, String codDom) {
        int position = Pos.wdeqCodDom(codDomIdx - 1);
        writeString(position, codDom, Len.COD_DOM);
    }

    /**Original name: WDEQ-COD-DOM<br>*/
    public String getCodDom(int codDomIdx) {
        int position = Pos.wdeqCodDom(codDomIdx - 1);
        return readString(position, Len.COD_DOM);
    }

    public void setCodDomCollg(int codDomCollgIdx, String codDomCollg) {
        int position = Pos.wdeqCodDomCollg(codDomCollgIdx - 1);
        writeString(position, codDomCollg, Len.COD_DOM_COLLG);
    }

    /**Original name: WDEQ-COD-DOM-COLLG<br>*/
    public String getCodDomCollg(int codDomCollgIdx) {
        int position = Pos.wdeqCodDomCollg(codDomCollgIdx - 1);
        return readString(position, Len.COD_DOM_COLLG);
    }

    public void setRispNum(int rispNumIdx, int rispNum) {
        int position = Pos.wdeqRispNum(rispNumIdx - 1);
        writeIntAsPacked(position, rispNum, Len.Int.RISP_NUM);
    }

    /**Original name: WDEQ-RISP-NUM<br>*/
    public int getRispNum(int rispNumIdx) {
        int position = Pos.wdeqRispNum(rispNumIdx - 1);
        return readPackedAsInt(position, Len.Int.RISP_NUM);
    }

    public void setRispFl(int rispFlIdx, char rispFl) {
        int position = Pos.wdeqRispFl(rispFlIdx - 1);
        writeChar(position, rispFl);
    }

    /**Original name: WDEQ-RISP-FL<br>*/
    public char getRispFl(int rispFlIdx) {
        int position = Pos.wdeqRispFl(rispFlIdx - 1);
        return readChar(position);
    }

    public void setRispTxt(int rispTxtIdx, String rispTxt) {
        int position = Pos.wdeqRispTxt(rispTxtIdx - 1);
        writeString(position, rispTxt, Len.RISP_TXT);
    }

    /**Original name: WDEQ-RISP-TXT<br>*/
    public String getRispTxt(int rispTxtIdx) {
        int position = Pos.wdeqRispTxt(rispTxtIdx - 1);
        return readString(position, Len.RISP_TXT);
    }

    public void setRispTs(int rispTsIdx, AfDecimal rispTs) {
        int position = Pos.wdeqRispTs(rispTsIdx - 1);
        writeDecimalAsPacked(position, rispTs.copy());
    }

    /**Original name: WDEQ-RISP-TS<br>*/
    public AfDecimal getRispTs(int rispTsIdx) {
        int position = Pos.wdeqRispTs(rispTsIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RISP_TS, Len.Fract.RISP_TS);
    }

    public void setRispImp(int rispImpIdx, AfDecimal rispImp) {
        int position = Pos.wdeqRispImp(rispImpIdx - 1);
        writeDecimalAsPacked(position, rispImp.copy());
    }

    /**Original name: WDEQ-RISP-IMP<br>*/
    public AfDecimal getRispImp(int rispImpIdx) {
        int position = Pos.wdeqRispImp(rispImpIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RISP_IMP, Len.Fract.RISP_IMP);
    }

    public void setRispPc(int rispPcIdx, AfDecimal rispPc) {
        int position = Pos.wdeqRispPc(rispPcIdx - 1);
        writeDecimalAsPacked(position, rispPc.copy());
    }

    /**Original name: WDEQ-RISP-PC<br>*/
    public AfDecimal getRispPc(int rispPcIdx) {
        int position = Pos.wdeqRispPc(rispPcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RISP_PC, Len.Fract.RISP_PC);
    }

    public void setRispDt(int rispDtIdx, int rispDt) {
        int position = Pos.wdeqRispDt(rispDtIdx - 1);
        writeIntAsPacked(position, rispDt, Len.Int.RISP_DT);
    }

    /**Original name: WDEQ-RISP-DT<br>*/
    public int getRispDt(int rispDtIdx) {
        int position = Pos.wdeqRispDt(rispDtIdx - 1);
        return readPackedAsInt(position, Len.Int.RISP_DT);
    }

    public void setRispKey(int rispKeyIdx, String rispKey) {
        int position = Pos.wdeqRispKey(rispKeyIdx - 1);
        writeString(position, rispKey, Len.RISP_KEY);
    }

    /**Original name: WDEQ-RISP-KEY<br>*/
    public String getRispKey(int rispKeyIdx) {
        int position = Pos.wdeqRispKey(rispKeyIdx - 1);
        return readString(position, Len.RISP_KEY);
    }

    public void setTpRisp(int tpRispIdx, String tpRisp) {
        int position = Pos.wdeqTpRisp(tpRispIdx - 1);
        writeString(position, tpRisp, Len.TP_RISP);
    }

    /**Original name: WDEQ-TP-RISP<br>*/
    public String getTpRisp(int tpRispIdx) {
        int position = Pos.wdeqTpRisp(tpRispIdx - 1);
        return readString(position, Len.TP_RISP);
    }

    public void setIdCompQuest(int idCompQuestIdx, int idCompQuest) {
        int position = Pos.wdeqIdCompQuest(idCompQuestIdx - 1);
        writeIntAsPacked(position, idCompQuest, Len.Int.ID_COMP_QUEST);
    }

    /**Original name: WDEQ-ID-COMP-QUEST<br>*/
    public int getIdCompQuest(int idCompQuestIdx) {
        int position = Pos.wdeqIdCompQuest(idCompQuestIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_COMP_QUEST);
    }

    public void setValRisp(int valRispIdx, String valRisp) {
        int position = Pos.wdeqValRisp(valRispIdx - 1);
        writeString(position, valRisp, Len.VAL_RISP);
    }

    /**Original name: WDEQ-VAL-RISP<br>*/
    public String getValRisp(int valRispIdx) {
        int position = Pos.wdeqValRisp(valRispIdx - 1);
        return readString(position, Len.VAL_RISP);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.wdeqDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: WDEQ-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.wdeqDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.wdeqDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: WDEQ-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.wdeqDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.wdeqDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: WDEQ-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.wdeqDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.wdeqDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: WDEQ-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.wdeqDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.wdeqDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: WDEQ-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.wdeqDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.wdeqDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: WDEQ-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.wdeqDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.wdeqDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: WDEQ-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.wdeqDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: WDEQ-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_TAB = 1;
        public static final int WDEQ_TAB_R = 1;
        public static final int FLR1 = WDEQ_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int wdeqTabDettQuest(int idx) {
            return WDEQ_TAB + idx * Len.TAB_DETT_QUEST;
        }

        public static int wdeqStatus(int idx) {
            return wdeqTabDettQuest(idx);
        }

        public static int wdeqIdPtf(int idx) {
            return wdeqStatus(idx) + Len.STATUS;
        }

        public static int wdeqDati(int idx) {
            return wdeqIdPtf(idx) + Len.ID_PTF;
        }

        public static int wdeqIdDettQuest(int idx) {
            return wdeqDati(idx);
        }

        public static int wdeqIdQuest(int idx) {
            return wdeqIdDettQuest(idx) + Len.ID_DETT_QUEST;
        }

        public static int wdeqIdMoviCrz(int idx) {
            return wdeqIdQuest(idx) + Len.ID_QUEST;
        }

        public static int wdeqIdMoviChiu(int idx) {
            return wdeqIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int wdeqIdMoviChiuNull(int idx) {
            return wdeqIdMoviChiu(idx);
        }

        public static int wdeqDtIniEff(int idx) {
            return wdeqIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int wdeqDtEndEff(int idx) {
            return wdeqDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int wdeqCodCompAnia(int idx) {
            return wdeqDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int wdeqCodQuest(int idx) {
            return wdeqCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int wdeqCodQuestNull(int idx) {
            return wdeqCodQuest(idx);
        }

        public static int wdeqCodDom(int idx) {
            return wdeqCodQuest(idx) + Len.COD_QUEST;
        }

        public static int wdeqCodDomNull(int idx) {
            return wdeqCodDom(idx);
        }

        public static int wdeqCodDomCollg(int idx) {
            return wdeqCodDom(idx) + Len.COD_DOM;
        }

        public static int wdeqCodDomCollgNull(int idx) {
            return wdeqCodDomCollg(idx);
        }

        public static int wdeqRispNum(int idx) {
            return wdeqCodDomCollg(idx) + Len.COD_DOM_COLLG;
        }

        public static int wdeqRispNumNull(int idx) {
            return wdeqRispNum(idx);
        }

        public static int wdeqRispFl(int idx) {
            return wdeqRispNum(idx) + Len.RISP_NUM;
        }

        public static int wdeqRispFlNull(int idx) {
            return wdeqRispFl(idx);
        }

        public static int wdeqRispTxt(int idx) {
            return wdeqRispFl(idx) + Len.RISP_FL;
        }

        public static int wdeqRispTs(int idx) {
            return wdeqRispTxt(idx) + Len.RISP_TXT;
        }

        public static int wdeqRispTsNull(int idx) {
            return wdeqRispTs(idx);
        }

        public static int wdeqRispImp(int idx) {
            return wdeqRispTs(idx) + Len.RISP_TS;
        }

        public static int wdeqRispImpNull(int idx) {
            return wdeqRispImp(idx);
        }

        public static int wdeqRispPc(int idx) {
            return wdeqRispImp(idx) + Len.RISP_IMP;
        }

        public static int wdeqRispPcNull(int idx) {
            return wdeqRispPc(idx);
        }

        public static int wdeqRispDt(int idx) {
            return wdeqRispPc(idx) + Len.RISP_PC;
        }

        public static int wdeqRispDtNull(int idx) {
            return wdeqRispDt(idx);
        }

        public static int wdeqRispKey(int idx) {
            return wdeqRispDt(idx) + Len.RISP_DT;
        }

        public static int wdeqRispKeyNull(int idx) {
            return wdeqRispKey(idx);
        }

        public static int wdeqTpRisp(int idx) {
            return wdeqRispKey(idx) + Len.RISP_KEY;
        }

        public static int wdeqTpRispNull(int idx) {
            return wdeqTpRisp(idx);
        }

        public static int wdeqIdCompQuest(int idx) {
            return wdeqTpRisp(idx) + Len.TP_RISP;
        }

        public static int wdeqIdCompQuestNull(int idx) {
            return wdeqIdCompQuest(idx);
        }

        public static int wdeqValRisp(int idx) {
            return wdeqIdCompQuest(idx) + Len.ID_COMP_QUEST;
        }

        public static int wdeqDsRiga(int idx) {
            return wdeqValRisp(idx) + Len.VAL_RISP;
        }

        public static int wdeqDsOperSql(int idx) {
            return wdeqDsRiga(idx) + Len.DS_RIGA;
        }

        public static int wdeqDsVer(int idx) {
            return wdeqDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int wdeqDsTsIniCptz(int idx) {
            return wdeqDsVer(idx) + Len.DS_VER;
        }

        public static int wdeqDsTsEndCptz(int idx) {
            return wdeqDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int wdeqDsUtente(int idx) {
            return wdeqDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int wdeqDsStatoElab(int idx) {
            return wdeqDsUtente(idx) + Len.DS_UTENTE;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_DETT_QUEST = 5;
        public static final int ID_QUEST = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_QUEST = 20;
        public static final int COD_DOM = 20;
        public static final int COD_DOM_COLLG = 20;
        public static final int RISP_NUM = 3;
        public static final int RISP_FL = 1;
        public static final int RISP_TXT = 100;
        public static final int RISP_TS = 8;
        public static final int RISP_IMP = 8;
        public static final int RISP_PC = 4;
        public static final int RISP_DT = 5;
        public static final int RISP_KEY = 20;
        public static final int TP_RISP = 2;
        public static final int ID_COMP_QUEST = 5;
        public static final int VAL_RISP = 250;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DATI = ID_DETT_QUEST + ID_QUEST + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + COD_QUEST + COD_DOM + COD_DOM_COLLG + RISP_NUM + RISP_FL + RISP_TXT + RISP_TS + RISP_IMP + RISP_PC + RISP_DT + RISP_KEY + TP_RISP + ID_COMP_QUEST + VAL_RISP + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB;
        public static final int TAB_DETT_QUEST = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 558;
        public static final int WDEQ_TAB = WdeqTab.TAB_DETT_QUEST_MAXOCCURS * TAB_DETT_QUEST;
        public static final int RESTO_TAB = 99882;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_DETT_QUEST = 9;
            public static final int ID_QUEST = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int RISP_NUM = 5;
            public static final int RISP_TS = 5;
            public static final int RISP_IMP = 12;
            public static final int RISP_PC = 3;
            public static final int RISP_DT = 8;
            public static final int ID_COMP_QUEST = 9;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RISP_TS = 9;
            public static final int RISP_IMP = 3;
            public static final int RISP_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
