package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-STOR-I<br>
 * Variable: WPCO-DT-ULT-BOLL-STOR-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollStorI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollStorI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_STOR_I;
    }

    public void setWpcoDtUltBollStorI(int wpcoDtUltBollStorI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_STOR_I, wpcoDtUltBollStorI, Len.Int.WPCO_DT_ULT_BOLL_STOR_I);
    }

    public void setDpcoDtUltBollStorIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_STOR_I, Pos.WPCO_DT_ULT_BOLL_STOR_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-STOR-I<br>*/
    public int getWpcoDtUltBollStorI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_STOR_I, Len.Int.WPCO_DT_ULT_BOLL_STOR_I);
    }

    public byte[] getWpcoDtUltBollStorIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_STOR_I, Pos.WPCO_DT_ULT_BOLL_STOR_I);
        return buffer;
    }

    public void setWpcoDtUltBollStorINull(String wpcoDtUltBollStorINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_STOR_I_NULL, wpcoDtUltBollStorINull, Len.WPCO_DT_ULT_BOLL_STOR_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-STOR-I-NULL<br>*/
    public String getWpcoDtUltBollStorINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_STOR_I_NULL, Len.WPCO_DT_ULT_BOLL_STOR_I_NULL);
    }

    public String getWpcoDtUltBollStorINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollStorINull(), Len.WPCO_DT_ULT_BOLL_STOR_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_STOR_I = 1;
        public static final int WPCO_DT_ULT_BOLL_STOR_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_STOR_I = 5;
        public static final int WPCO_DT_ULT_BOLL_STOR_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_STOR_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
