package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-CALL-CALCOLO<br>
 * Variable: WK-CALL-CALCOLO from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkCallCalcolo {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkCallCalcolo(char wkCallCalcolo) {
        this.value = wkCallCalcolo;
    }

    public char getWkCallCalcolo() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
