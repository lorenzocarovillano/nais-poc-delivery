package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-GG<br>
 * Variable: W-B03-DUR-GG from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurGgLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurGgLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_GG;
    }

    public void setwB03DurGg(int wB03DurGg) {
        writeIntAsPacked(Pos.W_B03_DUR_GG, wB03DurGg, Len.Int.W_B03_DUR_GG);
    }

    /**Original name: W-B03-DUR-GG<br>*/
    public int getwB03DurGg() {
        return readPackedAsInt(Pos.W_B03_DUR_GG, Len.Int.W_B03_DUR_GG);
    }

    public byte[] getwB03DurGgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_GG, Pos.W_B03_DUR_GG);
        return buffer;
    }

    public void setwB03DurGgNull(String wB03DurGgNull) {
        writeString(Pos.W_B03_DUR_GG_NULL, wB03DurGgNull, Len.W_B03_DUR_GG_NULL);
    }

    /**Original name: W-B03-DUR-GG-NULL<br>*/
    public String getwB03DurGgNull() {
        return readString(Pos.W_B03_DUR_GG_NULL, Len.W_B03_DUR_GG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GG = 1;
        public static final int W_B03_DUR_GG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_GG = 3;
        public static final int W_B03_DUR_GG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_GG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
