package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-ANTIDUR-RICOR-PREC<br>
 * Variable: W-B03-ANTIDUR-RICOR-PREC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03AntidurRicorPrecLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03AntidurRicorPrecLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_ANTIDUR_RICOR_PREC;
    }

    public void setwB03AntidurRicorPrec(int wB03AntidurRicorPrec) {
        writeIntAsPacked(Pos.W_B03_ANTIDUR_RICOR_PREC, wB03AntidurRicorPrec, Len.Int.W_B03_ANTIDUR_RICOR_PREC);
    }

    /**Original name: W-B03-ANTIDUR-RICOR-PREC<br>*/
    public int getwB03AntidurRicorPrec() {
        return readPackedAsInt(Pos.W_B03_ANTIDUR_RICOR_PREC, Len.Int.W_B03_ANTIDUR_RICOR_PREC);
    }

    public byte[] getwB03AntidurRicorPrecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_ANTIDUR_RICOR_PREC, Pos.W_B03_ANTIDUR_RICOR_PREC);
        return buffer;
    }

    public void setwB03AntidurRicorPrecNull(String wB03AntidurRicorPrecNull) {
        writeString(Pos.W_B03_ANTIDUR_RICOR_PREC_NULL, wB03AntidurRicorPrecNull, Len.W_B03_ANTIDUR_RICOR_PREC_NULL);
    }

    /**Original name: W-B03-ANTIDUR-RICOR-PREC-NULL<br>*/
    public String getwB03AntidurRicorPrecNull() {
        return readString(Pos.W_B03_ANTIDUR_RICOR_PREC_NULL, Len.W_B03_ANTIDUR_RICOR_PREC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_ANTIDUR_RICOR_PREC = 1;
        public static final int W_B03_ANTIDUR_RICOR_PREC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_ANTIDUR_RICOR_PREC = 3;
        public static final int W_B03_ANTIDUR_RICOR_PREC_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_ANTIDUR_RICOR_PREC = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
