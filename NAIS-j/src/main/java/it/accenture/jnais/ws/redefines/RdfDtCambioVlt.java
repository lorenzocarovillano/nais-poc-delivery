package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RDF-DT-CAMBIO-VLT<br>
 * Variable: RDF-DT-CAMBIO-VLT from program IDBSRDF0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RdfDtCambioVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RdfDtCambioVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RDF_DT_CAMBIO_VLT;
    }

    public void setRdfDtCambioVlt(int rdfDtCambioVlt) {
        writeIntAsPacked(Pos.RDF_DT_CAMBIO_VLT, rdfDtCambioVlt, Len.Int.RDF_DT_CAMBIO_VLT);
    }

    public void setRdfDtCambioVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RDF_DT_CAMBIO_VLT, Pos.RDF_DT_CAMBIO_VLT);
    }

    /**Original name: RDF-DT-CAMBIO-VLT<br>*/
    public int getRdfDtCambioVlt() {
        return readPackedAsInt(Pos.RDF_DT_CAMBIO_VLT, Len.Int.RDF_DT_CAMBIO_VLT);
    }

    public byte[] getRdfDtCambioVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RDF_DT_CAMBIO_VLT, Pos.RDF_DT_CAMBIO_VLT);
        return buffer;
    }

    public void setRdfDtCambioVltNull(String rdfDtCambioVltNull) {
        writeString(Pos.RDF_DT_CAMBIO_VLT_NULL, rdfDtCambioVltNull, Len.RDF_DT_CAMBIO_VLT_NULL);
    }

    /**Original name: RDF-DT-CAMBIO-VLT-NULL<br>*/
    public String getRdfDtCambioVltNull() {
        return readString(Pos.RDF_DT_CAMBIO_VLT_NULL, Len.RDF_DT_CAMBIO_VLT_NULL);
    }

    public String getRdfDtCambioVltNullFormatted() {
        return Functions.padBlanks(getRdfDtCambioVltNull(), Len.RDF_DT_CAMBIO_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RDF_DT_CAMBIO_VLT = 1;
        public static final int RDF_DT_CAMBIO_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RDF_DT_CAMBIO_VLT = 5;
        public static final int RDF_DT_CAMBIO_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RDF_DT_CAMBIO_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
