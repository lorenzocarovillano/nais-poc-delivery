package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.WcomFlModCalcolo;
import it.accenture.jnais.ws.enums.WcomFlRateAccorpate;
import it.accenture.jnais.ws.enums.WcomFrequenza;

/**Original name: WCOM-AREA-LCCC0320<br>
 * Variable: WCOM-AREA-LCCC0320 from program LCCS0320<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WcomAreaLccc0320 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: WCOM-FL-RATE-ACCORPATE
    private WcomFlRateAccorpate flRateAccorpate = new WcomFlRateAccorpate();
    //Original name: WCOM-NUM-RATE-ACCORPATE
    private int numRateAccorpate = DefaultValues.BIN_INT_VAL;
    //Original name: WCOM-FL-MOD-CALCOLO
    private WcomFlModCalcolo flModCalcolo = new WcomFlModCalcolo();
    //Original name: WCOM-DATA-RICOR-SUCC
    private String dataRicorSucc = "00000000";
    //Original name: WCOM-FREQUENZA
    private WcomFrequenza frequenza = new WcomFrequenza();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WCOM_AREA_LCCC0320;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWcomAreaLccc0320Bytes(buf);
    }

    public void setWcomAreaLccc0320Bytes(byte[] buffer) {
        setWcomAreaLccc0320Bytes(buffer, 1);
    }

    public byte[] getWcomAreaLccc0320Bytes() {
        byte[] buffer = new byte[Len.WCOM_AREA_LCCC0320];
        return getWcomAreaLccc0320Bytes(buffer, 1);
    }

    public void setWcomAreaLccc0320Bytes(byte[] buffer, int offset) {
        int position = offset;
        setDatiInputBytes(buffer, position);
    }

    public byte[] getWcomAreaLccc0320Bytes(byte[] buffer, int offset) {
        int position = offset;
        getDatiInputBytes(buffer, position);
        return buffer;
    }

    public void setDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        flRateAccorpate.setFlRateAccorpate(MarshalByte.readString(buffer, position, WcomFlRateAccorpate.Len.FL_RATE_ACCORPATE));
        position += WcomFlRateAccorpate.Len.FL_RATE_ACCORPATE;
        numRateAccorpate = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        setDatiRicorrenzaBytes(buffer, position);
    }

    public byte[] getDatiInputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flRateAccorpate.getFlRateAccorpate(), WcomFlRateAccorpate.Len.FL_RATE_ACCORPATE);
        position += WcomFlRateAccorpate.Len.FL_RATE_ACCORPATE;
        MarshalByte.writeBinaryInt(buffer, position, numRateAccorpate);
        position += Types.INT_SIZE;
        getDatiRicorrenzaBytes(buffer, position);
        return buffer;
    }

    public void setNumRateAccorpate(int numRateAccorpate) {
        this.numRateAccorpate = numRateAccorpate;
    }

    public int getNumRateAccorpate() {
        return this.numRateAccorpate;
    }

    public void setDatiRicorrenzaBytes(byte[] buffer, int offset) {
        int position = offset;
        flModCalcolo.value = MarshalByte.readFixedString(buffer, position, WcomFlModCalcolo.Len.FL_MOD_CALCOLO);
        position += WcomFlModCalcolo.Len.FL_MOD_CALCOLO;
        dataRicorSucc = MarshalByte.readFixedString(buffer, position, Len.DATA_RICOR_SUCC);
        position += Len.DATA_RICOR_SUCC;
        frequenza.value = MarshalByte.readFixedString(buffer, position, WcomFrequenza.Len.FREQUENZA);
    }

    public byte[] getDatiRicorrenzaBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, flModCalcolo.value, WcomFlModCalcolo.Len.FL_MOD_CALCOLO);
        position += WcomFlModCalcolo.Len.FL_MOD_CALCOLO;
        MarshalByte.writeString(buffer, position, dataRicorSucc, Len.DATA_RICOR_SUCC);
        position += Len.DATA_RICOR_SUCC;
        MarshalByte.writeString(buffer, position, frequenza.value, WcomFrequenza.Len.FREQUENZA);
        return buffer;
    }

    public int getDataRicorSucc() {
        return NumericDisplay.asInt(this.dataRicorSucc);
    }

    public WcomFlRateAccorpate getFlRateAccorpate() {
        return flRateAccorpate;
    }

    @Override
    public byte[] serialize() {
        return getWcomAreaLccc0320Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUM_RATE_ACCORPATE = 4;
        public static final int DATA_RICOR_SUCC = 8;
        public static final int DATI_RICORRENZA = WcomFlModCalcolo.Len.FL_MOD_CALCOLO + DATA_RICOR_SUCC + WcomFrequenza.Len.FREQUENZA;
        public static final int DATI_INPUT = WcomFlRateAccorpate.Len.FL_RATE_ACCORPATE + NUM_RATE_ACCORPATE + DATI_RICORRENZA;
        public static final int WCOM_AREA_LCCC0320 = DATI_INPUT;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
