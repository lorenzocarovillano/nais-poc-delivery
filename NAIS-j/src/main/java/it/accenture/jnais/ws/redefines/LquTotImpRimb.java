package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IMP-RIMB<br>
 * Variable: LQU-TOT-IMP-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IMP_RIMB;
    }

    public void setLquTotImpRimb(AfDecimal lquTotImpRimb) {
        writeDecimalAsPacked(Pos.LQU_TOT_IMP_RIMB, lquTotImpRimb.copy());
    }

    public void setLquTotImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IMP_RIMB, Pos.LQU_TOT_IMP_RIMB);
    }

    /**Original name: LQU-TOT-IMP-RIMB<br>*/
    public AfDecimal getLquTotImpRimb() {
        return readPackedAsDecimal(Pos.LQU_TOT_IMP_RIMB, Len.Int.LQU_TOT_IMP_RIMB, Len.Fract.LQU_TOT_IMP_RIMB);
    }

    public byte[] getLquTotImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IMP_RIMB, Pos.LQU_TOT_IMP_RIMB);
        return buffer;
    }

    public void setLquTotImpRimbNull(String lquTotImpRimbNull) {
        writeString(Pos.LQU_TOT_IMP_RIMB_NULL, lquTotImpRimbNull, Len.LQU_TOT_IMP_RIMB_NULL);
    }

    /**Original name: LQU-TOT-IMP-RIMB-NULL<br>*/
    public String getLquTotImpRimbNull() {
        return readString(Pos.LQU_TOT_IMP_RIMB_NULL, Len.LQU_TOT_IMP_RIMB_NULL);
    }

    public String getLquTotImpRimbNullFormatted() {
        return Functions.padBlanks(getLquTotImpRimbNull(), Len.LQU_TOT_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIMB = 1;
        public static final int LQU_TOT_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IMP_RIMB = 8;
        public static final int LQU_TOT_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
