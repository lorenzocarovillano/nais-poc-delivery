package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-RAT-REVOLVING<br>
 * Variable: P67-IMP-RAT-REVOLVING from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpRatRevolving extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpRatRevolving() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_RAT_REVOLVING;
    }

    public void setP67ImpRatRevolving(AfDecimal p67ImpRatRevolving) {
        writeDecimalAsPacked(Pos.P67_IMP_RAT_REVOLVING, p67ImpRatRevolving.copy());
    }

    public void setP67ImpRatRevolvingFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_RAT_REVOLVING, Pos.P67_IMP_RAT_REVOLVING);
    }

    /**Original name: P67-IMP-RAT-REVOLVING<br>*/
    public AfDecimal getP67ImpRatRevolving() {
        return readPackedAsDecimal(Pos.P67_IMP_RAT_REVOLVING, Len.Int.P67_IMP_RAT_REVOLVING, Len.Fract.P67_IMP_RAT_REVOLVING);
    }

    public byte[] getP67ImpRatRevolvingAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_RAT_REVOLVING, Pos.P67_IMP_RAT_REVOLVING);
        return buffer;
    }

    public void setP67ImpRatRevolvingNull(String p67ImpRatRevolvingNull) {
        writeString(Pos.P67_IMP_RAT_REVOLVING_NULL, p67ImpRatRevolvingNull, Len.P67_IMP_RAT_REVOLVING_NULL);
    }

    /**Original name: P67-IMP-RAT-REVOLVING-NULL<br>*/
    public String getP67ImpRatRevolvingNull() {
        return readString(Pos.P67_IMP_RAT_REVOLVING_NULL, Len.P67_IMP_RAT_REVOLVING_NULL);
    }

    public String getP67ImpRatRevolvingNullFormatted() {
        return Functions.padBlanks(getP67ImpRatRevolvingNull(), Len.P67_IMP_RAT_REVOLVING_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_RAT_REVOLVING = 1;
        public static final int P67_IMP_RAT_REVOLVING_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_RAT_REVOLVING = 8;
        public static final int P67_IMP_RAT_REVOLVING_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_RAT_REVOLVING = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_RAT_REVOLVING = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
