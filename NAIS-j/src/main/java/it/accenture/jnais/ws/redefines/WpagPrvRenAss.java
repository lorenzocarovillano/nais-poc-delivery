package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRV-REN-ASS<br>
 * Variable: WPAG-PRV-REN-ASS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrvRenAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrvRenAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRV_REN_ASS;
    }

    public void setWpagPrvRenAss(AfDecimal wpagPrvRenAss) {
        writeDecimalAsPacked(Pos.WPAG_PRV_REN_ASS, wpagPrvRenAss.copy());
    }

    public void setWpagPrvRenAssFormatted(String wpagPrvRenAss) {
        setWpagPrvRenAss(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRV_REN_ASS + Len.Fract.WPAG_PRV_REN_ASS, Len.Fract.WPAG_PRV_REN_ASS, wpagPrvRenAss));
    }

    public void setWpagPrvRenAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRV_REN_ASS, Pos.WPAG_PRV_REN_ASS);
    }

    /**Original name: WPAG-PRV-REN-ASS<br>*/
    public AfDecimal getWpagPrvRenAss() {
        return readPackedAsDecimal(Pos.WPAG_PRV_REN_ASS, Len.Int.WPAG_PRV_REN_ASS, Len.Fract.WPAG_PRV_REN_ASS);
    }

    public byte[] getWpagPrvRenAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRV_REN_ASS, Pos.WPAG_PRV_REN_ASS);
        return buffer;
    }

    public void initWpagPrvRenAssSpaces() {
        fill(Pos.WPAG_PRV_REN_ASS, Len.WPAG_PRV_REN_ASS, Types.SPACE_CHAR);
    }

    public void setWpagPrvRenAssNull(String wpagPrvRenAssNull) {
        writeString(Pos.WPAG_PRV_REN_ASS_NULL, wpagPrvRenAssNull, Len.WPAG_PRV_REN_ASS_NULL);
    }

    /**Original name: WPAG-PRV-REN-ASS-NULL<br>*/
    public String getWpagPrvRenAssNull() {
        return readString(Pos.WPAG_PRV_REN_ASS_NULL, Len.WPAG_PRV_REN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_REN_ASS = 1;
        public static final int WPAG_PRV_REN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRV_REN_ASS = 8;
        public static final int WPAG_PRV_REN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_REN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRV_REN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
