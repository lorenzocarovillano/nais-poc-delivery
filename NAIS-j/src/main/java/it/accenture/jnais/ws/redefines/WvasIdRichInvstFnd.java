package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WVAS-ID-RICH-INVST-FND<br>
 * Variable: WVAS-ID-RICH-INVST-FND from program LVVS0135<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WvasIdRichInvstFnd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WvasIdRichInvstFnd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WVAS_ID_RICH_INVST_FND;
    }

    public void setWvasIdRichInvstFnd(int wvasIdRichInvstFnd) {
        writeIntAsPacked(Pos.WVAS_ID_RICH_INVST_FND, wvasIdRichInvstFnd, Len.Int.WVAS_ID_RICH_INVST_FND);
    }

    /**Original name: WVAS-ID-RICH-INVST-FND<br>*/
    public int getWvasIdRichInvstFnd() {
        return readPackedAsInt(Pos.WVAS_ID_RICH_INVST_FND, Len.Int.WVAS_ID_RICH_INVST_FND);
    }

    public void setWvasIdRichInvstFndNull(String wvasIdRichInvstFndNull) {
        writeString(Pos.WVAS_ID_RICH_INVST_FND_NULL, wvasIdRichInvstFndNull, Len.WVAS_ID_RICH_INVST_FND_NULL);
    }

    /**Original name: WVAS-ID-RICH-INVST-FND-NULL<br>*/
    public String getWvasIdRichInvstFndNull() {
        return readString(Pos.WVAS_ID_RICH_INVST_FND_NULL, Len.WVAS_ID_RICH_INVST_FND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WVAS_ID_RICH_INVST_FND = 1;
        public static final int WVAS_ID_RICH_INVST_FND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WVAS_ID_RICH_INVST_FND = 5;
        public static final int WVAS_ID_RICH_INVST_FND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WVAS_ID_RICH_INVST_FND = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
