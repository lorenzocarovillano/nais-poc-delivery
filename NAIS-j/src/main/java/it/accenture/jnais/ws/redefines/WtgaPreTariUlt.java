package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PRE-TARI-ULT<br>
 * Variable: WTGA-PRE-TARI-ULT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaPreTariUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaPreTariUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PRE_TARI_ULT;
    }

    public void setWtgaPreTariUlt(AfDecimal wtgaPreTariUlt) {
        writeDecimalAsPacked(Pos.WTGA_PRE_TARI_ULT, wtgaPreTariUlt.copy());
    }

    public void setWtgaPreTariUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PRE_TARI_ULT, Pos.WTGA_PRE_TARI_ULT);
    }

    /**Original name: WTGA-PRE-TARI-ULT<br>*/
    public AfDecimal getWtgaPreTariUlt() {
        return readPackedAsDecimal(Pos.WTGA_PRE_TARI_ULT, Len.Int.WTGA_PRE_TARI_ULT, Len.Fract.WTGA_PRE_TARI_ULT);
    }

    public byte[] getWtgaPreTariUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PRE_TARI_ULT, Pos.WTGA_PRE_TARI_ULT);
        return buffer;
    }

    public void initWtgaPreTariUltSpaces() {
        fill(Pos.WTGA_PRE_TARI_ULT, Len.WTGA_PRE_TARI_ULT, Types.SPACE_CHAR);
    }

    public void setWtgaPreTariUltNull(String wtgaPreTariUltNull) {
        writeString(Pos.WTGA_PRE_TARI_ULT_NULL, wtgaPreTariUltNull, Len.WTGA_PRE_TARI_ULT_NULL);
    }

    /**Original name: WTGA-PRE-TARI-ULT-NULL<br>*/
    public String getWtgaPreTariUltNull() {
        return readString(Pos.WTGA_PRE_TARI_ULT_NULL, Len.WTGA_PRE_TARI_ULT_NULL);
    }

    public String getWtgaPreTariUltNullFormatted() {
        return Functions.padBlanks(getWtgaPreTariUltNull(), Len.WTGA_PRE_TARI_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_TARI_ULT = 1;
        public static final int WTGA_PRE_TARI_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PRE_TARI_ULT = 8;
        public static final int WTGA_PRE_TARI_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_TARI_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PRE_TARI_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
