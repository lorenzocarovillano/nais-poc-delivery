package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-IMP-VOLO<br>
 * Variable: TDR-IMP-VOLO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_IMP_VOLO;
    }

    public void setTdrImpVolo(AfDecimal tdrImpVolo) {
        writeDecimalAsPacked(Pos.TDR_IMP_VOLO, tdrImpVolo.copy());
    }

    public void setTdrImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_IMP_VOLO, Pos.TDR_IMP_VOLO);
    }

    /**Original name: TDR-IMP-VOLO<br>*/
    public AfDecimal getTdrImpVolo() {
        return readPackedAsDecimal(Pos.TDR_IMP_VOLO, Len.Int.TDR_IMP_VOLO, Len.Fract.TDR_IMP_VOLO);
    }

    public byte[] getTdrImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_IMP_VOLO, Pos.TDR_IMP_VOLO);
        return buffer;
    }

    public void setTdrImpVoloNull(String tdrImpVoloNull) {
        writeString(Pos.TDR_IMP_VOLO_NULL, tdrImpVoloNull, Len.TDR_IMP_VOLO_NULL);
    }

    /**Original name: TDR-IMP-VOLO-NULL<br>*/
    public String getTdrImpVoloNull() {
        return readString(Pos.TDR_IMP_VOLO_NULL, Len.TDR_IMP_VOLO_NULL);
    }

    public String getTdrImpVoloNullFormatted() {
        return Functions.padBlanks(getTdrImpVoloNull(), Len.TDR_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_IMP_VOLO = 1;
        public static final int TDR_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_IMP_VOLO = 8;
        public static final int TDR_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
