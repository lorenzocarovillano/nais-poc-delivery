package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-FLAG-RICERCA<br>
 * Variable: WS-FLAG-RICERCA from program LVES0269<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFlagRicerca {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setWsFlagRicerca(char wsFlagRicerca) {
        this.value = wsFlagRicerca;
    }

    public char getWsFlagRicerca() {
        return this.value;
    }

    public boolean isTrovato() {
        return value == TROVATO;
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNonTrovato() {
        value = NON_TROVATO;
    }
}
