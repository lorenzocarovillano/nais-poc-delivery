package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTC-PILDI-AA-I<br>
 * Variable: PCO-DT-ULTC-PILDI-AA-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltcPildiAaI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltcPildiAaI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTC_PILDI_AA_I;
    }

    public void setPcoDtUltcPildiAaI(int pcoDtUltcPildiAaI) {
        writeIntAsPacked(Pos.PCO_DT_ULTC_PILDI_AA_I, pcoDtUltcPildiAaI, Len.Int.PCO_DT_ULTC_PILDI_AA_I);
    }

    public void setPcoDtUltcPildiAaIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_AA_I, Pos.PCO_DT_ULTC_PILDI_AA_I);
    }

    /**Original name: PCO-DT-ULTC-PILDI-AA-I<br>*/
    public int getPcoDtUltcPildiAaI() {
        return readPackedAsInt(Pos.PCO_DT_ULTC_PILDI_AA_I, Len.Int.PCO_DT_ULTC_PILDI_AA_I);
    }

    public byte[] getPcoDtUltcPildiAaIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTC_PILDI_AA_I, Pos.PCO_DT_ULTC_PILDI_AA_I);
        return buffer;
    }

    public void initPcoDtUltcPildiAaIHighValues() {
        fill(Pos.PCO_DT_ULTC_PILDI_AA_I, Len.PCO_DT_ULTC_PILDI_AA_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltcPildiAaINull(String pcoDtUltcPildiAaINull) {
        writeString(Pos.PCO_DT_ULTC_PILDI_AA_I_NULL, pcoDtUltcPildiAaINull, Len.PCO_DT_ULTC_PILDI_AA_I_NULL);
    }

    /**Original name: PCO-DT-ULTC-PILDI-AA-I-NULL<br>*/
    public String getPcoDtUltcPildiAaINull() {
        return readString(Pos.PCO_DT_ULTC_PILDI_AA_I_NULL, Len.PCO_DT_ULTC_PILDI_AA_I_NULL);
    }

    public String getPcoDtUltcPildiAaINullFormatted() {
        return Functions.padBlanks(getPcoDtUltcPildiAaINull(), Len.PCO_DT_ULTC_PILDI_AA_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_AA_I = 1;
        public static final int PCO_DT_ULTC_PILDI_AA_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTC_PILDI_AA_I = 5;
        public static final int PCO_DT_ULTC_PILDI_AA_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTC_PILDI_AA_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
