package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-IMPST-SOST-1382011<br>
 * Variable: WBEL-IMPST-SOST-1382011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelImpstSost1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelImpstSost1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_IMPST_SOST1382011;
    }

    public void setWbelImpstSost1382011(AfDecimal wbelImpstSost1382011) {
        writeDecimalAsPacked(Pos.WBEL_IMPST_SOST1382011, wbelImpstSost1382011.copy());
    }

    public void setWbelImpstSost1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_IMPST_SOST1382011, Pos.WBEL_IMPST_SOST1382011);
    }

    /**Original name: WBEL-IMPST-SOST-1382011<br>*/
    public AfDecimal getWbelImpstSost1382011() {
        return readPackedAsDecimal(Pos.WBEL_IMPST_SOST1382011, Len.Int.WBEL_IMPST_SOST1382011, Len.Fract.WBEL_IMPST_SOST1382011);
    }

    public byte[] getWbelImpstSost1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_IMPST_SOST1382011, Pos.WBEL_IMPST_SOST1382011);
        return buffer;
    }

    public void initWbelImpstSost1382011Spaces() {
        fill(Pos.WBEL_IMPST_SOST1382011, Len.WBEL_IMPST_SOST1382011, Types.SPACE_CHAR);
    }

    public void setWbelImpstSost1382011Null(String wbelImpstSost1382011Null) {
        writeString(Pos.WBEL_IMPST_SOST1382011_NULL, wbelImpstSost1382011Null, Len.WBEL_IMPST_SOST1382011_NULL);
    }

    /**Original name: WBEL-IMPST-SOST-1382011-NULL<br>*/
    public String getWbelImpstSost1382011Null() {
        return readString(Pos.WBEL_IMPST_SOST1382011_NULL, Len.WBEL_IMPST_SOST1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_SOST1382011 = 1;
        public static final int WBEL_IMPST_SOST1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_IMPST_SOST1382011 = 8;
        public static final int WBEL_IMPST_SOST1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_SOST1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_IMPST_SOST1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
