package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IIMPST-PRVR-DFZ<br>
 * Variable: WDFL-IIMPST-PRVR-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIimpstPrvrDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIimpstPrvrDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IIMPST_PRVR_DFZ;
    }

    public void setWdflIimpstPrvrDfz(AfDecimal wdflIimpstPrvrDfz) {
        writeDecimalAsPacked(Pos.WDFL_IIMPST_PRVR_DFZ, wdflIimpstPrvrDfz.copy());
    }

    public void setWdflIimpstPrvrDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IIMPST_PRVR_DFZ, Pos.WDFL_IIMPST_PRVR_DFZ);
    }

    /**Original name: WDFL-IIMPST-PRVR-DFZ<br>*/
    public AfDecimal getWdflIimpstPrvrDfz() {
        return readPackedAsDecimal(Pos.WDFL_IIMPST_PRVR_DFZ, Len.Int.WDFL_IIMPST_PRVR_DFZ, Len.Fract.WDFL_IIMPST_PRVR_DFZ);
    }

    public byte[] getWdflIimpstPrvrDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IIMPST_PRVR_DFZ, Pos.WDFL_IIMPST_PRVR_DFZ);
        return buffer;
    }

    public void setWdflIimpstPrvrDfzNull(String wdflIimpstPrvrDfzNull) {
        writeString(Pos.WDFL_IIMPST_PRVR_DFZ_NULL, wdflIimpstPrvrDfzNull, Len.WDFL_IIMPST_PRVR_DFZ_NULL);
    }

    /**Original name: WDFL-IIMPST-PRVR-DFZ-NULL<br>*/
    public String getWdflIimpstPrvrDfzNull() {
        return readString(Pos.WDFL_IIMPST_PRVR_DFZ_NULL, Len.WDFL_IIMPST_PRVR_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST_PRVR_DFZ = 1;
        public static final int WDFL_IIMPST_PRVR_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IIMPST_PRVR_DFZ = 8;
        public static final int WDFL_IIMPST_PRVR_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST_PRVR_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IIMPST_PRVR_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
