package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FINE-FETCH<br>
 * Variable: FINE-FETCH from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FineFetch {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFineFetch(char fineFetch) {
        this.value = fineFetch;
    }

    public char getFineFetch() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public boolean isNo() {
        return value == NO;
    }

    public void setNo() {
        value = NO;
    }
}
