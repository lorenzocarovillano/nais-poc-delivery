package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-CPT-MIN-SCAD<br>
 * Variable: WTGA-CPT-MIN-SCAD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCptMinScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCptMinScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_CPT_MIN_SCAD;
    }

    public void setWtgaCptMinScad(AfDecimal wtgaCptMinScad) {
        writeDecimalAsPacked(Pos.WTGA_CPT_MIN_SCAD, wtgaCptMinScad.copy());
    }

    public void setWtgaCptMinScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_CPT_MIN_SCAD, Pos.WTGA_CPT_MIN_SCAD);
    }

    /**Original name: WTGA-CPT-MIN-SCAD<br>*/
    public AfDecimal getWtgaCptMinScad() {
        return readPackedAsDecimal(Pos.WTGA_CPT_MIN_SCAD, Len.Int.WTGA_CPT_MIN_SCAD, Len.Fract.WTGA_CPT_MIN_SCAD);
    }

    public byte[] getWtgaCptMinScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_CPT_MIN_SCAD, Pos.WTGA_CPT_MIN_SCAD);
        return buffer;
    }

    public void initWtgaCptMinScadSpaces() {
        fill(Pos.WTGA_CPT_MIN_SCAD, Len.WTGA_CPT_MIN_SCAD, Types.SPACE_CHAR);
    }

    public void setWtgaCptMinScadNull(String wtgaCptMinScadNull) {
        writeString(Pos.WTGA_CPT_MIN_SCAD_NULL, wtgaCptMinScadNull, Len.WTGA_CPT_MIN_SCAD_NULL);
    }

    /**Original name: WTGA-CPT-MIN-SCAD-NULL<br>*/
    public String getWtgaCptMinScadNull() {
        return readString(Pos.WTGA_CPT_MIN_SCAD_NULL, Len.WTGA_CPT_MIN_SCAD_NULL);
    }

    public String getWtgaCptMinScadNullFormatted() {
        return Functions.padBlanks(getWtgaCptMinScadNull(), Len.WTGA_CPT_MIN_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_CPT_MIN_SCAD = 1;
        public static final int WTGA_CPT_MIN_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_CPT_MIN_SCAD = 8;
        public static final int WTGA_CPT_MIN_SCAD_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_CPT_MIN_SCAD = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_CPT_MIN_SCAD = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
