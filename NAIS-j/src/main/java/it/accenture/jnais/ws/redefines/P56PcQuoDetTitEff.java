package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P56-PC-QUO-DET-TIT-EFF<br>
 * Variable: P56-PC-QUO-DET-TIT-EFF from program IDBSP560<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P56PcQuoDetTitEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P56PcQuoDetTitEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P56_PC_QUO_DET_TIT_EFF;
    }

    public void setP56PcQuoDetTitEff(AfDecimal p56PcQuoDetTitEff) {
        writeDecimalAsPacked(Pos.P56_PC_QUO_DET_TIT_EFF, p56PcQuoDetTitEff.copy());
    }

    public void setP56PcQuoDetTitEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P56_PC_QUO_DET_TIT_EFF, Pos.P56_PC_QUO_DET_TIT_EFF);
    }

    /**Original name: P56-PC-QUO-DET-TIT-EFF<br>*/
    public AfDecimal getP56PcQuoDetTitEff() {
        return readPackedAsDecimal(Pos.P56_PC_QUO_DET_TIT_EFF, Len.Int.P56_PC_QUO_DET_TIT_EFF, Len.Fract.P56_PC_QUO_DET_TIT_EFF);
    }

    public byte[] getP56PcQuoDetTitEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P56_PC_QUO_DET_TIT_EFF, Pos.P56_PC_QUO_DET_TIT_EFF);
        return buffer;
    }

    public void setP56PcQuoDetTitEffNull(String p56PcQuoDetTitEffNull) {
        writeString(Pos.P56_PC_QUO_DET_TIT_EFF_NULL, p56PcQuoDetTitEffNull, Len.P56_PC_QUO_DET_TIT_EFF_NULL);
    }

    /**Original name: P56-PC-QUO-DET-TIT-EFF-NULL<br>*/
    public String getP56PcQuoDetTitEffNull() {
        return readString(Pos.P56_PC_QUO_DET_TIT_EFF_NULL, Len.P56_PC_QUO_DET_TIT_EFF_NULL);
    }

    public String getP56PcQuoDetTitEffNullFormatted() {
        return Functions.padBlanks(getP56PcQuoDetTitEffNull(), Len.P56_PC_QUO_DET_TIT_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P56_PC_QUO_DET_TIT_EFF = 1;
        public static final int P56_PC_QUO_DET_TIT_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P56_PC_QUO_DET_TIT_EFF = 4;
        public static final int P56_PC_QUO_DET_TIT_EFF_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P56_PC_QUO_DET_TIT_EFF = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P56_PC_QUO_DET_TIT_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
