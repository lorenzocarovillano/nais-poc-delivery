package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-IMPB-IS-K3-ANTIC<br>
 * Variable: WDFA-IMPB-IS-K3-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaImpbIsK3Antic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaImpbIsK3Antic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_IMPB_IS_K3_ANTIC;
    }

    public void setWdfaImpbIsK3Antic(AfDecimal wdfaImpbIsK3Antic) {
        writeDecimalAsPacked(Pos.WDFA_IMPB_IS_K3_ANTIC, wdfaImpbIsK3Antic.copy());
    }

    public void setWdfaImpbIsK3AnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_IMPB_IS_K3_ANTIC, Pos.WDFA_IMPB_IS_K3_ANTIC);
    }

    /**Original name: WDFA-IMPB-IS-K3-ANTIC<br>*/
    public AfDecimal getWdfaImpbIsK3Antic() {
        return readPackedAsDecimal(Pos.WDFA_IMPB_IS_K3_ANTIC, Len.Int.WDFA_IMPB_IS_K3_ANTIC, Len.Fract.WDFA_IMPB_IS_K3_ANTIC);
    }

    public byte[] getWdfaImpbIsK3AnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_IMPB_IS_K3_ANTIC, Pos.WDFA_IMPB_IS_K3_ANTIC);
        return buffer;
    }

    public void setWdfaImpbIsK3AnticNull(String wdfaImpbIsK3AnticNull) {
        writeString(Pos.WDFA_IMPB_IS_K3_ANTIC_NULL, wdfaImpbIsK3AnticNull, Len.WDFA_IMPB_IS_K3_ANTIC_NULL);
    }

    /**Original name: WDFA-IMPB-IS-K3-ANTIC-NULL<br>*/
    public String getWdfaImpbIsK3AnticNull() {
        return readString(Pos.WDFA_IMPB_IS_K3_ANTIC_NULL, Len.WDFA_IMPB_IS_K3_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K3_ANTIC = 1;
        public static final int WDFA_IMPB_IS_K3_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_IMPB_IS_K3_ANTIC = 8;
        public static final int WDFA_IMPB_IS_K3_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K3_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_IMPB_IS_K3_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
