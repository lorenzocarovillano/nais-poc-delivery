package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.Ispc0140CodiceTpPremioP;

/**Original name: ISPC0140-AREA-TP-PREMIO-P<br>
 * Variables: ISPC0140-AREA-TP-PREMIO-P from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140AreaTpPremioP {

    //==== PROPERTIES ====
    public static final int COMPONENTE_P_MAXOCCURS = 100;
    //Original name: ISPC0140-CODICE-TP-PREMIO-P
    private Ispc0140CodiceTpPremioP codiceTpPremioP = new Ispc0140CodiceTpPremioP();
    //Original name: ISPC0140-O-NUM-COMP-MAX-ELE-P
    private String oNumCompMaxEleP = DefaultValues.stringVal(Len.O_NUM_COMP_MAX_ELE_P);
    //Original name: ISPC0140-COMPONENTE-P
    private Ispc0140ComponenteP[] componenteP = new Ispc0140ComponenteP[COMPONENTE_P_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public Ispc0140AreaTpPremioP() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int componentePIdx = 1; componentePIdx <= COMPONENTE_P_MAXOCCURS; componentePIdx++) {
            componenteP[componentePIdx - 1] = new Ispc0140ComponenteP();
        }
    }

    public void setAreaTpPremioPBytes(byte[] buffer, int offset) {
        int position = offset;
        codiceTpPremioP.setCodiceTpPremioP(MarshalByte.readString(buffer, position, Ispc0140CodiceTpPremioP.Len.CODICE_TP_PREMIO_P));
        position += Ispc0140CodiceTpPremioP.Len.CODICE_TP_PREMIO_P;
        oNumCompMaxEleP = MarshalByte.readFixedString(buffer, position, Len.O_NUM_COMP_MAX_ELE_P);
        position += Len.O_NUM_COMP_MAX_ELE_P;
        for (int idx = 1; idx <= COMPONENTE_P_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                componenteP[idx - 1].setComponentePBytes(buffer, position);
                position += Ispc0140ComponenteP.Len.COMPONENTE_P;
            }
            else {
                componenteP[idx - 1].initComponentePSpaces();
                position += Ispc0140ComponenteP.Len.COMPONENTE_P;
            }
        }
    }

    public byte[] getAreaTpPremioPBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codiceTpPremioP.getCodiceTpPremioP(), Ispc0140CodiceTpPremioP.Len.CODICE_TP_PREMIO_P);
        position += Ispc0140CodiceTpPremioP.Len.CODICE_TP_PREMIO_P;
        MarshalByte.writeString(buffer, position, oNumCompMaxEleP, Len.O_NUM_COMP_MAX_ELE_P);
        position += Len.O_NUM_COMP_MAX_ELE_P;
        for (int idx = 1; idx <= COMPONENTE_P_MAXOCCURS; idx++) {
            componenteP[idx - 1].getComponentePBytes(buffer, position);
            position += Ispc0140ComponenteP.Len.COMPONENTE_P;
        }
        return buffer;
    }

    public void initAreaTpPremioPSpaces() {
        codiceTpPremioP.setCodiceTpPremioP("");
        oNumCompMaxEleP = "";
        for (int idx = 1; idx <= COMPONENTE_P_MAXOCCURS; idx++) {
            componenteP[idx - 1].initComponentePSpaces();
        }
    }

    public short getIspc0140ONumCompMaxEleP() {
        return NumericDisplay.asShort(this.oNumCompMaxEleP);
    }

    public Ispc0140CodiceTpPremioP getCodiceTpPremioP() {
        return codiceTpPremioP;
    }

    public Ispc0140ComponenteP getComponenteP(int idx) {
        return componenteP[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int O_NUM_COMP_MAX_ELE_P = 3;
        public static final int AREA_TP_PREMIO_P = Ispc0140CodiceTpPremioP.Len.CODICE_TP_PREMIO_P + O_NUM_COMP_MAX_ELE_P + Ispc0140AreaTpPremioP.COMPONENTE_P_MAXOCCURS * Ispc0140ComponenteP.Len.COMPONENTE_P;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
