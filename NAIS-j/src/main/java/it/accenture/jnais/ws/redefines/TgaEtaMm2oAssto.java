package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-ETA-MM-2O-ASSTO<br>
 * Variable: TGA-ETA-MM-2O-ASSTO from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaEtaMm2oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaEtaMm2oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_ETA_MM2O_ASSTO;
    }

    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        writeShortAsPacked(Pos.TGA_ETA_MM2O_ASSTO, tgaEtaMm2oAssto, Len.Int.TGA_ETA_MM2O_ASSTO);
    }

    public void setTgaEtaMm2oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_ETA_MM2O_ASSTO, Pos.TGA_ETA_MM2O_ASSTO);
    }

    /**Original name: TGA-ETA-MM-2O-ASSTO<br>*/
    public short getTgaEtaMm2oAssto() {
        return readPackedAsShort(Pos.TGA_ETA_MM2O_ASSTO, Len.Int.TGA_ETA_MM2O_ASSTO);
    }

    public byte[] getTgaEtaMm2oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_ETA_MM2O_ASSTO, Pos.TGA_ETA_MM2O_ASSTO);
        return buffer;
    }

    public void setTgaEtaMm2oAsstoNull(String tgaEtaMm2oAsstoNull) {
        writeString(Pos.TGA_ETA_MM2O_ASSTO_NULL, tgaEtaMm2oAsstoNull, Len.TGA_ETA_MM2O_ASSTO_NULL);
    }

    /**Original name: TGA-ETA-MM-2O-ASSTO-NULL<br>*/
    public String getTgaEtaMm2oAsstoNull() {
        return readString(Pos.TGA_ETA_MM2O_ASSTO_NULL, Len.TGA_ETA_MM2O_ASSTO_NULL);
    }

    public String getTgaEtaMm2oAsstoNullFormatted() {
        return Functions.padBlanks(getTgaEtaMm2oAsstoNull(), Len.TGA_ETA_MM2O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_ETA_MM2O_ASSTO = 1;
        public static final int TGA_ETA_MM2O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_ETA_MM2O_ASSTO = 2;
        public static final int TGA_ETA_MM2O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_ETA_MM2O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
