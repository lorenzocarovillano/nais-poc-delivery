package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvade1Lvvs0010;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.ws.enums.WkPrestito;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0101;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0101<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0101Data {

    //==== PROPERTIES ====
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: WK-PRESTITO
    private WkPrestito wkPrestito = new WkPrestito();
    //Original name: WK-APPO-DATA
    private int wkAppoData = 0;
    //Original name: WK-IMPO-DIFF
    private AfDecimal wkImpoDiff = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: LDBVB441
    private Ldbvb441 ldbvb441 = new Ldbvb441();
    //Original name: LDBVB471
    private Ldbvb471 ldbvb471 = new Ldbvb471();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0101 wsMovimento = new WsMovimentoLvvs0101();
    //Original name: DADE-ELE-ADES-MAX
    private short dadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1Lvvs0010 lccvade1 = new Lccvade1Lvvs0010();
    //Original name: PREST
    private PrestIdbspre0 prest = new PrestIdbspre0();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ADE
    private short ixTabAde = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkAppoData(int wkAppoData) {
        this.wkAppoData = wkAppoData;
    }

    public int getWkAppoData() {
        return this.wkAppoData;
    }

    public void setWkImpoDiff(AfDecimal wkImpoDiff) {
        this.wkImpoDiff.assign(wkImpoDiff);
    }

    public AfDecimal getWkImpoDiff() {
        return this.wkImpoDiff.copy();
    }

    public void setDadeAreaAdesFormatted(String data) {
        byte[] buffer = new byte[Len.DADE_AREA_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.DADE_AREA_ADES);
        setDadeAreaAdesBytes(buffer, 1);
    }

    public void setDadeAreaAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDadeTabAdesBytes(buffer, position);
    }

    public void setDadeEleAdesMax(short dadeEleAdesMax) {
        this.dadeEleAdesMax = dadeEleAdesMax;
    }

    public short getDadeEleAdesMax() {
        return this.dadeEleAdesMax;
    }

    public void setDadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1Lvvs0010.Len.Int.ID_PTF, 0));
        position += Lccvade1Lvvs0010.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabAde(short ixTabAde) {
        this.ixTabAde = ixTabAde;
    }

    public short getIxTabAde() {
        return this.ixTabAde;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvade1Lvvs0010 getLccvade1() {
        return lccvade1;
    }

    public Ldbvb441 getLdbvb441() {
        return ldbvb441;
    }

    public Ldbvb471 getLdbvb471() {
        return ldbvb471;
    }

    public PrestIdbspre0 getPrest() {
        return prest;
    }

    public WkPrestito getWkPrestito() {
        return wkPrestito;
    }

    public WsMovimentoLvvs0101 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_APPO_DATA = 8;
        public static final int DADE_ELE_ADES_MAX = 2;
        public static final int DADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1Lvvs0010.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int DADE_AREA_ADES = DADE_ELE_ADES_MAX + DADE_TAB_ADES;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
