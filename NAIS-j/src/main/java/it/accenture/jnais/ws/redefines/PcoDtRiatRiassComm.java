package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-RIAT-RIASS-COMM<br>
 * Variable: PCO-DT-RIAT-RIASS-COMM from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtRiatRiassComm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtRiatRiassComm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_RIAT_RIASS_COMM;
    }

    public void setPcoDtRiatRiassComm(int pcoDtRiatRiassComm) {
        writeIntAsPacked(Pos.PCO_DT_RIAT_RIASS_COMM, pcoDtRiatRiassComm, Len.Int.PCO_DT_RIAT_RIASS_COMM);
    }

    public void setPcoDtRiatRiassCommFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_RIAT_RIASS_COMM, Pos.PCO_DT_RIAT_RIASS_COMM);
    }

    /**Original name: PCO-DT-RIAT-RIASS-COMM<br>*/
    public int getPcoDtRiatRiassComm() {
        return readPackedAsInt(Pos.PCO_DT_RIAT_RIASS_COMM, Len.Int.PCO_DT_RIAT_RIASS_COMM);
    }

    public byte[] getPcoDtRiatRiassCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_RIAT_RIASS_COMM, Pos.PCO_DT_RIAT_RIASS_COMM);
        return buffer;
    }

    public void initPcoDtRiatRiassCommHighValues() {
        fill(Pos.PCO_DT_RIAT_RIASS_COMM, Len.PCO_DT_RIAT_RIASS_COMM, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtRiatRiassCommNull(String pcoDtRiatRiassCommNull) {
        writeString(Pos.PCO_DT_RIAT_RIASS_COMM_NULL, pcoDtRiatRiassCommNull, Len.PCO_DT_RIAT_RIASS_COMM_NULL);
    }

    /**Original name: PCO-DT-RIAT-RIASS-COMM-NULL<br>*/
    public String getPcoDtRiatRiassCommNull() {
        return readString(Pos.PCO_DT_RIAT_RIASS_COMM_NULL, Len.PCO_DT_RIAT_RIASS_COMM_NULL);
    }

    public String getPcoDtRiatRiassCommNullFormatted() {
        return Functions.padBlanks(getPcoDtRiatRiassCommNull(), Len.PCO_DT_RIAT_RIASS_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_RIAT_RIASS_COMM = 1;
        public static final int PCO_DT_RIAT_RIASS_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_RIAT_RIASS_COMM = 5;
        public static final int PCO_DT_RIAT_RIASS_COMM_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_RIAT_RIASS_COMM = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
