package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-PP-INI<br>
 * Variable: WPAG-PRE-PP-INI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPrePpIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPrePpIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_PP_INI;
    }

    public void setWpagPrePpIni(AfDecimal wpagPrePpIni) {
        writeDecimalAsPacked(Pos.WPAG_PRE_PP_INI, wpagPrePpIni.copy());
    }

    public void setWpagPrePpIniFormatted(String wpagPrePpIni) {
        setWpagPrePpIni(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_PP_INI + Len.Fract.WPAG_PRE_PP_INI, Len.Fract.WPAG_PRE_PP_INI, wpagPrePpIni));
    }

    public void setWpagPrePpIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_PP_INI, Pos.WPAG_PRE_PP_INI);
    }

    /**Original name: WPAG-PRE-PP-INI<br>*/
    public AfDecimal getWpagPrePpIni() {
        return readPackedAsDecimal(Pos.WPAG_PRE_PP_INI, Len.Int.WPAG_PRE_PP_INI, Len.Fract.WPAG_PRE_PP_INI);
    }

    public byte[] getWpagPrePpIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_PP_INI, Pos.WPAG_PRE_PP_INI);
        return buffer;
    }

    public void initWpagPrePpIniSpaces() {
        fill(Pos.WPAG_PRE_PP_INI, Len.WPAG_PRE_PP_INI, Types.SPACE_CHAR);
    }

    public void setWpagPrePpIniNull(String wpagPrePpIniNull) {
        writeString(Pos.WPAG_PRE_PP_INI_NULL, wpagPrePpIniNull, Len.WPAG_PRE_PP_INI_NULL);
    }

    /**Original name: WPAG-PRE-PP-INI-NULL<br>*/
    public String getWpagPrePpIniNull() {
        return readString(Pos.WPAG_PRE_PP_INI_NULL, Len.WPAG_PRE_PP_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_PP_INI = 1;
        public static final int WPAG_PRE_PP_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_PP_INI = 8;
        public static final int WPAG_PRE_PP_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_PP_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_PP_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
