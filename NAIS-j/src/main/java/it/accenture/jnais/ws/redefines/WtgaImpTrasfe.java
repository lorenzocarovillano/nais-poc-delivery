package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-TRASFE<br>
 * Variable: WTGA-IMP-TRASFE from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_TRASFE;
    }

    public void setWtgaImpTrasfe(AfDecimal wtgaImpTrasfe) {
        writeDecimalAsPacked(Pos.WTGA_IMP_TRASFE, wtgaImpTrasfe.copy());
    }

    public void setWtgaImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_TRASFE, Pos.WTGA_IMP_TRASFE);
    }

    /**Original name: WTGA-IMP-TRASFE<br>*/
    public AfDecimal getWtgaImpTrasfe() {
        return readPackedAsDecimal(Pos.WTGA_IMP_TRASFE, Len.Int.WTGA_IMP_TRASFE, Len.Fract.WTGA_IMP_TRASFE);
    }

    public byte[] getWtgaImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_TRASFE, Pos.WTGA_IMP_TRASFE);
        return buffer;
    }

    public void initWtgaImpTrasfeSpaces() {
        fill(Pos.WTGA_IMP_TRASFE, Len.WTGA_IMP_TRASFE, Types.SPACE_CHAR);
    }

    public void setWtgaImpTrasfeNull(String wtgaImpTrasfeNull) {
        writeString(Pos.WTGA_IMP_TRASFE_NULL, wtgaImpTrasfeNull, Len.WTGA_IMP_TRASFE_NULL);
    }

    /**Original name: WTGA-IMP-TRASFE-NULL<br>*/
    public String getWtgaImpTrasfeNull() {
        return readString(Pos.WTGA_IMP_TRASFE_NULL, Len.WTGA_IMP_TRASFE_NULL);
    }

    public String getWtgaImpTrasfeNullFormatted() {
        return Functions.padBlanks(getWtgaImpTrasfeNull(), Len.WTGA_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_TRASFE = 1;
        public static final int WTGA_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_TRASFE = 8;
        public static final int WTGA_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
