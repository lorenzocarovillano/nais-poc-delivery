package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PRE-NET<br>
 * Variable: WTDR-TOT-PRE-NET from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PRE_NET;
    }

    public void setWtdrTotPreNet(AfDecimal wtdrTotPreNet) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PRE_NET, wtdrTotPreNet.copy());
    }

    /**Original name: WTDR-TOT-PRE-NET<br>*/
    public AfDecimal getWtdrTotPreNet() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PRE_NET, Len.Int.WTDR_TOT_PRE_NET, Len.Fract.WTDR_TOT_PRE_NET);
    }

    public void setWtdrTotPreNetNull(String wtdrTotPreNetNull) {
        writeString(Pos.WTDR_TOT_PRE_NET_NULL, wtdrTotPreNetNull, Len.WTDR_TOT_PRE_NET_NULL);
    }

    /**Original name: WTDR-TOT-PRE-NET-NULL<br>*/
    public String getWtdrTotPreNetNull() {
        return readString(Pos.WTDR_TOT_PRE_NET_NULL, Len.WTDR_TOT_PRE_NET_NULL);
    }

    public String getWtdrTotPreNetNullFormatted() {
        return Functions.padBlanks(getWtdrTotPreNetNull(), Len.WTDR_TOT_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_NET = 1;
        public static final int WTDR_TOT_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_NET = 8;
        public static final int WTDR_TOT_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
