package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-CNBT-ECC-4X100-K1<br>
 * Variable: WDFA-CNBT-ECC-4X100-K1 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaCnbtEcc4x100K1 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaCnbtEcc4x100K1() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_CNBT_ECC4X100_K1;
    }

    public void setWdfaCnbtEcc4x100K1(AfDecimal wdfaCnbtEcc4x100K1) {
        writeDecimalAsPacked(Pos.WDFA_CNBT_ECC4X100_K1, wdfaCnbtEcc4x100K1.copy());
    }

    public void setWdfaCnbtEcc4x100K1FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_CNBT_ECC4X100_K1, Pos.WDFA_CNBT_ECC4X100_K1);
    }

    /**Original name: WDFA-CNBT-ECC-4X100-K1<br>*/
    public AfDecimal getWdfaCnbtEcc4x100K1() {
        return readPackedAsDecimal(Pos.WDFA_CNBT_ECC4X100_K1, Len.Int.WDFA_CNBT_ECC4X100_K1, Len.Fract.WDFA_CNBT_ECC4X100_K1);
    }

    public byte[] getWdfaCnbtEcc4x100K1AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_CNBT_ECC4X100_K1, Pos.WDFA_CNBT_ECC4X100_K1);
        return buffer;
    }

    public void setWdfaCnbtEcc4x100K1Null(String wdfaCnbtEcc4x100K1Null) {
        writeString(Pos.WDFA_CNBT_ECC4X100_K1_NULL, wdfaCnbtEcc4x100K1Null, Len.WDFA_CNBT_ECC4X100_K1_NULL);
    }

    /**Original name: WDFA-CNBT-ECC-4X100-K1-NULL<br>*/
    public String getWdfaCnbtEcc4x100K1Null() {
        return readString(Pos.WDFA_CNBT_ECC4X100_K1_NULL, Len.WDFA_CNBT_ECC4X100_K1_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_CNBT_ECC4X100_K1 = 1;
        public static final int WDFA_CNBT_ECC4X100_K1_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_CNBT_ECC4X100_K1 = 8;
        public static final int WDFA_CNBT_ECC4X100_K1_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_CNBT_ECC4X100_K1 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_CNBT_ECC4X100_K1 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
