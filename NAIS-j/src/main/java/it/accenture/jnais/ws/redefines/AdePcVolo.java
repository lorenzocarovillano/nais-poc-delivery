package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-PC-VOLO<br>
 * Variable: ADE-PC-VOLO from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdePcVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdePcVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_PC_VOLO;
    }

    public void setAdePcVolo(AfDecimal adePcVolo) {
        writeDecimalAsPacked(Pos.ADE_PC_VOLO, adePcVolo.copy());
    }

    public void setAdePcVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_PC_VOLO, Pos.ADE_PC_VOLO);
    }

    /**Original name: ADE-PC-VOLO<br>*/
    public AfDecimal getAdePcVolo() {
        return readPackedAsDecimal(Pos.ADE_PC_VOLO, Len.Int.ADE_PC_VOLO, Len.Fract.ADE_PC_VOLO);
    }

    public byte[] getAdePcVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_PC_VOLO, Pos.ADE_PC_VOLO);
        return buffer;
    }

    public void setAdePcVoloNull(String adePcVoloNull) {
        writeString(Pos.ADE_PC_VOLO_NULL, adePcVoloNull, Len.ADE_PC_VOLO_NULL);
    }

    /**Original name: ADE-PC-VOLO-NULL<br>*/
    public String getAdePcVoloNull() {
        return readString(Pos.ADE_PC_VOLO_NULL, Len.ADE_PC_VOLO_NULL);
    }

    public String getAdePcVoloNullFormatted() {
        return Functions.padBlanks(getAdePcVoloNull(), Len.ADE_PC_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_PC_VOLO = 1;
        public static final int ADE_PC_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_PC_VOLO = 4;
        public static final int ADE_PC_VOLO_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_PC_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
