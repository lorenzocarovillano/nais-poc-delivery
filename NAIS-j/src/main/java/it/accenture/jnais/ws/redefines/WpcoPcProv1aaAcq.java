package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-PC-PROV-1AA-ACQ<br>
 * Variable: WPCO-PC-PROV-1AA-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoPcProv1aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoPcProv1aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_PC_PROV1AA_ACQ;
    }

    public void setWpcoPcProv1aaAcq(AfDecimal wpcoPcProv1aaAcq) {
        writeDecimalAsPacked(Pos.WPCO_PC_PROV1AA_ACQ, wpcoPcProv1aaAcq.copy());
    }

    public void setDpcoPcProv1aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_PC_PROV1AA_ACQ, Pos.WPCO_PC_PROV1AA_ACQ);
    }

    /**Original name: WPCO-PC-PROV-1AA-ACQ<br>*/
    public AfDecimal getWpcoPcProv1aaAcq() {
        return readPackedAsDecimal(Pos.WPCO_PC_PROV1AA_ACQ, Len.Int.WPCO_PC_PROV1AA_ACQ, Len.Fract.WPCO_PC_PROV1AA_ACQ);
    }

    public byte[] getWpcoPcProv1aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_PC_PROV1AA_ACQ, Pos.WPCO_PC_PROV1AA_ACQ);
        return buffer;
    }

    public void setWpcoPcProv1aaAcqNull(String wpcoPcProv1aaAcqNull) {
        writeString(Pos.WPCO_PC_PROV1AA_ACQ_NULL, wpcoPcProv1aaAcqNull, Len.WPCO_PC_PROV1AA_ACQ_NULL);
    }

    /**Original name: WPCO-PC-PROV-1AA-ACQ-NULL<br>*/
    public String getWpcoPcProv1aaAcqNull() {
        return readString(Pos.WPCO_PC_PROV1AA_ACQ_NULL, Len.WPCO_PC_PROV1AA_ACQ_NULL);
    }

    public String getWpcoPcProv1aaAcqNullFormatted() {
        return Functions.padBlanks(getWpcoPcProv1aaAcqNull(), Len.WPCO_PC_PROV1AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_PC_PROV1AA_ACQ = 1;
        public static final int WPCO_PC_PROV1AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_PC_PROV1AA_ACQ = 4;
        public static final int WPCO_PC_PROV1AA_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPCO_PC_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_PC_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
