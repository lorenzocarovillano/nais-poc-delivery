package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WPAG-TIPO-NUMERAZIONE<br>
 * Variable: WPAG-TIPO-NUMERAZIONE from copybook IDSV0161<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WpagTipoNumerazione {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char MANUALE = 'M';
    public static final char AUTOMATICA = 'A';

    //==== METHODS ====
    public void setTipoNumerazione(char tipoNumerazione) {
        this.value = tipoNumerazione;
    }

    public char getTipoNumerazione() {
        return this.value;
    }

    public boolean isManuale() {
        return value == MANUALE;
    }

    public void setC161Manuale() {
        value = MANUALE;
    }

    public boolean isAutomatica() {
        return value == AUTOMATICA;
    }

    public void setC161Automatica() {
        value = AUTOMATICA;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int TIPO_NUMERAZIONE = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
