package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ALL-ID-MOVI-CHIU<br>
 * Variable: ALL-ID-MOVI-CHIU from program LCCS1900<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AllIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AllIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ALL_ID_MOVI_CHIU;
    }

    public void setAllIdMoviChiu(int allIdMoviChiu) {
        writeIntAsPacked(Pos.ALL_ID_MOVI_CHIU, allIdMoviChiu, Len.Int.ALL_ID_MOVI_CHIU);
    }

    public void setAllIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ALL_ID_MOVI_CHIU, Pos.ALL_ID_MOVI_CHIU);
    }

    /**Original name: ALL-ID-MOVI-CHIU<br>*/
    public int getAllIdMoviChiu() {
        return readPackedAsInt(Pos.ALL_ID_MOVI_CHIU, Len.Int.ALL_ID_MOVI_CHIU);
    }

    public byte[] getAllIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ALL_ID_MOVI_CHIU, Pos.ALL_ID_MOVI_CHIU);
        return buffer;
    }

    public void setAllIdMoviChiuNull(String allIdMoviChiuNull) {
        writeString(Pos.ALL_ID_MOVI_CHIU_NULL, allIdMoviChiuNull, Len.ALL_ID_MOVI_CHIU_NULL);
    }

    /**Original name: ALL-ID-MOVI-CHIU-NULL<br>*/
    public String getAllIdMoviChiuNull() {
        return readString(Pos.ALL_ID_MOVI_CHIU_NULL, Len.ALL_ID_MOVI_CHIU_NULL);
    }

    public String getAllIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getAllIdMoviChiuNull(), Len.ALL_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ALL_ID_MOVI_CHIU = 1;
        public static final int ALL_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ALL_ID_MOVI_CHIU = 5;
        public static final int ALL_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ALL_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
