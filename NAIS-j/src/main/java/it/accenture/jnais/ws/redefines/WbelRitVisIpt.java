package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WBEL-RIT-VIS-IPT<br>
 * Variable: WBEL-RIT-VIS-IPT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WbelRitVisIpt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WbelRitVisIpt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WBEL_RIT_VIS_IPT;
    }

    public void setWbelRitVisIpt(AfDecimal wbelRitVisIpt) {
        writeDecimalAsPacked(Pos.WBEL_RIT_VIS_IPT, wbelRitVisIpt.copy());
    }

    public void setWbelRitVisIptFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WBEL_RIT_VIS_IPT, Pos.WBEL_RIT_VIS_IPT);
    }

    /**Original name: WBEL-RIT-VIS-IPT<br>*/
    public AfDecimal getWbelRitVisIpt() {
        return readPackedAsDecimal(Pos.WBEL_RIT_VIS_IPT, Len.Int.WBEL_RIT_VIS_IPT, Len.Fract.WBEL_RIT_VIS_IPT);
    }

    public byte[] getWbelRitVisIptAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WBEL_RIT_VIS_IPT, Pos.WBEL_RIT_VIS_IPT);
        return buffer;
    }

    public void initWbelRitVisIptSpaces() {
        fill(Pos.WBEL_RIT_VIS_IPT, Len.WBEL_RIT_VIS_IPT, Types.SPACE_CHAR);
    }

    public void setWbelRitVisIptNull(String wbelRitVisIptNull) {
        writeString(Pos.WBEL_RIT_VIS_IPT_NULL, wbelRitVisIptNull, Len.WBEL_RIT_VIS_IPT_NULL);
    }

    /**Original name: WBEL-RIT-VIS-IPT-NULL<br>*/
    public String getWbelRitVisIptNull() {
        return readString(Pos.WBEL_RIT_VIS_IPT_NULL, Len.WBEL_RIT_VIS_IPT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WBEL_RIT_VIS_IPT = 1;
        public static final int WBEL_RIT_VIS_IPT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WBEL_RIT_VIS_IPT = 8;
        public static final int WBEL_RIT_VIS_IPT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WBEL_RIT_VIS_IPT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WBEL_RIT_VIS_IPT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
