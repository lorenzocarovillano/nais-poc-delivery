package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-IMP-CNBT-ISC-K3<br>
 * Variable: DFA-IMP-CNBT-ISC-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaImpCnbtIscK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaImpCnbtIscK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_IMP_CNBT_ISC_K3;
    }

    public void setDfaImpCnbtIscK3(AfDecimal dfaImpCnbtIscK3) {
        writeDecimalAsPacked(Pos.DFA_IMP_CNBT_ISC_K3, dfaImpCnbtIscK3.copy());
    }

    public void setDfaImpCnbtIscK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_IMP_CNBT_ISC_K3, Pos.DFA_IMP_CNBT_ISC_K3);
    }

    /**Original name: DFA-IMP-CNBT-ISC-K3<br>*/
    public AfDecimal getDfaImpCnbtIscK3() {
        return readPackedAsDecimal(Pos.DFA_IMP_CNBT_ISC_K3, Len.Int.DFA_IMP_CNBT_ISC_K3, Len.Fract.DFA_IMP_CNBT_ISC_K3);
    }

    public byte[] getDfaImpCnbtIscK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_IMP_CNBT_ISC_K3, Pos.DFA_IMP_CNBT_ISC_K3);
        return buffer;
    }

    public void setDfaImpCnbtIscK3Null(String dfaImpCnbtIscK3Null) {
        writeString(Pos.DFA_IMP_CNBT_ISC_K3_NULL, dfaImpCnbtIscK3Null, Len.DFA_IMP_CNBT_ISC_K3_NULL);
    }

    /**Original name: DFA-IMP-CNBT-ISC-K3-NULL<br>*/
    public String getDfaImpCnbtIscK3Null() {
        return readString(Pos.DFA_IMP_CNBT_ISC_K3_NULL, Len.DFA_IMP_CNBT_ISC_K3_NULL);
    }

    public String getDfaImpCnbtIscK3NullFormatted() {
        return Functions.padBlanks(getDfaImpCnbtIscK3Null(), Len.DFA_IMP_CNBT_ISC_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_ISC_K3 = 1;
        public static final int DFA_IMP_CNBT_ISC_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_IMP_CNBT_ISC_K3 = 8;
        public static final int DFA_IMP_CNBT_ISC_K3_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_ISC_K3 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_IMP_CNBT_ISC_K3 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
