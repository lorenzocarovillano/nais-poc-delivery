package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-QTZ-EMIS<br>
 * Variable: B03-DT-QTZ-EMIS from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtQtzEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtQtzEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_QTZ_EMIS;
    }

    public void setB03DtQtzEmis(int b03DtQtzEmis) {
        writeIntAsPacked(Pos.B03_DT_QTZ_EMIS, b03DtQtzEmis, Len.Int.B03_DT_QTZ_EMIS);
    }

    public void setB03DtQtzEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_QTZ_EMIS, Pos.B03_DT_QTZ_EMIS);
    }

    /**Original name: B03-DT-QTZ-EMIS<br>*/
    public int getB03DtQtzEmis() {
        return readPackedAsInt(Pos.B03_DT_QTZ_EMIS, Len.Int.B03_DT_QTZ_EMIS);
    }

    public byte[] getB03DtQtzEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_QTZ_EMIS, Pos.B03_DT_QTZ_EMIS);
        return buffer;
    }

    public void setB03DtQtzEmisNull(String b03DtQtzEmisNull) {
        writeString(Pos.B03_DT_QTZ_EMIS_NULL, b03DtQtzEmisNull, Len.B03_DT_QTZ_EMIS_NULL);
    }

    /**Original name: B03-DT-QTZ-EMIS-NULL<br>*/
    public String getB03DtQtzEmisNull() {
        return readString(Pos.B03_DT_QTZ_EMIS_NULL, Len.B03_DT_QTZ_EMIS_NULL);
    }

    public String getB03DtQtzEmisNullFormatted() {
        return Functions.padBlanks(getB03DtQtzEmisNull(), Len.B03_DT_QTZ_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_QTZ_EMIS = 1;
        public static final int B03_DT_QTZ_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_QTZ_EMIS = 5;
        public static final int B03_DT_QTZ_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_QTZ_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
