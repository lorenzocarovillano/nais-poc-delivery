package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TLI-IMP-NET-DFZ<br>
 * Variable: TLI-IMP-NET-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TliImpNetDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TliImpNetDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TLI_IMP_NET_DFZ;
    }

    public void setTliImpNetDfz(AfDecimal tliImpNetDfz) {
        writeDecimalAsPacked(Pos.TLI_IMP_NET_DFZ, tliImpNetDfz.copy());
    }

    public void setTliImpNetDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TLI_IMP_NET_DFZ, Pos.TLI_IMP_NET_DFZ);
    }

    /**Original name: TLI-IMP-NET-DFZ<br>*/
    public AfDecimal getTliImpNetDfz() {
        return readPackedAsDecimal(Pos.TLI_IMP_NET_DFZ, Len.Int.TLI_IMP_NET_DFZ, Len.Fract.TLI_IMP_NET_DFZ);
    }

    public byte[] getTliImpNetDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TLI_IMP_NET_DFZ, Pos.TLI_IMP_NET_DFZ);
        return buffer;
    }

    public void setTliImpNetDfzNull(String tliImpNetDfzNull) {
        writeString(Pos.TLI_IMP_NET_DFZ_NULL, tliImpNetDfzNull, Len.TLI_IMP_NET_DFZ_NULL);
    }

    /**Original name: TLI-IMP-NET-DFZ-NULL<br>*/
    public String getTliImpNetDfzNull() {
        return readString(Pos.TLI_IMP_NET_DFZ_NULL, Len.TLI_IMP_NET_DFZ_NULL);
    }

    public String getTliImpNetDfzNullFormatted() {
        return Functions.padBlanks(getTliImpNetDfzNull(), Len.TLI_IMP_NET_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TLI_IMP_NET_DFZ = 1;
        public static final int TLI_IMP_NET_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TLI_IMP_NET_DFZ = 8;
        public static final int TLI_IMP_NET_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TLI_IMP_NET_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TLI_IMP_NET_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
