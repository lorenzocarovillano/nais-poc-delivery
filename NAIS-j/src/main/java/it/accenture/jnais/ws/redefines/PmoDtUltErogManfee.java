package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-DT-ULT-EROG-MANFEE<br>
 * Variable: PMO-DT-ULT-EROG-MANFEE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoDtUltErogManfee extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoDtUltErogManfee() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_DT_ULT_EROG_MANFEE;
    }

    public void setPmoDtUltErogManfee(int pmoDtUltErogManfee) {
        writeIntAsPacked(Pos.PMO_DT_ULT_EROG_MANFEE, pmoDtUltErogManfee, Len.Int.PMO_DT_ULT_EROG_MANFEE);
    }

    public void setPmoDtUltErogManfeeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_DT_ULT_EROG_MANFEE, Pos.PMO_DT_ULT_EROG_MANFEE);
    }

    /**Original name: PMO-DT-ULT-EROG-MANFEE<br>*/
    public int getPmoDtUltErogManfee() {
        return readPackedAsInt(Pos.PMO_DT_ULT_EROG_MANFEE, Len.Int.PMO_DT_ULT_EROG_MANFEE);
    }

    public byte[] getPmoDtUltErogManfeeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_DT_ULT_EROG_MANFEE, Pos.PMO_DT_ULT_EROG_MANFEE);
        return buffer;
    }

    public void initPmoDtUltErogManfeeHighValues() {
        fill(Pos.PMO_DT_ULT_EROG_MANFEE, Len.PMO_DT_ULT_EROG_MANFEE, Types.HIGH_CHAR_VAL);
    }

    public void setPmoDtUltErogManfeeNull(String pmoDtUltErogManfeeNull) {
        writeString(Pos.PMO_DT_ULT_EROG_MANFEE_NULL, pmoDtUltErogManfeeNull, Len.PMO_DT_ULT_EROG_MANFEE_NULL);
    }

    /**Original name: PMO-DT-ULT-EROG-MANFEE-NULL<br>*/
    public String getPmoDtUltErogManfeeNull() {
        return readString(Pos.PMO_DT_ULT_EROG_MANFEE_NULL, Len.PMO_DT_ULT_EROG_MANFEE_NULL);
    }

    public String getPmoDtUltErogManfeeNullFormatted() {
        return Functions.padBlanks(getPmoDtUltErogManfeeNull(), Len.PMO_DT_ULT_EROG_MANFEE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_DT_ULT_EROG_MANFEE = 1;
        public static final int PMO_DT_ULT_EROG_MANFEE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_DT_ULT_EROG_MANFEE = 5;
        public static final int PMO_DT_ULT_EROG_MANFEE_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_DT_ULT_EROG_MANFEE = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
