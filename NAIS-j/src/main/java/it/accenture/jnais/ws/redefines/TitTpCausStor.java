package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TP-CAUS-STOR<br>
 * Variable: TIT-TP-CAUS-STOR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTpCausStor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTpCausStor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TP_CAUS_STOR;
    }

    public void setTitTpCausStor(int titTpCausStor) {
        writeIntAsPacked(Pos.TIT_TP_CAUS_STOR, titTpCausStor, Len.Int.TIT_TP_CAUS_STOR);
    }

    public void setTitTpCausStorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TP_CAUS_STOR, Pos.TIT_TP_CAUS_STOR);
    }

    /**Original name: TIT-TP-CAUS-STOR<br>*/
    public int getTitTpCausStor() {
        return readPackedAsInt(Pos.TIT_TP_CAUS_STOR, Len.Int.TIT_TP_CAUS_STOR);
    }

    public byte[] getTitTpCausStorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TP_CAUS_STOR, Pos.TIT_TP_CAUS_STOR);
        return buffer;
    }

    public void setTitTpCausStorNull(String titTpCausStorNull) {
        writeString(Pos.TIT_TP_CAUS_STOR_NULL, titTpCausStorNull, Len.TIT_TP_CAUS_STOR_NULL);
    }

    /**Original name: TIT-TP-CAUS-STOR-NULL<br>*/
    public String getTitTpCausStorNull() {
        return readString(Pos.TIT_TP_CAUS_STOR_NULL, Len.TIT_TP_CAUS_STOR_NULL);
    }

    public String getTitTpCausStorNullFormatted() {
        return Functions.padBlanks(getTitTpCausStorNull(), Len.TIT_TP_CAUS_STOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TP_CAUS_STOR = 1;
        public static final int TIT_TP_CAUS_STOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TP_CAUS_STOR = 3;
        public static final int TIT_TP_CAUS_STOR_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TP_CAUS_STOR = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
