package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CUR-TGA<br>
 * Variable: FLAG-CUR-TGA from program LCCS0450<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCurTga {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char INIT_CUR_TGA = 'S';
    public static final char FINE_CUR_TGA = 'N';

    //==== METHODS ====
    public void setFlagCurTga(char flagCurTga) {
        this.value = flagCurTga;
    }

    public char getFlagCurTga() {
        return this.value;
    }

    public void setInitCurTga() {
        value = INIT_CUR_TGA;
    }

    public boolean isFineCurTga() {
        return value == FINE_CUR_TGA;
    }

    public void setFineCurTga() {
        value = FINE_CUR_TGA;
    }
}
