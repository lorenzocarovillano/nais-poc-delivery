package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-ANNULLO<br>
 * Variable: WS-TP-ANNULLO from copybook LCCVL510<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpAnnullo {

    //==== PROPERTIES ====
    private String value = "";
    public static final String AMMISSIBILE = "AM";
    public static final String BLOCCANTE = "BL";
    public static final String ENTRAMBI = "EN";
    public static final String ESPLICITO = "ES";
    public static final String IMPLICITO = "IM";

    //==== METHODS ====
    public void setWsTpAnnullo(String wsTpAnnullo) {
        this.value = Functions.subString(wsTpAnnullo, Len.WS_TP_ANNULLO);
    }

    public String getWsTpAnnullo() {
        return this.value;
    }

    public void setAmmissibile() {
        value = AMMISSIBILE;
    }

    public void setBloccante() {
        value = BLOCCANTE;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_ANNULLO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
