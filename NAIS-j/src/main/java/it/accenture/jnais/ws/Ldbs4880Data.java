package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS4880<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs4880Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-NOG-ULT-PROGR
    private short indNogUltProgr = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndNogUltProgr(short indNogUltProgr) {
        this.indNogUltProgr = indNogUltProgr;
    }

    public short getIndNogUltProgr() {
        return this.indNogUltProgr;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
