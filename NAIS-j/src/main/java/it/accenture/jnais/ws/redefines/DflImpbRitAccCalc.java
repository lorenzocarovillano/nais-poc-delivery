package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-RIT-ACC-CALC<br>
 * Variable: DFL-IMPB-RIT-ACC-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbRitAccCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbRitAccCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_RIT_ACC_CALC;
    }

    public void setDflImpbRitAccCalc(AfDecimal dflImpbRitAccCalc) {
        writeDecimalAsPacked(Pos.DFL_IMPB_RIT_ACC_CALC, dflImpbRitAccCalc.copy());
    }

    public void setDflImpbRitAccCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_RIT_ACC_CALC, Pos.DFL_IMPB_RIT_ACC_CALC);
    }

    /**Original name: DFL-IMPB-RIT-ACC-CALC<br>*/
    public AfDecimal getDflImpbRitAccCalc() {
        return readPackedAsDecimal(Pos.DFL_IMPB_RIT_ACC_CALC, Len.Int.DFL_IMPB_RIT_ACC_CALC, Len.Fract.DFL_IMPB_RIT_ACC_CALC);
    }

    public byte[] getDflImpbRitAccCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_RIT_ACC_CALC, Pos.DFL_IMPB_RIT_ACC_CALC);
        return buffer;
    }

    public void setDflImpbRitAccCalcNull(String dflImpbRitAccCalcNull) {
        writeString(Pos.DFL_IMPB_RIT_ACC_CALC_NULL, dflImpbRitAccCalcNull, Len.DFL_IMPB_RIT_ACC_CALC_NULL);
    }

    /**Original name: DFL-IMPB-RIT-ACC-CALC-NULL<br>*/
    public String getDflImpbRitAccCalcNull() {
        return readString(Pos.DFL_IMPB_RIT_ACC_CALC_NULL, Len.DFL_IMPB_RIT_ACC_CALC_NULL);
    }

    public String getDflImpbRitAccCalcNullFormatted() {
        return Functions.padBlanks(getDflImpbRitAccCalcNull(), Len.DFL_IMPB_RIT_ACC_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_RIT_ACC_CALC = 1;
        public static final int DFL_IMPB_RIT_ACC_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_RIT_ACC_CALC = 8;
        public static final int DFL_IMPB_RIT_ACC_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_RIT_ACC_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_RIT_ACC_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
