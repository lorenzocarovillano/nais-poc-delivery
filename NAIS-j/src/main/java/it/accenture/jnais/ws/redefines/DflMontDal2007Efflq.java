package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-MONT-DAL2007-EFFLQ<br>
 * Variable: DFL-MONT-DAL2007-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflMontDal2007Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflMontDal2007Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_MONT_DAL2007_EFFLQ;
    }

    public void setDflMontDal2007Efflq(AfDecimal dflMontDal2007Efflq) {
        writeDecimalAsPacked(Pos.DFL_MONT_DAL2007_EFFLQ, dflMontDal2007Efflq.copy());
    }

    public void setDflMontDal2007EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_MONT_DAL2007_EFFLQ, Pos.DFL_MONT_DAL2007_EFFLQ);
    }

    /**Original name: DFL-MONT-DAL2007-EFFLQ<br>*/
    public AfDecimal getDflMontDal2007Efflq() {
        return readPackedAsDecimal(Pos.DFL_MONT_DAL2007_EFFLQ, Len.Int.DFL_MONT_DAL2007_EFFLQ, Len.Fract.DFL_MONT_DAL2007_EFFLQ);
    }

    public byte[] getDflMontDal2007EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_MONT_DAL2007_EFFLQ, Pos.DFL_MONT_DAL2007_EFFLQ);
        return buffer;
    }

    public void setDflMontDal2007EfflqNull(String dflMontDal2007EfflqNull) {
        writeString(Pos.DFL_MONT_DAL2007_EFFLQ_NULL, dflMontDal2007EfflqNull, Len.DFL_MONT_DAL2007_EFFLQ_NULL);
    }

    /**Original name: DFL-MONT-DAL2007-EFFLQ-NULL<br>*/
    public String getDflMontDal2007EfflqNull() {
        return readString(Pos.DFL_MONT_DAL2007_EFFLQ_NULL, Len.DFL_MONT_DAL2007_EFFLQ_NULL);
    }

    public String getDflMontDal2007EfflqNullFormatted() {
        return Functions.padBlanks(getDflMontDal2007EfflqNull(), Len.DFL_MONT_DAL2007_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_MONT_DAL2007_EFFLQ = 1;
        public static final int DFL_MONT_DAL2007_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_MONT_DAL2007_EFFLQ = 8;
        public static final int DFL_MONT_DAL2007_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_MONT_DAL2007_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_MONT_DAL2007_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
