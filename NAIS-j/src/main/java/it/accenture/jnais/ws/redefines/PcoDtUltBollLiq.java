package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-LIQ<br>
 * Variable: PCO-DT-ULT-BOLL-LIQ from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_LIQ;
    }

    public void setPcoDtUltBollLiq(int pcoDtUltBollLiq) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_LIQ, pcoDtUltBollLiq, Len.Int.PCO_DT_ULT_BOLL_LIQ);
    }

    public void setPcoDtUltBollLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_LIQ, Pos.PCO_DT_ULT_BOLL_LIQ);
    }

    /**Original name: PCO-DT-ULT-BOLL-LIQ<br>*/
    public int getPcoDtUltBollLiq() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_LIQ, Len.Int.PCO_DT_ULT_BOLL_LIQ);
    }

    public byte[] getPcoDtUltBollLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_LIQ, Pos.PCO_DT_ULT_BOLL_LIQ);
        return buffer;
    }

    public void initPcoDtUltBollLiqHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_LIQ, Len.PCO_DT_ULT_BOLL_LIQ, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollLiqNull(String pcoDtUltBollLiqNull) {
        writeString(Pos.PCO_DT_ULT_BOLL_LIQ_NULL, pcoDtUltBollLiqNull, Len.PCO_DT_ULT_BOLL_LIQ_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-LIQ-NULL<br>*/
    public String getPcoDtUltBollLiqNull() {
        return readString(Pos.PCO_DT_ULT_BOLL_LIQ_NULL, Len.PCO_DT_ULT_BOLL_LIQ_NULL);
    }

    public String getPcoDtUltBollLiqNullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollLiqNull(), Len.PCO_DT_ULT_BOLL_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_LIQ = 1;
        public static final int PCO_DT_ULT_BOLL_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_LIQ = 5;
        public static final int PCO_DT_ULT_BOLL_LIQ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_LIQ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
