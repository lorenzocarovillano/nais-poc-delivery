package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.GarDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndGar;
import it.accenture.jnais.copy.Ldbv1421;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS1420<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs1420Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-GAR
    private IndGar indGar = new IndGar();
    //Original name: GAR-DB
    private GarDb garDb = new GarDb();
    //Original name: LDBV1421
    private Ldbv1421 ldbv1421 = new Ldbv1421();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public GarDb getGarDb() {
        return garDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndGar getIndGar() {
        return indGar;
    }

    public Ldbv1421 getLdbv1421() {
        return ldbv1421;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
