package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IABS0140<br>
 * Generated as a class for rule WS.<br>*/
public class Iabs0140Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WK-LABEL
    private String wkLabel = DefaultValues.stringVal(Len.WK_LABEL);
    //Original name: WK-DATABASE-NAME
    private String wkDatabaseName = DefaultValues.stringVal(Len.WK_DATABASE_NAME);
    //Original name: WK-USER
    private String wkUser = DefaultValues.stringVal(Len.WK_USER);
    //Original name: WK-PASSWORD
    private String wkPassword = DefaultValues.stringVal(Len.WK_PASSWORD);
    //Original name: AREA-X-IABS0140
    private AreaXIabs0140 areaXIabs0140 = new AreaXIabs0140();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWkLabel(String wkLabel) {
        this.wkLabel = Functions.subString(wkLabel, Len.WK_LABEL);
    }

    public String getWkLabel() {
        return this.wkLabel;
    }

    public void setWkDatabaseName(String wkDatabaseName) {
        this.wkDatabaseName = Functions.subString(wkDatabaseName, Len.WK_DATABASE_NAME);
    }

    public String getWkDatabaseName() {
        return this.wkDatabaseName;
    }

    public void setWkUser(String wkUser) {
        this.wkUser = Functions.subString(wkUser, Len.WK_USER);
    }

    public String getWkUser() {
        return this.wkUser;
    }

    public void setWkPassword(String wkPassword) {
        this.wkPassword = Functions.subString(wkPassword, Len.WK_PASSWORD);
    }

    public String getWkPassword() {
        return this.wkPassword;
    }

    public AreaXIabs0140 getAreaXIabs0140() {
        return areaXIabs0140;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_NOME_TABELLA = 15;
        public static final int WK_LABEL = 30;
        public static final int WK_DATABASE_NAME = 10;
        public static final int WK_USER = 8;
        public static final int WK_PASSWORD = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
