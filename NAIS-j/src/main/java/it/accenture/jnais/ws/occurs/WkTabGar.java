package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TAB-GAR<br>
 * Variables: WK-TAB-GAR from program LVES0269<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkTabGar {

    //==== PROPERTIES ====
    //Original name: WK-COD-GAR
    private String codGar = DefaultValues.stringVal(Len.COD_GAR);
    //Original name: WK-TP-IAS
    private String tpIas = DefaultValues.stringVal(Len.TP_IAS);

    //==== METHODS ====
    public void setCodGar(String codGar) {
        this.codGar = Functions.subString(codGar, Len.COD_GAR);
    }

    public String getCodGar() {
        return this.codGar;
    }

    public void setTpIas(String tpIas) {
        this.tpIas = Functions.subString(tpIas, Len.TP_IAS);
    }

    public String getTpIas() {
        return this.tpIas;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_GAR = 12;
        public static final int TP_IAS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
