package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-DUR-PAG-PRE<br>
 * Variable: WB03-DUR-PAG-PRE from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03DurPagPre extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03DurPagPre() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_DUR_PAG_PRE;
    }

    public void setWb03DurPagPre(int wb03DurPagPre) {
        writeIntAsPacked(Pos.WB03_DUR_PAG_PRE, wb03DurPagPre, Len.Int.WB03_DUR_PAG_PRE);
    }

    public void setWb03DurPagPreFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_DUR_PAG_PRE, Pos.WB03_DUR_PAG_PRE);
    }

    /**Original name: WB03-DUR-PAG-PRE<br>*/
    public int getWb03DurPagPre() {
        return readPackedAsInt(Pos.WB03_DUR_PAG_PRE, Len.Int.WB03_DUR_PAG_PRE);
    }

    public byte[] getWb03DurPagPreAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_DUR_PAG_PRE, Pos.WB03_DUR_PAG_PRE);
        return buffer;
    }

    public void setWb03DurPagPreNull(String wb03DurPagPreNull) {
        writeString(Pos.WB03_DUR_PAG_PRE_NULL, wb03DurPagPreNull, Len.WB03_DUR_PAG_PRE_NULL);
    }

    /**Original name: WB03-DUR-PAG-PRE-NULL<br>*/
    public String getWb03DurPagPreNull() {
        return readString(Pos.WB03_DUR_PAG_PRE_NULL, Len.WB03_DUR_PAG_PRE_NULL);
    }

    public String getWb03DurPagPreNullFormatted() {
        return Functions.padBlanks(getWb03DurPagPreNull(), Len.WB03_DUR_PAG_PRE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_DUR_PAG_PRE = 1;
        public static final int WB03_DUR_PAG_PRE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_DUR_PAG_PRE = 3;
        public static final int WB03_DUR_PAG_PRE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_DUR_PAG_PRE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
