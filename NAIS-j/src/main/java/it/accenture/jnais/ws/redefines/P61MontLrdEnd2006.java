package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-MONT-LRD-END2006<br>
 * Variable: P61-MONT-LRD-END2006 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61MontLrdEnd2006 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61MontLrdEnd2006() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_MONT_LRD_END2006;
    }

    public void setP61MontLrdEnd2006(AfDecimal p61MontLrdEnd2006) {
        writeDecimalAsPacked(Pos.P61_MONT_LRD_END2006, p61MontLrdEnd2006.copy());
    }

    public void setP61MontLrdEnd2006FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_MONT_LRD_END2006, Pos.P61_MONT_LRD_END2006);
    }

    /**Original name: P61-MONT-LRD-END2006<br>*/
    public AfDecimal getP61MontLrdEnd2006() {
        return readPackedAsDecimal(Pos.P61_MONT_LRD_END2006, Len.Int.P61_MONT_LRD_END2006, Len.Fract.P61_MONT_LRD_END2006);
    }

    public byte[] getP61MontLrdEnd2006AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_MONT_LRD_END2006, Pos.P61_MONT_LRD_END2006);
        return buffer;
    }

    public void setP61MontLrdEnd2006Null(String p61MontLrdEnd2006Null) {
        writeString(Pos.P61_MONT_LRD_END2006_NULL, p61MontLrdEnd2006Null, Len.P61_MONT_LRD_END2006_NULL);
    }

    /**Original name: P61-MONT-LRD-END2006-NULL<br>*/
    public String getP61MontLrdEnd2006Null() {
        return readString(Pos.P61_MONT_LRD_END2006_NULL, Len.P61_MONT_LRD_END2006_NULL);
    }

    public String getP61MontLrdEnd2006NullFormatted() {
        return Functions.padBlanks(getP61MontLrdEnd2006Null(), Len.P61_MONT_LRD_END2006_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_MONT_LRD_END2006 = 1;
        public static final int P61_MONT_LRD_END2006_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_MONT_LRD_END2006 = 8;
        public static final int P61_MONT_LRD_END2006_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_MONT_LRD_END2006 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P61_MONT_LRD_END2006 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
