package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-GESTIONE-LANCI-BUS<br>
 * Variable: FLAG-GESTIONE-LANCI-BUS from copybook IABV0007<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagGestioneLanciBus {

    //==== PROPERTIES ====
    private String value = "RT";
    public static final String REAL_TIME = "RT";
    public static final String SUSPEND = "SU";

    //==== METHODS ====
    public void setFlagGestioneLanciBus(String flagGestioneLanciBus) {
        this.value = Functions.subString(flagGestioneLanciBus, Len.FLAG_GESTIONE_LANCI_BUS);
    }

    public String getFlagGestioneLanciBus() {
        return this.value;
    }

    public void setRealTime() {
        value = REAL_TIME;
    }

    public boolean isSuspend() {
        return value.equals(SUSPEND);
    }

    public void setGestioneSuspend() {
        value = SUSPEND;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_GESTIONE_LANCI_BUS = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
