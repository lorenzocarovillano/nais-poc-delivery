package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-AREA-INIZIALIZE<br>
 * Variable: WK-AREA-INIZIALIZE from program LOAM0170<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkAreaInizialize {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_AREA_INIZIALIZE);
    public static final String SI = "SI";
    public static final String NO = "NO";

    //==== METHODS ====
    public void setWkAreaInizialize(String wkAreaInizialize) {
        this.value = Functions.subString(wkAreaInizialize, Len.WK_AREA_INIZIALIZE);
    }

    public String getWkAreaInizialize() {
        return this.value;
    }

    public boolean isSi() {
        return value.equals(SI);
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_AREA_INIZIALIZE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
