package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-ID-MOVI-CHIU<br>
 * Variable: P61-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_ID_MOVI_CHIU;
    }

    public void setP61IdMoviChiu(int p61IdMoviChiu) {
        writeIntAsPacked(Pos.P61_ID_MOVI_CHIU, p61IdMoviChiu, Len.Int.P61_ID_MOVI_CHIU);
    }

    public void setP61IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_ID_MOVI_CHIU, Pos.P61_ID_MOVI_CHIU);
    }

    /**Original name: P61-ID-MOVI-CHIU<br>*/
    public int getP61IdMoviChiu() {
        return readPackedAsInt(Pos.P61_ID_MOVI_CHIU, Len.Int.P61_ID_MOVI_CHIU);
    }

    public byte[] getP61IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_ID_MOVI_CHIU, Pos.P61_ID_MOVI_CHIU);
        return buffer;
    }

    public void setP61IdMoviChiuNull(String p61IdMoviChiuNull) {
        writeString(Pos.P61_ID_MOVI_CHIU_NULL, p61IdMoviChiuNull, Len.P61_ID_MOVI_CHIU_NULL);
    }

    /**Original name: P61-ID-MOVI-CHIU-NULL<br>*/
    public String getP61IdMoviChiuNull() {
        return readString(Pos.P61_ID_MOVI_CHIU_NULL, Len.P61_ID_MOVI_CHIU_NULL);
    }

    public String getP61IdMoviChiuNullFormatted() {
        return Functions.padBlanks(getP61IdMoviChiuNull(), Len.P61_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_ID_MOVI_CHIU = 1;
        public static final int P61_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_ID_MOVI_CHIU = 5;
        public static final int P61_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
