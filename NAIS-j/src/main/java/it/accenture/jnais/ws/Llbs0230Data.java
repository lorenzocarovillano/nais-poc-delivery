package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.AccCommLlbs0230;
import it.accenture.jnais.copy.AreaLdbv1131;
import it.accenture.jnais.copy.BilaFndEstrLlbs0230;
import it.accenture.jnais.copy.BilaTrchEstr;
import it.accenture.jnais.copy.BilaVarCalcP;
import it.accenture.jnais.copy.BilaVarCalcT;
import it.accenture.jnais.copy.DettTitCont;
import it.accenture.jnais.copy.DFiscAdes;
import it.accenture.jnais.copy.EstPoliCpiPr;
import it.accenture.jnais.copy.EstTrchDiGar;
import it.accenture.jnais.copy.Iabcsq99;
import it.accenture.jnais.copy.Iabvsqs1;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Idsv0503;
import it.accenture.jnais.copy.Idsv8888;
import it.accenture.jnais.copy.Ivvc0212;
import it.accenture.jnais.copy.Ivvc0216Lves0269;
import it.accenture.jnais.copy.Ivvc0218Loas0310;
import it.accenture.jnais.copy.Lccc0006;
import it.accenture.jnais.copy.Lccvb018;
import it.accenture.jnais.copy.Lccvb038;
import it.accenture.jnais.copy.Lccvb048;
import it.accenture.jnais.copy.Lccvb058;
import it.accenture.jnais.copy.Ldbv0371Llbs0230;
import it.accenture.jnais.copy.Ldbv0641;
import it.accenture.jnais.copy.Ldbv1241;
import it.accenture.jnais.copy.Ldbv1301;
import it.accenture.jnais.copy.Ldbv2631;
import it.accenture.jnais.copy.Ldbv2651;
import it.accenture.jnais.copy.Ldbv3401Llbs0230;
import it.accenture.jnais.copy.Ldbv3411Llbs0230;
import it.accenture.jnais.copy.Ldbv3421Llbs0230;
import it.accenture.jnais.copy.Ldbv4151Llbs0230;
import it.accenture.jnais.copy.Ldbv4911;
import it.accenture.jnais.copy.Ldbv9091Llbs0230;
import it.accenture.jnais.copy.Liq;
import it.accenture.jnais.copy.Movi;
import it.accenture.jnais.copy.ParamComp;
import it.accenture.jnais.copy.ParamMovi;
import it.accenture.jnais.copy.ParamOggLoas0800;
import it.accenture.jnais.copy.PersLlbs0230;
import it.accenture.jnais.copy.QuotzAggFndLccs0450;
import it.accenture.jnais.copy.QuotzFndUnitLccs0450;
import it.accenture.jnais.copy.RappAna;
import it.accenture.jnais.copy.RappRete;
import it.accenture.jnais.copy.RichDisFndLccs0450;
import it.accenture.jnais.copy.RisDiTrch;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.copy.ValAst;
import it.accenture.jnais.copy.WB01RecFisso;
import it.accenture.jnais.copy.WB03RecFisso;
import it.accenture.jnais.copy.WB04RecFisso;
import it.accenture.jnais.copy.WB05RecFisso;
import it.accenture.jnais.ws.enums.WkStrada;
import it.accenture.jnais.ws.enums.WkTipoElaborazione;
import it.accenture.jnais.ws.enums.WsMovimentoLlbs0230;
import it.accenture.jnais.ws.enums.WsTpCaus;
import it.accenture.jnais.ws.enums.WsTpDato;
import it.accenture.jnais.ws.enums.WsTpFrmAssva;
import it.accenture.jnais.ws.enums.WsTpPerPremio;
import it.accenture.jnais.ws.enums.WsTpQtz;
import it.accenture.jnais.ws.enums.WsTpRappAna;
import it.accenture.jnais.ws.enums.WsTpStatBus;
import it.accenture.jnais.ws.occurs.WgrzTabGar;
import it.accenture.jnais.ws.redefines.WkAlqMargCSubrsh;
import it.accenture.jnais.ws.redefines.WkAlqMargRis;
import it.accenture.jnais.ws.redefines.WkMesiCarenza;
import it.accenture.jnais.ws.redefines.WsDtCalcolo;
import it.accenture.jnais.ws.redefines.WsEtaRaggnDtCalc;
import it.accenture.jnais.ws.redefines.WskdTabValP;
import it.accenture.jnais.ws.redefines.WskdTabValT;
import it.accenture.jnais.ws.redefines.WsPreTot;
import it.accenture.jnais.ws.redefines.WsTaxTot;
import it.accenture.jnais.ws.redefines.WtgsTabTgs1;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LLBS0230<br>
 * Generated as a class for rule WS.<br>*/
public class Llbs0230Data {

    //==== PROPERTIES ====
    public static final int WSTR_TAB_GAR_MAXOCCURS = 20;
    /**Original name: WK-VAR-REC-SIZE-F01<br>
	 * <pre>----------------------------------------------------------------*
	 *     CAMPO CONTENENTE LUNGHEZZA DEI FILES VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private int wkVarRecSizeF01 = DefaultValues.BIN_INT_VAL;
    //Original name: WK-VAR-REC-SIZE-F02
    private int wkVarRecSizeF02 = DefaultValues.BIN_INT_VAL;
    //Original name: WK-VAR-REC-SIZE-F03
    private int wkVarRecSizeF03 = DefaultValues.BIN_INT_VAL;
    //Original name: WK-VAR-REC-SIZE-F04
    private int wkVarRecSizeF04 = DefaultValues.BIN_INT_VAL;
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LLBS0230";
    //Original name: TRCH-DI-GAR
    private TrchDiGarIvvs0216 trchDiGar = new TrchDiGarIvvs0216();
    //Original name: DETT-TIT-CONT
    private DettTitCont dettTitCont = new DettTitCont();
    //Original name: PARAM-MOVI
    private ParamMovi paramMovi = new ParamMovi();
    //Original name: D-FISC-ADES
    private DFiscAdes dFiscAdes = new DFiscAdes();
    //Original name: LIQ
    private Liq liq = new Liq();
    //Original name: RIS-DI-TRCH
    private RisDiTrch risDiTrch = new RisDiTrch();
    //Original name: MOVI
    private Movi movi = new Movi();
    //Original name: RAPP-ANA
    private RappAna rappAna = new RappAna();
    //Original name: PARAM-OGG
    private ParamOggLoas0800 paramOgg = new ParamOggLoas0800();
    //Original name: RAPP-RETE
    private RappRete rappRete = new RappRete();
    //Original name: BILA-TRCH-ESTR
    private BilaTrchEstr bilaTrchEstr = new BilaTrchEstr();
    //Original name: BILA-VAR-CALC-P
    private BilaVarCalcP bilaVarCalcP = new BilaVarCalcP();
    //Original name: BILA-VAR-CALC-T
    private BilaVarCalcT bilaVarCalcT = new BilaVarCalcT();
    //Original name: RICH-DIS-FND
    private RichDisFndLccs0450 richDisFnd = new RichDisFndLccs0450();
    //Original name: VAL-AST
    private ValAst valAst = new ValAst();
    //Original name: QUOTZ-AGG-FND
    private QuotzAggFndLccs0450 quotzAggFnd = new QuotzAggFndLccs0450();
    //Original name: PERS
    private PersLlbs0230 pers = new PersLlbs0230();
    //Original name: BILA-FND-ESTR
    private BilaFndEstrLlbs0230 bilaFndEstr = new BilaFndEstrLlbs0230();
    //Original name: EST-TRCH-DI-GAR
    private EstTrchDiGar estTrchDiGar = new EstTrchDiGar();
    //Original name: QUOTZ-FND-UNIT
    private QuotzFndUnitLccs0450 quotzFndUnit = new QuotzFndUnitLccs0450();
    //Original name: PARAM-COMP
    private ParamComp paramComp = new ParamComp();
    //Original name: EST-POLI-CPI-PR
    private EstPoliCpiPr estPoliCpiPr = new EstPoliCpiPr();
    //Original name: ACC-COMM
    private AccCommLlbs0230 accComm = new AccCommLlbs0230();
    //Original name: LDBV3401
    private Ldbv3401Llbs0230 ldbv3401 = new Ldbv3401Llbs0230();
    //Original name: LDBV3411
    private Ldbv3411Llbs0230 ldbv3411 = new Ldbv3411Llbs0230();
    //Original name: LDBV9091
    private Ldbv9091Llbs0230 ldbv9091 = new Ldbv9091Llbs0230();
    //Original name: LDBV3421
    private Ldbv3421Llbs0230 ldbv3421 = new Ldbv3421Llbs0230();
    //Original name: AREA-LDBV1131
    private AreaLdbv1131 areaLdbv1131 = new AreaLdbv1131();
    //Original name: LDBV2651
    private Ldbv2651 ldbv2651 = new Ldbv2651();
    //Original name: LDBV2631
    private Ldbv2631 ldbv2631 = new Ldbv2631();
    //Original name: LDBV1241
    private Ldbv1241 ldbv1241 = new Ldbv1241();
    //Original name: LDBV4151
    private Ldbv4151Llbs0230 ldbv4151 = new Ldbv4151Llbs0230();
    //Original name: LDBV0371
    private Ldbv0371Llbs0230 ldbv0371 = new Ldbv0371Llbs0230();
    //Original name: LDBV5471-ID-TRCH-DI-GAR
    private int ldbv5471IdTrchDiGar = DefaultValues.INT_VAL;
    //Original name: LCCC0450
    private Lccc0450 lccc0450 = new Lccc0450();
    //Original name: LDBV1301
    private Ldbv1301 ldbv1301 = new Ldbv1301();
    //Original name: LDBV0641
    private Ldbv0641 ldbv0641 = new Ldbv0641();
    //Original name: LDBVH601
    private Ldbv0641 ldbvh601 = new Ldbv0641();
    //Original name: AREA-OUT-B03
    private AreaOutB03 areaOutB03 = new AreaOutB03();
    //Original name: AREA-OUT-B04
    private AreaOutB04 areaOutB04 = new AreaOutB04();
    //Original name: AREA-OUT-B05
    private AreaOutB05 areaOutB05 = new AreaOutB05();
    //Original name: LCCVB058
    private Lccvb058 lccvb058 = new Lccvb058();
    //Original name: LCCVB038
    private Lccvb038 lccvb038 = new Lccvb038();
    //Original name: LCCVB018
    private Lccvb018 lccvb018 = new Lccvb018();
    //Original name: LCCVB048
    private Lccvb048 lccvb048 = new Lccvb048();
    //Original name: AREA-OUT-SCARTI
    private AreaOutScarti areaOutScarti = new AreaOutScarti();
    //Original name: REC-SCARTATI-1
    private short recScartati1 = ((short)1);
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IABVSQS1
    private Iabvsqs1 iabvsqs1 = new Iabvsqs1();
    //Original name: IABCSQ99
    private Iabcsq99 iabcsq99 = new Iabcsq99();
    //Original name: IDSV8888
    private Idsv8888 idsv8888 = new Idsv8888();
    //Original name: LDBV4911
    private Ldbv4911 ldbv4911 = new Ldbv4911();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    //Original name: AREA-IWFS0050
    private AreaCallIwfs0050 areaIwfs0050 = new AreaCallIwfs0050();
    //Original name: IDSV0503
    private Idsv0503 idsv0503 = new Idsv0503();
    /**Original name: PGM-IWFS0050<br>
	 * <pre>--> Conversione Stringa a Numerico</pre>*/
    private String pgmIwfs0050 = "IWFS0050";
    /**Original name: PGM-IDBSP670<br>
	 * <pre>--> MODULO LETTURA EST_POLI_CPI_PR</pre>*/
    private String pgmIdbsp670 = "IDBSP670";
    /**Original name: PGM-IDBSP630<br>
	 * <pre>--> MODULO LETTURA LIQ</pre>*/
    private String pgmIdbsp630 = "IDBSP630";
    //Original name: IX-INDICI
    private IxIndiciLlbs0230 ixIndici = new IxIndiciLlbs0230();
    /**Original name: WK-STRADA<br>
	 * <pre>----------------------------------------------------------------*
	 *     CAMPI DI APPOGGIO
	 * ----------------------------------------------------------------*
	 *   FLAG SCRITTURA SULLE 'TABELLE BILA'/'FILE SEQUENZIALI'</pre>*/
    private WkStrada wkStrada = new WkStrada();
    /**Original name: WK-STRINGATURA<br>
	 * <pre>  FLAG ABILITAZIONE ALLA STRINGATURA DELLE VARIABILI DA PASSARE
	 *   ALLA FASE DI CALCOLO</pre>*/
    private boolean wkStringatura = true;
    //Original name: WK-NUM-VAR
    private String wkNumVar = DefaultValues.stringVal(Len.WK_NUM_VAR);
    //Original name: WK-LUNGH-STRINGA
    private String wkLunghStringa = DefaultValues.stringVal(Len.WK_LUNGH_STRINGA);
    /**Original name: WS-FL-SCRIVI-TRCH<br>
	 * <pre>  FLAG ABILITAZIONE SCRITTURA DI UNA TRANCHE DI RAMO III SOLO
	 *   SE VIENE SCRITTA L'OCCORRENZA SULLA TAB. FND_ESTR</pre>*/
    private boolean wsFlScriviTrch = true;
    /**Original name: WS-FL-TUTTI-FND-A-0<br>
	 * <pre>  FLAG VERIFICA SE TUTTI I FONDI DI UNA TRANCHE HANNO SALDO
	 *   QUOTE UGUALE A ZERO</pre>*/
    private boolean wsFlTuttiFndA0 = true;
    //Original name: WK-ALQ-MARG-RIS
    private WkAlqMargRis wkAlqMargRis = new WkAlqMargRis();
    //Original name: WK-ALQ-MARG-C-SUBRSH
    private WkAlqMargCSubrsh wkAlqMargCSubrsh = new WkAlqMargCSubrsh();
    //Original name: WK-MESI-CARENZA
    private WkMesiCarenza wkMesiCarenza = new WkMesiCarenza();
    //Original name: WK-DATA-APPO-N
    private WkDataAppoN wkDataAppoN = new WkDataAppoN();
    //Original name: WK-DATA-APPO-9-AA
    private String wkDataAppo9Aa = DefaultValues.stringVal(Len.WK_DATA_APPO9_AA);
    //Original name: WK-DT-DECOR-TRCH
    private String wkDtDecorTrch = "00000000";
    //Original name: WS-DATA-RISERVA-COMP
    private int wsDataRiservaComp = DefaultValues.INT_VAL;
    //Original name: WK-APPO-AREA-STB
    private String wkAppoAreaStb = DefaultValues.stringVal(Len.WK_APPO_AREA_STB);
    //Original name: WK-DT-ULT-ADEG-PRE-PR
    private int wkDtUltAdegPrePr = DefaultValues.INT_VAL;
    //Original name: WK-COD-PARAM
    private String wkCodParam = DefaultValues.stringVal(Len.WK_COD_PARAM);
    //Original name: WK-TIPO-ELABORAZIONE
    private WkTipoElaborazione wkTipoElaborazione = new WkTipoElaborazione();
    //Original name: WK-DA-SCARTARE
    private boolean wkDaScartare = false;
    //Original name: WS-TP-STAT-BUS-POL
    private String wsTpStatBusPol = "";
    //Original name: WS-TP-CAUS-POL
    private String wsTpCausPol = "";
    //Original name: WS-TP-STAT-BUS-ADE
    private String wsTpStatBusAde = "";
    //Original name: WS-TP-CAUS-ADE
    private String wsTpCausAde = "";
    //Original name: WS-TP-STAT-BUS-TGA
    private String wsTpStatBusTga = "";
    //Original name: WS-TP-CAUS-TGA
    private String wsTpCausTga = "";
    //Original name: WS-DT-N
    private WsDtN wsDtN = new WsDtN();
    //Original name: WS-DT-X
    private WsDtX wsDtX = new WsDtX();
    //Original name: WS-DT-RST
    private WsDtRst wsDtRst = new WsDtRst();
    //Original name: WS-DT-RST-9
    private String wsDtRst9 = DefaultValues.stringVal(Len.WS_DT_RST9);
    //Original name: WS-DT-INI
    private WsDtIni wsDtIni = new WsDtIni();
    //Original name: WS-DT-INI-9
    private String wsDtIni9 = DefaultValues.stringVal(Len.WS_DT_INI9);
    //Original name: WK-ADESIONE-9
    private String wkAdesione9 = DefaultValues.stringVal(Len.WK_ADESIONE9);
    //Original name: WS-DT-PTF-X
    private String wsDtPtfX = DefaultValues.stringVal(Len.WS_DT_PTF_X);
    //Original name: WS-DT-TS-PTF
    private long wsDtTsPtf = DefaultValues.LONG_VAL;
    //Original name: WS-SOMMA-IMPO-LIQ
    private AfDecimal wsSommaImpoLiq = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-APPO-NUM-18
    private String wsAppoNum18 = LiteralGenerator.create("0", Len.WS_APPO_NUM18);
    /**Original name: WS-TP-PER-PREMIO<br>
	 * <pre>--> Tipo periodo premio (Garanzia)</pre>*/
    private WsTpPerPremio wsTpPerPremio = new WsTpPerPremio();
    /**Original name: WS-PRE-TOT<br>
	 * <pre>-- PREMIO TOTALE (DETTAGLIO TITOLO CONTABILE)</pre>*/
    private WsPreTot wsPreTot = new WsPreTot();
    /**Original name: WS-TAX-TOT<br>
	 * <pre>-- TAX TOTALE (DETTAGLIO TITOLO CONTABILE)</pre>*/
    private WsTaxTot wsTaxTot = new WsTaxTot();
    /**Original name: WS-DIFF-PRE<br>
	 * <pre>-- DIFF. TRA IL TOTALE PREMIO E IL TOT. TAX</pre>*/
    private AfDecimal wsDiffPre = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    /**Original name: WS-TOT-PROV-ACQ-WTGA<br>
	 * <pre>-- SOMMA TRA LE WTGA-PROV-1AA-ACQ
	 *                 WTGA-PROV-2AA-ACQ
	 *  DELLA TRANCHE DI GARANZIA</pre>*/
    private AfDecimal wsTotProvAcqWtga = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    //Original name: WS-DT-EFFETTO
    private String wsDtEffetto = DefaultValues.stringVal(Len.WS_DT_EFFETTO);
    //Original name: WK-TAB-CAUSALI
    private WkTabCausali wkTabCausali = new WkTabCausali();
    /**Original name: WK-COST-CAUSALE-TRCH<br>
	 * <pre>    COSTANTE PER CAUSALE DI BUSINESS PER TRANCHE CON
	 *     RICHIESTA DI DISINVESTIMENTO NON CONCLUSA</pre>*/
    private String wkCostCausaleTrch = "99";
    //Original name: WK-ID-ASSTO
    private int wkIdAssto = DefaultValues.INT_VAL;
    //Original name: WK-ID-STRINGA
    private String wkIdStringa = "";
    //Original name: WK-PERCENTUALE
    private AfDecimal wkPercentuale = new AfDecimal(DefaultValues.DEC_VAL, 12, 9);
    //Original name: WK-LQU-ID-LIQ
    private int wkLquIdLiq = DefaultValues.INT_VAL;
    //Original name: WK-RANPERS
    private WkRanpers wkRanpers = new WkRanpers();
    //Original name: SWITCHES
    private Switches switches = new Switches();
    /**Original name: FORMATO<br>
	 * <pre>--------------------------------------------------------------*
	 *     AREA CALCOLO DIFFERENZA TRA DATE                          *
	 * --------------------------------------------------------------*</pre>*/
    private char formato = DefaultValues.CHAR_VAL;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0490 dataInferiore = new DataInferioreLccs0490();
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0490 dataSuperiore = new DataSuperioreLccs0490();
    //Original name: GG-DIFF
    private String ggDiff = DefaultValues.stringVal(Len.GG_DIFF);
    //Original name: CODICE-RITORNO
    private char codiceRitorno = DefaultValues.CHAR_VAL;
    //Original name: DURATA
    private DurataLlbs0230 durata = new DurataLlbs0230();
    //Original name: WK-APPO-RESTO1
    private AfDecimal wkAppoResto1 = new AfDecimal(DefaultValues.DEC_VAL, 10, 5);
    //Original name: WK-APPO-LUNGHEZZA
    private int wkAppoLunghezza = DefaultValues.INT_VAL;
    //Original name: WK-LABEL-ERR
    private String wkLabelErr = DefaultValues.stringVal(Len.WK_LABEL_ERR);
    //Original name: WK-TABELLA
    private String wkTabella = DefaultValues.stringVal(Len.WK_TABELLA);
    //Original name: WS-TIMESTAMP
    private String wsTimestamp = DefaultValues.stringVal(Len.WS_TIMESTAMP);
    //Original name: WK-NOME-PGM
    private String wkNomePgm = "";
    //Original name: LCCC0006
    private Lccc0006 lccc0006 = new Lccc0006();
    /**Original name: WS-TP-STAT-BUS<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_BUS (Stato Oggetto di Business)
	 * *****************************************************************</pre>*/
    private WsTpStatBus wsTpStatBus = new WsTpStatBus();
    /**Original name: WS-TP-RAPP-ANA<br>
	 * <pre>*****************************************************************
	 *     TP_RAPP_ANA (Tipo Rapporto Anagrafico)
	 * *****************************************************************</pre>*/
    private WsTpRappAna wsTpRappAna = new WsTpRappAna();
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>    COPY LCCVXMV0.
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLlbs0230 wsMovimento = new WsMovimentoLlbs0230();
    /**Original name: WS-TP-FRM-ASSVA<br>
	 * <pre>*****************************************************************
	 *     TP_FRM_ASSVA (Forma Assicurativa)
	 * *****************************************************************</pre>*/
    private WsTpFrmAssva wsTpFrmAssva = new WsTpFrmAssva();
    /**Original name: WS-TP-DATO<br>
	 * <pre>*****************************************************************
	 *     TP_D (Tipo Dato)
	 * *****************************************************************</pre>*/
    private WsTpDato wsTpDato = new WsTpDato();
    /**Original name: WS-TP-CAUS<br>
	 * <pre>*****************************************************************
	 *     TP_CAUS (Tipo Causale)
	 * *****************************************************************</pre>*/
    private WsTpCaus wsTpCaus = new WsTpCaus();
    /**Original name: WS-FL-ASSICURATO<br>
	 * <pre>   FLAG PER VERIFICARE SE E' STATA EFFETTUATA LA LETTURA SULLA
	 *    RAPPORTO ANAGRAFICO DELL'ASSICURATO</pre>*/
    private boolean wsFlAssicurato = false;
    /**Original name: WS-FL-ADERENTE<br>
	 * <pre>   FLAG PER VERIFICARE SE E' STATA EFFETTUATA LA LETTURA SULLA
	 *    RAPPORTO ANAGRAFICO DELL'ADERENTE</pre>*/
    private boolean wsFlAderente = false;
    /**Original name: WS-FL-PREMIO-TOTALE<br>
	 * <pre>   FLAG PER VERIFICARE LA PRESENZA DEL CAMPO PREMIO TOTALE SULLA
	 *    DETTAGLIO TITOLO CONTABILE</pre>*/
    private boolean wsFlPremioTotale = true;
    //Original name: WS-DT-CALCOLO
    private WsDtCalcolo wsDtCalcolo = new WsDtCalcolo();
    //Original name: WS-DATA-APPO-NUM
    private String wsDataAppoNum = DefaultValues.stringVal(Len.WS_DATA_APPO_NUM);
    //Original name: WS-DT-QUOT
    private String wsDtQuot = DefaultValues.stringVal(Len.WS_DT_QUOT);
    //Original name: WS-COD-FND
    private String wsCodFnd = DefaultValues.stringVal(Len.WS_COD_FND);
    //Original name: WS-FL-SCARTA-ADES
    private boolean wsFlScartaAdes = false;
    //Original name: WS-ETA-RAGGN-DT-CALC
    private WsEtaRaggnDtCalc wsEtaRaggnDtCalc = new WsEtaRaggnDtCalc();
    //Original name: WSTR-ELE-GRZ-MAX
    private short wstrEleGrzMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSTR-TAB-GAR
    private WgrzTabGar[] wstrTabGar = new WgrzTabGar[WSTR_TAB_GAR_MAXOCCURS];
    //Original name: WTGS-ELE-TGS-MAX
    private short wtgsEleTgsMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WTGS-TAB-TGS1
    private WtgsTabTgs1 wtgsTabTgs1 = new WtgsTabTgs1();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003 = new IoA2kLccc0003();
    //Original name: IN-RCODE
    private String inRcode = DefaultValues.stringVal(Len.IN_RCODE);
    /**Original name: WS-TP-QTZ<br>
	 * <pre>*****************************************************************
	 *     TP_QTZ (Tipo Quotazione)
	 * *****************************************************************
	 * *****************************************************************
	 *     TP_QTZ (Tipo Quotazione)
	 * *****************************************************************</pre>*/
    private WsTpQtz wsTpQtz = new WsTpQtz();
    //Original name: WORK-COMMAREA
    private WorkCommareaLlbs0230 workCommarea = new WorkCommareaLlbs0230();
    //Original name: IVVC0216
    private Ivvc0216Lves0269 ivvc0216 = new Ivvc0216Lves0269();
    //Original name: IVVC0212
    private Ivvc0212 ivvc0212 = new Ivvc0212();
    //Original name: AREA-IO-IVVS0211
    private InputIvvs0211 areaIoIvvs0211 = new InputIvvs0211();
    //Original name: IVVC0218
    private Ivvc0218Loas0310 ivvc0218 = new Ivvc0218Loas0310();
    //Original name: INT-REGISTER1
    private short intRegister1 = DefaultValues.SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Llbs0230Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wstrTabGarIdx = 1; wstrTabGarIdx <= WSTR_TAB_GAR_MAXOCCURS; wstrTabGarIdx++) {
            wstrTabGar[wstrTabGarIdx - 1] = new WgrzTabGar();
        }
    }

    public void setWkVarRecSizeF01(int wkVarRecSizeF01) {
        this.wkVarRecSizeF01 = wkVarRecSizeF01;
    }

    public int getWkVarRecSizeF01() {
        return this.wkVarRecSizeF01;
    }

    public void setWkVarRecSizeF02(int wkVarRecSizeF02) {
        this.wkVarRecSizeF02 = wkVarRecSizeF02;
    }

    public int getWkVarRecSizeF02() {
        return this.wkVarRecSizeF02;
    }

    public void setWkVarRecSizeF03(int wkVarRecSizeF03) {
        this.wkVarRecSizeF03 = wkVarRecSizeF03;
    }

    public int getWkVarRecSizeF03() {
        return this.wkVarRecSizeF03;
    }

    public void setWkVarRecSizeF04(int wkVarRecSizeF04) {
        this.wkVarRecSizeF04 = wkVarRecSizeF04;
    }

    public int getWkVarRecSizeF04() {
        return this.wkVarRecSizeF04;
    }

    public String getWkPgm() {
        return this.wkPgm;
    }

    public String getLdbv5471Formatted() {
        return MarshalByteExt.bufferToStr(getLdbv5471Bytes());
    }

    /**Original name: LDBV5471<br>
	 * <pre>       &#x1c;261 -->  Costi di emissione FINE
	 * --> AREA WHERE CONDITION AD HOC PER LDBS5470</pre>*/
    public byte[] getLdbv5471Bytes() {
        byte[] buffer = new byte[Len.LDBV5471];
        return getLdbv5471Bytes(buffer, 1);
    }

    public byte[] getLdbv5471Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, ldbv5471IdTrchDiGar, Len.Int.LDBV5471_ID_TRCH_DI_GAR, 0);
        return buffer;
    }

    public void setLdbv5471IdTrchDiGar(int ldbv5471IdTrchDiGar) {
        this.ldbv5471IdTrchDiGar = ldbv5471IdTrchDiGar;
    }

    public int getLdbv5471IdTrchDiGar() {
        return this.ldbv5471IdTrchDiGar;
    }

    public String getAreaOutWB05Formatted() {
        return MarshalByteExt.bufferToStr(getAreaOutWB05Bytes());
    }

    /**Original name: AREA-OUT-W-B05<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA FLUSSO DI OUTPUT (FILESQS1) BILA_VAR_CALC_T
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getAreaOutWB05Bytes() {
        byte[] buffer = new byte[Len.AREA_OUT_W_B05];
        return getAreaOutWB05Bytes(buffer, 1);
    }

    public byte[] getAreaOutWB05Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lccvb058.getLenTot());
        position += Types.SHORT_SIZE;
        lccvb058.getRecFisso().getRecFissoBytes(buffer, position);
        position += WB05RecFisso.Len.REC_FISSO;
        MarshalByte.writeChar(buffer, position, lccvb058.getAreaDValorVarInd());
        position += Types.CHAR_SIZE;
        lccvb058.getAreaDValorVarVcharBytes(buffer, position);
        return buffer;
    }

    public String getAreaOutWB03Formatted() {
        return MarshalByteExt.bufferToStr(getAreaOutWB03Bytes());
    }

    /**Original name: AREA-OUT-W-B03<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA FLUSSO DI OUTPUT (FILESQS2) BILA_TRCH_ESTR
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getAreaOutWB03Bytes() {
        byte[] buffer = new byte[Len.AREA_OUT_W_B03];
        return getAreaOutWB03Bytes(buffer, 1);
    }

    public byte[] getAreaOutWB03Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lccvb038.getLenTot());
        position += Types.SHORT_SIZE;
        lccvb038.getRecFisso().getRecFissoBytes(buffer, position);
        return buffer;
    }

    public String getAreaOutWB01Formatted() {
        return MarshalByteExt.bufferToStr(getAreaOutWB01Bytes());
    }

    /**Original name: AREA-OUT-W-B01<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA FLUSSO DI OUTPUT (FILESQS3) BILA_FND_ESTR
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getAreaOutWB01Bytes() {
        byte[] buffer = new byte[Len.AREA_OUT_W_B01];
        return getAreaOutWB01Bytes(buffer, 1);
    }

    public byte[] getAreaOutWB01Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lccvb018.getLenTot());
        position += Types.SHORT_SIZE;
        lccvb018.getRecFisso().getRecFissoBytes(buffer, position);
        return buffer;
    }

    public String getAreaOutWB04Formatted() {
        return MarshalByteExt.bufferToStr(getAreaOutWB04Bytes());
    }

    /**Original name: AREA-OUT-W-B04<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA FLUSSO DI OUTPUT (FILESQS4) BILA_VAR_CALC_P
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getAreaOutWB04Bytes() {
        byte[] buffer = new byte[Len.AREA_OUT_W_B04];
        return getAreaOutWB04Bytes(buffer, 1);
    }

    public byte[] getAreaOutWB04Bytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, lccvb048.getLenTot());
        position += Types.SHORT_SIZE;
        lccvb048.getRecFisso().getRecFissoBytes(buffer, position);
        position += WB04RecFisso.Len.REC_FISSO;
        MarshalByte.writeChar(buffer, position, lccvb048.getAreaDValorVarInd());
        position += Types.CHAR_SIZE;
        lccvb048.getAreaDValorVarVcharBytes(buffer, position);
        return buffer;
    }

    public short getRecScartati1() {
        return this.recScartati1;
    }

    public String getPgmIwfs0050() {
        return this.pgmIwfs0050;
    }

    public String getPgmIwfs0050Formatted() {
        return Functions.padBlanks(getPgmIwfs0050(), Len.PGM_IWFS0050);
    }

    public String getPgmIdbsp670() {
        return this.pgmIdbsp670;
    }

    public String getPgmIdbsp670Formatted() {
        return Functions.padBlanks(getPgmIdbsp670(), Len.PGM_IDBSP670);
    }

    public String getPgmIdbsp630() {
        return this.pgmIdbsp630;
    }

    public String getPgmIdbsp630Formatted() {
        return Functions.padBlanks(getPgmIdbsp630(), Len.PGM_IDBSP630);
    }

    public boolean isWkStringatura() {
        return this.wkStringatura;
    }

    public void setWkNumVar(short wkNumVar) {
        this.wkNumVar = NumericDisplay.asString(wkNumVar, Len.WK_NUM_VAR);
    }

    public short getWkNumVar() {
        return NumericDisplay.asShort(this.wkNumVar);
    }

    public String getWkNumVarFormatted() {
        return this.wkNumVar;
    }

    public String getWkNumVarAsString() {
        return getWkNumVarFormatted();
    }

    public void setWkLunghStringa(short wkLunghStringa) {
        this.wkLunghStringa = NumericDisplay.asString(wkLunghStringa, Len.WK_LUNGH_STRINGA);
    }

    public short getWkLunghStringa() {
        return NumericDisplay.asShort(this.wkLunghStringa);
    }

    public String getWkLunghStringaFormatted() {
        return this.wkLunghStringa;
    }

    public String getWkLunghStringaAsString() {
        return getWkLunghStringaFormatted();
    }

    public void setWsFlScriviTrch(boolean wsFlScriviTrch) {
        this.wsFlScriviTrch = wsFlScriviTrch;
    }

    public boolean isWsFlScriviTrch() {
        return this.wsFlScriviTrch;
    }

    public void setWsFlTuttiFndA0(boolean wsFlTuttiFndA0) {
        this.wsFlTuttiFndA0 = wsFlTuttiFndA0;
    }

    public boolean isWsFlTuttiFndA0() {
        return this.wsFlTuttiFndA0;
    }

    public void setWkDataAppo9Aa(short wkDataAppo9Aa) {
        this.wkDataAppo9Aa = NumericDisplay.asString(wkDataAppo9Aa, Len.WK_DATA_APPO9_AA);
    }

    public void setWkDataAppo9AaFormatted(String wkDataAppo9Aa) {
        this.wkDataAppo9Aa = Trunc.toUnsignedNumeric(wkDataAppo9Aa, Len.WK_DATA_APPO9_AA);
    }

    public short getWkDataAppo9Aa() {
        return NumericDisplay.asShort(this.wkDataAppo9Aa);
    }

    public String getWkDataAppo9AaFormatted() {
        return this.wkDataAppo9Aa;
    }

    public void setWkDtDecorTrch(int wkDtDecorTrch) {
        this.wkDtDecorTrch = NumericDisplay.asString(wkDtDecorTrch, Len.WK_DT_DECOR_TRCH);
    }

    public int getWkDtDecorTrch() {
        return NumericDisplay.asInt(this.wkDtDecorTrch);
    }

    public String getWkDtDecorTrchFormatted() {
        return this.wkDtDecorTrch;
    }

    public void setWsDataRiservaComp(int wsDataRiservaComp) {
        this.wsDataRiservaComp = wsDataRiservaComp;
    }

    public int getWsDataRiservaComp() {
        return this.wsDataRiservaComp;
    }

    public void setWkAppoAreaStb(String wkAppoAreaStb) {
        this.wkAppoAreaStb = Functions.subString(wkAppoAreaStb, Len.WK_APPO_AREA_STB);
    }

    public String getWkAppoAreaStb() {
        return this.wkAppoAreaStb;
    }

    public String getWkAppoAreaStbFormatted() {
        return Functions.padBlanks(getWkAppoAreaStb(), Len.WK_APPO_AREA_STB);
    }

    public void setWkDtUltAdegPrePr(int wkDtUltAdegPrePr) {
        this.wkDtUltAdegPrePr = wkDtUltAdegPrePr;
    }

    public int getWkDtUltAdegPrePr() {
        return this.wkDtUltAdegPrePr;
    }

    public void setWkCodParam(String wkCodParam) {
        this.wkCodParam = Functions.subString(wkCodParam, Len.WK_COD_PARAM);
    }

    public String getWkCodParam() {
        return this.wkCodParam;
    }

    public void setWkDaScartare(boolean wkDaScartare) {
        this.wkDaScartare = wkDaScartare;
    }

    public boolean isWkDaScartare() {
        return this.wkDaScartare;
    }

    public void setWsTpStatBusPol(String wsTpStatBusPol) {
        this.wsTpStatBusPol = Functions.subString(wsTpStatBusPol, Len.WS_TP_STAT_BUS_POL);
    }

    public String getWsTpStatBusPol() {
        return this.wsTpStatBusPol;
    }

    public void setWsTpCausPol(String wsTpCausPol) {
        this.wsTpCausPol = Functions.subString(wsTpCausPol, Len.WS_TP_CAUS_POL);
    }

    public String getWsTpCausPol() {
        return this.wsTpCausPol;
    }

    public void setWsTpStatBusAde(String wsTpStatBusAde) {
        this.wsTpStatBusAde = Functions.subString(wsTpStatBusAde, Len.WS_TP_STAT_BUS_ADE);
    }

    public String getWsTpStatBusAde() {
        return this.wsTpStatBusAde;
    }

    public void setWsTpCausAde(String wsTpCausAde) {
        this.wsTpCausAde = Functions.subString(wsTpCausAde, Len.WS_TP_CAUS_ADE);
    }

    public String getWsTpCausAde() {
        return this.wsTpCausAde;
    }

    public void setWsTpStatBusTga(String wsTpStatBusTga) {
        this.wsTpStatBusTga = Functions.subString(wsTpStatBusTga, Len.WS_TP_STAT_BUS_TGA);
    }

    public String getWsTpStatBusTga() {
        return this.wsTpStatBusTga;
    }

    public void setWsTpCausTga(String wsTpCausTga) {
        this.wsTpCausTga = Functions.subString(wsTpCausTga, Len.WS_TP_CAUS_TGA);
    }

    public String getWsTpCausTga() {
        return this.wsTpCausTga;
    }

    public void setWsDtRst9(int wsDtRst9) {
        this.wsDtRst9 = NumericDisplay.asString(wsDtRst9, Len.WS_DT_RST9);
    }

    public void setWsDtRst9FromBuffer(byte[] buffer) {
        wsDtRst9 = MarshalByte.readFixedString(buffer, 1, Len.WS_DT_RST9);
    }

    public int getWsDtRst9() {
        return NumericDisplay.asInt(this.wsDtRst9);
    }

    public void setWsDtIni9(int wsDtIni9) {
        this.wsDtIni9 = NumericDisplay.asString(wsDtIni9, Len.WS_DT_INI9);
    }

    public void setWsDtIni9FromBuffer(byte[] buffer) {
        wsDtIni9 = MarshalByte.readFixedString(buffer, 1, Len.WS_DT_INI9);
    }

    public int getWsDtIni9() {
        return NumericDisplay.asInt(this.wsDtIni9);
    }

    public void setWkAdesione9(int wkAdesione9) {
        this.wkAdesione9 = NumericDisplay.asString(wkAdesione9, Len.WK_ADESIONE9);
    }

    public int getWkAdesione9() {
        return NumericDisplay.asInt(this.wkAdesione9);
    }

    public String getWkAdesione9Formatted() {
        return this.wkAdesione9;
    }

    public void setWsDtPtfX(String wsDtPtfX) {
        this.wsDtPtfX = Functions.subString(wsDtPtfX, Len.WS_DT_PTF_X);
    }

    public String getWsDtPtfX() {
        return this.wsDtPtfX;
    }

    public void setWsDtTsPtf(long wsDtTsPtf) {
        this.wsDtTsPtf = wsDtTsPtf;
    }

    public long getWsDtTsPtf() {
        return this.wsDtTsPtf;
    }

    public void setWsSommaImpoLiq(AfDecimal wsSommaImpoLiq) {
        this.wsSommaImpoLiq.assign(wsSommaImpoLiq);
    }

    public AfDecimal getWsSommaImpoLiq() {
        return this.wsSommaImpoLiq.copy();
    }

    public void setWsAppoNum18(long wsAppoNum18) {
        this.wsAppoNum18 = NumericDisplay.asString(wsAppoNum18, Len.WS_APPO_NUM18);
    }

    public long getWsAppoNum18() {
        return NumericDisplay.asLong(this.wsAppoNum18);
    }

    public String getWsAppoNum18Formatted() {
        return this.wsAppoNum18;
    }

    public void setWsDiffPre(AfDecimal wsDiffPre) {
        this.wsDiffPre.assign(wsDiffPre);
    }

    public AfDecimal getWsDiffPre() {
        return this.wsDiffPre.copy();
    }

    public void setWsTotProvAcqWtga(AfDecimal wsTotProvAcqWtga) {
        this.wsTotProvAcqWtga.assign(wsTotProvAcqWtga);
    }

    public AfDecimal getWsTotProvAcqWtga() {
        return this.wsTotProvAcqWtga.copy();
    }

    public void setWsDtEffettoFormatted(String wsDtEffetto) {
        this.wsDtEffetto = Trunc.toUnsignedNumeric(wsDtEffetto, Len.WS_DT_EFFETTO);
    }

    public int getWsDtEffetto() {
        return NumericDisplay.asInt(this.wsDtEffetto);
    }

    public String getWsDtEffettoFormatted() {
        return this.wsDtEffetto;
    }

    public String getWkCostCausaleTrch() {
        return this.wkCostCausaleTrch;
    }

    public void setWkIdAssto(int wkIdAssto) {
        this.wkIdAssto = wkIdAssto;
    }

    public int getWkIdAssto() {
        return this.wkIdAssto;
    }

    public void setWkIdStringa(String wkIdStringa) {
        this.wkIdStringa = Functions.subString(wkIdStringa, Len.WK_ID_STRINGA);
    }

    public String getWkIdStringa() {
        return this.wkIdStringa;
    }

    public String getWkIdStringaFormatted() {
        return Functions.padBlanks(getWkIdStringa(), Len.WK_ID_STRINGA);
    }

    public void setWkPercentuale(AfDecimal wkPercentuale) {
        this.wkPercentuale.assign(wkPercentuale);
    }

    public AfDecimal getWkPercentuale() {
        return this.wkPercentuale.copy();
    }

    public void setWkLquIdLiq(int wkLquIdLiq) {
        this.wkLquIdLiq = wkLquIdLiq;
    }

    public int getWkLquIdLiq() {
        return this.wkLquIdLiq;
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setFormatoFormatted(String formato) {
        setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        formato = MarshalByte.readChar(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    public void setGgDiff(int ggDiff) {
        this.ggDiff = NumericDisplay.asString(ggDiff, Len.GG_DIFF);
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        ggDiff = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    public String getGgDiffFormatted() {
        return this.ggDiff;
    }

    public void setCodiceRitorno(char codiceRitorno) {
        this.codiceRitorno = codiceRitorno;
    }

    public void setCodiceRitornoFromBuffer(byte[] buffer) {
        codiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getCodiceRitorno() {
        return this.codiceRitorno;
    }

    public void setWkAppoResto1(AfDecimal wkAppoResto1) {
        this.wkAppoResto1.assign(wkAppoResto1);
    }

    public AfDecimal getWkAppoResto1() {
        return this.wkAppoResto1.copy();
    }

    public void setWkAppoLunghezza(int wkAppoLunghezza) {
        this.wkAppoLunghezza = wkAppoLunghezza;
    }

    public int getWkAppoLunghezza() {
        return this.wkAppoLunghezza;
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public void setWkTabella(String wkTabella) {
        this.wkTabella = Functions.subString(wkTabella, Len.WK_TABELLA);
    }

    public String getWkTabella() {
        return this.wkTabella;
    }

    public String getWkTabellaFormatted() {
        return Functions.padBlanks(getWkTabella(), Len.WK_TABELLA);
    }

    public void setWsTimestamp(String wsTimestamp) {
        this.wsTimestamp = Functions.subString(wsTimestamp, Len.WS_TIMESTAMP);
    }

    public String getWsTimestamp() {
        return this.wsTimestamp;
    }

    public void setWkNomePgm(String wkNomePgm) {
        this.wkNomePgm = Functions.subString(wkNomePgm, Len.WK_NOME_PGM);
    }

    public String getWkNomePgm() {
        return this.wkNomePgm;
    }

    public void setWsFlAssicurato(boolean wsFlAssicurato) {
        this.wsFlAssicurato = wsFlAssicurato;
    }

    public boolean isWsFlAssicurato() {
        return this.wsFlAssicurato;
    }

    public void setWsFlAderente(boolean wsFlAderente) {
        this.wsFlAderente = wsFlAderente;
    }

    public boolean isWsFlAderente() {
        return this.wsFlAderente;
    }

    public void setWsFlPremioTotale(boolean wsFlPremioTotale) {
        this.wsFlPremioTotale = wsFlPremioTotale;
    }

    public boolean isWsFlPremioTotale() {
        return this.wsFlPremioTotale;
    }

    public void setWsDataAppoNum(int wsDataAppoNum) {
        this.wsDataAppoNum = NumericDisplay.asString(wsDataAppoNum, Len.WS_DATA_APPO_NUM);
    }

    public int getWsDataAppoNum() {
        return NumericDisplay.asInt(this.wsDataAppoNum);
    }

    public String getWsDataAppoNumFormatted() {
        return this.wsDataAppoNum;
    }

    public void setWsDtQuotFormatted(String wsDtQuot) {
        this.wsDtQuot = Trunc.toUnsignedNumeric(wsDtQuot, Len.WS_DT_QUOT);
    }

    public int getWsDtQuot() {
        return NumericDisplay.asInt(this.wsDtQuot);
    }

    public void setWsCodFnd(String wsCodFnd) {
        this.wsCodFnd = Functions.subString(wsCodFnd, Len.WS_COD_FND);
    }

    public String getWsCodFnd() {
        return this.wsCodFnd;
    }

    public void setWsFlScartaAdes(boolean wsFlScartaAdes) {
        this.wsFlScartaAdes = wsFlScartaAdes;
    }

    public boolean isWsFlScartaAdes() {
        return this.wsFlScartaAdes;
    }

    public void setWstrEleGrzMax(short wstrEleGrzMax) {
        this.wstrEleGrzMax = wstrEleGrzMax;
    }

    public short getWstrEleGrzMax() {
        return this.wstrEleGrzMax;
    }

    public void setWtgsEleTgsMax(short wtgsEleTgsMax) {
        this.wtgsEleTgsMax = wtgsEleTgsMax;
    }

    public short getWtgsEleTgsMax() {
        return this.wtgsEleTgsMax;
    }

    public void setInRcodeFormatted(String inRcode) {
        this.inRcode = Trunc.toUnsignedNumeric(inRcode, Len.IN_RCODE);
    }

    public void setInRcodeFromBuffer(byte[] buffer) {
        inRcode = MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
    }

    public short getInRcode() {
        return NumericDisplay.asShort(this.inRcode);
    }

    public String getInRcodeFormatted() {
        return this.inRcode;
    }

    public String getInRcodeAsString() {
        return getInRcodeFormatted();
    }

    public void setWskdAreaSchedaFormatted(String data) {
        byte[] buffer = new byte[Len.WSKD_AREA_SCHEDA];
        MarshalByte.writeString(buffer, 1, data, Len.WSKD_AREA_SCHEDA);
        setWskdAreaSchedaBytes(buffer, 1);
    }

    public void setWskdAreaSchedaBytes(byte[] buffer, int offset) {
        int position = offset;
        ivvc0216.setWskdDee(MarshalByte.readString(buffer, position, Ivvc0216Lves0269.Len.WSKD_DEE));
        position += Ivvc0216Lves0269.Len.WSKD_DEE;
        ivvc0216.setWskdEleLivelloMaxP(MarshalByte.readPackedAsShort(buffer, position, Ivvc0216Lves0269.Len.Int.WSKD_ELE_LIVELLO_MAX_P, 0));
        position += Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_P;
        ivvc0216.getWskdTabValP().setWskdTabValPBytes(buffer, position);
        position += WskdTabValP.Len.WSKD_TAB_VAL_P;
        ivvc0216.setWskdEleLivelloMaxT(MarshalByte.readPackedAsShort(buffer, position, Ivvc0216Lves0269.Len.Int.WSKD_ELE_LIVELLO_MAX_T, 0));
        position += Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_T;
        ivvc0216.getWskdTabValT().setWskdTabValTBytes(buffer, position);
        position += WskdTabValT.Len.WSKD_TAB_VAL_T;
        ivvc0216.setWskdVarAutOperBytes(buffer, position);
    }

    public String getWcntAreaDatiContestFormatted() {
        return ivvc0212.getWcntAreaVariabiliContFormatted();
    }

    public void setIntRegister1(short intRegister1) {
        this.intRegister1 = intRegister1;
    }

    public short getIntRegister1() {
        return this.intRegister1;
    }

    public AccCommLlbs0230 getAccComm() {
        return accComm;
    }

    public InputIvvs0211 getAreaIoIvvs0211() {
        return areaIoIvvs0211;
    }

    public AreaCallIwfs0050 getAreaIwfs0050() {
        return areaIwfs0050;
    }

    public AreaLdbv1131 getAreaLdbv1131() {
        return areaLdbv1131;
    }

    public AreaOutB03 getAreaOutB03() {
        return areaOutB03;
    }

    public AreaOutB04 getAreaOutB04() {
        return areaOutB04;
    }

    public AreaOutB05 getAreaOutB05() {
        return areaOutB05;
    }

    public AreaOutScarti getAreaOutScarti() {
        return areaOutScarti;
    }

    public BilaFndEstrLlbs0230 getBilaFndEstr() {
        return bilaFndEstr;
    }

    public BilaTrchEstr getBilaTrchEstr() {
        return bilaTrchEstr;
    }

    public BilaVarCalcP getBilaVarCalcP() {
        return bilaVarCalcP;
    }

    public BilaVarCalcT getBilaVarCalcT() {
        return bilaVarCalcT;
    }

    public DataInferioreLccs0490 getDataInferiore() {
        return dataInferiore;
    }

    public DataSuperioreLccs0490 getDataSuperiore() {
        return dataSuperiore;
    }

    public DettTitCont getDettTitCont() {
        return dettTitCont;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public DurataLlbs0230 getDurata() {
        return durata;
    }

    public EstPoliCpiPr getEstPoliCpiPr() {
        return estPoliCpiPr;
    }

    public EstTrchDiGar getEstTrchDiGar() {
        return estTrchDiGar;
    }

    public Iabcsq99 getIabcsq99() {
        return iabcsq99;
    }

    public Iabvsqs1 getIabvsqs1() {
        return iabvsqs1;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Idsv0503 getIdsv0503() {
        return idsv0503;
    }

    public Idsv8888 getIdsv8888() {
        return idsv8888;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public IoA2kLccc0003 getIoA2kLccc0003() {
        return ioA2kLccc0003;
    }

    public Ivvc0212 getIvvc0212() {
        return ivvc0212;
    }

    public Ivvc0216Lves0269 getIvvc0216() {
        return ivvc0216;
    }

    public Ivvc0218Loas0310 getIvvc0218() {
        return ivvc0218;
    }

    public IxIndiciLlbs0230 getIxIndici() {
        return ixIndici;
    }

    public Lccc0006 getLccc0006() {
        return lccc0006;
    }

    public Lccc0450 getLccc0450() {
        return lccc0450;
    }

    public Lccvb018 getLccvb018() {
        return lccvb018;
    }

    public Lccvb038 getLccvb038() {
        return lccvb038;
    }

    public Lccvb048 getLccvb048() {
        return lccvb048;
    }

    public Lccvb058 getLccvb058() {
        return lccvb058;
    }

    public Ldbv0371Llbs0230 getLdbv0371() {
        return ldbv0371;
    }

    public Ldbv0641 getLdbv0641() {
        return ldbv0641;
    }

    public Ldbv1241 getLdbv1241() {
        return ldbv1241;
    }

    public Ldbv1301 getLdbv1301() {
        return ldbv1301;
    }

    public Ldbv2631 getLdbv2631() {
        return ldbv2631;
    }

    public Ldbv2651 getLdbv2651() {
        return ldbv2651;
    }

    public Ldbv3401Llbs0230 getLdbv3401() {
        return ldbv3401;
    }

    public Ldbv3411Llbs0230 getLdbv3411() {
        return ldbv3411;
    }

    public Ldbv3421Llbs0230 getLdbv3421() {
        return ldbv3421;
    }

    public Ldbv4151Llbs0230 getLdbv4151() {
        return ldbv4151;
    }

    public Ldbv4911 getLdbv4911() {
        return ldbv4911;
    }

    public Ldbv9091Llbs0230 getLdbv9091() {
        return ldbv9091;
    }

    public Ldbv0641 getLdbvh601() {
        return ldbvh601;
    }

    public Liq getLiq() {
        return liq;
    }

    public Movi getMovi() {
        return movi;
    }

    public ParamComp getParamComp() {
        return paramComp;
    }

    public ParamMovi getParamMovi() {
        return paramMovi;
    }

    public ParamOggLoas0800 getParamOgg() {
        return paramOgg;
    }

    public PersLlbs0230 getPers() {
        return pers;
    }

    public QuotzAggFndLccs0450 getQuotzAggFnd() {
        return quotzAggFnd;
    }

    public QuotzFndUnitLccs0450 getQuotzFndUnit() {
        return quotzFndUnit;
    }

    public RappAna getRappAna() {
        return rappAna;
    }

    public RappRete getRappRete() {
        return rappRete;
    }

    public RichDisFndLccs0450 getRichDisFnd() {
        return richDisFnd;
    }

    public RisDiTrch getRisDiTrch() {
        return risDiTrch;
    }

    public Switches getSwitches() {
        return switches;
    }

    public TrchDiGarIvvs0216 getTrchDiGar() {
        return trchDiGar;
    }

    public ValAst getValAst() {
        return valAst;
    }

    public WkAlqMargCSubrsh getWkAlqMargCSubrsh() {
        return wkAlqMargCSubrsh;
    }

    public WkAlqMargRis getWkAlqMargRis() {
        return wkAlqMargRis;
    }

    public WkDataAppoN getWkDataAppoN() {
        return wkDataAppoN;
    }

    public WkMesiCarenza getWkMesiCarenza() {
        return wkMesiCarenza;
    }

    public WkRanpers getWkRanpers() {
        return wkRanpers;
    }

    public WkStrada getWkStrada() {
        return wkStrada;
    }

    public WkTabCausali getWkTabCausali() {
        return wkTabCausali;
    }

    public WkTipoElaborazione getWkTipoElaborazione() {
        return wkTipoElaborazione;
    }

    public WorkCommareaLlbs0230 getWorkCommarea() {
        return workCommarea;
    }

    public WsDtCalcolo getWsDtCalcolo() {
        return wsDtCalcolo;
    }

    public WsDtIni getWsDtIni() {
        return wsDtIni;
    }

    public WsDtN getWsDtN() {
        return wsDtN;
    }

    public WsDtRst getWsDtRst() {
        return wsDtRst;
    }

    public WsDtX getWsDtX() {
        return wsDtX;
    }

    public WsEtaRaggnDtCalc getWsEtaRaggnDtCalc() {
        return wsEtaRaggnDtCalc;
    }

    public WsMovimentoLlbs0230 getWsMovimento() {
        return wsMovimento;
    }

    public WsPreTot getWsPreTot() {
        return wsPreTot;
    }

    public WsTaxTot getWsTaxTot() {
        return wsTaxTot;
    }

    public WsTpCaus getWsTpCaus() {
        return wsTpCaus;
    }

    public WsTpDato getWsTpDato() {
        return wsTpDato;
    }

    public WsTpFrmAssva getWsTpFrmAssva() {
        return wsTpFrmAssva;
    }

    public WsTpPerPremio getWsTpPerPremio() {
        return wsTpPerPremio;
    }

    public WsTpQtz getWsTpQtz() {
        return wsTpQtz;
    }

    public WsTpRappAna getWsTpRappAna() {
        return wsTpRappAna;
    }

    public WsTpStatBus getWsTpStatBus() {
        return wsTpStatBus;
    }

    public WgrzTabGar getWstrTabGar(int idx) {
        return wstrTabGar[idx - 1];
    }

    public WtgsTabTgs1 getWtgsTabTgs1() {
        return wtgsTabTgs1;
    }

    public DFiscAdes getdFiscAdes() {
        return dFiscAdes;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_NUM_VAR = 2;
        public static final int WK_LUNGH_STRINGA = 4;
        public static final int WK_DATA_APPO9_AA = 4;
        public static final int WK_DT_DECOR_TRCH = 8;
        public static final int WK_DATA_CALCOLATA = 8;
        public static final int WK_DT_DECOR_POL = 8;
        public static final int FLR2 = 3;
        public static final int WK_APPO_AREA_STB = 92;
        public static final int WS_NUM_AA_PAG_PRE = 5;
        public static final int WS_FRQ_MOVI = 5;
        public static final int WS_FRAZ_INI_EROG_REN = 5;
        public static final int WS_PRSTZ_ULT = 12;
        public static final int WK_DT_ULT_ADEG_PRE_PR = 8;
        public static final int WS_DT_SCAD_PAG_PRE = 8;
        public static final int WS_DUR_RES_DT_CALC = 5;
        public static final int WS_NUM_PRE_PATT = 5;
        public static final int FLR4 = 4;
        public static final int FLR6 = 2;
        public static final int WK_COD_PARAM = 20;
        public static final int WS_ANNO = 4;
        public static final int WS_DT_RST9 = 8;
        public static final int WS_DT_INI9 = 8;
        public static final int WK_ADESIONE9 = 8;
        public static final int WS_DT_PTF_X = 10;
        public static final int WS_DT_COMPETENZA8 = 8;
        public static final int WS_LT40 = 2;
        public static final int WS_LT_ORA_COMPETENZA8 = 8;
        public static final int WS_DT_COMP_RST8 = 8;
        public static final int WS_LT40_RST = 2;
        public static final int WS_LT_ORA_COMP_RST8 = 8;
        public static final int WS_APPO_NUM18 = 18;
        public static final int WK_ID_MOVI_CRZ = 9;
        public static final int WS_DT_EFFETTO = 8;
        public static final int WK_DATA_ULT_ADEG = 8;
        public static final int WK_DATA_DECO_CALC = 8;
        public static final int WK_VARIABILE = 12;
        public static final int GG_DIFF = 5;
        public static final int WK_APPO_LUNGHEZZA = 9;
        public static final int WK_LABEL_ERR = 50;
        public static final int WK_STRING = 85;
        public static final int WK_TABELLA = 20;
        public static final int WS_TIMESTAMP = 18;
        public static final int WS_DATA_TS = 18;
        public static final int WK_APPO_DATA = 8;
        public static final int WS_DATA_APPO_NUM = 8;
        public static final int WS_DT_QUOT = 8;
        public static final int WS_COD_FND = 20;
        public static final int IN_RCODE = 2;
        public static final int WS_TP_STAT_BUS_POL = 2;
        public static final int WS_TP_CAUS_POL = 2;
        public static final int WS_TP_STAT_BUS_ADE = 2;
        public static final int WS_TP_CAUS_ADE = 2;
        public static final int WS_TP_STAT_BUS_TGA = 2;
        public static final int WS_TP_CAUS_TGA = 2;
        public static final int WK_NOME_PGM = 8;
        public static final int LDBV5471_ID_TRCH_DI_GAR = 5;
        public static final int LDBV5471 = LDBV5471_ID_TRCH_DI_GAR;
        public static final int WSKD_AREA_SCHEDA = Ivvc0216Lves0269.Len.WSKD_DEE + Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_P + WskdTabValP.Len.WSKD_TAB_VAL_P + Ivvc0216Lves0269.Len.WSKD_ELE_LIVELLO_MAX_T + WskdTabValT.Len.WSKD_TAB_VAL_T + Ivvc0216Lves0269.Len.WSKD_VAR_AUT_OPER;
        public static final int WCNT_AREA_DATI_CONTEST = Ivvc0212.Len.WCNT_AREA_VARIABILI_CONT;
        public static final int AREA_OUT_W_B03 = Lccvb038.Len.LEN_TOT + WB03RecFisso.Len.REC_FISSO;
        public static final int AREA_OUT_W_B01 = Lccvb018.Len.LEN_TOT + WB01RecFisso.Len.REC_FISSO;
        public static final int AREA_OUT_W_B05 = Lccvb058.Len.LEN_TOT + WB05RecFisso.Len.REC_FISSO + Lccvb058.Len.AREA_D_VALOR_VAR_IND + Lccvb058.Len.AREA_D_VALOR_VAR_VCHAR;
        public static final int AREA_OUT_W_B04 = Lccvb048.Len.LEN_TOT + WB04RecFisso.Len.REC_FISSO + Lccvb048.Len.AREA_D_VALOR_VAR_IND + Lccvb048.Len.AREA_D_VALOR_VAR_VCHAR;
        public static final int WK_ID_STRINGA = 20;
        public static final int PGM_IWFS0050 = 8;
        public static final int PGM_IDBSP670 = 8;
        public static final int PGM_IDBSP630 = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV5471_ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
