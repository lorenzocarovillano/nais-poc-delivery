package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMP-DIR-DA-RIMB<br>
 * Variable: LQU-IMP-DIR-DA-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpDirDaRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpDirDaRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMP_DIR_DA_RIMB;
    }

    public void setLquImpDirDaRimb(AfDecimal lquImpDirDaRimb) {
        writeDecimalAsPacked(Pos.LQU_IMP_DIR_DA_RIMB, lquImpDirDaRimb.copy());
    }

    public void setLquImpDirDaRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMP_DIR_DA_RIMB, Pos.LQU_IMP_DIR_DA_RIMB);
    }

    /**Original name: LQU-IMP-DIR-DA-RIMB<br>*/
    public AfDecimal getLquImpDirDaRimb() {
        return readPackedAsDecimal(Pos.LQU_IMP_DIR_DA_RIMB, Len.Int.LQU_IMP_DIR_DA_RIMB, Len.Fract.LQU_IMP_DIR_DA_RIMB);
    }

    public byte[] getLquImpDirDaRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMP_DIR_DA_RIMB, Pos.LQU_IMP_DIR_DA_RIMB);
        return buffer;
    }

    public void setLquImpDirDaRimbNull(String lquImpDirDaRimbNull) {
        writeString(Pos.LQU_IMP_DIR_DA_RIMB_NULL, lquImpDirDaRimbNull, Len.LQU_IMP_DIR_DA_RIMB_NULL);
    }

    /**Original name: LQU-IMP-DIR-DA-RIMB-NULL<br>*/
    public String getLquImpDirDaRimbNull() {
        return readString(Pos.LQU_IMP_DIR_DA_RIMB_NULL, Len.LQU_IMP_DIR_DA_RIMB_NULL);
    }

    public String getLquImpDirDaRimbNullFormatted() {
        return Functions.padBlanks(getLquImpDirDaRimbNull(), Len.LQU_IMP_DIR_DA_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMP_DIR_DA_RIMB = 1;
        public static final int LQU_IMP_DIR_DA_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMP_DIR_DA_RIMB = 8;
        public static final int LQU_IMP_DIR_DA_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMP_DIR_DA_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMP_DIR_DA_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
