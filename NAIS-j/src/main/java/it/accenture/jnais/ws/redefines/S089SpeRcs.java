package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-SPE-RCS<br>
 * Variable: S089-SPE-RCS from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089SpeRcs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089SpeRcs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_SPE_RCS;
    }

    public void setWlquSpeRcs(AfDecimal wlquSpeRcs) {
        writeDecimalAsPacked(Pos.S089_SPE_RCS, wlquSpeRcs.copy());
    }

    public void setWlquSpeRcsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_SPE_RCS, Pos.S089_SPE_RCS);
    }

    /**Original name: WLQU-SPE-RCS<br>*/
    public AfDecimal getWlquSpeRcs() {
        return readPackedAsDecimal(Pos.S089_SPE_RCS, Len.Int.WLQU_SPE_RCS, Len.Fract.WLQU_SPE_RCS);
    }

    public byte[] getWlquSpeRcsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_SPE_RCS, Pos.S089_SPE_RCS);
        return buffer;
    }

    public void initWlquSpeRcsSpaces() {
        fill(Pos.S089_SPE_RCS, Len.S089_SPE_RCS, Types.SPACE_CHAR);
    }

    public void setWlquSpeRcsNull(String wlquSpeRcsNull) {
        writeString(Pos.S089_SPE_RCS_NULL, wlquSpeRcsNull, Len.WLQU_SPE_RCS_NULL);
    }

    /**Original name: WLQU-SPE-RCS-NULL<br>*/
    public String getWlquSpeRcsNull() {
        return readString(Pos.S089_SPE_RCS_NULL, Len.WLQU_SPE_RCS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_SPE_RCS = 1;
        public static final int S089_SPE_RCS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_SPE_RCS = 8;
        public static final int WLQU_SPE_RCS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_SPE_RCS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_SPE_RCS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
