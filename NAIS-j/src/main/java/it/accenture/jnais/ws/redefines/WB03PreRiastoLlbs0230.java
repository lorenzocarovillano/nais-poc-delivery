package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PRE-RIASTO<br>
 * Variable: W-B03-PRE-RIASTO from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PreRiastoLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PreRiastoLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PRE_RIASTO;
    }

    public void setwB03PreRiasto(AfDecimal wB03PreRiasto) {
        writeDecimalAsPacked(Pos.W_B03_PRE_RIASTO, wB03PreRiasto.copy());
    }

    /**Original name: W-B03-PRE-RIASTO<br>*/
    public AfDecimal getwB03PreRiasto() {
        return readPackedAsDecimal(Pos.W_B03_PRE_RIASTO, Len.Int.W_B03_PRE_RIASTO, Len.Fract.W_B03_PRE_RIASTO);
    }

    public byte[] getwB03PreRiastoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PRE_RIASTO, Pos.W_B03_PRE_RIASTO);
        return buffer;
    }

    public void setwB03PreRiastoNull(String wB03PreRiastoNull) {
        writeString(Pos.W_B03_PRE_RIASTO_NULL, wB03PreRiastoNull, Len.W_B03_PRE_RIASTO_NULL);
    }

    /**Original name: W-B03-PRE-RIASTO-NULL<br>*/
    public String getwB03PreRiastoNull() {
        return readString(Pos.W_B03_PRE_RIASTO_NULL, Len.W_B03_PRE_RIASTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_RIASTO = 1;
        public static final int W_B03_PRE_RIASTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PRE_RIASTO = 8;
        public static final int W_B03_PRE_RIASTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_RIASTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PRE_RIASTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
