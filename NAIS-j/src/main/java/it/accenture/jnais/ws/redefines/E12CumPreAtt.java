package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: E12-CUM-PRE-ATT<br>
 * Variable: E12-CUM-PRE-ATT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class E12CumPreAtt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public E12CumPreAtt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.E12_CUM_PRE_ATT;
    }

    public void setE12CumPreAtt(AfDecimal e12CumPreAtt) {
        writeDecimalAsPacked(Pos.E12_CUM_PRE_ATT, e12CumPreAtt.copy());
    }

    public void setE12CumPreAttFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.E12_CUM_PRE_ATT, Pos.E12_CUM_PRE_ATT);
    }

    /**Original name: E12-CUM-PRE-ATT<br>*/
    public AfDecimal getE12CumPreAtt() {
        return readPackedAsDecimal(Pos.E12_CUM_PRE_ATT, Len.Int.E12_CUM_PRE_ATT, Len.Fract.E12_CUM_PRE_ATT);
    }

    public byte[] getE12CumPreAttAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.E12_CUM_PRE_ATT, Pos.E12_CUM_PRE_ATT);
        return buffer;
    }

    public void setE12CumPreAttNull(String e12CumPreAttNull) {
        writeString(Pos.E12_CUM_PRE_ATT_NULL, e12CumPreAttNull, Len.E12_CUM_PRE_ATT_NULL);
    }

    /**Original name: E12-CUM-PRE-ATT-NULL<br>*/
    public String getE12CumPreAttNull() {
        return readString(Pos.E12_CUM_PRE_ATT_NULL, Len.E12_CUM_PRE_ATT_NULL);
    }

    public String getE12CumPreAttNullFormatted() {
        return Functions.padBlanks(getE12CumPreAttNull(), Len.E12_CUM_PRE_ATT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int E12_CUM_PRE_ATT = 1;
        public static final int E12_CUM_PRE_ATT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int E12_CUM_PRE_ATT = 8;
        public static final int E12_CUM_PRE_ATT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int E12_CUM_PRE_ATT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int E12_CUM_PRE_ATT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
