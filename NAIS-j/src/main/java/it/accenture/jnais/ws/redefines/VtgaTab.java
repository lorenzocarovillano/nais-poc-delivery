package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: VTGA-TAB<br>
 * Variable: VTGA-TAB from program LOAS0310<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class VtgaTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 1250;
    public static final char VTGA_ST_ADD = 'A';
    public static final char VTGA_ST_MOD = 'M';
    public static final char VTGA_ST_INV = 'I';
    public static final char VTGA_ST_DEL = 'D';
    public static final char VTGA_ST_CON = 'C';

    //==== CONSTRUCTORS ====
    public VtgaTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.VTGA_TAB;
    }

    public String getVtgaTabFormatted() {
        return readFixedString(Pos.VTGA_TAB, Len.VTGA_TAB);
    }

    public void setVtgaTabBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.VTGA_TAB, Pos.VTGA_TAB);
    }

    public byte[] getVtgaTabBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.VTGA_TAB, Pos.VTGA_TAB);
        return buffer;
    }

    public void setTabTranBytes(int tabTranIdx, byte[] buffer) {
        setTabTranBytes(tabTranIdx, buffer, 1);
    }

    public void setTabTranBytes(int tabTranIdx, byte[] buffer, int offset) {
        int position = Pos.vtgaTabTran(tabTranIdx - 1);
        setBytes(buffer, offset, Len.TAB_TRAN, position);
    }

    public void setStatus(int statusIdx, char status) {
        int position = Pos.vtgaStatus(statusIdx - 1);
        writeChar(position, status);
    }

    /**Original name: VTGA-STATUS<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    AREA TRCH_DI_GAR
	 *    ALIAS TGA
	 *    ULTIMO AGG. 03 GIU 2019
	 * ------------------------------------------------------------</pre>*/
    public char getStatus(int statusIdx) {
        int position = Pos.vtgaStatus(statusIdx - 1);
        return readChar(position);
    }

    public void setIdPtf(int idPtfIdx, int idPtf) {
        int position = Pos.vtgaIdPtf(idPtfIdx - 1);
        writeIntAsPacked(position, idPtf, Len.Int.ID_PTF);
    }

    /**Original name: VTGA-ID-PTF<br>*/
    public int getIdPtf(int idPtfIdx) {
        int position = Pos.vtgaIdPtf(idPtfIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_PTF);
    }

    public void setIdTrchDiGar(int idTrchDiGarIdx, int idTrchDiGar) {
        int position = Pos.vtgaIdTrchDiGar(idTrchDiGarIdx - 1);
        writeIntAsPacked(position, idTrchDiGar, Len.Int.ID_TRCH_DI_GAR);
    }

    /**Original name: VTGA-ID-TRCH-DI-GAR<br>*/
    public int getIdTrchDiGar(int idTrchDiGarIdx) {
        int position = Pos.vtgaIdTrchDiGar(idTrchDiGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_TRCH_DI_GAR);
    }

    public void setIdGar(int idGarIdx, int idGar) {
        int position = Pos.vtgaIdGar(idGarIdx - 1);
        writeIntAsPacked(position, idGar, Len.Int.ID_GAR);
    }

    /**Original name: VTGA-ID-GAR<br>*/
    public int getIdGar(int idGarIdx) {
        int position = Pos.vtgaIdGar(idGarIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_GAR);
    }

    public void setIdAdes(int idAdesIdx, int idAdes) {
        int position = Pos.vtgaIdAdes(idAdesIdx - 1);
        writeIntAsPacked(position, idAdes, Len.Int.ID_ADES);
    }

    /**Original name: VTGA-ID-ADES<br>*/
    public int getIdAdes(int idAdesIdx) {
        int position = Pos.vtgaIdAdes(idAdesIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_ADES);
    }

    public void setIdPoli(int idPoliIdx, int idPoli) {
        int position = Pos.vtgaIdPoli(idPoliIdx - 1);
        writeIntAsPacked(position, idPoli, Len.Int.ID_POLI);
    }

    /**Original name: VTGA-ID-POLI<br>*/
    public int getIdPoli(int idPoliIdx) {
        int position = Pos.vtgaIdPoli(idPoliIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_POLI);
    }

    public void setIdMoviCrz(int idMoviCrzIdx, int idMoviCrz) {
        int position = Pos.vtgaIdMoviCrz(idMoviCrzIdx - 1);
        writeIntAsPacked(position, idMoviCrz, Len.Int.ID_MOVI_CRZ);
    }

    /**Original name: VTGA-ID-MOVI-CRZ<br>*/
    public int getIdMoviCrz(int idMoviCrzIdx) {
        int position = Pos.vtgaIdMoviCrz(idMoviCrzIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CRZ);
    }

    public void setIdMoviChiu(int idMoviChiuIdx, int idMoviChiu) {
        int position = Pos.vtgaIdMoviChiu(idMoviChiuIdx - 1);
        writeIntAsPacked(position, idMoviChiu, Len.Int.ID_MOVI_CHIU);
    }

    /**Original name: VTGA-ID-MOVI-CHIU<br>*/
    public int getIdMoviChiu(int idMoviChiuIdx) {
        int position = Pos.vtgaIdMoviChiu(idMoviChiuIdx - 1);
        return readPackedAsInt(position, Len.Int.ID_MOVI_CHIU);
    }

    public void setDtIniEff(int dtIniEffIdx, int dtIniEff) {
        int position = Pos.vtgaDtIniEff(dtIniEffIdx - 1);
        writeIntAsPacked(position, dtIniEff, Len.Int.DT_INI_EFF);
    }

    /**Original name: VTGA-DT-INI-EFF<br>*/
    public int getDtIniEff(int dtIniEffIdx) {
        int position = Pos.vtgaDtIniEff(dtIniEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_EFF);
    }

    public void setDtEndEff(int dtEndEffIdx, int dtEndEff) {
        int position = Pos.vtgaDtEndEff(dtEndEffIdx - 1);
        writeIntAsPacked(position, dtEndEff, Len.Int.DT_END_EFF);
    }

    /**Original name: VTGA-DT-END-EFF<br>*/
    public int getDtEndEff(int dtEndEffIdx) {
        int position = Pos.vtgaDtEndEff(dtEndEffIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_END_EFF);
    }

    public void setCodCompAnia(int codCompAniaIdx, int codCompAnia) {
        int position = Pos.vtgaCodCompAnia(codCompAniaIdx - 1);
        writeIntAsPacked(position, codCompAnia, Len.Int.COD_COMP_ANIA);
    }

    /**Original name: VTGA-COD-COMP-ANIA<br>*/
    public int getCodCompAnia(int codCompAniaIdx) {
        int position = Pos.vtgaCodCompAnia(codCompAniaIdx - 1);
        return readPackedAsInt(position, Len.Int.COD_COMP_ANIA);
    }

    public void setDtDecor(int dtDecorIdx, int dtDecor) {
        int position = Pos.vtgaDtDecor(dtDecorIdx - 1);
        writeIntAsPacked(position, dtDecor, Len.Int.DT_DECOR);
    }

    /**Original name: VTGA-DT-DECOR<br>*/
    public int getDtDecor(int dtDecorIdx) {
        int position = Pos.vtgaDtDecor(dtDecorIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_DECOR);
    }

    public void setDtScad(int dtScadIdx, int dtScad) {
        int position = Pos.vtgaDtScad(dtScadIdx - 1);
        writeIntAsPacked(position, dtScad, Len.Int.DT_SCAD);
    }

    /**Original name: VTGA-DT-SCAD<br>*/
    public int getDtScad(int dtScadIdx) {
        int position = Pos.vtgaDtScad(dtScadIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_SCAD);
    }

    public void setIbOgg(int ibOggIdx, String ibOgg) {
        int position = Pos.vtgaIbOgg(ibOggIdx - 1);
        writeString(position, ibOgg, Len.IB_OGG);
    }

    /**Original name: VTGA-IB-OGG<br>*/
    public String getIbOgg(int ibOggIdx) {
        int position = Pos.vtgaIbOgg(ibOggIdx - 1);
        return readString(position, Len.IB_OGG);
    }

    public void setTpRgmFisc(int tpRgmFiscIdx, String tpRgmFisc) {
        int position = Pos.vtgaTpRgmFisc(tpRgmFiscIdx - 1);
        writeString(position, tpRgmFisc, Len.TP_RGM_FISC);
    }

    /**Original name: VTGA-TP-RGM-FISC<br>*/
    public String getTpRgmFisc(int tpRgmFiscIdx) {
        int position = Pos.vtgaTpRgmFisc(tpRgmFiscIdx - 1);
        return readString(position, Len.TP_RGM_FISC);
    }

    public void setDtEmis(int dtEmisIdx, int dtEmis) {
        int position = Pos.vtgaDtEmis(dtEmisIdx - 1);
        writeIntAsPacked(position, dtEmis, Len.Int.DT_EMIS);
    }

    /**Original name: VTGA-DT-EMIS<br>*/
    public int getDtEmis(int dtEmisIdx) {
        int position = Pos.vtgaDtEmis(dtEmisIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_EMIS);
    }

    public void setTpTrch(int tpTrchIdx, String tpTrch) {
        int position = Pos.vtgaTpTrch(tpTrchIdx - 1);
        writeString(position, tpTrch, Len.TP_TRCH);
    }

    /**Original name: VTGA-TP-TRCH<br>*/
    public String getTpTrch(int tpTrchIdx) {
        int position = Pos.vtgaTpTrch(tpTrchIdx - 1);
        return readString(position, Len.TP_TRCH);
    }

    public void setDurAa(int durAaIdx, int durAa) {
        int position = Pos.vtgaDurAa(durAaIdx - 1);
        writeIntAsPacked(position, durAa, Len.Int.DUR_AA);
    }

    /**Original name: VTGA-DUR-AA<br>*/
    public int getDurAa(int durAaIdx) {
        int position = Pos.vtgaDurAa(durAaIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_AA);
    }

    public void setDurMm(int durMmIdx, int durMm) {
        int position = Pos.vtgaDurMm(durMmIdx - 1);
        writeIntAsPacked(position, durMm, Len.Int.DUR_MM);
    }

    /**Original name: VTGA-DUR-MM<br>*/
    public int getDurMm(int durMmIdx) {
        int position = Pos.vtgaDurMm(durMmIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_MM);
    }

    public void setDurGg(int durGgIdx, int durGg) {
        int position = Pos.vtgaDurGg(durGgIdx - 1);
        writeIntAsPacked(position, durGg, Len.Int.DUR_GG);
    }

    /**Original name: VTGA-DUR-GG<br>*/
    public int getDurGg(int durGgIdx) {
        int position = Pos.vtgaDurGg(durGgIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_GG);
    }

    public void setPreCasoMor(int preCasoMorIdx, AfDecimal preCasoMor) {
        int position = Pos.vtgaPreCasoMor(preCasoMorIdx - 1);
        writeDecimalAsPacked(position, preCasoMor.copy());
    }

    /**Original name: VTGA-PRE-CASO-MOR<br>*/
    public AfDecimal getPreCasoMor(int preCasoMorIdx) {
        int position = Pos.vtgaPreCasoMor(preCasoMorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_CASO_MOR, Len.Fract.PRE_CASO_MOR);
    }

    public void setPcIntrRiat(int pcIntrRiatIdx, AfDecimal pcIntrRiat) {
        int position = Pos.vtgaPcIntrRiat(pcIntrRiatIdx - 1);
        writeDecimalAsPacked(position, pcIntrRiat.copy());
    }

    /**Original name: VTGA-PC-INTR-RIAT<br>*/
    public AfDecimal getPcIntrRiat(int pcIntrRiatIdx) {
        int position = Pos.vtgaPcIntrRiat(pcIntrRiatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_INTR_RIAT, Len.Fract.PC_INTR_RIAT);
    }

    public void setImpBnsAntic(int impBnsAnticIdx, AfDecimal impBnsAntic) {
        int position = Pos.vtgaImpBnsAntic(impBnsAnticIdx - 1);
        writeDecimalAsPacked(position, impBnsAntic.copy());
    }

    /**Original name: VTGA-IMP-BNS-ANTIC<br>*/
    public AfDecimal getImpBnsAntic(int impBnsAnticIdx) {
        int position = Pos.vtgaImpBnsAntic(impBnsAnticIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS_ANTIC, Len.Fract.IMP_BNS_ANTIC);
    }

    public void setPreIniNet(int preIniNetIdx, AfDecimal preIniNet) {
        int position = Pos.vtgaPreIniNet(preIniNetIdx - 1);
        writeDecimalAsPacked(position, preIniNet.copy());
    }

    /**Original name: VTGA-PRE-INI-NET<br>*/
    public AfDecimal getPreIniNet(int preIniNetIdx) {
        int position = Pos.vtgaPreIniNet(preIniNetIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INI_NET, Len.Fract.PRE_INI_NET);
    }

    public void setPrePpIni(int prePpIniIdx, AfDecimal prePpIni) {
        int position = Pos.vtgaPrePpIni(prePpIniIdx - 1);
        writeDecimalAsPacked(position, prePpIni.copy());
    }

    /**Original name: VTGA-PRE-PP-INI<br>*/
    public AfDecimal getPrePpIni(int prePpIniIdx) {
        int position = Pos.vtgaPrePpIni(prePpIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PP_INI, Len.Fract.PRE_PP_INI);
    }

    public void setPrePpUlt(int prePpUltIdx, AfDecimal prePpUlt) {
        int position = Pos.vtgaPrePpUlt(prePpUltIdx - 1);
        writeDecimalAsPacked(position, prePpUlt.copy());
    }

    /**Original name: VTGA-PRE-PP-ULT<br>*/
    public AfDecimal getPrePpUlt(int prePpUltIdx) {
        int position = Pos.vtgaPrePpUlt(prePpUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PP_ULT, Len.Fract.PRE_PP_ULT);
    }

    public void setPreTariIni(int preTariIniIdx, AfDecimal preTariIni) {
        int position = Pos.vtgaPreTariIni(preTariIniIdx - 1);
        writeDecimalAsPacked(position, preTariIni.copy());
    }

    /**Original name: VTGA-PRE-TARI-INI<br>*/
    public AfDecimal getPreTariIni(int preTariIniIdx) {
        int position = Pos.vtgaPreTariIni(preTariIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_TARI_INI, Len.Fract.PRE_TARI_INI);
    }

    public void setPreTariUlt(int preTariUltIdx, AfDecimal preTariUlt) {
        int position = Pos.vtgaPreTariUlt(preTariUltIdx - 1);
        writeDecimalAsPacked(position, preTariUlt.copy());
    }

    /**Original name: VTGA-PRE-TARI-ULT<br>*/
    public AfDecimal getPreTariUlt(int preTariUltIdx) {
        int position = Pos.vtgaPreTariUlt(preTariUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_TARI_ULT, Len.Fract.PRE_TARI_ULT);
    }

    public void setPreInvrioIni(int preInvrioIniIdx, AfDecimal preInvrioIni) {
        int position = Pos.vtgaPreInvrioIni(preInvrioIniIdx - 1);
        writeDecimalAsPacked(position, preInvrioIni.copy());
    }

    /**Original name: VTGA-PRE-INVRIO-INI<br>*/
    public AfDecimal getPreInvrioIni(int preInvrioIniIdx) {
        int position = Pos.vtgaPreInvrioIni(preInvrioIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INVRIO_INI, Len.Fract.PRE_INVRIO_INI);
    }

    public void setPreInvrioUlt(int preInvrioUltIdx, AfDecimal preInvrioUlt) {
        int position = Pos.vtgaPreInvrioUlt(preInvrioUltIdx - 1);
        writeDecimalAsPacked(position, preInvrioUlt.copy());
    }

    /**Original name: VTGA-PRE-INVRIO-ULT<br>*/
    public AfDecimal getPreInvrioUlt(int preInvrioUltIdx) {
        int position = Pos.vtgaPreInvrioUlt(preInvrioUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_INVRIO_ULT, Len.Fract.PRE_INVRIO_ULT);
    }

    public void setPreRivto(int preRivtoIdx, AfDecimal preRivto) {
        int position = Pos.vtgaPreRivto(preRivtoIdx - 1);
        writeDecimalAsPacked(position, preRivto.copy());
    }

    /**Original name: VTGA-PRE-RIVTO<br>*/
    public AfDecimal getPreRivto(int preRivtoIdx) {
        int position = Pos.vtgaPreRivto(preRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_RIVTO, Len.Fract.PRE_RIVTO);
    }

    public void setImpSoprProf(int impSoprProfIdx, AfDecimal impSoprProf) {
        int position = Pos.vtgaImpSoprProf(impSoprProfIdx - 1);
        writeDecimalAsPacked(position, impSoprProf.copy());
    }

    /**Original name: VTGA-IMP-SOPR-PROF<br>*/
    public AfDecimal getImpSoprProf(int impSoprProfIdx) {
        int position = Pos.vtgaImpSoprProf(impSoprProfIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_PROF, Len.Fract.IMP_SOPR_PROF);
    }

    public void setImpSoprSan(int impSoprSanIdx, AfDecimal impSoprSan) {
        int position = Pos.vtgaImpSoprSan(impSoprSanIdx - 1);
        writeDecimalAsPacked(position, impSoprSan.copy());
    }

    /**Original name: VTGA-IMP-SOPR-SAN<br>*/
    public AfDecimal getImpSoprSan(int impSoprSanIdx) {
        int position = Pos.vtgaImpSoprSan(impSoprSanIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_SAN, Len.Fract.IMP_SOPR_SAN);
    }

    public void setImpSoprSpo(int impSoprSpoIdx, AfDecimal impSoprSpo) {
        int position = Pos.vtgaImpSoprSpo(impSoprSpoIdx - 1);
        writeDecimalAsPacked(position, impSoprSpo.copy());
    }

    /**Original name: VTGA-IMP-SOPR-SPO<br>*/
    public AfDecimal getImpSoprSpo(int impSoprSpoIdx) {
        int position = Pos.vtgaImpSoprSpo(impSoprSpoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_SPO, Len.Fract.IMP_SOPR_SPO);
    }

    public void setImpSoprTec(int impSoprTecIdx, AfDecimal impSoprTec) {
        int position = Pos.vtgaImpSoprTec(impSoprTecIdx - 1);
        writeDecimalAsPacked(position, impSoprTec.copy());
    }

    /**Original name: VTGA-IMP-SOPR-TEC<br>*/
    public AfDecimal getImpSoprTec(int impSoprTecIdx) {
        int position = Pos.vtgaImpSoprTec(impSoprTecIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SOPR_TEC, Len.Fract.IMP_SOPR_TEC);
    }

    public void setImpAltSopr(int impAltSoprIdx, AfDecimal impAltSopr) {
        int position = Pos.vtgaImpAltSopr(impAltSoprIdx - 1);
        writeDecimalAsPacked(position, impAltSopr.copy());
    }

    /**Original name: VTGA-IMP-ALT-SOPR<br>*/
    public AfDecimal getImpAltSopr(int impAltSoprIdx) {
        int position = Pos.vtgaImpAltSopr(impAltSoprIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_ALT_SOPR, Len.Fract.IMP_ALT_SOPR);
    }

    public void setPreStab(int preStabIdx, AfDecimal preStab) {
        int position = Pos.vtgaPreStab(preStabIdx - 1);
        writeDecimalAsPacked(position, preStab.copy());
    }

    /**Original name: VTGA-PRE-STAB<br>*/
    public AfDecimal getPreStab(int preStabIdx) {
        int position = Pos.vtgaPreStab(preStabIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_STAB, Len.Fract.PRE_STAB);
    }

    public void setDtEffStab(int dtEffStabIdx, int dtEffStab) {
        int position = Pos.vtgaDtEffStab(dtEffStabIdx - 1);
        writeIntAsPacked(position, dtEffStab, Len.Int.DT_EFF_STAB);
    }

    /**Original name: VTGA-DT-EFF-STAB<br>*/
    public int getDtEffStab(int dtEffStabIdx) {
        int position = Pos.vtgaDtEffStab(dtEffStabIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_EFF_STAB);
    }

    public void setTsRivalFis(int tsRivalFisIdx, AfDecimal tsRivalFis) {
        int position = Pos.vtgaTsRivalFis(tsRivalFisIdx - 1);
        writeDecimalAsPacked(position, tsRivalFis.copy());
    }

    /**Original name: VTGA-TS-RIVAL-FIS<br>*/
    public AfDecimal getTsRivalFis(int tsRivalFisIdx) {
        int position = Pos.vtgaTsRivalFis(tsRivalFisIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_FIS, Len.Fract.TS_RIVAL_FIS);
    }

    public void setTsRivalIndiciz(int tsRivalIndicizIdx, AfDecimal tsRivalIndiciz) {
        int position = Pos.vtgaTsRivalIndiciz(tsRivalIndicizIdx - 1);
        writeDecimalAsPacked(position, tsRivalIndiciz.copy());
    }

    /**Original name: VTGA-TS-RIVAL-INDICIZ<br>*/
    public AfDecimal getTsRivalIndiciz(int tsRivalIndicizIdx) {
        int position = Pos.vtgaTsRivalIndiciz(tsRivalIndicizIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_INDICIZ, Len.Fract.TS_RIVAL_INDICIZ);
    }

    public void setOldTsTec(int oldTsTecIdx, AfDecimal oldTsTec) {
        int position = Pos.vtgaOldTsTec(oldTsTecIdx - 1);
        writeDecimalAsPacked(position, oldTsTec.copy());
    }

    /**Original name: VTGA-OLD-TS-TEC<br>*/
    public AfDecimal getOldTsTec(int oldTsTecIdx) {
        int position = Pos.vtgaOldTsTec(oldTsTecIdx - 1);
        return readPackedAsDecimal(position, Len.Int.OLD_TS_TEC, Len.Fract.OLD_TS_TEC);
    }

    public void setRatLrd(int ratLrdIdx, AfDecimal ratLrd) {
        int position = Pos.vtgaRatLrd(ratLrdIdx - 1);
        writeDecimalAsPacked(position, ratLrd.copy());
    }

    /**Original name: VTGA-RAT-LRD<br>*/
    public AfDecimal getRatLrd(int ratLrdIdx) {
        int position = Pos.vtgaRatLrd(ratLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RAT_LRD, Len.Fract.RAT_LRD);
    }

    public void setPreLrd(int preLrdIdx, AfDecimal preLrd) {
        int position = Pos.vtgaPreLrd(preLrdIdx - 1);
        writeDecimalAsPacked(position, preLrd.copy());
    }

    /**Original name: VTGA-PRE-LRD<br>*/
    public AfDecimal getPreLrd(int preLrdIdx) {
        int position = Pos.vtgaPreLrd(preLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_LRD, Len.Fract.PRE_LRD);
    }

    public void setPrstzIni(int prstzIniIdx, AfDecimal prstzIni) {
        int position = Pos.vtgaPrstzIni(prstzIniIdx - 1);
        writeDecimalAsPacked(position, prstzIni.copy());
    }

    /**Original name: VTGA-PRSTZ-INI<br>*/
    public AfDecimal getPrstzIni(int prstzIniIdx) {
        int position = Pos.vtgaPrstzIni(prstzIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI, Len.Fract.PRSTZ_INI);
    }

    public void setPrstzUlt(int prstzUltIdx, AfDecimal prstzUlt) {
        int position = Pos.vtgaPrstzUlt(prstzUltIdx - 1);
        writeDecimalAsPacked(position, prstzUlt.copy());
    }

    /**Original name: VTGA-PRSTZ-ULT<br>*/
    public AfDecimal getPrstzUlt(int prstzUltIdx) {
        int position = Pos.vtgaPrstzUlt(prstzUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_ULT, Len.Fract.PRSTZ_ULT);
    }

    public void setCptInOpzRivto(int cptInOpzRivtoIdx, AfDecimal cptInOpzRivto) {
        int position = Pos.vtgaCptInOpzRivto(cptInOpzRivtoIdx - 1);
        writeDecimalAsPacked(position, cptInOpzRivto.copy());
    }

    /**Original name: VTGA-CPT-IN-OPZ-RIVTO<br>*/
    public AfDecimal getCptInOpzRivto(int cptInOpzRivtoIdx) {
        int position = Pos.vtgaCptInOpzRivto(cptInOpzRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_IN_OPZ_RIVTO, Len.Fract.CPT_IN_OPZ_RIVTO);
    }

    public void setPrstzIniStab(int prstzIniStabIdx, AfDecimal prstzIniStab) {
        int position = Pos.vtgaPrstzIniStab(prstzIniStabIdx - 1);
        writeDecimalAsPacked(position, prstzIniStab.copy());
    }

    /**Original name: VTGA-PRSTZ-INI-STAB<br>*/
    public AfDecimal getPrstzIniStab(int prstzIniStabIdx) {
        int position = Pos.vtgaPrstzIniStab(prstzIniStabIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_STAB, Len.Fract.PRSTZ_INI_STAB);
    }

    public void setCptRshMor(int cptRshMorIdx, AfDecimal cptRshMor) {
        int position = Pos.vtgaCptRshMor(cptRshMorIdx - 1);
        writeDecimalAsPacked(position, cptRshMor.copy());
    }

    /**Original name: VTGA-CPT-RSH-MOR<br>*/
    public AfDecimal getCptRshMor(int cptRshMorIdx) {
        int position = Pos.vtgaCptRshMor(cptRshMorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_RSH_MOR, Len.Fract.CPT_RSH_MOR);
    }

    public void setPrstzRidIni(int prstzRidIniIdx, AfDecimal prstzRidIni) {
        int position = Pos.vtgaPrstzRidIni(prstzRidIniIdx - 1);
        writeDecimalAsPacked(position, prstzRidIni.copy());
    }

    /**Original name: VTGA-PRSTZ-RID-INI<br>*/
    public AfDecimal getPrstzRidIni(int prstzRidIniIdx) {
        int position = Pos.vtgaPrstzRidIni(prstzRidIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_RID_INI, Len.Fract.PRSTZ_RID_INI);
    }

    public void setFlCarCont(int flCarContIdx, char flCarCont) {
        int position = Pos.vtgaFlCarCont(flCarContIdx - 1);
        writeChar(position, flCarCont);
    }

    /**Original name: VTGA-FL-CAR-CONT<br>*/
    public char getFlCarCont(int flCarContIdx) {
        int position = Pos.vtgaFlCarCont(flCarContIdx - 1);
        return readChar(position);
    }

    public void setBnsGiaLiqto(int bnsGiaLiqtoIdx, AfDecimal bnsGiaLiqto) {
        int position = Pos.vtgaBnsGiaLiqto(bnsGiaLiqtoIdx - 1);
        writeDecimalAsPacked(position, bnsGiaLiqto.copy());
    }

    /**Original name: VTGA-BNS-GIA-LIQTO<br>*/
    public AfDecimal getBnsGiaLiqto(int bnsGiaLiqtoIdx) {
        int position = Pos.vtgaBnsGiaLiqto(bnsGiaLiqtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.BNS_GIA_LIQTO, Len.Fract.BNS_GIA_LIQTO);
    }

    public void setImpBns(int impBnsIdx, AfDecimal impBns) {
        int position = Pos.vtgaImpBns(impBnsIdx - 1);
        writeDecimalAsPacked(position, impBns.copy());
    }

    /**Original name: VTGA-IMP-BNS<br>*/
    public AfDecimal getImpBns(int impBnsIdx) {
        int position = Pos.vtgaImpBns(impBnsIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_BNS, Len.Fract.IMP_BNS);
    }

    public void setCodDvs(int codDvsIdx, String codDvs) {
        int position = Pos.vtgaCodDvs(codDvsIdx - 1);
        writeString(position, codDvs, Len.COD_DVS);
    }

    /**Original name: VTGA-COD-DVS<br>*/
    public String getCodDvs(int codDvsIdx) {
        int position = Pos.vtgaCodDvs(codDvsIdx - 1);
        return readString(position, Len.COD_DVS);
    }

    public void setPrstzIniNewfis(int prstzIniNewfisIdx, AfDecimal prstzIniNewfis) {
        int position = Pos.vtgaPrstzIniNewfis(prstzIniNewfisIdx - 1);
        writeDecimalAsPacked(position, prstzIniNewfis.copy());
    }

    /**Original name: VTGA-PRSTZ-INI-NEWFIS<br>*/
    public AfDecimal getPrstzIniNewfis(int prstzIniNewfisIdx) {
        int position = Pos.vtgaPrstzIniNewfis(prstzIniNewfisIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_NEWFIS, Len.Fract.PRSTZ_INI_NEWFIS);
    }

    public void setImpScon(int impSconIdx, AfDecimal impScon) {
        int position = Pos.vtgaImpScon(impSconIdx - 1);
        writeDecimalAsPacked(position, impScon.copy());
    }

    /**Original name: VTGA-IMP-SCON<br>*/
    public AfDecimal getImpScon(int impSconIdx) {
        int position = Pos.vtgaImpScon(impSconIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_SCON, Len.Fract.IMP_SCON);
    }

    public void setAlqScon(int alqSconIdx, AfDecimal alqScon) {
        int position = Pos.vtgaAlqScon(alqSconIdx - 1);
        writeDecimalAsPacked(position, alqScon.copy());
    }

    /**Original name: VTGA-ALQ-SCON<br>*/
    public AfDecimal getAlqScon(int alqSconIdx) {
        int position = Pos.vtgaAlqScon(alqSconIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_SCON, Len.Fract.ALQ_SCON);
    }

    public void setImpCarAcq(int impCarAcqIdx, AfDecimal impCarAcq) {
        int position = Pos.vtgaImpCarAcq(impCarAcqIdx - 1);
        writeDecimalAsPacked(position, impCarAcq.copy());
    }

    /**Original name: VTGA-IMP-CAR-ACQ<br>*/
    public AfDecimal getImpCarAcq(int impCarAcqIdx) {
        int position = Pos.vtgaImpCarAcq(impCarAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_ACQ, Len.Fract.IMP_CAR_ACQ);
    }

    public void setImpCarInc(int impCarIncIdx, AfDecimal impCarInc) {
        int position = Pos.vtgaImpCarInc(impCarIncIdx - 1);
        writeDecimalAsPacked(position, impCarInc.copy());
    }

    /**Original name: VTGA-IMP-CAR-INC<br>*/
    public AfDecimal getImpCarInc(int impCarIncIdx) {
        int position = Pos.vtgaImpCarInc(impCarIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_INC, Len.Fract.IMP_CAR_INC);
    }

    public void setImpCarGest(int impCarGestIdx, AfDecimal impCarGest) {
        int position = Pos.vtgaImpCarGest(impCarGestIdx - 1);
        writeDecimalAsPacked(position, impCarGest.copy());
    }

    /**Original name: VTGA-IMP-CAR-GEST<br>*/
    public AfDecimal getImpCarGest(int impCarGestIdx) {
        int position = Pos.vtgaImpCarGest(impCarGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_CAR_GEST, Len.Fract.IMP_CAR_GEST);
    }

    public void setEtaAa1oAssto(int etaAa1oAsstoIdx, short etaAa1oAssto) {
        int position = Pos.vtgaEtaAa1oAssto(etaAa1oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa1oAssto, Len.Int.ETA_AA1O_ASSTO);
    }

    /**Original name: VTGA-ETA-AA-1O-ASSTO<br>*/
    public short getEtaAa1oAssto(int etaAa1oAsstoIdx) {
        int position = Pos.vtgaEtaAa1oAssto(etaAa1oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA1O_ASSTO);
    }

    public void setEtaMm1oAssto(int etaMm1oAsstoIdx, short etaMm1oAssto) {
        int position = Pos.vtgaEtaMm1oAssto(etaMm1oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm1oAssto, Len.Int.ETA_MM1O_ASSTO);
    }

    /**Original name: VTGA-ETA-MM-1O-ASSTO<br>*/
    public short getEtaMm1oAssto(int etaMm1oAsstoIdx) {
        int position = Pos.vtgaEtaMm1oAssto(etaMm1oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM1O_ASSTO);
    }

    public void setEtaAa2oAssto(int etaAa2oAsstoIdx, short etaAa2oAssto) {
        int position = Pos.vtgaEtaAa2oAssto(etaAa2oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa2oAssto, Len.Int.ETA_AA2O_ASSTO);
    }

    /**Original name: VTGA-ETA-AA-2O-ASSTO<br>*/
    public short getEtaAa2oAssto(int etaAa2oAsstoIdx) {
        int position = Pos.vtgaEtaAa2oAssto(etaAa2oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA2O_ASSTO);
    }

    public void setEtaMm2oAssto(int etaMm2oAsstoIdx, short etaMm2oAssto) {
        int position = Pos.vtgaEtaMm2oAssto(etaMm2oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm2oAssto, Len.Int.ETA_MM2O_ASSTO);
    }

    /**Original name: VTGA-ETA-MM-2O-ASSTO<br>*/
    public short getEtaMm2oAssto(int etaMm2oAsstoIdx) {
        int position = Pos.vtgaEtaMm2oAssto(etaMm2oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM2O_ASSTO);
    }

    public void setEtaAa3oAssto(int etaAa3oAsstoIdx, short etaAa3oAssto) {
        int position = Pos.vtgaEtaAa3oAssto(etaAa3oAsstoIdx - 1);
        writeShortAsPacked(position, etaAa3oAssto, Len.Int.ETA_AA3O_ASSTO);
    }

    /**Original name: VTGA-ETA-AA-3O-ASSTO<br>*/
    public short getEtaAa3oAssto(int etaAa3oAsstoIdx) {
        int position = Pos.vtgaEtaAa3oAssto(etaAa3oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_AA3O_ASSTO);
    }

    public void setEtaMm3oAssto(int etaMm3oAsstoIdx, short etaMm3oAssto) {
        int position = Pos.vtgaEtaMm3oAssto(etaMm3oAsstoIdx - 1);
        writeShortAsPacked(position, etaMm3oAssto, Len.Int.ETA_MM3O_ASSTO);
    }

    /**Original name: VTGA-ETA-MM-3O-ASSTO<br>*/
    public short getEtaMm3oAssto(int etaMm3oAsstoIdx) {
        int position = Pos.vtgaEtaMm3oAssto(etaMm3oAsstoIdx - 1);
        return readPackedAsShort(position, Len.Int.ETA_MM3O_ASSTO);
    }

    public void setRendtoLrd(int rendtoLrdIdx, AfDecimal rendtoLrd) {
        int position = Pos.vtgaRendtoLrd(rendtoLrdIdx - 1);
        writeDecimalAsPacked(position, rendtoLrd.copy());
    }

    /**Original name: VTGA-RENDTO-LRD<br>*/
    public AfDecimal getRendtoLrd(int rendtoLrdIdx) {
        int position = Pos.vtgaRendtoLrd(rendtoLrdIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RENDTO_LRD, Len.Fract.RENDTO_LRD);
    }

    public void setPcRetr(int pcRetrIdx, AfDecimal pcRetr) {
        int position = Pos.vtgaPcRetr(pcRetrIdx - 1);
        writeDecimalAsPacked(position, pcRetr.copy());
    }

    /**Original name: VTGA-PC-RETR<br>*/
    public AfDecimal getPcRetr(int pcRetrIdx) {
        int position = Pos.vtgaPcRetr(pcRetrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RETR, Len.Fract.PC_RETR);
    }

    public void setRendtoRetr(int rendtoRetrIdx, AfDecimal rendtoRetr) {
        int position = Pos.vtgaRendtoRetr(rendtoRetrIdx - 1);
        writeDecimalAsPacked(position, rendtoRetr.copy());
    }

    /**Original name: VTGA-RENDTO-RETR<br>*/
    public AfDecimal getRendtoRetr(int rendtoRetrIdx) {
        int position = Pos.vtgaRendtoRetr(rendtoRetrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RENDTO_RETR, Len.Fract.RENDTO_RETR);
    }

    public void setMinGarto(int minGartoIdx, AfDecimal minGarto) {
        int position = Pos.vtgaMinGarto(minGartoIdx - 1);
        writeDecimalAsPacked(position, minGarto.copy());
    }

    /**Original name: VTGA-MIN-GARTO<br>*/
    public AfDecimal getMinGarto(int minGartoIdx) {
        int position = Pos.vtgaMinGarto(minGartoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MIN_GARTO, Len.Fract.MIN_GARTO);
    }

    public void setMinTrnut(int minTrnutIdx, AfDecimal minTrnut) {
        int position = Pos.vtgaMinTrnut(minTrnutIdx - 1);
        writeDecimalAsPacked(position, minTrnut.copy());
    }

    /**Original name: VTGA-MIN-TRNUT<br>*/
    public AfDecimal getMinTrnut(int minTrnutIdx) {
        int position = Pos.vtgaMinTrnut(minTrnutIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MIN_TRNUT, Len.Fract.MIN_TRNUT);
    }

    public void setPreAttDiTrch(int preAttDiTrchIdx, AfDecimal preAttDiTrch) {
        int position = Pos.vtgaPreAttDiTrch(preAttDiTrchIdx - 1);
        writeDecimalAsPacked(position, preAttDiTrch.copy());
    }

    /**Original name: VTGA-PRE-ATT-DI-TRCH<br>*/
    public AfDecimal getPreAttDiTrch(int preAttDiTrchIdx) {
        int position = Pos.vtgaPreAttDiTrch(preAttDiTrchIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_ATT_DI_TRCH, Len.Fract.PRE_ATT_DI_TRCH);
    }

    public void setMatuEnd2000(int matuEnd2000Idx, AfDecimal matuEnd2000) {
        int position = Pos.vtgaMatuEnd2000(matuEnd2000Idx - 1);
        writeDecimalAsPacked(position, matuEnd2000.copy());
    }

    /**Original name: VTGA-MATU-END2000<br>*/
    public AfDecimal getMatuEnd2000(int matuEnd2000Idx) {
        int position = Pos.vtgaMatuEnd2000(matuEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.MATU_END2000, Len.Fract.MATU_END2000);
    }

    public void setAbbTotIni(int abbTotIniIdx, AfDecimal abbTotIni) {
        int position = Pos.vtgaAbbTotIni(abbTotIniIdx - 1);
        writeDecimalAsPacked(position, abbTotIni.copy());
    }

    /**Original name: VTGA-ABB-TOT-INI<br>*/
    public AfDecimal getAbbTotIni(int abbTotIniIdx) {
        int position = Pos.vtgaAbbTotIni(abbTotIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_TOT_INI, Len.Fract.ABB_TOT_INI);
    }

    public void setAbbTotUlt(int abbTotUltIdx, AfDecimal abbTotUlt) {
        int position = Pos.vtgaAbbTotUlt(abbTotUltIdx - 1);
        writeDecimalAsPacked(position, abbTotUlt.copy());
    }

    /**Original name: VTGA-ABB-TOT-ULT<br>*/
    public AfDecimal getAbbTotUlt(int abbTotUltIdx) {
        int position = Pos.vtgaAbbTotUlt(abbTotUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_TOT_ULT, Len.Fract.ABB_TOT_ULT);
    }

    public void setAbbAnnuUlt(int abbAnnuUltIdx, AfDecimal abbAnnuUlt) {
        int position = Pos.vtgaAbbAnnuUlt(abbAnnuUltIdx - 1);
        writeDecimalAsPacked(position, abbAnnuUlt.copy());
    }

    /**Original name: VTGA-ABB-ANNU-ULT<br>*/
    public AfDecimal getAbbAnnuUlt(int abbAnnuUltIdx) {
        int position = Pos.vtgaAbbAnnuUlt(abbAnnuUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ABB_ANNU_ULT, Len.Fract.ABB_ANNU_ULT);
    }

    public void setDurAbb(int durAbbIdx, int durAbb) {
        int position = Pos.vtgaDurAbb(durAbbIdx - 1);
        writeIntAsPacked(position, durAbb, Len.Int.DUR_ABB);
    }

    /**Original name: VTGA-DUR-ABB<br>*/
    public int getDurAbb(int durAbbIdx) {
        int position = Pos.vtgaDurAbb(durAbbIdx - 1);
        return readPackedAsInt(position, Len.Int.DUR_ABB);
    }

    public void setTpAdegAbb(int tpAdegAbbIdx, char tpAdegAbb) {
        int position = Pos.vtgaTpAdegAbb(tpAdegAbbIdx - 1);
        writeChar(position, tpAdegAbb);
    }

    /**Original name: VTGA-TP-ADEG-ABB<br>*/
    public char getTpAdegAbb(int tpAdegAbbIdx) {
        int position = Pos.vtgaTpAdegAbb(tpAdegAbbIdx - 1);
        return readChar(position);
    }

    public void setModCalc(int modCalcIdx, String modCalc) {
        int position = Pos.vtgaModCalc(modCalcIdx - 1);
        writeString(position, modCalc, Len.MOD_CALC);
    }

    /**Original name: VTGA-MOD-CALC<br>*/
    public String getModCalc(int modCalcIdx) {
        int position = Pos.vtgaModCalc(modCalcIdx - 1);
        return readString(position, Len.MOD_CALC);
    }

    public void setImpAz(int impAzIdx, AfDecimal impAz) {
        int position = Pos.vtgaImpAz(impAzIdx - 1);
        writeDecimalAsPacked(position, impAz.copy());
    }

    /**Original name: VTGA-IMP-AZ<br>*/
    public AfDecimal getImpAz(int impAzIdx) {
        int position = Pos.vtgaImpAz(impAzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_AZ, Len.Fract.IMP_AZ);
    }

    public void setImpAder(int impAderIdx, AfDecimal impAder) {
        int position = Pos.vtgaImpAder(impAderIdx - 1);
        writeDecimalAsPacked(position, impAder.copy());
    }

    /**Original name: VTGA-IMP-ADER<br>*/
    public AfDecimal getImpAder(int impAderIdx) {
        int position = Pos.vtgaImpAder(impAderIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_ADER, Len.Fract.IMP_ADER);
    }

    public void setImpTfr(int impTfrIdx, AfDecimal impTfr) {
        int position = Pos.vtgaImpTfr(impTfrIdx - 1);
        writeDecimalAsPacked(position, impTfr.copy());
    }

    /**Original name: VTGA-IMP-TFR<br>*/
    public AfDecimal getImpTfr(int impTfrIdx) {
        int position = Pos.vtgaImpTfr(impTfrIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TFR, Len.Fract.IMP_TFR);
    }

    public void setImpVolo(int impVoloIdx, AfDecimal impVolo) {
        int position = Pos.vtgaImpVolo(impVoloIdx - 1);
        writeDecimalAsPacked(position, impVolo.copy());
    }

    /**Original name: VTGA-IMP-VOLO<br>*/
    public AfDecimal getImpVolo(int impVoloIdx) {
        int position = Pos.vtgaImpVolo(impVoloIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_VOLO, Len.Fract.IMP_VOLO);
    }

    public void setVisEnd2000(int visEnd2000Idx, AfDecimal visEnd2000) {
        int position = Pos.vtgaVisEnd2000(visEnd2000Idx - 1);
        writeDecimalAsPacked(position, visEnd2000.copy());
    }

    /**Original name: VTGA-VIS-END2000<br>*/
    public AfDecimal getVisEnd2000(int visEnd2000Idx) {
        int position = Pos.vtgaVisEnd2000(visEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.VIS_END2000, Len.Fract.VIS_END2000);
    }

    public void setDtVldtProd(int dtVldtProdIdx, int dtVldtProd) {
        int position = Pos.vtgaDtVldtProd(dtVldtProdIdx - 1);
        writeIntAsPacked(position, dtVldtProd, Len.Int.DT_VLDT_PROD);
    }

    /**Original name: VTGA-DT-VLDT-PROD<br>*/
    public int getDtVldtProd(int dtVldtProdIdx) {
        int position = Pos.vtgaDtVldtProd(dtVldtProdIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_VLDT_PROD);
    }

    public void setDtIniValTar(int dtIniValTarIdx, int dtIniValTar) {
        int position = Pos.vtgaDtIniValTar(dtIniValTarIdx - 1);
        writeIntAsPacked(position, dtIniValTar, Len.Int.DT_INI_VAL_TAR);
    }

    /**Original name: VTGA-DT-INI-VAL-TAR<br>*/
    public int getDtIniValTar(int dtIniValTarIdx) {
        int position = Pos.vtgaDtIniValTar(dtIniValTarIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_INI_VAL_TAR);
    }

    public void setImpbVisEnd2000(int impbVisEnd2000Idx, AfDecimal impbVisEnd2000) {
        int position = Pos.vtgaImpbVisEnd2000(impbVisEnd2000Idx - 1);
        writeDecimalAsPacked(position, impbVisEnd2000.copy());
    }

    /**Original name: VTGA-IMPB-VIS-END2000<br>*/
    public AfDecimal getImpbVisEnd2000(int impbVisEnd2000Idx) {
        int position = Pos.vtgaImpbVisEnd2000(impbVisEnd2000Idx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_VIS_END2000, Len.Fract.IMPB_VIS_END2000);
    }

    public void setRenIniTsTec0(int renIniTsTec0Idx, AfDecimal renIniTsTec0) {
        int position = Pos.vtgaRenIniTsTec0(renIniTsTec0Idx - 1);
        writeDecimalAsPacked(position, renIniTsTec0.copy());
    }

    /**Original name: VTGA-REN-INI-TS-TEC-0<br>*/
    public AfDecimal getRenIniTsTec0(int renIniTsTec0Idx) {
        int position = Pos.vtgaRenIniTsTec0(renIniTsTec0Idx - 1);
        return readPackedAsDecimal(position, Len.Int.REN_INI_TS_TEC0, Len.Fract.REN_INI_TS_TEC0);
    }

    public void setPcRipPre(int pcRipPreIdx, AfDecimal pcRipPre) {
        int position = Pos.vtgaPcRipPre(pcRipPreIdx - 1);
        writeDecimalAsPacked(position, pcRipPre.copy());
    }

    /**Original name: VTGA-PC-RIP-PRE<br>*/
    public AfDecimal getPcRipPre(int pcRipPreIdx) {
        int position = Pos.vtgaPcRipPre(pcRipPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_RIP_PRE, Len.Fract.PC_RIP_PRE);
    }

    public void setFlImportiForz(int flImportiForzIdx, char flImportiForz) {
        int position = Pos.vtgaFlImportiForz(flImportiForzIdx - 1);
        writeChar(position, flImportiForz);
    }

    /**Original name: VTGA-FL-IMPORTI-FORZ<br>*/
    public char getFlImportiForz(int flImportiForzIdx) {
        int position = Pos.vtgaFlImportiForz(flImportiForzIdx - 1);
        return readChar(position);
    }

    public void setPrstzIniNforz(int prstzIniNforzIdx, AfDecimal prstzIniNforz) {
        int position = Pos.vtgaPrstzIniNforz(prstzIniNforzIdx - 1);
        writeDecimalAsPacked(position, prstzIniNforz.copy());
    }

    /**Original name: VTGA-PRSTZ-INI-NFORZ<br>*/
    public AfDecimal getPrstzIniNforz(int prstzIniNforzIdx) {
        int position = Pos.vtgaPrstzIniNforz(prstzIniNforzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_INI_NFORZ, Len.Fract.PRSTZ_INI_NFORZ);
    }

    public void setVisEnd2000Nforz(int visEnd2000NforzIdx, AfDecimal visEnd2000Nforz) {
        int position = Pos.vtgaVisEnd2000Nforz(visEnd2000NforzIdx - 1);
        writeDecimalAsPacked(position, visEnd2000Nforz.copy());
    }

    /**Original name: VTGA-VIS-END2000-NFORZ<br>*/
    public AfDecimal getVisEnd2000Nforz(int visEnd2000NforzIdx) {
        int position = Pos.vtgaVisEnd2000Nforz(visEnd2000NforzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.VIS_END2000_NFORZ, Len.Fract.VIS_END2000_NFORZ);
    }

    public void setIntrMora(int intrMoraIdx, AfDecimal intrMora) {
        int position = Pos.vtgaIntrMora(intrMoraIdx - 1);
        writeDecimalAsPacked(position, intrMora.copy());
    }

    /**Original name: VTGA-INTR-MORA<br>*/
    public AfDecimal getIntrMora(int intrMoraIdx) {
        int position = Pos.vtgaIntrMora(intrMoraIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INTR_MORA, Len.Fract.INTR_MORA);
    }

    public void setManfeeAntic(int manfeeAnticIdx, AfDecimal manfeeAntic) {
        int position = Pos.vtgaManfeeAntic(manfeeAnticIdx - 1);
        writeDecimalAsPacked(position, manfeeAntic.copy());
    }

    /**Original name: VTGA-MANFEE-ANTIC<br>*/
    public AfDecimal getManfeeAntic(int manfeeAnticIdx) {
        int position = Pos.vtgaManfeeAntic(manfeeAnticIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MANFEE_ANTIC, Len.Fract.MANFEE_ANTIC);
    }

    public void setManfeeRicor(int manfeeRicorIdx, AfDecimal manfeeRicor) {
        int position = Pos.vtgaManfeeRicor(manfeeRicorIdx - 1);
        writeDecimalAsPacked(position, manfeeRicor.copy());
    }

    /**Original name: VTGA-MANFEE-RICOR<br>*/
    public AfDecimal getManfeeRicor(int manfeeRicorIdx) {
        int position = Pos.vtgaManfeeRicor(manfeeRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.MANFEE_RICOR, Len.Fract.MANFEE_RICOR);
    }

    public void setPreUniRivto(int preUniRivtoIdx, AfDecimal preUniRivto) {
        int position = Pos.vtgaPreUniRivto(preUniRivtoIdx - 1);
        writeDecimalAsPacked(position, preUniRivto.copy());
    }

    /**Original name: VTGA-PRE-UNI-RIVTO<br>*/
    public AfDecimal getPreUniRivto(int preUniRivtoIdx) {
        int position = Pos.vtgaPreUniRivto(preUniRivtoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_UNI_RIVTO, Len.Fract.PRE_UNI_RIVTO);
    }

    public void setProv1aaAcq(int prov1aaAcqIdx, AfDecimal prov1aaAcq) {
        int position = Pos.vtgaProv1aaAcq(prov1aaAcqIdx - 1);
        writeDecimalAsPacked(position, prov1aaAcq.copy());
    }

    /**Original name: VTGA-PROV-1AA-ACQ<br>*/
    public AfDecimal getProv1aaAcq(int prov1aaAcqIdx) {
        int position = Pos.vtgaProv1aaAcq(prov1aaAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV1AA_ACQ, Len.Fract.PROV1AA_ACQ);
    }

    public void setProv2aaAcq(int prov2aaAcqIdx, AfDecimal prov2aaAcq) {
        int position = Pos.vtgaProv2aaAcq(prov2aaAcqIdx - 1);
        writeDecimalAsPacked(position, prov2aaAcq.copy());
    }

    /**Original name: VTGA-PROV-2AA-ACQ<br>*/
    public AfDecimal getProv2aaAcq(int prov2aaAcqIdx) {
        int position = Pos.vtgaProv2aaAcq(prov2aaAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV2AA_ACQ, Len.Fract.PROV2AA_ACQ);
    }

    public void setProvRicor(int provRicorIdx, AfDecimal provRicor) {
        int position = Pos.vtgaProvRicor(provRicorIdx - 1);
        writeDecimalAsPacked(position, provRicor.copy());
    }

    /**Original name: VTGA-PROV-RICOR<br>*/
    public AfDecimal getProvRicor(int provRicorIdx) {
        int position = Pos.vtgaProvRicor(provRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV_RICOR, Len.Fract.PROV_RICOR);
    }

    public void setProvInc(int provIncIdx, AfDecimal provInc) {
        int position = Pos.vtgaProvInc(provIncIdx - 1);
        writeDecimalAsPacked(position, provInc.copy());
    }

    /**Original name: VTGA-PROV-INC<br>*/
    public AfDecimal getProvInc(int provIncIdx) {
        int position = Pos.vtgaProvInc(provIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PROV_INC, Len.Fract.PROV_INC);
    }

    public void setAlqProvAcq(int alqProvAcqIdx, AfDecimal alqProvAcq) {
        int position = Pos.vtgaAlqProvAcq(alqProvAcqIdx - 1);
        writeDecimalAsPacked(position, alqProvAcq.copy());
    }

    /**Original name: VTGA-ALQ-PROV-ACQ<br>*/
    public AfDecimal getAlqProvAcq(int alqProvAcqIdx) {
        int position = Pos.vtgaAlqProvAcq(alqProvAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_ACQ, Len.Fract.ALQ_PROV_ACQ);
    }

    public void setAlqProvInc(int alqProvIncIdx, AfDecimal alqProvInc) {
        int position = Pos.vtgaAlqProvInc(alqProvIncIdx - 1);
        writeDecimalAsPacked(position, alqProvInc.copy());
    }

    /**Original name: VTGA-ALQ-PROV-INC<br>*/
    public AfDecimal getAlqProvInc(int alqProvIncIdx) {
        int position = Pos.vtgaAlqProvInc(alqProvIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_INC, Len.Fract.ALQ_PROV_INC);
    }

    public void setAlqProvRicor(int alqProvRicorIdx, AfDecimal alqProvRicor) {
        int position = Pos.vtgaAlqProvRicor(alqProvRicorIdx - 1);
        writeDecimalAsPacked(position, alqProvRicor.copy());
    }

    /**Original name: VTGA-ALQ-PROV-RICOR<br>*/
    public AfDecimal getAlqProvRicor(int alqProvRicorIdx) {
        int position = Pos.vtgaAlqProvRicor(alqProvRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_PROV_RICOR, Len.Fract.ALQ_PROV_RICOR);
    }

    public void setImpbProvAcq(int impbProvAcqIdx, AfDecimal impbProvAcq) {
        int position = Pos.vtgaImpbProvAcq(impbProvAcqIdx - 1);
        writeDecimalAsPacked(position, impbProvAcq.copy());
    }

    /**Original name: VTGA-IMPB-PROV-ACQ<br>*/
    public AfDecimal getImpbProvAcq(int impbProvAcqIdx) {
        int position = Pos.vtgaImpbProvAcq(impbProvAcqIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_ACQ, Len.Fract.IMPB_PROV_ACQ);
    }

    public void setImpbProvInc(int impbProvIncIdx, AfDecimal impbProvInc) {
        int position = Pos.vtgaImpbProvInc(impbProvIncIdx - 1);
        writeDecimalAsPacked(position, impbProvInc.copy());
    }

    /**Original name: VTGA-IMPB-PROV-INC<br>*/
    public AfDecimal getImpbProvInc(int impbProvIncIdx) {
        int position = Pos.vtgaImpbProvInc(impbProvIncIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_INC, Len.Fract.IMPB_PROV_INC);
    }

    public void setImpbProvRicor(int impbProvRicorIdx, AfDecimal impbProvRicor) {
        int position = Pos.vtgaImpbProvRicor(impbProvRicorIdx - 1);
        writeDecimalAsPacked(position, impbProvRicor.copy());
    }

    /**Original name: VTGA-IMPB-PROV-RICOR<br>*/
    public AfDecimal getImpbProvRicor(int impbProvRicorIdx) {
        int position = Pos.vtgaImpbProvRicor(impbProvRicorIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_PROV_RICOR, Len.Fract.IMPB_PROV_RICOR);
    }

    public void setFlProvForz(int flProvForzIdx, char flProvForz) {
        int position = Pos.vtgaFlProvForz(flProvForzIdx - 1);
        writeChar(position, flProvForz);
    }

    /**Original name: VTGA-FL-PROV-FORZ<br>*/
    public char getFlProvForz(int flProvForzIdx) {
        int position = Pos.vtgaFlProvForz(flProvForzIdx - 1);
        return readChar(position);
    }

    public void setPrstzAggIni(int prstzAggIniIdx, AfDecimal prstzAggIni) {
        int position = Pos.vtgaPrstzAggIni(prstzAggIniIdx - 1);
        writeDecimalAsPacked(position, prstzAggIni.copy());
    }

    /**Original name: VTGA-PRSTZ-AGG-INI<br>*/
    public AfDecimal getPrstzAggIni(int prstzAggIniIdx) {
        int position = Pos.vtgaPrstzAggIni(prstzAggIniIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_AGG_INI, Len.Fract.PRSTZ_AGG_INI);
    }

    public void setIncrPre(int incrPreIdx, AfDecimal incrPre) {
        int position = Pos.vtgaIncrPre(incrPreIdx - 1);
        writeDecimalAsPacked(position, incrPre.copy());
    }

    /**Original name: VTGA-INCR-PRE<br>*/
    public AfDecimal getIncrPre(int incrPreIdx) {
        int position = Pos.vtgaIncrPre(incrPreIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INCR_PRE, Len.Fract.INCR_PRE);
    }

    public void setIncrPrstz(int incrPrstzIdx, AfDecimal incrPrstz) {
        int position = Pos.vtgaIncrPrstz(incrPrstzIdx - 1);
        writeDecimalAsPacked(position, incrPrstz.copy());
    }

    /**Original name: VTGA-INCR-PRSTZ<br>*/
    public AfDecimal getIncrPrstz(int incrPrstzIdx) {
        int position = Pos.vtgaIncrPrstz(incrPrstzIdx - 1);
        return readPackedAsDecimal(position, Len.Int.INCR_PRSTZ, Len.Fract.INCR_PRSTZ);
    }

    public void setDtUltAdegPrePr(int dtUltAdegPrePrIdx, int dtUltAdegPrePr) {
        int position = Pos.vtgaDtUltAdegPrePr(dtUltAdegPrePrIdx - 1);
        writeIntAsPacked(position, dtUltAdegPrePr, Len.Int.DT_ULT_ADEG_PRE_PR);
    }

    /**Original name: VTGA-DT-ULT-ADEG-PRE-PR<br>*/
    public int getDtUltAdegPrePr(int dtUltAdegPrePrIdx) {
        int position = Pos.vtgaDtUltAdegPrePr(dtUltAdegPrePrIdx - 1);
        return readPackedAsInt(position, Len.Int.DT_ULT_ADEG_PRE_PR);
    }

    public void setPrstzAggUlt(int prstzAggUltIdx, AfDecimal prstzAggUlt) {
        int position = Pos.vtgaPrstzAggUlt(prstzAggUltIdx - 1);
        writeDecimalAsPacked(position, prstzAggUlt.copy());
    }

    /**Original name: VTGA-PRSTZ-AGG-ULT<br>*/
    public AfDecimal getPrstzAggUlt(int prstzAggUltIdx) {
        int position = Pos.vtgaPrstzAggUlt(prstzAggUltIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRSTZ_AGG_ULT, Len.Fract.PRSTZ_AGG_ULT);
    }

    public void setTsRivalNet(int tsRivalNetIdx, AfDecimal tsRivalNet) {
        int position = Pos.vtgaTsRivalNet(tsRivalNetIdx - 1);
        writeDecimalAsPacked(position, tsRivalNet.copy());
    }

    /**Original name: VTGA-TS-RIVAL-NET<br>*/
    public AfDecimal getTsRivalNet(int tsRivalNetIdx) {
        int position = Pos.vtgaTsRivalNet(tsRivalNetIdx - 1);
        return readPackedAsDecimal(position, Len.Int.TS_RIVAL_NET, Len.Fract.TS_RIVAL_NET);
    }

    public void setPrePattuito(int prePattuitoIdx, AfDecimal prePattuito) {
        int position = Pos.vtgaPrePattuito(prePattuitoIdx - 1);
        writeDecimalAsPacked(position, prePattuito.copy());
    }

    /**Original name: VTGA-PRE-PATTUITO<br>*/
    public AfDecimal getPrePattuito(int prePattuitoIdx) {
        int position = Pos.vtgaPrePattuito(prePattuitoIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PRE_PATTUITO, Len.Fract.PRE_PATTUITO);
    }

    public void setTpRival(int tpRivalIdx, String tpRival) {
        int position = Pos.vtgaTpRival(tpRivalIdx - 1);
        writeString(position, tpRival, Len.TP_RIVAL);
    }

    /**Original name: VTGA-TP-RIVAL<br>*/
    public String getTpRival(int tpRivalIdx) {
        int position = Pos.vtgaTpRival(tpRivalIdx - 1);
        return readString(position, Len.TP_RIVAL);
    }

    public void setRisMat(int risMatIdx, AfDecimal risMat) {
        int position = Pos.vtgaRisMat(risMatIdx - 1);
        writeDecimalAsPacked(position, risMat.copy());
    }

    /**Original name: VTGA-RIS-MAT<br>*/
    public AfDecimal getRisMat(int risMatIdx) {
        int position = Pos.vtgaRisMat(risMatIdx - 1);
        return readPackedAsDecimal(position, Len.Int.RIS_MAT, Len.Fract.RIS_MAT);
    }

    public void setCptMinScad(int cptMinScadIdx, AfDecimal cptMinScad) {
        int position = Pos.vtgaCptMinScad(cptMinScadIdx - 1);
        writeDecimalAsPacked(position, cptMinScad.copy());
    }

    /**Original name: VTGA-CPT-MIN-SCAD<br>*/
    public AfDecimal getCptMinScad(int cptMinScadIdx) {
        int position = Pos.vtgaCptMinScad(cptMinScadIdx - 1);
        return readPackedAsDecimal(position, Len.Int.CPT_MIN_SCAD, Len.Fract.CPT_MIN_SCAD);
    }

    public void setCommisGest(int commisGestIdx, AfDecimal commisGest) {
        int position = Pos.vtgaCommisGest(commisGestIdx - 1);
        writeDecimalAsPacked(position, commisGest.copy());
    }

    /**Original name: VTGA-COMMIS-GEST<br>*/
    public AfDecimal getCommisGest(int commisGestIdx) {
        int position = Pos.vtgaCommisGest(commisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_GEST, Len.Fract.COMMIS_GEST);
    }

    public void setTpManfeeAppl(int tpManfeeApplIdx, String tpManfeeAppl) {
        int position = Pos.vtgaTpManfeeAppl(tpManfeeApplIdx - 1);
        writeString(position, tpManfeeAppl, Len.TP_MANFEE_APPL);
    }

    /**Original name: VTGA-TP-MANFEE-APPL<br>*/
    public String getTpManfeeAppl(int tpManfeeApplIdx) {
        int position = Pos.vtgaTpManfeeAppl(tpManfeeApplIdx - 1);
        return readString(position, Len.TP_MANFEE_APPL);
    }

    public void setDsRiga(int dsRigaIdx, long dsRiga) {
        int position = Pos.vtgaDsRiga(dsRigaIdx - 1);
        writeLongAsPacked(position, dsRiga, Len.Int.DS_RIGA);
    }

    /**Original name: VTGA-DS-RIGA<br>*/
    public long getDsRiga(int dsRigaIdx) {
        int position = Pos.vtgaDsRiga(dsRigaIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_RIGA);
    }

    public void setDsOperSql(int dsOperSqlIdx, char dsOperSql) {
        int position = Pos.vtgaDsOperSql(dsOperSqlIdx - 1);
        writeChar(position, dsOperSql);
    }

    /**Original name: VTGA-DS-OPER-SQL<br>*/
    public char getDsOperSql(int dsOperSqlIdx) {
        int position = Pos.vtgaDsOperSql(dsOperSqlIdx - 1);
        return readChar(position);
    }

    public void setDsVer(int dsVerIdx, int dsVer) {
        int position = Pos.vtgaDsVer(dsVerIdx - 1);
        writeIntAsPacked(position, dsVer, Len.Int.DS_VER);
    }

    /**Original name: VTGA-DS-VER<br>*/
    public int getDsVer(int dsVerIdx) {
        int position = Pos.vtgaDsVer(dsVerIdx - 1);
        return readPackedAsInt(position, Len.Int.DS_VER);
    }

    public void setDsTsIniCptz(int dsTsIniCptzIdx, long dsTsIniCptz) {
        int position = Pos.vtgaDsTsIniCptz(dsTsIniCptzIdx - 1);
        writeLongAsPacked(position, dsTsIniCptz, Len.Int.DS_TS_INI_CPTZ);
    }

    /**Original name: VTGA-DS-TS-INI-CPTZ<br>*/
    public long getDsTsIniCptz(int dsTsIniCptzIdx) {
        int position = Pos.vtgaDsTsIniCptz(dsTsIniCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_INI_CPTZ);
    }

    public void setDsTsEndCptz(int dsTsEndCptzIdx, long dsTsEndCptz) {
        int position = Pos.vtgaDsTsEndCptz(dsTsEndCptzIdx - 1);
        writeLongAsPacked(position, dsTsEndCptz, Len.Int.DS_TS_END_CPTZ);
    }

    /**Original name: VTGA-DS-TS-END-CPTZ<br>*/
    public long getDsTsEndCptz(int dsTsEndCptzIdx) {
        int position = Pos.vtgaDsTsEndCptz(dsTsEndCptzIdx - 1);
        return readPackedAsLong(position, Len.Int.DS_TS_END_CPTZ);
    }

    public void setDsUtente(int dsUtenteIdx, String dsUtente) {
        int position = Pos.vtgaDsUtente(dsUtenteIdx - 1);
        writeString(position, dsUtente, Len.DS_UTENTE);
    }

    /**Original name: VTGA-DS-UTENTE<br>*/
    public String getDsUtente(int dsUtenteIdx) {
        int position = Pos.vtgaDsUtente(dsUtenteIdx - 1);
        return readString(position, Len.DS_UTENTE);
    }

    public void setDsStatoElab(int dsStatoElabIdx, char dsStatoElab) {
        int position = Pos.vtgaDsStatoElab(dsStatoElabIdx - 1);
        writeChar(position, dsStatoElab);
    }

    /**Original name: VTGA-DS-STATO-ELAB<br>*/
    public char getDsStatoElab(int dsStatoElabIdx) {
        int position = Pos.vtgaDsStatoElab(dsStatoElabIdx - 1);
        return readChar(position);
    }

    public void setPcCommisGest(int pcCommisGestIdx, AfDecimal pcCommisGest) {
        int position = Pos.vtgaPcCommisGest(pcCommisGestIdx - 1);
        writeDecimalAsPacked(position, pcCommisGest.copy());
    }

    /**Original name: VTGA-PC-COMMIS-GEST<br>*/
    public AfDecimal getPcCommisGest(int pcCommisGestIdx) {
        int position = Pos.vtgaPcCommisGest(pcCommisGestIdx - 1);
        return readPackedAsDecimal(position, Len.Int.PC_COMMIS_GEST, Len.Fract.PC_COMMIS_GEST);
    }

    public void setNumGgRival(int numGgRivalIdx, int numGgRival) {
        int position = Pos.vtgaNumGgRival(numGgRivalIdx - 1);
        writeIntAsPacked(position, numGgRival, Len.Int.NUM_GG_RIVAL);
    }

    /**Original name: VTGA-NUM-GG-RIVAL<br>*/
    public int getNumGgRival(int numGgRivalIdx) {
        int position = Pos.vtgaNumGgRival(numGgRivalIdx - 1);
        return readPackedAsInt(position, Len.Int.NUM_GG_RIVAL);
    }

    public void setImpTrasfe(int impTrasfeIdx, AfDecimal impTrasfe) {
        int position = Pos.vtgaImpTrasfe(impTrasfeIdx - 1);
        writeDecimalAsPacked(position, impTrasfe.copy());
    }

    /**Original name: VTGA-IMP-TRASFE<br>*/
    public AfDecimal getImpTrasfe(int impTrasfeIdx) {
        int position = Pos.vtgaImpTrasfe(impTrasfeIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TRASFE, Len.Fract.IMP_TRASFE);
    }

    public void setImpTfrStrc(int impTfrStrcIdx, AfDecimal impTfrStrc) {
        int position = Pos.vtgaImpTfrStrc(impTfrStrcIdx - 1);
        writeDecimalAsPacked(position, impTfrStrc.copy());
    }

    /**Original name: VTGA-IMP-TFR-STRC<br>*/
    public AfDecimal getImpTfrStrc(int impTfrStrcIdx) {
        int position = Pos.vtgaImpTfrStrc(impTfrStrcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMP_TFR_STRC, Len.Fract.IMP_TFR_STRC);
    }

    public void setAcqExp(int acqExpIdx, AfDecimal acqExp) {
        int position = Pos.vtgaAcqExp(acqExpIdx - 1);
        writeDecimalAsPacked(position, acqExp.copy());
    }

    /**Original name: VTGA-ACQ-EXP<br>*/
    public AfDecimal getAcqExp(int acqExpIdx) {
        int position = Pos.vtgaAcqExp(acqExpIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ACQ_EXP, Len.Fract.ACQ_EXP);
    }

    public void setRemunAss(int remunAssIdx, AfDecimal remunAss) {
        int position = Pos.vtgaRemunAss(remunAssIdx - 1);
        writeDecimalAsPacked(position, remunAss.copy());
    }

    /**Original name: VTGA-REMUN-ASS<br>*/
    public AfDecimal getRemunAss(int remunAssIdx) {
        int position = Pos.vtgaRemunAss(remunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.REMUN_ASS, Len.Fract.REMUN_ASS);
    }

    public void setCommisInter(int commisInterIdx, AfDecimal commisInter) {
        int position = Pos.vtgaCommisInter(commisInterIdx - 1);
        writeDecimalAsPacked(position, commisInter.copy());
    }

    /**Original name: VTGA-COMMIS-INTER<br>*/
    public AfDecimal getCommisInter(int commisInterIdx) {
        int position = Pos.vtgaCommisInter(commisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COMMIS_INTER, Len.Fract.COMMIS_INTER);
    }

    public void setAlqRemunAss(int alqRemunAssIdx, AfDecimal alqRemunAss) {
        int position = Pos.vtgaAlqRemunAss(alqRemunAssIdx - 1);
        writeDecimalAsPacked(position, alqRemunAss.copy());
    }

    /**Original name: VTGA-ALQ-REMUN-ASS<br>*/
    public AfDecimal getAlqRemunAss(int alqRemunAssIdx) {
        int position = Pos.vtgaAlqRemunAss(alqRemunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_REMUN_ASS, Len.Fract.ALQ_REMUN_ASS);
    }

    public void setAlqCommisInter(int alqCommisInterIdx, AfDecimal alqCommisInter) {
        int position = Pos.vtgaAlqCommisInter(alqCommisInterIdx - 1);
        writeDecimalAsPacked(position, alqCommisInter.copy());
    }

    /**Original name: VTGA-ALQ-COMMIS-INTER<br>*/
    public AfDecimal getAlqCommisInter(int alqCommisInterIdx) {
        int position = Pos.vtgaAlqCommisInter(alqCommisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.ALQ_COMMIS_INTER, Len.Fract.ALQ_COMMIS_INTER);
    }

    public void setImpbRemunAss(int impbRemunAssIdx, AfDecimal impbRemunAss) {
        int position = Pos.vtgaImpbRemunAss(impbRemunAssIdx - 1);
        writeDecimalAsPacked(position, impbRemunAss.copy());
    }

    /**Original name: VTGA-IMPB-REMUN-ASS<br>*/
    public AfDecimal getImpbRemunAss(int impbRemunAssIdx) {
        int position = Pos.vtgaImpbRemunAss(impbRemunAssIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_REMUN_ASS, Len.Fract.IMPB_REMUN_ASS);
    }

    public void setImpbCommisInter(int impbCommisInterIdx, AfDecimal impbCommisInter) {
        int position = Pos.vtgaImpbCommisInter(impbCommisInterIdx - 1);
        writeDecimalAsPacked(position, impbCommisInter.copy());
    }

    /**Original name: VTGA-IMPB-COMMIS-INTER<br>*/
    public AfDecimal getImpbCommisInter(int impbCommisInterIdx) {
        int position = Pos.vtgaImpbCommisInter(impbCommisInterIdx - 1);
        return readPackedAsDecimal(position, Len.Int.IMPB_COMMIS_INTER, Len.Fract.IMPB_COMMIS_INTER);
    }

    public void setCosRunAssva(int cosRunAssvaIdx, AfDecimal cosRunAssva) {
        int position = Pos.vtgaCosRunAssva(cosRunAssvaIdx - 1);
        writeDecimalAsPacked(position, cosRunAssva.copy());
    }

    /**Original name: VTGA-COS-RUN-ASSVA<br>*/
    public AfDecimal getCosRunAssva(int cosRunAssvaIdx) {
        int position = Pos.vtgaCosRunAssva(cosRunAssvaIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA, Len.Fract.COS_RUN_ASSVA);
    }

    public void setCosRunAssvaIdc(int cosRunAssvaIdcIdx, AfDecimal cosRunAssvaIdc) {
        int position = Pos.vtgaCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        writeDecimalAsPacked(position, cosRunAssvaIdc.copy());
    }

    /**Original name: VTGA-COS-RUN-ASSVA-IDC<br>*/
    public AfDecimal getCosRunAssvaIdc(int cosRunAssvaIdcIdx) {
        int position = Pos.vtgaCosRunAssvaIdc(cosRunAssvaIdcIdx - 1);
        return readPackedAsDecimal(position, Len.Int.COS_RUN_ASSVA_IDC, Len.Fract.COS_RUN_ASSVA_IDC);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: VTGA-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int VTGA_TAB = 1;
        public static final int VTGA_TAB_R = 1;
        public static final int FLR1 = VTGA_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int vtgaTabTran(int idx) {
            return VTGA_TAB + idx * Len.TAB_TRAN;
        }

        public static int vtgaStatus(int idx) {
            return vtgaTabTran(idx);
        }

        public static int vtgaIdPtf(int idx) {
            return vtgaStatus(idx) + Len.STATUS;
        }

        public static int vtgaDati(int idx) {
            return vtgaIdPtf(idx) + Len.ID_PTF;
        }

        public static int vtgaIdTrchDiGar(int idx) {
            return vtgaDati(idx);
        }

        public static int vtgaIdGar(int idx) {
            return vtgaIdTrchDiGar(idx) + Len.ID_TRCH_DI_GAR;
        }

        public static int vtgaIdAdes(int idx) {
            return vtgaIdGar(idx) + Len.ID_GAR;
        }

        public static int vtgaIdPoli(int idx) {
            return vtgaIdAdes(idx) + Len.ID_ADES;
        }

        public static int vtgaIdMoviCrz(int idx) {
            return vtgaIdPoli(idx) + Len.ID_POLI;
        }

        public static int vtgaIdMoviChiu(int idx) {
            return vtgaIdMoviCrz(idx) + Len.ID_MOVI_CRZ;
        }

        public static int vtgaIdMoviChiuNull(int idx) {
            return vtgaIdMoviChiu(idx);
        }

        public static int vtgaDtIniEff(int idx) {
            return vtgaIdMoviChiu(idx) + Len.ID_MOVI_CHIU;
        }

        public static int vtgaDtEndEff(int idx) {
            return vtgaDtIniEff(idx) + Len.DT_INI_EFF;
        }

        public static int vtgaCodCompAnia(int idx) {
            return vtgaDtEndEff(idx) + Len.DT_END_EFF;
        }

        public static int vtgaDtDecor(int idx) {
            return vtgaCodCompAnia(idx) + Len.COD_COMP_ANIA;
        }

        public static int vtgaDtScad(int idx) {
            return vtgaDtDecor(idx) + Len.DT_DECOR;
        }

        public static int vtgaDtScadNull(int idx) {
            return vtgaDtScad(idx);
        }

        public static int vtgaIbOgg(int idx) {
            return vtgaDtScad(idx) + Len.DT_SCAD;
        }

        public static int vtgaIbOggNull(int idx) {
            return vtgaIbOgg(idx);
        }

        public static int vtgaTpRgmFisc(int idx) {
            return vtgaIbOgg(idx) + Len.IB_OGG;
        }

        public static int vtgaDtEmis(int idx) {
            return vtgaTpRgmFisc(idx) + Len.TP_RGM_FISC;
        }

        public static int vtgaDtEmisNull(int idx) {
            return vtgaDtEmis(idx);
        }

        public static int vtgaTpTrch(int idx) {
            return vtgaDtEmis(idx) + Len.DT_EMIS;
        }

        public static int vtgaDurAa(int idx) {
            return vtgaTpTrch(idx) + Len.TP_TRCH;
        }

        public static int vtgaDurAaNull(int idx) {
            return vtgaDurAa(idx);
        }

        public static int vtgaDurMm(int idx) {
            return vtgaDurAa(idx) + Len.DUR_AA;
        }

        public static int vtgaDurMmNull(int idx) {
            return vtgaDurMm(idx);
        }

        public static int vtgaDurGg(int idx) {
            return vtgaDurMm(idx) + Len.DUR_MM;
        }

        public static int vtgaDurGgNull(int idx) {
            return vtgaDurGg(idx);
        }

        public static int vtgaPreCasoMor(int idx) {
            return vtgaDurGg(idx) + Len.DUR_GG;
        }

        public static int vtgaPreCasoMorNull(int idx) {
            return vtgaPreCasoMor(idx);
        }

        public static int vtgaPcIntrRiat(int idx) {
            return vtgaPreCasoMor(idx) + Len.PRE_CASO_MOR;
        }

        public static int vtgaPcIntrRiatNull(int idx) {
            return vtgaPcIntrRiat(idx);
        }

        public static int vtgaImpBnsAntic(int idx) {
            return vtgaPcIntrRiat(idx) + Len.PC_INTR_RIAT;
        }

        public static int vtgaImpBnsAnticNull(int idx) {
            return vtgaImpBnsAntic(idx);
        }

        public static int vtgaPreIniNet(int idx) {
            return vtgaImpBnsAntic(idx) + Len.IMP_BNS_ANTIC;
        }

        public static int vtgaPreIniNetNull(int idx) {
            return vtgaPreIniNet(idx);
        }

        public static int vtgaPrePpIni(int idx) {
            return vtgaPreIniNet(idx) + Len.PRE_INI_NET;
        }

        public static int vtgaPrePpIniNull(int idx) {
            return vtgaPrePpIni(idx);
        }

        public static int vtgaPrePpUlt(int idx) {
            return vtgaPrePpIni(idx) + Len.PRE_PP_INI;
        }

        public static int vtgaPrePpUltNull(int idx) {
            return vtgaPrePpUlt(idx);
        }

        public static int vtgaPreTariIni(int idx) {
            return vtgaPrePpUlt(idx) + Len.PRE_PP_ULT;
        }

        public static int vtgaPreTariIniNull(int idx) {
            return vtgaPreTariIni(idx);
        }

        public static int vtgaPreTariUlt(int idx) {
            return vtgaPreTariIni(idx) + Len.PRE_TARI_INI;
        }

        public static int vtgaPreTariUltNull(int idx) {
            return vtgaPreTariUlt(idx);
        }

        public static int vtgaPreInvrioIni(int idx) {
            return vtgaPreTariUlt(idx) + Len.PRE_TARI_ULT;
        }

        public static int vtgaPreInvrioIniNull(int idx) {
            return vtgaPreInvrioIni(idx);
        }

        public static int vtgaPreInvrioUlt(int idx) {
            return vtgaPreInvrioIni(idx) + Len.PRE_INVRIO_INI;
        }

        public static int vtgaPreInvrioUltNull(int idx) {
            return vtgaPreInvrioUlt(idx);
        }

        public static int vtgaPreRivto(int idx) {
            return vtgaPreInvrioUlt(idx) + Len.PRE_INVRIO_ULT;
        }

        public static int vtgaPreRivtoNull(int idx) {
            return vtgaPreRivto(idx);
        }

        public static int vtgaImpSoprProf(int idx) {
            return vtgaPreRivto(idx) + Len.PRE_RIVTO;
        }

        public static int vtgaImpSoprProfNull(int idx) {
            return vtgaImpSoprProf(idx);
        }

        public static int vtgaImpSoprSan(int idx) {
            return vtgaImpSoprProf(idx) + Len.IMP_SOPR_PROF;
        }

        public static int vtgaImpSoprSanNull(int idx) {
            return vtgaImpSoprSan(idx);
        }

        public static int vtgaImpSoprSpo(int idx) {
            return vtgaImpSoprSan(idx) + Len.IMP_SOPR_SAN;
        }

        public static int vtgaImpSoprSpoNull(int idx) {
            return vtgaImpSoprSpo(idx);
        }

        public static int vtgaImpSoprTec(int idx) {
            return vtgaImpSoprSpo(idx) + Len.IMP_SOPR_SPO;
        }

        public static int vtgaImpSoprTecNull(int idx) {
            return vtgaImpSoprTec(idx);
        }

        public static int vtgaImpAltSopr(int idx) {
            return vtgaImpSoprTec(idx) + Len.IMP_SOPR_TEC;
        }

        public static int vtgaImpAltSoprNull(int idx) {
            return vtgaImpAltSopr(idx);
        }

        public static int vtgaPreStab(int idx) {
            return vtgaImpAltSopr(idx) + Len.IMP_ALT_SOPR;
        }

        public static int vtgaPreStabNull(int idx) {
            return vtgaPreStab(idx);
        }

        public static int vtgaDtEffStab(int idx) {
            return vtgaPreStab(idx) + Len.PRE_STAB;
        }

        public static int vtgaDtEffStabNull(int idx) {
            return vtgaDtEffStab(idx);
        }

        public static int vtgaTsRivalFis(int idx) {
            return vtgaDtEffStab(idx) + Len.DT_EFF_STAB;
        }

        public static int vtgaTsRivalFisNull(int idx) {
            return vtgaTsRivalFis(idx);
        }

        public static int vtgaTsRivalIndiciz(int idx) {
            return vtgaTsRivalFis(idx) + Len.TS_RIVAL_FIS;
        }

        public static int vtgaTsRivalIndicizNull(int idx) {
            return vtgaTsRivalIndiciz(idx);
        }

        public static int vtgaOldTsTec(int idx) {
            return vtgaTsRivalIndiciz(idx) + Len.TS_RIVAL_INDICIZ;
        }

        public static int vtgaOldTsTecNull(int idx) {
            return vtgaOldTsTec(idx);
        }

        public static int vtgaRatLrd(int idx) {
            return vtgaOldTsTec(idx) + Len.OLD_TS_TEC;
        }

        public static int vtgaRatLrdNull(int idx) {
            return vtgaRatLrd(idx);
        }

        public static int vtgaPreLrd(int idx) {
            return vtgaRatLrd(idx) + Len.RAT_LRD;
        }

        public static int vtgaPreLrdNull(int idx) {
            return vtgaPreLrd(idx);
        }

        public static int vtgaPrstzIni(int idx) {
            return vtgaPreLrd(idx) + Len.PRE_LRD;
        }

        public static int vtgaPrstzIniNull(int idx) {
            return vtgaPrstzIni(idx);
        }

        public static int vtgaPrstzUlt(int idx) {
            return vtgaPrstzIni(idx) + Len.PRSTZ_INI;
        }

        public static int vtgaPrstzUltNull(int idx) {
            return vtgaPrstzUlt(idx);
        }

        public static int vtgaCptInOpzRivto(int idx) {
            return vtgaPrstzUlt(idx) + Len.PRSTZ_ULT;
        }

        public static int vtgaCptInOpzRivtoNull(int idx) {
            return vtgaCptInOpzRivto(idx);
        }

        public static int vtgaPrstzIniStab(int idx) {
            return vtgaCptInOpzRivto(idx) + Len.CPT_IN_OPZ_RIVTO;
        }

        public static int vtgaPrstzIniStabNull(int idx) {
            return vtgaPrstzIniStab(idx);
        }

        public static int vtgaCptRshMor(int idx) {
            return vtgaPrstzIniStab(idx) + Len.PRSTZ_INI_STAB;
        }

        public static int vtgaCptRshMorNull(int idx) {
            return vtgaCptRshMor(idx);
        }

        public static int vtgaPrstzRidIni(int idx) {
            return vtgaCptRshMor(idx) + Len.CPT_RSH_MOR;
        }

        public static int vtgaPrstzRidIniNull(int idx) {
            return vtgaPrstzRidIni(idx);
        }

        public static int vtgaFlCarCont(int idx) {
            return vtgaPrstzRidIni(idx) + Len.PRSTZ_RID_INI;
        }

        public static int vtgaFlCarContNull(int idx) {
            return vtgaFlCarCont(idx);
        }

        public static int vtgaBnsGiaLiqto(int idx) {
            return vtgaFlCarCont(idx) + Len.FL_CAR_CONT;
        }

        public static int vtgaBnsGiaLiqtoNull(int idx) {
            return vtgaBnsGiaLiqto(idx);
        }

        public static int vtgaImpBns(int idx) {
            return vtgaBnsGiaLiqto(idx) + Len.BNS_GIA_LIQTO;
        }

        public static int vtgaImpBnsNull(int idx) {
            return vtgaImpBns(idx);
        }

        public static int vtgaCodDvs(int idx) {
            return vtgaImpBns(idx) + Len.IMP_BNS;
        }

        public static int vtgaPrstzIniNewfis(int idx) {
            return vtgaCodDvs(idx) + Len.COD_DVS;
        }

        public static int vtgaPrstzIniNewfisNull(int idx) {
            return vtgaPrstzIniNewfis(idx);
        }

        public static int vtgaImpScon(int idx) {
            return vtgaPrstzIniNewfis(idx) + Len.PRSTZ_INI_NEWFIS;
        }

        public static int vtgaImpSconNull(int idx) {
            return vtgaImpScon(idx);
        }

        public static int vtgaAlqScon(int idx) {
            return vtgaImpScon(idx) + Len.IMP_SCON;
        }

        public static int vtgaAlqSconNull(int idx) {
            return vtgaAlqScon(idx);
        }

        public static int vtgaImpCarAcq(int idx) {
            return vtgaAlqScon(idx) + Len.ALQ_SCON;
        }

        public static int vtgaImpCarAcqNull(int idx) {
            return vtgaImpCarAcq(idx);
        }

        public static int vtgaImpCarInc(int idx) {
            return vtgaImpCarAcq(idx) + Len.IMP_CAR_ACQ;
        }

        public static int vtgaImpCarIncNull(int idx) {
            return vtgaImpCarInc(idx);
        }

        public static int vtgaImpCarGest(int idx) {
            return vtgaImpCarInc(idx) + Len.IMP_CAR_INC;
        }

        public static int vtgaImpCarGestNull(int idx) {
            return vtgaImpCarGest(idx);
        }

        public static int vtgaEtaAa1oAssto(int idx) {
            return vtgaImpCarGest(idx) + Len.IMP_CAR_GEST;
        }

        public static int vtgaEtaAa1oAsstoNull(int idx) {
            return vtgaEtaAa1oAssto(idx);
        }

        public static int vtgaEtaMm1oAssto(int idx) {
            return vtgaEtaAa1oAssto(idx) + Len.ETA_AA1O_ASSTO;
        }

        public static int vtgaEtaMm1oAsstoNull(int idx) {
            return vtgaEtaMm1oAssto(idx);
        }

        public static int vtgaEtaAa2oAssto(int idx) {
            return vtgaEtaMm1oAssto(idx) + Len.ETA_MM1O_ASSTO;
        }

        public static int vtgaEtaAa2oAsstoNull(int idx) {
            return vtgaEtaAa2oAssto(idx);
        }

        public static int vtgaEtaMm2oAssto(int idx) {
            return vtgaEtaAa2oAssto(idx) + Len.ETA_AA2O_ASSTO;
        }

        public static int vtgaEtaMm2oAsstoNull(int idx) {
            return vtgaEtaMm2oAssto(idx);
        }

        public static int vtgaEtaAa3oAssto(int idx) {
            return vtgaEtaMm2oAssto(idx) + Len.ETA_MM2O_ASSTO;
        }

        public static int vtgaEtaAa3oAsstoNull(int idx) {
            return vtgaEtaAa3oAssto(idx);
        }

        public static int vtgaEtaMm3oAssto(int idx) {
            return vtgaEtaAa3oAssto(idx) + Len.ETA_AA3O_ASSTO;
        }

        public static int vtgaEtaMm3oAsstoNull(int idx) {
            return vtgaEtaMm3oAssto(idx);
        }

        public static int vtgaRendtoLrd(int idx) {
            return vtgaEtaMm3oAssto(idx) + Len.ETA_MM3O_ASSTO;
        }

        public static int vtgaRendtoLrdNull(int idx) {
            return vtgaRendtoLrd(idx);
        }

        public static int vtgaPcRetr(int idx) {
            return vtgaRendtoLrd(idx) + Len.RENDTO_LRD;
        }

        public static int vtgaPcRetrNull(int idx) {
            return vtgaPcRetr(idx);
        }

        public static int vtgaRendtoRetr(int idx) {
            return vtgaPcRetr(idx) + Len.PC_RETR;
        }

        public static int vtgaRendtoRetrNull(int idx) {
            return vtgaRendtoRetr(idx);
        }

        public static int vtgaMinGarto(int idx) {
            return vtgaRendtoRetr(idx) + Len.RENDTO_RETR;
        }

        public static int vtgaMinGartoNull(int idx) {
            return vtgaMinGarto(idx);
        }

        public static int vtgaMinTrnut(int idx) {
            return vtgaMinGarto(idx) + Len.MIN_GARTO;
        }

        public static int vtgaMinTrnutNull(int idx) {
            return vtgaMinTrnut(idx);
        }

        public static int vtgaPreAttDiTrch(int idx) {
            return vtgaMinTrnut(idx) + Len.MIN_TRNUT;
        }

        public static int vtgaPreAttDiTrchNull(int idx) {
            return vtgaPreAttDiTrch(idx);
        }

        public static int vtgaMatuEnd2000(int idx) {
            return vtgaPreAttDiTrch(idx) + Len.PRE_ATT_DI_TRCH;
        }

        public static int vtgaMatuEnd2000Null(int idx) {
            return vtgaMatuEnd2000(idx);
        }

        public static int vtgaAbbTotIni(int idx) {
            return vtgaMatuEnd2000(idx) + Len.MATU_END2000;
        }

        public static int vtgaAbbTotIniNull(int idx) {
            return vtgaAbbTotIni(idx);
        }

        public static int vtgaAbbTotUlt(int idx) {
            return vtgaAbbTotIni(idx) + Len.ABB_TOT_INI;
        }

        public static int vtgaAbbTotUltNull(int idx) {
            return vtgaAbbTotUlt(idx);
        }

        public static int vtgaAbbAnnuUlt(int idx) {
            return vtgaAbbTotUlt(idx) + Len.ABB_TOT_ULT;
        }

        public static int vtgaAbbAnnuUltNull(int idx) {
            return vtgaAbbAnnuUlt(idx);
        }

        public static int vtgaDurAbb(int idx) {
            return vtgaAbbAnnuUlt(idx) + Len.ABB_ANNU_ULT;
        }

        public static int vtgaDurAbbNull(int idx) {
            return vtgaDurAbb(idx);
        }

        public static int vtgaTpAdegAbb(int idx) {
            return vtgaDurAbb(idx) + Len.DUR_ABB;
        }

        public static int vtgaTpAdegAbbNull(int idx) {
            return vtgaTpAdegAbb(idx);
        }

        public static int vtgaModCalc(int idx) {
            return vtgaTpAdegAbb(idx) + Len.TP_ADEG_ABB;
        }

        public static int vtgaModCalcNull(int idx) {
            return vtgaModCalc(idx);
        }

        public static int vtgaImpAz(int idx) {
            return vtgaModCalc(idx) + Len.MOD_CALC;
        }

        public static int vtgaImpAzNull(int idx) {
            return vtgaImpAz(idx);
        }

        public static int vtgaImpAder(int idx) {
            return vtgaImpAz(idx) + Len.IMP_AZ;
        }

        public static int vtgaImpAderNull(int idx) {
            return vtgaImpAder(idx);
        }

        public static int vtgaImpTfr(int idx) {
            return vtgaImpAder(idx) + Len.IMP_ADER;
        }

        public static int vtgaImpTfrNull(int idx) {
            return vtgaImpTfr(idx);
        }

        public static int vtgaImpVolo(int idx) {
            return vtgaImpTfr(idx) + Len.IMP_TFR;
        }

        public static int vtgaImpVoloNull(int idx) {
            return vtgaImpVolo(idx);
        }

        public static int vtgaVisEnd2000(int idx) {
            return vtgaImpVolo(idx) + Len.IMP_VOLO;
        }

        public static int vtgaVisEnd2000Null(int idx) {
            return vtgaVisEnd2000(idx);
        }

        public static int vtgaDtVldtProd(int idx) {
            return vtgaVisEnd2000(idx) + Len.VIS_END2000;
        }

        public static int vtgaDtVldtProdNull(int idx) {
            return vtgaDtVldtProd(idx);
        }

        public static int vtgaDtIniValTar(int idx) {
            return vtgaDtVldtProd(idx) + Len.DT_VLDT_PROD;
        }

        public static int vtgaDtIniValTarNull(int idx) {
            return vtgaDtIniValTar(idx);
        }

        public static int vtgaImpbVisEnd2000(int idx) {
            return vtgaDtIniValTar(idx) + Len.DT_INI_VAL_TAR;
        }

        public static int vtgaImpbVisEnd2000Null(int idx) {
            return vtgaImpbVisEnd2000(idx);
        }

        public static int vtgaRenIniTsTec0(int idx) {
            return vtgaImpbVisEnd2000(idx) + Len.IMPB_VIS_END2000;
        }

        public static int vtgaRenIniTsTec0Null(int idx) {
            return vtgaRenIniTsTec0(idx);
        }

        public static int vtgaPcRipPre(int idx) {
            return vtgaRenIniTsTec0(idx) + Len.REN_INI_TS_TEC0;
        }

        public static int vtgaPcRipPreNull(int idx) {
            return vtgaPcRipPre(idx);
        }

        public static int vtgaFlImportiForz(int idx) {
            return vtgaPcRipPre(idx) + Len.PC_RIP_PRE;
        }

        public static int vtgaFlImportiForzNull(int idx) {
            return vtgaFlImportiForz(idx);
        }

        public static int vtgaPrstzIniNforz(int idx) {
            return vtgaFlImportiForz(idx) + Len.FL_IMPORTI_FORZ;
        }

        public static int vtgaPrstzIniNforzNull(int idx) {
            return vtgaPrstzIniNforz(idx);
        }

        public static int vtgaVisEnd2000Nforz(int idx) {
            return vtgaPrstzIniNforz(idx) + Len.PRSTZ_INI_NFORZ;
        }

        public static int vtgaVisEnd2000NforzNull(int idx) {
            return vtgaVisEnd2000Nforz(idx);
        }

        public static int vtgaIntrMora(int idx) {
            return vtgaVisEnd2000Nforz(idx) + Len.VIS_END2000_NFORZ;
        }

        public static int vtgaIntrMoraNull(int idx) {
            return vtgaIntrMora(idx);
        }

        public static int vtgaManfeeAntic(int idx) {
            return vtgaIntrMora(idx) + Len.INTR_MORA;
        }

        public static int vtgaManfeeAnticNull(int idx) {
            return vtgaManfeeAntic(idx);
        }

        public static int vtgaManfeeRicor(int idx) {
            return vtgaManfeeAntic(idx) + Len.MANFEE_ANTIC;
        }

        public static int vtgaManfeeRicorNull(int idx) {
            return vtgaManfeeRicor(idx);
        }

        public static int vtgaPreUniRivto(int idx) {
            return vtgaManfeeRicor(idx) + Len.MANFEE_RICOR;
        }

        public static int vtgaPreUniRivtoNull(int idx) {
            return vtgaPreUniRivto(idx);
        }

        public static int vtgaProv1aaAcq(int idx) {
            return vtgaPreUniRivto(idx) + Len.PRE_UNI_RIVTO;
        }

        public static int vtgaProv1aaAcqNull(int idx) {
            return vtgaProv1aaAcq(idx);
        }

        public static int vtgaProv2aaAcq(int idx) {
            return vtgaProv1aaAcq(idx) + Len.PROV1AA_ACQ;
        }

        public static int vtgaProv2aaAcqNull(int idx) {
            return vtgaProv2aaAcq(idx);
        }

        public static int vtgaProvRicor(int idx) {
            return vtgaProv2aaAcq(idx) + Len.PROV2AA_ACQ;
        }

        public static int vtgaProvRicorNull(int idx) {
            return vtgaProvRicor(idx);
        }

        public static int vtgaProvInc(int idx) {
            return vtgaProvRicor(idx) + Len.PROV_RICOR;
        }

        public static int vtgaProvIncNull(int idx) {
            return vtgaProvInc(idx);
        }

        public static int vtgaAlqProvAcq(int idx) {
            return vtgaProvInc(idx) + Len.PROV_INC;
        }

        public static int vtgaAlqProvAcqNull(int idx) {
            return vtgaAlqProvAcq(idx);
        }

        public static int vtgaAlqProvInc(int idx) {
            return vtgaAlqProvAcq(idx) + Len.ALQ_PROV_ACQ;
        }

        public static int vtgaAlqProvIncNull(int idx) {
            return vtgaAlqProvInc(idx);
        }

        public static int vtgaAlqProvRicor(int idx) {
            return vtgaAlqProvInc(idx) + Len.ALQ_PROV_INC;
        }

        public static int vtgaAlqProvRicorNull(int idx) {
            return vtgaAlqProvRicor(idx);
        }

        public static int vtgaImpbProvAcq(int idx) {
            return vtgaAlqProvRicor(idx) + Len.ALQ_PROV_RICOR;
        }

        public static int vtgaImpbProvAcqNull(int idx) {
            return vtgaImpbProvAcq(idx);
        }

        public static int vtgaImpbProvInc(int idx) {
            return vtgaImpbProvAcq(idx) + Len.IMPB_PROV_ACQ;
        }

        public static int vtgaImpbProvIncNull(int idx) {
            return vtgaImpbProvInc(idx);
        }

        public static int vtgaImpbProvRicor(int idx) {
            return vtgaImpbProvInc(idx) + Len.IMPB_PROV_INC;
        }

        public static int vtgaImpbProvRicorNull(int idx) {
            return vtgaImpbProvRicor(idx);
        }

        public static int vtgaFlProvForz(int idx) {
            return vtgaImpbProvRicor(idx) + Len.IMPB_PROV_RICOR;
        }

        public static int vtgaFlProvForzNull(int idx) {
            return vtgaFlProvForz(idx);
        }

        public static int vtgaPrstzAggIni(int idx) {
            return vtgaFlProvForz(idx) + Len.FL_PROV_FORZ;
        }

        public static int vtgaPrstzAggIniNull(int idx) {
            return vtgaPrstzAggIni(idx);
        }

        public static int vtgaIncrPre(int idx) {
            return vtgaPrstzAggIni(idx) + Len.PRSTZ_AGG_INI;
        }

        public static int vtgaIncrPreNull(int idx) {
            return vtgaIncrPre(idx);
        }

        public static int vtgaIncrPrstz(int idx) {
            return vtgaIncrPre(idx) + Len.INCR_PRE;
        }

        public static int vtgaIncrPrstzNull(int idx) {
            return vtgaIncrPrstz(idx);
        }

        public static int vtgaDtUltAdegPrePr(int idx) {
            return vtgaIncrPrstz(idx) + Len.INCR_PRSTZ;
        }

        public static int vtgaDtUltAdegPrePrNull(int idx) {
            return vtgaDtUltAdegPrePr(idx);
        }

        public static int vtgaPrstzAggUlt(int idx) {
            return vtgaDtUltAdegPrePr(idx) + Len.DT_ULT_ADEG_PRE_PR;
        }

        public static int vtgaPrstzAggUltNull(int idx) {
            return vtgaPrstzAggUlt(idx);
        }

        public static int vtgaTsRivalNet(int idx) {
            return vtgaPrstzAggUlt(idx) + Len.PRSTZ_AGG_ULT;
        }

        public static int vtgaTsRivalNetNull(int idx) {
            return vtgaTsRivalNet(idx);
        }

        public static int vtgaPrePattuito(int idx) {
            return vtgaTsRivalNet(idx) + Len.TS_RIVAL_NET;
        }

        public static int vtgaPrePattuitoNull(int idx) {
            return vtgaPrePattuito(idx);
        }

        public static int vtgaTpRival(int idx) {
            return vtgaPrePattuito(idx) + Len.PRE_PATTUITO;
        }

        public static int vtgaTpRivalNull(int idx) {
            return vtgaTpRival(idx);
        }

        public static int vtgaRisMat(int idx) {
            return vtgaTpRival(idx) + Len.TP_RIVAL;
        }

        public static int vtgaRisMatNull(int idx) {
            return vtgaRisMat(idx);
        }

        public static int vtgaCptMinScad(int idx) {
            return vtgaRisMat(idx) + Len.RIS_MAT;
        }

        public static int vtgaCptMinScadNull(int idx) {
            return vtgaCptMinScad(idx);
        }

        public static int vtgaCommisGest(int idx) {
            return vtgaCptMinScad(idx) + Len.CPT_MIN_SCAD;
        }

        public static int vtgaCommisGestNull(int idx) {
            return vtgaCommisGest(idx);
        }

        public static int vtgaTpManfeeAppl(int idx) {
            return vtgaCommisGest(idx) + Len.COMMIS_GEST;
        }

        public static int vtgaTpManfeeApplNull(int idx) {
            return vtgaTpManfeeAppl(idx);
        }

        public static int vtgaDsRiga(int idx) {
            return vtgaTpManfeeAppl(idx) + Len.TP_MANFEE_APPL;
        }

        public static int vtgaDsOperSql(int idx) {
            return vtgaDsRiga(idx) + Len.DS_RIGA;
        }

        public static int vtgaDsVer(int idx) {
            return vtgaDsOperSql(idx) + Len.DS_OPER_SQL;
        }

        public static int vtgaDsTsIniCptz(int idx) {
            return vtgaDsVer(idx) + Len.DS_VER;
        }

        public static int vtgaDsTsEndCptz(int idx) {
            return vtgaDsTsIniCptz(idx) + Len.DS_TS_INI_CPTZ;
        }

        public static int vtgaDsUtente(int idx) {
            return vtgaDsTsEndCptz(idx) + Len.DS_TS_END_CPTZ;
        }

        public static int vtgaDsStatoElab(int idx) {
            return vtgaDsUtente(idx) + Len.DS_UTENTE;
        }

        public static int vtgaPcCommisGest(int idx) {
            return vtgaDsStatoElab(idx) + Len.DS_STATO_ELAB;
        }

        public static int vtgaPcCommisGestNull(int idx) {
            return vtgaPcCommisGest(idx);
        }

        public static int vtgaNumGgRival(int idx) {
            return vtgaPcCommisGest(idx) + Len.PC_COMMIS_GEST;
        }

        public static int vtgaNumGgRivalNull(int idx) {
            return vtgaNumGgRival(idx);
        }

        public static int vtgaImpTrasfe(int idx) {
            return vtgaNumGgRival(idx) + Len.NUM_GG_RIVAL;
        }

        public static int vtgaImpTrasfeNull(int idx) {
            return vtgaImpTrasfe(idx);
        }

        public static int vtgaImpTfrStrc(int idx) {
            return vtgaImpTrasfe(idx) + Len.IMP_TRASFE;
        }

        public static int vtgaImpTfrStrcNull(int idx) {
            return vtgaImpTfrStrc(idx);
        }

        public static int vtgaAcqExp(int idx) {
            return vtgaImpTfrStrc(idx) + Len.IMP_TFR_STRC;
        }

        public static int vtgaAcqExpNull(int idx) {
            return vtgaAcqExp(idx);
        }

        public static int vtgaRemunAss(int idx) {
            return vtgaAcqExp(idx) + Len.ACQ_EXP;
        }

        public static int vtgaRemunAssNull(int idx) {
            return vtgaRemunAss(idx);
        }

        public static int vtgaCommisInter(int idx) {
            return vtgaRemunAss(idx) + Len.REMUN_ASS;
        }

        public static int vtgaCommisInterNull(int idx) {
            return vtgaCommisInter(idx);
        }

        public static int vtgaAlqRemunAss(int idx) {
            return vtgaCommisInter(idx) + Len.COMMIS_INTER;
        }

        public static int vtgaAlqRemunAssNull(int idx) {
            return vtgaAlqRemunAss(idx);
        }

        public static int vtgaAlqCommisInter(int idx) {
            return vtgaAlqRemunAss(idx) + Len.ALQ_REMUN_ASS;
        }

        public static int vtgaAlqCommisInterNull(int idx) {
            return vtgaAlqCommisInter(idx);
        }

        public static int vtgaImpbRemunAss(int idx) {
            return vtgaAlqCommisInter(idx) + Len.ALQ_COMMIS_INTER;
        }

        public static int vtgaImpbRemunAssNull(int idx) {
            return vtgaImpbRemunAss(idx);
        }

        public static int vtgaImpbCommisInter(int idx) {
            return vtgaImpbRemunAss(idx) + Len.IMPB_REMUN_ASS;
        }

        public static int vtgaImpbCommisInterNull(int idx) {
            return vtgaImpbCommisInter(idx);
        }

        public static int vtgaCosRunAssva(int idx) {
            return vtgaImpbCommisInter(idx) + Len.IMPB_COMMIS_INTER;
        }

        public static int vtgaCosRunAssvaNull(int idx) {
            return vtgaCosRunAssva(idx);
        }

        public static int vtgaCosRunAssvaIdc(int idx) {
            return vtgaCosRunAssva(idx) + Len.COS_RUN_ASSVA;
        }

        public static int vtgaCosRunAssvaIdcNull(int idx) {
            return vtgaCosRunAssvaIdc(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int STATUS = 1;
        public static final int ID_PTF = 5;
        public static final int ID_TRCH_DI_GAR = 5;
        public static final int ID_GAR = 5;
        public static final int ID_ADES = 5;
        public static final int ID_POLI = 5;
        public static final int ID_MOVI_CRZ = 5;
        public static final int ID_MOVI_CHIU = 5;
        public static final int DT_INI_EFF = 5;
        public static final int DT_END_EFF = 5;
        public static final int COD_COMP_ANIA = 3;
        public static final int DT_DECOR = 5;
        public static final int DT_SCAD = 5;
        public static final int IB_OGG = 40;
        public static final int TP_RGM_FISC = 2;
        public static final int DT_EMIS = 5;
        public static final int TP_TRCH = 2;
        public static final int DUR_AA = 3;
        public static final int DUR_MM = 3;
        public static final int DUR_GG = 3;
        public static final int PRE_CASO_MOR = 8;
        public static final int PC_INTR_RIAT = 4;
        public static final int IMP_BNS_ANTIC = 8;
        public static final int PRE_INI_NET = 8;
        public static final int PRE_PP_INI = 8;
        public static final int PRE_PP_ULT = 8;
        public static final int PRE_TARI_INI = 8;
        public static final int PRE_TARI_ULT = 8;
        public static final int PRE_INVRIO_INI = 8;
        public static final int PRE_INVRIO_ULT = 8;
        public static final int PRE_RIVTO = 8;
        public static final int IMP_SOPR_PROF = 8;
        public static final int IMP_SOPR_SAN = 8;
        public static final int IMP_SOPR_SPO = 8;
        public static final int IMP_SOPR_TEC = 8;
        public static final int IMP_ALT_SOPR = 8;
        public static final int PRE_STAB = 8;
        public static final int DT_EFF_STAB = 5;
        public static final int TS_RIVAL_FIS = 8;
        public static final int TS_RIVAL_INDICIZ = 8;
        public static final int OLD_TS_TEC = 8;
        public static final int RAT_LRD = 8;
        public static final int PRE_LRD = 8;
        public static final int PRSTZ_INI = 8;
        public static final int PRSTZ_ULT = 8;
        public static final int CPT_IN_OPZ_RIVTO = 8;
        public static final int PRSTZ_INI_STAB = 8;
        public static final int CPT_RSH_MOR = 8;
        public static final int PRSTZ_RID_INI = 8;
        public static final int FL_CAR_CONT = 1;
        public static final int BNS_GIA_LIQTO = 8;
        public static final int IMP_BNS = 8;
        public static final int COD_DVS = 20;
        public static final int PRSTZ_INI_NEWFIS = 8;
        public static final int IMP_SCON = 8;
        public static final int ALQ_SCON = 4;
        public static final int IMP_CAR_ACQ = 8;
        public static final int IMP_CAR_INC = 8;
        public static final int IMP_CAR_GEST = 8;
        public static final int ETA_AA1O_ASSTO = 2;
        public static final int ETA_MM1O_ASSTO = 2;
        public static final int ETA_AA2O_ASSTO = 2;
        public static final int ETA_MM2O_ASSTO = 2;
        public static final int ETA_AA3O_ASSTO = 2;
        public static final int ETA_MM3O_ASSTO = 2;
        public static final int RENDTO_LRD = 8;
        public static final int PC_RETR = 4;
        public static final int RENDTO_RETR = 8;
        public static final int MIN_GARTO = 8;
        public static final int MIN_TRNUT = 8;
        public static final int PRE_ATT_DI_TRCH = 8;
        public static final int MATU_END2000 = 8;
        public static final int ABB_TOT_INI = 8;
        public static final int ABB_TOT_ULT = 8;
        public static final int ABB_ANNU_ULT = 8;
        public static final int DUR_ABB = 4;
        public static final int TP_ADEG_ABB = 1;
        public static final int MOD_CALC = 2;
        public static final int IMP_AZ = 8;
        public static final int IMP_ADER = 8;
        public static final int IMP_TFR = 8;
        public static final int IMP_VOLO = 8;
        public static final int VIS_END2000 = 8;
        public static final int DT_VLDT_PROD = 5;
        public static final int DT_INI_VAL_TAR = 5;
        public static final int IMPB_VIS_END2000 = 8;
        public static final int REN_INI_TS_TEC0 = 8;
        public static final int PC_RIP_PRE = 4;
        public static final int FL_IMPORTI_FORZ = 1;
        public static final int PRSTZ_INI_NFORZ = 8;
        public static final int VIS_END2000_NFORZ = 8;
        public static final int INTR_MORA = 8;
        public static final int MANFEE_ANTIC = 8;
        public static final int MANFEE_RICOR = 8;
        public static final int PRE_UNI_RIVTO = 8;
        public static final int PROV1AA_ACQ = 8;
        public static final int PROV2AA_ACQ = 8;
        public static final int PROV_RICOR = 8;
        public static final int PROV_INC = 8;
        public static final int ALQ_PROV_ACQ = 4;
        public static final int ALQ_PROV_INC = 4;
        public static final int ALQ_PROV_RICOR = 4;
        public static final int IMPB_PROV_ACQ = 8;
        public static final int IMPB_PROV_INC = 8;
        public static final int IMPB_PROV_RICOR = 8;
        public static final int FL_PROV_FORZ = 1;
        public static final int PRSTZ_AGG_INI = 8;
        public static final int INCR_PRE = 8;
        public static final int INCR_PRSTZ = 8;
        public static final int DT_ULT_ADEG_PRE_PR = 5;
        public static final int PRSTZ_AGG_ULT = 8;
        public static final int TS_RIVAL_NET = 8;
        public static final int PRE_PATTUITO = 8;
        public static final int TP_RIVAL = 2;
        public static final int RIS_MAT = 8;
        public static final int CPT_MIN_SCAD = 8;
        public static final int COMMIS_GEST = 8;
        public static final int TP_MANFEE_APPL = 2;
        public static final int DS_RIGA = 6;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_INI_CPTZ = 10;
        public static final int DS_TS_END_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int PC_COMMIS_GEST = 4;
        public static final int NUM_GG_RIVAL = 3;
        public static final int IMP_TRASFE = 8;
        public static final int IMP_TFR_STRC = 8;
        public static final int ACQ_EXP = 8;
        public static final int REMUN_ASS = 8;
        public static final int COMMIS_INTER = 8;
        public static final int ALQ_REMUN_ASS = 4;
        public static final int ALQ_COMMIS_INTER = 4;
        public static final int IMPB_REMUN_ASS = 8;
        public static final int IMPB_COMMIS_INTER = 8;
        public static final int COS_RUN_ASSVA = 8;
        public static final int COS_RUN_ASSVA_IDC = 8;
        public static final int DATI = ID_TRCH_DI_GAR + ID_GAR + ID_ADES + ID_POLI + ID_MOVI_CRZ + ID_MOVI_CHIU + DT_INI_EFF + DT_END_EFF + COD_COMP_ANIA + DT_DECOR + DT_SCAD + IB_OGG + TP_RGM_FISC + DT_EMIS + TP_TRCH + DUR_AA + DUR_MM + DUR_GG + PRE_CASO_MOR + PC_INTR_RIAT + IMP_BNS_ANTIC + PRE_INI_NET + PRE_PP_INI + PRE_PP_ULT + PRE_TARI_INI + PRE_TARI_ULT + PRE_INVRIO_INI + PRE_INVRIO_ULT + PRE_RIVTO + IMP_SOPR_PROF + IMP_SOPR_SAN + IMP_SOPR_SPO + IMP_SOPR_TEC + IMP_ALT_SOPR + PRE_STAB + DT_EFF_STAB + TS_RIVAL_FIS + TS_RIVAL_INDICIZ + OLD_TS_TEC + RAT_LRD + PRE_LRD + PRSTZ_INI + PRSTZ_ULT + CPT_IN_OPZ_RIVTO + PRSTZ_INI_STAB + CPT_RSH_MOR + PRSTZ_RID_INI + FL_CAR_CONT + BNS_GIA_LIQTO + IMP_BNS + COD_DVS + PRSTZ_INI_NEWFIS + IMP_SCON + ALQ_SCON + IMP_CAR_ACQ + IMP_CAR_INC + IMP_CAR_GEST + ETA_AA1O_ASSTO + ETA_MM1O_ASSTO + ETA_AA2O_ASSTO + ETA_MM2O_ASSTO + ETA_AA3O_ASSTO + ETA_MM3O_ASSTO + RENDTO_LRD + PC_RETR + RENDTO_RETR + MIN_GARTO + MIN_TRNUT + PRE_ATT_DI_TRCH + MATU_END2000 + ABB_TOT_INI + ABB_TOT_ULT + ABB_ANNU_ULT + DUR_ABB + TP_ADEG_ABB + MOD_CALC + IMP_AZ + IMP_ADER + IMP_TFR + IMP_VOLO + VIS_END2000 + DT_VLDT_PROD + DT_INI_VAL_TAR + IMPB_VIS_END2000 + REN_INI_TS_TEC0 + PC_RIP_PRE + FL_IMPORTI_FORZ + PRSTZ_INI_NFORZ + VIS_END2000_NFORZ + INTR_MORA + MANFEE_ANTIC + MANFEE_RICOR + PRE_UNI_RIVTO + PROV1AA_ACQ + PROV2AA_ACQ + PROV_RICOR + PROV_INC + ALQ_PROV_ACQ + ALQ_PROV_INC + ALQ_PROV_RICOR + IMPB_PROV_ACQ + IMPB_PROV_INC + IMPB_PROV_RICOR + FL_PROV_FORZ + PRSTZ_AGG_INI + INCR_PRE + INCR_PRSTZ + DT_ULT_ADEG_PRE_PR + PRSTZ_AGG_ULT + TS_RIVAL_NET + PRE_PATTUITO + TP_RIVAL + RIS_MAT + CPT_MIN_SCAD + COMMIS_GEST + TP_MANFEE_APPL + DS_RIGA + DS_OPER_SQL + DS_VER + DS_TS_INI_CPTZ + DS_TS_END_CPTZ + DS_UTENTE + DS_STATO_ELAB + PC_COMMIS_GEST + NUM_GG_RIVAL + IMP_TRASFE + IMP_TFR_STRC + ACQ_EXP + REMUN_ASS + COMMIS_INTER + ALQ_REMUN_ASS + ALQ_COMMIS_INTER + IMPB_REMUN_ASS + IMPB_COMMIS_INTER + COS_RUN_ASSVA + COS_RUN_ASSVA_IDC;
        public static final int TAB_TRAN = STATUS + ID_PTF + DATI;
        public static final int FLR1 = 911;
        public static final int VTGA_TAB = VtgaTab.TAB_TRAN_MAXOCCURS * TAB_TRAN;
        public static final int RESTO_TAB = 272389;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_PTF = 9;
            public static final int ID_TRCH_DI_GAR = 9;
            public static final int ID_GAR = 9;
            public static final int ID_ADES = 9;
            public static final int ID_POLI = 9;
            public static final int ID_MOVI_CRZ = 9;
            public static final int ID_MOVI_CHIU = 9;
            public static final int DT_INI_EFF = 8;
            public static final int DT_END_EFF = 8;
            public static final int COD_COMP_ANIA = 5;
            public static final int DT_DECOR = 8;
            public static final int DT_SCAD = 8;
            public static final int DT_EMIS = 8;
            public static final int DUR_AA = 5;
            public static final int DUR_MM = 5;
            public static final int DUR_GG = 5;
            public static final int PRE_CASO_MOR = 12;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 12;
            public static final int PRE_INI_NET = 12;
            public static final int PRE_PP_INI = 12;
            public static final int PRE_PP_ULT = 12;
            public static final int PRE_TARI_INI = 12;
            public static final int PRE_TARI_ULT = 12;
            public static final int PRE_INVRIO_INI = 12;
            public static final int PRE_INVRIO_ULT = 12;
            public static final int PRE_RIVTO = 12;
            public static final int IMP_SOPR_PROF = 12;
            public static final int IMP_SOPR_SAN = 12;
            public static final int IMP_SOPR_SPO = 12;
            public static final int IMP_SOPR_TEC = 12;
            public static final int IMP_ALT_SOPR = 12;
            public static final int PRE_STAB = 12;
            public static final int DT_EFF_STAB = 8;
            public static final int TS_RIVAL_FIS = 5;
            public static final int TS_RIVAL_INDICIZ = 5;
            public static final int OLD_TS_TEC = 5;
            public static final int RAT_LRD = 12;
            public static final int PRE_LRD = 12;
            public static final int PRSTZ_INI = 12;
            public static final int PRSTZ_ULT = 12;
            public static final int CPT_IN_OPZ_RIVTO = 12;
            public static final int PRSTZ_INI_STAB = 12;
            public static final int CPT_RSH_MOR = 12;
            public static final int PRSTZ_RID_INI = 12;
            public static final int BNS_GIA_LIQTO = 12;
            public static final int IMP_BNS = 12;
            public static final int PRSTZ_INI_NEWFIS = 12;
            public static final int IMP_SCON = 12;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 12;
            public static final int IMP_CAR_INC = 12;
            public static final int IMP_CAR_GEST = 12;
            public static final int ETA_AA1O_ASSTO = 3;
            public static final int ETA_MM1O_ASSTO = 3;
            public static final int ETA_AA2O_ASSTO = 3;
            public static final int ETA_MM2O_ASSTO = 3;
            public static final int ETA_AA3O_ASSTO = 3;
            public static final int ETA_MM3O_ASSTO = 3;
            public static final int RENDTO_LRD = 5;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 5;
            public static final int MIN_GARTO = 5;
            public static final int MIN_TRNUT = 5;
            public static final int PRE_ATT_DI_TRCH = 12;
            public static final int MATU_END2000 = 12;
            public static final int ABB_TOT_INI = 12;
            public static final int ABB_TOT_ULT = 12;
            public static final int ABB_ANNU_ULT = 12;
            public static final int DUR_ABB = 6;
            public static final int IMP_AZ = 12;
            public static final int IMP_ADER = 12;
            public static final int IMP_TFR = 12;
            public static final int IMP_VOLO = 12;
            public static final int VIS_END2000 = 12;
            public static final int DT_VLDT_PROD = 8;
            public static final int DT_INI_VAL_TAR = 8;
            public static final int IMPB_VIS_END2000 = 12;
            public static final int REN_INI_TS_TEC0 = 12;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 12;
            public static final int VIS_END2000_NFORZ = 12;
            public static final int INTR_MORA = 12;
            public static final int MANFEE_ANTIC = 12;
            public static final int MANFEE_RICOR = 12;
            public static final int PRE_UNI_RIVTO = 12;
            public static final int PROV1AA_ACQ = 12;
            public static final int PROV2AA_ACQ = 12;
            public static final int PROV_RICOR = 12;
            public static final int PROV_INC = 12;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 12;
            public static final int IMPB_PROV_INC = 12;
            public static final int IMPB_PROV_RICOR = 12;
            public static final int PRSTZ_AGG_INI = 12;
            public static final int INCR_PRE = 12;
            public static final int INCR_PRSTZ = 12;
            public static final int DT_ULT_ADEG_PRE_PR = 8;
            public static final int PRSTZ_AGG_ULT = 12;
            public static final int TS_RIVAL_NET = 5;
            public static final int PRE_PATTUITO = 12;
            public static final int RIS_MAT = 12;
            public static final int CPT_MIN_SCAD = 12;
            public static final int COMMIS_GEST = 12;
            public static final int DS_RIGA = 10;
            public static final int DS_VER = 9;
            public static final int DS_TS_INI_CPTZ = 18;
            public static final int DS_TS_END_CPTZ = 18;
            public static final int PC_COMMIS_GEST = 3;
            public static final int NUM_GG_RIVAL = 5;
            public static final int IMP_TRASFE = 12;
            public static final int IMP_TFR_STRC = 12;
            public static final int ACQ_EXP = 12;
            public static final int REMUN_ASS = 12;
            public static final int COMMIS_INTER = 12;
            public static final int ALQ_REMUN_ASS = 3;
            public static final int ALQ_COMMIS_INTER = 3;
            public static final int IMPB_REMUN_ASS = 12;
            public static final int IMPB_COMMIS_INTER = 12;
            public static final int COS_RUN_ASSVA = 12;
            public static final int COS_RUN_ASSVA_IDC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_CASO_MOR = 3;
            public static final int PC_INTR_RIAT = 3;
            public static final int IMP_BNS_ANTIC = 3;
            public static final int PRE_INI_NET = 3;
            public static final int PRE_PP_INI = 3;
            public static final int PRE_PP_ULT = 3;
            public static final int PRE_TARI_INI = 3;
            public static final int PRE_TARI_ULT = 3;
            public static final int PRE_INVRIO_INI = 3;
            public static final int PRE_INVRIO_ULT = 3;
            public static final int PRE_RIVTO = 3;
            public static final int IMP_SOPR_PROF = 3;
            public static final int IMP_SOPR_SAN = 3;
            public static final int IMP_SOPR_SPO = 3;
            public static final int IMP_SOPR_TEC = 3;
            public static final int IMP_ALT_SOPR = 3;
            public static final int PRE_STAB = 3;
            public static final int TS_RIVAL_FIS = 9;
            public static final int TS_RIVAL_INDICIZ = 9;
            public static final int OLD_TS_TEC = 9;
            public static final int RAT_LRD = 3;
            public static final int PRE_LRD = 3;
            public static final int PRSTZ_INI = 3;
            public static final int PRSTZ_ULT = 3;
            public static final int CPT_IN_OPZ_RIVTO = 3;
            public static final int PRSTZ_INI_STAB = 3;
            public static final int CPT_RSH_MOR = 3;
            public static final int PRSTZ_RID_INI = 3;
            public static final int BNS_GIA_LIQTO = 3;
            public static final int IMP_BNS = 3;
            public static final int PRSTZ_INI_NEWFIS = 3;
            public static final int IMP_SCON = 3;
            public static final int ALQ_SCON = 3;
            public static final int IMP_CAR_ACQ = 3;
            public static final int IMP_CAR_INC = 3;
            public static final int IMP_CAR_GEST = 3;
            public static final int RENDTO_LRD = 9;
            public static final int PC_RETR = 3;
            public static final int RENDTO_RETR = 9;
            public static final int MIN_GARTO = 9;
            public static final int MIN_TRNUT = 9;
            public static final int PRE_ATT_DI_TRCH = 3;
            public static final int MATU_END2000 = 3;
            public static final int ABB_TOT_INI = 3;
            public static final int ABB_TOT_ULT = 3;
            public static final int ABB_ANNU_ULT = 3;
            public static final int IMP_AZ = 3;
            public static final int IMP_ADER = 3;
            public static final int IMP_TFR = 3;
            public static final int IMP_VOLO = 3;
            public static final int VIS_END2000 = 3;
            public static final int IMPB_VIS_END2000 = 3;
            public static final int REN_INI_TS_TEC0 = 3;
            public static final int PC_RIP_PRE = 3;
            public static final int PRSTZ_INI_NFORZ = 3;
            public static final int VIS_END2000_NFORZ = 3;
            public static final int INTR_MORA = 3;
            public static final int MANFEE_ANTIC = 3;
            public static final int MANFEE_RICOR = 3;
            public static final int PRE_UNI_RIVTO = 3;
            public static final int PROV1AA_ACQ = 3;
            public static final int PROV2AA_ACQ = 3;
            public static final int PROV_RICOR = 3;
            public static final int PROV_INC = 3;
            public static final int ALQ_PROV_ACQ = 3;
            public static final int ALQ_PROV_INC = 3;
            public static final int ALQ_PROV_RICOR = 3;
            public static final int IMPB_PROV_ACQ = 3;
            public static final int IMPB_PROV_INC = 3;
            public static final int IMPB_PROV_RICOR = 3;
            public static final int PRSTZ_AGG_INI = 3;
            public static final int INCR_PRE = 3;
            public static final int INCR_PRSTZ = 3;
            public static final int PRSTZ_AGG_ULT = 3;
            public static final int TS_RIVAL_NET = 9;
            public static final int PRE_PATTUITO = 3;
            public static final int RIS_MAT = 3;
            public static final int CPT_MIN_SCAD = 3;
            public static final int COMMIS_GEST = 3;
            public static final int PC_COMMIS_GEST = 3;
            public static final int IMP_TRASFE = 3;
            public static final int IMP_TFR_STRC = 3;
            public static final int ACQ_EXP = 3;
            public static final int REMUN_ASS = 3;
            public static final int COMMIS_INTER = 3;
            public static final int ALQ_REMUN_ASS = 3;
            public static final int ALQ_COMMIS_INTER = 3;
            public static final int IMPB_REMUN_ASS = 3;
            public static final int IMPB_COMMIS_INTER = 3;
            public static final int COS_RUN_ASSVA = 3;
            public static final int COS_RUN_ASSVA_IDC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
