package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-RAT<br>
 * Variable: TCL-IMP-RAT from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_RAT;
    }

    public void setTclImpRat(AfDecimal tclImpRat) {
        writeDecimalAsPacked(Pos.TCL_IMP_RAT, tclImpRat.copy());
    }

    public void setTclImpRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_RAT, Pos.TCL_IMP_RAT);
    }

    /**Original name: TCL-IMP-RAT<br>*/
    public AfDecimal getTclImpRat() {
        return readPackedAsDecimal(Pos.TCL_IMP_RAT, Len.Int.TCL_IMP_RAT, Len.Fract.TCL_IMP_RAT);
    }

    public byte[] getTclImpRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_RAT, Pos.TCL_IMP_RAT);
        return buffer;
    }

    public void setTclImpRatNull(String tclImpRatNull) {
        writeString(Pos.TCL_IMP_RAT_NULL, tclImpRatNull, Len.TCL_IMP_RAT_NULL);
    }

    /**Original name: TCL-IMP-RAT-NULL<br>*/
    public String getTclImpRatNull() {
        return readString(Pos.TCL_IMP_RAT_NULL, Len.TCL_IMP_RAT_NULL);
    }

    public String getTclImpRatNullFormatted() {
        return Functions.padBlanks(getTclImpRatNull(), Len.TCL_IMP_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RAT = 1;
        public static final int TCL_IMP_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_RAT = 8;
        public static final int TCL_IMP_RAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
