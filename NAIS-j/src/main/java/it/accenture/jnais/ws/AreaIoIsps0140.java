package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ispc0140DatiInput;
import it.accenture.jnais.ws.occurs.Ispc0140CalcoliP;
import it.accenture.jnais.ws.occurs.Ispc0140CalcoliT;
import it.accenture.jnais.ws.occurs.Ispc0211TabErrori;

/**Original name: AREA-IO-ISPS0140<br>
 * Variable: AREA-IO-ISPS0140 from program ISPS0140<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaIoIsps0140 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int CALCOLI_P_MAXOCCURS = 1;
    public static final int CALCOLI_T_MAXOCCURS = 20;
    public static final int TAB_ERRORI_MAXOCCURS = 10;
    //Original name: ISPC0140-DATI-INPUT
    private Ispc0140DatiInput datiInput = new Ispc0140DatiInput();
    //Original name: ISPC0140-DATI-NUM-MAX-ELE-P
    private String datiNumMaxEleP = DefaultValues.stringVal(Len.DATI_NUM_MAX_ELE_P);
    //Original name: ISPC0140-CALCOLI-P
    private Ispc0140CalcoliP[] calcoliP = new Ispc0140CalcoliP[CALCOLI_P_MAXOCCURS];
    /**Original name: ISPC0140-DATI-NUM-MAX-ELE-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private String datiNumMaxEleT = DefaultValues.stringVal(Len.DATI_NUM_MAX_ELE_T);
    //Original name: ISPC0140-CALCOLI-T
    private Ispc0140CalcoliT[] calcoliT = new Ispc0140CalcoliT[CALCOLI_T_MAXOCCURS];
    //Original name: ISPC0140-ESITO
    private String esito = DefaultValues.stringVal(Len.ESITO);
    //Original name: ISPC0140-ERR-NUM-ELE
    private String errNumEle = DefaultValues.stringVal(Len.ERR_NUM_ELE);
    //Original name: ISPC0140-TAB-ERRORI
    private Ispc0211TabErrori[] tabErrori = new Ispc0211TabErrori[TAB_ERRORI_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public AreaIoIsps0140() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_IO_ISPS0140;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaIoIsps0140Bytes(buf);
    }

    public void init() {
        for (int calcoliPIdx = 1; calcoliPIdx <= CALCOLI_P_MAXOCCURS; calcoliPIdx++) {
            calcoliP[calcoliPIdx - 1] = new Ispc0140CalcoliP();
        }
        for (int calcoliTIdx = 1; calcoliTIdx <= CALCOLI_T_MAXOCCURS; calcoliTIdx++) {
            calcoliT[calcoliTIdx - 1] = new Ispc0140CalcoliT();
        }
        for (int tabErroriIdx = 1; tabErroriIdx <= TAB_ERRORI_MAXOCCURS; tabErroriIdx++) {
            tabErrori[tabErroriIdx - 1] = new Ispc0211TabErrori();
        }
    }

    public String getAreaIoCalcContrFormatted() {
        return MarshalByteExt.bufferToStr(getAreaIoIsps0140Bytes());
    }

    public void setAreaIoIsps0140Bytes(byte[] buffer) {
        setAreaIoIsps0140Bytes(buffer, 1);
    }

    public byte[] getAreaIoIsps0140Bytes() {
        byte[] buffer = new byte[Len.AREA_IO_ISPS0140];
        return getAreaIoIsps0140Bytes(buffer, 1);
    }

    public void setAreaIoIsps0140Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.setDatiInputBytes(buffer, position);
        position += Ispc0140DatiInput.Len.DATI_INPUT;
        setDatiOutputBytes(buffer, position);
        position += Len.DATI_OUTPUT;
        setAreaErroriBytes(buffer, position);
    }

    public byte[] getAreaIoIsps0140Bytes(byte[] buffer, int offset) {
        int position = offset;
        datiInput.getDatiInputBytes(buffer, position);
        position += Ispc0140DatiInput.Len.DATI_INPUT;
        getDatiOutputBytes(buffer, position);
        position += Len.DATI_OUTPUT;
        getAreaErroriBytes(buffer, position);
        return buffer;
    }

    public void setDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        datiNumMaxEleP = MarshalByte.readFixedString(buffer, position, Len.DATI_NUM_MAX_ELE_P);
        position += Len.DATI_NUM_MAX_ELE_P;
        for (int idx = 1; idx <= CALCOLI_P_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                calcoliP[idx - 1].setCalcoliPBytes(buffer, position);
                position += Ispc0140CalcoliP.Len.CALCOLI_P;
            }
            else {
                calcoliP[idx - 1].initCalcoliPSpaces();
                position += Ispc0140CalcoliP.Len.CALCOLI_P;
            }
        }
        datiNumMaxEleT = MarshalByte.readFixedString(buffer, position, Len.DATI_NUM_MAX_ELE_T);
        position += Len.DATI_NUM_MAX_ELE_T;
        for (int idx = 1; idx <= CALCOLI_T_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                calcoliT[idx - 1].setCalcoliTBytes(buffer, position);
                position += Ispc0140CalcoliT.Len.CALCOLI_T;
            }
            else {
                calcoliT[idx - 1].initCalcoliTSpaces();
                position += Ispc0140CalcoliT.Len.CALCOLI_T;
            }
        }
    }

    public byte[] getDatiOutputBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, datiNumMaxEleP, Len.DATI_NUM_MAX_ELE_P);
        position += Len.DATI_NUM_MAX_ELE_P;
        for (int idx = 1; idx <= CALCOLI_P_MAXOCCURS; idx++) {
            calcoliP[idx - 1].getCalcoliPBytes(buffer, position);
            position += Ispc0140CalcoliP.Len.CALCOLI_P;
        }
        MarshalByte.writeString(buffer, position, datiNumMaxEleT, Len.DATI_NUM_MAX_ELE_T);
        position += Len.DATI_NUM_MAX_ELE_T;
        for (int idx = 1; idx <= CALCOLI_T_MAXOCCURS; idx++) {
            calcoliT[idx - 1].getCalcoliTBytes(buffer, position);
            position += Ispc0140CalcoliT.Len.CALCOLI_T;
        }
        return buffer;
    }

    public short getDatiNumMaxEleP() {
        return NumericDisplay.asShort(this.datiNumMaxEleP);
    }

    public short getDatiNumMaxEleT() {
        return NumericDisplay.asShort(this.datiNumMaxEleT);
    }

    /**Original name: ISPC0140-AREA-ERRORI<br>
	 * <pre>----------------------------------------------------------------*
	 *    AREA ERRORI
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getAreaErroriBytes() {
        byte[] buffer = new byte[Len.AREA_ERRORI];
        return getAreaErroriBytes(buffer, 1);
    }

    public void setAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        esito = MarshalByte.readFixedString(buffer, position, Len.ESITO);
        position += Len.ESITO;
        errNumEle = MarshalByte.readFixedString(buffer, position, Len.ERR_NUM_ELE);
        position += Len.ERR_NUM_ELE;
        for (int idx = 1; idx <= TAB_ERRORI_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabErrori[idx - 1].setIspc0211TabErroriBytes(buffer, position);
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
            else {
                tabErrori[idx - 1].initIspc0211TabErroriSpaces();
                position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
            }
        }
    }

    public byte[] getAreaErroriBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, esito, Len.ESITO);
        position += Len.ESITO;
        MarshalByte.writeString(buffer, position, errNumEle, Len.ERR_NUM_ELE);
        position += Len.ERR_NUM_ELE;
        for (int idx = 1; idx <= TAB_ERRORI_MAXOCCURS; idx++) {
            tabErrori[idx - 1].getIspc0211TabErroriBytes(buffer, position);
            position += Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        }
        return buffer;
    }

    public Ispc0140CalcoliP getCalcoliP(int idx) {
        return calcoliP[idx - 1];
    }

    public Ispc0140CalcoliT getCalcoliT(int idx) {
        return calcoliT[idx - 1];
    }

    public Ispc0140DatiInput getDatiInput() {
        return datiInput;
    }

    @Override
    public byte[] serialize() {
        return getAreaIoIsps0140Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DATI_NUM_MAX_ELE_P = 3;
        public static final int DATI_NUM_MAX_ELE_T = 3;
        public static final int DATI_OUTPUT = DATI_NUM_MAX_ELE_P + AreaIoIsps0140.CALCOLI_P_MAXOCCURS * Ispc0140CalcoliP.Len.CALCOLI_P + DATI_NUM_MAX_ELE_T + AreaIoIsps0140.CALCOLI_T_MAXOCCURS * Ispc0140CalcoliT.Len.CALCOLI_T;
        public static final int ESITO = 2;
        public static final int ERR_NUM_ELE = 3;
        public static final int AREA_ERRORI = ESITO + ERR_NUM_ELE + AreaIoIsps0140.TAB_ERRORI_MAXOCCURS * Ispc0211TabErrori.Len.ISPC0211_TAB_ERRORI;
        public static final int AREA_IO_ISPS0140 = Ispc0140DatiInput.Len.DATI_INPUT + DATI_OUTPUT + AREA_ERRORI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
