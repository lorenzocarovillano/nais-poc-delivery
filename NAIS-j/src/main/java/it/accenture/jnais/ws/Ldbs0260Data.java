package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.AreaLdbi0260;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAmmbFunzFunz;
import it.accenture.jnais.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS0260<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs0260Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-TIMESTAMP
    private WsTimestamp wsTimestamp = new WsTimestamp();
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-MATR-MOVIMENTO
    private IndAmmbFunzFunz indMatrMovimento = new IndAmmbFunzFunz();
    //Original name: AREA-LDBI0260
    private AreaLdbi0260 areaLdbi0260 = new AreaLdbi0260();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public AreaLdbi0260 getAreaLdbi0260() {
        return areaLdbi0260;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAmmbFunzFunz getIndMatrMovimento() {
        return indMatrMovimento;
    }

    public WsTimestamp getWsTimestamp() {
        return wsTimestamp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
