package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-SOPR-SAN<br>
 * Variable: L3421-IMP-SOPR-SAN from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpSoprSan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpSoprSan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_SOPR_SAN;
    }

    public void setL3421ImpSoprSanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_SOPR_SAN, Pos.L3421_IMP_SOPR_SAN);
    }

    /**Original name: L3421-IMP-SOPR-SAN<br>*/
    public AfDecimal getL3421ImpSoprSan() {
        return readPackedAsDecimal(Pos.L3421_IMP_SOPR_SAN, Len.Int.L3421_IMP_SOPR_SAN, Len.Fract.L3421_IMP_SOPR_SAN);
    }

    public byte[] getL3421ImpSoprSanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_SOPR_SAN, Pos.L3421_IMP_SOPR_SAN);
        return buffer;
    }

    /**Original name: L3421-IMP-SOPR-SAN-NULL<br>*/
    public String getL3421ImpSoprSanNull() {
        return readString(Pos.L3421_IMP_SOPR_SAN_NULL, Len.L3421_IMP_SOPR_SAN_NULL);
    }

    public String getL3421ImpSoprSanNullFormatted() {
        return Functions.padBlanks(getL3421ImpSoprSanNull(), Len.L3421_IMP_SOPR_SAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_SAN = 1;
        public static final int L3421_IMP_SOPR_SAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_SAN = 8;
        public static final int L3421_IMP_SOPR_SAN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_SAN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_SAN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
