package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-MONT-DAL2007<br>
 * Variable: S089-MONT-DAL2007 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089MontDal2007 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089MontDal2007() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_MONT_DAL2007;
    }

    public void setWlquMontDal2007(AfDecimal wlquMontDal2007) {
        writeDecimalAsPacked(Pos.S089_MONT_DAL2007, wlquMontDal2007.copy());
    }

    public void setWlquMontDal2007FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_MONT_DAL2007, Pos.S089_MONT_DAL2007);
    }

    /**Original name: WLQU-MONT-DAL2007<br>*/
    public AfDecimal getWlquMontDal2007() {
        return readPackedAsDecimal(Pos.S089_MONT_DAL2007, Len.Int.WLQU_MONT_DAL2007, Len.Fract.WLQU_MONT_DAL2007);
    }

    public byte[] getWlquMontDal2007AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_MONT_DAL2007, Pos.S089_MONT_DAL2007);
        return buffer;
    }

    public void initWlquMontDal2007Spaces() {
        fill(Pos.S089_MONT_DAL2007, Len.S089_MONT_DAL2007, Types.SPACE_CHAR);
    }

    public void setWlquMontDal2007Null(String wlquMontDal2007Null) {
        writeString(Pos.S089_MONT_DAL2007_NULL, wlquMontDal2007Null, Len.WLQU_MONT_DAL2007_NULL);
    }

    /**Original name: WLQU-MONT-DAL2007-NULL<br>*/
    public String getWlquMontDal2007Null() {
        return readString(Pos.S089_MONT_DAL2007_NULL, Len.WLQU_MONT_DAL2007_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_MONT_DAL2007 = 1;
        public static final int S089_MONT_DAL2007_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_MONT_DAL2007 = 8;
        public static final int WLQU_MONT_DAL2007_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_MONT_DAL2007 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_MONT_DAL2007 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
