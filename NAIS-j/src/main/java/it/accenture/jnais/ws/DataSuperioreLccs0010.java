package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DATA-SUPERIORE<br>
 * Variable: DATA-SUPERIORE from program LCCS0010<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DataSuperioreLccs0010 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: GG-SUP
    private String ggSup = DefaultValues.stringVal(Len.GG_SUP);
    //Original name: MM-SUP
    private String mmSup = DefaultValues.stringVal(Len.MM_SUP);
    //Original name: AAAA-SUP
    private String aaaaSup = DefaultValues.stringVal(Len.AAAA_SUP);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DATA_SUPERIORE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setDataSuperioreBytes(buf);
    }

    public void setDataSuperioreBytes(byte[] buffer) {
        setDataSuperioreBytes(buffer, 1);
    }

    public byte[] getDataSuperioreBytes() {
        byte[] buffer = new byte[Len.DATA_SUPERIORE];
        return getDataSuperioreBytes(buffer, 1);
    }

    public void setDataSuperioreBytes(byte[] buffer, int offset) {
        int position = offset;
        ggSup = MarshalByte.readFixedString(buffer, position, Len.GG_SUP);
        position += Len.GG_SUP;
        mmSup = MarshalByte.readFixedString(buffer, position, Len.MM_SUP);
        position += Len.MM_SUP;
        aaaaSup = MarshalByte.readFixedString(buffer, position, Len.AAAA_SUP);
    }

    public byte[] getDataSuperioreBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ggSup, Len.GG_SUP);
        position += Len.GG_SUP;
        MarshalByte.writeString(buffer, position, mmSup, Len.MM_SUP);
        position += Len.MM_SUP;
        MarshalByte.writeString(buffer, position, aaaaSup, Len.AAAA_SUP);
        return buffer;
    }

    public String getGgSupFormatted() {
        return this.ggSup;
    }

    public String getMmSupFormatted() {
        return this.mmSup;
    }

    public String getAaaaSupFormatted() {
        return this.aaaaSup;
    }

    @Override
    public byte[] serialize() {
        return getDataSuperioreBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GG_SUP = 2;
        public static final int MM_SUP = 2;
        public static final int AAAA_SUP = 4;
        public static final int DATA_SUPERIORE = GG_SUP + MM_SUP + AAAA_SUP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
