package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-RIT-ACC-CALC<br>
 * Variable: WDFL-RIT-ACC-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflRitAccCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflRitAccCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_RIT_ACC_CALC;
    }

    public void setWdflRitAccCalc(AfDecimal wdflRitAccCalc) {
        writeDecimalAsPacked(Pos.WDFL_RIT_ACC_CALC, wdflRitAccCalc.copy());
    }

    public void setWdflRitAccCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_RIT_ACC_CALC, Pos.WDFL_RIT_ACC_CALC);
    }

    /**Original name: WDFL-RIT-ACC-CALC<br>*/
    public AfDecimal getWdflRitAccCalc() {
        return readPackedAsDecimal(Pos.WDFL_RIT_ACC_CALC, Len.Int.WDFL_RIT_ACC_CALC, Len.Fract.WDFL_RIT_ACC_CALC);
    }

    public byte[] getWdflRitAccCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_RIT_ACC_CALC, Pos.WDFL_RIT_ACC_CALC);
        return buffer;
    }

    public void setWdflRitAccCalcNull(String wdflRitAccCalcNull) {
        writeString(Pos.WDFL_RIT_ACC_CALC_NULL, wdflRitAccCalcNull, Len.WDFL_RIT_ACC_CALC_NULL);
    }

    /**Original name: WDFL-RIT-ACC-CALC-NULL<br>*/
    public String getWdflRitAccCalcNull() {
        return readString(Pos.WDFL_RIT_ACC_CALC_NULL, Len.WDFL_RIT_ACC_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_ACC_CALC = 1;
        public static final int WDFL_RIT_ACC_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_RIT_ACC_CALC = 8;
        public static final int WDFL_RIT_ACC_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_ACC_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_RIT_ACC_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
