package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-SPE-PC<br>
 * Variable: PMO-SPE-PC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoSpePc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoSpePc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_SPE_PC;
    }

    public void setPmoSpePc(AfDecimal pmoSpePc) {
        writeDecimalAsPacked(Pos.PMO_SPE_PC, pmoSpePc.copy());
    }

    public void setPmoSpePcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_SPE_PC, Pos.PMO_SPE_PC);
    }

    /**Original name: PMO-SPE-PC<br>*/
    public AfDecimal getPmoSpePc() {
        return readPackedAsDecimal(Pos.PMO_SPE_PC, Len.Int.PMO_SPE_PC, Len.Fract.PMO_SPE_PC);
    }

    public byte[] getPmoSpePcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_SPE_PC, Pos.PMO_SPE_PC);
        return buffer;
    }

    public void initPmoSpePcHighValues() {
        fill(Pos.PMO_SPE_PC, Len.PMO_SPE_PC, Types.HIGH_CHAR_VAL);
    }

    public void setPmoSpePcNull(String pmoSpePcNull) {
        writeString(Pos.PMO_SPE_PC_NULL, pmoSpePcNull, Len.PMO_SPE_PC_NULL);
    }

    /**Original name: PMO-SPE-PC-NULL<br>*/
    public String getPmoSpePcNull() {
        return readString(Pos.PMO_SPE_PC_NULL, Len.PMO_SPE_PC_NULL);
    }

    public String getPmoSpePcNullFormatted() {
        return Functions.padBlanks(getPmoSpePcNull(), Len.PMO_SPE_PC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_SPE_PC = 1;
        public static final int PMO_SPE_PC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_SPE_PC = 4;
        public static final int PMO_SPE_PC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_SPE_PC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_SPE_PC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
