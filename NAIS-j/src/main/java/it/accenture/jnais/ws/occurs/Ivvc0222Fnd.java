package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: IVVC0222-FND<br>
 * Variables: IVVC0222-FND from copybook IVVC0222<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ivvc0222Fnd implements ICopyable<Ivvc0222Fnd> {

    //==== PROPERTIES ====
    //Original name: IVVC0222-COD-FND
    private String codFnd = DefaultValues.stringVal(Len.COD_FND);
    //Original name: IVVC0222-CTRV-FND
    private AfDecimal ctrvFnd = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== CONSTRUCTORS ====
    public Ivvc0222Fnd() {
    }

    public Ivvc0222Fnd(Ivvc0222Fnd fnd) {
        this();
        this.codFnd = fnd.codFnd;
        this.ctrvFnd.assign(fnd.ctrvFnd);
    }

    //==== METHODS ====
    public void setFndBytes(byte[] buffer, int offset) {
        int position = offset;
        codFnd = MarshalByte.readString(buffer, position, Len.COD_FND);
        position += Len.COD_FND;
        ctrvFnd.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.CTRV_FND, Len.Fract.CTRV_FND));
    }

    public byte[] getFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codFnd, Len.COD_FND);
        position += Len.COD_FND;
        MarshalByte.writeDecimalAsPacked(buffer, position, ctrvFnd.copy());
        return buffer;
    }

    public void initFndSpaces() {
        codFnd = "";
        ctrvFnd.setNaN();
    }

    public void setCodFnd(String codFnd) {
        this.codFnd = Functions.subString(codFnd, Len.COD_FND);
    }

    public String getCodFnd() {
        return this.codFnd;
    }

    public void setCtrvFnd(AfDecimal ctrvFnd) {
        this.ctrvFnd.assign(ctrvFnd);
    }

    public AfDecimal getCtrvFnd() {
        return this.ctrvFnd.copy();
    }

    public Ivvc0222Fnd copy() {
        return new Ivvc0222Fnd(this);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_FND = 20;
        public static final int CTRV_FND = 8;
        public static final int FND = COD_FND + CTRV_FND;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int CTRV_FND = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int CTRV_FND = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
