package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-DIR<br>
 * Variable: WTIT-TOT-DIR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotDir extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotDir() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_DIR;
    }

    public void setWtitTotDir(AfDecimal wtitTotDir) {
        writeDecimalAsPacked(Pos.WTIT_TOT_DIR, wtitTotDir.copy());
    }

    public void setWtitTotDirFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_DIR, Pos.WTIT_TOT_DIR);
    }

    /**Original name: WTIT-TOT-DIR<br>*/
    public AfDecimal getWtitTotDir() {
        return readPackedAsDecimal(Pos.WTIT_TOT_DIR, Len.Int.WTIT_TOT_DIR, Len.Fract.WTIT_TOT_DIR);
    }

    public byte[] getWtitTotDirAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_DIR, Pos.WTIT_TOT_DIR);
        return buffer;
    }

    public void initWtitTotDirSpaces() {
        fill(Pos.WTIT_TOT_DIR, Len.WTIT_TOT_DIR, Types.SPACE_CHAR);
    }

    public void setWtitTotDirNull(String wtitTotDirNull) {
        writeString(Pos.WTIT_TOT_DIR_NULL, wtitTotDirNull, Len.WTIT_TOT_DIR_NULL);
    }

    /**Original name: WTIT-TOT-DIR-NULL<br>*/
    public String getWtitTotDirNull() {
        return readString(Pos.WTIT_TOT_DIR_NULL, Len.WTIT_TOT_DIR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_DIR = 1;
        public static final int WTIT_TOT_DIR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_DIR = 8;
        public static final int WTIT_TOT_DIR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_DIR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_DIR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
