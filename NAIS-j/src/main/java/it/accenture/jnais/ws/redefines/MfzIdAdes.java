package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-ID-ADES<br>
 * Variable: MFZ-ID-ADES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzIdAdes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzIdAdes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_ID_ADES;
    }

    public void setMfzIdAdes(int mfzIdAdes) {
        writeIntAsPacked(Pos.MFZ_ID_ADES, mfzIdAdes, Len.Int.MFZ_ID_ADES);
    }

    public void setMfzIdAdesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_ID_ADES, Pos.MFZ_ID_ADES);
    }

    /**Original name: MFZ-ID-ADES<br>*/
    public int getMfzIdAdes() {
        return readPackedAsInt(Pos.MFZ_ID_ADES, Len.Int.MFZ_ID_ADES);
    }

    public byte[] getMfzIdAdesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_ID_ADES, Pos.MFZ_ID_ADES);
        return buffer;
    }

    public void setMfzIdAdesNull(String mfzIdAdesNull) {
        writeString(Pos.MFZ_ID_ADES_NULL, mfzIdAdesNull, Len.MFZ_ID_ADES_NULL);
    }

    /**Original name: MFZ-ID-ADES-NULL<br>*/
    public String getMfzIdAdesNull() {
        return readString(Pos.MFZ_ID_ADES_NULL, Len.MFZ_ID_ADES_NULL);
    }

    public String getMfzIdAdesNullFormatted() {
        return Functions.padBlanks(getMfzIdAdesNull(), Len.MFZ_ID_ADES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_ID_ADES = 1;
        public static final int MFZ_ID_ADES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_ID_ADES = 5;
        public static final int MFZ_ID_ADES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_ID_ADES = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
