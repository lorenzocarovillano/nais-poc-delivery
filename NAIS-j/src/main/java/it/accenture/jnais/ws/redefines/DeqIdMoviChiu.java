package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DEQ-ID-MOVI-CHIU<br>
 * Variable: DEQ-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DeqIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DeqIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DEQ_ID_MOVI_CHIU;
    }

    public void setDeqIdMoviChiu(int deqIdMoviChiu) {
        writeIntAsPacked(Pos.DEQ_ID_MOVI_CHIU, deqIdMoviChiu, Len.Int.DEQ_ID_MOVI_CHIU);
    }

    public void setDeqIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DEQ_ID_MOVI_CHIU, Pos.DEQ_ID_MOVI_CHIU);
    }

    /**Original name: DEQ-ID-MOVI-CHIU<br>*/
    public int getDeqIdMoviChiu() {
        return readPackedAsInt(Pos.DEQ_ID_MOVI_CHIU, Len.Int.DEQ_ID_MOVI_CHIU);
    }

    public byte[] getDeqIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DEQ_ID_MOVI_CHIU, Pos.DEQ_ID_MOVI_CHIU);
        return buffer;
    }

    public void setDeqIdMoviChiuNull(String deqIdMoviChiuNull) {
        writeString(Pos.DEQ_ID_MOVI_CHIU_NULL, deqIdMoviChiuNull, Len.DEQ_ID_MOVI_CHIU_NULL);
    }

    /**Original name: DEQ-ID-MOVI-CHIU-NULL<br>*/
    public String getDeqIdMoviChiuNull() {
        return readString(Pos.DEQ_ID_MOVI_CHIU_NULL, Len.DEQ_ID_MOVI_CHIU_NULL);
    }

    public String getDeqIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getDeqIdMoviChiuNull(), Len.DEQ_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DEQ_ID_MOVI_CHIU = 1;
        public static final int DEQ_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DEQ_ID_MOVI_CHIU = 5;
        public static final int DEQ_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DEQ_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
