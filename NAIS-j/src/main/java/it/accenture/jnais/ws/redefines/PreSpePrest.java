package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-SPE-PREST<br>
 * Variable: PRE-SPE-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreSpePrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreSpePrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_SPE_PREST;
    }

    public void setPreSpePrest(AfDecimal preSpePrest) {
        writeDecimalAsPacked(Pos.PRE_SPE_PREST, preSpePrest.copy());
    }

    public void setPreSpePrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_SPE_PREST, Pos.PRE_SPE_PREST);
    }

    /**Original name: PRE-SPE-PREST<br>*/
    public AfDecimal getPreSpePrest() {
        return readPackedAsDecimal(Pos.PRE_SPE_PREST, Len.Int.PRE_SPE_PREST, Len.Fract.PRE_SPE_PREST);
    }

    public byte[] getPreSpePrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_SPE_PREST, Pos.PRE_SPE_PREST);
        return buffer;
    }

    public void setPreSpePrestNull(String preSpePrestNull) {
        writeString(Pos.PRE_SPE_PREST_NULL, preSpePrestNull, Len.PRE_SPE_PREST_NULL);
    }

    /**Original name: PRE-SPE-PREST-NULL<br>*/
    public String getPreSpePrestNull() {
        return readString(Pos.PRE_SPE_PREST_NULL, Len.PRE_SPE_PREST_NULL);
    }

    public String getPreSpePrestNullFormatted() {
        return Functions.padBlanks(getPreSpePrestNull(), Len.PRE_SPE_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_SPE_PREST = 1;
        public static final int PRE_SPE_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_SPE_PREST = 8;
        public static final int PRE_SPE_PREST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_SPE_PREST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_SPE_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
