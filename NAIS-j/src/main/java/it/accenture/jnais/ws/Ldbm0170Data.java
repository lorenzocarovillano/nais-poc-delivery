package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.DettTitContDb;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndParamMovi;
import it.accenture.jnais.copy.Ispv0000;
import it.accenture.jnais.copy.Loac0560;
import it.accenture.jnais.ws.enums.FlagAccessoXRangeLdbm0170;
import it.accenture.jnais.ws.redefines.WsTimestamp;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBM0170<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbm0170Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: WS-TIMESTAMP
    private WsTimestamp wsTimestamp = new WsTimestamp();
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: WS-TP-MOVI
    private int wsTpMovi = DefaultValues.INT_VAL;
    //Original name: WS-GARANZIA
    private String wsGaranzia = DefaultValues.stringVal(Len.WS_GARANZIA);
    //Original name: WS-FORMA1
    private String wsForma1 = DefaultValues.stringVal(Len.WS_FORMA1);
    //Original name: WS-FORMA2
    private String wsForma2 = DefaultValues.stringVal(Len.WS_FORMA2);
    //Original name: WS-DATA-EFF
    private String wsDataEff = DefaultValues.stringVal(Len.WS_DATA_EFF);
    //Original name: WS-DATA-EFF-9
    private String wsDataEff9 = DefaultValues.stringVal(Len.WS_DATA_EFF9);
    //Original name: WS-DT-ELAB-DA-DB
    private String wsDtElabDaDb = DefaultValues.stringVal(Len.WS_DT_ELAB_DA_DB);
    //Original name: WS-DT-ELAB-A-DB
    private String wsDtElabADb = DefaultValues.stringVal(Len.WS_DT_ELAB_A_DB);
    //Original name: WS-COD-RAMO
    private String wsCodRamo = DefaultValues.stringVal(Len.WS_COD_RAMO);
    //Original name: WK-CC-ASSICURATIVO
    private String wkCcAssicurativo = "312";
    //Original name: WS-DATA-STRUTTURA
    private WsDataStruttura wsDataStruttura = new WsDataStruttura();
    //Original name: FLAG-ACCESSO-X-RANGE
    private FlagAccessoXRangeLdbm0170 flagAccessoXRange = new FlagAccessoXRangeLdbm0170();
    //Original name: BUFFER-WH-COD-RAMO
    private String bufferWhCodRamo = DefaultValues.stringVal(Len.BUFFER_WH_COD_RAMO);
    //Original name: LOAC0560
    private Loac0560 loac0560 = new Loac0560();
    //Original name: ISPV0000
    private Ispv0000 ispv0000 = new Ispv0000();
    //Original name: IND-PARAM-MOVI
    private IndParamMovi indParamMovi = new IndParamMovi();
    //Original name: PARAM-MOVI-DB
    private DettTitContDb paramMoviDb = new DettTitContDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setWsTpMovi(int wsTpMovi) {
        this.wsTpMovi = wsTpMovi;
    }

    public int getWsTpMovi() {
        return this.wsTpMovi;
    }

    public void setWsGaranzia(String wsGaranzia) {
        this.wsGaranzia = Functions.subString(wsGaranzia, Len.WS_GARANZIA);
    }

    public String getWsGaranzia() {
        return this.wsGaranzia;
    }

    public void setWsForma1(String wsForma1) {
        this.wsForma1 = Functions.subString(wsForma1, Len.WS_FORMA1);
    }

    public String getWsForma1() {
        return this.wsForma1;
    }

    public void setWsForma2(String wsForma2) {
        this.wsForma2 = Functions.subString(wsForma2, Len.WS_FORMA2);
    }

    public String getWsForma2() {
        return this.wsForma2;
    }

    public void setWsDataEff(String wsDataEff) {
        this.wsDataEff = Functions.subString(wsDataEff, Len.WS_DATA_EFF);
    }

    public String getWsDataEff() {
        return this.wsDataEff;
    }

    public void setWsDataEff9Formatted(String wsDataEff9) {
        this.wsDataEff9 = Trunc.toUnsignedNumeric(wsDataEff9, Len.WS_DATA_EFF9);
    }

    public int getWsDataEff9() {
        return NumericDisplay.asInt(this.wsDataEff9);
    }

    public String getWsDataEff9Formatted() {
        return this.wsDataEff9;
    }

    public void setWsDtElabDaDb(String wsDtElabDaDb) {
        this.wsDtElabDaDb = Functions.subString(wsDtElabDaDb, Len.WS_DT_ELAB_DA_DB);
    }

    public String getWsDtElabDaDb() {
        return this.wsDtElabDaDb;
    }

    public void setWsDtElabADb(String wsDtElabADb) {
        this.wsDtElabADb = Functions.subString(wsDtElabADb, Len.WS_DT_ELAB_A_DB);
    }

    public String getWsDtElabADb() {
        return this.wsDtElabADb;
    }

    public void setWsCodRamo(String wsCodRamo) {
        this.wsCodRamo = Functions.subString(wsCodRamo, Len.WS_COD_RAMO);
    }

    public String getWsCodRamo() {
        return this.wsCodRamo;
    }

    public String getWkCcAssicurativo() {
        return this.wkCcAssicurativo;
    }

    public void setBufferWhereConditionFormatted(String data) {
        byte[] buffer = new byte[Len.BUFFER_WHERE_CONDITION];
        MarshalByte.writeString(buffer, 1, data, Len.BUFFER_WHERE_CONDITION);
        setBufferWhereConditionBytes(buffer, 1);
    }

    public void setBufferWhereConditionBytes(byte[] buffer, int offset) {
        int position = offset;
        bufferWhCodRamo = MarshalByte.readString(buffer, position, Len.BUFFER_WH_COD_RAMO);
    }

    public void setBufferWhCodRamo(String bufferWhCodRamo) {
        this.bufferWhCodRamo = Functions.subString(bufferWhCodRamo, Len.BUFFER_WH_COD_RAMO);
    }

    public String getBufferWhCodRamo() {
        return this.bufferWhCodRamo;
    }

    public FlagAccessoXRangeLdbm0170 getFlagAccessoXRange() {
        return flagAccessoXRange;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndParamMovi getIndParamMovi() {
        return indParamMovi;
    }

    public Ispv0000 getIspv0000() {
        return ispv0000;
    }

    public Loac0560 getLoac0560() {
        return loac0560;
    }

    public DettTitContDb getParamMoviDb() {
        return paramMoviDb;
    }

    public WsDataStruttura getWsDataStruttura() {
        return wsDataStruttura;
    }

    public WsTimestamp getWsTimestamp() {
        return wsTimestamp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_GARANZIA = 2;
        public static final int WS_FORMA1 = 2;
        public static final int WS_FORMA2 = 2;
        public static final int WS_DATA_EFF = 10;
        public static final int WS_DT_ELAB_DA_DB = 10;
        public static final int WS_DT_ELAB_A_DB = 10;
        public static final int WS_COD_RAMO = 12;
        public static final int WS_ID_MOVI_CRZ = 9;
        public static final int WK_ID_PARAM_MOVI = 9;
        public static final int WS_DATA_INI = 10;
        public static final int WS_DATA_END = 10;
        public static final int WS_DATA_EFF9 = 8;
        public static final int BUFFER_WH_COD_RAMO = 12;
        public static final int BUFFER_WHERE_CONDITION = BUFFER_WH_COD_RAMO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
