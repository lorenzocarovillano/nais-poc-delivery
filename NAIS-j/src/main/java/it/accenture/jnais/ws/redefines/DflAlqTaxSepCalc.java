package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-ALQ-TAX-SEP-CALC<br>
 * Variable: DFL-ALQ-TAX-SEP-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflAlqTaxSepCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflAlqTaxSepCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_ALQ_TAX_SEP_CALC;
    }

    public void setDflAlqTaxSepCalc(AfDecimal dflAlqTaxSepCalc) {
        writeDecimalAsPacked(Pos.DFL_ALQ_TAX_SEP_CALC, dflAlqTaxSepCalc.copy());
    }

    public void setDflAlqTaxSepCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_ALQ_TAX_SEP_CALC, Pos.DFL_ALQ_TAX_SEP_CALC);
    }

    /**Original name: DFL-ALQ-TAX-SEP-CALC<br>*/
    public AfDecimal getDflAlqTaxSepCalc() {
        return readPackedAsDecimal(Pos.DFL_ALQ_TAX_SEP_CALC, Len.Int.DFL_ALQ_TAX_SEP_CALC, Len.Fract.DFL_ALQ_TAX_SEP_CALC);
    }

    public byte[] getDflAlqTaxSepCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_ALQ_TAX_SEP_CALC, Pos.DFL_ALQ_TAX_SEP_CALC);
        return buffer;
    }

    public void setDflAlqTaxSepCalcNull(String dflAlqTaxSepCalcNull) {
        writeString(Pos.DFL_ALQ_TAX_SEP_CALC_NULL, dflAlqTaxSepCalcNull, Len.DFL_ALQ_TAX_SEP_CALC_NULL);
    }

    /**Original name: DFL-ALQ-TAX-SEP-CALC-NULL<br>*/
    public String getDflAlqTaxSepCalcNull() {
        return readString(Pos.DFL_ALQ_TAX_SEP_CALC_NULL, Len.DFL_ALQ_TAX_SEP_CALC_NULL);
    }

    public String getDflAlqTaxSepCalcNullFormatted() {
        return Functions.padBlanks(getDflAlqTaxSepCalcNull(), Len.DFL_ALQ_TAX_SEP_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_TAX_SEP_CALC = 1;
        public static final int DFL_ALQ_TAX_SEP_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_ALQ_TAX_SEP_CALC = 4;
        public static final int DFL_ALQ_TAX_SEP_CALC_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_ALQ_TAX_SEP_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
