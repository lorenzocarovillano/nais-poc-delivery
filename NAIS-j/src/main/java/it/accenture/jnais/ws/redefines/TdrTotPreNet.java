package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-PRE-NET<br>
 * Variable: TDR-TOT-PRE-NET from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotPreNet extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotPreNet() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_PRE_NET;
    }

    public void setTdrTotPreNet(AfDecimal tdrTotPreNet) {
        writeDecimalAsPacked(Pos.TDR_TOT_PRE_NET, tdrTotPreNet.copy());
    }

    public void setTdrTotPreNetFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_PRE_NET, Pos.TDR_TOT_PRE_NET);
    }

    /**Original name: TDR-TOT-PRE-NET<br>*/
    public AfDecimal getTdrTotPreNet() {
        return readPackedAsDecimal(Pos.TDR_TOT_PRE_NET, Len.Int.TDR_TOT_PRE_NET, Len.Fract.TDR_TOT_PRE_NET);
    }

    public byte[] getTdrTotPreNetAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_PRE_NET, Pos.TDR_TOT_PRE_NET);
        return buffer;
    }

    public void setTdrTotPreNetNull(String tdrTotPreNetNull) {
        writeString(Pos.TDR_TOT_PRE_NET_NULL, tdrTotPreNetNull, Len.TDR_TOT_PRE_NET_NULL);
    }

    /**Original name: TDR-TOT-PRE-NET-NULL<br>*/
    public String getTdrTotPreNetNull() {
        return readString(Pos.TDR_TOT_PRE_NET_NULL, Len.TDR_TOT_PRE_NET_NULL);
    }

    public String getTdrTotPreNetNullFormatted() {
        return Functions.padBlanks(getTdrTotPreNetNull(), Len.TDR_TOT_PRE_NET_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_NET = 1;
        public static final int TDR_TOT_PRE_NET_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_PRE_NET = 8;
        public static final int TDR_TOT_PRE_NET_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_NET = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_PRE_NET = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
