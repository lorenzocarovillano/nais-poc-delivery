package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPLI-PC-LIQ<br>
 * Variable: WPLI-PC-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpliPcLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpliPcLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPLI_PC_LIQ;
    }

    public void setWpliPcLiq(AfDecimal wpliPcLiq) {
        writeDecimalAsPacked(Pos.WPLI_PC_LIQ, wpliPcLiq.copy());
    }

    public void setWpliPcLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPLI_PC_LIQ, Pos.WPLI_PC_LIQ);
    }

    /**Original name: WPLI-PC-LIQ<br>*/
    public AfDecimal getWpliPcLiq() {
        return readPackedAsDecimal(Pos.WPLI_PC_LIQ, Len.Int.WPLI_PC_LIQ, Len.Fract.WPLI_PC_LIQ);
    }

    public byte[] getWpliPcLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPLI_PC_LIQ, Pos.WPLI_PC_LIQ);
        return buffer;
    }

    public void initWpliPcLiqSpaces() {
        fill(Pos.WPLI_PC_LIQ, Len.WPLI_PC_LIQ, Types.SPACE_CHAR);
    }

    public void setWpliPcLiqNull(String wpliPcLiqNull) {
        writeString(Pos.WPLI_PC_LIQ_NULL, wpliPcLiqNull, Len.WPLI_PC_LIQ_NULL);
    }

    /**Original name: WPLI-PC-LIQ-NULL<br>*/
    public String getWpliPcLiqNull() {
        return readString(Pos.WPLI_PC_LIQ_NULL, Len.WPLI_PC_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPLI_PC_LIQ = 1;
        public static final int WPLI_PC_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_PC_LIQ = 4;
        public static final int WPLI_PC_LIQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPLI_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPLI_PC_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
