package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-SOST-EFFLQ<br>
 * Variable: DFL-IMPST-SOST-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstSostEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstSostEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_SOST_EFFLQ;
    }

    public void setDflImpstSostEfflq(AfDecimal dflImpstSostEfflq) {
        writeDecimalAsPacked(Pos.DFL_IMPST_SOST_EFFLQ, dflImpstSostEfflq.copy());
    }

    public void setDflImpstSostEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_SOST_EFFLQ, Pos.DFL_IMPST_SOST_EFFLQ);
    }

    /**Original name: DFL-IMPST-SOST-EFFLQ<br>*/
    public AfDecimal getDflImpstSostEfflq() {
        return readPackedAsDecimal(Pos.DFL_IMPST_SOST_EFFLQ, Len.Int.DFL_IMPST_SOST_EFFLQ, Len.Fract.DFL_IMPST_SOST_EFFLQ);
    }

    public byte[] getDflImpstSostEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_SOST_EFFLQ, Pos.DFL_IMPST_SOST_EFFLQ);
        return buffer;
    }

    public void setDflImpstSostEfflqNull(String dflImpstSostEfflqNull) {
        writeString(Pos.DFL_IMPST_SOST_EFFLQ_NULL, dflImpstSostEfflqNull, Len.DFL_IMPST_SOST_EFFLQ_NULL);
    }

    /**Original name: DFL-IMPST-SOST-EFFLQ-NULL<br>*/
    public String getDflImpstSostEfflqNull() {
        return readString(Pos.DFL_IMPST_SOST_EFFLQ_NULL, Len.DFL_IMPST_SOST_EFFLQ_NULL);
    }

    public String getDflImpstSostEfflqNullFormatted() {
        return Functions.padBlanks(getDflImpstSostEfflqNull(), Len.DFL_IMPST_SOST_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_SOST_EFFLQ = 1;
        public static final int DFL_IMPST_SOST_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_SOST_EFFLQ = 8;
        public static final int DFL_IMPST_SOST_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_SOST_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_SOST_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
