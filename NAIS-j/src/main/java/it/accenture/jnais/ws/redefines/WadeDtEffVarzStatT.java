package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WADE-DT-EFF-VARZ-STAT-T<br>
 * Variable: WADE-DT-EFF-VARZ-STAT-T from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WadeDtEffVarzStatT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WadeDtEffVarzStatT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WADE_DT_EFF_VARZ_STAT_T;
    }

    public void setWadeDtEffVarzStatT(int wadeDtEffVarzStatT) {
        writeIntAsPacked(Pos.WADE_DT_EFF_VARZ_STAT_T, wadeDtEffVarzStatT, Len.Int.WADE_DT_EFF_VARZ_STAT_T);
    }

    public void setWadeDtEffVarzStatTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WADE_DT_EFF_VARZ_STAT_T, Pos.WADE_DT_EFF_VARZ_STAT_T);
    }

    /**Original name: WADE-DT-EFF-VARZ-STAT-T<br>*/
    public int getWadeDtEffVarzStatT() {
        return readPackedAsInt(Pos.WADE_DT_EFF_VARZ_STAT_T, Len.Int.WADE_DT_EFF_VARZ_STAT_T);
    }

    public byte[] getWadeDtEffVarzStatTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WADE_DT_EFF_VARZ_STAT_T, Pos.WADE_DT_EFF_VARZ_STAT_T);
        return buffer;
    }

    public void initWadeDtEffVarzStatTSpaces() {
        fill(Pos.WADE_DT_EFF_VARZ_STAT_T, Len.WADE_DT_EFF_VARZ_STAT_T, Types.SPACE_CHAR);
    }

    public void setWadeDtEffVarzStatTNull(String wadeDtEffVarzStatTNull) {
        writeString(Pos.WADE_DT_EFF_VARZ_STAT_T_NULL, wadeDtEffVarzStatTNull, Len.WADE_DT_EFF_VARZ_STAT_T_NULL);
    }

    /**Original name: WADE-DT-EFF-VARZ-STAT-T-NULL<br>*/
    public String getWadeDtEffVarzStatTNull() {
        return readString(Pos.WADE_DT_EFF_VARZ_STAT_T_NULL, Len.WADE_DT_EFF_VARZ_STAT_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WADE_DT_EFF_VARZ_STAT_T = 1;
        public static final int WADE_DT_EFF_VARZ_STAT_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WADE_DT_EFF_VARZ_STAT_T = 5;
        public static final int WADE_DT_EFF_VARZ_STAT_T_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WADE_DT_EFF_VARZ_STAT_T = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
