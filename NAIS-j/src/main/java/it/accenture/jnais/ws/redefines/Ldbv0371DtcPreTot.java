package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: LDBV0371-DTC-PRE-TOT<br>
 * Variable: LDBV0371-DTC-PRE-TOT from program LDBS0370<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Ldbv0371DtcPreTot extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Ldbv0371DtcPreTot() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBV0371_DTC_PRE_TOT;
    }

    public void setLdbv0371DtcPreTot(AfDecimal ldbv0371DtcPreTot) {
        writeDecimalAsPacked(Pos.LDBV0371_DTC_PRE_TOT, ldbv0371DtcPreTot.copy());
    }

    public void setLdbv0371DtcPreTotFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LDBV0371_DTC_PRE_TOT, Pos.LDBV0371_DTC_PRE_TOT);
    }

    /**Original name: LDBV0371-DTC-PRE-TOT<br>*/
    public AfDecimal getLdbv0371DtcPreTot() {
        return readPackedAsDecimal(Pos.LDBV0371_DTC_PRE_TOT, Len.Int.LDBV0371_DTC_PRE_TOT, Len.Fract.LDBV0371_DTC_PRE_TOT);
    }

    public byte[] getLdbv0371DtcPreTotAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LDBV0371_DTC_PRE_TOT, Pos.LDBV0371_DTC_PRE_TOT);
        return buffer;
    }

    public void setLdbv0371DtcPreTotNull(String ldbv0371DtcPreTotNull) {
        writeString(Pos.LDBV0371_DTC_PRE_TOT_NULL, ldbv0371DtcPreTotNull, Len.LDBV0371_DTC_PRE_TOT_NULL);
    }

    /**Original name: LDBV0371-DTC-PRE-TOT-NULL<br>*/
    public String getLdbv0371DtcPreTotNull() {
        return readString(Pos.LDBV0371_DTC_PRE_TOT_NULL, Len.LDBV0371_DTC_PRE_TOT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LDBV0371_DTC_PRE_TOT = 1;
        public static final int LDBV0371_DTC_PRE_TOT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LDBV0371_DTC_PRE_TOT = 8;
        public static final int LDBV0371_DTC_PRE_TOT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int LDBV0371_DTC_PRE_TOT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int LDBV0371_DTC_PRE_TOT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
