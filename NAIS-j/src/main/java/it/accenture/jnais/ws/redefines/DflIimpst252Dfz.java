package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IIMPST-252-DFZ<br>
 * Variable: DFL-IIMPST-252-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIimpst252Dfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIimpst252Dfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IIMPST252_DFZ;
    }

    public void setDflIimpst252Dfz(AfDecimal dflIimpst252Dfz) {
        writeDecimalAsPacked(Pos.DFL_IIMPST252_DFZ, dflIimpst252Dfz.copy());
    }

    public void setDflIimpst252DfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IIMPST252_DFZ, Pos.DFL_IIMPST252_DFZ);
    }

    /**Original name: DFL-IIMPST-252-DFZ<br>*/
    public AfDecimal getDflIimpst252Dfz() {
        return readPackedAsDecimal(Pos.DFL_IIMPST252_DFZ, Len.Int.DFL_IIMPST252_DFZ, Len.Fract.DFL_IIMPST252_DFZ);
    }

    public byte[] getDflIimpst252DfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IIMPST252_DFZ, Pos.DFL_IIMPST252_DFZ);
        return buffer;
    }

    public void setDflIimpst252DfzNull(String dflIimpst252DfzNull) {
        writeString(Pos.DFL_IIMPST252_DFZ_NULL, dflIimpst252DfzNull, Len.DFL_IIMPST252_DFZ_NULL);
    }

    /**Original name: DFL-IIMPST-252-DFZ-NULL<br>*/
    public String getDflIimpst252DfzNull() {
        return readString(Pos.DFL_IIMPST252_DFZ_NULL, Len.DFL_IIMPST252_DFZ_NULL);
    }

    public String getDflIimpst252DfzNullFormatted() {
        return Functions.padBlanks(getDflIimpst252DfzNull(), Len.DFL_IIMPST252_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST252_DFZ = 1;
        public static final int DFL_IIMPST252_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST252_DFZ = 8;
        public static final int DFL_IIMPST252_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST252_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST252_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
