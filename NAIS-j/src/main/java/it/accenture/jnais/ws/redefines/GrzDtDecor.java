package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-DT-DECOR<br>
 * Variable: GRZ-DT-DECOR from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzDtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzDtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_DT_DECOR;
    }

    public void setGrzDtDecor(int grzDtDecor) {
        writeIntAsPacked(Pos.GRZ_DT_DECOR, grzDtDecor, Len.Int.GRZ_DT_DECOR);
    }

    public void setGrzDtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_DT_DECOR, Pos.GRZ_DT_DECOR);
    }

    /**Original name: GRZ-DT-DECOR<br>*/
    public int getGrzDtDecor() {
        return readPackedAsInt(Pos.GRZ_DT_DECOR, Len.Int.GRZ_DT_DECOR);
    }

    public byte[] getGrzDtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_DT_DECOR, Pos.GRZ_DT_DECOR);
        return buffer;
    }

    public void setGrzDtDecorNull(String grzDtDecorNull) {
        writeString(Pos.GRZ_DT_DECOR_NULL, grzDtDecorNull, Len.GRZ_DT_DECOR_NULL);
    }

    /**Original name: GRZ-DT-DECOR-NULL<br>*/
    public String getGrzDtDecorNull() {
        return readString(Pos.GRZ_DT_DECOR_NULL, Len.GRZ_DT_DECOR_NULL);
    }

    public String getGrzDtDecorNullFormatted() {
        return Functions.padBlanks(getGrzDtDecorNull(), Len.GRZ_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_DT_DECOR = 1;
        public static final int GRZ_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_DT_DECOR = 5;
        public static final int GRZ_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
