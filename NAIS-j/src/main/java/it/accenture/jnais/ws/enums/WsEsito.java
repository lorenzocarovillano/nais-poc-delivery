package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-ESITO<br>
 * Variable: WS-ESITO from program IEAS9700<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEsito {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WS_ESITO);
    public static final String SI_CORRETTO = "OK";
    public static final String NO_CORRETTO = "KO";

    //==== METHODS ====
    public void setWsEsito(String wsEsito) {
        this.value = Functions.subString(wsEsito, Len.WS_ESITO);
    }

    public String getWsEsito() {
        return this.value;
    }

    public boolean isSiCorretto() {
        return value.equals(SI_CORRETTO);
    }

    public void setSiCorretto() {
        value = SI_CORRETTO;
    }

    public boolean isNoCorretto() {
        return value.equals(NO_CORRETTO);
    }

    public void setNoCorretto() {
        value = NO_CORRETTO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ESITO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
