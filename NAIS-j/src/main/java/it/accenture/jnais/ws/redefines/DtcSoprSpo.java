package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SOPR-SPO<br>
 * Variable: DTC-SOPR-SPO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_SOPR_SPO;
    }

    public void setDtcSoprSpo(AfDecimal dtcSoprSpo) {
        writeDecimalAsPacked(Pos.DTC_SOPR_SPO, dtcSoprSpo.copy());
    }

    public void setDtcSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_SOPR_SPO, Pos.DTC_SOPR_SPO);
    }

    /**Original name: DTC-SOPR-SPO<br>*/
    public AfDecimal getDtcSoprSpo() {
        return readPackedAsDecimal(Pos.DTC_SOPR_SPO, Len.Int.DTC_SOPR_SPO, Len.Fract.DTC_SOPR_SPO);
    }

    public byte[] getDtcSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_SOPR_SPO, Pos.DTC_SOPR_SPO);
        return buffer;
    }

    public void setDtcSoprSpoNull(String dtcSoprSpoNull) {
        writeString(Pos.DTC_SOPR_SPO_NULL, dtcSoprSpoNull, Len.DTC_SOPR_SPO_NULL);
    }

    /**Original name: DTC-SOPR-SPO-NULL<br>*/
    public String getDtcSoprSpoNull() {
        return readString(Pos.DTC_SOPR_SPO_NULL, Len.DTC_SOPR_SPO_NULL);
    }

    public String getDtcSoprSpoNullFormatted() {
        return Functions.padBlanks(getDtcSoprSpoNull(), Len.DTC_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_SPO = 1;
        public static final int DTC_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_SOPR_SPO = 8;
        public static final int DTC_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
