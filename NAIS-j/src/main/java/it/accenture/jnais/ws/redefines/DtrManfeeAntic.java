package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-MANFEE-ANTIC<br>
 * Variable: DTR-MANFEE-ANTIC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrManfeeAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrManfeeAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_MANFEE_ANTIC;
    }

    public void setDtrManfeeAntic(AfDecimal dtrManfeeAntic) {
        writeDecimalAsPacked(Pos.DTR_MANFEE_ANTIC, dtrManfeeAntic.copy());
    }

    public void setDtrManfeeAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_MANFEE_ANTIC, Pos.DTR_MANFEE_ANTIC);
    }

    /**Original name: DTR-MANFEE-ANTIC<br>*/
    public AfDecimal getDtrManfeeAntic() {
        return readPackedAsDecimal(Pos.DTR_MANFEE_ANTIC, Len.Int.DTR_MANFEE_ANTIC, Len.Fract.DTR_MANFEE_ANTIC);
    }

    public byte[] getDtrManfeeAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_MANFEE_ANTIC, Pos.DTR_MANFEE_ANTIC);
        return buffer;
    }

    public void setDtrManfeeAnticNull(String dtrManfeeAnticNull) {
        writeString(Pos.DTR_MANFEE_ANTIC_NULL, dtrManfeeAnticNull, Len.DTR_MANFEE_ANTIC_NULL);
    }

    /**Original name: DTR-MANFEE-ANTIC-NULL<br>*/
    public String getDtrManfeeAnticNull() {
        return readString(Pos.DTR_MANFEE_ANTIC_NULL, Len.DTR_MANFEE_ANTIC_NULL);
    }

    public String getDtrManfeeAnticNullFormatted() {
        return Functions.padBlanks(getDtrManfeeAnticNull(), Len.DTR_MANFEE_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_MANFEE_ANTIC = 1;
        public static final int DTR_MANFEE_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_MANFEE_ANTIC = 8;
        public static final int DTR_MANFEE_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_MANFEE_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_MANFEE_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
