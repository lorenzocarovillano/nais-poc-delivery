package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-TGA-IMP-ACQ-EXP<br>
 * Variable: WPAG-TGA-IMP-ACQ-EXP from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpAcqExp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpAcqExp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMP_ACQ_EXP;
    }

    public void setWpagTgaImpAcqExp(AfDecimal wpagTgaImpAcqExp) {
        writeDecimalAsPacked(Pos.WPAG_TGA_IMP_ACQ_EXP, wpagTgaImpAcqExp.copy());
    }

    public void setWpagTgaImpAcqExpFormatted(String wpagTgaImpAcqExp) {
        setWpagTgaImpAcqExp(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_TGA_IMP_ACQ_EXP + Len.Fract.WPAG_TGA_IMP_ACQ_EXP, Len.Fract.WPAG_TGA_IMP_ACQ_EXP, wpagTgaImpAcqExp));
    }

    public void setWpagTgaImpAcqExpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMP_ACQ_EXP, Pos.WPAG_TGA_IMP_ACQ_EXP);
    }

    /**Original name: WPAG-TGA-IMP-ACQ-EXP<br>*/
    public AfDecimal getWpagTgaImpAcqExp() {
        return readPackedAsDecimal(Pos.WPAG_TGA_IMP_ACQ_EXP, Len.Int.WPAG_TGA_IMP_ACQ_EXP, Len.Fract.WPAG_TGA_IMP_ACQ_EXP);
    }

    public byte[] getWpagTgaImpAcqExpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMP_ACQ_EXP, Pos.WPAG_TGA_IMP_ACQ_EXP);
        return buffer;
    }

    public void initWpagTgaImpAcqExpSpaces() {
        fill(Pos.WPAG_TGA_IMP_ACQ_EXP, Len.WPAG_TGA_IMP_ACQ_EXP, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpAcqExpNull(String wpagTgaImpAcqExpNull) {
        writeString(Pos.WPAG_TGA_IMP_ACQ_EXP_NULL, wpagTgaImpAcqExpNull, Len.WPAG_TGA_IMP_ACQ_EXP_NULL);
    }

    /**Original name: WPAG-TGA-IMP-ACQ-EXP-NULL<br>*/
    public String getWpagTgaImpAcqExpNull() {
        return readString(Pos.WPAG_TGA_IMP_ACQ_EXP_NULL, Len.WPAG_TGA_IMP_ACQ_EXP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_ACQ_EXP = 1;
        public static final int WPAG_TGA_IMP_ACQ_EXP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_ACQ_EXP = 8;
        public static final int WPAG_TGA_IMP_ACQ_EXP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_ACQ_EXP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_TGA_IMP_ACQ_EXP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
