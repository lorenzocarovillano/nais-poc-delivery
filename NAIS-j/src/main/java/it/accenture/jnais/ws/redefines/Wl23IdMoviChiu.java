package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL23-ID-MOVI-CHIU<br>
 * Variable: WL23-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23IdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23IdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_ID_MOVI_CHIU;
    }

    public void setWl23IdMoviChiu(int wl23IdMoviChiu) {
        writeIntAsPacked(Pos.WL23_ID_MOVI_CHIU, wl23IdMoviChiu, Len.Int.WL23_ID_MOVI_CHIU);
    }

    public void setWl23IdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_ID_MOVI_CHIU, Pos.WL23_ID_MOVI_CHIU);
    }

    /**Original name: WL23-ID-MOVI-CHIU<br>*/
    public int getWl23IdMoviChiu() {
        return readPackedAsInt(Pos.WL23_ID_MOVI_CHIU, Len.Int.WL23_ID_MOVI_CHIU);
    }

    public byte[] getWl23IdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_ID_MOVI_CHIU, Pos.WL23_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWl23IdMoviChiuSpaces() {
        fill(Pos.WL23_ID_MOVI_CHIU, Len.WL23_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWl23IdMoviChiuNull(String wl23IdMoviChiuNull) {
        writeString(Pos.WL23_ID_MOVI_CHIU_NULL, wl23IdMoviChiuNull, Len.WL23_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WL23-ID-MOVI-CHIU-NULL<br>*/
    public String getWl23IdMoviChiuNull() {
        return readString(Pos.WL23_ID_MOVI_CHIU_NULL, Len.WL23_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_ID_MOVI_CHIU = 1;
        public static final int WL23_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_ID_MOVI_CHIU = 5;
        public static final int WL23_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
