package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-MIN-GARTO<br>
 * Variable: WP85-MIN-GARTO from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85MinGarto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85MinGarto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_MIN_GARTO;
    }

    public void setWp85MinGarto(AfDecimal wp85MinGarto) {
        writeDecimalAsPacked(Pos.WP85_MIN_GARTO, wp85MinGarto.copy());
    }

    /**Original name: WP85-MIN-GARTO<br>*/
    public AfDecimal getWp85MinGarto() {
        return readPackedAsDecimal(Pos.WP85_MIN_GARTO, Len.Int.WP85_MIN_GARTO, Len.Fract.WP85_MIN_GARTO);
    }

    public void setWp85MinGartoNull(String wp85MinGartoNull) {
        writeString(Pos.WP85_MIN_GARTO_NULL, wp85MinGartoNull, Len.WP85_MIN_GARTO_NULL);
    }

    /**Original name: WP85-MIN-GARTO-NULL<br>*/
    public String getWp85MinGartoNull() {
        return readString(Pos.WP85_MIN_GARTO_NULL, Len.WP85_MIN_GARTO_NULL);
    }

    public String getWp85MinGartoNullFormatted() {
        return Functions.padBlanks(getWp85MinGartoNull(), Len.WP85_MIN_GARTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_MIN_GARTO = 1;
        public static final int WP85_MIN_GARTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_MIN_GARTO = 8;
        public static final int WP85_MIN_GARTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_MIN_GARTO = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_MIN_GARTO = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
