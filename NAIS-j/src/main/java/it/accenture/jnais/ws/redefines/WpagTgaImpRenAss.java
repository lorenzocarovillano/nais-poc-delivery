package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: WPAG-TGA-IMP-REN-ASS<br>
 * Variable: WPAG-TGA-IMP-REN-ASS from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagTgaImpRenAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagTgaImpRenAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_TGA_IMP_REN_ASS;
    }

    public void setWpagTgaImpRenAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_TGA_IMP_REN_ASS, Pos.WPAG_TGA_IMP_REN_ASS);
    }

    public byte[] getWpagTgaImpRenAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_TGA_IMP_REN_ASS, Pos.WPAG_TGA_IMP_REN_ASS);
        return buffer;
    }

    public void initWpagTgaImpRenAssSpaces() {
        fill(Pos.WPAG_TGA_IMP_REN_ASS, Len.WPAG_TGA_IMP_REN_ASS, Types.SPACE_CHAR);
    }

    public void setWpagTgaImpRenAssNull(String wpagTgaImpRenAssNull) {
        writeString(Pos.WPAG_TGA_IMP_REN_ASS_NULL, wpagTgaImpRenAssNull, Len.WPAG_TGA_IMP_REN_ASS_NULL);
    }

    /**Original name: WPAG-TGA-IMP-REN-ASS-NULL<br>*/
    public String getWpagTgaImpRenAssNull() {
        return readString(Pos.WPAG_TGA_IMP_REN_ASS_NULL, Len.WPAG_TGA_IMP_REN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_REN_ASS = 1;
        public static final int WPAG_TGA_IMP_REN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_TGA_IMP_REN_ASS = 8;
        public static final int WPAG_TGA_IMP_REN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
