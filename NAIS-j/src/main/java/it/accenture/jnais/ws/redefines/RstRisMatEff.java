package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-MAT-EFF<br>
 * Variable: RST-RIS-MAT-EFF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisMatEff extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisMatEff() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_MAT_EFF;
    }

    public void setRstRisMatEff(AfDecimal rstRisMatEff) {
        writeDecimalAsPacked(Pos.RST_RIS_MAT_EFF, rstRisMatEff.copy());
    }

    public void setRstRisMatEffFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_MAT_EFF, Pos.RST_RIS_MAT_EFF);
    }

    /**Original name: RST-RIS-MAT-EFF<br>*/
    public AfDecimal getRstRisMatEff() {
        return readPackedAsDecimal(Pos.RST_RIS_MAT_EFF, Len.Int.RST_RIS_MAT_EFF, Len.Fract.RST_RIS_MAT_EFF);
    }

    public byte[] getRstRisMatEffAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_MAT_EFF, Pos.RST_RIS_MAT_EFF);
        return buffer;
    }

    public void setRstRisMatEffNull(String rstRisMatEffNull) {
        writeString(Pos.RST_RIS_MAT_EFF_NULL, rstRisMatEffNull, Len.RST_RIS_MAT_EFF_NULL);
    }

    /**Original name: RST-RIS-MAT-EFF-NULL<br>*/
    public String getRstRisMatEffNull() {
        return readString(Pos.RST_RIS_MAT_EFF_NULL, Len.RST_RIS_MAT_EFF_NULL);
    }

    public String getRstRisMatEffNullFormatted() {
        return Functions.padBlanks(getRstRisMatEffNull(), Len.RST_RIS_MAT_EFF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_MAT_EFF = 1;
        public static final int RST_RIS_MAT_EFF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_MAT_EFF = 8;
        public static final int RST_RIS_MAT_EFF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_MAT_EFF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_MAT_EFF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
