package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-GARANZIA-BASE<br>
 * Variable: WS-GARANZIA-BASE from program LCCS0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsGaranziaBase {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWsGaranziaBase(char wsGaranziaBase) {
        this.value = wsGaranziaBase;
    }

    public char getWsGaranziaBase() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
