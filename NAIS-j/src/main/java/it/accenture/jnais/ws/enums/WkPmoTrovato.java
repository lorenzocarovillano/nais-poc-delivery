package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-PMO-TROVATO<br>
 * Variable: WK-PMO-TROVATO from program LCCS0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkPmoTrovato {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setWkPmoTrovato(char wkPmoTrovato) {
        this.value = wkPmoTrovato;
    }

    public char getWkPmoTrovato() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
