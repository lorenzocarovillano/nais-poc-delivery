package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPAG-IMP-ABB-TOT-INI<br>
 * Variable: WPAG-IMP-ABB-TOT-INI from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpAbbTotIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpAbbTotIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_ABB_TOT_INI;
    }

    public void setWpagImpAbbTotIni(AfDecimal wpagImpAbbTotIni) {
        writeDecimalAsPacked(Pos.WPAG_IMP_ABB_TOT_INI, wpagImpAbbTotIni.copy());
    }

    public void setWpagImpAbbTotIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_ABB_TOT_INI, Pos.WPAG_IMP_ABB_TOT_INI);
    }

    /**Original name: WPAG-IMP-ABB-TOT-INI<br>*/
    public AfDecimal getWpagImpAbbTotIni() {
        return readPackedAsDecimal(Pos.WPAG_IMP_ABB_TOT_INI, Len.Int.WPAG_IMP_ABB_TOT_INI, Len.Fract.WPAG_IMP_ABB_TOT_INI);
    }

    public byte[] getWpagImpAbbTotIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_ABB_TOT_INI, Pos.WPAG_IMP_ABB_TOT_INI);
        return buffer;
    }

    public void initWpagImpAbbTotIniSpaces() {
        fill(Pos.WPAG_IMP_ABB_TOT_INI, Len.WPAG_IMP_ABB_TOT_INI, Types.SPACE_CHAR);
    }

    public void setWpagImpAbbTotIniNull(String wpagImpAbbTotIniNull) {
        writeString(Pos.WPAG_IMP_ABB_TOT_INI_NULL, wpagImpAbbTotIniNull, Len.WPAG_IMP_ABB_TOT_INI_NULL);
    }

    /**Original name: WPAG-IMP-ABB-TOT-INI-NULL<br>*/
    public String getWpagImpAbbTotIniNull() {
        return readString(Pos.WPAG_IMP_ABB_TOT_INI_NULL, Len.WPAG_IMP_ABB_TOT_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ABB_TOT_INI = 1;
        public static final int WPAG_IMP_ABB_TOT_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_ABB_TOT_INI = 8;
        public static final int WPAG_IMP_ABB_TOT_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ABB_TOT_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_ABB_TOT_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
