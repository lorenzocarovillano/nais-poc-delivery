package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-DTC1<br>
 * Variable: SW-DTC1 from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwDtc1 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setSwDtc1(char swDtc1) {
        this.value = swDtc1;
    }

    public char getSwDtc1() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }
}
