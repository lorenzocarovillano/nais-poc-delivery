package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-READ-FILESQS5<br>
 * Variable: FLAG-READ-FILESQS5 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagReadFilesqs5 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagReadFilesqs5(char flagReadFilesqs5) {
        this.value = flagReadFilesqs5;
    }

    public char getFlagReadFilesqs5() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setReadFilesqs5Si() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
