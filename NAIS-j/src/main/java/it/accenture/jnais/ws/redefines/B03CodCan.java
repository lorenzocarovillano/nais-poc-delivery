package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-COD-CAN<br>
 * Variable: B03-COD-CAN from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CodCan extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CodCan() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_COD_CAN;
    }

    public void setB03CodCan(int b03CodCan) {
        writeIntAsPacked(Pos.B03_COD_CAN, b03CodCan, Len.Int.B03_COD_CAN);
    }

    public void setB03CodCanFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_COD_CAN, Pos.B03_COD_CAN);
    }

    /**Original name: B03-COD-CAN<br>*/
    public int getB03CodCan() {
        return readPackedAsInt(Pos.B03_COD_CAN, Len.Int.B03_COD_CAN);
    }

    public byte[] getB03CodCanAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_COD_CAN, Pos.B03_COD_CAN);
        return buffer;
    }

    public void setB03CodCanNull(String b03CodCanNull) {
        writeString(Pos.B03_COD_CAN_NULL, b03CodCanNull, Len.B03_COD_CAN_NULL);
    }

    /**Original name: B03-COD-CAN-NULL<br>*/
    public String getB03CodCanNull() {
        return readString(Pos.B03_COD_CAN_NULL, Len.B03_COD_CAN_NULL);
    }

    public String getB03CodCanNullFormatted() {
        return Functions.padBlanks(getB03CodCanNull(), Len.B03_COD_CAN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_COD_CAN = 1;
        public static final int B03_COD_CAN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_COD_CAN = 3;
        public static final int B03_COD_CAN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_COD_CAN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
