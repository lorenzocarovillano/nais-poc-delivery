package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-READ-FILESQS1<br>
 * Variable: FLAG-READ-FILESQS1 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagReadFilesqs1 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagReadFilesqs1(char flagReadFilesqs1) {
        this.value = flagReadFilesqs1;
    }

    public char getFlagReadFilesqs1() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setNo() {
        value = NO;
    }
}
