package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-QTZ<br>
 * Variable: WS-TP-QTZ from copybook LCCVL420<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpQtz {

    //==== PROPERTIES ====
    private String value = "";
    public static final String CALC_RIS = "CR";
    public static final String OPZ_SOT_FND_INDEX = "OP";
    public static final String ZER_COUP_SOT_FND_INDEX = "ZC";
    public static final String VAL_QUO_LRD = "QL";
    public static final String QUOZ_BENCHMARK = "BC";
    public static final String BOND = "BO";
    public static final String SWAP = "SW";
    public static final String TOTALE = "TT";
    public static final String OPZIONE_SOT_FND_INDEX = "OZ";
    public static final String BOND_INIZ = "IB";
    public static final String SWAP_INIZ = "IS";
    public static final String OPZIONE_INIZ = "IO";
    public static final String TOT_INIZ = "IT";

    //==== METHODS ====
    public void setWsTpQtz(String wsTpQtz) {
        this.value = Functions.subString(wsTpQtz, Len.WS_TP_QTZ);
    }

    public String getWsTpQtz() {
        return this.value;
    }

    public void setCalcRis() {
        value = CALC_RIS;
    }

    public boolean isOpzSotFndIndex() {
        return value.equals(OPZ_SOT_FND_INDEX);
    }

    public boolean isZerCoupSotFndIndex() {
        return value.equals(ZER_COUP_SOT_FND_INDEX);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_QTZ = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
