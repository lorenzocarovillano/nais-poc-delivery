package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-CPT-RSH-MOR<br>
 * Variable: WTGA-CPT-RSH-MOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaCptRshMor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaCptRshMor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_CPT_RSH_MOR;
    }

    public void setWtgaCptRshMor(AfDecimal wtgaCptRshMor) {
        writeDecimalAsPacked(Pos.WTGA_CPT_RSH_MOR, wtgaCptRshMor.copy());
    }

    public void setWtgaCptRshMorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_CPT_RSH_MOR, Pos.WTGA_CPT_RSH_MOR);
    }

    /**Original name: WTGA-CPT-RSH-MOR<br>*/
    public AfDecimal getWtgaCptRshMor() {
        return readPackedAsDecimal(Pos.WTGA_CPT_RSH_MOR, Len.Int.WTGA_CPT_RSH_MOR, Len.Fract.WTGA_CPT_RSH_MOR);
    }

    public byte[] getWtgaCptRshMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_CPT_RSH_MOR, Pos.WTGA_CPT_RSH_MOR);
        return buffer;
    }

    public void initWtgaCptRshMorSpaces() {
        fill(Pos.WTGA_CPT_RSH_MOR, Len.WTGA_CPT_RSH_MOR, Types.SPACE_CHAR);
    }

    public void setWtgaCptRshMorNull(String wtgaCptRshMorNull) {
        writeString(Pos.WTGA_CPT_RSH_MOR_NULL, wtgaCptRshMorNull, Len.WTGA_CPT_RSH_MOR_NULL);
    }

    /**Original name: WTGA-CPT-RSH-MOR-NULL<br>*/
    public String getWtgaCptRshMorNull() {
        return readString(Pos.WTGA_CPT_RSH_MOR_NULL, Len.WTGA_CPT_RSH_MOR_NULL);
    }

    public String getWtgaCptRshMorNullFormatted() {
        return Functions.padBlanks(getWtgaCptRshMorNull(), Len.WTGA_CPT_RSH_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_CPT_RSH_MOR = 1;
        public static final int WTGA_CPT_RSH_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_CPT_RSH_MOR = 8;
        public static final int WTGA_CPT_RSH_MOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_CPT_RSH_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_CPT_RSH_MOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
