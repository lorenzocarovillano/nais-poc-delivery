package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-GAR-INS<br>
 * Variable: SW-GAR-INS from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwGarIns {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI_GAR_INS = 'S';
    public static final char NO_GAR_INS = 'N';

    //==== METHODS ====
    public void setSwGarIns(char swGarIns) {
        this.value = swGarIns;
    }

    public char getSwGarIns() {
        return this.value;
    }

    public boolean isSiGarIns() {
        return value == SI_GAR_INS;
    }

    public void setSiGarIns() {
        value = SI_GAR_INS;
    }

    public boolean isNoGarIns() {
        return value == NO_GAR_INS;
    }

    public void setNoGarIns() {
        value = NO_GAR_INS;
    }
}
