package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P61-ID-TRCH-DI-GAR<br>
 * Variable: P61-ID-TRCH-DI-GAR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P61IdTrchDiGar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P61IdTrchDiGar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P61_ID_TRCH_DI_GAR;
    }

    public void setP61IdTrchDiGar(int p61IdTrchDiGar) {
        writeIntAsPacked(Pos.P61_ID_TRCH_DI_GAR, p61IdTrchDiGar, Len.Int.P61_ID_TRCH_DI_GAR);
    }

    public void setP61IdTrchDiGarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P61_ID_TRCH_DI_GAR, Pos.P61_ID_TRCH_DI_GAR);
    }

    /**Original name: P61-ID-TRCH-DI-GAR<br>*/
    public int getP61IdTrchDiGar() {
        return readPackedAsInt(Pos.P61_ID_TRCH_DI_GAR, Len.Int.P61_ID_TRCH_DI_GAR);
    }

    public byte[] getP61IdTrchDiGarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P61_ID_TRCH_DI_GAR, Pos.P61_ID_TRCH_DI_GAR);
        return buffer;
    }

    public void setP61IdTrchDiGarNull(String p61IdTrchDiGarNull) {
        writeString(Pos.P61_ID_TRCH_DI_GAR_NULL, p61IdTrchDiGarNull, Len.P61_ID_TRCH_DI_GAR_NULL);
    }

    /**Original name: P61-ID-TRCH-DI-GAR-NULL<br>*/
    public String getP61IdTrchDiGarNull() {
        return readString(Pos.P61_ID_TRCH_DI_GAR_NULL, Len.P61_ID_TRCH_DI_GAR_NULL);
    }

    public String getP61IdTrchDiGarNullFormatted() {
        return Functions.padBlanks(getP61IdTrchDiGarNull(), Len.P61_ID_TRCH_DI_GAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P61_ID_TRCH_DI_GAR = 1;
        public static final int P61_ID_TRCH_DI_GAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_ID_TRCH_DI_GAR = 5;
        public static final int P61_ID_TRCH_DI_GAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_ID_TRCH_DI_GAR = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
