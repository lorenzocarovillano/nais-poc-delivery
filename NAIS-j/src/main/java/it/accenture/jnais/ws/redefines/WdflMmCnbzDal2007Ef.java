package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFL-MM-CNBZ-DAL2007-EF<br>
 * Variable: WDFL-MM-CNBZ-DAL2007-EF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflMmCnbzDal2007Ef extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflMmCnbzDal2007Ef() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_MM_CNBZ_DAL2007_EF;
    }

    public void setWdflMmCnbzDal2007Ef(int wdflMmCnbzDal2007Ef) {
        writeIntAsPacked(Pos.WDFL_MM_CNBZ_DAL2007_EF, wdflMmCnbzDal2007Ef, Len.Int.WDFL_MM_CNBZ_DAL2007_EF);
    }

    public void setWdflMmCnbzDal2007EfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_MM_CNBZ_DAL2007_EF, Pos.WDFL_MM_CNBZ_DAL2007_EF);
    }

    /**Original name: WDFL-MM-CNBZ-DAL2007-EF<br>*/
    public int getWdflMmCnbzDal2007Ef() {
        return readPackedAsInt(Pos.WDFL_MM_CNBZ_DAL2007_EF, Len.Int.WDFL_MM_CNBZ_DAL2007_EF);
    }

    public byte[] getWdflMmCnbzDal2007EfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_MM_CNBZ_DAL2007_EF, Pos.WDFL_MM_CNBZ_DAL2007_EF);
        return buffer;
    }

    public void setWdflMmCnbzDal2007EfNull(String wdflMmCnbzDal2007EfNull) {
        writeString(Pos.WDFL_MM_CNBZ_DAL2007_EF_NULL, wdflMmCnbzDal2007EfNull, Len.WDFL_MM_CNBZ_DAL2007_EF_NULL);
    }

    /**Original name: WDFL-MM-CNBZ-DAL2007-EF-NULL<br>*/
    public String getWdflMmCnbzDal2007EfNull() {
        return readString(Pos.WDFL_MM_CNBZ_DAL2007_EF_NULL, Len.WDFL_MM_CNBZ_DAL2007_EF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_MM_CNBZ_DAL2007_EF = 1;
        public static final int WDFL_MM_CNBZ_DAL2007_EF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_MM_CNBZ_DAL2007_EF = 3;
        public static final int WDFL_MM_CNBZ_DAL2007_EF_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_MM_CNBZ_DAL2007_EF = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
