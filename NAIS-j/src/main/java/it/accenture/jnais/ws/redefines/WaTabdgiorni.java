package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WA-TABDGIORNI<br>
 * Variable: WA-TABDGIORNI from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WaTabdgiorni extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int DGIORNO_MAXOCCURS = 7;

    //==== CONSTRUCTORS ====
    public WaTabdgiorni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WA_TABDGIORNI;
    }

    @Override
    public void init() {
        int position = 1;
        writeString(position, "DOMENICA", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "LUNEDI", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "MARTEDI", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "MERCOLEDI", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "GIOVEDI", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "VENERDI", Len.FLR1);
        position += Len.FLR1;
        writeString(position, "SABATO", Len.FLR1);
    }

    /**Original name: WA-DGIORNO<br>*/
    public String getDgiorno(int dgiornoIdx) {
        int position = Pos.waDgiorno(dgiornoIdx - 1);
        return readString(position, Len.DGIORNO);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WA_TABDGIORNI = 1;
        public static final int FLR1 = WA_TABDGIORNI;
        public static final int FLR2 = FLR1 + Len.FLR1;
        public static final int FLR3 = FLR2 + Len.FLR1;
        public static final int FLR4 = FLR3 + Len.FLR1;
        public static final int FLR5 = FLR4 + Len.FLR1;
        public static final int FLR6 = FLR5 + Len.FLR1;
        public static final int FLR7 = FLR6 + Len.FLR1;
        public static final int FILLER_WS1 = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int waDgiorno(int idx) {
            return FILLER_WS1 + idx * Len.DGIORNO;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int FLR1 = 9;
        public static final int DGIORNO = 9;
        public static final int WA_TABDGIORNI = 7 * FLR1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
