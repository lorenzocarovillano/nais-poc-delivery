package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-PATTUITO-INI<br>
 * Variable: WB03-PRE-PATTUITO-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrePattuitoIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrePattuitoIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_PATTUITO_INI;
    }

    public void setWb03PrePattuitoIni(AfDecimal wb03PrePattuitoIni) {
        writeDecimalAsPacked(Pos.WB03_PRE_PATTUITO_INI, wb03PrePattuitoIni.copy());
    }

    public void setWb03PrePattuitoIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_PATTUITO_INI, Pos.WB03_PRE_PATTUITO_INI);
    }

    /**Original name: WB03-PRE-PATTUITO-INI<br>*/
    public AfDecimal getWb03PrePattuitoIni() {
        return readPackedAsDecimal(Pos.WB03_PRE_PATTUITO_INI, Len.Int.WB03_PRE_PATTUITO_INI, Len.Fract.WB03_PRE_PATTUITO_INI);
    }

    public byte[] getWb03PrePattuitoIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_PATTUITO_INI, Pos.WB03_PRE_PATTUITO_INI);
        return buffer;
    }

    public void setWb03PrePattuitoIniNull(String wb03PrePattuitoIniNull) {
        writeString(Pos.WB03_PRE_PATTUITO_INI_NULL, wb03PrePattuitoIniNull, Len.WB03_PRE_PATTUITO_INI_NULL);
    }

    /**Original name: WB03-PRE-PATTUITO-INI-NULL<br>*/
    public String getWb03PrePattuitoIniNull() {
        return readString(Pos.WB03_PRE_PATTUITO_INI_NULL, Len.WB03_PRE_PATTUITO_INI_NULL);
    }

    public String getWb03PrePattuitoIniNullFormatted() {
        return Functions.padBlanks(getWb03PrePattuitoIniNull(), Len.WB03_PRE_PATTUITO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_PATTUITO_INI = 1;
        public static final int WB03_PRE_PATTUITO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_PATTUITO_INI = 8;
        public static final int WB03_PRE_PATTUITO_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_PATTUITO_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_PATTUITO_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
