package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-TOT-IMP-RIMB<br>
 * Variable: S089-TOT-IMP-RIMB from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089TotImpRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089TotImpRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_TOT_IMP_RIMB;
    }

    public void setWlquTotImpRimb(AfDecimal wlquTotImpRimb) {
        writeDecimalAsPacked(Pos.S089_TOT_IMP_RIMB, wlquTotImpRimb.copy());
    }

    public void setWlquTotImpRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_TOT_IMP_RIMB, Pos.S089_TOT_IMP_RIMB);
    }

    /**Original name: WLQU-TOT-IMP-RIMB<br>*/
    public AfDecimal getWlquTotImpRimb() {
        return readPackedAsDecimal(Pos.S089_TOT_IMP_RIMB, Len.Int.WLQU_TOT_IMP_RIMB, Len.Fract.WLQU_TOT_IMP_RIMB);
    }

    public byte[] getWlquTotImpRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_TOT_IMP_RIMB, Pos.S089_TOT_IMP_RIMB);
        return buffer;
    }

    public void initWlquTotImpRimbSpaces() {
        fill(Pos.S089_TOT_IMP_RIMB, Len.S089_TOT_IMP_RIMB, Types.SPACE_CHAR);
    }

    public void setWlquTotImpRimbNull(String wlquTotImpRimbNull) {
        writeString(Pos.S089_TOT_IMP_RIMB_NULL, wlquTotImpRimbNull, Len.WLQU_TOT_IMP_RIMB_NULL);
    }

    /**Original name: WLQU-TOT-IMP-RIMB-NULL<br>*/
    public String getWlquTotImpRimbNull() {
        return readString(Pos.S089_TOT_IMP_RIMB_NULL, Len.WLQU_TOT_IMP_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIMB = 1;
        public static final int S089_TOT_IMP_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_TOT_IMP_RIMB = 8;
        public static final int WLQU_TOT_IMP_RIMB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIMB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_TOT_IMP_RIMB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
