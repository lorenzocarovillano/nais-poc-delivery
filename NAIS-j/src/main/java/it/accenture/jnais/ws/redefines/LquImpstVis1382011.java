package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-VIS-1382011<br>
 * Variable: LQU-IMPST-VIS-1382011 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstVis1382011 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstVis1382011() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_VIS1382011;
    }

    public void setLquImpstVis1382011(AfDecimal lquImpstVis1382011) {
        writeDecimalAsPacked(Pos.LQU_IMPST_VIS1382011, lquImpstVis1382011.copy());
    }

    public void setLquImpstVis1382011FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_VIS1382011, Pos.LQU_IMPST_VIS1382011);
    }

    /**Original name: LQU-IMPST-VIS-1382011<br>*/
    public AfDecimal getLquImpstVis1382011() {
        return readPackedAsDecimal(Pos.LQU_IMPST_VIS1382011, Len.Int.LQU_IMPST_VIS1382011, Len.Fract.LQU_IMPST_VIS1382011);
    }

    public byte[] getLquImpstVis1382011AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_VIS1382011, Pos.LQU_IMPST_VIS1382011);
        return buffer;
    }

    public void setLquImpstVis1382011Null(String lquImpstVis1382011Null) {
        writeString(Pos.LQU_IMPST_VIS1382011_NULL, lquImpstVis1382011Null, Len.LQU_IMPST_VIS1382011_NULL);
    }

    /**Original name: LQU-IMPST-VIS-1382011-NULL<br>*/
    public String getLquImpstVis1382011Null() {
        return readString(Pos.LQU_IMPST_VIS1382011_NULL, Len.LQU_IMPST_VIS1382011_NULL);
    }

    public String getLquImpstVis1382011NullFormatted() {
        return Functions.padBlanks(getLquImpstVis1382011Null(), Len.LQU_IMPST_VIS1382011_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_VIS1382011 = 1;
        public static final int LQU_IMPST_VIS1382011_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_VIS1382011 = 8;
        public static final int LQU_IMPST_VIS1382011_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_VIS1382011 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_VIS1382011 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
