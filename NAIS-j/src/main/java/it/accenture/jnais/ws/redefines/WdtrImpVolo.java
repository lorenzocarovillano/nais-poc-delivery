package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-IMP-VOLO<br>
 * Variable: WDTR-IMP-VOLO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_IMP_VOLO;
    }

    public void setWdtrImpVolo(AfDecimal wdtrImpVolo) {
        writeDecimalAsPacked(Pos.WDTR_IMP_VOLO, wdtrImpVolo.copy());
    }

    /**Original name: WDTR-IMP-VOLO<br>*/
    public AfDecimal getWdtrImpVolo() {
        return readPackedAsDecimal(Pos.WDTR_IMP_VOLO, Len.Int.WDTR_IMP_VOLO, Len.Fract.WDTR_IMP_VOLO);
    }

    public void setWdtrImpVoloNull(String wdtrImpVoloNull) {
        writeString(Pos.WDTR_IMP_VOLO_NULL, wdtrImpVoloNull, Len.WDTR_IMP_VOLO_NULL);
    }

    /**Original name: WDTR-IMP-VOLO-NULL<br>*/
    public String getWdtrImpVoloNull() {
        return readString(Pos.WDTR_IMP_VOLO_NULL, Len.WDTR_IMP_VOLO_NULL);
    }

    public String getWdtrImpVoloNullFormatted() {
        return Functions.padBlanks(getWdtrImpVoloNull(), Len.WDTR_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_VOLO = 1;
        public static final int WDTR_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_IMP_VOLO = 8;
        public static final int WDTR_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
