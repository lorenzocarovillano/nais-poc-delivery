package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IS-662014C<br>
 * Variable: DFL-IS-662014C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIs662014c extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIs662014c() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IS662014C;
    }

    public void setDflIs662014c(AfDecimal dflIs662014c) {
        writeDecimalAsPacked(Pos.DFL_IS662014C, dflIs662014c.copy());
    }

    public void setDflIs662014cFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IS662014C, Pos.DFL_IS662014C);
    }

    /**Original name: DFL-IS-662014C<br>*/
    public AfDecimal getDflIs662014c() {
        return readPackedAsDecimal(Pos.DFL_IS662014C, Len.Int.DFL_IS662014C, Len.Fract.DFL_IS662014C);
    }

    public byte[] getDflIs662014cAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IS662014C, Pos.DFL_IS662014C);
        return buffer;
    }

    public void setDflIs662014cNull(String dflIs662014cNull) {
        writeString(Pos.DFL_IS662014C_NULL, dflIs662014cNull, Len.DFL_IS662014C_NULL);
    }

    /**Original name: DFL-IS-662014C-NULL<br>*/
    public String getDflIs662014cNull() {
        return readString(Pos.DFL_IS662014C_NULL, Len.DFL_IS662014C_NULL);
    }

    public String getDflIs662014cNullFormatted() {
        return Functions.padBlanks(getDflIs662014cNull(), Len.DFL_IS662014C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IS662014C = 1;
        public static final int DFL_IS662014C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IS662014C = 8;
        public static final int DFL_IS662014C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IS662014C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IS662014C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
