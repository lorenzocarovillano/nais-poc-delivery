package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-INTR-RIAT<br>
 * Variable: WDTR-INTR-RIAT from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_INTR_RIAT;
    }

    public void setWdtrIntrRiat(AfDecimal wdtrIntrRiat) {
        writeDecimalAsPacked(Pos.WDTR_INTR_RIAT, wdtrIntrRiat.copy());
    }

    /**Original name: WDTR-INTR-RIAT<br>*/
    public AfDecimal getWdtrIntrRiat() {
        return readPackedAsDecimal(Pos.WDTR_INTR_RIAT, Len.Int.WDTR_INTR_RIAT, Len.Fract.WDTR_INTR_RIAT);
    }

    public void setWdtrIntrRiatNull(String wdtrIntrRiatNull) {
        writeString(Pos.WDTR_INTR_RIAT_NULL, wdtrIntrRiatNull, Len.WDTR_INTR_RIAT_NULL);
    }

    /**Original name: WDTR-INTR-RIAT-NULL<br>*/
    public String getWdtrIntrRiatNull() {
        return readString(Pos.WDTR_INTR_RIAT_NULL, Len.WDTR_INTR_RIAT_NULL);
    }

    public String getWdtrIntrRiatNullFormatted() {
        return Functions.padBlanks(getWdtrIntrRiatNull(), Len.WDTR_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_RIAT = 1;
        public static final int WDTR_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_INTR_RIAT = 8;
        public static final int WDTR_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
