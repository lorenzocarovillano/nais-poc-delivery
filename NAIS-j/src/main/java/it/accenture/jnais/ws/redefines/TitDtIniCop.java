package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-DT-INI-COP<br>
 * Variable: TIT-DT-INI-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitDtIniCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitDtIniCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_DT_INI_COP;
    }

    public void setTitDtIniCop(int titDtIniCop) {
        writeIntAsPacked(Pos.TIT_DT_INI_COP, titDtIniCop, Len.Int.TIT_DT_INI_COP);
    }

    public void setTitDtIniCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_DT_INI_COP, Pos.TIT_DT_INI_COP);
    }

    /**Original name: TIT-DT-INI-COP<br>*/
    public int getTitDtIniCop() {
        return readPackedAsInt(Pos.TIT_DT_INI_COP, Len.Int.TIT_DT_INI_COP);
    }

    public byte[] getTitDtIniCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_DT_INI_COP, Pos.TIT_DT_INI_COP);
        return buffer;
    }

    public void setTitDtIniCopNull(String titDtIniCopNull) {
        writeString(Pos.TIT_DT_INI_COP_NULL, titDtIniCopNull, Len.TIT_DT_INI_COP_NULL);
    }

    /**Original name: TIT-DT-INI-COP-NULL<br>*/
    public String getTitDtIniCopNull() {
        return readString(Pos.TIT_DT_INI_COP_NULL, Len.TIT_DT_INI_COP_NULL);
    }

    public String getTitDtIniCopNullFormatted() {
        return Functions.padBlanks(getTitDtIniCopNull(), Len.TIT_DT_INI_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_DT_INI_COP = 1;
        public static final int TIT_DT_INI_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_DT_INI_COP = 5;
        public static final int TIT_DT_INI_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_DT_INI_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
