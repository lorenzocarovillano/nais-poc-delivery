package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P89DtIniCntrlFnd;
import it.accenture.jnais.ws.redefines.P89IdMoviChiu;

/**Original name: D-ATT-SERV-VAL<br>
 * Variable: D-ATT-SERV-VAL from copybook IDBVP891<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DAttServVal extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P89-ID-D-ATT-SERV-VAL
    private int p89IdDAttServVal = DefaultValues.INT_VAL;
    //Original name: P89-COD-COMP-ANIA
    private int p89CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P89-ID-MOVI-CRZ
    private int p89IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P89-ID-MOVI-CHIU
    private P89IdMoviChiu p89IdMoviChiu = new P89IdMoviChiu();
    //Original name: P89-DT-INI-EFF
    private int p89DtIniEff = DefaultValues.INT_VAL;
    //Original name: P89-DT-END-EFF
    private int p89DtEndEff = DefaultValues.INT_VAL;
    //Original name: P89-ID-ATT-SERV-VAL
    private int p89IdAttServVal = DefaultValues.INT_VAL;
    //Original name: P89-TP-SERV-VAL
    private String p89TpServVal = DefaultValues.stringVal(Len.P89_TP_SERV_VAL);
    //Original name: P89-COD-FND
    private String p89CodFnd = DefaultValues.stringVal(Len.P89_COD_FND);
    //Original name: P89-DT-INI-CNTRL-FND
    private P89DtIniCntrlFnd p89DtIniCntrlFnd = new P89DtIniCntrlFnd();
    //Original name: P89-ID-OGG
    private int p89IdOgg = DefaultValues.INT_VAL;
    //Original name: P89-TP-OGG
    private String p89TpOgg = DefaultValues.stringVal(Len.P89_TP_OGG);
    //Original name: P89-DS-RIGA
    private long p89DsRiga = DefaultValues.LONG_VAL;
    //Original name: P89-DS-OPER-SQL
    private char p89DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P89-DS-VER
    private int p89DsVer = DefaultValues.INT_VAL;
    //Original name: P89-DS-TS-INI-CPTZ
    private long p89DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: P89-DS-TS-END-CPTZ
    private long p89DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: P89-DS-UTENTE
    private String p89DsUtente = DefaultValues.stringVal(Len.P89_DS_UTENTE);
    //Original name: P89-DS-STATO-ELAB
    private char p89DsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D_ATT_SERV_VAL;
    }

    @Override
    public void deserialize(byte[] buf) {
        setdAttServValBytes(buf);
    }

    public void setdAttServValFormatted(String data) {
        byte[] buffer = new byte[Len.D_ATT_SERV_VAL];
        MarshalByte.writeString(buffer, 1, data, Len.D_ATT_SERV_VAL);
        setdAttServValBytes(buffer, 1);
    }

    public String getdAttServValFormatted() {
        return MarshalByteExt.bufferToStr(getdAttServValBytes());
    }

    public void setdAttServValBytes(byte[] buffer) {
        setdAttServValBytes(buffer, 1);
    }

    public byte[] getdAttServValBytes() {
        byte[] buffer = new byte[Len.D_ATT_SERV_VAL];
        return getdAttServValBytes(buffer, 1);
    }

    public void setdAttServValBytes(byte[] buffer, int offset) {
        int position = offset;
        p89IdDAttServVal = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_ID_D_ATT_SERV_VAL, 0);
        position += Len.P89_ID_D_ATT_SERV_VAL;
        p89CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_COD_COMP_ANIA, 0);
        position += Len.P89_COD_COMP_ANIA;
        p89IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_ID_MOVI_CRZ, 0);
        position += Len.P89_ID_MOVI_CRZ;
        p89IdMoviChiu.setP89IdMoviChiuFromBuffer(buffer, position);
        position += P89IdMoviChiu.Len.P89_ID_MOVI_CHIU;
        p89DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_DT_INI_EFF, 0);
        position += Len.P89_DT_INI_EFF;
        p89DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_DT_END_EFF, 0);
        position += Len.P89_DT_END_EFF;
        p89IdAttServVal = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_ID_ATT_SERV_VAL, 0);
        position += Len.P89_ID_ATT_SERV_VAL;
        p89TpServVal = MarshalByte.readString(buffer, position, Len.P89_TP_SERV_VAL);
        position += Len.P89_TP_SERV_VAL;
        p89CodFnd = MarshalByte.readString(buffer, position, Len.P89_COD_FND);
        position += Len.P89_COD_FND;
        p89DtIniCntrlFnd.setP89DtIniCntrlFndFromBuffer(buffer, position);
        position += P89DtIniCntrlFnd.Len.P89_DT_INI_CNTRL_FND;
        p89IdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_ID_OGG, 0);
        position += Len.P89_ID_OGG;
        p89TpOgg = MarshalByte.readString(buffer, position, Len.P89_TP_OGG);
        position += Len.P89_TP_OGG;
        p89DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P89_DS_RIGA, 0);
        position += Len.P89_DS_RIGA;
        p89DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p89DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P89_DS_VER, 0);
        position += Len.P89_DS_VER;
        p89DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P89_DS_TS_INI_CPTZ, 0);
        position += Len.P89_DS_TS_INI_CPTZ;
        p89DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P89_DS_TS_END_CPTZ, 0);
        position += Len.P89_DS_TS_END_CPTZ;
        p89DsUtente = MarshalByte.readString(buffer, position, Len.P89_DS_UTENTE);
        position += Len.P89_DS_UTENTE;
        p89DsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getdAttServValBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p89IdDAttServVal, Len.Int.P89_ID_D_ATT_SERV_VAL, 0);
        position += Len.P89_ID_D_ATT_SERV_VAL;
        MarshalByte.writeIntAsPacked(buffer, position, p89CodCompAnia, Len.Int.P89_COD_COMP_ANIA, 0);
        position += Len.P89_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, p89IdMoviCrz, Len.Int.P89_ID_MOVI_CRZ, 0);
        position += Len.P89_ID_MOVI_CRZ;
        p89IdMoviChiu.getP89IdMoviChiuAsBuffer(buffer, position);
        position += P89IdMoviChiu.Len.P89_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, p89DtIniEff, Len.Int.P89_DT_INI_EFF, 0);
        position += Len.P89_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p89DtEndEff, Len.Int.P89_DT_END_EFF, 0);
        position += Len.P89_DT_END_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p89IdAttServVal, Len.Int.P89_ID_ATT_SERV_VAL, 0);
        position += Len.P89_ID_ATT_SERV_VAL;
        MarshalByte.writeString(buffer, position, p89TpServVal, Len.P89_TP_SERV_VAL);
        position += Len.P89_TP_SERV_VAL;
        MarshalByte.writeString(buffer, position, p89CodFnd, Len.P89_COD_FND);
        position += Len.P89_COD_FND;
        p89DtIniCntrlFnd.getP89DtIniCntrlFndAsBuffer(buffer, position);
        position += P89DtIniCntrlFnd.Len.P89_DT_INI_CNTRL_FND;
        MarshalByte.writeIntAsPacked(buffer, position, p89IdOgg, Len.Int.P89_ID_OGG, 0);
        position += Len.P89_ID_OGG;
        MarshalByte.writeString(buffer, position, p89TpOgg, Len.P89_TP_OGG);
        position += Len.P89_TP_OGG;
        MarshalByte.writeLongAsPacked(buffer, position, p89DsRiga, Len.Int.P89_DS_RIGA, 0);
        position += Len.P89_DS_RIGA;
        MarshalByte.writeChar(buffer, position, p89DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p89DsVer, Len.Int.P89_DS_VER, 0);
        position += Len.P89_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p89DsTsIniCptz, Len.Int.P89_DS_TS_INI_CPTZ, 0);
        position += Len.P89_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, p89DsTsEndCptz, Len.Int.P89_DS_TS_END_CPTZ, 0);
        position += Len.P89_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, p89DsUtente, Len.P89_DS_UTENTE);
        position += Len.P89_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p89DsStatoElab);
        return buffer;
    }

    public void setP89IdDAttServVal(int p89IdDAttServVal) {
        this.p89IdDAttServVal = p89IdDAttServVal;
    }

    public int getP89IdDAttServVal() {
        return this.p89IdDAttServVal;
    }

    public void setP89CodCompAnia(int p89CodCompAnia) {
        this.p89CodCompAnia = p89CodCompAnia;
    }

    public int getP89CodCompAnia() {
        return this.p89CodCompAnia;
    }

    public void setP89IdMoviCrz(int p89IdMoviCrz) {
        this.p89IdMoviCrz = p89IdMoviCrz;
    }

    public int getP89IdMoviCrz() {
        return this.p89IdMoviCrz;
    }

    public void setP89DtIniEff(int p89DtIniEff) {
        this.p89DtIniEff = p89DtIniEff;
    }

    public int getP89DtIniEff() {
        return this.p89DtIniEff;
    }

    public void setP89DtEndEff(int p89DtEndEff) {
        this.p89DtEndEff = p89DtEndEff;
    }

    public int getP89DtEndEff() {
        return this.p89DtEndEff;
    }

    public void setP89IdAttServVal(int p89IdAttServVal) {
        this.p89IdAttServVal = p89IdAttServVal;
    }

    public int getP89IdAttServVal() {
        return this.p89IdAttServVal;
    }

    public void setP89TpServVal(String p89TpServVal) {
        this.p89TpServVal = Functions.subString(p89TpServVal, Len.P89_TP_SERV_VAL);
    }

    public String getP89TpServVal() {
        return this.p89TpServVal;
    }

    public void setP89CodFnd(String p89CodFnd) {
        this.p89CodFnd = Functions.subString(p89CodFnd, Len.P89_COD_FND);
    }

    public String getP89CodFnd() {
        return this.p89CodFnd;
    }

    public void setP89IdOgg(int p89IdOgg) {
        this.p89IdOgg = p89IdOgg;
    }

    public int getP89IdOgg() {
        return this.p89IdOgg;
    }

    public void setP89TpOgg(String p89TpOgg) {
        this.p89TpOgg = Functions.subString(p89TpOgg, Len.P89_TP_OGG);
    }

    public String getP89TpOgg() {
        return this.p89TpOgg;
    }

    public void setP89DsRiga(long p89DsRiga) {
        this.p89DsRiga = p89DsRiga;
    }

    public long getP89DsRiga() {
        return this.p89DsRiga;
    }

    public void setP89DsOperSql(char p89DsOperSql) {
        this.p89DsOperSql = p89DsOperSql;
    }

    public void setP89DsOperSqlFormatted(String p89DsOperSql) {
        setP89DsOperSql(Functions.charAt(p89DsOperSql, Types.CHAR_SIZE));
    }

    public char getP89DsOperSql() {
        return this.p89DsOperSql;
    }

    public void setP89DsVer(int p89DsVer) {
        this.p89DsVer = p89DsVer;
    }

    public int getP89DsVer() {
        return this.p89DsVer;
    }

    public void setP89DsTsIniCptz(long p89DsTsIniCptz) {
        this.p89DsTsIniCptz = p89DsTsIniCptz;
    }

    public long getP89DsTsIniCptz() {
        return this.p89DsTsIniCptz;
    }

    public void setP89DsTsEndCptz(long p89DsTsEndCptz) {
        this.p89DsTsEndCptz = p89DsTsEndCptz;
    }

    public long getP89DsTsEndCptz() {
        return this.p89DsTsEndCptz;
    }

    public void setP89DsUtente(String p89DsUtente) {
        this.p89DsUtente = Functions.subString(p89DsUtente, Len.P89_DS_UTENTE);
    }

    public String getP89DsUtente() {
        return this.p89DsUtente;
    }

    public void setP89DsStatoElab(char p89DsStatoElab) {
        this.p89DsStatoElab = p89DsStatoElab;
    }

    public void setP89DsStatoElabFormatted(String p89DsStatoElab) {
        setP89DsStatoElab(Functions.charAt(p89DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP89DsStatoElab() {
        return this.p89DsStatoElab;
    }

    public P89DtIniCntrlFnd getP89DtIniCntrlFnd() {
        return p89DtIniCntrlFnd;
    }

    public P89IdMoviChiu getP89IdMoviChiu() {
        return p89IdMoviChiu;
    }

    @Override
    public byte[] serialize() {
        return getdAttServValBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P89_ID_D_ATT_SERV_VAL = 5;
        public static final int P89_COD_COMP_ANIA = 3;
        public static final int P89_ID_MOVI_CRZ = 5;
        public static final int P89_DT_INI_EFF = 5;
        public static final int P89_DT_END_EFF = 5;
        public static final int P89_ID_ATT_SERV_VAL = 5;
        public static final int P89_TP_SERV_VAL = 2;
        public static final int P89_COD_FND = 12;
        public static final int P89_ID_OGG = 5;
        public static final int P89_TP_OGG = 2;
        public static final int P89_DS_RIGA = 6;
        public static final int P89_DS_OPER_SQL = 1;
        public static final int P89_DS_VER = 5;
        public static final int P89_DS_TS_INI_CPTZ = 10;
        public static final int P89_DS_TS_END_CPTZ = 10;
        public static final int P89_DS_UTENTE = 20;
        public static final int P89_DS_STATO_ELAB = 1;
        public static final int D_ATT_SERV_VAL = P89_ID_D_ATT_SERV_VAL + P89_COD_COMP_ANIA + P89_ID_MOVI_CRZ + P89IdMoviChiu.Len.P89_ID_MOVI_CHIU + P89_DT_INI_EFF + P89_DT_END_EFF + P89_ID_ATT_SERV_VAL + P89_TP_SERV_VAL + P89_COD_FND + P89DtIniCntrlFnd.Len.P89_DT_INI_CNTRL_FND + P89_ID_OGG + P89_TP_OGG + P89_DS_RIGA + P89_DS_OPER_SQL + P89_DS_VER + P89_DS_TS_INI_CPTZ + P89_DS_TS_END_CPTZ + P89_DS_UTENTE + P89_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P89_ID_D_ATT_SERV_VAL = 9;
            public static final int P89_COD_COMP_ANIA = 5;
            public static final int P89_ID_MOVI_CRZ = 9;
            public static final int P89_DT_INI_EFF = 8;
            public static final int P89_DT_END_EFF = 8;
            public static final int P89_ID_ATT_SERV_VAL = 9;
            public static final int P89_ID_OGG = 9;
            public static final int P89_DS_RIGA = 10;
            public static final int P89_DS_VER = 9;
            public static final int P89_DS_TS_INI_CPTZ = 18;
            public static final int P89_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
