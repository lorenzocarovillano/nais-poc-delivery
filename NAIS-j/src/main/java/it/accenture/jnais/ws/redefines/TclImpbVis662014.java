package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPB-VIS-662014<br>
 * Variable: TCL-IMPB-VIS-662014 from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpbVis662014 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpbVis662014() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPB_VIS662014;
    }

    public void setTclImpbVis662014(AfDecimal tclImpbVis662014) {
        writeDecimalAsPacked(Pos.TCL_IMPB_VIS662014, tclImpbVis662014.copy());
    }

    public void setTclImpbVis662014FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPB_VIS662014, Pos.TCL_IMPB_VIS662014);
    }

    /**Original name: TCL-IMPB-VIS-662014<br>*/
    public AfDecimal getTclImpbVis662014() {
        return readPackedAsDecimal(Pos.TCL_IMPB_VIS662014, Len.Int.TCL_IMPB_VIS662014, Len.Fract.TCL_IMPB_VIS662014);
    }

    public byte[] getTclImpbVis662014AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPB_VIS662014, Pos.TCL_IMPB_VIS662014);
        return buffer;
    }

    public void setTclImpbVis662014Null(String tclImpbVis662014Null) {
        writeString(Pos.TCL_IMPB_VIS662014_NULL, tclImpbVis662014Null, Len.TCL_IMPB_VIS662014_NULL);
    }

    /**Original name: TCL-IMPB-VIS-662014-NULL<br>*/
    public String getTclImpbVis662014Null() {
        return readString(Pos.TCL_IMPB_VIS662014_NULL, Len.TCL_IMPB_VIS662014_NULL);
    }

    public String getTclImpbVis662014NullFormatted() {
        return Functions.padBlanks(getTclImpbVis662014Null(), Len.TCL_IMPB_VIS662014_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_VIS662014 = 1;
        public static final int TCL_IMPB_VIS662014_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPB_VIS662014 = 8;
        public static final int TCL_IMPB_VIS662014_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_VIS662014 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPB_VIS662014 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
