package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-QUIE-I<br>
 * Variable: PCO-DT-ULT-BOLL-QUIE-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollQuieI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollQuieI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_QUIE_I;
    }

    public void setPcoDtUltBollQuieI(int pcoDtUltBollQuieI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_QUIE_I, pcoDtUltBollQuieI, Len.Int.PCO_DT_ULT_BOLL_QUIE_I);
    }

    public void setPcoDtUltBollQuieIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_QUIE_I, Pos.PCO_DT_ULT_BOLL_QUIE_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-QUIE-I<br>*/
    public int getPcoDtUltBollQuieI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_QUIE_I, Len.Int.PCO_DT_ULT_BOLL_QUIE_I);
    }

    public byte[] getPcoDtUltBollQuieIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_QUIE_I, Pos.PCO_DT_ULT_BOLL_QUIE_I);
        return buffer;
    }

    public void initPcoDtUltBollQuieIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_QUIE_I, Len.PCO_DT_ULT_BOLL_QUIE_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollQuieINull(String pcoDtUltBollQuieINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_QUIE_I_NULL, pcoDtUltBollQuieINull, Len.PCO_DT_ULT_BOLL_QUIE_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-QUIE-I-NULL<br>*/
    public String getPcoDtUltBollQuieINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_QUIE_I_NULL, Len.PCO_DT_ULT_BOLL_QUIE_I_NULL);
    }

    public String getPcoDtUltBollQuieINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollQuieINull(), Len.PCO_DT_ULT_BOLL_QUIE_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_QUIE_I = 1;
        public static final int PCO_DT_ULT_BOLL_QUIE_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_QUIE_I = 5;
        public static final int PCO_DT_ULT_BOLL_QUIE_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_QUIE_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
