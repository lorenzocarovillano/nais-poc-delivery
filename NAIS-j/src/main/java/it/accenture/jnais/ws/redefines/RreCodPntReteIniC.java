package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RRE-COD-PNT-RETE-INI-C<br>
 * Variable: RRE-COD-PNT-RETE-INI-C from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RreCodPntReteIniC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RreCodPntReteIniC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RRE_COD_PNT_RETE_INI_C;
    }

    public void setRreCodPntReteIniC(long rreCodPntReteIniC) {
        writeLongAsPacked(Pos.RRE_COD_PNT_RETE_INI_C, rreCodPntReteIniC, Len.Int.RRE_COD_PNT_RETE_INI_C);
    }

    public void setRreCodPntReteIniCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RRE_COD_PNT_RETE_INI_C, Pos.RRE_COD_PNT_RETE_INI_C);
    }

    /**Original name: RRE-COD-PNT-RETE-INI-C<br>*/
    public long getRreCodPntReteIniC() {
        return readPackedAsLong(Pos.RRE_COD_PNT_RETE_INI_C, Len.Int.RRE_COD_PNT_RETE_INI_C);
    }

    public byte[] getRreCodPntReteIniCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RRE_COD_PNT_RETE_INI_C, Pos.RRE_COD_PNT_RETE_INI_C);
        return buffer;
    }

    public void setRreCodPntReteIniCNull(String rreCodPntReteIniCNull) {
        writeString(Pos.RRE_COD_PNT_RETE_INI_C_NULL, rreCodPntReteIniCNull, Len.RRE_COD_PNT_RETE_INI_C_NULL);
    }

    /**Original name: RRE-COD-PNT-RETE-INI-C-NULL<br>*/
    public String getRreCodPntReteIniCNull() {
        return readString(Pos.RRE_COD_PNT_RETE_INI_C_NULL, Len.RRE_COD_PNT_RETE_INI_C_NULL);
    }

    public String getRreCodPntReteIniCNullFormatted() {
        return Functions.padBlanks(getRreCodPntReteIniCNull(), Len.RRE_COD_PNT_RETE_INI_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RRE_COD_PNT_RETE_INI_C = 1;
        public static final int RRE_COD_PNT_RETE_INI_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RRE_COD_PNT_RETE_INI_C = 6;
        public static final int RRE_COD_PNT_RETE_INI_C_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RRE_COD_PNT_RETE_INI_C = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
