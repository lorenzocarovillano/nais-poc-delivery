package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.occurs.Wpers;
import it.accenture.jnais.ws.occurs.WrappTabRappAnag;

/**Original name: WK-RANPERS<br>
 * Variable: WK-RANPERS from program LLBS0230<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkRanpers {

    //==== PROPERTIES ====
    public static final int WRAPP_TAB_RAPP_ANAG_MAXOCCURS = 100;
    public static final int WPERS_MAXOCCURS = 100;
    //Original name: IX-WRAN
    private int ixWran = DefaultValues.INT_VAL;
    //Original name: IX-WRAN-MAX
    private String ixWranMax = DefaultValues.stringVal(Len.IX_WRAN_MAX);
    //Original name: WRAPP-TAB-RAPP-ANAG
    private WrappTabRappAnag[] wrappTabRappAnag = new WrappTabRappAnag[WRAPP_TAB_RAPP_ANAG_MAXOCCURS];
    //Original name: WPERS
    private Wpers[] wpers = new Wpers[WPERS_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WkRanpers() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wrappTabRappAnagIdx = 1; wrappTabRappAnagIdx <= WRAPP_TAB_RAPP_ANAG_MAXOCCURS; wrappTabRappAnagIdx++) {
            wrappTabRappAnag[wrappTabRappAnagIdx - 1] = new WrappTabRappAnag();
        }
        for (int wpersIdx = 1; wpersIdx <= WPERS_MAXOCCURS; wpersIdx++) {
            wpers[wpersIdx - 1] = new Wpers();
        }
    }

    public void setIxWran(int ixWran) {
        this.ixWran = ixWran;
    }

    public int getIxWran() {
        return this.ixWran;
    }

    public void setIxWranMax(int ixWranMax) {
        this.ixWranMax = NumericDisplay.asString(ixWranMax, Len.IX_WRAN_MAX);
    }

    public void setIxWranMaxFormatted(String ixWranMax) {
        this.ixWranMax = Trunc.toUnsignedNumeric(ixWranMax, Len.IX_WRAN_MAX);
    }

    public int getIxWranMax() {
        return NumericDisplay.asInt(this.ixWranMax);
    }

    public Wpers getWpers(int idx) {
        return wpers[idx - 1];
    }

    public WrappTabRappAnag getWrappTabRappAnag(int idx) {
        return wrappTabRappAnag[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IX_WRAN_MAX = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
