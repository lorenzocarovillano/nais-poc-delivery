package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WDFA-MM-CNBZ-K3<br>
 * Variable: WDFA-MM-CNBZ-K3 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaMmCnbzK3 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaMmCnbzK3() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_MM_CNBZ_K3;
    }

    public void setWdfaMmCnbzK3(short wdfaMmCnbzK3) {
        writeShortAsPacked(Pos.WDFA_MM_CNBZ_K3, wdfaMmCnbzK3, Len.Int.WDFA_MM_CNBZ_K3);
    }

    public void setWdfaMmCnbzK3FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_MM_CNBZ_K3, Pos.WDFA_MM_CNBZ_K3);
    }

    /**Original name: WDFA-MM-CNBZ-K3<br>*/
    public short getWdfaMmCnbzK3() {
        return readPackedAsShort(Pos.WDFA_MM_CNBZ_K3, Len.Int.WDFA_MM_CNBZ_K3);
    }

    public byte[] getWdfaMmCnbzK3AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_MM_CNBZ_K3, Pos.WDFA_MM_CNBZ_K3);
        return buffer;
    }

    public void setWdfaMmCnbzK3Null(String wdfaMmCnbzK3Null) {
        writeString(Pos.WDFA_MM_CNBZ_K3_NULL, wdfaMmCnbzK3Null, Len.WDFA_MM_CNBZ_K3_NULL);
    }

    /**Original name: WDFA-MM-CNBZ-K3-NULL<br>*/
    public String getWdfaMmCnbzK3Null() {
        return readString(Pos.WDFA_MM_CNBZ_K3_NULL, Len.WDFA_MM_CNBZ_K3_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_MM_CNBZ_K3 = 1;
        public static final int WDFA_MM_CNBZ_K3_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_MM_CNBZ_K3 = 3;
        public static final int WDFA_MM_CNBZ_K3_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_MM_CNBZ_K3 = 4;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
