package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-ACCPRE-SOST-DFZ<br>
 * Variable: WDFL-ACCPRE-SOST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflAccpreSostDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflAccpreSostDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_ACCPRE_SOST_DFZ;
    }

    public void setWdflAccpreSostDfz(AfDecimal wdflAccpreSostDfz) {
        writeDecimalAsPacked(Pos.WDFL_ACCPRE_SOST_DFZ, wdflAccpreSostDfz.copy());
    }

    public void setWdflAccpreSostDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_ACCPRE_SOST_DFZ, Pos.WDFL_ACCPRE_SOST_DFZ);
    }

    /**Original name: WDFL-ACCPRE-SOST-DFZ<br>*/
    public AfDecimal getWdflAccpreSostDfz() {
        return readPackedAsDecimal(Pos.WDFL_ACCPRE_SOST_DFZ, Len.Int.WDFL_ACCPRE_SOST_DFZ, Len.Fract.WDFL_ACCPRE_SOST_DFZ);
    }

    public byte[] getWdflAccpreSostDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_ACCPRE_SOST_DFZ, Pos.WDFL_ACCPRE_SOST_DFZ);
        return buffer;
    }

    public void setWdflAccpreSostDfzNull(String wdflAccpreSostDfzNull) {
        writeString(Pos.WDFL_ACCPRE_SOST_DFZ_NULL, wdflAccpreSostDfzNull, Len.WDFL_ACCPRE_SOST_DFZ_NULL);
    }

    /**Original name: WDFL-ACCPRE-SOST-DFZ-NULL<br>*/
    public String getWdflAccpreSostDfzNull() {
        return readString(Pos.WDFL_ACCPRE_SOST_DFZ_NULL, Len.WDFL_ACCPRE_SOST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_SOST_DFZ = 1;
        public static final int WDFL_ACCPRE_SOST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_ACCPRE_SOST_DFZ = 8;
        public static final int WDFL_ACCPRE_SOST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_SOST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_ACCPRE_SOST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
