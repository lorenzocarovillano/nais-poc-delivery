package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-BOLLO-DETT-C<br>
 * Variable: DFL-IMPST-BOLLO-DETT-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstBolloDettC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstBolloDettC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_BOLLO_DETT_C;
    }

    public void setDflImpstBolloDettC(AfDecimal dflImpstBolloDettC) {
        writeDecimalAsPacked(Pos.DFL_IMPST_BOLLO_DETT_C, dflImpstBolloDettC.copy());
    }

    public void setDflImpstBolloDettCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_BOLLO_DETT_C, Pos.DFL_IMPST_BOLLO_DETT_C);
    }

    /**Original name: DFL-IMPST-BOLLO-DETT-C<br>*/
    public AfDecimal getDflImpstBolloDettC() {
        return readPackedAsDecimal(Pos.DFL_IMPST_BOLLO_DETT_C, Len.Int.DFL_IMPST_BOLLO_DETT_C, Len.Fract.DFL_IMPST_BOLLO_DETT_C);
    }

    public byte[] getDflImpstBolloDettCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_BOLLO_DETT_C, Pos.DFL_IMPST_BOLLO_DETT_C);
        return buffer;
    }

    public void setDflImpstBolloDettCNull(String dflImpstBolloDettCNull) {
        writeString(Pos.DFL_IMPST_BOLLO_DETT_C_NULL, dflImpstBolloDettCNull, Len.DFL_IMPST_BOLLO_DETT_C_NULL);
    }

    /**Original name: DFL-IMPST-BOLLO-DETT-C-NULL<br>*/
    public String getDflImpstBolloDettCNull() {
        return readString(Pos.DFL_IMPST_BOLLO_DETT_C_NULL, Len.DFL_IMPST_BOLLO_DETT_C_NULL);
    }

    public String getDflImpstBolloDettCNullFormatted() {
        return Functions.padBlanks(getDflImpstBolloDettCNull(), Len.DFL_IMPST_BOLLO_DETT_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_DETT_C = 1;
        public static final int DFL_IMPST_BOLLO_DETT_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_BOLLO_DETT_C = 8;
        public static final int DFL_IMPST_BOLLO_DETT_C_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_DETT_C = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_BOLLO_DETT_C = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
