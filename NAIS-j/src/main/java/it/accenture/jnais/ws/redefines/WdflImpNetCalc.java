package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-NET-CALC<br>
 * Variable: WDFL-IMP-NET-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpNetCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpNetCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_NET_CALC;
    }

    public void setWdflImpNetCalc(AfDecimal wdflImpNetCalc) {
        writeDecimalAsPacked(Pos.WDFL_IMP_NET_CALC, wdflImpNetCalc.copy());
    }

    public void setWdflImpNetCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_NET_CALC, Pos.WDFL_IMP_NET_CALC);
    }

    /**Original name: WDFL-IMP-NET-CALC<br>*/
    public AfDecimal getWdflImpNetCalc() {
        return readPackedAsDecimal(Pos.WDFL_IMP_NET_CALC, Len.Int.WDFL_IMP_NET_CALC, Len.Fract.WDFL_IMP_NET_CALC);
    }

    public byte[] getWdflImpNetCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_NET_CALC, Pos.WDFL_IMP_NET_CALC);
        return buffer;
    }

    public void setWdflImpNetCalcNull(String wdflImpNetCalcNull) {
        writeString(Pos.WDFL_IMP_NET_CALC_NULL, wdflImpNetCalcNull, Len.WDFL_IMP_NET_CALC_NULL);
    }

    /**Original name: WDFL-IMP-NET-CALC-NULL<br>*/
    public String getWdflImpNetCalcNull() {
        return readString(Pos.WDFL_IMP_NET_CALC_NULL, Len.WDFL_IMP_NET_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_NET_CALC = 1;
        public static final int WDFL_IMP_NET_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_NET_CALC = 8;
        public static final int WDFL_IMP_NET_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_NET_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_NET_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
