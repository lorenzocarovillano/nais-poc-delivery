package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WRIF-IMP-MOVTO<br>
 * Variable: WRIF-IMP-MOVTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrifImpMovto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrifImpMovto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRIF_IMP_MOVTO;
    }

    public void setWrifImpMovto(AfDecimal wrifImpMovto) {
        writeDecimalAsPacked(Pos.WRIF_IMP_MOVTO, wrifImpMovto.copy());
    }

    public void setWrifImpMovtoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRIF_IMP_MOVTO, Pos.WRIF_IMP_MOVTO);
    }

    /**Original name: WRIF-IMP-MOVTO<br>*/
    public AfDecimal getWrifImpMovto() {
        return readPackedAsDecimal(Pos.WRIF_IMP_MOVTO, Len.Int.WRIF_IMP_MOVTO, Len.Fract.WRIF_IMP_MOVTO);
    }

    public byte[] getWrifImpMovtoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRIF_IMP_MOVTO, Pos.WRIF_IMP_MOVTO);
        return buffer;
    }

    public void initWrifImpMovtoSpaces() {
        fill(Pos.WRIF_IMP_MOVTO, Len.WRIF_IMP_MOVTO, Types.SPACE_CHAR);
    }

    public void setWrifImpMovtoNull(String wrifImpMovtoNull) {
        writeString(Pos.WRIF_IMP_MOVTO_NULL, wrifImpMovtoNull, Len.WRIF_IMP_MOVTO_NULL);
    }

    /**Original name: WRIF-IMP-MOVTO-NULL<br>*/
    public String getWrifImpMovtoNull() {
        return readString(Pos.WRIF_IMP_MOVTO_NULL, Len.WRIF_IMP_MOVTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRIF_IMP_MOVTO = 1;
        public static final int WRIF_IMP_MOVTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRIF_IMP_MOVTO = 8;
        public static final int WRIF_IMP_MOVTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WRIF_IMP_MOVTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WRIF_IMP_MOVTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
