package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B01-VAL-QUO-INI<br>
 * Variable: W-B01-VAL-QUO-INI from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB01ValQuoIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB01ValQuoIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B01_VAL_QUO_INI;
    }

    public void setwB01ValQuoIni(AfDecimal wB01ValQuoIni) {
        writeDecimalAsPacked(Pos.W_B01_VAL_QUO_INI, wB01ValQuoIni.copy());
    }

    /**Original name: W-B01-VAL-QUO-INI<br>*/
    public AfDecimal getwB01ValQuoIni() {
        return readPackedAsDecimal(Pos.W_B01_VAL_QUO_INI, Len.Int.W_B01_VAL_QUO_INI, Len.Fract.W_B01_VAL_QUO_INI);
    }

    public byte[] getwB01ValQuoIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B01_VAL_QUO_INI, Pos.W_B01_VAL_QUO_INI);
        return buffer;
    }

    public void setwB01ValQuoIniNull(String wB01ValQuoIniNull) {
        writeString(Pos.W_B01_VAL_QUO_INI_NULL, wB01ValQuoIniNull, Len.W_B01_VAL_QUO_INI_NULL);
    }

    /**Original name: W-B01-VAL-QUO-INI-NULL<br>*/
    public String getwB01ValQuoIniNull() {
        return readString(Pos.W_B01_VAL_QUO_INI_NULL, Len.W_B01_VAL_QUO_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B01_VAL_QUO_INI = 1;
        public static final int W_B01_VAL_QUO_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B01_VAL_QUO_INI = 7;
        public static final int W_B01_VAL_QUO_INI_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B01_VAL_QUO_INI = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B01_VAL_QUO_INI = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
