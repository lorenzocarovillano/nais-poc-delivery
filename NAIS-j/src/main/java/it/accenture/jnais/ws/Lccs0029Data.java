package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Idsv0002;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0029<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0029Data {

    //==== PROPERTIES ====
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *  COSTANTI E MODULI CHIAMATI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LOAS0800";
    //Original name: WK-LABEL-ERR
    private String wkLabelErr = "";
    //Original name: WK-APPO-DT
    private WkAppoDt wkAppoDt = new WkAppoDt();
    //Original name: IX-CICLO
    private short ixCiclo = ((short)0);
    /**Original name: PARAM<br>
	 * <pre>----------------------------------------------------------------*
	 *  AREA LCCS0004
	 * ----------------------------------------------------------------*</pre>*/
    private String param2 = DefaultValues.stringVal(Len.PARAM2);
    //Original name: X-DATA
    private XDataLccs0029 xData = new XDataLccs0029();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public void setIxCiclo(short ixCiclo) {
        this.ixCiclo = ixCiclo;
    }

    public short getIxCiclo() {
        return this.ixCiclo;
    }

    public void setParam2(short param2) {
        this.param2 = NumericDisplay.asString(param2, Len.PARAM2);
    }

    public void setParam2FromBuffer(byte[] buffer) {
        param2 = MarshalByte.readFixedString(buffer, 1, Len.PARAM2);
    }

    public short getParam2() {
        return NumericDisplay.asShort(this.param2);
    }

    public String getParam2Formatted() {
        return this.param2;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public WkAppoDt getWkAppoDt() {
        return wkAppoDt;
    }

    public XDataLccs0029 getxData() {
        return xData;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IX_CICLO = 1;
        public static final int PARAM2 = 1;
        public static final int WK_LABEL_ERR = 30;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
