package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-TP-TRCH<br>
 * Variable: FLAG-TP-TRCH from program LCCS0450<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagTpTrch {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char POSITIVA = 'S';
    public static final char NEGATIVA = 'N';

    //==== METHODS ====
    public void setFlagTpTrch(char flagTpTrch) {
        this.value = flagTpTrch;
    }

    public char getFlagTpTrch() {
        return this.value;
    }

    public boolean isPositiva() {
        return value == POSITIVA;
    }

    public void setPositiva() {
        value = POSITIVA;
    }

    public boolean isNegativa() {
        return value == NEGATIVA;
    }

    public void setNegativa() {
        value = NEGATIVA;
    }
}
