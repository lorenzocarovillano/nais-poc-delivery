package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-CLOSE-FILESQS-ALL<br>
 * Variable: FLAG-CLOSE-FILESQS-ALL from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagCloseFilesqsAll {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagCloseFilesqsAll(char flagCloseFilesqsAll) {
        this.value = flagCloseFilesqsAll;
    }

    public char getFlagCloseFilesqsAll() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }
}
