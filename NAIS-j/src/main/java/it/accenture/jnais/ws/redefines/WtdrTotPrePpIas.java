package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-PRE-PP-IAS<br>
 * Variable: WTDR-TOT-PRE-PP-IAS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotPrePpIas extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotPrePpIas() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_PRE_PP_IAS;
    }

    public void setWtdrTotPrePpIas(AfDecimal wtdrTotPrePpIas) {
        writeDecimalAsPacked(Pos.WTDR_TOT_PRE_PP_IAS, wtdrTotPrePpIas.copy());
    }

    /**Original name: WTDR-TOT-PRE-PP-IAS<br>*/
    public AfDecimal getWtdrTotPrePpIas() {
        return readPackedAsDecimal(Pos.WTDR_TOT_PRE_PP_IAS, Len.Int.WTDR_TOT_PRE_PP_IAS, Len.Fract.WTDR_TOT_PRE_PP_IAS);
    }

    public void setWtdrTotPrePpIasNull(String wtdrTotPrePpIasNull) {
        writeString(Pos.WTDR_TOT_PRE_PP_IAS_NULL, wtdrTotPrePpIasNull, Len.WTDR_TOT_PRE_PP_IAS_NULL);
    }

    /**Original name: WTDR-TOT-PRE-PP-IAS-NULL<br>*/
    public String getWtdrTotPrePpIasNull() {
        return readString(Pos.WTDR_TOT_PRE_PP_IAS_NULL, Len.WTDR_TOT_PRE_PP_IAS_NULL);
    }

    public String getWtdrTotPrePpIasNullFormatted() {
        return Functions.padBlanks(getWtdrTotPrePpIasNull(), Len.WTDR_TOT_PRE_PP_IAS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_PP_IAS = 1;
        public static final int WTDR_TOT_PRE_PP_IAS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_PRE_PP_IAS = 8;
        public static final int WTDR_TOT_PRE_PP_IAS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_PP_IAS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_PRE_PP_IAS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
