package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-RIVAL-IN<br>
 * Variable: WPCO-DT-ULT-RIVAL-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltRivalIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltRivalIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_RIVAL_IN;
    }

    public void setWpcoDtUltRivalIn(int wpcoDtUltRivalIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_RIVAL_IN, wpcoDtUltRivalIn, Len.Int.WPCO_DT_ULT_RIVAL_IN);
    }

    public void setDpcoDtUltRivalInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_RIVAL_IN, Pos.WPCO_DT_ULT_RIVAL_IN);
    }

    /**Original name: WPCO-DT-ULT-RIVAL-IN<br>*/
    public int getWpcoDtUltRivalIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_RIVAL_IN, Len.Int.WPCO_DT_ULT_RIVAL_IN);
    }

    public byte[] getWpcoDtUltRivalInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_RIVAL_IN, Pos.WPCO_DT_ULT_RIVAL_IN);
        return buffer;
    }

    public void setWpcoDtUltRivalInNull(String wpcoDtUltRivalInNull) {
        writeString(Pos.WPCO_DT_ULT_RIVAL_IN_NULL, wpcoDtUltRivalInNull, Len.WPCO_DT_ULT_RIVAL_IN_NULL);
    }

    /**Original name: WPCO-DT-ULT-RIVAL-IN-NULL<br>*/
    public String getWpcoDtUltRivalInNull() {
        return readString(Pos.WPCO_DT_ULT_RIVAL_IN_NULL, Len.WPCO_DT_ULT_RIVAL_IN_NULL);
    }

    public String getWpcoDtUltRivalInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltRivalInNull(), Len.WPCO_DT_ULT_RIVAL_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RIVAL_IN = 1;
        public static final int WPCO_DT_ULT_RIVAL_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_RIVAL_IN = 5;
        public static final int WPCO_DT_ULT_RIVAL_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_RIVAL_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
