package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-VIS-END2000-NFORZ<br>
 * Variable: WTGA-VIS-END2000-NFORZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaVisEnd2000Nforz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaVisEnd2000Nforz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_VIS_END2000_NFORZ;
    }

    public void setWtgaVisEnd2000Nforz(AfDecimal wtgaVisEnd2000Nforz) {
        writeDecimalAsPacked(Pos.WTGA_VIS_END2000_NFORZ, wtgaVisEnd2000Nforz.copy());
    }

    public void setWtgaVisEnd2000NforzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_VIS_END2000_NFORZ, Pos.WTGA_VIS_END2000_NFORZ);
    }

    /**Original name: WTGA-VIS-END2000-NFORZ<br>*/
    public AfDecimal getWtgaVisEnd2000Nforz() {
        return readPackedAsDecimal(Pos.WTGA_VIS_END2000_NFORZ, Len.Int.WTGA_VIS_END2000_NFORZ, Len.Fract.WTGA_VIS_END2000_NFORZ);
    }

    public byte[] getWtgaVisEnd2000NforzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_VIS_END2000_NFORZ, Pos.WTGA_VIS_END2000_NFORZ);
        return buffer;
    }

    public void initWtgaVisEnd2000NforzSpaces() {
        fill(Pos.WTGA_VIS_END2000_NFORZ, Len.WTGA_VIS_END2000_NFORZ, Types.SPACE_CHAR);
    }

    public void setWtgaVisEnd2000NforzNull(String wtgaVisEnd2000NforzNull) {
        writeString(Pos.WTGA_VIS_END2000_NFORZ_NULL, wtgaVisEnd2000NforzNull, Len.WTGA_VIS_END2000_NFORZ_NULL);
    }

    /**Original name: WTGA-VIS-END2000-NFORZ-NULL<br>*/
    public String getWtgaVisEnd2000NforzNull() {
        return readString(Pos.WTGA_VIS_END2000_NFORZ_NULL, Len.WTGA_VIS_END2000_NFORZ_NULL);
    }

    public String getWtgaVisEnd2000NforzNullFormatted() {
        return Functions.padBlanks(getWtgaVisEnd2000NforzNull(), Len.WTGA_VIS_END2000_NFORZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_VIS_END2000_NFORZ = 1;
        public static final int WTGA_VIS_END2000_NFORZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_VIS_END2000_NFORZ = 8;
        public static final int WTGA_VIS_END2000_NFORZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_VIS_END2000_NFORZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_VIS_END2000_NFORZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
