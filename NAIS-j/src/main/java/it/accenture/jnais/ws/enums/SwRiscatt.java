package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-RISCATT<br>
 * Variable: SW-RISCATT from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwRiscatt {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char OK = 'S';
    public static final char KO = 'N';

    //==== METHODS ====
    public void setSwRiscatt(char swRiscatt) {
        this.value = swRiscatt;
    }

    public char getSwRiscatt() {
        return this.value;
    }

    public boolean isOk() {
        return value == OK;
    }

    public void setOk() {
        value = OK;
    }

    public void setKo() {
        value = KO;
    }
}
