package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-NET-DFZ<br>
 * Variable: WDFL-IMP-NET-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpNetDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpNetDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_NET_DFZ;
    }

    public void setWdflImpNetDfz(AfDecimal wdflImpNetDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMP_NET_DFZ, wdflImpNetDfz.copy());
    }

    public void setWdflImpNetDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_NET_DFZ, Pos.WDFL_IMP_NET_DFZ);
    }

    /**Original name: WDFL-IMP-NET-DFZ<br>*/
    public AfDecimal getWdflImpNetDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMP_NET_DFZ, Len.Int.WDFL_IMP_NET_DFZ, Len.Fract.WDFL_IMP_NET_DFZ);
    }

    public byte[] getWdflImpNetDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_NET_DFZ, Pos.WDFL_IMP_NET_DFZ);
        return buffer;
    }

    public void setWdflImpNetDfzNull(String wdflImpNetDfzNull) {
        writeString(Pos.WDFL_IMP_NET_DFZ_NULL, wdflImpNetDfzNull, Len.WDFL_IMP_NET_DFZ_NULL);
    }

    /**Original name: WDFL-IMP-NET-DFZ-NULL<br>*/
    public String getWdflImpNetDfzNull() {
        return readString(Pos.WDFL_IMP_NET_DFZ_NULL, Len.WDFL_IMP_NET_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_NET_DFZ = 1;
        public static final int WDFL_IMP_NET_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_NET_DFZ = 8;
        public static final int WDFL_IMP_NET_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_NET_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_NET_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
