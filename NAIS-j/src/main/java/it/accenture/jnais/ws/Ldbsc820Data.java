package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.AdesDb;
import it.accenture.jnais.copy.AdesIvvs0216;
import it.accenture.jnais.copy.Idbvstb3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndAdes;
import it.accenture.jnais.copy.IndDettTitCont;
import it.accenture.jnais.copy.Poli;
import it.accenture.jnais.copy.StatOggBus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBSC820<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbsc820Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-ADES
    private IndAdes indAdes = new IndAdes();
    //Original name: IND-POLI
    private IndDettTitCont indPoli = new IndDettTitCont();
    //Original name: IND-STB-ID-MOVI-CHIU
    private short indStbIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: ADES-DB
    private AdesDb adesDb = new AdesDb();
    //Original name: POLI-DB
    private AdesDb poliDb = new AdesDb();
    //Original name: IDBVSTB3
    private Idbvstb3 idbvstb3 = new Idbvstb3();
    //Original name: ADES
    private AdesIvvs0216 ades = new AdesIvvs0216();
    //Original name: POLI
    private Poli poli = new Poli();
    //Original name: STAT-OGG-BUS
    private StatOggBus statOggBus = new StatOggBus();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndStbIdMoviChiu(short indStbIdMoviChiu) {
        this.indStbIdMoviChiu = indStbIdMoviChiu;
    }

    public short getIndStbIdMoviChiu() {
        return this.indStbIdMoviChiu;
    }

    public AdesIvvs0216 getAdes() {
        return ades;
    }

    public AdesDb getAdesDb() {
        return adesDb;
    }

    public Idbvstb3 getIdbvstb3() {
        return idbvstb3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public IndAdes getIndAdes() {
        return indAdes;
    }

    public IndDettTitCont getIndPoli() {
        return indPoli;
    }

    public Poli getPoli() {
        return poli;
    }

    public AdesDb getPoliDb() {
        return poliDb;
    }

    public StatOggBus getStatOggBus() {
        return statOggBus;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
