package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L30-COD-SUB-AGE<br>
 * Variable: L30-COD-SUB-AGE from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L30CodSubAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L30CodSubAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L30_COD_SUB_AGE;
    }

    public void setL30CodSubAge(int l30CodSubAge) {
        writeIntAsPacked(Pos.L30_COD_SUB_AGE, l30CodSubAge, Len.Int.L30_COD_SUB_AGE);
    }

    public void setL30CodSubAgeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L30_COD_SUB_AGE, Pos.L30_COD_SUB_AGE);
    }

    /**Original name: L30-COD-SUB-AGE<br>*/
    public int getL30CodSubAge() {
        return readPackedAsInt(Pos.L30_COD_SUB_AGE, Len.Int.L30_COD_SUB_AGE);
    }

    public byte[] getL30CodSubAgeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L30_COD_SUB_AGE, Pos.L30_COD_SUB_AGE);
        return buffer;
    }

    public void setL30CodSubAgeNull(String l30CodSubAgeNull) {
        writeString(Pos.L30_COD_SUB_AGE_NULL, l30CodSubAgeNull, Len.L30_COD_SUB_AGE_NULL);
    }

    /**Original name: L30-COD-SUB-AGE-NULL<br>*/
    public String getL30CodSubAgeNull() {
        return readString(Pos.L30_COD_SUB_AGE_NULL, Len.L30_COD_SUB_AGE_NULL);
    }

    public String getL30CodSubAgeNullFormatted() {
        return Functions.padBlanks(getL30CodSubAgeNull(), Len.L30_COD_SUB_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L30_COD_SUB_AGE = 1;
        public static final int L30_COD_SUB_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L30_COD_SUB_AGE = 3;
        public static final int L30_COD_SUB_AGE_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L30_COD_SUB_AGE = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
