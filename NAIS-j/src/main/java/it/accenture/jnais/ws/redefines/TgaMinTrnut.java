package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-MIN-TRNUT<br>
 * Variable: TGA-MIN-TRNUT from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaMinTrnut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaMinTrnut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_MIN_TRNUT;
    }

    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        writeDecimalAsPacked(Pos.TGA_MIN_TRNUT, tgaMinTrnut.copy());
    }

    public void setTgaMinTrnutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_MIN_TRNUT, Pos.TGA_MIN_TRNUT);
    }

    /**Original name: TGA-MIN-TRNUT<br>*/
    public AfDecimal getTgaMinTrnut() {
        return readPackedAsDecimal(Pos.TGA_MIN_TRNUT, Len.Int.TGA_MIN_TRNUT, Len.Fract.TGA_MIN_TRNUT);
    }

    public byte[] getTgaMinTrnutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_MIN_TRNUT, Pos.TGA_MIN_TRNUT);
        return buffer;
    }

    public void setTgaMinTrnutNull(String tgaMinTrnutNull) {
        writeString(Pos.TGA_MIN_TRNUT_NULL, tgaMinTrnutNull, Len.TGA_MIN_TRNUT_NULL);
    }

    /**Original name: TGA-MIN-TRNUT-NULL<br>*/
    public String getTgaMinTrnutNull() {
        return readString(Pos.TGA_MIN_TRNUT_NULL, Len.TGA_MIN_TRNUT_NULL);
    }

    public String getTgaMinTrnutNullFormatted() {
        return Functions.padBlanks(getTgaMinTrnutNull(), Len.TGA_MIN_TRNUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_MIN_TRNUT = 1;
        public static final int TGA_MIN_TRNUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_MIN_TRNUT = 8;
        public static final int TGA_MIN_TRNUT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_MIN_TRNUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_MIN_TRNUT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
