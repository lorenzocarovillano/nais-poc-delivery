package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;
import com.bphx.ctu.af.core.Types;

/**Original name: IDSV0302<br>
 * Variable: IDSV0302 from copybook IDSV0302<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class Idsv0302 extends BytesClass {

    //==== PROPERTIES ====
    public static final int ELE_MAXOCCURS = 1000000;
    private int idsv0302EleListenerSize = ELE_MAXOCCURS;
    private IValueChangeListener idsv0302EleListener = new Idsv0302EleListener();
    private Pos pos = new Pos(this);

    //==== CONSTRUCTORS ====
    public Idsv0302() {
    }

    public Idsv0302(byte[] data) {
        super(data);
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return getIdsv0302Size();
    }

    public int getIdsv0302Size() {
        return getBufferDataSize();
    }

    public void setIdsv0302Bytes(byte[] buffer) {
        setIdsv0302Bytes(buffer, 1);
    }

    public byte[] getIdsv0302Bytes() {
        byte[] buffer = new byte[getIdsv0302Size()];
        return getIdsv0302Bytes(buffer, 1);
    }

    public void setIdsv0302Bytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, getIdsv0302Size(), Pos.IDSV0302);
    }

    public byte[] getIdsv0302Bytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, getIdsv0302Size(), Pos.IDSV0302);
        return buffer;
    }

    public void setBufferDataSubstring(String replacement, int start, int length) {
        writeSubstring(Pos.BUFFER_DATA, replacement, start, length, Len.BUFFER_DATA);
    }

    public int getBufferDataSize() {
        return Types.CHAR_SIZE * idsv0302EleListenerSize;
    }

    public void setBufferDataFormatted(String data) {
        writeString(Pos.BUFFER_DATA, data, getBufferDataSize());
    }

    public String getBufferDataFormatted() {
        return readFixedString(Pos.BUFFER_DATA, getBufferDataSize());
    }

    public IValueChangeListener getIdsv0302EleListener() {
        return idsv0302EleListener;
    }

    //==== INNER CLASSES ====
    /**Original name: IDSV0302-ELE<br>*/
    public class Idsv0302EleListener implements IValueChangeListener {

        //==== METHODS ====
        public void change() {
            idsv0302EleListenerSize = ELE_MAXOCCURS;
        }

        public void change(int value) {
            idsv0302EleListenerSize = value < 1 ? 0 : (value > ELE_MAXOCCURS ? ELE_MAXOCCURS : value);
        }
    }

    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSV0302 = 1;
        public static final int BUFFER_DATA = IDSV0302;
        private Idsv0302 outer;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        public Pos(Idsv0302 outer) {
            this.outer = outer;
        }

        //==== METHODS ====
        public static int idsv0302Ele(int idx) {
            return BUFFER_DATA + idx * Len.ELE;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE = 1;
        public static final int BUFFER_DATA = Idsv0302.ELE_MAXOCCURS * ELE;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
