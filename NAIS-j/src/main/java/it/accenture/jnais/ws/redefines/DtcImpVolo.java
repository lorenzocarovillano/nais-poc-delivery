package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-IMP-VOLO<br>
 * Variable: DTC-IMP-VOLO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcImpVolo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcImpVolo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_IMP_VOLO;
    }

    public void setDtcImpVolo(AfDecimal dtcImpVolo) {
        writeDecimalAsPacked(Pos.DTC_IMP_VOLO, dtcImpVolo.copy());
    }

    public void setDtcImpVoloFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_IMP_VOLO, Pos.DTC_IMP_VOLO);
    }

    /**Original name: DTC-IMP-VOLO<br>*/
    public AfDecimal getDtcImpVolo() {
        return readPackedAsDecimal(Pos.DTC_IMP_VOLO, Len.Int.DTC_IMP_VOLO, Len.Fract.DTC_IMP_VOLO);
    }

    public byte[] getDtcImpVoloAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_IMP_VOLO, Pos.DTC_IMP_VOLO);
        return buffer;
    }

    public void setDtcImpVoloNull(String dtcImpVoloNull) {
        writeString(Pos.DTC_IMP_VOLO_NULL, dtcImpVoloNull, Len.DTC_IMP_VOLO_NULL);
    }

    /**Original name: DTC-IMP-VOLO-NULL<br>*/
    public String getDtcImpVoloNull() {
        return readString(Pos.DTC_IMP_VOLO_NULL, Len.DTC_IMP_VOLO_NULL);
    }

    public String getDtcImpVoloNullFormatted() {
        return Functions.padBlanks(getDtcImpVoloNull(), Len.DTC_IMP_VOLO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_IMP_VOLO = 1;
        public static final int DTC_IMP_VOLO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_IMP_VOLO = 8;
        public static final int DTC_IMP_VOLO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_IMP_VOLO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_IMP_VOLO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
