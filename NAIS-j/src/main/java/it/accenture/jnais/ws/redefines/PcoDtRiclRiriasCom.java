package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-RICL-RIRIAS-COM<br>
 * Variable: PCO-DT-RICL-RIRIAS-COM from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtRiclRiriasCom extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtRiclRiriasCom() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_RICL_RIRIAS_COM;
    }

    public void setPcoDtRiclRiriasCom(int pcoDtRiclRiriasCom) {
        writeIntAsPacked(Pos.PCO_DT_RICL_RIRIAS_COM, pcoDtRiclRiriasCom, Len.Int.PCO_DT_RICL_RIRIAS_COM);
    }

    public void setPcoDtRiclRiriasComFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_RICL_RIRIAS_COM, Pos.PCO_DT_RICL_RIRIAS_COM);
    }

    /**Original name: PCO-DT-RICL-RIRIAS-COM<br>*/
    public int getPcoDtRiclRiriasCom() {
        return readPackedAsInt(Pos.PCO_DT_RICL_RIRIAS_COM, Len.Int.PCO_DT_RICL_RIRIAS_COM);
    }

    public byte[] getPcoDtRiclRiriasComAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_RICL_RIRIAS_COM, Pos.PCO_DT_RICL_RIRIAS_COM);
        return buffer;
    }

    public void initPcoDtRiclRiriasComHighValues() {
        fill(Pos.PCO_DT_RICL_RIRIAS_COM, Len.PCO_DT_RICL_RIRIAS_COM, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtRiclRiriasComNull(String pcoDtRiclRiriasComNull) {
        writeString(Pos.PCO_DT_RICL_RIRIAS_COM_NULL, pcoDtRiclRiriasComNull, Len.PCO_DT_RICL_RIRIAS_COM_NULL);
    }

    /**Original name: PCO-DT-RICL-RIRIAS-COM-NULL<br>*/
    public String getPcoDtRiclRiriasComNull() {
        return readString(Pos.PCO_DT_RICL_RIRIAS_COM_NULL, Len.PCO_DT_RICL_RIRIAS_COM_NULL);
    }

    public String getPcoDtRiclRiriasComNullFormatted() {
        return Functions.padBlanks(getPcoDtRiclRiriasComNull(), Len.PCO_DT_RICL_RIRIAS_COM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_RICL_RIRIAS_COM = 1;
        public static final int PCO_DT_RICL_RIRIAS_COM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_RICL_RIRIAS_COM = 5;
        public static final int PCO_DT_RICL_RIRIAS_COM_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_RICL_RIRIAS_COM = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
