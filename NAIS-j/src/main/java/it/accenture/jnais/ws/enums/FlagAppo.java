package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FLAG-APPO<br>
 * Variable: FLAG-APPO from program LVVS1280<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagAppo {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.FLAG_APPO);
    public static final String TROVATO = "SI";
    public static final String NO_TROVATO = "NO";

    //==== METHODS ====
    public void setFlagAppo(String flagAppo) {
        this.value = Functions.subString(flagAppo, Len.FLAG_APPO);
    }

    public String getFlagAppo() {
        return this.value;
    }

    public boolean isTrovato() {
        return value.equals(TROVATO);
    }

    public void setTrovato() {
        value = TROVATO;
    }

    public void setNoTrovato() {
        value = NO_TROVATO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_APPO = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
