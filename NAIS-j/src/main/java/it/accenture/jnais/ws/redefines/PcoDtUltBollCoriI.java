package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-BOLL-CORI-I<br>
 * Variable: PCO-DT-ULT-BOLL-CORI-I from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltBollCoriI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltBollCoriI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_BOLL_CORI_I;
    }

    public void setPcoDtUltBollCoriI(int pcoDtUltBollCoriI) {
        writeIntAsPacked(Pos.PCO_DT_ULT_BOLL_CORI_I, pcoDtUltBollCoriI, Len.Int.PCO_DT_ULT_BOLL_CORI_I);
    }

    public void setPcoDtUltBollCoriIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_CORI_I, Pos.PCO_DT_ULT_BOLL_CORI_I);
    }

    /**Original name: PCO-DT-ULT-BOLL-CORI-I<br>*/
    public int getPcoDtUltBollCoriI() {
        return readPackedAsInt(Pos.PCO_DT_ULT_BOLL_CORI_I, Len.Int.PCO_DT_ULT_BOLL_CORI_I);
    }

    public byte[] getPcoDtUltBollCoriIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_BOLL_CORI_I, Pos.PCO_DT_ULT_BOLL_CORI_I);
        return buffer;
    }

    public void initPcoDtUltBollCoriIHighValues() {
        fill(Pos.PCO_DT_ULT_BOLL_CORI_I, Len.PCO_DT_ULT_BOLL_CORI_I, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltBollCoriINull(String pcoDtUltBollCoriINull) {
        writeString(Pos.PCO_DT_ULT_BOLL_CORI_I_NULL, pcoDtUltBollCoriINull, Len.PCO_DT_ULT_BOLL_CORI_I_NULL);
    }

    /**Original name: PCO-DT-ULT-BOLL-CORI-I-NULL<br>*/
    public String getPcoDtUltBollCoriINull() {
        return readString(Pos.PCO_DT_ULT_BOLL_CORI_I_NULL, Len.PCO_DT_ULT_BOLL_CORI_I_NULL);
    }

    public String getPcoDtUltBollCoriINullFormatted() {
        return Functions.padBlanks(getPcoDtUltBollCoriINull(), Len.PCO_DT_ULT_BOLL_CORI_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_CORI_I = 1;
        public static final int PCO_DT_ULT_BOLL_CORI_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_BOLL_CORI_I = 5;
        public static final int PCO_DT_ULT_BOLL_CORI_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_BOLL_CORI_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
