package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DUR-MM-FINANZ<br>
 * Variable: P67-DUR-MM-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DurMmFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DurMmFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DUR_MM_FINANZ;
    }

    public void setP67DurMmFinanz(int p67DurMmFinanz) {
        writeIntAsPacked(Pos.P67_DUR_MM_FINANZ, p67DurMmFinanz, Len.Int.P67_DUR_MM_FINANZ);
    }

    public void setP67DurMmFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DUR_MM_FINANZ, Pos.P67_DUR_MM_FINANZ);
    }

    /**Original name: P67-DUR-MM-FINANZ<br>*/
    public int getP67DurMmFinanz() {
        return readPackedAsInt(Pos.P67_DUR_MM_FINANZ, Len.Int.P67_DUR_MM_FINANZ);
    }

    public byte[] getP67DurMmFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DUR_MM_FINANZ, Pos.P67_DUR_MM_FINANZ);
        return buffer;
    }

    public void setP67DurMmFinanzNull(String p67DurMmFinanzNull) {
        writeString(Pos.P67_DUR_MM_FINANZ_NULL, p67DurMmFinanzNull, Len.P67_DUR_MM_FINANZ_NULL);
    }

    /**Original name: P67-DUR-MM-FINANZ-NULL<br>*/
    public String getP67DurMmFinanzNull() {
        return readString(Pos.P67_DUR_MM_FINANZ_NULL, Len.P67_DUR_MM_FINANZ_NULL);
    }

    public String getP67DurMmFinanzNullFormatted() {
        return Functions.padBlanks(getP67DurMmFinanzNull(), Len.P67_DUR_MM_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DUR_MM_FINANZ = 1;
        public static final int P67_DUR_MM_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DUR_MM_FINANZ = 3;
        public static final int P67_DUR_MM_FINANZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DUR_MM_FINANZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
