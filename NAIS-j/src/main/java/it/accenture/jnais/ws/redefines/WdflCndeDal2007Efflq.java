package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-CNDE-DAL2007-EFFLQ<br>
 * Variable: WDFL-CNDE-DAL2007-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflCndeDal2007Efflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflCndeDal2007Efflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_CNDE_DAL2007_EFFLQ;
    }

    public void setWdflCndeDal2007Efflq(AfDecimal wdflCndeDal2007Efflq) {
        writeDecimalAsPacked(Pos.WDFL_CNDE_DAL2007_EFFLQ, wdflCndeDal2007Efflq.copy());
    }

    public void setWdflCndeDal2007EfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_CNDE_DAL2007_EFFLQ, Pos.WDFL_CNDE_DAL2007_EFFLQ);
    }

    /**Original name: WDFL-CNDE-DAL2007-EFFLQ<br>*/
    public AfDecimal getWdflCndeDal2007Efflq() {
        return readPackedAsDecimal(Pos.WDFL_CNDE_DAL2007_EFFLQ, Len.Int.WDFL_CNDE_DAL2007_EFFLQ, Len.Fract.WDFL_CNDE_DAL2007_EFFLQ);
    }

    public byte[] getWdflCndeDal2007EfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_CNDE_DAL2007_EFFLQ, Pos.WDFL_CNDE_DAL2007_EFFLQ);
        return buffer;
    }

    public void setWdflCndeDal2007EfflqNull(String wdflCndeDal2007EfflqNull) {
        writeString(Pos.WDFL_CNDE_DAL2007_EFFLQ_NULL, wdflCndeDal2007EfflqNull, Len.WDFL_CNDE_DAL2007_EFFLQ_NULL);
    }

    /**Original name: WDFL-CNDE-DAL2007-EFFLQ-NULL<br>*/
    public String getWdflCndeDal2007EfflqNull() {
        return readString(Pos.WDFL_CNDE_DAL2007_EFFLQ_NULL, Len.WDFL_CNDE_DAL2007_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_DAL2007_EFFLQ = 1;
        public static final int WDFL_CNDE_DAL2007_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_CNDE_DAL2007_EFFLQ = 8;
        public static final int WDFL_CNDE_DAL2007_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_DAL2007_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_CNDE_DAL2007_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
