package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: IABI0011-APPEND-DATA-GATES<br>
 * Variable: IABI0011-APPEND-DATA-GATES from copybook IABI0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Iabi0011AppendDataGates {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char YES = 'Y';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setAppendDataGates(char appendDataGates) {
        this.value = appendDataGates;
    }

    public void setAppendDataGatesFormatted(String appendDataGates) {
        setAppendDataGates(Functions.charAt(appendDataGates, Types.CHAR_SIZE));
    }

    public char getAppendDataGates() {
        return this.value;
    }

    public boolean isYes() {
        return value == YES;
    }

    public boolean isNo() {
        return value == NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int APPEND_DATA_GATES = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
