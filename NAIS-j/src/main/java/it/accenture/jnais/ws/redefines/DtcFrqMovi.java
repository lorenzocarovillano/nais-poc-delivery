package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-FRQ-MOVI<br>
 * Variable: DTC-FRQ-MOVI from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcFrqMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcFrqMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_FRQ_MOVI;
    }

    public void setDtcFrqMovi(int dtcFrqMovi) {
        writeIntAsPacked(Pos.DTC_FRQ_MOVI, dtcFrqMovi, Len.Int.DTC_FRQ_MOVI);
    }

    public void setDtcFrqMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_FRQ_MOVI, Pos.DTC_FRQ_MOVI);
    }

    /**Original name: DTC-FRQ-MOVI<br>*/
    public int getDtcFrqMovi() {
        return readPackedAsInt(Pos.DTC_FRQ_MOVI, Len.Int.DTC_FRQ_MOVI);
    }

    public byte[] getDtcFrqMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_FRQ_MOVI, Pos.DTC_FRQ_MOVI);
        return buffer;
    }

    public void setDtcFrqMoviNull(String dtcFrqMoviNull) {
        writeString(Pos.DTC_FRQ_MOVI_NULL, dtcFrqMoviNull, Len.DTC_FRQ_MOVI_NULL);
    }

    /**Original name: DTC-FRQ-MOVI-NULL<br>*/
    public String getDtcFrqMoviNull() {
        return readString(Pos.DTC_FRQ_MOVI_NULL, Len.DTC_FRQ_MOVI_NULL);
    }

    public String getDtcFrqMoviNullFormatted() {
        return Functions.padBlanks(getDtcFrqMoviNull(), Len.DTC_FRQ_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_FRQ_MOVI = 1;
        public static final int DTC_FRQ_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_FRQ_MOVI = 3;
        public static final int DTC_FRQ_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_FRQ_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
