package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRSTZ-T<br>
 * Variable: WB03-PRSTZ-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrstzT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrstzT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRSTZ_T;
    }

    public void setWb03PrstzT(AfDecimal wb03PrstzT) {
        writeDecimalAsPacked(Pos.WB03_PRSTZ_T, wb03PrstzT.copy());
    }

    public void setWb03PrstzTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRSTZ_T, Pos.WB03_PRSTZ_T);
    }

    /**Original name: WB03-PRSTZ-T<br>*/
    public AfDecimal getWb03PrstzT() {
        return readPackedAsDecimal(Pos.WB03_PRSTZ_T, Len.Int.WB03_PRSTZ_T, Len.Fract.WB03_PRSTZ_T);
    }

    public byte[] getWb03PrstzTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRSTZ_T, Pos.WB03_PRSTZ_T);
        return buffer;
    }

    public void setWb03PrstzTNull(String wb03PrstzTNull) {
        writeString(Pos.WB03_PRSTZ_T_NULL, wb03PrstzTNull, Len.WB03_PRSTZ_T_NULL);
    }

    /**Original name: WB03-PRSTZ-T-NULL<br>*/
    public String getWb03PrstzTNull() {
        return readString(Pos.WB03_PRSTZ_T_NULL, Len.WB03_PRSTZ_T_NULL);
    }

    public String getWb03PrstzTNullFormatted() {
        return Functions.padBlanks(getWb03PrstzTNull(), Len.WB03_PRSTZ_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_T = 1;
        public static final int WB03_PRSTZ_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_T = 8;
        public static final int WB03_PRSTZ_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
