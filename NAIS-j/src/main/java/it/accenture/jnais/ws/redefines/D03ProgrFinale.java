package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: D03-PROGR-FINALE<br>
 * Variable: D03-PROGR-FINALE from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class D03ProgrFinale extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public D03ProgrFinale() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D03_PROGR_FINALE;
    }

    public void setD03ProgrFinale(long d03ProgrFinale) {
        writeLongAsPacked(Pos.D03_PROGR_FINALE, d03ProgrFinale, Len.Int.D03_PROGR_FINALE);
    }

    public void setD03ProgrFinaleFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.D03_PROGR_FINALE, Pos.D03_PROGR_FINALE);
    }

    /**Original name: D03-PROGR-FINALE<br>*/
    public long getD03ProgrFinale() {
        return readPackedAsLong(Pos.D03_PROGR_FINALE, Len.Int.D03_PROGR_FINALE);
    }

    public byte[] getD03ProgrFinaleAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.D03_PROGR_FINALE, Pos.D03_PROGR_FINALE);
        return buffer;
    }

    public void setD03ProgrFinaleNull(String d03ProgrFinaleNull) {
        writeString(Pos.D03_PROGR_FINALE_NULL, d03ProgrFinaleNull, Len.D03_PROGR_FINALE_NULL);
    }

    /**Original name: D03-PROGR-FINALE-NULL<br>*/
    public String getD03ProgrFinaleNull() {
        return readString(Pos.D03_PROGR_FINALE_NULL, Len.D03_PROGR_FINALE_NULL);
    }

    public String getD03ProgrFinaleNullFormatted() {
        return Functions.padBlanks(getD03ProgrFinaleNull(), Len.D03_PROGR_FINALE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int D03_PROGR_FINALE = 1;
        public static final int D03_PROGR_FINALE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int D03_PROGR_FINALE = 10;
        public static final int D03_PROGR_FINALE_NULL = 10;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D03_PROGR_FINALE = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
