package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMP-NET-EFFLQ<br>
 * Variable: WDFL-IMP-NET-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpNetEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpNetEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMP_NET_EFFLQ;
    }

    public void setWdflImpNetEfflq(AfDecimal wdflImpNetEfflq) {
        writeDecimalAsPacked(Pos.WDFL_IMP_NET_EFFLQ, wdflImpNetEfflq.copy());
    }

    public void setWdflImpNetEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMP_NET_EFFLQ, Pos.WDFL_IMP_NET_EFFLQ);
    }

    /**Original name: WDFL-IMP-NET-EFFLQ<br>*/
    public AfDecimal getWdflImpNetEfflq() {
        return readPackedAsDecimal(Pos.WDFL_IMP_NET_EFFLQ, Len.Int.WDFL_IMP_NET_EFFLQ, Len.Fract.WDFL_IMP_NET_EFFLQ);
    }

    public byte[] getWdflImpNetEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMP_NET_EFFLQ, Pos.WDFL_IMP_NET_EFFLQ);
        return buffer;
    }

    public void setWdflImpNetEfflqNull(String wdflImpNetEfflqNull) {
        writeString(Pos.WDFL_IMP_NET_EFFLQ_NULL, wdflImpNetEfflqNull, Len.WDFL_IMP_NET_EFFLQ_NULL);
    }

    /**Original name: WDFL-IMP-NET-EFFLQ-NULL<br>*/
    public String getWdflImpNetEfflqNull() {
        return readString(Pos.WDFL_IMP_NET_EFFLQ_NULL, Len.WDFL_IMP_NET_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_NET_EFFLQ = 1;
        public static final int WDFL_IMP_NET_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMP_NET_EFFLQ = 8;
        public static final int WDFL_IMP_NET_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_NET_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMP_NET_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
