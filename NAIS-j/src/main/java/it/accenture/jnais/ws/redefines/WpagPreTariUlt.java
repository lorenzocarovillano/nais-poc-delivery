package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-PRE-TARI-ULT<br>
 * Variable: WPAG-PRE-TARI-ULT from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagPreTariUlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagPreTariUlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_PRE_TARI_ULT;
    }

    public void setWpagPreTariUlt(AfDecimal wpagPreTariUlt) {
        writeDecimalAsPacked(Pos.WPAG_PRE_TARI_ULT, wpagPreTariUlt.copy());
    }

    public void setWpagPreTariUltFormatted(String wpagPreTariUlt) {
        setWpagPreTariUlt(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_PRE_TARI_ULT + Len.Fract.WPAG_PRE_TARI_ULT, Len.Fract.WPAG_PRE_TARI_ULT, wpagPreTariUlt));
    }

    public void setWpagPreTariUltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_PRE_TARI_ULT, Pos.WPAG_PRE_TARI_ULT);
    }

    /**Original name: WPAG-PRE-TARI-ULT<br>*/
    public AfDecimal getWpagPreTariUlt() {
        return readPackedAsDecimal(Pos.WPAG_PRE_TARI_ULT, Len.Int.WPAG_PRE_TARI_ULT, Len.Fract.WPAG_PRE_TARI_ULT);
    }

    public byte[] getWpagPreTariUltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_PRE_TARI_ULT, Pos.WPAG_PRE_TARI_ULT);
        return buffer;
    }

    public void initWpagPreTariUltSpaces() {
        fill(Pos.WPAG_PRE_TARI_ULT, Len.WPAG_PRE_TARI_ULT, Types.SPACE_CHAR);
    }

    public void setWpagPreTariUltNull(String wpagPreTariUltNull) {
        writeString(Pos.WPAG_PRE_TARI_ULT_NULL, wpagPreTariUltNull, Len.WPAG_PRE_TARI_ULT_NULL);
    }

    /**Original name: WPAG-PRE-TARI-ULT-NULL<br>*/
    public String getWpagPreTariUltNull() {
        return readString(Pos.WPAG_PRE_TARI_ULT_NULL, Len.WPAG_PRE_TARI_ULT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_TARI_ULT = 1;
        public static final int WPAG_PRE_TARI_ULT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_PRE_TARI_ULT = 8;
        public static final int WPAG_PRE_TARI_ULT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_TARI_ULT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_PRE_TARI_ULT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
