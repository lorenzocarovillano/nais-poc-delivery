package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-IMP-DEB-RES<br>
 * Variable: P67-IMP-DEB-RES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67ImpDebRes extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67ImpDebRes() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_IMP_DEB_RES;
    }

    public void setP67ImpDebRes(AfDecimal p67ImpDebRes) {
        writeDecimalAsPacked(Pos.P67_IMP_DEB_RES, p67ImpDebRes.copy());
    }

    public void setP67ImpDebResFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_IMP_DEB_RES, Pos.P67_IMP_DEB_RES);
    }

    /**Original name: P67-IMP-DEB-RES<br>*/
    public AfDecimal getP67ImpDebRes() {
        return readPackedAsDecimal(Pos.P67_IMP_DEB_RES, Len.Int.P67_IMP_DEB_RES, Len.Fract.P67_IMP_DEB_RES);
    }

    public byte[] getP67ImpDebResAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_IMP_DEB_RES, Pos.P67_IMP_DEB_RES);
        return buffer;
    }

    public void setP67ImpDebResNull(String p67ImpDebResNull) {
        writeString(Pos.P67_IMP_DEB_RES_NULL, p67ImpDebResNull, Len.P67_IMP_DEB_RES_NULL);
    }

    /**Original name: P67-IMP-DEB-RES-NULL<br>*/
    public String getP67ImpDebResNull() {
        return readString(Pos.P67_IMP_DEB_RES_NULL, Len.P67_IMP_DEB_RES_NULL);
    }

    public String getP67ImpDebResNullFormatted() {
        return Functions.padBlanks(getP67ImpDebResNull(), Len.P67_IMP_DEB_RES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_IMP_DEB_RES = 1;
        public static final int P67_IMP_DEB_RES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_IMP_DEB_RES = 8;
        public static final int P67_IMP_DEB_RES_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_IMP_DEB_RES = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int P67_IMP_DEB_RES = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
