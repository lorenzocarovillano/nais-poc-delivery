package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-CAR-INC<br>
 * Variable: DTC-CAR-INC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcCarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcCarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_CAR_INC;
    }

    public void setDtcCarInc(AfDecimal dtcCarInc) {
        writeDecimalAsPacked(Pos.DTC_CAR_INC, dtcCarInc.copy());
    }

    public void setDtcCarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_CAR_INC, Pos.DTC_CAR_INC);
    }

    /**Original name: DTC-CAR-INC<br>*/
    public AfDecimal getDtcCarInc() {
        return readPackedAsDecimal(Pos.DTC_CAR_INC, Len.Int.DTC_CAR_INC, Len.Fract.DTC_CAR_INC);
    }

    public byte[] getDtcCarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_CAR_INC, Pos.DTC_CAR_INC);
        return buffer;
    }

    public void setDtcCarIncNull(String dtcCarIncNull) {
        writeString(Pos.DTC_CAR_INC_NULL, dtcCarIncNull, Len.DTC_CAR_INC_NULL);
    }

    /**Original name: DTC-CAR-INC-NULL<br>*/
    public String getDtcCarIncNull() {
        return readString(Pos.DTC_CAR_INC_NULL, Len.DTC_CAR_INC_NULL);
    }

    public String getDtcCarIncNullFormatted() {
        return Functions.padBlanks(getDtcCarIncNull(), Len.DTC_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_CAR_INC = 1;
        public static final int DTC_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_CAR_INC = 8;
        public static final int DTC_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
