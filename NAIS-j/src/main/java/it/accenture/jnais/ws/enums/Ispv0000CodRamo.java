package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPV0000-COD-RAMO<br>
 * Variable: ISPV0000-COD-RAMO from copybook ISPV0000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ispv0000CodRamo {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.COD_RAMO);
    public static final String CO_VITA_PI = "054";
    public static final String CO_GRUPPO = "057";
    public static final String CO_BASE_AURORA = "058";
    public static final String CO_VITA_AIL = "064";
    public static final String IN_BASE = "089";
    public static final String IN_UNIT = "110";
    public static final String IN_INDEX = "111";
    public static final String CO_FONDI_CHIUSI = "120";
    public static final String CO_FONDI_PENSIONE = "154";
    public static final String CO_TFR = "155";
    public static final String CO_CAPITALIZ = "156";
    public static final String IN_UNIT_MULTIRAMO = "311";
    public static final String IN_CC_ASSICURATIVO = "312";
    public static final String CO_BASE_UNIPOL = "C10";

    //==== METHODS ====
    public void setCodRamo(String codRamo) {
        this.value = Functions.subString(codRamo, Len.COD_RAMO);
    }

    public String getCodRamo() {
        return this.value;
    }

    public boolean isInCcAssicurativo() {
        return value.equals(IN_CC_ASSICURATIVO);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_RAMO = 12;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
