package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: IDSV0503-STRUCT-NUM<br>
 * Variable: IDSV0503-STRUCT-NUM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Idsv0503StructNum extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int ELE_NUM_IMP_MAXOCCURS = 18;

    //==== CONSTRUCTORS ====
    public Idsv0503StructNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IDSV0503_STRUCT_NUM;
    }

    public void setIdsv0503StructNum(long idsv0503StructNum) {
        writeLong(Pos.IDSV0503_STRUCT_NUM, idsv0503StructNum, Len.Int.IDSV0503_STRUCT_NUM, SignType.NO_SIGN);
    }

    /**Original name: IDSV0503-STRUCT-NUM<br>*/
    public long getIdsv0503StructNum() {
        return readNumDispUnsignedLong(Pos.IDSV0503_STRUCT_NUM, Len.IDSV0503_STRUCT_NUM);
    }

    public void setIdsv0503StructImp(AfDecimal idsv0503StructImp) {
        writeDecimal(Pos.IDSV0503_STRUCT_IMP, idsv0503StructImp.copy(), SignType.NO_SIGN);
    }

    /**Original name: IDSV0503-STRUCT-IMP<br>*/
    public AfDecimal getIdsv0503StructImp() {
        return readDecimal(Pos.IDSV0503_STRUCT_IMP, Len.Int.IDSV0503_STRUCT_IMP, Len.Fract.IDSV0503_STRUCT_IMP, SignType.NO_SIGN);
    }

    public void setIdsv0503StructPerc(AfDecimal idsv0503StructPerc) {
        writeDecimal(Pos.IDSV0503_STRUCT_PERC, idsv0503StructPerc.copy(), SignType.NO_SIGN);
    }

    /**Original name: IDSV0503-STRUCT-PERC<br>*/
    public AfDecimal getIdsv0503StructPerc() {
        return readDecimal(Pos.IDSV0503_STRUCT_PERC, Len.Int.IDSV0503_STRUCT_PERC, Len.Fract.IDSV0503_STRUCT_PERC, SignType.NO_SIGN);
    }

    /**Original name: IDSV0503-ELE-NUM-IMP<br>*/
    public char getEleNumImp(int eleNumImpIdx) {
        int position = Pos.idsv0503EleNumImp(eleNumImpIdx - 1);
        return readChar(position);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IDSV0503_STRUCT_NUM = 1;
        public static final int IDSV0503_STRUCT_IMP = 1;
        public static final int IDSV0503_STRUCT_PERC = 1;
        public static final int IDSV0503_ELEMENT_NUM_IMP = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int idsv0503EleNumImp(int idx) {
            return IDSV0503_ELEMENT_NUM_IMP + idx * Len.ELE_NUM_IMP;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_NUM_IMP = 1;
        public static final int IDSV0503_STRUCT_NUM = 18;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int IDSV0503_STRUCT_NUM = 18;
            public static final int IDSV0503_STRUCT_IMP = 11;
            public static final int IDSV0503_STRUCT_PERC = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IDSV0503_STRUCT_IMP = 7;
            public static final int IDSV0503_STRUCT_PERC = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
