package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.enums.W800ModChiamataServizio;

/**Original name: W800-AREA-PAG<br>
 * Variable: W800-AREA-PAG from program LOAS0800<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class W800AreaPag extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: W800-MOD-CHIAMATA-SERVIZIO
    private W800ModChiamataServizio modChiamataServizio = new W800ModChiamataServizio();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W800_AREA_PAG;
    }

    @Override
    public void deserialize(byte[] buf) {
        setW800AreaPagBytes(buf);
    }

    public String getW800AreaPagFormatted() {
        return getAreaVarInpFormatted();
    }

    public void setW800AreaPagBytes(byte[] buffer) {
        setW800AreaPagBytes(buffer, 1);
    }

    public byte[] getW800AreaPagBytes() {
        byte[] buffer = new byte[Len.W800_AREA_PAG];
        return getW800AreaPagBytes(buffer, 1);
    }

    public void setW800AreaPagBytes(byte[] buffer, int offset) {
        int position = offset;
        setAreaVarInpBytes(buffer, position);
    }

    public byte[] getW800AreaPagBytes(byte[] buffer, int offset) {
        int position = offset;
        getAreaVarInpBytes(buffer, position);
        return buffer;
    }

    public String getAreaVarInpFormatted() {
        return modChiamataServizio.getModChiamataServizioFormatted();
    }

    public void setAreaVarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        modChiamataServizio.setModChiamataServizio(MarshalByte.readString(buffer, position, W800ModChiamataServizio.Len.MOD_CHIAMATA_SERVIZIO));
    }

    public byte[] getAreaVarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, modChiamataServizio.getModChiamataServizio(), W800ModChiamataServizio.Len.MOD_CHIAMATA_SERVIZIO);
        return buffer;
    }

    public W800ModChiamataServizio getModChiamataServizio() {
        return modChiamataServizio;
    }

    @Override
    public byte[] serialize() {
        return getW800AreaPagBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int AREA_VAR_INP = W800ModChiamataServizio.Len.MOD_CHIAMATA_SERVIZIO;
        public static final int W800_AREA_PAG = AREA_VAR_INP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
