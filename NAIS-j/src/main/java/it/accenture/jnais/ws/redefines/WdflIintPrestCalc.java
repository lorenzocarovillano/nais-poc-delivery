package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IINT-PREST-CALC<br>
 * Variable: WDFL-IINT-PREST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflIintPrestCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflIintPrestCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IINT_PREST_CALC;
    }

    public void setWdflIintPrestCalc(AfDecimal wdflIintPrestCalc) {
        writeDecimalAsPacked(Pos.WDFL_IINT_PREST_CALC, wdflIintPrestCalc.copy());
    }

    public void setWdflIintPrestCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IINT_PREST_CALC, Pos.WDFL_IINT_PREST_CALC);
    }

    /**Original name: WDFL-IINT-PREST-CALC<br>*/
    public AfDecimal getWdflIintPrestCalc() {
        return readPackedAsDecimal(Pos.WDFL_IINT_PREST_CALC, Len.Int.WDFL_IINT_PREST_CALC, Len.Fract.WDFL_IINT_PREST_CALC);
    }

    public byte[] getWdflIintPrestCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IINT_PREST_CALC, Pos.WDFL_IINT_PREST_CALC);
        return buffer;
    }

    public void setWdflIintPrestCalcNull(String wdflIintPrestCalcNull) {
        writeString(Pos.WDFL_IINT_PREST_CALC_NULL, wdflIintPrestCalcNull, Len.WDFL_IINT_PREST_CALC_NULL);
    }

    /**Original name: WDFL-IINT-PREST-CALC-NULL<br>*/
    public String getWdflIintPrestCalcNull() {
        return readString(Pos.WDFL_IINT_PREST_CALC_NULL, Len.WDFL_IINT_PREST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IINT_PREST_CALC = 1;
        public static final int WDFL_IINT_PREST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IINT_PREST_CALC = 8;
        public static final int WDFL_IINT_PREST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IINT_PREST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IINT_PREST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
