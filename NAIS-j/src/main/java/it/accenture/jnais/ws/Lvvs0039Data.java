package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.Ldbvf111Input;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.TrovataArea;
import it.accenture.jnais.ws.enums.TrovataGar;
import it.accenture.jnais.ws.enums.TrovataVar;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0039;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0039<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0039Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0039";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: TROVATA-GAR
    private TrovataGar trovataGar = new TrovataGar();
    //Original name: TROVATA-VAR
    private TrovataVar trovataVar = new TrovataVar();
    //Original name: TROVATA-AREA
    private TrovataArea trovataArea = new TrovataArea();
    //Original name: LDBVF981
    private Ldbvf111Input ldbvf981 = new Ldbvf111Input();
    //Original name: DETT-TIT-CONT
    private DettTitContIdbsdtc0 dettTitCont = new DettTitContIdbsdtc0();
    //Original name: DPOL-ELE-POL-MAX
    private short dpolElePolMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: FXT-ADDRESS
    private int fxtAddress = DefaultValues.BIN_INT_VAL;
    //Original name: VXG-ADDRESS
    private int vxgAddress = DefaultValues.BIN_INT_VAL;
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-INDICI
    private Indici ixIndici = new Indici();
    //Original name: IMPORTO-TOTALE
    private AfDecimal importoTotale = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   COPY TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0039 wsMovimento = new WsMovimentoLvvs0039();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setDpolAreaPolFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POL];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POL);
        setDpolAreaPolBytes(buffer, 1);
    }

    public void setDpolAreaPolBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePolMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPolBytes(buffer, position);
    }

    public void setDpolElePolMax(short dpolElePolMax) {
        this.dpolElePolMax = dpolElePolMax;
    }

    public short getDpolElePolMax() {
        return this.dpolElePolMax;
    }

    public void setDpolTabPolBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setAreaPointerFormatted(String data) {
        byte[] buffer = new byte[Len.AREA_POINTER];
        MarshalByte.writeString(buffer, 1, data, Len.AREA_POINTER);
        setAreaPointerBytes(buffer, 1);
    }

    public void setAreaPointerBytes(byte[] buffer, int offset) {
        int position = offset;
        fxtAddress = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        vxgAddress = MarshalByte.readBinaryInt(buffer, position);
    }

    public void setFxtAddress(int fxtAddress) {
        this.fxtAddress = fxtAddress;
    }

    public int getFxtAddress() {
        return this.fxtAddress;
    }

    public void setVxgAddress(int vxgAddress) {
        this.vxgAddress = vxgAddress;
    }

    public int getVxgAddress() {
        return this.vxgAddress;
    }

    public void setImportoTotale(AfDecimal importoTotale) {
        this.importoTotale.assign(importoTotale);
    }

    public AfDecimal getImportoTotale() {
        return this.importoTotale.copy();
    }

    public DettTitContIdbsdtc0 getDettTitCont() {
        return dettTitCont;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Indici getIxIndici() {
        return ixIndici;
    }

    public Ldbvf111Input getLdbvf981() {
        return ldbvf981;
    }

    public TrovataArea getTrovataArea() {
        return trovataArea;
    }

    public TrovataGar getTrovataGar() {
        return trovataGar;
    }

    public TrovataVar getTrovataVar() {
        return trovataVar;
    }

    public WsMovimentoLvvs0039 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int GG_DIFF = 5;
        public static final int DPOL_ELE_POL_MAX = 2;
        public static final int DPOL_TAB_POL = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POL = DPOL_ELE_POL_MAX + DPOL_TAB_POL;
        public static final int FXT_ADDRESS = 4;
        public static final int VXG_ADDRESS = 4;
        public static final int AREA_POINTER = FXT_ADDRESS + VXG_ADDRESS;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
