package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WRRE-COD-PNT-RETE-INI-C<br>
 * Variable: WRRE-COD-PNT-RETE-INI-C from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WrreCodPntReteIniC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WrreCodPntReteIniC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WRRE_COD_PNT_RETE_INI_C;
    }

    public void setWrreCodPntReteIniC(long wrreCodPntReteIniC) {
        writeLongAsPacked(Pos.WRRE_COD_PNT_RETE_INI_C, wrreCodPntReteIniC, Len.Int.WRRE_COD_PNT_RETE_INI_C);
    }

    public void setWrreCodPntReteIniCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WRRE_COD_PNT_RETE_INI_C, Pos.WRRE_COD_PNT_RETE_INI_C);
    }

    /**Original name: WRRE-COD-PNT-RETE-INI-C<br>*/
    public long getWrreCodPntReteIniC() {
        return readPackedAsLong(Pos.WRRE_COD_PNT_RETE_INI_C, Len.Int.WRRE_COD_PNT_RETE_INI_C);
    }

    public byte[] getWrreCodPntReteIniCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WRRE_COD_PNT_RETE_INI_C, Pos.WRRE_COD_PNT_RETE_INI_C);
        return buffer;
    }

    public void initWrreCodPntReteIniCSpaces() {
        fill(Pos.WRRE_COD_PNT_RETE_INI_C, Len.WRRE_COD_PNT_RETE_INI_C, Types.SPACE_CHAR);
    }

    public void setWrreCodPntReteIniCNull(String wrreCodPntReteIniCNull) {
        writeString(Pos.WRRE_COD_PNT_RETE_INI_C_NULL, wrreCodPntReteIniCNull, Len.WRRE_COD_PNT_RETE_INI_C_NULL);
    }

    /**Original name: WRRE-COD-PNT-RETE-INI-C-NULL<br>*/
    public String getWrreCodPntReteIniCNull() {
        return readString(Pos.WRRE_COD_PNT_RETE_INI_C_NULL, Len.WRRE_COD_PNT_RETE_INI_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WRRE_COD_PNT_RETE_INI_C = 1;
        public static final int WRRE_COD_PNT_RETE_INI_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WRRE_COD_PNT_RETE_INI_C = 6;
        public static final int WRRE_COD_PNT_RETE_INI_C_NULL = 6;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WRRE_COD_PNT_RETE_INI_C = 10;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
