package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: P67-DT-EROG-FINANZ<br>
 * Variable: P67-DT-EROG-FINANZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class P67DtErogFinanz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public P67DtErogFinanz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.P67_DT_EROG_FINANZ;
    }

    public void setP67DtErogFinanz(int p67DtErogFinanz) {
        writeIntAsPacked(Pos.P67_DT_EROG_FINANZ, p67DtErogFinanz, Len.Int.P67_DT_EROG_FINANZ);
    }

    public void setP67DtErogFinanzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.P67_DT_EROG_FINANZ, Pos.P67_DT_EROG_FINANZ);
    }

    /**Original name: P67-DT-EROG-FINANZ<br>*/
    public int getP67DtErogFinanz() {
        return readPackedAsInt(Pos.P67_DT_EROG_FINANZ, Len.Int.P67_DT_EROG_FINANZ);
    }

    public byte[] getP67DtErogFinanzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.P67_DT_EROG_FINANZ, Pos.P67_DT_EROG_FINANZ);
        return buffer;
    }

    public void setP67DtErogFinanzNull(String p67DtErogFinanzNull) {
        writeString(Pos.P67_DT_EROG_FINANZ_NULL, p67DtErogFinanzNull, Len.P67_DT_EROG_FINANZ_NULL);
    }

    /**Original name: P67-DT-EROG-FINANZ-NULL<br>*/
    public String getP67DtErogFinanzNull() {
        return readString(Pos.P67_DT_EROG_FINANZ_NULL, Len.P67_DT_EROG_FINANZ_NULL);
    }

    public String getP67DtErogFinanzNullFormatted() {
        return Functions.padBlanks(getP67DtErogFinanzNull(), Len.P67_DT_EROG_FINANZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int P67_DT_EROG_FINANZ = 1;
        public static final int P67_DT_EROG_FINANZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int P67_DT_EROG_FINANZ = 5;
        public static final int P67_DT_EROG_FINANZ_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P67_DT_EROG_FINANZ = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
