package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMPST-PRVR<br>
 * Variable: TCL-IMPST-PRVR from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpstPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpstPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMPST_PRVR;
    }

    public void setTclImpstPrvr(AfDecimal tclImpstPrvr) {
        writeDecimalAsPacked(Pos.TCL_IMPST_PRVR, tclImpstPrvr.copy());
    }

    public void setTclImpstPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMPST_PRVR, Pos.TCL_IMPST_PRVR);
    }

    /**Original name: TCL-IMPST-PRVR<br>*/
    public AfDecimal getTclImpstPrvr() {
        return readPackedAsDecimal(Pos.TCL_IMPST_PRVR, Len.Int.TCL_IMPST_PRVR, Len.Fract.TCL_IMPST_PRVR);
    }

    public byte[] getTclImpstPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMPST_PRVR, Pos.TCL_IMPST_PRVR);
        return buffer;
    }

    public void setTclImpstPrvrNull(String tclImpstPrvrNull) {
        writeString(Pos.TCL_IMPST_PRVR_NULL, tclImpstPrvrNull, Len.TCL_IMPST_PRVR_NULL);
    }

    /**Original name: TCL-IMPST-PRVR-NULL<br>*/
    public String getTclImpstPrvrNull() {
        return readString(Pos.TCL_IMPST_PRVR_NULL, Len.TCL_IMPST_PRVR_NULL);
    }

    public String getTclImpstPrvrNullFormatted() {
        return Functions.padBlanks(getTclImpstPrvrNull(), Len.TCL_IMPST_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMPST_PRVR = 1;
        public static final int TCL_IMPST_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMPST_PRVR = 8;
        public static final int TCL_IMPST_PRVR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMPST_PRVR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMPST_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
