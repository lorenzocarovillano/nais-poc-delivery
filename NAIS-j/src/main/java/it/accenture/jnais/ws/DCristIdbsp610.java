package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.P61CptIni30062014;
import it.accenture.jnais.ws.redefines.P61CptRivto31122011;
import it.accenture.jnais.ws.redefines.P61IdAdes;
import it.accenture.jnais.ws.redefines.P61IdMoviChiu;
import it.accenture.jnais.ws.redefines.P61IdTrchDiGar;
import it.accenture.jnais.ws.redefines.P61ImpbIs30062014;
import it.accenture.jnais.ws.redefines.P61ImpbIs31122011;
import it.accenture.jnais.ws.redefines.P61ImpbIsRpP2011;
import it.accenture.jnais.ws.redefines.P61ImpbIsRpP62014;
import it.accenture.jnais.ws.redefines.P61ImpbVis30062014;
import it.accenture.jnais.ws.redefines.P61ImpbVis31122011;
import it.accenture.jnais.ws.redefines.P61ImpbVisRpP2011;
import it.accenture.jnais.ws.redefines.P61ImpbVisRpP62014;
import it.accenture.jnais.ws.redefines.P61MontLrdDal2007;
import it.accenture.jnais.ws.redefines.P61MontLrdEnd2000;
import it.accenture.jnais.ws.redefines.P61MontLrdEnd2006;
import it.accenture.jnais.ws.redefines.P61PreLrdDal2007;
import it.accenture.jnais.ws.redefines.P61PreLrdEnd2000;
import it.accenture.jnais.ws.redefines.P61PreLrdEnd2006;
import it.accenture.jnais.ws.redefines.P61PreRshV30062014;
import it.accenture.jnais.ws.redefines.P61PreRshV31122011;
import it.accenture.jnais.ws.redefines.P61PreV30062014;
import it.accenture.jnais.ws.redefines.P61PreV31122011;
import it.accenture.jnais.ws.redefines.P61RendtoLrdDal2007;
import it.accenture.jnais.ws.redefines.P61RendtoLrdEnd2000;
import it.accenture.jnais.ws.redefines.P61RendtoLrdEnd2006;
import it.accenture.jnais.ws.redefines.P61RisMat30062014;
import it.accenture.jnais.ws.redefines.P61RisMat31122011;

/**Original name: D-CRIST<br>
 * Variable: D-CRIST from copybook IDBVP611<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DCristIdbsp610 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P61-ID-D-CRIST
    private int p61IdDCrist = DefaultValues.INT_VAL;
    //Original name: P61-ID-POLI
    private int p61IdPoli = DefaultValues.INT_VAL;
    //Original name: P61-COD-COMP-ANIA
    private int p61CodCompAnia = DefaultValues.INT_VAL;
    //Original name: P61-ID-MOVI-CRZ
    private int p61IdMoviCrz = DefaultValues.INT_VAL;
    //Original name: P61-ID-MOVI-CHIU
    private P61IdMoviChiu p61IdMoviChiu = new P61IdMoviChiu();
    //Original name: P61-DT-INI-EFF
    private int p61DtIniEff = DefaultValues.INT_VAL;
    //Original name: P61-DT-END-EFF
    private int p61DtEndEff = DefaultValues.INT_VAL;
    //Original name: P61-COD-PROD
    private String p61CodProd = DefaultValues.stringVal(Len.P61_COD_PROD);
    //Original name: P61-DT-DECOR
    private int p61DtDecor = DefaultValues.INT_VAL;
    //Original name: P61-DS-RIGA
    private long p61DsRiga = DefaultValues.LONG_VAL;
    //Original name: P61-DS-OPER-SQL
    private char p61DsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P61-DS-VER
    private int p61DsVer = DefaultValues.INT_VAL;
    //Original name: P61-DS-TS-INI-CPTZ
    private long p61DsTsIniCptz = DefaultValues.LONG_VAL;
    //Original name: P61-DS-TS-END-CPTZ
    private long p61DsTsEndCptz = DefaultValues.LONG_VAL;
    //Original name: P61-DS-UTENTE
    private String p61DsUtente = DefaultValues.stringVal(Len.P61_DS_UTENTE);
    //Original name: P61-DS-STATO-ELAB
    private char p61DsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P61-RIS-MAT-31122011
    private P61RisMat31122011 p61RisMat31122011 = new P61RisMat31122011();
    //Original name: P61-PRE-V-31122011
    private P61PreV31122011 p61PreV31122011 = new P61PreV31122011();
    //Original name: P61-PRE-RSH-V-31122011
    private P61PreRshV31122011 p61PreRshV31122011 = new P61PreRshV31122011();
    //Original name: P61-CPT-RIVTO-31122011
    private P61CptRivto31122011 p61CptRivto31122011 = new P61CptRivto31122011();
    //Original name: P61-IMPB-VIS-31122011
    private P61ImpbVis31122011 p61ImpbVis31122011 = new P61ImpbVis31122011();
    //Original name: P61-IMPB-IS-31122011
    private P61ImpbIs31122011 p61ImpbIs31122011 = new P61ImpbIs31122011();
    //Original name: P61-IMPB-VIS-RP-P2011
    private P61ImpbVisRpP2011 p61ImpbVisRpP2011 = new P61ImpbVisRpP2011();
    //Original name: P61-IMPB-IS-RP-P2011
    private P61ImpbIsRpP2011 p61ImpbIsRpP2011 = new P61ImpbIsRpP2011();
    //Original name: P61-PRE-V-30062014
    private P61PreV30062014 p61PreV30062014 = new P61PreV30062014();
    //Original name: P61-PRE-RSH-V-30062014
    private P61PreRshV30062014 p61PreRshV30062014 = new P61PreRshV30062014();
    //Original name: P61-CPT-INI-30062014
    private P61CptIni30062014 p61CptIni30062014 = new P61CptIni30062014();
    //Original name: P61-IMPB-VIS-30062014
    private P61ImpbVis30062014 p61ImpbVis30062014 = new P61ImpbVis30062014();
    //Original name: P61-IMPB-IS-30062014
    private P61ImpbIs30062014 p61ImpbIs30062014 = new P61ImpbIs30062014();
    //Original name: P61-IMPB-VIS-RP-P62014
    private P61ImpbVisRpP62014 p61ImpbVisRpP62014 = new P61ImpbVisRpP62014();
    //Original name: P61-IMPB-IS-RP-P62014
    private P61ImpbIsRpP62014 p61ImpbIsRpP62014 = new P61ImpbIsRpP62014();
    //Original name: P61-RIS-MAT-30062014
    private P61RisMat30062014 p61RisMat30062014 = new P61RisMat30062014();
    //Original name: P61-ID-ADES
    private P61IdAdes p61IdAdes = new P61IdAdes();
    //Original name: P61-MONT-LRD-END2000
    private P61MontLrdEnd2000 p61MontLrdEnd2000 = new P61MontLrdEnd2000();
    //Original name: P61-PRE-LRD-END2000
    private P61PreLrdEnd2000 p61PreLrdEnd2000 = new P61PreLrdEnd2000();
    //Original name: P61-RENDTO-LRD-END2000
    private P61RendtoLrdEnd2000 p61RendtoLrdEnd2000 = new P61RendtoLrdEnd2000();
    //Original name: P61-MONT-LRD-END2006
    private P61MontLrdEnd2006 p61MontLrdEnd2006 = new P61MontLrdEnd2006();
    //Original name: P61-PRE-LRD-END2006
    private P61PreLrdEnd2006 p61PreLrdEnd2006 = new P61PreLrdEnd2006();
    //Original name: P61-RENDTO-LRD-END2006
    private P61RendtoLrdEnd2006 p61RendtoLrdEnd2006 = new P61RendtoLrdEnd2006();
    //Original name: P61-MONT-LRD-DAL2007
    private P61MontLrdDal2007 p61MontLrdDal2007 = new P61MontLrdDal2007();
    //Original name: P61-PRE-LRD-DAL2007
    private P61PreLrdDal2007 p61PreLrdDal2007 = new P61PreLrdDal2007();
    //Original name: P61-RENDTO-LRD-DAL2007
    private P61RendtoLrdDal2007 p61RendtoLrdDal2007 = new P61RendtoLrdDal2007();
    //Original name: P61-ID-TRCH-DI-GAR
    private P61IdTrchDiGar p61IdTrchDiGar = new P61IdTrchDiGar();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D_CRIST;
    }

    @Override
    public void deserialize(byte[] buf) {
        setdCristBytes(buf);
    }

    public void setdCristFormatted(String data) {
        byte[] buffer = new byte[Len.D_CRIST];
        MarshalByte.writeString(buffer, 1, data, Len.D_CRIST);
        setdCristBytes(buffer, 1);
    }

    public String getdCristFormatted() {
        return MarshalByteExt.bufferToStr(getdCristBytes());
    }

    public void setdCristBytes(byte[] buffer) {
        setdCristBytes(buffer, 1);
    }

    public byte[] getdCristBytes() {
        byte[] buffer = new byte[Len.D_CRIST];
        return getdCristBytes(buffer, 1);
    }

    public void setdCristBytes(byte[] buffer, int offset) {
        int position = offset;
        p61IdDCrist = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_ID_D_CRIST, 0);
        position += Len.P61_ID_D_CRIST;
        p61IdPoli = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_ID_POLI, 0);
        position += Len.P61_ID_POLI;
        p61CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_COD_COMP_ANIA, 0);
        position += Len.P61_COD_COMP_ANIA;
        p61IdMoviCrz = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_ID_MOVI_CRZ, 0);
        position += Len.P61_ID_MOVI_CRZ;
        p61IdMoviChiu.setP61IdMoviChiuFromBuffer(buffer, position);
        position += P61IdMoviChiu.Len.P61_ID_MOVI_CHIU;
        p61DtIniEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_DT_INI_EFF, 0);
        position += Len.P61_DT_INI_EFF;
        p61DtEndEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_DT_END_EFF, 0);
        position += Len.P61_DT_END_EFF;
        p61CodProd = MarshalByte.readString(buffer, position, Len.P61_COD_PROD);
        position += Len.P61_COD_PROD;
        p61DtDecor = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_DT_DECOR, 0);
        position += Len.P61_DT_DECOR;
        p61DsRiga = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P61_DS_RIGA, 0);
        position += Len.P61_DS_RIGA;
        p61DsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p61DsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.P61_DS_VER, 0);
        position += Len.P61_DS_VER;
        p61DsTsIniCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P61_DS_TS_INI_CPTZ, 0);
        position += Len.P61_DS_TS_INI_CPTZ;
        p61DsTsEndCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.P61_DS_TS_END_CPTZ, 0);
        position += Len.P61_DS_TS_END_CPTZ;
        p61DsUtente = MarshalByte.readString(buffer, position, Len.P61_DS_UTENTE);
        position += Len.P61_DS_UTENTE;
        p61DsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        p61RisMat31122011.setP61RisMat31122011FromBuffer(buffer, position);
        position += P61RisMat31122011.Len.P61_RIS_MAT31122011;
        p61PreV31122011.setP61PreV31122011FromBuffer(buffer, position);
        position += P61PreV31122011.Len.P61_PRE_V31122011;
        p61PreRshV31122011.setP61PreRshV31122011FromBuffer(buffer, position);
        position += P61PreRshV31122011.Len.P61_PRE_RSH_V31122011;
        p61CptRivto31122011.setP61CptRivto31122011FromBuffer(buffer, position);
        position += P61CptRivto31122011.Len.P61_CPT_RIVTO31122011;
        p61ImpbVis31122011.setP61ImpbVis31122011FromBuffer(buffer, position);
        position += P61ImpbVis31122011.Len.P61_IMPB_VIS31122011;
        p61ImpbIs31122011.setP61ImpbIs31122011FromBuffer(buffer, position);
        position += P61ImpbIs31122011.Len.P61_IMPB_IS31122011;
        p61ImpbVisRpP2011.setP61ImpbVisRpP2011FromBuffer(buffer, position);
        position += P61ImpbVisRpP2011.Len.P61_IMPB_VIS_RP_P2011;
        p61ImpbIsRpP2011.setP61ImpbIsRpP2011FromBuffer(buffer, position);
        position += P61ImpbIsRpP2011.Len.P61_IMPB_IS_RP_P2011;
        p61PreV30062014.setP61PreV30062014FromBuffer(buffer, position);
        position += P61PreV30062014.Len.P61_PRE_V30062014;
        p61PreRshV30062014.setP61PreRshV30062014FromBuffer(buffer, position);
        position += P61PreRshV30062014.Len.P61_PRE_RSH_V30062014;
        p61CptIni30062014.setP61CptIni30062014FromBuffer(buffer, position);
        position += P61CptIni30062014.Len.P61_CPT_INI30062014;
        p61ImpbVis30062014.setP61ImpbVis30062014FromBuffer(buffer, position);
        position += P61ImpbVis30062014.Len.P61_IMPB_VIS30062014;
        p61ImpbIs30062014.setP61ImpbIs30062014FromBuffer(buffer, position);
        position += P61ImpbIs30062014.Len.P61_IMPB_IS30062014;
        p61ImpbVisRpP62014.setP61ImpbVisRpP62014FromBuffer(buffer, position);
        position += P61ImpbVisRpP62014.Len.P61_IMPB_VIS_RP_P62014;
        p61ImpbIsRpP62014.setP61ImpbIsRpP62014FromBuffer(buffer, position);
        position += P61ImpbIsRpP62014.Len.P61_IMPB_IS_RP_P62014;
        p61RisMat30062014.setP61RisMat30062014FromBuffer(buffer, position);
        position += P61RisMat30062014.Len.P61_RIS_MAT30062014;
        p61IdAdes.setP61IdAdesFromBuffer(buffer, position);
        position += P61IdAdes.Len.P61_ID_ADES;
        p61MontLrdEnd2000.setP61MontLrdEnd2000FromBuffer(buffer, position);
        position += P61MontLrdEnd2000.Len.P61_MONT_LRD_END2000;
        p61PreLrdEnd2000.setP61PreLrdEnd2000FromBuffer(buffer, position);
        position += P61PreLrdEnd2000.Len.P61_PRE_LRD_END2000;
        p61RendtoLrdEnd2000.setP61RendtoLrdEnd2000FromBuffer(buffer, position);
        position += P61RendtoLrdEnd2000.Len.P61_RENDTO_LRD_END2000;
        p61MontLrdEnd2006.setP61MontLrdEnd2006FromBuffer(buffer, position);
        position += P61MontLrdEnd2006.Len.P61_MONT_LRD_END2006;
        p61PreLrdEnd2006.setP61PreLrdEnd2006FromBuffer(buffer, position);
        position += P61PreLrdEnd2006.Len.P61_PRE_LRD_END2006;
        p61RendtoLrdEnd2006.setP61RendtoLrdEnd2006FromBuffer(buffer, position);
        position += P61RendtoLrdEnd2006.Len.P61_RENDTO_LRD_END2006;
        p61MontLrdDal2007.setP61MontLrdDal2007FromBuffer(buffer, position);
        position += P61MontLrdDal2007.Len.P61_MONT_LRD_DAL2007;
        p61PreLrdDal2007.setP61PreLrdDal2007FromBuffer(buffer, position);
        position += P61PreLrdDal2007.Len.P61_PRE_LRD_DAL2007;
        p61RendtoLrdDal2007.setP61RendtoLrdDal2007FromBuffer(buffer, position);
        position += P61RendtoLrdDal2007.Len.P61_RENDTO_LRD_DAL2007;
        p61IdTrchDiGar.setP61IdTrchDiGarFromBuffer(buffer, position);
    }

    public byte[] getdCristBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, p61IdDCrist, Len.Int.P61_ID_D_CRIST, 0);
        position += Len.P61_ID_D_CRIST;
        MarshalByte.writeIntAsPacked(buffer, position, p61IdPoli, Len.Int.P61_ID_POLI, 0);
        position += Len.P61_ID_POLI;
        MarshalByte.writeIntAsPacked(buffer, position, p61CodCompAnia, Len.Int.P61_COD_COMP_ANIA, 0);
        position += Len.P61_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, p61IdMoviCrz, Len.Int.P61_ID_MOVI_CRZ, 0);
        position += Len.P61_ID_MOVI_CRZ;
        p61IdMoviChiu.getP61IdMoviChiuAsBuffer(buffer, position);
        position += P61IdMoviChiu.Len.P61_ID_MOVI_CHIU;
        MarshalByte.writeIntAsPacked(buffer, position, p61DtIniEff, Len.Int.P61_DT_INI_EFF, 0);
        position += Len.P61_DT_INI_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, p61DtEndEff, Len.Int.P61_DT_END_EFF, 0);
        position += Len.P61_DT_END_EFF;
        MarshalByte.writeString(buffer, position, p61CodProd, Len.P61_COD_PROD);
        position += Len.P61_COD_PROD;
        MarshalByte.writeIntAsPacked(buffer, position, p61DtDecor, Len.Int.P61_DT_DECOR, 0);
        position += Len.P61_DT_DECOR;
        MarshalByte.writeLongAsPacked(buffer, position, p61DsRiga, Len.Int.P61_DS_RIGA, 0);
        position += Len.P61_DS_RIGA;
        MarshalByte.writeChar(buffer, position, p61DsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, p61DsVer, Len.Int.P61_DS_VER, 0);
        position += Len.P61_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, p61DsTsIniCptz, Len.Int.P61_DS_TS_INI_CPTZ, 0);
        position += Len.P61_DS_TS_INI_CPTZ;
        MarshalByte.writeLongAsPacked(buffer, position, p61DsTsEndCptz, Len.Int.P61_DS_TS_END_CPTZ, 0);
        position += Len.P61_DS_TS_END_CPTZ;
        MarshalByte.writeString(buffer, position, p61DsUtente, Len.P61_DS_UTENTE);
        position += Len.P61_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, p61DsStatoElab);
        position += Types.CHAR_SIZE;
        p61RisMat31122011.getP61RisMat31122011AsBuffer(buffer, position);
        position += P61RisMat31122011.Len.P61_RIS_MAT31122011;
        p61PreV31122011.getP61PreV31122011AsBuffer(buffer, position);
        position += P61PreV31122011.Len.P61_PRE_V31122011;
        p61PreRshV31122011.getP61PreRshV31122011AsBuffer(buffer, position);
        position += P61PreRshV31122011.Len.P61_PRE_RSH_V31122011;
        p61CptRivto31122011.getP61CptRivto31122011AsBuffer(buffer, position);
        position += P61CptRivto31122011.Len.P61_CPT_RIVTO31122011;
        p61ImpbVis31122011.getP61ImpbVis31122011AsBuffer(buffer, position);
        position += P61ImpbVis31122011.Len.P61_IMPB_VIS31122011;
        p61ImpbIs31122011.getP61ImpbIs31122011AsBuffer(buffer, position);
        position += P61ImpbIs31122011.Len.P61_IMPB_IS31122011;
        p61ImpbVisRpP2011.getP61ImpbVisRpP2011AsBuffer(buffer, position);
        position += P61ImpbVisRpP2011.Len.P61_IMPB_VIS_RP_P2011;
        p61ImpbIsRpP2011.getP61ImpbIsRpP2011AsBuffer(buffer, position);
        position += P61ImpbIsRpP2011.Len.P61_IMPB_IS_RP_P2011;
        p61PreV30062014.getP61PreV30062014AsBuffer(buffer, position);
        position += P61PreV30062014.Len.P61_PRE_V30062014;
        p61PreRshV30062014.getP61PreRshV30062014AsBuffer(buffer, position);
        position += P61PreRshV30062014.Len.P61_PRE_RSH_V30062014;
        p61CptIni30062014.getP61CptIni30062014AsBuffer(buffer, position);
        position += P61CptIni30062014.Len.P61_CPT_INI30062014;
        p61ImpbVis30062014.getP61ImpbVis30062014AsBuffer(buffer, position);
        position += P61ImpbVis30062014.Len.P61_IMPB_VIS30062014;
        p61ImpbIs30062014.getP61ImpbIs30062014AsBuffer(buffer, position);
        position += P61ImpbIs30062014.Len.P61_IMPB_IS30062014;
        p61ImpbVisRpP62014.getP61ImpbVisRpP62014AsBuffer(buffer, position);
        position += P61ImpbVisRpP62014.Len.P61_IMPB_VIS_RP_P62014;
        p61ImpbIsRpP62014.getP61ImpbIsRpP62014AsBuffer(buffer, position);
        position += P61ImpbIsRpP62014.Len.P61_IMPB_IS_RP_P62014;
        p61RisMat30062014.getP61RisMat30062014AsBuffer(buffer, position);
        position += P61RisMat30062014.Len.P61_RIS_MAT30062014;
        p61IdAdes.getP61IdAdesAsBuffer(buffer, position);
        position += P61IdAdes.Len.P61_ID_ADES;
        p61MontLrdEnd2000.getP61MontLrdEnd2000AsBuffer(buffer, position);
        position += P61MontLrdEnd2000.Len.P61_MONT_LRD_END2000;
        p61PreLrdEnd2000.getP61PreLrdEnd2000AsBuffer(buffer, position);
        position += P61PreLrdEnd2000.Len.P61_PRE_LRD_END2000;
        p61RendtoLrdEnd2000.getP61RendtoLrdEnd2000AsBuffer(buffer, position);
        position += P61RendtoLrdEnd2000.Len.P61_RENDTO_LRD_END2000;
        p61MontLrdEnd2006.getP61MontLrdEnd2006AsBuffer(buffer, position);
        position += P61MontLrdEnd2006.Len.P61_MONT_LRD_END2006;
        p61PreLrdEnd2006.getP61PreLrdEnd2006AsBuffer(buffer, position);
        position += P61PreLrdEnd2006.Len.P61_PRE_LRD_END2006;
        p61RendtoLrdEnd2006.getP61RendtoLrdEnd2006AsBuffer(buffer, position);
        position += P61RendtoLrdEnd2006.Len.P61_RENDTO_LRD_END2006;
        p61MontLrdDal2007.getP61MontLrdDal2007AsBuffer(buffer, position);
        position += P61MontLrdDal2007.Len.P61_MONT_LRD_DAL2007;
        p61PreLrdDal2007.getP61PreLrdDal2007AsBuffer(buffer, position);
        position += P61PreLrdDal2007.Len.P61_PRE_LRD_DAL2007;
        p61RendtoLrdDal2007.getP61RendtoLrdDal2007AsBuffer(buffer, position);
        position += P61RendtoLrdDal2007.Len.P61_RENDTO_LRD_DAL2007;
        p61IdTrchDiGar.getP61IdTrchDiGarAsBuffer(buffer, position);
        return buffer;
    }

    public void setP61IdDCrist(int p61IdDCrist) {
        this.p61IdDCrist = p61IdDCrist;
    }

    public int getP61IdDCrist() {
        return this.p61IdDCrist;
    }

    public void setP61IdPoli(int p61IdPoli) {
        this.p61IdPoli = p61IdPoli;
    }

    public int getP61IdPoli() {
        return this.p61IdPoli;
    }

    public void setP61CodCompAnia(int p61CodCompAnia) {
        this.p61CodCompAnia = p61CodCompAnia;
    }

    public int getP61CodCompAnia() {
        return this.p61CodCompAnia;
    }

    public void setP61IdMoviCrz(int p61IdMoviCrz) {
        this.p61IdMoviCrz = p61IdMoviCrz;
    }

    public int getP61IdMoviCrz() {
        return this.p61IdMoviCrz;
    }

    public void setP61DtIniEff(int p61DtIniEff) {
        this.p61DtIniEff = p61DtIniEff;
    }

    public int getP61DtIniEff() {
        return this.p61DtIniEff;
    }

    public void setP61DtEndEff(int p61DtEndEff) {
        this.p61DtEndEff = p61DtEndEff;
    }

    public int getP61DtEndEff() {
        return this.p61DtEndEff;
    }

    public void setP61CodProd(String p61CodProd) {
        this.p61CodProd = Functions.subString(p61CodProd, Len.P61_COD_PROD);
    }

    public String getP61CodProd() {
        return this.p61CodProd;
    }

    public void setP61DtDecor(int p61DtDecor) {
        this.p61DtDecor = p61DtDecor;
    }

    public int getP61DtDecor() {
        return this.p61DtDecor;
    }

    public void setP61DsRiga(long p61DsRiga) {
        this.p61DsRiga = p61DsRiga;
    }

    public long getP61DsRiga() {
        return this.p61DsRiga;
    }

    public void setP61DsOperSql(char p61DsOperSql) {
        this.p61DsOperSql = p61DsOperSql;
    }

    public void setP61DsOperSqlFormatted(String p61DsOperSql) {
        setP61DsOperSql(Functions.charAt(p61DsOperSql, Types.CHAR_SIZE));
    }

    public char getP61DsOperSql() {
        return this.p61DsOperSql;
    }

    public void setP61DsVer(int p61DsVer) {
        this.p61DsVer = p61DsVer;
    }

    public int getP61DsVer() {
        return this.p61DsVer;
    }

    public void setP61DsTsIniCptz(long p61DsTsIniCptz) {
        this.p61DsTsIniCptz = p61DsTsIniCptz;
    }

    public long getP61DsTsIniCptz() {
        return this.p61DsTsIniCptz;
    }

    public void setP61DsTsEndCptz(long p61DsTsEndCptz) {
        this.p61DsTsEndCptz = p61DsTsEndCptz;
    }

    public long getP61DsTsEndCptz() {
        return this.p61DsTsEndCptz;
    }

    public void setP61DsUtente(String p61DsUtente) {
        this.p61DsUtente = Functions.subString(p61DsUtente, Len.P61_DS_UTENTE);
    }

    public String getP61DsUtente() {
        return this.p61DsUtente;
    }

    public void setP61DsStatoElab(char p61DsStatoElab) {
        this.p61DsStatoElab = p61DsStatoElab;
    }

    public void setP61DsStatoElabFormatted(String p61DsStatoElab) {
        setP61DsStatoElab(Functions.charAt(p61DsStatoElab, Types.CHAR_SIZE));
    }

    public char getP61DsStatoElab() {
        return this.p61DsStatoElab;
    }

    public P61CptIni30062014 getP61CptIni30062014() {
        return p61CptIni30062014;
    }

    public P61CptRivto31122011 getP61CptRivto31122011() {
        return p61CptRivto31122011;
    }

    public P61IdAdes getP61IdAdes() {
        return p61IdAdes;
    }

    public P61IdMoviChiu getP61IdMoviChiu() {
        return p61IdMoviChiu;
    }

    public P61IdTrchDiGar getP61IdTrchDiGar() {
        return p61IdTrchDiGar;
    }

    public P61ImpbIs30062014 getP61ImpbIs30062014() {
        return p61ImpbIs30062014;
    }

    public P61ImpbIs31122011 getP61ImpbIs31122011() {
        return p61ImpbIs31122011;
    }

    public P61ImpbIsRpP2011 getP61ImpbIsRpP2011() {
        return p61ImpbIsRpP2011;
    }

    public P61ImpbIsRpP62014 getP61ImpbIsRpP62014() {
        return p61ImpbIsRpP62014;
    }

    public P61ImpbVis30062014 getP61ImpbVis30062014() {
        return p61ImpbVis30062014;
    }

    public P61ImpbVis31122011 getP61ImpbVis31122011() {
        return p61ImpbVis31122011;
    }

    public P61ImpbVisRpP2011 getP61ImpbVisRpP2011() {
        return p61ImpbVisRpP2011;
    }

    public P61ImpbVisRpP62014 getP61ImpbVisRpP62014() {
        return p61ImpbVisRpP62014;
    }

    public P61MontLrdDal2007 getP61MontLrdDal2007() {
        return p61MontLrdDal2007;
    }

    public P61MontLrdEnd2000 getP61MontLrdEnd2000() {
        return p61MontLrdEnd2000;
    }

    public P61MontLrdEnd2006 getP61MontLrdEnd2006() {
        return p61MontLrdEnd2006;
    }

    public P61PreLrdDal2007 getP61PreLrdDal2007() {
        return p61PreLrdDal2007;
    }

    public P61PreLrdEnd2000 getP61PreLrdEnd2000() {
        return p61PreLrdEnd2000;
    }

    public P61PreLrdEnd2006 getP61PreLrdEnd2006() {
        return p61PreLrdEnd2006;
    }

    public P61PreRshV30062014 getP61PreRshV30062014() {
        return p61PreRshV30062014;
    }

    public P61PreRshV31122011 getP61PreRshV31122011() {
        return p61PreRshV31122011;
    }

    public P61PreV30062014 getP61PreV30062014() {
        return p61PreV30062014;
    }

    public P61PreV31122011 getP61PreV31122011() {
        return p61PreV31122011;
    }

    public P61RendtoLrdDal2007 getP61RendtoLrdDal2007() {
        return p61RendtoLrdDal2007;
    }

    public P61RendtoLrdEnd2000 getP61RendtoLrdEnd2000() {
        return p61RendtoLrdEnd2000;
    }

    public P61RendtoLrdEnd2006 getP61RendtoLrdEnd2006() {
        return p61RendtoLrdEnd2006;
    }

    public P61RisMat30062014 getP61RisMat30062014() {
        return p61RisMat30062014;
    }

    public P61RisMat31122011 getP61RisMat31122011() {
        return p61RisMat31122011;
    }

    @Override
    public byte[] serialize() {
        return getdCristBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int P61_ID_D_CRIST = 5;
        public static final int P61_ID_POLI = 5;
        public static final int P61_COD_COMP_ANIA = 3;
        public static final int P61_ID_MOVI_CRZ = 5;
        public static final int P61_DT_INI_EFF = 5;
        public static final int P61_DT_END_EFF = 5;
        public static final int P61_COD_PROD = 12;
        public static final int P61_DT_DECOR = 5;
        public static final int P61_DS_RIGA = 6;
        public static final int P61_DS_OPER_SQL = 1;
        public static final int P61_DS_VER = 5;
        public static final int P61_DS_TS_INI_CPTZ = 10;
        public static final int P61_DS_TS_END_CPTZ = 10;
        public static final int P61_DS_UTENTE = 20;
        public static final int P61_DS_STATO_ELAB = 1;
        public static final int D_CRIST = P61_ID_D_CRIST + P61_ID_POLI + P61_COD_COMP_ANIA + P61_ID_MOVI_CRZ + P61IdMoviChiu.Len.P61_ID_MOVI_CHIU + P61_DT_INI_EFF + P61_DT_END_EFF + P61_COD_PROD + P61_DT_DECOR + P61_DS_RIGA + P61_DS_OPER_SQL + P61_DS_VER + P61_DS_TS_INI_CPTZ + P61_DS_TS_END_CPTZ + P61_DS_UTENTE + P61_DS_STATO_ELAB + P61RisMat31122011.Len.P61_RIS_MAT31122011 + P61PreV31122011.Len.P61_PRE_V31122011 + P61PreRshV31122011.Len.P61_PRE_RSH_V31122011 + P61CptRivto31122011.Len.P61_CPT_RIVTO31122011 + P61ImpbVis31122011.Len.P61_IMPB_VIS31122011 + P61ImpbIs31122011.Len.P61_IMPB_IS31122011 + P61ImpbVisRpP2011.Len.P61_IMPB_VIS_RP_P2011 + P61ImpbIsRpP2011.Len.P61_IMPB_IS_RP_P2011 + P61PreV30062014.Len.P61_PRE_V30062014 + P61PreRshV30062014.Len.P61_PRE_RSH_V30062014 + P61CptIni30062014.Len.P61_CPT_INI30062014 + P61ImpbVis30062014.Len.P61_IMPB_VIS30062014 + P61ImpbIs30062014.Len.P61_IMPB_IS30062014 + P61ImpbVisRpP62014.Len.P61_IMPB_VIS_RP_P62014 + P61ImpbIsRpP62014.Len.P61_IMPB_IS_RP_P62014 + P61RisMat30062014.Len.P61_RIS_MAT30062014 + P61IdAdes.Len.P61_ID_ADES + P61MontLrdEnd2000.Len.P61_MONT_LRD_END2000 + P61PreLrdEnd2000.Len.P61_PRE_LRD_END2000 + P61RendtoLrdEnd2000.Len.P61_RENDTO_LRD_END2000 + P61MontLrdEnd2006.Len.P61_MONT_LRD_END2006 + P61PreLrdEnd2006.Len.P61_PRE_LRD_END2006 + P61RendtoLrdEnd2006.Len.P61_RENDTO_LRD_END2006 + P61MontLrdDal2007.Len.P61_MONT_LRD_DAL2007 + P61PreLrdDal2007.Len.P61_PRE_LRD_DAL2007 + P61RendtoLrdDal2007.Len.P61_RENDTO_LRD_DAL2007 + P61IdTrchDiGar.Len.P61_ID_TRCH_DI_GAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int P61_ID_D_CRIST = 9;
            public static final int P61_ID_POLI = 9;
            public static final int P61_COD_COMP_ANIA = 5;
            public static final int P61_ID_MOVI_CRZ = 9;
            public static final int P61_DT_INI_EFF = 8;
            public static final int P61_DT_END_EFF = 8;
            public static final int P61_DT_DECOR = 8;
            public static final int P61_DS_RIGA = 10;
            public static final int P61_DS_VER = 9;
            public static final int P61_DS_TS_INI_CPTZ = 18;
            public static final int P61_DS_TS_END_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
