package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-INTR-FRAZ<br>
 * Variable: TIT-TOT-INTR-FRAZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotIntrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotIntrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_INTR_FRAZ;
    }

    public void setTitTotIntrFraz(AfDecimal titTotIntrFraz) {
        writeDecimalAsPacked(Pos.TIT_TOT_INTR_FRAZ, titTotIntrFraz.copy());
    }

    public void setTitTotIntrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_INTR_FRAZ, Pos.TIT_TOT_INTR_FRAZ);
    }

    /**Original name: TIT-TOT-INTR-FRAZ<br>*/
    public AfDecimal getTitTotIntrFraz() {
        return readPackedAsDecimal(Pos.TIT_TOT_INTR_FRAZ, Len.Int.TIT_TOT_INTR_FRAZ, Len.Fract.TIT_TOT_INTR_FRAZ);
    }

    public byte[] getTitTotIntrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_INTR_FRAZ, Pos.TIT_TOT_INTR_FRAZ);
        return buffer;
    }

    public void setTitTotIntrFrazNull(String titTotIntrFrazNull) {
        writeString(Pos.TIT_TOT_INTR_FRAZ_NULL, titTotIntrFrazNull, Len.TIT_TOT_INTR_FRAZ_NULL);
    }

    /**Original name: TIT-TOT-INTR-FRAZ-NULL<br>*/
    public String getTitTotIntrFrazNull() {
        return readString(Pos.TIT_TOT_INTR_FRAZ_NULL, Len.TIT_TOT_INTR_FRAZ_NULL);
    }

    public String getTitTotIntrFrazNullFormatted() {
        return Functions.padBlanks(getTitTotIntrFrazNull(), Len.TIT_TOT_INTR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_FRAZ = 1;
        public static final int TIT_TOT_INTR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_INTR_FRAZ = 8;
        public static final int TIT_TOT_INTR_FRAZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_FRAZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_INTR_FRAZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
