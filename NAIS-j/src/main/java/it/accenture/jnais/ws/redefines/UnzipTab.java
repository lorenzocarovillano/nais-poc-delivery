package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: UNZIP-TAB<br>
 * Variable: UNZIP-TAB from program IVVS0212<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class UnzipTab extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int TAB_VARIABILI_MAXOCCURS = 100;

    //==== CONSTRUCTORS ====
    public UnzipTab() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.UNZIP_TAB;
    }

    public String getUnzipTabFormatted() {
        return readFixedString(Pos.UNZIP_TAB, Len.UNZIP_TAB);
    }

    public void setCodVariabile(int codVariabileIdx, String codVariabile) {
        int position = Pos.unzipCodVariabile(codVariabileIdx - 1);
        writeString(position, codVariabile, Len.COD_VARIABILE);
    }

    /**Original name: UNZIP-COD-VARIABILE<br>*/
    public String getCodVariabile(int codVariabileIdx) {
        int position = Pos.unzipCodVariabile(codVariabileIdx - 1);
        return readString(position, Len.COD_VARIABILE);
    }

    public String getCodVariabileFormatted(int codVariabileIdx) {
        return Functions.padBlanks(getCodVariabile(codVariabileIdx), Len.COD_VARIABILE);
    }

    public void setRestoTab(String restoTab) {
        writeString(Pos.RESTO_TAB, restoTab, Len.RESTO_TAB);
    }

    /**Original name: UNZIP-RESTO-TAB<br>*/
    public String getRestoTab() {
        return readString(Pos.RESTO_TAB, Len.RESTO_TAB);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int UNZIP_TAB = 1;
        public static final int UNZIP_TAB_R = 1;
        public static final int FLR1 = UNZIP_TAB_R;
        public static final int RESTO_TAB = FLR1 + Len.FLR1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int unzipTabVariabili(int idx) {
            return UNZIP_TAB + idx * Len.TAB_VARIABILI;
        }

        public static int unzipCodVariabile(int idx) {
            return unzipTabVariabili(idx);
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_VARIABILE = 30;
        public static final int TAB_VARIABILI = COD_VARIABILE;
        public static final int FLR1 = 30;
        public static final int UNZIP_TAB = UnzipTab.TAB_VARIABILI_MAXOCCURS * TAB_VARIABILI;
        public static final int RESTO_TAB = 2970;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
