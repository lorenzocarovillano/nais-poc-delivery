package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-CONT-PREMIO-NETTO<br>
 * Variable: WPAG-CONT-PREMIO-NETTO from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagContPremioNetto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagContPremioNetto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_CONT_PREMIO_NETTO;
    }

    public void setWpagContPremioNetto(AfDecimal wpagContPremioNetto) {
        writeDecimalAsPacked(Pos.WPAG_CONT_PREMIO_NETTO, wpagContPremioNetto.copy());
    }

    public void setWpagContPremioNettoFormatted(String wpagContPremioNetto) {
        setWpagContPremioNetto(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_CONT_PREMIO_NETTO + Len.Fract.WPAG_CONT_PREMIO_NETTO, Len.Fract.WPAG_CONT_PREMIO_NETTO, wpagContPremioNetto));
    }

    public void setWpagContPremioNettoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_CONT_PREMIO_NETTO, Pos.WPAG_CONT_PREMIO_NETTO);
    }

    /**Original name: WPAG-CONT-PREMIO-NETTO<br>*/
    public AfDecimal getWpagContPremioNetto() {
        return readPackedAsDecimal(Pos.WPAG_CONT_PREMIO_NETTO, Len.Int.WPAG_CONT_PREMIO_NETTO, Len.Fract.WPAG_CONT_PREMIO_NETTO);
    }

    public byte[] getWpagContPremioNettoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_CONT_PREMIO_NETTO, Pos.WPAG_CONT_PREMIO_NETTO);
        return buffer;
    }

    public void initWpagContPremioNettoSpaces() {
        fill(Pos.WPAG_CONT_PREMIO_NETTO, Len.WPAG_CONT_PREMIO_NETTO, Types.SPACE_CHAR);
    }

    public void setWpagContPremioNettoNull(String wpagContPremioNettoNull) {
        writeString(Pos.WPAG_CONT_PREMIO_NETTO_NULL, wpagContPremioNettoNull, Len.WPAG_CONT_PREMIO_NETTO_NULL);
    }

    /**Original name: WPAG-CONT-PREMIO-NETTO-NULL<br>*/
    public String getWpagContPremioNettoNull() {
        return readString(Pos.WPAG_CONT_PREMIO_NETTO_NULL, Len.WPAG_CONT_PREMIO_NETTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_NETTO = 1;
        public static final int WPAG_CONT_PREMIO_NETTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_CONT_PREMIO_NETTO = 8;
        public static final int WPAG_CONT_PREMIO_NETTO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_NETTO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_CONT_PREMIO_NETTO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
