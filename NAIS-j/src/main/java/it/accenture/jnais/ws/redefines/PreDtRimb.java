package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-DT-RIMB<br>
 * Variable: PRE-DT-RIMB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreDtRimb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreDtRimb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_DT_RIMB;
    }

    public void setPreDtRimb(int preDtRimb) {
        writeIntAsPacked(Pos.PRE_DT_RIMB, preDtRimb, Len.Int.PRE_DT_RIMB);
    }

    public void setPreDtRimbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_DT_RIMB, Pos.PRE_DT_RIMB);
    }

    /**Original name: PRE-DT-RIMB<br>*/
    public int getPreDtRimb() {
        return readPackedAsInt(Pos.PRE_DT_RIMB, Len.Int.PRE_DT_RIMB);
    }

    public byte[] getPreDtRimbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_DT_RIMB, Pos.PRE_DT_RIMB);
        return buffer;
    }

    public void setPreDtRimbNull(String preDtRimbNull) {
        writeString(Pos.PRE_DT_RIMB_NULL, preDtRimbNull, Len.PRE_DT_RIMB_NULL);
    }

    /**Original name: PRE-DT-RIMB-NULL<br>*/
    public String getPreDtRimbNull() {
        return readString(Pos.PRE_DT_RIMB_NULL, Len.PRE_DT_RIMB_NULL);
    }

    public String getPreDtRimbNullFormatted() {
        return Functions.padBlanks(getPreDtRimbNull(), Len.PRE_DT_RIMB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_DT_RIMB = 1;
        public static final int PRE_DT_RIMB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_DT_RIMB = 5;
        public static final int PRE_DT_RIMB_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_DT_RIMB = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
