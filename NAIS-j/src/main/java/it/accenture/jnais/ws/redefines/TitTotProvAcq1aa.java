package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-PROV-ACQ-1AA<br>
 * Variable: TIT-TOT-PROV-ACQ-1AA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotProvAcq1aa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotProvAcq1aa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_PROV_ACQ1AA;
    }

    public void setTitTotProvAcq1aa(AfDecimal titTotProvAcq1aa) {
        writeDecimalAsPacked(Pos.TIT_TOT_PROV_ACQ1AA, titTotProvAcq1aa.copy());
    }

    public void setTitTotProvAcq1aaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_PROV_ACQ1AA, Pos.TIT_TOT_PROV_ACQ1AA);
    }

    /**Original name: TIT-TOT-PROV-ACQ-1AA<br>*/
    public AfDecimal getTitTotProvAcq1aa() {
        return readPackedAsDecimal(Pos.TIT_TOT_PROV_ACQ1AA, Len.Int.TIT_TOT_PROV_ACQ1AA, Len.Fract.TIT_TOT_PROV_ACQ1AA);
    }

    public byte[] getTitTotProvAcq1aaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_PROV_ACQ1AA, Pos.TIT_TOT_PROV_ACQ1AA);
        return buffer;
    }

    public void setTitTotProvAcq1aaNull(String titTotProvAcq1aaNull) {
        writeString(Pos.TIT_TOT_PROV_ACQ1AA_NULL, titTotProvAcq1aaNull, Len.TIT_TOT_PROV_ACQ1AA_NULL);
    }

    /**Original name: TIT-TOT-PROV-ACQ-1AA-NULL<br>*/
    public String getTitTotProvAcq1aaNull() {
        return readString(Pos.TIT_TOT_PROV_ACQ1AA_NULL, Len.TIT_TOT_PROV_ACQ1AA_NULL);
    }

    public String getTitTotProvAcq1aaNullFormatted() {
        return Functions.padBlanks(getTitTotProvAcq1aaNull(), Len.TIT_TOT_PROV_ACQ1AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_ACQ1AA = 1;
        public static final int TIT_TOT_PROV_ACQ1AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_PROV_ACQ1AA = 8;
        public static final int TIT_TOT_PROV_ACQ1AA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_ACQ1AA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_PROV_ACQ1AA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
