package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-TRANCHE<br>
 * Variable: SW-TRANCHE from program LLBS0230<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwTranche {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATA = 'S';
    public static final char NO_TROVATA = 'N';

    //==== METHODS ====
    public void setSwTranche(char swTranche) {
        this.value = swTranche;
    }

    public char getSwTranche() {
        return this.value;
    }

    public boolean isTrovata() {
        return value == TROVATA;
    }

    public void setTrovata() {
        value = TROVATA;
    }

    public void setNoTrovata() {
        value = NO_TROVATA;
    }
}
