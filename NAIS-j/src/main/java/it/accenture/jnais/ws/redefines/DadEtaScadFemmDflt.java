package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DAD-ETA-SCAD-FEMM-DFLT<br>
 * Variable: DAD-ETA-SCAD-FEMM-DFLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DadEtaScadFemmDflt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DadEtaScadFemmDflt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DAD_ETA_SCAD_FEMM_DFLT;
    }

    public void setDadEtaScadFemmDflt(int dadEtaScadFemmDflt) {
        writeIntAsPacked(Pos.DAD_ETA_SCAD_FEMM_DFLT, dadEtaScadFemmDflt, Len.Int.DAD_ETA_SCAD_FEMM_DFLT);
    }

    public void setDadEtaScadFemmDfltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DAD_ETA_SCAD_FEMM_DFLT, Pos.DAD_ETA_SCAD_FEMM_DFLT);
    }

    /**Original name: DAD-ETA-SCAD-FEMM-DFLT<br>*/
    public int getDadEtaScadFemmDflt() {
        return readPackedAsInt(Pos.DAD_ETA_SCAD_FEMM_DFLT, Len.Int.DAD_ETA_SCAD_FEMM_DFLT);
    }

    public byte[] getDadEtaScadFemmDfltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DAD_ETA_SCAD_FEMM_DFLT, Pos.DAD_ETA_SCAD_FEMM_DFLT);
        return buffer;
    }

    public void setDadEtaScadFemmDfltNull(String dadEtaScadFemmDfltNull) {
        writeString(Pos.DAD_ETA_SCAD_FEMM_DFLT_NULL, dadEtaScadFemmDfltNull, Len.DAD_ETA_SCAD_FEMM_DFLT_NULL);
    }

    /**Original name: DAD-ETA-SCAD-FEMM-DFLT-NULL<br>*/
    public String getDadEtaScadFemmDfltNull() {
        return readString(Pos.DAD_ETA_SCAD_FEMM_DFLT_NULL, Len.DAD_ETA_SCAD_FEMM_DFLT_NULL);
    }

    public String getDadEtaScadFemmDfltNullFormatted() {
        return Functions.padBlanks(getDadEtaScadFemmDfltNull(), Len.DAD_ETA_SCAD_FEMM_DFLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DAD_ETA_SCAD_FEMM_DFLT = 1;
        public static final int DAD_ETA_SCAD_FEMM_DFLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DAD_ETA_SCAD_FEMM_DFLT = 3;
        public static final int DAD_ETA_SCAD_FEMM_DFLT_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DAD_ETA_SCAD_FEMM_DFLT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
