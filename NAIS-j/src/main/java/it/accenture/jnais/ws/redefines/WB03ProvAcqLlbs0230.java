package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PROV-ACQ<br>
 * Variable: W-B03-PROV-ACQ from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03ProvAcqLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03ProvAcqLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PROV_ACQ;
    }

    public void setwB03ProvAcq(AfDecimal wB03ProvAcq) {
        writeDecimalAsPacked(Pos.W_B03_PROV_ACQ, wB03ProvAcq.copy());
    }

    /**Original name: W-B03-PROV-ACQ<br>*/
    public AfDecimal getwB03ProvAcq() {
        return readPackedAsDecimal(Pos.W_B03_PROV_ACQ, Len.Int.W_B03_PROV_ACQ, Len.Fract.W_B03_PROV_ACQ);
    }

    public byte[] getwB03ProvAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PROV_ACQ, Pos.W_B03_PROV_ACQ);
        return buffer;
    }

    public void setwB03ProvAcqNull(String wB03ProvAcqNull) {
        writeString(Pos.W_B03_PROV_ACQ_NULL, wB03ProvAcqNull, Len.W_B03_PROV_ACQ_NULL);
    }

    /**Original name: W-B03-PROV-ACQ-NULL<br>*/
    public String getwB03ProvAcqNull() {
        return readString(Pos.W_B03_PROV_ACQ_NULL, Len.W_B03_PROV_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PROV_ACQ = 1;
        public static final int W_B03_PROV_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PROV_ACQ = 8;
        public static final int W_B03_PROV_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PROV_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PROV_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
