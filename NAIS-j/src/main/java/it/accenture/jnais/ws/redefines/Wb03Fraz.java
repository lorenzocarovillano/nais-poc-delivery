package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-FRAZ<br>
 * Variable: WB03-FRAZ from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Fraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Fraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_FRAZ;
    }

    public void setWb03Fraz(int wb03Fraz) {
        writeIntAsPacked(Pos.WB03_FRAZ, wb03Fraz, Len.Int.WB03_FRAZ);
    }

    public void setWb03FrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_FRAZ, Pos.WB03_FRAZ);
    }

    /**Original name: WB03-FRAZ<br>*/
    public int getWb03Fraz() {
        return readPackedAsInt(Pos.WB03_FRAZ, Len.Int.WB03_FRAZ);
    }

    public byte[] getWb03FrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_FRAZ, Pos.WB03_FRAZ);
        return buffer;
    }

    public void setWb03FrazNull(String wb03FrazNull) {
        writeString(Pos.WB03_FRAZ_NULL, wb03FrazNull, Len.WB03_FRAZ_NULL);
    }

    /**Original name: WB03-FRAZ-NULL<br>*/
    public String getWb03FrazNull() {
        return readString(Pos.WB03_FRAZ_NULL, Len.WB03_FRAZ_NULL);
    }

    public String getWb03FrazNullFormatted() {
        return Functions.padBlanks(getWb03FrazNull(), Len.WB03_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_FRAZ = 1;
        public static final int WB03_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_FRAZ = 3;
        public static final int WB03_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
