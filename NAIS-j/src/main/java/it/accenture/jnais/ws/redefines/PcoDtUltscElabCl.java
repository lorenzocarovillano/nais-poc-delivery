package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULTSC-ELAB-CL<br>
 * Variable: PCO-DT-ULTSC-ELAB-CL from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltscElabCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltscElabCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULTSC_ELAB_CL;
    }

    public void setPcoDtUltscElabCl(int pcoDtUltscElabCl) {
        writeIntAsPacked(Pos.PCO_DT_ULTSC_ELAB_CL, pcoDtUltscElabCl, Len.Int.PCO_DT_ULTSC_ELAB_CL);
    }

    public void setPcoDtUltscElabClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULTSC_ELAB_CL, Pos.PCO_DT_ULTSC_ELAB_CL);
    }

    /**Original name: PCO-DT-ULTSC-ELAB-CL<br>*/
    public int getPcoDtUltscElabCl() {
        return readPackedAsInt(Pos.PCO_DT_ULTSC_ELAB_CL, Len.Int.PCO_DT_ULTSC_ELAB_CL);
    }

    public byte[] getPcoDtUltscElabClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULTSC_ELAB_CL, Pos.PCO_DT_ULTSC_ELAB_CL);
        return buffer;
    }

    public void initPcoDtUltscElabClHighValues() {
        fill(Pos.PCO_DT_ULTSC_ELAB_CL, Len.PCO_DT_ULTSC_ELAB_CL, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltscElabClNull(String pcoDtUltscElabClNull) {
        writeString(Pos.PCO_DT_ULTSC_ELAB_CL_NULL, pcoDtUltscElabClNull, Len.PCO_DT_ULTSC_ELAB_CL_NULL);
    }

    /**Original name: PCO-DT-ULTSC-ELAB-CL-NULL<br>*/
    public String getPcoDtUltscElabClNull() {
        return readString(Pos.PCO_DT_ULTSC_ELAB_CL_NULL, Len.PCO_DT_ULTSC_ELAB_CL_NULL);
    }

    public String getPcoDtUltscElabClNullFormatted() {
        return Functions.padBlanks(getPcoDtUltscElabClNull(), Len.PCO_DT_ULTSC_ELAB_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_ELAB_CL = 1;
        public static final int PCO_DT_ULTSC_ELAB_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULTSC_ELAB_CL = 5;
        public static final int PCO_DT_ULTSC_ELAB_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULTSC_ELAB_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
