package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WK-APPO-DEC-V<br>
 * Variable: WK-APPO-DEC-V from program LVVS0000<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WkAppoDecV {

    //==== PROPERTIES ====
    //Original name: WK-NUMER-AP
    private String numerAp = DefaultValues.stringVal(Len.NUMER_AP);
    //Original name: WK-DECIM-AP
    private String decimAp = DefaultValues.stringVal(Len.DECIM_AP);

    //==== METHODS ====
    public void setWkAppoDec(AfDecimal wkAppoDec) {
        int position = 1;
        byte[] buffer = getWkAppoDecVBytes();
        MarshalByte.writeDecimal(buffer, position, wkAppoDec.copy(), SignType.NO_SIGN);
        setWkAppoDecVBytes(buffer);
    }

    /**Original name: WK-APPO-DEC<br>*/
    public AfDecimal getWkAppoDec() {
        int position = 1;
        return MarshalByte.readDecimal(getWkAppoDecVBytes(), position, Len.Int.WK_APPO_DEC, Len.Fract.WK_APPO_DEC, SignType.NO_SIGN);
    }

    public void setWkAppoDecVBytes(byte[] buffer) {
        setWkAppoDecVBytes(buffer, 1);
    }

    /**Original name: WK-APPO-DEC-V<br>*/
    public byte[] getWkAppoDecVBytes() {
        byte[] buffer = new byte[Len.WK_APPO_DEC_V];
        return getWkAppoDecVBytes(buffer, 1);
    }

    public void setWkAppoDecVBytes(byte[] buffer, int offset) {
        int position = offset;
        numerAp = MarshalByte.readFixedString(buffer, position, Len.NUMER_AP);
        position += Len.NUMER_AP;
        decimAp = MarshalByte.readFixedString(buffer, position, Len.DECIM_AP);
    }

    public byte[] getWkAppoDecVBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, numerAp, Len.NUMER_AP);
        position += Len.NUMER_AP;
        MarshalByte.writeString(buffer, position, decimAp, Len.DECIM_AP);
        return buffer;
    }

    public String getDecimApFormatted() {
        return this.decimAp;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int NUMER_AP = 1;
        public static final int DECIM_AP = 7;
        public static final int WK_APPO_DEC_V = NUMER_AP + DECIM_AP;
        public static final int WK_APPO_DEC = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WK_APPO_DEC = 1;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WK_APPO_DEC = 7;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
