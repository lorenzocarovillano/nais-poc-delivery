package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Ldbv5141TpTrch;

/**Original name: LDBVE261<br>
 * Variable: LDBVE261 from copybook LDBVE261<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class Ldbve261 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: LDBVE261-TP-TRCH
    private Ldbv5141TpTrch tpTrch = new Ldbv5141TpTrch();
    //Original name: LDBVE261-ID-ADES
    private int idAdes = DefaultValues.INT_VAL;
    //Original name: LDBVE261-ID-MOVI
    private int idMovi = DefaultValues.INT_VAL;
    //Original name: LDBVE261-IMPB-VIS-END2000
    private AfDecimal impbVisEnd2000 = new AfDecimal(DefaultValues.DEC_VAL, 15, 3);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LDBVE261;
    }

    @Override
    public void deserialize(byte[] buf) {
        setLdbve261Bytes(buf);
    }

    public String getLdbve261Formatted() {
        return MarshalByteExt.bufferToStr(getLdbve261Bytes());
    }

    public void setLdbve261Bytes(byte[] buffer) {
        setLdbve261Bytes(buffer, 1);
    }

    public byte[] getLdbve261Bytes() {
        byte[] buffer = new byte[Len.LDBVE261];
        return getLdbve261Bytes(buffer, 1);
    }

    public void setLdbve261Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpTrch.setTpTrchBytes(buffer, position);
        position += Ldbv5141TpTrch.Len.TP_TRCH;
        idAdes = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        idMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_MOVI, 0);
        position += Len.ID_MOVI;
        impbVisEnd2000.assign(MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.IMPB_VIS_END2000, Len.Fract.IMPB_VIS_END2000));
    }

    public byte[] getLdbve261Bytes(byte[] buffer, int offset) {
        int position = offset;
        tpTrch.getTpTrchBytes(buffer, position);
        position += Ldbv5141TpTrch.Len.TP_TRCH;
        MarshalByte.writeIntAsPacked(buffer, position, idAdes, Len.Int.ID_ADES, 0);
        position += Len.ID_ADES;
        MarshalByte.writeIntAsPacked(buffer, position, idMovi, Len.Int.ID_MOVI, 0);
        position += Len.ID_MOVI;
        MarshalByte.writeDecimalAsPacked(buffer, position, impbVisEnd2000.copy());
        return buffer;
    }

    public void setIdAdes(int idAdes) {
        this.idAdes = idAdes;
    }

    public int getIdAdes() {
        return this.idAdes;
    }

    public void setIdMovi(int idMovi) {
        this.idMovi = idMovi;
    }

    public int getIdMovi() {
        return this.idMovi;
    }

    public void setImpbVisEnd2000(AfDecimal impbVisEnd2000) {
        this.impbVisEnd2000.assign(impbVisEnd2000);
    }

    public AfDecimal getImpbVisEnd2000() {
        return this.impbVisEnd2000.copy();
    }

    public Ldbv5141TpTrch getTpTrch() {
        return tpTrch;
    }

    @Override
    public byte[] serialize() {
        return getLdbve261Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ADES = 5;
        public static final int ID_MOVI = 5;
        public static final int IMPB_VIS_END2000 = 8;
        public static final int LDBVE261 = Ldbv5141TpTrch.Len.TP_TRCH + ID_ADES + ID_MOVI + IMPB_VIS_END2000;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ADES = 9;
            public static final int ID_MOVI = 9;
            public static final int IMPB_VIS_END2000 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int IMPB_VIS_END2000 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
