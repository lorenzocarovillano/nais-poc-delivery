package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-DUR-AA<br>
 * Variable: L3401-DUR-AA from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401DurAa extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401DurAa() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_DUR_AA;
    }

    public void setL3401DurAa(int l3401DurAa) {
        writeIntAsPacked(Pos.L3401_DUR_AA, l3401DurAa, Len.Int.L3401_DUR_AA);
    }

    public void setL3401DurAaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_DUR_AA, Pos.L3401_DUR_AA);
    }

    /**Original name: L3401-DUR-AA<br>*/
    public int getL3401DurAa() {
        return readPackedAsInt(Pos.L3401_DUR_AA, Len.Int.L3401_DUR_AA);
    }

    public byte[] getL3401DurAaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_DUR_AA, Pos.L3401_DUR_AA);
        return buffer;
    }

    /**Original name: L3401-DUR-AA-NULL<br>*/
    public String getL3401DurAaNull() {
        return readString(Pos.L3401_DUR_AA_NULL, Len.L3401_DUR_AA_NULL);
    }

    public String getL3401DurAaNullFormatted() {
        return Functions.padBlanks(getL3401DurAaNull(), Len.L3401_DUR_AA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_DUR_AA = 1;
        public static final int L3401_DUR_AA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_DUR_AA = 3;
        public static final int L3401_DUR_AA_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_DUR_AA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
