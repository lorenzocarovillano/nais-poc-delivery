package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-FINE-LETTURA-P61<br>
 * Variable: FLAG-FINE-LETTURA-P61 from program LVVS2800<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagFineLetturaP61 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagFineLetturaP61(char flagFineLetturaP61) {
        this.value = flagFineLetturaP61;
    }

    public char getFlagFineLetturaP61() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
