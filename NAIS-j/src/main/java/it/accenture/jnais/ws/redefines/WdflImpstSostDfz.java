package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFL-IMPST-SOST-DFZ<br>
 * Variable: WDFL-IMPST-SOST-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdflImpstSostDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdflImpstSostDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFL_IMPST_SOST_DFZ;
    }

    public void setWdflImpstSostDfz(AfDecimal wdflImpstSostDfz) {
        writeDecimalAsPacked(Pos.WDFL_IMPST_SOST_DFZ, wdflImpstSostDfz.copy());
    }

    public void setWdflImpstSostDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFL_IMPST_SOST_DFZ, Pos.WDFL_IMPST_SOST_DFZ);
    }

    /**Original name: WDFL-IMPST-SOST-DFZ<br>*/
    public AfDecimal getWdflImpstSostDfz() {
        return readPackedAsDecimal(Pos.WDFL_IMPST_SOST_DFZ, Len.Int.WDFL_IMPST_SOST_DFZ, Len.Fract.WDFL_IMPST_SOST_DFZ);
    }

    public byte[] getWdflImpstSostDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFL_IMPST_SOST_DFZ, Pos.WDFL_IMPST_SOST_DFZ);
        return buffer;
    }

    public void setWdflImpstSostDfzNull(String wdflImpstSostDfzNull) {
        writeString(Pos.WDFL_IMPST_SOST_DFZ_NULL, wdflImpstSostDfzNull, Len.WDFL_IMPST_SOST_DFZ_NULL);
    }

    /**Original name: WDFL-IMPST-SOST-DFZ-NULL<br>*/
    public String getWdflImpstSostDfzNull() {
        return readString(Pos.WDFL_IMPST_SOST_DFZ_NULL, Len.WDFL_IMPST_SOST_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_SOST_DFZ = 1;
        public static final int WDFL_IMPST_SOST_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFL_IMPST_SOST_DFZ = 8;
        public static final int WDFL_IMPST_SOST_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_SOST_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFL_IMPST_SOST_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
