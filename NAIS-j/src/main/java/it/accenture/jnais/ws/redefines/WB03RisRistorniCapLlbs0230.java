package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-RIS-RISTORNI-CAP<br>
 * Variable: W-B03-RIS-RISTORNI-CAP from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03RisRistorniCapLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03RisRistorniCapLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_RIS_RISTORNI_CAP;
    }

    public void setwB03RisRistorniCap(AfDecimal wB03RisRistorniCap) {
        writeDecimalAsPacked(Pos.W_B03_RIS_RISTORNI_CAP, wB03RisRistorniCap.copy());
    }

    /**Original name: W-B03-RIS-RISTORNI-CAP<br>*/
    public AfDecimal getwB03RisRistorniCap() {
        return readPackedAsDecimal(Pos.W_B03_RIS_RISTORNI_CAP, Len.Int.W_B03_RIS_RISTORNI_CAP, Len.Fract.W_B03_RIS_RISTORNI_CAP);
    }

    public byte[] getwB03RisRistorniCapAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_RIS_RISTORNI_CAP, Pos.W_B03_RIS_RISTORNI_CAP);
        return buffer;
    }

    public void setwB03RisRistorniCapNull(String wB03RisRistorniCapNull) {
        writeString(Pos.W_B03_RIS_RISTORNI_CAP_NULL, wB03RisRistorniCapNull, Len.W_B03_RIS_RISTORNI_CAP_NULL);
    }

    /**Original name: W-B03-RIS-RISTORNI-CAP-NULL<br>*/
    public String getwB03RisRistorniCapNull() {
        return readString(Pos.W_B03_RIS_RISTORNI_CAP_NULL, Len.W_B03_RIS_RISTORNI_CAP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_RISTORNI_CAP = 1;
        public static final int W_B03_RIS_RISTORNI_CAP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_RIS_RISTORNI_CAP = 8;
        public static final int W_B03_RIS_RISTORNI_CAP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_RISTORNI_CAP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_RIS_RISTORNI_CAP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
