package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WK-VALORE-RIS<br>
 * Variable: WK-VALORE-RIS from program IVVS0216<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkValoreRis {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char TROVATO = 'S';
    public static final char NON_TROVATO = 'N';

    //==== METHODS ====
    public void setWkValoreRis(char wkValoreRis) {
        this.value = wkValoreRis;
    }

    public char getWkValoreRis() {
        return this.value;
    }

    public void setTrovato() {
        value = TROVATO;
    }
}
