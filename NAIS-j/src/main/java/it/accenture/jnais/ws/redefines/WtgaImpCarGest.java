package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-CAR-GEST<br>
 * Variable: WTGA-IMP-CAR-GEST from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpCarGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpCarGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_CAR_GEST;
    }

    public void setWtgaImpCarGest(AfDecimal wtgaImpCarGest) {
        writeDecimalAsPacked(Pos.WTGA_IMP_CAR_GEST, wtgaImpCarGest.copy());
    }

    public void setWtgaImpCarGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_CAR_GEST, Pos.WTGA_IMP_CAR_GEST);
    }

    /**Original name: WTGA-IMP-CAR-GEST<br>*/
    public AfDecimal getWtgaImpCarGest() {
        return readPackedAsDecimal(Pos.WTGA_IMP_CAR_GEST, Len.Int.WTGA_IMP_CAR_GEST, Len.Fract.WTGA_IMP_CAR_GEST);
    }

    public byte[] getWtgaImpCarGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_CAR_GEST, Pos.WTGA_IMP_CAR_GEST);
        return buffer;
    }

    public void initWtgaImpCarGestSpaces() {
        fill(Pos.WTGA_IMP_CAR_GEST, Len.WTGA_IMP_CAR_GEST, Types.SPACE_CHAR);
    }

    public void setWtgaImpCarGestNull(String wtgaImpCarGestNull) {
        writeString(Pos.WTGA_IMP_CAR_GEST_NULL, wtgaImpCarGestNull, Len.WTGA_IMP_CAR_GEST_NULL);
    }

    /**Original name: WTGA-IMP-CAR-GEST-NULL<br>*/
    public String getWtgaImpCarGestNull() {
        return readString(Pos.WTGA_IMP_CAR_GEST_NULL, Len.WTGA_IMP_CAR_GEST_NULL);
    }

    public String getWtgaImpCarGestNullFormatted() {
        return Functions.padBlanks(getWtgaImpCarGestNull(), Len.WTGA_IMP_CAR_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_CAR_GEST = 1;
        public static final int WTGA_IMP_CAR_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_CAR_GEST = 8;
        public static final int WTGA_IMP_CAR_GEST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_CAR_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_CAR_GEST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
