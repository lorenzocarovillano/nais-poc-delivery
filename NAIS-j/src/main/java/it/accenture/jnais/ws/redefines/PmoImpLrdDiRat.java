package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-IMP-LRD-DI-RAT<br>
 * Variable: PMO-IMP-LRD-DI-RAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoImpLrdDiRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoImpLrdDiRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_IMP_LRD_DI_RAT;
    }

    public void setPmoImpLrdDiRat(AfDecimal pmoImpLrdDiRat) {
        writeDecimalAsPacked(Pos.PMO_IMP_LRD_DI_RAT, pmoImpLrdDiRat.copy());
    }

    public void setPmoImpLrdDiRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_IMP_LRD_DI_RAT, Pos.PMO_IMP_LRD_DI_RAT);
    }

    /**Original name: PMO-IMP-LRD-DI-RAT<br>*/
    public AfDecimal getPmoImpLrdDiRat() {
        return readPackedAsDecimal(Pos.PMO_IMP_LRD_DI_RAT, Len.Int.PMO_IMP_LRD_DI_RAT, Len.Fract.PMO_IMP_LRD_DI_RAT);
    }

    public byte[] getPmoImpLrdDiRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_IMP_LRD_DI_RAT, Pos.PMO_IMP_LRD_DI_RAT);
        return buffer;
    }

    public void initPmoImpLrdDiRatHighValues() {
        fill(Pos.PMO_IMP_LRD_DI_RAT, Len.PMO_IMP_LRD_DI_RAT, Types.HIGH_CHAR_VAL);
    }

    public void setPmoImpLrdDiRatNull(String pmoImpLrdDiRatNull) {
        writeString(Pos.PMO_IMP_LRD_DI_RAT_NULL, pmoImpLrdDiRatNull, Len.PMO_IMP_LRD_DI_RAT_NULL);
    }

    /**Original name: PMO-IMP-LRD-DI-RAT-NULL<br>*/
    public String getPmoImpLrdDiRatNull() {
        return readString(Pos.PMO_IMP_LRD_DI_RAT_NULL, Len.PMO_IMP_LRD_DI_RAT_NULL);
    }

    public String getPmoImpLrdDiRatNullFormatted() {
        return Functions.padBlanks(getPmoImpLrdDiRatNull(), Len.PMO_IMP_LRD_DI_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_IMP_LRD_DI_RAT = 1;
        public static final int PMO_IMP_LRD_DI_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_IMP_LRD_DI_RAT = 8;
        public static final int PMO_IMP_LRD_DI_RAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_IMP_LRD_DI_RAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_IMP_LRD_DI_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
