package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RIC-ID-BATCH<br>
 * Variable: RIC-ID-BATCH from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RicIdBatch extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RicIdBatch() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RIC_ID_BATCH;
    }

    public void setRicIdBatch(int ricIdBatch) {
        writeIntAsPacked(Pos.RIC_ID_BATCH, ricIdBatch, Len.Int.RIC_ID_BATCH);
    }

    public void setRicIdBatchFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RIC_ID_BATCH, Pos.RIC_ID_BATCH);
    }

    /**Original name: RIC-ID-BATCH<br>*/
    public int getRicIdBatch() {
        return readPackedAsInt(Pos.RIC_ID_BATCH, Len.Int.RIC_ID_BATCH);
    }

    public byte[] getRicIdBatchAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RIC_ID_BATCH, Pos.RIC_ID_BATCH);
        return buffer;
    }

    public void setRicIdBatchNull(String ricIdBatchNull) {
        writeString(Pos.RIC_ID_BATCH_NULL, ricIdBatchNull, Len.RIC_ID_BATCH_NULL);
    }

    /**Original name: RIC-ID-BATCH-NULL<br>*/
    public String getRicIdBatchNull() {
        return readString(Pos.RIC_ID_BATCH_NULL, Len.RIC_ID_BATCH_NULL);
    }

    public String getRicIdBatchNullFormatted() {
        return Functions.padBlanks(getRicIdBatchNull(), Len.RIC_ID_BATCH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RIC_ID_BATCH = 1;
        public static final int RIC_ID_BATCH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RIC_ID_BATCH = 5;
        public static final int RIC_ID_BATCH_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RIC_ID_BATCH = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
