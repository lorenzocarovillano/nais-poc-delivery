package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: GRZ-PC-REVRSB<br>
 * Variable: GRZ-PC-REVRSB from program LDBS1350<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class GrzPcRevrsb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public GrzPcRevrsb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.GRZ_PC_REVRSB;
    }

    public void setGrzPcRevrsb(AfDecimal grzPcRevrsb) {
        writeDecimalAsPacked(Pos.GRZ_PC_REVRSB, grzPcRevrsb.copy());
    }

    public void setGrzPcRevrsbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.GRZ_PC_REVRSB, Pos.GRZ_PC_REVRSB);
    }

    /**Original name: GRZ-PC-REVRSB<br>*/
    public AfDecimal getGrzPcRevrsb() {
        return readPackedAsDecimal(Pos.GRZ_PC_REVRSB, Len.Int.GRZ_PC_REVRSB, Len.Fract.GRZ_PC_REVRSB);
    }

    public byte[] getGrzPcRevrsbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.GRZ_PC_REVRSB, Pos.GRZ_PC_REVRSB);
        return buffer;
    }

    public void setGrzPcRevrsbNull(String grzPcRevrsbNull) {
        writeString(Pos.GRZ_PC_REVRSB_NULL, grzPcRevrsbNull, Len.GRZ_PC_REVRSB_NULL);
    }

    /**Original name: GRZ-PC-REVRSB-NULL<br>*/
    public String getGrzPcRevrsbNull() {
        return readString(Pos.GRZ_PC_REVRSB_NULL, Len.GRZ_PC_REVRSB_NULL);
    }

    public String getGrzPcRevrsbNullFormatted() {
        return Functions.padBlanks(getGrzPcRevrsbNull(), Len.GRZ_PC_REVRSB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int GRZ_PC_REVRSB = 1;
        public static final int GRZ_PC_REVRSB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int GRZ_PC_REVRSB = 4;
        public static final int GRZ_PC_REVRSB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int GRZ_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int GRZ_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
