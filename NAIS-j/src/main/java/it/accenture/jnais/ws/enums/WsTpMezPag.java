package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TP-MEZ-PAG<br>
 * Variable: WS-TP-MEZ-PAG from copybook LCCVXMZ0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsTpMezPag {

    //==== PROPERTIES ====
    private String value = "";
    public static final String RID = "RI";
    public static final String RID_EX26 = "NR";
    public static final String SWITCH_FLD = "SW";
    public static final String TRAS_FON = "TF";
    public static final String NON_AUT = "XE";
    public static final String CASH = "CH";
    public static final String ACC_CC = "CC";
    public static final String ACC_CASH = "C2";
    public static final String TRAT_STIP = "TS";
    public static final String BONIFICO = "BO";
    public static final String BONIF_EST = "BT";
    public static final String BONIF_MAN = "BM";
    public static final String BONIF_EDW = "BE";
    public static final String BONIF_BNL = "BP";
    public static final String ASSEGNO = "AS";
    public static final String ASS_CIR_BNL = "AC";
    public static final String ASS_CIR_ALTRI = "AI";
    public static final String ASS_BAN_BNL = "AB";
    public static final String ASS_BAN_ALTRI = "AA";
    public static final String RID_ESTERNO = "RD";
    public static final String DA_COMPLETARE = "DC";
    public static final String COMPENSAZIONE = "CP";
    public static final String ACCANTONAMENTO = "AT";
    public static final String CARTA_DI_CREDITO = "CD";

    //==== METHODS ====
    public void setWsTpMezPag(String wsTpMezPag) {
        this.value = Functions.subString(wsTpMezPag, Len.WS_TP_MEZ_PAG);
    }

    public String getWsTpMezPag() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_TP_MEZ_PAG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
