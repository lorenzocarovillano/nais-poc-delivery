package it.accenture.jnais.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.Types;

/**Original name: IVVC0222-AREA-FND-X-TRANCHE<br>
 * Variable: IVVC0222-AREA-FND-X-TRANCHE from program IVVS0216<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class Ivvc0222AreaFndXTranche extends BytesAllocatingClass {

    //==== PROPERTIES ====
    public static final int FND_MAXOCCURS = 100;
    public static final int TAB_TRAN_MAXOCCURS = 1250;

    //==== CONSTRUCTORS ====
    public Ivvc0222AreaFndXTranche() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.IVVC0222_AREA_FND_X_TRANCHE;
    }

    public void setIvvc0222AreaFndXTrancheBytes(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.IVVC0222_AREA_FND_X_TRANCHE, Pos.IVVC0222_AREA_FND_X_TRANCHE);
    }

    public byte[] getIvvc0222AreaFndXTrancheBytes(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.IVVC0222_AREA_FND_X_TRANCHE, Pos.IVVC0222_AREA_FND_X_TRANCHE);
        return buffer;
    }

    /**Original name: IVVC0222-ELE-TRCH-MAX<br>*/
    public short getEleTrchMax() {
        return readBinaryShort(Pos.ELE_TRCH_MAX);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int IVVC0222_AREA_FND_X_TRANCHE = 1;
        public static final int ELE_TRCH_MAX = IVVC0222_AREA_FND_X_TRANCHE;

        //==== CONSTRUCTORS ====
        private Pos() {
        }

        //==== METHODS ====
        public static int ivvc0222TabTran(int idx) {
            return ELE_TRCH_MAX + Len.ELE_TRCH_MAX + idx * Len.TAB_TRAN;
        }

        public static int ivvc0222IdTranche(int idx) {
            return ivvc0222TabTran(idx);
        }

        public static int ivvc0222TpTrch(int idx) {
            return ivvc0222IdTranche(idx) + Len.ID_TRANCHE;
        }

        public static int ivvc0222EleFndMax(int idx) {
            return ivvc0222TpTrch(idx) + Len.TP_TRCH;
        }

        public static int ivvc0222Fnd(int idx1, int idx2) {
            return ivvc0222EleFndMax(idx1) + Len.ELE_FND_MAX + idx2 * Len.FND;
        }

        public static int ivvc0222CodFnd(int idx1, int idx2) {
            return ivvc0222Fnd(idx1, idx2);
        }

        public static int ivvc0222CtrvFnd(int idx1, int idx2) {
            return ivvc0222CodFnd(idx1, idx2) + Len.COD_FND;
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_TRCH_MAX = 2;
        public static final int ID_TRANCHE = 5;
        public static final int TP_TRCH = 2;
        public static final int ELE_FND_MAX = 2;
        public static final int COD_FND = 20;
        public static final int CTRV_FND = 8;
        public static final int FND = COD_FND + CTRV_FND;
        public static final int TAB_TRAN = ID_TRANCHE + TP_TRCH + ELE_FND_MAX + Ivvc0222AreaFndXTranche.FND_MAXOCCURS * FND;
        public static final int IVVC0222_AREA_FND_X_TRANCHE = ELE_TRCH_MAX + Ivvc0222AreaFndXTranche.TAB_TRAN_MAXOCCURS * TAB_TRAN;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
