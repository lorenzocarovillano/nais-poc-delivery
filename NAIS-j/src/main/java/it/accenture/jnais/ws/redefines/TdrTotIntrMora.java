package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-TOT-INTR-MORA<br>
 * Variable: TDR-TOT-INTR-MORA from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrTotIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrTotIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_TOT_INTR_MORA;
    }

    public void setTdrTotIntrMora(AfDecimal tdrTotIntrMora) {
        writeDecimalAsPacked(Pos.TDR_TOT_INTR_MORA, tdrTotIntrMora.copy());
    }

    public void setTdrTotIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_TOT_INTR_MORA, Pos.TDR_TOT_INTR_MORA);
    }

    /**Original name: TDR-TOT-INTR-MORA<br>*/
    public AfDecimal getTdrTotIntrMora() {
        return readPackedAsDecimal(Pos.TDR_TOT_INTR_MORA, Len.Int.TDR_TOT_INTR_MORA, Len.Fract.TDR_TOT_INTR_MORA);
    }

    public byte[] getTdrTotIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_TOT_INTR_MORA, Pos.TDR_TOT_INTR_MORA);
        return buffer;
    }

    public void setTdrTotIntrMoraNull(String tdrTotIntrMoraNull) {
        writeString(Pos.TDR_TOT_INTR_MORA_NULL, tdrTotIntrMoraNull, Len.TDR_TOT_INTR_MORA_NULL);
    }

    /**Original name: TDR-TOT-INTR-MORA-NULL<br>*/
    public String getTdrTotIntrMoraNull() {
        return readString(Pos.TDR_TOT_INTR_MORA_NULL, Len.TDR_TOT_INTR_MORA_NULL);
    }

    public String getTdrTotIntrMoraNullFormatted() {
        return Functions.padBlanks(getTdrTotIntrMoraNull(), Len.TDR_TOT_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_MORA = 1;
        public static final int TDR_TOT_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_TOT_INTR_MORA = 8;
        public static final int TDR_TOT_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TDR_TOT_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
