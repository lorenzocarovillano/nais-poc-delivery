package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-CREDITO-IS<br>
 * Variable: WDFA-CREDITO-IS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaCreditoIs extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaCreditoIs() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_CREDITO_IS;
    }

    public void setWdfaCreditoIs(AfDecimal wdfaCreditoIs) {
        writeDecimalAsPacked(Pos.WDFA_CREDITO_IS, wdfaCreditoIs.copy());
    }

    public void setWdfaCreditoIsFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_CREDITO_IS, Pos.WDFA_CREDITO_IS);
    }

    /**Original name: WDFA-CREDITO-IS<br>*/
    public AfDecimal getWdfaCreditoIs() {
        return readPackedAsDecimal(Pos.WDFA_CREDITO_IS, Len.Int.WDFA_CREDITO_IS, Len.Fract.WDFA_CREDITO_IS);
    }

    public byte[] getWdfaCreditoIsAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_CREDITO_IS, Pos.WDFA_CREDITO_IS);
        return buffer;
    }

    public void setWdfaCreditoIsNull(String wdfaCreditoIsNull) {
        writeString(Pos.WDFA_CREDITO_IS_NULL, wdfaCreditoIsNull, Len.WDFA_CREDITO_IS_NULL);
    }

    /**Original name: WDFA-CREDITO-IS-NULL<br>*/
    public String getWdfaCreditoIsNull() {
        return readString(Pos.WDFA_CREDITO_IS_NULL, Len.WDFA_CREDITO_IS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_CREDITO_IS = 1;
        public static final int WDFA_CREDITO_IS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_CREDITO_IS = 8;
        public static final int WDFA_CREDITO_IS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_CREDITO_IS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_CREDITO_IS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
