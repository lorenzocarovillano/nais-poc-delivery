package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-PRE-SOLO-RSH<br>
 * Variable: WDTC-PRE-SOLO-RSH from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcPreSoloRsh extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcPreSoloRsh() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_PRE_SOLO_RSH;
    }

    public void setWdtcPreSoloRsh(AfDecimal wdtcPreSoloRsh) {
        writeDecimalAsPacked(Pos.WDTC_PRE_SOLO_RSH, wdtcPreSoloRsh.copy());
    }

    public void setWdtcPreSoloRshFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_PRE_SOLO_RSH, Pos.WDTC_PRE_SOLO_RSH);
    }

    /**Original name: WDTC-PRE-SOLO-RSH<br>*/
    public AfDecimal getWdtcPreSoloRsh() {
        return readPackedAsDecimal(Pos.WDTC_PRE_SOLO_RSH, Len.Int.WDTC_PRE_SOLO_RSH, Len.Fract.WDTC_PRE_SOLO_RSH);
    }

    public byte[] getWdtcPreSoloRshAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_PRE_SOLO_RSH, Pos.WDTC_PRE_SOLO_RSH);
        return buffer;
    }

    public void initWdtcPreSoloRshSpaces() {
        fill(Pos.WDTC_PRE_SOLO_RSH, Len.WDTC_PRE_SOLO_RSH, Types.SPACE_CHAR);
    }

    public void setWdtcPreSoloRshNull(String wdtcPreSoloRshNull) {
        writeString(Pos.WDTC_PRE_SOLO_RSH_NULL, wdtcPreSoloRshNull, Len.WDTC_PRE_SOLO_RSH_NULL);
    }

    /**Original name: WDTC-PRE-SOLO-RSH-NULL<br>*/
    public String getWdtcPreSoloRshNull() {
        return readString(Pos.WDTC_PRE_SOLO_RSH_NULL, Len.WDTC_PRE_SOLO_RSH_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_SOLO_RSH = 1;
        public static final int WDTC_PRE_SOLO_RSH_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_PRE_SOLO_RSH = 8;
        public static final int WDTC_PRE_SOLO_RSH_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_SOLO_RSH = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_PRE_SOLO_RSH = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
