package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalUtil;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Lccvl271;
import it.accenture.jnais.copy.Lccvode1;
import it.accenture.jnais.ws.occurs.WmdeTabMde;
import static com.bphx.ctu.af.lang.AfSystem.strLen;

/**Original name: WK-VARIABILI<br>
 * Variable: WK-VARIABILI from program LCCS0005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkVariabili {

    //==== PROPERTIES ====
    public static final int WMDE_TAB_MDE_MAXOCCURS = 20;
    //Original name: WS-COD-COMPAGNIA
    private String wsCodCompagnia = "";
    //Original name: WS-SEQUENCE-PTF
    private String wsSequencePtf = "";
    //Original name: WS-RICHIESTA
    private String wsRichiesta = "";
    //Original name: WS-MOVIMENTO-APPO
    private String wsMovimentoAppo = "";
    //Original name: WS-ID-RICH-PTF
    private String wsIdRichPtf = "000000000";
    //Original name: WS-ELE-ULT-PROG
    private short wsEleUltProg = DefaultValues.BIN_SHORT_VAL;
    //Original name: WS-ID-MOVI-PTF
    private String wsIdMoviPtf = "000000000";
    //Original name: WODE-ELE-ODE-MAX
    private short wodeEleOdeMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVODE1
    private Lccvode1 lccvode1 = new Lccvode1();
    //Original name: WMDE-ELE-MDE-MAX
    private short wmdeEleMdeMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WMDE-TAB-MDE
    private WmdeTabMde[] wmdeTabMde = new WmdeTabMde[WMDE_TAB_MDE_MAXOCCURS];
    //Original name: WL27-ELE-PREVENTIVO-MAX
    private short wl27ElePreventivoMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVL271
    private Lccvl271 lccvl271 = new Lccvl271();

    //==== CONSTRUCTORS ====
    public WkVariabili() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int wmdeTabMdeIdx = 1; wmdeTabMdeIdx <= WMDE_TAB_MDE_MAXOCCURS; wmdeTabMdeIdx++) {
            wmdeTabMde[wmdeTabMdeIdx - 1] = new WmdeTabMde();
        }
    }

    /**Original name: WS-OGGETTO<br>*/
    public byte[] getWsOggettoBytes() {
        byte[] buffer = new byte[Len.WS_OGGETTO];
        return getWsOggettoBytes(buffer, 1);
    }

    public byte[] getWsOggettoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, wsCodCompagnia, Len.WS_COD_COMPAGNIA);
        position += Len.WS_COD_COMPAGNIA;
        MarshalByte.writeString(buffer, position, wsSequencePtf, Len.WS_SEQUENCE_PTF);
        return buffer;
    }

    public void setWsCodCompagnia(String wsCodCompagnia) {
        this.wsCodCompagnia = Functions.subString(wsCodCompagnia, Len.WS_COD_COMPAGNIA);
    }

    public String getWsCodCompagnia() {
        return this.wsCodCompagnia;
    }

    public void setWsSequencePtf(String wsSequencePtf) {
        this.wsSequencePtf = Functions.subString(wsSequencePtf, Len.WS_SEQUENCE_PTF);
    }

    public void setWsSequencePtfFormatted(String wsSequencePtf) {
        String field = Functions.leftPad(wsSequencePtf, Len.WS_SEQUENCE_PTF, Types.SPACE_CHAR);
        this.wsSequencePtf = Functions.subString(field, strLen(field) - Len.WS_SEQUENCE_PTF + 1, Len.WS_SEQUENCE_PTF);
    }

    public String getWsSequencePtf() {
        return this.wsSequencePtf;
    }

    public void setWsRichiesta(String wsRichiesta) {
        this.wsRichiesta = Functions.subString(wsRichiesta, Len.WS_RICHIESTA);
    }

    public void setWsRichiestaFormatted(String wsRichiesta) {
        String field = Functions.leftPad(wsRichiesta, Len.WS_RICHIESTA, Types.SPACE_CHAR);
        this.wsRichiesta = Functions.subString(field, strLen(field) - Len.WS_RICHIESTA + 1, Len.WS_RICHIESTA);
    }

    public void setWsRichiestaFromBuffer(byte[] buffer) {
        int offset = buffer.length > Len.WS_RICHIESTA ? buffer.length - Len.WS_RICHIESTA + 1 : 1;
        wsRichiesta = MarshalByte.readString(buffer, offset, Len.WS_RICHIESTA);
    }

    public String getWsRichiesta() {
        return this.wsRichiesta;
    }

    public String getWsRichiestaFormatted() {
        return this.wsRichiesta;
    }

    public void setWsMovimentoAppo(String wsMovimentoAppo) {
        this.wsMovimentoAppo = Functions.subString(wsMovimentoAppo, Len.WS_MOVIMENTO_APPO);
    }

    public void setWsMovimentoAppoFormatted(String wsMovimentoAppo) {
        String field = Functions.leftPad(wsMovimentoAppo, Len.WS_MOVIMENTO_APPO, Types.SPACE_CHAR);
        this.wsMovimentoAppo = Functions.subString(field, strLen(field) - Len.WS_MOVIMENTO_APPO + 1, Len.WS_MOVIMENTO_APPO);
    }

    public void setWsMovimentoAppoFromBuffer(byte[] buffer) {
        int offset = buffer.length > Len.WS_MOVIMENTO_APPO ? buffer.length - Len.WS_MOVIMENTO_APPO + 1 : 1;
        wsMovimentoAppo = MarshalByte.readString(buffer, offset, Len.WS_MOVIMENTO_APPO);
    }

    public String getWsMovimentoAppo() {
        return this.wsMovimentoAppo;
    }

    public String getWsMovimentoAppoFormatted() {
        return this.wsMovimentoAppo;
    }

    public void setWsIdRichPtf(int wsIdRichPtf) {
        this.wsIdRichPtf = NumericDisplay.asString(wsIdRichPtf, Len.WS_ID_RICH_PTF);
    }

    public void setWsIdRichPtfFormatted(String wsIdRichPtf) {
        this.wsIdRichPtf = Trunc.toUnsignedNumeric(wsIdRichPtf, Len.WS_ID_RICH_PTF);
    }

    public int getWsIdRichPtf() {
        return NumericDisplay.asInt(this.wsIdRichPtf);
    }

    public String getWsIdRichPtfFormatted() {
        return this.wsIdRichPtf;
    }

    public void setWsEleUltProg(short wsEleUltProg) {
        this.wsEleUltProg = wsEleUltProg;
    }

    public short getWsEleUltProg() {
        return this.wsEleUltProg;
    }

    public void setWsIdMoviPtf(int wsIdMoviPtf) {
        this.wsIdMoviPtf = NumericDisplay.asString(wsIdMoviPtf, Len.WS_ID_MOVI_PTF);
    }

    public void setWsIdMoviPtfFormatted(String wsIdMoviPtf) {
        this.wsIdMoviPtf = Trunc.toUnsignedNumeric(wsIdMoviPtf, Len.WS_ID_MOVI_PTF);
    }

    public int getWsIdMoviPtf() {
        return NumericDisplay.asInt(this.wsIdMoviPtf);
    }

    public String getWsIdMoviPtfFormatted() {
        return this.wsIdMoviPtf;
    }

    public void setWodeEleOdeMax(short wodeEleOdeMax) {
        this.wodeEleOdeMax = wodeEleOdeMax;
    }

    public short getWodeEleOdeMax() {
        return this.wodeEleOdeMax;
    }

    public void setWmdeEleMdeMax(short wmdeEleMdeMax) {
        this.wmdeEleMdeMax = wmdeEleMdeMax;
    }

    public short getWmdeEleMdeMax() {
        return this.wmdeEleMdeMax;
    }

    public void setWl27ElePreventivoMax(short wl27ElePreventivoMax) {
        this.wl27ElePreventivoMax = wl27ElePreventivoMax;
    }

    public short getWl27ElePreventivoMax() {
        return this.wl27ElePreventivoMax;
    }

    public Lccvl271 getLccvl271() {
        return lccvl271;
    }

    public Lccvode1 getLccvode1() {
        return lccvode1;
    }

    public WmdeTabMde getWmdeTabMde(int idx) {
        return wmdeTabMde[idx - 1];
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_RICH_PTF = 9;
        public static final int WS_ID_MOVI_PTF = 9;
        public static final int WS_COD_COMPAGNIA = 5;
        public static final int WS_SEQUENCE_PTF = 9;
        public static final int WS_RICHIESTA = 40;
        public static final int WS_MOVIMENTO_APPO = 40;
        public static final int WS_OGGETTO = WS_COD_COMPAGNIA + WS_SEQUENCE_PTF;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
