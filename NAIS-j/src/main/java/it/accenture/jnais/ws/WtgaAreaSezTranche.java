package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WtgaTabTran;

/**Original name: WTGA-AREA-SEZ-TRANCHE<br>
 * Variable: WTGA-AREA-SEZ-TRANCHE from program LCCS0234<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WtgaAreaSezTranche extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_TRAN_MAXOCCURS = 20;
    //Original name: WTGA-ELE-TRAN-MAX
    private short eleTranMax = ((short)0);
    //Original name: WTGA-TAB-TRAN
    private WtgaTabTran[] tabTran = new WtgaTabTran[TAB_TRAN_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WtgaAreaSezTranche() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_AREA_SEZ_TRANCHE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWtgaAreaSezTrancheBytes(buf);
    }

    public void init() {
        for (int tabTranIdx = 1; tabTranIdx <= TAB_TRAN_MAXOCCURS; tabTranIdx++) {
            tabTran[tabTranIdx - 1] = new WtgaTabTran();
        }
    }

    public void setWtgaAreaSezTrancheBytes(byte[] buffer) {
        setWtgaAreaSezTrancheBytes(buffer, 1);
    }

    public byte[] getWtgaAreaSezTrancheBytes() {
        byte[] buffer = new byte[Len.WTGA_AREA_SEZ_TRANCHE];
        return getWtgaAreaSezTrancheBytes(buffer, 1);
    }

    public void setWtgaAreaSezTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        eleTranMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_TRAN_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabTran[idx - 1].setTabTranBytes(buffer, position);
                position += WtgaTabTran.Len.TAB_TRAN;
            }
            else {
                tabTran[idx - 1].initTabTranSpaces();
                position += WtgaTabTran.Len.TAB_TRAN;
            }
        }
    }

    public byte[] getWtgaAreaSezTrancheBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleTranMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_TRAN_MAXOCCURS; idx++) {
            tabTran[idx - 1].getTabTranBytes(buffer, position);
            position += WtgaTabTran.Len.TAB_TRAN;
        }
        return buffer;
    }

    public void setEleTranMax(short eleTranMax) {
        this.eleTranMax = eleTranMax;
    }

    public short getEleTranMax() {
        return this.eleTranMax;
    }

    public WtgaTabTran getTabTran(int idx) {
        return tabTran[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWtgaAreaSezTrancheBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_TRAN_MAX = 2;
        public static final int WTGA_AREA_SEZ_TRANCHE = ELE_TRAN_MAX + WtgaAreaSezTranche.TAB_TRAN_MAXOCCURS * WtgaTabTran.Len.TAB_TRAN;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
