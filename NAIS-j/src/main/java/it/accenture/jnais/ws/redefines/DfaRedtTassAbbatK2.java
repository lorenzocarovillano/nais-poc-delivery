package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-REDT-TASS-ABBAT-K2<br>
 * Variable: DFA-REDT-TASS-ABBAT-K2 from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaRedtTassAbbatK2 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaRedtTassAbbatK2() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_REDT_TASS_ABBAT_K2;
    }

    public void setDfaRedtTassAbbatK2(AfDecimal dfaRedtTassAbbatK2) {
        writeDecimalAsPacked(Pos.DFA_REDT_TASS_ABBAT_K2, dfaRedtTassAbbatK2.copy());
    }

    public void setDfaRedtTassAbbatK2FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_REDT_TASS_ABBAT_K2, Pos.DFA_REDT_TASS_ABBAT_K2);
    }

    /**Original name: DFA-REDT-TASS-ABBAT-K2<br>*/
    public AfDecimal getDfaRedtTassAbbatK2() {
        return readPackedAsDecimal(Pos.DFA_REDT_TASS_ABBAT_K2, Len.Int.DFA_REDT_TASS_ABBAT_K2, Len.Fract.DFA_REDT_TASS_ABBAT_K2);
    }

    public byte[] getDfaRedtTassAbbatK2AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_REDT_TASS_ABBAT_K2, Pos.DFA_REDT_TASS_ABBAT_K2);
        return buffer;
    }

    public void setDfaRedtTassAbbatK2Null(String dfaRedtTassAbbatK2Null) {
        writeString(Pos.DFA_REDT_TASS_ABBAT_K2_NULL, dfaRedtTassAbbatK2Null, Len.DFA_REDT_TASS_ABBAT_K2_NULL);
    }

    /**Original name: DFA-REDT-TASS-ABBAT-K2-NULL<br>*/
    public String getDfaRedtTassAbbatK2Null() {
        return readString(Pos.DFA_REDT_TASS_ABBAT_K2_NULL, Len.DFA_REDT_TASS_ABBAT_K2_NULL);
    }

    public String getDfaRedtTassAbbatK2NullFormatted() {
        return Functions.padBlanks(getDfaRedtTassAbbatK2Null(), Len.DFA_REDT_TASS_ABBAT_K2_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_REDT_TASS_ABBAT_K2 = 1;
        public static final int DFA_REDT_TASS_ABBAT_K2_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_REDT_TASS_ABBAT_K2 = 8;
        public static final int DFA_REDT_TASS_ABBAT_K2_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_REDT_TASS_ABBAT_K2 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_REDT_TASS_ABBAT_K2 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
