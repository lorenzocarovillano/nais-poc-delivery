package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-IMP-GAR-CNBT<br>
 * Variable: ADE-IMP-GAR-CNBT from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeImpGarCnbt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeImpGarCnbt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_IMP_GAR_CNBT;
    }

    public void setAdeImpGarCnbt(AfDecimal adeImpGarCnbt) {
        writeDecimalAsPacked(Pos.ADE_IMP_GAR_CNBT, adeImpGarCnbt.copy());
    }

    public void setAdeImpGarCnbtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_IMP_GAR_CNBT, Pos.ADE_IMP_GAR_CNBT);
    }

    /**Original name: ADE-IMP-GAR-CNBT<br>*/
    public AfDecimal getAdeImpGarCnbt() {
        return readPackedAsDecimal(Pos.ADE_IMP_GAR_CNBT, Len.Int.ADE_IMP_GAR_CNBT, Len.Fract.ADE_IMP_GAR_CNBT);
    }

    public byte[] getAdeImpGarCnbtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_IMP_GAR_CNBT, Pos.ADE_IMP_GAR_CNBT);
        return buffer;
    }

    public void setAdeImpGarCnbtNull(String adeImpGarCnbtNull) {
        writeString(Pos.ADE_IMP_GAR_CNBT_NULL, adeImpGarCnbtNull, Len.ADE_IMP_GAR_CNBT_NULL);
    }

    /**Original name: ADE-IMP-GAR-CNBT-NULL<br>*/
    public String getAdeImpGarCnbtNull() {
        return readString(Pos.ADE_IMP_GAR_CNBT_NULL, Len.ADE_IMP_GAR_CNBT_NULL);
    }

    public String getAdeImpGarCnbtNullFormatted() {
        return Functions.padBlanks(getAdeImpGarCnbtNull(), Len.ADE_IMP_GAR_CNBT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_IMP_GAR_CNBT = 1;
        public static final int ADE_IMP_GAR_CNBT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_IMP_GAR_CNBT = 8;
        public static final int ADE_IMP_GAR_CNBT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_IMP_GAR_CNBT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int ADE_IMP_GAR_CNBT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
