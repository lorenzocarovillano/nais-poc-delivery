package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPMO-IMP-RISC-PARZ-PRGT<br>
 * Variable: WPMO-IMP-RISC-PARZ-PRGT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpmoImpRiscParzPrgt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpmoImpRiscParzPrgt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPMO_IMP_RISC_PARZ_PRGT;
    }

    public void setWpmoImpRiscParzPrgt(AfDecimal wpmoImpRiscParzPrgt) {
        writeDecimalAsPacked(Pos.WPMO_IMP_RISC_PARZ_PRGT, wpmoImpRiscParzPrgt.copy());
    }

    public void setWpmoImpRiscParzPrgtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPMO_IMP_RISC_PARZ_PRGT, Pos.WPMO_IMP_RISC_PARZ_PRGT);
    }

    /**Original name: WPMO-IMP-RISC-PARZ-PRGT<br>*/
    public AfDecimal getWpmoImpRiscParzPrgt() {
        return readPackedAsDecimal(Pos.WPMO_IMP_RISC_PARZ_PRGT, Len.Int.WPMO_IMP_RISC_PARZ_PRGT, Len.Fract.WPMO_IMP_RISC_PARZ_PRGT);
    }

    public byte[] getWpmoImpRiscParzPrgtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPMO_IMP_RISC_PARZ_PRGT, Pos.WPMO_IMP_RISC_PARZ_PRGT);
        return buffer;
    }

    public void initWpmoImpRiscParzPrgtSpaces() {
        fill(Pos.WPMO_IMP_RISC_PARZ_PRGT, Len.WPMO_IMP_RISC_PARZ_PRGT, Types.SPACE_CHAR);
    }

    public void setWpmoImpRiscParzPrgtNull(String wpmoImpRiscParzPrgtNull) {
        writeString(Pos.WPMO_IMP_RISC_PARZ_PRGT_NULL, wpmoImpRiscParzPrgtNull, Len.WPMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    /**Original name: WPMO-IMP-RISC-PARZ-PRGT-NULL<br>*/
    public String getWpmoImpRiscParzPrgtNull() {
        return readString(Pos.WPMO_IMP_RISC_PARZ_PRGT_NULL, Len.WPMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    public String getWpmoImpRiscParzPrgtNullFormatted() {
        return Functions.padBlanks(getWpmoImpRiscParzPrgtNull(), Len.WPMO_IMP_RISC_PARZ_PRGT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_RISC_PARZ_PRGT = 1;
        public static final int WPMO_IMP_RISC_PARZ_PRGT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPMO_IMP_RISC_PARZ_PRGT = 8;
        public static final int WPMO_IMP_RISC_PARZ_PRGT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_RISC_PARZ_PRGT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPMO_IMP_RISC_PARZ_PRGT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
