package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-1382011L<br>
 * Variable: DFL-IMPST-VIS-1382011L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVis1382011l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVis1382011l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS1382011L;
    }

    public void setDflImpstVis1382011l(AfDecimal dflImpstVis1382011l) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS1382011L, dflImpstVis1382011l.copy());
    }

    public void setDflImpstVis1382011lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS1382011L, Pos.DFL_IMPST_VIS1382011L);
    }

    /**Original name: DFL-IMPST-VIS-1382011L<br>*/
    public AfDecimal getDflImpstVis1382011l() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS1382011L, Len.Int.DFL_IMPST_VIS1382011L, Len.Fract.DFL_IMPST_VIS1382011L);
    }

    public byte[] getDflImpstVis1382011lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS1382011L, Pos.DFL_IMPST_VIS1382011L);
        return buffer;
    }

    public void setDflImpstVis1382011lNull(String dflImpstVis1382011lNull) {
        writeString(Pos.DFL_IMPST_VIS1382011L_NULL, dflImpstVis1382011lNull, Len.DFL_IMPST_VIS1382011L_NULL);
    }

    /**Original name: DFL-IMPST-VIS-1382011L-NULL<br>*/
    public String getDflImpstVis1382011lNull() {
        return readString(Pos.DFL_IMPST_VIS1382011L_NULL, Len.DFL_IMPST_VIS1382011L_NULL);
    }

    public String getDflImpstVis1382011lNullFormatted() {
        return Functions.padBlanks(getDflImpstVis1382011lNull(), Len.DFL_IMPST_VIS1382011L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS1382011L = 1;
        public static final int DFL_IMPST_VIS1382011L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS1382011L = 8;
        public static final int DFL_IMPST_VIS1382011L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS1382011L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS1382011L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
