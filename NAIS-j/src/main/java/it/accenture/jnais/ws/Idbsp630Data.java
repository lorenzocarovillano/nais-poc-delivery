package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.AccCommDb;
import it.accenture.jnais.copy.Idsv0010;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program IDBSP630<br>
 * Generated as a class for rule WS.<br>*/
public class Idbsp630Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-P63-DESC-ACC-COMM-MAST
    private short indP63DescAccCommMast = DefaultValues.BIN_SHORT_VAL;
    //Original name: ACC-COMM-DB
    private AccCommDb accCommDb = new AccCommDb();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndP63DescAccCommMast(short indP63DescAccCommMast) {
        this.indP63DescAccCommMast = indP63DescAccCommMast;
    }

    public short getIndP63DescAccCommMast() {
        return this.indP63DescAccCommMast;
    }

    public AccCommDb getAccCommDb() {
        return accCommDb;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
