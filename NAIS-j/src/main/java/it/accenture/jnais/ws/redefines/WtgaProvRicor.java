package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-PROV-RICOR<br>
 * Variable: WTGA-PROV-RICOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaProvRicor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaProvRicor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_PROV_RICOR;
    }

    public void setWtgaProvRicor(AfDecimal wtgaProvRicor) {
        writeDecimalAsPacked(Pos.WTGA_PROV_RICOR, wtgaProvRicor.copy());
    }

    public void setWtgaProvRicorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_PROV_RICOR, Pos.WTGA_PROV_RICOR);
    }

    /**Original name: WTGA-PROV-RICOR<br>*/
    public AfDecimal getWtgaProvRicor() {
        return readPackedAsDecimal(Pos.WTGA_PROV_RICOR, Len.Int.WTGA_PROV_RICOR, Len.Fract.WTGA_PROV_RICOR);
    }

    public byte[] getWtgaProvRicorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_PROV_RICOR, Pos.WTGA_PROV_RICOR);
        return buffer;
    }

    public void initWtgaProvRicorSpaces() {
        fill(Pos.WTGA_PROV_RICOR, Len.WTGA_PROV_RICOR, Types.SPACE_CHAR);
    }

    public void setWtgaProvRicorNull(String wtgaProvRicorNull) {
        writeString(Pos.WTGA_PROV_RICOR_NULL, wtgaProvRicorNull, Len.WTGA_PROV_RICOR_NULL);
    }

    /**Original name: WTGA-PROV-RICOR-NULL<br>*/
    public String getWtgaProvRicorNull() {
        return readString(Pos.WTGA_PROV_RICOR_NULL, Len.WTGA_PROV_RICOR_NULL);
    }

    public String getWtgaProvRicorNullFormatted() {
        return Functions.padBlanks(getWtgaProvRicorNull(), Len.WTGA_PROV_RICOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_PROV_RICOR = 1;
        public static final int WTGA_PROV_RICOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_PROV_RICOR = 8;
        public static final int WTGA_PROV_RICOR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_PROV_RICOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_PROV_RICOR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
