package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WP85-MIN-TRNUT<br>
 * Variable: WP85-MIN-TRNUT from program LRGS0660<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp85MinTrnut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp85MinTrnut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP85_MIN_TRNUT;
    }

    public void setWp85MinTrnut(AfDecimal wp85MinTrnut) {
        writeDecimalAsPacked(Pos.WP85_MIN_TRNUT, wp85MinTrnut.copy());
    }

    /**Original name: WP85-MIN-TRNUT<br>*/
    public AfDecimal getWp85MinTrnut() {
        return readPackedAsDecimal(Pos.WP85_MIN_TRNUT, Len.Int.WP85_MIN_TRNUT, Len.Fract.WP85_MIN_TRNUT);
    }

    public void setWp85MinTrnutNull(String wp85MinTrnutNull) {
        writeString(Pos.WP85_MIN_TRNUT_NULL, wp85MinTrnutNull, Len.WP85_MIN_TRNUT_NULL);
    }

    /**Original name: WP85-MIN-TRNUT-NULL<br>*/
    public String getWp85MinTrnutNull() {
        return readString(Pos.WP85_MIN_TRNUT_NULL, Len.WP85_MIN_TRNUT_NULL);
    }

    public String getWp85MinTrnutNullFormatted() {
        return Functions.padBlanks(getWp85MinTrnutNull(), Len.WP85_MIN_TRNUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP85_MIN_TRNUT = 1;
        public static final int WP85_MIN_TRNUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP85_MIN_TRNUT = 8;
        public static final int WP85_MIN_TRNUT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP85_MIN_TRNUT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP85_MIN_TRNUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
