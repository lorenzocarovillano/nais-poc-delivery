package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-AZ<br>
 * Variable: WTGA-IMP-AZ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpAz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpAz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_AZ;
    }

    public void setWtgaImpAz(AfDecimal wtgaImpAz) {
        writeDecimalAsPacked(Pos.WTGA_IMP_AZ, wtgaImpAz.copy());
    }

    public void setWtgaImpAzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_AZ, Pos.WTGA_IMP_AZ);
    }

    /**Original name: WTGA-IMP-AZ<br>*/
    public AfDecimal getWtgaImpAz() {
        return readPackedAsDecimal(Pos.WTGA_IMP_AZ, Len.Int.WTGA_IMP_AZ, Len.Fract.WTGA_IMP_AZ);
    }

    public byte[] getWtgaImpAzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_AZ, Pos.WTGA_IMP_AZ);
        return buffer;
    }

    public void initWtgaImpAzSpaces() {
        fill(Pos.WTGA_IMP_AZ, Len.WTGA_IMP_AZ, Types.SPACE_CHAR);
    }

    public void setWtgaImpAzNull(String wtgaImpAzNull) {
        writeString(Pos.WTGA_IMP_AZ_NULL, wtgaImpAzNull, Len.WTGA_IMP_AZ_NULL);
    }

    /**Original name: WTGA-IMP-AZ-NULL<br>*/
    public String getWtgaImpAzNull() {
        return readString(Pos.WTGA_IMP_AZ_NULL, Len.WTGA_IMP_AZ_NULL);
    }

    public String getWtgaImpAzNullFormatted() {
        return Functions.padBlanks(getWtgaImpAzNull(), Len.WTGA_IMP_AZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_AZ = 1;
        public static final int WTGA_IMP_AZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_AZ = 8;
        public static final int WTGA_IMP_AZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_AZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_AZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
