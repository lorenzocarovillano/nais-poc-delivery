package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-NUM-GG-RIVAL<br>
 * Variable: DTC-NUM-GG-RIVAL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcNumGgRival extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcNumGgRival() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_NUM_GG_RIVAL;
    }

    public void setDtcNumGgRival(int dtcNumGgRival) {
        writeIntAsPacked(Pos.DTC_NUM_GG_RIVAL, dtcNumGgRival, Len.Int.DTC_NUM_GG_RIVAL);
    }

    public void setDtcNumGgRivalFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_NUM_GG_RIVAL, Pos.DTC_NUM_GG_RIVAL);
    }

    /**Original name: DTC-NUM-GG-RIVAL<br>*/
    public int getDtcNumGgRival() {
        return readPackedAsInt(Pos.DTC_NUM_GG_RIVAL, Len.Int.DTC_NUM_GG_RIVAL);
    }

    public byte[] getDtcNumGgRivalAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_NUM_GG_RIVAL, Pos.DTC_NUM_GG_RIVAL);
        return buffer;
    }

    public void setDtcNumGgRivalNull(String dtcNumGgRivalNull) {
        writeString(Pos.DTC_NUM_GG_RIVAL_NULL, dtcNumGgRivalNull, Len.DTC_NUM_GG_RIVAL_NULL);
    }

    /**Original name: DTC-NUM-GG-RIVAL-NULL<br>*/
    public String getDtcNumGgRivalNull() {
        return readString(Pos.DTC_NUM_GG_RIVAL_NULL, Len.DTC_NUM_GG_RIVAL_NULL);
    }

    public String getDtcNumGgRivalNullFormatted() {
        return Functions.padBlanks(getDtcNumGgRivalNull(), Len.DTC_NUM_GG_RIVAL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_NUM_GG_RIVAL = 1;
        public static final int DTC_NUM_GG_RIVAL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_NUM_GG_RIVAL = 3;
        public static final int DTC_NUM_GG_RIVAL_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_NUM_GG_RIVAL = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
