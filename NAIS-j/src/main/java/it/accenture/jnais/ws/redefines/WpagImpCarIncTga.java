package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WPAG-IMP-CAR-INC-TGA<br>
 * Variable: WPAG-IMP-CAR-INC-TGA from program LVES0269<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpagImpCarIncTga extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpagImpCarIncTga() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPAG_IMP_CAR_INC_TGA;
    }

    public void setWpagImpCarIncTga(AfDecimal wpagImpCarIncTga) {
        writeDecimalAsPacked(Pos.WPAG_IMP_CAR_INC_TGA, wpagImpCarIncTga.copy());
    }

    public void setWpagImpCarIncTgaFormatted(String wpagImpCarIncTga) {
        setWpagImpCarIncTga(PicParser.display(new PicParams("S9(12)V9(3)").setUsage(PicUsage.PACKED)).parseDecimal(Len.Int.WPAG_IMP_CAR_INC_TGA + Len.Fract.WPAG_IMP_CAR_INC_TGA, Len.Fract.WPAG_IMP_CAR_INC_TGA, wpagImpCarIncTga));
    }

    public void setWpagImpCarIncTgaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPAG_IMP_CAR_INC_TGA, Pos.WPAG_IMP_CAR_INC_TGA);
    }

    /**Original name: WPAG-IMP-CAR-INC-TGA<br>*/
    public AfDecimal getWpagImpCarIncTga() {
        return readPackedAsDecimal(Pos.WPAG_IMP_CAR_INC_TGA, Len.Int.WPAG_IMP_CAR_INC_TGA, Len.Fract.WPAG_IMP_CAR_INC_TGA);
    }

    public byte[] getWpagImpCarIncTgaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPAG_IMP_CAR_INC_TGA, Pos.WPAG_IMP_CAR_INC_TGA);
        return buffer;
    }

    public void initWpagImpCarIncTgaSpaces() {
        fill(Pos.WPAG_IMP_CAR_INC_TGA, Len.WPAG_IMP_CAR_INC_TGA, Types.SPACE_CHAR);
    }

    public void setWpagImpCarIncTgaNull(String wpagImpCarIncTgaNull) {
        writeString(Pos.WPAG_IMP_CAR_INC_TGA_NULL, wpagImpCarIncTgaNull, Len.WPAG_IMP_CAR_INC_TGA_NULL);
    }

    /**Original name: WPAG-IMP-CAR-INC-TGA-NULL<br>*/
    public String getWpagImpCarIncTgaNull() {
        return readString(Pos.WPAG_IMP_CAR_INC_TGA_NULL, Len.WPAG_IMP_CAR_INC_TGA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_INC_TGA = 1;
        public static final int WPAG_IMP_CAR_INC_TGA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPAG_IMP_CAR_INC_TGA = 8;
        public static final int WPAG_IMP_CAR_INC_TGA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_INC_TGA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPAG_IMP_CAR_INC_TGA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
