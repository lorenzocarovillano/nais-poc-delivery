package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: ISPC0040-IMP-SCONTO<br>
 * Variables: ISPC0040-IMP-SCONTO from copybook ISPC0040<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0040ImpSconto {

    //==== PROPERTIES ====
    //Original name: ISPC0040-COD-IMP-SCO
    private String codImpSco = DefaultValues.stringVal(Len.COD_IMP_SCO);
    //Original name: ISPC0040-DESC-IMP-SCO
    private String descImpSco = DefaultValues.stringVal(Len.DESC_IMP_SCO);

    //==== METHODS ====
    public void setImpScontoBytes(byte[] buffer, int offset) {
        int position = offset;
        codImpSco = MarshalByte.readString(buffer, position, Len.COD_IMP_SCO);
        position += Len.COD_IMP_SCO;
        descImpSco = MarshalByte.readString(buffer, position, Len.DESC_IMP_SCO);
    }

    public byte[] getImpScontoBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, codImpSco, Len.COD_IMP_SCO);
        position += Len.COD_IMP_SCO;
        MarshalByte.writeString(buffer, position, descImpSco, Len.DESC_IMP_SCO);
        return buffer;
    }

    public void initImpScontoSpaces() {
        codImpSco = "";
        descImpSco = "";
    }

    public void setCodImpSco(String codImpSco) {
        this.codImpSco = Functions.subString(codImpSco, Len.COD_IMP_SCO);
    }

    public String getCodImpSco() {
        return this.codImpSco;
    }

    public void setDescImpSco(String descImpSco) {
        this.descImpSco = Functions.subString(descImpSco, Len.DESC_IMP_SCO);
    }

    public String getDescImpSco() {
        return this.descImpSco;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int COD_IMP_SCO = 2;
        public static final int DESC_IMP_SCO = 30;
        public static final int IMP_SCONTO = COD_IMP_SCO + DESC_IMP_SCO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
