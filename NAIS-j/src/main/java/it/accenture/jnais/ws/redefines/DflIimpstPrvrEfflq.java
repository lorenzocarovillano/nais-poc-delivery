package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IIMPST-PRVR-EFFLQ<br>
 * Variable: DFL-IIMPST-PRVR-EFFLQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIimpstPrvrEfflq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIimpstPrvrEfflq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IIMPST_PRVR_EFFLQ;
    }

    public void setDflIimpstPrvrEfflq(AfDecimal dflIimpstPrvrEfflq) {
        writeDecimalAsPacked(Pos.DFL_IIMPST_PRVR_EFFLQ, dflIimpstPrvrEfflq.copy());
    }

    public void setDflIimpstPrvrEfflqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IIMPST_PRVR_EFFLQ, Pos.DFL_IIMPST_PRVR_EFFLQ);
    }

    /**Original name: DFL-IIMPST-PRVR-EFFLQ<br>*/
    public AfDecimal getDflIimpstPrvrEfflq() {
        return readPackedAsDecimal(Pos.DFL_IIMPST_PRVR_EFFLQ, Len.Int.DFL_IIMPST_PRVR_EFFLQ, Len.Fract.DFL_IIMPST_PRVR_EFFLQ);
    }

    public byte[] getDflIimpstPrvrEfflqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IIMPST_PRVR_EFFLQ, Pos.DFL_IIMPST_PRVR_EFFLQ);
        return buffer;
    }

    public void setDflIimpstPrvrEfflqNull(String dflIimpstPrvrEfflqNull) {
        writeString(Pos.DFL_IIMPST_PRVR_EFFLQ_NULL, dflIimpstPrvrEfflqNull, Len.DFL_IIMPST_PRVR_EFFLQ_NULL);
    }

    /**Original name: DFL-IIMPST-PRVR-EFFLQ-NULL<br>*/
    public String getDflIimpstPrvrEfflqNull() {
        return readString(Pos.DFL_IIMPST_PRVR_EFFLQ_NULL, Len.DFL_IIMPST_PRVR_EFFLQ_NULL);
    }

    public String getDflIimpstPrvrEfflqNullFormatted() {
        return Functions.padBlanks(getDflIimpstPrvrEfflqNull(), Len.DFL_IIMPST_PRVR_EFFLQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST_PRVR_EFFLQ = 1;
        public static final int DFL_IIMPST_PRVR_EFFLQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IIMPST_PRVR_EFFLQ = 8;
        public static final int DFL_IIMPST_PRVR_EFFLQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST_PRVR_EFFLQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IIMPST_PRVR_EFFLQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
