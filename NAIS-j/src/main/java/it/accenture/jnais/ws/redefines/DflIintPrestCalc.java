package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IINT-PREST-CALC<br>
 * Variable: DFL-IINT-PREST-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflIintPrestCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflIintPrestCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IINT_PREST_CALC;
    }

    public void setDflIintPrestCalc(AfDecimal dflIintPrestCalc) {
        writeDecimalAsPacked(Pos.DFL_IINT_PREST_CALC, dflIintPrestCalc.copy());
    }

    public void setDflIintPrestCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IINT_PREST_CALC, Pos.DFL_IINT_PREST_CALC);
    }

    /**Original name: DFL-IINT-PREST-CALC<br>*/
    public AfDecimal getDflIintPrestCalc() {
        return readPackedAsDecimal(Pos.DFL_IINT_PREST_CALC, Len.Int.DFL_IINT_PREST_CALC, Len.Fract.DFL_IINT_PREST_CALC);
    }

    public byte[] getDflIintPrestCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IINT_PREST_CALC, Pos.DFL_IINT_PREST_CALC);
        return buffer;
    }

    public void setDflIintPrestCalcNull(String dflIintPrestCalcNull) {
        writeString(Pos.DFL_IINT_PREST_CALC_NULL, dflIintPrestCalcNull, Len.DFL_IINT_PREST_CALC_NULL);
    }

    /**Original name: DFL-IINT-PREST-CALC-NULL<br>*/
    public String getDflIintPrestCalcNull() {
        return readString(Pos.DFL_IINT_PREST_CALC_NULL, Len.DFL_IINT_PREST_CALC_NULL);
    }

    public String getDflIintPrestCalcNullFormatted() {
        return Functions.padBlanks(getDflIintPrestCalcNull(), Len.DFL_IINT_PREST_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IINT_PREST_CALC = 1;
        public static final int DFL_IINT_PREST_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IINT_PREST_CALC = 8;
        public static final int DFL_IINT_PREST_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IINT_PREST_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IINT_PREST_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
