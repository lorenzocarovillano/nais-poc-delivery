package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.D09CsocketPortNum;
import it.accenture.jnais.ws.redefines.D09MqTempoAttesa1;
import it.accenture.jnais.ws.redefines.D09MqTempoAttesa2;
import it.accenture.jnais.ws.redefines.D09MqTempoExpiry;

/**Original name: PARAM-INFR-APPL<br>
 * Variable: PARAM-INFR-APPL from copybook IDBVD091<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ParamInfrAppl extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: D09-COD-COMP-ANIA
    private int d09CodCompAnia = DefaultValues.INT_VAL;
    //Original name: D09-AMBIENTE
    private String d09Ambiente = DefaultValues.stringVal(Len.D09_AMBIENTE);
    //Original name: D09-PIATTAFORMA
    private String d09Piattaforma = DefaultValues.stringVal(Len.D09_PIATTAFORMA);
    //Original name: D09-TP-COM-COBOL-JAVA
    private String d09TpComCobolJava = DefaultValues.stringVal(Len.D09_TP_COM_COBOL_JAVA);
    //Original name: D09-MQ-TP-UTILIZZO-API
    private String d09MqTpUtilizzoApi = DefaultValues.stringVal(Len.D09_MQ_TP_UTILIZZO_API);
    //Original name: D09-MQ-QUEUE-MANAGER
    private String d09MqQueueManager = DefaultValues.stringVal(Len.D09_MQ_QUEUE_MANAGER);
    //Original name: D09-MQ-CODA-PUT
    private String d09MqCodaPut = DefaultValues.stringVal(Len.D09_MQ_CODA_PUT);
    //Original name: D09-MQ-CODA-GET
    private String d09MqCodaGet = DefaultValues.stringVal(Len.D09_MQ_CODA_GET);
    //Original name: D09-MQ-OPZ-PERSISTENZA
    private char d09MqOpzPersistenza = DefaultValues.CHAR_VAL;
    //Original name: D09-MQ-OPZ-WAIT
    private char d09MqOpzWait = DefaultValues.CHAR_VAL;
    //Original name: D09-MQ-OPZ-SYNCPOINT
    private char d09MqOpzSyncpoint = DefaultValues.CHAR_VAL;
    //Original name: D09-MQ-ATTESA-RISPOSTA
    private char d09MqAttesaRisposta = DefaultValues.CHAR_VAL;
    //Original name: D09-MQ-TEMPO-ATTESA-1
    private D09MqTempoAttesa1 d09MqTempoAttesa1 = new D09MqTempoAttesa1();
    //Original name: D09-MQ-TEMPO-ATTESA-2
    private D09MqTempoAttesa2 d09MqTempoAttesa2 = new D09MqTempoAttesa2();
    //Original name: D09-MQ-TEMPO-EXPIRY
    private D09MqTempoExpiry d09MqTempoExpiry = new D09MqTempoExpiry();
    //Original name: D09-CSOCKET-IP-ADDRESS
    private String d09CsocketIpAddress = DefaultValues.stringVal(Len.D09_CSOCKET_IP_ADDRESS);
    //Original name: D09-CSOCKET-PORT-NUM
    private D09CsocketPortNum d09CsocketPortNum = new D09CsocketPortNum();
    //Original name: D09-FL-COMPRESSORE-C
    private char d09FlCompressoreC = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PARAM_INFR_APPL;
    }

    @Override
    public void deserialize(byte[] buf) {
        setParamInfrApplBytes(buf);
    }

    public String getParamInfrApplFormatted() {
        return MarshalByteExt.bufferToStr(getParamInfrApplBytes());
    }

    public void setParamInfrApplBytes(byte[] buffer) {
        setParamInfrApplBytes(buffer, 1);
    }

    public byte[] getParamInfrApplBytes() {
        byte[] buffer = new byte[Len.PARAM_INFR_APPL];
        return getParamInfrApplBytes(buffer, 1);
    }

    public void setParamInfrApplBytes(byte[] buffer, int offset) {
        int position = offset;
        d09CodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.D09_COD_COMP_ANIA, 0);
        position += Len.D09_COD_COMP_ANIA;
        d09Ambiente = MarshalByte.readString(buffer, position, Len.D09_AMBIENTE);
        position += Len.D09_AMBIENTE;
        d09Piattaforma = MarshalByte.readString(buffer, position, Len.D09_PIATTAFORMA);
        position += Len.D09_PIATTAFORMA;
        d09TpComCobolJava = MarshalByte.readString(buffer, position, Len.D09_TP_COM_COBOL_JAVA);
        position += Len.D09_TP_COM_COBOL_JAVA;
        d09MqTpUtilizzoApi = MarshalByte.readString(buffer, position, Len.D09_MQ_TP_UTILIZZO_API);
        position += Len.D09_MQ_TP_UTILIZZO_API;
        d09MqQueueManager = MarshalByte.readString(buffer, position, Len.D09_MQ_QUEUE_MANAGER);
        position += Len.D09_MQ_QUEUE_MANAGER;
        d09MqCodaPut = MarshalByte.readString(buffer, position, Len.D09_MQ_CODA_PUT);
        position += Len.D09_MQ_CODA_PUT;
        d09MqCodaGet = MarshalByte.readString(buffer, position, Len.D09_MQ_CODA_GET);
        position += Len.D09_MQ_CODA_GET;
        d09MqOpzPersistenza = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        d09MqOpzWait = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        d09MqOpzSyncpoint = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        d09MqAttesaRisposta = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        d09MqTempoAttesa1.setD09MqTempoAttesa1FromBuffer(buffer, position);
        position += D09MqTempoAttesa1.Len.D09_MQ_TEMPO_ATTESA1;
        d09MqTempoAttesa2.setD09MqTempoAttesa2FromBuffer(buffer, position);
        position += D09MqTempoAttesa2.Len.D09_MQ_TEMPO_ATTESA2;
        d09MqTempoExpiry.setD09MqTempoExpiryFromBuffer(buffer, position);
        position += D09MqTempoExpiry.Len.D09_MQ_TEMPO_EXPIRY;
        d09CsocketIpAddress = MarshalByte.readString(buffer, position, Len.D09_CSOCKET_IP_ADDRESS);
        position += Len.D09_CSOCKET_IP_ADDRESS;
        d09CsocketPortNum.setD09CsocketPortNumFromBuffer(buffer, position);
        position += D09CsocketPortNum.Len.D09_CSOCKET_PORT_NUM;
        d09FlCompressoreC = MarshalByte.readChar(buffer, position);
    }

    public byte[] getParamInfrApplBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, d09CodCompAnia, Len.Int.D09_COD_COMP_ANIA, 0);
        position += Len.D09_COD_COMP_ANIA;
        MarshalByte.writeString(buffer, position, d09Ambiente, Len.D09_AMBIENTE);
        position += Len.D09_AMBIENTE;
        MarshalByte.writeString(buffer, position, d09Piattaforma, Len.D09_PIATTAFORMA);
        position += Len.D09_PIATTAFORMA;
        MarshalByte.writeString(buffer, position, d09TpComCobolJava, Len.D09_TP_COM_COBOL_JAVA);
        position += Len.D09_TP_COM_COBOL_JAVA;
        MarshalByte.writeString(buffer, position, d09MqTpUtilizzoApi, Len.D09_MQ_TP_UTILIZZO_API);
        position += Len.D09_MQ_TP_UTILIZZO_API;
        MarshalByte.writeString(buffer, position, d09MqQueueManager, Len.D09_MQ_QUEUE_MANAGER);
        position += Len.D09_MQ_QUEUE_MANAGER;
        MarshalByte.writeString(buffer, position, d09MqCodaPut, Len.D09_MQ_CODA_PUT);
        position += Len.D09_MQ_CODA_PUT;
        MarshalByte.writeString(buffer, position, d09MqCodaGet, Len.D09_MQ_CODA_GET);
        position += Len.D09_MQ_CODA_GET;
        MarshalByte.writeChar(buffer, position, d09MqOpzPersistenza);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, d09MqOpzWait);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, d09MqOpzSyncpoint);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, d09MqAttesaRisposta);
        position += Types.CHAR_SIZE;
        d09MqTempoAttesa1.getD09MqTempoAttesa1AsBuffer(buffer, position);
        position += D09MqTempoAttesa1.Len.D09_MQ_TEMPO_ATTESA1;
        d09MqTempoAttesa2.getD09MqTempoAttesa2AsBuffer(buffer, position);
        position += D09MqTempoAttesa2.Len.D09_MQ_TEMPO_ATTESA2;
        d09MqTempoExpiry.getD09MqTempoExpiryAsBuffer(buffer, position);
        position += D09MqTempoExpiry.Len.D09_MQ_TEMPO_EXPIRY;
        MarshalByte.writeString(buffer, position, d09CsocketIpAddress, Len.D09_CSOCKET_IP_ADDRESS);
        position += Len.D09_CSOCKET_IP_ADDRESS;
        d09CsocketPortNum.getD09CsocketPortNumAsBuffer(buffer, position);
        position += D09CsocketPortNum.Len.D09_CSOCKET_PORT_NUM;
        MarshalByte.writeChar(buffer, position, d09FlCompressoreC);
        return buffer;
    }

    public void setD09CodCompAnia(int d09CodCompAnia) {
        this.d09CodCompAnia = d09CodCompAnia;
    }

    public int getD09CodCompAnia() {
        return this.d09CodCompAnia;
    }

    public void setD09Ambiente(String d09Ambiente) {
        this.d09Ambiente = Functions.subString(d09Ambiente, Len.D09_AMBIENTE);
    }

    public String getD09Ambiente() {
        return this.d09Ambiente;
    }

    public void setD09Piattaforma(String d09Piattaforma) {
        this.d09Piattaforma = Functions.subString(d09Piattaforma, Len.D09_PIATTAFORMA);
    }

    public String getD09Piattaforma() {
        return this.d09Piattaforma;
    }

    public void setD09TpComCobolJava(String d09TpComCobolJava) {
        this.d09TpComCobolJava = Functions.subString(d09TpComCobolJava, Len.D09_TP_COM_COBOL_JAVA);
    }

    public String getD09TpComCobolJava() {
        return this.d09TpComCobolJava;
    }

    public void setD09MqTpUtilizzoApi(String d09MqTpUtilizzoApi) {
        this.d09MqTpUtilizzoApi = Functions.subString(d09MqTpUtilizzoApi, Len.D09_MQ_TP_UTILIZZO_API);
    }

    public String getD09MqTpUtilizzoApi() {
        return this.d09MqTpUtilizzoApi;
    }

    public String getD09MqTpUtilizzoApiFormatted() {
        return Functions.padBlanks(getD09MqTpUtilizzoApi(), Len.D09_MQ_TP_UTILIZZO_API);
    }

    public void setD09MqQueueManager(String d09MqQueueManager) {
        this.d09MqQueueManager = Functions.subString(d09MqQueueManager, Len.D09_MQ_QUEUE_MANAGER);
    }

    public String getD09MqQueueManager() {
        return this.d09MqQueueManager;
    }

    public String getD09MqQueueManagerFormatted() {
        return Functions.padBlanks(getD09MqQueueManager(), Len.D09_MQ_QUEUE_MANAGER);
    }

    public void setD09MqCodaPut(String d09MqCodaPut) {
        this.d09MqCodaPut = Functions.subString(d09MqCodaPut, Len.D09_MQ_CODA_PUT);
    }

    public String getD09MqCodaPut() {
        return this.d09MqCodaPut;
    }

    public String getD09MqCodaPutFormatted() {
        return Functions.padBlanks(getD09MqCodaPut(), Len.D09_MQ_CODA_PUT);
    }

    public void setD09MqCodaGet(String d09MqCodaGet) {
        this.d09MqCodaGet = Functions.subString(d09MqCodaGet, Len.D09_MQ_CODA_GET);
    }

    public String getD09MqCodaGet() {
        return this.d09MqCodaGet;
    }

    public String getD09MqCodaGetFormatted() {
        return Functions.padBlanks(getD09MqCodaGet(), Len.D09_MQ_CODA_GET);
    }

    public void setD09MqOpzPersistenza(char d09MqOpzPersistenza) {
        this.d09MqOpzPersistenza = d09MqOpzPersistenza;
    }

    public char getD09MqOpzPersistenza() {
        return this.d09MqOpzPersistenza;
    }

    public void setD09MqOpzWait(char d09MqOpzWait) {
        this.d09MqOpzWait = d09MqOpzWait;
    }

    public char getD09MqOpzWait() {
        return this.d09MqOpzWait;
    }

    public void setD09MqOpzSyncpoint(char d09MqOpzSyncpoint) {
        this.d09MqOpzSyncpoint = d09MqOpzSyncpoint;
    }

    public char getD09MqOpzSyncpoint() {
        return this.d09MqOpzSyncpoint;
    }

    public void setD09MqAttesaRisposta(char d09MqAttesaRisposta) {
        this.d09MqAttesaRisposta = d09MqAttesaRisposta;
    }

    public char getD09MqAttesaRisposta() {
        return this.d09MqAttesaRisposta;
    }

    public void setD09CsocketIpAddress(String d09CsocketIpAddress) {
        this.d09CsocketIpAddress = Functions.subString(d09CsocketIpAddress, Len.D09_CSOCKET_IP_ADDRESS);
    }

    public String getD09CsocketIpAddress() {
        return this.d09CsocketIpAddress;
    }

    public String getD09CsocketIpAddressFormatted() {
        return Functions.padBlanks(getD09CsocketIpAddress(), Len.D09_CSOCKET_IP_ADDRESS);
    }

    public void setD09FlCompressoreC(char d09FlCompressoreC) {
        this.d09FlCompressoreC = d09FlCompressoreC;
    }

    public char getD09FlCompressoreC() {
        return this.d09FlCompressoreC;
    }

    public D09CsocketPortNum getD09CsocketPortNum() {
        return d09CsocketPortNum;
    }

    public D09MqTempoAttesa1 getD09MqTempoAttesa1() {
        return d09MqTempoAttesa1;
    }

    public D09MqTempoAttesa2 getD09MqTempoAttesa2() {
        return d09MqTempoAttesa2;
    }

    public D09MqTempoExpiry getD09MqTempoExpiry() {
        return d09MqTempoExpiry;
    }

    @Override
    public byte[] serialize() {
        return getParamInfrApplBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int D09_COD_COMP_ANIA = 3;
        public static final int D09_AMBIENTE = 10;
        public static final int D09_PIATTAFORMA = 10;
        public static final int D09_TP_COM_COBOL_JAVA = 10;
        public static final int D09_MQ_TP_UTILIZZO_API = 3;
        public static final int D09_MQ_QUEUE_MANAGER = 10;
        public static final int D09_MQ_CODA_PUT = 10;
        public static final int D09_MQ_CODA_GET = 10;
        public static final int D09_MQ_OPZ_PERSISTENZA = 1;
        public static final int D09_MQ_OPZ_WAIT = 1;
        public static final int D09_MQ_OPZ_SYNCPOINT = 1;
        public static final int D09_MQ_ATTESA_RISPOSTA = 1;
        public static final int D09_CSOCKET_IP_ADDRESS = 20;
        public static final int D09_FL_COMPRESSORE_C = 1;
        public static final int PARAM_INFR_APPL = D09_COD_COMP_ANIA + D09_AMBIENTE + D09_PIATTAFORMA + D09_TP_COM_COBOL_JAVA + D09_MQ_TP_UTILIZZO_API + D09_MQ_QUEUE_MANAGER + D09_MQ_CODA_PUT + D09_MQ_CODA_GET + D09_MQ_OPZ_PERSISTENZA + D09_MQ_OPZ_WAIT + D09_MQ_OPZ_SYNCPOINT + D09_MQ_ATTESA_RISPOSTA + D09MqTempoAttesa1.Len.D09_MQ_TEMPO_ATTESA1 + D09MqTempoAttesa2.Len.D09_MQ_TEMPO_ATTESA2 + D09MqTempoExpiry.Len.D09_MQ_TEMPO_EXPIRY + D09_CSOCKET_IP_ADDRESS + D09CsocketPortNum.Len.D09_CSOCKET_PORT_NUM + D09_FL_COMPRESSORE_C;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D09_COD_COMP_ANIA = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
