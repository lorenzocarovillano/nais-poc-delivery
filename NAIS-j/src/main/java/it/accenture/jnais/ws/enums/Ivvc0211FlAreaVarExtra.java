package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IVVC0211-FL-AREA-VAR-EXTRA<br>
 * Variable: IVVC0211-FL-AREA-VAR-EXTRA from copybook IVVC0211<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Ivvc0211FlAreaVarExtra {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlAreaVarExtra(char flAreaVarExtra) {
        this.value = flAreaVarExtra;
    }

    public char getFlAreaVarExtra() {
        return this.value;
    }

    public void setS211FlAreaVarExtraSi() {
        value = SI;
    }

    public void setS211FlAreaVarExtraNo() {
        value = NO;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FL_AREA_VAR_EXTRA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
