package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-RP-IN<br>
 * Variable: WPCO-DT-ULT-BOLL-RP-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollRpIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollRpIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_RP_IN;
    }

    public void setWpcoDtUltBollRpIn(int wpcoDtUltBollRpIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_RP_IN, wpcoDtUltBollRpIn, Len.Int.WPCO_DT_ULT_BOLL_RP_IN);
    }

    public void setDpcoDtUltBollRpInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RP_IN, Pos.WPCO_DT_ULT_BOLL_RP_IN);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RP-IN<br>*/
    public int getWpcoDtUltBollRpIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_RP_IN, Len.Int.WPCO_DT_ULT_BOLL_RP_IN);
    }

    public byte[] getWpcoDtUltBollRpInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RP_IN, Pos.WPCO_DT_ULT_BOLL_RP_IN);
        return buffer;
    }

    public void setWpcoDtUltBollRpInNull(String wpcoDtUltBollRpInNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_RP_IN_NULL, wpcoDtUltBollRpInNull, Len.WPCO_DT_ULT_BOLL_RP_IN_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RP-IN-NULL<br>*/
    public String getWpcoDtUltBollRpInNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_RP_IN_NULL, Len.WPCO_DT_ULT_BOLL_RP_IN_NULL);
    }

    public String getWpcoDtUltBollRpInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollRpInNull(), Len.WPCO_DT_ULT_BOLL_RP_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RP_IN = 1;
        public static final int WPCO_DT_ULT_BOLL_RP_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RP_IN = 5;
        public static final int WPCO_DT_ULT_BOLL_RP_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_RP_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
