package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-REMUN-ASS<br>
 * Variable: WDTR-REMUN-ASS from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_REMUN_ASS;
    }

    public void setWdtrRemunAss(AfDecimal wdtrRemunAss) {
        writeDecimalAsPacked(Pos.WDTR_REMUN_ASS, wdtrRemunAss.copy());
    }

    /**Original name: WDTR-REMUN-ASS<br>*/
    public AfDecimal getWdtrRemunAss() {
        return readPackedAsDecimal(Pos.WDTR_REMUN_ASS, Len.Int.WDTR_REMUN_ASS, Len.Fract.WDTR_REMUN_ASS);
    }

    public void setWdtrRemunAssNull(String wdtrRemunAssNull) {
        writeString(Pos.WDTR_REMUN_ASS_NULL, wdtrRemunAssNull, Len.WDTR_REMUN_ASS_NULL);
    }

    /**Original name: WDTR-REMUN-ASS-NULL<br>*/
    public String getWdtrRemunAssNull() {
        return readString(Pos.WDTR_REMUN_ASS_NULL, Len.WDTR_REMUN_ASS_NULL);
    }

    public String getWdtrRemunAssNullFormatted() {
        return Functions.padBlanks(getWdtrRemunAssNull(), Len.WDTR_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_REMUN_ASS = 1;
        public static final int WDTR_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_REMUN_ASS = 8;
        public static final int WDTR_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
