package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-SPE-MED<br>
 * Variable: DTC-SPE-MED from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_SPE_MED;
    }

    public void setDtcSpeMed(AfDecimal dtcSpeMed) {
        writeDecimalAsPacked(Pos.DTC_SPE_MED, dtcSpeMed.copy());
    }

    public void setDtcSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_SPE_MED, Pos.DTC_SPE_MED);
    }

    /**Original name: DTC-SPE-MED<br>*/
    public AfDecimal getDtcSpeMed() {
        return readPackedAsDecimal(Pos.DTC_SPE_MED, Len.Int.DTC_SPE_MED, Len.Fract.DTC_SPE_MED);
    }

    public byte[] getDtcSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_SPE_MED, Pos.DTC_SPE_MED);
        return buffer;
    }

    public void setDtcSpeMedNull(String dtcSpeMedNull) {
        writeString(Pos.DTC_SPE_MED_NULL, dtcSpeMedNull, Len.DTC_SPE_MED_NULL);
    }

    /**Original name: DTC-SPE-MED-NULL<br>*/
    public String getDtcSpeMedNull() {
        return readString(Pos.DTC_SPE_MED_NULL, Len.DTC_SPE_MED_NULL);
    }

    public String getDtcSpeMedNullFormatted() {
        return Functions.padBlanks(getDtcSpeMedNull(), Len.DTC_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_SPE_MED = 1;
        public static final int DTC_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_SPE_MED = 8;
        public static final int DTC_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTC_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
