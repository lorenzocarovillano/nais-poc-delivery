package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TIPO-SEGNO<br>
 * Variable: TIPO-SEGNO from copybook IDSV0501<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TipoSegno {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SENZA_SEGNO = ' ';
    public static final char SEGNO_POSITIVO = '+';
    public static final char SEGNO_NEGATIVO = '-';

    //==== METHODS ====
    public void setTipoSegno(char tipoSegno) {
        this.value = tipoSegno;
    }

    public char getTipoSegno() {
        return this.value;
    }

    public void setSenzaSegno() {
        value = SENZA_SEGNO;
    }

    public void setSegnoPositivo() {
        value = SEGNO_POSITIVO;
    }

    public boolean isSegnoNegativo() {
        return value == SEGNO_NEGATIVO;
    }

    public void setSegnoNegativo() {
        value = SEGNO_NEGATIVO;
    }
}
