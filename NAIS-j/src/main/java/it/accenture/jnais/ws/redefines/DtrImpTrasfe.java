package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-IMP-TRASFE<br>
 * Variable: DTR-IMP-TRASFE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrImpTrasfe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrImpTrasfe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_IMP_TRASFE;
    }

    public void setDtrImpTrasfe(AfDecimal dtrImpTrasfe) {
        writeDecimalAsPacked(Pos.DTR_IMP_TRASFE, dtrImpTrasfe.copy());
    }

    public void setDtrImpTrasfeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_IMP_TRASFE, Pos.DTR_IMP_TRASFE);
    }

    /**Original name: DTR-IMP-TRASFE<br>*/
    public AfDecimal getDtrImpTrasfe() {
        return readPackedAsDecimal(Pos.DTR_IMP_TRASFE, Len.Int.DTR_IMP_TRASFE, Len.Fract.DTR_IMP_TRASFE);
    }

    public byte[] getDtrImpTrasfeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_IMP_TRASFE, Pos.DTR_IMP_TRASFE);
        return buffer;
    }

    public void setDtrImpTrasfeNull(String dtrImpTrasfeNull) {
        writeString(Pos.DTR_IMP_TRASFE_NULL, dtrImpTrasfeNull, Len.DTR_IMP_TRASFE_NULL);
    }

    /**Original name: DTR-IMP-TRASFE-NULL<br>*/
    public String getDtrImpTrasfeNull() {
        return readString(Pos.DTR_IMP_TRASFE_NULL, Len.DTR_IMP_TRASFE_NULL);
    }

    public String getDtrImpTrasfeNullFormatted() {
        return Functions.padBlanks(getDtrImpTrasfeNull(), Len.DTR_IMP_TRASFE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_IMP_TRASFE = 1;
        public static final int DTR_IMP_TRASFE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_IMP_TRASFE = 8;
        public static final int DTR_IMP_TRASFE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_IMP_TRASFE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_IMP_TRASFE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
