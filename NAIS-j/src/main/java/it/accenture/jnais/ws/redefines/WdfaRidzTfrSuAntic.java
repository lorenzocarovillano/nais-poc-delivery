package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDFA-RIDZ-TFR-SU-ANTIC<br>
 * Variable: WDFA-RIDZ-TFR-SU-ANTIC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdfaRidzTfrSuAntic extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdfaRidzTfrSuAntic() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDFA_RIDZ_TFR_SU_ANTIC;
    }

    public void setWdfaRidzTfrSuAntic(AfDecimal wdfaRidzTfrSuAntic) {
        writeDecimalAsPacked(Pos.WDFA_RIDZ_TFR_SU_ANTIC, wdfaRidzTfrSuAntic.copy());
    }

    public void setWdfaRidzTfrSuAnticFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDFA_RIDZ_TFR_SU_ANTIC, Pos.WDFA_RIDZ_TFR_SU_ANTIC);
    }

    /**Original name: WDFA-RIDZ-TFR-SU-ANTIC<br>*/
    public AfDecimal getWdfaRidzTfrSuAntic() {
        return readPackedAsDecimal(Pos.WDFA_RIDZ_TFR_SU_ANTIC, Len.Int.WDFA_RIDZ_TFR_SU_ANTIC, Len.Fract.WDFA_RIDZ_TFR_SU_ANTIC);
    }

    public byte[] getWdfaRidzTfrSuAnticAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDFA_RIDZ_TFR_SU_ANTIC, Pos.WDFA_RIDZ_TFR_SU_ANTIC);
        return buffer;
    }

    public void setWdfaRidzTfrSuAnticNull(String wdfaRidzTfrSuAnticNull) {
        writeString(Pos.WDFA_RIDZ_TFR_SU_ANTIC_NULL, wdfaRidzTfrSuAnticNull, Len.WDFA_RIDZ_TFR_SU_ANTIC_NULL);
    }

    /**Original name: WDFA-RIDZ-TFR-SU-ANTIC-NULL<br>*/
    public String getWdfaRidzTfrSuAnticNull() {
        return readString(Pos.WDFA_RIDZ_TFR_SU_ANTIC_NULL, Len.WDFA_RIDZ_TFR_SU_ANTIC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDFA_RIDZ_TFR_SU_ANTIC = 1;
        public static final int WDFA_RIDZ_TFR_SU_ANTIC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDFA_RIDZ_TFR_SU_ANTIC = 8;
        public static final int WDFA_RIDZ_TFR_SU_ANTIC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDFA_RIDZ_TFR_SU_ANTIC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDFA_RIDZ_TFR_SU_ANTIC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
