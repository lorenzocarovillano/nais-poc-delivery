package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: PLI-ID-RAPP-ANA<br>
 * Variable: PLI-ID-RAPP-ANA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PliIdRappAna extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PliIdRappAna() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PLI_ID_RAPP_ANA;
    }

    public void setPliIdRappAna(int pliIdRappAna) {
        writeIntAsPacked(Pos.PLI_ID_RAPP_ANA, pliIdRappAna, Len.Int.PLI_ID_RAPP_ANA);
    }

    public void setPliIdRappAnaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PLI_ID_RAPP_ANA, Pos.PLI_ID_RAPP_ANA);
    }

    /**Original name: PLI-ID-RAPP-ANA<br>*/
    public int getPliIdRappAna() {
        return readPackedAsInt(Pos.PLI_ID_RAPP_ANA, Len.Int.PLI_ID_RAPP_ANA);
    }

    public byte[] getPliIdRappAnaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PLI_ID_RAPP_ANA, Pos.PLI_ID_RAPP_ANA);
        return buffer;
    }

    public void setPliIdRappAnaNull(String pliIdRappAnaNull) {
        writeString(Pos.PLI_ID_RAPP_ANA_NULL, pliIdRappAnaNull, Len.PLI_ID_RAPP_ANA_NULL);
    }

    /**Original name: PLI-ID-RAPP-ANA-NULL<br>*/
    public String getPliIdRappAnaNull() {
        return readString(Pos.PLI_ID_RAPP_ANA_NULL, Len.PLI_ID_RAPP_ANA_NULL);
    }

    public String getPliIdRappAnaNullFormatted() {
        return Functions.padBlanks(getPliIdRappAnaNull(), Len.PLI_ID_RAPP_ANA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PLI_ID_RAPP_ANA = 1;
        public static final int PLI_ID_RAPP_ANA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PLI_ID_RAPP_ANA = 5;
        public static final int PLI_ID_RAPP_ANA_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PLI_ID_RAPP_ANA = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
