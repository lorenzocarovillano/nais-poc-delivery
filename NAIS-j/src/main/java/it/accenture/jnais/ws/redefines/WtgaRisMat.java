package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-RIS-MAT<br>
 * Variable: WTGA-RIS-MAT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaRisMat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaRisMat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_RIS_MAT;
    }

    public void setWtgaRisMat(AfDecimal wtgaRisMat) {
        writeDecimalAsPacked(Pos.WTGA_RIS_MAT, wtgaRisMat.copy());
    }

    public void setWtgaRisMatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_RIS_MAT, Pos.WTGA_RIS_MAT);
    }

    /**Original name: WTGA-RIS-MAT<br>*/
    public AfDecimal getWtgaRisMat() {
        return readPackedAsDecimal(Pos.WTGA_RIS_MAT, Len.Int.WTGA_RIS_MAT, Len.Fract.WTGA_RIS_MAT);
    }

    public byte[] getWtgaRisMatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_RIS_MAT, Pos.WTGA_RIS_MAT);
        return buffer;
    }

    public void initWtgaRisMatSpaces() {
        fill(Pos.WTGA_RIS_MAT, Len.WTGA_RIS_MAT, Types.SPACE_CHAR);
    }

    public void setWtgaRisMatNull(String wtgaRisMatNull) {
        writeString(Pos.WTGA_RIS_MAT_NULL, wtgaRisMatNull, Len.WTGA_RIS_MAT_NULL);
    }

    /**Original name: WTGA-RIS-MAT-NULL<br>*/
    public String getWtgaRisMatNull() {
        return readString(Pos.WTGA_RIS_MAT_NULL, Len.WTGA_RIS_MAT_NULL);
    }

    public String getWtgaRisMatNullFormatted() {
        return Functions.padBlanks(getWtgaRisMatNull(), Len.WTGA_RIS_MAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_RIS_MAT = 1;
        public static final int WTGA_RIS_MAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_RIS_MAT = 8;
        public static final int WTGA_RIS_MAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_RIS_MAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_RIS_MAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
