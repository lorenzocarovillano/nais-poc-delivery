package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRE-DOV-RIVTO-T<br>
 * Variable: WB03-PRE-DOV-RIVTO-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PreDovRivtoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PreDovRivtoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRE_DOV_RIVTO_T;
    }

    public void setWb03PreDovRivtoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRE_DOV_RIVTO_T, Pos.WB03_PRE_DOV_RIVTO_T);
    }

    /**Original name: WB03-PRE-DOV-RIVTO-T<br>*/
    public AfDecimal getWb03PreDovRivtoT() {
        return readPackedAsDecimal(Pos.WB03_PRE_DOV_RIVTO_T, Len.Int.WB03_PRE_DOV_RIVTO_T, Len.Fract.WB03_PRE_DOV_RIVTO_T);
    }

    public byte[] getWb03PreDovRivtoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRE_DOV_RIVTO_T, Pos.WB03_PRE_DOV_RIVTO_T);
        return buffer;
    }

    public void setWb03PreDovRivtoTNull(String wb03PreDovRivtoTNull) {
        writeString(Pos.WB03_PRE_DOV_RIVTO_T_NULL, wb03PreDovRivtoTNull, Len.WB03_PRE_DOV_RIVTO_T_NULL);
    }

    /**Original name: WB03-PRE-DOV-RIVTO-T-NULL<br>*/
    public String getWb03PreDovRivtoTNull() {
        return readString(Pos.WB03_PRE_DOV_RIVTO_T_NULL, Len.WB03_PRE_DOV_RIVTO_T_NULL);
    }

    public String getWb03PreDovRivtoTNullFormatted() {
        return Functions.padBlanks(getWb03PreDovRivtoTNull(), Len.WB03_PRE_DOV_RIVTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRE_DOV_RIVTO_T = 1;
        public static final int WB03_PRE_DOV_RIVTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRE_DOV_RIVTO_T = 8;
        public static final int WB03_PRE_DOV_RIVTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRE_DOV_RIVTO_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRE_DOV_RIVTO_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
