package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTC-DT-END-COP<br>
 * Variable: DTC-DT-END-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtcDtEndCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtcDtEndCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTC_DT_END_COP;
    }

    public void setDtcDtEndCop(int dtcDtEndCop) {
        writeIntAsPacked(Pos.DTC_DT_END_COP, dtcDtEndCop, Len.Int.DTC_DT_END_COP);
    }

    public void setDtcDtEndCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTC_DT_END_COP, Pos.DTC_DT_END_COP);
    }

    /**Original name: DTC-DT-END-COP<br>*/
    public int getDtcDtEndCop() {
        return readPackedAsInt(Pos.DTC_DT_END_COP, Len.Int.DTC_DT_END_COP);
    }

    public byte[] getDtcDtEndCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTC_DT_END_COP, Pos.DTC_DT_END_COP);
        return buffer;
    }

    public void setDtcDtEndCopNull(String dtcDtEndCopNull) {
        writeString(Pos.DTC_DT_END_COP_NULL, dtcDtEndCopNull, Len.DTC_DT_END_COP_NULL);
    }

    /**Original name: DTC-DT-END-COP-NULL<br>*/
    public String getDtcDtEndCopNull() {
        return readString(Pos.DTC_DT_END_COP_NULL, Len.DTC_DT_END_COP_NULL);
    }

    public String getDtcDtEndCopNullFormatted() {
        return Functions.padBlanks(getDtcDtEndCopNull(), Len.DTC_DT_END_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTC_DT_END_COP = 1;
        public static final int DTC_DT_END_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTC_DT_END_COP = 5;
        public static final int DTC_DT_END_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DTC_DT_END_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
