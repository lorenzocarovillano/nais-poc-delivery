package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: ACC-COMM<br>
 * Variable: ACC-COMM from copybook IDBVP631<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AccComm extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: P63-ID-ACC-COMM
    private int idAccComm = DefaultValues.INT_VAL;
    //Original name: P63-TP-ACC-COMM
    private String tpAccComm = DefaultValues.stringVal(Len.TP_ACC_COMM);
    //Original name: P63-COD-COMP-ANIA
    private int codCompAnia = DefaultValues.INT_VAL;
    //Original name: P63-COD-PARTNER
    private long codPartner = DefaultValues.LONG_VAL;
    //Original name: P63-IB-ACC-COMM
    private String ibAccComm = DefaultValues.stringVal(Len.IB_ACC_COMM);
    //Original name: P63-IB-ACC-COMM-MASTER
    private String ibAccCommMaster = DefaultValues.stringVal(Len.IB_ACC_COMM_MASTER);
    //Original name: P63-DESC-ACC-COMM-LEN
    private short descAccCommLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P63-DESC-ACC-COMM
    private String descAccComm = DefaultValues.stringVal(Len.DESC_ACC_COMM);
    //Original name: P63-DT-FIRMA
    private int dtFirma = DefaultValues.INT_VAL;
    //Original name: P63-DT-INI-VLDT
    private int dtIniVldt = DefaultValues.INT_VAL;
    //Original name: P63-DT-END-VLDT
    private int dtEndVldt = DefaultValues.INT_VAL;
    //Original name: P63-FL-INVIO-CONFERME
    private char flInvioConferme = DefaultValues.CHAR_VAL;
    //Original name: P63-DS-OPER-SQL
    private char dsOperSql = DefaultValues.CHAR_VAL;
    //Original name: P63-DS-VER
    private int dsVer = DefaultValues.INT_VAL;
    //Original name: P63-DS-TS-CPTZ
    private long dsTsCptz = DefaultValues.LONG_VAL;
    //Original name: P63-DS-UTENTE
    private String dsUtente = DefaultValues.stringVal(Len.DS_UTENTE);
    //Original name: P63-DS-STATO-ELAB
    private char dsStatoElab = DefaultValues.CHAR_VAL;
    //Original name: P63-DESC-ACC-COMM-MAST-LEN
    private short descAccCommMastLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: P63-DESC-ACC-COMM-MAST
    private String descAccCommMast = DefaultValues.stringVal(Len.DESC_ACC_COMM_MAST);

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ACC_COMM;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAccCommBytes(buf);
    }

    public void setAccCommBytes(byte[] buffer) {
        setAccCommBytes(buffer, 1);
    }

    public byte[] getAccCommBytes() {
        byte[] buffer = new byte[Len.ACC_COMM];
        return getAccCommBytes(buffer, 1);
    }

    public void setAccCommBytes(byte[] buffer, int offset) {
        int position = offset;
        idAccComm = MarshalByte.readPackedAsInt(buffer, position, Len.Int.ID_ACC_COMM, 0);
        position += Len.ID_ACC_COMM;
        tpAccComm = MarshalByte.readString(buffer, position, Len.TP_ACC_COMM);
        position += Len.TP_ACC_COMM;
        codCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        codPartner = MarshalByte.readPackedAsLong(buffer, position, Len.Int.COD_PARTNER, 0);
        position += Len.COD_PARTNER;
        ibAccComm = MarshalByte.readString(buffer, position, Len.IB_ACC_COMM);
        position += Len.IB_ACC_COMM;
        ibAccCommMaster = MarshalByte.readString(buffer, position, Len.IB_ACC_COMM_MASTER);
        position += Len.IB_ACC_COMM_MASTER;
        setDescAccCommVcharBytes(buffer, position);
        position += Len.DESC_ACC_COMM_VCHAR;
        dtFirma = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_FIRMA, 0);
        position += Len.DT_FIRMA;
        dtIniVldt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_INI_VLDT, 0);
        position += Len.DT_INI_VLDT;
        dtEndVldt = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DT_END_VLDT, 0);
        position += Len.DT_END_VLDT;
        flInvioConferme = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        dsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        dsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        dsUtente = MarshalByte.readString(buffer, position, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        dsStatoElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setDescAccCommMastVcharBytes(buffer, position);
    }

    public byte[] getAccCommBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, idAccComm, Len.Int.ID_ACC_COMM, 0);
        position += Len.ID_ACC_COMM;
        MarshalByte.writeString(buffer, position, tpAccComm, Len.TP_ACC_COMM);
        position += Len.TP_ACC_COMM;
        MarshalByte.writeIntAsPacked(buffer, position, codCompAnia, Len.Int.COD_COMP_ANIA, 0);
        position += Len.COD_COMP_ANIA;
        MarshalByte.writeLongAsPacked(buffer, position, codPartner, Len.Int.COD_PARTNER, 0);
        position += Len.COD_PARTNER;
        MarshalByte.writeString(buffer, position, ibAccComm, Len.IB_ACC_COMM);
        position += Len.IB_ACC_COMM;
        MarshalByte.writeString(buffer, position, ibAccCommMaster, Len.IB_ACC_COMM_MASTER);
        position += Len.IB_ACC_COMM_MASTER;
        getDescAccCommVcharBytes(buffer, position);
        position += Len.DESC_ACC_COMM_VCHAR;
        MarshalByte.writeIntAsPacked(buffer, position, dtFirma, Len.Int.DT_FIRMA, 0);
        position += Len.DT_FIRMA;
        MarshalByte.writeIntAsPacked(buffer, position, dtIniVldt, Len.Int.DT_INI_VLDT, 0);
        position += Len.DT_INI_VLDT;
        MarshalByte.writeIntAsPacked(buffer, position, dtEndVldt, Len.Int.DT_END_VLDT, 0);
        position += Len.DT_END_VLDT;
        MarshalByte.writeChar(buffer, position, flInvioConferme);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, dsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, dsVer, Len.Int.DS_VER, 0);
        position += Len.DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, dsTsCptz, Len.Int.DS_TS_CPTZ, 0);
        position += Len.DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, dsUtente, Len.DS_UTENTE);
        position += Len.DS_UTENTE;
        MarshalByte.writeChar(buffer, position, dsStatoElab);
        position += Types.CHAR_SIZE;
        getDescAccCommMastVcharBytes(buffer, position);
        return buffer;
    }

    public void setIdAccComm(int idAccComm) {
        this.idAccComm = idAccComm;
    }

    public int getIdAccComm() {
        return this.idAccComm;
    }

    public void setTpAccComm(String tpAccComm) {
        this.tpAccComm = Functions.subString(tpAccComm, Len.TP_ACC_COMM);
    }

    public String getTpAccComm() {
        return this.tpAccComm;
    }

    public void setCodCompAnia(int codCompAnia) {
        this.codCompAnia = codCompAnia;
    }

    public int getCodCompAnia() {
        return this.codCompAnia;
    }

    public void setCodPartner(long codPartner) {
        this.codPartner = codPartner;
    }

    public long getCodPartner() {
        return this.codPartner;
    }

    public void setIbAccComm(String ibAccComm) {
        this.ibAccComm = Functions.subString(ibAccComm, Len.IB_ACC_COMM);
    }

    public String getIbAccComm() {
        return this.ibAccComm;
    }

    public void setIbAccCommMaster(String ibAccCommMaster) {
        this.ibAccCommMaster = Functions.subString(ibAccCommMaster, Len.IB_ACC_COMM_MASTER);
    }

    public String getIbAccCommMaster() {
        return this.ibAccCommMaster;
    }

    public void setDescAccCommVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DESC_ACC_COMM_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DESC_ACC_COMM_VCHAR);
        setDescAccCommVcharBytes(buffer, 1);
    }

    public String getDescAccCommVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDescAccCommVcharBytes());
    }

    /**Original name: P63-DESC-ACC-COMM-VCHAR<br>*/
    public byte[] getDescAccCommVcharBytes() {
        byte[] buffer = new byte[Len.DESC_ACC_COMM_VCHAR];
        return getDescAccCommVcharBytes(buffer, 1);
    }

    public void setDescAccCommVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        descAccCommLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        descAccComm = MarshalByte.readString(buffer, position, Len.DESC_ACC_COMM);
    }

    public byte[] getDescAccCommVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, descAccCommLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, descAccComm, Len.DESC_ACC_COMM);
        return buffer;
    }

    public void setDescAccCommLen(short descAccCommLen) {
        this.descAccCommLen = descAccCommLen;
    }

    public short getDescAccCommLen() {
        return this.descAccCommLen;
    }

    public void setDescAccComm(String descAccComm) {
        this.descAccComm = Functions.subString(descAccComm, Len.DESC_ACC_COMM);
    }

    public String getDescAccComm() {
        return this.descAccComm;
    }

    public void setDtFirma(int dtFirma) {
        this.dtFirma = dtFirma;
    }

    public int getDtFirma() {
        return this.dtFirma;
    }

    public void setDtIniVldt(int dtIniVldt) {
        this.dtIniVldt = dtIniVldt;
    }

    public int getDtIniVldt() {
        return this.dtIniVldt;
    }

    public void setDtEndVldt(int dtEndVldt) {
        this.dtEndVldt = dtEndVldt;
    }

    public int getDtEndVldt() {
        return this.dtEndVldt;
    }

    public void setFlInvioConferme(char flInvioConferme) {
        this.flInvioConferme = flInvioConferme;
    }

    public char getFlInvioConferme() {
        return this.flInvioConferme;
    }

    public void setDsOperSql(char dsOperSql) {
        this.dsOperSql = dsOperSql;
    }

    public void setDsOperSqlFormatted(String dsOperSql) {
        setDsOperSql(Functions.charAt(dsOperSql, Types.CHAR_SIZE));
    }

    public char getDsOperSql() {
        return this.dsOperSql;
    }

    public void setDsVer(int dsVer) {
        this.dsVer = dsVer;
    }

    public int getDsVer() {
        return this.dsVer;
    }

    public void setDsTsCptz(long dsTsCptz) {
        this.dsTsCptz = dsTsCptz;
    }

    public long getDsTsCptz() {
        return this.dsTsCptz;
    }

    public void setDsUtente(String dsUtente) {
        this.dsUtente = Functions.subString(dsUtente, Len.DS_UTENTE);
    }

    public String getDsUtente() {
        return this.dsUtente;
    }

    public void setDsStatoElab(char dsStatoElab) {
        this.dsStatoElab = dsStatoElab;
    }

    public void setDsStatoElabFormatted(String dsStatoElab) {
        setDsStatoElab(Functions.charAt(dsStatoElab, Types.CHAR_SIZE));
    }

    public char getDsStatoElab() {
        return this.dsStatoElab;
    }

    public void setDescAccCommMastVcharFormatted(String data) {
        byte[] buffer = new byte[Len.DESC_ACC_COMM_MAST_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.DESC_ACC_COMM_MAST_VCHAR);
        setDescAccCommMastVcharBytes(buffer, 1);
    }

    public String getDescAccCommMastVcharFormatted() {
        return MarshalByteExt.bufferToStr(getDescAccCommMastVcharBytes());
    }

    /**Original name: P63-DESC-ACC-COMM-MAST-VCHAR<br>*/
    public byte[] getDescAccCommMastVcharBytes() {
        byte[] buffer = new byte[Len.DESC_ACC_COMM_MAST_VCHAR];
        return getDescAccCommMastVcharBytes(buffer, 1);
    }

    public void setDescAccCommMastVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        descAccCommMastLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        descAccCommMast = MarshalByte.readString(buffer, position, Len.DESC_ACC_COMM_MAST);
    }

    public byte[] getDescAccCommMastVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, descAccCommMastLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, descAccCommMast, Len.DESC_ACC_COMM_MAST);
        return buffer;
    }

    public void setDescAccCommMastLen(short descAccCommMastLen) {
        this.descAccCommMastLen = descAccCommMastLen;
    }

    public short getDescAccCommMastLen() {
        return this.descAccCommMastLen;
    }

    public void setDescAccCommMast(String descAccCommMast) {
        this.descAccCommMast = Functions.subString(descAccCommMast, Len.DESC_ACC_COMM_MAST);
    }

    public String getDescAccCommMast() {
        return this.descAccCommMast;
    }

    @Override
    public byte[] serialize() {
        return getAccCommBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ID_ACC_COMM = 5;
        public static final int TP_ACC_COMM = 2;
        public static final int COD_COMP_ANIA = 3;
        public static final int COD_PARTNER = 6;
        public static final int IB_ACC_COMM = 40;
        public static final int IB_ACC_COMM_MASTER = 40;
        public static final int DESC_ACC_COMM_LEN = 2;
        public static final int DESC_ACC_COMM = 100;
        public static final int DESC_ACC_COMM_VCHAR = DESC_ACC_COMM_LEN + DESC_ACC_COMM;
        public static final int DT_FIRMA = 5;
        public static final int DT_INI_VLDT = 5;
        public static final int DT_END_VLDT = 5;
        public static final int FL_INVIO_CONFERME = 1;
        public static final int DS_OPER_SQL = 1;
        public static final int DS_VER = 5;
        public static final int DS_TS_CPTZ = 10;
        public static final int DS_UTENTE = 20;
        public static final int DS_STATO_ELAB = 1;
        public static final int DESC_ACC_COMM_MAST_LEN = 2;
        public static final int DESC_ACC_COMM_MAST = 100;
        public static final int DESC_ACC_COMM_MAST_VCHAR = DESC_ACC_COMM_MAST_LEN + DESC_ACC_COMM_MAST;
        public static final int ACC_COMM = ID_ACC_COMM + TP_ACC_COMM + COD_COMP_ANIA + COD_PARTNER + IB_ACC_COMM + IB_ACC_COMM_MASTER + DESC_ACC_COMM_VCHAR + DT_FIRMA + DT_INI_VLDT + DT_END_VLDT + FL_INVIO_CONFERME + DS_OPER_SQL + DS_VER + DS_TS_CPTZ + DS_UTENTE + DS_STATO_ELAB + DESC_ACC_COMM_MAST_VCHAR;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ID_ACC_COMM = 9;
            public static final int COD_COMP_ANIA = 5;
            public static final int COD_PARTNER = 10;
            public static final int DT_FIRMA = 8;
            public static final int DT_INI_VLDT = 8;
            public static final int DT_END_VLDT = 8;
            public static final int DS_VER = 9;
            public static final int DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
