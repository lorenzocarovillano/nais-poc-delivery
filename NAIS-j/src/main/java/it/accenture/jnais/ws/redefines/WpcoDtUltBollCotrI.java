package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-COTR-I<br>
 * Variable: WPCO-DT-ULT-BOLL-COTR-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollCotrI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollCotrI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_COTR_I;
    }

    public void setWpcoDtUltBollCotrI(int wpcoDtUltBollCotrI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_COTR_I, wpcoDtUltBollCotrI, Len.Int.WPCO_DT_ULT_BOLL_COTR_I);
    }

    public void setDpcoDtUltBollCotrIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_COTR_I, Pos.WPCO_DT_ULT_BOLL_COTR_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-COTR-I<br>*/
    public int getWpcoDtUltBollCotrI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_COTR_I, Len.Int.WPCO_DT_ULT_BOLL_COTR_I);
    }

    public byte[] getWpcoDtUltBollCotrIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_COTR_I, Pos.WPCO_DT_ULT_BOLL_COTR_I);
        return buffer;
    }

    public void setWpcoDtUltBollCotrINull(String wpcoDtUltBollCotrINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_COTR_I_NULL, wpcoDtUltBollCotrINull, Len.WPCO_DT_ULT_BOLL_COTR_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-COTR-I-NULL<br>*/
    public String getWpcoDtUltBollCotrINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_COTR_I_NULL, Len.WPCO_DT_ULT_BOLL_COTR_I_NULL);
    }

    public String getWpcoDtUltBollCotrINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollCotrINull(), Len.WPCO_DT_ULT_BOLL_COTR_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_COTR_I = 1;
        public static final int WPCO_DT_ULT_BOLL_COTR_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_COTR_I = 5;
        public static final int WPCO_DT_ULT_BOLL_COTR_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_COTR_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
