package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS3260<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs3260Data {

    //==== PROPERTIES ====
    //Original name: PGM-IDBSE060
    private String pgmIdbse060 = "IDBSE060";
    //Original name: WK-CAP-PROT
    private AfDecimal wkCapProt = new AfDecimal(DefaultValues.DEC_VAL, 18, 7);
    //Original name: EST-POLI
    private EstPoli estPoli = new EstPoli();
    //Original name: DPOL-ELE-ADES-MAX
    private short dpolEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = ((short)0);

    //==== METHODS ====
    public String getPgmIdbse060() {
        return this.pgmIdbse060;
    }

    public void setWkCapProt(AfDecimal wkCapProt) {
        this.wkCapProt.assign(wkCapProt);
    }

    public AfDecimal getWkCapProt() {
        return this.wkCapProt.copy();
    }

    public void setDpolAreaPoliFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POLI];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POLI);
        setDpolAreaPoliBytes(buffer, 1);
    }

    public void setDpolAreaPoliBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabAdesBytes(buffer, position);
    }

    public void setDpolEleAdesMax(short dpolEleAdesMax) {
        this.dpolEleAdesMax = dpolEleAdesMax;
    }

    public short getDpolEleAdesMax() {
        return this.dpolEleAdesMax;
    }

    public void setDpolTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public EstPoli getEstPoli() {
        return estPoli;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int DPOL_ELE_ADES_MAX = 2;
        public static final int DPOL_TAB_ADES = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POLI = DPOL_ELE_ADES_MAX + DPOL_TAB_ADES;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
