package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTLI-IMP-LRD-DFZ<br>
 * Variable: WTLI-IMP-LRD-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtliImpLrdDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtliImpLrdDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTLI_IMP_LRD_DFZ;
    }

    public void setWtliImpLrdDfz(AfDecimal wtliImpLrdDfz) {
        writeDecimalAsPacked(Pos.WTLI_IMP_LRD_DFZ, wtliImpLrdDfz.copy());
    }

    public void setWtliImpLrdDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTLI_IMP_LRD_DFZ, Pos.WTLI_IMP_LRD_DFZ);
    }

    /**Original name: WTLI-IMP-LRD-DFZ<br>*/
    public AfDecimal getWtliImpLrdDfz() {
        return readPackedAsDecimal(Pos.WTLI_IMP_LRD_DFZ, Len.Int.WTLI_IMP_LRD_DFZ, Len.Fract.WTLI_IMP_LRD_DFZ);
    }

    public byte[] getWtliImpLrdDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTLI_IMP_LRD_DFZ, Pos.WTLI_IMP_LRD_DFZ);
        return buffer;
    }

    public void initWtliImpLrdDfzSpaces() {
        fill(Pos.WTLI_IMP_LRD_DFZ, Len.WTLI_IMP_LRD_DFZ, Types.SPACE_CHAR);
    }

    public void setWtliImpLrdDfzNull(String wtliImpLrdDfzNull) {
        writeString(Pos.WTLI_IMP_LRD_DFZ_NULL, wtliImpLrdDfzNull, Len.WTLI_IMP_LRD_DFZ_NULL);
    }

    /**Original name: WTLI-IMP-LRD-DFZ-NULL<br>*/
    public String getWtliImpLrdDfzNull() {
        return readString(Pos.WTLI_IMP_LRD_DFZ_NULL, Len.WTLI_IMP_LRD_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_LRD_DFZ = 1;
        public static final int WTLI_IMP_LRD_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTLI_IMP_LRD_DFZ = 8;
        public static final int WTLI_IMP_LRD_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_LRD_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTLI_IMP_LRD_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
