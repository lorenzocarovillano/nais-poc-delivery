package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSV0003-RETURN-CODE<br>
 * Variable: IDSV0003-RETURN-CODE from copybook IDSV0003<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0003ReturnCode {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.RETURN_CODE);
    public static final String SUCCESSFUL_RC = "00";
    public static final String GENERIC_ERROR = "KO";
    public static final String INVALID_LEVEL_OPER = "D1";
    public static final String INVALID_OPER = "D2";
    public static final String SQL_ERROR = "D3";
    public static final String CONCURRENT_UPDATE = "D4";
    public static final String FIELD_NOT_VALUED = "C1";
    public static final String INVALID_CONVERSION = "C2";
    public static final String SEQUENCE_NOT_FOUND = "S1";
    public static final String COD_COMP_NOT_VALID = "ZA";
    public static final String STR_DATO_NOT_VALID = "ZB";
    public static final String BUFFER_DATI_NOT_V = "ZC";
    public static final String STR_DATO_DB_NOT_F = "ZD";
    public static final String COD_SERV_NOT_V = "ZE";
    public static final String EXCESS_COPY_I_O = "ZF";
    public static final String TIPO_MOVIM_NOT_V = "ZG";
    public static final String OPER_NOT_V = "ZH";
    public static final String SH_MEMORY_NOT_V = "ZI";
    public static final String QUEUE_MANAGEMENT = "ZL";
    public static final String SH_MEMORY_FLOOD = "ZM";
    public static final String EXCESS_SERV_NEWLIFE = "ZO";
    public static final String EXCESS_SERV_VSAM = "ZP";
    public static final String KEY_FIELDS_NOT_F = "ZQ";
    public static final String WHERE_FIELDS_NOT_F = "ZR";
    public static final String SERV_NOT_F_ON_MSS = "ZS";
    public static final String STR_RED_NOT_F_ON_RDS = "ZT";
    public static final String INSERT_SH_MEMORY = "ZU";
    public static final String CONVERTER_NOT_F = "ZV";
    public static final String EXCESS_OF_RECURSION = "ZW";
    public static final String VALUE_DEFAULT_NOT_F = "ZX";
    public static final String COPY_NOT_F_ON_IOS = "ZY";
    public static final String VALUE_NOT_F_ON_VSS = "ZZ";
    public static final String CACHE_PIENA = "CP";

    //==== METHODS ====
    public void setReturnCode(String returnCode) {
        this.value = Functions.subString(returnCode, Len.RETURN_CODE);
    }

    public String getReturnCode() {
        return this.value;
    }

    public String getReturnCodeFormatted() {
        return Functions.padBlanks(getReturnCode(), Len.RETURN_CODE);
    }

    public boolean isSuccessfulRc() {
        return value.equals(SUCCESSFUL_RC);
    }

    public void setIdsv0003SuccessfulRc() {
        value = SUCCESSFUL_RC;
    }

    public void setGenericError() {
        value = GENERIC_ERROR;
    }

    public void setInvalidLevelOper() {
        value = INVALID_LEVEL_OPER;
    }

    public boolean isIdsv0003InvalidOper() {
        return value.equals(INVALID_OPER);
    }

    public void setInvalidOper() {
        value = INVALID_OPER;
    }

    public boolean isIdsv0003SqlError() {
        return value.equals(SQL_ERROR);
    }

    public void setSqlError() {
        value = SQL_ERROR;
    }

    public boolean isIdsv0003FieldNotValued() {
        return value.equals(FIELD_NOT_VALUED);
    }

    public void setFieldNotValued() {
        value = FIELD_NOT_VALUED;
    }

    public boolean isIdsv0003InvalidConversion() {
        return value.equals(INVALID_CONVERSION);
    }

    public void setIdsv0003InvalidConversion() {
        value = INVALID_CONVERSION;
    }

    public void setCodCompNotValid() {
        value = COD_COMP_NOT_VALID;
    }

    public void setIdsv0003OperNotV() {
        value = OPER_NOT_V;
    }

    public void setIdsv0003ServNotFOnMss() {
        value = SERV_NOT_F_ON_MSS;
    }

    public void setIdsv0003CachePiena() {
        value = CACHE_PIENA;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int RETURN_CODE = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
