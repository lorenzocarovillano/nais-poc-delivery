package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L30-TP-INVST-LIQ<br>
 * Variable: L30-TP-INVST-LIQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L30TpInvstLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L30TpInvstLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L30_TP_INVST_LIQ;
    }

    public void setL30TpInvstLiq(short l30TpInvstLiq) {
        writeShortAsPacked(Pos.L30_TP_INVST_LIQ, l30TpInvstLiq, Len.Int.L30_TP_INVST_LIQ);
    }

    public void setL30TpInvstLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L30_TP_INVST_LIQ, Pos.L30_TP_INVST_LIQ);
    }

    /**Original name: L30-TP-INVST-LIQ<br>*/
    public short getL30TpInvstLiq() {
        return readPackedAsShort(Pos.L30_TP_INVST_LIQ, Len.Int.L30_TP_INVST_LIQ);
    }

    public byte[] getL30TpInvstLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L30_TP_INVST_LIQ, Pos.L30_TP_INVST_LIQ);
        return buffer;
    }

    public void setL30TpInvstLiqNull(String l30TpInvstLiqNull) {
        writeString(Pos.L30_TP_INVST_LIQ_NULL, l30TpInvstLiqNull, Len.L30_TP_INVST_LIQ_NULL);
    }

    /**Original name: L30-TP-INVST-LIQ-NULL<br>*/
    public String getL30TpInvstLiqNull() {
        return readString(Pos.L30_TP_INVST_LIQ_NULL, Len.L30_TP_INVST_LIQ_NULL);
    }

    public String getL30TpInvstLiqNullFormatted() {
        return Functions.padBlanks(getL30TpInvstLiqNull(), Len.L30_TP_INVST_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L30_TP_INVST_LIQ = 1;
        public static final int L30_TP_INVST_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L30_TP_INVST_LIQ = 2;
        public static final int L30_TP_INVST_LIQ_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L30_TP_INVST_LIQ = 2;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
