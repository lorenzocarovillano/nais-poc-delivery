package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WISO-DT-END-PER<br>
 * Variable: WISO-DT-END-PER from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoDtEndPer extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoDtEndPer() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_DT_END_PER;
    }

    public void setWisoDtEndPer(int wisoDtEndPer) {
        writeIntAsPacked(Pos.WISO_DT_END_PER, wisoDtEndPer, Len.Int.WISO_DT_END_PER);
    }

    public void setWisoDtEndPerFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_DT_END_PER, Pos.WISO_DT_END_PER);
    }

    /**Original name: WISO-DT-END-PER<br>*/
    public int getWisoDtEndPer() {
        return readPackedAsInt(Pos.WISO_DT_END_PER, Len.Int.WISO_DT_END_PER);
    }

    public byte[] getWisoDtEndPerAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_DT_END_PER, Pos.WISO_DT_END_PER);
        return buffer;
    }

    public void initWisoDtEndPerSpaces() {
        fill(Pos.WISO_DT_END_PER, Len.WISO_DT_END_PER, Types.SPACE_CHAR);
    }

    public void setWisoDtEndPerNull(String wisoDtEndPerNull) {
        writeString(Pos.WISO_DT_END_PER_NULL, wisoDtEndPerNull, Len.WISO_DT_END_PER_NULL);
    }

    /**Original name: WISO-DT-END-PER-NULL<br>*/
    public String getWisoDtEndPerNull() {
        return readString(Pos.WISO_DT_END_PER_NULL, Len.WISO_DT_END_PER_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_DT_END_PER = 1;
        public static final int WISO_DT_END_PER_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_DT_END_PER = 5;
        public static final int WISO_DT_END_PER_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_DT_END_PER = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
