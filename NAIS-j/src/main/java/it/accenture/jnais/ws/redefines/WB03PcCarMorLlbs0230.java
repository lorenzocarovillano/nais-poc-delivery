package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-PC-CAR-MOR<br>
 * Variable: W-B03-PC-CAR-MOR from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03PcCarMorLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03PcCarMorLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_PC_CAR_MOR;
    }

    public void setwB03PcCarMor(AfDecimal wB03PcCarMor) {
        writeDecimalAsPacked(Pos.W_B03_PC_CAR_MOR, wB03PcCarMor.copy());
    }

    /**Original name: W-B03-PC-CAR-MOR<br>*/
    public AfDecimal getwB03PcCarMor() {
        return readPackedAsDecimal(Pos.W_B03_PC_CAR_MOR, Len.Int.W_B03_PC_CAR_MOR, Len.Fract.W_B03_PC_CAR_MOR);
    }

    public byte[] getwB03PcCarMorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_PC_CAR_MOR, Pos.W_B03_PC_CAR_MOR);
        return buffer;
    }

    public void setwB03PcCarMorNull(String wB03PcCarMorNull) {
        writeString(Pos.W_B03_PC_CAR_MOR_NULL, wB03PcCarMorNull, Len.W_B03_PC_CAR_MOR_NULL);
    }

    /**Original name: W-B03-PC-CAR-MOR-NULL<br>*/
    public String getwB03PcCarMorNull() {
        return readString(Pos.W_B03_PC_CAR_MOR_NULL, Len.W_B03_PC_CAR_MOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_PC_CAR_MOR = 1;
        public static final int W_B03_PC_CAR_MOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_PC_CAR_MOR = 4;
        public static final int W_B03_PC_CAR_MOR_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_PC_CAR_MOR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_PC_CAR_MOR = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
