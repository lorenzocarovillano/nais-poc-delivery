package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-INTR-RIAT<br>
 * Variable: WDTC-INTR-RIAT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcIntrRiat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcIntrRiat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_INTR_RIAT;
    }

    public void setWdtcIntrRiat(AfDecimal wdtcIntrRiat) {
        writeDecimalAsPacked(Pos.WDTC_INTR_RIAT, wdtcIntrRiat.copy());
    }

    public void setWdtcIntrRiatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_INTR_RIAT, Pos.WDTC_INTR_RIAT);
    }

    /**Original name: WDTC-INTR-RIAT<br>*/
    public AfDecimal getWdtcIntrRiat() {
        return readPackedAsDecimal(Pos.WDTC_INTR_RIAT, Len.Int.WDTC_INTR_RIAT, Len.Fract.WDTC_INTR_RIAT);
    }

    public byte[] getWdtcIntrRiatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_INTR_RIAT, Pos.WDTC_INTR_RIAT);
        return buffer;
    }

    public void initWdtcIntrRiatSpaces() {
        fill(Pos.WDTC_INTR_RIAT, Len.WDTC_INTR_RIAT, Types.SPACE_CHAR);
    }

    public void setWdtcIntrRiatNull(String wdtcIntrRiatNull) {
        writeString(Pos.WDTC_INTR_RIAT_NULL, wdtcIntrRiatNull, Len.WDTC_INTR_RIAT_NULL);
    }

    /**Original name: WDTC-INTR-RIAT-NULL<br>*/
    public String getWdtcIntrRiatNull() {
        return readString(Pos.WDTC_INTR_RIAT_NULL, Len.WDTC_INTR_RIAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_RIAT = 1;
        public static final int WDTC_INTR_RIAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_INTR_RIAT = 8;
        public static final int WDTC_INTR_RIAT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_RIAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_INTR_RIAT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
