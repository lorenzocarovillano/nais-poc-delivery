package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: IDSI0021-OPERAZIONE<br>
 * Variable: IDSI0021-OPERAZIONE from copybook IDSI0021<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsi0021Operazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPERAZIONE);
    private static final String[] SELECT = new String[] {"SELECT", "SE"};
    public static final String AGGIORNAMENTO_STORICO = "AGG_STORICO";
    private static final String[] INSERT = new String[] {"INSERT", "IN"};
    private static final String[] UPDATE = new String[] {"UPDATE", "UP"};
    private static final String[] DELETE = new String[] {"DELETE", "DF"};
    private static final String[] DELETE_LOGICA = new String[] {"DELETE_LOGICA", "DL"};
    private static final String[] OPEN_CURSOR = new String[] {"OPEN_CURSOR", "OP"};
    private static final String[] CLOSE_CURSOR = new String[] {"CLOSE_CURSOR", "CL"};
    public static final String FETCH_FIRST = "FF";
    private static final String[] FETCH_NEXT = new String[] {"FETCH", "FN"};
    public static final String FETCH_FIRST_MULTIPLE = "FFM";
    public static final String FETCH_NEXT_MULTIPLE = "FNM";
    public static final String PRESA_IN_CARICO = "PRESA_IN_CARICO";

    //==== METHODS ====
    public void setOperazione(String operazione) {
        this.value = Functions.subString(operazione, Len.OPERAZIONE);
    }

    public String getOperazione() {
        return this.value;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPERAZIONE = 15;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
