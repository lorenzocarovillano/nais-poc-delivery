package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-PILDI-AA-C<br>
 * Variable: WPCO-DT-ULTC-PILDI-AA-C from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcPildiAaC extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcPildiAaC() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_PILDI_AA_C;
    }

    public void setWpcoDtUltcPildiAaC(int wpcoDtUltcPildiAaC) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_PILDI_AA_C, wpcoDtUltcPildiAaC, Len.Int.WPCO_DT_ULTC_PILDI_AA_C);
    }

    public void setDpcoDtUltcPildiAaCFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_AA_C, Pos.WPCO_DT_ULTC_PILDI_AA_C);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-AA-C<br>*/
    public int getWpcoDtUltcPildiAaC() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_PILDI_AA_C, Len.Int.WPCO_DT_ULTC_PILDI_AA_C);
    }

    public byte[] getWpcoDtUltcPildiAaCAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_PILDI_AA_C, Pos.WPCO_DT_ULTC_PILDI_AA_C);
        return buffer;
    }

    public void setWpcoDtUltcPildiAaCNull(String wpcoDtUltcPildiAaCNull) {
        writeString(Pos.WPCO_DT_ULTC_PILDI_AA_C_NULL, wpcoDtUltcPildiAaCNull, Len.WPCO_DT_ULTC_PILDI_AA_C_NULL);
    }

    /**Original name: WPCO-DT-ULTC-PILDI-AA-C-NULL<br>*/
    public String getWpcoDtUltcPildiAaCNull() {
        return readString(Pos.WPCO_DT_ULTC_PILDI_AA_C_NULL, Len.WPCO_DT_ULTC_PILDI_AA_C_NULL);
    }

    public String getWpcoDtUltcPildiAaCNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcPildiAaCNull(), Len.WPCO_DT_ULTC_PILDI_AA_C_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_AA_C = 1;
        public static final int WPCO_DT_ULTC_PILDI_AA_C_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_PILDI_AA_C = 5;
        public static final int WPCO_DT_ULTC_PILDI_AA_C_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_PILDI_AA_C = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
