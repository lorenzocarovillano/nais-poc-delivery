package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WGRZ-ETA-A-SCAD<br>
 * Variable: WGRZ-ETA-A-SCAD from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzEtaAScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzEtaAScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_ETA_A_SCAD;
    }

    public void setWgrzEtaAScad(int wgrzEtaAScad) {
        writeIntAsPacked(Pos.WGRZ_ETA_A_SCAD, wgrzEtaAScad, Len.Int.WGRZ_ETA_A_SCAD);
    }

    public void setWgrzEtaAScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_ETA_A_SCAD, Pos.WGRZ_ETA_A_SCAD);
    }

    /**Original name: WGRZ-ETA-A-SCAD<br>*/
    public int getWgrzEtaAScad() {
        return readPackedAsInt(Pos.WGRZ_ETA_A_SCAD, Len.Int.WGRZ_ETA_A_SCAD);
    }

    public byte[] getWgrzEtaAScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_ETA_A_SCAD, Pos.WGRZ_ETA_A_SCAD);
        return buffer;
    }

    public void initWgrzEtaAScadSpaces() {
        fill(Pos.WGRZ_ETA_A_SCAD, Len.WGRZ_ETA_A_SCAD, Types.SPACE_CHAR);
    }

    public void setWgrzEtaAScadNull(String wgrzEtaAScadNull) {
        writeString(Pos.WGRZ_ETA_A_SCAD_NULL, wgrzEtaAScadNull, Len.WGRZ_ETA_A_SCAD_NULL);
    }

    /**Original name: WGRZ-ETA-A-SCAD-NULL<br>*/
    public String getWgrzEtaAScadNull() {
        return readString(Pos.WGRZ_ETA_A_SCAD_NULL, Len.WGRZ_ETA_A_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_A_SCAD = 1;
        public static final int WGRZ_ETA_A_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_ETA_A_SCAD = 3;
        public static final int WGRZ_ETA_A_SCAD_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_ETA_A_SCAD = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
