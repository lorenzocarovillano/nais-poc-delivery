package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import it.accenture.jnais.copy.Idbvstb3;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Ldbv2631;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LDBS2630<br>
 * Generated as a class for rule WS.<br>*/
public class Ldbs2630Data {

    //==== PROPERTIES ====
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-STB-ID-MOVI-CHIU
    private short indStbIdMoviChiu = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDBVSTB3
    private Idbvstb3 idbvstb3 = new Idbvstb3();
    //Original name: LDBV2631
    private Ldbv2631 ldbv2631 = new Ldbv2631();

    //==== METHODS ====
    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndStbIdMoviChiu(short indStbIdMoviChiu) {
        this.indStbIdMoviChiu = indStbIdMoviChiu;
    }

    public short getIndStbIdMoviChiu() {
        return this.indStbIdMoviChiu;
    }

    public Idbvstb3 getIdbvstb3() {
        return idbvstb3;
    }

    public Idsv0010 getIdsv0010() {
        return idsv0010;
    }

    public Ldbv2631 getLdbv2631() {
        return ldbv2631;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
