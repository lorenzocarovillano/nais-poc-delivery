package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WGRZ-PC-1O-RAT<br>
 * Variable: WGRZ-PC-1O-RAT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzPc1oRat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzPc1oRat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_PC1O_RAT;
    }

    public void setWgrzPc1oRat(AfDecimal wgrzPc1oRat) {
        writeDecimalAsPacked(Pos.WGRZ_PC1O_RAT, wgrzPc1oRat.copy());
    }

    public void setWgrzPc1oRatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_PC1O_RAT, Pos.WGRZ_PC1O_RAT);
    }

    /**Original name: WGRZ-PC-1O-RAT<br>*/
    public AfDecimal getWgrzPc1oRat() {
        return readPackedAsDecimal(Pos.WGRZ_PC1O_RAT, Len.Int.WGRZ_PC1O_RAT, Len.Fract.WGRZ_PC1O_RAT);
    }

    public byte[] getWgrzPc1oRatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_PC1O_RAT, Pos.WGRZ_PC1O_RAT);
        return buffer;
    }

    public void initWgrzPc1oRatSpaces() {
        fill(Pos.WGRZ_PC1O_RAT, Len.WGRZ_PC1O_RAT, Types.SPACE_CHAR);
    }

    public void setWgrzPc1oRatNull(String wgrzPc1oRatNull) {
        writeString(Pos.WGRZ_PC1O_RAT_NULL, wgrzPc1oRatNull, Len.WGRZ_PC1O_RAT_NULL);
    }

    /**Original name: WGRZ-PC-1O-RAT-NULL<br>*/
    public String getWgrzPc1oRatNull() {
        return readString(Pos.WGRZ_PC1O_RAT_NULL, Len.WGRZ_PC1O_RAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_PC1O_RAT = 1;
        public static final int WGRZ_PC1O_RAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_PC1O_RAT = 4;
        public static final int WGRZ_PC1O_RAT_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WGRZ_PC1O_RAT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_PC1O_RAT = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
