package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WISO-ID-OGG<br>
 * Variable: WISO-ID-OGG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WisoIdOgg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WisoIdOgg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WISO_ID_OGG;
    }

    public void setWisoIdOgg(int wisoIdOgg) {
        writeIntAsPacked(Pos.WISO_ID_OGG, wisoIdOgg, Len.Int.WISO_ID_OGG);
    }

    public void setWisoIdOggFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WISO_ID_OGG, Pos.WISO_ID_OGG);
    }

    /**Original name: WISO-ID-OGG<br>*/
    public int getWisoIdOgg() {
        return readPackedAsInt(Pos.WISO_ID_OGG, Len.Int.WISO_ID_OGG);
    }

    public byte[] getWisoIdOggAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WISO_ID_OGG, Pos.WISO_ID_OGG);
        return buffer;
    }

    public void initWisoIdOggSpaces() {
        fill(Pos.WISO_ID_OGG, Len.WISO_ID_OGG, Types.SPACE_CHAR);
    }

    public void setWisoIdOggNull(String wisoIdOggNull) {
        writeString(Pos.WISO_ID_OGG_NULL, wisoIdOggNull, Len.WISO_ID_OGG_NULL);
    }

    /**Original name: WISO-ID-OGG-NULL<br>*/
    public String getWisoIdOggNull() {
        return readString(Pos.WISO_ID_OGG_NULL, Len.WISO_ID_OGG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WISO_ID_OGG = 1;
        public static final int WISO_ID_OGG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WISO_ID_OGG = 5;
        public static final int WISO_ID_OGG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WISO_ID_OGG = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
