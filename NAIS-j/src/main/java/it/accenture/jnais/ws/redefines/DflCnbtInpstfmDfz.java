package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNBT-INPSTFM-DFZ<br>
 * Variable: DFL-CNBT-INPSTFM-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCnbtInpstfmDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCnbtInpstfmDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNBT_INPSTFM_DFZ;
    }

    public void setDflCnbtInpstfmDfz(AfDecimal dflCnbtInpstfmDfz) {
        writeDecimalAsPacked(Pos.DFL_CNBT_INPSTFM_DFZ, dflCnbtInpstfmDfz.copy());
    }

    public void setDflCnbtInpstfmDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNBT_INPSTFM_DFZ, Pos.DFL_CNBT_INPSTFM_DFZ);
    }

    /**Original name: DFL-CNBT-INPSTFM-DFZ<br>*/
    public AfDecimal getDflCnbtInpstfmDfz() {
        return readPackedAsDecimal(Pos.DFL_CNBT_INPSTFM_DFZ, Len.Int.DFL_CNBT_INPSTFM_DFZ, Len.Fract.DFL_CNBT_INPSTFM_DFZ);
    }

    public byte[] getDflCnbtInpstfmDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNBT_INPSTFM_DFZ, Pos.DFL_CNBT_INPSTFM_DFZ);
        return buffer;
    }

    public void setDflCnbtInpstfmDfzNull(String dflCnbtInpstfmDfzNull) {
        writeString(Pos.DFL_CNBT_INPSTFM_DFZ_NULL, dflCnbtInpstfmDfzNull, Len.DFL_CNBT_INPSTFM_DFZ_NULL);
    }

    /**Original name: DFL-CNBT-INPSTFM-DFZ-NULL<br>*/
    public String getDflCnbtInpstfmDfzNull() {
        return readString(Pos.DFL_CNBT_INPSTFM_DFZ_NULL, Len.DFL_CNBT_INPSTFM_DFZ_NULL);
    }

    public String getDflCnbtInpstfmDfzNullFormatted() {
        return Functions.padBlanks(getDflCnbtInpstfmDfzNull(), Len.DFL_CNBT_INPSTFM_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNBT_INPSTFM_DFZ = 1;
        public static final int DFL_CNBT_INPSTFM_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNBT_INPSTFM_DFZ = 8;
        public static final int DFL_CNBT_INPSTFM_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNBT_INPSTFM_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNBT_INPSTFM_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
