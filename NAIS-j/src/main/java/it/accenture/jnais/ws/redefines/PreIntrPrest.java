package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PRE-INTR-PREST<br>
 * Variable: PRE-INTR-PREST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PreIntrPrest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PreIntrPrest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PRE_INTR_PREST;
    }

    public void setPreIntrPrest(AfDecimal preIntrPrest) {
        writeDecimalAsPacked(Pos.PRE_INTR_PREST, preIntrPrest.copy());
    }

    public void setPreIntrPrestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PRE_INTR_PREST, Pos.PRE_INTR_PREST);
    }

    /**Original name: PRE-INTR-PREST<br>*/
    public AfDecimal getPreIntrPrest() {
        return readPackedAsDecimal(Pos.PRE_INTR_PREST, Len.Int.PRE_INTR_PREST, Len.Fract.PRE_INTR_PREST);
    }

    public byte[] getPreIntrPrestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PRE_INTR_PREST, Pos.PRE_INTR_PREST);
        return buffer;
    }

    public void setPreIntrPrestNull(String preIntrPrestNull) {
        writeString(Pos.PRE_INTR_PREST_NULL, preIntrPrestNull, Len.PRE_INTR_PREST_NULL);
    }

    /**Original name: PRE-INTR-PREST-NULL<br>*/
    public String getPreIntrPrestNull() {
        return readString(Pos.PRE_INTR_PREST_NULL, Len.PRE_INTR_PREST_NULL);
    }

    public String getPreIntrPrestNullFormatted() {
        return Functions.padBlanks(getPreIntrPrestNull(), Len.PRE_INTR_PREST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PRE_INTR_PREST = 1;
        public static final int PRE_INTR_PREST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PRE_INTR_PREST = 4;
        public static final int PRE_INTR_PREST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PRE_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PRE_INTR_PREST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
