package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.occurs.WsdiTabStraInv;

/**Original name: WSDI-AREA-STRA-INV<br>
 * Variable: WSDI-AREA-STRA-INV from program LVES0269<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsdiAreaStraInv extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int TAB_STRA_INV_MAXOCCURS = 20;
    //Original name: WSDI-ELE-STRA-INV-MAX
    private short eleStraInvMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: WSDI-TAB-STRA-INV
    private WsdiTabStraInv[] tabStraInv = new WsdiTabStraInv[TAB_STRA_INV_MAXOCCURS];

    //==== CONSTRUCTORS ====
    public WsdiAreaStraInv() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WSDI_AREA_STRA_INV;
    }

    @Override
    public void deserialize(byte[] buf) {
        setWsdiAreaStraInvBytes(buf);
    }

    public void init() {
        for (int tabStraInvIdx = 1; tabStraInvIdx <= TAB_STRA_INV_MAXOCCURS; tabStraInvIdx++) {
            tabStraInv[tabStraInvIdx - 1] = new WsdiTabStraInv();
        }
    }

    public String getWsdiAreaStraInvFormatted() {
        return MarshalByteExt.bufferToStr(getWsdiAreaStraInvBytes());
    }

    public void setWsdiAreaStraInvBytes(byte[] buffer) {
        setWsdiAreaStraInvBytes(buffer, 1);
    }

    public byte[] getWsdiAreaStraInvBytes() {
        byte[] buffer = new byte[Len.WSDI_AREA_STRA_INV];
        return getWsdiAreaStraInvBytes(buffer, 1);
    }

    public void setWsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        eleStraInvMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_STRA_INV_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                tabStraInv[idx - 1].setWsdiTabStraInvBytes(buffer, position);
                position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
            }
            else {
                tabStraInv[idx - 1].initWsdiTabStraInvSpaces();
                position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
            }
        }
    }

    public byte[] getWsdiAreaStraInvBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, eleStraInvMax);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= TAB_STRA_INV_MAXOCCURS; idx++) {
            tabStraInv[idx - 1].getWsdiTabStraInvBytes(buffer, position);
            position += WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;
        }
        return buffer;
    }

    public void setEleStraInvMax(short eleStraInvMax) {
        this.eleStraInvMax = eleStraInvMax;
    }

    public short getEleStraInvMax() {
        return this.eleStraInvMax;
    }

    public WsdiTabStraInv getTabStraInv(int idx) {
        return tabStraInv[idx - 1];
    }

    @Override
    public byte[] serialize() {
        return getWsdiAreaStraInvBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int ELE_STRA_INV_MAX = 2;
        public static final int WSDI_AREA_STRA_INV = ELE_STRA_INV_MAX + WsdiAreaStraInv.TAB_STRA_INV_MAXOCCURS * WsdiTabStraInv.Len.WSDI_TAB_STRA_INV;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
