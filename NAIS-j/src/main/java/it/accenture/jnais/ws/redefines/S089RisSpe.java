package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-RIS-SPE<br>
 * Variable: S089-RIS-SPE from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089RisSpe extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089RisSpe() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_RIS_SPE;
    }

    public void setWlquRisSpe(AfDecimal wlquRisSpe) {
        writeDecimalAsPacked(Pos.S089_RIS_SPE, wlquRisSpe.copy());
    }

    public void setWlquRisSpeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_RIS_SPE, Pos.S089_RIS_SPE);
    }

    /**Original name: WLQU-RIS-SPE<br>*/
    public AfDecimal getWlquRisSpe() {
        return readPackedAsDecimal(Pos.S089_RIS_SPE, Len.Int.WLQU_RIS_SPE, Len.Fract.WLQU_RIS_SPE);
    }

    public byte[] getWlquRisSpeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_RIS_SPE, Pos.S089_RIS_SPE);
        return buffer;
    }

    public void initWlquRisSpeSpaces() {
        fill(Pos.S089_RIS_SPE, Len.S089_RIS_SPE, Types.SPACE_CHAR);
    }

    public void setWlquRisSpeNull(String wlquRisSpeNull) {
        writeString(Pos.S089_RIS_SPE_NULL, wlquRisSpeNull, Len.WLQU_RIS_SPE_NULL);
    }

    /**Original name: WLQU-RIS-SPE-NULL<br>*/
    public String getWlquRisSpeNull() {
        return readString(Pos.S089_RIS_SPE_NULL, Len.WLQU_RIS_SPE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_RIS_SPE = 1;
        public static final int S089_RIS_SPE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_RIS_SPE = 8;
        public static final int WLQU_RIS_SPE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_RIS_SPE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_RIS_SPE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
