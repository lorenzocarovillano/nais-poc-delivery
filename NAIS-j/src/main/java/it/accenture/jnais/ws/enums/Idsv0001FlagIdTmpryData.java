package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IDSV0001-FLAG-ID-TMPRY-DATA<br>
 * Variable: IDSV0001-FLAG-ID-TMPRY-DATA from copybook IDSV0001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idsv0001FlagIdTmpryData {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagIdTmpryData(char flagIdTmpryData) {
        this.value = flagIdTmpryData;
    }

    public char getFlagIdTmpryData() {
        return this.value;
    }

    public boolean isIdsv0001IdTmpryDataSi() {
        return value == SI;
    }

    public void setIdsv0001IdTmpryDataSi() {
        value = SI;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int FLAG_ID_TMPRY_DATA = 1;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
