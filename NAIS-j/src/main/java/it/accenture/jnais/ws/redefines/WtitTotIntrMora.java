package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-INTR-MORA<br>
 * Variable: WTIT-TOT-INTR-MORA from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotIntrMora extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotIntrMora() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_INTR_MORA;
    }

    public void setWtitTotIntrMora(AfDecimal wtitTotIntrMora) {
        writeDecimalAsPacked(Pos.WTIT_TOT_INTR_MORA, wtitTotIntrMora.copy());
    }

    public void setWtitTotIntrMoraFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_INTR_MORA, Pos.WTIT_TOT_INTR_MORA);
    }

    /**Original name: WTIT-TOT-INTR-MORA<br>*/
    public AfDecimal getWtitTotIntrMora() {
        return readPackedAsDecimal(Pos.WTIT_TOT_INTR_MORA, Len.Int.WTIT_TOT_INTR_MORA, Len.Fract.WTIT_TOT_INTR_MORA);
    }

    public byte[] getWtitTotIntrMoraAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_INTR_MORA, Pos.WTIT_TOT_INTR_MORA);
        return buffer;
    }

    public void initWtitTotIntrMoraSpaces() {
        fill(Pos.WTIT_TOT_INTR_MORA, Len.WTIT_TOT_INTR_MORA, Types.SPACE_CHAR);
    }

    public void setWtitTotIntrMoraNull(String wtitTotIntrMoraNull) {
        writeString(Pos.WTIT_TOT_INTR_MORA_NULL, wtitTotIntrMoraNull, Len.WTIT_TOT_INTR_MORA_NULL);
    }

    /**Original name: WTIT-TOT-INTR-MORA-NULL<br>*/
    public String getWtitTotIntrMoraNull() {
        return readString(Pos.WTIT_TOT_INTR_MORA_NULL, Len.WTIT_TOT_INTR_MORA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_MORA = 1;
        public static final int WTIT_TOT_INTR_MORA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_MORA = 8;
        public static final int WTIT_TOT_INTR_MORA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_MORA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_MORA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
