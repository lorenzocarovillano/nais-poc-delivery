package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.ws.occurs.WtitTabTitCont;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0096<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0096Data {

    //==== PROPERTIES ====
    public static final int DTIT_TAB_TIT_CONT_MAXOCCURS = 200;
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: WK-SQLCODE
    private String wkSqlcode = DefaultValues.stringVal(Len.WK_SQLCODE);
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DT-INI-COP
    private String wkDtIniCop = "00000000";
    //Original name: WK-DT-END-COP
    private String wkDtEndCop = "00000000";
    /**Original name: FORMATO<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO</pre>*/
    private char formato = DefaultValues.CHAR_VAL;
    //Original name: DATA-INFERIORE
    private DataInferioreLccs0490 dataInferiore = new DataInferioreLccs0490();
    //Original name: DATA-SUPERIORE
    private DataSuperioreLccs0490 dataSuperiore = new DataSuperioreLccs0490();
    //Original name: GG-DIFF
    private String ggDiff = DefaultValues.stringVal(Len.GG_DIFF);
    //Original name: GG-DIFF-1
    private String ggDiff1 = DefaultValues.stringVal(Len.GG_DIFF1);
    //Original name: CODICE-RITORNO
    private char codiceRitorno = DefaultValues.CHAR_VAL;
    //Original name: LDBV4011
    private Ldbv4011 ldbv4011 = new Ldbv4011();
    //Original name: DTIT-ELE-TIT-MAX
    private short dtitEleTitMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: DTIT-TAB-TIT-CONT
    private WtitTabTitCont[] dtitTabTitCont = new WtitTabTitCont[DTIT_TAB_TIT_CONT_MAXOCCURS];
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-TIT
    private short ixTabTit = DefaultValues.BIN_SHORT_VAL;

    //==== CONSTRUCTORS ====
    public Lvvs0096Data() {
        init();
    }

    //==== METHODS ====
    public void init() {
        for (int dtitTabTitContIdx = 1; dtitTabTitContIdx <= DTIT_TAB_TIT_CONT_MAXOCCURS; dtitTabTitContIdx++) {
            dtitTabTitCont[dtitTabTitContIdx - 1] = new WtitTabTitCont();
        }
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public void setWkSqlcode(long wkSqlcode) {
        this.wkSqlcode = PicFormatter.display("--(7)9").format(wkSqlcode).toString();
    }

    public long getWkSqlcode() {
        return PicParser.display("--(7)9").parseLong(this.wkSqlcode);
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setWkDtIniCopFormatted(String wkDtIniCop) {
        this.wkDtIniCop = Trunc.toUnsignedNumeric(wkDtIniCop, Len.WK_DT_INI_COP);
    }

    public int getWkDtIniCop() {
        return NumericDisplay.asInt(this.wkDtIniCop);
    }

    public String getWkDtIniCopFormatted() {
        return this.wkDtIniCop;
    }

    public void setWkDtEndCopFormatted(String wkDtEndCop) {
        this.wkDtEndCop = Trunc.toUnsignedNumeric(wkDtEndCop, Len.WK_DT_END_COP);
    }

    public int getWkDtEndCop() {
        return NumericDisplay.asInt(this.wkDtEndCop);
    }

    public String getWkDtEndCopFormatted() {
        return this.wkDtEndCop;
    }

    public void setFormato(char formato) {
        this.formato = formato;
    }

    public void setFormatoFormatted(String formato) {
        setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
    }

    public void setFormatoFromBuffer(byte[] buffer) {
        formato = MarshalByte.readChar(buffer, 1);
    }

    public char getFormato() {
        return this.formato;
    }

    public void setGgDiffFromBuffer(byte[] buffer) {
        ggDiff = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
    }

    public int getGgDiff() {
        return NumericDisplay.asInt(this.ggDiff);
    }

    public String getGgDiffFormatted() {
        return this.ggDiff;
    }

    public void setGgDiff1FromBuffer(byte[] buffer) {
        ggDiff1 = MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF1);
    }

    public int getGgDiff1() {
        return NumericDisplay.asInt(this.ggDiff1);
    }

    public String getGgDiff1Formatted() {
        return this.ggDiff1;
    }

    public void setCodiceRitorno(char codiceRitorno) {
        this.codiceRitorno = codiceRitorno;
    }

    public void setCodiceRitornoFromBuffer(byte[] buffer) {
        codiceRitorno = MarshalByte.readChar(buffer, 1);
    }

    public char getCodiceRitorno() {
        return this.codiceRitorno;
    }

    public void setDtitAreaTitFormatted(String data) {
        byte[] buffer = new byte[Len.DTIT_AREA_TIT];
        MarshalByte.writeString(buffer, 1, data, Len.DTIT_AREA_TIT);
        setDtitAreaTitBytes(buffer, 1);
    }

    public void setDtitAreaTitBytes(byte[] buffer, int offset) {
        int position = offset;
        dtitEleTitMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        for (int idx = 1; idx <= DTIT_TAB_TIT_CONT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                dtitTabTitCont[idx - 1].setWtitTabTitContBytes(buffer, position);
                position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
            }
            else {
                dtitTabTitCont[idx - 1].initWtitTabTitContSpaces();
                position += WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
            }
        }
    }

    public void setDtitEleTitMax(short dtitEleTitMax) {
        this.dtitEleTitMax = dtitEleTitMax;
    }

    public short getDtitEleTitMax() {
        return this.dtitEleTitMax;
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public void setIxTabTit(short ixTabTit) {
        this.ixTabTit = ixTabTit;
    }

    public short getIxTabTit() {
        return this.ixTabTit;
    }

    public DataInferioreLccs0490 getDataInferiore() {
        return dataInferiore;
    }

    public DataSuperioreLccs0490 getDataSuperiore() {
        return dataSuperiore;
    }

    public WtitTabTitCont getDtitTabTitCont(int idx) {
        return dtitTabTitCont[idx - 1];
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Ldbv4011 getLdbv4011() {
        return ldbv4011;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_SQLCODE = 9;
        public static final int WK_DT_INI_COP = 8;
        public static final int WK_DT_END_COP = 8;
        public static final int GG_DIFF = 5;
        public static final int GG_DIFF1 = 5;
        public static final int DTIT_ELE_TIT_MAX = 2;
        public static final int DTIT_AREA_TIT = DTIT_ELE_TIT_MAX + Lvvs0096Data.DTIT_TAB_TIT_CONT_MAXOCCURS * WtitTabTitCont.Len.WTIT_TAB_TIT_CONT;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
