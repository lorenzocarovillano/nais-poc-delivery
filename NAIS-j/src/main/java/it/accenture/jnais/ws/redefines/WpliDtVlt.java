package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WPLI-DT-VLT<br>
 * Variable: WPLI-DT-VLT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpliDtVlt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpliDtVlt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPLI_DT_VLT;
    }

    public void setWpliDtVlt(int wpliDtVlt) {
        writeIntAsPacked(Pos.WPLI_DT_VLT, wpliDtVlt, Len.Int.WPLI_DT_VLT);
    }

    public void setWpliDtVltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPLI_DT_VLT, Pos.WPLI_DT_VLT);
    }

    /**Original name: WPLI-DT-VLT<br>*/
    public int getWpliDtVlt() {
        return readPackedAsInt(Pos.WPLI_DT_VLT, Len.Int.WPLI_DT_VLT);
    }

    public byte[] getWpliDtVltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPLI_DT_VLT, Pos.WPLI_DT_VLT);
        return buffer;
    }

    public void initWpliDtVltSpaces() {
        fill(Pos.WPLI_DT_VLT, Len.WPLI_DT_VLT, Types.SPACE_CHAR);
    }

    public void setWpliDtVltNull(String wpliDtVltNull) {
        writeString(Pos.WPLI_DT_VLT_NULL, wpliDtVltNull, Len.WPLI_DT_VLT_NULL);
    }

    /**Original name: WPLI-DT-VLT-NULL<br>*/
    public String getWpliDtVltNull() {
        return readString(Pos.WPLI_DT_VLT_NULL, Len.WPLI_DT_VLT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPLI_DT_VLT = 1;
        public static final int WPLI_DT_VLT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPLI_DT_VLT = 5;
        public static final int WPLI_DT_VLT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPLI_DT_VLT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
