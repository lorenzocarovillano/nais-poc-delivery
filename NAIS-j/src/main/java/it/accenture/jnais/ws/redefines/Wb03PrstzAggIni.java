package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-PRSTZ-AGG-INI<br>
 * Variable: WB03-PRSTZ-AGG-INI from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03PrstzAggIni extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03PrstzAggIni() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_PRSTZ_AGG_INI;
    }

    public void setWb03PrstzAggIni(AfDecimal wb03PrstzAggIni) {
        writeDecimalAsPacked(Pos.WB03_PRSTZ_AGG_INI, wb03PrstzAggIni.copy());
    }

    public void setWb03PrstzAggIniFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_PRSTZ_AGG_INI, Pos.WB03_PRSTZ_AGG_INI);
    }

    /**Original name: WB03-PRSTZ-AGG-INI<br>*/
    public AfDecimal getWb03PrstzAggIni() {
        return readPackedAsDecimal(Pos.WB03_PRSTZ_AGG_INI, Len.Int.WB03_PRSTZ_AGG_INI, Len.Fract.WB03_PRSTZ_AGG_INI);
    }

    public byte[] getWb03PrstzAggIniAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_PRSTZ_AGG_INI, Pos.WB03_PRSTZ_AGG_INI);
        return buffer;
    }

    public void setWb03PrstzAggIniNull(String wb03PrstzAggIniNull) {
        writeString(Pos.WB03_PRSTZ_AGG_INI_NULL, wb03PrstzAggIniNull, Len.WB03_PRSTZ_AGG_INI_NULL);
    }

    /**Original name: WB03-PRSTZ-AGG-INI-NULL<br>*/
    public String getWb03PrstzAggIniNull() {
        return readString(Pos.WB03_PRSTZ_AGG_INI_NULL, Len.WB03_PRSTZ_AGG_INI_NULL);
    }

    public String getWb03PrstzAggIniNullFormatted() {
        return Functions.padBlanks(getWb03PrstzAggIniNull(), Len.WB03_PRSTZ_AGG_INI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_AGG_INI = 1;
        public static final int WB03_PRSTZ_AGG_INI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_PRSTZ_AGG_INI = 8;
        public static final int WB03_PRSTZ_AGG_INI_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_AGG_INI = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_PRSTZ_AGG_INI = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
