package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WDTC-CAR-ACQ<br>
 * Variable: WDTC-CAR-ACQ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_CAR_ACQ;
    }

    public void setWdtcCarAcq(AfDecimal wdtcCarAcq) {
        writeDecimalAsPacked(Pos.WDTC_CAR_ACQ, wdtcCarAcq.copy());
    }

    public void setWdtcCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_CAR_ACQ, Pos.WDTC_CAR_ACQ);
    }

    /**Original name: WDTC-CAR-ACQ<br>*/
    public AfDecimal getWdtcCarAcq() {
        return readPackedAsDecimal(Pos.WDTC_CAR_ACQ, Len.Int.WDTC_CAR_ACQ, Len.Fract.WDTC_CAR_ACQ);
    }

    public byte[] getWdtcCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_CAR_ACQ, Pos.WDTC_CAR_ACQ);
        return buffer;
    }

    public void initWdtcCarAcqSpaces() {
        fill(Pos.WDTC_CAR_ACQ, Len.WDTC_CAR_ACQ, Types.SPACE_CHAR);
    }

    public void setWdtcCarAcqNull(String wdtcCarAcqNull) {
        writeString(Pos.WDTC_CAR_ACQ_NULL, wdtcCarAcqNull, Len.WDTC_CAR_ACQ_NULL);
    }

    /**Original name: WDTC-CAR-ACQ-NULL<br>*/
    public String getWdtcCarAcqNull() {
        return readString(Pos.WDTC_CAR_ACQ_NULL, Len.WDTC_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_ACQ = 1;
        public static final int WDTC_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_CAR_ACQ = 8;
        public static final int WDTC_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
