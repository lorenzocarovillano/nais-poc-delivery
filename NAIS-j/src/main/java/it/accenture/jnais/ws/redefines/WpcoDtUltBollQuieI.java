package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-QUIE-I<br>
 * Variable: WPCO-DT-ULT-BOLL-QUIE-I from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollQuieI extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollQuieI() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_QUIE_I;
    }

    public void setWpcoDtUltBollQuieI(int wpcoDtUltBollQuieI) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_QUIE_I, wpcoDtUltBollQuieI, Len.Int.WPCO_DT_ULT_BOLL_QUIE_I);
    }

    public void setDpcoDtUltBollQuieIFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_QUIE_I, Pos.WPCO_DT_ULT_BOLL_QUIE_I);
    }

    /**Original name: WPCO-DT-ULT-BOLL-QUIE-I<br>*/
    public int getWpcoDtUltBollQuieI() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_QUIE_I, Len.Int.WPCO_DT_ULT_BOLL_QUIE_I);
    }

    public byte[] getWpcoDtUltBollQuieIAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_QUIE_I, Pos.WPCO_DT_ULT_BOLL_QUIE_I);
        return buffer;
    }

    public void setWpcoDtUltBollQuieINull(String wpcoDtUltBollQuieINull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_QUIE_I_NULL, wpcoDtUltBollQuieINull, Len.WPCO_DT_ULT_BOLL_QUIE_I_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-QUIE-I-NULL<br>*/
    public String getWpcoDtUltBollQuieINull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_QUIE_I_NULL, Len.WPCO_DT_ULT_BOLL_QUIE_I_NULL);
    }

    public String getWpcoDtUltBollQuieINullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollQuieINull(), Len.WPCO_DT_ULT_BOLL_QUIE_I_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_QUIE_I = 1;
        public static final int WPCO_DT_ULT_BOLL_QUIE_I_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_QUIE_I = 5;
        public static final int WPCO_DT_ULT_BOLL_QUIE_I_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_QUIE_I = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
