package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FLAG-OPEN-FILESQS6<br>
 * Variable: FLAG-OPEN-FILESQS6 from copybook IABVSQS1<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlagOpenFilesqs6 {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlagOpenFilesqs6(char flagOpenFilesqs6) {
        this.value = flagOpenFilesqs6;
    }

    public char getFlagOpenFilesqs6() {
        return this.value;
    }

    public boolean isOpenFilesqs6Si() {
        return value == SI;
    }

    public void setOpenFilesqs6Si() {
        value = SI;
    }

    public void setOpenFilesqs6No() {
        value = NO;
    }
}
