package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-MIN-GARTO-T<br>
 * Variable: B03-MIN-GARTO-T from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03MinGartoT extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03MinGartoT() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_MIN_GARTO_T;
    }

    public void setB03MinGartoT(AfDecimal b03MinGartoT) {
        writeDecimalAsPacked(Pos.B03_MIN_GARTO_T, b03MinGartoT.copy());
    }

    public void setB03MinGartoTFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_MIN_GARTO_T, Pos.B03_MIN_GARTO_T);
    }

    /**Original name: B03-MIN-GARTO-T<br>*/
    public AfDecimal getB03MinGartoT() {
        return readPackedAsDecimal(Pos.B03_MIN_GARTO_T, Len.Int.B03_MIN_GARTO_T, Len.Fract.B03_MIN_GARTO_T);
    }

    public byte[] getB03MinGartoTAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_MIN_GARTO_T, Pos.B03_MIN_GARTO_T);
        return buffer;
    }

    public void setB03MinGartoTNull(String b03MinGartoTNull) {
        writeString(Pos.B03_MIN_GARTO_T_NULL, b03MinGartoTNull, Len.B03_MIN_GARTO_T_NULL);
    }

    /**Original name: B03-MIN-GARTO-T-NULL<br>*/
    public String getB03MinGartoTNull() {
        return readString(Pos.B03_MIN_GARTO_T_NULL, Len.B03_MIN_GARTO_T_NULL);
    }

    public String getB03MinGartoTNullFormatted() {
        return Functions.padBlanks(getB03MinGartoTNull(), Len.B03_MIN_GARTO_T_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_MIN_GARTO_T = 1;
        public static final int B03_MIN_GARTO_T_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_MIN_GARTO_T = 8;
        public static final int B03_MIN_GARTO_T_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_MIN_GARTO_T = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_MIN_GARTO_T = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
