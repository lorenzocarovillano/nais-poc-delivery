package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DT-EMIS<br>
 * Variable: WTGA-DT-EMIS from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDtEmis extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDtEmis() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DT_EMIS;
    }

    public void setWtgaDtEmis(int wtgaDtEmis) {
        writeIntAsPacked(Pos.WTGA_DT_EMIS, wtgaDtEmis, Len.Int.WTGA_DT_EMIS);
    }

    public void setWtgaDtEmisFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DT_EMIS, Pos.WTGA_DT_EMIS);
    }

    /**Original name: WTGA-DT-EMIS<br>*/
    public int getWtgaDtEmis() {
        return readPackedAsInt(Pos.WTGA_DT_EMIS, Len.Int.WTGA_DT_EMIS);
    }

    public byte[] getWtgaDtEmisAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DT_EMIS, Pos.WTGA_DT_EMIS);
        return buffer;
    }

    public void initWtgaDtEmisSpaces() {
        fill(Pos.WTGA_DT_EMIS, Len.WTGA_DT_EMIS, Types.SPACE_CHAR);
    }

    public void setWtgaDtEmisNull(String wtgaDtEmisNull) {
        writeString(Pos.WTGA_DT_EMIS_NULL, wtgaDtEmisNull, Len.WTGA_DT_EMIS_NULL);
    }

    /**Original name: WTGA-DT-EMIS-NULL<br>*/
    public String getWtgaDtEmisNull() {
        return readString(Pos.WTGA_DT_EMIS_NULL, Len.WTGA_DT_EMIS_NULL);
    }

    public String getWtgaDtEmisNullFormatted() {
        return Functions.padBlanks(getWtgaDtEmisNull(), Len.WTGA_DT_EMIS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DT_EMIS = 1;
        public static final int WTGA_DT_EMIS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DT_EMIS = 5;
        public static final int WTGA_DT_EMIS_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DT_EMIS = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
