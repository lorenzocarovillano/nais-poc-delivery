package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TAX-SEP<br>
 * Variable: LQU-TAX-SEP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTaxSep extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTaxSep() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TAX_SEP;
    }

    public void setLquTaxSep(AfDecimal lquTaxSep) {
        writeDecimalAsPacked(Pos.LQU_TAX_SEP, lquTaxSep.copy());
    }

    public void setLquTaxSepFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TAX_SEP, Pos.LQU_TAX_SEP);
    }

    /**Original name: LQU-TAX-SEP<br>*/
    public AfDecimal getLquTaxSep() {
        return readPackedAsDecimal(Pos.LQU_TAX_SEP, Len.Int.LQU_TAX_SEP, Len.Fract.LQU_TAX_SEP);
    }

    public byte[] getLquTaxSepAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TAX_SEP, Pos.LQU_TAX_SEP);
        return buffer;
    }

    public void setLquTaxSepNull(String lquTaxSepNull) {
        writeString(Pos.LQU_TAX_SEP_NULL, lquTaxSepNull, Len.LQU_TAX_SEP_NULL);
    }

    /**Original name: LQU-TAX-SEP-NULL<br>*/
    public String getLquTaxSepNull() {
        return readString(Pos.LQU_TAX_SEP_NULL, Len.LQU_TAX_SEP_NULL);
    }

    public String getLquTaxSepNullFormatted() {
        return Functions.padBlanks(getLquTaxSepNull(), Len.LQU_TAX_SEP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TAX_SEP = 1;
        public static final int LQU_TAX_SEP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TAX_SEP = 8;
        public static final int LQU_TAX_SEP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TAX_SEP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TAX_SEP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
