package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: BEL-DT-ULT-DOCTO<br>
 * Variable: BEL-DT-ULT-DOCTO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class BelDtUltDocto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public BelDtUltDocto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BEL_DT_ULT_DOCTO;
    }

    public void setBelDtUltDocto(int belDtUltDocto) {
        writeIntAsPacked(Pos.BEL_DT_ULT_DOCTO, belDtUltDocto, Len.Int.BEL_DT_ULT_DOCTO);
    }

    public void setBelDtUltDoctoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.BEL_DT_ULT_DOCTO, Pos.BEL_DT_ULT_DOCTO);
    }

    /**Original name: BEL-DT-ULT-DOCTO<br>*/
    public int getBelDtUltDocto() {
        return readPackedAsInt(Pos.BEL_DT_ULT_DOCTO, Len.Int.BEL_DT_ULT_DOCTO);
    }

    public byte[] getBelDtUltDoctoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.BEL_DT_ULT_DOCTO, Pos.BEL_DT_ULT_DOCTO);
        return buffer;
    }

    public void setBelDtUltDoctoNull(String belDtUltDoctoNull) {
        writeString(Pos.BEL_DT_ULT_DOCTO_NULL, belDtUltDoctoNull, Len.BEL_DT_ULT_DOCTO_NULL);
    }

    /**Original name: BEL-DT-ULT-DOCTO-NULL<br>*/
    public String getBelDtUltDoctoNull() {
        return readString(Pos.BEL_DT_ULT_DOCTO_NULL, Len.BEL_DT_ULT_DOCTO_NULL);
    }

    public String getBelDtUltDoctoNullFormatted() {
        return Functions.padBlanks(getBelDtUltDoctoNull(), Len.BEL_DT_ULT_DOCTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int BEL_DT_ULT_DOCTO = 1;
        public static final int BEL_DT_ULT_DOCTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int BEL_DT_ULT_DOCTO = 5;
        public static final int BEL_DT_ULT_DOCTO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int BEL_DT_ULT_DOCTO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
