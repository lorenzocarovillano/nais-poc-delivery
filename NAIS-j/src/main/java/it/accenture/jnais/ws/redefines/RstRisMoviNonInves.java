package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: RST-RIS-MOVI-NON-INVES<br>
 * Variable: RST-RIS-MOVI-NON-INVES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RstRisMoviNonInves extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RstRisMoviNonInves() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RST_RIS_MOVI_NON_INVES;
    }

    public void setRstRisMoviNonInves(AfDecimal rstRisMoviNonInves) {
        writeDecimalAsPacked(Pos.RST_RIS_MOVI_NON_INVES, rstRisMoviNonInves.copy());
    }

    public void setRstRisMoviNonInvesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RST_RIS_MOVI_NON_INVES, Pos.RST_RIS_MOVI_NON_INVES);
    }

    /**Original name: RST-RIS-MOVI-NON-INVES<br>*/
    public AfDecimal getRstRisMoviNonInves() {
        return readPackedAsDecimal(Pos.RST_RIS_MOVI_NON_INVES, Len.Int.RST_RIS_MOVI_NON_INVES, Len.Fract.RST_RIS_MOVI_NON_INVES);
    }

    public byte[] getRstRisMoviNonInvesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RST_RIS_MOVI_NON_INVES, Pos.RST_RIS_MOVI_NON_INVES);
        return buffer;
    }

    public void setRstRisMoviNonInvesNull(String rstRisMoviNonInvesNull) {
        writeString(Pos.RST_RIS_MOVI_NON_INVES_NULL, rstRisMoviNonInvesNull, Len.RST_RIS_MOVI_NON_INVES_NULL);
    }

    /**Original name: RST-RIS-MOVI-NON-INVES-NULL<br>*/
    public String getRstRisMoviNonInvesNull() {
        return readString(Pos.RST_RIS_MOVI_NON_INVES_NULL, Len.RST_RIS_MOVI_NON_INVES_NULL);
    }

    public String getRstRisMoviNonInvesNullFormatted() {
        return Functions.padBlanks(getRstRisMoviNonInvesNull(), Len.RST_RIS_MOVI_NON_INVES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RST_RIS_MOVI_NON_INVES = 1;
        public static final int RST_RIS_MOVI_NON_INVES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RST_RIS_MOVI_NON_INVES = 8;
        public static final int RST_RIS_MOVI_NON_INVES_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RST_RIS_MOVI_NON_INVES = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int RST_RIS_MOVI_NON_INVES = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
