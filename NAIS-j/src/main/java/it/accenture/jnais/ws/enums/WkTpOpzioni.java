package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TP-OPZIONI<br>
 * Variable: WK-TP-OPZIONI from program LCCS0320<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WkTpOpzioni {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.WK_TP_OPZIONI);
    public static final String NESSUNA_OPZIONE = "00";
    public static final String RENDITA_VITALIZIA = "01";
    public static final String RENDITA_CERTA = "02";
    public static final String RENDITA_REVERSIBILE = "03";
    public static final String RENDITA_TEMPORANEA = "04";
    public static final String DIFFERIMENTO = "05";
    public static final String PROROGA = "06";
    public static final String CAPITALE = "07";

    //==== METHODS ====
    public void setWkTpOpzioni(String wkTpOpzioni) {
        this.value = Functions.subString(wkTpOpzioni, Len.WK_TP_OPZIONI);
    }

    public String getWkTpOpzioni() {
        return this.value;
    }

    public boolean isDifferimento() {
        return value.equals(DIFFERIMENTO);
    }

    public boolean isProroga() {
        return value.equals(PROROGA);
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_TP_OPZIONI = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
