package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WPOL-SPE-MED<br>
 * Variable: WPOL-SPE-MED from program IDSS0160<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpolSpeMed extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpolSpeMed() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPOL_SPE_MED;
    }

    public void setWpolSpeMed(AfDecimal wpolSpeMed) {
        writeDecimalAsPacked(Pos.WPOL_SPE_MED, wpolSpeMed.copy());
    }

    public void setWpolSpeMedFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPOL_SPE_MED, Pos.WPOL_SPE_MED);
    }

    /**Original name: WPOL-SPE-MED<br>*/
    public AfDecimal getWpolSpeMed() {
        return readPackedAsDecimal(Pos.WPOL_SPE_MED, Len.Int.WPOL_SPE_MED, Len.Fract.WPOL_SPE_MED);
    }

    public byte[] getWpolSpeMedAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPOL_SPE_MED, Pos.WPOL_SPE_MED);
        return buffer;
    }

    public void setWpolSpeMedNull(String wpolSpeMedNull) {
        writeString(Pos.WPOL_SPE_MED_NULL, wpolSpeMedNull, Len.WPOL_SPE_MED_NULL);
    }

    /**Original name: WPOL-SPE-MED-NULL<br>*/
    public String getWpolSpeMedNull() {
        return readString(Pos.WPOL_SPE_MED_NULL, Len.WPOL_SPE_MED_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPOL_SPE_MED = 1;
        public static final int WPOL_SPE_MED_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOL_SPE_MED = 8;
        public static final int WPOL_SPE_MED_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WPOL_SPE_MED = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WPOL_SPE_MED = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
