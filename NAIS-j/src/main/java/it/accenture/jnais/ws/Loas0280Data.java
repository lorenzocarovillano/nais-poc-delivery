package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.Idsv00122;
import it.accenture.jnais.copy.Ldbv3361;
import it.accenture.jnais.ws.enums.WsTpOggLccs0024;
import org.apache.commons.lang3.ArrayUtils;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LOAS0280<br>
 * Generated as a class for rule WS.<br>*/
public class Loas0280Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------
	 *     COSTANTI
	 * ----------------------------------------------------------------</pre>*/
    private String wkPgm = "LOAS0280";
    //Original name: WK-LABEL-ERR
    private String wkLabelErr = DefaultValues.stringVal(Len.WK_LABEL_ERR);
    //Original name: WK-MESS
    private String wkMess = DefaultValues.stringVal(Len.WK_MESS);
    //Original name: WK-CTRL-TP-OGG
    private String wkCtrlTpOgg = DefaultValues.stringVal(Len.WK_CTRL_TP_OGG);
    private static final String[] WK_TP_OGG_VALID = new String[] {"PO", "AD"};
    //Original name: IX-ERR
    private short ixErr = DefaultValues.BIN_SHORT_VAL;
    //Original name: IX-TAB-ERR
    private short ixTabErr = DefaultValues.BIN_SHORT_VAL;
    /**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
    private WsTpOggLccs0024 wsTpOgg = new WsTpOggLccs0024();
    //Original name: IDSV0012
    private Idsv00122 idsv00122 = new Idsv00122();
    //Original name: DISPATCHER-VARIABLES
    private DispatcherVariables dispatcherVariables = new DispatcherVariables();
    //Original name: LDBV3361
    private Ldbv3361 ldbv3361 = new Ldbv3361();
    //Original name: IEAI9901-AREA
    private Ieai9901Area ieai9901Area = new Ieai9901Area();
    //Original name: IEAO9901-AREA
    private Ieao9901Area ieao9901Area = new Ieao9901Area();
    //Original name: IDSV0002
    private Idsv0002 idsv0002 = new Idsv0002();

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkLabelErr(String wkLabelErr) {
        this.wkLabelErr = Functions.subString(wkLabelErr, Len.WK_LABEL_ERR);
    }

    public String getWkLabelErr() {
        return this.wkLabelErr;
    }

    public void setWkMess(String wkMess) {
        this.wkMess = Functions.subString(wkMess, Len.WK_MESS);
    }

    public String getWkMess() {
        return this.wkMess;
    }

    public String getWkMessFormatted() {
        return Functions.padBlanks(getWkMess(), Len.WK_MESS);
    }

    public void setWkCtrlTpOgg(String wkCtrlTpOgg) {
        this.wkCtrlTpOgg = Functions.subString(wkCtrlTpOgg, Len.WK_CTRL_TP_OGG);
    }

    public String getWkCtrlTpOgg() {
        return this.wkCtrlTpOgg;
    }

    public boolean isWkTpOggValid() {
        return ArrayUtils.contains(WK_TP_OGG_VALID, wkCtrlTpOgg);
    }

    public void setIxErr(short ixErr) {
        this.ixErr = ixErr;
    }

    public short getIxErr() {
        return this.ixErr;
    }

    public void setIxTabErr(short ixTabErr) {
        this.ixTabErr = ixTabErr;
    }

    public short getIxTabErr() {
        return this.ixTabErr;
    }

    public DispatcherVariables getDispatcherVariables() {
        return dispatcherVariables;
    }

    public Idsv0002 getIdsv0002() {
        return idsv0002;
    }

    public Idsv00122 getIdsv00122() {
        return idsv00122;
    }

    public Ieai9901Area getIeai9901Area() {
        return ieai9901Area;
    }

    public Ieao9901Area getIeao9901Area() {
        return ieao9901Area;
    }

    public Ldbv3361 getLdbv3361() {
        return ldbv3361;
    }

    public WsTpOggLccs0024 getWsTpOgg() {
        return wsTpOgg;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LABEL_ERR = 30;
        public static final int WK_MESS = 15;
        public static final int WK_CTRL_TP_OGG = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
