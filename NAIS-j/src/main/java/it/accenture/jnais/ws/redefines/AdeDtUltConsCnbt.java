package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: ADE-DT-ULT-CONS-CNBT<br>
 * Variable: ADE-DT-ULT-CONS-CNBT from program IDBSADE0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class AdeDtUltConsCnbt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public AdeDtUltConsCnbt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.ADE_DT_ULT_CONS_CNBT;
    }

    public void setAdeDtUltConsCnbt(int adeDtUltConsCnbt) {
        writeIntAsPacked(Pos.ADE_DT_ULT_CONS_CNBT, adeDtUltConsCnbt, Len.Int.ADE_DT_ULT_CONS_CNBT);
    }

    public void setAdeDtUltConsCnbtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.ADE_DT_ULT_CONS_CNBT, Pos.ADE_DT_ULT_CONS_CNBT);
    }

    /**Original name: ADE-DT-ULT-CONS-CNBT<br>*/
    public int getAdeDtUltConsCnbt() {
        return readPackedAsInt(Pos.ADE_DT_ULT_CONS_CNBT, Len.Int.ADE_DT_ULT_CONS_CNBT);
    }

    public byte[] getAdeDtUltConsCnbtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.ADE_DT_ULT_CONS_CNBT, Pos.ADE_DT_ULT_CONS_CNBT);
        return buffer;
    }

    public void setAdeDtUltConsCnbtNull(String adeDtUltConsCnbtNull) {
        writeString(Pos.ADE_DT_ULT_CONS_CNBT_NULL, adeDtUltConsCnbtNull, Len.ADE_DT_ULT_CONS_CNBT_NULL);
    }

    /**Original name: ADE-DT-ULT-CONS-CNBT-NULL<br>*/
    public String getAdeDtUltConsCnbtNull() {
        return readString(Pos.ADE_DT_ULT_CONS_CNBT_NULL, Len.ADE_DT_ULT_CONS_CNBT_NULL);
    }

    public String getAdeDtUltConsCnbtNullFormatted() {
        return Functions.padBlanks(getAdeDtUltConsCnbtNull(), Len.ADE_DT_ULT_CONS_CNBT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int ADE_DT_ULT_CONS_CNBT = 1;
        public static final int ADE_DT_ULT_CONS_CNBT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int ADE_DT_ULT_CONS_CNBT = 5;
        public static final int ADE_DT_ULT_CONS_CNBT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int ADE_DT_ULT_CONS_CNBT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
