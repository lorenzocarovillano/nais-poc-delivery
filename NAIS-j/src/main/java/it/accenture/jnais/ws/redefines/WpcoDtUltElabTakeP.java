package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-ELAB-TAKE-P<br>
 * Variable: WPCO-DT-ULT-ELAB-TAKE-P from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltElabTakeP extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltElabTakeP() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_ELAB_TAKE_P;
    }

    public void setWpcoDtUltElabTakeP(int wpcoDtUltElabTakeP) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_ELAB_TAKE_P, wpcoDtUltElabTakeP, Len.Int.WPCO_DT_ULT_ELAB_TAKE_P);
    }

    public void setDpcoDtUltElabTakePFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_TAKE_P, Pos.WPCO_DT_ULT_ELAB_TAKE_P);
    }

    /**Original name: WPCO-DT-ULT-ELAB-TAKE-P<br>*/
    public int getWpcoDtUltElabTakeP() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_ELAB_TAKE_P, Len.Int.WPCO_DT_ULT_ELAB_TAKE_P);
    }

    public byte[] getWpcoDtUltElabTakePAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_ELAB_TAKE_P, Pos.WPCO_DT_ULT_ELAB_TAKE_P);
        return buffer;
    }

    public void setWpcoDtUltElabTakePNull(String wpcoDtUltElabTakePNull) {
        writeString(Pos.WPCO_DT_ULT_ELAB_TAKE_P_NULL, wpcoDtUltElabTakePNull, Len.WPCO_DT_ULT_ELAB_TAKE_P_NULL);
    }

    /**Original name: WPCO-DT-ULT-ELAB-TAKE-P-NULL<br>*/
    public String getWpcoDtUltElabTakePNull() {
        return readString(Pos.WPCO_DT_ULT_ELAB_TAKE_P_NULL, Len.WPCO_DT_ULT_ELAB_TAKE_P_NULL);
    }

    public String getWpcoDtUltElabTakePNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltElabTakePNull(), Len.WPCO_DT_ULT_ELAB_TAKE_P_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_TAKE_P = 1;
        public static final int WPCO_DT_ULT_ELAB_TAKE_P_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_ELAB_TAKE_P = 5;
        public static final int WPCO_DT_ULT_ELAB_TAKE_P_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_ELAB_TAKE_P = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
