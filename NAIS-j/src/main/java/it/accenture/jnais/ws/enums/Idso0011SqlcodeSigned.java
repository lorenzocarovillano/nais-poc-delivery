package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: IDSO0011-SQLCODE-SIGNED<br>
 * Variable: IDSO0011-SQLCODE-SIGNED from copybook IDSO0011<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Idso0011SqlcodeSigned {

    //==== PROPERTIES ====
    private int value = DefaultValues.INT_VAL;
    public static final int SUCCESSFUL_SQL = 0;
    public static final int NOT_FOUND = 100;
    public static final int DUPLICATE_KEY = -803;
    public static final int MORE_THAN_ONE_ROW = -811;
    private static final int[] DEADLOCK_TIMEOUT = new int[] {-911, -913};
    public static final int CONNECTION_ERROR = -924;

    //==== METHODS ====
    public void setSqlcodeSigned(int sqlcodeSigned) {
        this.value = sqlcodeSigned;
    }

    public int getSqlcodeSigned() {
        return this.value;
    }

    public String getIdso0021SqlcodeFormatted() {
        return NumericDisplaySigned.asString(getSqlcodeSigned(), Len.SQLCODE_SIGNED);
    }

    public String getIdso0021SqlcodeAsString() {
        return getIdso0021SqlcodeFormatted();
    }

    public boolean isSuccessfulSql() {
        return value == SUCCESSFUL_SQL;
    }

    public void setSuccessfulSql() {
        value = SUCCESSFUL_SQL;
    }

    public boolean isNotFound() {
        return value == NOT_FOUND;
    }

    public void setNotFound() {
        value = NOT_FOUND;
    }

    public boolean isMoreThanOneRow() {
        return value == MORE_THAN_ONE_ROW;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int SQLCODE_SIGNED = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
