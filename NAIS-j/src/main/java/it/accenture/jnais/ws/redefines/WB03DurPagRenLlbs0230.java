package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: W-B03-DUR-PAG-REN<br>
 * Variable: W-B03-DUR-PAG-REN from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03DurPagRenLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03DurPagRenLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_DUR_PAG_REN;
    }

    public void setwB03DurPagRen(int wB03DurPagRen) {
        writeIntAsPacked(Pos.W_B03_DUR_PAG_REN, wB03DurPagRen, Len.Int.W_B03_DUR_PAG_REN);
    }

    /**Original name: W-B03-DUR-PAG-REN<br>*/
    public int getwB03DurPagRen() {
        return readPackedAsInt(Pos.W_B03_DUR_PAG_REN, Len.Int.W_B03_DUR_PAG_REN);
    }

    public byte[] getwB03DurPagRenAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_DUR_PAG_REN, Pos.W_B03_DUR_PAG_REN);
        return buffer;
    }

    public void setwB03DurPagRenNull(String wB03DurPagRenNull) {
        writeString(Pos.W_B03_DUR_PAG_REN_NULL, wB03DurPagRenNull, Len.W_B03_DUR_PAG_REN_NULL);
    }

    /**Original name: W-B03-DUR-PAG-REN-NULL<br>*/
    public String getwB03DurPagRenNull() {
        return readString(Pos.W_B03_DUR_PAG_REN_NULL, Len.W_B03_DUR_PAG_REN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_PAG_REN = 1;
        public static final int W_B03_DUR_PAG_REN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_DUR_PAG_REN = 3;
        public static final int W_B03_DUR_PAG_REN_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_DUR_PAG_REN = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
