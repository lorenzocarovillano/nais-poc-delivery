package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTDR-TOT-SPE-AGE<br>
 * Variable: WTDR-TOT-SPE-AGE from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtdrTotSpeAge extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtdrTotSpeAge() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTDR_TOT_SPE_AGE;
    }

    public void setWtdrTotSpeAge(AfDecimal wtdrTotSpeAge) {
        writeDecimalAsPacked(Pos.WTDR_TOT_SPE_AGE, wtdrTotSpeAge.copy());
    }

    /**Original name: WTDR-TOT-SPE-AGE<br>*/
    public AfDecimal getWtdrTotSpeAge() {
        return readPackedAsDecimal(Pos.WTDR_TOT_SPE_AGE, Len.Int.WTDR_TOT_SPE_AGE, Len.Fract.WTDR_TOT_SPE_AGE);
    }

    public void setWtdrTotSpeAgeNull(String wtdrTotSpeAgeNull) {
        writeString(Pos.WTDR_TOT_SPE_AGE_NULL, wtdrTotSpeAgeNull, Len.WTDR_TOT_SPE_AGE_NULL);
    }

    /**Original name: WTDR-TOT-SPE-AGE-NULL<br>*/
    public String getWtdrTotSpeAgeNull() {
        return readString(Pos.WTDR_TOT_SPE_AGE_NULL, Len.WTDR_TOT_SPE_AGE_NULL);
    }

    public String getWtdrTotSpeAgeNullFormatted() {
        return Functions.padBlanks(getWtdrTotSpeAgeNull(), Len.WTDR_TOT_SPE_AGE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SPE_AGE = 1;
        public static final int WTDR_TOT_SPE_AGE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTDR_TOT_SPE_AGE = 8;
        public static final int WTDR_TOT_SPE_AGE_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SPE_AGE = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTDR_TOT_SPE_AGE = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
