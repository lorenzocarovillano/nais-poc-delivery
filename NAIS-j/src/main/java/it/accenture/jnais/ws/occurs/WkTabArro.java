package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WK-TAB-ARRO<br>
 * Variables: WK-TAB-ARRO from program LVVS0116<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WkTabArro {

    //==== PROPERTIES ====
    //Original name: WK-IMP-ARRO
    private char wkImpArro = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setWkTabArroBytes(byte[] buffer, int offset) {
        int position = offset;
        wkImpArro = MarshalByte.readChar(buffer, position);
    }

    public byte[] getWkTabArroBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, wkImpArro);
        return buffer;
    }

    public void initWkTabArroSpaces() {
        wkImpArro = Types.SPACE_CHAR;
    }

    public void setWkImpArro(char wkImpArro) {
        this.wkImpArro = wkImpArro;
    }

    public void setWkImpArroFormatted(String wkImpArro) {
        setWkImpArro(Functions.charAt(wkImpArro, Types.CHAR_SIZE));
    }

    public char getWkImpArro() {
        return this.wkImpArro;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_IMP_ARRO = 1;
        public static final int WK_TAB_ARRO = WK_IMP_ARRO;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
