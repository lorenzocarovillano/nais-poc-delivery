package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: D09-CSOCKET-PORT-NUM<br>
 * Variable: D09-CSOCKET-PORT-NUM from program LDBS6730<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class D09CsocketPortNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public D09CsocketPortNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.D09_CSOCKET_PORT_NUM;
    }

    public void setD09CsocketPortNum(int d09CsocketPortNum) {
        writeIntAsPacked(Pos.D09_CSOCKET_PORT_NUM, d09CsocketPortNum, Len.Int.D09_CSOCKET_PORT_NUM);
    }

    public void setD09CsocketPortNumFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.D09_CSOCKET_PORT_NUM, Pos.D09_CSOCKET_PORT_NUM);
    }

    /**Original name: D09-CSOCKET-PORT-NUM<br>*/
    public int getD09CsocketPortNum() {
        return readPackedAsInt(Pos.D09_CSOCKET_PORT_NUM, Len.Int.D09_CSOCKET_PORT_NUM);
    }

    public byte[] getD09CsocketPortNumAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.D09_CSOCKET_PORT_NUM, Pos.D09_CSOCKET_PORT_NUM);
        return buffer;
    }

    public void setD09CsocketPortNumNull(String d09CsocketPortNumNull) {
        writeString(Pos.D09_CSOCKET_PORT_NUM_NULL, d09CsocketPortNumNull, Len.D09_CSOCKET_PORT_NUM_NULL);
    }

    /**Original name: D09-CSOCKET-PORT-NUM-NULL<br>*/
    public String getD09CsocketPortNumNull() {
        return readString(Pos.D09_CSOCKET_PORT_NUM_NULL, Len.D09_CSOCKET_PORT_NUM_NULL);
    }

    public String getD09CsocketPortNumNullFormatted() {
        return Functions.padBlanks(getD09CsocketPortNumNull(), Len.D09_CSOCKET_PORT_NUM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int D09_CSOCKET_PORT_NUM = 1;
        public static final int D09_CSOCKET_PORT_NUM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int D09_CSOCKET_PORT_NUM = 3;
        public static final int D09_CSOCKET_PORT_NUM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int D09_CSOCKET_PORT_NUM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
