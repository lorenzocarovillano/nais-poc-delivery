package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-PC-COMMIS-GEST<br>
 * Variable: TGA-PC-COMMIS-GEST from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaPcCommisGest extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaPcCommisGest() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_PC_COMMIS_GEST;
    }

    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        writeDecimalAsPacked(Pos.TGA_PC_COMMIS_GEST, tgaPcCommisGest.copy());
    }

    public void setTgaPcCommisGestFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_PC_COMMIS_GEST, Pos.TGA_PC_COMMIS_GEST);
    }

    /**Original name: TGA-PC-COMMIS-GEST<br>*/
    public AfDecimal getTgaPcCommisGest() {
        return readPackedAsDecimal(Pos.TGA_PC_COMMIS_GEST, Len.Int.TGA_PC_COMMIS_GEST, Len.Fract.TGA_PC_COMMIS_GEST);
    }

    public byte[] getTgaPcCommisGestAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_PC_COMMIS_GEST, Pos.TGA_PC_COMMIS_GEST);
        return buffer;
    }

    public void setTgaPcCommisGestNull(String tgaPcCommisGestNull) {
        writeString(Pos.TGA_PC_COMMIS_GEST_NULL, tgaPcCommisGestNull, Len.TGA_PC_COMMIS_GEST_NULL);
    }

    /**Original name: TGA-PC-COMMIS-GEST-NULL<br>*/
    public String getTgaPcCommisGestNull() {
        return readString(Pos.TGA_PC_COMMIS_GEST_NULL, Len.TGA_PC_COMMIS_GEST_NULL);
    }

    public String getTgaPcCommisGestNullFormatted() {
        return Functions.padBlanks(getTgaPcCommisGestNull(), Len.TGA_PC_COMMIS_GEST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_PC_COMMIS_GEST = 1;
        public static final int TGA_PC_COMMIS_GEST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_PC_COMMIS_GEST = 4;
        public static final int TGA_PC_COMMIS_GEST_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TGA_PC_COMMIS_GEST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
