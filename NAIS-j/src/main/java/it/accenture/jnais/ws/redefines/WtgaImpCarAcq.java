package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-IMP-CAR-ACQ<br>
 * Variable: WTGA-IMP-CAR-ACQ from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaImpCarAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaImpCarAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_IMP_CAR_ACQ;
    }

    public void setWtgaImpCarAcq(AfDecimal wtgaImpCarAcq) {
        writeDecimalAsPacked(Pos.WTGA_IMP_CAR_ACQ, wtgaImpCarAcq.copy());
    }

    public void setWtgaImpCarAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_IMP_CAR_ACQ, Pos.WTGA_IMP_CAR_ACQ);
    }

    /**Original name: WTGA-IMP-CAR-ACQ<br>*/
    public AfDecimal getWtgaImpCarAcq() {
        return readPackedAsDecimal(Pos.WTGA_IMP_CAR_ACQ, Len.Int.WTGA_IMP_CAR_ACQ, Len.Fract.WTGA_IMP_CAR_ACQ);
    }

    public byte[] getWtgaImpCarAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_IMP_CAR_ACQ, Pos.WTGA_IMP_CAR_ACQ);
        return buffer;
    }

    public void initWtgaImpCarAcqSpaces() {
        fill(Pos.WTGA_IMP_CAR_ACQ, Len.WTGA_IMP_CAR_ACQ, Types.SPACE_CHAR);
    }

    public void setWtgaImpCarAcqNull(String wtgaImpCarAcqNull) {
        writeString(Pos.WTGA_IMP_CAR_ACQ_NULL, wtgaImpCarAcqNull, Len.WTGA_IMP_CAR_ACQ_NULL);
    }

    /**Original name: WTGA-IMP-CAR-ACQ-NULL<br>*/
    public String getWtgaImpCarAcqNull() {
        return readString(Pos.WTGA_IMP_CAR_ACQ_NULL, Len.WTGA_IMP_CAR_ACQ_NULL);
    }

    public String getWtgaImpCarAcqNullFormatted() {
        return Functions.padBlanks(getWtgaImpCarAcqNull(), Len.WTGA_IMP_CAR_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_CAR_ACQ = 1;
        public static final int WTGA_IMP_CAR_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_IMP_CAR_ACQ = 8;
        public static final int WTGA_IMP_CAR_ACQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_CAR_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_IMP_CAR_ACQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
