package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: IX-INDICI<br>
 * Variable: IX-INDICI from program LCCS0490<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class IxIndiciLccs0490 {

    //==== PROPERTIES ====
    //Original name: IX-TAB-GRZ
    private short grz = DefaultValues.SHORT_VAL;
    //Original name: IX-TAB-RATE
    private short rate = DefaultValues.SHORT_VAL;
    //Original name: IX-TAB-TIT
    private short tit = DefaultValues.SHORT_VAL;
    //Original name: IX-TAB-DTC
    private short dtc = DefaultValues.SHORT_VAL;

    //==== METHODS ====
    public void setGrz(short grz) {
        this.grz = grz;
    }

    public short getGrz() {
        return this.grz;
    }

    public void setRate(short rate) {
        this.rate = rate;
    }

    public short getRate() {
        return this.rate;
    }

    public void setTit(short tit) {
        this.tit = tit;
    }

    public short getTit() {
        return this.tit;
    }

    public void setDtc(short dtc) {
        this.dtc = dtc;
    }

    public short getDtc() {
        return this.dtc;
    }
}
