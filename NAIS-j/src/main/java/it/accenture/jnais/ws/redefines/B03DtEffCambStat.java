package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DT-EFF-CAMB-STAT<br>
 * Variable: B03-DT-EFF-CAMB-STAT from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DtEffCambStat extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DtEffCambStat() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DT_EFF_CAMB_STAT;
    }

    public void setB03DtEffCambStat(int b03DtEffCambStat) {
        writeIntAsPacked(Pos.B03_DT_EFF_CAMB_STAT, b03DtEffCambStat, Len.Int.B03_DT_EFF_CAMB_STAT);
    }

    public void setB03DtEffCambStatFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DT_EFF_CAMB_STAT, Pos.B03_DT_EFF_CAMB_STAT);
    }

    /**Original name: B03-DT-EFF-CAMB-STAT<br>*/
    public int getB03DtEffCambStat() {
        return readPackedAsInt(Pos.B03_DT_EFF_CAMB_STAT, Len.Int.B03_DT_EFF_CAMB_STAT);
    }

    public byte[] getB03DtEffCambStatAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DT_EFF_CAMB_STAT, Pos.B03_DT_EFF_CAMB_STAT);
        return buffer;
    }

    public void setB03DtEffCambStatNull(String b03DtEffCambStatNull) {
        writeString(Pos.B03_DT_EFF_CAMB_STAT_NULL, b03DtEffCambStatNull, Len.B03_DT_EFF_CAMB_STAT_NULL);
    }

    /**Original name: B03-DT-EFF-CAMB-STAT-NULL<br>*/
    public String getB03DtEffCambStatNull() {
        return readString(Pos.B03_DT_EFF_CAMB_STAT_NULL, Len.B03_DT_EFF_CAMB_STAT_NULL);
    }

    public String getB03DtEffCambStatNullFormatted() {
        return Functions.padBlanks(getB03DtEffCambStatNull(), Len.B03_DT_EFF_CAMB_STAT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DT_EFF_CAMB_STAT = 1;
        public static final int B03_DT_EFF_CAMB_STAT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DT_EFF_CAMB_STAT = 5;
        public static final int B03_DT_EFF_CAMB_STAT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DT_EFF_CAMB_STAT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
