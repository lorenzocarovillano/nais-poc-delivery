package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-DT-INI-VAL-TAR<br>
 * Variable: WTGA-DT-INI-VAL-TAR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaDtIniValTar extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaDtIniValTar() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_DT_INI_VAL_TAR;
    }

    public void setWtgaDtIniValTar(int wtgaDtIniValTar) {
        writeIntAsPacked(Pos.WTGA_DT_INI_VAL_TAR, wtgaDtIniValTar, Len.Int.WTGA_DT_INI_VAL_TAR);
    }

    public void setWtgaDtIniValTarFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_DT_INI_VAL_TAR, Pos.WTGA_DT_INI_VAL_TAR);
    }

    /**Original name: WTGA-DT-INI-VAL-TAR<br>*/
    public int getWtgaDtIniValTar() {
        return readPackedAsInt(Pos.WTGA_DT_INI_VAL_TAR, Len.Int.WTGA_DT_INI_VAL_TAR);
    }

    public String getWtopDtIniValTarFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWtgaDtIniValTar()).toString();
    }

    public byte[] getWtgaDtIniValTarAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_DT_INI_VAL_TAR, Pos.WTGA_DT_INI_VAL_TAR);
        return buffer;
    }

    public void initWtgaDtIniValTarSpaces() {
        fill(Pos.WTGA_DT_INI_VAL_TAR, Len.WTGA_DT_INI_VAL_TAR, Types.SPACE_CHAR);
    }

    public void setWtgaDtIniValTarNull(String wtgaDtIniValTarNull) {
        writeString(Pos.WTGA_DT_INI_VAL_TAR_NULL, wtgaDtIniValTarNull, Len.WTGA_DT_INI_VAL_TAR_NULL);
    }

    /**Original name: WTGA-DT-INI-VAL-TAR-NULL<br>*/
    public String getWtgaDtIniValTarNull() {
        return readString(Pos.WTGA_DT_INI_VAL_TAR_NULL, Len.WTGA_DT_INI_VAL_TAR_NULL);
    }

    public String getWtgaDtIniValTarNullFormatted() {
        return Functions.padBlanks(getWtgaDtIniValTarNull(), Len.WTGA_DT_INI_VAL_TAR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_DT_INI_VAL_TAR = 1;
        public static final int WTGA_DT_INI_VAL_TAR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_DT_INI_VAL_TAR = 5;
        public static final int WTGA_DT_INI_VAL_TAR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_DT_INI_VAL_TAR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
