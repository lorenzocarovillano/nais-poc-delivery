package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFA-RIDZ-TFR<br>
 * Variable: DFA-RIDZ-TFR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DfaRidzTfr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DfaRidzTfr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFA_RIDZ_TFR;
    }

    public void setDfaRidzTfr(AfDecimal dfaRidzTfr) {
        writeDecimalAsPacked(Pos.DFA_RIDZ_TFR, dfaRidzTfr.copy());
    }

    public void setDfaRidzTfrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFA_RIDZ_TFR, Pos.DFA_RIDZ_TFR);
    }

    /**Original name: DFA-RIDZ-TFR<br>*/
    public AfDecimal getDfaRidzTfr() {
        return readPackedAsDecimal(Pos.DFA_RIDZ_TFR, Len.Int.DFA_RIDZ_TFR, Len.Fract.DFA_RIDZ_TFR);
    }

    public byte[] getDfaRidzTfrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFA_RIDZ_TFR, Pos.DFA_RIDZ_TFR);
        return buffer;
    }

    public void setDfaRidzTfrNull(String dfaRidzTfrNull) {
        writeString(Pos.DFA_RIDZ_TFR_NULL, dfaRidzTfrNull, Len.DFA_RIDZ_TFR_NULL);
    }

    /**Original name: DFA-RIDZ-TFR-NULL<br>*/
    public String getDfaRidzTfrNull() {
        return readString(Pos.DFA_RIDZ_TFR_NULL, Len.DFA_RIDZ_TFR_NULL);
    }

    public String getDfaRidzTfrNullFormatted() {
        return Functions.padBlanks(getDfaRidzTfrNull(), Len.DFA_RIDZ_TFR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFA_RIDZ_TFR = 1;
        public static final int DFA_RIDZ_TFR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFA_RIDZ_TFR = 8;
        public static final int DFA_RIDZ_TFR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFA_RIDZ_TFR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFA_RIDZ_TFR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
