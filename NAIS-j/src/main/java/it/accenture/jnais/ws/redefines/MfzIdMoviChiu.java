package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: MFZ-ID-MOVI-CHIU<br>
 * Variable: MFZ-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class MfzIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public MfzIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MFZ_ID_MOVI_CHIU;
    }

    public void setMfzIdMoviChiu(int mfzIdMoviChiu) {
        writeIntAsPacked(Pos.MFZ_ID_MOVI_CHIU, mfzIdMoviChiu, Len.Int.MFZ_ID_MOVI_CHIU);
    }

    public void setMfzIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.MFZ_ID_MOVI_CHIU, Pos.MFZ_ID_MOVI_CHIU);
    }

    /**Original name: MFZ-ID-MOVI-CHIU<br>*/
    public int getMfzIdMoviChiu() {
        return readPackedAsInt(Pos.MFZ_ID_MOVI_CHIU, Len.Int.MFZ_ID_MOVI_CHIU);
    }

    public byte[] getMfzIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.MFZ_ID_MOVI_CHIU, Pos.MFZ_ID_MOVI_CHIU);
        return buffer;
    }

    public void setMfzIdMoviChiuNull(String mfzIdMoviChiuNull) {
        writeString(Pos.MFZ_ID_MOVI_CHIU_NULL, mfzIdMoviChiuNull, Len.MFZ_ID_MOVI_CHIU_NULL);
    }

    /**Original name: MFZ-ID-MOVI-CHIU-NULL<br>*/
    public String getMfzIdMoviChiuNull() {
        return readString(Pos.MFZ_ID_MOVI_CHIU_NULL, Len.MFZ_ID_MOVI_CHIU_NULL);
    }

    public String getMfzIdMoviChiuNullFormatted() {
        return Functions.padBlanks(getMfzIdMoviChiuNull(), Len.MFZ_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int MFZ_ID_MOVI_CHIU = 1;
        public static final int MFZ_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int MFZ_ID_MOVI_CHIU = 5;
        public static final int MFZ_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MFZ_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
