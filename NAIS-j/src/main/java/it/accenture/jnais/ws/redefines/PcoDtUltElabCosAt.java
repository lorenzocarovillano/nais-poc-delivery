package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-ELAB-COS-AT<br>
 * Variable: PCO-DT-ULT-ELAB-COS-AT from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltElabCosAt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltElabCosAt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_ELAB_COS_AT;
    }

    public void setPcoDtUltElabCosAt(int pcoDtUltElabCosAt) {
        writeIntAsPacked(Pos.PCO_DT_ULT_ELAB_COS_AT, pcoDtUltElabCosAt, Len.Int.PCO_DT_ULT_ELAB_COS_AT);
    }

    public void setPcoDtUltElabCosAtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_COS_AT, Pos.PCO_DT_ULT_ELAB_COS_AT);
    }

    /**Original name: PCO-DT-ULT-ELAB-COS-AT<br>*/
    public int getPcoDtUltElabCosAt() {
        return readPackedAsInt(Pos.PCO_DT_ULT_ELAB_COS_AT, Len.Int.PCO_DT_ULT_ELAB_COS_AT);
    }

    public byte[] getPcoDtUltElabCosAtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_ELAB_COS_AT, Pos.PCO_DT_ULT_ELAB_COS_AT);
        return buffer;
    }

    public void initPcoDtUltElabCosAtHighValues() {
        fill(Pos.PCO_DT_ULT_ELAB_COS_AT, Len.PCO_DT_ULT_ELAB_COS_AT, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltElabCosAtNull(String pcoDtUltElabCosAtNull) {
        writeString(Pos.PCO_DT_ULT_ELAB_COS_AT_NULL, pcoDtUltElabCosAtNull, Len.PCO_DT_ULT_ELAB_COS_AT_NULL);
    }

    /**Original name: PCO-DT-ULT-ELAB-COS-AT-NULL<br>*/
    public String getPcoDtUltElabCosAtNull() {
        return readString(Pos.PCO_DT_ULT_ELAB_COS_AT_NULL, Len.PCO_DT_ULT_ELAB_COS_AT_NULL);
    }

    public String getPcoDtUltElabCosAtNullFormatted() {
        return Functions.padBlanks(getPcoDtUltElabCosAtNull(), Len.PCO_DT_ULT_ELAB_COS_AT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_COS_AT = 1;
        public static final int PCO_DT_ULT_ELAB_COS_AT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_ELAB_COS_AT = 5;
        public static final int PCO_DT_ULT_ELAB_COS_AT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_ELAB_COS_AT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
