package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TCL-IMP-DIR-LIQ<br>
 * Variable: TCL-IMP-DIR-LIQ from program IDBSTCL0<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TclImpDirLiq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TclImpDirLiq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TCL_IMP_DIR_LIQ;
    }

    public void setTclImpDirLiq(AfDecimal tclImpDirLiq) {
        writeDecimalAsPacked(Pos.TCL_IMP_DIR_LIQ, tclImpDirLiq.copy());
    }

    public void setTclImpDirLiqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TCL_IMP_DIR_LIQ, Pos.TCL_IMP_DIR_LIQ);
    }

    /**Original name: TCL-IMP-DIR-LIQ<br>*/
    public AfDecimal getTclImpDirLiq() {
        return readPackedAsDecimal(Pos.TCL_IMP_DIR_LIQ, Len.Int.TCL_IMP_DIR_LIQ, Len.Fract.TCL_IMP_DIR_LIQ);
    }

    public byte[] getTclImpDirLiqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TCL_IMP_DIR_LIQ, Pos.TCL_IMP_DIR_LIQ);
        return buffer;
    }

    public void setTclImpDirLiqNull(String tclImpDirLiqNull) {
        writeString(Pos.TCL_IMP_DIR_LIQ_NULL, tclImpDirLiqNull, Len.TCL_IMP_DIR_LIQ_NULL);
    }

    /**Original name: TCL-IMP-DIR-LIQ-NULL<br>*/
    public String getTclImpDirLiqNull() {
        return readString(Pos.TCL_IMP_DIR_LIQ_NULL, Len.TCL_IMP_DIR_LIQ_NULL);
    }

    public String getTclImpDirLiqNullFormatted() {
        return Functions.padBlanks(getTclImpDirLiqNull(), Len.TCL_IMP_DIR_LIQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TCL_IMP_DIR_LIQ = 1;
        public static final int TCL_IMP_DIR_LIQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TCL_IMP_DIR_LIQ = 8;
        public static final int TCL_IMP_DIR_LIQ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TCL_IMP_DIR_LIQ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TCL_IMP_DIR_LIQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
