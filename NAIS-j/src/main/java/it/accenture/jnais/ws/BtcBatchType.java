package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.BbtCommitFrequency;
import it.accenture.jnais.ws.redefines.BbtTpMovi;

/**Original name: BTC-BATCH-TYPE<br>
 * Variable: BTC-BATCH-TYPE from copybook IDBVBBT1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class BtcBatchType extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: BBT-COD-BATCH-TYPE
    private int bbtCodBatchType = DefaultValues.BIN_INT_VAL;
    //Original name: BBT-DES-LEN
    private short bbtDesLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: BBT-DES
    private String bbtDes = DefaultValues.stringVal(Len.BBT_DES);
    //Original name: BBT-DEF-CONTENT-TYPE
    private String bbtDefContentType = DefaultValues.stringVal(Len.BBT_DEF_CONTENT_TYPE);
    //Original name: BBT-COMMIT-FREQUENCY
    private BbtCommitFrequency bbtCommitFrequency = new BbtCommitFrequency();
    //Original name: BBT-SERVICE-NAME
    private String bbtServiceName = DefaultValues.stringVal(Len.BBT_SERVICE_NAME);
    //Original name: BBT-SERVICE-GUIDE-NAME
    private String bbtServiceGuideName = DefaultValues.stringVal(Len.BBT_SERVICE_GUIDE_NAME);
    //Original name: BBT-SERVICE-ALPO-NAME
    private String bbtServiceAlpoName = DefaultValues.stringVal(Len.BBT_SERVICE_ALPO_NAME);
    //Original name: BBT-COD-MACROFUNCT
    private String bbtCodMacrofunct = DefaultValues.stringVal(Len.BBT_COD_MACROFUNCT);
    //Original name: BBT-TP-MOVI
    private BbtTpMovi bbtTpMovi = new BbtTpMovi();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.BTC_BATCH_TYPE;
    }

    @Override
    public void deserialize(byte[] buf) {
        setBtcBatchTypeBytes(buf);
    }

    public String getBtcBatchTypeFormatted() {
        return MarshalByteExt.bufferToStr(getBtcBatchTypeBytes());
    }

    public void setBtcBatchTypeBytes(byte[] buffer) {
        setBtcBatchTypeBytes(buffer, 1);
    }

    public byte[] getBtcBatchTypeBytes() {
        byte[] buffer = new byte[Len.BTC_BATCH_TYPE];
        return getBtcBatchTypeBytes(buffer, 1);
    }

    public void setBtcBatchTypeBytes(byte[] buffer, int offset) {
        int position = offset;
        bbtCodBatchType = MarshalByte.readBinaryInt(buffer, position);
        position += Types.INT_SIZE;
        setBbtDesVcharBytes(buffer, position);
        position += Len.BBT_DES_VCHAR;
        bbtDefContentType = MarshalByte.readString(buffer, position, Len.BBT_DEF_CONTENT_TYPE);
        position += Len.BBT_DEF_CONTENT_TYPE;
        bbtCommitFrequency.setBbtCommitFrequencyFromBuffer(buffer, position);
        position += Types.INT_SIZE;
        bbtServiceName = MarshalByte.readString(buffer, position, Len.BBT_SERVICE_NAME);
        position += Len.BBT_SERVICE_NAME;
        bbtServiceGuideName = MarshalByte.readString(buffer, position, Len.BBT_SERVICE_GUIDE_NAME);
        position += Len.BBT_SERVICE_GUIDE_NAME;
        bbtServiceAlpoName = MarshalByte.readString(buffer, position, Len.BBT_SERVICE_ALPO_NAME);
        position += Len.BBT_SERVICE_ALPO_NAME;
        bbtCodMacrofunct = MarshalByte.readString(buffer, position, Len.BBT_COD_MACROFUNCT);
        position += Len.BBT_COD_MACROFUNCT;
        bbtTpMovi.setBbtTpMoviFromBuffer(buffer, position);
    }

    public byte[] getBtcBatchTypeBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryInt(buffer, position, bbtCodBatchType);
        position += Types.INT_SIZE;
        getBbtDesVcharBytes(buffer, position);
        position += Len.BBT_DES_VCHAR;
        MarshalByte.writeString(buffer, position, bbtDefContentType, Len.BBT_DEF_CONTENT_TYPE);
        position += Len.BBT_DEF_CONTENT_TYPE;
        bbtCommitFrequency.getBbtCommitFrequencyAsBuffer(buffer, position);
        position += Types.INT_SIZE;
        MarshalByte.writeString(buffer, position, bbtServiceName, Len.BBT_SERVICE_NAME);
        position += Len.BBT_SERVICE_NAME;
        MarshalByte.writeString(buffer, position, bbtServiceGuideName, Len.BBT_SERVICE_GUIDE_NAME);
        position += Len.BBT_SERVICE_GUIDE_NAME;
        MarshalByte.writeString(buffer, position, bbtServiceAlpoName, Len.BBT_SERVICE_ALPO_NAME);
        position += Len.BBT_SERVICE_ALPO_NAME;
        MarshalByte.writeString(buffer, position, bbtCodMacrofunct, Len.BBT_COD_MACROFUNCT);
        position += Len.BBT_COD_MACROFUNCT;
        bbtTpMovi.getBbtTpMoviAsBuffer(buffer, position);
        return buffer;
    }

    public void setBbtCodBatchType(int bbtCodBatchType) {
        this.bbtCodBatchType = bbtCodBatchType;
    }

    public int getBbtCodBatchType() {
        return this.bbtCodBatchType;
    }

    public void setBbtDesVcharFormatted(String data) {
        byte[] buffer = new byte[Len.BBT_DES_VCHAR];
        MarshalByte.writeString(buffer, 1, data, Len.BBT_DES_VCHAR);
        setBbtDesVcharBytes(buffer, 1);
    }

    public String getBbtDesVcharFormatted() {
        return MarshalByteExt.bufferToStr(getBbtDesVcharBytes());
    }

    /**Original name: BBT-DES-VCHAR<br>*/
    public byte[] getBbtDesVcharBytes() {
        byte[] buffer = new byte[Len.BBT_DES_VCHAR];
        return getBbtDesVcharBytes(buffer, 1);
    }

    public void setBbtDesVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        bbtDesLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        bbtDes = MarshalByte.readString(buffer, position, Len.BBT_DES);
    }

    public byte[] getBbtDesVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, bbtDesLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, bbtDes, Len.BBT_DES);
        return buffer;
    }

    public void setBbtDesLen(short bbtDesLen) {
        this.bbtDesLen = bbtDesLen;
    }

    public short getBbtDesLen() {
        return this.bbtDesLen;
    }

    public void setBbtDes(String bbtDes) {
        this.bbtDes = Functions.subString(bbtDes, Len.BBT_DES);
    }

    public String getBbtDes() {
        return this.bbtDes;
    }

    public void setBbtDefContentType(String bbtDefContentType) {
        this.bbtDefContentType = Functions.subString(bbtDefContentType, Len.BBT_DEF_CONTENT_TYPE);
    }

    public String getBbtDefContentType() {
        return this.bbtDefContentType;
    }

    public String getBbtDefContentTypeFormatted() {
        return Functions.padBlanks(getBbtDefContentType(), Len.BBT_DEF_CONTENT_TYPE);
    }

    public void setBbtServiceName(String bbtServiceName) {
        this.bbtServiceName = Functions.subString(bbtServiceName, Len.BBT_SERVICE_NAME);
    }

    public String getBbtServiceName() {
        return this.bbtServiceName;
    }

    public void setBbtServiceGuideName(String bbtServiceGuideName) {
        this.bbtServiceGuideName = Functions.subString(bbtServiceGuideName, Len.BBT_SERVICE_GUIDE_NAME);
    }

    public String getBbtServiceGuideName() {
        return this.bbtServiceGuideName;
    }

    public void setBbtServiceAlpoName(String bbtServiceAlpoName) {
        this.bbtServiceAlpoName = Functions.subString(bbtServiceAlpoName, Len.BBT_SERVICE_ALPO_NAME);
    }

    public String getBbtServiceAlpoName() {
        return this.bbtServiceAlpoName;
    }

    public void setBbtCodMacrofunct(String bbtCodMacrofunct) {
        this.bbtCodMacrofunct = Functions.subString(bbtCodMacrofunct, Len.BBT_COD_MACROFUNCT);
    }

    public String getBbtCodMacrofunct() {
        return this.bbtCodMacrofunct;
    }

    public String getBbtCodMacrofunctFormatted() {
        return Functions.padBlanks(getBbtCodMacrofunct(), Len.BBT_COD_MACROFUNCT);
    }

    public BbtCommitFrequency getBbtCommitFrequency() {
        return bbtCommitFrequency;
    }

    public BbtTpMovi getBbtTpMovi() {
        return bbtTpMovi;
    }

    @Override
    public byte[] serialize() {
        return getBtcBatchTypeBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int BBT_COD_BATCH_TYPE = 4;
        public static final int BBT_DES_LEN = 2;
        public static final int BBT_DES = 100;
        public static final int BBT_DES_VCHAR = BBT_DES_LEN + BBT_DES;
        public static final int BBT_DEF_CONTENT_TYPE = 30;
        public static final int BBT_SERVICE_NAME = 50;
        public static final int BBT_SERVICE_GUIDE_NAME = 50;
        public static final int BBT_SERVICE_ALPO_NAME = 50;
        public static final int BBT_COD_MACROFUNCT = 2;
        public static final int BTC_BATCH_TYPE = BBT_COD_BATCH_TYPE + BBT_DES_VCHAR + BBT_DEF_CONTENT_TYPE + BbtCommitFrequency.Len.BBT_COMMIT_FREQUENCY + BBT_SERVICE_NAME + BBT_SERVICE_GUIDE_NAME + BBT_SERVICE_ALPO_NAME + BBT_COD_MACROFUNCT + BbtTpMovi.Len.BBT_TP_MOVI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
