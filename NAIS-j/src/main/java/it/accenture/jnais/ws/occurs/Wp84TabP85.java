package it.accenture.jnais.ws.occurs;

import it.accenture.jnais.copy.Lccvp851;

/**Original name: WP84-TAB-P85<br>
 * Variables: WP84-TAB-P85 from program LRGS0660<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wp84TabP85 {

    //==== PROPERTIES ====
    //Original name: LCCVP851
    private Lccvp851 lccvp851 = new Lccvp851();

    //==== METHODS ====
    public Lccvp851 getLccvp851() {
        return lccvp851;
    }
}
