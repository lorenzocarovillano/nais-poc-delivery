package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvl191;
import it.accenture.jnais.copy.Wl19Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WL19-TAB-FND<br>
 * Variables: WL19-TAB-FND from copybook LCCVL197<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wl19TabFnd {

    //==== PROPERTIES ====
    //Original name: LCCVL191
    private Lccvl191 lccvl191 = new Lccvl191();

    //==== METHODS ====
    public void setWl19TabFndBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvl191.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvl191.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWl19TabFndBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvl191.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        lccvl191.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWl19TabFndSpaces() {
        lccvl191.initLccvl191Spaces();
    }

    public Lccvl191 getLccvl191() {
        return lccvl191;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL19_TAB_FND = WpolStatus.Len.STATUS + Wl19Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
