package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULT-BOLL-RSP-IN<br>
 * Variable: WPCO-DT-ULT-BOLL-RSP-IN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltBollRspIn extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltBollRspIn() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULT_BOLL_RSP_IN;
    }

    public void setWpcoDtUltBollRspIn(int wpcoDtUltBollRspIn) {
        writeIntAsPacked(Pos.WPCO_DT_ULT_BOLL_RSP_IN, wpcoDtUltBollRspIn, Len.Int.WPCO_DT_ULT_BOLL_RSP_IN);
    }

    public void setDpcoDtUltBollRspInFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RSP_IN, Pos.WPCO_DT_ULT_BOLL_RSP_IN);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RSP-IN<br>*/
    public int getWpcoDtUltBollRspIn() {
        return readPackedAsInt(Pos.WPCO_DT_ULT_BOLL_RSP_IN, Len.Int.WPCO_DT_ULT_BOLL_RSP_IN);
    }

    public byte[] getWpcoDtUltBollRspInAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULT_BOLL_RSP_IN, Pos.WPCO_DT_ULT_BOLL_RSP_IN);
        return buffer;
    }

    public void setWpcoDtUltBollRspInNull(String wpcoDtUltBollRspInNull) {
        writeString(Pos.WPCO_DT_ULT_BOLL_RSP_IN_NULL, wpcoDtUltBollRspInNull, Len.WPCO_DT_ULT_BOLL_RSP_IN_NULL);
    }

    /**Original name: WPCO-DT-ULT-BOLL-RSP-IN-NULL<br>*/
    public String getWpcoDtUltBollRspInNull() {
        return readString(Pos.WPCO_DT_ULT_BOLL_RSP_IN_NULL, Len.WPCO_DT_ULT_BOLL_RSP_IN_NULL);
    }

    public String getWpcoDtUltBollRspInNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltBollRspInNull(), Len.WPCO_DT_ULT_BOLL_RSP_IN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RSP_IN = 1;
        public static final int WPCO_DT_ULT_BOLL_RSP_IN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULT_BOLL_RSP_IN = 5;
        public static final int WPCO_DT_ULT_BOLL_RSP_IN_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULT_BOLL_RSP_IN = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
