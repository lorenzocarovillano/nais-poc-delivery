package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvl231;
import it.accenture.jnais.copy.Wl23Dati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WL23-TAB-VINC-PEG<br>
 * Variables: WL23-TAB-VINC-PEG from program IVVS0216<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Wl23TabVincPeg {

    //==== PROPERTIES ====
    //Original name: LCCVL231
    private Lccvl231 lccvl231 = new Lccvl231();

    //==== METHODS ====
    public void setWl23TabVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvl231.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvl231.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvl231.Len.Int.ID_PTF, 0));
        position += Lccvl231.Len.ID_PTF;
        lccvl231.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWl23TabVincPegBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvl231.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvl231.getIdPtf(), Lccvl231.Len.Int.ID_PTF, 0);
        position += Lccvl231.Len.ID_PTF;
        lccvl231.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWl23TabVincPegSpaces() {
        lccvl231.initLccvl231Spaces();
    }

    public Lccvl231 getLccvl231() {
        return lccvl231;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_TAB_VINC_PEG = WpolStatus.Len.STATUS + Lccvl231.Len.ID_PTF + Wl23Dati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
