package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-CONT<br>
 * Variable: PCO-DT-CONT from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtCont extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtCont() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_CONT;
    }

    public void setPcoDtCont(int pcoDtCont) {
        writeIntAsPacked(Pos.PCO_DT_CONT, pcoDtCont, Len.Int.PCO_DT_CONT);
    }

    public void setPcoDtContFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_CONT, Pos.PCO_DT_CONT);
    }

    /**Original name: PCO-DT-CONT<br>*/
    public int getPcoDtCont() {
        return readPackedAsInt(Pos.PCO_DT_CONT, Len.Int.PCO_DT_CONT);
    }

    public byte[] getPcoDtContAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_CONT, Pos.PCO_DT_CONT);
        return buffer;
    }

    public void initPcoDtContHighValues() {
        fill(Pos.PCO_DT_CONT, Len.PCO_DT_CONT, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtContNull(String pcoDtContNull) {
        writeString(Pos.PCO_DT_CONT_NULL, pcoDtContNull, Len.PCO_DT_CONT_NULL);
    }

    /**Original name: PCO-DT-CONT-NULL<br>*/
    public String getPcoDtContNull() {
        return readString(Pos.PCO_DT_CONT_NULL, Len.PCO_DT_CONT_NULL);
    }

    public String getPcoDtContNullFormatted() {
        return Functions.padBlanks(getPcoDtContNull(), Len.PCO_DT_CONT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_CONT = 1;
        public static final int PCO_DT_CONT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_CONT = 5;
        public static final int PCO_DT_CONT_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_CONT = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
