package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-PC-PROV-1AA-ACQ<br>
 * Variable: PCO-PC-PROV-1AA-ACQ from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoPcProv1aaAcq extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoPcProv1aaAcq() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_PC_PROV1AA_ACQ;
    }

    public void setPcoPcProv1aaAcq(AfDecimal pcoPcProv1aaAcq) {
        writeDecimalAsPacked(Pos.PCO_PC_PROV1AA_ACQ, pcoPcProv1aaAcq.copy());
    }

    public void setPcoPcProv1aaAcqFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_PC_PROV1AA_ACQ, Pos.PCO_PC_PROV1AA_ACQ);
    }

    /**Original name: PCO-PC-PROV-1AA-ACQ<br>*/
    public AfDecimal getPcoPcProv1aaAcq() {
        return readPackedAsDecimal(Pos.PCO_PC_PROV1AA_ACQ, Len.Int.PCO_PC_PROV1AA_ACQ, Len.Fract.PCO_PC_PROV1AA_ACQ);
    }

    public byte[] getPcoPcProv1aaAcqAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_PC_PROV1AA_ACQ, Pos.PCO_PC_PROV1AA_ACQ);
        return buffer;
    }

    public void initPcoPcProv1aaAcqHighValues() {
        fill(Pos.PCO_PC_PROV1AA_ACQ, Len.PCO_PC_PROV1AA_ACQ, Types.HIGH_CHAR_VAL);
    }

    public void setPcoPcProv1aaAcqNull(String pcoPcProv1aaAcqNull) {
        writeString(Pos.PCO_PC_PROV1AA_ACQ_NULL, pcoPcProv1aaAcqNull, Len.PCO_PC_PROV1AA_ACQ_NULL);
    }

    /**Original name: PCO-PC-PROV-1AA-ACQ-NULL<br>*/
    public String getPcoPcProv1aaAcqNull() {
        return readString(Pos.PCO_PC_PROV1AA_ACQ_NULL, Len.PCO_PC_PROV1AA_ACQ_NULL);
    }

    public String getPcoPcProv1aaAcqNullFormatted() {
        return Functions.padBlanks(getPcoPcProv1aaAcqNull(), Len.PCO_PC_PROV1AA_ACQ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_PC_PROV1AA_ACQ = 1;
        public static final int PCO_PC_PROV1AA_ACQ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_PC_PROV1AA_ACQ = 4;
        public static final int PCO_PC_PROV1AA_ACQ_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_PC_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PCO_PC_PROV1AA_ACQ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
