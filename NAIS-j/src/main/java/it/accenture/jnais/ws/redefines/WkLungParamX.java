package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WK-LUNG-PARAM-X<br>
 * Variable: WK-LUNG-PARAM-X from program LLBM0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WkLungParamX extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WkLungParamX() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WK_LUNG_PARAM_X;
    }

    public void setWkLungParamX(String wkLungParamX) {
        writeString(Pos.WK_LUNG_PARAM_X, wkLungParamX, Len.WK_LUNG_PARAM_X);
    }

    /**Original name: WK-LUNG-PARAM-X<br>*/
    public String getWkLungParamX() {
        return readString(Pos.WK_LUNG_PARAM_X, Len.WK_LUNG_PARAM_X);
    }

    /**Original name: WK-LUNG-PARAM-9<br>*/
    public short getWkLungParam9() {
        return readNumDispUnsignedShort(Pos.WK_LUNG_PARAM9, Len.WK_LUNG_PARAM9);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WK_LUNG_PARAM_X = 1;
        public static final int WK_LUNG_PARAM9 = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WK_LUNG_PARAM_X = 2;
        public static final int WK_LUNG_PARAM9 = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
