package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WP67-CPT-FIN<br>
 * Variable: WP67-CPT-FIN from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67CptFin extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67CptFin() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_CPT_FIN;
    }

    public void setWp67CptFin(AfDecimal wp67CptFin) {
        writeDecimalAsPacked(Pos.WP67_CPT_FIN, wp67CptFin.copy());
    }

    public void setWp67CptFinFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_CPT_FIN, Pos.WP67_CPT_FIN);
    }

    /**Original name: WP67-CPT-FIN<br>*/
    public AfDecimal getWp67CptFin() {
        return readPackedAsDecimal(Pos.WP67_CPT_FIN, Len.Int.WP67_CPT_FIN, Len.Fract.WP67_CPT_FIN);
    }

    public byte[] getWp67CptFinAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_CPT_FIN, Pos.WP67_CPT_FIN);
        return buffer;
    }

    public void setWp67CptFinNull(String wp67CptFinNull) {
        writeString(Pos.WP67_CPT_FIN_NULL, wp67CptFinNull, Len.WP67_CPT_FIN_NULL);
    }

    /**Original name: WP67-CPT-FIN-NULL<br>*/
    public String getWp67CptFinNull() {
        return readString(Pos.WP67_CPT_FIN_NULL, Len.WP67_CPT_FIN_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_CPT_FIN = 1;
        public static final int WP67_CPT_FIN_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_CPT_FIN = 8;
        public static final int WP67_CPT_FIN_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WP67_CPT_FIN = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_CPT_FIN = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
