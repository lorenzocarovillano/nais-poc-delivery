package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: WPCO-DT-ULTC-BNSRIC-CL<br>
 * Variable: WPCO-DT-ULTC-BNSRIC-CL from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WpcoDtUltcBnsricCl extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WpcoDtUltcBnsricCl() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WPCO_DT_ULTC_BNSRIC_CL;
    }

    public void setWpcoDtUltcBnsricCl(int wpcoDtUltcBnsricCl) {
        writeIntAsPacked(Pos.WPCO_DT_ULTC_BNSRIC_CL, wpcoDtUltcBnsricCl, Len.Int.WPCO_DT_ULTC_BNSRIC_CL);
    }

    public void setDpcoDtUltcBnsricClFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSRIC_CL, Pos.WPCO_DT_ULTC_BNSRIC_CL);
    }

    /**Original name: WPCO-DT-ULTC-BNSRIC-CL<br>*/
    public int getWpcoDtUltcBnsricCl() {
        return readPackedAsInt(Pos.WPCO_DT_ULTC_BNSRIC_CL, Len.Int.WPCO_DT_ULTC_BNSRIC_CL);
    }

    public byte[] getWpcoDtUltcBnsricClAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WPCO_DT_ULTC_BNSRIC_CL, Pos.WPCO_DT_ULTC_BNSRIC_CL);
        return buffer;
    }

    public void setWpcoDtUltcBnsricClNull(String wpcoDtUltcBnsricClNull) {
        writeString(Pos.WPCO_DT_ULTC_BNSRIC_CL_NULL, wpcoDtUltcBnsricClNull, Len.WPCO_DT_ULTC_BNSRIC_CL_NULL);
    }

    /**Original name: WPCO-DT-ULTC-BNSRIC-CL-NULL<br>*/
    public String getWpcoDtUltcBnsricClNull() {
        return readString(Pos.WPCO_DT_ULTC_BNSRIC_CL_NULL, Len.WPCO_DT_ULTC_BNSRIC_CL_NULL);
    }

    public String getWpcoDtUltcBnsricClNullFormatted() {
        return Functions.padBlanks(getWpcoDtUltcBnsricClNull(), Len.WPCO_DT_ULTC_BNSRIC_CL_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSRIC_CL = 1;
        public static final int WPCO_DT_ULTC_BNSRIC_CL_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WPCO_DT_ULTC_BNSRIC_CL = 5;
        public static final int WPCO_DT_ULTC_BNSRIC_CL_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WPCO_DT_ULTC_BNSRIC_CL = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
