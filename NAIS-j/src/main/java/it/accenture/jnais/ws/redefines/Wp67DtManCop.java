package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;

/**Original name: WP67-DT-MAN-COP<br>
 * Variable: WP67-DT-MAN-COP from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wp67DtManCop extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wp67DtManCop() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WP67_DT_MAN_COP;
    }

    public void setWp67DtManCop(int wp67DtManCop) {
        writeIntAsPacked(Pos.WP67_DT_MAN_COP, wp67DtManCop, Len.Int.WP67_DT_MAN_COP);
    }

    public void setWp67DtManCopFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WP67_DT_MAN_COP, Pos.WP67_DT_MAN_COP);
    }

    /**Original name: WP67-DT-MAN-COP<br>*/
    public int getWp67DtManCop() {
        return readPackedAsInt(Pos.WP67_DT_MAN_COP, Len.Int.WP67_DT_MAN_COP);
    }

    public byte[] getWp67DtManCopAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WP67_DT_MAN_COP, Pos.WP67_DT_MAN_COP);
        return buffer;
    }

    public void setWp67DtManCopNull(String wp67DtManCopNull) {
        writeString(Pos.WP67_DT_MAN_COP_NULL, wp67DtManCopNull, Len.WP67_DT_MAN_COP_NULL);
    }

    /**Original name: WP67-DT-MAN-COP-NULL<br>*/
    public String getWp67DtManCopNull() {
        return readString(Pos.WP67_DT_MAN_COP_NULL, Len.WP67_DT_MAN_COP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WP67_DT_MAN_COP = 1;
        public static final int WP67_DT_MAN_COP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WP67_DT_MAN_COP = 5;
        public static final int WP67_DT_MAN_COP_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WP67_DT_MAN_COP = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
