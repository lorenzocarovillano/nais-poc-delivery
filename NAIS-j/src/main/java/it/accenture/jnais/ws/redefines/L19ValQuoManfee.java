package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L19-VAL-QUO-MANFEE<br>
 * Variable: L19-VAL-QUO-MANFEE from program IDBSL190<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L19ValQuoManfee extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L19ValQuoManfee() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L19_VAL_QUO_MANFEE;
    }

    public void setL19ValQuoManfee(AfDecimal l19ValQuoManfee) {
        writeDecimalAsPacked(Pos.L19_VAL_QUO_MANFEE, l19ValQuoManfee.copy());
    }

    public void setL19ValQuoManfeeFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L19_VAL_QUO_MANFEE, Pos.L19_VAL_QUO_MANFEE);
    }

    /**Original name: L19-VAL-QUO-MANFEE<br>*/
    public AfDecimal getL19ValQuoManfee() {
        return readPackedAsDecimal(Pos.L19_VAL_QUO_MANFEE, Len.Int.L19_VAL_QUO_MANFEE, Len.Fract.L19_VAL_QUO_MANFEE);
    }

    public byte[] getL19ValQuoManfeeAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L19_VAL_QUO_MANFEE, Pos.L19_VAL_QUO_MANFEE);
        return buffer;
    }

    public void setL19ValQuoManfeeNull(String l19ValQuoManfeeNull) {
        writeString(Pos.L19_VAL_QUO_MANFEE_NULL, l19ValQuoManfeeNull, Len.L19_VAL_QUO_MANFEE_NULL);
    }

    /**Original name: L19-VAL-QUO-MANFEE-NULL<br>*/
    public String getL19ValQuoManfeeNull() {
        return readString(Pos.L19_VAL_QUO_MANFEE_NULL, Len.L19_VAL_QUO_MANFEE_NULL);
    }

    public String getL19ValQuoManfeeNullFormatted() {
        return Functions.padBlanks(getL19ValQuoManfeeNull(), Len.L19_VAL_QUO_MANFEE_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L19_VAL_QUO_MANFEE = 1;
        public static final int L19_VAL_QUO_MANFEE_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L19_VAL_QUO_MANFEE = 7;
        public static final int L19_VAL_QUO_MANFEE_NULL = 7;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L19_VAL_QUO_MANFEE = 7;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L19_VAL_QUO_MANFEE = 5;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
