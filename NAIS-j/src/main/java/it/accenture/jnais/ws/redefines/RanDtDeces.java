package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: RAN-DT-DECES<br>
 * Variable: RAN-DT-DECES from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class RanDtDeces extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public RanDtDeces() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.RAN_DT_DECES;
    }

    public void setRanDtDeces(int ranDtDeces) {
        writeIntAsPacked(Pos.RAN_DT_DECES, ranDtDeces, Len.Int.RAN_DT_DECES);
    }

    public void setRanDtDecesFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.RAN_DT_DECES, Pos.RAN_DT_DECES);
    }

    /**Original name: RAN-DT-DECES<br>*/
    public int getRanDtDeces() {
        return readPackedAsInt(Pos.RAN_DT_DECES, Len.Int.RAN_DT_DECES);
    }

    public byte[] getRanDtDecesAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.RAN_DT_DECES, Pos.RAN_DT_DECES);
        return buffer;
    }

    public void setRanDtDecesNull(String ranDtDecesNull) {
        writeString(Pos.RAN_DT_DECES_NULL, ranDtDecesNull, Len.RAN_DT_DECES_NULL);
    }

    /**Original name: RAN-DT-DECES-NULL<br>*/
    public String getRanDtDecesNull() {
        return readString(Pos.RAN_DT_DECES_NULL, Len.RAN_DT_DECES_NULL);
    }

    public String getRanDtDecesNullFormatted() {
        return Functions.padBlanks(getRanDtDecesNull(), Len.RAN_DT_DECES_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int RAN_DT_DECES = 1;
        public static final int RAN_DT_DECES_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int RAN_DT_DECES = 5;
        public static final int RAN_DT_DECES_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int RAN_DT_DECES = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
