package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.ws.redefines.MbsIdBatch;
import it.accenture.jnais.ws.redefines.MbsIdJob;

/**Original name: MOVI-BATCH-SOSP<br>
 * Variable: MOVI-BATCH-SOSP from copybook IDBVMBS1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class MoviBatchSospLoas9000 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: MBS-ID-MOVI-BATCH-SOSP
    private int mbsIdMoviBatchSosp = DefaultValues.INT_VAL;
    //Original name: MBS-COD-COMP-ANIA
    private int mbsCodCompAnia = DefaultValues.INT_VAL;
    //Original name: MBS-ID-OGG
    private int mbsIdOgg = DefaultValues.INT_VAL;
    //Original name: MBS-TP-OGG
    private String mbsTpOgg = DefaultValues.stringVal(Len.MBS_TP_OGG);
    //Original name: MBS-ID-MOVI
    private int mbsIdMovi = DefaultValues.INT_VAL;
    //Original name: MBS-TP-MOVI
    private int mbsTpMovi = DefaultValues.INT_VAL;
    //Original name: MBS-DT-EFF
    private int mbsDtEff = DefaultValues.INT_VAL;
    //Original name: MBS-ID-OGG-BLOCCO
    private int mbsIdOggBlocco = DefaultValues.INT_VAL;
    //Original name: MBS-TP-FRM-ASSVA
    private String mbsTpFrmAssva = DefaultValues.stringVal(Len.MBS_TP_FRM_ASSVA);
    //Original name: MBS-ID-BATCH
    private MbsIdBatch mbsIdBatch = new MbsIdBatch();
    //Original name: MBS-ID-JOB
    private MbsIdJob mbsIdJob = new MbsIdJob();
    //Original name: MBS-STEP-ELAB
    private char mbsStepElab = DefaultValues.CHAR_VAL;
    //Original name: MBS-FL-MOVI-SOSP
    private char mbsFlMoviSosp = DefaultValues.CHAR_VAL;
    //Original name: MBS-D-INPUT-MOVI-SOSP-LEN
    private short mbsDInputMoviSospLen = DefaultValues.BIN_SHORT_VAL;
    //Original name: MBS-D-INPUT-MOVI-SOSP
    private String mbsDInputMoviSosp = DefaultValues.stringVal(Len.MBS_D_INPUT_MOVI_SOSP);
    //Original name: MBS-DS-OPER-SQL
    private char mbsDsOperSql = DefaultValues.CHAR_VAL;
    //Original name: MBS-DS-VER
    private int mbsDsVer = DefaultValues.INT_VAL;
    //Original name: MBS-DS-TS-CPTZ
    private long mbsDsTsCptz = DefaultValues.LONG_VAL;
    //Original name: MBS-DS-UTENTE
    private String mbsDsUtente = DefaultValues.stringVal(Len.MBS_DS_UTENTE);
    //Original name: MBS-DS-STATO-ELAB
    private char mbsDsStatoElab = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.MOVI_BATCH_SOSP;
    }

    @Override
    public void deserialize(byte[] buf) {
        setMoviBatchSospBytes(buf);
    }

    public void setMoviBatchSospBytes(byte[] buffer) {
        setMoviBatchSospBytes(buffer, 1);
    }

    public byte[] getMoviBatchSospBytes() {
        byte[] buffer = new byte[Len.MOVI_BATCH_SOSP];
        return getMoviBatchSospBytes(buffer, 1);
    }

    public void setMoviBatchSospBytes(byte[] buffer, int offset) {
        int position = offset;
        mbsIdMoviBatchSosp = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_ID_MOVI_BATCH_SOSP, 0);
        position += Len.MBS_ID_MOVI_BATCH_SOSP;
        mbsCodCompAnia = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_COD_COMP_ANIA, 0);
        position += Len.MBS_COD_COMP_ANIA;
        mbsIdOgg = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_ID_OGG, 0);
        position += Len.MBS_ID_OGG;
        mbsTpOgg = MarshalByte.readString(buffer, position, Len.MBS_TP_OGG);
        position += Len.MBS_TP_OGG;
        mbsIdMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_ID_MOVI, 0);
        position += Len.MBS_ID_MOVI;
        mbsTpMovi = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_TP_MOVI, 0);
        position += Len.MBS_TP_MOVI;
        mbsDtEff = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_DT_EFF, 0);
        position += Len.MBS_DT_EFF;
        mbsIdOggBlocco = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_ID_OGG_BLOCCO, 0);
        position += Len.MBS_ID_OGG_BLOCCO;
        mbsTpFrmAssva = MarshalByte.readString(buffer, position, Len.MBS_TP_FRM_ASSVA);
        position += Len.MBS_TP_FRM_ASSVA;
        mbsIdBatch.setMbsIdBatchFromBuffer(buffer, position);
        position += MbsIdBatch.Len.MBS_ID_BATCH;
        mbsIdJob.setMbsIdJobFromBuffer(buffer, position);
        position += MbsIdJob.Len.MBS_ID_JOB;
        mbsStepElab = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mbsFlMoviSosp = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        setMbsDInputMoviSospVcharBytes(buffer, position);
        position += Len.MBS_D_INPUT_MOVI_SOSP_VCHAR;
        mbsDsOperSql = MarshalByte.readChar(buffer, position);
        position += Types.CHAR_SIZE;
        mbsDsVer = MarshalByte.readPackedAsInt(buffer, position, Len.Int.MBS_DS_VER, 0);
        position += Len.MBS_DS_VER;
        mbsDsTsCptz = MarshalByte.readPackedAsLong(buffer, position, Len.Int.MBS_DS_TS_CPTZ, 0);
        position += Len.MBS_DS_TS_CPTZ;
        mbsDsUtente = MarshalByte.readString(buffer, position, Len.MBS_DS_UTENTE);
        position += Len.MBS_DS_UTENTE;
        mbsDsStatoElab = MarshalByte.readChar(buffer, position);
    }

    public byte[] getMoviBatchSospBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeIntAsPacked(buffer, position, mbsIdMoviBatchSosp, Len.Int.MBS_ID_MOVI_BATCH_SOSP, 0);
        position += Len.MBS_ID_MOVI_BATCH_SOSP;
        MarshalByte.writeIntAsPacked(buffer, position, mbsCodCompAnia, Len.Int.MBS_COD_COMP_ANIA, 0);
        position += Len.MBS_COD_COMP_ANIA;
        MarshalByte.writeIntAsPacked(buffer, position, mbsIdOgg, Len.Int.MBS_ID_OGG, 0);
        position += Len.MBS_ID_OGG;
        MarshalByte.writeString(buffer, position, mbsTpOgg, Len.MBS_TP_OGG);
        position += Len.MBS_TP_OGG;
        MarshalByte.writeIntAsPacked(buffer, position, mbsIdMovi, Len.Int.MBS_ID_MOVI, 0);
        position += Len.MBS_ID_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, mbsTpMovi, Len.Int.MBS_TP_MOVI, 0);
        position += Len.MBS_TP_MOVI;
        MarshalByte.writeIntAsPacked(buffer, position, mbsDtEff, Len.Int.MBS_DT_EFF, 0);
        position += Len.MBS_DT_EFF;
        MarshalByte.writeIntAsPacked(buffer, position, mbsIdOggBlocco, Len.Int.MBS_ID_OGG_BLOCCO, 0);
        position += Len.MBS_ID_OGG_BLOCCO;
        MarshalByte.writeString(buffer, position, mbsTpFrmAssva, Len.MBS_TP_FRM_ASSVA);
        position += Len.MBS_TP_FRM_ASSVA;
        mbsIdBatch.getMbsIdBatchAsBuffer(buffer, position);
        position += MbsIdBatch.Len.MBS_ID_BATCH;
        mbsIdJob.getMbsIdJobAsBuffer(buffer, position);
        position += MbsIdJob.Len.MBS_ID_JOB;
        MarshalByte.writeChar(buffer, position, mbsStepElab);
        position += Types.CHAR_SIZE;
        MarshalByte.writeChar(buffer, position, mbsFlMoviSosp);
        position += Types.CHAR_SIZE;
        getMbsDInputMoviSospVcharBytes(buffer, position);
        position += Len.MBS_D_INPUT_MOVI_SOSP_VCHAR;
        MarshalByte.writeChar(buffer, position, mbsDsOperSql);
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, mbsDsVer, Len.Int.MBS_DS_VER, 0);
        position += Len.MBS_DS_VER;
        MarshalByte.writeLongAsPacked(buffer, position, mbsDsTsCptz, Len.Int.MBS_DS_TS_CPTZ, 0);
        position += Len.MBS_DS_TS_CPTZ;
        MarshalByte.writeString(buffer, position, mbsDsUtente, Len.MBS_DS_UTENTE);
        position += Len.MBS_DS_UTENTE;
        MarshalByte.writeChar(buffer, position, mbsDsStatoElab);
        return buffer;
    }

    public void setMbsIdMoviBatchSosp(int mbsIdMoviBatchSosp) {
        this.mbsIdMoviBatchSosp = mbsIdMoviBatchSosp;
    }

    public int getMbsIdMoviBatchSosp() {
        return this.mbsIdMoviBatchSosp;
    }

    public void setMbsCodCompAnia(int mbsCodCompAnia) {
        this.mbsCodCompAnia = mbsCodCompAnia;
    }

    public int getMbsCodCompAnia() {
        return this.mbsCodCompAnia;
    }

    public void setMbsIdOgg(int mbsIdOgg) {
        this.mbsIdOgg = mbsIdOgg;
    }

    public int getMbsIdOgg() {
        return this.mbsIdOgg;
    }

    public void setMbsTpOgg(String mbsTpOgg) {
        this.mbsTpOgg = Functions.subString(mbsTpOgg, Len.MBS_TP_OGG);
    }

    public String getMbsTpOgg() {
        return this.mbsTpOgg;
    }

    public void setMbsIdMovi(int mbsIdMovi) {
        this.mbsIdMovi = mbsIdMovi;
    }

    public int getMbsIdMovi() {
        return this.mbsIdMovi;
    }

    public void setMbsTpMovi(int mbsTpMovi) {
        this.mbsTpMovi = mbsTpMovi;
    }

    public int getMbsTpMovi() {
        return this.mbsTpMovi;
    }

    public void setMbsDtEff(int mbsDtEff) {
        this.mbsDtEff = mbsDtEff;
    }

    public int getMbsDtEff() {
        return this.mbsDtEff;
    }

    public void setMbsIdOggBlocco(int mbsIdOggBlocco) {
        this.mbsIdOggBlocco = mbsIdOggBlocco;
    }

    public int getMbsIdOggBlocco() {
        return this.mbsIdOggBlocco;
    }

    public void setMbsTpFrmAssva(String mbsTpFrmAssva) {
        this.mbsTpFrmAssva = Functions.subString(mbsTpFrmAssva, Len.MBS_TP_FRM_ASSVA);
    }

    public String getMbsTpFrmAssva() {
        return this.mbsTpFrmAssva;
    }

    public void setMbsStepElab(char mbsStepElab) {
        this.mbsStepElab = mbsStepElab;
    }

    public char getMbsStepElab() {
        return this.mbsStepElab;
    }

    public void setMbsFlMoviSosp(char mbsFlMoviSosp) {
        this.mbsFlMoviSosp = mbsFlMoviSosp;
    }

    public char getMbsFlMoviSosp() {
        return this.mbsFlMoviSosp;
    }

    public void setMbsDInputMoviSospVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        mbsDInputMoviSospLen = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        mbsDInputMoviSosp = MarshalByte.readString(buffer, position, Len.MBS_D_INPUT_MOVI_SOSP);
    }

    public byte[] getMbsDInputMoviSospVcharBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeBinaryShort(buffer, position, mbsDInputMoviSospLen);
        position += Types.SHORT_SIZE;
        MarshalByte.writeString(buffer, position, mbsDInputMoviSosp, Len.MBS_D_INPUT_MOVI_SOSP);
        return buffer;
    }

    public void setMbsDInputMoviSospLen(short mbsDInputMoviSospLen) {
        this.mbsDInputMoviSospLen = mbsDInputMoviSospLen;
    }

    public short getMbsDInputMoviSospLen() {
        return this.mbsDInputMoviSospLen;
    }

    public void setMbsDInputMoviSosp(String mbsDInputMoviSosp) {
        this.mbsDInputMoviSosp = Functions.subString(mbsDInputMoviSosp, Len.MBS_D_INPUT_MOVI_SOSP);
    }

    public String getMbsDInputMoviSosp() {
        return this.mbsDInputMoviSosp;
    }

    public String getMbsDInputMoviSospFormatted() {
        return Functions.padBlanks(getMbsDInputMoviSosp(), Len.MBS_D_INPUT_MOVI_SOSP);
    }

    public void setMbsDsOperSql(char mbsDsOperSql) {
        this.mbsDsOperSql = mbsDsOperSql;
    }

    public char getMbsDsOperSql() {
        return this.mbsDsOperSql;
    }

    public void setMbsDsVer(int mbsDsVer) {
        this.mbsDsVer = mbsDsVer;
    }

    public int getMbsDsVer() {
        return this.mbsDsVer;
    }

    public void setMbsDsTsCptz(long mbsDsTsCptz) {
        this.mbsDsTsCptz = mbsDsTsCptz;
    }

    public long getMbsDsTsCptz() {
        return this.mbsDsTsCptz;
    }

    public void setMbsDsUtente(String mbsDsUtente) {
        this.mbsDsUtente = Functions.subString(mbsDsUtente, Len.MBS_DS_UTENTE);
    }

    public String getMbsDsUtente() {
        return this.mbsDsUtente;
    }

    public void setMbsDsStatoElab(char mbsDsStatoElab) {
        this.mbsDsStatoElab = mbsDsStatoElab;
    }

    public char getMbsDsStatoElab() {
        return this.mbsDsStatoElab;
    }

    @Override
    public byte[] serialize() {
        return getMoviBatchSospBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int MBS_ID_MOVI_BATCH_SOSP = 5;
        public static final int MBS_COD_COMP_ANIA = 3;
        public static final int MBS_ID_OGG = 5;
        public static final int MBS_TP_OGG = 2;
        public static final int MBS_ID_MOVI = 5;
        public static final int MBS_TP_MOVI = 3;
        public static final int MBS_DT_EFF = 5;
        public static final int MBS_ID_OGG_BLOCCO = 5;
        public static final int MBS_TP_FRM_ASSVA = 2;
        public static final int MBS_STEP_ELAB = 1;
        public static final int MBS_FL_MOVI_SOSP = 1;
        public static final int MBS_D_INPUT_MOVI_SOSP_LEN = 2;
        public static final int MBS_D_INPUT_MOVI_SOSP = 300;
        public static final int MBS_D_INPUT_MOVI_SOSP_VCHAR = MBS_D_INPUT_MOVI_SOSP_LEN + MBS_D_INPUT_MOVI_SOSP;
        public static final int MBS_DS_OPER_SQL = 1;
        public static final int MBS_DS_VER = 5;
        public static final int MBS_DS_TS_CPTZ = 10;
        public static final int MBS_DS_UTENTE = 20;
        public static final int MBS_DS_STATO_ELAB = 1;
        public static final int MOVI_BATCH_SOSP = MBS_ID_MOVI_BATCH_SOSP + MBS_COD_COMP_ANIA + MBS_ID_OGG + MBS_TP_OGG + MBS_ID_MOVI + MBS_TP_MOVI + MBS_DT_EFF + MBS_ID_OGG_BLOCCO + MBS_TP_FRM_ASSVA + MbsIdBatch.Len.MBS_ID_BATCH + MbsIdJob.Len.MBS_ID_JOB + MBS_STEP_ELAB + MBS_FL_MOVI_SOSP + MBS_D_INPUT_MOVI_SOSP_VCHAR + MBS_DS_OPER_SQL + MBS_DS_VER + MBS_DS_TS_CPTZ + MBS_DS_UTENTE + MBS_DS_STATO_ELAB;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int MBS_ID_MOVI_BATCH_SOSP = 9;
            public static final int MBS_COD_COMP_ANIA = 5;
            public static final int MBS_ID_OGG = 9;
            public static final int MBS_ID_MOVI = 9;
            public static final int MBS_TP_MOVI = 5;
            public static final int MBS_DT_EFF = 8;
            public static final int MBS_ID_OGG_BLOCCO = 9;
            public static final int MBS_DS_VER = 9;
            public static final int MBS_DS_TS_CPTZ = 18;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
