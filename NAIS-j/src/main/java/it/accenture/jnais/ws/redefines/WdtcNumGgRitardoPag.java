package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-NUM-GG-RITARDO-PAG<br>
 * Variable: WDTC-NUM-GG-RITARDO-PAG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcNumGgRitardoPag extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcNumGgRitardoPag() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_NUM_GG_RITARDO_PAG;
    }

    public void setWdtcNumGgRitardoPag(int wdtcNumGgRitardoPag) {
        writeIntAsPacked(Pos.WDTC_NUM_GG_RITARDO_PAG, wdtcNumGgRitardoPag, Len.Int.WDTC_NUM_GG_RITARDO_PAG);
    }

    public void setWdtcNumGgRitardoPagFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_NUM_GG_RITARDO_PAG, Pos.WDTC_NUM_GG_RITARDO_PAG);
    }

    /**Original name: WDTC-NUM-GG-RITARDO-PAG<br>*/
    public int getWdtcNumGgRitardoPag() {
        return readPackedAsInt(Pos.WDTC_NUM_GG_RITARDO_PAG, Len.Int.WDTC_NUM_GG_RITARDO_PAG);
    }

    public byte[] getWdtcNumGgRitardoPagAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_NUM_GG_RITARDO_PAG, Pos.WDTC_NUM_GG_RITARDO_PAG);
        return buffer;
    }

    public void initWdtcNumGgRitardoPagSpaces() {
        fill(Pos.WDTC_NUM_GG_RITARDO_PAG, Len.WDTC_NUM_GG_RITARDO_PAG, Types.SPACE_CHAR);
    }

    public void setWdtcNumGgRitardoPagNull(String wdtcNumGgRitardoPagNull) {
        writeString(Pos.WDTC_NUM_GG_RITARDO_PAG_NULL, wdtcNumGgRitardoPagNull, Len.WDTC_NUM_GG_RITARDO_PAG_NULL);
    }

    /**Original name: WDTC-NUM-GG-RITARDO-PAG-NULL<br>*/
    public String getWdtcNumGgRitardoPagNull() {
        return readString(Pos.WDTC_NUM_GG_RITARDO_PAG_NULL, Len.WDTC_NUM_GG_RITARDO_PAG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_NUM_GG_RITARDO_PAG = 1;
        public static final int WDTC_NUM_GG_RITARDO_PAG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_NUM_GG_RITARDO_PAG = 3;
        public static final int WDTC_NUM_GG_RITARDO_PAG_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_NUM_GG_RITARDO_PAG = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
