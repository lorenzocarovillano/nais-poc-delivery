package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DTR-SOPR-SPO<br>
 * Variable: DTR-SOPR-SPO from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DtrSoprSpo extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DtrSoprSpo() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DTR_SOPR_SPO;
    }

    public void setDtrSoprSpo(AfDecimal dtrSoprSpo) {
        writeDecimalAsPacked(Pos.DTR_SOPR_SPO, dtrSoprSpo.copy());
    }

    public void setDtrSoprSpoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DTR_SOPR_SPO, Pos.DTR_SOPR_SPO);
    }

    /**Original name: DTR-SOPR-SPO<br>*/
    public AfDecimal getDtrSoprSpo() {
        return readPackedAsDecimal(Pos.DTR_SOPR_SPO, Len.Int.DTR_SOPR_SPO, Len.Fract.DTR_SOPR_SPO);
    }

    public byte[] getDtrSoprSpoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DTR_SOPR_SPO, Pos.DTR_SOPR_SPO);
        return buffer;
    }

    public void setDtrSoprSpoNull(String dtrSoprSpoNull) {
        writeString(Pos.DTR_SOPR_SPO_NULL, dtrSoprSpoNull, Len.DTR_SOPR_SPO_NULL);
    }

    /**Original name: DTR-SOPR-SPO-NULL<br>*/
    public String getDtrSoprSpoNull() {
        return readString(Pos.DTR_SOPR_SPO_NULL, Len.DTR_SOPR_SPO_NULL);
    }

    public String getDtrSoprSpoNullFormatted() {
        return Functions.padBlanks(getDtrSoprSpoNull(), Len.DTR_SOPR_SPO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_SPO = 1;
        public static final int DTR_SOPR_SPO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DTR_SOPR_SPO = 8;
        public static final int DTR_SOPR_SPO_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_SPO = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int DTR_SOPR_SPO = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
