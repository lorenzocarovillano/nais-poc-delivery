package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-COS-RUN-ASSVA<br>
 * Variable: L3421-COS-RUN-ASSVA from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421CosRunAssva extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421CosRunAssva() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_COS_RUN_ASSVA;
    }

    public void setL3421CosRunAssvaFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_COS_RUN_ASSVA, Pos.L3421_COS_RUN_ASSVA);
    }

    /**Original name: L3421-COS-RUN-ASSVA<br>
	 * <pre> INC000006400747</pre>*/
    public AfDecimal getL3421CosRunAssva() {
        return readPackedAsDecimal(Pos.L3421_COS_RUN_ASSVA, Len.Int.L3421_COS_RUN_ASSVA, Len.Fract.L3421_COS_RUN_ASSVA);
    }

    public byte[] getL3421CosRunAssvaAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_COS_RUN_ASSVA, Pos.L3421_COS_RUN_ASSVA);
        return buffer;
    }

    /**Original name: L3421-COS-RUN-ASSVA-NULL<br>*/
    public String getL3421CosRunAssvaNull() {
        return readString(Pos.L3421_COS_RUN_ASSVA_NULL, Len.L3421_COS_RUN_ASSVA_NULL);
    }

    public String getL3421CosRunAssvaNullFormatted() {
        return Functions.padBlanks(getL3421CosRunAssvaNull(), Len.L3421_COS_RUN_ASSVA_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_COS_RUN_ASSVA = 1;
        public static final int L3421_COS_RUN_ASSVA_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_COS_RUN_ASSVA = 8;
        public static final int L3421_COS_RUN_ASSVA_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_COS_RUN_ASSVA = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_COS_RUN_ASSVA = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
