package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WTGA-MIN-TRNUT<br>
 * Variable: WTGA-MIN-TRNUT from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtgaMinTrnut extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtgaMinTrnut() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTGA_MIN_TRNUT;
    }

    public void setWtgaMinTrnut(AfDecimal wtgaMinTrnut) {
        writeDecimalAsPacked(Pos.WTGA_MIN_TRNUT, wtgaMinTrnut.copy());
    }

    public void setWtgaMinTrnutFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTGA_MIN_TRNUT, Pos.WTGA_MIN_TRNUT);
    }

    /**Original name: WTGA-MIN-TRNUT<br>*/
    public AfDecimal getWtgaMinTrnut() {
        return readPackedAsDecimal(Pos.WTGA_MIN_TRNUT, Len.Int.WTGA_MIN_TRNUT, Len.Fract.WTGA_MIN_TRNUT);
    }

    public byte[] getWtgaMinTrnutAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTGA_MIN_TRNUT, Pos.WTGA_MIN_TRNUT);
        return buffer;
    }

    public void initWtgaMinTrnutSpaces() {
        fill(Pos.WTGA_MIN_TRNUT, Len.WTGA_MIN_TRNUT, Types.SPACE_CHAR);
    }

    public void setWtgaMinTrnutNull(String wtgaMinTrnutNull) {
        writeString(Pos.WTGA_MIN_TRNUT_NULL, wtgaMinTrnutNull, Len.WTGA_MIN_TRNUT_NULL);
    }

    /**Original name: WTGA-MIN-TRNUT-NULL<br>*/
    public String getWtgaMinTrnutNull() {
        return readString(Pos.WTGA_MIN_TRNUT_NULL, Len.WTGA_MIN_TRNUT_NULL);
    }

    public String getWtgaMinTrnutNullFormatted() {
        return Functions.padBlanks(getWtgaMinTrnutNull(), Len.WTGA_MIN_TRNUT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTGA_MIN_TRNUT = 1;
        public static final int WTGA_MIN_TRNUT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTGA_MIN_TRNUT = 8;
        public static final int WTGA_MIN_TRNUT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTGA_MIN_TRNUT = 9;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTGA_MIN_TRNUT = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
