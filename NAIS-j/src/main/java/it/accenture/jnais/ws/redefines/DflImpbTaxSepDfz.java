package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPB-TAX-SEP-DFZ<br>
 * Variable: DFL-IMPB-TAX-SEP-DFZ from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpbTaxSepDfz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpbTaxSepDfz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPB_TAX_SEP_DFZ;
    }

    public void setDflImpbTaxSepDfz(AfDecimal dflImpbTaxSepDfz) {
        writeDecimalAsPacked(Pos.DFL_IMPB_TAX_SEP_DFZ, dflImpbTaxSepDfz.copy());
    }

    public void setDflImpbTaxSepDfzFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPB_TAX_SEP_DFZ, Pos.DFL_IMPB_TAX_SEP_DFZ);
    }

    /**Original name: DFL-IMPB-TAX-SEP-DFZ<br>*/
    public AfDecimal getDflImpbTaxSepDfz() {
        return readPackedAsDecimal(Pos.DFL_IMPB_TAX_SEP_DFZ, Len.Int.DFL_IMPB_TAX_SEP_DFZ, Len.Fract.DFL_IMPB_TAX_SEP_DFZ);
    }

    public byte[] getDflImpbTaxSepDfzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPB_TAX_SEP_DFZ, Pos.DFL_IMPB_TAX_SEP_DFZ);
        return buffer;
    }

    public void setDflImpbTaxSepDfzNull(String dflImpbTaxSepDfzNull) {
        writeString(Pos.DFL_IMPB_TAX_SEP_DFZ_NULL, dflImpbTaxSepDfzNull, Len.DFL_IMPB_TAX_SEP_DFZ_NULL);
    }

    /**Original name: DFL-IMPB-TAX-SEP-DFZ-NULL<br>*/
    public String getDflImpbTaxSepDfzNull() {
        return readString(Pos.DFL_IMPB_TAX_SEP_DFZ_NULL, Len.DFL_IMPB_TAX_SEP_DFZ_NULL);
    }

    public String getDflImpbTaxSepDfzNullFormatted() {
        return Functions.padBlanks(getDflImpbTaxSepDfzNull(), Len.DFL_IMPB_TAX_SEP_DFZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TAX_SEP_DFZ = 1;
        public static final int DFL_IMPB_TAX_SEP_DFZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPB_TAX_SEP_DFZ = 8;
        public static final int DFL_IMPB_TAX_SEP_DFZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TAX_SEP_DFZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPB_TAX_SEP_DFZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
