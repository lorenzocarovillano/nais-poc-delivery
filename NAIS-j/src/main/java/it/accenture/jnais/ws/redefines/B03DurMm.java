package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-DUR-MM<br>
 * Variable: B03-DUR-MM from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03DurMm extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03DurMm() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_DUR_MM;
    }

    public void setB03DurMm(int b03DurMm) {
        writeIntAsPacked(Pos.B03_DUR_MM, b03DurMm, Len.Int.B03_DUR_MM);
    }

    public void setB03DurMmFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_DUR_MM, Pos.B03_DUR_MM);
    }

    /**Original name: B03-DUR-MM<br>*/
    public int getB03DurMm() {
        return readPackedAsInt(Pos.B03_DUR_MM, Len.Int.B03_DUR_MM);
    }

    public byte[] getB03DurMmAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_DUR_MM, Pos.B03_DUR_MM);
        return buffer;
    }

    public void setB03DurMmNull(String b03DurMmNull) {
        writeString(Pos.B03_DUR_MM_NULL, b03DurMmNull, Len.B03_DUR_MM_NULL);
    }

    /**Original name: B03-DUR-MM-NULL<br>*/
    public String getB03DurMmNull() {
        return readString(Pos.B03_DUR_MM_NULL, Len.B03_DUR_MM_NULL);
    }

    public String getB03DurMmNullFormatted() {
        return Functions.padBlanks(getB03DurMmNull(), Len.B03_DUR_MM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_DUR_MM = 1;
        public static final int B03_DUR_MM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_DUR_MM = 3;
        public static final int B03_DUR_MM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_DUR_MM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
