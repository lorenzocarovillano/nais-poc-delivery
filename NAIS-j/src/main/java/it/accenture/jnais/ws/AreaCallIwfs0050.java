package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.Types;
import com.modernsystems.ctu.core.SerializableParameter;
import it.accenture.jnais.copy.Iwfo0051;

/**Original name: AREA-CALL<br>
 * Variable: AREA-CALL from program IWFS0050<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class AreaCallIwfs0050 extends SerializableParameter {

    //==== PROPERTIES ====
    public static final int IWFI0051_CAMPO_INPUT_MAXOCCURS = 20;
    //Original name: IWFI0051-CAMPO-INPUT
    private char[] iwfi0051CampoInput = new char[IWFI0051_CAMPO_INPUT_MAXOCCURS];
    //Original name: IWFO0051
    private Iwfo0051 iwfo0051 = new Iwfo0051();

    //==== CONSTRUCTORS ====
    public AreaCallIwfs0050() {
        init();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.AREA_CALL;
    }

    @Override
    public void deserialize(byte[] buf) {
        setAreaCallBytes(buf);
    }

    public void init() {
        for (int iwfi0051CampoInputIdx = 1; iwfi0051CampoInputIdx <= IWFI0051_CAMPO_INPUT_MAXOCCURS; iwfi0051CampoInputIdx++) {
            setIwfi0051CampoInput(iwfi0051CampoInputIdx, DefaultValues.CHAR_VAL);
        }
    }

    public String getAreaCallIwfs0050Formatted() {
        return MarshalByteExt.bufferToStr(getAreaCallBytes());
    }

    public void setAreaCallBytes(byte[] buffer) {
        setAreaCallBytes(buffer, 1);
    }

    public byte[] getAreaCallBytes() {
        byte[] buffer = new byte[Len.AREA_CALL];
        return getAreaCallBytes(buffer, 1);
    }

    public void setAreaCallBytes(byte[] buffer, int offset) {
        int position = offset;
        setIwfi0051ZonaDatiBytes(buffer, position);
        position += Len.IWFI0051_ZONA_DATI;
        iwfo0051.setZonaDatiBytes(buffer, position);
    }

    public byte[] getAreaCallBytes(byte[] buffer, int offset) {
        int position = offset;
        getIwfi0051ZonaDatiBytes(buffer, position);
        position += Len.IWFI0051_ZONA_DATI;
        iwfo0051.getZonaDatiBytes(buffer, position);
        return buffer;
    }

    public void initAreaCallIwfs0050HighValues() {
        initIwfi0051ZonaDatiHighValues();
        iwfo0051.initIwfo0051HighValues();
    }

    public void setIwfi0051ZonaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        setIwfi0051ArrayStringaInputBytes(buffer, position);
    }

    public byte[] getIwfi0051ZonaDatiBytes(byte[] buffer, int offset) {
        int position = offset;
        getIwfi0051ArrayStringaInputBytes(buffer, position);
        return buffer;
    }

    public void initIwfi0051ZonaDatiHighValues() {
        initIwfi0051ArrayStringaInputHighValues();
    }

    public void setIwfi0051ArrayStringaInputFormatted(String data) {
        byte[] buffer = new byte[Len.IWFI0051_ARRAY_STRINGA_INPUT];
        MarshalByte.writeString(buffer, 1, data, Len.IWFI0051_ARRAY_STRINGA_INPUT);
        setIwfi0051ArrayStringaInputBytes(buffer, 1);
    }

    public String getIwfi0051ArrayStringaInputFormatted() {
        return MarshalByteExt.bufferToStr(getIwfi0051ArrayStringaInputBytes());
    }

    /**Original name: IWFI0051-ARRAY-STRINGA-INPUT<br>*/
    public byte[] getIwfi0051ArrayStringaInputBytes() {
        byte[] buffer = new byte[Len.IWFI0051_ARRAY_STRINGA_INPUT];
        return getIwfi0051ArrayStringaInputBytes(buffer, 1);
    }

    public void setIwfi0051ArrayStringaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= IWFI0051_CAMPO_INPUT_MAXOCCURS; idx++) {
            if (position <= buffer.length) {
                setIwfi0051CampoInput(idx, MarshalByte.readChar(buffer, position));
                position += Types.CHAR_SIZE;
            }
            else {
                setIwfi0051CampoInput(idx, Types.SPACE_CHAR);
                position += Types.CHAR_SIZE;
            }
        }
    }

    public byte[] getIwfi0051ArrayStringaInputBytes(byte[] buffer, int offset) {
        int position = offset;
        for (int idx = 1; idx <= IWFI0051_CAMPO_INPUT_MAXOCCURS; idx++) {
            MarshalByte.writeChar(buffer, position, getIwfi0051CampoInput(idx));
            position += Types.CHAR_SIZE;
        }
        return buffer;
    }

    public void initIwfi0051ArrayStringaInputHighValues() {
        for (int idx = 1; idx <= IWFI0051_CAMPO_INPUT_MAXOCCURS; idx++) {
            iwfi0051CampoInput[idx - 1] = (Types.HIGH_CHAR_VAL);
        }
    }

    public void setIwfi0051CampoInput(int iwfi0051CampoInputIdx, char iwfi0051CampoInput) {
        this.iwfi0051CampoInput[iwfi0051CampoInputIdx - 1] = iwfi0051CampoInput;
    }

    public char getIwfi0051CampoInput(int iwfi0051CampoInputIdx) {
        return this.iwfi0051CampoInput[iwfi0051CampoInputIdx - 1];
    }

    public Iwfo0051 getIwfo0051() {
        return iwfo0051;
    }

    @Override
    public byte[] serialize() {
        return getAreaCallBytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int IWFI0051_CAMPO_INPUT = 1;
        public static final int IWFI0051_ARRAY_STRINGA_INPUT = AreaCallIwfs0050.IWFI0051_CAMPO_INPUT_MAXOCCURS * IWFI0051_CAMPO_INPUT;
        public static final int IWFI0051_ZONA_DATI = IWFI0051_ARRAY_STRINGA_INPUT;
        public static final int AREA_CALL = IWFI0051_ZONA_DATI + Iwfo0051.Len.ZONA_DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
