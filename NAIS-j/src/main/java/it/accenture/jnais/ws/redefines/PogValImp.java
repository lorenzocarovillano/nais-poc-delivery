package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: POG-VAL-IMP<br>
 * Variable: POG-VAL-IMP from program LDBS1130<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PogValImp extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PogValImp() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.POG_VAL_IMP;
    }

    public void setPogValImp(AfDecimal pogValImp) {
        writeDecimalAsPacked(Pos.POG_VAL_IMP, pogValImp.copy());
    }

    public void setPogValImpFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.POG_VAL_IMP, Pos.POG_VAL_IMP);
    }

    /**Original name: POG-VAL-IMP<br>*/
    public AfDecimal getPogValImp() {
        return readPackedAsDecimal(Pos.POG_VAL_IMP, Len.Int.POG_VAL_IMP, Len.Fract.POG_VAL_IMP);
    }

    public byte[] getPogValImpAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.POG_VAL_IMP, Pos.POG_VAL_IMP);
        return buffer;
    }

    public void setPogValImpNull(String pogValImpNull) {
        writeString(Pos.POG_VAL_IMP_NULL, pogValImpNull, Len.POG_VAL_IMP_NULL);
    }

    /**Original name: POG-VAL-IMP-NULL<br>*/
    public String getPogValImpNull() {
        return readString(Pos.POG_VAL_IMP_NULL, Len.POG_VAL_IMP_NULL);
    }

    public String getPogValImpNullFormatted() {
        return Functions.padBlanks(getPogValImpNull(), Len.POG_VAL_IMP_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int POG_VAL_IMP = 1;
        public static final int POG_VAL_IMP_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int POG_VAL_IMP = 8;
        public static final int POG_VAL_IMP_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int POG_VAL_IMP = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int POG_VAL_IMP = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
