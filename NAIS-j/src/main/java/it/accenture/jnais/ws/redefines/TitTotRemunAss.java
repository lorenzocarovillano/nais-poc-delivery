package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-REMUN-ASS<br>
 * Variable: TIT-TOT-REMUN-ASS from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotRemunAss extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotRemunAss() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_REMUN_ASS;
    }

    public void setTitTotRemunAss(AfDecimal titTotRemunAss) {
        writeDecimalAsPacked(Pos.TIT_TOT_REMUN_ASS, titTotRemunAss.copy());
    }

    public void setTitTotRemunAssFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_REMUN_ASS, Pos.TIT_TOT_REMUN_ASS);
    }

    /**Original name: TIT-TOT-REMUN-ASS<br>*/
    public AfDecimal getTitTotRemunAss() {
        return readPackedAsDecimal(Pos.TIT_TOT_REMUN_ASS, Len.Int.TIT_TOT_REMUN_ASS, Len.Fract.TIT_TOT_REMUN_ASS);
    }

    public byte[] getTitTotRemunAssAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_REMUN_ASS, Pos.TIT_TOT_REMUN_ASS);
        return buffer;
    }

    public void setTitTotRemunAssNull(String titTotRemunAssNull) {
        writeString(Pos.TIT_TOT_REMUN_ASS_NULL, titTotRemunAssNull, Len.TIT_TOT_REMUN_ASS_NULL);
    }

    /**Original name: TIT-TOT-REMUN-ASS-NULL<br>*/
    public String getTitTotRemunAssNull() {
        return readString(Pos.TIT_TOT_REMUN_ASS_NULL, Len.TIT_TOT_REMUN_ASS_NULL);
    }

    public String getTitTotRemunAssNullFormatted() {
        return Functions.padBlanks(getTitTotRemunAssNull(), Len.TIT_TOT_REMUN_ASS_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_REMUN_ASS = 1;
        public static final int TIT_TOT_REMUN_ASS_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_REMUN_ASS = 8;
        public static final int TIT_TOT_REMUN_ASS_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_REMUN_ASS = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_REMUN_ASS = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
