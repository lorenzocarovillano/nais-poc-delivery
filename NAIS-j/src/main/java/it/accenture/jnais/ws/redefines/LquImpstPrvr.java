package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-PRVR<br>
 * Variable: LQU-IMPST-PRVR from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstPrvr extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstPrvr() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_PRVR;
    }

    public void setLquImpstPrvr(AfDecimal lquImpstPrvr) {
        writeDecimalAsPacked(Pos.LQU_IMPST_PRVR, lquImpstPrvr.copy());
    }

    public void setLquImpstPrvrFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_PRVR, Pos.LQU_IMPST_PRVR);
    }

    /**Original name: LQU-IMPST-PRVR<br>*/
    public AfDecimal getLquImpstPrvr() {
        return readPackedAsDecimal(Pos.LQU_IMPST_PRVR, Len.Int.LQU_IMPST_PRVR, Len.Fract.LQU_IMPST_PRVR);
    }

    public byte[] getLquImpstPrvrAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_PRVR, Pos.LQU_IMPST_PRVR);
        return buffer;
    }

    public void setLquImpstPrvrNull(String lquImpstPrvrNull) {
        writeString(Pos.LQU_IMPST_PRVR_NULL, lquImpstPrvrNull, Len.LQU_IMPST_PRVR_NULL);
    }

    /**Original name: LQU-IMPST-PRVR-NULL<br>*/
    public String getLquImpstPrvrNull() {
        return readString(Pos.LQU_IMPST_PRVR_NULL, Len.LQU_IMPST_PRVR_NULL);
    }

    public String getLquImpstPrvrNullFormatted() {
        return Functions.padBlanks(getLquImpstPrvrNull(), Len.LQU_IMPST_PRVR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_PRVR = 1;
        public static final int LQU_IMPST_PRVR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_PRVR = 8;
        public static final int LQU_IMPST_PRVR_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_PRVR = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_PRVR = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
