package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WOCO-IMP-TRASF<br>
 * Variable: WOCO-IMP-TRASF from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WocoImpTrasf extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WocoImpTrasf() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WOCO_IMP_TRASF;
    }

    public void setWocoImpTrasf(AfDecimal wocoImpTrasf) {
        writeDecimalAsPacked(Pos.WOCO_IMP_TRASF, wocoImpTrasf.copy());
    }

    public void setWocoImpTrasfFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WOCO_IMP_TRASF, Pos.WOCO_IMP_TRASF);
    }

    /**Original name: WOCO-IMP-TRASF<br>*/
    public AfDecimal getWocoImpTrasf() {
        return readPackedAsDecimal(Pos.WOCO_IMP_TRASF, Len.Int.WOCO_IMP_TRASF, Len.Fract.WOCO_IMP_TRASF);
    }

    public byte[] getWocoImpTrasfAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WOCO_IMP_TRASF, Pos.WOCO_IMP_TRASF);
        return buffer;
    }

    public void initWocoImpTrasfSpaces() {
        fill(Pos.WOCO_IMP_TRASF, Len.WOCO_IMP_TRASF, Types.SPACE_CHAR);
    }

    public void setWocoImpTrasfNull(String wocoImpTrasfNull) {
        writeString(Pos.WOCO_IMP_TRASF_NULL, wocoImpTrasfNull, Len.WOCO_IMP_TRASF_NULL);
    }

    /**Original name: WOCO-IMP-TRASF-NULL<br>*/
    public String getWocoImpTrasfNull() {
        return readString(Pos.WOCO_IMP_TRASF_NULL, Len.WOCO_IMP_TRASF_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_TRASF = 1;
        public static final int WOCO_IMP_TRASF_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WOCO_IMP_TRASF = 8;
        public static final int WOCO_IMP_TRASF_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_TRASF = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WOCO_IMP_TRASF = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
