package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CPT-RIASTO-ECC<br>
 * Variable: W-B03-CPT-RIASTO-ECC from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CptRiastoEccLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CptRiastoEccLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CPT_RIASTO_ECC;
    }

    public void setwB03CptRiastoEcc(AfDecimal wB03CptRiastoEcc) {
        writeDecimalAsPacked(Pos.W_B03_CPT_RIASTO_ECC, wB03CptRiastoEcc.copy());
    }

    /**Original name: W-B03-CPT-RIASTO-ECC<br>*/
    public AfDecimal getwB03CptRiastoEcc() {
        return readPackedAsDecimal(Pos.W_B03_CPT_RIASTO_ECC, Len.Int.W_B03_CPT_RIASTO_ECC, Len.Fract.W_B03_CPT_RIASTO_ECC);
    }

    public byte[] getwB03CptRiastoEccAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CPT_RIASTO_ECC, Pos.W_B03_CPT_RIASTO_ECC);
        return buffer;
    }

    public void setwB03CptRiastoEccNull(String wB03CptRiastoEccNull) {
        writeString(Pos.W_B03_CPT_RIASTO_ECC_NULL, wB03CptRiastoEccNull, Len.W_B03_CPT_RIASTO_ECC_NULL);
    }

    /**Original name: W-B03-CPT-RIASTO-ECC-NULL<br>*/
    public String getwB03CptRiastoEccNull() {
        return readString(Pos.W_B03_CPT_RIASTO_ECC_NULL, Len.W_B03_CPT_RIASTO_ECC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_RIASTO_ECC = 1;
        public static final int W_B03_CPT_RIASTO_ECC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_RIASTO_ECC = 8;
        public static final int W_B03_CPT_RIASTO_ECC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_RIASTO_ECC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_RIASTO_ECC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
