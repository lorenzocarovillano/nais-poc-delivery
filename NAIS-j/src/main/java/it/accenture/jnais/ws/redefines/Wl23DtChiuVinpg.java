package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WL23-DT-CHIU-VINPG<br>
 * Variable: WL23-DT-CHIU-VINPG from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wl23DtChiuVinpg extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wl23DtChiuVinpg() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WL23_DT_CHIU_VINPG;
    }

    public void setWl23DtChiuVinpg(int wl23DtChiuVinpg) {
        writeIntAsPacked(Pos.WL23_DT_CHIU_VINPG, wl23DtChiuVinpg, Len.Int.WL23_DT_CHIU_VINPG);
    }

    public void setWl23DtChiuVinpgFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WL23_DT_CHIU_VINPG, Pos.WL23_DT_CHIU_VINPG);
    }

    /**Original name: WL23-DT-CHIU-VINPG<br>*/
    public int getWl23DtChiuVinpg() {
        return readPackedAsInt(Pos.WL23_DT_CHIU_VINPG, Len.Int.WL23_DT_CHIU_VINPG);
    }

    public byte[] getWl23DtChiuVinpgAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WL23_DT_CHIU_VINPG, Pos.WL23_DT_CHIU_VINPG);
        return buffer;
    }

    public void initWl23DtChiuVinpgSpaces() {
        fill(Pos.WL23_DT_CHIU_VINPG, Len.WL23_DT_CHIU_VINPG, Types.SPACE_CHAR);
    }

    public void setWl23DtChiuVinpgNull(String wl23DtChiuVinpgNull) {
        writeString(Pos.WL23_DT_CHIU_VINPG_NULL, wl23DtChiuVinpgNull, Len.WL23_DT_CHIU_VINPG_NULL);
    }

    /**Original name: WL23-DT-CHIU-VINPG-NULL<br>*/
    public String getWl23DtChiuVinpgNull() {
        return readString(Pos.WL23_DT_CHIU_VINPG_NULL, Len.WL23_DT_CHIU_VINPG_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WL23_DT_CHIU_VINPG = 1;
        public static final int WL23_DT_CHIU_VINPG_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WL23_DT_CHIU_VINPG = 5;
        public static final int WL23_DT_CHIU_VINPG_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WL23_DT_CHIU_VINPG = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
