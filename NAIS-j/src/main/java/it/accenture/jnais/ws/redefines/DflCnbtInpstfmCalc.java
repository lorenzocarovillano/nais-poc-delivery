package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-CNBT-INPSTFM-CALC<br>
 * Variable: DFL-CNBT-INPSTFM-CALC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflCnbtInpstfmCalc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflCnbtInpstfmCalc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_CNBT_INPSTFM_CALC;
    }

    public void setDflCnbtInpstfmCalc(AfDecimal dflCnbtInpstfmCalc) {
        writeDecimalAsPacked(Pos.DFL_CNBT_INPSTFM_CALC, dflCnbtInpstfmCalc.copy());
    }

    public void setDflCnbtInpstfmCalcFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_CNBT_INPSTFM_CALC, Pos.DFL_CNBT_INPSTFM_CALC);
    }

    /**Original name: DFL-CNBT-INPSTFM-CALC<br>*/
    public AfDecimal getDflCnbtInpstfmCalc() {
        return readPackedAsDecimal(Pos.DFL_CNBT_INPSTFM_CALC, Len.Int.DFL_CNBT_INPSTFM_CALC, Len.Fract.DFL_CNBT_INPSTFM_CALC);
    }

    public byte[] getDflCnbtInpstfmCalcAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_CNBT_INPSTFM_CALC, Pos.DFL_CNBT_INPSTFM_CALC);
        return buffer;
    }

    public void setDflCnbtInpstfmCalcNull(String dflCnbtInpstfmCalcNull) {
        writeString(Pos.DFL_CNBT_INPSTFM_CALC_NULL, dflCnbtInpstfmCalcNull, Len.DFL_CNBT_INPSTFM_CALC_NULL);
    }

    /**Original name: DFL-CNBT-INPSTFM-CALC-NULL<br>*/
    public String getDflCnbtInpstfmCalcNull() {
        return readString(Pos.DFL_CNBT_INPSTFM_CALC_NULL, Len.DFL_CNBT_INPSTFM_CALC_NULL);
    }

    public String getDflCnbtInpstfmCalcNullFormatted() {
        return Functions.padBlanks(getDflCnbtInpstfmCalcNull(), Len.DFL_CNBT_INPSTFM_CALC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_CNBT_INPSTFM_CALC = 1;
        public static final int DFL_CNBT_INPSTFM_CALC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_CNBT_INPSTFM_CALC = 8;
        public static final int DFL_CNBT_INPSTFM_CALC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_CNBT_INPSTFM_CALC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_CNBT_INPSTFM_CALC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
