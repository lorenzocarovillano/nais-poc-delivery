package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-OVER-COMM<br>
 * Variable: W-B03-OVER-COMM from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03OverCommLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03OverCommLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_OVER_COMM;
    }

    public void setwB03OverComm(AfDecimal wB03OverComm) {
        writeDecimalAsPacked(Pos.W_B03_OVER_COMM, wB03OverComm.copy());
    }

    /**Original name: W-B03-OVER-COMM<br>*/
    public AfDecimal getwB03OverComm() {
        return readPackedAsDecimal(Pos.W_B03_OVER_COMM, Len.Int.W_B03_OVER_COMM, Len.Fract.W_B03_OVER_COMM);
    }

    public byte[] getwB03OverCommAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_OVER_COMM, Pos.W_B03_OVER_COMM);
        return buffer;
    }

    public void setwB03OverCommNull(String wB03OverCommNull) {
        writeString(Pos.W_B03_OVER_COMM_NULL, wB03OverCommNull, Len.W_B03_OVER_COMM_NULL);
    }

    /**Original name: W-B03-OVER-COMM-NULL<br>*/
    public String getwB03OverCommNull() {
        return readString(Pos.W_B03_OVER_COMM_NULL, Len.W_B03_OVER_COMM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_OVER_COMM = 1;
        public static final int W_B03_OVER_COMM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_OVER_COMM = 8;
        public static final int W_B03_OVER_COMM_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_OVER_COMM = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_OVER_COMM = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
