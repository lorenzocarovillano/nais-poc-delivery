package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: LCCC0023-TP-MOVI<br>
 * Variable: LCCC0023-TP-MOVI from program LCCS0023<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Lccc0023TpMovi extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Lccc0023TpMovi() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LCCC0023_TP_MOVI;
    }

    public void setLccc0023TpMovi(int lccc0023TpMovi) {
        writeIntAsPacked(Pos.LCCC0023_TP_MOVI, lccc0023TpMovi, Len.Int.LCCC0023_TP_MOVI);
    }

    public void setLccc0023TpMoviFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LCCC0023_TP_MOVI, Pos.LCCC0023_TP_MOVI);
    }

    /**Original name: LCCC0023-TP-MOVI<br>*/
    public int getLccc0023TpMovi() {
        return readPackedAsInt(Pos.LCCC0023_TP_MOVI, Len.Int.LCCC0023_TP_MOVI);
    }

    public byte[] getLccc0023TpMoviAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LCCC0023_TP_MOVI, Pos.LCCC0023_TP_MOVI);
        return buffer;
    }

    public void initLccc0023TpMoviSpaces() {
        fill(Pos.LCCC0023_TP_MOVI, Len.LCCC0023_TP_MOVI, Types.SPACE_CHAR);
    }

    public void setLccc0023TpMoviNull(String lccc0023TpMoviNull) {
        writeString(Pos.LCCC0023_TP_MOVI_NULL, lccc0023TpMoviNull, Len.LCCC0023_TP_MOVI_NULL);
    }

    /**Original name: LCCC0023-TP-MOVI-NULL<br>*/
    public String getLccc0023TpMoviNull() {
        return readString(Pos.LCCC0023_TP_MOVI_NULL, Len.LCCC0023_TP_MOVI_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LCCC0023_TP_MOVI = 1;
        public static final int LCCC0023_TP_MOVI_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LCCC0023_TP_MOVI = 3;
        public static final int LCCC0023_TP_MOVI_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LCCC0023_TP_MOVI = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
