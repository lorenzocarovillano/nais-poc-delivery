package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.copy.Ivvc0218Ivvs0211;
import it.accenture.jnais.copy.Lccvade1Lvvs0010;
import it.accenture.jnais.copy.Lccvgrz1Lvvs0037;
import it.accenture.jnais.copy.Lccvpol1Lvvs0002;
import it.accenture.jnais.copy.Lccvtga1Lvvs0037;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.copy.WgrzDati;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.copy.WtgaDati;
import it.accenture.jnais.ws.enums.FlagComunTrov;
import it.accenture.jnais.ws.enums.FlagCurMov;
import it.accenture.jnais.ws.enums.FlagUltimaLettura;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimentoLvvs0037;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0037<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0037Data {

    //==== PROPERTIES ====
    /**Original name: WK-PGM<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkPgm = "LVVS0037";
    //Original name: WK-CALL-PGM
    private String wkCallPgm = "";
    //Original name: LDBS6040
    private String ldbs6040 = "LDBS6040";
    /**Original name: WK-DATA-OUTPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VARIABILI
	 * ----------------------------------------------------------------*</pre>*/
    private AfDecimal wkDataOutput = new AfDecimal(DefaultValues.DEC_VAL, 11, 7);
    //Original name: WK-DATA-X-12
    private WkDataX12 wkDataX12 = new WkDataX12();
    //Original name: WS-MOVIMENTO-ORIG
    private String wsMovimentoOrig = "00000";
    /**Original name: WS-MOVIMENTO<br>
	 * <pre>---------------------------------------------------------------*
	 *   LISTA TABELLE E TIPOLOGICHE
	 * ---------------------------------------------------------------*
	 * *****************************************************************
	 *     TP_MOVI (Tipi Movimenti)
	 * *****************************************************************</pre>*/
    private WsMovimentoLvvs0037 wsMovimento = new WsMovimentoLvvs0037();
    /**Original name: FLAG-CUR-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     FLAG
	 * ----------------------------------------------------------------*</pre>*/
    private FlagCurMov flagCurMov = new FlagCurMov();
    //Original name: FLAG-COMUN-TROV
    private FlagComunTrov flagComunTrov = new FlagComunTrov();
    //Original name: FLAG-ULTIMA-LETTURA
    private FlagUltimaLettura flagUltimaLettura = new FlagUltimaLettura();
    //Original name: WK-VAR-MOVI-COMUN
    private WkVarMoviComun wkVarMoviComun = new WkVarMoviComun();
    //Original name: MOVI
    private MoviLdbs1530 movi = new MoviLdbs1530();
    //Original name: LDBV1701
    private Ldbv1701 ldbv1701 = new Ldbv1701();
    //Original name: DPOL-ELE-POL-MAX
    private short dpolElePolMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVPOL1
    private Lccvpol1Lvvs0002 lccvpol1 = new Lccvpol1Lvvs0002();
    //Original name: DADE-ELE-ADES-MAX
    private short dadeEleAdesMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVADE1
    private Lccvade1Lvvs0010 lccvade1 = new Lccvade1Lvvs0010();
    //Original name: DGRZ-ELE-GAR-MAX
    private short dgrzEleGarMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVGRZ1
    private Lccvgrz1Lvvs0037 lccvgrz1 = new Lccvgrz1Lvvs0037();
    //Original name: DTGA-ELE-TGA-MAX
    private short dtgaEleTgaMax = DefaultValues.BIN_SHORT_VAL;
    //Original name: LCCVTGA1
    private Lccvtga1Lvvs0037 lccvtga1 = new Lccvtga1Lvvs0037();
    //Original name: IVVC0218
    private Ivvc0218Ivvs0211 ivvc0218 = new Ivvc0218Ivvs0211();
    //Original name: IX-DCLGEN
    private short ixDclgen = DefaultValues.BIN_SHORT_VAL;

    //==== METHODS ====
    public String getWkPgm() {
        return this.wkPgm;
    }

    public void setWkCallPgm(String wkCallPgm) {
        this.wkCallPgm = Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
    }

    public String getWkCallPgm() {
        return this.wkCallPgm;
    }

    public String getLdbs6040() {
        return this.ldbs6040;
    }

    public void setWkDataOutput(AfDecimal wkDataOutput) {
        this.wkDataOutput.assign(wkDataOutput);
    }

    public AfDecimal getWkDataOutput() {
        return this.wkDataOutput.copy();
    }

    public void setWsMovimentoOrigFormatted(String wsMovimentoOrig) {
        this.wsMovimentoOrig = Trunc.toUnsignedNumeric(wsMovimentoOrig, Len.WS_MOVIMENTO_ORIG);
    }

    public int getWsMovimentoOrig() {
        return NumericDisplay.asInt(this.wsMovimentoOrig);
    }

    public String getWsMovimentoOrigFormatted() {
        return this.wsMovimentoOrig;
    }

    public void setDpolAreaPolFormatted(String data) {
        byte[] buffer = new byte[Len.DPOL_AREA_POL];
        MarshalByte.writeString(buffer, 1, data, Len.DPOL_AREA_POL);
        setDpolAreaPolBytes(buffer, 1);
    }

    public void setDpolAreaPolBytes(byte[] buffer, int offset) {
        int position = offset;
        dpolElePolMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDpolTabPolBytes(buffer, position);
    }

    public void setDpolElePolMax(short dpolElePolMax) {
        this.dpolElePolMax = dpolElePolMax;
    }

    public short getDpolElePolMax() {
        return this.dpolElePolMax;
    }

    public void setDpolTabPolBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpol1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpol1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpol1Lvvs0002.Len.Int.ID_PTF, 0));
        position += Lccvpol1Lvvs0002.Len.ID_PTF;
        lccvpol1.getDati().setDatiBytes(buffer, position);
    }

    public void setDadeAreaAdesFormatted(String data) {
        byte[] buffer = new byte[Len.DADE_AREA_ADES];
        MarshalByte.writeString(buffer, 1, data, Len.DADE_AREA_ADES);
        setDadeAreaAdesBytes(buffer, 1);
    }

    public void setDadeAreaAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        dadeEleAdesMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDadeTabAdesBytes(buffer, position);
    }

    public void setDadeEleAdesMax(short dadeEleAdesMax) {
        this.dadeEleAdesMax = dadeEleAdesMax;
    }

    public short getDadeEleAdesMax() {
        return this.dadeEleAdesMax;
    }

    public void setDadeTabAdesBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvade1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvade1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvade1Lvvs0010.Len.Int.ID_PTF, 0));
        position += Lccvade1Lvvs0010.Len.ID_PTF;
        lccvade1.getDati().setDatiBytes(buffer, position);
    }

    public void setDgrzAreaGarFormatted(String data) {
        byte[] buffer = new byte[Len.DGRZ_AREA_GAR];
        MarshalByte.writeString(buffer, 1, data, Len.DGRZ_AREA_GAR);
        setDgrzAreaGarBytes(buffer, 1);
    }

    public void setDgrzAreaGarBytes(byte[] buffer, int offset) {
        int position = offset;
        dgrzEleGarMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDgrzTabGarBytes(buffer, position);
    }

    public void setDgrzEleGarMax(short dgrzEleGarMax) {
        this.dgrzEleGarMax = dgrzEleGarMax;
    }

    public short getDgrzEleGarMax() {
        return this.dgrzEleGarMax;
    }

    public void setDgrzTabGarBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvgrz1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvgrz1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvgrz1Lvvs0037.Len.Int.ID_PTF, 0));
        position += Lccvgrz1Lvvs0037.Len.ID_PTF;
        lccvgrz1.getDati().setDatiBytes(buffer, position);
    }

    public void setDtgaAreaTgaFormatted(String data) {
        byte[] buffer = new byte[Len.DTGA_AREA_TGA];
        MarshalByte.writeString(buffer, 1, data, Len.DTGA_AREA_TGA);
        setDtgaAreaTgaBytes(buffer, 1);
    }

    public void setDtgaAreaTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        dtgaEleTgaMax = MarshalByte.readBinaryShort(buffer, position);
        position += Types.SHORT_SIZE;
        setDtgaTabTgaBytes(buffer, position);
    }

    public void setDtgaEleTgaMax(short dtgaEleTgaMax) {
        this.dtgaEleTgaMax = dtgaEleTgaMax;
    }

    public short getDtgaEleTgaMax() {
        return this.dtgaEleTgaMax;
    }

    public void setDtgaTabTgaBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvtga1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvtga1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvtga1Lvvs0037.Len.Int.ID_PTF, 0));
        position += Lccvtga1Lvvs0037.Len.ID_PTF;
        lccvtga1.getDati().setDatiBytes(buffer, position);
    }

    public void setIxDclgen(short ixDclgen) {
        this.ixDclgen = ixDclgen;
    }

    public short getIxDclgen() {
        return this.ixDclgen;
    }

    public FlagComunTrov getFlagComunTrov() {
        return flagComunTrov;
    }

    public FlagCurMov getFlagCurMov() {
        return flagCurMov;
    }

    public FlagUltimaLettura getFlagUltimaLettura() {
        return flagUltimaLettura;
    }

    public Ivvc0218Ivvs0211 getIvvc0218() {
        return ivvc0218;
    }

    public Lccvade1Lvvs0010 getLccvade1() {
        return lccvade1;
    }

    public Lccvgrz1Lvvs0037 getLccvgrz1() {
        return lccvgrz1;
    }

    public Lccvpol1Lvvs0002 getLccvpol1() {
        return lccvpol1;
    }

    public Lccvtga1Lvvs0037 getLccvtga1() {
        return lccvtga1;
    }

    public Ldbv1701 getLdbv1701() {
        return ldbv1701;
    }

    public MoviLdbs1530 getMovi() {
        return movi;
    }

    public WkDataX12 getWkDataX12() {
        return wkDataX12;
    }

    public WkVarMoviComun getWkVarMoviComun() {
        return wkVarMoviComun;
    }

    public WsMovimentoLvvs0037 getWsMovimento() {
        return wsMovimento;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_MOVIMENTO_ORIG = 5;
        public static final int DPOL_ELE_POL_MAX = 2;
        public static final int DPOL_TAB_POL = WpolStatus.Len.STATUS + Lccvpol1Lvvs0002.Len.ID_PTF + WpolDati.Len.DATI;
        public static final int DPOL_AREA_POL = DPOL_ELE_POL_MAX + DPOL_TAB_POL;
        public static final int DADE_ELE_ADES_MAX = 2;
        public static final int DADE_TAB_ADES = WpolStatus.Len.STATUS + Lccvade1Lvvs0010.Len.ID_PTF + WadeDati.Len.DATI;
        public static final int DADE_AREA_ADES = DADE_ELE_ADES_MAX + DADE_TAB_ADES;
        public static final int DGRZ_ELE_GAR_MAX = 2;
        public static final int DGRZ_TAB_GAR = WpolStatus.Len.STATUS + Lccvgrz1Lvvs0037.Len.ID_PTF + WgrzDati.Len.DATI;
        public static final int DGRZ_AREA_GAR = DGRZ_ELE_GAR_MAX + DGRZ_TAB_GAR;
        public static final int DTGA_ELE_TGA_MAX = 2;
        public static final int DTGA_TAB_TGA = WpolStatus.Len.STATUS + Lccvtga1Lvvs0037.Len.ID_PTF + WtgaDati.Len.DATI;
        public static final int DTGA_AREA_TGA = DTGA_ELE_TGA_MAX + DTGA_TAB_TGA;
        public static final int WK_CALL_PGM = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
