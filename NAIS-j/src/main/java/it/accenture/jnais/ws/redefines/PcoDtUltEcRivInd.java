package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: PCO-DT-ULT-EC-RIV-IND<br>
 * Variable: PCO-DT-ULT-EC-RIV-IND from program IDSS0150<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PcoDtUltEcRivInd extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PcoDtUltEcRivInd() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PCO_DT_ULT_EC_RIV_IND;
    }

    public void setPcoDtUltEcRivInd(int pcoDtUltEcRivInd) {
        writeIntAsPacked(Pos.PCO_DT_ULT_EC_RIV_IND, pcoDtUltEcRivInd, Len.Int.PCO_DT_ULT_EC_RIV_IND);
    }

    public void setPcoDtUltEcRivIndFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PCO_DT_ULT_EC_RIV_IND, Pos.PCO_DT_ULT_EC_RIV_IND);
    }

    /**Original name: PCO-DT-ULT-EC-RIV-IND<br>*/
    public int getPcoDtUltEcRivInd() {
        return readPackedAsInt(Pos.PCO_DT_ULT_EC_RIV_IND, Len.Int.PCO_DT_ULT_EC_RIV_IND);
    }

    public byte[] getPcoDtUltEcRivIndAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PCO_DT_ULT_EC_RIV_IND, Pos.PCO_DT_ULT_EC_RIV_IND);
        return buffer;
    }

    public void initPcoDtUltEcRivIndHighValues() {
        fill(Pos.PCO_DT_ULT_EC_RIV_IND, Len.PCO_DT_ULT_EC_RIV_IND, Types.HIGH_CHAR_VAL);
    }

    public void setPcoDtUltEcRivIndNull(String pcoDtUltEcRivIndNull) {
        writeString(Pos.PCO_DT_ULT_EC_RIV_IND_NULL, pcoDtUltEcRivIndNull, Len.PCO_DT_ULT_EC_RIV_IND_NULL);
    }

    /**Original name: PCO-DT-ULT-EC-RIV-IND-NULL<br>*/
    public String getPcoDtUltEcRivIndNull() {
        return readString(Pos.PCO_DT_ULT_EC_RIV_IND_NULL, Len.PCO_DT_ULT_EC_RIV_IND_NULL);
    }

    public String getPcoDtUltEcRivIndNullFormatted() {
        return Functions.padBlanks(getPcoDtUltEcRivIndNull(), Len.PCO_DT_ULT_EC_RIV_IND_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_RIV_IND = 1;
        public static final int PCO_DT_ULT_EC_RIV_IND_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PCO_DT_ULT_EC_RIV_IND = 5;
        public static final int PCO_DT_ULT_EC_RIV_IND_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PCO_DT_ULT_EC_RIV_IND = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
