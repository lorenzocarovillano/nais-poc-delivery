package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WB03-ABB<br>
 * Variable: WB03-ABB from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Wb03Abb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public Wb03Abb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WB03_ABB;
    }

    public void setWb03AbbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WB03_ABB, Pos.WB03_ABB);
    }

    /**Original name: WB03-ABB<br>*/
    public AfDecimal getWb03Abb() {
        return readPackedAsDecimal(Pos.WB03_ABB, Len.Int.WB03_ABB, Len.Fract.WB03_ABB);
    }

    public byte[] getWb03AbbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WB03_ABB, Pos.WB03_ABB);
        return buffer;
    }

    public void setWb03AbbNull(String wb03AbbNull) {
        writeString(Pos.WB03_ABB_NULL, wb03AbbNull, Len.WB03_ABB_NULL);
    }

    /**Original name: WB03-ABB-NULL<br>*/
    public String getWb03AbbNull() {
        return readString(Pos.WB03_ABB_NULL, Len.WB03_ABB_NULL);
    }

    public String getWb03AbbNullFormatted() {
        return Functions.padBlanks(getWb03AbbNull(), Len.WB03_ABB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WB03_ABB = 1;
        public static final int WB03_ABB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WB03_ABB = 8;
        public static final int WB03_ABB_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WB03_ABB = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int WB03_ABB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
