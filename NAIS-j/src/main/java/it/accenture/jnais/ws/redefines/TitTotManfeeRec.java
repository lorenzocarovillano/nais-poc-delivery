package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TIT-TOT-MANFEE-REC<br>
 * Variable: TIT-TOT-MANFEE-REC from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TitTotManfeeRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TitTotManfeeRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TIT_TOT_MANFEE_REC;
    }

    public void setTitTotManfeeRec(AfDecimal titTotManfeeRec) {
        writeDecimalAsPacked(Pos.TIT_TOT_MANFEE_REC, titTotManfeeRec.copy());
    }

    public void setTitTotManfeeRecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TIT_TOT_MANFEE_REC, Pos.TIT_TOT_MANFEE_REC);
    }

    /**Original name: TIT-TOT-MANFEE-REC<br>*/
    public AfDecimal getTitTotManfeeRec() {
        return readPackedAsDecimal(Pos.TIT_TOT_MANFEE_REC, Len.Int.TIT_TOT_MANFEE_REC, Len.Fract.TIT_TOT_MANFEE_REC);
    }

    public byte[] getTitTotManfeeRecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TIT_TOT_MANFEE_REC, Pos.TIT_TOT_MANFEE_REC);
        return buffer;
    }

    public void setTitTotManfeeRecNull(String titTotManfeeRecNull) {
        writeString(Pos.TIT_TOT_MANFEE_REC_NULL, titTotManfeeRecNull, Len.TIT_TOT_MANFEE_REC_NULL);
    }

    /**Original name: TIT-TOT-MANFEE-REC-NULL<br>*/
    public String getTitTotManfeeRecNull() {
        return readString(Pos.TIT_TOT_MANFEE_REC_NULL, Len.TIT_TOT_MANFEE_REC_NULL);
    }

    public String getTitTotManfeeRecNullFormatted() {
        return Functions.padBlanks(getTitTotManfeeRecNull(), Len.TIT_TOT_MANFEE_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TIT_TOT_MANFEE_REC = 1;
        public static final int TIT_TOT_MANFEE_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TIT_TOT_MANFEE_REC = 8;
        public static final int TIT_TOT_MANFEE_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TIT_TOT_MANFEE_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int TIT_TOT_MANFEE_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
