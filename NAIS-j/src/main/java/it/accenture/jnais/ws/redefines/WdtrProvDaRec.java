package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: WDTR-PROV-DA-REC<br>
 * Variable: WDTR-PROV-DA-REC from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtrProvDaRec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtrProvDaRec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTR_PROV_DA_REC;
    }

    public void setWdtrProvDaRec(AfDecimal wdtrProvDaRec) {
        writeDecimalAsPacked(Pos.WDTR_PROV_DA_REC, wdtrProvDaRec.copy());
    }

    /**Original name: WDTR-PROV-DA-REC<br>*/
    public AfDecimal getWdtrProvDaRec() {
        return readPackedAsDecimal(Pos.WDTR_PROV_DA_REC, Len.Int.WDTR_PROV_DA_REC, Len.Fract.WDTR_PROV_DA_REC);
    }

    public void setWdtrProvDaRecNull(String wdtrProvDaRecNull) {
        writeString(Pos.WDTR_PROV_DA_REC_NULL, wdtrProvDaRecNull, Len.WDTR_PROV_DA_REC_NULL);
    }

    /**Original name: WDTR-PROV-DA-REC-NULL<br>*/
    public String getWdtrProvDaRecNull() {
        return readString(Pos.WDTR_PROV_DA_REC_NULL, Len.WDTR_PROV_DA_REC_NULL);
    }

    public String getWdtrProvDaRecNullFormatted() {
        return Functions.padBlanks(getWdtrProvDaRecNull(), Len.WDTR_PROV_DA_REC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_DA_REC = 1;
        public static final int WDTR_PROV_DA_REC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTR_PROV_DA_REC = 8;
        public static final int WDTR_PROV_DA_REC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_DA_REC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTR_PROV_DA_REC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
