package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;
import it.accenture.jnais.ws.enums.W870DatiGgincassoContesto;
import it.accenture.jnais.ws.redefines.W870TabTit;

/**Original name: W870-AREA-LOAS0870<br>
 * Variable: W870-AREA-LOAS0870 from program LOAS0870<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class W870AreaLoas0870 extends SerializableParameter {

    //==== PROPERTIES ====
    //Original name: W870-DATI-GGINCASSO-CONTESTO
    private W870DatiGgincassoContesto w870DatiGgincassoContesto = new W870DatiGgincassoContesto();
    //Original name: W870-GGINCASSO
    private String w870Ggincasso = DefaultValues.stringVal(Len.W870_GGINCASSO);
    //Original name: W870-TGA-NUM-MAX-ELE
    private short w870TgaNumMaxEle = DefaultValues.SHORT_VAL;
    //Original name: W870-TAB-TIT
    private W870TabTit w870TabTit = new W870TabTit();

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W870_AREA_LOAS0870;
    }

    @Override
    public void deserialize(byte[] buf) {
        setW870AreaLoas0870Bytes(buf);
    }

    public String getW870AreaLoas0870Formatted() {
        return getW870AreaXx0VarInpFormatted();
    }

    public void setW870AreaLoas0870Bytes(byte[] buffer) {
        setW870AreaLoas0870Bytes(buffer, 1);
    }

    public byte[] getW870AreaLoas0870Bytes() {
        byte[] buffer = new byte[Len.W870_AREA_LOAS0870];
        return getW870AreaLoas0870Bytes(buffer, 1);
    }

    public void setW870AreaLoas0870Bytes(byte[] buffer, int offset) {
        int position = offset;
        setW870AreaXx0VarInpBytes(buffer, position);
    }

    public byte[] getW870AreaLoas0870Bytes(byte[] buffer, int offset) {
        int position = offset;
        getW870AreaXx0VarInpBytes(buffer, position);
        return buffer;
    }

    public String getW870AreaXx0VarInpFormatted() {
        return MarshalByteExt.bufferToStr(getW870AreaXx0VarInpBytes());
    }

    /**Original name: W870-AREA-XX0-VAR-INP<br>
	 * <pre>----------------------------------------------------------------*
	 *    PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
	 *    AREA VARIABILI E COSTANTI DI INPUT
	 *    SERVIZIO DI RIVALUTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    public byte[] getW870AreaXx0VarInpBytes() {
        byte[] buffer = new byte[Len.W870_AREA_XX0_VAR_INP];
        return getW870AreaXx0VarInpBytes(buffer, 1);
    }

    public void setW870AreaXx0VarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        setW870DatiGgincBytes(buffer, position);
        position += Len.W870_DATI_GGINC;
        setW870AreaTgaTitBytes(buffer, position);
    }

    public byte[] getW870AreaXx0VarInpBytes(byte[] buffer, int offset) {
        int position = offset;
        getW870DatiGgincBytes(buffer, position);
        position += Len.W870_DATI_GGINC;
        getW870AreaTgaTitBytes(buffer, position);
        return buffer;
    }

    public void setW870DatiGgincBytes(byte[] buffer, int offset) {
        int position = offset;
        w870DatiGgincassoContesto.setW870DatiGgincassoContesto(MarshalByte.readString(buffer, position, W870DatiGgincassoContesto.Len.W870_DATI_GGINCASSO_CONTESTO));
        position += W870DatiGgincassoContesto.Len.W870_DATI_GGINCASSO_CONTESTO;
        w870Ggincasso = MarshalByte.readFixedString(buffer, position, Len.W870_GGINCASSO);
    }

    public byte[] getW870DatiGgincBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, w870DatiGgincassoContesto.getW870DatiGgincassoContesto(), W870DatiGgincassoContesto.Len.W870_DATI_GGINCASSO_CONTESTO);
        position += W870DatiGgincassoContesto.Len.W870_DATI_GGINCASSO_CONTESTO;
        MarshalByte.writeString(buffer, position, w870Ggincasso, Len.W870_GGINCASSO);
        return buffer;
    }

    public void setW870Ggincasso(int w870Ggincasso) {
        this.w870Ggincasso = NumericDisplay.asString(w870Ggincasso, Len.W870_GGINCASSO);
    }

    public int getW870Ggincasso() {
        return NumericDisplay.asInt(this.w870Ggincasso);
    }

    public void setW870AreaTgaTitBytes(byte[] buffer, int offset) {
        int position = offset;
        w870TgaNumMaxEle = MarshalByte.readShort(buffer, position, Len.W870_TGA_NUM_MAX_ELE);
        position += Len.W870_TGA_NUM_MAX_ELE;
        w870TabTit.setW870TabTitBytes(buffer, position);
    }

    public byte[] getW870AreaTgaTitBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeShort(buffer, position, w870TgaNumMaxEle, Len.W870_TGA_NUM_MAX_ELE);
        position += Len.W870_TGA_NUM_MAX_ELE;
        w870TabTit.getW870TabTitBytes(buffer, position);
        return buffer;
    }

    public void setW870TgaNumMaxEle(short w870TgaNumMaxEle) {
        this.w870TgaNumMaxEle = w870TgaNumMaxEle;
    }

    public short getW870TgaNumMaxEle() {
        return this.w870TgaNumMaxEle;
    }

    public W870DatiGgincassoContesto getW870DatiGgincassoContesto() {
        return w870DatiGgincassoContesto;
    }

    public W870TabTit getW870TabTit() {
        return w870TabTit;
    }

    @Override
    public byte[] serialize() {
        return getW870AreaLoas0870Bytes();
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int W870_GGINCASSO = 5;
        public static final int W870_DATI_GGINC = W870DatiGgincassoContesto.Len.W870_DATI_GGINCASSO_CONTESTO + W870_GGINCASSO;
        public static final int W870_TGA_NUM_MAX_ELE = 4;
        public static final int W870_AREA_TGA_TIT = W870_TGA_NUM_MAX_ELE + W870TabTit.Len.W870_TAB_TIT;
        public static final int W870_AREA_XX0_VAR_INP = W870_DATI_GGINC + W870_AREA_TGA_TIT;
        public static final int W870_AREA_LOAS0870 = W870_AREA_XX0_VAR_INP;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
