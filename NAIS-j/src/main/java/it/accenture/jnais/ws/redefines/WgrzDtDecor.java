package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.bphx.ctu.af.util.Functions;

/**Original name: WGRZ-DT-DECOR<br>
 * Variable: WGRZ-DT-DECOR from program LCCS0234<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WgrzDtDecor extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WgrzDtDecor() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WGRZ_DT_DECOR;
    }

    public void setWgrzDtDecor(int wgrzDtDecor) {
        writeIntAsPacked(Pos.WGRZ_DT_DECOR, wgrzDtDecor, Len.Int.WGRZ_DT_DECOR);
    }

    public void setWgrzDtDecorFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WGRZ_DT_DECOR, Pos.WGRZ_DT_DECOR);
    }

    /**Original name: WGRZ-DT-DECOR<br>*/
    public int getWgrzDtDecor() {
        return readPackedAsInt(Pos.WGRZ_DT_DECOR, Len.Int.WGRZ_DT_DECOR);
    }

    public String getWgrzDtDecorFormatted() {
        return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).format(getWgrzDtDecor()).toString();
    }

    public byte[] getWgrzDtDecorAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WGRZ_DT_DECOR, Pos.WGRZ_DT_DECOR);
        return buffer;
    }

    public void initWgrzDtDecorSpaces() {
        fill(Pos.WGRZ_DT_DECOR, Len.WGRZ_DT_DECOR, Types.SPACE_CHAR);
    }

    public void setWgrzDtDecorNull(String wgrzDtDecorNull) {
        writeString(Pos.WGRZ_DT_DECOR_NULL, wgrzDtDecorNull, Len.WGRZ_DT_DECOR_NULL);
    }

    /**Original name: WGRZ-DT-DECOR-NULL<br>*/
    public String getWgrzDtDecorNull() {
        return readString(Pos.WGRZ_DT_DECOR_NULL, Len.WGRZ_DT_DECOR_NULL);
    }

    public String getVgrzDtDecorNullFormatted() {
        return Functions.padBlanks(getWgrzDtDecorNull(), Len.WGRZ_DT_DECOR_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_DECOR = 1;
        public static final int WGRZ_DT_DECOR_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WGRZ_DT_DECOR = 5;
        public static final int WGRZ_DT_DECOR_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WGRZ_DT_DECOR = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
