package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: S089-MONT-END2006<br>
 * Variable: S089-MONT-END2006 from program LCCS0005<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class S089MontEnd2006 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public S089MontEnd2006() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.S089_MONT_END2006;
    }

    public void setWlquMontEnd2006(AfDecimal wlquMontEnd2006) {
        writeDecimalAsPacked(Pos.S089_MONT_END2006, wlquMontEnd2006.copy());
    }

    public void setWlquMontEnd2006FromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.S089_MONT_END2006, Pos.S089_MONT_END2006);
    }

    /**Original name: WLQU-MONT-END2006<br>*/
    public AfDecimal getWlquMontEnd2006() {
        return readPackedAsDecimal(Pos.S089_MONT_END2006, Len.Int.WLQU_MONT_END2006, Len.Fract.WLQU_MONT_END2006);
    }

    public byte[] getWlquMontEnd2006AsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.S089_MONT_END2006, Pos.S089_MONT_END2006);
        return buffer;
    }

    public void initWlquMontEnd2006Spaces() {
        fill(Pos.S089_MONT_END2006, Len.S089_MONT_END2006, Types.SPACE_CHAR);
    }

    public void setWlquMontEnd2006Null(String wlquMontEnd2006Null) {
        writeString(Pos.S089_MONT_END2006_NULL, wlquMontEnd2006Null, Len.WLQU_MONT_END2006_NULL);
    }

    /**Original name: WLQU-MONT-END2006-NULL<br>*/
    public String getWlquMontEnd2006Null() {
        return readString(Pos.S089_MONT_END2006_NULL, Len.WLQU_MONT_END2006_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int S089_MONT_END2006 = 1;
        public static final int S089_MONT_END2006_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int S089_MONT_END2006 = 8;
        public static final int WLQU_MONT_END2006_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WLQU_MONT_END2006 = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WLQU_MONT_END2006 = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
