package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3421-IMP-SOPR-TEC<br>
 * Variable: L3421-IMP-SOPR-TEC from program LDBS3420<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3421ImpSoprTec extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3421ImpSoprTec() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3421_IMP_SOPR_TEC;
    }

    public void setL3421ImpSoprTecFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3421_IMP_SOPR_TEC, Pos.L3421_IMP_SOPR_TEC);
    }

    /**Original name: L3421-IMP-SOPR-TEC<br>*/
    public AfDecimal getL3421ImpSoprTec() {
        return readPackedAsDecimal(Pos.L3421_IMP_SOPR_TEC, Len.Int.L3421_IMP_SOPR_TEC, Len.Fract.L3421_IMP_SOPR_TEC);
    }

    public byte[] getL3421ImpSoprTecAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3421_IMP_SOPR_TEC, Pos.L3421_IMP_SOPR_TEC);
        return buffer;
    }

    /**Original name: L3421-IMP-SOPR-TEC-NULL<br>*/
    public String getL3421ImpSoprTecNull() {
        return readString(Pos.L3421_IMP_SOPR_TEC_NULL, Len.L3421_IMP_SOPR_TEC_NULL);
    }

    public String getL3421ImpSoprTecNullFormatted() {
        return Functions.padBlanks(getL3421ImpSoprTecNull(), Len.L3421_IMP_SOPR_TEC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_TEC = 1;
        public static final int L3421_IMP_SOPR_TEC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3421_IMP_SOPR_TEC = 8;
        public static final int L3421_IMP_SOPR_TEC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_TEC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int L3421_IMP_SOPR_TEC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
