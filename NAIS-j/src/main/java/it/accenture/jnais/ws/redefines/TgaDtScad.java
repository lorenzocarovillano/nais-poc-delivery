package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TGA-DT-SCAD<br>
 * Variable: TGA-DT-SCAD from program LDBS1360<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TgaDtScad extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TgaDtScad() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TGA_DT_SCAD;
    }

    public void setTgaDtScad(int tgaDtScad) {
        writeIntAsPacked(Pos.TGA_DT_SCAD, tgaDtScad, Len.Int.TGA_DT_SCAD);
    }

    public void setTgaDtScadFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TGA_DT_SCAD, Pos.TGA_DT_SCAD);
    }

    /**Original name: TGA-DT-SCAD<br>*/
    public int getTgaDtScad() {
        return readPackedAsInt(Pos.TGA_DT_SCAD, Len.Int.TGA_DT_SCAD);
    }

    public byte[] getTgaDtScadAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TGA_DT_SCAD, Pos.TGA_DT_SCAD);
        return buffer;
    }

    public void setTgaDtScadNull(String tgaDtScadNull) {
        writeString(Pos.TGA_DT_SCAD_NULL, tgaDtScadNull, Len.TGA_DT_SCAD_NULL);
    }

    /**Original name: TGA-DT-SCAD-NULL<br>*/
    public String getTgaDtScadNull() {
        return readString(Pos.TGA_DT_SCAD_NULL, Len.TGA_DT_SCAD_NULL);
    }

    public String getTgaDtScadNullFormatted() {
        return Functions.padBlanks(getTgaDtScadNull(), Len.TGA_DT_SCAD_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TGA_DT_SCAD = 1;
        public static final int TGA_DT_SCAD_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TGA_DT_SCAD = 5;
        public static final int TGA_DT_SCAD_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TGA_DT_SCAD = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
