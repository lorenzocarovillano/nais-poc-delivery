package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: B03-CAR-INC<br>
 * Variable: B03-CAR-INC from program LLBS0240<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class B03CarInc extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public B03CarInc() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.B03_CAR_INC;
    }

    public void setB03CarInc(AfDecimal b03CarInc) {
        writeDecimalAsPacked(Pos.B03_CAR_INC, b03CarInc.copy());
    }

    public void setB03CarIncFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.B03_CAR_INC, Pos.B03_CAR_INC);
    }

    /**Original name: B03-CAR-INC<br>*/
    public AfDecimal getB03CarInc() {
        return readPackedAsDecimal(Pos.B03_CAR_INC, Len.Int.B03_CAR_INC, Len.Fract.B03_CAR_INC);
    }

    public byte[] getB03CarIncAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.B03_CAR_INC, Pos.B03_CAR_INC);
        return buffer;
    }

    public void setB03CarIncNull(String b03CarIncNull) {
        writeString(Pos.B03_CAR_INC_NULL, b03CarIncNull, Len.B03_CAR_INC_NULL);
    }

    /**Original name: B03-CAR-INC-NULL<br>*/
    public String getB03CarIncNull() {
        return readString(Pos.B03_CAR_INC_NULL, Len.B03_CAR_INC_NULL);
    }

    public String getB03CarIncNullFormatted() {
        return Functions.padBlanks(getB03CarIncNull(), Len.B03_CAR_INC_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int B03_CAR_INC = 1;
        public static final int B03_CAR_INC_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int B03_CAR_INC = 8;
        public static final int B03_CAR_INC_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int B03_CAR_INC = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int B03_CAR_INC = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
