package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: TDR-FRAZ<br>
 * Variable: TDR-FRAZ from program LOAS0320<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class TdrFraz extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public TdrFraz() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.TDR_FRAZ;
    }

    public void setTdrFraz(int tdrFraz) {
        writeIntAsPacked(Pos.TDR_FRAZ, tdrFraz, Len.Int.TDR_FRAZ);
    }

    public void setTdrFrazFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.TDR_FRAZ, Pos.TDR_FRAZ);
    }

    /**Original name: TDR-FRAZ<br>*/
    public int getTdrFraz() {
        return readPackedAsInt(Pos.TDR_FRAZ, Len.Int.TDR_FRAZ);
    }

    public byte[] getTdrFrazAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.TDR_FRAZ, Pos.TDR_FRAZ);
        return buffer;
    }

    public void setTdrFrazNull(String tdrFrazNull) {
        writeString(Pos.TDR_FRAZ_NULL, tdrFrazNull, Len.TDR_FRAZ_NULL);
    }

    /**Original name: TDR-FRAZ-NULL<br>*/
    public String getTdrFrazNull() {
        return readString(Pos.TDR_FRAZ_NULL, Len.TDR_FRAZ_NULL);
    }

    public String getTdrFrazNullFormatted() {
        return Functions.padBlanks(getTdrFrazNull(), Len.TDR_FRAZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int TDR_FRAZ = 1;
        public static final int TDR_FRAZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int TDR_FRAZ = 3;
        public static final int TDR_FRAZ_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int TDR_FRAZ = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
