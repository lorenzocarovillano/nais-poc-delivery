package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-TOT-IAS-RST-DPST<br>
 * Variable: LQU-TOT-IAS-RST-DPST from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquTotIasRstDpst extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquTotIasRstDpst() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_TOT_IAS_RST_DPST;
    }

    public void setLquTotIasRstDpst(AfDecimal lquTotIasRstDpst) {
        writeDecimalAsPacked(Pos.LQU_TOT_IAS_RST_DPST, lquTotIasRstDpst.copy());
    }

    public void setLquTotIasRstDpstFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_TOT_IAS_RST_DPST, Pos.LQU_TOT_IAS_RST_DPST);
    }

    /**Original name: LQU-TOT-IAS-RST-DPST<br>*/
    public AfDecimal getLquTotIasRstDpst() {
        return readPackedAsDecimal(Pos.LQU_TOT_IAS_RST_DPST, Len.Int.LQU_TOT_IAS_RST_DPST, Len.Fract.LQU_TOT_IAS_RST_DPST);
    }

    public byte[] getLquTotIasRstDpstAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_TOT_IAS_RST_DPST, Pos.LQU_TOT_IAS_RST_DPST);
        return buffer;
    }

    public void setLquTotIasRstDpstNull(String lquTotIasRstDpstNull) {
        writeString(Pos.LQU_TOT_IAS_RST_DPST_NULL, lquTotIasRstDpstNull, Len.LQU_TOT_IAS_RST_DPST_NULL);
    }

    /**Original name: LQU-TOT-IAS-RST-DPST-NULL<br>*/
    public String getLquTotIasRstDpstNull() {
        return readString(Pos.LQU_TOT_IAS_RST_DPST_NULL, Len.LQU_TOT_IAS_RST_DPST_NULL);
    }

    public String getLquTotIasRstDpstNullFormatted() {
        return Functions.padBlanks(getLquTotIasRstDpstNull(), Len.LQU_TOT_IAS_RST_DPST_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_RST_DPST = 1;
        public static final int LQU_TOT_IAS_RST_DPST_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_TOT_IAS_RST_DPST = 8;
        public static final int LQU_TOT_IAS_RST_DPST_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_RST_DPST = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_TOT_IAS_RST_DPST = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
