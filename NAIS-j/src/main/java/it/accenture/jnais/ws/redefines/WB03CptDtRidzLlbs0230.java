package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: W-B03-CPT-DT-RIDZ<br>
 * Variable: W-B03-CPT-DT-RIDZ from program LLBS0230<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WB03CptDtRidzLlbs0230 extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WB03CptDtRidzLlbs0230() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.W_B03_CPT_DT_RIDZ;
    }

    public void setwB03CptDtRidz(AfDecimal wB03CptDtRidz) {
        writeDecimalAsPacked(Pos.W_B03_CPT_DT_RIDZ, wB03CptDtRidz.copy());
    }

    /**Original name: W-B03-CPT-DT-RIDZ<br>*/
    public AfDecimal getwB03CptDtRidz() {
        return readPackedAsDecimal(Pos.W_B03_CPT_DT_RIDZ, Len.Int.W_B03_CPT_DT_RIDZ, Len.Fract.W_B03_CPT_DT_RIDZ);
    }

    public byte[] getwB03CptDtRidzAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.W_B03_CPT_DT_RIDZ, Pos.W_B03_CPT_DT_RIDZ);
        return buffer;
    }

    public void setwB03CptDtRidzNull(String wB03CptDtRidzNull) {
        writeString(Pos.W_B03_CPT_DT_RIDZ_NULL, wB03CptDtRidzNull, Len.W_B03_CPT_DT_RIDZ_NULL);
    }

    /**Original name: W-B03-CPT-DT-RIDZ-NULL<br>*/
    public String getwB03CptDtRidzNull() {
        return readString(Pos.W_B03_CPT_DT_RIDZ_NULL, Len.W_B03_CPT_DT_RIDZ_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_DT_RIDZ = 1;
        public static final int W_B03_CPT_DT_RIDZ_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int W_B03_CPT_DT_RIDZ = 8;
        public static final int W_B03_CPT_DT_RIDZ_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_DT_RIDZ = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int W_B03_CPT_DT_RIDZ = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
