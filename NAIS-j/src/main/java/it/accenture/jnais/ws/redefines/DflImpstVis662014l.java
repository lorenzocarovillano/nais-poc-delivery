package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: DFL-IMPST-VIS-662014L<br>
 * Variable: DFL-IMPST-VIS-662014L from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class DflImpstVis662014l extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public DflImpstVis662014l() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.DFL_IMPST_VIS662014L;
    }

    public void setDflImpstVis662014l(AfDecimal dflImpstVis662014l) {
        writeDecimalAsPacked(Pos.DFL_IMPST_VIS662014L, dflImpstVis662014l.copy());
    }

    public void setDflImpstVis662014lFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.DFL_IMPST_VIS662014L, Pos.DFL_IMPST_VIS662014L);
    }

    /**Original name: DFL-IMPST-VIS-662014L<br>*/
    public AfDecimal getDflImpstVis662014l() {
        return readPackedAsDecimal(Pos.DFL_IMPST_VIS662014L, Len.Int.DFL_IMPST_VIS662014L, Len.Fract.DFL_IMPST_VIS662014L);
    }

    public byte[] getDflImpstVis662014lAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.DFL_IMPST_VIS662014L, Pos.DFL_IMPST_VIS662014L);
        return buffer;
    }

    public void setDflImpstVis662014lNull(String dflImpstVis662014lNull) {
        writeString(Pos.DFL_IMPST_VIS662014L_NULL, dflImpstVis662014lNull, Len.DFL_IMPST_VIS662014L_NULL);
    }

    /**Original name: DFL-IMPST-VIS-662014L-NULL<br>*/
    public String getDflImpstVis662014lNull() {
        return readString(Pos.DFL_IMPST_VIS662014L_NULL, Len.DFL_IMPST_VIS662014L_NULL);
    }

    public String getDflImpstVis662014lNullFormatted() {
        return Functions.padBlanks(getDflImpstVis662014lNull(), Len.DFL_IMPST_VIS662014L_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS662014L = 1;
        public static final int DFL_IMPST_VIS662014L_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int DFL_IMPST_VIS662014L = 8;
        public static final int DFL_IMPST_VIS662014L_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS662014L = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int DFL_IMPST_VIS662014L = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
