package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L3401-ETA-AA-1O-ASSTO<br>
 * Variable: L3401-ETA-AA-1O-ASSTO from program LDBS3400<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L3401EtaAa1oAssto extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L3401EtaAa1oAssto() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L3401_ETA_AA1O_ASSTO;
    }

    public void setL3401EtaAa1oAssto(short l3401EtaAa1oAssto) {
        writeShortAsPacked(Pos.L3401_ETA_AA1O_ASSTO, l3401EtaAa1oAssto, Len.Int.L3401_ETA_AA1O_ASSTO);
    }

    public void setL3401EtaAa1oAsstoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L3401_ETA_AA1O_ASSTO, Pos.L3401_ETA_AA1O_ASSTO);
    }

    /**Original name: L3401-ETA-AA-1O-ASSTO<br>*/
    public short getL3401EtaAa1oAssto() {
        return readPackedAsShort(Pos.L3401_ETA_AA1O_ASSTO, Len.Int.L3401_ETA_AA1O_ASSTO);
    }

    public byte[] getL3401EtaAa1oAsstoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L3401_ETA_AA1O_ASSTO, Pos.L3401_ETA_AA1O_ASSTO);
        return buffer;
    }

    /**Original name: L3401-ETA-AA-1O-ASSTO-NULL<br>*/
    public String getL3401EtaAa1oAsstoNull() {
        return readString(Pos.L3401_ETA_AA1O_ASSTO_NULL, Len.L3401_ETA_AA1O_ASSTO_NULL);
    }

    public String getL3401EtaAa1oAsstoNullFormatted() {
        return Functions.padBlanks(getL3401EtaAa1oAsstoNull(), Len.L3401_ETA_AA1O_ASSTO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L3401_ETA_AA1O_ASSTO = 1;
        public static final int L3401_ETA_AA1O_ASSTO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L3401_ETA_AA1O_ASSTO = 2;
        public static final int L3401_ETA_AA1O_ASSTO_NULL = 2;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L3401_ETA_AA1O_ASSTO = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
