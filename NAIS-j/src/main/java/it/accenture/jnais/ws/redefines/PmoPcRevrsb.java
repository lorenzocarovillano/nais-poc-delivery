package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: PMO-PC-REVRSB<br>
 * Variable: PMO-PC-REVRSB from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class PmoPcRevrsb extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public PmoPcRevrsb() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.PMO_PC_REVRSB;
    }

    public void setPmoPcRevrsb(AfDecimal pmoPcRevrsb) {
        writeDecimalAsPacked(Pos.PMO_PC_REVRSB, pmoPcRevrsb.copy());
    }

    public void setPmoPcRevrsbFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.PMO_PC_REVRSB, Pos.PMO_PC_REVRSB);
    }

    /**Original name: PMO-PC-REVRSB<br>*/
    public AfDecimal getPmoPcRevrsb() {
        return readPackedAsDecimal(Pos.PMO_PC_REVRSB, Len.Int.PMO_PC_REVRSB, Len.Fract.PMO_PC_REVRSB);
    }

    public byte[] getPmoPcRevrsbAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.PMO_PC_REVRSB, Pos.PMO_PC_REVRSB);
        return buffer;
    }

    public void initPmoPcRevrsbHighValues() {
        fill(Pos.PMO_PC_REVRSB, Len.PMO_PC_REVRSB, Types.HIGH_CHAR_VAL);
    }

    public void setPmoPcRevrsbNull(String pmoPcRevrsbNull) {
        writeString(Pos.PMO_PC_REVRSB_NULL, pmoPcRevrsbNull, Len.PMO_PC_REVRSB_NULL);
    }

    /**Original name: PMO-PC-REVRSB-NULL<br>*/
    public String getPmoPcRevrsbNull() {
        return readString(Pos.PMO_PC_REVRSB_NULL, Len.PMO_PC_REVRSB_NULL);
    }

    public String getPmoPcRevrsbNullFormatted() {
        return Functions.padBlanks(getPmoPcRevrsbNull(), Len.PMO_PC_REVRSB_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int PMO_PC_REVRSB = 1;
        public static final int PMO_PC_REVRSB_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int PMO_PC_REVRSB = 4;
        public static final int PMO_PC_REVRSB_NULL = 4;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int PMO_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int PMO_PC_REVRSB = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
