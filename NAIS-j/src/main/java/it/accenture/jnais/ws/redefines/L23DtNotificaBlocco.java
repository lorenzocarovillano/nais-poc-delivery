package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

/**Original name: L23-DT-NOTIFICA-BLOCCO<br>
 * Variable: L23-DT-NOTIFICA-BLOCCO from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class L23DtNotificaBlocco extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public L23DtNotificaBlocco() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.L23_DT_NOTIFICA_BLOCCO;
    }

    public void setL23DtNotificaBlocco(int l23DtNotificaBlocco) {
        writeIntAsPacked(Pos.L23_DT_NOTIFICA_BLOCCO, l23DtNotificaBlocco, Len.Int.L23_DT_NOTIFICA_BLOCCO);
    }

    public void setL23DtNotificaBloccoFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.L23_DT_NOTIFICA_BLOCCO, Pos.L23_DT_NOTIFICA_BLOCCO);
    }

    /**Original name: L23-DT-NOTIFICA-BLOCCO<br>*/
    public int getL23DtNotificaBlocco() {
        return readPackedAsInt(Pos.L23_DT_NOTIFICA_BLOCCO, Len.Int.L23_DT_NOTIFICA_BLOCCO);
    }

    public byte[] getL23DtNotificaBloccoAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.L23_DT_NOTIFICA_BLOCCO, Pos.L23_DT_NOTIFICA_BLOCCO);
        return buffer;
    }

    public void setL23DtNotificaBloccoNull(String l23DtNotificaBloccoNull) {
        writeString(Pos.L23_DT_NOTIFICA_BLOCCO_NULL, l23DtNotificaBloccoNull, Len.L23_DT_NOTIFICA_BLOCCO_NULL);
    }

    /**Original name: L23-DT-NOTIFICA-BLOCCO-NULL<br>*/
    public String getL23DtNotificaBloccoNull() {
        return readString(Pos.L23_DT_NOTIFICA_BLOCCO_NULL, Len.L23_DT_NOTIFICA_BLOCCO_NULL);
    }

    public String getL23DtNotificaBloccoNullFormatted() {
        return Functions.padBlanks(getL23DtNotificaBloccoNull(), Len.L23_DT_NOTIFICA_BLOCCO_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int L23_DT_NOTIFICA_BLOCCO = 1;
        public static final int L23_DT_NOTIFICA_BLOCCO_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int L23_DT_NOTIFICA_BLOCCO = 5;
        public static final int L23_DT_NOTIFICA_BLOCCO_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int L23_DT_NOTIFICA_BLOCCO = 8;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
