package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: FL-CICLO-GAR<br>
 * Variable: FL-CICLO-GAR from program LCCS0033<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FlCicloGar {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char SI = 'S';
    public static final char NO = 'N';

    //==== METHODS ====
    public void setFlCicloGar(char flCicloGar) {
        this.value = flCicloGar;
    }

    public char getFlCicloGar() {
        return this.value;
    }

    public boolean isSi() {
        return value == SI;
    }

    public void setSi() {
        value = SI;
    }

    public void setNo() {
        value = NO;
    }
}
