package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: LQU-IMPST-APPL-RILT<br>
 * Variable: LQU-IMPST-APPL-RILT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class LquImpstApplRilt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public LquImpstApplRilt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.LQU_IMPST_APPL_RILT;
    }

    public void setLquImpstApplRilt(AfDecimal lquImpstApplRilt) {
        writeDecimalAsPacked(Pos.LQU_IMPST_APPL_RILT, lquImpstApplRilt.copy());
    }

    public void setLquImpstApplRiltFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.LQU_IMPST_APPL_RILT, Pos.LQU_IMPST_APPL_RILT);
    }

    /**Original name: LQU-IMPST-APPL-RILT<br>*/
    public AfDecimal getLquImpstApplRilt() {
        return readPackedAsDecimal(Pos.LQU_IMPST_APPL_RILT, Len.Int.LQU_IMPST_APPL_RILT, Len.Fract.LQU_IMPST_APPL_RILT);
    }

    public byte[] getLquImpstApplRiltAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.LQU_IMPST_APPL_RILT, Pos.LQU_IMPST_APPL_RILT);
        return buffer;
    }

    public void setLquImpstApplRiltNull(String lquImpstApplRiltNull) {
        writeString(Pos.LQU_IMPST_APPL_RILT_NULL, lquImpstApplRiltNull, Len.LQU_IMPST_APPL_RILT_NULL);
    }

    /**Original name: LQU-IMPST-APPL-RILT-NULL<br>*/
    public String getLquImpstApplRiltNull() {
        return readString(Pos.LQU_IMPST_APPL_RILT_NULL, Len.LQU_IMPST_APPL_RILT_NULL);
    }

    public String getLquImpstApplRiltNullFormatted() {
        return Functions.padBlanks(getLquImpstApplRiltNull(), Len.LQU_IMPST_APPL_RILT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_APPL_RILT = 1;
        public static final int LQU_IMPST_APPL_RILT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int LQU_IMPST_APPL_RILT = 8;
        public static final int LQU_IMPST_APPL_RILT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_APPL_RILT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }

        public static class Fract {

            //==== PROPERTIES ====
            public static final int LQU_IMPST_APPL_RILT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }
    }
}
