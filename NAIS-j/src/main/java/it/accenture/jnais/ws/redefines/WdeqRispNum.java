package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDEQ-RISP-NUM<br>
 * Variable: WDEQ-RISP-NUM from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdeqRispNum extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdeqRispNum() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDEQ_RISP_NUM;
    }

    public void setWdeqRispNum(int wdeqRispNum) {
        writeIntAsPacked(Pos.WDEQ_RISP_NUM, wdeqRispNum, Len.Int.WDEQ_RISP_NUM);
    }

    public void setWdeqRispNumFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDEQ_RISP_NUM, Pos.WDEQ_RISP_NUM);
    }

    /**Original name: WDEQ-RISP-NUM<br>*/
    public int getWdeqRispNum() {
        return readPackedAsInt(Pos.WDEQ_RISP_NUM, Len.Int.WDEQ_RISP_NUM);
    }

    public byte[] getWdeqRispNumAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDEQ_RISP_NUM, Pos.WDEQ_RISP_NUM);
        return buffer;
    }

    public void initWdeqRispNumSpaces() {
        fill(Pos.WDEQ_RISP_NUM, Len.WDEQ_RISP_NUM, Types.SPACE_CHAR);
    }

    public void setWdeqRispNumNull(String wdeqRispNumNull) {
        writeString(Pos.WDEQ_RISP_NUM_NULL, wdeqRispNumNull, Len.WDEQ_RISP_NUM_NULL);
    }

    /**Original name: WDEQ-RISP-NUM-NULL<br>*/
    public String getWdeqRispNumNull() {
        return readString(Pos.WDEQ_RISP_NUM_NULL, Len.WDEQ_RISP_NUM_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_NUM = 1;
        public static final int WDEQ_RISP_NUM_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDEQ_RISP_NUM = 3;
        public static final int WDEQ_RISP_NUM_NULL = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDEQ_RISP_NUM = 5;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
