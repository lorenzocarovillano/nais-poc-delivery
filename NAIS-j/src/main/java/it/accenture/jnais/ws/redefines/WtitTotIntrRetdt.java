package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: WTIT-TOT-INTR-RETDT<br>
 * Variable: WTIT-TOT-INTR-RETDT from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WtitTotIntrRetdt extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WtitTotIntrRetdt() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WTIT_TOT_INTR_RETDT;
    }

    public void setWtitTotIntrRetdt(AfDecimal wtitTotIntrRetdt) {
        writeDecimalAsPacked(Pos.WTIT_TOT_INTR_RETDT, wtitTotIntrRetdt.copy());
    }

    public void setWtitTotIntrRetdtFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WTIT_TOT_INTR_RETDT, Pos.WTIT_TOT_INTR_RETDT);
    }

    /**Original name: WTIT-TOT-INTR-RETDT<br>*/
    public AfDecimal getWtitTotIntrRetdt() {
        return readPackedAsDecimal(Pos.WTIT_TOT_INTR_RETDT, Len.Int.WTIT_TOT_INTR_RETDT, Len.Fract.WTIT_TOT_INTR_RETDT);
    }

    public byte[] getWtitTotIntrRetdtAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WTIT_TOT_INTR_RETDT, Pos.WTIT_TOT_INTR_RETDT);
        return buffer;
    }

    public void initWtitTotIntrRetdtSpaces() {
        fill(Pos.WTIT_TOT_INTR_RETDT, Len.WTIT_TOT_INTR_RETDT, Types.SPACE_CHAR);
    }

    public void setWtitTotIntrRetdtNull(String wtitTotIntrRetdtNull) {
        writeString(Pos.WTIT_TOT_INTR_RETDT_NULL, wtitTotIntrRetdtNull, Len.WTIT_TOT_INTR_RETDT_NULL);
    }

    /**Original name: WTIT-TOT-INTR-RETDT-NULL<br>*/
    public String getWtitTotIntrRetdtNull() {
        return readString(Pos.WTIT_TOT_INTR_RETDT_NULL, Len.WTIT_TOT_INTR_RETDT_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_RETDT = 1;
        public static final int WTIT_TOT_INTR_RETDT_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WTIT_TOT_INTR_RETDT = 8;
        public static final int WTIT_TOT_INTR_RETDT_NULL = 8;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Fract {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_RETDT = 3;

            //==== CONSTRUCTORS ====
            private Fract() {
            }
        }

        public static class Int {

            //==== PROPERTIES ====
            public static final int WTIT_TOT_INTR_RETDT = 12;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
