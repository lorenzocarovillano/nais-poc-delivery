package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: AREA0140-OPERAZIONE<br>
 * Variable: AREA0140-OPERAZIONE from program IABS0140<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Area0140Operazione {

    //==== PROPERTIES ====
    private String value = DefaultValues.stringVal(Len.OPERAZIONE);
    public static final String CONNECT = "CONNECT";
    public static final String DISCONNECT = "DISCONNECT";
    public static final String CONNECT_RESET = "CONNECT RESET";

    //==== METHODS ====
    public void setOperazione(String operazione) {
        this.value = Functions.subString(operazione, Len.OPERAZIONE);
    }

    public String getOperazione() {
        return this.value;
    }

    public void setArea0140Connect() {
        value = CONNECT;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OPERAZIONE = 15;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
