package it.accenture.jnais.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;

/**Original name: WDTC-ID-MOVI-CHIU<br>
 * Variable: WDTC-ID-MOVI-CHIU from program IVVS0216<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WdtcIdMoviChiu extends BytesAllocatingClass {

    //==== CONSTRUCTORS ====
    public WdtcIdMoviChiu() {
        super();
    }

    //==== METHODS ====
    @Override
    public int getLength() {
        return Len.WDTC_ID_MOVI_CHIU;
    }

    public void setWdtcIdMoviChiu(int wdtcIdMoviChiu) {
        writeIntAsPacked(Pos.WDTC_ID_MOVI_CHIU, wdtcIdMoviChiu, Len.Int.WDTC_ID_MOVI_CHIU);
    }

    public void setWdtcIdMoviChiuFromBuffer(byte[] buffer, int offset) {
        setBytes(buffer, offset, Len.WDTC_ID_MOVI_CHIU, Pos.WDTC_ID_MOVI_CHIU);
    }

    /**Original name: WDTC-ID-MOVI-CHIU<br>*/
    public int getWdtcIdMoviChiu() {
        return readPackedAsInt(Pos.WDTC_ID_MOVI_CHIU, Len.Int.WDTC_ID_MOVI_CHIU);
    }

    public byte[] getWdtcIdMoviChiuAsBuffer(byte[] buffer, int offset) {
        getBytes(buffer, offset, Len.WDTC_ID_MOVI_CHIU, Pos.WDTC_ID_MOVI_CHIU);
        return buffer;
    }

    public void initWdtcIdMoviChiuSpaces() {
        fill(Pos.WDTC_ID_MOVI_CHIU, Len.WDTC_ID_MOVI_CHIU, Types.SPACE_CHAR);
    }

    public void setWdtcIdMoviChiuNull(String wdtcIdMoviChiuNull) {
        writeString(Pos.WDTC_ID_MOVI_CHIU_NULL, wdtcIdMoviChiuNull, Len.WDTC_ID_MOVI_CHIU_NULL);
    }

    /**Original name: WDTC-ID-MOVI-CHIU-NULL<br>*/
    public String getWdtcIdMoviChiuNull() {
        return readString(Pos.WDTC_ID_MOVI_CHIU_NULL, Len.WDTC_ID_MOVI_CHIU_NULL);
    }

    //==== INNER CLASSES ====
    public static class Pos {

        //==== PROPERTIES ====
        public static final int WDTC_ID_MOVI_CHIU = 1;
        public static final int WDTC_ID_MOVI_CHIU_NULL = 1;

        //==== CONSTRUCTORS ====
        private Pos() {
        }
    }

    public static class Len {

        //==== PROPERTIES ====
        public static final int WDTC_ID_MOVI_CHIU = 5;
        public static final int WDTC_ID_MOVI_CHIU_NULL = 5;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int WDTC_ID_MOVI_CHIU = 9;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
