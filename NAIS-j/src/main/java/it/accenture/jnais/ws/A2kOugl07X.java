package it.accenture.jnais.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: A2K-OUGL07-X<br>
 * Variable: A2K-OUGL07-X from program LCCS0003<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class A2kOugl07X {

    //==== PROPERTIES ====
    //Original name: A2K-OUGL07
    private String ougl07 = DefaultValues.stringVal(Len.OUGL07);
    //Original name: FILLER-A2K-OUGL07-X
    private char flr1 = DefaultValues.CHAR_VAL;

    //==== METHODS ====
    public void setA2kOufa07(short a2kOufa07) {
        int position = 1;
        byte[] buffer = getA2kOugl07XBytes();
        MarshalByte.writeShort(buffer, position, a2kOufa07, Len.Int.A2K_OUFA07, SignType.NO_SIGN);
        setA2kOugl07XBytes(buffer);
    }

    public void setA2kOufa07Formatted(String a2kOufa07) {
        int position = 1;
        byte[] buffer = getA2kOugl07XBytes();
        MarshalByte.writeString(buffer, position, Trunc.toNumeric(a2kOufa07, Len.A2K_OUFA07), Len.A2K_OUFA07);
        setA2kOugl07XBytes(buffer);
    }

    /**Original name: A2K-OUFA07<br>
	 * <pre>                                            giorni a fine anno</pre>*/
    public short getA2kOufa07() {
        int position = 1;
        return MarshalByte.readShort(getA2kOugl07XBytes(), position, Len.Int.A2K_OUFA07, SignType.NO_SIGN);
    }

    public void setA2kOugl07XBytes(byte[] buffer) {
        setA2kOugl07XBytes(buffer, 1);
    }

    /**Original name: A2K-OUGL07-X<br>
	 * <pre>                                            ult. gg lav. del mese</pre>*/
    public byte[] getA2kOugl07XBytes() {
        byte[] buffer = new byte[Len.A2K_OUGL07_X];
        return getA2kOugl07XBytes(buffer, 1);
    }

    public void setA2kOugl07XBytes(byte[] buffer, int offset) {
        int position = offset;
        ougl07 = MarshalByte.readFixedString(buffer, position, Len.OUGL07);
        position += Len.OUGL07;
        flr1 = MarshalByte.readChar(buffer, position);
    }

    public byte[] getA2kOugl07XBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeString(buffer, position, ougl07, Len.OUGL07);
        position += Len.OUGL07;
        MarshalByte.writeChar(buffer, position, flr1);
        return buffer;
    }

    public void initA2kOugl07XSpaces() {
        ougl07 = "";
        flr1 = Types.SPACE_CHAR;
    }

    public void setOugl07Formatted(String ougl07) {
        this.ougl07 = Trunc.toUnsignedNumeric(ougl07, Len.OUGL07);
    }

    public short getOugl07() {
        return NumericDisplay.asShort(this.ougl07);
    }

    public void setFlr1(char flr1) {
        this.flr1 = flr1;
    }

    public char getFlr1() {
        return this.flr1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int OUGL07 = 2;
        public static final int FLR1 = 1;
        public static final int A2K_OUGL07_X = OUGL07 + FLR1;
        public static final int A2K_OUFA07 = 3;

        //==== CONSTRUCTORS ====
        private Len() {
        }

        //==== INNER CLASSES ====
        public static class Int {

            //==== PROPERTIES ====
            public static final int A2K_OUFA07 = 3;

            //==== CONSTRUCTORS ====
            private Int() {
            }
        }
    }
}
