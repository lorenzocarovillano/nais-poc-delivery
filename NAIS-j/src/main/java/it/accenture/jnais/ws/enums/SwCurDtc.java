package it.accenture.jnais.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SW-CUR-DTC<br>
 * Variable: SW-CUR-DTC from program LOAS0310<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwCurDtc {

    //==== PROPERTIES ====
    private char value = DefaultValues.CHAR_VAL;
    public static final char INIZ_CUR_DTC = 'S';
    public static final char FINE_CUR_DTC = 'N';

    //==== METHODS ====
    public void setSwCurDtc(char swCurDtc) {
        this.value = swCurDtc;
    }

    public char getSwCurDtc() {
        return this.value;
    }

    public void setInizCurDtc() {
        value = INIZ_CUR_DTC;
    }

    public boolean isFineCurDtc() {
        return value == FINE_CUR_DTC;
    }

    public void setFineCurDtc() {
        value = FINE_CUR_DTC;
    }
}
