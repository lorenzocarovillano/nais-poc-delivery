package it.accenture.jnais.ws.occurs;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.core.Types;
import it.accenture.jnais.copy.Lccvpog1;
import it.accenture.jnais.copy.WpogDati;
import it.accenture.jnais.ws.enums.WpolStatus;

/**Original name: WPOG-TAB-PARAM-OGG<br>
 * Variables: WPOG-TAB-PARAM-OGG from copybook LCCVPOGB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WpogTabParamOgg {

    //==== PROPERTIES ====
    //Original name: LCCVPOG1
    private Lccvpog1 lccvpog1 = new Lccvpog1();

    //==== METHODS ====
    public void setVpogTabParamOggBytes(byte[] buffer) {
        setWpogTabParamOggBytes(buffer, 1);
    }

    public byte[] getTabParamOggBytes() {
        byte[] buffer = new byte[Len.WPOG_TAB_PARAM_OGG];
        return getWpogTabParamOggBytes(buffer, 1);
    }

    public void setWpogTabParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        lccvpog1.getStatus().setStatus(MarshalByte.readChar(buffer, position));
        position += Types.CHAR_SIZE;
        lccvpog1.setIdPtf(MarshalByte.readPackedAsInt(buffer, position, Lccvpog1.Len.Int.ID_PTF, 0));
        position += Lccvpog1.Len.ID_PTF;
        lccvpog1.getDati().setDatiBytes(buffer, position);
    }

    public byte[] getWpogTabParamOggBytes(byte[] buffer, int offset) {
        int position = offset;
        MarshalByte.writeChar(buffer, position, lccvpog1.getStatus().getStatus());
        position += Types.CHAR_SIZE;
        MarshalByte.writeIntAsPacked(buffer, position, lccvpog1.getIdPtf(), Lccvpog1.Len.Int.ID_PTF, 0);
        position += Lccvpog1.Len.ID_PTF;
        lccvpog1.getDati().getDatiBytes(buffer, position);
        return buffer;
    }

    public void initWpogTabParamOggSpaces() {
        lccvpog1.initLccvpog1Spaces();
    }

    public Lccvpog1 getLccvpog1() {
        return lccvpog1;
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WPOG_TAB_PARAM_OGG = WpolStatus.Len.STATUS + Lccvpog1.Len.ID_PTF + WpogDati.Len.DATI;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
