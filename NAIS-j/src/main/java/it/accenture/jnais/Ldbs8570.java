package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TitContDao;
import it.accenture.jnais.commons.data.to.ITitCont;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs8570Data;
import it.accenture.jnais.ws.redefines.TitDtApplzMora;
import it.accenture.jnais.ws.redefines.TitDtCambioVlt;
import it.accenture.jnais.ws.redefines.TitDtCertFisc;
import it.accenture.jnais.ws.redefines.TitDtEmisTit;
import it.accenture.jnais.ws.redefines.TitDtEndCop;
import it.accenture.jnais.ws.redefines.TitDtEsiTit;
import it.accenture.jnais.ws.redefines.TitDtIniCop;
import it.accenture.jnais.ws.redefines.TitDtRichAddRid;
import it.accenture.jnais.ws.redefines.TitDtVlt;
import it.accenture.jnais.ws.redefines.TitFraz;
import it.accenture.jnais.ws.redefines.TitIdMoviChiu;
import it.accenture.jnais.ws.redefines.TitIdRappAna;
import it.accenture.jnais.ws.redefines.TitIdRappRete;
import it.accenture.jnais.ws.redefines.TitImpAder;
import it.accenture.jnais.ws.redefines.TitImpAz;
import it.accenture.jnais.ws.redefines.TitImpPag;
import it.accenture.jnais.ws.redefines.TitImpTfr;
import it.accenture.jnais.ws.redefines.TitImpTfrStrc;
import it.accenture.jnais.ws.redefines.TitImpTrasfe;
import it.accenture.jnais.ws.redefines.TitImpVolo;
import it.accenture.jnais.ws.redefines.TitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.TitProgTit;
import it.accenture.jnais.ws.redefines.TitTotAcqExp;
import it.accenture.jnais.ws.redefines.TitTotCarAcq;
import it.accenture.jnais.ws.redefines.TitTotCarGest;
import it.accenture.jnais.ws.redefines.TitTotCarIas;
import it.accenture.jnais.ws.redefines.TitTotCarInc;
import it.accenture.jnais.ws.redefines.TitTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.TitTotCommisInter;
import it.accenture.jnais.ws.redefines.TitTotDir;
import it.accenture.jnais.ws.redefines.TitTotIntrFraz;
import it.accenture.jnais.ws.redefines.TitTotIntrMora;
import it.accenture.jnais.ws.redefines.TitTotIntrPrest;
import it.accenture.jnais.ws.redefines.TitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TitTotIntrRiat;
import it.accenture.jnais.ws.redefines.TitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TitTotManfeeRec;
import it.accenture.jnais.ws.redefines.TitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TitTotPreNet;
import it.accenture.jnais.ws.redefines.TitTotPrePpIas;
import it.accenture.jnais.ws.redefines.TitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TitTotPreTot;
import it.accenture.jnais.ws.redefines.TitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TitTotProvDaRec;
import it.accenture.jnais.ws.redefines.TitTotProvInc;
import it.accenture.jnais.ws.redefines.TitTotProvRicor;
import it.accenture.jnais.ws.redefines.TitTotRemunAss;
import it.accenture.jnais.ws.redefines.TitTotSoprAlt;
import it.accenture.jnais.ws.redefines.TitTotSoprProf;
import it.accenture.jnais.ws.redefines.TitTotSoprSan;
import it.accenture.jnais.ws.redefines.TitTotSoprSpo;
import it.accenture.jnais.ws.redefines.TitTotSoprTec;
import it.accenture.jnais.ws.redefines.TitTotSpeAge;
import it.accenture.jnais.ws.redefines.TitTotSpeMed;
import it.accenture.jnais.ws.redefines.TitTotTax;
import it.accenture.jnais.ws.redefines.TitTpCausStor;
import it.accenture.jnais.ws.TitContIdbstit0;

/**Original name: LDBS8570<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  08 OTT 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs8570 extends Program implements ITitCont {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TitContDao titContDao = new TitContDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs8570Data ws = new Ldbs8570Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TIT-CONT
    private TitContIdbstit0 titCont;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS8570_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TitContIdbstit0 titCont) {
        this.idsv0003 = idsv0003;
        this.titCont = titCont;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs8570 getInstance() {
        return ((Ldbs8570)Programs.getInstance(Ldbs8570.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS8570'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS8570");
        // COB_CODE: MOVE 'TIT-CONT' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TIT-CONT");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,IB_RICH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,TP_MEZ_PAG_ADD
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,DT_VLT
        //                    ,FL_FORZ_DT_VLT
        //                    ,DT_CAMBIO_VLT
        //                    ,TOT_SPE_AGE
        //                    ,TOT_CAR_IAS
        //                    ,NUM_RAT_ACCORPATE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_TIT_DA_REINVST
        //                    ,DT_RICH_ADD_RID
        //                    ,TP_ESI_RID
        //                    ,COD_IBAN
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,DT_CERT_FISC
        //                    ,TP_CAUS_STOR
        //                    ,TP_CAUS_DISP_STOR
        //                    ,TP_TIT_MIGRAZ
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TP_CAUS_RIMB
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //              FROM TIT_CONT
        //              WHERE  ID_OGG  = :TIT-ID-OGG AND
        //                    TP_OGG  = :TIT-TP-OGG AND
        //                    TP_TIT  = :TIT-TP-TIT AND
        //                    TP_STAT_TIT = :TIT-TP-STAT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,IB_RICH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,TP_MEZ_PAG_ADD
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,DT_VLT
        //                    ,FL_FORZ_DT_VLT
        //                    ,DT_CAMBIO_VLT
        //                    ,TOT_SPE_AGE
        //                    ,TOT_CAR_IAS
        //                    ,NUM_RAT_ACCORPATE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_TIT_DA_REINVST
        //                    ,DT_RICH_ADD_RID
        //                    ,TP_ESI_RID
        //                    ,COD_IBAN
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,DT_CERT_FISC
        //                    ,TP_CAUS_STOR
        //                    ,TP_CAUS_DISP_STOR
        //                    ,TP_TIT_MIGRAZ
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TP_CAUS_RIMB
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //             INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //             FROM TIT_CONT
        //             WHERE  ID_OGG  = :TIT-ID-OGG AND
        //                    TP_OGG  = :TIT-TP-OGG AND
        //                    TP_TIT  = :TIT-TP-TIT AND
        //                    TP_STAT_TIT = :TIT-TP-STAT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        titContDao.selectRec13(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        titContDao.openCEff38(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        titContDao.closeCEff38();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //           END-EXEC.
        titContDao.fetchCEff38(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,IB_RICH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,TP_MEZ_PAG_ADD
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,DT_VLT
        //                    ,FL_FORZ_DT_VLT
        //                    ,DT_CAMBIO_VLT
        //                    ,TOT_SPE_AGE
        //                    ,TOT_CAR_IAS
        //                    ,NUM_RAT_ACCORPATE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_TIT_DA_REINVST
        //                    ,DT_RICH_ADD_RID
        //                    ,TP_ESI_RID
        //                    ,COD_IBAN
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,DT_CERT_FISC
        //                    ,TP_CAUS_STOR
        //                    ,TP_CAUS_DISP_STOR
        //                    ,TP_TIT_MIGRAZ
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TP_CAUS_RIMB
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //              FROM TIT_CONT
        //              WHERE  ID_OGG  = :TIT-ID-OGG AND
        //                        TP_OGG  = :TIT-TP-OGG AND
        //                        TP_TIT  = :TIT-TP-TIT AND
        //                        TP_STAT_TIT = :TIT-TP-STAT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_TIT_CONT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,IB_RICH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_TIT
        //                    ,PROG_TIT
        //                    ,TP_PRE_TIT
        //                    ,TP_STAT_TIT
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,IMP_PAG
        //                    ,FL_SOLL
        //                    ,FRAZ
        //                    ,DT_APPLZ_MORA
        //                    ,FL_MORA
        //                    ,ID_RAPP_RETE
        //                    ,ID_RAPP_ANA
        //                    ,COD_DVS
        //                    ,DT_EMIS_TIT
        //                    ,DT_ESI_TIT
        //                    ,TOT_PRE_NET
        //                    ,TOT_INTR_FRAZ
        //                    ,TOT_INTR_MORA
        //                    ,TOT_INTR_PREST
        //                    ,TOT_INTR_RETDT
        //                    ,TOT_INTR_RIAT
        //                    ,TOT_DIR
        //                    ,TOT_SPE_MED
        //                    ,TOT_TAX
        //                    ,TOT_SOPR_SAN
        //                    ,TOT_SOPR_TEC
        //                    ,TOT_SOPR_SPO
        //                    ,TOT_SOPR_PROF
        //                    ,TOT_SOPR_ALT
        //                    ,TOT_PRE_TOT
        //                    ,TOT_PRE_PP_IAS
        //                    ,TOT_CAR_ACQ
        //                    ,TOT_CAR_GEST
        //                    ,TOT_CAR_INC
        //                    ,TOT_PRE_SOLO_RSH
        //                    ,TOT_PROV_ACQ_1AA
        //                    ,TOT_PROV_ACQ_2AA
        //                    ,TOT_PROV_RICOR
        //                    ,TOT_PROV_INC
        //                    ,TOT_PROV_DA_REC
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,TOT_MANFEE_ANTIC
        //                    ,TOT_MANFEE_RICOR
        //                    ,TOT_MANFEE_REC
        //                    ,TP_MEZ_PAG_ADD
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,DT_VLT
        //                    ,FL_FORZ_DT_VLT
        //                    ,DT_CAMBIO_VLT
        //                    ,TOT_SPE_AGE
        //                    ,TOT_CAR_IAS
        //                    ,NUM_RAT_ACCORPATE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_TIT_DA_REINVST
        //                    ,DT_RICH_ADD_RID
        //                    ,TP_ESI_RID
        //                    ,COD_IBAN
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,DT_CERT_FISC
        //                    ,TP_CAUS_STOR
        //                    ,TP_CAUS_DISP_STOR
        //                    ,TP_TIT_MIGRAZ
        //                    ,TOT_ACQ_EXP
        //                    ,TOT_REMUN_ASS
        //                    ,TOT_COMMIS_INTER
        //                    ,TP_CAUS_RIMB
        //                    ,TOT_CNBT_ANTIRAC
        //                    ,FL_INC_AUTOGEN
        //             INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //             FROM TIT_CONT
        //             WHERE  ID_OGG  = :TIT-ID-OGG AND
        //                    TP_OGG  = :TIT-TP-OGG AND
        //                    TP_TIT  = :TIT-TP-TIT AND
        //                    TP_STAT_TIT = :TIT-TP-STAT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        titContDao.selectRec14(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        titContDao.openCCpz38(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        titContDao.closeCCpz38();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :TIT-ID-TIT-CONT
        //               ,:TIT-ID-OGG
        //               ,:TIT-TP-OGG
        //               ,:TIT-IB-RICH
        //                :IND-TIT-IB-RICH
        //               ,:TIT-ID-MOVI-CRZ
        //               ,:TIT-ID-MOVI-CHIU
        //                :IND-TIT-ID-MOVI-CHIU
        //               ,:TIT-DT-INI-EFF-DB
        //               ,:TIT-DT-END-EFF-DB
        //               ,:TIT-COD-COMP-ANIA
        //               ,:TIT-TP-TIT
        //               ,:TIT-PROG-TIT
        //                :IND-TIT-PROG-TIT
        //               ,:TIT-TP-PRE-TIT
        //               ,:TIT-TP-STAT-TIT
        //               ,:TIT-DT-INI-COP-DB
        //                :IND-TIT-DT-INI-COP
        //               ,:TIT-DT-END-COP-DB
        //                :IND-TIT-DT-END-COP
        //               ,:TIT-IMP-PAG
        //                :IND-TIT-IMP-PAG
        //               ,:TIT-FL-SOLL
        //                :IND-TIT-FL-SOLL
        //               ,:TIT-FRAZ
        //                :IND-TIT-FRAZ
        //               ,:TIT-DT-APPLZ-MORA-DB
        //                :IND-TIT-DT-APPLZ-MORA
        //               ,:TIT-FL-MORA
        //                :IND-TIT-FL-MORA
        //               ,:TIT-ID-RAPP-RETE
        //                :IND-TIT-ID-RAPP-RETE
        //               ,:TIT-ID-RAPP-ANA
        //                :IND-TIT-ID-RAPP-ANA
        //               ,:TIT-COD-DVS
        //                :IND-TIT-COD-DVS
        //               ,:TIT-DT-EMIS-TIT-DB
        //                :IND-TIT-DT-EMIS-TIT
        //               ,:TIT-DT-ESI-TIT-DB
        //                :IND-TIT-DT-ESI-TIT
        //               ,:TIT-TOT-PRE-NET
        //                :IND-TIT-TOT-PRE-NET
        //               ,:TIT-TOT-INTR-FRAZ
        //                :IND-TIT-TOT-INTR-FRAZ
        //               ,:TIT-TOT-INTR-MORA
        //                :IND-TIT-TOT-INTR-MORA
        //               ,:TIT-TOT-INTR-PREST
        //                :IND-TIT-TOT-INTR-PREST
        //               ,:TIT-TOT-INTR-RETDT
        //                :IND-TIT-TOT-INTR-RETDT
        //               ,:TIT-TOT-INTR-RIAT
        //                :IND-TIT-TOT-INTR-RIAT
        //               ,:TIT-TOT-DIR
        //                :IND-TIT-TOT-DIR
        //               ,:TIT-TOT-SPE-MED
        //                :IND-TIT-TOT-SPE-MED
        //               ,:TIT-TOT-TAX
        //                :IND-TIT-TOT-TAX
        //               ,:TIT-TOT-SOPR-SAN
        //                :IND-TIT-TOT-SOPR-SAN
        //               ,:TIT-TOT-SOPR-TEC
        //                :IND-TIT-TOT-SOPR-TEC
        //               ,:TIT-TOT-SOPR-SPO
        //                :IND-TIT-TOT-SOPR-SPO
        //               ,:TIT-TOT-SOPR-PROF
        //                :IND-TIT-TOT-SOPR-PROF
        //               ,:TIT-TOT-SOPR-ALT
        //                :IND-TIT-TOT-SOPR-ALT
        //               ,:TIT-TOT-PRE-TOT
        //                :IND-TIT-TOT-PRE-TOT
        //               ,:TIT-TOT-PRE-PP-IAS
        //                :IND-TIT-TOT-PRE-PP-IAS
        //               ,:TIT-TOT-CAR-ACQ
        //                :IND-TIT-TOT-CAR-ACQ
        //               ,:TIT-TOT-CAR-GEST
        //                :IND-TIT-TOT-CAR-GEST
        //               ,:TIT-TOT-CAR-INC
        //                :IND-TIT-TOT-CAR-INC
        //               ,:TIT-TOT-PRE-SOLO-RSH
        //                :IND-TIT-TOT-PRE-SOLO-RSH
        //               ,:TIT-TOT-PROV-ACQ-1AA
        //                :IND-TIT-TOT-PROV-ACQ-1AA
        //               ,:TIT-TOT-PROV-ACQ-2AA
        //                :IND-TIT-TOT-PROV-ACQ-2AA
        //               ,:TIT-TOT-PROV-RICOR
        //                :IND-TIT-TOT-PROV-RICOR
        //               ,:TIT-TOT-PROV-INC
        //                :IND-TIT-TOT-PROV-INC
        //               ,:TIT-TOT-PROV-DA-REC
        //                :IND-TIT-TOT-PROV-DA-REC
        //               ,:TIT-IMP-AZ
        //                :IND-TIT-IMP-AZ
        //               ,:TIT-IMP-ADER
        //                :IND-TIT-IMP-ADER
        //               ,:TIT-IMP-TFR
        //                :IND-TIT-IMP-TFR
        //               ,:TIT-IMP-VOLO
        //                :IND-TIT-IMP-VOLO
        //               ,:TIT-TOT-MANFEE-ANTIC
        //                :IND-TIT-TOT-MANFEE-ANTIC
        //               ,:TIT-TOT-MANFEE-RICOR
        //                :IND-TIT-TOT-MANFEE-RICOR
        //               ,:TIT-TOT-MANFEE-REC
        //                :IND-TIT-TOT-MANFEE-REC
        //               ,:TIT-TP-MEZ-PAG-ADD
        //                :IND-TIT-TP-MEZ-PAG-ADD
        //               ,:TIT-ESTR-CNT-CORR-ADD
        //                :IND-TIT-ESTR-CNT-CORR-ADD
        //               ,:TIT-DT-VLT-DB
        //                :IND-TIT-DT-VLT
        //               ,:TIT-FL-FORZ-DT-VLT
        //                :IND-TIT-FL-FORZ-DT-VLT
        //               ,:TIT-DT-CAMBIO-VLT-DB
        //                :IND-TIT-DT-CAMBIO-VLT
        //               ,:TIT-TOT-SPE-AGE
        //                :IND-TIT-TOT-SPE-AGE
        //               ,:TIT-TOT-CAR-IAS
        //                :IND-TIT-TOT-CAR-IAS
        //               ,:TIT-NUM-RAT-ACCORPATE
        //                :IND-TIT-NUM-RAT-ACCORPATE
        //               ,:TIT-DS-RIGA
        //               ,:TIT-DS-OPER-SQL
        //               ,:TIT-DS-VER
        //               ,:TIT-DS-TS-INI-CPTZ
        //               ,:TIT-DS-TS-END-CPTZ
        //               ,:TIT-DS-UTENTE
        //               ,:TIT-DS-STATO-ELAB
        //               ,:TIT-FL-TIT-DA-REINVST
        //                :IND-TIT-FL-TIT-DA-REINVST
        //               ,:TIT-DT-RICH-ADD-RID-DB
        //                :IND-TIT-DT-RICH-ADD-RID
        //               ,:TIT-TP-ESI-RID
        //                :IND-TIT-TP-ESI-RID
        //               ,:TIT-COD-IBAN
        //                :IND-TIT-COD-IBAN
        //               ,:TIT-IMP-TRASFE
        //                :IND-TIT-IMP-TRASFE
        //               ,:TIT-IMP-TFR-STRC
        //                :IND-TIT-IMP-TFR-STRC
        //               ,:TIT-DT-CERT-FISC-DB
        //                :IND-TIT-DT-CERT-FISC
        //               ,:TIT-TP-CAUS-STOR
        //                :IND-TIT-TP-CAUS-STOR
        //               ,:TIT-TP-CAUS-DISP-STOR
        //                :IND-TIT-TP-CAUS-DISP-STOR
        //               ,:TIT-TP-TIT-MIGRAZ
        //                :IND-TIT-TP-TIT-MIGRAZ
        //               ,:TIT-TOT-ACQ-EXP
        //                :IND-TIT-TOT-ACQ-EXP
        //               ,:TIT-TOT-REMUN-ASS
        //                :IND-TIT-TOT-REMUN-ASS
        //               ,:TIT-TOT-COMMIS-INTER
        //                :IND-TIT-TOT-COMMIS-INTER
        //               ,:TIT-TP-CAUS-RIMB
        //                :IND-TIT-TP-CAUS-RIMB
        //               ,:TIT-TOT-CNBT-ANTIRAC
        //                :IND-TIT-TOT-CNBT-ANTIRAC
        //               ,:TIT-FL-INC-AUTOGEN
        //                :IND-TIT-FL-INC-AUTOGEN
        //           END-EXEC.
        titContDao.fetchCCpz38(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TIT-IB-RICH = -1
        //              MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
        //           END-IF
        if (ws.getIndTitCont().getIbRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
            titCont.setTitIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_IB_RICH));
        }
        // COB_CODE: IF IND-TIT-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
            titCont.getTitIdMoviChiu().setTitIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TIT-PROG-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getProgTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
            titCont.getTitProgTit().setTitProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitProgTit.Len.TIT_PROG_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
            titCont.getTitDtIniCop().setTitDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtIniCop.Len.TIT_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
            titCont.getTitDtEndCop().setTitDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEndCop.Len.TIT_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-PAG = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
            titCont.getTitImpPag().setTitImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpPag.Len.TIT_IMP_PAG_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-SOLL = -1
        //              MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlSoll() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
            titCont.setTitFlSoll(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FRAZ-NULL
            titCont.getTitFraz().setTitFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitFraz.Len.TIT_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-APPLZ-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtApplzMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
            titCont.getTitDtApplzMora().setTitDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtApplzMora.Len.TIT_DT_APPLZ_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
            titCont.setTitFlMora(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-RETE = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappRete() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
            titCont.getTitIdRappRete().setTitIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappRete.Len.TIT_ID_RAPP_RETE_NULL));
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
            titCont.getTitIdRappAna().setTitIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappAna.Len.TIT_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-TIT-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
            titCont.setTitCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_COD_DVS));
        }
        // COB_CODE: IF IND-TIT-DT-EMIS-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEmisTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
            titCont.getTitDtEmisTit().setTitDtEmisTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEmisTit.Len.TIT_DT_EMIS_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
            titCont.getTitDtEsiTit().setTitDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEsiTit.Len.TIT_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-NET = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
            titCont.getTitTotPreNet().setTitTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreNet.Len.TIT_TOT_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
            titCont.getTitTotIntrFraz().setTitTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
            titCont.getTitTotIntrMora().setTitTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrMora.Len.TIT_TOT_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
            titCont.getTitTotIntrPrest().setTitTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrPrest.Len.TIT_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
            titCont.getTitTotIntrRetdt().setTitTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
            titCont.getTitTotIntrRiat().setTitTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-DIR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
            titCont.getTitTotDir().setTitTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotDir.Len.TIT_TOT_DIR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-MED = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
            titCont.getTitTotSpeMed().setTitTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeMed.Len.TIT_TOT_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-TAX = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
            titCont.getTitTotTax().setTitTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotTax.Len.TIT_TOT_TAX_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
            titCont.getTitTotSoprSan().setTitTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSan.Len.TIT_TOT_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
            titCont.getTitTotSoprTec().setTitTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprTec.Len.TIT_TOT_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
            titCont.getTitTotSoprSpo().setTitTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
            titCont.getTitTotSoprProf().setTitTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprProf.Len.TIT_TOT_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
            titCont.getTitTotSoprAlt().setTitTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
            titCont.getTitTotPreTot().setTitTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreTot.Len.TIT_TOT_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
            titCont.getTitTotPrePpIas().setTitTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
            titCont.getTitTotCarAcq().setTitTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarAcq.Len.TIT_TOT_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
            titCont.getTitTotCarGest().setTitTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarGest.Len.TIT_TOT_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
            titCont.getTitTotCarInc().setTitTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarInc.Len.TIT_TOT_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
            titCont.getTitTotPreSoloRsh().setTitTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
            titCont.getTitTotProvAcq1aa().setTitTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
            titCont.getTitTotProvAcq2aa().setTitTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
            titCont.getTitTotProvRicor().setTitTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvRicor.Len.TIT_TOT_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
            titCont.getTitTotProvInc().setTitTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvInc.Len.TIT_TOT_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
            titCont.getTitTotProvDaRec().setTitTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
            titCont.getTitImpAz().setTitImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAz.Len.TIT_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
            titCont.getTitImpAder().setTitImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAder.Len.TIT_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
            titCont.getTitImpTfr().setTitImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfr.Len.TIT_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
            titCont.getTitImpVolo().setTitImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpVolo.Len.TIT_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
            titCont.getTitTotManfeeAntic().setTitTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
            titCont.getTitTotManfeeRicor().setTitTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
            titCont.getTitTotManfeeRec().setTitTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-MEZ-PAG-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpMezPagAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
            titCont.setTitTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_MEZ_PAG_ADD));
        }
        // COB_CODE: IF IND-TIT-ESTR-CNT-CORR-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getEstrCntCorrAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
            titCont.setTitEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_ESTR_CNT_CORR_ADD));
        }
        // COB_CODE: IF IND-TIT-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
            titCont.getTitDtVlt().setTitDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtVlt.Len.TIT_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-FORZ-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlForzDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
            titCont.setTitFlForzDtVlt(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-CAMBIO-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
            titCont.getTitDtCambioVlt().setTitDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
            titCont.getTitTotSpeAge().setTitTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeAge.Len.TIT_TOT_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
            titCont.getTitTotCarIas().setTitTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarIas.Len.TIT_TOT_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-NUM-RAT-ACCORPATE = -1
        //              MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
        //           END-IF
        if (ws.getIndTitCont().getNumRatAccorpate() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
            titCont.getTitNumRatAccorpate().setTitNumRatAccorpateNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-TIT-DA-REINVST = -1
        //              MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlTitDaReinvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
            titCont.setTitFlTitDaReinvst(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-RICH-ADD-RID = -1
        //              MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtRichAddRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
            titCont.getTitDtRichAddRid().setTitDtRichAddRidNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-ESI-RID = -1
        //              MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpEsiRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
            titCont.setTitTpEsiRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_ESI_RID));
        }
        // COB_CODE: IF IND-TIT-COD-IBAN = -1
        //              MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodIban() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
            titCont.setTitCodIban(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_COD_IBAN));
        }
        // COB_CODE: IF IND-TIT-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
            titCont.getTitImpTrasfe().setTitImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTrasfe.Len.TIT_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
            titCont.getTitImpTfrStrc().setTitImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfrStrc.Len.TIT_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-CERT-FISC = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCertFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
            titCont.getTitDtCertFisc().setTitDtCertFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCertFisc.Len.TIT_DT_CERT_FISC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
            titCont.getTitTpCausStor().setTitTpCausStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTpCausStor.Len.TIT_TP_CAUS_STOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-DISP-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausDispStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
            titCont.setTitTpCausDispStor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_CAUS_DISP_STOR));
        }
        // COB_CODE: IF IND-TIT-TP-TIT-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpTitMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
            titCont.setTitTpTitMigraz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_TIT_MIGRAZ));
        }
        // COB_CODE: IF IND-TIT-TOT-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
            titCont.getTitTotAcqExp().setTitTotAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotAcqExp.Len.TIT_TOT_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
            titCont.getTitTotRemunAss().setTitTotRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotRemunAss.Len.TIT_TOT_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTotCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
            titCont.getTitTotCommisInter().setTitTotCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCommisInter.Len.TIT_TOT_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-RIMB = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTpCausRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
            titCont.setTitTpCausRimb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitContIdbstit0.Len.TIT_TP_CAUS_RIMB));
        }
        // COB_CODE: IF IND-TIT-TOT-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTotCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
            titCont.getTitTotCnbtAntirac().setTitTotCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCnbtAntirac.Len.TIT_TOT_CNBT_ANTIRAC_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-INC-AUTOGEN = -1
        //              MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
        //           END-IF.
        if (ws.getIndTitCont().getFlIncAutogen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
            titCont.setTitFlIncAutogen(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TIT-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitContDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-INI-EFF
        titCont.setTitDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TIT-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-END-EFF
        titCont.setTitDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TIT-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO TIT-DT-INI-COP
        //           END-IF
        if (ws.getIndTitCont().getDtIniCop() == 0) {
            // COB_CODE: MOVE TIT-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-INI-COP
            titCont.getTitDtIniCop().setTitDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO TIT-DT-END-COP
        //           END-IF
        if (ws.getIndTitCont().getDtEndCop() == 0) {
            // COB_CODE: MOVE TIT-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-END-COP
            titCont.getTitDtEndCop().setTitDtEndCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-APPLZ-MORA = 0
        //               MOVE WS-DATE-N      TO TIT-DT-APPLZ-MORA
        //           END-IF
        if (ws.getIndTitCont().getDtApplzMora() == 0) {
            // COB_CODE: MOVE TIT-DT-APPLZ-MORA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getApplzMoraDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-APPLZ-MORA
            titCont.getTitDtApplzMora().setTitDtApplzMora(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-EMIS-TIT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-EMIS-TIT
        //           END-IF
        if (ws.getIndTitCont().getDtEmisTit() == 0) {
            // COB_CODE: MOVE TIT-DT-EMIS-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEmisTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-EMIS-TIT
            titCont.getTitDtEmisTit().setTitDtEmisTit(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-ESI-TIT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-ESI-TIT
        //           END-IF
        if (ws.getIndTitCont().getDtEsiTit() == 0) {
            // COB_CODE: MOVE TIT-DT-ESI-TIT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-ESI-TIT
            titCont.getTitDtEsiTit().setTitDtEsiTit(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-VLT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-VLT
        //           END-IF
        if (ws.getIndTitCont().getDtVlt() == 0) {
            // COB_CODE: MOVE TIT-DT-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getVltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-VLT
            titCont.getTitDtVlt().setTitDtVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-CAMBIO-VLT = 0
        //               MOVE WS-DATE-N      TO TIT-DT-CAMBIO-VLT
        //           END-IF
        if (ws.getIndTitCont().getDtCambioVlt() == 0) {
            // COB_CODE: MOVE TIT-DT-CAMBIO-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getCambioVltDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-CAMBIO-VLT
            titCont.getTitDtCambioVlt().setTitDtCambioVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-RICH-ADD-RID = 0
        //               MOVE WS-DATE-N      TO TIT-DT-RICH-ADD-RID
        //           END-IF
        if (ws.getIndTitCont().getDtRichAddRid() == 0) {
            // COB_CODE: MOVE TIT-DT-RICH-ADD-RID-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getRichAddRidDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-RICH-ADD-RID
            titCont.getTitDtRichAddRid().setTitDtRichAddRid(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TIT-DT-CERT-FISC = 0
        //               MOVE WS-DATE-N      TO TIT-DT-CERT-FISC
        //           END-IF.
        if (ws.getIndTitCont().getDtCertFisc() == 0) {
            // COB_CODE: MOVE TIT-DT-CERT-FISC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTitContDb().getCertFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TIT-DT-CERT-FISC
            titCont.getTitDtCertFisc().setTitDtCertFisc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return titCont.getTitCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.titCont.setTitCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return titCont.getTitCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.titCont.setTitCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndTitCont().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndTitCont().setCodDvs(((short)0));
        }
        else {
            ws.getIndTitCont().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getCodIban() {
        return titCont.getTitCodIban();
    }

    @Override
    public void setCodIban(String codIban) {
        this.titCont.setTitCodIban(codIban);
    }

    @Override
    public String getCodIbanObj() {
        if (ws.getIndTitCont().getCodIban() >= 0) {
            return getCodIban();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodIbanObj(String codIbanObj) {
        if (codIbanObj != null) {
            setCodIban(codIbanObj);
            ws.getIndTitCont().setCodIban(((short)0));
        }
        else {
            ws.getIndTitCont().setCodIban(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return titCont.getTitDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.titCont.setTitDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return titCont.getTitDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.titCont.setTitDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return titCont.getTitDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.titCont.setTitDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return titCont.getTitDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.titCont.setTitDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return titCont.getTitDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.titCont.setTitDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return titCont.getTitDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.titCont.setTitDsVer(dsVer);
    }

    @Override
    public String getDtApplzMoraDb() {
        return ws.getTitContDb().getApplzMoraDb();
    }

    @Override
    public void setDtApplzMoraDb(String dtApplzMoraDb) {
        this.ws.getTitContDb().setApplzMoraDb(dtApplzMoraDb);
    }

    @Override
    public String getDtApplzMoraDbObj() {
        if (ws.getIndTitCont().getDtApplzMora() >= 0) {
            return getDtApplzMoraDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtApplzMoraDbObj(String dtApplzMoraDbObj) {
        if (dtApplzMoraDbObj != null) {
            setDtApplzMoraDb(dtApplzMoraDbObj);
            ws.getIndTitCont().setDtApplzMora(((short)0));
        }
        else {
            ws.getIndTitCont().setDtApplzMora(((short)-1));
        }
    }

    @Override
    public String getDtCambioVltDb() {
        return ws.getTitContDb().getCambioVltDb();
    }

    @Override
    public void setDtCambioVltDb(String dtCambioVltDb) {
        this.ws.getTitContDb().setCambioVltDb(dtCambioVltDb);
    }

    @Override
    public String getDtCambioVltDbObj() {
        if (ws.getIndTitCont().getDtCambioVlt() >= 0) {
            return getDtCambioVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtCambioVltDbObj(String dtCambioVltDbObj) {
        if (dtCambioVltDbObj != null) {
            setDtCambioVltDb(dtCambioVltDbObj);
            ws.getIndTitCont().setDtCambioVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public String getDtCertFiscDb() {
        return ws.getTitContDb().getCertFiscDb();
    }

    @Override
    public void setDtCertFiscDb(String dtCertFiscDb) {
        this.ws.getTitContDb().setCertFiscDb(dtCertFiscDb);
    }

    @Override
    public String getDtCertFiscDbObj() {
        if (ws.getIndTitCont().getDtCertFisc() >= 0) {
            return getDtCertFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtCertFiscDbObj(String dtCertFiscDbObj) {
        if (dtCertFiscDbObj != null) {
            setDtCertFiscDb(dtCertFiscDbObj);
            ws.getIndTitCont().setDtCertFisc(((short)0));
        }
        else {
            ws.getIndTitCont().setDtCertFisc(((short)-1));
        }
    }

    @Override
    public String getDtEmisTitDb() {
        return ws.getTitContDb().getEmisTitDb();
    }

    @Override
    public void setDtEmisTitDb(String dtEmisTitDb) {
        this.ws.getTitContDb().setEmisTitDb(dtEmisTitDb);
    }

    @Override
    public String getDtEmisTitDbObj() {
        if (ws.getIndTitCont().getDtEmisTit() >= 0) {
            return getDtEmisTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEmisTitDbObj(String dtEmisTitDbObj) {
        if (dtEmisTitDbObj != null) {
            setDtEmisTitDb(dtEmisTitDbObj);
            ws.getIndTitCont().setDtEmisTit(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEmisTit(((short)-1));
        }
    }

    @Override
    public String getDtEndCopDb() {
        return ws.getTitContDb().getEndCopDb();
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        this.ws.getTitContDb().setEndCopDb(dtEndCopDb);
    }

    @Override
    public String getDtEndCopDbObj() {
        if (ws.getIndTitCont().getDtEndCop() >= 0) {
            return getDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        if (dtEndCopDbObj != null) {
            setDtEndCopDb(dtEndCopDbObj);
            ws.getIndTitCont().setDtEndCop(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getTitContDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getTitContDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtEsiTitDb() {
        return ws.getTitContDb().getEsiTitDb();
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        this.ws.getTitContDb().setEsiTitDb(dtEsiTitDb);
    }

    @Override
    public String getDtEsiTitDbObj() {
        if (ws.getIndTitCont().getDtEsiTit() >= 0) {
            return getDtEsiTitDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        if (dtEsiTitDbObj != null) {
            setDtEsiTitDb(dtEsiTitDbObj);
            ws.getIndTitCont().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndTitCont().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getTitContDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getTitContDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRichAddRidDb() {
        return ws.getTitContDb().getRichAddRidDb();
    }

    @Override
    public void setDtRichAddRidDb(String dtRichAddRidDb) {
        this.ws.getTitContDb().setRichAddRidDb(dtRichAddRidDb);
    }

    @Override
    public String getDtRichAddRidDbObj() {
        if (ws.getIndTitCont().getDtRichAddRid() >= 0) {
            return getDtRichAddRidDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRichAddRidDbObj(String dtRichAddRidDbObj) {
        if (dtRichAddRidDbObj != null) {
            setDtRichAddRidDb(dtRichAddRidDbObj);
            ws.getIndTitCont().setDtRichAddRid(((short)0));
        }
        else {
            ws.getIndTitCont().setDtRichAddRid(((short)-1));
        }
    }

    @Override
    public String getDtVltDb() {
        return ws.getTitContDb().getVltDb();
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        this.ws.getTitContDb().setVltDb(dtVltDb);
    }

    @Override
    public String getDtVltDbObj() {
        if (ws.getIndTitCont().getDtVlt() >= 0) {
            return getDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        if (dtVltDbObj != null) {
            setDtVltDb(dtVltDbObj);
            ws.getIndTitCont().setDtVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setDtVlt(((short)-1));
        }
    }

    @Override
    public String getEstrCntCorrAdd() {
        return titCont.getTitEstrCntCorrAdd();
    }

    @Override
    public void setEstrCntCorrAdd(String estrCntCorrAdd) {
        this.titCont.setTitEstrCntCorrAdd(estrCntCorrAdd);
    }

    @Override
    public String getEstrCntCorrAddObj() {
        if (ws.getIndTitCont().getEstrCntCorrAdd() >= 0) {
            return getEstrCntCorrAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrCntCorrAddObj(String estrCntCorrAddObj) {
        if (estrCntCorrAddObj != null) {
            setEstrCntCorrAdd(estrCntCorrAddObj);
            ws.getIndTitCont().setEstrCntCorrAdd(((short)0));
        }
        else {
            ws.getIndTitCont().setEstrCntCorrAdd(((short)-1));
        }
    }

    @Override
    public char getFlForzDtVlt() {
        return titCont.getTitFlForzDtVlt();
    }

    @Override
    public void setFlForzDtVlt(char flForzDtVlt) {
        this.titCont.setTitFlForzDtVlt(flForzDtVlt);
    }

    @Override
    public Character getFlForzDtVltObj() {
        if (ws.getIndTitCont().getFlForzDtVlt() >= 0) {
            return ((Character)getFlForzDtVlt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlForzDtVltObj(Character flForzDtVltObj) {
        if (flForzDtVltObj != null) {
            setFlForzDtVlt(((char)flForzDtVltObj));
            ws.getIndTitCont().setFlForzDtVlt(((short)0));
        }
        else {
            ws.getIndTitCont().setFlForzDtVlt(((short)-1));
        }
    }

    @Override
    public char getFlIncAutogen() {
        return titCont.getTitFlIncAutogen();
    }

    @Override
    public void setFlIncAutogen(char flIncAutogen) {
        this.titCont.setTitFlIncAutogen(flIncAutogen);
    }

    @Override
    public Character getFlIncAutogenObj() {
        if (ws.getIndTitCont().getFlIncAutogen() >= 0) {
            return ((Character)getFlIncAutogen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIncAutogenObj(Character flIncAutogenObj) {
        if (flIncAutogenObj != null) {
            setFlIncAutogen(((char)flIncAutogenObj));
            ws.getIndTitCont().setFlIncAutogen(((short)0));
        }
        else {
            ws.getIndTitCont().setFlIncAutogen(((short)-1));
        }
    }

    @Override
    public char getFlMora() {
        return titCont.getTitFlMora();
    }

    @Override
    public void setFlMora(char flMora) {
        this.titCont.setTitFlMora(flMora);
    }

    @Override
    public Character getFlMoraObj() {
        if (ws.getIndTitCont().getFlMora() >= 0) {
            return ((Character)getFlMora());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlMoraObj(Character flMoraObj) {
        if (flMoraObj != null) {
            setFlMora(((char)flMoraObj));
            ws.getIndTitCont().setFlMora(((short)0));
        }
        else {
            ws.getIndTitCont().setFlMora(((short)-1));
        }
    }

    @Override
    public char getFlSoll() {
        return titCont.getTitFlSoll();
    }

    @Override
    public void setFlSoll(char flSoll) {
        this.titCont.setTitFlSoll(flSoll);
    }

    @Override
    public Character getFlSollObj() {
        if (ws.getIndTitCont().getFlSoll() >= 0) {
            return ((Character)getFlSoll());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSollObj(Character flSollObj) {
        if (flSollObj != null) {
            setFlSoll(((char)flSollObj));
            ws.getIndTitCont().setFlSoll(((short)0));
        }
        else {
            ws.getIndTitCont().setFlSoll(((short)-1));
        }
    }

    @Override
    public char getFlTitDaReinvst() {
        return titCont.getTitFlTitDaReinvst();
    }

    @Override
    public void setFlTitDaReinvst(char flTitDaReinvst) {
        this.titCont.setTitFlTitDaReinvst(flTitDaReinvst);
    }

    @Override
    public Character getFlTitDaReinvstObj() {
        if (ws.getIndTitCont().getFlTitDaReinvst() >= 0) {
            return ((Character)getFlTitDaReinvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTitDaReinvstObj(Character flTitDaReinvstObj) {
        if (flTitDaReinvstObj != null) {
            setFlTitDaReinvst(((char)flTitDaReinvstObj));
            ws.getIndTitCont().setFlTitDaReinvst(((short)0));
        }
        else {
            ws.getIndTitCont().setFlTitDaReinvst(((short)-1));
        }
    }

    @Override
    public int getFraz() {
        return titCont.getTitFraz().getTitFraz();
    }

    @Override
    public void setFraz(int fraz) {
        this.titCont.getTitFraz().setTitFraz(fraz);
    }

    @Override
    public Integer getFrazObj() {
        if (ws.getIndTitCont().getFraz() >= 0) {
            return ((Integer)getFraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        if (frazObj != null) {
            setFraz(((int)frazObj));
            ws.getIndTitCont().setFraz(((short)0));
        }
        else {
            ws.getIndTitCont().setFraz(((short)-1));
        }
    }

    @Override
    public String getIbRich() {
        return titCont.getTitIbRich();
    }

    @Override
    public void setIbRich(String ibRich) {
        this.titCont.setTitIbRich(ibRich);
    }

    @Override
    public String getIbRichObj() {
        if (ws.getIndTitCont().getIbRich() >= 0) {
            return getIbRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        if (ibRichObj != null) {
            setIbRich(ibRichObj);
            ws.getIndTitCont().setIbRich(((short)0));
        }
        else {
            ws.getIndTitCont().setIbRich(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return titCont.getTitIdMoviChiu().getTitIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.titCont.getTitIdMoviChiu().setTitIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndTitCont().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndTitCont().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTitCont().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return titCont.getTitIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.titCont.setTitIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return titCont.getTitIdRappAna().getTitIdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.titCont.getTitIdRappAna().setTitIdRappAna(idRappAna);
    }

    @Override
    public Integer getIdRappAnaObj() {
        if (ws.getIndTitCont().getIdRappAna() >= 0) {
            return ((Integer)getIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaObj(Integer idRappAnaObj) {
        if (idRappAnaObj != null) {
            setIdRappAna(((int)idRappAnaObj));
            ws.getIndTitCont().setIdRappAna(((short)0));
        }
        else {
            ws.getIndTitCont().setIdRappAna(((short)-1));
        }
    }

    @Override
    public int getIdRappRete() {
        return titCont.getTitIdRappRete().getTitIdRappRete();
    }

    @Override
    public void setIdRappRete(int idRappRete) {
        this.titCont.getTitIdRappRete().setTitIdRappRete(idRappRete);
    }

    @Override
    public Integer getIdRappReteObj() {
        if (ws.getIndTitCont().getIdRappRete() >= 0) {
            return ((Integer)getIdRappRete());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappReteObj(Integer idRappReteObj) {
        if (idRappReteObj != null) {
            setIdRappRete(((int)idRappReteObj));
            ws.getIndTitCont().setIdRappRete(((short)0));
        }
        else {
            ws.getIndTitCont().setIdRappRete(((short)-1));
        }
    }

    @Override
    public int getIdTitCont() {
        return titCont.getTitIdTitCont();
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        this.titCont.setTitIdTitCont(idTitCont);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        return titCont.getTitImpAder().getTitImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.titCont.getTitImpAder().setTitImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndTitCont().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndTitCont().setImpAder(((short)0));
        }
        else {
            ws.getIndTitCont().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return titCont.getTitImpAz().getTitImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.titCont.getTitImpAz().setTitImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndTitCont().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndTitCont().setImpAz(((short)0));
        }
        else {
            ws.getIndTitCont().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPag() {
        return titCont.getTitImpPag().getTitImpPag();
    }

    @Override
    public void setImpPag(AfDecimal impPag) {
        this.titCont.getTitImpPag().setTitImpPag(impPag.copy());
    }

    @Override
    public AfDecimal getImpPagObj() {
        if (ws.getIndTitCont().getImpPag() >= 0) {
            return getImpPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPagObj(AfDecimal impPagObj) {
        if (impPagObj != null) {
            setImpPag(new AfDecimal(impPagObj, 15, 3));
            ws.getIndTitCont().setImpPag(((short)0));
        }
        else {
            ws.getIndTitCont().setImpPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return titCont.getTitImpTfr().getTitImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.titCont.getTitImpTfr().setTitImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndTitCont().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndTitCont().setImpTfr(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        return titCont.getTitImpTfrStrc().getTitImpTfrStrc();
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        this.titCont.getTitImpTfrStrc().setTitImpTfrStrc(impTfrStrc.copy());
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        if (ws.getIndTitCont().getImpTfrStrc() >= 0) {
            return getImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        if (impTfrStrcObj != null) {
            setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
            ws.getIndTitCont().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasfe() {
        return titCont.getTitImpTrasfe().getTitImpTrasfe();
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        this.titCont.getTitImpTrasfe().setTitImpTrasfe(impTrasfe.copy());
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        if (ws.getIndTitCont().getImpTrasfe() >= 0) {
            return getImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        if (impTrasfeObj != null) {
            setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
            ws.getIndTitCont().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTitCont().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return titCont.getTitImpVolo().getTitImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.titCont.getTitImpVolo().setTitImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndTitCont().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndTitCont().setImpVolo(((short)0));
        }
        else {
            ws.getIndTitCont().setImpVolo(((short)-1));
        }
    }

    @Override
    public String getIsoDtIniPerDb() {
        throw new FieldNotMappedException("isoDtIniPerDb");
    }

    @Override
    public void setIsoDtIniPerDb(String isoDtIniPerDb) {
        throw new FieldNotMappedException("isoDtIniPerDb");
    }

    @Override
    public int getLdbv1591IdOgg() {
        throw new FieldNotMappedException("ldbv1591IdOgg");
    }

    @Override
    public void setLdbv1591IdOgg(int ldbv1591IdOgg) {
        throw new FieldNotMappedException("ldbv1591IdOgg");
    }

    @Override
    public AfDecimal getLdbv1591ImpTot() {
        throw new FieldNotMappedException("ldbv1591ImpTot");
    }

    @Override
    public void setLdbv1591ImpTot(AfDecimal ldbv1591ImpTot) {
        throw new FieldNotMappedException("ldbv1591ImpTot");
    }

    @Override
    public String getLdbv1591TpOgg() {
        throw new FieldNotMappedException("ldbv1591TpOgg");
    }

    @Override
    public void setLdbv1591TpOgg(String ldbv1591TpOgg) {
        throw new FieldNotMappedException("ldbv1591TpOgg");
    }

    @Override
    public int getLdbv2091IdOgg() {
        throw new FieldNotMappedException("ldbv2091IdOgg");
    }

    @Override
    public void setLdbv2091IdOgg(int ldbv2091IdOgg) {
        throw new FieldNotMappedException("ldbv2091IdOgg");
    }

    @Override
    public AfDecimal getLdbv2091TotPremi() {
        throw new FieldNotMappedException("ldbv2091TotPremi");
    }

    @Override
    public void setLdbv2091TotPremi(AfDecimal ldbv2091TotPremi) {
        throw new FieldNotMappedException("ldbv2091TotPremi");
    }

    @Override
    public String getLdbv2091TpOgg() {
        throw new FieldNotMappedException("ldbv2091TpOgg");
    }

    @Override
    public void setLdbv2091TpOgg(String ldbv2091TpOgg) {
        throw new FieldNotMappedException("ldbv2091TpOgg");
    }

    @Override
    public String getLdbv2091TpStatTit01() {
        throw new FieldNotMappedException("ldbv2091TpStatTit01");
    }

    @Override
    public void setLdbv2091TpStatTit01(String ldbv2091TpStatTit01) {
        throw new FieldNotMappedException("ldbv2091TpStatTit01");
    }

    @Override
    public String getLdbv2091TpStatTit02() {
        throw new FieldNotMappedException("ldbv2091TpStatTit02");
    }

    @Override
    public void setLdbv2091TpStatTit02(String ldbv2091TpStatTit02) {
        throw new FieldNotMappedException("ldbv2091TpStatTit02");
    }

    @Override
    public String getLdbv2091TpStatTit03() {
        throw new FieldNotMappedException("ldbv2091TpStatTit03");
    }

    @Override
    public void setLdbv2091TpStatTit03(String ldbv2091TpStatTit03) {
        throw new FieldNotMappedException("ldbv2091TpStatTit03");
    }

    @Override
    public String getLdbv2091TpStatTit04() {
        throw new FieldNotMappedException("ldbv2091TpStatTit04");
    }

    @Override
    public void setLdbv2091TpStatTit04(String ldbv2091TpStatTit04) {
        throw new FieldNotMappedException("ldbv2091TpStatTit04");
    }

    @Override
    public String getLdbv2091TpStatTit05() {
        throw new FieldNotMappedException("ldbv2091TpStatTit05");
    }

    @Override
    public void setLdbv2091TpStatTit05(String ldbv2091TpStatTit05) {
        throw new FieldNotMappedException("ldbv2091TpStatTit05");
    }

    @Override
    public String getLdbv6151DtDecorPrestDb() {
        throw new FieldNotMappedException("ldbv6151DtDecorPrestDb");
    }

    @Override
    public void setLdbv6151DtDecorPrestDb(String ldbv6151DtDecorPrestDb) {
        throw new FieldNotMappedException("ldbv6151DtDecorPrestDb");
    }

    @Override
    public String getLdbv6151DtMaxDb() {
        throw new FieldNotMappedException("ldbv6151DtMaxDb");
    }

    @Override
    public void setLdbv6151DtMaxDb(String ldbv6151DtMaxDb) {
        throw new FieldNotMappedException("ldbv6151DtMaxDb");
    }

    @Override
    public String getLdbv6151DtMaxDbObj() {
        return getLdbv6151DtMaxDb();
    }

    @Override
    public void setLdbv6151DtMaxDbObj(String ldbv6151DtMaxDbObj) {
        setLdbv6151DtMaxDb(ldbv6151DtMaxDbObj);
    }

    @Override
    public int getLdbv6151IdOgg() {
        throw new FieldNotMappedException("ldbv6151IdOgg");
    }

    @Override
    public void setLdbv6151IdOgg(int ldbv6151IdOgg) {
        throw new FieldNotMappedException("ldbv6151IdOgg");
    }

    @Override
    public String getLdbv6151TpOgg() {
        throw new FieldNotMappedException("ldbv6151TpOgg");
    }

    @Override
    public void setLdbv6151TpOgg(String ldbv6151TpOgg) {
        throw new FieldNotMappedException("ldbv6151TpOgg");
    }

    @Override
    public String getLdbv6151TpStatTit() {
        throw new FieldNotMappedException("ldbv6151TpStatTit");
    }

    @Override
    public void setLdbv6151TpStatTit(String ldbv6151TpStatTit) {
        throw new FieldNotMappedException("ldbv6151TpStatTit");
    }

    @Override
    public String getLdbv6151TpTit01() {
        throw new FieldNotMappedException("ldbv6151TpTit01");
    }

    @Override
    public void setLdbv6151TpTit01(String ldbv6151TpTit01) {
        throw new FieldNotMappedException("ldbv6151TpTit01");
    }

    @Override
    public String getLdbv6151TpTit02() {
        throw new FieldNotMappedException("ldbv6151TpTit02");
    }

    @Override
    public void setLdbv6151TpTit02(String ldbv6151TpTit02) {
        throw new FieldNotMappedException("ldbv6151TpTit02");
    }

    @Override
    public String getLdbvb441DtMaxDb() {
        throw new FieldNotMappedException("ldbvb441DtMaxDb");
    }

    @Override
    public void setLdbvb441DtMaxDb(String ldbvb441DtMaxDb) {
        throw new FieldNotMappedException("ldbvb441DtMaxDb");
    }

    @Override
    public String getLdbvb441DtMaxDbObj() {
        return getLdbvb441DtMaxDb();
    }

    @Override
    public void setLdbvb441DtMaxDbObj(String ldbvb441DtMaxDbObj) {
        setLdbvb441DtMaxDb(ldbvb441DtMaxDbObj);
    }

    @Override
    public int getLdbvb441IdOgg() {
        throw new FieldNotMappedException("ldbvb441IdOgg");
    }

    @Override
    public void setLdbvb441IdOgg(int ldbvb441IdOgg) {
        throw new FieldNotMappedException("ldbvb441IdOgg");
    }

    @Override
    public String getLdbvb441TpOgg() {
        throw new FieldNotMappedException("ldbvb441TpOgg");
    }

    @Override
    public void setLdbvb441TpOgg(String ldbvb441TpOgg) {
        throw new FieldNotMappedException("ldbvb441TpOgg");
    }

    @Override
    public String getLdbvb441TpStatTit1() {
        throw new FieldNotMappedException("ldbvb441TpStatTit1");
    }

    @Override
    public void setLdbvb441TpStatTit1(String ldbvb441TpStatTit1) {
        throw new FieldNotMappedException("ldbvb441TpStatTit1");
    }

    @Override
    public String getLdbvb441TpStatTit2() {
        throw new FieldNotMappedException("ldbvb441TpStatTit2");
    }

    @Override
    public void setLdbvb441TpStatTit2(String ldbvb441TpStatTit2) {
        throw new FieldNotMappedException("ldbvb441TpStatTit2");
    }

    @Override
    public String getLdbvb441TpStatTit3() {
        throw new FieldNotMappedException("ldbvb441TpStatTit3");
    }

    @Override
    public void setLdbvb441TpStatTit3(String ldbvb441TpStatTit3) {
        throw new FieldNotMappedException("ldbvb441TpStatTit3");
    }

    @Override
    public String getLdbvb441TpTit01() {
        throw new FieldNotMappedException("ldbvb441TpTit01");
    }

    @Override
    public void setLdbvb441TpTit01(String ldbvb441TpTit01) {
        throw new FieldNotMappedException("ldbvb441TpTit01");
    }

    @Override
    public String getLdbvb441TpTit02() {
        throw new FieldNotMappedException("ldbvb441TpTit02");
    }

    @Override
    public void setLdbvb441TpTit02(String ldbvb441TpTit02) {
        throw new FieldNotMappedException("ldbvb441TpTit02");
    }

    @Override
    public String getLdbvb471DtMaxDb() {
        throw new FieldNotMappedException("ldbvb471DtMaxDb");
    }

    @Override
    public void setLdbvb471DtMaxDb(String ldbvb471DtMaxDb) {
        throw new FieldNotMappedException("ldbvb471DtMaxDb");
    }

    @Override
    public int getLdbvb471IdOgg() {
        throw new FieldNotMappedException("ldbvb471IdOgg");
    }

    @Override
    public void setLdbvb471IdOgg(int ldbvb471IdOgg) {
        throw new FieldNotMappedException("ldbvb471IdOgg");
    }

    @Override
    public String getLdbvb471TpOgg() {
        throw new FieldNotMappedException("ldbvb471TpOgg");
    }

    @Override
    public void setLdbvb471TpOgg(String ldbvb471TpOgg) {
        throw new FieldNotMappedException("ldbvb471TpOgg");
    }

    @Override
    public String getLdbvb471TpStatTit1() {
        throw new FieldNotMappedException("ldbvb471TpStatTit1");
    }

    @Override
    public void setLdbvb471TpStatTit1(String ldbvb471TpStatTit1) {
        throw new FieldNotMappedException("ldbvb471TpStatTit1");
    }

    @Override
    public String getLdbvb471TpStatTit2() {
        throw new FieldNotMappedException("ldbvb471TpStatTit2");
    }

    @Override
    public void setLdbvb471TpStatTit2(String ldbvb471TpStatTit2) {
        throw new FieldNotMappedException("ldbvb471TpStatTit2");
    }

    @Override
    public String getLdbvb471TpStatTit3() {
        throw new FieldNotMappedException("ldbvb471TpStatTit3");
    }

    @Override
    public void setLdbvb471TpStatTit3(String ldbvb471TpStatTit3) {
        throw new FieldNotMappedException("ldbvb471TpStatTit3");
    }

    @Override
    public String getLdbvb471TpTit01() {
        throw new FieldNotMappedException("ldbvb471TpTit01");
    }

    @Override
    public void setLdbvb471TpTit01(String ldbvb471TpTit01) {
        throw new FieldNotMappedException("ldbvb471TpTit01");
    }

    @Override
    public String getLdbvb471TpTit02() {
        throw new FieldNotMappedException("ldbvb471TpTit02");
    }

    @Override
    public void setLdbvb471TpTit02(String ldbvb471TpTit02) {
        throw new FieldNotMappedException("ldbvb471TpTit02");
    }

    @Override
    public AfDecimal getLdbvf111CumPreVers() {
        throw new FieldNotMappedException("ldbvf111CumPreVers");
    }

    @Override
    public void setLdbvf111CumPreVers(AfDecimal ldbvf111CumPreVers) {
        throw new FieldNotMappedException("ldbvf111CumPreVers");
    }

    @Override
    public int getLdbvf111IdOgg() {
        throw new FieldNotMappedException("ldbvf111IdOgg");
    }

    @Override
    public void setLdbvf111IdOgg(int ldbvf111IdOgg) {
        throw new FieldNotMappedException("ldbvf111IdOgg");
    }

    @Override
    public String getLdbvf111TpStatTit1() {
        throw new FieldNotMappedException("ldbvf111TpStatTit1");
    }

    @Override
    public void setLdbvf111TpStatTit1(String ldbvf111TpStatTit1) {
        throw new FieldNotMappedException("ldbvf111TpStatTit1");
    }

    @Override
    public String getLdbvf111TpStatTit2() {
        throw new FieldNotMappedException("ldbvf111TpStatTit2");
    }

    @Override
    public void setLdbvf111TpStatTit2(String ldbvf111TpStatTit2) {
        throw new FieldNotMappedException("ldbvf111TpStatTit2");
    }

    @Override
    public String getLdbvf111TpStatTit3() {
        throw new FieldNotMappedException("ldbvf111TpStatTit3");
    }

    @Override
    public void setLdbvf111TpStatTit3(String ldbvf111TpStatTit3) {
        throw new FieldNotMappedException("ldbvf111TpStatTit3");
    }

    @Override
    public String getLdbvf111TpStatTit4() {
        throw new FieldNotMappedException("ldbvf111TpStatTit4");
    }

    @Override
    public void setLdbvf111TpStatTit4(String ldbvf111TpStatTit4) {
        throw new FieldNotMappedException("ldbvf111TpStatTit4");
    }

    @Override
    public String getLdbvf111TpStatTit5() {
        throw new FieldNotMappedException("ldbvf111TpStatTit5");
    }

    @Override
    public void setLdbvf111TpStatTit5(String ldbvf111TpStatTit5) {
        throw new FieldNotMappedException("ldbvf111TpStatTit5");
    }

    @Override
    public int getNumRatAccorpate() {
        return titCont.getTitNumRatAccorpate().getTitNumRatAccorpate();
    }

    @Override
    public void setNumRatAccorpate(int numRatAccorpate) {
        this.titCont.getTitNumRatAccorpate().setTitNumRatAccorpate(numRatAccorpate);
    }

    @Override
    public Integer getNumRatAccorpateObj() {
        if (ws.getIndTitCont().getNumRatAccorpate() >= 0) {
            return ((Integer)getNumRatAccorpate());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumRatAccorpateObj(Integer numRatAccorpateObj) {
        if (numRatAccorpateObj != null) {
            setNumRatAccorpate(((int)numRatAccorpateObj));
            ws.getIndTitCont().setNumRatAccorpate(((short)0));
        }
        else {
            ws.getIndTitCont().setNumRatAccorpate(((short)-1));
        }
    }

    @Override
    public int getProgTit() {
        return titCont.getTitProgTit().getTitProgTit();
    }

    @Override
    public void setProgTit(int progTit) {
        this.titCont.getTitProgTit().setTitProgTit(progTit);
    }

    @Override
    public Integer getProgTitObj() {
        if (ws.getIndTitCont().getProgTit() >= 0) {
            return ((Integer)getProgTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setProgTitObj(Integer progTitObj) {
        if (progTitObj != null) {
            setProgTit(((int)progTitObj));
            ws.getIndTitCont().setProgTit(((short)0));
        }
        else {
            ws.getIndTitCont().setProgTit(((short)-1));
        }
    }

    @Override
    public long getTitDsRiga() {
        return titCont.getTitDsRiga();
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        this.titCont.setTitDsRiga(titDsRiga);
    }

    @Override
    public String getTitDtIniCopDb() {
        return ws.getTitContDb().getIniCopDb();
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        this.ws.getTitContDb().setIniCopDb(titDtIniCopDb);
    }

    @Override
    public String getTitDtIniCopDbObj() {
        if (ws.getIndTitCont().getDtIniCop() >= 0) {
            return getTitDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        if (titDtIniCopDbObj != null) {
            setTitDtIniCopDb(titDtIniCopDbObj);
            ws.getIndTitCont().setDtIniCop(((short)0));
        }
        else {
            ws.getIndTitCont().setDtIniCop(((short)-1));
        }
    }

    @Override
    public int getTitIdOgg() {
        return titCont.getTitIdOgg();
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        this.titCont.setTitIdOgg(titIdOgg);
    }

    @Override
    public String getTitTpOgg() {
        return titCont.getTitTpOgg();
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        this.titCont.setTitTpOgg(titTpOgg);
    }

    @Override
    public String getTitTpPreTit() {
        return titCont.getTitTpPreTit();
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        this.titCont.setTitTpPreTit(titTpPreTit);
    }

    @Override
    public String getTitTpStatTit() {
        return titCont.getTitTpStatTit();
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        this.titCont.setTitTpStatTit(titTpStatTit);
    }

    @Override
    public String getTitTpTit() {
        return titCont.getTitTpTit();
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        this.titCont.setTitTpTit(titTpTit);
    }

    @Override
    public AfDecimal getTotAcqExp() {
        return titCont.getTitTotAcqExp().getTitTotAcqExp();
    }

    @Override
    public void setTotAcqExp(AfDecimal totAcqExp) {
        this.titCont.getTitTotAcqExp().setTitTotAcqExp(totAcqExp.copy());
    }

    @Override
    public AfDecimal getTotAcqExpObj() {
        if (ws.getIndTitCont().getTotAcqExp() >= 0) {
            return getTotAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotAcqExpObj(AfDecimal totAcqExpObj) {
        if (totAcqExpObj != null) {
            setTotAcqExp(new AfDecimal(totAcqExpObj, 15, 3));
            ws.getIndTitCont().setTotAcqExp(((short)0));
        }
        else {
            ws.getIndTitCont().setTotAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarAcq() {
        return titCont.getTitTotCarAcq().getTitTotCarAcq();
    }

    @Override
    public void setTotCarAcq(AfDecimal totCarAcq) {
        this.titCont.getTitTotCarAcq().setTitTotCarAcq(totCarAcq.copy());
    }

    @Override
    public AfDecimal getTotCarAcqObj() {
        if (ws.getIndTitCont().getTotCarAcq() >= 0) {
            return getTotCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarAcqObj(AfDecimal totCarAcqObj) {
        if (totCarAcqObj != null) {
            setTotCarAcq(new AfDecimal(totCarAcqObj, 15, 3));
            ws.getIndTitCont().setTotCarAcq(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarGest() {
        return titCont.getTitTotCarGest().getTitTotCarGest();
    }

    @Override
    public void setTotCarGest(AfDecimal totCarGest) {
        this.titCont.getTitTotCarGest().setTitTotCarGest(totCarGest.copy());
    }

    @Override
    public AfDecimal getTotCarGestObj() {
        if (ws.getIndTitCont().getTotCarGest() >= 0) {
            return getTotCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarGestObj(AfDecimal totCarGestObj) {
        if (totCarGestObj != null) {
            setTotCarGest(new AfDecimal(totCarGestObj, 15, 3));
            ws.getIndTitCont().setTotCarGest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarIas() {
        return titCont.getTitTotCarIas().getTitTotCarIas();
    }

    @Override
    public void setTotCarIas(AfDecimal totCarIas) {
        this.titCont.getTitTotCarIas().setTitTotCarIas(totCarIas.copy());
    }

    @Override
    public AfDecimal getTotCarIasObj() {
        if (ws.getIndTitCont().getTotCarIas() >= 0) {
            return getTotCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarIasObj(AfDecimal totCarIasObj) {
        if (totCarIasObj != null) {
            setTotCarIas(new AfDecimal(totCarIasObj, 15, 3));
            ws.getIndTitCont().setTotCarIas(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCarInc() {
        return titCont.getTitTotCarInc().getTitTotCarInc();
    }

    @Override
    public void setTotCarInc(AfDecimal totCarInc) {
        this.titCont.getTitTotCarInc().setTitTotCarInc(totCarInc.copy());
    }

    @Override
    public AfDecimal getTotCarIncObj() {
        if (ws.getIndTitCont().getTotCarInc() >= 0) {
            return getTotCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCarIncObj(AfDecimal totCarIncObj) {
        if (totCarIncObj != null) {
            setTotCarInc(new AfDecimal(totCarIncObj, 15, 3));
            ws.getIndTitCont().setTotCarInc(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCnbtAntirac() {
        return titCont.getTitTotCnbtAntirac().getTitTotCnbtAntirac();
    }

    @Override
    public void setTotCnbtAntirac(AfDecimal totCnbtAntirac) {
        this.titCont.getTitTotCnbtAntirac().setTitTotCnbtAntirac(totCnbtAntirac.copy());
    }

    @Override
    public AfDecimal getTotCnbtAntiracObj() {
        if (ws.getIndTitCont().getTotCnbtAntirac() >= 0) {
            return getTotCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCnbtAntiracObj(AfDecimal totCnbtAntiracObj) {
        if (totCnbtAntiracObj != null) {
            setTotCnbtAntirac(new AfDecimal(totCnbtAntiracObj, 15, 3));
            ws.getIndTitCont().setTotCnbtAntirac(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCnbtAntirac(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotCommisInter() {
        return titCont.getTitTotCommisInter().getTitTotCommisInter();
    }

    @Override
    public void setTotCommisInter(AfDecimal totCommisInter) {
        this.titCont.getTitTotCommisInter().setTitTotCommisInter(totCommisInter.copy());
    }

    @Override
    public AfDecimal getTotCommisInterObj() {
        if (ws.getIndTitCont().getTotCommisInter() >= 0) {
            return getTotCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotCommisInterObj(AfDecimal totCommisInterObj) {
        if (totCommisInterObj != null) {
            setTotCommisInter(new AfDecimal(totCommisInterObj, 15, 3));
            ws.getIndTitCont().setTotCommisInter(((short)0));
        }
        else {
            ws.getIndTitCont().setTotCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotDir() {
        return titCont.getTitTotDir().getTitTotDir();
    }

    @Override
    public void setTotDir(AfDecimal totDir) {
        this.titCont.getTitTotDir().setTitTotDir(totDir.copy());
    }

    @Override
    public AfDecimal getTotDirObj() {
        if (ws.getIndTitCont().getTotDir() >= 0) {
            return getTotDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotDirObj(AfDecimal totDirObj) {
        if (totDirObj != null) {
            setTotDir(new AfDecimal(totDirObj, 15, 3));
            ws.getIndTitCont().setTotDir(((short)0));
        }
        else {
            ws.getIndTitCont().setTotDir(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrFraz() {
        return titCont.getTitTotIntrFraz().getTitTotIntrFraz();
    }

    @Override
    public void setTotIntrFraz(AfDecimal totIntrFraz) {
        this.titCont.getTitTotIntrFraz().setTitTotIntrFraz(totIntrFraz.copy());
    }

    @Override
    public AfDecimal getTotIntrFrazObj() {
        if (ws.getIndTitCont().getTotIntrFraz() >= 0) {
            return getTotIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrFrazObj(AfDecimal totIntrFrazObj) {
        if (totIntrFrazObj != null) {
            setTotIntrFraz(new AfDecimal(totIntrFrazObj, 15, 3));
            ws.getIndTitCont().setTotIntrFraz(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrMora() {
        return titCont.getTitTotIntrMora().getTitTotIntrMora();
    }

    @Override
    public void setTotIntrMora(AfDecimal totIntrMora) {
        this.titCont.getTitTotIntrMora().setTitTotIntrMora(totIntrMora.copy());
    }

    @Override
    public AfDecimal getTotIntrMoraObj() {
        if (ws.getIndTitCont().getTotIntrMora() >= 0) {
            return getTotIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrMoraObj(AfDecimal totIntrMoraObj) {
        if (totIntrMoraObj != null) {
            setTotIntrMora(new AfDecimal(totIntrMoraObj, 15, 3));
            ws.getIndTitCont().setTotIntrMora(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        return titCont.getTitTotIntrPrest().getTitTotIntrPrest();
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        this.titCont.getTitTotIntrPrest().setTitTotIntrPrest(totIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        if (ws.getIndTitCont().getTotIntrPrest() >= 0) {
            return getTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        if (totIntrPrestObj != null) {
            setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
            ws.getIndTitCont().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrRetdt() {
        return titCont.getTitTotIntrRetdt().getTitTotIntrRetdt();
    }

    @Override
    public void setTotIntrRetdt(AfDecimal totIntrRetdt) {
        this.titCont.getTitTotIntrRetdt().setTitTotIntrRetdt(totIntrRetdt.copy());
    }

    @Override
    public AfDecimal getTotIntrRetdtObj() {
        if (ws.getIndTitCont().getTotIntrRetdt() >= 0) {
            return getTotIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrRetdtObj(AfDecimal totIntrRetdtObj) {
        if (totIntrRetdtObj != null) {
            setTotIntrRetdt(new AfDecimal(totIntrRetdtObj, 15, 3));
            ws.getIndTitCont().setTotIntrRetdt(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrRiat() {
        return titCont.getTitTotIntrRiat().getTitTotIntrRiat();
    }

    @Override
    public void setTotIntrRiat(AfDecimal totIntrRiat) {
        this.titCont.getTitTotIntrRiat().setTitTotIntrRiat(totIntrRiat.copy());
    }

    @Override
    public AfDecimal getTotIntrRiatObj() {
        if (ws.getIndTitCont().getTotIntrRiat() >= 0) {
            return getTotIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrRiatObj(AfDecimal totIntrRiatObj) {
        if (totIntrRiatObj != null) {
            setTotIntrRiat(new AfDecimal(totIntrRiatObj, 15, 3));
            ws.getIndTitCont().setTotIntrRiat(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotManfeeAntic() {
        return titCont.getTitTotManfeeAntic().getTitTotManfeeAntic();
    }

    @Override
    public void setTotManfeeAntic(AfDecimal totManfeeAntic) {
        this.titCont.getTitTotManfeeAntic().setTitTotManfeeAntic(totManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTotManfeeAnticObj() {
        if (ws.getIndTitCont().getTotManfeeAntic() >= 0) {
            return getTotManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotManfeeAnticObj(AfDecimal totManfeeAnticObj) {
        if (totManfeeAnticObj != null) {
            setTotManfeeAntic(new AfDecimal(totManfeeAnticObj, 15, 3));
            ws.getIndTitCont().setTotManfeeAntic(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotManfeeRec() {
        return titCont.getTitTotManfeeRec().getTitTotManfeeRec();
    }

    @Override
    public void setTotManfeeRec(AfDecimal totManfeeRec) {
        this.titCont.getTitTotManfeeRec().setTitTotManfeeRec(totManfeeRec.copy());
    }

    @Override
    public AfDecimal getTotManfeeRecObj() {
        if (ws.getIndTitCont().getTotManfeeRec() >= 0) {
            return getTotManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotManfeeRecObj(AfDecimal totManfeeRecObj) {
        if (totManfeeRecObj != null) {
            setTotManfeeRec(new AfDecimal(totManfeeRecObj, 15, 3));
            ws.getIndTitCont().setTotManfeeRec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotManfeeRicor() {
        return titCont.getTitTotManfeeRicor().getTitTotManfeeRicor();
    }

    @Override
    public void setTotManfeeRicor(AfDecimal totManfeeRicor) {
        this.titCont.getTitTotManfeeRicor().setTitTotManfeeRicor(totManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTotManfeeRicorObj() {
        if (ws.getIndTitCont().getTotManfeeRicor() >= 0) {
            return getTotManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotManfeeRicorObj(AfDecimal totManfeeRicorObj) {
        if (totManfeeRicorObj != null) {
            setTotManfeeRicor(new AfDecimal(totManfeeRicorObj, 15, 3));
            ws.getIndTitCont().setTotManfeeRicor(((short)0));
        }
        else {
            ws.getIndTitCont().setTotManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPreNet() {
        return titCont.getTitTotPreNet().getTitTotPreNet();
    }

    @Override
    public void setTotPreNet(AfDecimal totPreNet) {
        this.titCont.getTitTotPreNet().setTitTotPreNet(totPreNet.copy());
    }

    @Override
    public AfDecimal getTotPreNetObj() {
        if (ws.getIndTitCont().getTotPreNet() >= 0) {
            return getTotPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreNetObj(AfDecimal totPreNetObj) {
        if (totPreNetObj != null) {
            setTotPreNet(new AfDecimal(totPreNetObj, 15, 3));
            ws.getIndTitCont().setTotPreNet(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPrePpIas() {
        return titCont.getTitTotPrePpIas().getTitTotPrePpIas();
    }

    @Override
    public void setTotPrePpIas(AfDecimal totPrePpIas) {
        this.titCont.getTitTotPrePpIas().setTitTotPrePpIas(totPrePpIas.copy());
    }

    @Override
    public AfDecimal getTotPrePpIasObj() {
        if (ws.getIndTitCont().getTotPrePpIas() >= 0) {
            return getTotPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPrePpIasObj(AfDecimal totPrePpIasObj) {
        if (totPrePpIasObj != null) {
            setTotPrePpIas(new AfDecimal(totPrePpIasObj, 15, 3));
            ws.getIndTitCont().setTotPrePpIas(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPreSoloRsh() {
        return titCont.getTitTotPreSoloRsh().getTitTotPreSoloRsh();
    }

    @Override
    public void setTotPreSoloRsh(AfDecimal totPreSoloRsh) {
        this.titCont.getTitTotPreSoloRsh().setTitTotPreSoloRsh(totPreSoloRsh.copy());
    }

    @Override
    public AfDecimal getTotPreSoloRshObj() {
        if (ws.getIndTitCont().getTotPreSoloRsh() >= 0) {
            return getTotPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreSoloRshObj(AfDecimal totPreSoloRshObj) {
        if (totPreSoloRshObj != null) {
            setTotPreSoloRsh(new AfDecimal(totPreSoloRshObj, 15, 3));
            ws.getIndTitCont().setTotPreSoloRsh(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPreTot() {
        return titCont.getTitTotPreTot().getTitTotPreTot();
    }

    @Override
    public void setTotPreTot(AfDecimal totPreTot) {
        this.titCont.getTitTotPreTot().setTitTotPreTot(totPreTot.copy());
    }

    @Override
    public AfDecimal getTotPreTotObj() {
        if (ws.getIndTitCont().getTotPreTot() >= 0) {
            return getTotPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreTotObj(AfDecimal totPreTotObj) {
        if (totPreTotObj != null) {
            setTotPreTot(new AfDecimal(totPreTotObj, 15, 3));
            ws.getIndTitCont().setTotPreTot(((short)0));
        }
        else {
            ws.getIndTitCont().setTotPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvAcq1aa() {
        return titCont.getTitTotProvAcq1aa().getTitTotProvAcq1aa();
    }

    @Override
    public void setTotProvAcq1aa(AfDecimal totProvAcq1aa) {
        this.titCont.getTitTotProvAcq1aa().setTitTotProvAcq1aa(totProvAcq1aa.copy());
    }

    @Override
    public AfDecimal getTotProvAcq1aaObj() {
        if (ws.getIndTitCont().getTotProvAcq1aa() >= 0) {
            return getTotProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvAcq1aaObj(AfDecimal totProvAcq1aaObj) {
        if (totProvAcq1aaObj != null) {
            setTotProvAcq1aa(new AfDecimal(totProvAcq1aaObj, 15, 3));
            ws.getIndTitCont().setTotProvAcq1aa(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvAcq2aa() {
        return titCont.getTitTotProvAcq2aa().getTitTotProvAcq2aa();
    }

    @Override
    public void setTotProvAcq2aa(AfDecimal totProvAcq2aa) {
        this.titCont.getTitTotProvAcq2aa().setTitTotProvAcq2aa(totProvAcq2aa.copy());
    }

    @Override
    public AfDecimal getTotProvAcq2aaObj() {
        if (ws.getIndTitCont().getTotProvAcq2aa() >= 0) {
            return getTotProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvAcq2aaObj(AfDecimal totProvAcq2aaObj) {
        if (totProvAcq2aaObj != null) {
            setTotProvAcq2aa(new AfDecimal(totProvAcq2aaObj, 15, 3));
            ws.getIndTitCont().setTotProvAcq2aa(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvDaRec() {
        return titCont.getTitTotProvDaRec().getTitTotProvDaRec();
    }

    @Override
    public void setTotProvDaRec(AfDecimal totProvDaRec) {
        this.titCont.getTitTotProvDaRec().setTitTotProvDaRec(totProvDaRec.copy());
    }

    @Override
    public AfDecimal getTotProvDaRecObj() {
        if (ws.getIndTitCont().getTotProvDaRec() >= 0) {
            return getTotProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvDaRecObj(AfDecimal totProvDaRecObj) {
        if (totProvDaRecObj != null) {
            setTotProvDaRec(new AfDecimal(totProvDaRecObj, 15, 3));
            ws.getIndTitCont().setTotProvDaRec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvInc() {
        return titCont.getTitTotProvInc().getTitTotProvInc();
    }

    @Override
    public void setTotProvInc(AfDecimal totProvInc) {
        this.titCont.getTitTotProvInc().setTitTotProvInc(totProvInc.copy());
    }

    @Override
    public AfDecimal getTotProvIncObj() {
        if (ws.getIndTitCont().getTotProvInc() >= 0) {
            return getTotProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvIncObj(AfDecimal totProvIncObj) {
        if (totProvIncObj != null) {
            setTotProvInc(new AfDecimal(totProvIncObj, 15, 3));
            ws.getIndTitCont().setTotProvInc(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotProvRicor() {
        return titCont.getTitTotProvRicor().getTitTotProvRicor();
    }

    @Override
    public void setTotProvRicor(AfDecimal totProvRicor) {
        this.titCont.getTitTotProvRicor().setTitTotProvRicor(totProvRicor.copy());
    }

    @Override
    public AfDecimal getTotProvRicorObj() {
        if (ws.getIndTitCont().getTotProvRicor() >= 0) {
            return getTotProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotProvRicorObj(AfDecimal totProvRicorObj) {
        if (totProvRicorObj != null) {
            setTotProvRicor(new AfDecimal(totProvRicorObj, 15, 3));
            ws.getIndTitCont().setTotProvRicor(((short)0));
        }
        else {
            ws.getIndTitCont().setTotProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotRemunAss() {
        return titCont.getTitTotRemunAss().getTitTotRemunAss();
    }

    @Override
    public void setTotRemunAss(AfDecimal totRemunAss) {
        this.titCont.getTitTotRemunAss().setTitTotRemunAss(totRemunAss.copy());
    }

    @Override
    public AfDecimal getTotRemunAssObj() {
        if (ws.getIndTitCont().getTotRemunAss() >= 0) {
            return getTotRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotRemunAssObj(AfDecimal totRemunAssObj) {
        if (totRemunAssObj != null) {
            setTotRemunAss(new AfDecimal(totRemunAssObj, 15, 3));
            ws.getIndTitCont().setTotRemunAss(((short)0));
        }
        else {
            ws.getIndTitCont().setTotRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprAlt() {
        return titCont.getTitTotSoprAlt().getTitTotSoprAlt();
    }

    @Override
    public void setTotSoprAlt(AfDecimal totSoprAlt) {
        this.titCont.getTitTotSoprAlt().setTitTotSoprAlt(totSoprAlt.copy());
    }

    @Override
    public AfDecimal getTotSoprAltObj() {
        if (ws.getIndTitCont().getTotSoprAlt() >= 0) {
            return getTotSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprAltObj(AfDecimal totSoprAltObj) {
        if (totSoprAltObj != null) {
            setTotSoprAlt(new AfDecimal(totSoprAltObj, 15, 3));
            ws.getIndTitCont().setTotSoprAlt(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprProf() {
        return titCont.getTitTotSoprProf().getTitTotSoprProf();
    }

    @Override
    public void setTotSoprProf(AfDecimal totSoprProf) {
        this.titCont.getTitTotSoprProf().setTitTotSoprProf(totSoprProf.copy());
    }

    @Override
    public AfDecimal getTotSoprProfObj() {
        if (ws.getIndTitCont().getTotSoprProf() >= 0) {
            return getTotSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprProfObj(AfDecimal totSoprProfObj) {
        if (totSoprProfObj != null) {
            setTotSoprProf(new AfDecimal(totSoprProfObj, 15, 3));
            ws.getIndTitCont().setTotSoprProf(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprSan() {
        return titCont.getTitTotSoprSan().getTitTotSoprSan();
    }

    @Override
    public void setTotSoprSan(AfDecimal totSoprSan) {
        this.titCont.getTitTotSoprSan().setTitTotSoprSan(totSoprSan.copy());
    }

    @Override
    public AfDecimal getTotSoprSanObj() {
        if (ws.getIndTitCont().getTotSoprSan() >= 0) {
            return getTotSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprSanObj(AfDecimal totSoprSanObj) {
        if (totSoprSanObj != null) {
            setTotSoprSan(new AfDecimal(totSoprSanObj, 15, 3));
            ws.getIndTitCont().setTotSoprSan(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprSpo() {
        return titCont.getTitTotSoprSpo().getTitTotSoprSpo();
    }

    @Override
    public void setTotSoprSpo(AfDecimal totSoprSpo) {
        this.titCont.getTitTotSoprSpo().setTitTotSoprSpo(totSoprSpo.copy());
    }

    @Override
    public AfDecimal getTotSoprSpoObj() {
        if (ws.getIndTitCont().getTotSoprSpo() >= 0) {
            return getTotSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprSpoObj(AfDecimal totSoprSpoObj) {
        if (totSoprSpoObj != null) {
            setTotSoprSpo(new AfDecimal(totSoprSpoObj, 15, 3));
            ws.getIndTitCont().setTotSoprSpo(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSoprTec() {
        return titCont.getTitTotSoprTec().getTitTotSoprTec();
    }

    @Override
    public void setTotSoprTec(AfDecimal totSoprTec) {
        this.titCont.getTitTotSoprTec().setTitTotSoprTec(totSoprTec.copy());
    }

    @Override
    public AfDecimal getTotSoprTecObj() {
        if (ws.getIndTitCont().getTotSoprTec() >= 0) {
            return getTotSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSoprTecObj(AfDecimal totSoprTecObj) {
        if (totSoprTecObj != null) {
            setTotSoprTec(new AfDecimal(totSoprTecObj, 15, 3));
            ws.getIndTitCont().setTotSoprTec(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSpeAge() {
        return titCont.getTitTotSpeAge().getTitTotSpeAge();
    }

    @Override
    public void setTotSpeAge(AfDecimal totSpeAge) {
        this.titCont.getTitTotSpeAge().setTitTotSpeAge(totSpeAge.copy());
    }

    @Override
    public AfDecimal getTotSpeAgeObj() {
        if (ws.getIndTitCont().getTotSpeAge() >= 0) {
            return getTotSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSpeAgeObj(AfDecimal totSpeAgeObj) {
        if (totSpeAgeObj != null) {
            setTotSpeAge(new AfDecimal(totSpeAgeObj, 15, 3));
            ws.getIndTitCont().setTotSpeAge(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotSpeMed() {
        return titCont.getTitTotSpeMed().getTitTotSpeMed();
    }

    @Override
    public void setTotSpeMed(AfDecimal totSpeMed) {
        this.titCont.getTitTotSpeMed().setTitTotSpeMed(totSpeMed.copy());
    }

    @Override
    public AfDecimal getTotSpeMedObj() {
        if (ws.getIndTitCont().getTotSpeMed() >= 0) {
            return getTotSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotSpeMedObj(AfDecimal totSpeMedObj) {
        if (totSpeMedObj != null) {
            setTotSpeMed(new AfDecimal(totSpeMedObj, 15, 3));
            ws.getIndTitCont().setTotSpeMed(((short)0));
        }
        else {
            ws.getIndTitCont().setTotSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotTax() {
        return titCont.getTitTotTax().getTitTotTax();
    }

    @Override
    public void setTotTax(AfDecimal totTax) {
        this.titCont.getTitTotTax().setTitTotTax(totTax.copy());
    }

    @Override
    public AfDecimal getTotTaxObj() {
        if (ws.getIndTitCont().getTotTax() >= 0) {
            return getTotTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotTaxObj(AfDecimal totTaxObj) {
        if (totTaxObj != null) {
            setTotTax(new AfDecimal(totTaxObj, 15, 3));
            ws.getIndTitCont().setTotTax(((short)0));
        }
        else {
            ws.getIndTitCont().setTotTax(((short)-1));
        }
    }

    @Override
    public String getTpCausDispStor() {
        return titCont.getTitTpCausDispStor();
    }

    @Override
    public void setTpCausDispStor(String tpCausDispStor) {
        this.titCont.setTitTpCausDispStor(tpCausDispStor);
    }

    @Override
    public String getTpCausDispStorObj() {
        if (ws.getIndTitCont().getTpCausDispStor() >= 0) {
            return getTpCausDispStor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausDispStorObj(String tpCausDispStorObj) {
        if (tpCausDispStorObj != null) {
            setTpCausDispStor(tpCausDispStorObj);
            ws.getIndTitCont().setTpCausDispStor(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausDispStor(((short)-1));
        }
    }

    @Override
    public String getTpCausRimb() {
        return titCont.getTitTpCausRimb();
    }

    @Override
    public void setTpCausRimb(String tpCausRimb) {
        this.titCont.setTitTpCausRimb(tpCausRimb);
    }

    @Override
    public String getTpCausRimbObj() {
        if (ws.getIndTitCont().getTpCausRimb() >= 0) {
            return getTpCausRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausRimbObj(String tpCausRimbObj) {
        if (tpCausRimbObj != null) {
            setTpCausRimb(tpCausRimbObj);
            ws.getIndTitCont().setTpCausRimb(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausRimb(((short)-1));
        }
    }

    @Override
    public int getTpCausStor() {
        return titCont.getTitTpCausStor().getTitTpCausStor();
    }

    @Override
    public void setTpCausStor(int tpCausStor) {
        this.titCont.getTitTpCausStor().setTitTpCausStor(tpCausStor);
    }

    @Override
    public Integer getTpCausStorObj() {
        if (ws.getIndTitCont().getTpCausStor() >= 0) {
            return ((Integer)getTpCausStor());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausStorObj(Integer tpCausStorObj) {
        if (tpCausStorObj != null) {
            setTpCausStor(((int)tpCausStorObj));
            ws.getIndTitCont().setTpCausStor(((short)0));
        }
        else {
            ws.getIndTitCont().setTpCausStor(((short)-1));
        }
    }

    @Override
    public String getTpEsiRid() {
        return titCont.getTitTpEsiRid();
    }

    @Override
    public void setTpEsiRid(String tpEsiRid) {
        this.titCont.setTitTpEsiRid(tpEsiRid);
    }

    @Override
    public String getTpEsiRidObj() {
        if (ws.getIndTitCont().getTpEsiRid() >= 0) {
            return getTpEsiRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpEsiRidObj(String tpEsiRidObj) {
        if (tpEsiRidObj != null) {
            setTpEsiRid(tpEsiRidObj);
            ws.getIndTitCont().setTpEsiRid(((short)0));
        }
        else {
            ws.getIndTitCont().setTpEsiRid(((short)-1));
        }
    }

    @Override
    public String getTpMezPagAdd() {
        return titCont.getTitTpMezPagAdd();
    }

    @Override
    public void setTpMezPagAdd(String tpMezPagAdd) {
        this.titCont.setTitTpMezPagAdd(tpMezPagAdd);
    }

    @Override
    public String getTpMezPagAddObj() {
        if (ws.getIndTitCont().getTpMezPagAdd() >= 0) {
            return getTpMezPagAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagAddObj(String tpMezPagAddObj) {
        if (tpMezPagAddObj != null) {
            setTpMezPagAdd(tpMezPagAddObj);
            ws.getIndTitCont().setTpMezPagAdd(((short)0));
        }
        else {
            ws.getIndTitCont().setTpMezPagAdd(((short)-1));
        }
    }

    @Override
    public String getTpTitMigraz() {
        return titCont.getTitTpTitMigraz();
    }

    @Override
    public void setTpTitMigraz(String tpTitMigraz) {
        this.titCont.setTitTpTitMigraz(tpTitMigraz);
    }

    @Override
    public String getTpTitMigrazObj() {
        if (ws.getIndTitCont().getTpTitMigraz() >= 0) {
            return getTpTitMigraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTitMigrazObj(String tpTitMigrazObj) {
        if (tpTitMigrazObj != null) {
            setTpTitMigraz(tpTitMigrazObj);
            ws.getIndTitCont().setTpTitMigraz(((short)0));
        }
        else {
            ws.getIndTitCont().setTpTitMigraz(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }

    @Override
    public long getWsTsInfinito1() {
        throw new FieldNotMappedException("wsTsInfinito1");
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        throw new FieldNotMappedException("wsTsInfinito1");
    }
}
