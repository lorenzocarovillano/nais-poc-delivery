package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Lccc1751;
import it.accenture.jnais.ws.WkCampiDiComodo;
import static java.lang.Math.abs;

/**Original name: LCCS1750<br>
 * <pre>AUTHOR.            SYNTAX VITA.
 * DATE-WRITTEN.           31 OTTOBRE 2013.
 * ****************************************************************
 *                                                                *
 *             R O U T I N E         B A T C H                    *
 *                                                                *
 *             ( PROGRAMMA COBOL CHIAMATO CON UNA CALL)           *
 *                                                                *
 * *****************************************************************
 *                ROUTINE PER IL CALCOLO DELL'ETA'
 *   INPUT :
 *     LCCC1751-DATA-INFERIORE-N    DATA INFERIORE FORMATO AAAAMMGG
 *     LCCC1751-DATA-SUPERIORE-N    DATA SUPERIORE FORMATO AAAAMMGG
 *   OUTPUT :
 *     LCCC1751-ETA-AA-ASSICURATO NUMERO ANNI E NUMERO MESI CHE
 *     LCCC1751-ETA-MM-ASSICURATO INTERCORRE TRA LE DUE DATE
 *     LCCC1751-RETURN-CODE         RETURN CODE
 *     LCCC1751-DESCRIZ-ERR         DESCRIZIONE ERRORE
 * *****************************************************************</pre>*/
public class Lccs1750 extends Program {

    //==== PROPERTIES ====
    //Original name: WK-CAMPI-DI-COMODO
    private WkCampiDiComodo wkCampiDiComodo = new WkCampiDiComodo();
    //Original name: LCCC1751
    private Lccc1751 lccc1751;

    //==== METHODS ====
    /**Original name: INIZIO<br>*/
    public long execute(Lccc1751 lccc1751) {
        ConcatUtil concatUtil = null;
        Lccs0010 lccs0010 = null;
        GenericParam wkFormato = null;
        GenericParam wkMmDiff = null;
        GenericParam wkCodiceRitorno = null;
        this.lccc1751 = lccc1751;
        // COB_CODE: MOVE ZERO                       TO LCCC1751-ETA-AA-ASSICURATO
        //                                              LCCC1751-ETA-MM-ASSICURATO
        this.lccc1751.setEtaAaAssicurato(0);
        this.lccc1751.setEtaMmAssicurato(((short)0));
        // COB_CODE: MOVE SPACES                     TO LCCC1751-RETURN-CODE
        //                                              WK-RETURN-CODE
        //                                              LCCC1751-DESCRIZ-ERR
        this.lccc1751.setReturnCode("");
        wkCampiDiComodo.getWkReturnCode().setWkReturnCode("");
        this.lccc1751.setDescrizErr("");
        // COB_CODE: MOVE LCCC1751-DATA-INFERIORE  TO WK-DATA-INFERIORE-N
        wkCampiDiComodo.getWkDataInferiore().setWkDataInferioreNFormatted(this.lccc1751.getDataInferioreFormatted());
        // COB_CODE: MOVE LCCC1751-DATA-SUPERIORE  TO WK-DATA-SUPERIORE-N
        wkCampiDiComodo.getWkDataSuperiore().setWkDataSuperioreNFormatted(this.lccc1751.getDataSuperioreFormatted());
        // COB_CODE: CALL WK-CALL-PGM      USING   WK-FORMATO,
        //                                         WK-DATA-INFERIORE,
        //                                         WK-DATA-SUPERIORE,
        //                                         WK-MM-DIFF,
        //                                         WK-CODICE-RITORNO
        //           ON EXCEPTION
        //                SET WK-INVALID-OPER      TO TRUE
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            wkFormato = new GenericParam(MarshalByteExt.chToBuffer(wkCampiDiComodo.getWkFormato()));
            wkMmDiff = new GenericParam(MarshalByteExt.strToBuffer(wkCampiDiComodo.getWkMmDiffFormatted(), WkCampiDiComodo.Len.WK_MM_DIFF));
            wkCodiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(wkCampiDiComodo.getWkCodiceRitorno()));
            lccs0010.run(wkFormato, wkCampiDiComodo, wkCampiDiComodo.getWkDataSuperiore(), wkMmDiff, wkCodiceRitorno);
            wkCampiDiComodo.setWkFormatoFromBuffer(wkFormato.getByteData());
            wkCampiDiComodo.setWkMmDiffFromBuffer(wkMmDiff.getByteData());
            wkCampiDiComodo.setWkCodiceRitornoFromBuffer(wkCodiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO WK-COD-SERVIZIO-BE
            wkCampiDiComodo.setWkCodServizioBe(wkCampiDiComodo.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA '
            //              WK-CALL-PGM
            //              DELIMITED BY SIZE
            //              INTO LCCC1751-DESCRIZ-ERR
            concatUtil = ConcatUtil.buildString(Lccc1751.Len.DESCRIZ_ERR, "ERRORE CHIAMATA ", wkCampiDiComodo.getWkCallPgmFormatted());
            this.lccc1751.setDescrizErr(concatUtil.replaceInString(this.lccc1751.getDescrizErrFormatted()));
            // COB_CODE: SET WK-INVALID-OPER      TO TRUE
            wkCampiDiComodo.getWkReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF WK-CODICE-RITORNO EQUAL ZERO
        //              MOVE  WK-RESTO             TO LCCC1751-ETA-MM-ASSICURATO
        //           ELSE
        //              SET WK-FIELD-NOT-VALUED TO TRUE
        //           END-IF.
        if (Conditions.eq(wkCampiDiComodo.getWkCodiceRitorno(), '0')) {
            // COB_CODE: IF  WK-GG-SUP < WK-GG-INF
            //               SUBTRACT  1            FROM   WK-MM-DIFF
            //           END-IF
            if (wkCampiDiComodo.getWkDataSuperiore().getGgSup() < wkCampiDiComodo.getWkDataInferiore().getGgInf()) {
                // COB_CODE: SUBTRACT  1            FROM   WK-MM-DIFF
                wkCampiDiComodo.setWkMmDiff(Trunc.toInt(abs(wkCampiDiComodo.getWkMmDiff() - 1), 5));
            }
            // COB_CODE: DIVIDE WK-MM-DIFF BY 12 GIVING WK-INTERO
            //                                REMAINDER WK-RESTO
            wkCampiDiComodo.setWkIntero(wkCampiDiComodo.getWkMmDiff() / 12);
            wkCampiDiComodo.setWkResto(((short)(wkCampiDiComodo.getWkMmDiff() % 12)));
            // COB_CODE: MOVE  WK-INTERO            TO LCCC1751-ETA-AA-ASSICURATO
            this.lccc1751.setEtaAaAssicuratoFormatted(wkCampiDiComodo.getWkInteroFormatted());
            // COB_CODE: MOVE  WK-RESTO             TO LCCC1751-ETA-MM-ASSICURATO
            this.lccc1751.setEtaMmAssicuratoFormatted(wkCampiDiComodo.getWkRestoFormatted());
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM           TO WK-COD-SERVIZIO-BE
            wkCampiDiComodo.setWkCodServizioBe(wkCampiDiComodo.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LCCS0010 COD-RIT:'
            //                   WK-CODICE-RITORNO ';'
            //              DELIMITED BY SIZE INTO LCCC1751-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Lccc1751.Len.DESCRIZ_ERR, "CHIAMATA LCCS0010 COD-RIT:", String.valueOf(wkCampiDiComodo.getWkCodiceRitorno()), ";");
            this.lccc1751.setDescrizErr(concatUtil.replaceInString(this.lccc1751.getDescrizErrFormatted()));
            // COB_CODE: SET WK-FIELD-NOT-VALUED TO TRUE
            wkCampiDiComodo.getWkReturnCode().setFieldNotValued();
        }
        // COB_CODE: MOVE  WK-RETURN-CODE          TO LCCC1751-RETURN-CODE.
        this.lccc1751.setReturnCode(wkCampiDiComodo.getWkReturnCode().getWkReturnCode());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lccs1750 getInstance() {
        return ((Lccs1750)Programs.getInstance(Lccs1750.class));
    }
}
