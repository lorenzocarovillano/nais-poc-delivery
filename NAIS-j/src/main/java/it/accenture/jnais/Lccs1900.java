package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccc1901;
import it.accenture.jnais.ws.Lccs1900Data;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.WadeAreaAdesioneLccs0005;
import it.accenture.jnais.ws.WkVariabiliLccs1900;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import javax.inject.Inject;

/**Original name: LCCS1900<br>
 * <pre>AUTHOR.             ATS NAPOLI.
 * DATE-WRITTEN.       MARZO 2015.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LCCS1900
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... GETRA
 *   FUNZIONE....... ROUTINE CONCOMITANZA
 *   DESCRIZIONE.... VERIFICA FLAG ESEGUIBILITA S/N
 * **------------------------------------------------------------***</pre>*/
public class Lccs1900 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Lccs1900Data ws = new Lccs1900Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione;
    //Original name: LCCC1901-AREA
    private Lccc1901 lccc1901;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS1900_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLccs0005 wadeAreaAdesione, Lccc1901 lccc1901) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.lccc1901 = lccc1901;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        //
        // COB_CODE: PERFORM S9000-OPERAZ-FINALI
        //              THRU EX-S9000.
        s9000OperazFinali();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lccs1900 getInstance() {
        return ((Lccs1900)Programs.getInstance(Lccs1900.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE 'S0000-OPERAZIONI-INIZIALI'    TO WK-LABEL.
        ws.getWkVariabili().setLabel("S0000-OPERAZIONI-INIZIALI");
        // COB_CODE: INITIALIZE                             WK-VARIABILI.
        initWkVariabili();
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: MOVE 'S1000-ELABORAZIONE'         TO WK-LABEL.
        ws.getWkVariabili().setLabel("S1000-ELABORAZIONE");
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO      TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: SET LCCC1901-FL-ESEGUIBILE-SI     TO TRUE
        lccc1901.getFlEseguibile().setSi();
        // COB_CODE: IF  IDSV0001-ON-LINE
        //           AND COMUN-SWITCH-FND
        //               CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001OnLine() && ws.getWsMovimento().isComunSwitchFnd()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU RECUPERA-RAPP-RETE-EX
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM RECUPERA-RAPP-RETE
                //              THRU RECUPERA-RAPP-RETE-EX
                recuperaRappRete();
            }
            //       IF WK-COD-CAN = 3 OR 4
            // COB_CODE: IF WK-COD-CAN = 4
            //              END-IF
            //           END-IF
            if (ws.getWkVariabili().getCodCan() == 4) {
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU LEGGI-PARAM-MOVI-EX
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM LEGGI-PARAM-MOVI
                    //              THRU LEGGI-PARAM-MOVI-EX
                    leggiParamMovi();
                }
                // COB_CODE: IF WK-DT-RICOR-SUCC > ZERO
                //              END-IF
                //           END-IF
                if (Characters.GT_ZERO.test(ws.getWkVariabili().getDtRicorSuccFormatted())) {
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: IF COMUN-SWITCH-FND
                        //              END-IF
                        //           END-IF
                        if (ws.getWsMovimento().isComunSwitchFnd()) {
                            // COB_CODE: IF WK-DT-RICOR-SUCC <= IDSV0001-DATA-EFFETTO
                            //              SET LCCC1901-FL-ESEGUIBILE-NO TO TRUE
                            //           END-IF
                            if (ws.getWkVariabili().getDtRicorSucc() <= areaIdsv0001.getAreaComune().getIdsv0001DataEffetto()) {
                                // COB_CODE: SET LCCC1901-FL-ESEGUIBILE-NO TO TRUE
                                lccc1901.getFlEseguibile().setNo();
                            }
                        }
                    }
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //           AND (COMUN-RISPAR-IND
                    //                OR VERSAM-AGGIUNTIVO)
                    //               END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isVersamAggiuntivo())) {
                        // COB_CODE: IF WK-DT-RICOR-SUCC <= IDSV0001-DATA-EFFETTO
                        //              END-IF
                        //           END-IF
                        if (ws.getWkVariabili().getDtRicorSucc() <= areaIdsv0001.getAreaComune().getIdsv0001DataEffetto()) {
                            // COB_CODE: IF LCCC1901-GAR-MAX > 0
                            //              END-IF
                            //           END-IF
                            if (lccc1901.getGarMax() > 0) {
                                // COB_CODE: PERFORM LEGGI-STRA-INVST
                                //              THRU LEGGI-STRA-INVST-EX
                                leggiStraInvst();
                                // COB_CODE: IF IDSV0001-ESITO-OK
                                //                 THRU VERIFICA-GRZ-EX
                                //           END-IF
                                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                                    // COB_CODE: PERFORM VERIFICA-GRZ
                                    //              THRU VERIFICA-GRZ-EX
                                    verificaGrz();
                                }
                                // COB_CODE: IF TROVATA-GAR-SI
                                //              SET LCCC1901-FL-ESEGUIBILE-NO TO TRUE
                                //           END-IF
                                if (ws.getTrovataGar().isSi()) {
                                    // COB_CODE: SET LCCC1901-FL-ESEGUIBILE-NO TO TRUE
                                    lccc1901.getFlEseguibile().setNo();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: VERIFICA-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 * --> VERIFICA I CODICI TARIFFA DA ELIMINARE/AGGIUNGERE CON LA
	 * --> STRATEGIA D INVESTIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void verificaGrz() {
        // COB_CODE: SET TROVATA-GAR-NO           TO TRUE
        ws.getTrovataGar().setNo();
        // COB_CODE: PERFORM VARYING IX-LCCC1901 FROM 1 BY 1
        //                    UNTIL  IX-LCCC1901 > LCCC1901-GAR-MAX
        //             END-PERFORM
        //           END-PERFORM.
        ws.setIxLccc1901(((short)1));
        while (!(ws.getIxLccc1901() > lccc1901.getGarMax())) {
            // COB_CODE: PERFORM VARYING IX-TAB-ALL FROM 1 BY 1
            //                     UNTIL IX-TAB-ALL > WALL-ELE-ASSET-ALL-MAX
            //               END-IF
            //           END-PERFORM
            ws.setIxTabAll(((short)1));
            while (!(ws.getIxTabAll() > ws.getWallEleAssetAllMax())) {
                // COB_CODE: IF LCCC1901-COD-TARI(IX-LCCC1901) =
                //              WALL-COD-TARI(IX-TAB-ALL)
                //              SET TROVATA-GAR-SI  TO TRUE
                //           END-IF
                if (Conditions.eq(lccc1901.getCodTariOccurs(ws.getIxLccc1901()).getLccc1901CodTari(), ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallCodTari())) {
                    // COB_CODE: SET TROVATA-GAR-SI  TO TRUE
                    ws.getTrovataGar().setSi();
                }
                ws.setIxTabAll(Trunc.toShort(ws.getIxTabAll() + 1, 4));
            }
            ws.setIxLccc1901(Trunc.toShort(ws.getIxLccc1901() + 1, 4));
        }
    }

    /**Original name: LEGGI-AST-ALLOC<br>
	 * <pre>----------------------------------------------------------------*
	 * --> RECUPERA ASSET ALLOCATION PER LA STRATEGIA DEL CONTRATTO
	 * ----------------------------------------------------------------*</pre>*/
    private void leggiAstAlloc() {
        // COB_CODE: MOVE 'LEGGI-AST-ALLOC'
        //              TO WK-LABEL
        ws.getWkVariabili().setLabel("LEGGI-AST-ALLOC");
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        //                      AST-ALLOC.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        initAstAlloc();
        // COB_CODE: MOVE ZERO
        //              TO WALL-ELE-ASSET-ALL-MAX
        ws.setWallEleAssetAllMax(((short)0));
        //--> TRATTAMENTO STORICITA
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSI0011-ID-PADRE      TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdPadre();
        // COB_CODE: MOVE SDI-ID-STRA-DI-INVST  TO ALL-ID-STRA-DI-INVST
        ws.getAstAlloc().setAllIdStraDiInvst(ws.getStraDiInvst().getSdiIdStraDiInvst());
        // COB_CODE: MOVE 'AST-ALLOC'           TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("AST-ALLOC");
        // COB_CODE: MOVE AST-ALLOC             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAstAlloc().getAstAllocFormatted());
        // COB_CODE: MOVE ZEROES                TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                         IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZEROES                TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE:      PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                           OR NOT IDSO0011-SUCCESSFUL-SQL
        //                           OR IDSV0001-ESITO-KO
        //           *---
        //                      END-IF
        //                END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //---
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            //---
            // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *--->      GESTIRE ERRORE
            //                         THRU EX-S0300
            //                      END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                //-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE:            EVALUATE TRUE
                //           *-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                //                        WHEN IDSO0011-SUCCESSFUL-SQL
                //                            TO TRUE
                //           *--->        CHIAVE NON TROVATA
                //                        WHEN IDSO0011-NOT-FOUND
                //                           END-IF
                //           *--->        ERRORE DI ACCESSO AL DB
                //                        WHEN OTHER
                //                              THRU EX-S0300
                //                      END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO AST-ALLOC
                        ws.getAstAlloc().setAstAllocFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: ADD  1
                        //             TO WALL-ELE-ASSET-ALL-MAX
                        ws.setWallEleAssetAllMax(Trunc.toShort(1 + ws.getWallEleAssetAllMax(), 4));
                        // COB_CODE: MOVE WALL-ELE-ASSET-ALL-MAX
                        //              TO IX-TAB-ALL
                        ws.setIxTabAll(TruncAbs.toShort(ws.getWallEleAssetAllMax(), 4));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-ALL
                        //              THRU VALORIZZA-OUTPUT-ALL-EX
                        valorizzaOutputAll();
                        // COB_CODE: SET WALL-ST-INV(IX-TAB-ALL)
                        //            TO TRUE
                        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getStatus().setInv();
                        // COB_CODE: MOVE IX-TAB-ALL
                        //             TO WALL-ELE-ASSET-ALL-MAX
                        ws.setWallEleAssetAllMax(ws.getIxTabAll());
                        // COB_CODE: SET IDSI0011-FETCH-NEXT
                        //            TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        //--->        CHIAVE NON TROVATA
                        break;

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: IF IDSI0011-FETCH-FIRST
                        //                 THRU EX-S0300
                        //           END-IF
                        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                            // COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                            // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005016");
                            // COB_CODE: MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
                            //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                            // COB_CODE: STRING PGM-IDBSALL0 '; '
                            //             IDSO0011-RETURN-CODE ';'
                            //             IDSO0011-SQLCODE
                            //             DELIMITED BY SIZE
                            //             INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getPgmIdbsall0Formatted()).append("; ").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                        //--->        ERRORE DI ACCESSO AL DB
                        break;

                    default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                        // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: MOVE SPACES           TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING PGM-IDBSALL0 '; '
                        //             IDSO0011-RETURN-CODE ';'
                        //             IDSO0011-SQLCODE
                        //             DELIMITED BY SIZE
                        //             INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getPgmIdbsall0Formatted()).append("; ").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //--->      GESTIRE ERRORE
                // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING PGM-IDBSALL0 '; '
                //             IDSO0011-RETURN-CODE ';'
                //             IDSO0011-SQLCODE
                //             DELIMITED BY SIZE
                //             INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getPgmIdbsall0Formatted()).append("; ").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: LEGGI-STRA-INVST<br>
	 * <pre>----------------------------------------------------------------*
	 * --> RECUPERA LA STRATEGIA D INVESTIMENTO PRESENTE SUL CONTRATTO
	 * ----------------------------------------------------------------*</pre>*/
    private void leggiStraInvst() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LEGGI-STRA-INVST'
        //              TO WK-LABEL
        ws.getWkVariabili().setLabel("LEGGI-STRA-INVST");
        // COB_CODE: INITIALIZE STRA-DI-INVST.
        initStraDiInvst();
        //
        // COB_CODE: MOVE WADE-ID-ADES
        //             TO SDI-ID-OGG
        ws.getStraDiInvst().setSdiIdOgg(wadeAreaAdesione.getLccvade1().getDati().getWadeIdAdes());
        // COB_CODE: SET ADESIONE
        //             TO TRUE
        ws.getWsTpOgg().setAdesione();
        // COB_CODE: MOVE WS-TP-OGG
        //             TO SDI-TP-OGG
        ws.getStraDiInvst().setSdiTpOgg(ws.getWsTpOgg().getWsTpOgg());
        //
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        // COB_CODE: MOVE 'STRA-DI-INVST'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("STRA-DI-INVST");
        //
        // COB_CODE: MOVE STRA-DI-INVST
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStraDiInvst().getStraDiInvstFormatted());
        //
        // COB_CODE: SET IDSI0011-SELECT
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-ID-OGGETTO
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //            TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //            TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:        IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore Dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //                               THRU EX-S0300
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //                               THRU LEGGI-AST-ALLOC-EX
            //                       WHEN OTHER
            //           *
            //           *  --> Errore Lettura
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                    // COB_CODE: MOVE '005015'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING 'STRAT.DI INVEST. NON TROVATA ' ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "STRAT.DI INVEST. NON TROVATA ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO STRA-DI-INVST
                    ws.getStraDiInvst().setStraDiInvstFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: PERFORM LEGGI-AST-ALLOC
                    //              THRU LEGGI-AST-ALLOC-EX
                    leggiAstAlloc();
                    break;

                default://
                    //  --> Errore Lettura
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                    // COB_CODE: MOVE '005015'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING 'ERRORE LETTURA STRAT.DI INVEST. ' ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE LETTURA STRAT.DI INVEST. ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore Dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA STRAT.DI INVEST.'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA STRAT.DI INVEST.");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: RECUPERA-RAPP-RETE<br>
	 * <pre>----------------------------------------------------------------*
	 * --> RECUPERA IL CANALE DI POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void recuperaRappRete() {
        // COB_CODE: MOVE 'RECUPERA-RAPP-RETE'
        //              TO WK-LABEL
        ws.getWkVariabili().setLabel("RECUPERA-RAPP-RETE");
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        //                      RAPP-RETE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        initRappRete();
        // COB_CODE: MOVE WPOL-ID-POLI            TO RRE-ID-OGG
        ws.getRappRete().getRreIdOgg().setRreIdOgg(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: SET POLIZZA                  TO TRUE
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG               TO RRE-TP-OGG
        ws.getRappRete().setRreTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                           IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE 'RAPP-RETE'             TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("RAPP-RETE");
        // COB_CODE: MOVE  RAPP-RETE              TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRappRete().getRappReteFormatted());
        // COB_CODE: MOVE  SPACES                 TO IDSI0011-BUFFER-WHERE-COND
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: SET IDSI0011-FETCH-FIRST               TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSI0011-ID-OGGETTO                TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA        TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC            TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR NOT IDSO0011-SUCCESSFUL-SQL
        //                      OR IDSI0011-CLOSE-CURSOR
        //              END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isCloseCursor())) {
            // COB_CODE: PERFORM CALL-DISPATCHER   THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->       GESTIRE ERRORE
            //                         THRU EX-S0300
            //                   END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                         WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                //                           END-IF
                //                         WHEN IDSO0011-NOT-FOUND
                //           *-->               GESTIONE ERRORE
                //                                 THRU EX-S0300
                //                         WHEN OTHER
                //           *--->              EROREDI ACCESSO AL DB
                //                                 THRU EX-S0300
                //                      END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI   TO RAPP-RETE
                        ws.getRappRete().setRappReteFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: IF RRE-COD-CAN-NULL NOT EQUAL HIGH-VALUE
                        //                 THRU B200-CLOSE-CURSOR-EX
                        //           ELSE
                        //              SET IDSI0011-FETCH-NEXT  TO TRUE
                        //           END-IF
                        if (!Characters.EQ_HIGH.test(ws.getRappRete().getRreCodCan().getRreCodCanNullFormatted())) {
                            // COB_CODE: MOVE RRE-COD-CAN
                            //             TO WK-COD-CAN
                            ws.getWkVariabili().setCodCan(ws.getRappRete().getRreCodCan().getRreCodCan());
                            // COB_CODE: PERFORM B200-CLOSE-CURSOR
                            //              THRU B200-CLOSE-CURSOR-EX
                            b200CloseCursor();
                        }
                        else {
                            // COB_CODE: SET IDSI0011-FETCH-NEXT  TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        }
                        break;

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->               GESTIONE ERRORE
                        // COB_CODE: MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL     TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                        // COB_CODE: MOVE '005016'     TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING PGM-IDBSRRE0         ';'
                        //                  IDSO0011-RETURN-CODE ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getPgmIdbsrre0Formatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;

                    default://--->              EROREDI ACCESSO AL DB
                        // COB_CODE: MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE WK-LABEL     TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                        // COB_CODE: MOVE '005016'     TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING PGM-IDBSRRE0         ';'
                        //                  IDSO0011-RETURN-CODE ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getPgmIdbsrre0Formatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->       GESTIRE ERRORE
                // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: MOVE SPACES             TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING PGM-IDBSRRE0         ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getPgmIdbsrre0Formatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: B200-CLOSE-CURSOR<br>
	 * <pre>----------------------------------------------------------------*
	 *  FORZO LA CHIUSURA DEL CURSORE SULLA RAPP-RETE
	 * ----------------------------------------------------------------*</pre>*/
    private void b200CloseCursor() {
        // COB_CODE: MOVE 'B200-CLOSE-CURSOR'      TO WK-LABEL.
        ws.getWkVariabili().setLabel("B200-CLOSE-CURSOR");
        // COB_CODE: MOVE WK-LABEL                 TO IDSV8888-AREA-DISPLAY.
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getWkVariabili().getLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY        THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        //                      IDSI0011-OPERAZIONE.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setOperazione("");
        // COB_CODE: MOVE 'RAPP-RETE'            TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("RAPP-RETE");
        // COB_CODE: MOVE  SPACES                TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE  SPACES                TO IDSI0011-BUFFER-WHERE-COND
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: SET IDSI0011-CLOSE-CURSOR                     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsv0003CloseCursor();
        // COB_CODE: SET IDSI0011-ID-OGGETTO                       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA               TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC                    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL                   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
            //                           CONTINUE
            //                       WHEN OTHER
            //           *-->              ERRORE DI ACCESSO AL DB
            //                                THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                    break;

                default://-->              ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL    TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
                    // COB_CODE: MOVE '005016'    TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                    //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                    // COB_CODE: STRING 'B200-CLOSE-CURSOR'       ';'
                    //                IDSO0011-RETURN-CODE        ';'
                    //                IDSO0011-SQLCODE
                    //                DELIMITED BY SIZE
                    //                INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append("B200-CLOSE-CURSOR").append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkVariabili().getLabel());
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
            //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
            // COB_CODE: STRING 'B200-CLOSE-CURSOR'       ';'
            //                  IDSO0011-RETURN-CODE      ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append("B200-CLOSE-CURSOR").append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: LEGGI-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 * --> RECUPERA LA PARAM_MOVI DI GENERAZIONE TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private void leggiParamMovi() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LEGGI-PARAM-MOVI'      TO WK-LABEL.
        ws.getWkVariabili().setLabel("LEGGI-PARAM-MOVI");
        // COB_CODE: MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
        ws.getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getWkVariabili().getLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: INITIALIZE PARAM-MOVI
        //                      LDBV0641
        //                      IDSI0011-BUFFER-DATI.
        initParamMovi();
        initLdbv0641();
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET IDSV0001-ESITO-OK         TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: MOVE WPOL-ID-POLI             TO LDBV0641-ID-POLI.
        ws.getLdbv0641().setIdPoli(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: MOVE 6003             TO LDBV0641-TP-MOVI-1
        ws.getLdbv0641().setTpMovi1(6003);
        //    MOVE IDSV0001-TIPO-MOVIMENTO  TO WS-MOVIMENTO
        // COB_CODE: MOVE LDBS0640                 TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs0640());
        // COB_CODE: MOVE PARAM-MOVI               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamMovi().getParamMoviFormatted());
        // COB_CODE: MOVE LDBV0641                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv0641().getLdbv0641Formatted());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE ZERO
        //              TO WK-DT-RICOR-SUCC
        ws.getWkVariabili().setDtRicorSucc(0);
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
        //                         NOT IDSO0011-SUCCESSFUL-SQL OR
        //                         NOT IDSV0001-ESITO-OK
        //               END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || !areaIdsv0001.getEsito().isIdsv0001EsitoOk())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:          IF IDSO0011-SUCCESSFUL-RC
            //                       END-EVALUATE
            //                    ELSE
            //           *->         ERRORE DISPATCHER
            //                          THRU EX-S0300
            //                    END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:             EVALUATE TRUE
                //                           WHEN IDSO0011-NOT-FOUND
                //           *-->            SE CHIAVE NON TROVATA
                //                              CONTINUE
                //                           WHEN IDSO0011-SUCCESSFUL-SQL
                //                              SET IDSI0011-FETCH-NEXT      TO TRUE
                //                           WHEN OTHER
                //           *-->            ERRORE DI ACCESSO AL DB
                //                                 THRU EX-S0300
                //                       END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND://-->            SE CHIAVE NON TROVATA
                    // COB_CODE: CONTINUE
                    //continue
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI    TO PARAM-MOVI
                        ws.getParamMovi().setParamMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: IF PMO-DT-RICOR-SUCC-NULL NOT = HIGH-VALUE
                        //                                       AND LOW-VALUE
                        //                                       AND SPACE
                        //              END-IF
                        //           END-IF
                        if (!Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted()) && !Characters.EQ_LOW.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted()) && !Characters.EQ_SPACE.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNull())) {
                            // COB_CODE: IF WK-DT-RICOR-SUCC > PMO-DT-RICOR-SUCC
                            //             CONTINUE
                            //           ELSE
                            //                 TO WK-DT-RICOR-SUCC
                            //           END-IF
                            if (ws.getWkVariabili().getDtRicorSucc() > ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc()) {
                            // COB_CODE: CONTINUE
                            //continue
                            }
                            else {
                                // COB_CODE: MOVE PMO-DT-RICOR-SUCC
                                //              TO WK-DT-RICOR-SUCC
                                ws.getWkVariabili().setDtRicorSucc(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc(), 8));
                            }
                        }
                        // COB_CODE: SET IDSI0011-FETCH-NEXT      TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        break;

                    default://-->            ERRORE DI ACCESSO AL DB
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'S1200-CERCA-PARAM-MOV'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S1200-CERCA-PARAM-MOV");
                        // COB_CODE: MOVE '005016'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                        //                  IDSO0011-RETURN-CODE     ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //->         ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1200-CERCA-PARAM-MOV'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1200-CERCA-PARAM-MOV");
                // COB_CODE: MOVE '005016'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                //                  IDSO0011-RETURN-CODE     ';'
                //                  IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S9000-OPERAZ-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazFinali() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORI
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    /**Original name: VALORIZZA-OUTPUT-ALL<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE AREE TABELLE
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVALL3
	 *    ULTIMO AGG. 31 MAR 2020
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputAll() {
        // COB_CODE: MOVE ALL-ID-AST-ALLOC
        //             TO (SF)-ID-PTF(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().setIdPtf(ws.getAstAlloc().getAllIdAstAlloc());
        // COB_CODE: MOVE ALL-ID-AST-ALLOC
        //             TO (SF)-ID-AST-ALLOC(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallIdAstAlloc(ws.getAstAlloc().getAllIdAstAlloc());
        // COB_CODE: MOVE ALL-ID-STRA-DI-INVST
        //             TO (SF)-ID-STRA-DI-INVST(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallIdStraDiInvst(ws.getAstAlloc().getAllIdStraDiInvst());
        // COB_CODE: MOVE ALL-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallIdMoviCrz(ws.getAstAlloc().getAllIdMoviCrz());
        // COB_CODE: IF ALL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllIdMoviChiu().getAllIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE ALL-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallIdMoviChiu().setWallIdMoviChiuNull(ws.getAstAlloc().getAllIdMoviChiu().getAllIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE ALL-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallIdMoviChiu().setWallIdMoviChiu(ws.getAstAlloc().getAllIdMoviChiu().getAllIdMoviChiu());
        }
        // COB_CODE: MOVE ALL-DT-INI-VLDT
        //             TO (SF)-DT-INI-VLDT(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDtIniVldt(ws.getAstAlloc().getAllDtIniVldt());
        // COB_CODE: IF ALL-DT-END-VLDT-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-VLDT-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-DT-END-VLDT(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllDtEndVldt().getAllDtEndVldtNullFormatted())) {
            // COB_CODE: MOVE ALL-DT-END-VLDT-NULL
            //             TO (SF)-DT-END-VLDT-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallDtEndVldt().setWallDtEndVldtNull(ws.getAstAlloc().getAllDtEndVldt().getAllDtEndVldtNull());
        }
        else {
            // COB_CODE: MOVE ALL-DT-END-VLDT
            //             TO (SF)-DT-END-VLDT(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallDtEndVldt().setWallDtEndVldt(ws.getAstAlloc().getAllDtEndVldt().getAllDtEndVldt());
        }
        // COB_CODE: MOVE ALL-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDtIniEff(ws.getAstAlloc().getAllDtIniEff());
        // COB_CODE: MOVE ALL-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDtEndEff(ws.getAstAlloc().getAllDtEndEff());
        // COB_CODE: MOVE ALL-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallCodCompAnia(ws.getAstAlloc().getAllCodCompAnia());
        // COB_CODE: IF ALL-COD-FND-NULL = HIGH-VALUES
        //                TO (SF)-COD-FND-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-COD-FND(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllCodFndFormatted())) {
            // COB_CODE: MOVE ALL-COD-FND-NULL
            //             TO (SF)-COD-FND-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallCodFnd(ws.getAstAlloc().getAllCodFnd());
        }
        else {
            // COB_CODE: MOVE ALL-COD-FND
            //             TO (SF)-COD-FND(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallCodFnd(ws.getAstAlloc().getAllCodFnd());
        }
        // COB_CODE: IF ALL-COD-TARI-NULL = HIGH-VALUES
        //                TO (SF)-COD-TARI-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-COD-TARI(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllCodTariFormatted())) {
            // COB_CODE: MOVE ALL-COD-TARI-NULL
            //             TO (SF)-COD-TARI-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallCodTari(ws.getAstAlloc().getAllCodTari());
        }
        else {
            // COB_CODE: MOVE ALL-COD-TARI
            //             TO (SF)-COD-TARI(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallCodTari(ws.getAstAlloc().getAllCodTari());
        }
        // COB_CODE: IF ALL-TP-APPLZ-AST-NULL = HIGH-VALUES
        //                TO (SF)-TP-APPLZ-AST-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-TP-APPLZ-AST(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllTpApplzAstFormatted())) {
            // COB_CODE: MOVE ALL-TP-APPLZ-AST-NULL
            //             TO (SF)-TP-APPLZ-AST-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallTpApplzAst(ws.getAstAlloc().getAllTpApplzAst());
        }
        else {
            // COB_CODE: MOVE ALL-TP-APPLZ-AST
            //             TO (SF)-TP-APPLZ-AST(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallTpApplzAst(ws.getAstAlloc().getAllTpApplzAst());
        }
        // COB_CODE: IF ALL-PC-RIP-AST-NULL = HIGH-VALUES
        //                TO (SF)-PC-RIP-AST-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-PC-RIP-AST(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllPcRipAst().getAllPcRipAstNullFormatted())) {
            // COB_CODE: MOVE ALL-PC-RIP-AST-NULL
            //             TO (SF)-PC-RIP-AST-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallPcRipAst().setWallPcRipAstNull(ws.getAstAlloc().getAllPcRipAst().getAllPcRipAstNull());
        }
        else {
            // COB_CODE: MOVE ALL-PC-RIP-AST
            //             TO (SF)-PC-RIP-AST(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallPcRipAst().setWallPcRipAst(Trunc.toDecimal(ws.getAstAlloc().getAllPcRipAst().getAllPcRipAst(), 6, 3));
        }
        // COB_CODE: MOVE ALL-TP-FND
        //             TO (SF)-TP-FND(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallTpFnd(ws.getAstAlloc().getAllTpFnd());
        // COB_CODE: MOVE ALL-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsRiga(ws.getAstAlloc().getAllDsRiga());
        // COB_CODE: MOVE ALL-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsOperSql(ws.getAstAlloc().getAllDsOperSql());
        // COB_CODE: MOVE ALL-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsVer(ws.getAstAlloc().getAllDsVer());
        // COB_CODE: MOVE ALL-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsTsIniCptz(ws.getAstAlloc().getAllDsTsIniCptz());
        // COB_CODE: MOVE ALL-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsTsEndCptz(ws.getAstAlloc().getAllDsTsEndCptz());
        // COB_CODE: MOVE ALL-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsUtente(ws.getAstAlloc().getAllDsUtente());
        // COB_CODE: MOVE ALL-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-ALL)
        ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallDsStatoElab(ws.getAstAlloc().getAllDsStatoElab());
        // COB_CODE: IF ALL-PERIODO-NULL = HIGH-VALUES
        //                TO (SF)-PERIODO-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-PERIODO(IX-TAB-ALL)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllPeriodo().getAllPeriodoNullFormatted())) {
            // COB_CODE: MOVE ALL-PERIODO-NULL
            //             TO (SF)-PERIODO-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallPeriodo().setWallPeriodoNull(ws.getAstAlloc().getAllPeriodo().getAllPeriodoNull());
        }
        else {
            // COB_CODE: MOVE ALL-PERIODO
            //             TO (SF)-PERIODO(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().getWallPeriodo().setWallPeriodo(ws.getAstAlloc().getAllPeriodo().getAllPeriodo());
        }
        // COB_CODE: IF ALL-TP-RIBIL-FND-NULL = HIGH-VALUES
        //                TO (SF)-TP-RIBIL-FND-NULL(IX-TAB-ALL)
        //           ELSE
        //                TO (SF)-TP-RIBIL-FND(IX-TAB-ALL)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getAstAlloc().getAllTpRibilFndFormatted())) {
            // COB_CODE: MOVE ALL-TP-RIBIL-FND-NULL
            //             TO (SF)-TP-RIBIL-FND-NULL(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallTpRibilFnd(ws.getAstAlloc().getAllTpRibilFnd());
        }
        else {
            // COB_CODE: MOVE ALL-TP-RIBIL-FND
            //             TO (SF)-TP-RIBIL-FND(IX-TAB-ALL)
            ws.getWallTabAssetAll(ws.getIxTabAll()).getLccvall1().getDati().setWallTpRibilFnd(ws.getAstAlloc().getAllTpRibilFnd());
        }
    }

    public void initWkVariabili() {
        ws.getWkVariabili().setDtRicorSuccFormatted("00000000");
        ws.getWkVariabili().setCodCan(0);
        ws.getWkVariabili().setLabel("");
    }

    public void initAstAlloc() {
        ws.getAstAlloc().setAllIdAstAlloc(0);
        ws.getAstAlloc().setAllIdStraDiInvst(0);
        ws.getAstAlloc().setAllIdMoviCrz(0);
        ws.getAstAlloc().getAllIdMoviChiu().setAllIdMoviChiu(0);
        ws.getAstAlloc().setAllDtIniVldt(0);
        ws.getAstAlloc().getAllDtEndVldt().setAllDtEndVldt(0);
        ws.getAstAlloc().setAllDtIniEff(0);
        ws.getAstAlloc().setAllDtEndEff(0);
        ws.getAstAlloc().setAllCodCompAnia(0);
        ws.getAstAlloc().setAllCodFnd("");
        ws.getAstAlloc().setAllCodTari("");
        ws.getAstAlloc().setAllTpApplzAst("");
        ws.getAstAlloc().getAllPcRipAst().setAllPcRipAst(new AfDecimal(0, 6, 3));
        ws.getAstAlloc().setAllTpFnd(Types.SPACE_CHAR);
        ws.getAstAlloc().setAllDsRiga(0);
        ws.getAstAlloc().setAllDsOperSql(Types.SPACE_CHAR);
        ws.getAstAlloc().setAllDsVer(0);
        ws.getAstAlloc().setAllDsTsIniCptz(0);
        ws.getAstAlloc().setAllDsTsEndCptz(0);
        ws.getAstAlloc().setAllDsUtente("");
        ws.getAstAlloc().setAllDsStatoElab(Types.SPACE_CHAR);
        ws.getAstAlloc().getAllPeriodo().setAllPeriodo(((short)0));
        ws.getAstAlloc().setAllTpRibilFnd("");
    }

    public void initStraDiInvst() {
        ws.getStraDiInvst().setSdiIdStraDiInvst(0);
        ws.getStraDiInvst().setSdiIdOgg(0);
        ws.getStraDiInvst().setSdiTpOgg("");
        ws.getStraDiInvst().setSdiIdMoviCrz(0);
        ws.getStraDiInvst().getSdiIdMoviChiu().setSdiIdMoviChiu(0);
        ws.getStraDiInvst().setSdiDtIniEff(0);
        ws.getStraDiInvst().setSdiDtEndEff(0);
        ws.getStraDiInvst().setSdiCodCompAnia(0);
        ws.getStraDiInvst().setSdiCodStra("");
        ws.getStraDiInvst().getSdiModGest().setSdiModGest(((short)0));
        ws.getStraDiInvst().setSdiVarRifto("");
        ws.getStraDiInvst().setSdiValVarRifto("");
        ws.getStraDiInvst().getSdiDtFis().setSdiDtFis(((short)0));
        ws.getStraDiInvst().getSdiFrqValut().setSdiFrqValut(0);
        ws.getStraDiInvst().setSdiFlIniEndPer(Types.SPACE_CHAR);
        ws.getStraDiInvst().setSdiDsRiga(0);
        ws.getStraDiInvst().setSdiDsOperSql(Types.SPACE_CHAR);
        ws.getStraDiInvst().setSdiDsVer(0);
        ws.getStraDiInvst().setSdiDsTsIniCptz(0);
        ws.getStraDiInvst().setSdiDsTsEndCptz(0);
        ws.getStraDiInvst().setSdiDsUtente("");
        ws.getStraDiInvst().setSdiDsStatoElab(Types.SPACE_CHAR);
        ws.getStraDiInvst().setSdiTpStra("");
        ws.getStraDiInvst().setSdiTpProvzaStra("");
        ws.getStraDiInvst().getSdiPcProtezione().setSdiPcProtezione(new AfDecimal(0, 6, 3));
    }

    public void initRappRete() {
        ws.getRappRete().setRreIdRappRete(0);
        ws.getRappRete().getRreIdOgg().setRreIdOgg(0);
        ws.getRappRete().setRreTpOgg("");
        ws.getRappRete().setRreIdMoviCrz(0);
        ws.getRappRete().getRreIdMoviChiu().setRreIdMoviChiu(0);
        ws.getRappRete().setRreDtIniEff(0);
        ws.getRappRete().setRreDtEndEff(0);
        ws.getRappRete().setRreCodCompAnia(0);
        ws.getRappRete().setRreTpRete("");
        ws.getRappRete().getRreTpAcqsCntrt().setRreTpAcqsCntrt(0);
        ws.getRappRete().getRreCodAcqsCntrt().setRreCodAcqsCntrt(0);
        ws.getRappRete().getRreCodPntReteIni().setRreCodPntReteIni(0);
        ws.getRappRete().getRreCodPntReteEnd().setRreCodPntReteEnd(0);
        ws.getRappRete().setRreFlPntRete1rio(Types.SPACE_CHAR);
        ws.getRappRete().getRreCodCan().setRreCodCan(0);
        ws.getRappRete().setRreDsRiga(0);
        ws.getRappRete().setRreDsOperSql(Types.SPACE_CHAR);
        ws.getRappRete().setRreDsVer(0);
        ws.getRappRete().setRreDsTsIniCptz(0);
        ws.getRappRete().setRreDsTsEndCptz(0);
        ws.getRappRete().setRreDsUtente("");
        ws.getRappRete().setRreDsStatoElab(Types.SPACE_CHAR);
        ws.getRappRete().getRreCodPntReteIniC().setRreCodPntReteIniC(0);
        ws.getRappRete().setRreMatrOprt("");
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initLdbv0641() {
        ws.getLdbv0641().setIdPoli(0);
        ws.getLdbv0641().setTpMovi1(0);
        ws.getLdbv0641().setTpMovi2(0);
        ws.getLdbv0641().setTpMovi3(0);
        ws.getLdbv0641().setTpMovi4(0);
        ws.getLdbv0641().setTpMovi5(0);
        ws.getLdbv0641().setTpMovi6(0);
        ws.getLdbv0641().setTpMovi7(0);
        ws.getLdbv0641().setTpMovi8(0);
        ws.getLdbv0641().setTpMovi9(0);
        ws.getLdbv0641().setTpMovi10(0);
        ws.getLdbv0641().setTpMovi11(0);
        ws.getLdbv0641().setTpMovi12(0);
        ws.getLdbv0641().setTpMovi13(0);
        ws.getLdbv0641().setTpMovi14(0);
        ws.getLdbv0641().setTpMovi15(0);
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
