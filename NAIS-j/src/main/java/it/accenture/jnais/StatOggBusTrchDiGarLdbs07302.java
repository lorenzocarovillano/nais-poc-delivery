package it.accenture.jnais;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;

/**Original name: StatOggBusTrchDiGarLdbs0730<br>*/
public class StatOggBusTrchDiGarLdbs07302 implements IStatOggBusTrchDiGar {

    //==== PROPERTIES ====
    private Ldbs0730 ldbs0730;

    //==== CONSTRUCTORS ====
    public StatOggBusTrchDiGarLdbs07302(Ldbs0730 ldbs0730) {
        this.ldbs0730 = ldbs0730;
    }

    //==== METHODS ====
    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return ldbs0730.getIdsv0003().getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        ldbs0730.getIdsv0003().setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLc601IdMoviCrz() {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public void setLc601IdMoviCrz(int lc601IdMoviCrz) {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public int getLc601IdTrchDiGar() {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public void setLc601IdTrchDiGar(int lc601IdTrchDiGar) {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public int getLdbi0731IdAdes() {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public int getLdbi0731IdPoli() {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public int getLdbi0731IdTrch() {
        return ldbs0730.getWs().getLdbi0731().getIdTrch();
    }

    @Override
    public void setLdbi0731IdTrch(int ldbi0731IdTrch) {
        ldbs0730.getWs().getLdbi0731().setIdTrch(ldbi0731IdTrch);
    }

    @Override
    public String getLdbi0731TpStatBus() {
        return ldbs0730.getWs().getLdbi0731().getTpStatBus();
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        ldbs0730.getWs().getLdbi0731().setTpStatBus(ldbi0731TpStatBus);
    }

    @Override
    public String getLdbv1361TpCaus01() {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public void setLdbv1361TpCaus01(String ldbv1361TpCaus01) {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public String getLdbv1361TpCaus02() {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public void setLdbv1361TpCaus02(String ldbv1361TpCaus02) {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public String getLdbv1361TpCaus03() {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public void setLdbv1361TpCaus03(String ldbv1361TpCaus03) {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public String getLdbv1361TpCaus04() {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public void setLdbv1361TpCaus04(String ldbv1361TpCaus04) {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public String getLdbv1361TpCaus05() {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public void setLdbv1361TpCaus05(String ldbv1361TpCaus05) {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public String getLdbv1361TpCaus06() {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public void setLdbv1361TpCaus06(String ldbv1361TpCaus06) {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public String getLdbv1361TpCaus07() {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public void setLdbv1361TpCaus07(String ldbv1361TpCaus07) {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public String getLdbv1361TpStatBus01() {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01) {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public String getLdbv1361TpStatBus02() {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02) {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public String getLdbv1361TpStatBus03() {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03) {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public String getLdbv1361TpStatBus04() {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04) {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public String getLdbv1361TpStatBus05() {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05) {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public String getLdbv1361TpStatBus06() {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06) {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public String getLdbv1361TpStatBus07() {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07) {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public int getLdbv2911IdAdes() {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public void setLdbv2911IdAdes(int ldbv2911IdAdes) {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public int getLdbv2911IdPoli() {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public void setLdbv2911IdPoli(int ldbv2911IdPoli) {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public AfDecimal getLdbv2911PrstzIniNewfis() {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis) {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public int getLdbv3021IdOgg() {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public void setLdbv3021IdOgg(int ldbv3021IdOgg) {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public String getLdbv3021TpCausBus10() {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10) {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public String getLdbv3021TpCausBus11() {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11) {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public String getLdbv3021TpCausBus12() {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12) {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public String getLdbv3021TpCausBus13() {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13) {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public String getLdbv3021TpCausBus14() {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14) {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public String getLdbv3021TpCausBus15() {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15) {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public String getLdbv3021TpCausBus16() {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16) {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public String getLdbv3021TpCausBus17() {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17) {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public String getLdbv3021TpCausBus18() {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18) {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public String getLdbv3021TpCausBus19() {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19) {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public String getLdbv3021TpCausBus1() {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1) {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public String getLdbv3021TpCausBus20() {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20) {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public String getLdbv3021TpCausBus2() {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2) {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public String getLdbv3021TpCausBus3() {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3) {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public String getLdbv3021TpCausBus4() {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4) {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public String getLdbv3021TpCausBus5() {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5) {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public String getLdbv3021TpCausBus6() {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6) {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public String getLdbv3021TpCausBus7() {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7) {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public String getLdbv3021TpCausBus8() {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8) {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public String getLdbv3021TpCausBus9() {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9) {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public String getLdbv3021TpOgg() {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public void setLdbv3021TpOgg(String ldbv3021TpOgg) {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public String getLdbv3021TpStatBus() {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public void setLdbv3021TpStatBus(String ldbv3021TpStatBus) {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public int getLdbv3421IdAdes() {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public int getLdbv3421IdGar() {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public int getLdbv3421IdPoli() {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public String getLdbv3421TpOgg() {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public String getLdbv3421TpStatBus1() {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public String getLdbv3421TpStatBus2() {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public int getLdbvd511IdAdes() {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public void setLdbvd511IdAdes(int ldbvd511IdAdes) {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public int getLdbvd511IdPoli() {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public void setLdbvd511IdPoli(int ldbvd511IdPoli) {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public String getLdbvd511TpCaus() {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public void setLdbvd511TpCaus(String ldbvd511TpCaus) {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public String getLdbvd511TpOgg() {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public void setLdbvd511TpOgg(String ldbvd511TpOgg) {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public String getLdbvd511TpStatBus() {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public void setLdbvd511TpStatBus(String ldbvd511TpStatBus) {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public int getLdbve251IdAdes() {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public void setLdbve251IdAdes(int ldbve251IdAdes) {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public int getLdbve251IdMovi() {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public void setLdbve251IdMovi(int ldbve251IdMovi) {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public AfDecimal getLdbve251ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public String getLdbve251TpTrch10() {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public void setLdbve251TpTrch10(String ldbve251TpTrch10) {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public String getLdbve251TpTrch11() {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public void setLdbve251TpTrch11(String ldbve251TpTrch11) {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public String getLdbve251TpTrch12() {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public void setLdbve251TpTrch12(String ldbve251TpTrch12) {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public String getLdbve251TpTrch13() {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public void setLdbve251TpTrch13(String ldbve251TpTrch13) {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public String getLdbve251TpTrch14() {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public void setLdbve251TpTrch14(String ldbve251TpTrch14) {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public String getLdbve251TpTrch15() {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public void setLdbve251TpTrch15(String ldbve251TpTrch15) {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public String getLdbve251TpTrch16() {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public void setLdbve251TpTrch16(String ldbve251TpTrch16) {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public String getLdbve251TpTrch1() {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public void setLdbve251TpTrch1(String ldbve251TpTrch1) {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public String getLdbve251TpTrch2() {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public void setLdbve251TpTrch2(String ldbve251TpTrch2) {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public String getLdbve251TpTrch3() {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public void setLdbve251TpTrch3(String ldbve251TpTrch3) {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public String getLdbve251TpTrch4() {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public void setLdbve251TpTrch4(String ldbve251TpTrch4) {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public String getLdbve251TpTrch5() {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public void setLdbve251TpTrch5(String ldbve251TpTrch5) {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public String getLdbve251TpTrch6() {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public void setLdbve251TpTrch6(String ldbve251TpTrch6) {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public String getLdbve251TpTrch7() {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public void setLdbve251TpTrch7(String ldbve251TpTrch7) {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public String getLdbve251TpTrch8() {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public void setLdbve251TpTrch8(String ldbve251TpTrch8) {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public String getLdbve251TpTrch9() {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public void setLdbve251TpTrch9(String ldbve251TpTrch9) {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public int getLdbve261IdAdes() {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public void setLdbve261IdAdes(int ldbve261IdAdes) {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public AfDecimal getLdbve261ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public String getLdbve261TpTrch10() {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public void setLdbve261TpTrch10(String ldbve261TpTrch10) {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public String getLdbve261TpTrch11() {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public void setLdbve261TpTrch11(String ldbve261TpTrch11) {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public String getLdbve261TpTrch12() {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public void setLdbve261TpTrch12(String ldbve261TpTrch12) {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public String getLdbve261TpTrch13() {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public void setLdbve261TpTrch13(String ldbve261TpTrch13) {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public String getLdbve261TpTrch14() {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public void setLdbve261TpTrch14(String ldbve261TpTrch14) {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public String getLdbve261TpTrch15() {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public void setLdbve261TpTrch15(String ldbve261TpTrch15) {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public String getLdbve261TpTrch16() {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public void setLdbve261TpTrch16(String ldbve261TpTrch16) {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public String getLdbve261TpTrch1() {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public void setLdbve261TpTrch1(String ldbve261TpTrch1) {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public String getLdbve261TpTrch2() {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public void setLdbve261TpTrch2(String ldbve261TpTrch2) {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public String getLdbve261TpTrch3() {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public void setLdbve261TpTrch3(String ldbve261TpTrch3) {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public String getLdbve261TpTrch4() {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public void setLdbve261TpTrch4(String ldbve261TpTrch4) {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public String getLdbve261TpTrch5() {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public void setLdbve261TpTrch5(String ldbve261TpTrch5) {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public String getLdbve261TpTrch6() {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public void setLdbve261TpTrch6(String ldbve261TpTrch6) {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public String getLdbve261TpTrch7() {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public void setLdbve261TpTrch7(String ldbve261TpTrch7) {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public String getLdbve261TpTrch8() {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public void setLdbve261TpTrch8(String ldbve261TpTrch8) {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public String getLdbve261TpTrch9() {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public void setLdbve261TpTrch9(String ldbve261TpTrch9) {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public int getStbCodCompAnia() {
        return ldbs0730.getWs().getStatOggBus().getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        ldbs0730.getWs().getStatOggBus().setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return ldbs0730.getWs().getStatOggBus().getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        ldbs0730.getWs().getStatOggBus().setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return ldbs0730.getWs().getStatOggBus().getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        ldbs0730.getWs().getStatOggBus().setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return ldbs0730.getWs().getStatOggBus().getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        ldbs0730.getWs().getStatOggBus().setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return ldbs0730.getWs().getStatOggBus().getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        ldbs0730.getWs().getStatOggBus().setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return ldbs0730.getWs().getStatOggBus().getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        ldbs0730.getWs().getStatOggBus().setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return ldbs0730.getWs().getStatOggBus().getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        ldbs0730.getWs().getStatOggBus().setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return ldbs0730.getWs().getStatOggBus().getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        ldbs0730.getWs().getStatOggBus().setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return ldbs0730.getWs().getIdbvstb3().getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        ldbs0730.getWs().getIdbvstb3().setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return ldbs0730.getWs().getIdbvstb3().getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        ldbs0730.getWs().getIdbvstb3().setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return ldbs0730.getWs().getStatOggBus().getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        ldbs0730.getWs().getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (ldbs0730.getWs().getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            ldbs0730.getWs().setIndStbIdMoviChiu(((short)0));
        }
        else {
            ldbs0730.getWs().setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return ldbs0730.getWs().getStatOggBus().getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        ldbs0730.getWs().getStatOggBus().setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return ldbs0730.getWs().getStatOggBus().getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        ldbs0730.getWs().getStatOggBus().setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return ldbs0730.getWs().getStatOggBus().getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        ldbs0730.getWs().getStatOggBus().setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return ldbs0730.getWs().getStatOggBus().getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        ldbs0730.getWs().getStatOggBus().setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return ldbs0730.getWs().getStatOggBus().getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        ldbs0730.getWs().getStatOggBus().setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return ldbs0730.getWs().getStatOggBus().getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        ldbs0730.getWs().getStatOggBus().setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public AfDecimal getTgaAbbAnnuUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt();
    }

    @Override
    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(tgaAbbAnnuUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbAnnuUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAbbAnnuUlt() >= 0) {
            return getTgaAbbAnnuUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj) {
        if (tgaAbbAnnuUltObj != null) {
            setTgaAbbAnnuUlt(new AfDecimal(tgaAbbAnnuUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAbbAnnuUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAbbAnnuUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni();
    }

    @Override
    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIni(tgaAbbTotIni.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAbbTotIni() >= 0) {
            return getTgaAbbTotIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj) {
        if (tgaAbbTotIniObj != null) {
            setTgaAbbTotIni(new AfDecimal(tgaAbbTotIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAbbTotIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAbbTotIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt();
    }

    @Override
    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUlt(tgaAbbTotUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAbbTotUlt() >= 0) {
            return getTgaAbbTotUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj) {
        if (tgaAbbTotUltObj != null) {
            setTgaAbbTotUlt(new AfDecimal(tgaAbbTotUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAbbTotUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAbbTotUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAcqExp() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAcqExp().getTgaAcqExp();
    }

    @Override
    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        ldbs0730.getWs().getTrchDiGar().getTgaAcqExp().setTgaAcqExp(tgaAcqExp.copy());
    }

    @Override
    public AfDecimal getTgaAcqExpObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAcqExp() >= 0) {
            return getTgaAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAcqExpObj(AfDecimal tgaAcqExpObj) {
        if (tgaAcqExpObj != null) {
            setTgaAcqExp(new AfDecimal(tgaAcqExpObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAcqExp(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqCommisInter() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter();
    }

    @Override
    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        ldbs0730.getWs().getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInter(tgaAlqCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaAlqCommisInterObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAlqCommisInter() >= 0) {
            return getTgaAlqCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj) {
        if (tgaAlqCommisInterObj != null) {
            setTgaAlqCommisInter(new AfDecimal(tgaAlqCommisInterObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAlqCommisInter(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAlqCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvAcq() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq();
    }

    @Override
    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        ldbs0730.getWs().getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcq(tgaAlqProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvAcqObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAlqProvAcq() >= 0) {
            return getTgaAlqProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj) {
        if (tgaAlqProvAcqObj != null) {
            setTgaAlqProvAcq(new AfDecimal(tgaAlqProvAcqObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAlqProvAcq(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAlqProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvInc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc();
    }

    @Override
    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        ldbs0730.getWs().getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvInc(tgaAlqProvInc.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvIncObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAlqProvInc() >= 0) {
            return getTgaAlqProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj) {
        if (tgaAlqProvIncObj != null) {
            setTgaAlqProvInc(new AfDecimal(tgaAlqProvIncObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAlqProvInc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAlqProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvRicor() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor();
    }

    @Override
    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        ldbs0730.getWs().getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicor(tgaAlqProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvRicorObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAlqProvRicor() >= 0) {
            return getTgaAlqProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj) {
        if (tgaAlqProvRicorObj != null) {
            setTgaAlqProvRicor(new AfDecimal(tgaAlqProvRicorObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAlqProvRicor(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAlqProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqRemunAss() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss();
    }

    @Override
    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        ldbs0730.getWs().getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAss(tgaAlqRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaAlqRemunAssObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAlqRemunAss() >= 0) {
            return getTgaAlqRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj) {
        if (tgaAlqRemunAssObj != null) {
            setTgaAlqRemunAss(new AfDecimal(tgaAlqRemunAssObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAlqRemunAss(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAlqRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqScon() {
        return ldbs0730.getWs().getTrchDiGar().getTgaAlqScon().getTgaAlqScon();
    }

    @Override
    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        ldbs0730.getWs().getTrchDiGar().getTgaAlqScon().setTgaAlqScon(tgaAlqScon.copy());
    }

    @Override
    public AfDecimal getTgaAlqSconObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getAlqScon() >= 0) {
            return getTgaAlqScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqSconObj(AfDecimal tgaAlqSconObj) {
        if (tgaAlqSconObj != null) {
            setTgaAlqScon(new AfDecimal(tgaAlqSconObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setAlqScon(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setAlqScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto();
    }

    @Override
    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        ldbs0730.getWs().getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(tgaBnsGiaLiqto.copy());
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqtoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getBnsGiaLiqto() >= 0) {
            return getTgaBnsGiaLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj) {
        if (tgaBnsGiaLiqtoObj != null) {
            setTgaBnsGiaLiqto(new AfDecimal(tgaBnsGiaLiqtoObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setBnsGiaLiqto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setBnsGiaLiqto(((short)-1));
        }
    }

    @Override
    public int getTgaCodCompAnia() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCodCompAnia();
    }

    @Override
    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        ldbs0730.getWs().getTrchDiGar().setTgaCodCompAnia(tgaCodCompAnia);
    }

    @Override
    public String getTgaCodDvs() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCodDvs();
    }

    @Override
    public void setTgaCodDvs(String tgaCodDvs) {
        ldbs0730.getWs().getTrchDiGar().setTgaCodDvs(tgaCodDvs);
    }

    @Override
    public AfDecimal getTgaCommisGest() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCommisGest().getTgaCommisGest();
    }

    @Override
    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        ldbs0730.getWs().getTrchDiGar().getTgaCommisGest().setTgaCommisGest(tgaCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaCommisGestObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCommisGest() >= 0) {
            return getTgaCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisGestObj(AfDecimal tgaCommisGestObj) {
        if (tgaCommisGestObj != null) {
            setTgaCommisGest(new AfDecimal(tgaCommisGestObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCommisGest(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCommisInter() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCommisInter().getTgaCommisInter();
    }

    @Override
    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        ldbs0730.getWs().getTrchDiGar().getTgaCommisInter().setTgaCommisInter(tgaCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaCommisInterObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCommisInter() >= 0) {
            return getTgaCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisInterObj(AfDecimal tgaCommisInterObj) {
        if (tgaCommisInterObj != null) {
            setTgaCommisInter(new AfDecimal(tgaCommisInterObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCommisInter(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssva() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva();
    }

    @Override
    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        ldbs0730.getWs().getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssva(tgaCosRunAssva.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc();
    }

    @Override
    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        ldbs0730.getWs().getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(tgaCosRunAssvaIdc.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdcObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCosRunAssvaIdc() >= 0) {
            return getTgaCosRunAssvaIdc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj) {
        if (tgaCosRunAssvaIdcObj != null) {
            setTgaCosRunAssvaIdc(new AfDecimal(tgaCosRunAssvaIdcObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCosRunAssvaIdc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCosRunAssvaIdc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssvaObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCosRunAssva() >= 0) {
            return getTgaCosRunAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj) {
        if (tgaCosRunAssvaObj != null) {
            setTgaCosRunAssva(new AfDecimal(tgaCosRunAssvaObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCosRunAssva(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCosRunAssva(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptInOpzRivto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto();
    }

    @Override
    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        ldbs0730.getWs().getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivto(tgaCptInOpzRivto.copy());
    }

    @Override
    public AfDecimal getTgaCptInOpzRivtoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCptInOpzRivto() >= 0) {
            return getTgaCptInOpzRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj) {
        if (tgaCptInOpzRivtoObj != null) {
            setTgaCptInOpzRivto(new AfDecimal(tgaCptInOpzRivtoObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCptInOpzRivto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCptInOpzRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptMinScad() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad();
    }

    @Override
    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        ldbs0730.getWs().getTrchDiGar().getTgaCptMinScad().setTgaCptMinScad(tgaCptMinScad.copy());
    }

    @Override
    public AfDecimal getTgaCptMinScadObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCptMinScad() >= 0) {
            return getTgaCptMinScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj) {
        if (tgaCptMinScadObj != null) {
            setTgaCptMinScad(new AfDecimal(tgaCptMinScadObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCptMinScad(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCptMinScad(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptRshMor() {
        return ldbs0730.getWs().getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor();
    }

    @Override
    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        ldbs0730.getWs().getTrchDiGar().getTgaCptRshMor().setTgaCptRshMor(tgaCptRshMor.copy());
    }

    @Override
    public AfDecimal getTgaCptRshMorObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getCptRshMor() >= 0) {
            return getTgaCptRshMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj) {
        if (tgaCptRshMorObj != null) {
            setTgaCptRshMor(new AfDecimal(tgaCptRshMorObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setCptRshMor(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setCptRshMor(((short)-1));
        }
    }

    @Override
    public char getTgaDsOperSql() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsOperSql();
    }

    @Override
    public void setTgaDsOperSql(char tgaDsOperSql) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsOperSql(tgaDsOperSql);
    }

    @Override
    public long getTgaDsRiga() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsRiga();
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsRiga(tgaDsRiga);
    }

    @Override
    public char getTgaDsStatoElab() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsStatoElab();
    }

    @Override
    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsStatoElab(tgaDsStatoElab);
    }

    @Override
    public long getTgaDsTsEndCptz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsTsEndCptz();
    }

    @Override
    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsTsEndCptz(tgaDsTsEndCptz);
    }

    @Override
    public long getTgaDsTsIniCptz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsTsIniCptz();
    }

    @Override
    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsTsIniCptz(tgaDsTsIniCptz);
    }

    @Override
    public String getTgaDsUtente() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsUtente();
    }

    @Override
    public void setTgaDsUtente(String tgaDsUtente) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsUtente(tgaDsUtente);
    }

    @Override
    public int getTgaDsVer() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDsVer();
    }

    @Override
    public void setTgaDsVer(int tgaDsVer) {
        ldbs0730.getWs().getTrchDiGar().setTgaDsVer(tgaDsVer);
    }

    @Override
    public String getTgaDtDecorDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getDecorDb();
    }

    @Override
    public void setTgaDtDecorDb(String tgaDtDecorDb) {
        ldbs0730.getWs().getTrchDiGarDb().setDecorDb(tgaDtDecorDb);
    }

    @Override
    public String getTgaDtEffStabDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getEffStabDb();
    }

    @Override
    public void setTgaDtEffStabDb(String tgaDtEffStabDb) {
        ldbs0730.getWs().getTrchDiGarDb().setEffStabDb(tgaDtEffStabDb);
    }

    @Override
    public String getTgaDtEffStabDbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDtEffStab() >= 0) {
            return getTgaDtEffStabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj) {
        if (tgaDtEffStabDbObj != null) {
            setTgaDtEffStabDb(tgaDtEffStabDbObj);
            ldbs0730.getWs().getIndTrchDiGar().setDtEffStab(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDtEffStab(((short)-1));
        }
    }

    @Override
    public String getTgaDtEmisDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getEmisDb();
    }

    @Override
    public void setTgaDtEmisDb(String tgaDtEmisDb) {
        ldbs0730.getWs().getTrchDiGarDb().setEmisDb(tgaDtEmisDb);
    }

    @Override
    public String getTgaDtEmisDbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDtEmis() >= 0) {
            return getTgaDtEmisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEmisDbObj(String tgaDtEmisDbObj) {
        if (tgaDtEmisDbObj != null) {
            setTgaDtEmisDb(tgaDtEmisDbObj);
            ldbs0730.getWs().getIndTrchDiGar().setDtEmis(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDtEmis(((short)-1));
        }
    }

    @Override
    public String getTgaDtEndEffDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getEndEffDb();
    }

    @Override
    public void setTgaDtEndEffDb(String tgaDtEndEffDb) {
        ldbs0730.getWs().getTrchDiGarDb().setEndEffDb(tgaDtEndEffDb);
    }

    @Override
    public String getTgaDtIniEffDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getIniEffDb();
    }

    @Override
    public void setTgaDtIniEffDb(String tgaDtIniEffDb) {
        ldbs0730.getWs().getTrchDiGarDb().setIniEffDb(tgaDtIniEffDb);
    }

    @Override
    public String getTgaDtIniValTarDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getIniValTarDb();
    }

    @Override
    public void setTgaDtIniValTarDb(String tgaDtIniValTarDb) {
        ldbs0730.getWs().getTrchDiGarDb().setIniValTarDb(tgaDtIniValTarDb);
    }

    @Override
    public String getTgaDtIniValTarDbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDtIniValTar() >= 0) {
            return getTgaDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj) {
        if (tgaDtIniValTarDbObj != null) {
            setTgaDtIniValTarDb(tgaDtIniValTarDbObj);
            ldbs0730.getWs().getIndTrchDiGar().setDtIniValTar(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getTgaDtScadDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getScadDb();
    }

    @Override
    public void setTgaDtScadDb(String tgaDtScadDb) {
        ldbs0730.getWs().getTrchDiGarDb().setScadDb(tgaDtScadDb);
    }

    @Override
    public String getTgaDtScadDbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDtScad() >= 0) {
            return getTgaDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtScadDbObj(String tgaDtScadDbObj) {
        if (tgaDtScadDbObj != null) {
            setTgaDtScadDb(tgaDtScadDbObj);
            ldbs0730.getWs().getIndTrchDiGar().setDtScad(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDtScad(((short)-1));
        }
    }

    @Override
    public String getTgaDtUltAdegPrePrDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getUltAdegPrePrDb();
    }

    @Override
    public void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb) {
        ldbs0730.getWs().getTrchDiGarDb().setUltAdegPrePrDb(tgaDtUltAdegPrePrDb);
    }

    @Override
    public String getTgaDtUltAdegPrePrDbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDtUltAdegPrePr() >= 0) {
            return getTgaDtUltAdegPrePrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj) {
        if (tgaDtUltAdegPrePrDbObj != null) {
            setTgaDtUltAdegPrePrDb(tgaDtUltAdegPrePrDbObj);
            ldbs0730.getWs().getIndTrchDiGar().setDtUltAdegPrePr(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDtUltAdegPrePr(((short)-1));
        }
    }

    @Override
    public String getTgaDtVldtProdDb() {
        return ldbs0730.getWs().getTrchDiGarDb().getVldtProdDb();
    }

    @Override
    public void setTgaDtVldtProdDb(String tgaDtVldtProdDb) {
        ldbs0730.getWs().getTrchDiGarDb().setVldtProdDb(tgaDtVldtProdDb);
    }

    @Override
    public String getTgaDtVldtProdDbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDtVldtProd() >= 0) {
            return getTgaDtVldtProdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj) {
        if (tgaDtVldtProdDbObj != null) {
            setTgaDtVldtProdDb(tgaDtVldtProdDbObj);
            ldbs0730.getWs().getIndTrchDiGar().setDtVldtProd(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDtVldtProd(((short)-1));
        }
    }

    @Override
    public int getTgaDurAa() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDurAa().getTgaDurAa();
    }

    @Override
    public void setTgaDurAa(int tgaDurAa) {
        ldbs0730.getWs().getTrchDiGar().getTgaDurAa().setTgaDurAa(tgaDurAa);
    }

    @Override
    public Integer getTgaDurAaObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDurAa() >= 0) {
            return ((Integer)getTgaDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAaObj(Integer tgaDurAaObj) {
        if (tgaDurAaObj != null) {
            setTgaDurAa(((int)tgaDurAaObj));
            ldbs0730.getWs().getIndTrchDiGar().setDurAa(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDurAa(((short)-1));
        }
    }

    @Override
    public int getTgaDurAbb() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDurAbb().getTgaDurAbb();
    }

    @Override
    public void setTgaDurAbb(int tgaDurAbb) {
        ldbs0730.getWs().getTrchDiGar().getTgaDurAbb().setTgaDurAbb(tgaDurAbb);
    }

    @Override
    public Integer getTgaDurAbbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDurAbb() >= 0) {
            return ((Integer)getTgaDurAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAbbObj(Integer tgaDurAbbObj) {
        if (tgaDurAbbObj != null) {
            setTgaDurAbb(((int)tgaDurAbbObj));
            ldbs0730.getWs().getIndTrchDiGar().setDurAbb(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDurAbb(((short)-1));
        }
    }

    @Override
    public int getTgaDurGg() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDurGg().getTgaDurGg();
    }

    @Override
    public void setTgaDurGg(int tgaDurGg) {
        ldbs0730.getWs().getTrchDiGar().getTgaDurGg().setTgaDurGg(tgaDurGg);
    }

    @Override
    public Integer getTgaDurGgObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDurGg() >= 0) {
            return ((Integer)getTgaDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurGgObj(Integer tgaDurGgObj) {
        if (tgaDurGgObj != null) {
            setTgaDurGg(((int)tgaDurGgObj));
            ldbs0730.getWs().getIndTrchDiGar().setDurGg(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDurGg(((short)-1));
        }
    }

    @Override
    public int getTgaDurMm() {
        return ldbs0730.getWs().getTrchDiGar().getTgaDurMm().getTgaDurMm();
    }

    @Override
    public void setTgaDurMm(int tgaDurMm) {
        ldbs0730.getWs().getTrchDiGar().getTgaDurMm().setTgaDurMm(tgaDurMm);
    }

    @Override
    public Integer getTgaDurMmObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getDurMm() >= 0) {
            return ((Integer)getTgaDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurMmObj(Integer tgaDurMmObj) {
        if (tgaDurMmObj != null) {
            setTgaDurMm(((int)tgaDurMmObj));
            ldbs0730.getWs().getIndTrchDiGar().setDurMm(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setDurMm(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa1oAssto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto();
    }

    @Override
    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        ldbs0730.getWs().getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(tgaEtaAa1oAssto);
    }

    @Override
    public Short getTgaEtaAa1oAsstoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getEtaAa1oAssto() >= 0) {
            return ((Short)getTgaEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj) {
        if (tgaEtaAa1oAsstoObj != null) {
            setTgaEtaAa1oAssto(((short)tgaEtaAa1oAsstoObj));
            ldbs0730.getWs().getIndTrchDiGar().setEtaAa1oAssto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa2oAssto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto();
    }

    @Override
    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        ldbs0730.getWs().getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(tgaEtaAa2oAssto);
    }

    @Override
    public Short getTgaEtaAa2oAsstoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getEtaAa2oAssto() >= 0) {
            return ((Short)getTgaEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj) {
        if (tgaEtaAa2oAsstoObj != null) {
            setTgaEtaAa2oAssto(((short)tgaEtaAa2oAsstoObj));
            ldbs0730.getWs().getIndTrchDiGar().setEtaAa2oAssto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa3oAssto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto();
    }

    @Override
    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        ldbs0730.getWs().getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(tgaEtaAa3oAssto);
    }

    @Override
    public Short getTgaEtaAa3oAsstoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getEtaAa3oAssto() >= 0) {
            return ((Short)getTgaEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj) {
        if (tgaEtaAa3oAsstoObj != null) {
            setTgaEtaAa3oAssto(((short)tgaEtaAa3oAsstoObj));
            ldbs0730.getWs().getIndTrchDiGar().setEtaAa3oAssto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm1oAssto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto();
    }

    @Override
    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        ldbs0730.getWs().getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(tgaEtaMm1oAssto);
    }

    @Override
    public Short getTgaEtaMm1oAsstoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getEtaMm1oAssto() >= 0) {
            return ((Short)getTgaEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj) {
        if (tgaEtaMm1oAsstoObj != null) {
            setTgaEtaMm1oAssto(((short)tgaEtaMm1oAsstoObj));
            ldbs0730.getWs().getIndTrchDiGar().setEtaMm1oAssto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm2oAssto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto();
    }

    @Override
    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        ldbs0730.getWs().getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(tgaEtaMm2oAssto);
    }

    @Override
    public Short getTgaEtaMm2oAsstoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getEtaMm2oAssto() >= 0) {
            return ((Short)getTgaEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj) {
        if (tgaEtaMm2oAsstoObj != null) {
            setTgaEtaMm2oAssto(((short)tgaEtaMm2oAsstoObj));
            ldbs0730.getWs().getIndTrchDiGar().setEtaMm2oAssto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm3oAssto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto();
    }

    @Override
    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        ldbs0730.getWs().getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(tgaEtaMm3oAssto);
    }

    @Override
    public Short getTgaEtaMm3oAsstoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getEtaMm3oAssto() >= 0) {
            return ((Short)getTgaEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj) {
        if (tgaEtaMm3oAsstoObj != null) {
            setTgaEtaMm3oAssto(((short)tgaEtaMm3oAsstoObj));
            ldbs0730.getWs().getIndTrchDiGar().setEtaMm3oAssto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public char getTgaFlCarCont() {
        return ldbs0730.getWs().getTrchDiGar().getTgaFlCarCont();
    }

    @Override
    public void setTgaFlCarCont(char tgaFlCarCont) {
        ldbs0730.getWs().getTrchDiGar().setTgaFlCarCont(tgaFlCarCont);
    }

    @Override
    public Character getTgaFlCarContObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getFlCarCont() >= 0) {
            return ((Character)getTgaFlCarCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlCarContObj(Character tgaFlCarContObj) {
        if (tgaFlCarContObj != null) {
            setTgaFlCarCont(((char)tgaFlCarContObj));
            ldbs0730.getWs().getIndTrchDiGar().setFlCarCont(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setFlCarCont(((short)-1));
        }
    }

    @Override
    public char getTgaFlImportiForz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaFlImportiForz();
    }

    @Override
    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        ldbs0730.getWs().getTrchDiGar().setTgaFlImportiForz(tgaFlImportiForz);
    }

    @Override
    public Character getTgaFlImportiForzObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getFlImportiForz() >= 0) {
            return ((Character)getTgaFlImportiForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlImportiForzObj(Character tgaFlImportiForzObj) {
        if (tgaFlImportiForzObj != null) {
            setTgaFlImportiForz(((char)tgaFlImportiForzObj));
            ldbs0730.getWs().getIndTrchDiGar().setFlImportiForz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setFlImportiForz(((short)-1));
        }
    }

    @Override
    public char getTgaFlProvForz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaFlProvForz();
    }

    @Override
    public void setTgaFlProvForz(char tgaFlProvForz) {
        ldbs0730.getWs().getTrchDiGar().setTgaFlProvForz(tgaFlProvForz);
    }

    @Override
    public Character getTgaFlProvForzObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getFlProvForz() >= 0) {
            return ((Character)getTgaFlProvForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlProvForzObj(Character tgaFlProvForzObj) {
        if (tgaFlProvForzObj != null) {
            setTgaFlProvForz(((char)tgaFlProvForzObj));
            ldbs0730.getWs().getIndTrchDiGar().setFlProvForz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setFlProvForz(((short)-1));
        }
    }

    @Override
    public String getTgaIbOgg() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIbOgg();
    }

    @Override
    public void setTgaIbOgg(String tgaIbOgg) {
        ldbs0730.getWs().getTrchDiGar().setTgaIbOgg(tgaIbOgg);
    }

    @Override
    public String getTgaIbOggObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getIbOgg() >= 0) {
            return getTgaIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIbOggObj(String tgaIbOggObj) {
        if (tgaIbOggObj != null) {
            setTgaIbOgg(tgaIbOggObj);
            ldbs0730.getWs().getIndTrchDiGar().setIbOgg(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getTgaIdAdes() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIdAdes();
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        ldbs0730.getWs().getTrchDiGar().setTgaIdAdes(tgaIdAdes);
    }

    @Override
    public int getTgaIdGar() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIdGar();
    }

    @Override
    public void setTgaIdGar(int tgaIdGar) {
        ldbs0730.getWs().getTrchDiGar().setTgaIdGar(tgaIdGar);
    }

    @Override
    public int getTgaIdMoviChiu() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu();
    }

    @Override
    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        ldbs0730.getWs().getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiu(tgaIdMoviChiu);
    }

    @Override
    public Integer getTgaIdMoviChiuObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getIdMoviChiu() >= 0) {
            return ((Integer)getTgaIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj) {
        if (tgaIdMoviChiuObj != null) {
            setTgaIdMoviChiu(((int)tgaIdMoviChiuObj));
            ldbs0730.getWs().getIndTrchDiGar().setIdMoviChiu(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTgaIdMoviCrz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIdMoviCrz();
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        ldbs0730.getWs().getTrchDiGar().setTgaIdMoviCrz(tgaIdMoviCrz);
    }

    @Override
    public int getTgaIdPoli() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIdPoli();
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        ldbs0730.getWs().getTrchDiGar().setTgaIdPoli(tgaIdPoli);
    }

    @Override
    public int getTgaIdTrchDiGar() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIdTrchDiGar();
    }

    @Override
    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        ldbs0730.getWs().getTrchDiGar().setTgaIdTrchDiGar(tgaIdTrchDiGar);
    }

    @Override
    public AfDecimal getTgaImpAder() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpAder().getTgaImpAder();
    }

    @Override
    public void setTgaImpAder(AfDecimal tgaImpAder) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpAder().setTgaImpAder(tgaImpAder.copy());
    }

    @Override
    public AfDecimal getTgaImpAderObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpAder() >= 0) {
            return getTgaImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAderObj(AfDecimal tgaImpAderObj) {
        if (tgaImpAderObj != null) {
            setTgaImpAder(new AfDecimal(tgaImpAderObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpAder(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAltSopr() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr();
    }

    @Override
    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSopr(tgaImpAltSopr.copy());
    }

    @Override
    public AfDecimal getTgaImpAltSoprObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpAltSopr() >= 0) {
            return getTgaImpAltSopr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj) {
        if (tgaImpAltSoprObj != null) {
            setTgaImpAltSopr(new AfDecimal(tgaImpAltSoprObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpAltSopr(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpAltSopr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpAz().getTgaImpAz();
    }

    @Override
    public void setTgaImpAz(AfDecimal tgaImpAz) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpAz().setTgaImpAz(tgaImpAz.copy());
    }

    @Override
    public AfDecimal getTgaImpAzObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpAz() >= 0) {
            return getTgaImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAzObj(AfDecimal tgaImpAzObj) {
        if (tgaImpAzObj != null) {
            setTgaImpAz(new AfDecimal(tgaImpAzObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpAz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpBns() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpBns().getTgaImpBns();
    }

    @Override
    public AfDecimal getTgaImpBnsAntic() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic();
    }

    @Override
    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAntic(tgaImpBnsAntic.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsAnticObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpBnsAntic() >= 0) {
            return getTgaImpBnsAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj) {
        if (tgaImpBnsAnticObj != null) {
            setTgaImpBnsAntic(new AfDecimal(tgaImpBnsAnticObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpBnsAntic(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpBnsAntic(((short)-1));
        }
    }

    @Override
    public void setTgaImpBns(AfDecimal tgaImpBns) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpBns().setTgaImpBns(tgaImpBns.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpBns() >= 0) {
            return getTgaImpBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsObj(AfDecimal tgaImpBnsObj) {
        if (tgaImpBnsObj != null) {
            setTgaImpBns(new AfDecimal(tgaImpBnsObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpBns(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarAcq() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq();
    }

    @Override
    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcq(tgaImpCarAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpCarAcqObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpCarAcq() >= 0) {
            return getTgaImpCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj) {
        if (tgaImpCarAcqObj != null) {
            setTgaImpCarAcq(new AfDecimal(tgaImpCarAcqObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpCarAcq(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarGest() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest();
    }

    @Override
    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpCarGest().setTgaImpCarGest(tgaImpCarGest.copy());
    }

    @Override
    public AfDecimal getTgaImpCarGestObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpCarGest() >= 0) {
            return getTgaImpCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj) {
        if (tgaImpCarGestObj != null) {
            setTgaImpCarGest(new AfDecimal(tgaImpCarGestObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpCarGest(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarInc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc();
    }

    @Override
    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpCarInc().setTgaImpCarInc(tgaImpCarInc.copy());
    }

    @Override
    public AfDecimal getTgaImpCarIncObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpCarInc() >= 0) {
            return getTgaImpCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj) {
        if (tgaImpCarIncObj != null) {
            setTgaImpCarInc(new AfDecimal(tgaImpCarIncObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpCarInc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpScon() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpScon().getTgaImpScon();
    }

    @Override
    public void setTgaImpScon(AfDecimal tgaImpScon) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpScon().setTgaImpScon(tgaImpScon.copy());
    }

    @Override
    public AfDecimal getTgaImpSconObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpScon() >= 0) {
            return getTgaImpScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSconObj(AfDecimal tgaImpSconObj) {
        if (tgaImpSconObj != null) {
            setTgaImpScon(new AfDecimal(tgaImpSconObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpScon(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprProf() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf();
    }

    @Override
    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProf(tgaImpSoprProf.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprProfObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpSoprProf() >= 0) {
            return getTgaImpSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj) {
        if (tgaImpSoprProfObj != null) {
            setTgaImpSoprProf(new AfDecimal(tgaImpSoprProfObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprProf(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSan() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan();
    }

    @Override
    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSan(tgaImpSoprSan.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSanObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpSoprSan() >= 0) {
            return getTgaImpSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj) {
        if (tgaImpSoprSanObj != null) {
            setTgaImpSoprSan(new AfDecimal(tgaImpSoprSanObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprSan(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSpo() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo();
    }

    @Override
    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpo(tgaImpSoprSpo.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSpoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpSoprSpo() >= 0) {
            return getTgaImpSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj) {
        if (tgaImpSoprSpoObj != null) {
            setTgaImpSoprSpo(new AfDecimal(tgaImpSoprSpoObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprSpo(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprTec() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec();
    }

    @Override
    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTec(tgaImpSoprTec.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprTecObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpSoprTec() >= 0) {
            return getTgaImpSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj) {
        if (tgaImpSoprTecObj != null) {
            setTgaImpSoprTec(new AfDecimal(tgaImpSoprTecObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprTec(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfr() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpTfr().getTgaImpTfr();
    }

    @Override
    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpTfr().setTgaImpTfr(tgaImpTfr.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpTfr() >= 0) {
            return getTgaImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrObj(AfDecimal tgaImpTfrObj) {
        if (tgaImpTfrObj != null) {
            setTgaImpTfr(new AfDecimal(tgaImpTfrObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpTfr(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfrStrc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc();
    }

    @Override
    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrc(tgaImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrStrcObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpTfrStrc() >= 0) {
            return getTgaImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj) {
        if (tgaImpTfrStrcObj != null) {
            setTgaImpTfrStrc(new AfDecimal(tgaImpTfrStrcObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpTfrStrc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTrasfe() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe();
    }

    @Override
    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfe(tgaImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTgaImpTrasfeObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpTrasfe() >= 0) {
            return getTgaImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj) {
        if (tgaImpTrasfeObj != null) {
            setTgaImpTrasfe(new AfDecimal(tgaImpTrasfeObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpTrasfe(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpVolo() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpVolo().getTgaImpVolo();
    }

    @Override
    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpVolo().setTgaImpVolo(tgaImpVolo.copy());
    }

    @Override
    public AfDecimal getTgaImpVoloObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpVolo() >= 0) {
            return getTgaImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpVoloObj(AfDecimal tgaImpVoloObj) {
        if (tgaImpVoloObj != null) {
            setTgaImpVolo(new AfDecimal(tgaImpVoloObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpVolo(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbCommisInter() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter();
    }

    @Override
    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInter(tgaImpbCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaImpbCommisInterObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpbCommisInter() >= 0) {
            return getTgaImpbCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj) {
        if (tgaImpbCommisInterObj != null) {
            setTgaImpbCommisInter(new AfDecimal(tgaImpbCommisInterObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpbCommisInter(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpbCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvAcq() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq();
    }

    @Override
    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcq(tgaImpbProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvAcqObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpbProvAcq() >= 0) {
            return getTgaImpbProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj) {
        if (tgaImpbProvAcqObj != null) {
            setTgaImpbProvAcq(new AfDecimal(tgaImpbProvAcqObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpbProvAcq(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpbProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvInc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc();
    }

    @Override
    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvInc(tgaImpbProvInc.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvIncObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpbProvInc() >= 0) {
            return getTgaImpbProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj) {
        if (tgaImpbProvIncObj != null) {
            setTgaImpbProvInc(new AfDecimal(tgaImpbProvIncObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpbProvInc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpbProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvRicor() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor();
    }

    @Override
    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicor(tgaImpbProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvRicorObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpbProvRicor() >= 0) {
            return getTgaImpbProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj) {
        if (tgaImpbProvRicorObj != null) {
            setTgaImpbProvRicor(new AfDecimal(tgaImpbProvRicorObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpbProvRicor(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpbProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbRemunAss() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss();
    }

    @Override
    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAss(tgaImpbRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaImpbRemunAssObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpbRemunAss() >= 0) {
            return getTgaImpbRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj) {
        if (tgaImpbRemunAssObj != null) {
            setTgaImpbRemunAss(new AfDecimal(tgaImpbRemunAssObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpbRemunAss(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpbRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000() {
        return ldbs0730.getWs().getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000();
    }

    @Override
    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        ldbs0730.getWs().getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(tgaImpbVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000Obj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getImpbVisEnd2000() >= 0) {
            return getTgaImpbVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj) {
        if (tgaImpbVisEnd2000Obj != null) {
            setTgaImpbVisEnd2000(new AfDecimal(tgaImpbVisEnd2000Obj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setImpbVisEnd2000(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setImpbVisEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPre() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIncrPre().getTgaIncrPre();
    }

    @Override
    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        ldbs0730.getWs().getTrchDiGar().getTgaIncrPre().setTgaIncrPre(tgaIncrPre.copy());
    }

    @Override
    public AfDecimal getTgaIncrPreObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getIncrPre() >= 0) {
            return getTgaIncrPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPreObj(AfDecimal tgaIncrPreObj) {
        if (tgaIncrPreObj != null) {
            setTgaIncrPre(new AfDecimal(tgaIncrPreObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setIncrPre(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setIncrPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPrstz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz();
    }

    @Override
    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        ldbs0730.getWs().getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstz(tgaIncrPrstz.copy());
    }

    @Override
    public AfDecimal getTgaIncrPrstzObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getIncrPrstz() >= 0) {
            return getTgaIncrPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj) {
        if (tgaIncrPrstzObj != null) {
            setTgaIncrPrstz(new AfDecimal(tgaIncrPrstzObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setIncrPrstz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setIncrPrstz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIntrMora() {
        return ldbs0730.getWs().getTrchDiGar().getTgaIntrMora().getTgaIntrMora();
    }

    @Override
    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        ldbs0730.getWs().getTrchDiGar().getTgaIntrMora().setTgaIntrMora(tgaIntrMora.copy());
    }

    @Override
    public AfDecimal getTgaIntrMoraObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getIntrMora() >= 0) {
            return getTgaIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj) {
        if (tgaIntrMoraObj != null) {
            setTgaIntrMora(new AfDecimal(tgaIntrMoraObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setIntrMora(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeAntic() {
        return ldbs0730.getWs().getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic();
    }

    @Override
    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        ldbs0730.getWs().getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAntic(tgaManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTgaManfeeAnticObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getManfeeAntic() >= 0) {
            return getTgaManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj) {
        if (tgaManfeeAnticObj != null) {
            setTgaManfeeAntic(new AfDecimal(tgaManfeeAnticObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setManfeeAntic(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeRicor() {
        return ldbs0730.getWs().getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor();
    }

    @Override
    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        ldbs0730.getWs().getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicor(tgaManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTgaManfeeRicorObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getManfeeRicor() >= 0) {
            return getTgaManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj) {
        if (tgaManfeeRicorObj != null) {
            setTgaManfeeRicor(new AfDecimal(tgaManfeeRicorObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setManfeeRicor(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMatuEnd2000() {
        return ldbs0730.getWs().getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000();
    }

    @Override
    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        ldbs0730.getWs().getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000(tgaMatuEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaMatuEnd2000Obj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getMatuEnd2000() >= 0) {
            return getTgaMatuEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj) {
        if (tgaMatuEnd2000Obj != null) {
            setTgaMatuEnd2000(new AfDecimal(tgaMatuEnd2000Obj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setMatuEnd2000(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setMatuEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinGarto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaMinGarto().getTgaMinGarto();
    }

    @Override
    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        ldbs0730.getWs().getTrchDiGar().getTgaMinGarto().setTgaMinGarto(tgaMinGarto.copy());
    }

    @Override
    public AfDecimal getTgaMinGartoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getMinGarto() >= 0) {
            return getTgaMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinGartoObj(AfDecimal tgaMinGartoObj) {
        if (tgaMinGartoObj != null) {
            setTgaMinGarto(new AfDecimal(tgaMinGartoObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setMinGarto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setMinGarto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinTrnut() {
        return ldbs0730.getWs().getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut();
    }

    @Override
    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        ldbs0730.getWs().getTrchDiGar().getTgaMinTrnut().setTgaMinTrnut(tgaMinTrnut.copy());
    }

    @Override
    public AfDecimal getTgaMinTrnutObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getMinTrnut() >= 0) {
            return getTgaMinTrnut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj) {
        if (tgaMinTrnutObj != null) {
            setTgaMinTrnut(new AfDecimal(tgaMinTrnutObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setMinTrnut(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setMinTrnut(((short)-1));
        }
    }

    @Override
    public String getTgaModCalc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaModCalc();
    }

    @Override
    public void setTgaModCalc(String tgaModCalc) {
        ldbs0730.getWs().getTrchDiGar().setTgaModCalc(tgaModCalc);
    }

    @Override
    public String getTgaModCalcObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getModCalc() >= 0) {
            return getTgaModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaModCalcObj(String tgaModCalcObj) {
        if (tgaModCalcObj != null) {
            setTgaModCalc(tgaModCalcObj);
            ldbs0730.getWs().getIndTrchDiGar().setModCalc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setModCalc(((short)-1));
        }
    }

    @Override
    public int getTgaNumGgRival() {
        return ldbs0730.getWs().getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival();
    }

    @Override
    public void setTgaNumGgRival(int tgaNumGgRival) {
        ldbs0730.getWs().getTrchDiGar().getTgaNumGgRival().setTgaNumGgRival(tgaNumGgRival);
    }

    @Override
    public Integer getTgaNumGgRivalObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getNumGgRival() >= 0) {
            return ((Integer)getTgaNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj) {
        if (tgaNumGgRivalObj != null) {
            setTgaNumGgRival(((int)tgaNumGgRivalObj));
            ldbs0730.getWs().getIndTrchDiGar().setNumGgRival(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaOldTsTec() {
        return ldbs0730.getWs().getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec();
    }

    @Override
    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        ldbs0730.getWs().getTrchDiGar().getTgaOldTsTec().setTgaOldTsTec(tgaOldTsTec.copy());
    }

    @Override
    public AfDecimal getTgaOldTsTecObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getOldTsTec() >= 0) {
            return getTgaOldTsTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj) {
        if (tgaOldTsTecObj != null) {
            setTgaOldTsTec(new AfDecimal(tgaOldTsTecObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setOldTsTec(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setOldTsTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcCommisGest() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest();
    }

    @Override
    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        ldbs0730.getWs().getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGest(tgaPcCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaPcCommisGestObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPcCommisGest() >= 0) {
            return getTgaPcCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj) {
        if (tgaPcCommisGestObj != null) {
            setTgaPcCommisGest(new AfDecimal(tgaPcCommisGestObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPcCommisGest(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPcCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcIntrRiat() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat();
    }

    @Override
    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        ldbs0730.getWs().getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiat(tgaPcIntrRiat.copy());
    }

    @Override
    public AfDecimal getTgaPcIntrRiatObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPcIntrRiat() >= 0) {
            return getTgaPcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj) {
        if (tgaPcIntrRiatObj != null) {
            setTgaPcIntrRiat(new AfDecimal(tgaPcIntrRiatObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPcIntrRiat(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPcIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRetr() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPcRetr().getTgaPcRetr();
    }

    @Override
    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        ldbs0730.getWs().getTrchDiGar().getTgaPcRetr().setTgaPcRetr(tgaPcRetr.copy());
    }

    @Override
    public AfDecimal getTgaPcRetrObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPcRetr() >= 0) {
            return getTgaPcRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRetrObj(AfDecimal tgaPcRetrObj) {
        if (tgaPcRetrObj != null) {
            setTgaPcRetr(new AfDecimal(tgaPcRetrObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPcRetr(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPcRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRipPre() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre();
    }

    @Override
    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        ldbs0730.getWs().getTrchDiGar().getTgaPcRipPre().setTgaPcRipPre(tgaPcRipPre.copy());
    }

    @Override
    public AfDecimal getTgaPcRipPreObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPcRipPre() >= 0) {
            return getTgaPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj) {
        if (tgaPcRipPreObj != null) {
            setTgaPcRipPre(new AfDecimal(tgaPcRipPreObj, 6, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPcRipPre(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPcRipPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreAttDiTrch() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch();
    }

    @Override
    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrch(tgaPreAttDiTrch.copy());
    }

    @Override
    public AfDecimal getTgaPreAttDiTrchObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreAttDiTrch() >= 0) {
            return getTgaPreAttDiTrch();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj) {
        if (tgaPreAttDiTrchObj != null) {
            setTgaPreAttDiTrch(new AfDecimal(tgaPreAttDiTrchObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreAttDiTrch(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreAttDiTrch(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreCasoMor() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor();
    }

    @Override
    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMor(tgaPreCasoMor.copy());
    }

    @Override
    public AfDecimal getTgaPreCasoMorObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreCasoMor() >= 0) {
            return getTgaPreCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj) {
        if (tgaPreCasoMorObj != null) {
            setTgaPreCasoMor(new AfDecimal(tgaPreCasoMorObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreCasoMor(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreIniNet() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet();
    }

    @Override
    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreIniNet().setTgaPreIniNet(tgaPreIniNet.copy());
    }

    @Override
    public AfDecimal getTgaPreIniNetObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreIniNet() >= 0) {
            return getTgaPreIniNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj) {
        if (tgaPreIniNetObj != null) {
            setTgaPreIniNet(new AfDecimal(tgaPreIniNetObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreIniNet(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreIniNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni();
    }

    @Override
    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIni(tgaPreInvrioIni.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreInvrioIni() >= 0) {
            return getTgaPreInvrioIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj) {
        if (tgaPreInvrioIniObj != null) {
            setTgaPreInvrioIni(new AfDecimal(tgaPreInvrioIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreInvrioIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreInvrioIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt();
    }

    @Override
    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUlt(tgaPreInvrioUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreInvrioUlt() >= 0) {
            return getTgaPreInvrioUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj) {
        if (tgaPreInvrioUltObj != null) {
            setTgaPreInvrioUlt(new AfDecimal(tgaPreInvrioUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreInvrioUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreInvrioUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreLrd() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreLrd().getTgaPreLrd();
    }

    @Override
    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreLrd().setTgaPreLrd(tgaPreLrd.copy());
    }

    @Override
    public AfDecimal getTgaPreLrdObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreLrd() >= 0) {
            return getTgaPreLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreLrdObj(AfDecimal tgaPreLrdObj) {
        if (tgaPreLrdObj != null) {
            setTgaPreLrd(new AfDecimal(tgaPreLrdObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreLrd(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePattuito() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito();
    }

    @Override
    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrePattuito().setTgaPrePattuito(tgaPrePattuito.copy());
    }

    @Override
    public AfDecimal getTgaPrePattuitoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrePattuito() >= 0) {
            return getTgaPrePattuito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj) {
        if (tgaPrePattuitoObj != null) {
            setTgaPrePattuito(new AfDecimal(tgaPrePattuitoObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrePattuito(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrePattuito(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni();
    }

    @Override
    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrePpIni().setTgaPrePpIni(tgaPrePpIni.copy());
    }

    @Override
    public AfDecimal getTgaPrePpIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrePpIni() >= 0) {
            return getTgaPrePpIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj) {
        if (tgaPrePpIniObj != null) {
            setTgaPrePpIni(new AfDecimal(tgaPrePpIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrePpIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrePpIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt();
    }

    @Override
    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUlt(tgaPrePpUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrePpUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrePpUlt() >= 0) {
            return getTgaPrePpUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj) {
        if (tgaPrePpUltObj != null) {
            setTgaPrePpUlt(new AfDecimal(tgaPrePpUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrePpUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrePpUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreRivto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreRivto().getTgaPreRivto();
    }

    @Override
    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreRivto().setTgaPreRivto(tgaPreRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreRivtoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreRivto() >= 0) {
            return getTgaPreRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj) {
        if (tgaPreRivtoObj != null) {
            setTgaPreRivto(new AfDecimal(tgaPreRivtoObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreRivto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreStab() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreStab().getTgaPreStab();
    }

    @Override
    public void setTgaPreStab(AfDecimal tgaPreStab) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreStab().setTgaPreStab(tgaPreStab.copy());
    }

    @Override
    public AfDecimal getTgaPreStabObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreStab() >= 0) {
            return getTgaPreStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreStabObj(AfDecimal tgaPreStabObj) {
        if (tgaPreStabObj != null) {
            setTgaPreStab(new AfDecimal(tgaPreStabObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreStab(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni();
    }

    @Override
    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreTariIni().setTgaPreTariIni(tgaPreTariIni.copy());
    }

    @Override
    public AfDecimal getTgaPreTariIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreTariIni() >= 0) {
            return getTgaPreTariIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj) {
        if (tgaPreTariIniObj != null) {
            setTgaPreTariIni(new AfDecimal(tgaPreTariIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreTariIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreTariIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt();
    }

    @Override
    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUlt(tgaPreTariUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreTariUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreTariUlt() >= 0) {
            return getTgaPreTariUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj) {
        if (tgaPreTariUltObj != null) {
            setTgaPreTariUlt(new AfDecimal(tgaPreTariUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreTariUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreTariUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreUniRivto() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto();
    }

    @Override
    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        ldbs0730.getWs().getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivto(tgaPreUniRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreUniRivtoObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPreUniRivto() >= 0) {
            return getTgaPreUniRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj) {
        if (tgaPreUniRivtoObj != null) {
            setTgaPreUniRivto(new AfDecimal(tgaPreUniRivtoObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPreUniRivto(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPreUniRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv1aaAcq() {
        return ldbs0730.getWs().getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq();
    }

    @Override
    public void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq) {
        ldbs0730.getWs().getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcq(tgaProv1aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv1aaAcqObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getProv1aaAcq() >= 0) {
            return getTgaProv1aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj) {
        if (tgaProv1aaAcqObj != null) {
            setTgaProv1aaAcq(new AfDecimal(tgaProv1aaAcqObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setProv1aaAcq(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setProv1aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv2aaAcq() {
        return ldbs0730.getWs().getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq();
    }

    @Override
    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        ldbs0730.getWs().getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcq(tgaProv2aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv2aaAcqObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getProv2aaAcq() >= 0) {
            return getTgaProv2aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj) {
        if (tgaProv2aaAcqObj != null) {
            setTgaProv2aaAcq(new AfDecimal(tgaProv2aaAcqObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setProv2aaAcq(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setProv2aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvInc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaProvInc().getTgaProvInc();
    }

    @Override
    public void setTgaProvInc(AfDecimal tgaProvInc) {
        ldbs0730.getWs().getTrchDiGar().getTgaProvInc().setTgaProvInc(tgaProvInc.copy());
    }

    @Override
    public AfDecimal getTgaProvIncObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getProvInc() >= 0) {
            return getTgaProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvIncObj(AfDecimal tgaProvIncObj) {
        if (tgaProvIncObj != null) {
            setTgaProvInc(new AfDecimal(tgaProvIncObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setProvInc(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvRicor() {
        return ldbs0730.getWs().getTrchDiGar().getTgaProvRicor().getTgaProvRicor();
    }

    @Override
    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        ldbs0730.getWs().getTrchDiGar().getTgaProvRicor().setTgaProvRicor(tgaProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaProvRicorObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getProvRicor() >= 0) {
            return getTgaProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvRicorObj(AfDecimal tgaProvRicorObj) {
        if (tgaProvRicorObj != null) {
            setTgaProvRicor(new AfDecimal(tgaProvRicorObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setProvRicor(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni();
    }

    @Override
    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIni(tgaPrstzAggIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzAggIni() >= 0) {
            return getTgaPrstzAggIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj) {
        if (tgaPrstzAggIniObj != null) {
            setTgaPrstzAggIni(new AfDecimal(tgaPrstzAggIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzAggIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzAggIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt();
    }

    @Override
    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUlt(tgaPrstzAggUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzAggUlt() >= 0) {
            return getTgaPrstzAggUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj) {
        if (tgaPrstzAggUltObj != null) {
            setTgaPrstzAggUlt(new AfDecimal(tgaPrstzAggUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzAggUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzAggUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni();
    }

    @Override
    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzIni().setTgaPrstzIni(tgaPrstzIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfis() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis();
    }

    @Override
    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(tgaPrstzIniNewfis.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfisObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzIniNewfis() >= 0) {
            return getTgaPrstzIniNewfis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj) {
        if (tgaPrstzIniNewfisObj != null) {
            setTgaPrstzIniNewfis(new AfDecimal(tgaPrstzIniNewfisObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIniNewfis(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIniNewfis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniNforz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz();
    }

    @Override
    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforz(tgaPrstzIniNforz.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNforzObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzIniNforz() >= 0) {
            return getTgaPrstzIniNforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj) {
        if (tgaPrstzIniNforzObj != null) {
            setTgaPrstzIniNforz(new AfDecimal(tgaPrstzIniNforzObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIniNforz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIniNforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzIni() >= 0) {
            return getTgaPrstzIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj) {
        if (tgaPrstzIniObj != null) {
            setTgaPrstzIni(new AfDecimal(tgaPrstzIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniStab() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab();
    }

    @Override
    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStab(tgaPrstzIniStab.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniStabObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzIniStab() >= 0) {
            return getTgaPrstzIniStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj) {
        if (tgaPrstzIniStabObj != null) {
            setTgaPrstzIniStab(new AfDecimal(tgaPrstzIniStabObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIniStab(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzIniStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzRidIni() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni();
    }

    @Override
    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIni(tgaPrstzRidIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzRidIniObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzRidIni() >= 0) {
            return getTgaPrstzRidIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj) {
        if (tgaPrstzRidIniObj != null) {
            setTgaPrstzRidIni(new AfDecimal(tgaPrstzRidIniObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzRidIni(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzRidIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzUlt() {
        return ldbs0730.getWs().getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt();
    }

    @Override
    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        ldbs0730.getWs().getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUlt(tgaPrstzUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzUltObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getPrstzUlt() >= 0) {
            return getTgaPrstzUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj) {
        if (tgaPrstzUltObj != null) {
            setTgaPrstzUlt(new AfDecimal(tgaPrstzUltObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setPrstzUlt(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setPrstzUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRatLrd() {
        return ldbs0730.getWs().getTrchDiGar().getTgaRatLrd().getTgaRatLrd();
    }

    @Override
    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        ldbs0730.getWs().getTrchDiGar().getTgaRatLrd().setTgaRatLrd(tgaRatLrd.copy());
    }

    @Override
    public AfDecimal getTgaRatLrdObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getRatLrd() >= 0) {
            return getTgaRatLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRatLrdObj(AfDecimal tgaRatLrdObj) {
        if (tgaRatLrdObj != null) {
            setTgaRatLrd(new AfDecimal(tgaRatLrdObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setRatLrd(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setRatLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRemunAss() {
        return ldbs0730.getWs().getTrchDiGar().getTgaRemunAss().getTgaRemunAss();
    }

    @Override
    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        ldbs0730.getWs().getTrchDiGar().getTgaRemunAss().setTgaRemunAss(tgaRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaRemunAssObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getRemunAss() >= 0) {
            return getTgaRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRemunAssObj(AfDecimal tgaRemunAssObj) {
        if (tgaRemunAssObj != null) {
            setTgaRemunAss(new AfDecimal(tgaRemunAssObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setRemunAss(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0() {
        return ldbs0730.getWs().getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0();
    }

    @Override
    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        ldbs0730.getWs().getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0(tgaRenIniTsTec0.copy());
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0Obj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getRenIniTsTec0() >= 0) {
            return getTgaRenIniTsTec0();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj) {
        if (tgaRenIniTsTec0Obj != null) {
            setTgaRenIniTsTec0(new AfDecimal(tgaRenIniTsTec0Obj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setRenIniTsTec0(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setRenIniTsTec0(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoLrd() {
        return ldbs0730.getWs().getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd();
    }

    @Override
    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        ldbs0730.getWs().getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrd(tgaRendtoLrd.copy());
    }

    @Override
    public AfDecimal getTgaRendtoLrdObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getRendtoLrd() >= 0) {
            return getTgaRendtoLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj) {
        if (tgaRendtoLrdObj != null) {
            setTgaRendtoLrd(new AfDecimal(tgaRendtoLrdObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setRendtoLrd(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setRendtoLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoRetr() {
        return ldbs0730.getWs().getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr();
    }

    @Override
    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        ldbs0730.getWs().getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetr(tgaRendtoRetr.copy());
    }

    @Override
    public AfDecimal getTgaRendtoRetrObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getRendtoRetr() >= 0) {
            return getTgaRendtoRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj) {
        if (tgaRendtoRetrObj != null) {
            setTgaRendtoRetr(new AfDecimal(tgaRendtoRetrObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setRendtoRetr(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setRendtoRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRisMat() {
        return ldbs0730.getWs().getTrchDiGar().getTgaRisMat().getTgaRisMat();
    }

    @Override
    public void setTgaRisMat(AfDecimal tgaRisMat) {
        ldbs0730.getWs().getTrchDiGar().getTgaRisMat().setTgaRisMat(tgaRisMat.copy());
    }

    @Override
    public AfDecimal getTgaRisMatObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getRisMat() >= 0) {
            return getTgaRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRisMatObj(AfDecimal tgaRisMatObj) {
        if (tgaRisMatObj != null) {
            setTgaRisMat(new AfDecimal(tgaRisMatObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setRisMat(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setRisMat(((short)-1));
        }
    }

    @Override
    public char getTgaTpAdegAbb() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTpAdegAbb();
    }

    @Override
    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        ldbs0730.getWs().getTrchDiGar().setTgaTpAdegAbb(tgaTpAdegAbb);
    }

    @Override
    public Character getTgaTpAdegAbbObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getTpAdegAbb() >= 0) {
            return ((Character)getTgaTpAdegAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj) {
        if (tgaTpAdegAbbObj != null) {
            setTgaTpAdegAbb(((char)tgaTpAdegAbbObj));
            ldbs0730.getWs().getIndTrchDiGar().setTpAdegAbb(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setTpAdegAbb(((short)-1));
        }
    }

    @Override
    public String getTgaTpManfeeAppl() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTpManfeeAppl();
    }

    @Override
    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        ldbs0730.getWs().getTrchDiGar().setTgaTpManfeeAppl(tgaTpManfeeAppl);
    }

    @Override
    public String getTgaTpManfeeApplObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getTpManfeeAppl() >= 0) {
            return getTgaTpManfeeAppl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj) {
        if (tgaTpManfeeApplObj != null) {
            setTgaTpManfeeAppl(tgaTpManfeeApplObj);
            ldbs0730.getWs().getIndTrchDiGar().setTpManfeeAppl(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setTpManfeeAppl(((short)-1));
        }
    }

    @Override
    public String getTgaTpRgmFisc() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTpRgmFisc();
    }

    @Override
    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        ldbs0730.getWs().getTrchDiGar().setTgaTpRgmFisc(tgaTpRgmFisc);
    }

    @Override
    public String getTgaTpRival() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTpRival();
    }

    @Override
    public void setTgaTpRival(String tgaTpRival) {
        ldbs0730.getWs().getTrchDiGar().setTgaTpRival(tgaTpRival);
    }

    @Override
    public String getTgaTpRivalObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getTpRival() >= 0) {
            return getTgaTpRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpRivalObj(String tgaTpRivalObj) {
        if (tgaTpRivalObj != null) {
            setTgaTpRival(tgaTpRivalObj);
            ldbs0730.getWs().getIndTrchDiGar().setTpRival(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setTpRival(((short)-1));
        }
    }

    @Override
    public String getTgaTpTrch() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTpTrch();
    }

    @Override
    public void setTgaTpTrch(String tgaTpTrch) {
        ldbs0730.getWs().getTrchDiGar().setTgaTpTrch(tgaTpTrch);
    }

    @Override
    public AfDecimal getTgaTsRivalFis() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis();
    }

    @Override
    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        ldbs0730.getWs().getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFis(tgaTsRivalFis.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalFisObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getTsRivalFis() >= 0) {
            return getTgaTsRivalFis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj) {
        if (tgaTsRivalFisObj != null) {
            setTgaTsRivalFis(new AfDecimal(tgaTsRivalFisObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setTsRivalFis(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setTsRivalFis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalIndiciz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz();
    }

    @Override
    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        ldbs0730.getWs().getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(tgaTsRivalIndiciz.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalIndicizObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getTsRivalIndiciz() >= 0) {
            return getTgaTsRivalIndiciz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj) {
        if (tgaTsRivalIndicizObj != null) {
            setTgaTsRivalIndiciz(new AfDecimal(tgaTsRivalIndicizObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setTsRivalIndiciz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setTsRivalIndiciz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalNet() {
        return ldbs0730.getWs().getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet();
    }

    @Override
    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        ldbs0730.getWs().getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNet(tgaTsRivalNet.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalNetObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getTsRivalNet() >= 0) {
            return getTgaTsRivalNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj) {
        if (tgaTsRivalNetObj != null) {
            setTgaTsRivalNet(new AfDecimal(tgaTsRivalNetObj, 14, 9));
            ldbs0730.getWs().getIndTrchDiGar().setTsRivalNet(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setTsRivalNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000() {
        return ldbs0730.getWs().getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000();
    }

    @Override
    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        ldbs0730.getWs().getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000(tgaVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000Nforz() {
        return ldbs0730.getWs().getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz();
    }

    @Override
    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        ldbs0730.getWs().getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(tgaVisEnd2000Nforz.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000NforzObj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getVisEnd2000Nforz() >= 0) {
            return getTgaVisEnd2000Nforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj) {
        if (tgaVisEnd2000NforzObj != null) {
            setTgaVisEnd2000Nforz(new AfDecimal(tgaVisEnd2000NforzObj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setVisEnd2000Nforz(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setVisEnd2000Nforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000Obj() {
        if (ldbs0730.getWs().getIndTrchDiGar().getVisEnd2000() >= 0) {
            return getTgaVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj) {
        if (tgaVisEnd2000Obj != null) {
            setTgaVisEnd2000(new AfDecimal(tgaVisEnd2000Obj, 15, 3));
            ldbs0730.getWs().getIndTrchDiGar().setVisEnd2000(((short)0));
        }
        else {
            ldbs0730.getWs().getIndTrchDiGar().setVisEnd2000(((short)-1));
        }
    }

    @Override
    public int getWkIdAdesA() {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public int getWkIdAdesDa() {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public int getWkIdGarA() {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public int getWkIdGarDa() {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ldbs0730.getWs().getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        ldbs0730.getWs().getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        return ldbs0730.getWs().getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        ldbs0730.getWs().getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
