package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Idsv0501;
import it.accenture.jnais.copy.Idsv0501Output;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ivvc0501Output;
import it.accenture.jnais.ws.Lvvs0560Data;
import static java.lang.Math.abs;

/**Original name: LVVS0560<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2010.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 * *************************************************************</pre>*/
public class Lvvs0560 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0560Data ws = new Lvvs0560Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0213
    private Ivvc0213 ivvc0213;
    //Original name: IVVC0501-OUTPUT
    private Ivvc0501Output ivvc0501Output;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0560_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213, Ivvc0501Output ivvc0501Output) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        this.ivvc0501Output = ivvc0501Output;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0560 getInstance() {
        return ((Lvvs0560)Programs.getInstance(Lvvs0560.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             IDSV0501-INPUT
        //                                             IVVC0501-OUTPUT.
        initIxIndici();
        initTabOutput();
        initIdsv0501Input();
        initIvvc0501Output();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE VAL-AST
        //                      AREA-IO-TGA
        //                      AREA-IO-GAR
        //                      AREA-IO-L19.
        initValAst();
        initAreaIoTga();
        initAreaIoGar();
        initAreaIoL19();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
            //
            // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
            //           AND IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE:  IF DGRZ-TP-INVST(IX-GUIDA-GRZ) = 7 OR 8
                //               END-IF
                //           END-IF
                if (ws.getDgrzTabGar(ws.getIxIndici().getGuidaGrz()).getLccvgrz1().getDati().getWgrzTpInvst().getWgrzTpInvst() == 7 || ws.getDgrzTabGar(ws.getIxIndici().getGuidaGrz()).getLccvgrz1().getDati().getWgrzTpInvst().getWgrzTpInvst() == 8) {
                    // COB_CODE:                 IF DL19-ELE-FND-MAX EQUAL ZERO
                    //                              CONTINUE
                    //                           ELSE
                    //                              END-IF
                    //           *                  PERFORM S1250-CALCOLA-QUOTE     THRU S1250-EX
                    //           *                  IF  IDSV0003-SUCCESSFUL-RC
                    //           *                  AND IDSV0003-SUCCESSFUL-SQL
                    //           *                     PERFORM S1260-SOTTRAI-ANNULLO
                    //           *                        THRU S1260-EX
                    //           *                  END-IF
                    //                           END-IF
                    if (ws.getDl19EleFndMax() == 0) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: IF LIQUI-RISPAR-POLIND
                        //           OR LIQUI-RPP-REDDITO-PROGR
                        //           OR LIQUI-RISPAR-ADE
                        //           OR LIQUI-RPP-TAKE-PROFIT
                        //            END-IF
                        //           ELSE
                        //                THRU S1250-EX
                        //           END-IF
                        if (ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppTakeProfit()) {
                            // COB_CODE: PERFORM S1230-RECUP-ID-COMUN
                            //              THRU S1230-EX
                            s1230RecupIdComun();
                            // COB_CODE: PERFORM S1250-CALCOLA-QUOTE
                            //              THRU S1250-EX
                            s1250CalcolaQuote();
                            // COB_CODE: IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
                            //                  TO IDSV0003-DATA-INIZIO-EFFETTO
                            //           END-IF
                            if (ws.getWkAppoDate().getInizioEffetto() > 0) {
                                // COB_CODE: MOVE WK-APPO-DATA-INIZIO-EFFETTO
                                //              TO IDSV0003-DATA-INIZIO-EFFETTO
                                idsv0003.setDataInizioEffetto(ws.getWkAppoDate().getInizioEffetto());
                            }
                            // COB_CODE: IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
                            //                 TO IDSV0003-DATA-FINE-EFFETTO
                            //           END-IF
                            if (ws.getWkAppoDate().getFineEffetto() > 0) {
                                // COB_CODE: MOVE WK-APPO-DATA-FINE-EFFETTO
                                //              TO IDSV0003-DATA-FINE-EFFETTO
                                idsv0003.setDataFineEffetto(ws.getWkAppoDate().getFineEffetto());
                            }
                            // COB_CODE: IF WK-APPO-DATA-COMPETENZA > ZEROES
                            //                TO IDSV0003-DATA-COMPETENZA
                            //           END-IF
                            if (ws.getWkAppoDate().getCompetenza() > 0) {
                                // COB_CODE: MOVE WK-APPO-DATA-COMPETENZA
                                //             TO IDSV0003-DATA-COMPETENZA
                                idsv0003.setDataCompetenza(ws.getWkAppoDate().getCompetenza());
                            }
                        }
                        else {
                            // COB_CODE: MOVE ZEROES
                            //             TO WK-ID-MOVI-FINRIO
                            ws.setWkIdMoviFinrio(0);
                            // COB_CODE: PERFORM S1250-CALCOLA-QUOTE
                            //             THRU S1250-EX
                            s1250CalcolaQuote();
                        }
                        //                  PERFORM S1250-CALCOLA-QUOTE     THRU S1250-EX
                        //                  IF  IDSV0003-SUCCESSFUL-RC
                        //                  AND IDSV0003-SUCCESSFUL-SQL
                        //                     PERFORM S1260-SOTTRAI-ANNULLO
                        //                        THRU S1260-EX
                        //                  END-IF
                    }
                }
            }
        }
        //
        //    VALORIZZA LA VARIABILE A LISTA
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //                 THRU S1350-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM S1350-VAR-POSITIVA
            //              THRU S1350-EX
            s1350VarPositiva();
        }
        //
        //    VALORIZZA LA VARIABILE A LISTA
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //                 THRU S1400-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM S1400-VALORIZZA-VARIABILE
            //              THRU S1400-EX
            s1400ValorizzaVariabile();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GRA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GRA
            ws.setDgrzAreaGraFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-QOTAZ-FON
        //                TO DL19-AREA-QUOTA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasQotazFon())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DL19-AREA-QUOTA
            ws.setDl19AreaQuotaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: SET WK-ID-COD-NON-TROVATO              TO TRUE.
        ws.getWkIdCodLivFlag().setNonTrovato();
        //
        // COB_CODE:      IF DGRZ-ELE-GAR-MAX GREATER ZERO
        //           *
        //                   END-IF
        //           *
        //                ELSE
        //                     TO IDSV0003-DESCRIZ-ERR-DB2
        //                END-IF.
        if (ws.getDgrzEleGarMax() > 0) {
            //
            // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
            //             UNTIL IX-TAB-GRZ > DGRZ-ELE-GAR-MAX
            //                OR WK-ID-COD-TROVATO
            //                END-IF
            //           END-PERFORM
            ws.getIxIndici().setTabGrz(((short)1));
            while (!(ws.getIxIndici().getTabGrz() > ws.getDgrzEleGarMax() || ws.getWkIdCodLivFlag().isTrovato())) {
                // COB_CODE: IF DTGA-ID-GAR(IVVC0213-IX-TABB) =
                //              DGRZ-ID-GAR(IX-TAB-GRZ)
                //              MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                //           END-IF
                if (ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdGar() == ws.getDgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    // COB_CODE: SET WK-ID-COD-TROVATO     TO TRUE
                    ws.getWkIdCodLivFlag().setTrovato();
                    // COB_CODE: MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                    ws.getIxIndici().setGuidaGrz(ws.getIxIndici().getTabGrz());
                }
                ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
            }
            //
            // COB_CODE: IF WK-ID-COD-NON-TROVATO
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getWkIdCodLivFlag().isNonTrovato()) {
                // COB_CODE: SET  IDSV0003-INVALID-OPER               TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'GARANZIA NON VALORIZZATA PER CALCOLO QUOTE'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("GARANZIA NON VALORIZZATA PER CALCOLO QUOTE");
            }
            //
        }
        else {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'GARANZIA NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("GARANZIA NON VALORIZZATA");
        }
    }

    /**Original name: S1230-RECUP-ID-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *   RECUPERO L'ID DEL MOVIMENTO DI COMUNICAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1230RecupIdComun() {
        Ldbs1530 ldbs1530 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                      MOVI.
        initMovi();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: IF LIQUI-RISPAR-POLIND
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRisparPolind()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET COMUN-RISPAR-IND         TO TRUE
            ws.getWsMovimento().setComunRisparInd();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: IF LIQUI-RPP-REDDITO-PROGR
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRppRedditoProgr()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET RPP-REDDITO-PROGRAMMATO  TO TRUE
            ws.getWsMovimento().setRppRedditoProgrammato();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: IF LIQUI-RISPAR-ADE
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRisparAde()) {
            // COB_CODE: MOVE IVVC0213-ID-ADESIONE    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdAdesione());
            // COB_CODE: MOVE 'AD'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("AD");
            // COB_CODE: SET COMUN-RISPAR-ADE         TO TRUE
            ws.getWsMovimento().setComunRisparAde();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: IF LIQUI-RPP-TAKE-PROFIT
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRppTakeProfit()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET RPP-TAKE-PROFIT          TO TRUE
            ws.getWsMovimento().setRppTakeProfit();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: MOVE MOVI                       TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getMovi().getMoviFormatted());
        // COB_CODE: CALL LDBS1530  USING  IDSV0003 MOVI
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs1530 = Ldbs1530.getInstance();
            ldbs1530.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS1530
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs1530());
            // COB_CODE: MOVE 'ERRORE CALL LDBS1530'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS1530");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN IDSV0003-SUCCESSFUL-SQL
        //           *-->       OPERAZIONE ESEGUITA CON SUCCESSO
        //                            THRU S1235-EX
        //                    WHEN IDSV0003-NOT-FOUND
        //           *-->     CHIAVE NON TROVATA
        //                           TO WK-ID-MOVI-FINRIO
        //                    WHEN OTHER
        //           *-->     ERRORE DB
        //                       END-STRING
        //                END-EVALUATE.
        switch (idsv0003.getSqlcode().getSqlcode()) {

            case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CON SUCCESSO
                // COB_CODE: PERFORM S1235-RECUP-MOVI-FINRIO
                //              THRU S1235-EX
                s1235RecupMoviFinrio();
                break;

            case Idsv0003Sqlcode.NOT_FOUND://-->     CHIAVE NON TROVATA
                // COB_CODE: MOVE ZEROES
                //             TO WK-ID-MOVI-FINRIO
                ws.setWkIdMoviFinrio(0);
                break;

            default://-->     ERRORE DB
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                //                 IDSV0003-RETURN-CODE ';'
                //                 IDSV0003-SQLCODE
                //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LDBS1530 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                break;
        }
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
    }

    /**Original name: S1235-RECUP-MOVI-FINRIO<br>
	 * <pre>----------------------------------------------------------------*
	 *   RECUPERO L'ID DELLA MOVIMENTO FINANZIARIO DEL RISC. PARZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1235RecupMoviFinrio() {
        Ldbs5950 ldbs5950 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                      MOVI-FINRIO.
        initMoviFinrio();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: MOVE MOV-ID-MOVI                TO MFZ-ID-MOVI-CRZ
        ws.getMoviFinrio().setMfzIdMoviCrz(ws.getMovi().getMovIdMovi());
        //--> SALVO LE DATE DI CONTESTO PER RIPRISTINARLE DOPO L'ACCESSO
        // COB_CODE: INITIALIZE WK-APPO-DATE
        initWkAppoDate();
        // COB_CODE: IF  IDSV0003-DATA-INIZIO-EFFETTO IS NUMERIC
        //           AND IDSV0003-DATA-INIZIO-EFFETTO > ZEROES
        //                 TO WK-APPO-DATA-INIZIO-EFFETTO
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataInizioEffetto()) && idsv0003.getDataInizioEffetto() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
            //             TO WK-APPO-DATA-INIZIO-EFFETTO
            ws.getWkAppoDate().setInizioEffetto(idsv0003.getDataInizioEffetto());
        }
        // COB_CODE: IF  IDSV0003-DATA-FINE-EFFETTO IS NUMERIC
        //           AND IDSV0003-DATA-FINE-EFFETTO > ZEROES
        //                 TO WK-APPO-DATA-FINE-EFFETTO
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO
            //             TO WK-APPO-DATA-FINE-EFFETTO
            ws.getWkAppoDate().setFineEffetto(idsv0003.getDataFineEffetto());
        }
        // COB_CODE: IF  IDSV0003-DATA-COMPETENZA IS NUMERIC
        //           AND IDSV0003-DATA-COMPETENZA > ZEROES
        //                 TO WK-APPO-DATA-COMPETENZA
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataCompetenza()) && idsv0003.getDataCompetenza() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
            //             TO WK-APPO-DATA-COMPETENZA
            ws.getWkAppoDate().setCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE: MOVE MOV-DT-EFF               TO IDSV0003-DATA-INIZIO-EFFETTO
        //                                            IDSV0003-DATA-FINE-EFFETTO
        idsv0003.setDataInizioEffetto(ws.getMovi().getMovDtEff());
        idsv0003.setDataFineEffetto(ws.getMovi().getMovDtEff());
        // COB_CODE: MOVE MOV-DS-TS-CPTZ           TO IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ws.getMovi().getMovDsTsCptz());
        // COB_CODE: MOVE MOVI-FINRIO                TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getMoviFinrio().getMoviFinrioFormatted());
        // COB_CODE: CALL LDBS5950  USING  IDSV0003 MOVI-FINRIO
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs5950 = Ldbs5950.getInstance();
            ldbs5950.run(idsv0003, ws.getMoviFinrio());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS5950
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs5950());
            // COB_CODE: MOVE 'ERRORE CALL LDBS5950'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS5950");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN IDSV0003-SUCCESSFUL-SQL
        //           *-->       OPERAZIONE ESEGUITA CON SUCCESSO
        //                           TO WK-ID-MOVI-FINRIO
        //                    WHEN IDSV0003-NOT-FOUND
        //           *-->     CHIAVE NON TROVATA
        //                           TO WK-ID-MOVI-FINRIO
        //                    WHEN OTHER
        //           *-->     ERRORE DB
        //                       END-STRING
        //                END-EVALUATE.
        switch (idsv0003.getSqlcode().getSqlcode()) {

            case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CON SUCCESSO
                // COB_CODE: MOVE MFZ-ID-MOVI-FINRIO
                //             TO WK-ID-MOVI-FINRIO
                ws.setWkIdMoviFinrio(ws.getMoviFinrio().getMfzIdMoviFinrio());
                break;

            case Idsv0003Sqlcode.NOT_FOUND://-->     CHIAVE NON TROVATA
                // COB_CODE: MOVE ZEROES
                //             TO WK-ID-MOVI-FINRIO
                ws.setWkIdMoviFinrio(0);
                break;

            default://-->     ERRORE DB
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                //                 IDSV0003-RETURN-CODE ';'
                //                 IDSV0003-SQLCODE
                //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LDBS1530 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                break;
        }
    }

    /**Original name: S1250-CALCOLA-QUOTE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaQuote() {
        Ldbs4910 ldbs4910 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-FETCH-FIRST            TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //
        // COB_CODE: INITIALIZE LDBV4911
        initLdbv4911();
        // COB_CODE: MOVE IVVC0213-ID-TRANCHE   TO LDBV4911-ID-TRCH-DI-GAR
        ws.getLdbv4911().setIdTrchDiGar(ivvc0213.getIdTranche());
        // COB_CODE: MOVE 'LQ'                  TO LDBV4911-TP-VAL-AST-1
        ws.getLdbv4911().setTpValAst1("LQ");
        // COB_CODE: MOVE 'AL'                  TO LDBV4911-TP-VAL-AST-2
        ws.getLdbv4911().setTpValAst2("AL");
        // COB_CODE: MOVE LDBV4911              TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbv4911().getLdbv4911Formatted());
        // COB_CODE:      PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                           OR NOT IDSV0003-SUCCESSFUL-SQL
        //           *
        //                   END-EVALUATE
        //           *
        //                END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            //
            // COB_CODE: CALL LDBS4910  USING  IDSV0003 VAL-AST
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs4910 = Ldbs4910.getInstance();
                ldbs4910.run(idsv0003, ws.getValAst());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS4910
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
                // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSV0003-NOT-FOUND
            //           *-->          CHIAVE NON TROVATA
            //                         END-IF
            //                      WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         SET IDSV0003-FETCH-NEXT TO TRUE
            //           *
            //                      WHEN OTHER
            //                         END-STRING
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.NOT_FOUND://-->          CHIAVE NON TROVATA
                    // COB_CODE: IF IDSV0003-FETCH-FIRST
                    //                TO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-IF
                    if (idsv0003.getOperazione().isFetchFirst()) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'LETTURA VALORE ASSETT - SQLCODE = 100 '
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("LETTURA VALORE ASSETT - SQLCODE = 100 ");
                    }
                    break;

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM S1251-CALCOLA-QUOTE
                    //              THRU S1251-EX
                    s1251CalcolaQuote();
                    // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE
                    idsv0003.getLivelloOperazione().setWhereCondition();
                    // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                    //
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                    //                 IDSV0003-RETURN-CODE ';'
                    //                 IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA VALORE ASSET ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
            //
        }
    }

    /**Original name: S1251-CALCOLA-QUOTE<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA VALORIZZAZIONE VALORE ASSETT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1251CalcolaQuote() {
        // COB_CODE: SET WK-ID-COD-NON-TROVATO           TO TRUE.
        ws.getWkIdCodLivFlag().setNonTrovato();
        //
        // COB_CODE: PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
        //             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
        //                OR WK-ID-COD-TROVATO
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabL19(((short)1));
        while (!(ws.getIxIndici().getTabL19() > ws.getDl19EleFndMax() || ws.getWkIdCodLivFlag().isTrovato())) {
            // COB_CODE: IF VAS-COD-FND = DL19-COD-FND(IX-TAB-L19)
            //              END-IF
            //           END-IF
            if (Conditions.eq(ws.getValAst().getVasCodFnd(), ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19CodFnd())) {
                // COB_CODE: SET WK-ID-COD-TROVATO       TO TRUE
                ws.getWkIdCodLivFlag().setTrovato();
                //
                // COB_CODE:              IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC
                //           *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                //                           END-IF
                //           *
                //                        ELSE
                //           *
                //                            SET IDSV0003-INVALID-OPER  TO TRUE
                //                        END-IF
                if (Functions.isNumber(ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().getWl19ValQuo())) {
                    //--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                    // COB_CODE: IF VAS-NUM-QUO IS NUMERIC
                    //              END-IF
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (Functions.isNumber(ws.getValAst().getVasNumQuo().getVasNumQuo())) {
                        // COB_CODE: IF VAS-ID-MOVI-FINRIO = WK-ID-MOVI-FINRIO
                        //              CONTINUE
                        //           ELSE
                        //                 THRU S1254-EX
                        //           END-IF
                        if (ws.getValAst().getVasIdMoviFinrio() == ws.getWkIdMoviFinrio()) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: MOVE ZERO          TO WK-VAL-FONDO
                            ws.setWkValFondo(new AfDecimal(0, 15, 3));
                            // COB_CODE: COMPUTE WK-VAL-FONDO = (VAS-NUM-QUO *
                            //                             DL19-VAL-QUO(IX-TAB-L19))
                            ws.setWkValFondo(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo().multiply(ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().getWl19ValQuo()), 15, 3));
                            // COB_CODE: COMPUTE WK-VAL-FONDO-2DEC ROUNDED =
                            //                   WK-VAL-FONDO * 1
                            ws.setWkValFondo2dec(Trunc.toDecimal(MathUtil.convertRoundDecimal((ws.getWkValFondo().multiply(1)), 2, RoundingMode.ROUND_UP, 31, 2), 14, 2));
                            // COB_CODE: PERFORM S1254-VALORIZZA-LISTA
                            //              THRU S1254-EX
                            s1254ValorizzaLista();
                        }
                    }
                    else {
                        // COB_CODE: IF  VAS-ID-RICH-INVST-FND IS NUMERIC
                        //           AND VAS-ID-RICH-INVST-FND > ZERO
                        //                  THRU S1252-EX
                        //           END-IF
                        if (Functions.isNumber(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd()) && ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd() > 0) {
                            // COB_CODE: PERFORM S1252-RECUP-IMP-INVES
                            //              THRU S1252-EX
                            s1252RecupImpInves();
                        }
                        // COB_CODE: IF  VAS-ID-RICH-DIS-FND IS NUMERIC
                        //           AND VAS-ID-RICH-DIS-FND > ZERO
                        //                  THRU S1253-EX
                        //           END-IF
                        if (Functions.isNumber(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFnd()) && ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFnd() > 0) {
                            // COB_CODE: PERFORM S1253-RECUP-IMP-DISIN
                            //              THRU S1253-EX
                            s1253RecupImpDisin();
                        }
                    }
                    //
                }
                else {
                    //
                    // COB_CODE: MOVE LDBS4910
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
                    // COB_CODE: MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("NUM-QUO / VAL-QUO NON NUMERICI");
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
            ws.getIxIndici().setTabL19(Trunc.toShort(ws.getIxIndici().getTabL19() + 1, 4));
        }
    }

    /**Original name: S1252-RECUP-IMP-INVES<br>
	 * <pre>----------------------------------------------------------------*
	 *     RECUPERA RICHIESTA DI INVESTIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1252RecupImpInves() {
        Idbsrif0 idbsrif0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE RICH-INVST-FND.
        initRichInvstFnd();
        // COB_CODE: SET  IDSV0003-ID                 TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET  IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE VAS-ID-RICH-INVST-FND TO RIF-ID-RICH-INVST-FND.
        ws.getRichInvstFnd().setRifIdRichInvstFnd(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd());
        // COB_CODE: CALL   IDBSRIF0 USING IDSV0003 RICH-INVST-FND.
        idbsrif0 = Idbsrif0.getInstance();
        idbsrif0.run(idsv0003, ws.getRichInvstFnd());
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIRE ERRORE
        //                   END-STRING
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-IF
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                         END-STRING
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: IF RIF-IMP-MOVTO IS NUMERIC
                    //           AND RIF-IMP-MOVTO > ZERO
                    //                 THRU S1254-EX
                    //           END-IF
                    if (Functions.isNumber(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto()) && ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto().compareTo(0) > 0) {
                        // COB_CODE: MOVE RIF-IMP-MOVTO
                        //             TO WK-VAL-FONDO-2DEC
                        ws.setWkValFondo2dec(Trunc.toDecimal(ws.getRichInvstFnd().getRifImpMovto().getRifImpMovto(), 14, 2));
                        // COB_CODE: PERFORM S1254-VALORIZZA-LISTA
                        //              THRU S1254-EX
                        s1254ValorizzaLista();
                    }
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA RICH_INVST_FND ;'
                    //                 IDSV0003-RETURN-CODE ';'
                    //                 IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA RICH_INVST_FND ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'ERRORE LETTURA TABELLA RICH_INVST_FND ;'
            //                 IDSV0003-RETURN-CODE ';'
            //                 IDSV0003-SQLCODE
            //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA RICH_INVST_FND ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1253-RECUP-IMP-DISIN<br>
	 * <pre>*---------------------------------------------------------------*
	 *     RECUPERA RICHIESTA DI DISINVESTIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1253RecupImpDisin() {
        Idbsrdf0 idbsrdf0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE RICH-DIS-FND.
        initRichDisFnd();
        // COB_CODE: SET  IDSV0003-ID                 TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: SET  IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET  IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE VAS-ID-RICH-DIS-FND
        //             TO RDF-ID-RICH-DIS-FND.
        ws.getRichDisFnd().setRdfIdRichDisFnd(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFnd());
        // COB_CODE: CALL   IDBSRDF0 USING IDSV0003 RICH-DIS-FND.
        idbsrdf0 = Idbsrdf0.getInstance();
        idbsrdf0.run(idsv0003, ws.getRichDisFnd());
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *--> GESTIRE ERRORE
        //                   END-STRING
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            END-IF
            //                       WHEN OTHER
            //           *--->       ERRORE DI ACCESSO AL DB
            //                         END-STRING
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: IF RDF-IMP-MOVTO IS NUMERIC
                    //           AND RDF-IMP-MOVTO > ZERO
                    //                 THRU S1254-EX
                    //           END-IF
                    if (Functions.isNumber(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto()) && ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto().compareTo(0) > 0) {
                        // COB_CODE: MOVE RDF-IMP-MOVTO
                        //             TO WK-VAL-FONDO-2DEC
                        ws.setWkValFondo2dec(Trunc.toDecimal(ws.getRichDisFnd().getRdfImpMovto().getRdfImpMovto(), 14, 2));
                        // COB_CODE: PERFORM S1254-VALORIZZA-LISTA
                        //              THRU S1254-EX
                        s1254ValorizzaLista();
                    }
                    break;

                default://--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA RICH_DISINV_FND ;'
                    //                 IDSV0003-RETURN-CODE ';'
                    //                 IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA RICH_DISINV_FND ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //--> GESTIRE ERRORE
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'ERRORE LETTURA TABELLA RICH_DISINV_FND ;'
            //                 IDSV0003-RETURN-CODE ';'
            //                 IDSV0003-SQLCODE
            //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA RICH_DISINV_FND ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1254-VALORIZZA-LISTA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA VARIABILE A LISTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1254ValorizzaLista() {
        // COB_CODE: MOVE VAS-TP-VAL-AST TO WK-TP-VAL-AST
        ws.getWkTpValAst().setWkTpValAst(ws.getValAst().getVasTpValAst());
        // COB_CODE: SET FONDO-TROVATO-NO TO TRUE
        ws.getFlagFondoTrovato().setNo();
        // COB_CODE: PERFORM VARYING IX-TAB-TEMP FROM 1 BY 1
        //           UNTIL IX-TAB-TEMP > IX-TAB-0501
        //           OR FONDO-TROVATO-SI
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabTemp(((short)1));
        while (!(ws.getIxIndici().getTabTemp() > ws.getIxIndici().getTab0501() || ws.getFlagFondoTrovato().isSi())) {
            // COB_CODE: IF IDSV0501-TP-DATO(IX-TAB-TEMP) = 'L'
            //              END-IF
            //           END-IF
            if (ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabTemp()).getIdsv0501TpDato() == 'L') {
                // COB_CODE: MOVE IDSV0501-VAL-STR(IX-TAB-TEMP)
                //             TO WK-TEMP-COD-FND
                ws.setWkTempCodFnd(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabTemp()).getIdsv0501ValStr());
                // COB_CODE: IF VAS-COD-FND = WK-TEMP-COD-FND
                //                 THRU S1310-EX
                //           END-IF
                if (Conditions.eq(ws.getValAst().getVasCodFnd(), ws.getWkTempCodFnd())) {
                    // COB_CODE: SET FONDO-TROVATO-SI TO TRUE
                    ws.getFlagFondoTrovato().setSi();
                    // COB_CODE: ADD 1 TO IX-TAB-TEMP
                    ws.getIxIndici().setTabTemp(Trunc.toShort(1 + ws.getIxIndici().getTabTemp(), 4));
                    // COB_CODE: PERFORM S1310-VALOR-LISTA-IMP
                    //              THRU S1310-EX
                    s1310ValorListaImp();
                }
            }
            ws.getIxIndici().setTabTemp(Trunc.toShort(ws.getIxIndici().getTabTemp() + 1, 4));
        }
        // COB_CODE: IF FONDO-TROVATO-NO
        //                 THRU S1300-EX
        //           END-IF.
        if (ws.getFlagFondoTrovato().isNo()) {
            // COB_CODE: PERFORM S1300-VALOR-VARLIST
            //              THRU S1300-EX
            s1300ValorVarlist();
        }
    }

    /**Original name: S1300-VALOR-VARLIST<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZA VARIABILI A LISTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300ValorVarlist() {
        // COB_CODE: IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
        //                TO IDSV0501-VAL-STR(IX-TAB-0501)
        //           ELSE
        //               SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-IF.
        if (ws.getIxIndici().getTab0501() < ws.getIdsv0501().getLimiteArrayInput()) {
            // COB_CODE: ADD 1                    TO IX-TAB-0501
            ws.getIxIndici().setTab0501(Trunc.toShort(1 + ws.getIxIndici().getTab0501(), 4));
            // COB_CODE: MOVE 'L'                 TO IDSV0501-TP-DATO(IX-TAB-0501)
            ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501TpDatoFormatted("L");
            // COB_CODE: MOVE VAS-COD-FND
            //             TO IDSV0501-VAL-STR(IX-TAB-0501)
            ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValStr(ws.getValAst().getVasCodFnd());
        }
        else {
            // COB_CODE: MOVE 'VARLIST'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("VARLIST");
            // COB_CODE: MOVE 'LVVS0560 - LIMITE-ARRAY-INPUT SUPERATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("LVVS0560 - LIMITE-ARRAY-INPUT SUPERATO");
            // COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
        //              END-IF
        //           ELSE
        //               SET IDSV0003-INVALID-OPER       TO TRUE
        //           END-IF.
        if (ws.getIxIndici().getTab0501() < ws.getIdsv0501().getLimiteArrayInput()) {
            // COB_CODE: ADD 1                    TO IX-TAB-0501
            ws.getIxIndici().setTab0501(Trunc.toShort(1 + ws.getIxIndici().getTab0501(), 4));
            // COB_CODE: MOVE 'R'                 TO IDSV0501-TP-DATO(IX-TAB-0501)
            ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501TpDatoFormatted("R");
            // COB_CODE: IF VALORE-POSITIVO
            //           OR ANNULLO-NEGATIVO
            //                      WK-VAL-FONDO-2DEC
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getWkTpValAst().isValorePositivo() || ws.getWkTpValAst().isAnnulloNegativo()) {
                // COB_CODE: COMPUTE IDSV0501-VAL-IMP(IX-TAB-0501) =
                //                   IDSV0501-VAL-IMP(IX-TAB-0501) +
                //                   WK-VAL-FONDO-2DEC
                ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValImp(Trunc.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).getIdsv0501ValImp().add(ws.getWkValFondo2dec()), 18, 7));
            }
            else if (ws.getWkTpValAst().isValoreNegativo() || ws.getWkTpValAst().isAnnulloPositivo()) {
                // COB_CODE: IF VALORE-NEGATIVO
                //           OR ANNULLO-POSITIVO
                //                      WK-VAL-FONDO-2DEC
                //           END-IF
                // COB_CODE: COMPUTE IDSV0501-VAL-IMP(IX-TAB-0501) =
                //                   IDSV0501-VAL-IMP(IX-TAB-0501) -
                //                   WK-VAL-FONDO-2DEC
                ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).setIdsv0501ValImp(Trunc.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTab0501()).getIdsv0501ValImp().subtract(ws.getWkValFondo2dec()), 18, 7));
            }
        }
        else {
            // COB_CODE: MOVE 'VARLIST'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("VARLIST");
            // COB_CODE: MOVE 'LVVS0560 - LIMITE-ARRAY-INPUT SUPERATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("LVVS0560 - LIMITE-ARRAY-INPUT SUPERATO");
            // COB_CODE: SET IDSV0003-INVALID-OPER       TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S1310-VALOR-LISTA-IMP<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZA VARIABILI A LISTA DI TIPO IMPORTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1310ValorListaImp() {
        // COB_CODE: IF VALORE-POSITIVO
        //           OR ANNULLO-NEGATIVO
        //                      WK-VAL-FONDO-2DEC
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getWkTpValAst().isValorePositivo() || ws.getWkTpValAst().isAnnulloNegativo()) {
            // COB_CODE: COMPUTE IDSV0501-VAL-IMP(IX-TAB-TEMP) =
            //                   IDSV0501-VAL-IMP(IX-TAB-TEMP) +
            //                   WK-VAL-FONDO-2DEC
            ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabTemp()).setIdsv0501ValImp(Trunc.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabTemp()).getIdsv0501ValImp().add(ws.getWkValFondo2dec()), 18, 7));
        }
        else if (ws.getWkTpValAst().isValoreNegativo() || ws.getWkTpValAst().isAnnulloPositivo()) {
            // COB_CODE: IF VALORE-NEGATIVO
            //           OR ANNULLO-POSITIVO
            //                      WK-VAL-FONDO-2DEC
            //           END-IF
            // COB_CODE: COMPUTE IDSV0501-VAL-IMP(IX-TAB-TEMP) =
            //                   IDSV0501-VAL-IMP(IX-TAB-TEMP) -
            //                   WK-VAL-FONDO-2DEC
            ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabTemp()).setIdsv0501ValImp(Trunc.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabTemp()).getIdsv0501ValImp().subtract(ws.getWkValFondo2dec()), 18, 7));
        }
    }

    /**Original name: S1350-VAR-POSITIVA<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZO LA VARIABILE A LISTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1350VarPositiva() {
        // COB_CODE: PERFORM VARYING IX-TAB-POS FROM 1 BY 1
        //             UNTIL IX-TAB-POS > IX-TAB-0501
        //                OR IDSV0501-TP-DATO(IX-TAB-POS) = SPACES
        //                   END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabPos(((short)1));
        while (!(ws.getIxIndici().getTabPos() > ws.getIxIndici().getTab0501() || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabPos()).getIdsv0501TpDato(), Types.SPACE_CHAR))) {
            // COB_CODE: IF  IDSV0501-VAL-IMP(IX-TAB-POS) IS NUMERIC
            //           AND IDSV0501-VAL-IMP(IX-TAB-POS) < ZERO
            //                       IDSV0501-VAL-IMP(IX-TAB-POS) * -1
            //           END-IF
            if (Functions.isNumber(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabPos()).getIdsv0501ValImp()) && ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabPos()).getIdsv0501ValImp().compareTo(0) < 0) {
                // COB_CODE: COMPUTE IDSV0501-VAL-IMP(IX-TAB-POS) =
                //                   IDSV0501-VAL-IMP(IX-TAB-POS) * -1
                ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabPos()).setIdsv0501ValImp(Trunc.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIxIndici().getTabPos()).getIdsv0501ValImp().multiply(-1), 18, 7));
            }
            ws.getIxIndici().setTabPos(Trunc.toShort(ws.getIxIndici().getTabPos() + 1, 4));
        }
    }

    /**Original name: S1400-VALORIZZA-VARIABILE<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZO LA VARIABILE A LISTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1400ValorizzaVariabile() {
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM STRINGA-X-VALORIZZATORE
            //              THRU STRINGA-X-VALORIZZATORE-EX
            stringaXValorizzatore();
            // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
            //                TO IVVC0501-OUTPUT
            //           ELSE
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           END-IF
            if (ws.getIdsv0501().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
                // COB_CODE: MOVE IDSV0501-OUTPUT
                //             TO IVVC0501-OUTPUT
                ivvc0501Output.setIvvc0501OutputBytes(ws.getIdsv0501().getIdsv0501Output().getIdsv0501OutputBytes());
            }
            else {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: STRINGA-X-VALORIZZATORE<br>
	 * <pre>----------------------------------------------------------------*
	 *    CODICE PER STRINGATURA VARIABILI
	 * ----------------------------------------------------------------*
	 * *************************************************************</pre>*/
    private void stringaXValorizzatore() {
        // COB_CODE: SET IDSV0501-SUCCESSFUL-RC TO TRUE
        ws.getIdsv0501().getIdsv0501ReturnCode().setIdsv0501SuccessfulRc();
        // COB_CODE: MOVE SPACES                TO IDSV0501-DESCRIZ-ERR
        ws.getIdsv0501().setIdsv0501DescrizErr("");
        // COB_CODE: SET IDSV0501-SEGNO-POSTIV-NO TO TRUE
        ws.getIdsv0501().getIdsv0501SegnoPostiv().setIdsv0501SegnoPostivNo();
        // COB_CODE: SET IDSV0501-SEGNO-ANTERIORE TO TRUE
        ws.getIdsv0501().getIdsv0501PosSegno().setIdsv0501SegnoAnteriore();
        // COB_CODE: MOVE 3               TO IDSV0501-DECIMALI-ESPOSTI-IMP
        ws.getIdsv0501().setIdsv0501DecimaliEspostiImp(((short)3));
        // COB_CODE: MOVE 3               TO IDSV0501-DECIMALI-ESPOSTI-PERC
        ws.getIdsv0501().setIdsv0501DecimaliEspostiPerc(((short)3));
        // COB_CODE: MOVE 9               TO IDSV0501-DECIMALI-ESPOSTI-TASS
        ws.getIdsv0501().setIdsv0501DecimaliEspostiTass(((short)9));
        // COB_CODE: MOVE ';'             TO IDSV0501-SEPARATORE
        ws.getIdsv0501().setIdsv0501SeparatoreFormatted(";");
        // COB_CODE: MOVE '.'             TO IDSV0501-SIMBOLO-DECIMALE
        ws.getIdsv0501().setIdsv0501SimboloDecimaleFormatted(".");
        // COB_CODE: PERFORM CONVERTI-FORMATI THRU CONVERTI-FORMATI-EX
        convertiFormati();
        // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
        //              PERFORM CALL-STRINGATURA THRU CALL-STRINGATURA-EX
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
            // COB_CODE: PERFORM CALL-STRINGATURA THRU CALL-STRINGATURA-EX
            callStringatura();
        }
    }

    /**Original name: CONVERTI-FORMATI<br>
	 * <pre>*************************************************************</pre>*/
    private void convertiFormati() {
        // COB_CODE: PERFORM VARYING IND-VAR FROM 1 BY 1
        //                     UNTIL IND-VAR > LIMITE-ARRAY-INPUT OR
        //                           IDSV0501-TP-DATO(IND-VAR) =
        //                           SPACES OR LOW-VALUES OR HIGH-VALUES
        //                    END-EVALUATE
        //           END-PERFORM.
        ConcatUtil concatUtil = null;
        ws.getIdsv0501().setIndVar(((short)1));
        while (!(ws.getIdsv0501().getIndVar() > ws.getIdsv0501().getLimiteArrayInput() || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato(), Types.SPACE_CHAR) || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato(), Types.HIGH_CHAR_VAL))) {
            // COB_CODE: EVALUATE IDSV0501-TP-DATO(IND-VAR)
            //              WHEN 'R'
            //                    MOVE IMPORTO  TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'Z'
            //                   MOVE MILLESIMI TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'O'
            //                                  TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'T'
            //                   MOVE TASSO     TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'L'
            //                   MOVE STRINGA   TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'C'
            //                   MOVE DT        TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN 'H'
            //                   MOVE NUMERICO  TO TP-DATO-PARALLELO(IND-VAR)
            //              WHEN OTHER
            //                   END-STRING
            //           END-EVALUATE
            switch (ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato()) {

                case 'R':// COB_CODE: MOVE IMPORTO  TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getImporto());
                    break;

                case 'Z':// COB_CODE: MOVE MILLESIMI TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getMillesimi());
                    break;

                case 'O':// COB_CODE: MOVE PERCENTUALE
                    //                          TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getPercentuale());
                    break;

                case 'T':// COB_CODE: MOVE TASSO     TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getTasso());
                    break;

                case 'L':// COB_CODE: MOVE STRINGA   TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getStringa());
                    break;

                case 'C':// COB_CODE: MOVE DT        TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getDt());
                    break;

                case 'H':// COB_CODE: MOVE NUMERICO  TO TP-DATO-PARALLELO(IND-VAR)
                    ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).setTpDatoParallelo(ws.getIdsv0501().getTipoFormato().getNumerico());
                    break;

                default:// COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                    ws.getIdsv0501().getIdsv0501ReturnCode().setIdsv0501GenericError();
                    // COB_CODE: STRING 'TIPO DATO LISTA NON VALIDO  : '''
                    //                  IDSV0501-TP-DATO(IND-VAR)
                    //                  ''' SU OCCORRENZA : '
                    //                  IND-VAR
                    //                  DELIMITED BY SIZE
                    //                  INTO IDSV0501-DESCRIZ-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0501.Len.IDSV0501_DESCRIZ_ERR, "TIPO DATO LISTA NON VALIDO  : '", String.valueOf(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato()), "' SU OCCORRENZA : ", ws.getIdsv0501().getIndVarAsString());
                    ws.getIdsv0501().setIdsv0501DescrizErr(concatUtil.replaceInString(ws.getIdsv0501().getIdsv0501DescrizErrFormatted()));
                    break;
            }
            ws.getIdsv0501().setIndVar(Trunc.toShort(ws.getIdsv0501().getIndVar() + 1, 2));
        }
    }

    /**Original name: CALL-STRINGATURA<br>
	 * <pre>*************************************************************</pre>*/
    private void callStringatura() {
        // COB_CODE: PERFORM INITIALIZE-CAMPI THRU INITIALIZE-CAMPI-EX
        initializeCampi();
        // COB_CODE: PERFORM ELABORA-DATI     THRU ELABORA-DATI-EX.
        elaboraDati();
    }

    /**Original name: INITIALIZE-CAMPI<br>
	 * <pre>*************************************************************</pre>*/
    private void initializeCampi() {
        // COB_CODE: SET SENZA-SEGNO                 TO TRUE.
        ws.getIdsv0501().getTipoSegno().setSenzaSegno();
        // COB_CODE: SET DATO-INPUT-TROVATO-NO       TO TRUE.
        ws.getIdsv0501().getDatoInputTrovato().setDatoInputTrovatoNo();
        // COB_CODE: SET PRIMA-VOLTA-SI              TO TRUE.
        ws.getIdsv0501().getPrimaVolta().setPrimaVoltaSi();
        // COB_CODE: INITIALIZE                         IDSV0501-OUTPUT
        //                                              CAMPO-ALFA
        //                                              CAMPO-INTERI
        //                                              CAMPO-DECIMALI.
        initIdsv0501Output();
        ws.getIdsv0501().getCampoAlfa().setCampoAlfa("");
        ws.getIdsv0501().getCampoInteri().setCampoInteri("");
        ws.getIdsv0501().getCampoDecimali().setCampoDecimali("");
        // COB_CODE: MOVE 1                          TO IND-OUT
        //                                              POSIZIONE
        //                                              IDSV0501-MAX-TAB-STR.
        ws.getIdsv0501().setIndOut(((short)1));
        ws.getIdsv0501().setPosizione(((short)1));
        ws.getIdsv0501().getIdsv0501Output().setIdsv0501MaxTabStr(((short)1));
        // COB_CODE: MOVE LIMITE-STRINGA             TO SPAZIO-RESTANTE
        ws.getIdsv0501().setSpazioRestanteFormatted(ws.getIdsv0501().getLimiteStringaFormatted());
        // COB_CODE: MOVE 0                          TO IND-VAR
        //                                              IND-STRINGA
        //                                              IND-INT
        //                                              IND-DEC.
        ws.getIdsv0501().setIndVar(((short)0));
        ws.getIdsv0501().setIndStringa(((short)0));
        ws.getIdsv0501().setIndInt(((short)0));
        ws.getIdsv0501().setIndDec(((short)0));
    }

    /**Original name: ELABORA-DATI<br>
	 * <pre>*************************************************************</pre>*/
    private void elaboraDati() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM VARYING IND-VAR FROM 1 BY 1
        //                     UNTIL IND-VAR > LIMITE-ARRAY-INPUT        OR
        //                           IDSV0501-TP-DATO(IND-VAR) =
        //                           SPACES OR LOW-VALUES OR HIGH-VALUES OR
        //                           NOT IDSV0501-SUCCESSFUL-RC
        //              END-IF
        //           END-PERFORM.
        ws.getIdsv0501().setIndVar(((short)1));
        while (!(ws.getIdsv0501().getIndVar() > ws.getIdsv0501().getLimiteArrayInput() || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato(), Types.SPACE_CHAR) || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato(), Types.HIGH_CHAR_VAL) || !ws.getIdsv0501().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc())) {
            // COB_CODE: SET DATO-INPUT-TROVATO-SI      TO TRUE
            ws.getIdsv0501().getDatoInputTrovato().setDatoInputTrovatoSi();
            // COB_CODE: SET SENZA-SEGNO                TO TRUE
            ws.getIdsv0501().getTipoSegno().setSenzaSegno();
            // COB_CODE: IF PRIMA-VOLTA-SI
            //                                          TO WK-TP-DATO-PARALLELO
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIdsv0501().getPrimaVolta().isPrimaVoltaSi()) {
                // COB_CODE: SET PRIMA-VOLTA-NO          TO TRUE
                ws.getIdsv0501().getPrimaVolta().setPrimaVoltaNo();
                // COB_CODE: MOVE TP-DATO-PARALLELO(IND-VAR)
                //                                       TO IDSV0501-TP-STRINGA
                ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo());
                // COB_CODE: MOVE TP-DATO-PARALLELO(IND-VAR)
                //                                       TO WK-TP-DATO-PARALLELO
                ws.getIdsv0501().setWkTpDatoParallelo(ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo());
            }
            else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() != ws.getIdsv0501().getWkTpDatoParallelo()) {
                // COB_CODE:            IF TP-DATO-PARALLELO(IND-VAR) NOT =
                //                         WK-TP-DATO-PARALLELO
                //                         MOVE MISTA               TO IDSV0501-TP-STRINGA
                //                      ELSE
                //           * SIR 20310 GESTIONE LISTE OMOGENEE
                //                         PERFORM LISTA-OMOGENEA   THRU LISTA-OMOGENEA-EX
                //                      END-IF
                // COB_CODE: MOVE MISTA               TO IDSV0501-TP-STRINGA
                ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getMista());
            }
            else {
                // SIR 20310 GESTIONE LISTE OMOGENEE
                // COB_CODE: PERFORM LISTA-OMOGENEA   THRU LISTA-OMOGENEA-EX
                listaOmogenea();
            }
            // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
            //              WHEN IMPORTO
            //                   PERFORM TRATTA-IMPORTO THRU TRATTA-IMPORTO-EX
            //              WHEN NUMERICO
            //                                          THRU TRATTA-NUMERICO-EX
            //              WHEN TASSO
            //              WHEN PERCENTUALE
            //              WHEN MILLESIMI
            //                                          THRU TRATTA-PERCENTUALE-EX
            //              WHEN DT
            //              WHEN STRINGA
            //                                          THRU TRATTA-STRINGA-EX
            //              WHEN OTHER
            //                   END-STRING
            //           END-EVALUATE
            if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getImporto()) {
                // COB_CODE: PERFORM TRATTA-IMPORTO THRU TRATTA-IMPORTO-EX
                trattaImporto();
            }
            else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getNumerico()) {
                // COB_CODE: PERFORM TRATTA-NUMERICO
                //                                  THRU TRATTA-NUMERICO-EX
                trattaNumerico();
            }
            else if ((ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getTasso()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getPercentuale()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getMillesimi())) {
                // COB_CODE: PERFORM TRATTA-PERCENTUALE
                //                                  THRU TRATTA-PERCENTUALE-EX
                trattaPercentuale();
            }
            else if ((ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getDt()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getStringa())) {
                // COB_CODE: PERFORM TRATTA-STRINGA
                //                                  THRU TRATTA-STRINGA-EX
                trattaStringa();
            }
            else {
                // COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                ws.getIdsv0501().getIdsv0501ReturnCode().setIdsv0501GenericError();
                // COB_CODE: STRING 'TIPO DATO LISTA NON VALIDO  : '''
                //                  IDSV0501-TP-DATO(IND-VAR)
                //                  ''' SU OCCORRENZA : '
                //                  IND-VAR
                //                  DELIMITED BY SIZE
                //                  INTO IDSV0501-DESCRIZ-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0501.Len.IDSV0501_DESCRIZ_ERR, "TIPO DATO LISTA NON VALIDO  : '", String.valueOf(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501TpDato()), "' SU OCCORRENZA : ", ws.getIdsv0501().getIndVarAsString());
                ws.getIdsv0501().setIdsv0501DescrizErr(concatUtil.replaceInString(ws.getIdsv0501().getIdsv0501DescrizErrFormatted()));
            }
            // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
            //              ADD 1                       TO POSIZIONE
            //           END-IF
            if (ws.getIdsv0501().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
                // COB_CODE: MOVE IDSV0501-SEPARATORE    TO IDSV0501-STRINGA-TOT
                //                                         (IND-OUT)(POSIZIONE:1)
                ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(String.valueOf(ws.getIdsv0501().getIdsv0501Separatore()), ws.getIdsv0501().getPosizione(), 1);
                // COB_CODE: ADD 1                       TO POSIZIONE
                ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
            }
            ws.getIdsv0501().setIndVar(Trunc.toShort(ws.getIdsv0501().getIndVar() + 1, 2));
        }
        // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
            // COB_CODE: IF DATO-INPUT-TROVATO-NO
            //              END-STRING
            //           END-IF
            if (ws.getIdsv0501().getDatoInputTrovato().isDatoInputTrovatoNo()) {
                // COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                ws.getIdsv0501().getIdsv0501ReturnCode().setIdsv0501GenericError();
                // COB_CODE: STRING 'NESSUN DATO DI INPUT TROVATO'
                //                  DELIMITED BY SIZE
                //                  INTO IDSV0501-DESCRIZ-ERR
                //           END-STRING
                ws.getIdsv0501().setIdsv0501DescrizErr("NESSUN DATO DI INPUT TROVATO");
            }
        }
    }

    /**Original name: TRATTA-IMPORTO<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaImporto() {
        // COB_CODE: MOVE LIM-INT-IMP                 TO LIMITE-INTERI.
        ws.getIdsv0501().setLimiteInteriFormatted(ws.getIdsv0501().getLimIntImpFormatted());
        // COB_CODE: MOVE LIM-DEC-IMP                 TO LIMITE-DECIMALI.
        ws.getIdsv0501().setLimiteDecimaliFormatted(ws.getIdsv0501().getLimDecImpFormatted());
        // COB_CODE: IF IDSV0501-VAL-IMP (IND-VAR) >= 0
        //              SET SEGNO-POSITIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValImp().compareTo(0) >= 0) {
            // COB_CODE: SET SEGNO-POSITIVO            TO TRUE
            ws.getIdsv0501().getTipoSegno().setSegnoPositivo();
        }
        // COB_CODE: IF IDSV0501-VAL-IMP (IND-VAR) <  0
        //              SET SEGNO-NEGATIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValImp().compareTo(0) < 0) {
            // COB_CODE: SET SEGNO-NEGATIVO            TO TRUE
            ws.getIdsv0501().getTipoSegno().setSegnoNegativo();
        }
        // COB_CODE: INITIALIZE                       STRUCT-IMP.
        ws.getIdsv0501().getStructNum().setStructImp(new AfDecimal(0, 18, 7));
        // COB_CODE: MOVE IDSV0501-VAL-IMP (IND-VAR)  TO STRUCT-IMP.
        ws.getIdsv0501().getStructNum().setStructImp(TruncAbs.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValImp(), 18, 7));
        // COB_CODE: PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.
        estraiIntero();
        // COB_CODE: PERFORM ESTRAI-DECIMALI          THRU ESTRAI-DECIMALI-EX.
        estraiDecimali();
        // COB_CODE: PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.
        stringaCampo();
        // COB_CODE: IF IDSV0501-SEGNO-POSTERIORE
        //              PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501PosSegno().isIdsv0501SegnoPosteriore()) {
            // COB_CODE: PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
            trattaSegno();
        }
    }

    /**Original name: TRATTA-NUMERICO<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaNumerico() {
        // COB_CODE: MOVE LIMITE-INT-DEC              TO LIMITE-INTERI.
        ws.getIdsv0501().setLimiteInteriFormatted(ws.getIdsv0501().getLimiteIntDecFormatted());
        // COB_CODE: IF IDSV0501-VAL-NUM (IND-VAR) >= ZEROES
        //              SET SEGNO-POSITIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValNum() >= 0) {
            // COB_CODE: SET SEGNO-POSITIVO            TO TRUE
            ws.getIdsv0501().getTipoSegno().setSegnoPositivo();
        }
        // COB_CODE: IF IDSV0501-VAL-NUM (IND-VAR) < ZEROES
        //              SET SEGNO-NEGATIVO            TO TRUE
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValNum() < 0) {
            // COB_CODE: SET SEGNO-NEGATIVO            TO TRUE
            ws.getIdsv0501().getTipoSegno().setSegnoNegativo();
        }
        // COB_CODE: INITIALIZE                          STRUCT-NUM.
        ws.getIdsv0501().getStructNum().setStructNum(0);
        // COB_CODE: MOVE IDSV0501-VAL-NUM (IND-VAR)  TO STRUCT-NUM.
        ws.getIdsv0501().getStructNum().setStructNum(TruncAbs.toLong(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValNum(), 18));
        // COB_CODE: PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.
        estraiIntero();
        // COB_CODE: PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.
        stringaCampo();
        // COB_CODE: IF IDSV0501-SEGNO-POSTERIORE
        //              PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501PosSegno().isIdsv0501SegnoPosteriore()) {
            // COB_CODE: PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
            trattaSegno();
        }
    }

    /**Original name: TRATTA-PERCENTUALE<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaPercentuale() {
        // COB_CODE: MOVE LIM-INT-PERC                TO LIMITE-INTERI.
        ws.getIdsv0501().setLimiteInteriFormatted(ws.getIdsv0501().getLimIntPercFormatted());
        // COB_CODE: MOVE LIM-DEC-PERC                TO LIMITE-DECIMALI.
        ws.getIdsv0501().setLimiteDecimaliFormatted(ws.getIdsv0501().getLimDecPercFormatted());
        // COB_CODE: INITIALIZE                          STRUCT-IMP.
        ws.getIdsv0501().getStructNum().setStructImp(new AfDecimal(0, 18, 7));
        // COB_CODE: MOVE IDSV0501-VAL-PERC (IND-VAR) TO STRUCT-PERC.
        ws.getIdsv0501().getStructNum().setStructPerc(Trunc.toDecimal(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValPerc(), 18, 9));
        // COB_CODE: PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.
        estraiIntero();
        // COB_CODE: PERFORM ESTRAI-DECIMALI          THRU ESTRAI-DECIMALI-EX.
        estraiDecimali();
        // COB_CODE: PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.
        stringaCampo();
    }

    /**Original name: TRATTA-STRINGA<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaStringa() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                          STRUCT-ALFA.
        ws.getIdsv0501().getStructAlfa().setStructAlfa("");
        // COB_CODE: MOVE IDSV0501-VAL-STR (IND-VAR)  TO STRUCT-ALFA.
        ws.getIdsv0501().getStructAlfa().setStructAlfa(ws.getIdsv0501().getIdsv0501TabVariabili(ws.getIdsv0501().getIndVar()).getIdsv0501ValStr());
        // COB_CODE: PERFORM ESTRAI-ALFANUMERICO      THRU ESTRAI-ALFANUMERICO-EX.
        estraiAlfanumerico();
        // COB_CODE: IF IND-ALFA >= LIMITE-STRINGA
        //              END-STRING
        //           ELSE
        //              PERFORM STRINGA-CAMPO         THRU STRINGA-CAMPO-EX
        //           END-IF.
        if (ws.getIdsv0501().getIndAlfa() >= ws.getIdsv0501().getLimiteStringa()) {
            // COB_CODE: SET IDSV0501-GENERIC-ERROR    TO TRUE
            ws.getIdsv0501().getIdsv0501ReturnCode().setIdsv0501GenericError();
            // COB_CODE: STRING 'SUPERATO IL LIMITE DI VALORE PER STRINGA'
            //                  ' SU OCCORRENZA : '
            //                  IND-VAR
            //                  DELIMITED BY SIZE
            //                  INTO IDSV0501-DESCRIZ-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0501.Len.IDSV0501_DESCRIZ_ERR, "SUPERATO IL LIMITE DI VALORE PER STRINGA", " SU OCCORRENZA : ", ws.getIdsv0501().getIndVarAsString());
            ws.getIdsv0501().setIdsv0501DescrizErr(concatUtil.replaceInString(ws.getIdsv0501().getIdsv0501DescrizErrFormatted()));
        }
        else {
            // COB_CODE: PERFORM STRINGA-CAMPO         THRU STRINGA-CAMPO-EX
            stringaCampo();
        }
    }

    /**Original name: ESTRAI-ALFANUMERICO<br>
	 * <pre>*************************************************************</pre>*/
    private void estraiAlfanumerico() {
        // COB_CODE: SET ALFANUMERICO-TROVATO-NO TO TRUE.
        ws.getIdsv0501().getAlfanumericoTrovato().setAlfanumericoTrovatoNo();
        // COB_CODE: SET FINE-STRINGA-NO         TO TRUE
        ws.getIdsv0501().getFineStringa().setFineStringaNo();
        // COB_CODE: INITIALIZE                  CAMPO-ALFA
        //                                       IND-ALFA.
        ws.getIdsv0501().getCampoAlfa().setCampoAlfa("");
        ws.getIdsv0501().setIndAlfa(((short)0));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM 1 BY 1
        //                   UNTIL   IND-STRINGA > LIMITE-STRINGA OR
        //                           FINE-STRINGA-SI
        //                      END-IF
        //            END-PERFORM.
        ws.getIdsv0501().setIndStringa(((short)1));
        while (!(ws.getIdsv0501().getIndStringa() > ws.getIdsv0501().getLimiteStringa() || ws.getIdsv0501().getFineStringa().isFineStringaSi())) {
            // COB_CODE: IF ELE-ALFA(IND-STRINGA) =
            //              SPACES OR LOW-VALUE OR HIGH-VALUE
            //              SET FINE-STRINGA-SI         TO TRUE
            //           ELSE
            //                TO ELE-STRINGA-ALFA(IND-ALFA)
            //           END-IF
            if (Conditions.eq(ws.getIdsv0501().getStructAlfa().getEleAlfa(ws.getIdsv0501().getIndStringa()), Types.SPACE_CHAR) || Conditions.eq(ws.getIdsv0501().getStructAlfa().getEleAlfa(ws.getIdsv0501().getIndStringa()), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIdsv0501().getStructAlfa().getEleAlfa(ws.getIdsv0501().getIndStringa()), Types.HIGH_CHAR_VAL)) {
                // COB_CODE: SET FINE-STRINGA-SI         TO TRUE
                ws.getIdsv0501().getFineStringa().setFineStringaSi();
            }
            else {
                // COB_CODE: SET ALFANUMERICO-TROVATO-SI TO TRUE
                ws.getIdsv0501().getAlfanumericoTrovato().setAlfanumericoTrovatoSi();
                // COB_CODE: ADD 1                       TO IND-ALFA
                ws.getIdsv0501().setIndAlfa(Trunc.toShort(1 + ws.getIdsv0501().getIndAlfa(), 2));
                // COB_CODE: MOVE ELE-ALFA(IND-STRINGA)
                //             TO ELE-STRINGA-ALFA(IND-ALFA)
                ws.getIdsv0501().getCampoAlfa().setEleStringaAlfa(ws.getIdsv0501().getIndAlfa(), ws.getIdsv0501().getStructAlfa().getEleAlfa(ws.getIdsv0501().getIndStringa()));
            }
            ws.getIdsv0501().setIndStringa(Trunc.toShort(ws.getIdsv0501().getIndStringa() + 1, 2));
        }
    }

    /**Original name: ESTRAI-INTERO<br>
	 * <pre>*************************************************************</pre>*/
    private void estraiIntero() {
        // COB_CODE: SET INT-TROVATO-NO          TO TRUE.
        ws.getIdsv0501().getIntTrovato().setIntTrovatoNo();
        // COB_CODE: INITIALIZE                      CAMPO-INTERI
        //                                           IND-INT.
        ws.getIdsv0501().getCampoInteri().setCampoInteri("");
        ws.getIdsv0501().setIndInt(((short)0));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM 1 BY 1
        //                   UNTIL   IND-STRINGA > LIMITE-INT-DEC    OR
        //                           IND-STRINGA > LIMITE-INTERI
        //                      END-IF
        //            END-PERFORM.
        ws.getIdsv0501().setIndStringa(((short)1));
        while (!(ws.getIdsv0501().getIndStringa() > ws.getIdsv0501().getLimiteIntDec() || ws.getIdsv0501().getIndStringa() > ws.getIdsv0501().getLimiteInteri())) {
            // COB_CODE: IF ELE-NUM-IMP(IND-STRINGA)
            //                                     NOT = ZEROES
            //              SET INT-TROVATO-SI     TO TRUE
            //           END-IF
            if (!Conditions.eq(ws.getIdsv0501().getStructNum().getEleNumImp(ws.getIdsv0501().getIndStringa()), '0')) {
                // COB_CODE: SET INT-TROVATO-SI     TO TRUE
                ws.getIdsv0501().getIntTrovato().setIntTrovatoSi();
            }
            // COB_CODE: IF INT-TROVATO-SI
            //                TO ELE-STRINGA-INTERI(IND-INT)
            //           END-IF
            if (ws.getIdsv0501().getIntTrovato().isIntTrovatoSi()) {
                // COB_CODE: ADD 1                  TO IND-INT
                ws.getIdsv0501().setIndInt(Trunc.toShort(1 + ws.getIdsv0501().getIndInt(), 2));
                // COB_CODE: MOVE ELE-NUM-IMP(IND-STRINGA)
                //             TO ELE-STRINGA-INTERI(IND-INT)
                ws.getIdsv0501().getCampoInteri().setEleStringaInteri(ws.getIdsv0501().getIndInt(), ws.getIdsv0501().getStructNum().getEleNumImp(ws.getIdsv0501().getIndStringa()));
            }
            ws.getIdsv0501().setIndStringa(Trunc.toShort(ws.getIdsv0501().getIndStringa() + 1, 2));
        }
        // COB_CODE: IF INT-TROVATO-NO
        //              MOVE 1                 TO  IND-INT
        //           END-IF.
        if (ws.getIdsv0501().getIntTrovato().isIntTrovatoNo()) {
            // COB_CODE: MOVE 1                 TO  IND-INT
            ws.getIdsv0501().setIndInt(((short)1));
        }
    }

    /**Original name: ESTRAI-DECIMALI<br>
	 * <pre>*************************************************************</pre>*/
    private void estraiDecimali() {
        // COB_CODE: INITIALIZE                      CAMPO-DECIMALI
        //                                           IND-DEC.
        ws.getIdsv0501().getCampoDecimali().setCampoDecimali("");
        ws.getIdsv0501().setIndDec(((short)0));
        // COB_CODE: PERFORM VARYING IND-STRINGA FROM LIMITE-DECIMALI BY 1
        //                   UNTIL   IND-STRINGA > LIMITE-INT-DEC
        //                           TO ELE-STRINGA-DECIMALI(IND-DEC)
        //           END-PERFORM.
        ws.getIdsv0501().setIndStringa(ws.getIdsv0501().getLimiteDecimali());
        while (!(ws.getIdsv0501().getIndStringa() > ws.getIdsv0501().getLimiteIntDec())) {
            // COB_CODE: ADD 1                  TO IND-DEC
            ws.getIdsv0501().setIndDec(Trunc.toShort(1 + ws.getIdsv0501().getIndDec(), 2));
            // COB_CODE: MOVE ELE-NUM-IMP(IND-STRINGA)
            //             TO ELE-STRINGA-DECIMALI(IND-DEC)
            ws.getIdsv0501().getCampoDecimali().setEleStringaDecimali(ws.getIdsv0501().getIndDec(), ws.getIdsv0501().getStructNum().getEleNumImp(ws.getIdsv0501().getIndStringa()));
            ws.getIdsv0501().setIndStringa(Trunc.toShort(ws.getIdsv0501().getIndStringa() + 1, 2));
        }
    }

    /**Original name: TRATTA-SEGNO<br>
	 * <pre>*************************************************************</pre>*/
    private void trattaSegno() {
        // COB_CODE: IF SEGNO-NEGATIVO
        //              ADD 1               TO POSIZIONE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIdsv0501().getTipoSegno().isSegnoNegativo()) {
            // COB_CODE: MOVE '-'            TO IDSV0501-STRINGA-TOT
            //                                 (IND-OUT)(POSIZIONE:1)
            ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring("-", ws.getIdsv0501().getPosizione(), 1);
            // COB_CODE: ADD 1               TO POSIZIONE
            ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
        }
        else if (ws.getIdsv0501().getIdsv0501SegnoPostiv().isIdsv0501SegnoPostivSi()) {
            // COB_CODE: IF IDSV0501-SEGNO-POSTIV-SI
            //              ADD 1            TO POSIZIONE
            //           END-IF
            // COB_CODE: MOVE '+'         TO IDSV0501-STRINGA-TOT
            //                              (IND-OUT)(POSIZIONE:1)
            ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring("+", ws.getIdsv0501().getPosizione(), 1);
            // COB_CODE: ADD 1            TO POSIZIONE
            ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
        }
    }

    /**Original name: STRINGA-CAMPO<br>
	 * <pre>*************************************************************</pre>*/
    private void stringaCampo() {
        // COB_CODE: PERFORM CALCOLA-SPAZIO       THRU CALCOLA-SPAZIO-EX.
        calcolaSpazio();
        // COB_CODE: IF IDSV0501-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501ReturnCode().isIdsv0501SuccessfulRc()) {
            // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
            //              WHEN STRINGA
            //              WHEN DT
            //                  END-IF
            //              WHEN IMPORTO
            //              WHEN PERCENTUALE
            //              WHEN TASSO
            //                  END-IF
            //              WHEN NUMERICO
            //                  END-IF
            //           END-EVALUATE
            if ((ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getStringa()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getDt())) {
                // COB_CODE: IF ALFANUMERICO-TROVATO-SI
                //              ADD IND-ALFA    TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0501().getAlfanumericoTrovato().isAlfanumericoTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-ALFA(1:IND-ALFA)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-ALFA)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0501().getCampoAlfa().getCampoAlfaFormatted().substring((1) - 1, ws.getIdsv0501().getIndAlfa()), ws.getIdsv0501().getPosizione(), ws.getIdsv0501().getIndAlfa());
                    // COB_CODE: ADD IND-ALFA    TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(ws.getIdsv0501().getIndAlfa() + ws.getIdsv0501().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE SPACES     TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(LiteralGenerator.create(Types.SPACE_CHAR, 1), ws.getIdsv0501().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
                }
            }
            else if ((ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getImporto()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getPercentuale()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getTasso())) {
                // COB_CODE: IF IDSV0501-SEGNO-ANTERIORE
                //              PERFORM TRATTA-SEGNO  THRU TRATTA-SEGNO-EX
                //           END-IF
                if (ws.getIdsv0501().getIdsv0501PosSegno().isIdsv0501SegnoAnteriore()) {
                    // COB_CODE: PERFORM TRATTA-SEGNO  THRU TRATTA-SEGNO-EX
                    trattaSegno();
                }
                // COB_CODE: IF INT-TROVATO-SI
                //              ADD IND-INT     TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0501().getIntTrovato().isIntTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-INTERI(1:IND-INT)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-INT)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0501().getCampoInteri().getCampoInteriFormatted().substring((1) - 1, ws.getIdsv0501().getIndInt()), ws.getIdsv0501().getPosizione(), ws.getIdsv0501().getIndInt());
                    // COB_CODE: ADD IND-INT     TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(ws.getIdsv0501().getIndInt() + ws.getIdsv0501().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE 0          TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring("0", ws.getIdsv0501().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
                }
                // COB_CODE: MOVE IDSV0501-SIMBOLO-DECIMALE
                //                              TO IDSV0501-STRINGA-TOT
                //                                (IND-OUT)(POSIZIONE:1)
                ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(String.valueOf(ws.getIdsv0501().getIdsv0501SimboloDecimale()), ws.getIdsv0501().getPosizione(), 1);
                // COB_CODE: ADD 1              TO POSIZIONE
                ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
                // COB_CODE: IF DEC-TROVATO-SI
                //              ADD IND-DEC     TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0501().getDecTrovato().isDecTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-DECIMALI(1:IND-DEC)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-DEC)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0501().getCampoDecimali().getCampoDecimaliFormatted().substring((1) - 1, ws.getIdsv0501().getIndDec()), ws.getIdsv0501().getPosizione(), ws.getIdsv0501().getIndDec());
                    // COB_CODE: ADD IND-DEC     TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(ws.getIdsv0501().getIndDec() + ws.getIdsv0501().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE 0          TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring("0", ws.getIdsv0501().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
                }
            }
            else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getNumerico()) {
                // COB_CODE: IF IDSV0501-SEGNO-ANTERIORE
                //              PERFORM TRATTA-SEGNO THRU TRATTA-SEGNO-EX
                //           END-IF
                if (ws.getIdsv0501().getIdsv0501PosSegno().isIdsv0501SegnoAnteriore()) {
                    // COB_CODE: PERFORM TRATTA-SEGNO THRU TRATTA-SEGNO-EX
                    trattaSegno();
                }
                // COB_CODE: IF INT-TROVATO-SI
                //              ADD IND-INT     TO POSIZIONE
                //           ELSE
                //              ADD 1           TO POSIZIONE
                //           END-IF
                if (ws.getIdsv0501().getIntTrovato().isIntTrovatoSi()) {
                    // COB_CODE: MOVE CAMPO-INTERI(1:IND-INT)
                    //                           TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:IND-INT)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring(ws.getIdsv0501().getCampoInteri().getCampoInteriFormatted().substring((1) - 1, ws.getIdsv0501().getIndInt()), ws.getIdsv0501().getPosizione(), ws.getIdsv0501().getIndInt());
                    // COB_CODE: ADD IND-INT     TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(ws.getIdsv0501().getIndInt() + ws.getIdsv0501().getPosizione(), 2));
                }
                else {
                    // COB_CODE: MOVE 0          TO IDSV0501-STRINGA-TOT
                    //                              (IND-OUT)(POSIZIONE:1)
                    ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(ws.getIdsv0501().getIndOut()).setIdsv0501StringaTotSubstring("0", ws.getIdsv0501().getPosizione(), 1);
                    // COB_CODE: ADD 1           TO POSIZIONE
                    ws.getIdsv0501().setPosizione(Trunc.toShort(1 + ws.getIdsv0501().getPosizione(), 2));
                }
            }
        }
    }

    /**Original name: CALCOLA-SPAZIO<br>
	 * <pre>*************************************************************</pre>*/
    private void calcolaSpazio() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF TP-DATO-PARALLELO(IND-VAR) NOT = STRINGA
        //              PERFORM INDIVIDUA-IND-DEC    THRU INDIVIDUA-IND-DEC-EX
        //           END-IF.
        if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() != ws.getIdsv0501().getTipoFormato().getStringa()) {
            // COB_CODE: PERFORM INDIVIDUA-IND-DEC    THRU INDIVIDUA-IND-DEC-EX
            individuaIndDec();
        }
        // COB_CODE: MOVE ZEROES                     TO SPAZIO-RICHIESTO.
        ws.getIdsv0501().setSpazioRichiesto(((short)0));
        // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
        //              WHEN STRINGA
        //              WHEN DT
        //                   COMPUTE SPAZIO-RICHIESTO = IND-ALFA + 1
        //              WHEN NUMERICO
        //                   COMPUTE SPAZIO-RICHIESTO = IND-INT + 1
        //              WHEN OTHER
        //                   COMPUTE SPAZIO-RICHIESTO = IND-INT + IND-DEC + 1 + 1
        //           END-EVALUATE.
        if ((ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getStringa()) || (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getDt())) {
            // COB_CODE: COMPUTE SPAZIO-RICHIESTO = IND-ALFA + 1
            ws.getIdsv0501().setSpazioRichiesto(Trunc.toShort(ws.getIdsv0501().getIndAlfa() + 1, 2));
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getNumerico()) {
            // COB_CODE: COMPUTE SPAZIO-RICHIESTO = IND-INT + 1
            ws.getIdsv0501().setSpazioRichiesto(Trunc.toShort(ws.getIdsv0501().getIndInt() + 1, 2));
        }
        else {
            // COB_CODE: COMPUTE SPAZIO-RICHIESTO = IND-INT + IND-DEC + 1 + 1
            ws.getIdsv0501().setSpazioRichiesto(Trunc.toShort(ws.getIdsv0501().getIndInt() + ws.getIdsv0501().getIndDec() + 1 + 1, 2));
        }
        // COB_CODE: IF (IDSV0501-SEGNO-ANTERIORE OR
        //               IDSV0501-SEGNO-POSTERIORE)
        //               END-IF
        //           END-IF.
        if (ws.getIdsv0501().getIdsv0501PosSegno().isIdsv0501SegnoAnteriore() || ws.getIdsv0501().getIdsv0501PosSegno().isIdsv0501SegnoPosteriore()) {
            // COB_CODE: IF SEGNO-NEGATIVO
            //              SUBTRACT  1    FROM  SPAZIO-RESTANTE
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIdsv0501().getTipoSegno().isSegnoNegativo()) {
                // COB_CODE: SUBTRACT  1    FROM  SPAZIO-RESTANTE
                ws.getIdsv0501().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0501().getSpazioRestante() - 1), 2));
            }
            else if (ws.getIdsv0501().getIdsv0501SegnoPostiv().isIdsv0501SegnoPostivSi()) {
                // COB_CODE: IF IDSV0501-SEGNO-POSTIV-SI
                //              SUBTRACT  1    FROM  SPAZIO-RESTANTE
                //           END-IF
                // COB_CODE: SUBTRACT  1    FROM  SPAZIO-RESTANTE
                ws.getIdsv0501().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0501().getSpazioRestante() - 1), 2));
            }
        }
        // COB_CODE: IF SPAZIO-RICHIESTO >= SPAZIO-RESTANTE
        //              END-IF
        //           ELSE
        //                                        SPAZIO-RICHIESTO
        //           END-IF.
        if (ws.getIdsv0501().getSpazioRichiesto() >= ws.getIdsv0501().getSpazioRestante()) {
            // COB_CODE: code not available
            ws.setIntRegister1(((short)1));
            ws.getIdsv0501().setIndOut(Trunc.toShort(abs(ws.getIntRegister1() + ws.getIdsv0501().getIndOut()), 2));
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501MaxTabStr(Trunc.toShort(ws.getIntRegister1() + ws.getIdsv0501().getIdsv0501Output().getIdsv0501MaxTabStr(), 2));
            // COB_CODE: IF IND-OUT > LIMITE-ARRAY-OUTPUT
            //              END-STRING
            //           ELSE
            //                                        SPAZIO-RICHIESTO
            //           END-IF
            if (ws.getIdsv0501().getIndOut() > ws.getIdsv0501().getLimiteArrayOutput()) {
                // COB_CODE: SET IDSV0501-GENERIC-ERROR  TO TRUE
                ws.getIdsv0501().getIdsv0501ReturnCode().setIdsv0501GenericError();
                // COB_CODE: STRING 'SUPERATO LIMITE STRUTTURE OUTPUT'
                //                  DELIMITED BY SIZE
                //                  INTO IDSV0501-DESCRIZ-ERR
                //           END-STRING
                ws.getIdsv0501().setIdsv0501DescrizErr("SUPERATO LIMITE STRUTTURE OUTPUT");
            }
            else {
                // COB_CODE: MOVE LIMITE-STRINGA  TO SPAZIO-RESTANTE
                ws.getIdsv0501().setSpazioRestanteFormatted(ws.getIdsv0501().getLimiteStringaFormatted());
                // COB_CODE: MOVE 1               TO POSIZIONE
                ws.getIdsv0501().setPosizione(((short)1));
                // COB_CODE: COMPUTE SPAZIO-RESTANTE = SPAZIO-RESTANTE -
                //                                     SPAZIO-RICHIESTO
                ws.getIdsv0501().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0501().getSpazioRestante() - ws.getIdsv0501().getSpazioRichiesto()), 2));
            }
        }
        else {
            // COB_CODE: COMPUTE SPAZIO-RESTANTE = SPAZIO-RESTANTE -
            //                                     SPAZIO-RICHIESTO
            ws.getIdsv0501().setSpazioRestante(Trunc.toShort(abs(ws.getIdsv0501().getSpazioRestante() - ws.getIdsv0501().getSpazioRichiesto()), 2));
        }
    }

    /**Original name: INDIVIDUA-IND-DEC<br>
	 * <pre>*************************************************************</pre>*/
    private void individuaIndDec() {
        // COB_CODE: SET DEC-TROVATO-NO                    TO TRUE.
        ws.getIdsv0501().getDecTrovato().setDecTrovatoNo();
        // COB_CODE: MOVE 1                                TO IND-DEC.
        ws.getIdsv0501().setIndDec(((short)1));
        // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
        //               WHEN IMPORTO
        //                    END-PERFORM
        //               WHEN PERCENTUALE
        //                    END-PERFORM
        //               WHEN TASSO
        //                    END-PERFORM
        //           END-EVALUATE.
        if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getImporto()) {
            // COB_CODE: PERFORM VARYING IND-RICERCA
            //               FROM IDSV0501-DECIMALI-ESPOSTI-IMP
            //               BY -1
            //               UNTIL IND-RICERCA <= ZEROES OR
            //                     DEC-TROVATO-SI
            //               END-IF
            //           END-PERFORM
            ws.getIdsv0501().setIndRicerca(ws.getIdsv0501().getIdsv0501DecimaliEspostiImp());
            while (!(ws.getIdsv0501().getIndRicerca() <= 0 || ws.getIdsv0501().getDecTrovato().isDecTrovatoSi())) {
                // COB_CODE: IF  ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) IS NUMERIC
                //           AND ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) NOT = ZEROES
                //               MOVE IND-RICERCA   TO IND-DEC
                //           END-IF
                if (Functions.isNumber(ws.getIdsv0501().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0501().getIndRicerca())) && !Conditions.eq(ws.getIdsv0501().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0501().getIndRicerca()), '0')) {
                    // COB_CODE: SET DEC-TROVATO-SI TO TRUE
                    ws.getIdsv0501().getDecTrovato().setDecTrovatoSi();
                    // COB_CODE: MOVE IND-RICERCA   TO IND-DEC
                    ws.getIdsv0501().setIndDec(ws.getIdsv0501().getIndRicerca());
                }
                ws.getIdsv0501().setIndRicerca(Trunc.toShort(ws.getIdsv0501().getIndRicerca() + (-1), 2));
            }
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getPercentuale()) {
            // COB_CODE: PERFORM VARYING IND-RICERCA
            //               FROM IDSV0501-DECIMALI-ESPOSTI-PERC
            //               BY -1
            //               UNTIL IND-RICERCA <= ZEROES OR
            //                     DEC-TROVATO-SI
            //               END-IF
            //           END-PERFORM
            ws.getIdsv0501().setIndRicerca(ws.getIdsv0501().getIdsv0501DecimaliEspostiPerc());
            while (!(ws.getIdsv0501().getIndRicerca() <= 0 || ws.getIdsv0501().getDecTrovato().isDecTrovatoSi())) {
                // COB_CODE: IF  ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) IS NUMERIC
                //           AND ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) NOT = ZEROES
                //               MOVE IND-RICERCA   TO IND-DEC
                //           END-IF
                if (Functions.isNumber(ws.getIdsv0501().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0501().getIndRicerca())) && !Conditions.eq(ws.getIdsv0501().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0501().getIndRicerca()), '0')) {
                    // COB_CODE: SET DEC-TROVATO-SI TO TRUE
                    ws.getIdsv0501().getDecTrovato().setDecTrovatoSi();
                    // COB_CODE: MOVE IND-RICERCA   TO IND-DEC
                    ws.getIdsv0501().setIndDec(ws.getIdsv0501().getIndRicerca());
                }
                ws.getIdsv0501().setIndRicerca(Trunc.toShort(ws.getIdsv0501().getIndRicerca() + (-1), 2));
            }
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getTasso()) {
            // COB_CODE: PERFORM VARYING IND-RICERCA
            //               FROM IDSV0501-DECIMALI-ESPOSTI-TASS
            //               BY -1
            //               UNTIL IND-RICERCA <= ZEROES OR
            //                     DEC-TROVATO-SI
            //               END-IF
            //           END-PERFORM
            ws.getIdsv0501().setIndRicerca(ws.getIdsv0501().getIdsv0501DecimaliEspostiTass());
            while (!(ws.getIdsv0501().getIndRicerca() <= 0 || ws.getIdsv0501().getDecTrovato().isDecTrovatoSi())) {
                // COB_CODE: IF  ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) IS NUMERIC
                //           AND ELE-STRINGA-DECIMALI
                //                            (IND-RICERCA) NOT = ZEROES
                //               MOVE IND-RICERCA   TO IND-DEC
                //           END-IF
                if (Functions.isNumber(ws.getIdsv0501().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0501().getIndRicerca())) && !Conditions.eq(ws.getIdsv0501().getCampoDecimali().getEleStringaDecimali(ws.getIdsv0501().getIndRicerca()), '0')) {
                    // COB_CODE: SET DEC-TROVATO-SI TO TRUE
                    ws.getIdsv0501().getDecTrovato().setDecTrovatoSi();
                    // COB_CODE: MOVE IND-RICERCA   TO IND-DEC
                    ws.getIdsv0501().setIndDec(ws.getIdsv0501().getIndRicerca());
                }
                ws.getIdsv0501().setIndRicerca(Trunc.toShort(ws.getIdsv0501().getIndRicerca() + (-1), 2));
            }
        }
    }

    /**Original name: LISTA-OMOGENEA<br>
	 * <pre>*************************************************************
	 *  SIR 20310 GESTIONE LISTE OMOGENEE
	 * *************************************************************</pre>*/
    private void listaOmogenea() {
        // COB_CODE: EVALUATE TP-DATO-PARALLELO(IND-VAR)
        //               WHEN STRINGA
        //                    MOVE LISTA-STRINGA        TO IDSV0501-TP-STRINGA
        //               WHEN DT
        //                    MOVE LISTA-DT             TO IDSV0501-TP-STRINGA
        //               WHEN IMPORTO
        //                    MOVE LISTA-IMPORTO        TO IDSV0501-TP-STRINGA
        //               WHEN PERCENTUALE
        //                    MOVE LISTA-PERCENTUALE    TO IDSV0501-TP-STRINGA
        //               WHEN TASSO
        //                    MOVE LISTA-TASSO          TO IDSV0501-TP-STRINGA
        //               WHEN NUMERICO
        //                    MOVE LISTA-NUMERICO       TO IDSV0501-TP-STRINGA
        //               WHEN MILLESIMI
        //                    MOVE LISTA-MILLESIMI      TO IDSV0501-TP-STRINGA
        //           END-EVALUATE.
        if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getStringa()) {
            // COB_CODE: MOVE LISTA-STRINGA        TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaStringa());
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getDt()) {
            // COB_CODE: MOVE LISTA-DT             TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaDt());
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getImporto()) {
            // COB_CODE: MOVE LISTA-IMPORTO        TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaImporto());
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getPercentuale()) {
            // COB_CODE: MOVE LISTA-PERCENTUALE    TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaPercentuale());
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getTasso()) {
            // COB_CODE: MOVE LISTA-TASSO          TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaTasso());
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getNumerico()) {
            // COB_CODE: MOVE LISTA-NUMERICO       TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaNumerico());
        }
        else if (ws.getIdsv0501().getTabellaTpDato(ws.getIdsv0501().getIndVar()).getTpDatoParallelo() == ws.getIdsv0501().getTipoFormato().getMillesimi()) {
            // COB_CODE: MOVE LISTA-MILLESIMI      TO IDSV0501-TP-STRINGA
            ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(ws.getIdsv0501().getTipoFormato().getListaMillesimi());
        }
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabL19(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabVas(((short)0));
        ws.getIxIndici().setGuidaGrz(((short)0));
        ws.getIxIndici().setTab0501(((short)0));
        ws.getIxIndici().setTabTemp(((short)0));
        ws.getIxIndici().setTabPos(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initIdsv0501Input() {
        for (int idx0 = 1; idx0 <= Idsv0501.IDSV0501_TAB_VARIABILI_MAXOCCURS; idx0++) {
            ws.getIdsv0501().getIdsv0501TabVariabili(idx0).setIdsv0501TpDato(Types.SPACE_CHAR);
            ws.getIdsv0501().getIdsv0501TabVariabili(idx0).setIdsv0501ValNum(0);
            ws.getIdsv0501().getIdsv0501TabVariabili(idx0).setIdsv0501ValImp(new AfDecimal(0, 18, 7));
            ws.getIdsv0501().getIdsv0501TabVariabili(idx0).setIdsv0501ValPerc(new AfDecimal(0, 18, 9));
            ws.getIdsv0501().getIdsv0501TabVariabili(idx0).setIdsv0501ValStr("");
        }
    }

    public void initIvvc0501Output() {
        ivvc0501Output.setTpStringa(Types.SPACE_CHAR);
        ivvc0501Output.setMaxTabStr(((short)0));
        for (int idx0 = 1; idx0 <= Ivvc0501Output.TAB_STRINGHE_TOT_MAXOCCURS; idx0++) {
            ivvc0501Output.getTabStringheTot(idx0).setIvvc0501StringaTot("");
        }
    }

    public void initValAst() {
        ws.getValAst().setVasIdValAst(0);
        ws.getValAst().getVasIdTrchDiGar().setVasIdTrchDiGar(0);
        ws.getValAst().setVasIdMoviFinrio(0);
        ws.getValAst().setVasIdMoviCrz(0);
        ws.getValAst().getVasIdMoviChiu().setVasIdMoviChiu(0);
        ws.getValAst().setVasDtIniEff(0);
        ws.getValAst().setVasDtEndEff(0);
        ws.getValAst().setVasCodCompAnia(0);
        ws.getValAst().setVasCodFnd("");
        ws.getValAst().getVasNumQuo().setVasNumQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasValQuo().setVasValQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzz().setVasDtValzz(0);
        ws.getValAst().setVasTpValAst("");
        ws.getValAst().getVasIdRichInvstFnd().setVasIdRichInvstFnd(0);
        ws.getValAst().getVasIdRichDisFnd().setVasIdRichDisFnd(0);
        ws.getValAst().setVasDsRiga(0);
        ws.getValAst().setVasDsOperSql(Types.SPACE_CHAR);
        ws.getValAst().setVasDsVer(0);
        ws.getValAst().setVasDsTsIniCptz(0);
        ws.getValAst().setVasDsTsEndCptz(0);
        ws.getValAst().setVasDsUtente("");
        ws.getValAst().setVasDsStatoElab(Types.SPACE_CHAR);
        ws.getValAst().getVasPreMovto().setVasPreMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasImpMovto().setVasImpMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPcInvDis().setVasPcInvDis(new AfDecimal(0, 14, 9));
        ws.getValAst().getVasNumQuoLorde().setVasNumQuoLorde(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzzCalc().setVasDtValzzCalc(0);
        ws.getValAst().getVasMinusValenza().setVasMinusValenza(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPlusValenza().setVasPlusValenza(new AfDecimal(0, 15, 3));
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0560Data.DTGA_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getDtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
    }

    public void initAreaIoGar() {
        ws.setDgrzEleGarMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0560Data.DGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getDgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initAreaIoL19() {
        ws.setDl19EleFndMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0560Data.DL19_TAB_FND_MAXOCCURS; idx0++) {
            ws.getDl19TabFnd(idx0).getLccvl191().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19CodCompAnia(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19CodFnd("");
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DtQtz(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuo(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuoManfee().setDl19ValQuoManfee(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsOperSql(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsVer(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsTsCptz(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsUtente("");
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsStatoElab(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19TpFnd(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19DtRilevazioneNav().setDl19DtRilevazioneNav(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuoAcq().setDl19ValQuoAcq(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19FlNoNav(Types.SPACE_CHAR);
        }
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initMoviFinrio() {
        ws.getMoviFinrio().setMfzIdMoviFinrio(0);
        ws.getMoviFinrio().getMfzIdAdes().setMfzIdAdes(0);
        ws.getMoviFinrio().getMfzIdLiq().setMfzIdLiq(0);
        ws.getMoviFinrio().getMfzIdTitCont().setMfzIdTitCont(0);
        ws.getMoviFinrio().setMfzIdMoviCrz(0);
        ws.getMoviFinrio().getMfzIdMoviChiu().setMfzIdMoviChiu(0);
        ws.getMoviFinrio().setMfzDtIniEff(0);
        ws.getMoviFinrio().setMfzDtEndEff(0);
        ws.getMoviFinrio().setMfzCodCompAnia(0);
        ws.getMoviFinrio().setMfzTpMoviFinrio("");
        ws.getMoviFinrio().getMfzDtEffMoviFinrio().setMfzDtEffMoviFinrio(0);
        ws.getMoviFinrio().getMfzDtElab().setMfzDtElab(0);
        ws.getMoviFinrio().getMfzDtRichMovi().setMfzDtRichMovi(0);
        ws.getMoviFinrio().getMfzCosOprz().setMfzCosOprz(new AfDecimal(0, 15, 3));
        ws.getMoviFinrio().setMfzStatMovi("");
        ws.getMoviFinrio().setMfzDsRiga(0);
        ws.getMoviFinrio().setMfzDsOperSql(Types.SPACE_CHAR);
        ws.getMoviFinrio().setMfzDsVer(0);
        ws.getMoviFinrio().setMfzDsTsIniCptz(0);
        ws.getMoviFinrio().setMfzDsTsEndCptz(0);
        ws.getMoviFinrio().setMfzDsUtente("");
        ws.getMoviFinrio().setMfzDsStatoElab(Types.SPACE_CHAR);
        ws.getMoviFinrio().setMfzTpCausRettifica("");
    }

    public void initWkAppoDate() {
        ws.getWkAppoDate().setInizioEffetto(0);
        ws.getWkAppoDate().setFineEffetto(0);
        ws.getWkAppoDate().setCompetenza(0);
    }

    public void initLdbv4911() {
        ws.getLdbv4911().setTpValAst1("");
        ws.getLdbv4911().setTpValAst2("");
        ws.getLdbv4911().setTpValAst3("");
        ws.getLdbv4911().setTpValAst4("");
        ws.getLdbv4911().setTpValAst5("");
        ws.getLdbv4911().setTpValAst6("");
        ws.getLdbv4911().setTpValAst7("");
        ws.getLdbv4911().setTpValAst8("");
        ws.getLdbv4911().setTpValAst9("");
        ws.getLdbv4911().setTpValAst10("");
        ws.getLdbv4911().setIdTrchDiGar(0);
    }

    public void initRichInvstFnd() {
        ws.getRichInvstFnd().setRifIdRichInvstFnd(0);
        ws.getRichInvstFnd().setRifIdMoviFinrio(0);
        ws.getRichInvstFnd().setRifIdMoviCrz(0);
        ws.getRichInvstFnd().getRifIdMoviChiu().setRifIdMoviChiu(0);
        ws.getRichInvstFnd().setRifDtIniEff(0);
        ws.getRichInvstFnd().setRifDtEndEff(0);
        ws.getRichInvstFnd().setRifCodCompAnia(0);
        ws.getRichInvstFnd().setRifCodFnd("");
        ws.getRichInvstFnd().getRifNumQuo().setRifNumQuo(new AfDecimal(0, 12, 5));
        ws.getRichInvstFnd().getRifPc().setRifPc(new AfDecimal(0, 6, 3));
        ws.getRichInvstFnd().getRifImpMovto().setRifImpMovto(new AfDecimal(0, 15, 3));
        ws.getRichInvstFnd().getRifDtInvst().setRifDtInvst(0);
        ws.getRichInvstFnd().setRifCodTari("");
        ws.getRichInvstFnd().setRifTpStat("");
        ws.getRichInvstFnd().setRifTpModInvst("");
        ws.getRichInvstFnd().setRifCodDiv("");
        ws.getRichInvstFnd().getRifDtCambioVlt().setRifDtCambioVlt(0);
        ws.getRichInvstFnd().setRifTpFnd(Types.SPACE_CHAR);
        ws.getRichInvstFnd().setRifDsRiga(0);
        ws.getRichInvstFnd().setRifDsOperSql(Types.SPACE_CHAR);
        ws.getRichInvstFnd().setRifDsVer(0);
        ws.getRichInvstFnd().setRifDsTsIniCptz(0);
        ws.getRichInvstFnd().setRifDsTsEndCptz(0);
        ws.getRichInvstFnd().setRifDsUtente("");
        ws.getRichInvstFnd().setRifDsStatoElab(Types.SPACE_CHAR);
        ws.getRichInvstFnd().getRifDtInvstCalc().setRifDtInvstCalc(0);
        ws.getRichInvstFnd().setRifFlCalcInvto(Types.SPACE_CHAR);
        ws.getRichInvstFnd().getRifImpGapEvent().setRifImpGapEvent(new AfDecimal(0, 15, 3));
        ws.getRichInvstFnd().setRifFlSwmBp2s(Types.SPACE_CHAR);
    }

    public void initRichDisFnd() {
        ws.getRichDisFnd().setRdfIdRichDisFnd(0);
        ws.getRichDisFnd().setRdfIdMoviFinrio(0);
        ws.getRichDisFnd().setRdfIdMoviCrz(0);
        ws.getRichDisFnd().getRdfIdMoviChiu().setRdfIdMoviChiu(0);
        ws.getRichDisFnd().setRdfDtIniEff(0);
        ws.getRichDisFnd().setRdfDtEndEff(0);
        ws.getRichDisFnd().setRdfCodCompAnia(0);
        ws.getRichDisFnd().setRdfCodFnd("");
        ws.getRichDisFnd().getRdfNumQuo().setRdfNumQuo(new AfDecimal(0, 12, 5));
        ws.getRichDisFnd().getRdfPc().setRdfPc(new AfDecimal(0, 6, 3));
        ws.getRichDisFnd().getRdfImpMovto().setRdfImpMovto(new AfDecimal(0, 15, 3));
        ws.getRichDisFnd().getRdfDtDis().setRdfDtDis(0);
        ws.getRichDisFnd().setRdfCodTari("");
        ws.getRichDisFnd().setRdfTpStat("");
        ws.getRichDisFnd().setRdfTpModDis("");
        ws.getRichDisFnd().setRdfCodDiv("");
        ws.getRichDisFnd().getRdfDtCambioVlt().setRdfDtCambioVlt(0);
        ws.getRichDisFnd().setRdfTpFnd(Types.SPACE_CHAR);
        ws.getRichDisFnd().setRdfDsRiga(0);
        ws.getRichDisFnd().setRdfDsOperSql(Types.SPACE_CHAR);
        ws.getRichDisFnd().setRdfDsVer(0);
        ws.getRichDisFnd().setRdfDsTsIniCptz(0);
        ws.getRichDisFnd().setRdfDsTsEndCptz(0);
        ws.getRichDisFnd().setRdfDsUtente("");
        ws.getRichDisFnd().setRdfDsStatoElab(Types.SPACE_CHAR);
        ws.getRichDisFnd().getRdfDtDisCalc().setRdfDtDisCalc(0);
        ws.getRichDisFnd().setRdfFlCalcDis(Types.SPACE_CHAR);
        ws.getRichDisFnd().getRdfCommisGest().setRdfCommisGest(new AfDecimal(0, 18, 7));
        ws.getRichDisFnd().getRdfNumQuoCdgFnz().setRdfNumQuoCdgFnz(new AfDecimal(0, 12, 5));
        ws.getRichDisFnd().getRdfNumQuoCdgtotFnz().setRdfNumQuoCdgtotFnz(new AfDecimal(0, 12, 5));
        ws.getRichDisFnd().getRdfCosRunAssvaIdc().setRdfCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        ws.getRichDisFnd().setRdfFlSwmBp2s(Types.SPACE_CHAR);
    }

    public void initIdsv0501Output() {
        ws.getIdsv0501().getIdsv0501Output().setIdsv0501TpStringa(Types.SPACE_CHAR);
        ws.getIdsv0501().getIdsv0501Output().setIdsv0501MaxTabStr(((short)0));
        for (int idx0 = 1; idx0 <= Idsv0501Output.TAB_STRINGHE_TOT_MAXOCCURS; idx0++) {
            ws.getIdsv0501().getIdsv0501Output().getTabStringheTot(idx0).setIdsv0501StringaTot("");
        }
    }
}
