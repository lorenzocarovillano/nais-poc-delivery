package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DbService;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.ConnectionManager;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Area0140Operazione;
import it.accenture.jnais.ws.Iabs0140Data;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IABS0140<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE CONNECT / DISCONNECT
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0140 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    //Original name: WORKING-STORAGE
    private Iabs0140Data ws = new Iabs0140Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0140_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003) {
        this.idsv0003 = idsv0003;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM B000-VALORIZZA-BUFFER    THRU B000-EX.
        b000ValorizzaBuffer();
        // COB_CODE: PERFORM C000-CTRL-INPUT          THRU C000-EX.
        c000CtrlInput();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A300-ELABORA          THRU A300-EX
        //           END-IF
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A300-ELABORA          THRU A300-EX
            a300Elabora();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0140 getInstance() {
        return ((Iabs0140)Programs.getInstance(Iabs0140.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0140'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0140");
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
    }

    /**Original name: A100-VALORIZZA-RITORNO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100ValorizzaRitorno() {
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2.
        idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
    }

    /**Original name: B000-VALORIZZA-BUFFER<br>
	 * <pre>*****************************************************************</pre>*/
    private void b000ValorizzaBuffer() {
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO AREA-X-IABS0140.
        ws.getAreaXIabs0140().setAreaXIabs0140Formatted(idsv0003.getBufferWhereCondFormatted());
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN AREA0140-CONNECT
        //                 PERFORM A310-CONNECT       THRU A310-EX
        //              WHEN AREA0140-DISCONNECT
        //                 PERFORM A320-DISCONNECT    THRU A320-EX
        //              WHEN AREA0140-CONNECT-RESET
        //                 PERFORM A330-CONNECT-RESET THRU A330-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-EVALUATE.
        switch (ws.getAreaXIabs0140().getOperazione().getOperazione()) {

            case Area0140Operazione.CONNECT:// COB_CODE: PERFORM A310-CONNECT       THRU A310-EX
                a310Connect();
                break;

            case Area0140Operazione.DISCONNECT:// COB_CODE: PERFORM A320-DISCONNECT    THRU A320-EX
                a320Disconnect();
                break;

            case Area0140Operazione.CONNECT_RESET:// COB_CODE: PERFORM A330-CONNECT-RESET THRU A330-EX
                a330ConnectReset();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                break;
        }
    }

    /**Original name: A310-CONNECT<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESEGUI CONNECT
	 * ----------------------------------------------------------------*</pre>*/
    private void a310Connect() {
        // COB_CODE: MOVE 'A310-CONNECT'               TO WK-LABEL.
        ws.setWkLabel("A310-CONNECT");
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A100-VALORIZZA-RITORNO THRU A100-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE AREA0140-DATABASE-NAME       TO WK-DATABASE-NAME
            ws.setWkDatabaseName(ws.getAreaXIabs0140().getDatabaseName());
            // COB_CODE: MOVE AREA0140-USER                TO WK-USER
            ws.setWkUser(ws.getAreaXIabs0140().getUser());
            // COB_CODE: MOVE AREA0140-PASSWORD            TO WK-PASSWORD
            ws.setWkPassword(ws.getAreaXIabs0140().getPassword());
            // COB_CODE: EXEC SQL
            //                CONNECT TO    :WK-DATABASE-NAME
            //                        USER  :WK-USER
            //                        USING :WK-PASSWORD
            //           END-EXEC
            ConnectionManager.connect(dbAccessStatus, ws.getWkUser());
            // COB_CODE: PERFORM A100-VALORIZZA-RITORNO THRU A100-EX
            a100ValorizzaRitorno();
        }
    }

    /**Original name: A320-DISCONNECT<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESEGUI DISCONNECT
	 * ----------------------------------------------------------------*</pre>*/
    private void a320Disconnect() {
        // COB_CODE: MOVE 'A320-DISCONNECT'        TO WK-LABEL.
        ws.setWkLabel("A320-DISCONNECT");
        // COB_CODE: MOVE AREA0140-DATABASE-NAME       TO WK-DATABASE-NAME
        ws.setWkDatabaseName(ws.getAreaXIabs0140().getDatabaseName());
        // COB_CODE: EXEC SQL
        //                DISCONNECT    :WK-DATABASE-NAME
        //           END-EXEC.
        DbService.getCurrent().close();
        // COB_CODE: PERFORM A100-VALORIZZA-RITORNO THRU A100-EX.
        a100ValorizzaRitorno();
    }

    /**Original name: A330-CONNECT-RESET<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESEGUI CONNECT RESET
	 * ----------------------------------------------------------------*</pre>*/
    private void a330ConnectReset() {
        // COB_CODE: MOVE 'A330-CONNECT-RESET '        TO WK-LABEL.
        ws.setWkLabel("A330-CONNECT-RESET ");
        // COB_CODE: EXEC SQL
        //                CONNECT RESET
        //           END-EXEC.
        ConnectionManager.connectLocal(dbAccessStatus);
        // COB_CODE: PERFORM A100-VALORIZZA-RITORNO    THRU A100-EX.
        a100ValorizzaRitorno();
    }

    /**Original name: C000-CTRL-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *  controlli input
	 * ----------------------------------------------------------------*</pre>*/
    private void c000CtrlInput() {
        // COB_CODE: MOVE 'C000-CTRL-INPUT'               TO WK-LABEL.
        ws.setWkLabel("C000-CTRL-INPUT");
        // COB_CODE: IF AREA0140-DATABASE-NAME = LOW-VALUE  OR
        //                                       HIGH-VALUE OR
        //                                       SPACES
        //              MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE
        //           END-IF.
        if (Characters.EQ_LOW.test(ws.getAreaXIabs0140().getDatabaseNameFormatted()) || Characters.EQ_HIGH.test(ws.getAreaXIabs0140().getDatabaseNameFormatted()) || Characters.EQ_SPACE.test(ws.getAreaXIabs0140().getDatabaseName())) {
            // COB_CODE: SET IDSV0003-GENERIC-ERROR TO TRUE
            idsv0003.getReturnCode().setGenericError();
            // COB_CODE: MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
            //skipped translation for moving SPACES to IDSV0003-DESCRIZ-ERR-DB2; considered in STRING statement translation below
            // COB_CODE: STRING 'DATABASE NAME DI CONNESSIONE NON VALIDA'
            //                  DELIMITED BY SIZE
            //                  INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            idsv0003.getCampiEsito().setDescrizErrDb2("DATABASE NAME DI CONNESSIONE NON VALIDA");
            // COB_CODE: MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("IABS0140");
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF AREA0140-USER          = LOW-VALUE  OR
            //                                       HIGH-VALUE OR
            //                                       SPACES
            //              MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE
            //           END-IF
            if (Characters.EQ_LOW.test(ws.getAreaXIabs0140().getUserFormatted()) || Characters.EQ_HIGH.test(ws.getAreaXIabs0140().getUserFormatted()) || Characters.EQ_SPACE.test(ws.getAreaXIabs0140().getUser())) {
                // COB_CODE: SET IDSV0003-GENERIC-ERROR TO TRUE
                idsv0003.getReturnCode().setGenericError();
                // COB_CODE: MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
                //skipped translation for moving SPACES to IDSV0003-DESCRIZ-ERR-DB2; considered in STRING statement translation below
                // COB_CODE: STRING 'USER DI CONNESSIONE NON VALIDA'
                //                  DELIMITED BY SIZE
                //                  INTO
                //                  IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                idsv0003.getCampiEsito().setDescrizErrDb2("USER DI CONNESSIONE NON VALIDA");
                // COB_CODE: MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe("IABS0140");
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: IF AREA0140-PASSWORD      = LOW-VALUE  OR
                //                                       HIGH-VALUE OR
                //                                       SPACES
                //              MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE
                //           END-IF
                if (Characters.EQ_LOW.test(ws.getAreaXIabs0140().getPasswordFormatted()) || Characters.EQ_HIGH.test(ws.getAreaXIabs0140().getPasswordFormatted()) || Characters.EQ_SPACE.test(ws.getAreaXIabs0140().getPassword())) {
                    // COB_CODE: SET IDSV0003-GENERIC-ERROR TO TRUE
                    idsv0003.getReturnCode().setGenericError();
                    // COB_CODE: MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
                    //skipped translation for moving SPACES to IDSV0003-DESCRIZ-ERR-DB2; considered in STRING statement translation below
                    // COB_CODE: STRING 'PASSWORD DI CONNESSIONE NON VALIDA'
                    //                  DELIMITED BY SIZE
                    //                  INTO
                    //                  IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    idsv0003.getCampiEsito().setDescrizErrDb2("PASSWORD DI CONNESSIONE NON VALIDA");
                    // COB_CODE: MOVE 'IABS0140'  TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe("IABS0140");
                }
            }
        }
    }
}
