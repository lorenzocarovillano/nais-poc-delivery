package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ImpstBolloDao;
import it.accenture.jnais.commons.data.to.IImpstBollo;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.ImpstBollo;
import it.accenture.jnais.ws.Ldbse590Data;
import it.accenture.jnais.ws.redefines.P58IdMoviChiu;
import it.accenture.jnais.ws.redefines.P58ImpstBolloDettV;
import it.accenture.jnais.ws.redefines.P58ImpstBolloTotV;

/**Original name: LDBSE590<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 OTT 2013.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbse590 extends Program implements IImpstBollo {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ImpstBolloDao impstBolloDao = new ImpstBolloDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbse590Data ws = new Ldbse590Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IMPST-BOLLO
    private ImpstBollo impstBollo;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBSE590_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ImpstBollo impstBollo) {
        this.idsv0003 = idsv0003;
        this.impstBollo = impstBollo;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbse590 getInstance() {
        return ((Ldbse590)Programs.getInstance(Ldbse590.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBSE590'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBSE590");
        // COB_CODE: MOVE 'IMPST-BOLLO' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("IMPST-BOLLO");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM IMPST_BOLLO
        //              WHERE  ID_POLI = :P58-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE  ID_POLI = :P58-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        impstBolloDao.selectRec10(impstBollo.getP58IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        impstBolloDao.openCEff45(impstBollo.getP58IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        impstBolloDao.closeCEff45();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCEff45(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM IMPST_BOLLO
        //              WHERE  ID_POLI = :P58-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_IMPST_BOLLO
        //                    ,COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,IB_POLI
        //                    ,COD_FISC
        //                    ,COD_PART_IVA
        //                    ,ID_RAPP_ANA
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_CALC
        //                    ,DT_END_CALC
        //                    ,TP_CAUS_BOLLO
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_DETT_V
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_TOT_R
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //             FROM IMPST_BOLLO
        //             WHERE  ID_POLI = :P58-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        impstBolloDao.selectRec11(impstBollo.getP58IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        impstBolloDao.openCCpz45(impstBollo.getP58IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        impstBolloDao.closeCCpz45();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :P58-ID-IMPST-BOLLO
        //               ,:P58-COD-COMP-ANIA
        //               ,:P58-ID-POLI
        //               ,:P58-IB-POLI
        //               ,:P58-COD-FISC
        //                :IND-P58-COD-FISC
        //               ,:P58-COD-PART-IVA
        //                :IND-P58-COD-PART-IVA
        //               ,:P58-ID-RAPP-ANA
        //               ,:P58-ID-MOVI-CRZ
        //               ,:P58-ID-MOVI-CHIU
        //                :IND-P58-ID-MOVI-CHIU
        //               ,:P58-DT-INI-EFF-DB
        //               ,:P58-DT-END-EFF-DB
        //               ,:P58-DT-INI-CALC-DB
        //               ,:P58-DT-END-CALC-DB
        //               ,:P58-TP-CAUS-BOLLO
        //               ,:P58-IMPST-BOLLO-DETT-C
        //               ,:P58-IMPST-BOLLO-DETT-V
        //                :IND-P58-IMPST-BOLLO-DETT-V
        //               ,:P58-IMPST-BOLLO-TOT-V
        //                :IND-P58-IMPST-BOLLO-TOT-V
        //               ,:P58-IMPST-BOLLO-TOT-R
        //               ,:P58-DS-RIGA
        //               ,:P58-DS-OPER-SQL
        //               ,:P58-DS-VER
        //               ,:P58-DS-TS-INI-CPTZ
        //               ,:P58-DS-TS-END-CPTZ
        //               ,:P58-DS-UTENTE
        //               ,:P58-DS-STATO-ELAB
        //           END-EXEC.
        impstBolloDao.fetchCCpz45(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicitä
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilitä comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P58-COD-FISC = -1
        //              MOVE HIGH-VALUES TO P58-COD-FISC-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getValQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-COD-FISC-NULL
            impstBollo.setP58CodFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ImpstBollo.Len.P58_COD_FISC));
        }
        // COB_CODE: IF IND-P58-COD-PART-IVA = -1
        //              MOVE HIGH-VALUES TO P58-COD-PART-IVA-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getValQuoManfee() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-COD-PART-IVA-NULL
            impstBollo.setP58CodPartIva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ImpstBollo.Len.P58_COD_PART_IVA));
        }
        // COB_CODE: IF IND-P58-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO P58-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getDtRilevazioneNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-ID-MOVI-CHIU-NULL
            impstBollo.getP58IdMoviChiu().setP58IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58IdMoviChiu.Len.P58_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-P58-IMPST-BOLLO-DETT-V = -1
        //              MOVE HIGH-VALUES TO P58-IMPST-BOLLO-DETT-V-NULL
        //           END-IF
        if (ws.getIndImpstBollo().getValQuoAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-IMPST-BOLLO-DETT-V-NULL
            impstBollo.getP58ImpstBolloDettV().setP58ImpstBolloDettVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58ImpstBolloDettV.Len.P58_IMPST_BOLLO_DETT_V_NULL));
        }
        // COB_CODE: IF IND-P58-IMPST-BOLLO-TOT-V = -1
        //              MOVE HIGH-VALUES TO P58-IMPST-BOLLO-TOT-V-NULL
        //           END-IF.
        if (ws.getIndImpstBollo().getFlNoNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P58-IMPST-BOLLO-TOT-V-NULL
            impstBollo.getP58ImpstBolloTotV().setP58ImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P58ImpstBolloTotV.Len.P58_IMPST_BOLLO_TOT_V_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P58-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-INI-EFF
        impstBollo.setP58DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P58-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-END-EFF
        impstBollo.setP58DtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P58-DT-INI-CALC-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getPervRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-INI-CALC
        impstBollo.setP58DtIniCalc(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P58-DT-END-CALC-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstBolloDb().getEsecRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P58-DT-END-CALC.
        impstBollo.setP58DtEndCalc(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return impstBollo.getP58CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.impstBollo.setP58CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFisc() {
        return impstBollo.getP58CodFisc();
    }

    @Override
    public void setCodFisc(String codFisc) {
        this.impstBollo.setP58CodFisc(codFisc);
    }

    @Override
    public String getCodFiscObj() {
        if (ws.getIndImpstBollo().getValQuo() >= 0) {
            return getCodFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscObj(String codFiscObj) {
        if (codFiscObj != null) {
            setCodFisc(codFiscObj);
            ws.getIndImpstBollo().setValQuo(((short)0));
        }
        else {
            ws.getIndImpstBollo().setValQuo(((short)-1));
        }
    }

    @Override
    public String getCodPartIva() {
        return impstBollo.getP58CodPartIva();
    }

    @Override
    public void setCodPartIva(String codPartIva) {
        this.impstBollo.setP58CodPartIva(codPartIva);
    }

    @Override
    public String getCodPartIvaObj() {
        if (ws.getIndImpstBollo().getValQuoManfee() >= 0) {
            return getCodPartIva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPartIvaObj(String codPartIvaObj) {
        if (codPartIvaObj != null) {
            setCodPartIva(codPartIvaObj);
            ws.getIndImpstBollo().setValQuoManfee(((short)0));
        }
        else {
            ws.getIndImpstBollo().setValQuoManfee(((short)-1));
        }
    }

    @Override
    public AfDecimal getDettC() {
        throw new FieldNotMappedException("dettC");
    }

    @Override
    public void setDettC(AfDecimal dettC) {
        throw new FieldNotMappedException("dettC");
    }

    @Override
    public AfDecimal getDettV() {
        throw new FieldNotMappedException("dettV");
    }

    @Override
    public void setDettV(AfDecimal dettV) {
        throw new FieldNotMappedException("dettV");
    }

    @Override
    public AfDecimal getDettVObj() {
        return getDettV();
    }

    @Override
    public void setDettVObj(AfDecimal dettVObj) {
        setDettV(new AfDecimal(dettVObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        return impstBollo.getP58DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.impstBollo.setP58DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return impstBollo.getP58DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.impstBollo.setP58DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return impstBollo.getP58DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.impstBollo.setP58DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return impstBollo.getP58DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.impstBollo.setP58DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return impstBollo.getP58DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.impstBollo.setP58DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return impstBollo.getP58DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.impstBollo.setP58DsVer(dsVer);
    }

    @Override
    public String getDtEndCalcDb() {
        return ws.getImpstBolloDb().getEsecRichDb();
    }

    @Override
    public void setDtEndCalcDb(String dtEndCalcDb) {
        this.ws.getImpstBolloDb().setEsecRichDb(dtEndCalcDb);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getImpstBolloDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getImpstBolloDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniCalcDb() {
        return ws.getImpstBolloDb().getPervRichDb();
    }

    @Override
    public void setDtIniCalcDb(String dtIniCalcDb) {
        this.ws.getImpstBolloDb().setPervRichDb(dtIniCalcDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getImpstBolloDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getImpstBolloDb().setEffDb(dtIniEffDb);
    }

    @Override
    public String getIbPoli() {
        return impstBollo.getP58IbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.impstBollo.setP58IbPoli(ibPoli);
    }

    @Override
    public int getIdImpstBollo() {
        return impstBollo.getP58IdImpstBollo();
    }

    @Override
    public void setIdImpstBollo(int idImpstBollo) {
        this.impstBollo.setP58IdImpstBollo(idImpstBollo);
    }

    @Override
    public int getIdMoviChiu() {
        return impstBollo.getP58IdMoviChiu().getP58IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.impstBollo.getP58IdMoviChiu().setP58IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndImpstBollo().getDtRilevazioneNav() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndImpstBollo().setDtRilevazioneNav(((short)0));
        }
        else {
            ws.getIndImpstBollo().setDtRilevazioneNav(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return impstBollo.getP58IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.impstBollo.setP58IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return impstBollo.getP58IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.impstBollo.setP58IdPoli(idPoli);
    }

    @Override
    public int getIdRappAna() {
        return impstBollo.getP58IdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.impstBollo.setP58IdRappAna(idRappAna);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public AfDecimal getImpstBolloDettC() {
        return impstBollo.getP58ImpstBolloDettC();
    }

    @Override
    public void setImpstBolloDettC(AfDecimal impstBolloDettC) {
        this.impstBollo.setP58ImpstBolloDettC(impstBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettV() {
        return impstBollo.getP58ImpstBolloDettV().getP58ImpstBolloDettV();
    }

    @Override
    public void setImpstBolloDettV(AfDecimal impstBolloDettV) {
        this.impstBollo.getP58ImpstBolloDettV().setP58ImpstBolloDettV(impstBolloDettV.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettVObj() {
        if (ws.getIndImpstBollo().getValQuoAcq() >= 0) {
            return getImpstBolloDettV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettVObj(AfDecimal impstBolloDettVObj) {
        if (impstBolloDettVObj != null) {
            setImpstBolloDettV(new AfDecimal(impstBolloDettVObj, 15, 3));
            ws.getIndImpstBollo().setValQuoAcq(((short)0));
        }
        else {
            ws.getIndImpstBollo().setValQuoAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotR() {
        return impstBollo.getP58ImpstBolloTotR();
    }

    @Override
    public void setImpstBolloTotR(AfDecimal impstBolloTotR) {
        this.impstBollo.setP58ImpstBolloTotR(impstBolloTotR.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotV() {
        return impstBollo.getP58ImpstBolloTotV().getP58ImpstBolloTotV();
    }

    @Override
    public void setImpstBolloTotV(AfDecimal impstBolloTotV) {
        this.impstBollo.getP58ImpstBolloTotV().setP58ImpstBolloTotV(impstBolloTotV.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVObj() {
        if (ws.getIndImpstBollo().getFlNoNav() >= 0) {
            return getImpstBolloTotV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj) {
        if (impstBolloTotVObj != null) {
            setImpstBolloTotV(new AfDecimal(impstBolloTotVObj, 15, 3));
            ws.getIndImpstBollo().setFlNoNav(((short)0));
        }
        else {
            ws.getIndImpstBollo().setFlNoNav(((short)-1));
        }
    }

    @Override
    public String getLdbve391CodFisc() {
        throw new FieldNotMappedException("ldbve391CodFisc");
    }

    @Override
    public void setLdbve391CodFisc(String ldbve391CodFisc) {
        throw new FieldNotMappedException("ldbve391CodFisc");
    }

    @Override
    public String getLdbve391DtEndCalcDb() {
        throw new FieldNotMappedException("ldbve391DtEndCalcDb");
    }

    @Override
    public void setLdbve391DtEndCalcDb(String ldbve391DtEndCalcDb) {
        throw new FieldNotMappedException("ldbve391DtEndCalcDb");
    }

    @Override
    public String getLdbve391DtIniCalcDb() {
        throw new FieldNotMappedException("ldbve391DtIniCalcDb");
    }

    @Override
    public void setLdbve391DtIniCalcDb(String ldbve391DtIniCalcDb) {
        throw new FieldNotMappedException("ldbve391DtIniCalcDb");
    }

    @Override
    public String getLdbve391TpCausBollo1() {
        throw new FieldNotMappedException("ldbve391TpCausBollo1");
    }

    @Override
    public void setLdbve391TpCausBollo1(String ldbve391TpCausBollo1) {
        throw new FieldNotMappedException("ldbve391TpCausBollo1");
    }

    @Override
    public String getLdbve391TpCausBollo2() {
        throw new FieldNotMappedException("ldbve391TpCausBollo2");
    }

    @Override
    public void setLdbve391TpCausBollo2(String ldbve391TpCausBollo2) {
        throw new FieldNotMappedException("ldbve391TpCausBollo2");
    }

    @Override
    public String getLdbve391TpCausBollo3() {
        throw new FieldNotMappedException("ldbve391TpCausBollo3");
    }

    @Override
    public void setLdbve391TpCausBollo3(String ldbve391TpCausBollo3) {
        throw new FieldNotMappedException("ldbve391TpCausBollo3");
    }

    @Override
    public String getLdbve391TpCausBollo4() {
        throw new FieldNotMappedException("ldbve391TpCausBollo4");
    }

    @Override
    public void setLdbve391TpCausBollo4(String ldbve391TpCausBollo4) {
        throw new FieldNotMappedException("ldbve391TpCausBollo4");
    }

    @Override
    public String getLdbve391TpCausBollo5() {
        throw new FieldNotMappedException("ldbve391TpCausBollo5");
    }

    @Override
    public void setLdbve391TpCausBollo5(String ldbve391TpCausBollo5) {
        throw new FieldNotMappedException("ldbve391TpCausBollo5");
    }

    @Override
    public String getLdbve391TpCausBollo6() {
        throw new FieldNotMappedException("ldbve391TpCausBollo6");
    }

    @Override
    public void setLdbve391TpCausBollo6(String ldbve391TpCausBollo6) {
        throw new FieldNotMappedException("ldbve391TpCausBollo6");
    }

    @Override
    public String getLdbve391TpCausBollo7() {
        throw new FieldNotMappedException("ldbve391TpCausBollo7");
    }

    @Override
    public void setLdbve391TpCausBollo7(String ldbve391TpCausBollo7) {
        throw new FieldNotMappedException("ldbve391TpCausBollo7");
    }

    @Override
    public String getLdbve421CodPartIva() {
        throw new FieldNotMappedException("ldbve421CodPartIva");
    }

    @Override
    public void setLdbve421CodPartIva(String ldbve421CodPartIva) {
        throw new FieldNotMappedException("ldbve421CodPartIva");
    }

    @Override
    public String getLdbve421DtEndCalcDb() {
        throw new FieldNotMappedException("ldbve421DtEndCalcDb");
    }

    @Override
    public void setLdbve421DtEndCalcDb(String ldbve421DtEndCalcDb) {
        throw new FieldNotMappedException("ldbve421DtEndCalcDb");
    }

    @Override
    public String getLdbve421DtIniCalcDb() {
        throw new FieldNotMappedException("ldbve421DtIniCalcDb");
    }

    @Override
    public void setLdbve421DtIniCalcDb(String ldbve421DtIniCalcDb) {
        throw new FieldNotMappedException("ldbve421DtIniCalcDb");
    }

    @Override
    public String getLdbve421TpCausBollo1() {
        throw new FieldNotMappedException("ldbve421TpCausBollo1");
    }

    @Override
    public void setLdbve421TpCausBollo1(String ldbve421TpCausBollo1) {
        throw new FieldNotMappedException("ldbve421TpCausBollo1");
    }

    @Override
    public String getLdbve421TpCausBollo2() {
        throw new FieldNotMappedException("ldbve421TpCausBollo2");
    }

    @Override
    public void setLdbve421TpCausBollo2(String ldbve421TpCausBollo2) {
        throw new FieldNotMappedException("ldbve421TpCausBollo2");
    }

    @Override
    public String getLdbve421TpCausBollo3() {
        throw new FieldNotMappedException("ldbve421TpCausBollo3");
    }

    @Override
    public void setLdbve421TpCausBollo3(String ldbve421TpCausBollo3) {
        throw new FieldNotMappedException("ldbve421TpCausBollo3");
    }

    @Override
    public String getLdbve421TpCausBollo4() {
        throw new FieldNotMappedException("ldbve421TpCausBollo4");
    }

    @Override
    public void setLdbve421TpCausBollo4(String ldbve421TpCausBollo4) {
        throw new FieldNotMappedException("ldbve421TpCausBollo4");
    }

    @Override
    public String getLdbve421TpCausBollo5() {
        throw new FieldNotMappedException("ldbve421TpCausBollo5");
    }

    @Override
    public void setLdbve421TpCausBollo5(String ldbve421TpCausBollo5) {
        throw new FieldNotMappedException("ldbve421TpCausBollo5");
    }

    @Override
    public String getLdbve421TpCausBollo6() {
        throw new FieldNotMappedException("ldbve421TpCausBollo6");
    }

    @Override
    public void setLdbve421TpCausBollo6(String ldbve421TpCausBollo6) {
        throw new FieldNotMappedException("ldbve421TpCausBollo6");
    }

    @Override
    public String getLdbve421TpCausBollo7() {
        throw new FieldNotMappedException("ldbve421TpCausBollo7");
    }

    @Override
    public void setLdbve421TpCausBollo7(String ldbve421TpCausBollo7) {
        throw new FieldNotMappedException("ldbve421TpCausBollo7");
    }

    @Override
    public long getP58DsRiga() {
        return impstBollo.getP58DsRiga();
    }

    @Override
    public void setP58DsRiga(long p58DsRiga) {
        this.impstBollo.setP58DsRiga(p58DsRiga);
    }

    @Override
    public AfDecimal getTotR() {
        throw new FieldNotMappedException("totR");
    }

    @Override
    public void setTotR(AfDecimal totR) {
        throw new FieldNotMappedException("totR");
    }

    @Override
    public AfDecimal getTotV() {
        throw new FieldNotMappedException("totV");
    }

    @Override
    public void setTotV(AfDecimal totV) {
        throw new FieldNotMappedException("totV");
    }

    @Override
    public AfDecimal getTotVObj() {
        return getTotV();
    }

    @Override
    public void setTotVObj(AfDecimal totVObj) {
        setTotV(new AfDecimal(totVObj, 15, 3));
    }

    @Override
    public String getTpCausBollo() {
        return impstBollo.getP58TpCausBollo();
    }

    @Override
    public void setTpCausBollo(String tpCausBollo) {
        this.impstBollo.setP58TpCausBollo(tpCausBollo);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }
}
