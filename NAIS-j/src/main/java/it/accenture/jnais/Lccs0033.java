package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoLccs0033;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccs0033Data;

/**Original name: LCCS0033<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA .....
 *     TIPOLOGIA...... COMPONENTE COMUNE
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... VERIFICA/ESTRAZIONE POLIZZE PER RECUPERO
 *                     PROVVIGGIONALE
 *     PAGINA WEB.....
 * ----------------------------------------------------------------*</pre>*/
public class Lccs0033 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0033Data ws = new Lccs0033Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: AREA-IO-LCCS0033
    private AreaIoLccs0033 areaIoLccs0033;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0033_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, AreaIoLccs0033 areaIoLccs0033) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.areaIoLccs0033 = areaIoLccs0033;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0033 getInstance() {
        return ((Lccs0033)Programs.getInstance(Lccs0033.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE WS-INDICI
        //                      WS-VARIABILI
        //                      LCCC0033-DATI-OUTPUT.
        initWsIndici();
        initWsVariabili();
        initDatiOutput();
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU EX-S0005.
        s0005CtrlDatiInput();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM S0010-LETTURA-PCO THRU S0010-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S0010-LETTURA-PCO THRU S0010-EX
            s0010LetturaPco();
        }
        // COB_CODE: SET CONTRAENTE       TO TRUE
        ws.getWsTpRappAna().setContraente();
        // COB_CODE: SET WS-CONTRAENTE-NO TO TRUE
        ws.getWsContraente().setNo();
        // COB_CODE: IF LCCC0033-TP-RAPP-ANA = WS-TP-RAPP-ANA
        //              SET WS-CONTRAENTE-SI TO TRUE
        //           END-IF.
        if (Conditions.eq(areaIoLccs0033.getTpRappAna(), ws.getWsTpRappAna().getWsTpRappAna())) {
            // COB_CODE: SET WS-CONTRAENTE-SI TO TRUE
            ws.getWsContraente().setSi();
        }
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONTROLLI DATI INPUT                                          *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005CtrlDatiInput() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S0010-LETTURA-PCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Lettura in ptf della parametro compagnia
	 * ----------------------------------------------------------------*</pre>*/
    private void s0010LetturaPco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE HIGH-VALUE              TO PARAM-COMP.
        ws.getParamComp().initParamCompHighValues();
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                           IDSI0011-DATA-FINE-EFFETTO
        //                                           IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO
        //                                               WK-TABELLA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PARAM-COMP");
        ws.setWkTabella("PARAM-COMP");
        // COB_CODE: MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        // COB_CODE: SET IDSI0011-SELECT              TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-NOT-FOUND
            //                       THRU EX-S0300
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0010-LETTURA-PCO'
                    //                                TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0010-LETTURA-PCO");
                    // COB_CODE: MOVE '005069'        TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
                    ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0010-LETTURA-PCO'
            //                                   TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0010-LETTURA-PCO");
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --  Estrazione dal PTF di tutte le polizze appartenenti allo
	 * --  stesso contraente</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF WS-CONTRAENTE-SI
        //                 THRU S1015-EX
        //           END-IF.
        if (ws.getWsContraente().isSi()) {
            // COB_CODE: PERFORM S1015-ELAB-ALTRE-POL-CONTR
            //              THRU S1015-EX
            s1015ElabAltrePolContr();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S1025-ABILITA-TASTO-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1025-ABILITA-TASTO
            //              THRU S1025-ABILITA-TASTO-EX
            s1025AbilitaTasto();
        }
    }

    /**Original name: S1015-ELAB-ALTRE-POL-CONTR<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1015ElabAltrePolContr() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE 'LDBS5560'              TO WK-TABELLA
        ws.setWkTabella("LDBS5560");
        // COB_CODE: SET IDSI0011-FETCH-FIRST     TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC   TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL  TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET FINE-CICLO-POL-NO        TO TRUE
        ws.getFlCicloPol().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CICLO-POL-SI
        //             END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getFlCicloPol().isSi())) {
            // COB_CODE: MOVE WS-DT-INFINITO-1   TO IDSI0011-DATA-INIZIO-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWsDtInfinito1());
            // COB_CODE: MOVE ZERO               TO IDSI0011-DATA-FINE-EFFETTO
            //                                      IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
            // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
            // COB_CODE: SET IDSI0011-WHERE-CONDITION    TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
            // COB_CODE: INITIALIZE LDBV5561
            initLdbv5561();
            // COB_CODE: MOVE LCCC0033-COD-SOGG      TO LDBV5561-COD-SOGG
            ws.setLdbv5561CodSogg(areaIoLccs0033.getCodSogg());
            // COB_CODE: MOVE LDBV5561               TO IDSI0011-BUFFER-WHERE-COND
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv5561Formatted());
            // COB_CODE: MOVE 'LDBS5560'             TO IDSI0011-CODICE-STR-DATO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS5560");
            // COB_CODE: MOVE SPACES                 TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
                //              END-EVALUATE
                //           ELSE
                //                 THRU EX-S0300
                //           END-IF
                if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: EVALUATE TRUE
                    //             WHEN IDSO0011-SUCCESSFUL-SQL
                    //                  END-IF
                    //             WHEN IDSO0011-NOT-FOUND
                    //                  SET FINE-CICLO-POL-SI     TO TRUE
                    //           END-EVALUATE
                    switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                        case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO POLI
                            ws.getPoli().setPoliFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                            // COB_CODE: PERFORM S1020-CTRL-POL-REC-PROVV
                            //              THRU S1020-EX
                            s1020CtrlPolRecProvv();
                            // COB_CODE:                     IF  IDSV0001-ESITO-OK
                            //                               AND SCARTO-POL-NO
                            //                               AND LCCC0033-VERIFICA-REC-PROVV
                            //                                   SET FINE-CICLO-POL-SI       TO TRUE
                            //           *                        PERFORM S8100-CLOSE-CURSOR
                            //           *                           THRU S8100-EX
                            //                               ELSE
                            //                                  SET IDSI0011-FETCH-NEXT  TO TRUE
                            //                               END-IF
                            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlScartoPol().isNo() && areaIoLccs0033.getModalitaServizio().isVerificaRecProvv()) {
                                // COB_CODE: SET LCCC0033-RECUP-PROVV-SI TO TRUE
                                areaIoLccs0033.getFlagRecupProvv().setSi();
                                // COB_CODE: SET FINE-CICLO-POL-SI       TO TRUE
                                ws.getFlCicloPol().setSi();
                                //                        PERFORM S8100-CLOSE-CURSOR
                                //                           THRU S8100-EX
                            }
                            else {
                                // COB_CODE: SET IDSI0011-FETCH-NEXT  TO TRUE
                                ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                            }
                            break;

                        case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: SET FINE-CICLO-POL-SI     TO TRUE
                            ws.getFlCicloPol().setSi();
                            break;

                        default:break;
                    }
                }
                else {
                    // COB_CODE: MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1015-ELAB-ALTRE-POL-CONTR'
                    //                                     TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1015-ELAB-ALTRE-POL-CONTR");
                    // COB_CODE: MOVE '005016'             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
        }
    }

    /**Original name: S1020-CTRL-POL-REC-PROVV<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1020CtrlPolRecProvv() {
        // COB_CODE: SET SCARTO-POL-NO TO TRUE
        ws.getFlScartoPol().setNo();
        //--  Controllo lo stato e causale della polizza
        // COB_CODE: PERFORM S1025-CTRL-STATO-CAUSALE
        //              THRU S1025-EX
        s1025CtrlStatoCausale();
        //--  Controllo che la data effetto dello stato e causale
        //--  deve essere compresa entro i due anni dalla data decorrenza
        //--  del contratto in emissione
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND SCARTO-POL-NO
        //              END-IF
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlScartoPol().isNo()) {
            // COB_CODE: IF LCCC0033-VENDITA
            //                 THRU S1030-EX
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIoLccs0033.getMacroFunzione().isVendita()) {
                // COB_CODE: PERFORM S1030-CTRL-EFFETTO-STORNO
                //              THRU S1030-EX
                s1030CtrlEffettoStorno();
            }
            else if (areaIoLccs0033.getMacroFunzione().isStorni()) {
                // COB_CODE: IF LCCC0033-STORNI
                //                 THRU S1032-EX
                //           END-IF
                // COB_CODE: PERFORM S1032-CTRL-EFFETTO-EMIS
                //              THRU S1032-EX
                s1032CtrlEffettoEmis();
            }
        }
        //--  Controllo che la polizza sia a premio Annuo e non una TCM
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND SCARTO-POL-NO
        //                  THRU S1040-EX
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlScartoPol().isNo()) {
            // COB_CODE: PERFORM S1040-CTRL-PROD-PREMIO-ANNUO
            //              THRU S1040-EX
            s1040CtrlProdPremioAnnuo();
        }
        //--  Calcolo del premio annuo di polizza
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND SCARTO-POL-NO
        //                  THRU S1045-EX
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlScartoPol().isNo()) {
            // COB_CODE: PERFORM S1045-CALC-PREMIO-ANNUO
            //              THRU S1045-EX
            s1045CalcPremioAnnuo();
        }
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND SCARTO-POL-NO AND LCCC0033-ESTR-POL-REC-PROVV
        //                  THRU S1055-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getFlScartoPol().isNo() && areaIoLccs0033.getModalitaServizio().isEstrPolRecProvv()) {
            // COB_CODE: PERFORM S1055-VAL-LISTA-ALTRE-POL
            //              THRU S1055-EX
            s1055ValListaAltrePol();
        }
    }

    /**Original name: S1025-CTRL-STATO-CAUSALE<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1025CtrlStatoCausale() {
        // COB_CODE: MOVE POL-ID-POLI TO WK-ID-OGG
        ws.getWsVariabili().setWkIdOgg(ws.getPoli().getPolIdPoli());
        // COB_CODE: SET POLIZZA TO TRUE
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: PERFORM S1022-LETTURA-STB
        //              THRU S1022-EX
        s1022LetturaStb();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE STB-TP-STAT-BUS TO WS-TP-STAT-BUS
            ws.getWsTpStatBus().setWsTpStatBus(ws.getStatOggBus().getStbTpStatBus());
            // COB_CODE: MOVE STB-TP-CAUS     TO WS-TP-CAUS
            ws.getWsTpCaus().setWsTpCaus(ws.getStatOggBus().getStbTpCaus());
            // COB_CODE: IF LCCC0033-VENDITA
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIoLccs0033.getMacroFunzione().isVendita()) {
                // COB_CODE: EVALUATE TRUE ALSO TRUE
                //               WHEN STORNATO   ALSO RISCATTO-TOTALE
                //               WHEN COMPLETATA ALSO PAGAMENT-RISCATTO-TOTALE
                //               WHEN IN-VIGORE  ALSO RIDOTTA
                //               WHEN STORNATO   ALSO INSOLVENZA
                //                  SET SCARTO-POL-NO TO TRUE
                //               WHEN OTHER
                //                  SET SCARTO-POL-SI TO TRUE
                //           END-EVALUATE
                if (ws.getWsTpStatBus().isStornato() && ws.getWsTpCaus().isRiscattoTotale() || (ws.getWsTpStatBus().isCompletata() && ws.getWsTpCaus().isPagamentRiscattoTotale()) || (ws.getWsTpStatBus().isInVigore() && ws.getWsTpCaus().isRidotta()) || (ws.getWsTpStatBus().isStornato() && ws.getWsTpCaus().isInsolvenza())) {
                    // COB_CODE: SET SCARTO-POL-NO TO TRUE
                    ws.getFlScartoPol().setNo();
                }
                else {
                    // COB_CODE: SET SCARTO-POL-SI TO TRUE
                    ws.getFlScartoPol().setSi();
                }
            }
            else if (areaIoLccs0033.getMacroFunzione().isStorni()) {
                // COB_CODE: IF LCCC0033-STORNI
                //              END-EVALUATE
                //           END-IF
                // COB_CODE: EVALUATE TRUE ALSO TRUE
                //               WHEN IN-VIGORE  ALSO ATTESA-PERFEZIONAMENTO
                //               WHEN IN-VIGORE  ALSO PERFEZIONATA
                //                  SET SCARTO-POL-NO TO TRUE
                //               WHEN OTHER
                //                  SET SCARTO-POL-SI TO TRUE
                //           END-EVALUATE
                if (ws.getWsTpStatBus().isInVigore() && ws.getWsTpCaus().isAttesaPerfezionamento() || (ws.getWsTpStatBus().isInVigore() && ws.getWsTpCaus().isPerfezionata())) {
                    // COB_CODE: SET SCARTO-POL-NO TO TRUE
                    ws.getFlScartoPol().setNo();
                }
                else {
                    // COB_CODE: SET SCARTO-POL-SI TO TRUE
                    ws.getFlScartoPol().setSi();
                }
            }
        }
    }

    /**Original name: S1022-LETTURA-STB<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1022LetturaStb() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'STAT-OGG-BUS'              TO WK-TABELLA
        ws.setWkTabella("STAT-OGG-BUS");
        // COB_CODE: INITIALIZE STAT-OGG-BUS.
        initStatOggBus();
        // COB_CODE: MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWsDtInfinito1());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-ID-OGGETTO       TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE WK-ID-OGG                TO STB-ID-OGG
        ws.getStatOggBus().setStbIdOgg(ws.getWsVariabili().getWkIdOgg());
        // COB_CODE: MOVE WS-TP-OGG                TO STB-TP-OGG
        ws.getStatOggBus().setStbTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: MOVE 'STAT-OGG-BUS'           TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("STAT-OGG-BUS");
        // COB_CODE: MOVE STAT-OGG-BUS             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatOggBus().getStatOggBusFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  MOVE STB-TP-STAT-BUS  TO WS-TP-STAT-BUS
            //               WHEN IDSO0011-NOT-FOUND
            //                     THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI  TO STAT-OGG-BUS
                    ws.getStatOggBus().setStatOggBusFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE STB-TP-STAT-BUS  TO WS-TP-STAT-BUS
                    ws.getWsTpStatBus().setWsTpStatBus(ws.getStatOggBus().getStbTpStatBus());
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1022-LETTURA-STB' TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1022-LETTURA-STB");
                    // COB_CODE: MOVE '005069'           TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: MOVE WK-TABELLA         TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getWkTabella());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0001-LETTURA-STB'      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0001-LETTURA-STB");
            // COB_CODE: MOVE '005016'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1030-CTRL-EFFETTO-STORNO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1030CtrlEffettoStorno() {
        // COB_CODE: MOVE '03'                        TO A2K-FUNZ
        ws.getIoA2kLccc0003().getInput().setA2kFunz("03");
        // COB_CODE: MOVE '03'                        TO A2K-INFO
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE LCCC0033-DT-DECOR-POL       TO A2K-INDATA
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(areaIoLccs0033.getDtDecorPolFormatted());
        // COB_CODE: MOVE 2                           TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)2));
        // COB_CODE: MOVE 'A'                         TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        // COB_CODE: MOVE '0'                         TO A2K-FISLAV
        //                                               A2K-INICON
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX
        callLccs0003();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG              TO DATA-INF-AMG
            ws.setDataInfAmgFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
            // COB_CODE: MOVE LCCC0033-DT-DECOR-POL  TO DATA-SUP-AMG
            ws.setDataSupAmg(TruncAbs.toInt(areaIoLccs0033.getDtDecorPol(), 8));
            // COB_CODE: IF  STB-DT-INI-EFF <= DATA-SUP-AMG
            //           AND STB-DT-INI-EFF >= DATA-INF-AMG
            //              CONTINUE
            //           ELSE
            //              SET SCARTO-POL-SI TO TRUE
            //           END-IF
            if (ws.getStatOggBus().getStbDtIniEff() <= ws.getDataSupAmg() && ws.getStatOggBus().getStbDtIniEff() >= ws.getDataInfAmg()) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: SET SCARTO-POL-SI TO TRUE
                ws.getFlScartoPol().setSi();
            }
        }
    }

    /**Original name: S1032-CTRL-EFFETTO-EMIS<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1032CtrlEffettoEmis() {
        // COB_CODE: MOVE '03'                        TO A2K-FUNZ
        ws.getIoA2kLccc0003().getInput().setA2kFunz("03");
        // COB_CODE: MOVE '03'                        TO A2K-INFO
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE LCCC0033-DT-STORNO-POL      TO A2K-INDATA
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(areaIoLccs0033.getDtStornoPolFormatted());
        // COB_CODE: MOVE 2                           TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)2));
        // COB_CODE: MOVE 'A'                         TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        // COB_CODE: MOVE '0'                         TO A2K-FISLAV
        //                                               A2K-INICON
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX
        callLccs0003();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG               TO DATA-INF-AMG
            ws.setDataInfAmgFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
            // COB_CODE: MOVE LCCC0033-DT-STORNO-POL  TO DATA-SUP-AMG
            ws.setDataSupAmg(TruncAbs.toInt(areaIoLccs0033.getDtStornoPol(), 8));
            // COB_CODE: IF  POL-DT-DECOR <= DATA-SUP-AMG
            //           AND POL-DT-DECOR >= DATA-INF-AMG
            //              CONTINUE
            //           ELSE
            //              SET SCARTO-POL-SI TO TRUE
            //           END-IF
            if (ws.getPoli().getPolDtDecor() <= ws.getDataSupAmg() && ws.getPoli().getPolDtDecor() >= ws.getDataInfAmg()) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: SET SCARTO-POL-SI TO TRUE
                ws.getFlScartoPol().setSi();
            }
        }
    }

    /**Original name: S1040-CTRL-PROD-PREMIO-ANNUO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1040CtrlProdPremioAnnuo() {
        // COB_CODE: PERFORM S1041-LETTURA-ADE
        //              THRU S1041-EX
        s1041LetturaAde();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S1042-EX
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1042-LETTURA-GAR
            //              THRU S1042-EX
            s1042LetturaGar();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF WS-GARANZIA-BASE-SI
            //           *          IF  GRZ-TP-RSH NOT = 'TC'
            //                      END-IF
            //                   ELSE
            //                      SET SCARTO-POL-SI TO TRUE
            //                   END-IF
            if (ws.getWsGaranziaBase().isSi()) {
                //          IF  GRZ-TP-RSH NOT = 'TC'
                // COB_CODE: IF  GRZ-TP-RSH NOT = 'MO'
                //           AND GRZ-TP-PER-PRE = 'A'
                //             CONTINUE
                //           ELSE
                //              SET SCARTO-POL-SI TO TRUE
                //           END-IF
                if (!Conditions.eq(ws.getGar().getGrzTpRsh(), "MO") && Conditions.eq(ws.getGar().getGrzTpPerPre(), "A")) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET SCARTO-POL-SI TO TRUE
                    ws.getFlScartoPol().setSi();
                }
            }
            else {
                // COB_CODE: SET SCARTO-POL-SI TO TRUE
                ws.getFlScartoPol().setSi();
            }
        }
    }

    /**Original name: S1041-LETTURA-ADE<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1041LetturaAde() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'ADES'                   TO WK-TABELLA
        ws.setWkTabella("ADES");
        // COB_CODE: INITIALIZE ADES
        initAdes();
        // COB_CODE: MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWsDtInfinito1());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSI0011-ID-PADRE         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdPadre();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE POL-ID-POLI              TO ADE-ID-POLI
        ws.getAdes().setAdeIdPoli(ws.getPoli().getPolIdPoli());
        // COB_CODE: MOVE 'ADES'           TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("ADES");
        // COB_CODE: MOVE ADES             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAdes().getAdesFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                       THRU S8100-EX
            //               WHEN IDSO0011-NOT-FOUND
            //                       THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI   TO ADES
                    ws.getAdes().setAdesFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: PERFORM S8100-CLOSE-CURSOR
                    //              THRU S8100-EX
                    s8100CloseCursor();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1041-LETTURA-ADE' TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1041-LETTURA-ADE");
                    // COB_CODE: MOVE '005069'        TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005069");
                    // COB_CODE: MOVE WK-TABELLA      TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr(ws.getWkTabella());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1041-LETTURA-ADE'  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1041-LETTURA-ADE");
            // COB_CODE: MOVE '005016'             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1042-LETTURA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1042LetturaGar() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'GAR'                   TO WK-TABELLA
        ws.setWkTabella("GAR");
        // COB_CODE: INITIALIZE GAR
        initGar();
        // COB_CODE: MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWsDtInfinito1());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSI0011-ID-PADRE         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdPadre();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE POL-ID-POLI              TO GRZ-ID-POLI
        ws.getGar().setGrzIdPoli(ws.getPoli().getPolIdPoli());
        // COB_CODE: MOVE ADE-ID-ADES              TO GRZ-ID-ADES
        ws.getGar().getGrzIdAdes().setGrzIdAdes(ws.getAdes().getAdeIdAdes());
        // COB_CODE: MOVE 'GAR'                    TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("GAR");
        // COB_CODE: MOVE GAR                      TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getGar().getGarFormatted());
        // COB_CODE: SET FINE-CICLO-GAR-NO TO TRUE
        ws.getFlCicloGar().setNo();
        // COB_CODE: SET WS-GARANZIA-BASE-NO  TO TRUE
        ws.getWsGaranziaBase().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CICLO-GAR-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getFlCicloGar().isSi())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
            //              END-EVALUATE
            //           ELSE
            //                 THRU EX-S0300
            //           END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSO0011-SUCCESSFUL-SQL
                //                  END-IF
                //               WHEN IDSO0011-NOT-FOUND
                //                  SET FINE-CICLO-GAR-SI TO TRUE
                //           END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI  TO GAR
                        ws.getGar().setGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: MOVE GRZ-TP-GAR TO ACT-TP-GARANZIA
                        ws.getActTpGaranzia().setActTpGaranzia(TruncAbs.toShort(ws.getGar().getGrzTpGar().getGrzTpGar(), 1));
                        // COB_CODE: IF TP-GAR-BASE
                        //                 THRU S8100-EX
                        //           ELSE
                        //              SET IDSI0011-FETCH-NEXT TO TRUE
                        //           END-IF
                        if (ws.getActTpGaranzia().isBaseFld()) {
                            // COB_CODE: SET FINE-CICLO-GAR-SI   TO TRUE
                            ws.getFlCicloGar().setSi();
                            // COB_CODE: SET WS-GARANZIA-BASE-SI TO TRUE
                            ws.getWsGaranziaBase().setSi();
                            // COB_CODE: PERFORM S8100-CLOSE-CURSOR
                            //              THRU S8100-EX
                            s8100CloseCursor();
                        }
                        else {
                            // COB_CODE: SET IDSI0011-FETCH-NEXT TO TRUE
                            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        }
                        break;

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: SET FINE-CICLO-GAR-SI TO TRUE
                        ws.getFlCicloGar().setSi();
                        break;

                    default:break;
                }
            }
            else {
                // COB_CODE: MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1042-LETTURA-GAR'  TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1042-LETTURA-GAR");
                // COB_CODE: MOVE '005016'             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING WK-TABELLA           ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1045-CALC-PREMIO-ANNUO<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1045CalcPremioAnnuo() {
        // COB_CODE: MOVE ZERO TO WS-PREMIO-ANNUO-CALC
        ws.getWsVariabili().setWsPremioAnnuoCalc(new AfDecimal(0, 15, 3));
        // COB_CODE: PERFORM S1048-LETTURA-TGA
        //              THRU S1048-EX
        s1048LetturaTga();
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //           AND WS-PREMIO-ANNUO-TGA > ZERO
        //               END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWsVariabili().getWsPremioAnnuoTga().compareTo(0) > 0) {
            // COB_CODE: PERFORM S1050-SUM-IMP-COLLG
            //              THRU S1050-EX
            s1050SumImpCollg();
            // COB_CODE: IF  IDSV0001-ESITO-OK
            //           AND LDBV5571-TOT-IMP-COLL >= ZERO
            //                                             LDBV5571-TOT-IMP-COLL
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getLdbv5571().getLdbv5571TotImpColl().compareTo(0) >= 0) {
                // COB_CODE: COMPUTE WS-PREMIO-ANNUO-CALC = WS-PREMIO-ANNUO-TGA -
                //                                          LDBV5571-TOT-IMP-COLL
                ws.getWsVariabili().setWsPremioAnnuoCalc(Trunc.toDecimal(ws.getWsVariabili().getWsPremioAnnuoTga().subtract(ws.getLdbv5571().getLdbv5571TotImpColl()), 15, 3));
            }
            // COB_CODE: IF  WS-PREMIO-ANNUO-CALC <= ZERO
            //               SET SCARTO-POL-SI TO TRUE
            //           END-IF
            if (ws.getWsVariabili().getWsPremioAnnuoCalc().compareTo(0) <= 0) {
                // COB_CODE: SET SCARTO-POL-SI TO TRUE
                ws.getFlScartoPol().setSi();
            }
        }
    }

    /**Original name: S1048-LETTURA-TGA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1048LetturaTga() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LDBSD510'               TO WK-TABELLA
        ws.setWkTabella("LDBSD510");
        // COB_CODE: INITIALIZE LDBVD511.
        initLdbvd511();
        // COB_CODE: MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWsDtInfinito1());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--  Stato Business di partenza
        // COB_CODE: MOVE WS-TP-STAT-BUS           TO LDBVD511-TP-STAT-BUS
        ws.getLdbvd511().setTpStatBus(ws.getWsTpStatBus().getWsTpStatBus());
        //--  Causale Business di partenza
        // COB_CODE: MOVE WS-TP-CAUS               TO LDBVD511-TP-CAUS
        ws.getLdbvd511().setTpCaus(ws.getWsTpCaus().getWsTpCaus());
        // COB_CODE: SET  TRANCHE                  TO TRUE
        ws.getWsTpOgg().setTranche();
        // COB_CODE: MOVE WS-TP-OGG                TO LDBVD511-TP-OGG.
        ws.getLdbvd511().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: MOVE POL-ID-POLI              TO LDBVD511-ID-POLI
        ws.getLdbvd511().setIdPoli(ws.getPoli().getPolIdPoli());
        // COB_CODE: MOVE ADE-ID-ADES              TO LDBVD511-ID-ADES
        ws.getLdbvd511().setIdAdes(ws.getAdes().getAdeIdAdes());
        // COB_CODE: MOVE 'LDBSD510'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSD510");
        // COB_CODE: MOVE LDBVD511                 TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvd511().getLdbvd511Formatted());
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET FINE-CICLO-TGA-NO         TO TRUE
        ws.getFlCicloTga().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CICLO-TGA-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.getFlCicloTga().isSi())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
            //              END-EVALUATE
            //           ELSE
            //                 THRU EX-S0300
            //           END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSO0011-SUCCESSFUL-SQL
                //                    SET IDSI0011-FETCH-NEXT TO TRUE
                //               WHEN IDSO0011-NOT-FOUND
                //                    SET FINE-CICLO-TGA-SI     TO TRUE
                //           END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO TRCH-DI-GAR
                        ws.getTrchDiGar().setTrchDiGarFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        // COB_CODE: IF TGA-PRE-TARI-INI-NULL = HIGH-VALUE
                        //              CONTINUE
                        //           ELSE
                        //              ADD TGA-PRE-TARI-INI TO WS-PREMIO-ANNUO-TGA
                        //           END-IF
                        if (Characters.EQ_HIGH.test(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIniNullFormatted())) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: ADD TGA-PRE-TARI-INI TO WS-PREMIO-ANNUO-TGA
                            ws.getWsVariabili().setWsPremioAnnuoTga(Trunc.toDecimal(ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni().add(ws.getWsVariabili().getWsPremioAnnuoTga()), 15, 3));
                        }
                        // COB_CODE: SET IDSI0011-FETCH-NEXT TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        break;

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: SET FINE-CICLO-TGA-SI     TO TRUE
                        ws.getFlCicloTga().setSi();
                        break;

                    default:break;
                }
            }
            else {
                // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1048-LETTURA-TGA'    TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1048-LETTURA-TGA");
                // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING WK-TABELLA           ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1050-SUM-IMP-COLLG<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1050SumImpCollg() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LDBS5570'               TO WK-TABELLA
        ws.setWkTabella("LDBS5570");
        // COB_CODE: INITIALIZE LDBV5571.
        initLdbv5571();
        // COB_CODE: MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWsDtInfinito1());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE 'LDBS5570'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS5570");
        // COB_CODE: MOVE  POL-ID-POLI             TO LDBV5571-ID-OGG-DER
        ws.getLdbv5571().setLdbv5571IdOggDer(ws.getPoli().getPolIdPoli());
        // COB_CODE: SET POLIZZA TO TRUE
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE  WS-TP-OGG               TO LDBV5571-TP-OGG-DER
        ws.getLdbv5571().setLdbv5571TpOggDer(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: IF LCCC0033-VENDITA
        //              SET RECUP-PROVV-STORNI-ALRE-POL   TO TRUE
        //           ELSE
        //              END-IF
        //           END-IF
        if (areaIoLccs0033.getMacroFunzione().isVendita()) {
            // COB_CODE: SET RECUP-PROVV-STORNI-ALRE-POL   TO TRUE
            ws.getWsTpCollgm().setRecupProvvStorniAlrePol();
        }
        else if (areaIoLccs0033.getMacroFunzione().isStorni()) {
            // COB_CODE: IF LCCC0033-STORNI
            //              SET RECUP-PROVV-EMISS-ALRE-POL TO TRUE
            //           END-IF
            // COB_CODE: SET RECUP-PROVV-EMISS-ALRE-POL TO TRUE
            ws.getWsTpCollgm().setRecupProvvEmissAlrePol();
        }
        // COB_CODE: MOVE WS-TP-COLLGM             TO LDBV5571-TP-COLL-1
        ws.getLdbv5571().setLdbv5571TpColl1(ws.getWsTpCollgm().getWsTpCollgm());
        // COB_CODE: MOVE LDBV5571                 TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getLdbv5571().getLdbv5571Formatted());
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            MOVE IDSO0011-BUFFER-DATI TO LDBV5571
            //           *                 PERFORM S8100-CLOSE-CURSOR
            //           *                    THRU S8100-EX
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO LDBV5571
                    ws.getLdbv5571().setLdbv5571Formatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //                 PERFORM S8100-CLOSE-CURSOR
                    //                    THRU S8100-EX
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1050-SUM-IMP-COLLG'  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1050-SUM-IMP-COLLG");
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1055-VAL-LISTA-ALTRE-POL<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1055ValListaAltrePol() {
        // COB_CODE: IF LCCC0033-ELE-MAX-RECUP-PROVV < WK-MAX-POL-RECUP-PROVV
        //                TO LCCC0033-IMP-PREMIO-ANNUO(IX-TAB-PAG)
        //           END-IF.
        if (areaIoLccs0033.getEleMaxRecupProvv() < ws.getWkMaxPolRecupProvv()) {
            // COB_CODE: ADD 1 TO IX-TAB-PAG
            ws.getWsIndici().setTabPag(Trunc.toShort(1 + ws.getWsIndici().getTabPag(), 4));
            // COB_CODE: ADD 1 TO LCCC0033-ELE-MAX-RECUP-PROVV
            areaIoLccs0033.setEleMaxRecupProvv(Trunc.toShort(1 + areaIoLccs0033.getEleMaxRecupProvv(), 4));
            // COB_CODE: MOVE POL-ID-POLI    TO LCCC0033-ID-POLI(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setIdPoli(ws.getPoli().getPolIdPoli());
            // COB_CODE: MOVE POL-IB-OGG     TO LCCC0033-IB-OGG(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setIbOgg(ws.getPoli().getPolIbOgg());
            // COB_CODE: MOVE STB-TP-STAT-BUS
            //                               TO LCCC0033-STATO(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setStato(ws.getStatOggBus().getStbTpStatBus());
            // COB_CODE: MOVE STB-TP-CAUS    TO LCCC0033-CAUSALE(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setCausale(ws.getStatOggBus().getStbTpCaus());
            // COB_CODE: MOVE POL-DT-DECOR   TO LCCC0033-DT-DECORRENZA(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setDtDecorrenza(ws.getPoli().getPolDtDecor());
            // COB_CODE: MOVE STB-DT-INI-EFF TO LCCC0033-DT-STORNO(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setDtStorno(ws.getStatOggBus().getStbDtIniEff());
            // COB_CODE: MOVE WS-PREMIO-ANNUO-CALC
            //             TO LCCC0033-IMP-PREMIO-ANNUO(IX-TAB-PAG)
            areaIoLccs0033.getRecuperoProvv(ws.getWsIndici().getTabPag()).setImpPremioAnnuo(Trunc.toDecimal(ws.getWsVariabili().getWsPremioAnnuoCalc(), 15, 3));
        }
    }

    /**Original name: S8100-CLOSE-CURSOR<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s8100CloseCursor() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSI0011-CLOSE-CURSOR TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsv0003CloseCursor();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              CONTINUE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S8110-CLOSE-CURSOR'
            //                                        TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S8110-CLOSE-CURSOR");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: CALL-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES PER IL CALCOLO SULLE DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
        //           ON EXCEPTION
        //                  THRU EX-S0290
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lccs0033Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
            //                                          TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO OPERAZIONI SULLE DATE");
            // COB_CODE: MOVE 'CALL-LCCS0003'          TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-LCCS0003");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE:      IF IN-RCODE  = '00'
        //                   CONTINUE
        //           *       MOVE A2K-OUAMG              TO WS-DATA
        //                ELSE
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
        // COB_CODE: CONTINUE
        //continue
        //       MOVE A2K-OUAMG              TO WS-DATA
        }
        else {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL-LCCS0003'        TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-LCCS0003");
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1025-ABILITA-TASTO<br>
	 * <pre>----------------------------------------------------------------*
	 *           ABILITA TASTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1025AbilitaTasto() {
        // COB_CODE: IF LCCC0033-ESTR-POL-REC-PROVV
        //              END-IF
        //           END-IF.
        if (areaIoLccs0033.getModalitaServizio().isEstrPolRecProvv()) {
            // COB_CODE: IF LCCC0033-ELE-MAX-RECUP-PROVV > ZERO
            //              SET LCCC0033-RECUP-PROVV-SI TO TRUE
            //           ELSE
            //              SET LCCC0033-RECUP-PROVV-NO TO TRUE
            //           END-IF
            if (areaIoLccs0033.getEleMaxRecupProvv() > 0) {
                // COB_CODE: SET LCCC0033-RECUP-PROVV-SI TO TRUE
                areaIoLccs0033.getFlagRecupProvv().setSi();
            }
            else {
                // COB_CODE: SET LCCC0033-RECUP-PROVV-NO TO TRUE
                areaIoLccs0033.getFlagRecupProvv().setNo();
            }
        }
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ROUTINES GESTIONE ERRORI                                       *
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initWsIndici() {
        ws.getWsIndici().setTabPvt(((short)0));
        ws.getWsIndici().setTabPag(((short)0));
        ws.getWsIndici().setGarTari(((short)0));
        ws.getWsIndici().setRicRan(((short)0));
    }

    public void initWsVariabili() {
        ws.getWsVariabili().setWkIdOgg(0);
        ws.getWsVariabili().setWsPremioAnnuoTga(new AfDecimal(0, 15, 3));
        ws.getWsVariabili().setWsPremioAnnuoCalc(new AfDecimal(0, 15, 3));
    }

    public void initDatiOutput() {
        areaIoLccs0033.getFlagRecupProvv().setFlagRecupProvv("");
        areaIoLccs0033.setEleMaxRecupProvv(((short)0));
        for (int idx0 = 1; idx0 <= AreaIoLccs0033.RECUPERO_PROVV_MAXOCCURS; idx0++) {
            areaIoLccs0033.getRecuperoProvv(idx0).setIdPoli(0);
            areaIoLccs0033.getRecuperoProvv(idx0).setIbOgg("");
            areaIoLccs0033.getRecuperoProvv(idx0).setStato("");
            areaIoLccs0033.getRecuperoProvv(idx0).setCausale("");
            areaIoLccs0033.getRecuperoProvv(idx0).setDtDecorrenza(0);
            areaIoLccs0033.getRecuperoProvv(idx0).setDtStorno(0);
            areaIoLccs0033.getRecuperoProvv(idx0).setImpPremioAnnuo(new AfDecimal(0, 15, 3));
        }
    }

    public void initLdbv5561() {
        ws.setLdbv5561CodSogg("");
    }

    public void initStatOggBus() {
        ws.getStatOggBus().setStbIdStatOggBus(0);
        ws.getStatOggBus().setStbIdOgg(0);
        ws.getStatOggBus().setStbTpOgg("");
        ws.getStatOggBus().setStbIdMoviCrz(0);
        ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(0);
        ws.getStatOggBus().setStbDtIniEff(0);
        ws.getStatOggBus().setStbDtEndEff(0);
        ws.getStatOggBus().setStbCodCompAnia(0);
        ws.getStatOggBus().setStbTpStatBus("");
        ws.getStatOggBus().setStbTpCaus("");
        ws.getStatOggBus().setStbDsRiga(0);
        ws.getStatOggBus().setStbDsOperSql(Types.SPACE_CHAR);
        ws.getStatOggBus().setStbDsVer(0);
        ws.getStatOggBus().setStbDsTsIniCptz(0);
        ws.getStatOggBus().setStbDsTsEndCptz(0);
        ws.getStatOggBus().setStbDsUtente("");
        ws.getStatOggBus().setStbDsStatoElab(Types.SPACE_CHAR);
    }

    public void initAdes() {
        ws.getAdes().setAdeIdAdes(0);
        ws.getAdes().setAdeIdPoli(0);
        ws.getAdes().setAdeIdMoviCrz(0);
        ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(0);
        ws.getAdes().setAdeDtIniEff(0);
        ws.getAdes().setAdeDtEndEff(0);
        ws.getAdes().setAdeIbPrev("");
        ws.getAdes().setAdeIbOgg("");
        ws.getAdes().setAdeCodCompAnia(0);
        ws.getAdes().getAdeDtDecor().setAdeDtDecor(0);
        ws.getAdes().getAdeDtScad().setAdeDtScad(0);
        ws.getAdes().getAdeEtaAScad().setAdeEtaAScad(0);
        ws.getAdes().getAdeDurAa().setAdeDurAa(0);
        ws.getAdes().getAdeDurMm().setAdeDurMm(0);
        ws.getAdes().getAdeDurGg().setAdeDurGg(0);
        ws.getAdes().setAdeTpRgmFisc("");
        ws.getAdes().setAdeTpRiat("");
        ws.getAdes().setAdeTpModPagTit("");
        ws.getAdes().setAdeTpIas("");
        ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIas(0);
        ws.getAdes().getAdePreNetInd().setAdePreNetInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePreLrdInd().setAdePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlCoincAssto(Types.SPACE_CHAR);
        ws.getAdes().setAdeIbDflt("");
        ws.getAdes().setAdeModCalc("");
        ws.getAdes().setAdeTpFntCnbtva("");
        ws.getAdes().getAdeImpAz().setAdeImpAz(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpAder().setAdeImpAder(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpTfr().setAdeImpTfr(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpVolo().setAdeImpVolo(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePcAz().setAdePcAz(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcAder().setAdePcAder(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcTfr().setAdePcTfr(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcVolo().setAdePcVolo(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(0);
        ws.getAdes().setAdeFlAttiv(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getAdes().setAdeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(0);
        ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(0);
        ws.getAdes().setAdeDsRiga(0);
        ws.getAdes().setAdeDsOperSql(Types.SPACE_CHAR);
        ws.getAdes().setAdeDsVer(0);
        ws.getAdes().setAdeDsTsIniCptz(0);
        ws.getAdes().setAdeDsTsEndCptz(0);
        ws.getAdes().setAdeDsUtente("");
        ws.getAdes().setAdeDsStatoElab(Types.SPACE_CHAR);
        ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(0);
        ws.getAdes().setAdeIdenIscFnd("");
        ws.getAdes().getAdeNumRatPian().setAdeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getAdes().getAdeDtPresc().setAdeDtPresc(0);
        ws.getAdes().setAdeConcsPrest(Types.SPACE_CHAR);
    }

    public void initGar() {
        ws.getGar().setGrzIdGar(0);
        ws.getGar().getGrzIdAdes().setGrzIdAdes(0);
        ws.getGar().setGrzIdPoli(0);
        ws.getGar().setGrzIdMoviCrz(0);
        ws.getGar().getGrzIdMoviChiu().setGrzIdMoviChiu(0);
        ws.getGar().setGrzDtIniEff(0);
        ws.getGar().setGrzDtEndEff(0);
        ws.getGar().setGrzCodCompAnia(0);
        ws.getGar().setGrzIbOgg("");
        ws.getGar().getGrzDtDecor().setGrzDtDecor(0);
        ws.getGar().getGrzDtScad().setGrzDtScad(0);
        ws.getGar().setGrzCodSez("");
        ws.getGar().setGrzCodTari("");
        ws.getGar().setGrzRamoBila("");
        ws.getGar().getGrzDtIniValTar().setGrzDtIniValTar(0);
        ws.getGar().getGrzId1oAssto().setGrzId1oAssto(0);
        ws.getGar().getGrzId2oAssto().setGrzId2oAssto(0);
        ws.getGar().getGrzId3oAssto().setGrzId3oAssto(0);
        ws.getGar().getGrzTpGar().setGrzTpGar(((short)0));
        ws.getGar().setGrzTpRsh("");
        ws.getGar().getGrzTpInvst().setGrzTpInvst(((short)0));
        ws.getGar().setGrzModPagGarcol("");
        ws.getGar().setGrzTpPerPre("");
        ws.getGar().getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(((short)0));
        ws.getGar().getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(((short)0));
        ws.getGar().getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(((short)0));
        ws.getGar().getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(((short)0));
        ws.getGar().getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(((short)0));
        ws.getGar().getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(((short)0));
        ws.getGar().setGrzTpEmisPur(Types.SPACE_CHAR);
        ws.getGar().getGrzEtaAScad().setGrzEtaAScad(0);
        ws.getGar().setGrzTpCalcPrePrstz("");
        ws.getGar().setGrzTpPre(Types.SPACE_CHAR);
        ws.getGar().setGrzTpDur("");
        ws.getGar().getGrzDurAa().setGrzDurAa(0);
        ws.getGar().getGrzDurMm().setGrzDurMm(0);
        ws.getGar().getGrzDurGg().setGrzDurGg(0);
        ws.getGar().getGrzNumAaPagPre().setGrzNumAaPagPre(0);
        ws.getGar().getGrzAaPagPreUni().setGrzAaPagPreUni(0);
        ws.getGar().getGrzMmPagPreUni().setGrzMmPagPreUni(0);
        ws.getGar().getGrzFrazIniErogRen().setGrzFrazIniErogRen(0);
        ws.getGar().getGrzMm1oRat().setGrzMm1oRat(((short)0));
        ws.getGar().getGrzPc1oRat().setGrzPc1oRat(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPrstzAssta("");
        ws.getGar().getGrzDtEndCarz().setGrzDtEndCarz(0);
        ws.getGar().getGrzPcRipPre().setGrzPcRipPre(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzCodFnd("");
        ws.getGar().setGrzAaRenCer("");
        ws.getGar().getGrzPcRevrsb().setGrzPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPcRip("");
        ws.getGar().getGrzPcOpz().setGrzPcOpz(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpIas("");
        ws.getGar().setGrzTpStab("");
        ws.getGar().setGrzTpAdegPre(Types.SPACE_CHAR);
        ws.getGar().getGrzDtVarzTpIas().setGrzDtVarzTpIas(0);
        ws.getGar().getGrzFrazDecrCpt().setGrzFrazDecrCpt(0);
        ws.getGar().setGrzCodTratRiass("");
        ws.getGar().setGrzTpDtEmisRiass("");
        ws.getGar().setGrzTpCessRiass("");
        ws.getGar().setGrzDsRiga(0);
        ws.getGar().setGrzDsOperSql(Types.SPACE_CHAR);
        ws.getGar().setGrzDsVer(0);
        ws.getGar().setGrzDsTsIniCptz(0);
        ws.getGar().setGrzDsTsEndCptz(0);
        ws.getGar().setGrzDsUtente("");
        ws.getGar().setGrzDsStatoElab(Types.SPACE_CHAR);
        ws.getGar().getGrzAaStab().setGrzAaStab(0);
        ws.getGar().getGrzTsStabLimitata().setGrzTsStabLimitata(new AfDecimal(0, 14, 9));
        ws.getGar().getGrzDtPresc().setGrzDtPresc(0);
        ws.getGar().setGrzRshInvst(Types.SPACE_CHAR);
        ws.getGar().setGrzTpRamoBila("");
    }

    public void initLdbvd511() {
        ws.getLdbvd511().setIdPoli(0);
        ws.getLdbvd511().setIdAdes(0);
        ws.getLdbvd511().setTpStatBus("");
        ws.getLdbvd511().setTpCaus("");
        ws.getLdbvd511().setTpOgg("");
    }

    public void initLdbv5571() {
        ws.getLdbv5571().setLdbv5571IdOggDer(0);
        ws.getLdbv5571().setLdbv5571TpOggDer("");
        ws.getLdbv5571().setLdbv5571TpColl1("");
        ws.getLdbv5571().setLdbv5571TpColl2("");
        ws.getLdbv5571().setLdbv5571TpColl3("");
        ws.getLdbv5571().setLdbv5571TotImpColl(new AfDecimal(0, 15, 3));
    }
}
