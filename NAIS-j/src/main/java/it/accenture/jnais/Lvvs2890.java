package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2890Data;

/**Original name: LVVS2890<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2013.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2890 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2890Data ws = new Lvvs2890Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2890_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2890 getInstance() {
        return ((Lvvs2890)Programs.getInstance(Lvvs2890.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        //
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: PERFORM S1200-CONTROLLO-DATI
        //              THRU S1200-CONTROLLO-DATI-EX.
        s1200ControlloDati();
        // COB_CODE: PERFORM S1300-RECUPERO-INFO
        //              THRU S1300-RECUPERO-INFO-EX.
        s1300RecuperoInfo();
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO DRAN-AREA-RAN
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DRAN-AREA-RAN
            ws.setDranAreaRanFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF DRAN-TP-PERS(IVVC0213-IX-TABB) EQUAL HIGH-VALUE
        //           OR LOW-VALUE OR SPACES
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (Conditions.eq(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranTpPers(), Types.HIGH_CHAR_VAL) || Conditions.eq(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranTpPers(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranTpPers(), Types.SPACE_CHAR)) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'TIPO PERSONA NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("TIPO PERSONA NON VALORIZZATO");
        }
    }

    /**Original name: S1300-RECUPERO-INFO<br>
	 * <pre>----------------------------------------------------------------*
	 *    RECUPERO INFORMAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300RecuperoInfo() {
        // COB_CODE: PERFORM ACCESSO-PERS
        //              THRU ACCESSO-PERS-EX.
        accessoPers();
    }

    /**Original name: ACCESSO-PERS<br>
	 * <pre>----------------------------------------------------------------*
	 *    RECUPERO DELLA PERS
	 * ----------------------------------------------------------------*</pre>*/
    private void accessoPers() {
        ConcatUtil concatUtil = null;
        Ldbs1300 ldbs1300 = null;
        // COB_CODE: INITIALIZE LDBV1301
        //                      PERS.
        initLdbv1301();
        initPers();
        // COB_CODE: IF DRAN-COD-SOGG-NULL(IVVC0213-IX-TABB) = HIGH-VALUES OR
        //                                                     LOW-VALUES  OR
        //                                                     SPACES
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           ELSE
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getDranCodSoggFormatted()) || Characters.EQ_LOW.test(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getDranCodSoggFormatted()) || Characters.EQ_SPACE.test(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranCodSogg())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CODICE SOGGETTO NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CODICE SOGGETTO NON VALORIZZATO");
        }
        else {
            // COB_CODE: MOVE DRAN-COD-SOGG(IVVC0213-IX-TABB) TO WK-ID-STRINGA
            ws.getAreaRecuperoAnagrafica().setWkIdStringa(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranCodSogg());
            // COB_CODE: PERFORM E000-CONVERTI-CHAR
            //              THRU EX-E000
            e000ConvertiChar();
            // COB_CODE: MOVE WK-ID-NUM                    TO WK-ID-PERS
            ws.getAreaRecuperoAnagrafica().setWkIdPers(Trunc.toInt(ws.getAreaRecuperoAnagrafica().getWkIdNum(), 9));
            // COB_CODE: MOVE 'LDBS1300'                   TO WK-CALL-PGM
            ws.setWkCallPgm("LDBS1300");
            // COB_CODE: SET IDSV0003-PRIMARY-KEY          TO TRUE
            idsv0003.getLivelloOperazione().setPrimaryKey();
            // COB_CODE: SET IDSV0003-SELECT               TO TRUE
            idsv0003.getOperazione().setSelect();
            // COB_CODE: MOVE WK-ID-PERS                   TO LDBV1301-ID-TAB
            ws.getLdbv1301().setLdbv1301IdTab(ws.getAreaRecuperoAnagrafica().getWkIdPers());
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-DATA-EFFETTO
            ws.getAreaRecuperoAnagrafica().getWkTimestamp().setWkDataEffetto(idsv0003.getDataInizioEffettoFormatted());
            // COB_CODE: MOVE WK-TIMESTAMP-N               TO LDBV1301-TIMESTAMP
            ws.getLdbv1301().setLdbv1301Timestamp(ws.getAreaRecuperoAnagrafica().getWkTimestamp().getWkTimestampN());
            // COB_CODE: MOVE LDBV1301 TO IDSV0003-BUFFER-WHERE-COND
            idsv0003.setBufferWhereCond(ws.getLdbv1301().getLdbv1301Formatted());
            // COB_CODE: CALL WK-CALL-PGM  USING  IDSV0003 PERS
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs1300 = Ldbs1300.getInstance();
                ldbs1300.run(idsv0003, ws.getPers());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2890'
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //            END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2890");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF.
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: IF A25-COD-FRM-GIURD-NULL = HIGH-VALUES OR
                //                                       LOW-VALUES  OR
                //                                       SPACES
                //              MOVE SPACES               TO A25-COD-FRM-GIURD
                //           END-IF
                if (Characters.EQ_HIGH.test(ws.getPers().getA25CodFrmGiurdFormatted()) || Characters.EQ_LOW.test(ws.getPers().getA25CodFrmGiurdFormatted()) || Characters.EQ_SPACE.test(ws.getPers().getA25CodFrmGiurd())) {
                    // COB_CODE: MOVE SPACES               TO A25-COD-FRM-GIURD
                    ws.getPers().setA25CodFrmGiurd("");
                }
                // COB_CODE: IF A25-IND-CLI = 'G' AND
                //              (A25-COD-FRM-GIURD NOT = 'F' AND 'I')
                //              MOVE 'G'                  TO IVVC0213-VAL-STR-O
                //           ELSE
                //              MOVE 'F'                  TO IVVC0213-VAL-STR-O
                //           END-IF
                if (ws.getPers().getA25IndCli() == 'G' && !Conditions.eq(ws.getPers().getA25CodFrmGiurd(), "F") && !Conditions.eq(ws.getPers().getA25CodFrmGiurd(), "I")) {
                    // COB_CODE: MOVE 'G'                  TO IVVC0213-VAL-STR-O
                    ivvc0213.getTabOutput().setValStrO("G");
                }
                else {
                    // COB_CODE: MOVE 'F'                  TO IVVC0213-VAL-STR-O
                    ivvc0213.getTabOutput().setValStrO("F");
                }
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2890'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, ws.getWkPgmFormatted(), " ERRORE CHIAMATA - MODULO LVVS2890", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound()) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: E000-CONVERTI-CHAR<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void e000ConvertiChar() {
        Iwfs0050 iwfs0050 = null;
        // COB_CODE: MOVE ZERO                  TO WK-ID-NUM.
        ws.getAreaRecuperoAnagrafica().setWkIdNum(0);
        // COB_CODE: MOVE HIGH-VALUE            TO AREA-CALL-IWFS0050.
        ws.getAreaCallIwfs0050().initAreaCallIwfs0050HighValues();
        // COB_CODE: MOVE WK-ID-STRINGA         TO IWFI0051-ARRAY-STRINGA-INPUT.
        ws.getAreaCallIwfs0050().setIwfi0051ArrayStringaInputFormatted(ws.getAreaRecuperoAnagrafica().getWkIdStringaFormatted());
        // COB_CODE: CALL IWFS0050              USING AREA-CALL-IWFS0050
        iwfs0050 = Iwfs0050.getInstance();
        iwfs0050.run(ws.getAreaCallIwfs0050());
        //    MOVE IWFO0051-ESITO        TO IDSV0003-ESITO.
        //    MOVE IWFO0051-LOG-ERRORE   TO IDSV0003-LOG-ERRORE.
        // COB_CODE: MOVE IWFO0051-CAMPO-OUTPUT-DEFI
        //                                      TO WK-ID-NUM.
        ws.getAreaRecuperoAnagrafica().setWkIdNum(TruncAbs.toLong(ws.getAreaCallIwfs0050().getIwfo0051().getCampoOutputDefi(), 11));
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initLdbv1301() {
        ws.getLdbv1301().setLdbv1301IdTab(0);
        ws.getLdbv1301().setLdbv1301Timestamp(0);
    }

    public void initPers() {
        ws.getPers().setA25IdPers(0);
        ws.getPers().setA25TstamIniVldt(0);
        ws.getPers().getA25TstamEndVldt().setA25TstamEndVldt(0);
        ws.getPers().setA25CodPers(0);
        ws.getPers().setA25RiftoRete("");
        ws.getPers().setA25CodPrtIva("");
        ws.getPers().setA25IndPvcyPrsnl(Types.SPACE_CHAR);
        ws.getPers().setA25IndPvcyCmmrc(Types.SPACE_CHAR);
        ws.getPers().setA25IndPvcyIndst(Types.SPACE_CHAR);
        ws.getPers().getA25DtNascCli().setA25DtNascCli(0);
        ws.getPers().getA25DtAcqsPers().setA25DtAcqsPers(0);
        ws.getPers().setA25IndCli(Types.SPACE_CHAR);
        ws.getPers().setA25CodCmn("");
        ws.getPers().setA25CodFrmGiurd("");
        ws.getPers().setA25CodEntePubb("");
        ws.getPers().setA25DenRgnSocLen(((short)0));
        ws.getPers().setA25DenRgnSoc("");
        ws.getPers().setA25DenSigRgnSocLen(((short)0));
        ws.getPers().setA25DenSigRgnSoc("");
        ws.getPers().setA25IndEseFisc(Types.SPACE_CHAR);
        ws.getPers().setA25CodStatCvl("");
        ws.getPers().setA25DenNomeLen(((short)0));
        ws.getPers().setA25DenNome("");
        ws.getPers().setA25DenCognLen(((short)0));
        ws.getPers().setA25DenCogn("");
        ws.getPers().setA25CodFisc("");
        ws.getPers().setA25IndSex(Types.SPACE_CHAR);
        ws.getPers().setA25IndCpctGiurd(Types.SPACE_CHAR);
        ws.getPers().setA25IndPortHdcp(Types.SPACE_CHAR);
        ws.getPers().setA25CodUserIns("");
        ws.getPers().setA25TstamInsRiga(0);
        ws.getPers().setA25CodUserAggm("");
        ws.getPers().getA25TstamAggmRiga().setA25TstamAggmRiga(0);
        ws.getPers().setA25DenCmnNascStrnLen(((short)0));
        ws.getPers().setA25DenCmnNascStrn("");
        ws.getPers().setA25CodRamoStgr("");
        ws.getPers().setA25CodStgrAtvtUic("");
        ws.getPers().setA25CodRamoAtvtUic("");
        ws.getPers().getA25DtEndVldtPers().setA25DtEndVldtPers(0);
        ws.getPers().getA25DtDeadPers().setA25DtDeadPers(0);
        ws.getPers().setA25TpStatCli("");
        ws.getPers().getA25DtBlocCli().setA25DtBlocCli(0);
        ws.getPers().getA25CodPersSecond().setA25CodPersSecond(0);
        ws.getPers().getA25IdSegmentazCli().setA25IdSegmentazCli(0);
        ws.getPers().getA25Dt1aAtvt().setA25Dt1aAtvt(0);
        ws.getPers().getA25DtSegnalPartner().setA25DtSegnalPartner(0);
    }
}
