package it.accenture.jnais;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0113Data;
import static java.lang.Math.abs;

/**Original name: LVVS0113<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0113
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0113 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0113Data ws = new Lvvs0113Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0113
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0113_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lvvs0113 getInstance() {
        return ((Lvvs0113)Programs.getInstance(Lvvs0113.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        Idbsrst0 idbsrst0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE RIS-DI-TRCH.
        initRisDiTrch();
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZERO                  TO WS-DT-RST-9.
        ws.setWsDtRst9(0);
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                      TO WS-DT-RST-AP.
        ws.setWsDtRstAp(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
        // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
        //                                      TO WS-DATA-COMPETENZA.
        ws.setWsDataCompetenza(idsv0003.getDataCompetenza());
        // COB_CODE: MOVE WS-DT-RST-AP          TO WS-DT-9999MMGG.
        ws.getWsDt9999mmgg().setWsDt9999mmggFormatted(ws.getWsDtRstApFormatted());
        // COB_CODE: COMPUTE WS-DT-RST-AA = WS-DT-AA - 1
        ws.getWsDtRst().setAa(Trunc.toShort(abs(ws.getWsDt9999mmgg().getAa() - 1), 4));
        // COB_CODE: MOVE WS-DT-RST             TO WS-DT-RST-9.
        ws.setWsDtRst9FromBuffer(ws.getWsDtRst().getWsDtRstBytes());
        // COB_CODE: MOVE ZEROES                TO IDSV0003-DATA-FINE-EFFETTO.
        idsv0003.setDataFineEffetto(0);
        // COB_CODE: MOVE WS-DT-RST-9           TO IDSV0003-DATA-INIZIO-EFFETTO.
        idsv0003.setDataInizioEffetto(ws.getWsDtRst9());
        // COB_CODE: SET IDSV0003-SELECT        TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-ID-PADRE      TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdPadre();
        //
        // COB_CODE: MOVE WS-DT-RST-9
        //             TO WS-DT-COMP-RST-8.
        ws.getWsDataCompRstX().setDtCompRst8Formatted(ws.getWsDtRst9Formatted());
        // COB_CODE: MOVE '40'
        //             TO WS-LT-40-RST.
        ws.getWsDataCompRstX().setLt40Rst("40");
        // COB_CODE: MOVE '23595999'
        //             TO WS-LT-ORA-COMP-RST-8.
        ws.getWsDataCompRstX().setLtOraCompRst8("23595999");
        // COB_CODE: MOVE WS-DATA-COMP-RST-9    TO IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ws.getWsDataCompRstX().getWsDataCompRst9());
        //--> VALORRIZZA DCLGEN TABELLA
        // COB_CODE: MOVE IVVC0213-ID-TRANCHE   TO RST-ID-TRCH-DI-GAR
        ws.getRisDiTrch().setRstIdTrchDiGar(ivvc0213.getIdTranche());
        //
        // COB_CODE: MOVE 'IDBSRST0'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSRST0");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 RIS-DI-TRCH
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            idbsrst0 = Idbsrst0.getInstance();
            idbsrst0.run(idsv0003, ws.getRisDiTrch());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-IDBSRST0 ERRORE CHIAMATA'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSRST0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //           AND IDSV0003-SUCCESSFUL-RC
        //              MOVE RST-ULT-COEFF-RIS            TO IVVC0213-VAL-PERC-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql() && idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE RST-ULT-COEFF-RIS            TO IVVC0213-VAL-PERC-O
            ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(ws.getRisDiTrch().getRstUltCoeffRis().getRstUltCoeffRis(), 14, 9));
        }
        else if (idsv0003.getSqlcode().getSqlcode() == 100) {
            // COB_CODE: IF IDSV0003-SQLCODE = +100
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA IDBSRST0 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSRST0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
        //
        // COB_CODE: MOVE WS-DT-RST-AP
        //             TO IDSV0003-DATA-INIZIO-EFFETTO.
        idsv0003.setDataInizioEffetto(ws.getWsDtRstAp());
        //
        // COB_CODE: MOVE WS-DATA-COMPETENZA
        //             TO IDSV0003-DATA-COMPETENZA.
        idsv0003.setDataCompetenza(ws.getWsDataCompetenza());
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabAde(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initRisDiTrch() {
        ws.getRisDiTrch().setRstIdRisDiTrch(0);
        ws.getRisDiTrch().setRstIdMoviCrz(0);
        ws.getRisDiTrch().getRstIdMoviChiu().setRstIdMoviChiu(0);
        ws.getRisDiTrch().setRstDtIniEff(0);
        ws.getRisDiTrch().setRstDtEndEff(0);
        ws.getRisDiTrch().setRstCodCompAnia(0);
        ws.getRisDiTrch().setRstTpCalcRis("");
        ws.getRisDiTrch().getRstUltRm().setRstUltRm(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstDtCalc().setRstDtCalc(0);
        ws.getRisDiTrch().getRstDtElab().setRstDtElab(0);
        ws.getRisDiTrch().getRstRisBila().setRstRisBila(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisMat().setRstRisMat(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstIncrXRival().setRstIncrXRival(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRptoPre().setRstRptoPre(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstFrazPrePp().setRstFrazPrePp(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisTot().setRstRisTot(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisSpe().setRstRisSpe(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisAbb().setRstRisAbb(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisBnsfdt().setRstRisBnsfdt(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisSopr().setRstRisSopr(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisIntegBasTec().setRstRisIntegBasTec(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisIntegDecrTs().setRstRisIntegDecrTs(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisGarCasoMor().setRstRisGarCasoMor(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisZil().setRstRisZil(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisFaivl().setRstRisFaivl(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisCosAmmtz().setRstRisCosAmmtz(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisSpeFaivl().setRstRisSpeFaivl(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisPrestFaivl().setRstRisPrestFaivl(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisComponAssva().setRstRisComponAssva(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstUltCoeffRis().setRstUltCoeffRis(new AfDecimal(0, 14, 9));
        ws.getRisDiTrch().getRstUltCoeffAggRis().setRstUltCoeffAggRis(new AfDecimal(0, 14, 9));
        ws.getRisDiTrch().getRstRisAcq().setRstRisAcq(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisUti().setRstRisUti(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisMatEff().setRstRisMatEff(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisRistorniCap().setRstRisRistorniCap(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisTrmBns().setRstRisTrmBns(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisBnsric().setRstRisBnsric(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().setRstDsRiga(0);
        ws.getRisDiTrch().setRstDsOperSql(Types.SPACE_CHAR);
        ws.getRisDiTrch().setRstDsVer(0);
        ws.getRisDiTrch().setRstDsTsIniCptz(0);
        ws.getRisDiTrch().setRstDsTsEndCptz(0);
        ws.getRisDiTrch().setRstDsUtente("");
        ws.getRisDiTrch().setRstDsStatoElab(Types.SPACE_CHAR);
        ws.getRisDiTrch().setRstIdTrchDiGar(0);
        ws.getRisDiTrch().setRstCodFnd("");
        ws.getRisDiTrch().setRstIdPoli(0);
        ws.getRisDiTrch().setRstIdAdes(0);
        ws.getRisDiTrch().setRstIdGar(0);
        ws.getRisDiTrch().getRstRisMinGarto().setRstRisMinGarto(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisRshDflt().setRstRisRshDflt(new AfDecimal(0, 15, 3));
        ws.getRisDiTrch().getRstRisMoviNonInves().setRstRisMoviNonInves(new AfDecimal(0, 15, 3));
    }
}
