package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.InRcode;
import it.accenture.jnais.ws.IoA2kLccc0003;
import it.accenture.jnais.ws.Lccs0003Data;
import static java.lang.Math.abs;

/**Original name: LCCS0003<br>
 * <pre>*****************************************************************
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             NOMOS SISTEMA.
 * DATE-WRITTEN.       2006.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA...... LCCS0003
 *     TIPOLOGIA...... CONTROLLI SULLE DATE
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE....
 *      LA  ROUTINE CONVERTE INIZIALMENTE IL FORMATO DATA
 *      FORNITO IN INPUT AL FORMATO GGGGGGG  PROGRESSIVO
 *      SOMMA/SOTTRAE I GIORNI/MESI/ANNI
 *      RICONVERTE IL PROGRESSIVO NEI FORMATI DI OUTPUT
 *      AREADAT E' DESCRITTA NELLA COPY LCCC0003
 *      IN ESSA DEVE ESSERE DEFINITO :
 *     -FUNZ = FUNZIONE DA EFFETTUARE SULLA DATA PASSATA
 *           01  CONVERSIONE TRA IL FORMATO DI INPUT E TUTTI
 *               QUELLI DI OUTPUT
 *           02  SOMMARE IL CAMPO DELTA ALLA DATA DI INPUT
 *           03  SOTTRARRE  DELTA  ALLA DATA DI INPUT
 *           04  CONTROLLO SE LA DATA DI INPUT E' FESTA
 *           05  CONTROLLO SE FINE MESE, DECADALE, QUINDICINA
 *               LE, NR. TRIMESTRE
 *           06  CALCOLO PASQUA PER LA DATA DI INPUT
 *           07  COME FUNZ 01 TRANNE CHE PER OUGG07 E OUFA07
 *               NUMERO GG LAV DEL MESE E ULTIMO GG LAV DEL MESE
 *     -INFO = INDICARE  IL FORMATO DELLA DATA DI INPUT
 *           01 LA DATA E' IN INGMA   FORMATO GGMMAAAA
 *           02 LA DATA E' IN INGBMBA FORMATO GG/MM/AAAA
 *           03 LA DATA E' IN INAMG   FORMATO AAAAMMGG
 *           04 LA DATA E' IN INAMGP  PACKED  AAAAMMGGS
 *           05 LA DATA E' IN INAMG0P PACKED  AAAAMMGG0S
 *           06 LA DATA E' IN INPROG9 PACKED  GGGGGS PROGRESS.
 *                                                  DAL 1900
 *           07 LA DATA E' IN INPROG PACKED GGGGGGGS PROGRESSIVO
 *           08 LA DATA E' IN INJUL  FORMATO AAAAGGG GIULIANO
 *           09 LA DATA E' IN INPROGC PACKED  GGGGGS PROGRESS
 *                           DAL 1900 SECONDO ANNO COMMERCIALE
 *           10 LA DATA E' IN INPROCO PACKED  GGGGGS PROGRESS
 *                                     ANNO COMMERCIALE
 *           11 LA DATA E' IN INGBMBA FORMATO GG.MM.AAAA
 *     -DELTA= INDICARE IL NUMERO DA SOMMARE/SOTTRARRE
 *     -TDELTA = SPACE DELTA CONTIENE GIORNI
 *           M       DELTA CONTIENE MESI
 *           A       DELTA CONTIENE ANNI
 *     -FISLAV= 0 DELTA CONTIENE GIORNI FISSI
 *              1 DELTA CONTIENE GIORNI LAVORATIVI
 *     -INICON= INIZIO CONTEGGIO PER SOMMA E SOTTRAZIONE
 *              0 INIZIO DALLO STESSO GIORNO
 *              1 INIZIO DAL PRIMO GIORNO LAVORATIVO
 *              2 INIZIO DAL GIORNO SOLO SE FESTIVO
 * **------------------------------------------------------------***
 * ----------------------------------------------------------------*</pre>*/
public class Lccs0003 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0003Data ws = new Lccs0003Data();
    //Original name: IO-A2K-LCCC0003
    private IoA2kLccc0003 ioA2kLccc0003;
    //Original name: IN-RCODE
    private InRcode inRcode;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(IoA2kLccc0003 ioA2kLccc0003, InRcode inRcode) {
        this.ioA2kLccc0003 = ioA2kLccc0003;
        this.inRcode = inRcode;
        aMain();
        a999Exit();
        return 0;
    }

    public static Lccs0003 getInstance() {
        return ((Lccs0003)Programs.getInstance(Lccs0003.class));
    }

    /**Original name: A-MAIN_FIRST_SENTENCES<br>
	 * <pre>---------------
	 * >>  OTTIENI IL NUMERO DI PARAMETRI DI CHIAMATA - IN RETURN-CODE
	 * >>  POSSIBILE 1 O 2 PARAMETRI
	 * >>  SE NESSUN PARAMETRO, RETURN-CODE = 99</pre>*/
    private void aMain() {
        // COB_CODE: MOVE SPACES TO A2K-OUTPUT
        ioA2kLccc0003.getOutput().initOutputSpaces();
        // COB_CODE: MOVE ZERO   TO A2K-OUAMGP
        //                          A2K-OUAMG0P
        //                          A2K-OUPROG9
        //                          A2K-OUPROGC
        //                          A2K-OUPROG
        //                          A2K-OUPROCO
        ioA2kLccc0003.getOutput().setA2kOuamgp(0);
        ioA2kLccc0003.getOutput().setA2kOuamg0p(0);
        ioA2kLccc0003.getOutput().setA2kOuprog9(0);
        ioA2kLccc0003.getOutput().setA2kOuprogc(0);
        ioA2kLccc0003.getOutput().setA2kOuprog(0);
        ioA2kLccc0003.getOutput().setA2kOuproco(0);
        // COB_CODE: MOVE ZERO   TO WS-RCODE
        ws.setWsRcode(((short)0));
        // COB_CODE: PERFORM B-VALIDATE-INPUT
        bValidateInput();
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO A-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO A-999-EXIT
            a999Exit();
        }
        // COB_CODE: IF  A2K-FUNZ = '02'
        //               PERFORM C-ADD-DELTA
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kFunz(), "02")) {
            // COB_CODE: PERFORM C-ADD-DELTA
            cAddDelta();
        }
        // COB_CODE: IF  A2K-FUNZ = '03'
        //               PERFORM D-SUBTRACT-DELTA
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kFunz(), "03")) {
            // COB_CODE: PERFORM D-SUBTRACT-DELTA
            dSubtractDelta();
        }
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO A-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO A-999-EXIT
            a999Exit();
        }
        // COB_CODE: PERFORM E-PROCESS-CONVERSIONI.
        eProcessConversioni();
    }

    /**Original name: A-999-EXIT<br>
	 * <pre>-----------</pre>*/
    private void a999Exit() {
        // COB_CODE: MOVE WS-RCODE TO IN-RCODE
        inRcode.setInRcodeFormatted(ws.getWsRcodeFormatted());
        // COB_CODE: MOVE ZERO TO RETURN-CODE
        Session.setReturnCode(0);
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: B-VALIDATE-INPUT_FIRST_SENTENCES<br>
	 * <pre>-------------------------</pre>*/
    private void bValidateInput() {
        // COB_CODE: IF  A2K-FUNZ LESS    '01'
        //           OR  A2K-FUNZ GREATER '07'
        //               MOVE '84' TO WS-RCODE
        //           END-IF
        if (Conditions.lt(ioA2kLccc0003.getInput().getA2kFunz(), "01") || Conditions.gt(ioA2kLccc0003.getInput().getA2kFunz(), "07")) {
            // COB_CODE: MOVE '84' TO WS-RCODE
            ws.setWsRcodeFormatted("84");
        }
        // COB_CODE: IF  A2K-INFO LESS    '01'
        //           OR  A2K-INFO GREATER '11'
        //               MOVE '85' TO WS-RCODE
        //           END-IF
        if (Conditions.lt(ioA2kLccc0003.getInput().getA2kInfo(), "01") || Conditions.gt(ioA2kLccc0003.getInput().getA2kInfo(), "11")) {
            // COB_CODE: MOVE '85' TO WS-RCODE
            ws.setWsRcodeFormatted("85");
        }
        // COB_CODE: EVALUATE A2K-INFO
        //               WHEN '01'
        //                    PERFORM  B01-VALIDATE-INFO-01
        //               WHEN '02'
        //                    PERFORM  B02-VALIDATE-INFO-02
        //               WHEN '03'
        //                    PERFORM  B03-VALIDATE-INFO-03
        //               WHEN '04'
        //                    PERFORM  B04-VALIDATE-INFO-04
        //               WHEN '05'
        //                    PERFORM  B05-VALIDATE-INFO-05
        //               WHEN '06'
        //                    PERFORM  B06-VALIDATE-INFO-06
        //               WHEN '07'
        //                    PERFORM  B07-VALIDATE-INFO-07
        //               WHEN '08'
        //                    PERFORM  B08-VALIDATE-INFO-08
        //               WHEN '09'
        //                    PERFORM  B09-VALIDATE-INFO-09
        //               WHEN '10'
        //                    PERFORM  B10-VALIDATE-INFO-10
        //               WHEN '11'
        //                    PERFORM  B02-VALIDATE-INFO-02
        //           END-EVALUATE.
        switch (ioA2kLccc0003.getInput().getA2kInfo()) {

            case "01":// COB_CODE: PERFORM  B01-VALIDATE-INFO-01
                b01ValidateInfo01();
                break;

            case "02":// COB_CODE: PERFORM  B02-VALIDATE-INFO-02
                b02ValidateInfo02();
                break;

            case "03":// COB_CODE: PERFORM  B03-VALIDATE-INFO-03
                b03ValidateInfo03();
                break;

            case "04":// COB_CODE: PERFORM  B04-VALIDATE-INFO-04
                b04ValidateInfo04();
                break;

            case "05":// COB_CODE: PERFORM  B05-VALIDATE-INFO-05
                b05ValidateInfo05();
                break;

            case "06":// COB_CODE: PERFORM  B06-VALIDATE-INFO-06
                b06ValidateInfo06();
                break;

            case "07":// COB_CODE: PERFORM  B07-VALIDATE-INFO-07
                b07ValidateInfo07();
                break;

            case "08":// COB_CODE: PERFORM  B08-VALIDATE-INFO-08
                b08ValidateInfo08();
                break;

            case "09":// COB_CODE: PERFORM  B09-VALIDATE-INFO-09
                b09ValidateInfo09();
                break;

            case "10":// COB_CODE: PERFORM  B10-VALIDATE-INFO-10
                rngB10ValidateInfo10();
                break;

            case "11":// COB_CODE: PERFORM  B02-VALIDATE-INFO-02
                b02ValidateInfo02();
                break;

            default:break;
        }
    }

    /**Original name: B01-VALIDATE-INFO-01_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO GGMMAAAA IN A2K-INGMA</pre>*/
    private void b01ValidateInfo01() {
        // COB_CODE: IF  A2K-INGMA-X NOT NUMERIC
        //               GO TO B01-999-EXIT
        //           END-IF
        if (!Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getIngmaXBytes())) {
            // COB_CODE: MOVE '93' TO WS-RCODE
            ws.setWsRcodeFormatted("93");
            // COB_CODE: GO TO B01-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-INSS = ZERO
        //           AND A2K-INAA = ZERO
        //               GO TO B01-999-EXIT
        //           END-IF
        if (Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInssFormatted()) && Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInaaFormatted())) {
            // COB_CODE: MOVE '93' TO WS-RCODE
            ws.setWsRcodeFormatted("93");
            // COB_CODE: GO TO B01-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INGMA TO WS-DATA
        ws.getWsData().setWsDataFormatted(ioA2kLccc0003.getInput().getA2kIndata().getIngmaFormatted());
        // COB_CODE: PERFORM BA-VALIDATE-MMGG.
        baValidateMmgg();
    }

    /**Original name: B02-VALIDATE-INFO-02_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO GG/MM/AAAA IN A2K-INGBMBA
	 *             GG.MM.AAAA</pre>*/
    private void b02ValidateInfo02() {
        // COB_CODE: IF  A2K-INSS02 = ZERO
        //           AND A2K-INAA02 = ZERO
        //               GO TO B02-999-EXIT
        //           END-IF
        if (Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInss02Formatted()) && Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInaa02Formatted())) {
            // COB_CODE: MOVE '95' TO WS-RCODE
            ws.setWsRcodeFormatted("95");
            // COB_CODE: GO TO B02-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INGG02 TO WS-DATA-GG
        ws.getWsData().setDataGgFormatted(ioA2kLccc0003.getInput().getA2kIndata().getIngg02Formatted());
        // COB_CODE: MOVE A2K-INMM02 TO WS-DATA-MM
        ws.getWsData().setDataMmFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInmm02Formatted());
        // COB_CODE: MOVE A2K-INSS02 TO WS-DATA-SS
        ws.getWsData().setDataSsFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInss02Formatted());
        // COB_CODE: MOVE A2K-INAA02 TO WS-DATA-AA
        ws.getWsData().setDataAaFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInaa02Formatted());
        // COB_CODE: PERFORM BA-VALIDATE-MMGG.
        baValidateMmgg();
    }

    /**Original name: B03-VALIDATE-INFO-03_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO AAAAMMGG IN A2K-INAMG</pre>*/
    private void b03ValidateInfo03() {
        // COB_CODE: IF  A2K-INSS03 = ZERO
        //           AND A2K-INAA03 = ZERO
        //               GO TO B03-999-EXIT
        //           END-IF
        if (Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInss03Formatted()) && Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInaa03Formatted())) {
            // COB_CODE: MOVE '96' TO WS-RCODE
            ws.setWsRcodeFormatted("96");
            // COB_CODE: GO TO B03-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INGG03 TO WS-DATA-GG
        ws.getWsData().setDataGgFormatted(ioA2kLccc0003.getInput().getA2kIndata().getIngg03Formatted());
        // COB_CODE: MOVE A2K-INMM03 TO WS-DATA-MM
        ws.getWsData().setDataMmFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInmm03Formatted());
        // COB_CODE: MOVE A2K-INSS03 TO WS-DATA-SS
        ws.getWsData().setDataSsFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInss03Formatted());
        // COB_CODE: MOVE A2K-INAA03 TO WS-DATA-AA
        ws.getWsData().setDataAaFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInaa03Formatted());
        // COB_CODE: PERFORM BA-VALIDATE-MMGG.
        baValidateMmgg();
    }

    /**Original name: B04-VALIDATE-INFO-04_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO AAAAMMGGS IN A2K-INAMGP</pre>*/
    private void b04ValidateInfo04() {
        // COB_CODE: IF  A2K-INDATA (1:5) = SPACES
        //           OR  A2K-INAMGP NOT NUMERIC
        //           OR  A2K-INAMGP = ZERO
        //               GO TO B04-999-EXIT
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kIndata().getA2kIndataFormatted().substring((1) - 1, 5), "") || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInamgp()) || ioA2kLccc0003.getInput().getA2kIndata().getInamgp() == 0) {
            // COB_CODE: MOVE '97' TO WS-RCODE
            ws.setWsRcodeFormatted("97");
            // COB_CODE: GO TO B04-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INAMGP TO WS-DATA-NUM
        ws.getWsDataNum().setWsDataNum(TruncAbs.toInt(ioA2kLccc0003.getInput().getA2kIndata().getInamgp(), 8));
        // COB_CODE: MOVE WS-DATA-NUM (1:2) TO WS-DATA-SS
        ws.getWsData().setDataSsFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((1) - 1, 2));
        // COB_CODE: MOVE WS-DATA-NUM (3:2) TO WS-DATA-AA
        ws.getWsData().setDataAaFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((3) - 1, 4));
        // COB_CODE: MOVE WS-DATA-NUM (5:2) TO WS-DATA-MM
        ws.getWsData().setDataMmFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((5) - 1, 6));
        // COB_CODE: MOVE WS-DATA-NUM (7:2) TO WS-DATA-GG
        ws.getWsData().setDataGgFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((7) - 1, 8));
        // COB_CODE: PERFORM BA-VALIDATE-MMGG
        baValidateMmgg();
        // COB_CODE: IF  WS-RCODE = ZERO
        //               MOVE A2K-INAMGP TO A2K-OUAMG
        //           END-IF.
        if (Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: MOVE A2K-INAMGP TO A2K-OUAMG
            ioA2kLccc0003.getOutput().getA2kOuamgX().setA2kOuamg(TruncAbs.toInt(ioA2kLccc0003.getInput().getA2kIndata().getInamgp(), 8));
        }
    }

    /**Original name: B05-VALIDATE-INFO-05_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO AAAAMMGG0S IN A2K-INAMG0P</pre>*/
    private void b05ValidateInfo05() {
        // COB_CODE: IF  A2K-INDATA (1:5) = SPACES
        //           OR  A2K-INAMG0P NOT NUMERIC
        //           OR  A2K-INAMG0P = ZERO
        //               GO TO B05-999-EXIT
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kIndata().getA2kIndataFormatted().substring((1) - 1, 5), "") || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInamg0p()) || ioA2kLccc0003.getInput().getA2kIndata().getInamg0p() == 0) {
            // COB_CODE: MOVE '98' TO WS-RCODE
            ws.setWsRcodeFormatted("98");
            // COB_CODE: GO TO B05-999-EXIT
            return;
        }
        // COB_CODE: DIVIDE 10 INTO A2K-INAMG0P GIVING WS-DATA-NUM
        ws.getWsDataNum().setWsDataNum(Trunc.toInt(abs(ioA2kLccc0003.getInput().getA2kIndata().getInamg0p() / 10), 8));
        // COB_CODE: MOVE WS-DATA-NUM (1:2) TO WS-DATA-SS
        ws.getWsData().setDataSsFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((1) - 1, 2));
        // COB_CODE: MOVE WS-DATA-NUM (3:2) TO WS-DATA-AA
        ws.getWsData().setDataAaFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((3) - 1, 4));
        // COB_CODE: MOVE WS-DATA-NUM (5:2) TO WS-DATA-MM
        ws.getWsData().setDataMmFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((5) - 1, 6));
        // COB_CODE: MOVE WS-DATA-NUM (7:2) TO WS-DATA-GG
        ws.getWsData().setDataGgFormatted(ws.getWsDataNum().getWsDataNumFormatted().substring((7) - 1, 8));
        // COB_CODE: PERFORM BA-VALIDATE-MMGG.
        baValidateMmgg();
    }

    /**Original name: B06-VALIDATE-INFO-06_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO GGGGGS IN A2K-INPROG9
	 *              PROGRESSIVO DAL 1900</pre>*/
    private void b06ValidateInfo06() {
        // COB_CODE: IF  A2K-INDATA (1:3) = SPACES
        //           OR  A2K-INPROG9 NOT NUMERIC
        //           OR  A2K-INPROG9 = ZERO
        //               GO TO B06-999-EXIT
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kIndata().getA2kIndataFormatted().substring((1) - 1, 3), "") || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInprog9()) || ioA2kLccc0003.getInput().getA2kIndata().getInprog9() == 0) {
            // COB_CODE: MOVE '92' TO WS-RCODE
            ws.setWsRcodeFormatted("92");
            // COB_CODE: GO TO B06-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INPROG9 TO A2K-OUPROG
        ioA2kLccc0003.getOutput().setA2kOuprog(TruncAbs.toInt(ioA2kLccc0003.getInput().getA2kIndata().getInprog9(), 7));
        // COB_CODE: ADD  WA-INIPROG  TO A2K-OUPROG.
        ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ws.getWaIniprog() + ioA2kLccc0003.getOutput().getA2kOuprog()), 7));
    }

    /**Original name: B07-VALIDATE-INFO-07_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO GGGGGGGS IN A2K-INPROG
	 *                        PROGRESSIVO</pre>*/
    private void b07ValidateInfo07() {
        // COB_CODE: IF  A2K-INDATA (1:4) = SPACES
        //           OR  A2K-INPROG NOT NUMERIC
        //           OR  A2K-INPROG = ZERO
        //               GO TO B07-999-EXIT
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kIndata().getA2kIndataFormatted().substring((1) - 1, 4), "") || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInprog()) || ioA2kLccc0003.getInput().getA2kIndata().getInprog() == 0) {
            // COB_CODE: MOVE '91' TO WS-RCODE
            ws.setWsRcodeFormatted("91");
            // COB_CODE: GO TO B07-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INPROG TO A2K-OUPROG.
        ioA2kLccc0003.getOutput().setA2kOuprog(TruncAbs.toInt(ioA2kLccc0003.getInput().getA2kIndata().getInprog(), 7));
    }

    /**Original name: B08-VALIDATE-INFO-08_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO AAAAGGG IN A2K-INJUL
	 *                    DATA GIULIANA</pre>*/
    private void b08ValidateInfo08() {
        // COB_CODE: IF  A2K-INJUL-X = SPACES
        //           OR  A2K-INJUL NOT NUMERIC
        //           OR  A2K-INJUL = ZERO
        //               GO TO B08-999-EXIT
        //           END-IF
        if (Characters.EQ_SPACE.test(ioA2kLccc0003.getInput().getA2kIndata().getInjulXBytes()) || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInjulFormatted()) || Characters.EQ_ZERO.test(ioA2kLccc0003.getInput().getA2kIndata().getInjulFormatted())) {
            // COB_CODE: MOVE '99' TO WS-RCODE
            ws.setWsRcodeFormatted("99");
            // COB_CODE: GO TO B08-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-INJGGG GREATER 366
        //               GO TO B08-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kIndata().getInjggg() > 366) {
            // COB_CODE: MOVE '99' TO WS-RCODE
            ws.setWsRcodeFormatted("99");
            // COB_CODE: GO TO B08-999-EXIT
            return;
        }
        // COB_CODE: MOVE ZERO TO WS-DATA
        ws.getWsData().setWsData(0);
        // COB_CODE: MOVE A2K-INJSS TO WS-DATA-SS
        ws.getWsData().setDataSsFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInjssFormatted());
        // COB_CODE: MOVE A2K-INJAA TO WS-DATA-AA
        ws.getWsData().setDataAaFormatted(ioA2kLccc0003.getInput().getA2kIndata().getInjaaFormatted());
        // COB_CODE: MOVE WS-DATA   TO WS-DATAP
        ws.setWsDatap(Trunc.toInt(ws.getWsData().getWsData(), 7));
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: IF  A2K-INJGGG = 366
        //           AND WS-MESE (2) NOT = 29
        //               GO TO B08-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kIndata().getInjggg() == 366 && ws.getWsMese(2) != 29) {
            // COB_CODE: MOVE '99' TO WS-RCODE
            ws.setWsRcodeFormatted("99");
            // COB_CODE: GO TO B08-999-EXIT
            return;
        }
        // COB_CODE: SUBTRACT 1 FROM WS-DATAP
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() - 1, 7));
        // COB_CODE: PERFORM O-COMPUTE-CAL3112
        oComputeCal3112();
        // COB_CODE: ADD A2K-INJGGG TO WS-DATAP
        ws.setWsDatap(Trunc.toInt(ioA2kLccc0003.getInput().getA2kIndata().getInjggg() + ws.getWsDatap(), 7));
        // COB_CODE: MOVE WS-DATAP TO A2K-OUPROG.
        ioA2kLccc0003.getOutput().setA2kOuprog(TruncAbs.toInt(ws.getWsDatap(), 7));
    }

    /**Original name: B09-VALIDATE-INFO-09_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO GGGGGS IN A2K-INPROGC
	 *             PROGR. COMM. DAL 1900</pre>*/
    private void b09ValidateInfo09() {
        // COB_CODE: IF  A2K-INDATA (1:3) = SPACES
        //           OR  A2K-INPROGC NOT NUMERIC
        //           OR  A2K-INPROGC = ZERO
        //               GO TO B09-999-EXIT
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kIndata().getA2kIndataFormatted().substring((1) - 1, 3), "") || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInprogc()) || ioA2kLccc0003.getInput().getA2kIndata().getInprogc() == 0) {
            // COB_CODE: MOVE '90' TO WS-RCODE
            ws.setWsRcodeFormatted("90");
            // COB_CODE: GO TO B09-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-INPROGC TO WS-DATAP
        ws.setWsDatap(ioA2kLccc0003.getInput().getA2kIndata().getInprogc());
        // COB_CODE: DIVIDE 360 INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 360);
        ws.setWsRemainder(ws.getWsDatap() % 360);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO B09-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: ADD 1900 WS-QUOTIENT GIVING WS-DATA-SSAA
            ws.getWsData().setDataSsaa(Trunc.toShort(abs(1900 + ws.getWsQuotient()), 4));
            // COB_CODE: MOVE 31 TO WS-DATA-GG
            ws.getWsData().setDataGg(((short)31));
            // COB_CODE: MOVE 12 TO WS-DATA-MM
            ws.getWsData().setDataMm(((short)12));
            // COB_CODE: PERFORM BA-VALIDATE-MMGG
            baValidateMmgg();
            // COB_CODE: GO TO B09-999-EXIT
            return;
        }
        // COB_CODE: ADD 1901 WS-QUOTIENT GIVING WS-DATA-SSAA
        ws.getWsData().setDataSsaa(Trunc.toShort(abs(1901 + ws.getWsQuotient()), 4));
        // COB_CODE: MOVE WS-REMAINDER TO WS-DATA-WK
        ws.setWsDataWk(ws.getWsRemainder());
        // COB_CODE: DIVIDE 30  INTO WS-DATA-WK
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDataWk() / 30);
        ws.setWsRemainder(ws.getWsDataWk() % 30);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               MOVE 99 TO WS-DATA-GG
        //           ELSE
        //               MOVE WS-REMAINDER TO WS-DATA-GG
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-QUOTIENT TO WS-DATA-MM
            ws.getWsData().setDataMm(TruncAbs.toShort(ws.getWsQuotient(), 2));
            // COB_CODE: MOVE 99 TO WS-DATA-GG
            ws.getWsData().setDataGg(((short)99));
        }
        else {
            // COB_CODE: ADD 1 WS-QUOTIENT GIVING WS-DATA-MM
            ws.getWsData().setDataMm(Trunc.toShort(abs(1 + ws.getWsQuotient()), 2));
            // COB_CODE: MOVE WS-REMAINDER TO WS-DATA-GG
            ws.getWsData().setDataGg(TruncAbs.toShort(ws.getWsRemainder(), 2));
        }
        // COB_CODE: MOVE WS-DATA-SSAA TO WS-DATAP
        ws.setWsDatap(ws.getWsData().getDataSsaa());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: MOVE WS-DATA-MM TO I2
        ws.setI2(ws.getWsData().getDataMm());
        // COB_CODE: IF  WS-DATA-GG GREATER WS-MESE (I2)
        //               MOVE WS-MESE (I2) TO WS-DATA-GG
        //           END-IF.
        if (ws.getWsData().getDataGg() > ws.getWsMese(ws.getI2())) {
            // COB_CODE: MOVE WS-MESE (I2) TO WS-DATA-GG
            ws.getWsData().setDataGgFormatted(ws.getWsMeseFormatted(ws.getI2()));
        }
        // COB_CODE: PERFORM BA-VALIDATE-MMGG.
        baValidateMmgg();
    }

    /**Original name: B09-999-EXIT<br>*/
    private void b09999Exit() {
    }

    /**Original name: B10-VALIDATE-INFO-10_FIRST_SENTENCES<br>
	 * <pre>-----------------------------
	 *     FORMATO GGGGGGGS IN A2K-INPROGO
	 *             PROGRESSIVO COMMERCIALE</pre>*/
    private String b10ValidateInfo10() {
        // COB_CODE: IF  A2K-INDATA (1:4) = SPACES
        //           OR  A2K-INPROCO NOT NUMERIC
        //           OR  A2K-INPROCO = ZERO
        //               GO TO B09-999-EXIT
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kIndata().getA2kIndataFormatted().substring((1) - 1, 4), "") || !Functions.isNumber(ioA2kLccc0003.getInput().getA2kIndata().getInproco()) || ioA2kLccc0003.getInput().getA2kIndata().getInproco() == 0) {
            // COB_CODE: MOVE '86' TO WS-RCODE
            ws.setWsRcodeFormatted("86");
            // COB_CODE: GO TO B09-999-EXIT
            return "B09-999-EXIT";
        }
        // COB_CODE: MOVE A2K-INPROCO TO A2K-OUPROCO
        ioA2kLccc0003.getOutput().setA2kOuproco(TruncAbs.toInt(ioA2kLccc0003.getInput().getA2kIndata().getInproco(), 7));
        // COB_CODE: MOVE A2K-INPROCO TO WS-DATAP
        ws.setWsDatap(ioA2kLccc0003.getInput().getA2kIndata().getInproco());
        // COB_CODE: DIVIDE 360 INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 360);
        ws.setWsRemainder(ws.getWsDatap() % 360);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO B10-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-QUOTIENT TO WS-DATA-SSAA
            ws.getWsData().setDataSsaa(TruncAbs.toShort(ws.getWsQuotient(), 4));
            // COB_CODE: MOVE 31 TO WS-DATA-GG
            ws.getWsData().setDataGg(((short)31));
            // COB_CODE: MOVE 12 TO WS-DATA-MM
            ws.getWsData().setDataMm(((short)12));
            // COB_CODE: PERFORM BA-VALIDATE-MMGG
            baValidateMmgg();
            // COB_CODE: GO TO B10-999-EXIT
            return "";
        }
        // COB_CODE: ADD 1 WS-QUOTIENT GIVING WS-DATA-SSAA
        ws.getWsData().setDataSsaa(Trunc.toShort(abs(1 + ws.getWsQuotient()), 4));
        // COB_CODE: MOVE WS-REMAINDER TO WS-DATA-WK
        ws.setWsDataWk(ws.getWsRemainder());
        // COB_CODE: DIVIDE 30  INTO WS-DATA-WK
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDataWk() / 30);
        ws.setWsRemainder(ws.getWsDataWk() % 30);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               MOVE 99 TO WS-DATA-GG
        //           ELSE
        //               MOVE WS-REMAINDER TO WS-DATA-GG
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-QUOTIENT TO WS-DATA-MM
            ws.getWsData().setDataMm(TruncAbs.toShort(ws.getWsQuotient(), 2));
            // COB_CODE: MOVE 99 TO WS-DATA-GG
            ws.getWsData().setDataGg(((short)99));
        }
        else {
            // COB_CODE: ADD 1 WS-QUOTIENT GIVING WS-DATA-MM
            ws.getWsData().setDataMm(Trunc.toShort(abs(1 + ws.getWsQuotient()), 2));
            // COB_CODE: MOVE WS-REMAINDER TO WS-DATA-GG
            ws.getWsData().setDataGg(TruncAbs.toShort(ws.getWsRemainder(), 2));
        }
        // COB_CODE: MOVE WS-DATA-SSAA TO WS-DATAP
        ws.setWsDatap(ws.getWsData().getDataSsaa());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: MOVE WS-DATA-MM TO I2
        ws.setI2(ws.getWsData().getDataMm());
        // COB_CODE: IF  WS-DATA-GG GREATER WS-MESE (I2)
        //               MOVE WS-MESE (I2) TO WS-DATA-GG
        //           END-IF.
        if (ws.getWsData().getDataGg() > ws.getWsMese(ws.getI2())) {
            // COB_CODE: MOVE WS-MESE (I2) TO WS-DATA-GG
            ws.getWsData().setDataGgFormatted(ws.getWsMeseFormatted(ws.getI2()));
        }
        // COB_CODE: PERFORM BA-VALIDATE-MMGG.
        baValidateMmgg();
        return "";
    }

    /**Original name: B10-999-EXIT<br>*/
    private void b10999Exit() {
    }

    /**Original name: BA-VALIDATE-MMGG_FIRST_SENTENCES<br>
	 * <pre>-------------------------</pre>*/
    private void baValidateMmgg() {
        // COB_CODE: IF  WS-DATA-MM LESS    '01'
        //           OR  WS-DATA-MM GREATER '12'
        //               GO TO BA-999-EXIT
        //           END-IF
        if (ws.getWsData().getDataMmFormatted().compareTo("01") < 0 || ws.getWsData().getDataMmFormatted().compareTo("12") > 0) {
            // COB_CODE: MOVE '94' TO WS-RCODE
            ws.setWsRcodeFormatted("94");
            // COB_CODE: GO TO BA-999-EXIT
            return;
        }
        // COB_CODE: IF  WS-DATA-GG LESS    '01'
        //           OR  WS-DATA-GG GREATER '31'
        //               GO TO BA-999-EXIT
        //           END-IF
        if (ws.getWsData().getDataGgFormatted().compareTo("01") < 0 || ws.getWsData().getDataGgFormatted().compareTo("31") > 0) {
            // COB_CODE: MOVE '80' TO WS-RCODE
            ws.setWsRcodeFormatted("80");
            // COB_CODE: GO TO BA-999-EXIT
            return;
        }
        // COB_CODE: PERFORM L-CONVERT-CONVDG
        lConvertConvdg();
        // COB_CODE: IF  WS-RCODE = ZERO
        //               MOVE WS-DATAP TO A2K-OUPROG
        //           END-IF.
        if (Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: MOVE WS-DATAP TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(TruncAbs.toInt(ws.getWsDatap(), 7));
        }
    }

    /**Original name: C-ADD-DELTA_FIRST_SENTENCES<br>
	 * <pre>--------------------
	 *     SOMMA DELTA</pre>*/
    private void cAddDelta() {
        // COB_CODE: IF  A2K-DELTA-X = SPACES
        //               GO TO C-999-EXIT
        //           END-IF
        if (Characters.EQ_SPACE.test(ioA2kLccc0003.getInput().getA2kDeltaXBytes())) {
            // COB_CODE: GO TO C-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-TDELTA = ' ' OR 'G'
        //               GO TO C-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kTdelta() == ' ' || ioA2kLccc0003.getInput().getA2kTdelta() == 'G') {
            // COB_CODE: PERFORM CA-ADD-GIORNI
            caAddGiorni();
            // COB_CODE: GO TO C-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-TDELTA NOT = 'M'
        //           AND A2K-TDELTA NOT = 'A'
        //               GO TO C-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kTdelta() != 'M' && ioA2kLccc0003.getInput().getA2kTdelta() != 'A') {
            // COB_CODE: MOVE '81' TO WS-RCODE
            ws.setWsRcodeFormatted("81");
            // COB_CODE: GO TO C-999-EXIT
            return;
        }
        // COB_CODE: PERFORM M-CONVERT-CONVGE
        mConvertConvge();
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO C-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO C-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-TDELTA = 'M'
        //               MOVE WS-DATA-MM3          TO WS-DATA-MM
        //           ELSE
        //               ADD A2K-DELTA TO WS-DATA-SSAA
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kTdelta() == 'M') {
            // COB_CODE: ADD A2K-DELTA WS-DATA-MM  GIVING WS-DATA-MM3
            ws.setWsDataMm3(Trunc.toShort(ioA2kLccc0003.getInput().getA2kDelta() + ws.getWsData().getDataMm(), 3));
            // COB_CODE: PERFORM UNTIL WS-DATA-MM3 NOT GREATER 12
            //               ADD 1 TO WS-DATA-SSAA
            //           END-PERFORM
            while (!(ws.getWsDataMm3() <= 12)) {
                // COB_CODE: SUBTRACT 12 FROM WS-DATA-MM3
                ws.setWsDataMm3(Trunc.toShort(abs(ws.getWsDataMm3() - 12), 3));
                // COB_CODE: ADD 1 TO WS-DATA-SSAA
                ws.getWsData().setDataSsaa(Trunc.toShort(1 + ws.getWsData().getDataSsaa(), 4));
            }
            // COB_CODE: MOVE WS-DATA-MM3          TO WS-DATA-MM
            ws.getWsData().setDataMmFormatted(ws.getWsDataMm3Formatted());
        }
        else {
            // COB_CODE: ADD A2K-DELTA TO WS-DATA-SSAA
            ws.getWsData().setDataSsaa(Trunc.toShort(ioA2kLccc0003.getInput().getA2kDelta() + ws.getWsData().getDataSsaa(), 4));
        }
        // COB_CODE: MOVE WS-DATA-SSAA TO WS-DATAP
        ws.setWsDatap(ws.getWsData().getDataSsaa());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: MOVE WS-DATA-MM TO I2
        ws.setI2(ws.getWsData().getDataMm());
        // COB_CODE: IF  WS-DATA-GG GREATER WS-MESE (I2)
        //               MOVE WS-MESE (I2) TO WS-DATA-GG
        //           END-IF.
        if (ws.getWsData().getDataGg() > ws.getWsMese(ws.getI2())) {
            // COB_CODE: MOVE WS-MESE (I2) TO WS-DATA-GG
            ws.getWsData().setDataGgFormatted(ws.getWsMeseFormatted(ws.getI2()));
        }
        // COB_CODE: MOVE WS-DATA TO A2K-OUGMA
        ioA2kLccc0003.getOutput().getA2kOugmaX().setA2kOugmaFormatted(ws.getWsData().getWsDataFormatted());
        // COB_CODE: PERFORM L-CONVERT-CONVDG
        lConvertConvdg();
        // COB_CODE: IF  WS-RCODE = ZERO
        //               MOVE WS-DATAP TO A2K-OUPROG
        //           END-IF.
        if (Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: MOVE WS-DATAP TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(TruncAbs.toInt(ws.getWsDatap(), 7));
        }
    }

    /**Original name: CA-ADD-GIORNI_FIRST_SENTENCES<br>
	 * <pre>----------------------
	 * >>  INIZIA DAL PRIMO GIORNO LAVORATIVO</pre>*/
    private void caAddGiorni() {
        // COB_CODE: IF  A2K-INICON = '1'
        //               END-PERFORM
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kInicon() == '1') {
            // COB_CODE: ADD 1 TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(1 + ioA2kLccc0003.getOutput().getA2kOuprog(), 7));
            // COB_CODE: PERFORM P-CHECK-VEDIFEST
            pCheckVedifest();
            // COB_CODE: PERFORM UNTIL A2K-OUTG06 NOT = 'F'
            //               PERFORM P-CHECK-VEDIFEST
            //           END-PERFORM
            while (!(ioA2kLccc0003.getOutput().getA2kOutg06() != 'F')) {
                // COB_CODE: ADD 1 TO A2K-OUPROG
                ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(1 + ioA2kLccc0003.getOutput().getA2kOuprog(), 7));
                // COB_CODE: PERFORM P-CHECK-VEDIFEST
                pCheckVedifest();
            }
        }
        //>>  SOMMA GIORNI FISSI
        // COB_CODE: IF  A2K-FISLAV = ' ' OR '0'
        //               GO TO CA-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kFislav() == ' ' || ioA2kLccc0003.getInput().getA2kFislav() == '0') {
            // COB_CODE: ADD A2K-DELTA TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(ioA2kLccc0003.getInput().getA2kDelta() + ioA2kLccc0003.getOutput().getA2kOuprog(), 7));
            // COB_CODE: GO TO CA-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-FISLAV NOT = '1'
        //               GO TO CA-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kFislav() != '1') {
            // COB_CODE: MOVE '83' TO WS-RCODE
            ws.setWsRcodeFormatted("83");
            // COB_CODE: GO TO CA-999-EXIT
            return;
        }
        //>>  GIORNI LAVORATIVI
        // COB_CODE: MOVE A2K-DELTA TO WS-DELTA
        ws.setWsDeltaFormatted(ioA2kLccc0003.getInput().getA2kDeltaFormatted());
        // COB_CODE: PERFORM UNTIL WS-DELTA = ZERO
        //               SUBTRACT 1 FROM WS-DELTA
        //           END-PERFORM.
        while (!Characters.EQ_ZERO.test(ws.getWsDeltaFormatted())) {
            // COB_CODE: ADD 1 TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(1 + ioA2kLccc0003.getOutput().getA2kOuprog(), 7));
            // COB_CODE: PERFORM P-CHECK-VEDIFEST
            pCheckVedifest();
            // COB_CODE: PERFORM UNTIL A2K-OUTG06 NOT = 'F'
            //               PERFORM P-CHECK-VEDIFEST
            //           END-PERFORM
            while (!(ioA2kLccc0003.getOutput().getA2kOutg06() != 'F')) {
                // COB_CODE: ADD 1 TO A2K-OUPROG
                ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(1 + ioA2kLccc0003.getOutput().getA2kOuprog(), 7));
                // COB_CODE: PERFORM P-CHECK-VEDIFEST
                pCheckVedifest();
            }
            // COB_CODE: SUBTRACT 1 FROM WS-DELTA
            ws.setWsDelta(Trunc.toShort(abs(ws.getWsDelta() - 1), 3));
        }
    }

    /**Original name: D-SUBTRACT-DELTA_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 *     SOTTRAE DELTA</pre>*/
    private void dSubtractDelta() {
        // COB_CODE: IF  A2K-DELTA-X = SPACES
        //               GO TO D-999-EXIT
        //           END-IF
        if (Characters.EQ_SPACE.test(ioA2kLccc0003.getInput().getA2kDeltaXBytes())) {
            // COB_CODE: GO TO D-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-TDELTA = ' ' OR 'G'
        //               GO TO D-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kTdelta() == ' ' || ioA2kLccc0003.getInput().getA2kTdelta() == 'G') {
            // COB_CODE: PERFORM DA-SUBTRACT-GIORNI
            daSubtractGiorni();
            // COB_CODE: GO TO D-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-TDELTA NOT = 'M'
        //           AND A2K-TDELTA NOT = 'A'
        //               GO TO D-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kTdelta() != 'M' && ioA2kLccc0003.getInput().getA2kTdelta() != 'A') {
            // COB_CODE: MOVE '81' TO WS-RCODE
            ws.setWsRcodeFormatted("81");
            // COB_CODE: GO TO D-999-EXIT
            return;
        }
        // COB_CODE: PERFORM M-CONVERT-CONVGE
        mConvertConvge();
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO D-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO D-999-EXIT
            return;
        }
        // COB_CODE: MOVE A2K-DELTA TO WS-DATA-WK
        ws.setWsDataWk(ioA2kLccc0003.getInput().getA2kDelta());
        // COB_CODE: IF  A2K-TDELTA = 'M'
        //               SUBTRACT WS-DATA-WK FROM WS-DATA-MM
        //           ELSE
        //               SUBTRACT A2K-DELTA FROM WS-DATA-SSAA
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kTdelta() == 'M') {
            // COB_CODE: PERFORM UNTIL WS-DATA-MM GREATER WS-DATA-WK
            //               SUBTRACT 1 FROM WS-DATA-SSAA
            //           END-PERFORM
            while (!(ws.getWsData().getDataMm() > ws.getWsDataWk())) {
                // COB_CODE: SUBTRACT WS-DATA-MM FROM WS-DATA-WK
                ws.setWsDataWk(Trunc.toInt(ws.getWsDataWk() - ws.getWsData().getDataMm(), 7));
                // COB_CODE: MOVE 12 TO WS-DATA-MM
                ws.getWsData().setDataMm(((short)12));
                // COB_CODE: SUBTRACT 1 FROM WS-DATA-SSAA
                ws.getWsData().setDataSsaa(Trunc.toShort(abs(ws.getWsData().getDataSsaa() - 1), 4));
            }
            // COB_CODE: SUBTRACT WS-DATA-WK FROM WS-DATA-MM
            ws.getWsData().setDataMm(Trunc.toShort(abs(ws.getWsData().getDataMm() - ws.getWsDataWk()), 2));
        }
        else {
            // COB_CODE: SUBTRACT A2K-DELTA FROM WS-DATA-SSAA
            ws.getWsData().setDataSsaa(Trunc.toShort(abs(ws.getWsData().getDataSsaa() - ioA2kLccc0003.getInput().getA2kDelta()), 4));
        }
        // COB_CODE: MOVE WS-DATA-SSAA TO WS-DATAP
        ws.setWsDatap(ws.getWsData().getDataSsaa());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: MOVE WS-DATA-MM TO I2
        ws.setI2(ws.getWsData().getDataMm());
        // COB_CODE: IF  WS-DATA-GG GREATER WS-MESE (I2)
        //               MOVE WS-MESE (I2) TO WS-DATA-GG
        //           END-IF.
        if (ws.getWsData().getDataGg() > ws.getWsMese(ws.getI2())) {
            // COB_CODE: MOVE WS-MESE (I2) TO WS-DATA-GG
            ws.getWsData().setDataGgFormatted(ws.getWsMeseFormatted(ws.getI2()));
        }
        // COB_CODE: MOVE WS-DATA TO A2K-OUGMA
        ioA2kLccc0003.getOutput().getA2kOugmaX().setA2kOugmaFormatted(ws.getWsData().getWsDataFormatted());
        // COB_CODE: PERFORM L-CONVERT-CONVDG
        lConvertConvdg();
        // COB_CODE: IF  WS-RCODE = ZERO
        //               MOVE WS-DATAP TO A2K-OUPROG
        //           END-IF.
        if (Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: MOVE WS-DATAP TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(TruncAbs.toInt(ws.getWsDatap(), 7));
        }
    }

    /**Original name: DA-SUBTRACT-GIORNI_FIRST_SENTENCES<br>
	 * <pre>---------------------------
	 * >>  SE FESTIVO LAVORATIVO PRECEDENTE</pre>*/
    private void daSubtractGiorni() {
        // COB_CODE: IF  A2K-INICON = '2'
        //               GO TO DA-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kInicon() == '2') {
            // COB_CODE: PERFORM P-CHECK-VEDIFEST
            pCheckVedifest();
            // COB_CODE: PERFORM UNTIL A2K-OUTG06 NOT = 'F'
            //               PERFORM P-CHECK-VEDIFEST
            //           END-PERFORM
            while (!(ioA2kLccc0003.getOutput().getA2kOutg06() != 'F')) {
                // COB_CODE: SUBTRACT 1 FROM A2K-OUPROG
                ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - 1), 7));
                // COB_CODE: PERFORM P-CHECK-VEDIFEST
                pCheckVedifest();
            }
            // COB_CODE: GO TO DA-999-EXIT
            return;
        }
        //>>  INIZIA DAL PRIMO GIORNO LAVORATIVO
        // COB_CODE: IF  A2K-INICON = '1'
        //               END-PERFORM
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kInicon() == '1') {
            // COB_CODE: SUBTRACT 1 FROM A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - 1), 7));
            // COB_CODE: PERFORM P-CHECK-VEDIFEST
            pCheckVedifest();
            // COB_CODE: PERFORM UNTIL A2K-OUTG06 NOT = 'F'
            //               PERFORM P-CHECK-VEDIFEST
            //           END-PERFORM
            while (!(ioA2kLccc0003.getOutput().getA2kOutg06() != 'F')) {
                // COB_CODE: SUBTRACT 1 FROM A2K-OUPROG
                ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - 1), 7));
                // COB_CODE: PERFORM P-CHECK-VEDIFEST
                pCheckVedifest();
            }
        }
        //>>  SOTTRAE GIORNI FISSI
        // COB_CODE: IF  A2K-FISLAV = ' ' OR '0'
        //               GO TO DA-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kFislav() == ' ' || ioA2kLccc0003.getInput().getA2kFislav() == '0') {
            // COB_CODE: SUBTRACT A2K-DELTA FROM A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - ioA2kLccc0003.getInput().getA2kDelta()), 7));
            // COB_CODE: GO TO DA-999-EXIT
            return;
        }
        // COB_CODE: IF  A2K-FISLAV NOT = '1'
        //               GO TO DA-999-EXIT
        //           END-IF
        if (ioA2kLccc0003.getInput().getA2kFislav() != '1') {
            // COB_CODE: MOVE '83' TO WS-RCODE
            ws.setWsRcodeFormatted("83");
            // COB_CODE: GO TO DA-999-EXIT
            return;
        }
        //>>  GIORNI LABORATIVI
        // COB_CODE: MOVE A2K-DELTA TO WS-DELTA
        ws.setWsDeltaFormatted(ioA2kLccc0003.getInput().getA2kDeltaFormatted());
        // COB_CODE: PERFORM UNTIL WS-DELTA = ZERO
        //               SUBTRACT 1 FROM WS-DELTA
        //           END-PERFORM.
        while (!Characters.EQ_ZERO.test(ws.getWsDeltaFormatted())) {
            // COB_CODE: SUBTRACT 1 FROM A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - 1), 7));
            // COB_CODE: PERFORM P-CHECK-VEDIFEST
            pCheckVedifest();
            // COB_CODE: PERFORM UNTIL A2K-OUTG06 NOT = 'F'
            //               PERFORM P-CHECK-VEDIFEST
            //           END-PERFORM
            while (!(ioA2kLccc0003.getOutput().getA2kOutg06() != 'F')) {
                // COB_CODE: SUBTRACT 1 FROM A2K-OUPROG
                ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - 1), 7));
                // COB_CODE: PERFORM P-CHECK-VEDIFEST
                pCheckVedifest();
            }
            // COB_CODE: SUBTRACT 1 FROM WS-DELTA
            ws.setWsDelta(Trunc.toShort(abs(ws.getWsDelta() - 1), 3));
        }
    }

    /**Original name: E-PROCESS-CONVERSIONI_FIRST_SENTENCES<br>
	 * <pre>------------------------------
	 * >>  CONTROLLO SE FESTA</pre>*/
    private void eProcessConversioni() {
        // COB_CODE: IF  A2K-FUNZ = '06'
        //               PERFORM EA-PROCESS-FUNPASQ
        //           ELSE
        //               END-IF
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kFunz(), "06")) {
            // COB_CODE: PERFORM EA-PROCESS-FUNPASQ
            eaProcessFunpasq();
        }
        else {
            // COB_CODE: PERFORM P-CHECK-VEDIFEST
            pCheckVedifest();
            // COB_CODE: IF  A2K-FUNZ = '04'
            //               GO TO E-999-EXIT
            //           END-IF
            if (Conditions.eq(ioA2kLccc0003.getInput().getA2kFunz(), "04")) {
                // COB_CODE: GO TO E-999-EXIT
                return;
            }
        }
        //>>  PROGRESSIVO DA 1/1/1900 =
        //>>  PROGRESSIVO CORRENTE - PROGRESSIVO AL 31/12/1899
        // COB_CODE: MOVE A2K-OUPROG TO WS-DATAP
        ws.setWsDatap(ioA2kLccc0003.getOutput().getA2kOuprog());
        // COB_CODE: SUBTRACT WA-INIPROG FROM WS-DATAP
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() - ws.getWaIniprog(), 7));
        // COB_CODE: MOVE WS-DATAP TO A2K-OUPROG9
        ioA2kLccc0003.getOutput().setA2kOuprog9(TruncAbs.toInt(ws.getWsDatap(), 5));
        //>>  IL GIORNO DELLA SETTIMANA
        // COB_CODE: PERFORM Q-CHECK-SABDOM
        qCheckSabdom();
        // COB_CODE: ADD 1 A2K-OUNG06 GIVING I2
        ws.setI2(((short)(1 + ioA2kLccc0003.getOutput().getA2kOung06())));
        // COB_CODE: MOVE WA-DGIORNO (I2) TO A2K-OUGG06
        ioA2kLccc0003.getOutput().setA2kOugg06(ws.getWaTabdgiorni().getDgiorno(ws.getI2()));
        //>>  CONVERTE DATA DA PROGRESSIVO
        // COB_CODE: PERFORM M-CONVERT-CONVGE
        mConvertConvge();
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO E-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO E-999-EXIT
            return;
        }
        // COB_CODE: MOVE WS-DATA TO A2K-OUGMA
        ioA2kLccc0003.getOutput().getA2kOugmaX().setA2kOugmaFormatted(ws.getWsData().getWsDataFormatted());
        // COB_CODE: MOVE WS-DATA-GG TO A2K-OUGG02
        ioA2kLccc0003.getOutput().getA2kOugbmba().setOugg02Formatted(ws.getWsData().getDataGgFormatted());
        // COB_CODE: MOVE WS-DATA-MM TO A2K-OUMM02
        ioA2kLccc0003.getOutput().getA2kOugbmba().setOumm02Formatted(ws.getWsData().getDataMmFormatted());
        // COB_CODE: MOVE WS-DATA-SS TO A2K-OUSS02
        ioA2kLccc0003.getOutput().getA2kOugbmba().setOuss02Formatted(ws.getWsData().getDataSsFormatted());
        // COB_CODE: MOVE WS-DATA-AA TO A2K-OUAA02
        ioA2kLccc0003.getOutput().getA2kOugbmba().setOuaa02Formatted(ws.getWsData().getDataAaFormatted());
        // COB_CODE: MOVE '/'        TO A2K-OUS102
        //                              A2K-OUS202
        ioA2kLccc0003.getOutput().getA2kOugbmba().setOus102Formatted("/");
        ioA2kLccc0003.getOutput().getA2kOugbmba().setOus202Formatted("/");
        // COB_CODE: MOVE WS-DATA-GG TO A2K-OUGG03
        ioA2kLccc0003.getOutput().getA2kOuamgX().setOugg03Formatted(ws.getWsData().getDataGgFormatted());
        // COB_CODE: MOVE WS-DATA-MM TO A2K-OUMM03
        ioA2kLccc0003.getOutput().getA2kOuamgX().setOumm03Formatted(ws.getWsData().getDataMmFormatted());
        // COB_CODE: MOVE WS-DATA-SS TO A2K-OUSS03
        ioA2kLccc0003.getOutput().getA2kOuamgX().setOuss03Formatted(ws.getWsData().getDataSsFormatted());
        // COB_CODE: MOVE WS-DATA-AA TO A2K-OUAA03
        ioA2kLccc0003.getOutput().getA2kOuamgX().setOuaa03Formatted(ws.getWsData().getDataAaFormatted());
        // COB_CODE: MOVE A2K-OUAMG TO A2K-OUAMGP
        ioA2kLccc0003.getOutput().setA2kOuamgp(ioA2kLccc0003.getOutput().getA2kOuamgX().getA2kOuamg());
        // COB_CODE: MULTIPLY A2K-OUAMG BY 10 GIVING A2K-OUAMG0P
        ioA2kLccc0003.getOutput().setA2kOuamg0p(Trunc.toInt(ioA2kLccc0003.getOutput().getA2kOuamgX().getA2kOuamg() * 10, 9));
        // COB_CODE: MOVE WS-DATA-SS TO A2K-OUJSS
        ioA2kLccc0003.getOutput().getA2kOujulX().setOujssFormatted(ws.getWsData().getDataSsFormatted());
        // COB_CODE: MOVE WS-DATA-AA TO A2K-OUJAA
        ioA2kLccc0003.getOutput().getA2kOujulX().setOujaaFormatted(ws.getWsData().getDataAaFormatted());
        //>>  CALCOLO PROGRESSIVO AL 31/12/DATAP
        // COB_CODE: MOVE WS-DATA-SSAA TO WS-DATAP
        ws.setWsDatap(ws.getWsData().getDataSsaa());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: PERFORM O-COMPUTE-CAL3112
        oComputeCal3112();
        // COB_CODE: MOVE A2K-OUPROG TO WS-DATA-WK
        ws.setWsDataWk(ioA2kLccc0003.getOutput().getA2kOuprog());
        // COB_CODE: SUBTRACT WS-DATAP FROM WS-DATA-WK
        ws.setWsDataWk(Trunc.toInt(ws.getWsDataWk() - ws.getWsDatap(), 7));
        // COB_CODE: MOVE WS-DATA-WK TO A2K-OUFA07
        ioA2kLccc0003.getOutput().getA2kOugl07X().setA2kOufa07(TruncAbs.toShort(ws.getWsDataWk(), 3));
        //>>  PROGRESSIVO GIULIANO NELL'ANNO
        // COB_CODE: IF  WS-MESE (2) = 28
        //               ADD 365 TO WS-DATA-WK
        //           ELSE
        //               ADD 366 TO WS-DATA-WK
        //           END-IF
        if (ws.getWsMese(2) == 28) {
            // COB_CODE: ADD 365 TO WS-DATA-WK
            ws.setWsDataWk(Trunc.toInt(365 + ws.getWsDataWk(), 7));
        }
        else {
            // COB_CODE: ADD 366 TO WS-DATA-WK
            ws.setWsDataWk(Trunc.toInt(366 + ws.getWsDataWk(), 7));
        }
        // COB_CODE: MOVE WS-DATA-WK TO A2K-OUJGGG
        ioA2kLccc0003.getOutput().getA2kOujulX().setOujggg(TruncAbs.toShort(ws.getWsDataWk(), 3));
        //>>  NR. GIORNI DEL MESE
        // COB_CODE: MOVE A2K-OUMM TO I2
        ws.setI2(ioA2kLccc0003.getOutput().getA2kOugmaX().getOumm());
        // COB_CODE: MOVE WS-MESE (I2) TO A2K-OUGG07
        ioA2kLccc0003.getOutput().setA2kOugg07Formatted(ws.getWsMeseFormatted(ws.getI2()));
        //>>  DESCRIZIONE MESE
        // COB_CODE: MOVE WS-DATA-GG    TO A2K-OUGG04
        ioA2kLccc0003.getOutput().getA2kOusta().setOugg04Formatted(ws.getWsData().getDataGgFormatted());
        // COB_CODE: MOVE WA-DMESE (I2) TO A2K-OUMM04
        ioA2kLccc0003.getOutput().getA2kOusta().setOumm04(ws.getWaTabdmesi().getDmese(ws.getI2()));
        // COB_CODE: MOVE WS-DATA-SS    TO A2K-OUSS04
        ioA2kLccc0003.getOutput().getA2kOusta().setOuss04Formatted(ws.getWsData().getDataSsFormatted());
        // COB_CODE: MOVE WS-DATA-AA    TO A2K-OUAA04
        ioA2kLccc0003.getOutput().getA2kOusta().setOuaa04Formatted(ws.getWsData().getDataAaFormatted());
        // COB_CODE: MOVE WS-DATA-GG    TO A2K-OUGG05
        ioA2kLccc0003.getOutput().getA2kOurid().setOugg05Formatted(ws.getWsData().getDataGgFormatted());
        // COB_CODE: MOVE WA-DMESE (I2) TO A2K-OUMM05
        ioA2kLccc0003.getOutput().getA2kOurid().setOumm05(ws.getWaTabdmesi().getDmese(ws.getI2()));
        // COB_CODE: MOVE WS-DATA-AA    TO A2K-OUAA05
        ioA2kLccc0003.getOutput().getA2kOurid().setOuaa05Formatted(ws.getWsData().getDataAaFormatted());
        //>>  TIPO GIORNO
        // COB_CODE: IF  WS-DATA-GG = A2K-OUGG07
        //               MOVE 'M' TO A2K-OUGG09
        //           END-IF
        if (ws.getWsData().getDataGg() == ioA2kLccc0003.getOutput().getA2kOugg07()) {
            // COB_CODE: MOVE 'M' TO A2K-OUGG09
            ioA2kLccc0003.getOutput().setA2kOugg09Formatted("M");
        }
        // COB_CODE: IF  WS-DATA-GG = 15
        //               MOVE 'Q' TO A2K-OUGG09
        //           END-IF
        if (ws.getWsData().getDataGg() == 15) {
            // COB_CODE: MOVE 'Q' TO A2K-OUGG09
            ioA2kLccc0003.getOutput().setA2kOugg09Formatted("Q");
        }
        // COB_CODE: IF  WS-DATA-GG = 10 OR 20
        //               MOVE 'D' TO A2K-OUGG09
        //           END-IF
        if (ws.getWsData().getDataGg() == 10 || ws.getWsData().getDataGg() == 20) {
            // COB_CODE: MOVE 'D' TO A2K-OUGG09
            ioA2kLccc0003.getOutput().setA2kOugg09Formatted("D");
        }
        //>>  NR. TRIMESTRE
        // COB_CODE: COMPUTE WS-DATA-WK = (WS-DATA-MM - 1) / 3 + 1
        ws.setWsDataWk(((new AfDecimal(((((double)(ws.getWsData().getDataMm() - 1))) / 3), 3, 0)).add(1)).toInt());
        // COB_CODE: MOVE WS-DATA-WK TO A2K-OUGG08
        ioA2kLccc0003.getOutput().setA2kOugg08(TruncAbs.toShort(ws.getWsDataWk(), 1));
        // COB_CODE: IF  A2K-FUNZ = '07'
        //               PERFORM R-COMPUTE-RLAVOM
        //           END-IF
        if (Conditions.eq(ioA2kLccc0003.getInput().getA2kFunz(), "07")) {
            // COB_CODE: PERFORM R-COMPUTE-RLAVOM
            rComputeRlavom();
        }
        //>>  CONV. PROGRESSIVO COMMERCIALE
        // COB_CODE: COMPUTE A2K-OUPROGC = (WS-DATA-MM - 1) * 30
        ioA2kLccc0003.getOutput().setA2kOuprogc(abs((ws.getWsData().getDataMm() - 1) * 30));
        // COB_CODE: COMPUTE WS-DATA-WK  = (WS-DATA-SSAA - 1901) * 360
        ws.setWsDataWk(Trunc.toInt((ws.getWsData().getDataSsaa() - 1901) * 360, 7));
        // COB_CODE: COMPUTE A2K-OUPROGC = A2K-OUPROGC + WS-DATA-WK + WS-DATA-GG
        ioA2kLccc0003.getOutput().setA2kOuprogc(Trunc.toInt(ioA2kLccc0003.getOutput().getA2kOuprogc() + ws.getWsDataWk() + ws.getWsData().getDataGg(), 5));
        // COB_CODE: IF  WS-DATA-GG NOT LESS 28
        //               END-IF
        //           END-IF
        if (ws.getWsData().getDataGg() >= 28) {
            // COB_CODE: IF  WS-DATA-MM = 2
            //           OR  WS-DATA-GG NOT LESS 30
            //               COMPUTE A2K-OUPROGC = A2K-OUPROGC - WS-DATA-GG + 30
            //           END-IF
            if (ws.getWsData().getDataMm() == 2 || ws.getWsData().getDataGg() >= 30) {
                // COB_CODE: COMPUTE A2K-OUPROGC = A2K-OUPROGC - WS-DATA-GG + 30
                ioA2kLccc0003.getOutput().setA2kOuprogc(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprogc() - ws.getWsData().getDataGg() + 30), 5));
            }
        }
        // COB_CODE: COMPUTE A2K-OUPROCO = (WS-DATA-MM - 1) * 30
        ioA2kLccc0003.getOutput().setA2kOuproco(abs((ws.getWsData().getDataMm() - 1) * 30));
        // COB_CODE: COMPUTE WS-DATA-WK  = (WS-DATA-SSAA - 1) * 360
        ws.setWsDataWk(Trunc.toInt((ws.getWsData().getDataSsaa() - 1) * 360, 7));
        // COB_CODE: COMPUTE A2K-OUPROCO = A2K-OUPROCO + WS-DATA-WK + WS-DATA-GG
        ioA2kLccc0003.getOutput().setA2kOuproco(Trunc.toInt(ioA2kLccc0003.getOutput().getA2kOuproco() + ws.getWsDataWk() + ws.getWsData().getDataGg(), 7));
        // COB_CODE: IF  WS-DATA-GG NOT LESS 28
        //               END-IF
        //           END-IF.
        if (ws.getWsData().getDataGg() >= 28) {
            // COB_CODE: IF  WS-DATA-MM = 2
            //           OR  WS-DATA-GG NOT LESS 30
            //               COMPUTE A2K-OUPROCO = A2K-OUPROCO - WS-DATA-GG + 30
            //           END-IF
            if (ws.getWsData().getDataMm() == 2 || ws.getWsData().getDataGg() >= 30) {
                // COB_CODE: COMPUTE A2K-OUPROCO = A2K-OUPROCO - WS-DATA-GG + 30
                ioA2kLccc0003.getOutput().setA2kOuproco(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuproco() - ws.getWsData().getDataGg() + 30), 7));
            }
        }
    }

    /**Original name: EA-PROCESS-FUNPASQ_FIRST_SENTENCES<br>
	 * <pre>---------------------------
	 * ----------------------------------------------------------------
	 *   RICHIESTA CALCOLO PASQUA
	 * ----------------------------------------------------------------</pre>*/
    private void eaProcessFunpasq() {
        // COB_CODE: MOVE 'N' TO A2K-OUTG06
        ioA2kLccc0003.getOutput().setA2kOutg06Formatted("N");
        // COB_CODE: MOVE WS-DATA TO A2K-OUGMA
        ioA2kLccc0003.getOutput().getA2kOugmaX().setA2kOugmaFormatted(ws.getWsData().getWsDataFormatted());
        //>>  CONV. PROGRESSIVO A DATA
        // COB_CODE: PERFORM M-CONVERT-CONVGE
        mConvertConvge();
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO EA-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO EA-999-EXIT
            return;
        }
        //>>  CALCOLO DI PASQUA
        // COB_CODE: PERFORM S-CHECK-TESTPASQ
        sCheckTestpasq();
        // COB_CODE: MOVE A2K-OUGMA TO WS-DATA
        ws.getWsData().setWsDataFormatted(ioA2kLccc0003.getOutput().getA2kOugmaX().getA2kOugmaFormatted());
        // COB_CODE: COMPUTE A2K-OUPROG = WS-DATAP - 1
        ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ws.getWsDatap() - 1), 7));
        // COB_CODE: MOVE 'F' TO A2K-OUTG06
        ioA2kLccc0003.getOutput().setA2kOutg06Formatted("F");
        // COB_CODE: MOVE 'P' TO A2K-OUGG10.
        ioA2kLccc0003.getOutput().setA2kOugg10Formatted("P");
    }

    /**Original name: L-CONVERT-CONVDG_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 * ----------------------------------------------------------------
	 *   CONVERSIONE DA GGMMAAAA A PROGRESSIVO
	 * ----------------------------------------------------------------</pre>*/
    private void lConvertConvdg() {
        // COB_CODE: MOVE WS-DATA-SSAA TO WS-DATAP
        ws.setWsDatap(ws.getWsData().getDataSsaa());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: SUBTRACT 1 FROM WS-DATAP
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() - 1, 7));
        // COB_CODE: PERFORM O-COMPUTE-CAL3112
        oComputeCal3112();
        // COB_CODE: SUBTRACT 1 FROM WS-DATA-MM GIVING WS-MESI-WK
        ws.setWsMesiWk(Trunc.toShort(abs(ws.getWsData().getDataMm() - 1), 2));
        // COB_CODE: MOVE 1 TO I2
        ws.setI2(((short)1));
        // COB_CODE: IF  WS-MESI-WK NOT = ZERO
        //               END-PERFORM
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsMesiWkFormatted())) {
            // COB_CODE: PERFORM UNTIL I2 GREATER WS-MESI-WK
            //               ADD 1 TO I2
            //           END-PERFORM
            while (!(ws.getI2() > ws.getWsMesiWk())) {
                // COB_CODE: ADD WS-MESE (I2) TO WS-DATAP
                ws.setWsDatap(Trunc.toInt(ws.getWsMese(ws.getI2()) + ws.getWsDatap(), 7));
                // COB_CODE: ADD 1 TO I2
                ws.setI2(Trunc.toShort(1 + ws.getI2(), 2));
            }
        }
        // COB_CODE: IF  WS-DATA-GG GREATER WS-MESE (I2)
        //               MOVE '80' TO WS-RCODE
        //           ELSE
        //               ADD WS-DATA-GG TO WS-DATAP
        //           END-IF.
        if (ws.getWsData().getDataGg() > ws.getWsMese(ws.getI2())) {
            // COB_CODE: MOVE '80' TO WS-RCODE
            ws.setWsRcodeFormatted("80");
        }
        else {
            // COB_CODE: ADD WS-DATA-GG TO WS-DATAP
            ws.setWsDatap(Trunc.toInt(ws.getWsData().getDataGg() + ws.getWsDatap(), 7));
        }
    }

    /**Original name: M-CONVERT-CONVGE_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 * ----------------------------------------------------------------
	 *   CONVERSIONE DA PROGRESSIVO A GGMMAAAA
	 * ----------------------------------------------------------------</pre>*/
    private void mConvertConvge() {
        // COB_CODE: MOVE WA-TABMESI TO WS-MESI
        ws.setWsMesiFormatted(ws.getWaTabmesiFormatted());
        // COB_CODE: DIVIDE WA-GG-IN-400-ANNI INTO A2K-OUPROG
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ioA2kLccc0003.getOutput().getA2kOuprog() / ws.getWaGgIn400Anni());
        ws.setWsRemainder(Trunc.toInt(ioA2kLccc0003.getOutput().getA2kOuprog() % ws.getWaGgIn400Anni(), 7));
        // COB_CODE: COMPUTE WS-DATAP = WS-QUOTIENT * 400
        ws.setWsDatap(Trunc.toInt(ws.getWsQuotient() * 400, 7));
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO M-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-DATAP TO WS-DATA
            ws.getWsData().setWsData(TruncAbs.toInt(ws.getWsDatap(), 8));
            // COB_CODE: MOVE 31 TO WS-DATA-GG
            ws.getWsData().setDataGg(((short)31));
            // COB_CODE: MOVE 12 TO WS-DATA-MM
            ws.getWsData().setDataMm(((short)12));
            // COB_CODE: GO TO M-999-EXIT
            return;
        }
        // COB_CODE: DIVIDE WA-GG-IN-100-ANNI INTO WS-REMAINDER
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsRemainder() / ws.getWaGgIn100Anni());
        ws.setWsRemainder(Trunc.toInt(ws.getWsRemainder() % ws.getWaGgIn100Anni(), 7));
        // COB_CODE: COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * 100
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() + ws.getWsQuotient() * 100, 7));
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO M-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-DATAP TO WS-DATA
            ws.getWsData().setWsData(TruncAbs.toInt(ws.getWsDatap(), 8));
            // COB_CODE: PERFORM N-CHECK-BISESTIL
            nCheckBisestil();
            // COB_CODE: IF  WS-MESE (2) = 28
            //               MOVE 12 TO WS-DATA-MM
            //           ELSE
            //               MOVE 12 TO WS-DATA-MM
            //           END-IF
            if (ws.getWsMese(2) == 28) {
                // COB_CODE: MOVE 31 TO WS-DATA-GG
                ws.getWsData().setDataGg(((short)31));
                // COB_CODE: MOVE 12 TO WS-DATA-MM
                ws.getWsData().setDataMm(((short)12));
            }
            else {
                // COB_CODE: MOVE 30 TO WS-DATA-GG
                ws.getWsData().setDataGg(((short)30));
                // COB_CODE: MOVE 12 TO WS-DATA-MM
                ws.getWsData().setDataMm(((short)12));
            }
            // COB_CODE: GO TO M-999-EXIT
            return;
        }
        // COB_CODE: DIVIDE WA-GG-IN-4-ANNI INTO WS-REMAINDER
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsRemainder() / ws.getWaGgIn4Anni());
        ws.setWsRemainder(Trunc.toInt(ws.getWsRemainder() % ws.getWaGgIn4Anni(), 7));
        // COB_CODE: COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * 4
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() + ws.getWsQuotient() * 4, 7));
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO M-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-DATAP TO WS-DATA
            ws.getWsData().setWsData(TruncAbs.toInt(ws.getWsDatap(), 8));
            // COB_CODE: MOVE 31 TO WS-DATA-GG
            ws.getWsData().setDataGg(((short)31));
            // COB_CODE: MOVE 12 TO WS-DATA-MM
            ws.getWsData().setDataMm(((short)12));
            // COB_CODE: GO TO M-999-EXIT
            return;
        }
        // COB_CODE: DIVIDE 365 INTO WS-REMAINDER
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsRemainder() / 365);
        ws.setWsRemainder(ws.getWsRemainder() % 365);
        // COB_CODE: COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() + ws.getWsQuotient(), 7));
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO M-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE WS-DATAP TO WS-DATA
            ws.getWsData().setWsData(TruncAbs.toInt(ws.getWsDatap(), 8));
            // COB_CODE: PERFORM N-CHECK-BISESTIL
            nCheckBisestil();
            // COB_CODE: IF  WS-MESE (2) = 28
            //               MOVE 12 TO WS-DATA-MM
            //           ELSE
            //               MOVE 12 TO WS-DATA-MM
            //           END-IF
            if (ws.getWsMese(2) == 28) {
                // COB_CODE: MOVE 31 TO WS-DATA-GG
                ws.getWsData().setDataGg(((short)31));
                // COB_CODE: MOVE 12 TO WS-DATA-MM
                ws.getWsData().setDataMm(((short)12));
            }
            else {
                // COB_CODE: MOVE 30 TO WS-DATA-GG
                ws.getWsData().setDataGg(((short)30));
                // COB_CODE: MOVE 12 TO WS-DATA-MM
                ws.getWsData().setDataMm(((short)12));
            }
            // COB_CODE: GO TO M-999-EXIT
            return;
        }
        // COB_CODE: ADD 1 TO WS-DATAP
        ws.setWsDatap(Trunc.toInt(1 + ws.getWsDatap(), 7));
        // COB_CODE: MOVE WS-DATAP TO WS-DATA
        ws.getWsData().setWsData(TruncAbs.toInt(ws.getWsDatap(), 8));
        // COB_CODE: MOVE WS-REMAINDER TO WS-ANY-NUM
        ws.setWsAnyNum(ws.getWsRemainder());
        // COB_CODE: PERFORM N-CHECK-BISESTIL
        nCheckBisestil();
        // COB_CODE: MOVE 1 TO WS-DATA-MM
        ws.getWsData().setDataMm(((short)1));
        // COB_CODE: MOVE 1 TO I2
        ws.setI2(((short)1));
        // COB_CODE: PERFORM UNTIL WS-ANY-NUM NOT GREATER WS-MESE (I2)
        //               ADD 1 TO I2
        //           END-PERFORM.
        while (!(ws.getWsAnyNum() <= ws.getWsMese(ws.getI2()))) {
            // COB_CODE: SUBTRACT WS-MESE (I2) FROM WS-ANY-NUM
            ws.setWsAnyNum(Trunc.toInt(ws.getWsAnyNum() - ws.getWsMese(ws.getI2()), 7));
            // COB_CODE: ADD 1 TO WS-DATA-MM
            ws.getWsData().setDataMm(Trunc.toShort(1 + ws.getWsData().getDataMm(), 2));
            // COB_CODE: ADD 1 TO I2
            ws.setI2(Trunc.toShort(1 + ws.getI2(), 2));
        }
        // COB_CODE: MOVE WS-ANY-NUM TO WS-DATA-GG.
        ws.getWsData().setDataGg(TruncAbs.toShort(ws.getWsAnyNum(), 2));
    }

    /**Original name: N-CHECK-BISESTIL_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 * ----------------------------------------------------------------
	 *   CONTROLLO ANNO BISESTILE
	 * ----------------------------------------------------------------</pre>*/
    private void nCheckBisestil() {
        // COB_CODE: MOVE WA-TABMESI TO WS-MESI
        ws.setWsMesiFormatted(ws.getWaTabmesiFormatted());
        // COB_CODE: DIVIDE 400 INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 400);
        ws.setWsRemainder(ws.getWsDatap() % 400);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO N-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE 29 TO WS-MESE (2)
            ws.setWsMese(2, ((short)29));
            // COB_CODE: GO TO N-999-EXIT
            return;
        }
        // COB_CODE: DIVIDE 100 INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 100);
        ws.setWsRemainder(ws.getWsDatap() % 100);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               GO TO N-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: GO TO N-999-EXIT
            return;
        }
        // COB_CODE: DIVIDE 4 INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 4);
        ws.setWsRemainder(ws.getWsDatap() % 4);
        // COB_CODE: IF  WS-REMAINDER = ZERO
        //               MOVE 29 TO WS-MESE (2)
        //           END-IF.
        if (ws.getWsRemainder() == 0) {
            // COB_CODE: MOVE 29 TO WS-MESE (2)
            ws.setWsMese(2, ((short)29));
        }
    }

    /**Original name: O-COMPUTE-CAL3112_FIRST_SENTENCES<br>
	 * <pre>--------------------------
	 * ----------------------------------------------------------------
	 *   CALCOLA PROGRESSIVO 31/12/ANNO DATAP
	 * ----------------------------------------------------------------</pre>*/
    private void oComputeCal3112() {
        // COB_CODE: DIVIDE 400 INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 400);
        ws.setWsRemainder(ws.getWsDatap() % 400);
        // COB_CODE: COMPUTE WS-DATAP = WS-QUOTIENT * WA-GG-IN-400-ANNI
        ws.setWsDatap(Trunc.toInt(ws.getWsQuotient() * ws.getWaGgIn400Anni(), 7));
        // COB_CODE: MOVE WS-REMAINDER TO WS-DATA-WK
        ws.setWsDataWk(ws.getWsRemainder());
        // COB_CODE: DIVIDE 100 INTO WS-DATA-WK
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDataWk() / 100);
        ws.setWsRemainder(ws.getWsDataWk() % 100);
        // COB_CODE: COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * WA-GG-IN-100-ANNI
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() + ws.getWsQuotient() * ws.getWaGgIn100Anni(), 7));
        // COB_CODE: MOVE WS-REMAINDER TO WS-DATA-WK
        ws.setWsDataWk(ws.getWsRemainder());
        // COB_CODE: DIVIDE 4   INTO WS-DATA-WK
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDataWk() / 4);
        ws.setWsRemainder(ws.getWsDataWk() % 4);
        // COB_CODE: COMPUTE WS-DATAP = WS-DATAP + WS-QUOTIENT * WA-GG-IN-4-ANNI
        //                            + WS-REMAINDER * 365.
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() + ws.getWsQuotient() * ws.getWaGgIn4Anni() + ws.getWsRemainder() * 365, 7));
    }

    /**Original name: P-CHECK-VEDIFEST_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 * ----------------------------------------------------------------
	 *   CONTROLLO FESTIVITA'   SABATO DOMENICA
	 * ----------------------------------------------------------------</pre>*/
    private void pCheckVedifest() {
        // COB_CODE: MOVE 'N' TO A2K-OUTG06
        ioA2kLccc0003.getOutput().setA2kOutg06Formatted("N");
        // COB_CODE: PERFORM M-CONVERT-CONVGE
        mConvertConvge();
        // COB_CODE: IF  WS-RCODE NOT = ZERO
        //               GO TO P-999-EXIT
        //           END-IF
        if (!Characters.EQ_ZERO.test(ws.getWsRcodeFormatted())) {
            // COB_CODE: GO TO P-999-EXIT
            return;
        }
        // COB_CODE: MOVE WS-DATA TO A2K-OUGMA
        ioA2kLccc0003.getOutput().getA2kOugmaX().setA2kOugmaFormatted(ws.getWsData().getWsDataFormatted());
        // COB_CODE: PERFORM Q-CHECK-SABDOM
        qCheckSabdom();
        //>>  SE FESTA FISSA
        // COB_CODE: PERFORM T-CHECK-TESTFFIS
        tCheckTestffis();
        //>>  SE DOMENICA DI PASQUA
        // COB_CODE: IF  A2K-OUTG06 = 'F'
        //           AND A2K-OUNG06 = 0
        //               SUBTRACT 1 FROM A2K-OUPROG
        //           END-IF
        if (ioA2kLccc0003.getOutput().getA2kOutg06() == 'F' && ioA2kLccc0003.getOutput().getA2kOung06() == 0) {
            // COB_CODE: ADD 1 TO A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(1 + ioA2kLccc0003.getOutput().getA2kOuprog(), 7));
            // COB_CODE: PERFORM S-CHECK-TESTPASQ
            sCheckTestpasq();
            // COB_CODE: MOVE A2K-OUGMA TO WS-DATA
            ws.getWsData().setWsDataFormatted(ioA2kLccc0003.getOutput().getA2kOugmaX().getA2kOugmaFormatted());
            // COB_CODE: SUBTRACT 1 FROM A2K-OUPROG
            ioA2kLccc0003.getOutput().setA2kOuprog(Trunc.toInt(abs(ioA2kLccc0003.getOutput().getA2kOuprog() - 1), 7));
        }
        //>>  SE FESTA FISSA
        //>>  SE LUNEDI DI PASQUA
        // COB_CODE: IF  A2K-OUTG06 NOT = 'F'
        //           AND A2K-OUNG06 = 1
        //               MOVE A2K-OUGMA TO WS-DATA
        //           END-IF.
        if (ioA2kLccc0003.getOutput().getA2kOutg06() != 'F' && ioA2kLccc0003.getOutput().getA2kOung06() == 1) {
            // COB_CODE: PERFORM S-CHECK-TESTPASQ
            sCheckTestpasq();
            // COB_CODE: MOVE A2K-OUGMA TO WS-DATA
            ws.getWsData().setWsDataFormatted(ioA2kLccc0003.getOutput().getA2kOugmaX().getA2kOugmaFormatted());
        }
    }

    /**Original name: Q-CHECK-SABDOM_FIRST_SENTENCES<br>
	 * <pre>-----------------------
	 * ----------------------------------------------------------------
	 *   CONTROLLO SABATO O DOMENICA
	 * ----------------------------------------------------------------
	 * >>  GIORNO DELLA SETTIMANA</pre>*/
    private void qCheckSabdom() {
        // COB_CODE: DIVIDE 7   INTO A2K-OUPROG
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ioA2kLccc0003.getOutput().getA2kOuprog() / 7);
        ws.setWsRemainder(ioA2kLccc0003.getOutput().getA2kOuprog() % 7);
        // COB_CODE: MOVE WS-REMAINDER TO A2K-OUNG06
        ioA2kLccc0003.getOutput().setA2kOung06(TruncAbs.toShort(ws.getWsRemainder(), 1));
        //>>  SE SABATO O DOMENICA
        // COB_CODE: IF  A2K-OUNG06 = 0 OR 6
        //               MOVE 'F' TO A2K-OUTG06
        //           END-IF.
        if (ioA2kLccc0003.getOutput().getA2kOung06() == 0 || ioA2kLccc0003.getOutput().getA2kOung06() == 6) {
            // COB_CODE: MOVE 'F' TO A2K-OUTG06
            ioA2kLccc0003.getOutput().setA2kOutg06Formatted("F");
        }
    }

    /**Original name: R-COMPUTE-RLAVOM_FIRST_SENTENCES<br>
	 * <pre>-----------------------
	 * ----------------------------------------------------------------
	 *   CALCOLO GIORNI LAVORATIVI E
	 *    L'ULTIMO GIORNO LAVORATIVO DEL MESE
	 * ----------------------------------------------------------------</pre>*/
    private void rComputeRlavom() {
        // COB_CODE: MOVE 1 TO WS-DATA-NUM
        ws.getWsDataNum().setWsDataNum(1);
        // COB_CODE: MOVE A2K-OUAMG-X (1:6) TO WS-DATA-NUM-AAMM
        ws.getWsDataNum().setNumAammFormatted(ioA2kLccc0003.getOutput().getA2kOuamgX().getA2kOuamgXFormatted().substring((1) - 1, 6));
        // COB_CODE: MOVE ZERO TO WS-GMLAV
        ws.setWsGmlav(((short)0));
        //>>  SE MARZO O APRILE, CALCOLA DATA PASQUETTA
        // COB_CODE: IF  WS-DATA-NUM-MM = 03 OR 04
        //               MOVE WS-DATAP TO WS-DATA-PASQUETTA
        //           ELSE
        //               MOVE ZERO TO WS-DATA-PASQUETTA
        //           END-IF
        if (ws.getWsDataNum().getNumMm() == 3 || ws.getWsDataNum().getNumMm() == 4) {
            // COB_CODE: PERFORM S-CHECK-TESTPASQ
            sCheckTestpasq();
            // COB_CODE: MOVE A2K-OUGMA TO WS-DATA
            ws.getWsData().setWsDataFormatted(ioA2kLccc0003.getOutput().getA2kOugmaX().getA2kOugmaFormatted());
            // COB_CODE: MOVE WS-DATAP TO WS-DATA-PASQUETTA
            ws.setWsDataPasquetta(ws.getWsDatap());
        }
        else {
            // COB_CODE: MOVE ZERO TO WS-DATA-PASQUETTA
            ws.setWsDataPasquetta(0);
        }
        //>>  IL CALCOLO PER IL BISESTILE E' GIA' FATTO
        // COB_CODE: SUBTRACT 1 FROM WS-DATA-NUM-AA GIVING WS-DATAP
        ws.setWsDatap(ws.getWsDataNum().getNumAa() - 1);
        // COB_CODE: PERFORM O-COMPUTE-CAL3112
        oComputeCal3112();
        //>>  NR. GIORNI AL FINE DEL MESE PRECEDENTE
        // COB_CODE: MOVE WS-DATA-NUM-MM TO I2
        ws.setI2(ws.getWsDataNum().getNumMm());
        // COB_CODE: SUBTRACT 1 FROM I2
        ws.setI2(Trunc.toShort(abs(ws.getI2() - 1), 2));
        // COB_CODE: PERFORM UNTIL I2 EQUAL ZERO
        //               SUBTRACT 1 FROM I2
        //           END-PERFORM
        while (!(ws.getI2() == 0)) {
            // COB_CODE: ADD WS-MESE (I2) TO WS-DATAP
            ws.setWsDatap(Trunc.toInt(ws.getWsMese(ws.getI2()) + ws.getWsDatap(), 7));
            // COB_CODE: SUBTRACT 1 FROM I2
            ws.setI2(Trunc.toShort(abs(ws.getI2() - 1), 2));
        }
        //>>  NR. GIORNI AD OGGI
        // COB_CODE: ADD WS-DATA-NUM-GG TO WS-DATAP
        ws.setWsDatap(Trunc.toInt(ws.getWsDataNum().getNumGg() + ws.getWsDatap(), 7));
        //>>  PROCESS FINO ALL'ULTIMO DEL MESE
        // COB_CODE: PERFORM RA-VEDI-SE-LAVORATIVO
        //               UNTIL WS-DATA-NUM-GG GREATER A2K-OUGG07
        while (!(ws.getWsDataNum().getNumGg() > ioA2kLccc0003.getOutput().getA2kOugg07())) {
            raVediSeLavorativo();
        }
        // COB_CODE: MOVE WS-GMLAV TO A2K-OUGG07
        ioA2kLccc0003.getOutput().setA2kOugg07Formatted(ws.getWsGmlavFormatted());
        // COB_CODE: MOVE SPACES TO A2K-OUGL07-X
        ioA2kLccc0003.getOutput().getA2kOugl07X().initA2kOugl07XSpaces();
        // COB_CODE: MOVE WS-FMLAV TO A2K-OUGL07.
        ioA2kLccc0003.getOutput().getA2kOugl07X().setOugl07Formatted(ws.getWsFmlavFormatted());
    }

    /**Original name: RA-VEDI-SE-LAVORATIVO_FIRST_SENTENCES<br>
	 * <pre>------------------------------
	 * ----------------------------------------------------------------
	 *   VEDI SE UN GIORNO E' LAVORATIVO
	 * ----------------------------------------------------------------
	 * >>  VEDI SE FESTA FISSA</pre>*/
    private void raVediSeLavorativo() {
        // COB_CODE: MOVE 1 TO I2
        ws.setI2(((short)1));
        // COB_CODE: PERFORM UNTIL WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
        //                      OR WA-FESTEX (I2) EQUAL HIGH-VALUE
        //                      OR I2 GREATER WA-MAX-FESTEX
        //               ADD 1 TO I2
        //           END-PERFORM
        while (!(Conditions.eq(ws.getWsDataNum().getNumMmggBytes(), ws.getWaTafestex().getFestexAsBuffer(ws.getI2())) || Characters.EQ_HIGH.test(ws.getWaTafestex().getFestexFormatted(ws.getI2())) || ws.getI2() > ws.getWaMaxFestex())) {
            // COB_CODE: ADD 1 TO I2
            ws.setI2(Trunc.toShort(1 + ws.getI2(), 2));
        }
        //>>  RITORNA SE FESTA FISSA O PASQUETTA
        // COB_CODE: IF  WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
        //           OR  WS-DATAP EQUAL WS-DATA-PASQUETTA
        //               GO TO RA-999-EXIT
        //           END-IF
        if (Conditions.eq(ws.getWsDataNum().getNumMmggBytes(), ws.getWaTafestex().getFestexAsBuffer(ws.getI2())) || ws.getWsDatap() == ws.getWsDataPasquetta()) {
            // COB_CODE: ADD 1 TO WS-DATA-NUM-GG
            ws.getWsDataNum().setNumGg(Trunc.toShort(1 + ws.getWsDataNum().getNumGg(), 2));
            // COB_CODE: ADD 1 TO WS-DATAP
            ws.setWsDatap(Trunc.toInt(1 + ws.getWsDatap(), 7));
            // COB_CODE: GO TO RA-999-EXIT
            return;
        }
        //>>  GIORNO DELLA SETTIMANA
        // COB_CODE: DIVIDE 7   INTO WS-DATAP
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsDatap() / 7);
        ws.setWsRemainder(ws.getWsDatap() % 7);
        //>>  RITORNA SE SABATO O DOMENICA
        // COB_CODE: IF  WS-REMAINDER = 0 OR 6
        //               GO TO RA-999-EXIT
        //           END-IF
        if (ws.getWsRemainder() == 0 || ws.getWsRemainder() == 6) {
            // COB_CODE: ADD 1 TO WS-DATA-NUM-GG
            ws.getWsDataNum().setNumGg(Trunc.toShort(1 + ws.getWsDataNum().getNumGg(), 2));
            // COB_CODE: ADD 1 TO WS-DATAP
            ws.setWsDatap(Trunc.toInt(1 + ws.getWsDatap(), 7));
            // COB_CODE: GO TO RA-999-EXIT
            return;
        }
        // COB_CODE: ADD 1 TO WS-GMLAV
        ws.setWsGmlav(Trunc.toShort(1 + ws.getWsGmlav(), 2));
        // COB_CODE: MOVE WS-DATA-NUM-GG TO WS-FMLAV
        ws.setWsFmlavFormatted(ws.getWsDataNum().getNumGgFormatted());
        // COB_CODE: ADD 1 TO WS-DATA-NUM-GG
        ws.getWsDataNum().setNumGg(Trunc.toShort(1 + ws.getWsDataNum().getNumGg(), 2));
        // COB_CODE: ADD 1 TO WS-DATAP.
        ws.setWsDatap(Trunc.toInt(1 + ws.getWsDatap(), 7));
    }

    /**Original name: S-CHECK-TESTPASQ_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 * ----------------------------------------------------------------
	 *   CALCOLO LA PASQUA CON LA FORMULA DI GAUSS
	 * ----------------------------------------------------------------</pre>*/
    private void sCheckTestpasq() {
        // COB_CODE: DIVIDE 19  INTO WS-DATA-SSAA
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-RESTOA
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsData().getDataSsaa() / 19);
        ws.getFlr2().setRestoa(((short)(ws.getWsData().getDataSsaa() % 19)));
        // COB_CODE: COMPUTE WS-PROD = WS-RESTOA * 19 + 24
        ws.getFlr2().setProd(ws.getFlr2().getRestoa() * 19 + 24);
        // COB_CODE: DIVIDE 30  INTO WS-PROD
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-DELTB
        //           END-DIVIDE
        ws.setWsQuotient(ws.getFlr2().getProd() / 30);
        ws.getFlr2().setDeltb(((short)(ws.getFlr2().getProd() % 30)));
        // COB_CODE: MOVE WS-DELTB TO WS-DELTAA
        ws.getFlr2().setDeltaaFormatted(ws.getFlr2().getDeltbFormatted());
        // COB_CODE: COMPUTE WS-COMO = WS-DELTB * 6 + 5
        ws.getFlr2().setComo(ws.getFlr2().getDeltb() * 6 + 5);
        // COB_CODE: DIVIDE 4   INTO WS-DATA-SSAA
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsData().getDataSsaa() / 4);
        ws.setWsRemainder(ws.getWsData().getDataSsaa() % 4);
        // COB_CODE: COMPUTE WS-COMO = WS-COMO + WS-REMAINDER * 2
        ws.getFlr2().setComo(Trunc.toInt(ws.getFlr2().getComo() + ws.getWsRemainder() * 2, 5));
        // COB_CODE: DIVIDE 7   INTO WS-DATA-SSAA
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getWsData().getDataSsaa() / 7);
        ws.setWsRemainder(ws.getWsData().getDataSsaa() % 7);
        // COB_CODE: COMPUTE WS-COMO = WS-COMO + WS-REMAINDER * 4
        ws.getFlr2().setComo(Trunc.toInt(ws.getFlr2().getComo() + ws.getWsRemainder() * 4, 5));
        // COB_CODE: DIVIDE 7   INTO WS-COMO
        //                    GIVING WS-QUOTIENT
        //                 REMAINDER WS-REMAINDER
        //           END-DIVIDE
        ws.setWsQuotient(ws.getFlr2().getComo() / 7);
        ws.setWsRemainder(ws.getFlr2().getComo() % 7);
        // COB_CODE: ADD WS-REMAINDER TO WS-DELTB
        ws.getFlr2().setDeltb(Trunc.toShort(abs(ws.getWsRemainder() + ws.getFlr2().getDeltb()), 2));
        //>>  CONTROLLO LE 2 ECCEZIONI DEL 25 E 26 APRILE
        //>>  SE CADE IL 25 APRILE E
        //>>      IL PRIMO RESTO DIVERSO DA ZERO E
        //>>      IL DELTAA UGUALE A 28
        //>>  ... ALORA PASQUE E' IL 18 APRILE
        // COB_CODE: IF  WS-DELTB = 34
        //           AND WS-RESTOA NOT = ZERO
        //           AND WS-DELTAA = 28
        //               MOVE 27 TO WS-DELTB
        //           END-IF
        if (ws.getFlr2().getDeltb() == 34 && !Characters.EQ_ZERO.test(ws.getFlr2().getRestoaFormatted()) && ws.getFlr2().getDeltaa() == 28) {
            // COB_CODE: MOVE 27 TO WS-DELTB
            ws.getFlr2().setDeltb(((short)27));
        }
        //>>  SE CADE IL 26 APRILE E
        //>>  ... ALORA PASQUE E' IL 19 APRILE
        // COB_CODE: IF  WS-DELTB = 35
        //               MOVE 28 TO WS-DELTB
        //           END-IF
        if (ws.getFlr2().getDeltb() == 35) {
            // COB_CODE: MOVE 28 TO WS-DELTB
            ws.getFlr2().setDeltb(((short)28));
        }
        //>>  CALCOLO PROGRESSIVO PER 22/03/ANNO DATA
        // COB_CODE: MOVE 22 TO WS-DATA-GG
        ws.getWsData().setDataGg(((short)22));
        // COB_CODE: MOVE 03 TO WS-DATA-MM
        ws.getWsData().setDataMm(((short)3));
        // COB_CODE: PERFORM L-CONVERT-CONVDG
        lConvertConvdg();
        //>>  CALCOLO PROGRESSIVO PASQUETTA - LUNEDI
        // COB_CODE: COMPUTE WS-DATAP = WS-DATAP + WS-DELTB + 1
        ws.setWsDatap(Trunc.toInt(ws.getWsDatap() + ws.getFlr2().getDeltb() + 1, 7));
        // COB_CODE: IF  WS-DATAP = A2K-OUPROG
        //               MOVE 'P' TO A2K-OUGG10
        //           END-IF.
        if (ws.getWsDatap() == ioA2kLccc0003.getOutput().getA2kOuprog()) {
            // COB_CODE: MOVE 'F' TO A2K-OUTG06
            ioA2kLccc0003.getOutput().setA2kOutg06Formatted("F");
            // COB_CODE: MOVE 'P' TO A2K-OUGG10
            ioA2kLccc0003.getOutput().setA2kOugg10Formatted("P");
        }
    }

    /**Original name: T-CHECK-TESTFFIS_FIRST_SENTENCES<br>
	 * <pre>-------------------------
	 * ----------------------------------------------------------------
	 *   CONTROLLO FESTE FISSE
	 * ----------------------------------------------------------------</pre>*/
    private void tCheckTestffis() {
        // COB_CODE: MOVE SPACES     TO A2K-OUGG10
        ioA2kLccc0003.getOutput().setA2kOugg10(Types.SPACE_CHAR);
        // COB_CODE: MOVE WS-DATA-MM TO WS-DATA-NUM-MM
        ws.getWsDataNum().setNumMmFormatted(ws.getWsData().getDataMmFormatted());
        // COB_CODE: MOVE WS-DATA-GG TO WS-DATA-NUM-GG
        ws.getWsDataNum().setNumGgFormatted(ws.getWsData().getDataGgFormatted());
        // COB_CODE: MOVE 1 TO I2
        ws.setI2(((short)1));
        // COB_CODE: PERFORM UNTIL WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
        //                      OR WA-FESTEX (I2) EQUAL HIGH-VALUE
        //                      OR I2 GREATER WA-MAX-FESTEX
        //               ADD 1 TO I2
        //           END-PERFORM
        while (!(Conditions.eq(ws.getWsDataNum().getNumMmggBytes(), ws.getWaTafestex().getFestexAsBuffer(ws.getI2())) || Characters.EQ_HIGH.test(ws.getWaTafestex().getFestexFormatted(ws.getI2())) || ws.getI2() > ws.getWaMaxFestex())) {
            // COB_CODE: ADD 1 TO I2
            ws.setI2(Trunc.toShort(1 + ws.getI2(), 2));
        }
        // COB_CODE: IF  WS-DATA-NUM-MMGG EQUAL WA-FESTEX (I2)
        //               MOVE 'F' TO A2K-OUGG10
        //           END-IF.
        if (Conditions.eq(ws.getWsDataNum().getNumMmggBytes(), ws.getWaTafestex().getFestexAsBuffer(ws.getI2()))) {
            // COB_CODE: MOVE 'F' TO A2K-OUTG06
            ioA2kLccc0003.getOutput().setA2kOutg06Formatted("F");
            // COB_CODE: MOVE 'F' TO A2K-OUGG10
            ioA2kLccc0003.getOutput().setA2kOugg10Formatted("F");
        }
    }

    /**Original name: RNG_B10-VALIDATE-INFO-10_FIRST_SENTENCES-_-B10-999-EXIT<br>*/
    private void rngB10ValidateInfo10() {
        String retcode = "";
        boolean gotoB09999Exit = false;
        boolean gotoB10ValidateInfo10FirstSentences = false;
        do {
            if (gotoB09999Exit) {
                gotoB09999Exit = false;
                b09999Exit();
            }
            gotoB10ValidateInfo10FirstSentences = false;
            retcode = b10ValidateInfo10();
            gotoB09999Exit = retcode.equals("B09-999-EXIT");
        }
        while (gotoB09999Exit);
        b10999Exit();
    }
}
