package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamInfrApplDao;
import it.accenture.jnais.commons.data.to.IParamInfrAppl;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsd090Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.ParamInfrAppl;
import it.accenture.jnais.ws.redefines.D09CsocketPortNum;
import it.accenture.jnais.ws.redefines.D09MqTempoAttesa1;
import it.accenture.jnais.ws.redefines.D09MqTempoAttesa2;
import it.accenture.jnais.ws.redefines.D09MqTempoExpiry;

/**Original name: IDBSD090<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  10 MAG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsd090 extends Program implements IParamInfrAppl {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamInfrApplDao paramInfrApplDao = new ParamInfrApplDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsd090Data ws = new Idbsd090Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PARAM-INFR-APPL
    private ParamInfrAppl paramInfrAppl;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSD090_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ParamInfrAppl paramInfrAppl) {
        this.idsv0003 = idsv0003;
        this.paramInfrAppl = paramInfrAppl;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsd090 getInstance() {
        return ((Idbsd090)Programs.getInstance(Idbsd090.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSD090'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSD090");
        // COB_CODE: MOVE 'PARAM_INFR_APPL' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PARAM_INFR_APPL");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_COMP_ANIA
        //                ,AMBIENTE
        //                ,PIATTAFORMA
        //                ,TP_COM_COBOL_JAVA
        //                ,MQ_TP_UTILIZZO_API
        //                ,MQ_QUEUE_MANAGER
        //                ,MQ_CODA_PUT
        //                ,MQ_CODA_GET
        //                ,MQ_OPZ_PERSISTENZA
        //                ,MQ_OPZ_WAIT
        //                ,MQ_OPZ_SYNCPOINT
        //                ,MQ_ATTESA_RISPOSTA
        //                ,MQ_TEMPO_ATTESA_1
        //                ,MQ_TEMPO_ATTESA_2
        //                ,MQ_TEMPO_EXPIRY
        //                ,CSOCKET_IP_ADDRESS
        //                ,CSOCKET_PORT_NUM
        //                ,FL_COMPRESSORE_C
        //             INTO
        //                :D09-COD-COMP-ANIA
        //               ,:D09-AMBIENTE
        //               ,:D09-PIATTAFORMA
        //               ,:D09-TP-COM-COBOL-JAVA
        //               ,:D09-MQ-TP-UTILIZZO-API
        //                :IND-D09-MQ-TP-UTILIZZO-API
        //               ,:D09-MQ-QUEUE-MANAGER
        //                :IND-D09-MQ-QUEUE-MANAGER
        //               ,:D09-MQ-CODA-PUT
        //                :IND-D09-MQ-CODA-PUT
        //               ,:D09-MQ-CODA-GET
        //                :IND-D09-MQ-CODA-GET
        //               ,:D09-MQ-OPZ-PERSISTENZA
        //                :IND-D09-MQ-OPZ-PERSISTENZA
        //               ,:D09-MQ-OPZ-WAIT
        //                :IND-D09-MQ-OPZ-WAIT
        //               ,:D09-MQ-OPZ-SYNCPOINT
        //                :IND-D09-MQ-OPZ-SYNCPOINT
        //               ,:D09-MQ-ATTESA-RISPOSTA
        //                :IND-D09-MQ-ATTESA-RISPOSTA
        //               ,:D09-MQ-TEMPO-ATTESA-1
        //                :IND-D09-MQ-TEMPO-ATTESA-1
        //               ,:D09-MQ-TEMPO-ATTESA-2
        //                :IND-D09-MQ-TEMPO-ATTESA-2
        //               ,:D09-MQ-TEMPO-EXPIRY
        //                :IND-D09-MQ-TEMPO-EXPIRY
        //               ,:D09-CSOCKET-IP-ADDRESS
        //                :IND-D09-CSOCKET-IP-ADDRESS
        //               ,:D09-CSOCKET-PORT-NUM
        //                :IND-D09-CSOCKET-PORT-NUM
        //               ,:D09-FL-COMPRESSORE-C
        //                :IND-D09-FL-COMPRESSORE-C
        //             FROM PARAM_INFR_APPL
        //             WHERE     COD_COMP_ANIA = :D09-COD-COMP-ANIA
        //           END-EXEC.
        paramInfrApplDao.selectByD09CodCompAnia(paramInfrAppl.getD09CodCompAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO PARAM_INFR_APPL
            //                  (
            //                     COD_COMP_ANIA
            //                    ,AMBIENTE
            //                    ,PIATTAFORMA
            //                    ,TP_COM_COBOL_JAVA
            //                    ,MQ_TP_UTILIZZO_API
            //                    ,MQ_QUEUE_MANAGER
            //                    ,MQ_CODA_PUT
            //                    ,MQ_CODA_GET
            //                    ,MQ_OPZ_PERSISTENZA
            //                    ,MQ_OPZ_WAIT
            //                    ,MQ_OPZ_SYNCPOINT
            //                    ,MQ_ATTESA_RISPOSTA
            //                    ,MQ_TEMPO_ATTESA_1
            //                    ,MQ_TEMPO_ATTESA_2
            //                    ,MQ_TEMPO_EXPIRY
            //                    ,CSOCKET_IP_ADDRESS
            //                    ,CSOCKET_PORT_NUM
            //                    ,FL_COMPRESSORE_C
            //                  )
            //              VALUES
            //                  (
            //                    :D09-COD-COMP-ANIA
            //                    ,:D09-AMBIENTE
            //                    ,:D09-PIATTAFORMA
            //                    ,:D09-TP-COM-COBOL-JAVA
            //                    ,:D09-MQ-TP-UTILIZZO-API
            //                     :IND-D09-MQ-TP-UTILIZZO-API
            //                    ,:D09-MQ-QUEUE-MANAGER
            //                     :IND-D09-MQ-QUEUE-MANAGER
            //                    ,:D09-MQ-CODA-PUT
            //                     :IND-D09-MQ-CODA-PUT
            //                    ,:D09-MQ-CODA-GET
            //                     :IND-D09-MQ-CODA-GET
            //                    ,:D09-MQ-OPZ-PERSISTENZA
            //                     :IND-D09-MQ-OPZ-PERSISTENZA
            //                    ,:D09-MQ-OPZ-WAIT
            //                     :IND-D09-MQ-OPZ-WAIT
            //                    ,:D09-MQ-OPZ-SYNCPOINT
            //                     :IND-D09-MQ-OPZ-SYNCPOINT
            //                    ,:D09-MQ-ATTESA-RISPOSTA
            //                     :IND-D09-MQ-ATTESA-RISPOSTA
            //                    ,:D09-MQ-TEMPO-ATTESA-1
            //                     :IND-D09-MQ-TEMPO-ATTESA-1
            //                    ,:D09-MQ-TEMPO-ATTESA-2
            //                     :IND-D09-MQ-TEMPO-ATTESA-2
            //                    ,:D09-MQ-TEMPO-EXPIRY
            //                     :IND-D09-MQ-TEMPO-EXPIRY
            //                    ,:D09-CSOCKET-IP-ADDRESS
            //                     :IND-D09-CSOCKET-IP-ADDRESS
            //                    ,:D09-CSOCKET-PORT-NUM
            //                     :IND-D09-CSOCKET-PORT-NUM
            //                    ,:D09-FL-COMPRESSORE-C
            //                     :IND-D09-FL-COMPRESSORE-C
            //                  )
            //           END-EXEC
            paramInfrApplDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PARAM_INFR_APPL SET
        //                   COD_COMP_ANIA          =
        //                :D09-COD-COMP-ANIA
        //                  ,AMBIENTE               =
        //                :D09-AMBIENTE
        //                  ,PIATTAFORMA            =
        //                :D09-PIATTAFORMA
        //                  ,TP_COM_COBOL_JAVA      =
        //                :D09-TP-COM-COBOL-JAVA
        //                  ,MQ_TP_UTILIZZO_API     =
        //                :D09-MQ-TP-UTILIZZO-API
        //                                       :IND-D09-MQ-TP-UTILIZZO-API
        //                  ,MQ_QUEUE_MANAGER       =
        //                :D09-MQ-QUEUE-MANAGER
        //                                       :IND-D09-MQ-QUEUE-MANAGER
        //                  ,MQ_CODA_PUT            =
        //                :D09-MQ-CODA-PUT
        //                                       :IND-D09-MQ-CODA-PUT
        //                  ,MQ_CODA_GET            =
        //                :D09-MQ-CODA-GET
        //                                       :IND-D09-MQ-CODA-GET
        //                  ,MQ_OPZ_PERSISTENZA     =
        //                :D09-MQ-OPZ-PERSISTENZA
        //                                       :IND-D09-MQ-OPZ-PERSISTENZA
        //                  ,MQ_OPZ_WAIT            =
        //                :D09-MQ-OPZ-WAIT
        //                                       :IND-D09-MQ-OPZ-WAIT
        //                  ,MQ_OPZ_SYNCPOINT       =
        //                :D09-MQ-OPZ-SYNCPOINT
        //                                       :IND-D09-MQ-OPZ-SYNCPOINT
        //                  ,MQ_ATTESA_RISPOSTA     =
        //                :D09-MQ-ATTESA-RISPOSTA
        //                                       :IND-D09-MQ-ATTESA-RISPOSTA
        //                  ,MQ_TEMPO_ATTESA_1      =
        //                :D09-MQ-TEMPO-ATTESA-1
        //                                       :IND-D09-MQ-TEMPO-ATTESA-1
        //                  ,MQ_TEMPO_ATTESA_2      =
        //                :D09-MQ-TEMPO-ATTESA-2
        //                                       :IND-D09-MQ-TEMPO-ATTESA-2
        //                  ,MQ_TEMPO_EXPIRY        =
        //                :D09-MQ-TEMPO-EXPIRY
        //                                       :IND-D09-MQ-TEMPO-EXPIRY
        //                  ,CSOCKET_IP_ADDRESS     =
        //                :D09-CSOCKET-IP-ADDRESS
        //                                       :IND-D09-CSOCKET-IP-ADDRESS
        //                  ,CSOCKET_PORT_NUM       =
        //                :D09-CSOCKET-PORT-NUM
        //                                       :IND-D09-CSOCKET-PORT-NUM
        //                  ,FL_COMPRESSORE_C       =
        //                :D09-FL-COMPRESSORE-C
        //                                       :IND-D09-FL-COMPRESSORE-C
        //                WHERE     COD_COMP_ANIA = :D09-COD-COMP-ANIA
        //           END-EXEC.
        paramInfrApplDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM PARAM_INFR_APPL
        //                WHERE     COD_COMP_ANIA = :D09-COD-COMP-ANIA
        //           END-EXEC.
        paramInfrApplDao.deleteByD09CodCompAnia(paramInfrAppl.getD09CodCompAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-D09-MQ-TP-UTILIZZO-API = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TP-UTILIZZO-API-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TP-UTILIZZO-API-NULL
            paramInfrAppl.setD09MqTpUtilizzoApi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_TP_UTILIZZO_API));
        }
        // COB_CODE: IF IND-D09-MQ-QUEUE-MANAGER = -1
        //              MOVE HIGH-VALUES TO D09-MQ-QUEUE-MANAGER-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-QUEUE-MANAGER-NULL
            paramInfrAppl.setD09MqQueueManager(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_QUEUE_MANAGER));
        }
        // COB_CODE: IF IND-D09-MQ-CODA-PUT = -1
        //              MOVE HIGH-VALUES TO D09-MQ-CODA-PUT-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getNumQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-CODA-PUT-NULL
            paramInfrAppl.setD09MqCodaPut(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_CODA_PUT));
        }
        // COB_CODE: IF IND-D09-MQ-CODA-GET = -1
        //              MOVE HIGH-VALUES TO D09-MQ-CODA-GET-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-CODA-GET-NULL
            paramInfrAppl.setD09MqCodaGet(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_CODA_GET));
        }
        // COB_CODE: IF IND-D09-MQ-OPZ-PERSISTENZA = -1
        //              MOVE HIGH-VALUES TO D09-MQ-OPZ-PERSISTENZA-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getImpMovto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-OPZ-PERSISTENZA-NULL
            paramInfrAppl.setD09MqOpzPersistenza(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-OPZ-WAIT = -1
        //              MOVE HIGH-VALUES TO D09-MQ-OPZ-WAIT-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getDtInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-OPZ-WAIT-NULL
            paramInfrAppl.setD09MqOpzWait(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-OPZ-SYNCPOINT = -1
        //              MOVE HIGH-VALUES TO D09-MQ-OPZ-SYNCPOINT-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-OPZ-SYNCPOINT-NULL
            paramInfrAppl.setD09MqOpzSyncpoint(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-ATTESA-RISPOSTA = -1
        //              MOVE HIGH-VALUES TO D09-MQ-ATTESA-RISPOSTA-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getTpStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-ATTESA-RISPOSTA-NULL
            paramInfrAppl.setD09MqAttesaRisposta(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-TEMPO-ATTESA-1 = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-1-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getTpModInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-1-NULL
            paramInfrAppl.getD09MqTempoAttesa1().setD09MqTempoAttesa1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09MqTempoAttesa1.Len.D09_MQ_TEMPO_ATTESA1_NULL));
        }
        // COB_CODE: IF IND-D09-MQ-TEMPO-ATTESA-2 = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-2-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-2-NULL
            paramInfrAppl.getD09MqTempoAttesa2().setD09MqTempoAttesa2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09MqTempoAttesa2.Len.D09_MQ_TEMPO_ATTESA2_NULL));
        }
        // COB_CODE: IF IND-D09-MQ-TEMPO-EXPIRY = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TEMPO-EXPIRY-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getDtInvstCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TEMPO-EXPIRY-NULL
            paramInfrAppl.getD09MqTempoExpiry().setD09MqTempoExpiryNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09MqTempoExpiry.Len.D09_MQ_TEMPO_EXPIRY_NULL));
        }
        // COB_CODE: IF IND-D09-CSOCKET-IP-ADDRESS = -1
        //              MOVE HIGH-VALUES TO D09-CSOCKET-IP-ADDRESS-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getFlCalcInvto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-CSOCKET-IP-ADDRESS-NULL
            paramInfrAppl.setD09CsocketIpAddress(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_CSOCKET_IP_ADDRESS));
        }
        // COB_CODE: IF IND-D09-CSOCKET-PORT-NUM = -1
        //              MOVE HIGH-VALUES TO D09-CSOCKET-PORT-NUM-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getImpGapEvent() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-CSOCKET-PORT-NUM-NULL
            paramInfrAppl.getD09CsocketPortNum().setD09CsocketPortNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09CsocketPortNum.Len.D09_CSOCKET_PORT_NUM_NULL));
        }
        // COB_CODE: IF IND-D09-FL-COMPRESSORE-C = -1
        //              MOVE HIGH-VALUES TO D09-FL-COMPRESSORE-C-NULL
        //           END-IF.
        if (ws.getIndParamInfrAppl().getFlSwmBp2s() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-FL-COMPRESSORE-C-NULL
            paramInfrAppl.setD09FlCompressoreC(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF D09-MQ-TP-UTILIZZO-API-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-TP-UTILIZZO-API
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-TP-UTILIZZO-API
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqTpUtilizzoApiFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-TP-UTILIZZO-API
            ws.getIndParamInfrAppl().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-TP-UTILIZZO-API
            ws.getIndParamInfrAppl().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF D09-MQ-QUEUE-MANAGER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-QUEUE-MANAGER
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-QUEUE-MANAGER
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqQueueManagerFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-QUEUE-MANAGER
            ws.getIndParamInfrAppl().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-QUEUE-MANAGER
            ws.getIndParamInfrAppl().setCodFnd(((short)0));
        }
        // COB_CODE: IF D09-MQ-CODA-PUT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-CODA-PUT
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-CODA-PUT
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqCodaPutFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-CODA-PUT
            ws.getIndParamInfrAppl().setNumQuo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-CODA-PUT
            ws.getIndParamInfrAppl().setNumQuo(((short)0));
        }
        // COB_CODE: IF D09-MQ-CODA-GET-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-CODA-GET
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-CODA-GET
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqCodaGetFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-CODA-GET
            ws.getIndParamInfrAppl().setPc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-CODA-GET
            ws.getIndParamInfrAppl().setPc(((short)0));
        }
        // COB_CODE: IF D09-MQ-OPZ-PERSISTENZA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-OPZ-PERSISTENZA
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-OPZ-PERSISTENZA
        //           END-IF
        if (Conditions.eq(paramInfrAppl.getD09MqOpzPersistenza(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-OPZ-PERSISTENZA
            ws.getIndParamInfrAppl().setImpMovto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-OPZ-PERSISTENZA
            ws.getIndParamInfrAppl().setImpMovto(((short)0));
        }
        // COB_CODE: IF D09-MQ-OPZ-WAIT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-OPZ-WAIT
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-OPZ-WAIT
        //           END-IF
        if (Conditions.eq(paramInfrAppl.getD09MqOpzWait(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-OPZ-WAIT
            ws.getIndParamInfrAppl().setDtInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-OPZ-WAIT
            ws.getIndParamInfrAppl().setDtInvst(((short)0));
        }
        // COB_CODE: IF D09-MQ-OPZ-SYNCPOINT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-OPZ-SYNCPOINT
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-OPZ-SYNCPOINT
        //           END-IF
        if (Conditions.eq(paramInfrAppl.getD09MqOpzSyncpoint(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-OPZ-SYNCPOINT
            ws.getIndParamInfrAppl().setCodTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-OPZ-SYNCPOINT
            ws.getIndParamInfrAppl().setCodTari(((short)0));
        }
        // COB_CODE: IF D09-MQ-ATTESA-RISPOSTA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-ATTESA-RISPOSTA
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-ATTESA-RISPOSTA
        //           END-IF
        if (Conditions.eq(paramInfrAppl.getD09MqAttesaRisposta(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-ATTESA-RISPOSTA
            ws.getIndParamInfrAppl().setTpStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-ATTESA-RISPOSTA
            ws.getIndParamInfrAppl().setTpStat(((short)0));
        }
        // COB_CODE: IF D09-MQ-TEMPO-ATTESA-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-TEMPO-ATTESA-1
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-TEMPO-ATTESA-1
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqTempoAttesa1().getD09MqTempoAttesa1NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-TEMPO-ATTESA-1
            ws.getIndParamInfrAppl().setTpModInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-TEMPO-ATTESA-1
            ws.getIndParamInfrAppl().setTpModInvst(((short)0));
        }
        // COB_CODE: IF D09-MQ-TEMPO-ATTESA-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-TEMPO-ATTESA-2
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-TEMPO-ATTESA-2
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqTempoAttesa2().getD09MqTempoAttesa2NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-TEMPO-ATTESA-2
            ws.getIndParamInfrAppl().setDtCambioVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-TEMPO-ATTESA-2
            ws.getIndParamInfrAppl().setDtCambioVlt(((short)0));
        }
        // COB_CODE: IF D09-MQ-TEMPO-EXPIRY-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-MQ-TEMPO-EXPIRY
        //           ELSE
        //              MOVE 0 TO IND-D09-MQ-TEMPO-EXPIRY
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09MqTempoExpiry().getD09MqTempoExpiryNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-MQ-TEMPO-EXPIRY
            ws.getIndParamInfrAppl().setDtInvstCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-MQ-TEMPO-EXPIRY
            ws.getIndParamInfrAppl().setDtInvstCalc(((short)0));
        }
        // COB_CODE: IF D09-CSOCKET-IP-ADDRESS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-CSOCKET-IP-ADDRESS
        //           ELSE
        //              MOVE 0 TO IND-D09-CSOCKET-IP-ADDRESS
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09CsocketIpAddressFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-CSOCKET-IP-ADDRESS
            ws.getIndParamInfrAppl().setFlCalcInvto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-CSOCKET-IP-ADDRESS
            ws.getIndParamInfrAppl().setFlCalcInvto(((short)0));
        }
        // COB_CODE: IF D09-CSOCKET-PORT-NUM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-CSOCKET-PORT-NUM
        //           ELSE
        //              MOVE 0 TO IND-D09-CSOCKET-PORT-NUM
        //           END-IF
        if (Characters.EQ_HIGH.test(paramInfrAppl.getD09CsocketPortNum().getD09CsocketPortNumNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-D09-CSOCKET-PORT-NUM
            ws.getIndParamInfrAppl().setImpGapEvent(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-CSOCKET-PORT-NUM
            ws.getIndParamInfrAppl().setImpGapEvent(((short)0));
        }
        // COB_CODE: IF D09-FL-COMPRESSORE-C-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-D09-FL-COMPRESSORE-C
        //           ELSE
        //              MOVE 0 TO IND-D09-FL-COMPRESSORE-C
        //           END-IF.
        if (Conditions.eq(paramInfrAppl.getD09FlCompressoreC(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-D09-FL-COMPRESSORE-C
            ws.getIndParamInfrAppl().setFlSwmBp2s(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-D09-FL-COMPRESSORE-C
            ws.getIndParamInfrAppl().setFlSwmBp2s(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public String getAmbiente() {
        return paramInfrAppl.getD09Ambiente();
    }

    @Override
    public void setAmbiente(String ambiente) {
        this.paramInfrAppl.setD09Ambiente(ambiente);
    }

    @Override
    public String getCsocketIpAddress() {
        return paramInfrAppl.getD09CsocketIpAddress();
    }

    @Override
    public void setCsocketIpAddress(String csocketIpAddress) {
        this.paramInfrAppl.setD09CsocketIpAddress(csocketIpAddress);
    }

    @Override
    public String getCsocketIpAddressObj() {
        if (ws.getIndParamInfrAppl().getFlCalcInvto() >= 0) {
            return getCsocketIpAddress();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCsocketIpAddressObj(String csocketIpAddressObj) {
        if (csocketIpAddressObj != null) {
            setCsocketIpAddress(csocketIpAddressObj);
            ws.getIndParamInfrAppl().setFlCalcInvto(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setFlCalcInvto(((short)-1));
        }
    }

    @Override
    public int getCsocketPortNum() {
        return paramInfrAppl.getD09CsocketPortNum().getD09CsocketPortNum();
    }

    @Override
    public void setCsocketPortNum(int csocketPortNum) {
        this.paramInfrAppl.getD09CsocketPortNum().setD09CsocketPortNum(csocketPortNum);
    }

    @Override
    public Integer getCsocketPortNumObj() {
        if (ws.getIndParamInfrAppl().getImpGapEvent() >= 0) {
            return ((Integer)getCsocketPortNum());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCsocketPortNumObj(Integer csocketPortNumObj) {
        if (csocketPortNumObj != null) {
            setCsocketPortNum(((int)csocketPortNumObj));
            ws.getIndParamInfrAppl().setImpGapEvent(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setImpGapEvent(((short)-1));
        }
    }

    @Override
    public int getD09CodCompAnia() {
        return paramInfrAppl.getD09CodCompAnia();
    }

    @Override
    public void setD09CodCompAnia(int d09CodCompAnia) {
        this.paramInfrAppl.setD09CodCompAnia(d09CodCompAnia);
    }

    @Override
    public char getFlCompressoreC() {
        return paramInfrAppl.getD09FlCompressoreC();
    }

    @Override
    public void setFlCompressoreC(char flCompressoreC) {
        this.paramInfrAppl.setD09FlCompressoreC(flCompressoreC);
    }

    @Override
    public Character getFlCompressoreCObj() {
        if (ws.getIndParamInfrAppl().getFlSwmBp2s() >= 0) {
            return ((Character)getFlCompressoreC());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCompressoreCObj(Character flCompressoreCObj) {
        if (flCompressoreCObj != null) {
            setFlCompressoreC(((char)flCompressoreCObj));
            ws.getIndParamInfrAppl().setFlSwmBp2s(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setFlSwmBp2s(((short)-1));
        }
    }

    @Override
    public char getMqAttesaRisposta() {
        return paramInfrAppl.getD09MqAttesaRisposta();
    }

    @Override
    public void setMqAttesaRisposta(char mqAttesaRisposta) {
        this.paramInfrAppl.setD09MqAttesaRisposta(mqAttesaRisposta);
    }

    @Override
    public Character getMqAttesaRispostaObj() {
        if (ws.getIndParamInfrAppl().getTpStat() >= 0) {
            return ((Character)getMqAttesaRisposta());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqAttesaRispostaObj(Character mqAttesaRispostaObj) {
        if (mqAttesaRispostaObj != null) {
            setMqAttesaRisposta(((char)mqAttesaRispostaObj));
            ws.getIndParamInfrAppl().setTpStat(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setTpStat(((short)-1));
        }
    }

    @Override
    public String getMqCodaGet() {
        return paramInfrAppl.getD09MqCodaGet();
    }

    @Override
    public void setMqCodaGet(String mqCodaGet) {
        this.paramInfrAppl.setD09MqCodaGet(mqCodaGet);
    }

    @Override
    public String getMqCodaGetObj() {
        if (ws.getIndParamInfrAppl().getPc() >= 0) {
            return getMqCodaGet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqCodaGetObj(String mqCodaGetObj) {
        if (mqCodaGetObj != null) {
            setMqCodaGet(mqCodaGetObj);
            ws.getIndParamInfrAppl().setPc(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setPc(((short)-1));
        }
    }

    @Override
    public String getMqCodaPut() {
        return paramInfrAppl.getD09MqCodaPut();
    }

    @Override
    public void setMqCodaPut(String mqCodaPut) {
        this.paramInfrAppl.setD09MqCodaPut(mqCodaPut);
    }

    @Override
    public String getMqCodaPutObj() {
        if (ws.getIndParamInfrAppl().getNumQuo() >= 0) {
            return getMqCodaPut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqCodaPutObj(String mqCodaPutObj) {
        if (mqCodaPutObj != null) {
            setMqCodaPut(mqCodaPutObj);
            ws.getIndParamInfrAppl().setNumQuo(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setNumQuo(((short)-1));
        }
    }

    @Override
    public char getMqOpzPersistenza() {
        return paramInfrAppl.getD09MqOpzPersistenza();
    }

    @Override
    public void setMqOpzPersistenza(char mqOpzPersistenza) {
        this.paramInfrAppl.setD09MqOpzPersistenza(mqOpzPersistenza);
    }

    @Override
    public Character getMqOpzPersistenzaObj() {
        if (ws.getIndParamInfrAppl().getImpMovto() >= 0) {
            return ((Character)getMqOpzPersistenza());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqOpzPersistenzaObj(Character mqOpzPersistenzaObj) {
        if (mqOpzPersistenzaObj != null) {
            setMqOpzPersistenza(((char)mqOpzPersistenzaObj));
            ws.getIndParamInfrAppl().setImpMovto(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setImpMovto(((short)-1));
        }
    }

    @Override
    public char getMqOpzSyncpoint() {
        return paramInfrAppl.getD09MqOpzSyncpoint();
    }

    @Override
    public void setMqOpzSyncpoint(char mqOpzSyncpoint) {
        this.paramInfrAppl.setD09MqOpzSyncpoint(mqOpzSyncpoint);
    }

    @Override
    public Character getMqOpzSyncpointObj() {
        if (ws.getIndParamInfrAppl().getCodTari() >= 0) {
            return ((Character)getMqOpzSyncpoint());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqOpzSyncpointObj(Character mqOpzSyncpointObj) {
        if (mqOpzSyncpointObj != null) {
            setMqOpzSyncpoint(((char)mqOpzSyncpointObj));
            ws.getIndParamInfrAppl().setCodTari(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setCodTari(((short)-1));
        }
    }

    @Override
    public char getMqOpzWait() {
        return paramInfrAppl.getD09MqOpzWait();
    }

    @Override
    public void setMqOpzWait(char mqOpzWait) {
        this.paramInfrAppl.setD09MqOpzWait(mqOpzWait);
    }

    @Override
    public Character getMqOpzWaitObj() {
        if (ws.getIndParamInfrAppl().getDtInvst() >= 0) {
            return ((Character)getMqOpzWait());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqOpzWaitObj(Character mqOpzWaitObj) {
        if (mqOpzWaitObj != null) {
            setMqOpzWait(((char)mqOpzWaitObj));
            ws.getIndParamInfrAppl().setDtInvst(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setDtInvst(((short)-1));
        }
    }

    @Override
    public String getMqQueueManager() {
        return paramInfrAppl.getD09MqQueueManager();
    }

    @Override
    public void setMqQueueManager(String mqQueueManager) {
        this.paramInfrAppl.setD09MqQueueManager(mqQueueManager);
    }

    @Override
    public String getMqQueueManagerObj() {
        if (ws.getIndParamInfrAppl().getCodFnd() >= 0) {
            return getMqQueueManager();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqQueueManagerObj(String mqQueueManagerObj) {
        if (mqQueueManagerObj != null) {
            setMqQueueManager(mqQueueManagerObj);
            ws.getIndParamInfrAppl().setCodFnd(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setCodFnd(((short)-1));
        }
    }

    @Override
    public long getMqTempoAttesa1() {
        return paramInfrAppl.getD09MqTempoAttesa1().getD09MqTempoAttesa1();
    }

    @Override
    public void setMqTempoAttesa1(long mqTempoAttesa1) {
        this.paramInfrAppl.getD09MqTempoAttesa1().setD09MqTempoAttesa1(mqTempoAttesa1);
    }

    @Override
    public Long getMqTempoAttesa1Obj() {
        if (ws.getIndParamInfrAppl().getTpModInvst() >= 0) {
            return ((Long)getMqTempoAttesa1());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTempoAttesa1Obj(Long mqTempoAttesa1Obj) {
        if (mqTempoAttesa1Obj != null) {
            setMqTempoAttesa1(((long)mqTempoAttesa1Obj));
            ws.getIndParamInfrAppl().setTpModInvst(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setTpModInvst(((short)-1));
        }
    }

    @Override
    public long getMqTempoAttesa2() {
        return paramInfrAppl.getD09MqTempoAttesa2().getD09MqTempoAttesa2();
    }

    @Override
    public void setMqTempoAttesa2(long mqTempoAttesa2) {
        this.paramInfrAppl.getD09MqTempoAttesa2().setD09MqTempoAttesa2(mqTempoAttesa2);
    }

    @Override
    public Long getMqTempoAttesa2Obj() {
        if (ws.getIndParamInfrAppl().getDtCambioVlt() >= 0) {
            return ((Long)getMqTempoAttesa2());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTempoAttesa2Obj(Long mqTempoAttesa2Obj) {
        if (mqTempoAttesa2Obj != null) {
            setMqTempoAttesa2(((long)mqTempoAttesa2Obj));
            ws.getIndParamInfrAppl().setDtCambioVlt(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public long getMqTempoExpiry() {
        return paramInfrAppl.getD09MqTempoExpiry().getD09MqTempoExpiry();
    }

    @Override
    public void setMqTempoExpiry(long mqTempoExpiry) {
        this.paramInfrAppl.getD09MqTempoExpiry().setD09MqTempoExpiry(mqTempoExpiry);
    }

    @Override
    public Long getMqTempoExpiryObj() {
        if (ws.getIndParamInfrAppl().getDtInvstCalc() >= 0) {
            return ((Long)getMqTempoExpiry());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTempoExpiryObj(Long mqTempoExpiryObj) {
        if (mqTempoExpiryObj != null) {
            setMqTempoExpiry(((long)mqTempoExpiryObj));
            ws.getIndParamInfrAppl().setDtInvstCalc(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setDtInvstCalc(((short)-1));
        }
    }

    @Override
    public String getMqTpUtilizzoApi() {
        return paramInfrAppl.getD09MqTpUtilizzoApi();
    }

    @Override
    public void setMqTpUtilizzoApi(String mqTpUtilizzoApi) {
        this.paramInfrAppl.setD09MqTpUtilizzoApi(mqTpUtilizzoApi);
    }

    @Override
    public String getMqTpUtilizzoApiObj() {
        if (ws.getIndParamInfrAppl().getIdMoviChiu() >= 0) {
            return getMqTpUtilizzoApi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTpUtilizzoApiObj(String mqTpUtilizzoApiObj) {
        if (mqTpUtilizzoApiObj != null) {
            setMqTpUtilizzoApi(mqTpUtilizzoApiObj);
            ws.getIndParamInfrAppl().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public String getPiattaforma() {
        return paramInfrAppl.getD09Piattaforma();
    }

    @Override
    public void setPiattaforma(String piattaforma) {
        this.paramInfrAppl.setD09Piattaforma(piattaforma);
    }

    @Override
    public String getTpComCobolJava() {
        return paramInfrAppl.getD09TpComCobolJava();
    }

    @Override
    public void setTpComCobolJava(String tpComCobolJava) {
        this.paramInfrAppl.setD09TpComCobolJava(tpComCobolJava);
    }
}
