package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RichEstDao;
import it.accenture.jnais.commons.data.to.IRichEst;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsp010Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.P01DtEff;
import it.accenture.jnais.ws.redefines.P01DtEstFinanz;
import it.accenture.jnais.ws.redefines.P01DtManCop;
import it.accenture.jnais.ws.redefines.P01DtSin;
import it.accenture.jnais.ws.redefines.P01IdLiq;
import it.accenture.jnais.ws.redefines.P01IdRichEstCollg;
import it.accenture.jnais.ws.redefines.P01IdRichiedente;
import it.accenture.jnais.ws.RichEstIdbsp010;

/**Original name: IDBSP010<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2017.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsp010 extends Program implements IRichEst {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RichEstDao richEstDao = new RichEstDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsp010Data ws = new Idbsp010Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RICH-EST
    private RichEstIdbsp010 richEst;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSP010_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RichEstIdbsp010 richEst) {
        this.idsv0003 = idsv0003;
        this.richEst = richEst;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsp010 getInstance() {
        return ((Idbsp010)Programs.getInstance(Idbsp010.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSP010'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSP010");
        // COB_CODE: MOVE 'RICH_EST' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RICH_EST");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_EST
        //                ,ID_RICH_EST_COLLG
        //                ,ID_LIQ
        //                ,COD_COMP_ANIA
        //                ,IB_RICH_EST
        //                ,TP_MOVI
        //                ,DT_FORM_RICH
        //                ,DT_INVIO_RICH
        //                ,DT_PERV_RICH
        //                ,DT_RGSTRZ_RICH
        //                ,DT_EFF
        //                ,DT_SIN
        //                ,TP_OGG
        //                ,ID_OGG
        //                ,IB_OGG
        //                ,FL_MOD_EXEC
        //                ,ID_RICHIEDENTE
        //                ,COD_PROD
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_CAN
        //                ,IB_OGG_ORIG
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,FL_GEST_PROTEZIONE
        //             INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //             FROM RICH_EST
        //             WHERE     ID_RICH_EST = :P01-ID-RICH-EST
        //           END-EXEC.
        richEstDao.selectByP01IdRichEst(richEst.getP01IdRichEst(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: INITIALIZE WK-ID-OGG
            ws.setWkIdOggFormatted("000000000");
            // COB_CODE: MOVE P01-ID-OGG
            //             TO WK-ID-OGG
            ws.setWkIdOgg(TruncAbs.toInt(richEst.getP01IdOgg(), 9));
            // COB_CODE: MOVE WK-ID-OGG(9:1)
            //             TO P01-DS-VER
            richEst.setP01DsVerFormatted(ws.getWkIdOggFormatted().substring((9) - 1, 9));
            // COB_CODE: IF P01-DS-VER = 0
            //              MOVE 10 TO P01-DS-VER
            //           END-IF
            if (richEst.getP01DsVer() == 0) {
                // COB_CODE: MOVE 10 TO P01-DS-VER
                richEst.setP01DsVer(10);
            }
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO RICH_EST
            //                  (
            //                     ID_RICH_EST
            //                    ,ID_RICH_EST_COLLG
            //                    ,ID_LIQ
            //                    ,COD_COMP_ANIA
            //                    ,IB_RICH_EST
            //                    ,TP_MOVI
            //                    ,DT_FORM_RICH
            //                    ,DT_INVIO_RICH
            //                    ,DT_PERV_RICH
            //                    ,DT_RGSTRZ_RICH
            //                    ,DT_EFF
            //                    ,DT_SIN
            //                    ,TP_OGG
            //                    ,ID_OGG
            //                    ,IB_OGG
            //                    ,FL_MOD_EXEC
            //                    ,ID_RICHIEDENTE
            //                    ,COD_PROD
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,COD_CAN
            //                    ,IB_OGG_ORIG
            //                    ,DT_EST_FINANZ
            //                    ,DT_MAN_COP
            //                    ,FL_GEST_PROTEZIONE
            //                  )
            //              VALUES
            //                  (
            //                    :P01-ID-RICH-EST
            //                    ,:P01-ID-RICH-EST-COLLG
            //                     :IND-P01-ID-RICH-EST-COLLG
            //                    ,:P01-ID-LIQ
            //                     :IND-P01-ID-LIQ
            //                    ,:P01-COD-COMP-ANIA
            //                    ,:P01-IB-RICH-EST
            //                     :IND-P01-IB-RICH-EST
            //                    ,:P01-TP-MOVI
            //                    ,:P01-DT-FORM-RICH-DB
            //                    ,:P01-DT-INVIO-RICH-DB
            //                    ,:P01-DT-PERV-RICH-DB
            //                    ,:P01-DT-RGSTRZ-RICH-DB
            //                    ,:P01-DT-EFF-DB
            //                     :IND-P01-DT-EFF
            //                    ,:P01-DT-SIN-DB
            //                     :IND-P01-DT-SIN
            //                    ,:P01-TP-OGG
            //                    ,:P01-ID-OGG
            //                    ,:P01-IB-OGG
            //                    ,:P01-FL-MOD-EXEC
            //                    ,:P01-ID-RICHIEDENTE
            //                     :IND-P01-ID-RICHIEDENTE
            //                    ,:P01-COD-PROD
            //                    ,:P01-DS-OPER-SQL
            //                    ,:P01-DS-VER
            //                    ,:P01-DS-TS-CPTZ
            //                    ,:P01-DS-UTENTE
            //                    ,:P01-DS-STATO-ELAB
            //                    ,:P01-COD-CAN
            //                    ,:P01-IB-OGG-ORIG
            //                     :IND-P01-IB-OGG-ORIG
            //                    ,:P01-DT-EST-FINANZ-DB
            //                     :IND-P01-DT-EST-FINANZ
            //                    ,:P01-DT-MAN-COP-DB
            //                     :IND-P01-DT-MAN-COP
            //                    ,:P01-FL-GEST-PROTEZIONE
            //                     :IND-P01-FL-GEST-PROTEZIONE
            //                  )
            //           END-EXEC
            richEstDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH_EST SET
        //                   ID_RICH_EST            =
        //                :P01-ID-RICH-EST
        //                  ,ID_RICH_EST_COLLG      =
        //                :P01-ID-RICH-EST-COLLG
        //                                       :IND-P01-ID-RICH-EST-COLLG
        //                  ,ID_LIQ                 =
        //                :P01-ID-LIQ
        //                                       :IND-P01-ID-LIQ
        //                  ,COD_COMP_ANIA          =
        //                :P01-COD-COMP-ANIA
        //                  ,IB_RICH_EST            =
        //                :P01-IB-RICH-EST
        //                                       :IND-P01-IB-RICH-EST
        //                  ,TP_MOVI                =
        //                :P01-TP-MOVI
        //                  ,DT_FORM_RICH           =
        //           :P01-DT-FORM-RICH-DB
        //                  ,DT_INVIO_RICH          =
        //           :P01-DT-INVIO-RICH-DB
        //                  ,DT_PERV_RICH           =
        //           :P01-DT-PERV-RICH-DB
        //                  ,DT_RGSTRZ_RICH         =
        //           :P01-DT-RGSTRZ-RICH-DB
        //                  ,DT_EFF                 =
        //           :P01-DT-EFF-DB
        //                                       :IND-P01-DT-EFF
        //                  ,DT_SIN                 =
        //           :P01-DT-SIN-DB
        //                                       :IND-P01-DT-SIN
        //                  ,TP_OGG                 =
        //                :P01-TP-OGG
        //                  ,ID_OGG                 =
        //                :P01-ID-OGG
        //                  ,IB_OGG                 =
        //                :P01-IB-OGG
        //                  ,FL_MOD_EXEC            =
        //                :P01-FL-MOD-EXEC
        //                  ,ID_RICHIEDENTE         =
        //                :P01-ID-RICHIEDENTE
        //                                       :IND-P01-ID-RICHIEDENTE
        //                  ,COD_PROD               =
        //                :P01-COD-PROD
        //                  ,DS_OPER_SQL            =
        //                :P01-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P01-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :P01-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :P01-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P01-DS-STATO-ELAB
        //                  ,COD_CAN                =
        //                :P01-COD-CAN
        //                  ,IB_OGG_ORIG            =
        //                :P01-IB-OGG-ORIG
        //                                       :IND-P01-IB-OGG-ORIG
        //                  ,DT_EST_FINANZ          =
        //           :P01-DT-EST-FINANZ-DB
        //                                       :IND-P01-DT-EST-FINANZ
        //                  ,DT_MAN_COP             =
        //           :P01-DT-MAN-COP-DB
        //                                       :IND-P01-DT-MAN-COP
        //                  ,FL_GEST_PROTEZIONE     =
        //                :P01-FL-GEST-PROTEZIONE
        //                                       :IND-P01-FL-GEST-PROTEZIONE
        //                WHERE     ID_RICH_EST = :P01-ID-RICH-EST
        //           END-EXEC.
        richEstDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM RICH_EST
        //                WHERE     ID_RICH_EST = :P01-ID-RICH-EST
        //           END-EXEC.
        richEstDao.deleteByP01IdRichEst(richEst.getP01IdRichEst());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-NST-P01 CURSOR FOR
        //              SELECT
        //                     ID_RICH_EST
        //                    ,ID_RICH_EST_COLLG
        //                    ,ID_LIQ
        //                    ,COD_COMP_ANIA
        //                    ,IB_RICH_EST
        //                    ,TP_MOVI
        //                    ,DT_FORM_RICH
        //                    ,DT_INVIO_RICH
        //                    ,DT_PERV_RICH
        //                    ,DT_RGSTRZ_RICH
        //                    ,DT_EFF
        //                    ,DT_SIN
        //                    ,TP_OGG
        //                    ,ID_OGG
        //                    ,IB_OGG
        //                    ,FL_MOD_EXEC
        //                    ,ID_RICHIEDENTE
        //                    ,COD_PROD
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_CAN
        //                    ,IB_OGG_ORIG
        //                    ,DT_EST_FINANZ
        //                    ,DT_MAN_COP
        //                    ,FL_GEST_PROTEZIONE
        //              FROM RICH_EST
        //              WHERE     IB_OGG = :P01-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY ID_RICH_EST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_EST
        //                ,ID_RICH_EST_COLLG
        //                ,ID_LIQ
        //                ,COD_COMP_ANIA
        //                ,IB_RICH_EST
        //                ,TP_MOVI
        //                ,DT_FORM_RICH
        //                ,DT_INVIO_RICH
        //                ,DT_PERV_RICH
        //                ,DT_RGSTRZ_RICH
        //                ,DT_EFF
        //                ,DT_SIN
        //                ,TP_OGG
        //                ,ID_OGG
        //                ,IB_OGG
        //                ,FL_MOD_EXEC
        //                ,ID_RICHIEDENTE
        //                ,COD_PROD
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_CAN
        //                ,IB_OGG_ORIG
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,FL_GEST_PROTEZIONE
        //             INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //             FROM RICH_EST
        //             WHERE     IB_OGG = :P01-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        richEstDao.selectRec(richEst.getP01IbOgg(), idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-NST-P01
        //           END-EXEC.
        richEstDao.openCIboNstP01(richEst.getP01IbOgg(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-NST-P01
        //           END-EXEC.
        richEstDao.closeCIboNstP01();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-NST-P01
        //           INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //           END-EXEC.
        richEstDao.fetchCIboNstP01(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DCL-CUR-IBS-RICH-EST<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsRichEst() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-NST-P01-0 CURSOR FOR
    //              SELECT
    //                     ID_RICH_EST
    //                    ,ID_RICH_EST_COLLG
    //                    ,ID_LIQ
    //                    ,COD_COMP_ANIA
    //                    ,IB_RICH_EST
    //                    ,TP_MOVI
    //                    ,DT_FORM_RICH
    //                    ,DT_INVIO_RICH
    //                    ,DT_PERV_RICH
    //                    ,DT_RGSTRZ_RICH
    //                    ,DT_EFF
    //                    ,DT_SIN
    //                    ,TP_OGG
    //                    ,ID_OGG
    //                    ,IB_OGG
    //                    ,FL_MOD_EXEC
    //                    ,ID_RICHIEDENTE
    //                    ,COD_PROD
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,COD_CAN
    //                    ,IB_OGG_ORIG
    //                    ,DT_EST_FINANZ
    //                    ,DT_MAN_COP
    //                    ,FL_GEST_PROTEZIONE
    //              FROM RICH_EST
    //              WHERE     IB_RICH_EST = :P01-IB-RICH-EST
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //              ORDER BY ID_RICH_EST ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DCL-CUR-IBS-OGG-ORIG<br>*/
    private void a605DclCurIbsOggOrig() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-NST-P01-1 CURSOR FOR
    //              SELECT
    //                     ID_RICH_EST
    //                    ,ID_RICH_EST_COLLG
    //                    ,ID_LIQ
    //                    ,COD_COMP_ANIA
    //                    ,IB_RICH_EST
    //                    ,TP_MOVI
    //                    ,DT_FORM_RICH
    //                    ,DT_INVIO_RICH
    //                    ,DT_PERV_RICH
    //                    ,DT_RGSTRZ_RICH
    //                    ,DT_EFF
    //                    ,DT_SIN
    //                    ,TP_OGG
    //                    ,ID_OGG
    //                    ,IB_OGG
    //                    ,FL_MOD_EXEC
    //                    ,ID_RICHIEDENTE
    //                    ,COD_PROD
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,COD_CAN
    //                    ,IB_OGG_ORIG
    //                    ,DT_EST_FINANZ
    //                    ,DT_MAN_COP
    //                    ,FL_GEST_PROTEZIONE
    //              FROM RICH_EST
    //              WHERE     IB_OGG_ORIG = :P01-IB-OGG-ORIG
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //              ORDER BY ID_RICH_EST ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P01-IB-RICH-EST NOT = HIGH-VALUES
        //                  THRU A605-RICH-EST-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(richEst.getP01IbRichEst(), RichEstIdbsp010.Len.P01_IB_RICH_EST)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-RICH-EST
            //              THRU A605-RICH-EST-EX
            a605DclCurIbsRichEst();
        }
        else if (!Characters.EQ_HIGH.test(richEst.getP01IbOggOrig(), RichEstIdbsp010.Len.P01_IB_OGG_ORIG)) {
            // COB_CODE: IF P01-IB-OGG-ORIG NOT = HIGH-VALUES
            //                  THRU A605-OGG-ORIG-EX
            //           END-IF
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-OGG-ORIG
            //              THRU A605-OGG-ORIG-EX
            a605DclCurIbsOggOrig();
        }
    }

    /**Original name: A610-SELECT-IBS-RICH-EST<br>*/
    private void a610SelectIbsRichEst() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_EST
        //                ,ID_RICH_EST_COLLG
        //                ,ID_LIQ
        //                ,COD_COMP_ANIA
        //                ,IB_RICH_EST
        //                ,TP_MOVI
        //                ,DT_FORM_RICH
        //                ,DT_INVIO_RICH
        //                ,DT_PERV_RICH
        //                ,DT_RGSTRZ_RICH
        //                ,DT_EFF
        //                ,DT_SIN
        //                ,TP_OGG
        //                ,ID_OGG
        //                ,IB_OGG
        //                ,FL_MOD_EXEC
        //                ,ID_RICHIEDENTE
        //                ,COD_PROD
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_CAN
        //                ,IB_OGG_ORIG
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,FL_GEST_PROTEZIONE
        //             INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //             FROM RICH_EST
        //             WHERE     IB_RICH_EST = :P01-IB-RICH-EST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        richEstDao.selectRec1(richEst.getP01IbRichEst(), idsv0003.getCodiceCompagniaAnia(), this);
    }

    /**Original name: A610-SELECT-IBS-OGG-ORIG<br>*/
    private void a610SelectIbsOggOrig() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_EST
        //                ,ID_RICH_EST_COLLG
        //                ,ID_LIQ
        //                ,COD_COMP_ANIA
        //                ,IB_RICH_EST
        //                ,TP_MOVI
        //                ,DT_FORM_RICH
        //                ,DT_INVIO_RICH
        //                ,DT_PERV_RICH
        //                ,DT_RGSTRZ_RICH
        //                ,DT_EFF
        //                ,DT_SIN
        //                ,TP_OGG
        //                ,ID_OGG
        //                ,IB_OGG
        //                ,FL_MOD_EXEC
        //                ,ID_RICHIEDENTE
        //                ,COD_PROD
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_CAN
        //                ,IB_OGG_ORIG
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,FL_GEST_PROTEZIONE
        //             INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //             FROM RICH_EST
        //             WHERE     IB_OGG_ORIG = :P01-IB-OGG-ORIG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        richEstDao.selectRec2(richEst.getP01IbOggOrig(), idsv0003.getCodiceCompagniaAnia(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P01-IB-RICH-EST NOT = HIGH-VALUES
        //                  THRU A610-RICH-EST-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(richEst.getP01IbRichEst(), RichEstIdbsp010.Len.P01_IB_RICH_EST)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-RICH-EST
            //              THRU A610-RICH-EST-EX
            a610SelectIbsRichEst();
        }
        else if (!Characters.EQ_HIGH.test(richEst.getP01IbOggOrig(), RichEstIdbsp010.Len.P01_IB_OGG_ORIG)) {
            // COB_CODE: IF P01-IB-OGG-ORIG NOT = HIGH-VALUES
            //                  THRU A610-OGG-ORIG-EX
            //           END-IF
            // COB_CODE: PERFORM A610-SELECT-IBS-OGG-ORIG
            //              THRU A610-OGG-ORIG-EX
            a610SelectIbsOggOrig();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF P01-IB-RICH-EST NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(richEst.getP01IbRichEst(), RichEstIdbsp010.Len.P01_IB_RICH_EST)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-NST-P01-0
            //           END-EXEC
            richEstDao.openCIbsNstP010(richEst.getP01IbRichEst(), idsv0003.getCodiceCompagniaAnia());
        }
        else if (!Characters.EQ_HIGH.test(richEst.getP01IbOggOrig(), RichEstIdbsp010.Len.P01_IB_OGG_ORIG)) {
            // COB_CODE: IF P01-IB-OGG-ORIG NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-NST-P01-1
            //           END-EXEC
            richEstDao.openCIbsNstP011(richEst.getP01IbOggOrig(), idsv0003.getCodiceCompagniaAnia());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF P01-IB-RICH-EST NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(richEst.getP01IbRichEst(), RichEstIdbsp010.Len.P01_IB_RICH_EST)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-NST-P01-0
            //           END-EXEC
            richEstDao.closeCIbsNstP010();
        }
        else if (!Characters.EQ_HIGH.test(richEst.getP01IbOggOrig(), RichEstIdbsp010.Len.P01_IB_OGG_ORIG)) {
            // COB_CODE: IF P01-IB-OGG-ORIG NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-NST-P01-1
            //           END-EXEC
            richEstDao.closeCIbsNstP011();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-RICH-EST<br>*/
    private void a690FnIbsRichEst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-NST-P01-0
        //           INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //           END-EXEC.
        richEstDao.fetchCIbsNstP010(this);
    }

    /**Original name: A690-FN-IBS-OGG-ORIG<br>*/
    private void a690FnIbsOggOrig() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-NST-P01-1
        //           INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //           END-EXEC.
        richEstDao.fetchCIbsNstP011(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF P01-IB-RICH-EST NOT = HIGH-VALUES
        //                  THRU A690-RICH-EST-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(richEst.getP01IbRichEst(), RichEstIdbsp010.Len.P01_IB_RICH_EST)) {
            // COB_CODE: PERFORM A690-FN-IBS-RICH-EST
            //              THRU A690-RICH-EST-EX
            a690FnIbsRichEst();
        }
        else if (!Characters.EQ_HIGH.test(richEst.getP01IbOggOrig(), RichEstIdbsp010.Len.P01_IB_OGG_ORIG)) {
            // COB_CODE: IF P01-IB-OGG-ORIG NOT = HIGH-VALUES
            //                  THRU A690-OGG-ORIG-EX
            //           END-IF
            // COB_CODE: PERFORM A690-FN-IBS-OGG-ORIG
            //              THRU A690-OGG-ORIG-EX
            a690FnIbsOggOrig();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-NST-P01 CURSOR FOR
        //              SELECT
        //                     ID_RICH_EST
        //                    ,ID_RICH_EST_COLLG
        //                    ,ID_LIQ
        //                    ,COD_COMP_ANIA
        //                    ,IB_RICH_EST
        //                    ,TP_MOVI
        //                    ,DT_FORM_RICH
        //                    ,DT_INVIO_RICH
        //                    ,DT_PERV_RICH
        //                    ,DT_RGSTRZ_RICH
        //                    ,DT_EFF
        //                    ,DT_SIN
        //                    ,TP_OGG
        //                    ,ID_OGG
        //                    ,IB_OGG
        //                    ,FL_MOD_EXEC
        //                    ,ID_RICHIEDENTE
        //                    ,COD_PROD
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_CAN
        //                    ,IB_OGG_ORIG
        //                    ,DT_EST_FINANZ
        //                    ,DT_MAN_COP
        //                    ,FL_GEST_PROTEZIONE
        //              FROM RICH_EST
        //              WHERE     ID_OGG = :P01-ID-OGG
        //                    AND TP_OGG = :P01-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY ID_RICH_EST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_EST
        //                ,ID_RICH_EST_COLLG
        //                ,ID_LIQ
        //                ,COD_COMP_ANIA
        //                ,IB_RICH_EST
        //                ,TP_MOVI
        //                ,DT_FORM_RICH
        //                ,DT_INVIO_RICH
        //                ,DT_PERV_RICH
        //                ,DT_RGSTRZ_RICH
        //                ,DT_EFF
        //                ,DT_SIN
        //                ,TP_OGG
        //                ,ID_OGG
        //                ,IB_OGG
        //                ,FL_MOD_EXEC
        //                ,ID_RICHIEDENTE
        //                ,COD_PROD
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_CAN
        //                ,IB_OGG_ORIG
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,FL_GEST_PROTEZIONE
        //             INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //             FROM RICH_EST
        //             WHERE     ID_OGG = :P01-ID-OGG
        //                    AND TP_OGG = :P01-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        richEstDao.selectRec3(richEst.getP01IdOgg(), richEst.getP01TpOgg(), idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-NST-P01
        //           END-EXEC.
        richEstDao.openCIdoNstP01(richEst.getP01IdOgg(), richEst.getP01TpOgg(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-NST-P01
        //           END-EXEC.
        richEstDao.closeCIdoNstP01();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-NST-P01
        //           INTO
        //                :P01-ID-RICH-EST
        //               ,:P01-ID-RICH-EST-COLLG
        //                :IND-P01-ID-RICH-EST-COLLG
        //               ,:P01-ID-LIQ
        //                :IND-P01-ID-LIQ
        //               ,:P01-COD-COMP-ANIA
        //               ,:P01-IB-RICH-EST
        //                :IND-P01-IB-RICH-EST
        //               ,:P01-TP-MOVI
        //               ,:P01-DT-FORM-RICH-DB
        //               ,:P01-DT-INVIO-RICH-DB
        //               ,:P01-DT-PERV-RICH-DB
        //               ,:P01-DT-RGSTRZ-RICH-DB
        //               ,:P01-DT-EFF-DB
        //                :IND-P01-DT-EFF
        //               ,:P01-DT-SIN-DB
        //                :IND-P01-DT-SIN
        //               ,:P01-TP-OGG
        //               ,:P01-ID-OGG
        //               ,:P01-IB-OGG
        //               ,:P01-FL-MOD-EXEC
        //               ,:P01-ID-RICHIEDENTE
        //                :IND-P01-ID-RICHIEDENTE
        //               ,:P01-COD-PROD
        //               ,:P01-DS-OPER-SQL
        //               ,:P01-DS-VER
        //               ,:P01-DS-TS-CPTZ
        //               ,:P01-DS-UTENTE
        //               ,:P01-DS-STATO-ELAB
        //               ,:P01-COD-CAN
        //               ,:P01-IB-OGG-ORIG
        //                :IND-P01-IB-OGG-ORIG
        //               ,:P01-DT-EST-FINANZ-DB
        //                :IND-P01-DT-EST-FINANZ
        //               ,:P01-DT-MAN-COP-DB
        //                :IND-P01-DT-MAN-COP
        //               ,:P01-FL-GEST-PROTEZIONE
        //                :IND-P01-FL-GEST-PROTEZIONE
        //           END-EXEC.
        richEstDao.fetchCIdoNstP01(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P01-ID-RICH-EST-COLLG = -1
        //              MOVE HIGH-VALUES TO P01-ID-RICH-EST-COLLG-NULL
        //           END-IF
        if (ws.getIndRichEst().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-ID-RICH-EST-COLLG-NULL
            richEst.getP01IdRichEstCollg().setP01IdRichEstCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01IdRichEstCollg.Len.P01_ID_RICH_EST_COLLG_NULL));
        }
        // COB_CODE: IF IND-P01-ID-LIQ = -1
        //              MOVE HIGH-VALUES TO P01-ID-LIQ-NULL
        //           END-IF
        if (ws.getIndRichEst().getDtSin1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-ID-LIQ-NULL
            richEst.getP01IdLiq().setP01IdLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01IdLiq.Len.P01_ID_LIQ_NULL));
        }
        // COB_CODE: IF IND-P01-IB-RICH-EST = -1
        //              MOVE HIGH-VALUES TO P01-IB-RICH-EST-NULL
        //           END-IF
        if (ws.getIndRichEst().getCauSin1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-IB-RICH-EST-NULL
            richEst.setP01IbRichEst(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichEstIdbsp010.Len.P01_IB_RICH_EST));
        }
        // COB_CODE: IF IND-P01-DT-EFF = -1
        //              MOVE HIGH-VALUES TO P01-DT-EFF-NULL
        //           END-IF
        if (ws.getIndRichEst().getTpSin1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-DT-EFF-NULL
            richEst.getP01DtEff().setP01DtEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01DtEff.Len.P01_DT_EFF_NULL));
        }
        // COB_CODE: IF IND-P01-DT-SIN = -1
        //              MOVE HIGH-VALUES TO P01-DT-SIN-NULL
        //           END-IF
        if (ws.getIndRichEst().getDtSin2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-DT-SIN-NULL
            richEst.getP01DtSin().setP01DtSinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01DtSin.Len.P01_DT_SIN_NULL));
        }
        // COB_CODE: IF IND-P01-ID-RICHIEDENTE = -1
        //              MOVE HIGH-VALUES TO P01-ID-RICHIEDENTE-NULL
        //           END-IF
        if (ws.getIndRichEst().getCauSin2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-ID-RICHIEDENTE-NULL
            richEst.getP01IdRichiedente().setP01IdRichiedenteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01IdRichiedente.Len.P01_ID_RICHIEDENTE_NULL));
        }
        // COB_CODE: IF IND-P01-IB-OGG-ORIG = -1
        //              MOVE HIGH-VALUES TO P01-IB-OGG-ORIG-NULL
        //           END-IF
        if (ws.getIndRichEst().getTpSin2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-IB-OGG-ORIG-NULL
            richEst.setP01IbOggOrig(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichEstIdbsp010.Len.P01_IB_OGG_ORIG));
        }
        // COB_CODE: IF IND-P01-DT-EST-FINANZ = -1
        //              MOVE HIGH-VALUES TO P01-DT-EST-FINANZ-NULL
        //           END-IF
        if (ws.getIndRichEst().getDtSin3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-DT-EST-FINANZ-NULL
            richEst.getP01DtEstFinanz().setP01DtEstFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01DtEstFinanz.Len.P01_DT_EST_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P01-DT-MAN-COP = -1
        //              MOVE HIGH-VALUES TO P01-DT-MAN-COP-NULL
        //           END-IF
        if (ws.getIndRichEst().getCauSin3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-DT-MAN-COP-NULL
            richEst.getP01DtManCop().setP01DtManCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P01DtManCop.Len.P01_DT_MAN_COP_NULL));
        }
        // COB_CODE: IF IND-P01-FL-GEST-PROTEZIONE = -1
        //              MOVE HIGH-VALUES TO P01-FL-GEST-PROTEZIONE-NULL
        //           END-IF.
        if (ws.getIndRichEst().getTpSin3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P01-FL-GEST-PROTEZIONE-NULL
            richEst.setP01FlGestProtezione(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO P01-DS-OPER-SQL
        richEst.setP01DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO P01-DS-VER
        richEst.setP01DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P01-DS-UTENTE
        richEst.setP01DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO P01-DS-STATO-ELAB
        richEst.setP01DsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO P01-DS-TS-CPTZ.
        richEst.setP01DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO P01-DS-OPER-SQL
        richEst.setP01DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P01-DS-UTENTE
        richEst.setP01DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO P01-DS-TS-CPTZ.
        richEst.setP01DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF P01-ID-RICH-EST-COLLG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-ID-RICH-EST-COLLG
        //           ELSE
        //              MOVE 0 TO IND-P01-ID-RICH-EST-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01IdRichEstCollg().getP01IdRichEstCollgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-ID-RICH-EST-COLLG
            ws.getIndRichEst().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-ID-RICH-EST-COLLG
            ws.getIndRichEst().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF P01-ID-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-ID-LIQ
        //           ELSE
        //              MOVE 0 TO IND-P01-ID-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01IdLiq().getP01IdLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-ID-LIQ
            ws.getIndRichEst().setDtSin1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-ID-LIQ
            ws.getIndRichEst().setDtSin1oAssto(((short)0));
        }
        // COB_CODE: IF P01-IB-RICH-EST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-IB-RICH-EST
        //           ELSE
        //              MOVE 0 TO IND-P01-IB-RICH-EST
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01IbRichEst(), RichEstIdbsp010.Len.P01_IB_RICH_EST)) {
            // COB_CODE: MOVE -1 TO IND-P01-IB-RICH-EST
            ws.getIndRichEst().setCauSin1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-IB-RICH-EST
            ws.getIndRichEst().setCauSin1oAssto(((short)0));
        }
        // COB_CODE: IF P01-DT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-DT-EFF
        //           ELSE
        //              MOVE 0 TO IND-P01-DT-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01DtEff().getP01DtEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-DT-EFF
            ws.getIndRichEst().setTpSin1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-DT-EFF
            ws.getIndRichEst().setTpSin1oAssto(((short)0));
        }
        // COB_CODE: IF P01-DT-SIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-DT-SIN
        //           ELSE
        //              MOVE 0 TO IND-P01-DT-SIN
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01DtSin().getP01DtSinNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-DT-SIN
            ws.getIndRichEst().setDtSin2oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-DT-SIN
            ws.getIndRichEst().setDtSin2oAssto(((short)0));
        }
        // COB_CODE: IF P01-ID-RICHIEDENTE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-ID-RICHIEDENTE
        //           ELSE
        //              MOVE 0 TO IND-P01-ID-RICHIEDENTE
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01IdRichiedente().getP01IdRichiedenteNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-ID-RICHIEDENTE
            ws.getIndRichEst().setCauSin2oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-ID-RICHIEDENTE
            ws.getIndRichEst().setCauSin2oAssto(((short)0));
        }
        // COB_CODE: IF P01-IB-OGG-ORIG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-IB-OGG-ORIG
        //           ELSE
        //              MOVE 0 TO IND-P01-IB-OGG-ORIG
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01IbOggOrig(), RichEstIdbsp010.Len.P01_IB_OGG_ORIG)) {
            // COB_CODE: MOVE -1 TO IND-P01-IB-OGG-ORIG
            ws.getIndRichEst().setTpSin2oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-IB-OGG-ORIG
            ws.getIndRichEst().setTpSin2oAssto(((short)0));
        }
        // COB_CODE: IF P01-DT-EST-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-DT-EST-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P01-DT-EST-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01DtEstFinanz().getP01DtEstFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-DT-EST-FINANZ
            ws.getIndRichEst().setDtSin3oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-DT-EST-FINANZ
            ws.getIndRichEst().setDtSin3oAssto(((short)0));
        }
        // COB_CODE: IF P01-DT-MAN-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-DT-MAN-COP
        //           ELSE
        //              MOVE 0 TO IND-P01-DT-MAN-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(richEst.getP01DtManCop().getP01DtManCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P01-DT-MAN-COP
            ws.getIndRichEst().setCauSin3oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-DT-MAN-COP
            ws.getIndRichEst().setCauSin3oAssto(((short)0));
        }
        // COB_CODE: IF P01-FL-GEST-PROTEZIONE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P01-FL-GEST-PROTEZIONE
        //           ELSE
        //              MOVE 0 TO IND-P01-FL-GEST-PROTEZIONE
        //           END-IF.
        if (Conditions.eq(richEst.getP01FlGestProtezione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-P01-FL-GEST-PROTEZIONE
            ws.getIndRichEst().setTpSin3oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P01-FL-GEST-PROTEZIONE
            ws.getIndRichEst().setTpSin3oAssto(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE P01-DT-FORM-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtFormRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P01-DT-FORM-RICH-DB
        ws.getRichEstDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P01-DT-INVIO-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtInvioRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P01-DT-INVIO-RICH-DB
        ws.getRichEstDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P01-DT-PERV-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtPervRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P01-DT-PERV-RICH-DB
        ws.getRichEstDb().setDecorDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P01-DT-RGSTRZ-RICH TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtRgstrzRich(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P01-DT-RGSTRZ-RICH-DB
        ws.getRichEstDb().setScadDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-P01-DT-EFF = 0
        //               MOVE WS-DATE-X      TO P01-DT-EFF-DB
        //           END-IF
        if (ws.getIndRichEst().getTpSin1oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-EFF TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtEff().getP01DtEff(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P01-DT-EFF-DB
            ws.getRichEstDb().setIniValTarDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P01-DT-SIN = 0
        //               MOVE WS-DATE-X      TO P01-DT-SIN-DB
        //           END-IF
        if (ws.getIndRichEst().getDtSin2oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-SIN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtSin().getP01DtSin(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P01-DT-SIN-DB
            ws.getRichEstDb().setEndCarzDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P01-DT-EST-FINANZ = 0
        //               MOVE WS-DATE-X      TO P01-DT-EST-FINANZ-DB
        //           END-IF
        if (ws.getIndRichEst().getDtSin3oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-EST-FINANZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtEstFinanz().getP01DtEstFinanz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P01-DT-EST-FINANZ-DB
            ws.getRichEstDb().setVarzTpIasDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P01-DT-MAN-COP = 0
        //               MOVE WS-DATE-X      TO P01-DT-MAN-COP-DB
        //           END-IF.
        if (ws.getIndRichEst().getCauSin3oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-MAN-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richEst.getP01DtManCop().getP01DtManCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P01-DT-MAN-COP-DB
            ws.getRichEstDb().setPrescDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P01-DT-FORM-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P01-DT-FORM-RICH
        richEst.setP01DtFormRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P01-DT-INVIO-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P01-DT-INVIO-RICH
        richEst.setP01DtInvioRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P01-DT-PERV-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P01-DT-PERV-RICH
        richEst.setP01DtPervRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P01-DT-RGSTRZ-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getScadDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P01-DT-RGSTRZ-RICH
        richEst.setP01DtRgstrzRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-P01-DT-EFF = 0
        //               MOVE WS-DATE-N      TO P01-DT-EFF
        //           END-IF
        if (ws.getIndRichEst().getTpSin1oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-EFF-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P01-DT-EFF
            richEst.getP01DtEff().setP01DtEff(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P01-DT-SIN = 0
        //               MOVE WS-DATE-N      TO P01-DT-SIN
        //           END-IF
        if (ws.getIndRichEst().getDtSin2oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-SIN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getEndCarzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P01-DT-SIN
            richEst.getP01DtSin().setP01DtSin(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P01-DT-EST-FINANZ = 0
        //               MOVE WS-DATE-N      TO P01-DT-EST-FINANZ
        //           END-IF
        if (ws.getIndRichEst().getDtSin3oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-EST-FINANZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P01-DT-EST-FINANZ
            richEst.getP01DtEstFinanz().setP01DtEstFinanz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P01-DT-MAN-COP = 0
        //               MOVE WS-DATE-N      TO P01-DT-MAN-COP
        //           END-IF.
        if (ws.getIndRichEst().getCauSin3oAssto() == 0) {
            // COB_CODE: MOVE P01-DT-MAN-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichEstDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P01-DT-MAN-COP
            richEst.getP01DtManCop().setP01DtManCop(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCan() {
        return richEst.getP01CodCan();
    }

    @Override
    public void setCodCan(int codCan) {
        this.richEst.setP01CodCan(codCan);
    }

    @Override
    public int getCodCompAnia() {
        return richEst.getP01CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.richEst.setP01CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodProd() {
        return richEst.getP01CodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.richEst.setP01CodProd(codProd);
    }

    @Override
    public char getDsOperSql() {
        return richEst.getP01DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.richEst.setP01DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return richEst.getP01DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.richEst.setP01DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return richEst.getP01DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.richEst.setP01DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return richEst.getP01DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.richEst.setP01DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return richEst.getP01DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.richEst.setP01DsVer(dsVer);
    }

    @Override
    public String getDtEffDb() {
        return ws.getRichEstDb().getIniValTarDb();
    }

    @Override
    public void setDtEffDb(String dtEffDb) {
        this.ws.getRichEstDb().setIniValTarDb(dtEffDb);
    }

    @Override
    public String getDtEffDbObj() {
        if (ws.getIndRichEst().getTpSin1oAssto() >= 0) {
            return getDtEffDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffDbObj(String dtEffDbObj) {
        if (dtEffDbObj != null) {
            setDtEffDb(dtEffDbObj);
            ws.getIndRichEst().setTpSin1oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setTpSin1oAssto(((short)-1));
        }
    }

    @Override
    public String getDtEstFinanzDb() {
        return ws.getRichEstDb().getVarzTpIasDb();
    }

    @Override
    public void setDtEstFinanzDb(String dtEstFinanzDb) {
        this.ws.getRichEstDb().setVarzTpIasDb(dtEstFinanzDb);
    }

    @Override
    public String getDtEstFinanzDbObj() {
        if (ws.getIndRichEst().getDtSin3oAssto() >= 0) {
            return getDtEstFinanzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEstFinanzDbObj(String dtEstFinanzDbObj) {
        if (dtEstFinanzDbObj != null) {
            setDtEstFinanzDb(dtEstFinanzDbObj);
            ws.getIndRichEst().setDtSin3oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setDtSin3oAssto(((short)-1));
        }
    }

    @Override
    public String getDtFormRichDb() {
        return ws.getRichEstDb().getIniEffDb();
    }

    @Override
    public void setDtFormRichDb(String dtFormRichDb) {
        this.ws.getRichEstDb().setIniEffDb(dtFormRichDb);
    }

    @Override
    public String getDtInvioRichDb() {
        return ws.getRichEstDb().getEndEffDb();
    }

    @Override
    public void setDtInvioRichDb(String dtInvioRichDb) {
        this.ws.getRichEstDb().setEndEffDb(dtInvioRichDb);
    }

    @Override
    public String getDtManCopDb() {
        return ws.getRichEstDb().getPrescDb();
    }

    @Override
    public void setDtManCopDb(String dtManCopDb) {
        this.ws.getRichEstDb().setPrescDb(dtManCopDb);
    }

    @Override
    public String getDtManCopDbObj() {
        if (ws.getIndRichEst().getCauSin3oAssto() >= 0) {
            return getDtManCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtManCopDbObj(String dtManCopDbObj) {
        if (dtManCopDbObj != null) {
            setDtManCopDb(dtManCopDbObj);
            ws.getIndRichEst().setCauSin3oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setCauSin3oAssto(((short)-1));
        }
    }

    @Override
    public String getDtPervRichDb() {
        return ws.getRichEstDb().getDecorDb();
    }

    @Override
    public void setDtPervRichDb(String dtPervRichDb) {
        this.ws.getRichEstDb().setDecorDb(dtPervRichDb);
    }

    @Override
    public String getDtRgstrzRichDb() {
        return ws.getRichEstDb().getScadDb();
    }

    @Override
    public void setDtRgstrzRichDb(String dtRgstrzRichDb) {
        this.ws.getRichEstDb().setScadDb(dtRgstrzRichDb);
    }

    @Override
    public String getDtSinDb() {
        return ws.getRichEstDb().getEndCarzDb();
    }

    @Override
    public void setDtSinDb(String dtSinDb) {
        this.ws.getRichEstDb().setEndCarzDb(dtSinDb);
    }

    @Override
    public String getDtSinDbObj() {
        if (ws.getIndRichEst().getDtSin2oAssto() >= 0) {
            return getDtSinDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtSinDbObj(String dtSinDbObj) {
        if (dtSinDbObj != null) {
            setDtSinDb(dtSinDbObj);
            ws.getIndRichEst().setDtSin2oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setDtSin2oAssto(((short)-1));
        }
    }

    @Override
    public char getFlGestProtezione() {
        return richEst.getP01FlGestProtezione();
    }

    @Override
    public void setFlGestProtezione(char flGestProtezione) {
        this.richEst.setP01FlGestProtezione(flGestProtezione);
    }

    @Override
    public Character getFlGestProtezioneObj() {
        if (ws.getIndRichEst().getTpSin3oAssto() >= 0) {
            return ((Character)getFlGestProtezione());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlGestProtezioneObj(Character flGestProtezioneObj) {
        if (flGestProtezioneObj != null) {
            setFlGestProtezione(((char)flGestProtezioneObj));
            ws.getIndRichEst().setTpSin3oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setTpSin3oAssto(((short)-1));
        }
    }

    @Override
    public char getFlModExec() {
        return richEst.getP01FlModExec();
    }

    @Override
    public void setFlModExec(char flModExec) {
        this.richEst.setP01FlModExec(flModExec);
    }

    @Override
    public String getIbOgg() {
        return richEst.getP01IbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.richEst.setP01IbOgg(ibOgg);
    }

    @Override
    public String getIbOggOrig() {
        return richEst.getP01IbOggOrig();
    }

    @Override
    public void setIbOggOrig(String ibOggOrig) {
        this.richEst.setP01IbOggOrig(ibOggOrig);
    }

    @Override
    public String getIbOggOrigObj() {
        if (ws.getIndRichEst().getTpSin2oAssto() >= 0) {
            return getIbOggOrig();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggOrigObj(String ibOggOrigObj) {
        if (ibOggOrigObj != null) {
            setIbOggOrig(ibOggOrigObj);
            ws.getIndRichEst().setTpSin2oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setTpSin2oAssto(((short)-1));
        }
    }

    @Override
    public String getIbRichEst() {
        return richEst.getP01IbRichEst();
    }

    @Override
    public void setIbRichEst(String ibRichEst) {
        this.richEst.setP01IbRichEst(ibRichEst);
    }

    @Override
    public String getIbRichEstObj() {
        if (ws.getIndRichEst().getCauSin1oAssto() >= 0) {
            return getIbRichEst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbRichEstObj(String ibRichEstObj) {
        if (ibRichEstObj != null) {
            setIbRichEst(ibRichEstObj);
            ws.getIndRichEst().setCauSin1oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setCauSin1oAssto(((short)-1));
        }
    }

    @Override
    public int getIdLiq() {
        return richEst.getP01IdLiq().getP01IdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.richEst.getP01IdLiq().setP01IdLiq(idLiq);
    }

    @Override
    public Integer getIdLiqObj() {
        if (ws.getIndRichEst().getDtSin1oAssto() >= 0) {
            return ((Integer)getIdLiq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdLiqObj(Integer idLiqObj) {
        if (idLiqObj != null) {
            setIdLiq(((int)idLiqObj));
            ws.getIndRichEst().setDtSin1oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setDtSin1oAssto(((short)-1));
        }
    }

    @Override
    public int getIdOgg() {
        return richEst.getP01IdOgg();
    }

    @Override
    public void setIdOgg(int idOgg) {
        this.richEst.setP01IdOgg(idOgg);
    }

    @Override
    public int getIdRichEstCollg() {
        return richEst.getP01IdRichEstCollg().getP01IdRichEstCollg();
    }

    @Override
    public void setIdRichEstCollg(int idRichEstCollg) {
        this.richEst.getP01IdRichEstCollg().setP01IdRichEstCollg(idRichEstCollg);
    }

    @Override
    public Integer getIdRichEstCollgObj() {
        if (ws.getIndRichEst().getIdMoviChiu() >= 0) {
            return ((Integer)getIdRichEstCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichEstCollgObj(Integer idRichEstCollgObj) {
        if (idRichEstCollgObj != null) {
            setIdRichEstCollg(((int)idRichEstCollgObj));
            ws.getIndRichEst().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndRichEst().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdRichiedente() {
        return richEst.getP01IdRichiedente().getP01IdRichiedente();
    }

    @Override
    public void setIdRichiedente(int idRichiedente) {
        this.richEst.getP01IdRichiedente().setP01IdRichiedente(idRichiedente);
    }

    @Override
    public Integer getIdRichiedenteObj() {
        if (ws.getIndRichEst().getCauSin2oAssto() >= 0) {
            return ((Integer)getIdRichiedente());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichiedenteObj(Integer idRichiedenteObj) {
        if (idRichiedenteObj != null) {
            setIdRichiedente(((int)idRichiedenteObj));
            ws.getIndRichEst().setCauSin2oAssto(((short)0));
        }
        else {
            ws.getIndRichEst().setCauSin2oAssto(((short)-1));
        }
    }

    @Override
    public int getP01IdRichEst() {
        return richEst.getP01IdRichEst();
    }

    @Override
    public void setP01IdRichEst(int p01IdRichEst) {
        this.richEst.setP01IdRichEst(p01IdRichEst);
    }

    @Override
    public int getTpMovi() {
        return richEst.getP01TpMovi();
    }

    @Override
    public void setTpMovi(int tpMovi) {
        this.richEst.setP01TpMovi(tpMovi);
    }

    @Override
    public String getTpOgg() {
        return richEst.getP01TpOgg();
    }

    @Override
    public void setTpOgg(String tpOgg) {
        this.richEst.setP01TpOgg(tpOgg);
    }
}
