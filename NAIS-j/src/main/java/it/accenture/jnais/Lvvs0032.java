package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.InputLvvs0000;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0032Data;
import it.accenture.jnais.ws.WkDataInput;

/**Original name: LVVS0032<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0026
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... MODULO DI CALCOLO
 *   DESCRIZIONE.... Ricava dalla POLIZZA una data
 *                   in formato AAA,MM
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0032 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0032Data ws = new Lvvs0032Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0032
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0032_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM L000-OPERAZIONI-INIZIALI
        //              THRU L000-EX.
        l000OperazioniIniziali();
        // COB_CODE: PERFORM L100-ELABORAZIONE
        //              THRU L100-EX.
        l100Elaborazione();
        // COB_CODE: PERFORM L900-OPERAZIONI-FINALI
        //              THRU L900-EX.
        l900OperazioniFinali();
        return 0;
    }

    public static Lvvs0032 getInstance() {
        return ((Lvvs0032)Programs.getInstance(Lvvs0032.class));
    }

    /**Original name: L000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void l000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IVVC0213-TAB-OUTPUT
        //                                             WK-DATA-INPUT.
        initTabOutput();
        initWkDataInput();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET STRUTTURA-TROVATA-NO          TO TRUE.
        ws.getFlagStrutturaTrovata().setNo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: L100-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DELLA COPY IVVC0214</pre>*/
    private void l100Elaborazione() {
        // COB_CODE: PERFORM L500-VALORIZZA-DCLGEN THRU L500-EX
        //                   VARYING IND-STR FROM 1 BY 1
        //                     UNTIL IND-STR > IVVC0213-ELE-INFO-MAX OR
        //                           IVVC0213-TAB-ALIAS(IND-STR) =
        //                      SPACES OR LOW-VALUE OR HIGH-VALUE
        ConcatUtil concatUtil = null;
        ws.setIndStr(((short)1));
        while (!(ws.getIndStr() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIndStr()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIndStr()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIndStr()).getIvvc0213TabAliasFormatted()))) {
            l500ValorizzaDclgen();
            ws.setIndStr(Trunc.toShort(ws.getIndStr() + 1, 4));
        }
        // COB_CODE: IF STRUTTURA-TROVATA-SI
        //              PERFORM L700-CALL-LVVS0000         THRU L700-EX
        //           ELSE
        //                SET IDSV0003-FIELD-NOT-VALUED    TO TRUE
        //           END-IF.
        if (ws.getFlagStrutturaTrovata().isSi()) {
            // COB_CODE: PERFORM L700-CALL-LVVS0000         THRU L700-EX
            l700CallLvvs0000();
        }
        else {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'STRUTTURA NON TROVATA'
            //                  ' : '
            //                  ALIAS-POLI
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //            END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "STRUTTURA NON TROVATA", " : ", ws.getAliasPoliFormatted());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED    TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: L500-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DELLA COPY IVVC0214
	 * ----------------------------------------------------------------*</pre>*/
    private void l500ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IND-STR) = IVVC0213-ALIAS-ADES
        //                SET STRUTTURA-TROVATA-SI TO TRUE
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIndStr()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IND-STR) :
            //                IVVC0213-LUNGHEZZA(IND-STR))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIndStr()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIndStr()).getPosizIni() + ivvc0213.getTabInfo(ws.getIndStr()).getLunghezza() - 1));
            // COB_CODE: SET STRUTTURA-TROVATA-SI TO TRUE
            ws.getFlagStrutturaTrovata().setSi();
        }
    }

    /**Original name: L600-PREPARA-CALL<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA CALL LVVS0000
	 * ----------------------------------------------------------------*
	 * --> COPY LVVC0000</pre>*/
    private void l600PreparaCall() {
        // COB_CODE: INITIALIZE INPUT-LVVS0000.
        initInputLvvs0000();
        // COB_CODE: SET LVVC0000-AAA-V-MM         TO TRUE.
        ws.getInputLvvs0000().getFormatDate().setLvvc0000AaaVMm();
        // COB_CODE: IF DADE-DUR-AA IS NUMERIC
        //              MOVE DADE-DUR-AA            TO WK-AAAA-INPUT
        //           ELSE
        //              MOVE ZEROES                 TO WK-AAAA-INPUT
        //           END-IF.
        if (Functions.isNumber(ws.getLccvade1().getDati().getWadeDurAa().getWadeDurAa())) {
            // COB_CODE: MOVE DADE-DUR-AA            TO WK-AAAA-INPUT
            ws.getWkDataInput().setAaaaInput(TruncAbs.toShort(ws.getLccvade1().getDati().getWadeDurAa().getWadeDurAa(), 4));
        }
        else {
            // COB_CODE: MOVE ZEROES                 TO WK-AAAA-INPUT
            ws.getWkDataInput().setAaaaInput(((short)0));
        }
        // COB_CODE: IF DADE-DUR-MM IS NUMERIC
        //              MOVE DADE-DUR-MM            TO WK-MM-INPUT
        //           ELSE
        //              MOVE ZEROES                 TO WK-MM-INPUT
        //           END-IF.
        if (Functions.isNumber(ws.getLccvade1().getDati().getWadeDurMm().getWadeDurMm())) {
            // COB_CODE: MOVE DADE-DUR-MM            TO WK-MM-INPUT
            ws.getWkDataInput().setMmInput(TruncAbs.toShort(ws.getLccvade1().getDati().getWadeDurMm().getWadeDurMm(), 2));
        }
        else {
            // COB_CODE: MOVE ZEROES                 TO WK-MM-INPUT
            ws.getWkDataInput().setMmInput(((short)0));
        }
        // COB_CODE: IF DADE-DUR-GG IS NUMERIC
        //              MOVE DADE-DUR-GG            TO WK-GG-INPUT
        //           ELSE
        //              MOVE ZEROES                 TO WK-GG-INPUT
        //           END-IF.
        if (Functions.isNumber(ws.getLccvade1().getDati().getWadeDurGg().getWadeDurGg())) {
            // COB_CODE: MOVE DADE-DUR-GG            TO WK-GG-INPUT
            ws.getWkDataInput().setGgInput(TruncAbs.toShort(ws.getLccvade1().getDati().getWadeDurGg().getWadeDurGg(), 2));
        }
        else {
            // COB_CODE: MOVE ZEROES                 TO WK-GG-INPUT
            ws.getWkDataInput().setGgInput(((short)0));
        }
        // COB_CODE: MOVE WK-DATA-INPUT             TO LVVC0000-DATA-INPUT-1.
        ws.getInputLvvs0000().setDataInput1FromBuffer(ws.getWkDataInput().getWkDataInputBytes());
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
    }

    /**Original name: L700-CALL-LVVS0000<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALL LDBS1650
	 * ----------------------------------------------------------------*</pre>*/
    private void l700CallLvvs0000() {
        Lvvs0000 lvvs0000 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM L600-PREPARA-CALL  THRU L600-EX
        l600PreparaCall();
        //
        // COB_CODE:      CALL PGM-LVVS0000  USING  IDSV0003 INPUT-LVVS0000
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            lvvs0000 = Lvvs0000.getInstance();
            lvvs0000.run(idsv0003, ws.getInputLvvs0000());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE PGM-LVVS0000
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmLvvs0000());
            // COB_CODE: MOVE 'CALL LVVS0000 ERRORE CHIAMATA'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL LVVS0000 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE LVVC0000-DATA-OUTPUT    TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LVVC0000-DATA-OUTPUT    TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getInputLvvs0000().getDataOutput(), 18, 7));
        }
        else {
            // COB_CODE: MOVE PGM-LVVS0000            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmLvvs0000());
            // COB_CODE: STRING 'ERRORE ELABORAZIONE LVVS0000'
            //                  IDSV0003-RETURN-CODE
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE ELABORAZIONE LVVS0000", idsv0003.getReturnCode().getReturnCodeFormatted());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: L900-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void l900OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkDataInput() {
        ws.getWkDataInput().setAaaaInputFormatted("0000");
        ws.getWkDataInput().setMmInputFormatted("00");
        ws.getWkDataInput().setGgInputFormatted("00");
    }

    public void initInputLvvs0000() {
        ws.getInputLvvs0000().getFormatDate().setFormatDate(Types.SPACE_CHAR);
        ws.getInputLvvs0000().setDataInputFormatted("00000000");
        ws.getInputLvvs0000().setDataInput1Formatted("00000000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000AnniInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000MesiInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000GiorniInput2Formatted("00000");
        ws.getInputLvvs0000().setAnniInput3Formatted("00000");
        ws.getInputLvvs0000().setMesiInput3Formatted("00000");
        ws.getInputLvvs0000().setDataOutput(new AfDecimal(0, 11, 7));
    }
}
