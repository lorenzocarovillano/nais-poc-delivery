package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BilaFndEstrDao;
import it.accenture.jnais.commons.data.to.IBilaFndEstr;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BilaFndEstr;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsb010Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.B01DtQtzIni;
import it.accenture.jnais.ws.redefines.B01DtValzzQuoDtCa;
import it.accenture.jnais.ws.redefines.B01IdRichEstrazAgg;
import it.accenture.jnais.ws.redefines.B01NumQuoDtCalc;
import it.accenture.jnais.ws.redefines.B01NumQuoIni;
import it.accenture.jnais.ws.redefines.B01PcInvst;
import it.accenture.jnais.ws.redefines.B01ValQuoDtCalc;
import it.accenture.jnais.ws.redefines.B01ValQuoIni;
import it.accenture.jnais.ws.redefines.B01ValQuoT;

/**Original name: IDBSB010<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsb010 extends Program implements IBilaFndEstr {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BilaFndEstrDao bilaFndEstrDao = new BilaFndEstrDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsb010Data ws = new Idbsb010Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BILA-FND-ESTR
    private BilaFndEstr bilaFndEstr;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSB010_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BilaFndEstr bilaFndEstr) {
        this.idsv0003 = idsv0003;
        this.bilaFndEstr = bilaFndEstr;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsb010 getInstance() {
        return ((Idbsb010)Programs.getInstance(Idbsb010.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSB010'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSB010");
        // COB_CODE: MOVE 'BILA_FND_ESTR' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BILA_FND_ESTR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BILA_FND_ESTR
        //                ,ID_BILA_TRCH_ESTR
        //                ,COD_COMP_ANIA
        //                ,ID_RICH_ESTRAZ_MAS
        //                ,ID_RICH_ESTRAZ_AGG
        //                ,DT_RIS
        //                ,ID_POLI
        //                ,ID_ADES
        //                ,ID_GAR
        //                ,ID_TRCH_DI_GAR
        //                ,COD_FND
        //                ,DT_QTZ_INI
        //                ,NUM_QUO_INI
        //                ,NUM_QUO_DT_CALC
        //                ,VAL_QUO_INI
        //                ,DT_VALZZ_QUO_DT_CA
        //                ,VAL_QUO_DT_CALC
        //                ,VAL_QUO_T
        //                ,PC_INVST
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :B01-ID-BILA-FND-ESTR
        //               ,:B01-ID-BILA-TRCH-ESTR
        //               ,:B01-COD-COMP-ANIA
        //               ,:B01-ID-RICH-ESTRAZ-MAS
        //               ,:B01-ID-RICH-ESTRAZ-AGG
        //                :IND-B01-ID-RICH-ESTRAZ-AGG
        //               ,:B01-DT-RIS-DB
        //               ,:B01-ID-POLI
        //               ,:B01-ID-ADES
        //               ,:B01-ID-GAR
        //               ,:B01-ID-TRCH-DI-GAR
        //               ,:B01-COD-FND
        //               ,:B01-DT-QTZ-INI-DB
        //                :IND-B01-DT-QTZ-INI
        //               ,:B01-NUM-QUO-INI
        //                :IND-B01-NUM-QUO-INI
        //               ,:B01-NUM-QUO-DT-CALC
        //                :IND-B01-NUM-QUO-DT-CALC
        //               ,:B01-VAL-QUO-INI
        //                :IND-B01-VAL-QUO-INI
        //               ,:B01-DT-VALZZ-QUO-DT-CA-DB
        //                :IND-B01-DT-VALZZ-QUO-DT-CA
        //               ,:B01-VAL-QUO-DT-CALC
        //                :IND-B01-VAL-QUO-DT-CALC
        //               ,:B01-VAL-QUO-T
        //                :IND-B01-VAL-QUO-T
        //               ,:B01-PC-INVST
        //                :IND-B01-PC-INVST
        //               ,:B01-DS-OPER-SQL
        //               ,:B01-DS-VER
        //               ,:B01-DS-TS-CPTZ
        //               ,:B01-DS-UTENTE
        //               ,:B01-DS-STATO-ELAB
        //             FROM BILA_FND_ESTR
        //             WHERE     ID_BILA_FND_ESTR = :B01-ID-BILA-FND-ESTR
        //           END-EXEC.
        bilaFndEstrDao.selectByB01IdBilaFndEstr(bilaFndEstr.getB01IdBilaFndEstr(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BILA_FND_ESTR
            //                  (
            //                     ID_BILA_FND_ESTR
            //                    ,ID_BILA_TRCH_ESTR
            //                    ,COD_COMP_ANIA
            //                    ,ID_RICH_ESTRAZ_MAS
            //                    ,ID_RICH_ESTRAZ_AGG
            //                    ,DT_RIS
            //                    ,ID_POLI
            //                    ,ID_ADES
            //                    ,ID_GAR
            //                    ,ID_TRCH_DI_GAR
            //                    ,COD_FND
            //                    ,DT_QTZ_INI
            //                    ,NUM_QUO_INI
            //                    ,NUM_QUO_DT_CALC
            //                    ,VAL_QUO_INI
            //                    ,DT_VALZZ_QUO_DT_CA
            //                    ,VAL_QUO_DT_CALC
            //                    ,VAL_QUO_T
            //                    ,PC_INVST
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                  )
            //              VALUES
            //                  (
            //                    :B01-ID-BILA-FND-ESTR
            //                    ,:B01-ID-BILA-TRCH-ESTR
            //                    ,:B01-COD-COMP-ANIA
            //                    ,:B01-ID-RICH-ESTRAZ-MAS
            //                    ,:B01-ID-RICH-ESTRAZ-AGG
            //                     :IND-B01-ID-RICH-ESTRAZ-AGG
            //                    ,:B01-DT-RIS-DB
            //                    ,:B01-ID-POLI
            //                    ,:B01-ID-ADES
            //                    ,:B01-ID-GAR
            //                    ,:B01-ID-TRCH-DI-GAR
            //                    ,:B01-COD-FND
            //                    ,:B01-DT-QTZ-INI-DB
            //                     :IND-B01-DT-QTZ-INI
            //                    ,:B01-NUM-QUO-INI
            //                     :IND-B01-NUM-QUO-INI
            //                    ,:B01-NUM-QUO-DT-CALC
            //                     :IND-B01-NUM-QUO-DT-CALC
            //                    ,:B01-VAL-QUO-INI
            //                     :IND-B01-VAL-QUO-INI
            //                    ,:B01-DT-VALZZ-QUO-DT-CA-DB
            //                     :IND-B01-DT-VALZZ-QUO-DT-CA
            //                    ,:B01-VAL-QUO-DT-CALC
            //                     :IND-B01-VAL-QUO-DT-CALC
            //                    ,:B01-VAL-QUO-T
            //                     :IND-B01-VAL-QUO-T
            //                    ,:B01-PC-INVST
            //                     :IND-B01-PC-INVST
            //                    ,:B01-DS-OPER-SQL
            //                    ,:B01-DS-VER
            //                    ,:B01-DS-TS-CPTZ
            //                    ,:B01-DS-UTENTE
            //                    ,:B01-DS-STATO-ELAB
            //                  )
            //           END-EXEC
            bilaFndEstrDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BILA_FND_ESTR SET
        //                   ID_BILA_FND_ESTR       =
        //                :B01-ID-BILA-FND-ESTR
        //                  ,ID_BILA_TRCH_ESTR      =
        //                :B01-ID-BILA-TRCH-ESTR
        //                  ,COD_COMP_ANIA          =
        //                :B01-COD-COMP-ANIA
        //                  ,ID_RICH_ESTRAZ_MAS     =
        //                :B01-ID-RICH-ESTRAZ-MAS
        //                  ,ID_RICH_ESTRAZ_AGG     =
        //                :B01-ID-RICH-ESTRAZ-AGG
        //                                       :IND-B01-ID-RICH-ESTRAZ-AGG
        //                  ,DT_RIS                 =
        //           :B01-DT-RIS-DB
        //                  ,ID_POLI                =
        //                :B01-ID-POLI
        //                  ,ID_ADES                =
        //                :B01-ID-ADES
        //                  ,ID_GAR                 =
        //                :B01-ID-GAR
        //                  ,ID_TRCH_DI_GAR         =
        //                :B01-ID-TRCH-DI-GAR
        //                  ,COD_FND                =
        //                :B01-COD-FND
        //                  ,DT_QTZ_INI             =
        //           :B01-DT-QTZ-INI-DB
        //                                       :IND-B01-DT-QTZ-INI
        //                  ,NUM_QUO_INI            =
        //                :B01-NUM-QUO-INI
        //                                       :IND-B01-NUM-QUO-INI
        //                  ,NUM_QUO_DT_CALC        =
        //                :B01-NUM-QUO-DT-CALC
        //                                       :IND-B01-NUM-QUO-DT-CALC
        //                  ,VAL_QUO_INI            =
        //                :B01-VAL-QUO-INI
        //                                       :IND-B01-VAL-QUO-INI
        //                  ,DT_VALZZ_QUO_DT_CA     =
        //           :B01-DT-VALZZ-QUO-DT-CA-DB
        //                                       :IND-B01-DT-VALZZ-QUO-DT-CA
        //                  ,VAL_QUO_DT_CALC        =
        //                :B01-VAL-QUO-DT-CALC
        //                                       :IND-B01-VAL-QUO-DT-CALC
        //                  ,VAL_QUO_T              =
        //                :B01-VAL-QUO-T
        //                                       :IND-B01-VAL-QUO-T
        //                  ,PC_INVST               =
        //                :B01-PC-INVST
        //                                       :IND-B01-PC-INVST
        //                  ,DS_OPER_SQL            =
        //                :B01-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :B01-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :B01-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :B01-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :B01-DS-STATO-ELAB
        //                WHERE     ID_BILA_FND_ESTR = :B01-ID-BILA-FND-ESTR
        //           END-EXEC.
        bilaFndEstrDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BILA_FND_ESTR
        //                WHERE     ID_BILA_FND_ESTR = :B01-ID-BILA-FND-ESTR
        //           END-EXEC.
        bilaFndEstrDao.deleteByB01IdBilaFndEstr(bilaFndEstr.getB01IdBilaFndEstr());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-B01-ID-RICH-ESTRAZ-AGG = -1
        //              MOVE HIGH-VALUES TO B01-ID-RICH-ESTRAZ-AGG-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-ID-RICH-ESTRAZ-AGG-NULL
            bilaFndEstr.getB01IdRichEstrazAgg().setB01IdRichEstrazAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01IdRichEstrazAgg.Len.B01_ID_RICH_ESTRAZ_AGG_NULL));
        }
        // COB_CODE: IF IND-B01-DT-QTZ-INI = -1
        //              MOVE HIGH-VALUES TO B01-DT-QTZ-INI-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getIdLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-DT-QTZ-INI-NULL
            bilaFndEstr.getB01DtQtzIni().setB01DtQtzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01DtQtzIni.Len.B01_DT_QTZ_INI_NULL));
        }
        // COB_CODE: IF IND-B01-NUM-QUO-INI = -1
        //              MOVE HIGH-VALUES TO B01-NUM-QUO-INI-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getIdTitCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-NUM-QUO-INI-NULL
            bilaFndEstr.getB01NumQuoIni().setB01NumQuoIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01NumQuoIni.Len.B01_NUM_QUO_INI_NULL));
        }
        // COB_CODE: IF IND-B01-NUM-QUO-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B01-NUM-QUO-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-NUM-QUO-DT-CALC-NULL
            bilaFndEstr.getB01NumQuoDtCalc().setB01NumQuoDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01NumQuoDtCalc.Len.B01_NUM_QUO_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B01-VAL-QUO-INI = -1
        //              MOVE HIGH-VALUES TO B01-VAL-QUO-INI-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getDtEffMoviFinrio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-VAL-QUO-INI-NULL
            bilaFndEstr.getB01ValQuoIni().setB01ValQuoIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01ValQuoIni.Len.B01_VAL_QUO_INI_NULL));
        }
        // COB_CODE: IF IND-B01-DT-VALZZ-QUO-DT-CA = -1
        //              MOVE HIGH-VALUES TO B01-DT-VALZZ-QUO-DT-CA-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getDtElab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-DT-VALZZ-QUO-DT-CA-NULL
            bilaFndEstr.getB01DtValzzQuoDtCa().setB01DtValzzQuoDtCaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01DtValzzQuoDtCa.Len.B01_DT_VALZZ_QUO_DT_CA_NULL));
        }
        // COB_CODE: IF IND-B01-VAL-QUO-DT-CALC = -1
        //              MOVE HIGH-VALUES TO B01-VAL-QUO-DT-CALC-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getDtRichMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-VAL-QUO-DT-CALC-NULL
            bilaFndEstr.getB01ValQuoDtCalc().setB01ValQuoDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01ValQuoDtCalc.Len.B01_VAL_QUO_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-B01-VAL-QUO-T = -1
        //              MOVE HIGH-VALUES TO B01-VAL-QUO-T-NULL
        //           END-IF
        if (ws.getIndBilaFndEstr().getCosOprz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-VAL-QUO-T-NULL
            bilaFndEstr.getB01ValQuoT().setB01ValQuoTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01ValQuoT.Len.B01_VAL_QUO_T_NULL));
        }
        // COB_CODE: IF IND-B01-PC-INVST = -1
        //              MOVE HIGH-VALUES TO B01-PC-INVST-NULL
        //           END-IF.
        if (ws.getIndBilaFndEstr().getTpCausRettifica() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO B01-PC-INVST-NULL
            bilaFndEstr.getB01PcInvst().setB01PcInvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B01PcInvst.Len.B01_PC_INVST_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO B01-DS-OPER-SQL
        bilaFndEstr.setB01DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO B01-DS-VER
        bilaFndEstr.setB01DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO B01-DS-UTENTE
        bilaFndEstr.setB01DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO B01-DS-STATO-ELAB
        bilaFndEstr.setB01DsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO B01-DS-TS-CPTZ.
        bilaFndEstr.setB01DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO B01-DS-OPER-SQL
        bilaFndEstr.setB01DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO B01-DS-UTENTE
        bilaFndEstr.setB01DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO B01-DS-TS-CPTZ.
        bilaFndEstr.setB01DsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF B01-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-ID-RICH-ESTRAZ-AGG
        //           ELSE
        //              MOVE 0 TO IND-B01-ID-RICH-ESTRAZ-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01IdRichEstrazAgg().getB01IdRichEstrazAggNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-ID-RICH-ESTRAZ-AGG
            ws.getIndBilaFndEstr().setIdAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-ID-RICH-ESTRAZ-AGG
            ws.getIndBilaFndEstr().setIdAdes(((short)0));
        }
        // COB_CODE: IF B01-DT-QTZ-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-DT-QTZ-INI
        //           ELSE
        //              MOVE 0 TO IND-B01-DT-QTZ-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01DtQtzIni().getB01DtQtzIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-DT-QTZ-INI
            ws.getIndBilaFndEstr().setIdLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-DT-QTZ-INI
            ws.getIndBilaFndEstr().setIdLiq(((short)0));
        }
        // COB_CODE: IF B01-NUM-QUO-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-NUM-QUO-INI
        //           ELSE
        //              MOVE 0 TO IND-B01-NUM-QUO-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01NumQuoIni().getB01NumQuoIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-NUM-QUO-INI
            ws.getIndBilaFndEstr().setIdTitCont(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-NUM-QUO-INI
            ws.getIndBilaFndEstr().setIdTitCont(((short)0));
        }
        // COB_CODE: IF B01-NUM-QUO-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-NUM-QUO-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B01-NUM-QUO-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01NumQuoDtCalc().getB01NumQuoDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-NUM-QUO-DT-CALC
            ws.getIndBilaFndEstr().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-NUM-QUO-DT-CALC
            ws.getIndBilaFndEstr().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF B01-VAL-QUO-INI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-VAL-QUO-INI
        //           ELSE
        //              MOVE 0 TO IND-B01-VAL-QUO-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01ValQuoIni().getB01ValQuoIniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-VAL-QUO-INI
            ws.getIndBilaFndEstr().setDtEffMoviFinrio(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-VAL-QUO-INI
            ws.getIndBilaFndEstr().setDtEffMoviFinrio(((short)0));
        }
        // COB_CODE: IF B01-DT-VALZZ-QUO-DT-CA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-DT-VALZZ-QUO-DT-CA
        //           ELSE
        //              MOVE 0 TO IND-B01-DT-VALZZ-QUO-DT-CA
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01DtValzzQuoDtCa().getB01DtValzzQuoDtCaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-DT-VALZZ-QUO-DT-CA
            ws.getIndBilaFndEstr().setDtElab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-DT-VALZZ-QUO-DT-CA
            ws.getIndBilaFndEstr().setDtElab(((short)0));
        }
        // COB_CODE: IF B01-VAL-QUO-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-VAL-QUO-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-B01-VAL-QUO-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01ValQuoDtCalc().getB01ValQuoDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-VAL-QUO-DT-CALC
            ws.getIndBilaFndEstr().setDtRichMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-VAL-QUO-DT-CALC
            ws.getIndBilaFndEstr().setDtRichMovi(((short)0));
        }
        // COB_CODE: IF B01-VAL-QUO-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-VAL-QUO-T
        //           ELSE
        //              MOVE 0 TO IND-B01-VAL-QUO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01ValQuoT().getB01ValQuoTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-VAL-QUO-T
            ws.getIndBilaFndEstr().setCosOprz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-VAL-QUO-T
            ws.getIndBilaFndEstr().setCosOprz(((short)0));
        }
        // COB_CODE: IF B01-PC-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-B01-PC-INVST
        //           ELSE
        //              MOVE 0 TO IND-B01-PC-INVST
        //           END-IF.
        if (Characters.EQ_HIGH.test(bilaFndEstr.getB01PcInvst().getB01PcInvstNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-B01-PC-INVST
            ws.getIndBilaFndEstr().setTpCausRettifica(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-B01-PC-INVST
            ws.getIndBilaFndEstr().setTpCausRettifica(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE B01-DT-RIS TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaFndEstr.getB01DtRis(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO B01-DT-RIS-DB
        ws.getBilaFndEstrDb().setRisDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-B01-DT-QTZ-INI = 0
        //               MOVE WS-DATE-X      TO B01-DT-QTZ-INI-DB
        //           END-IF
        if (ws.getIndBilaFndEstr().getIdLiq() == 0) {
            // COB_CODE: MOVE B01-DT-QTZ-INI TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaFndEstr.getB01DtQtzIni().getB01DtQtzIni(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B01-DT-QTZ-INI-DB
            ws.getBilaFndEstrDb().setQtzIniDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-B01-DT-VALZZ-QUO-DT-CA = 0
        //               MOVE WS-DATE-X      TO B01-DT-VALZZ-QUO-DT-CA-DB
        //           END-IF.
        if (ws.getIndBilaFndEstr().getDtElab() == 0) {
            // COB_CODE: MOVE B01-DT-VALZZ-QUO-DT-CA TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bilaFndEstr.getB01DtValzzQuoDtCa().getB01DtValzzQuoDtCa(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO B01-DT-VALZZ-QUO-DT-CA-DB
            ws.getBilaFndEstrDb().setValzzQuoDtCaDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE B01-DT-RIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBilaFndEstrDb().getRisDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO B01-DT-RIS
        bilaFndEstr.setB01DtRis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-B01-DT-QTZ-INI = 0
        //               MOVE WS-DATE-N      TO B01-DT-QTZ-INI
        //           END-IF
        if (ws.getIndBilaFndEstr().getIdLiq() == 0) {
            // COB_CODE: MOVE B01-DT-QTZ-INI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaFndEstrDb().getQtzIniDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B01-DT-QTZ-INI
            bilaFndEstr.getB01DtQtzIni().setB01DtQtzIni(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-B01-DT-VALZZ-QUO-DT-CA = 0
        //               MOVE WS-DATE-N      TO B01-DT-VALZZ-QUO-DT-CA
        //           END-IF.
        if (ws.getIndBilaFndEstr().getDtElab() == 0) {
            // COB_CODE: MOVE B01-DT-VALZZ-QUO-DT-CA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBilaFndEstrDb().getValzzQuoDtCaDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO B01-DT-VALZZ-QUO-DT-CA
            bilaFndEstr.getB01DtValzzQuoDtCa().setB01DtValzzQuoDtCa(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getB01IdBilaFndEstr() {
        return bilaFndEstr.getB01IdBilaFndEstr();
    }

    @Override
    public void setB01IdBilaFndEstr(int b01IdBilaFndEstr) {
        this.bilaFndEstr.setB01IdBilaFndEstr(b01IdBilaFndEstr);
    }

    @Override
    public int getCodCompAnia() {
        return bilaFndEstr.getB01CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.bilaFndEstr.setB01CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return bilaFndEstr.getB01CodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.bilaFndEstr.setB01CodFnd(codFnd);
    }

    @Override
    public char getDsOperSql() {
        return bilaFndEstr.getB01DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.bilaFndEstr.setB01DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return bilaFndEstr.getB01DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.bilaFndEstr.setB01DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return bilaFndEstr.getB01DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.bilaFndEstr.setB01DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return bilaFndEstr.getB01DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.bilaFndEstr.setB01DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return bilaFndEstr.getB01DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.bilaFndEstr.setB01DsVer(dsVer);
    }

    @Override
    public String getDtQtzIniDb() {
        return ws.getBilaFndEstrDb().getQtzIniDb();
    }

    @Override
    public void setDtQtzIniDb(String dtQtzIniDb) {
        this.ws.getBilaFndEstrDb().setQtzIniDb(dtQtzIniDb);
    }

    @Override
    public String getDtQtzIniDbObj() {
        if (ws.getIndBilaFndEstr().getIdLiq() >= 0) {
            return getDtQtzIniDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtQtzIniDbObj(String dtQtzIniDbObj) {
        if (dtQtzIniDbObj != null) {
            setDtQtzIniDb(dtQtzIniDbObj);
            ws.getIndBilaFndEstr().setIdLiq(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setIdLiq(((short)-1));
        }
    }

    @Override
    public String getDtRisDb() {
        return ws.getBilaFndEstrDb().getRisDb();
    }

    @Override
    public void setDtRisDb(String dtRisDb) {
        this.ws.getBilaFndEstrDb().setRisDb(dtRisDb);
    }

    @Override
    public String getDtValzzQuoDtCaDb() {
        return ws.getBilaFndEstrDb().getValzzQuoDtCaDb();
    }

    @Override
    public void setDtValzzQuoDtCaDb(String dtValzzQuoDtCaDb) {
        this.ws.getBilaFndEstrDb().setValzzQuoDtCaDb(dtValzzQuoDtCaDb);
    }

    @Override
    public String getDtValzzQuoDtCaDbObj() {
        if (ws.getIndBilaFndEstr().getDtElab() >= 0) {
            return getDtValzzQuoDtCaDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtValzzQuoDtCaDbObj(String dtValzzQuoDtCaDbObj) {
        if (dtValzzQuoDtCaDbObj != null) {
            setDtValzzQuoDtCaDb(dtValzzQuoDtCaDbObj);
            ws.getIndBilaFndEstr().setDtElab(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setDtElab(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return bilaFndEstr.getB01IdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.bilaFndEstr.setB01IdAdes(idAdes);
    }

    @Override
    public int getIdBilaTrchEstr() {
        return bilaFndEstr.getB01IdBilaTrchEstr();
    }

    @Override
    public void setIdBilaTrchEstr(int idBilaTrchEstr) {
        this.bilaFndEstr.setB01IdBilaTrchEstr(idBilaTrchEstr);
    }

    @Override
    public int getIdGar() {
        return bilaFndEstr.getB01IdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.bilaFndEstr.setB01IdGar(idGar);
    }

    @Override
    public int getIdPoli() {
        return bilaFndEstr.getB01IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.bilaFndEstr.setB01IdPoli(idPoli);
    }

    @Override
    public int getIdRichEstrazAgg() {
        return bilaFndEstr.getB01IdRichEstrazAgg().getB01IdRichEstrazAgg();
    }

    @Override
    public void setIdRichEstrazAgg(int idRichEstrazAgg) {
        this.bilaFndEstr.getB01IdRichEstrazAgg().setB01IdRichEstrazAgg(idRichEstrazAgg);
    }

    @Override
    public Integer getIdRichEstrazAggObj() {
        if (ws.getIndBilaFndEstr().getIdAdes() >= 0) {
            return ((Integer)getIdRichEstrazAgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichEstrazAggObj(Integer idRichEstrazAggObj) {
        if (idRichEstrazAggObj != null) {
            setIdRichEstrazAgg(((int)idRichEstrazAggObj));
            ws.getIndBilaFndEstr().setIdAdes(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getIdRichEstrazMas() {
        return bilaFndEstr.getB01IdRichEstrazMas();
    }

    @Override
    public void setIdRichEstrazMas(int idRichEstrazMas) {
        this.bilaFndEstr.setB01IdRichEstrazMas(idRichEstrazMas);
    }

    @Override
    public int getIdTrchDiGar() {
        return bilaFndEstr.getB01IdTrchDiGar();
    }

    @Override
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.bilaFndEstr.setB01IdTrchDiGar(idTrchDiGar);
    }

    @Override
    public AfDecimal getNumQuoDtCalc() {
        return bilaFndEstr.getB01NumQuoDtCalc().getB01NumQuoDtCalc();
    }

    @Override
    public void setNumQuoDtCalc(AfDecimal numQuoDtCalc) {
        this.bilaFndEstr.getB01NumQuoDtCalc().setB01NumQuoDtCalc(numQuoDtCalc.copy());
    }

    @Override
    public AfDecimal getNumQuoDtCalcObj() {
        if (ws.getIndBilaFndEstr().getIdMoviChiu() >= 0) {
            return getNumQuoDtCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoDtCalcObj(AfDecimal numQuoDtCalcObj) {
        if (numQuoDtCalcObj != null) {
            setNumQuoDtCalc(new AfDecimal(numQuoDtCalcObj, 12, 5));
            ws.getIndBilaFndEstr().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuoIni() {
        return bilaFndEstr.getB01NumQuoIni().getB01NumQuoIni();
    }

    @Override
    public void setNumQuoIni(AfDecimal numQuoIni) {
        this.bilaFndEstr.getB01NumQuoIni().setB01NumQuoIni(numQuoIni.copy());
    }

    @Override
    public AfDecimal getNumQuoIniObj() {
        if (ws.getIndBilaFndEstr().getIdTitCont() >= 0) {
            return getNumQuoIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoIniObj(AfDecimal numQuoIniObj) {
        if (numQuoIniObj != null) {
            setNumQuoIni(new AfDecimal(numQuoIniObj, 12, 5));
            ws.getIndBilaFndEstr().setIdTitCont(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setIdTitCont(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcInvst() {
        return bilaFndEstr.getB01PcInvst().getB01PcInvst();
    }

    @Override
    public void setPcInvst(AfDecimal pcInvst) {
        this.bilaFndEstr.getB01PcInvst().setB01PcInvst(pcInvst.copy());
    }

    @Override
    public AfDecimal getPcInvstObj() {
        if (ws.getIndBilaFndEstr().getTpCausRettifica() >= 0) {
            return getPcInvst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcInvstObj(AfDecimal pcInvstObj) {
        if (pcInvstObj != null) {
            setPcInvst(new AfDecimal(pcInvstObj, 6, 3));
            ws.getIndBilaFndEstr().setTpCausRettifica(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setTpCausRettifica(((short)-1));
        }
    }

    @Override
    public AfDecimal getValQuoDtCalc() {
        return bilaFndEstr.getB01ValQuoDtCalc().getB01ValQuoDtCalc();
    }

    @Override
    public void setValQuoDtCalc(AfDecimal valQuoDtCalc) {
        this.bilaFndEstr.getB01ValQuoDtCalc().setB01ValQuoDtCalc(valQuoDtCalc.copy());
    }

    @Override
    public AfDecimal getValQuoDtCalcObj() {
        if (ws.getIndBilaFndEstr().getDtRichMovi() >= 0) {
            return getValQuoDtCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoDtCalcObj(AfDecimal valQuoDtCalcObj) {
        if (valQuoDtCalcObj != null) {
            setValQuoDtCalc(new AfDecimal(valQuoDtCalcObj, 12, 5));
            ws.getIndBilaFndEstr().setDtRichMovi(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setDtRichMovi(((short)-1));
        }
    }

    @Override
    public AfDecimal getValQuoIni() {
        return bilaFndEstr.getB01ValQuoIni().getB01ValQuoIni();
    }

    @Override
    public void setValQuoIni(AfDecimal valQuoIni) {
        this.bilaFndEstr.getB01ValQuoIni().setB01ValQuoIni(valQuoIni.copy());
    }

    @Override
    public AfDecimal getValQuoIniObj() {
        if (ws.getIndBilaFndEstr().getDtEffMoviFinrio() >= 0) {
            return getValQuoIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoIniObj(AfDecimal valQuoIniObj) {
        if (valQuoIniObj != null) {
            setValQuoIni(new AfDecimal(valQuoIniObj, 12, 5));
            ws.getIndBilaFndEstr().setDtEffMoviFinrio(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setDtEffMoviFinrio(((short)-1));
        }
    }

    @Override
    public AfDecimal getValQuoT() {
        return bilaFndEstr.getB01ValQuoT().getB01ValQuoT();
    }

    @Override
    public void setValQuoT(AfDecimal valQuoT) {
        this.bilaFndEstr.getB01ValQuoT().setB01ValQuoT(valQuoT.copy());
    }

    @Override
    public AfDecimal getValQuoTObj() {
        if (ws.getIndBilaFndEstr().getCosOprz() >= 0) {
            return getValQuoT();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoTObj(AfDecimal valQuoTObj) {
        if (valQuoTObj != null) {
            setValQuoT(new AfDecimal(valQuoTObj, 12, 5));
            ws.getIndBilaFndEstr().setCosOprz(((short)0));
        }
        else {
            ws.getIndBilaFndEstr().setCosOprz(((short)-1));
        }
    }
}
