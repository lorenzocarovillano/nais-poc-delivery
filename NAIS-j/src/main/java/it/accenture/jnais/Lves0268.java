package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Lves0268Data;
import it.accenture.jnais.ws.WallAreaAssetLves0268;
import it.accenture.jnais.ws.WorkCommareaLves0268;

/**Original name: LVES0268<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       MARZO 2008.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LVES0268
 *     TIPOLOGIA ..... MAIN
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... CREAZIONE PROPOSTA INDIVIDUALE
 *     PAGINA WEB..... CALCOLI
 * ----------------------------------------------------------------*</pre>*/
public class Lves0268 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lves0268Data ws = new Lves0268Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WORK-COMMAREA
    private WorkCommareaLves0268 workCommarea;

    //==== METHODS ====
    /**Original name: PROGRAM_LVES0268_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WorkCommareaLves0268 workCommarea) {
        this.areaIdsv0001 = areaIdsv0001;
        this.workCommarea = workCommarea;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lves0268 getInstance() {
        return ((Lves0268)Programs.getInstance(Lves0268.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: IF WALL-ELE-ASSET-ALL-MAX < 0
        //              INITIALIZE  WALL-AREA-ASSET
        //           END-IF.
        if (workCommarea.getWallAreaAsset().getEleAssetAllMax() < 0) {
            // COB_CODE: INITIALIZE  WALL-AREA-ASSET
            initWallAreaAsset();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: EVALUATE WCOM-TIPO-OPERAZIONE
        //           WHEN '00'
        //                  THRU EX-S1100
        //           WHEN '01'
        //                  THRU EX-S1101
        //           WHEN '02'
        //           WHEN '03'
        //           WHEN '04'
        //           WHEN '05'
        //               CONTINUE
        //           END-EVALUATE.
        switch (workCommarea.getLccc0001().getTipoOperazione().getTipoOperazione()) {

            case "00":// COB_CODE: PERFORM S1100-CALL-LVES0269
                //              THRU EX-S1100
                s1100CallLves0269();
                break;

            case "01":// COB_CODE: PERFORM S1101-CALL-LVES0270
                //              THRU EX-S1101
                s1101CallLves0270();
                break;

            case "02":
            case "03":
            case "04":
            case "05":// COB_CODE: CONTINUE
            //continue
                break;

            default:break;
        }
    }

    /**Original name: S1100-CALL-LVES0269<br>
	 * <pre>----------------------------------------------------------------*
	 *     SERVIZIO PREPARA MAPPA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100CallLves0269() {
        Lves0269 lves0269 = null;
        // COB_CODE: CALL LVES0269 USING  AREA-IDSV0001
        //                                WCOM-AREA-STATI
        //                                WPOL-AREA-POLIZZA
        //                                WDCO-AREA-DT-COLL
        //                                WADE-AREA-ADESIONE
        //                                WDAD-AREA-DEF-ADES
        //                                WRAN-AREA-RAPP-ANAG
        //                                WDFA-AREA-DT-FISC-ADES
        //                                WGRZ-AREA-GARANZIA
        //                                WTGA-AREA-TRANCHE
        //                                WSPG-AREA-SOPRAP-GAR
        //                                WSDI-AREA-STRA-INV
        //                                WALL-AREA-ASSET
        //                                WPMO-AREA-PARAM-MOV
        //                                WPOG-AREA-PARAM-OGG
        //                                WBEP-AREA-BENEFICIARI
        //                                WQUE-AREA-QUEST
        //                                WDEQ-AREA-DETT-QUEST
        //                                WCLT-AREA-CLAUSOLE
        //                                WOCO-AREA-OGG-COLLG
        //                                WRRE-AREA-RAP-RETE
        //                                WL23-AREA-VINC-PEG
        //                                WP67-AREA-EST-POLI-CPI-PR
        //                                WPAG-AREA-PAGINA
        //                                WPAG-AREA-IAS
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lves0269 = Lves0269.getInstance();
            lves0269.run(new Object[] {areaIdsv0001, workCommarea, workCommarea.getWpolAreaPolizza(), workCommarea.getWdcoAreaDtColl(), workCommarea.getWadeAreaAdesione(), workCommarea.getWdadAreaDefAdes(), workCommarea.getWranAreaRappAnag(), workCommarea.getWdfaAreaDtFiscAdes(), workCommarea.getWgrzAreaGaranzia(), workCommarea.getWtgaAreaTranche(), workCommarea.getWspgAreaSoprapGar(), workCommarea.getWsdiAreaStraInv(), workCommarea.getWallAreaAsset(), workCommarea.getWpmoAreaParamMov(), workCommarea.getWpogAreaParamOgg(), workCommarea.getWbepAreaBeneficiari(), workCommarea.getWqueAreaQuest(), workCommarea.getWdeqAreaDettQuest(), workCommarea.getWcltAreaClausole(), workCommarea.getWocoAreaOggCollg(), workCommarea.getWrreAreaRapRete(), workCommarea.getWl23AreaVincPeg(), workCommarea.getWp67AreaEstPoliCpiPr(), workCommarea.getWpagAreaPagina(), workCommarea.getWpagAreaIas()});
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'INIZIALIZZAZIONE DELLA PAGINA'
            //                                          TO CALL-DESC
            ws.getIdsv0002().setCallDesc("INIZIALIZZAZIONE DELLA PAGINA");
            // COB_CODE: MOVE 'S1100-CALL-LVES0269'     TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-CALL-LVES0269");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: S1101-CALL-LVES0270<br>
	 * <pre>----------------------------------------------------------------*
	 *     SERVIZIO CONTROLLI
	 * ----------------------------------------------------------------*
	 * MOD20110512
	 * MOD20110512</pre>*/
    private void s1101CallLves0270() {
        Lves0270 lves0270 = null;
        // COB_CODE:      CALL LVES0270 USING  AREA-IDSV0001
        //                                     WCOM-AREA-STATI
        //                                     WPOL-AREA-POLIZZA
        //                                     WADE-AREA-ADESIONE
        //                                     WGRZ-AREA-GARANZIA
        //                                     WTGA-AREA-TRANCHE
        //                                     WL23-AREA-VINC-PEG
        //                                     WALL-AREA-ASSET
        //                                     WPOG-AREA-PARAM-OGG
        //           *MOD20110512
        //                                     WPMO-AREA-PARAM-MOV
        //                                     WBEP-AREA-BENEFICIARI
        //           *MOD20110512
        //                ON EXCEPTION
        //                      THRU EX-S0290
        //                END-CALL.
        try {
            lves0270 = Lves0270.getInstance();
            lves0270.run(new Object[] {areaIdsv0001, workCommarea, workCommarea.getWpolAreaPolizza(), workCommarea.getWadeAreaAdesione(), workCommarea.getWgrzAreaGaranzia(), workCommarea.getWtgaAreaTranche(), workCommarea.getWl23AreaVincPeg(), workCommarea.getWallAreaAsset(), workCommarea.getWpogAreaParamOgg(), workCommarea.getWpmoAreaParamMov(), workCommarea.getWbepAreaBeneficiari()});
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'INIZIALIZZAZIONE DELLA PAGINA'
            //                                          TO CALL-DESC
            ws.getIdsv0002().setCallDesc("INIZIALIZZAZIONE DELLA PAGINA");
            // COB_CODE: MOVE 'S1100-CALL-LVES0269'     TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-CALL-LVES0269");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ROUTINES GESTIONE ERRORI                                       *
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    public void initWallAreaAsset() {
        workCommarea.getWallAreaAsset().setEleAssetAllMax(((short)0));
        for (int idx0 = 1; idx0 <= WallAreaAssetLves0268.TAB_ASSET_ALL_MAXOCCURS; idx0++) {
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getStatus().setStatus(Types.SPACE_CHAR);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().setIdPtf(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallIdAstAlloc(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallIdStraDiInvst(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallIdMoviCrz(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().getWallIdMoviChiu().setWallIdMoviChiu(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDtIniVldt(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().getWallDtEndVldt().setWallDtEndVldt(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDtIniEff(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDtEndEff(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallCodCompAnia(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallCodFnd("");
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallCodTari("");
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallTpApplzAst("");
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().getWallPcRipAst().setWallPcRipAst(new AfDecimal(0, 6, 3));
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallTpFnd(Types.SPACE_CHAR);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsRiga(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsOperSql(Types.SPACE_CHAR);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsVer(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsTsIniCptz(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsTsEndCptz(0);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsUtente("");
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallDsStatoElab(Types.SPACE_CHAR);
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().getWallPeriodo().setWallPeriodo(((short)0));
            workCommarea.getWallAreaAsset().getTabAssetAll(idx0).getLccvall1().getDati().setWallTpRibilFnd("");
        }
    }
}
