package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Wb05Dati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaOutLlbs0250;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Llbs0250Data;
import it.accenture.jnais.ws.redefines.B05DtRis;

/**Original name: LLBS0250<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             IASS.
 * DATE-WRITTEN.       GIUGNO 2008.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LLBS0250
 *     TIPOLOGIA...... DRIVER EOC
 *     PROCESSO....... LEGGE E BILANCIO
 *     FUNZIONE....... ESTRAZIONE RISERVA MATEMATICA
 *     DESCRIZIONE....
 *     PAGINA WEB.....
 * **------------------------------------------------------------***</pre>*/
public class Llbs0250 extends BatchProgram {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Llbs0250Data ws = new Llbs0250Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: AREA-OUT
    private AreaOutLlbs0250 areaOut;

    //==== METHODS ====
    /**Original name: PROGRAM_LLBS0250_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaOutLlbs0250 areaOut) {
        this.areaIdsv0001 = areaIdsv0001;
        this.areaOut = areaOut;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI        THRU EX-S0000.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LLBS0250.cbl:line=95, because the code is unreachable.
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM S1000-ELABORAZIONE            THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE            THRU EX-S1000
            s1000Elaborazione();
        }
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI          THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Llbs0250 getInstance() {
        return ((Llbs0250)Programs.getInstance(Llbs0250.class));
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU SCRIVI-BIL-VAR-DI-CALC-T-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM SCRIVI-BIL-VAR-DI-CALC-T
            //              THRU SCRIVI-BIL-VAR-DI-CALC-T-EX
            scriviBilVarDiCalcT();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VAL-DCLGEN-B05<br>
	 * <pre>  ---------------------------------------------------------------
	 *      CONTIENE STATEMENTS PER LA FASE DI EOC
	 *   ---------------------------------------------------------------
	 * --> VARIABILI DI CALCOLO RISERVA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVB055
	 *    ULTIMO AGG. 14 MAR 2011
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenB05() {
        // COB_CODE: MOVE (SF)-ID-BILA-VAR-CALC-T
        //              TO B05-ID-BILA-VAR-CALC-T
        ws.getBilaVarCalcT().setB05IdBilaVarCalcT(areaOut.getLccvb051().getDati().getWb05IdBilaVarCalcT());
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO B05-COD-COMP-ANIA
        ws.getBilaVarCalcT().setB05CodCompAnia(areaOut.getLccvb051().getDati().getWb05CodCompAnia());
        // COB_CODE: MOVE (SF)-ID-BILA-TRCH-ESTR
        //              TO B05-ID-BILA-TRCH-ESTR
        ws.getBilaVarCalcT().setB05IdBilaTrchEstr(areaOut.getLccvb051().getDati().getWb05IdBilaTrchEstr());
        // COB_CODE: MOVE (SF)-ID-RICH-ESTRAZ-MAS
        //              TO B05-ID-RICH-ESTRAZ-MAS
        ws.getBilaVarCalcT().setB05IdRichEstrazMas(areaOut.getLccvb051().getDati().getWb05IdRichEstrazMas());
        // COB_CODE: IF (SF)-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
        //              TO B05-ID-RICH-ESTRAZ-AGG-NULL
        //           ELSE
        //              TO B05-ID-RICH-ESTRAZ-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb051().getDati().getWb05IdRichEstrazAgg().getWb05IdRichEstrazAggNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-RICH-ESTRAZ-AGG-NULL
            //           TO B05-ID-RICH-ESTRAZ-AGG-NULL
            ws.getBilaVarCalcT().getB05IdRichEstrazAgg().setB05IdRichEstrazAggNull(areaOut.getLccvb051().getDati().getWb05IdRichEstrazAgg().getWb05IdRichEstrazAggNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-RICH-ESTRAZ-AGG
            //           TO B05-ID-RICH-ESTRAZ-AGG
            ws.getBilaVarCalcT().getB05IdRichEstrazAgg().setB05IdRichEstrazAgg(areaOut.getLccvb051().getDati().getWb05IdRichEstrazAgg().getWb05IdRichEstrazAgg());
        }
        // COB_CODE: IF (SF)-DT-RIS-NULL = HIGH-VALUES
        //              TO B05-DT-RIS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb051().getDati().getWb05DtRis().getWb05DtRisNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-RIS-NULL
            //           TO B05-DT-RIS-NULL
            ws.getBilaVarCalcT().getB05DtRis().setB05DtRisNull(areaOut.getLccvb051().getDati().getWb05DtRis().getWb05DtRisNull());
        }
        else if (areaOut.getLccvb051().getDati().getWb05DtRis().getWb05DtRis() == 0) {
            // COB_CODE: IF (SF)-DT-RIS = ZERO
            //              TO B05-DT-RIS-NULL
            //           ELSE
            //            TO B05-DT-RIS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B05-DT-RIS-NULL
            ws.getBilaVarCalcT().getB05DtRis().setB05DtRisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B05DtRis.Len.B05_DT_RIS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-RIS
            //           TO B05-DT-RIS
            ws.getBilaVarCalcT().getB05DtRis().setB05DtRis(areaOut.getLccvb051().getDati().getWb05DtRis().getWb05DtRis());
        }
        // COB_CODE: MOVE (SF)-ID-POLI
        //              TO B05-ID-POLI
        ws.getBilaVarCalcT().setB05IdPoli(areaOut.getLccvb051().getDati().getWb05IdPoli());
        // COB_CODE: MOVE (SF)-ID-ADES
        //              TO B05-ID-ADES
        ws.getBilaVarCalcT().setB05IdAdes(areaOut.getLccvb051().getDati().getWb05IdAdes());
        // COB_CODE: MOVE (SF)-ID-TRCH-DI-GAR
        //              TO B05-ID-TRCH-DI-GAR
        ws.getBilaVarCalcT().setB05IdTrchDiGar(areaOut.getLccvb051().getDati().getWb05IdTrchDiGar());
        // COB_CODE: IF (SF)-PROG-SCHEDA-VALOR-NULL = HIGH-VALUES
        //              TO B05-PROG-SCHEDA-VALOR-NULL
        //           ELSE
        //              TO B05-PROG-SCHEDA-VALOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb051().getDati().getWb05ProgSchedaValor().getWb05ProgSchedaValorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROG-SCHEDA-VALOR-NULL
            //           TO B05-PROG-SCHEDA-VALOR-NULL
            ws.getBilaVarCalcT().getB05ProgSchedaValor().setB05ProgSchedaValorNull(areaOut.getLccvb051().getDati().getWb05ProgSchedaValor().getWb05ProgSchedaValorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROG-SCHEDA-VALOR
            //           TO B05-PROG-SCHEDA-VALOR
            ws.getBilaVarCalcT().getB05ProgSchedaValor().setB05ProgSchedaValor(areaOut.getLccvb051().getDati().getWb05ProgSchedaValor().getWb05ProgSchedaValor());
        }
        // COB_CODE: MOVE (SF)-DT-INI-VLDT-TARI
        //              TO B05-DT-INI-VLDT-TARI
        ws.getBilaVarCalcT().setB05DtIniVldtTari(areaOut.getLccvb051().getDati().getWb05DtIniVldtTari());
        // COB_CODE: MOVE (SF)-TP-RGM-FISC
        //              TO B05-TP-RGM-FISC
        ws.getBilaVarCalcT().setB05TpRgmFisc(areaOut.getLccvb051().getDati().getWb05TpRgmFisc());
        // COB_CODE: MOVE (SF)-DT-INI-VLDT-PROD
        //              TO B05-DT-INI-VLDT-PROD
        ws.getBilaVarCalcT().setB05DtIniVldtProd(areaOut.getLccvb051().getDati().getWb05DtIniVldtProd());
        // COB_CODE: MOVE (SF)-DT-DECOR-TRCH
        //              TO B05-DT-DECOR-TRCH
        ws.getBilaVarCalcT().setB05DtDecorTrch(areaOut.getLccvb051().getDati().getWb05DtDecorTrch());
        // COB_CODE: MOVE (SF)-COD-VAR
        //              TO B05-COD-VAR
        ws.getBilaVarCalcT().setB05CodVar(areaOut.getLccvb051().getDati().getWb05CodVar());
        // COB_CODE: MOVE (SF)-TP-D
        //              TO B05-TP-D
        ws.getBilaVarCalcT().setB05TpD(areaOut.getLccvb051().getDati().getWb05TpD());
        // COB_CODE: IF (SF)-VAL-IMP-NULL = HIGH-VALUES
        //              TO B05-VAL-IMP-NULL
        //           ELSE
        //              TO B05-VAL-IMP
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb051().getDati().getWb05ValImp().getWb05ValImpNullFormatted())) {
            // COB_CODE: MOVE (SF)-VAL-IMP-NULL
            //           TO B05-VAL-IMP-NULL
            ws.getBilaVarCalcT().getB05ValImp().setB05ValImpNull(areaOut.getLccvb051().getDati().getWb05ValImp().getWb05ValImpNull());
        }
        else {
            // COB_CODE: MOVE (SF)-VAL-IMP
            //           TO B05-VAL-IMP
            ws.getBilaVarCalcT().getB05ValImp().setB05ValImp(Trunc.toDecimal(areaOut.getLccvb051().getDati().getWb05ValImp().getWb05ValImp(), 18, 7));
        }
        // COB_CODE: IF (SF)-VAL-PC-NULL = HIGH-VALUES
        //              TO B05-VAL-PC-NULL
        //           ELSE
        //              TO B05-VAL-PC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb051().getDati().getWb05ValPc().getWb05ValPcNullFormatted())) {
            // COB_CODE: MOVE (SF)-VAL-PC-NULL
            //           TO B05-VAL-PC-NULL
            ws.getBilaVarCalcT().getB05ValPc().setB05ValPcNull(areaOut.getLccvb051().getDati().getWb05ValPc().getWb05ValPcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-VAL-PC
            //           TO B05-VAL-PC
            ws.getBilaVarCalcT().getB05ValPc().setB05ValPc(Trunc.toDecimal(areaOut.getLccvb051().getDati().getWb05ValPc().getWb05ValPc(), 14, 9));
        }
        // COB_CODE: IF (SF)-VAL-STRINGA-NULL = HIGH-VALUES
        //              TO B05-VAL-STRINGA-NULL
        //           ELSE
        //              TO B05-VAL-STRINGA
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb051().getDati().getWb05ValStringa(), Wb05Dati.Len.WB05_VAL_STRINGA)) {
            // COB_CODE: MOVE (SF)-VAL-STRINGA-NULL
            //           TO B05-VAL-STRINGA-NULL
            ws.getBilaVarCalcT().setB05ValStringa(areaOut.getLccvb051().getDati().getWb05ValStringa());
        }
        else {
            // COB_CODE: MOVE (SF)-VAL-STRINGA
            //           TO B05-VAL-STRINGA
            ws.getBilaVarCalcT().setB05ValStringa(areaOut.getLccvb051().getDati().getWb05ValStringa());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO B05-DS-OPER-SQL
        ws.getBilaVarCalcT().setB05DsOperSql(areaOut.getLccvb051().getDati().getWb05DsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO B05-DS-VER
        //           ELSE
        //              TO B05-DS-VER
        //           END-IF
        if (!Functions.isNumber(areaOut.getLccvb051().getDati().getWb05DsVer())) {
            // COB_CODE: MOVE 0 TO B05-DS-VER
            ws.getBilaVarCalcT().setB05DsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO B05-DS-VER
            ws.getBilaVarCalcT().setB05DsVer(areaOut.getLccvb051().getDati().getWb05DsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO B05-DS-TS-CPTZ
        //           ELSE
        //              TO B05-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(areaOut.getLccvb051().getDati().getWb05DsTsCptz())) {
            // COB_CODE: MOVE 0 TO B05-DS-TS-CPTZ
            ws.getBilaVarCalcT().setB05DsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO B05-DS-TS-CPTZ
            ws.getBilaVarCalcT().setB05DsTsCptz(areaOut.getLccvb051().getDati().getWb05DsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO B05-DS-UTENTE
        ws.getBilaVarCalcT().setB05DsUtente(areaOut.getLccvb051().getDati().getWb05DsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO B05-DS-STATO-ELAB
        ws.getBilaVarCalcT().setB05DsStatoElab(areaOut.getLccvb051().getDati().getWb05DsStatoElab());
        // COB_CODE: MOVE (SF)-AREA-D-VALOR-VAR-VCHAR
        //              TO B05-AREA-D-VALOR-VAR-VCHAR.
        ws.getBilaVarCalcT().setB05AreaDValorVarVcharBytes(areaOut.getLccvb051().getDati().getWb05AreaDValorVarVcharBytes());
    }

    /**Original name: SCRIVI-BIL-VAR-DI-CALC-T<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVB056
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO BIL_ESTRATTI
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCVB055 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN VINCOLO PEGNO (LCCVB051)
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void scriviBilVarDiCalcT() {
        // COB_CODE: INITIALIZE BILA-VAR-CALC-T.
        initBilaVarCalcT();
        //--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
        // COB_CODE:      IF  NOT WB05-ST-INV
        //                AND NOT WB05-ST-CON
        //                AND WB05-ELE-B05-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                   END-IF
        //                END-IF.
        if (!areaOut.getLccvb051().getStatus().isInv() && !areaOut.getLccvb051().getStatus().isWpmoStCon() && areaOut.getWb05EleB05Max() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WB05-ST-ADD
            //           *-->    ESTRAZIONE SEQUENCE E VALORIZZAZIONE CONSEGUENTE DEL CAMPO
            //           *-->    WB05-ID-BILA-VAR-CALC-T INIBITE X MOTIVI DI PERFORMANCE
            //           *
            //           *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
            //           *                 PERFORM ESTR-SEQUENCE
            //           *                    THRU ESTR-SEQUENCE-EX
            //           *                 IF IDSV0001-ESITO-OK
            //           *                    MOVE S090-SEQ-TABELLA
            //                            SET IDSI0011-INSERT TO TRUE
            //           *-->        MODIFICA
            //           *           WHEN WB05-ST-MOD
            //           *
            //           *
            //           *                MOVE WB05-ID-TRCH-DI-GAR
            //           *                  TO B05-ID-TRCH-DI-GAR
            //           *                MOVE WB05-ID-RICH-ESTRAZ-MAS
            //           *                  TO B05-ID-RICH-ESTRAZ-MAS
            //           *
            //           *-->        TIPO OPERAZIONE DISPATCHER
            //           *                SET IDSI0011-UPDATE TO TRUE
            //           *
            //           *-->        CANCELLAZIONE
            //           *           WHEN WB05-ST-DEL
            //           *
            //           *                MOVE WB05-ID-TRCH-DI-GAR
            //           *                  TO B05-ID-TRCH-DI-GAR
            //           *                MOVE WB05-ID-RICH-ESTRAZ-MAS
            //           *                  TO B05-ID-RICH-ESTRAZ-MAS
            //           *
            //           *-->             TIPO OPERAZIONE DISPATCHER
            //           *                SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //                   END-EVALUATE
            switch (areaOut.getLccvb051().getStatus().getStatus()) {

                case WpolStatus.ADD://-->    ESTRAZIONE SEQUENCE E VALORIZZAZIONE CONSEGUENTE DEL CAMPO
                    //-->    WB05-ID-BILA-VAR-CALC-T INIBITE X MOTIVI DI PERFORMANCE
                    //
                    //-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    //                 PERFORM ESTR-SEQUENCE
                    //                    THRU ESTR-SEQUENCE-EX
                    //                 IF IDSV0001-ESITO-OK
                    //                    MOVE S090-SEQ-TABELLA
                    // COB_CODE: MOVE ZERO
                    //            TO WB05-ID-BILA-VAR-CALC-T
                    areaOut.getLccvb051().getDati().setWb05IdBilaVarCalcT(0);
                    //                 END-IF
                    //-->        TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET IDSI0011-INSERT TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
                    //-->        MODIFICA
                    //           WHEN WB05-ST-MOD
                    //
                    //
                    //                MOVE WB05-ID-TRCH-DI-GAR
                    //                  TO B05-ID-TRCH-DI-GAR
                    //                MOVE WB05-ID-RICH-ESTRAZ-MAS
                    //                  TO B05-ID-RICH-ESTRAZ-MAS
                    //
                    //-->        TIPO OPERAZIONE DISPATCHER
                    //                SET IDSI0011-UPDATE TO TRUE
                    //
                    //-->        CANCELLAZIONE
                    //           WHEN WB05-ST-DEL
                    //
                    //                MOVE WB05-ID-TRCH-DI-GAR
                    //                  TO B05-ID-TRCH-DI-GAR
                    //                MOVE WB05-ID-RICH-ESTRAZ-MAS
                    //                  TO B05-ID-RICH-ESTRAZ-MAS
                    //
                    //-->             TIPO OPERAZIONE DISPATCHER
                    //                SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    break;

                default:break;
            }
            //-->    VALORIZZA DCLGEN ADESIONE
            // COB_CODE: PERFORM VAL-DCLGEN-B05
            //              THRU VAL-DCLGEN-B05-EX
            valDclgenB05();
            //-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
            // COB_CODE: IF WB05-ST-ADD
            //                 THRU AGGIORNA-TABELLA-EX
            //           END-IF
            if (areaOut.getLccvb051().getStatus().isAdd()) {
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-B05
                //              THRU VALORIZZA-AREA-DSH-B05-EX
                valorizzaAreaDshB05();
                //-->    CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-B05<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void valorizzaAreaDshB05() {
        // COB_CODE: MOVE 'BILA-VAR-CALC-T'         TO WK-TABELLA.
        ws.setWkTabella("BILA-VAR-CALC-T");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE BILA-VAR-CALC-T             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getBilaVarCalcT().getBilaVarCalcTFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY        TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO   TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre> ----------------------------------------------------------------
	 *   ROUTINES GESTIONE ERRORI
	 *  ----------------------------------------------------------------
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    public void initBilaVarCalcT() {
        ws.getBilaVarCalcT().setB05IdBilaVarCalcT(0);
        ws.getBilaVarCalcT().setB05CodCompAnia(0);
        ws.getBilaVarCalcT().setB05IdBilaTrchEstr(0);
        ws.getBilaVarCalcT().setB05IdRichEstrazMas(0);
        ws.getBilaVarCalcT().getB05IdRichEstrazAgg().setB05IdRichEstrazAgg(0);
        ws.getBilaVarCalcT().getB05DtRis().setB05DtRis(0);
        ws.getBilaVarCalcT().setB05IdPoli(0);
        ws.getBilaVarCalcT().setB05IdAdes(0);
        ws.getBilaVarCalcT().setB05IdTrchDiGar(0);
        ws.getBilaVarCalcT().getB05ProgSchedaValor().setB05ProgSchedaValor(0);
        ws.getBilaVarCalcT().setB05DtIniVldtTari(0);
        ws.getBilaVarCalcT().setB05TpRgmFisc("");
        ws.getBilaVarCalcT().setB05DtIniVldtProd(0);
        ws.getBilaVarCalcT().setB05DtDecorTrch(0);
        ws.getBilaVarCalcT().setB05CodVar("");
        ws.getBilaVarCalcT().setB05TpD(Types.SPACE_CHAR);
        ws.getBilaVarCalcT().getB05ValImp().setB05ValImp(new AfDecimal(0, 18, 7));
        ws.getBilaVarCalcT().getB05ValPc().setB05ValPc(new AfDecimal(0, 14, 9));
        ws.getBilaVarCalcT().setB05ValStringa("");
        ws.getBilaVarCalcT().setB05DsOperSql(Types.SPACE_CHAR);
        ws.getBilaVarCalcT().setB05DsVer(0);
        ws.getBilaVarCalcT().setB05DsTsCptz(0);
        ws.getBilaVarCalcT().setB05DsUtente("");
        ws.getBilaVarCalcT().setB05DsStatoElab(Types.SPACE_CHAR);
        ws.getBilaVarCalcT().setB05AreaDValorVarLen(((short)0));
        ws.getBilaVarCalcT().setB05AreaDValorVar("");
    }
}
