package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3390Data;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: LVVS3390<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2018.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *  DESCRIZIONE.... VALORIZZAZIONE VARIABILE PRETOTEMIPOL
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3390 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3390Data ws = new Lvvs3390Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0213
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3390_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3390 getInstance() {
        return ((Lvvs3390)Programs.getInstance(Lvvs3390.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             AREA-IO-TGA.
        initIxIndici();
        initTabOutput();
        initAreaIoTga();
        // COB_CODE: MOVE ZEROES                       TO IVVC0213-VAL-IMP-O
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS3390.cbl:line=105, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        //--> ESTRAE DETT-TIT-CONT PER TRANCHE DI GAR
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1400-CHIAMA-LDBSF220-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1400-CHIAMA-LDBSF220
            //              THRU S1400-CHIAMA-LDBSF220-EX
            s1400ChiamaLdbsf220();
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF IVVC0213-ID-LIVELLO IS NUMERIC
        //              END-IF
        //           ELSE
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (Functions.isNumber(ivvc0213.getDatiLivello().getIvvc0213IdLivelloFormatted())) {
            // COB_CODE: IF IVVC0213-ID-LIVELLO = 0
            //               TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ivvc0213.getDatiLivello().getIdLivello() == 0) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-LIVELLO NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-LIVELLO NON VALORIZZATO");
            }
        }
        else {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ID-LIVELLO NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ID-LIVELLO NON VALORIZZATO");
        }
    }

    /**Original name: S1400-CHIAMA-LDBSF220<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA LDBSF220 - ESTRAE DETT_TIT_CONT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1400ChiamaLdbsf220() {
        Ldbsf220 ldbsf220 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                       DETT-TIT-CONT.
        initDettTitCont();
        //
        // COB_CODE: MOVE 'TG'                        TO DTC-TP-OGG.
        ws.getDettTitCont().setDtcTpOgg("TG");
        // COB_CODE: MOVE IVVC0213-ID-LIVELLO         TO DTC-ID-OGG.
        ws.getDettTitCont().setDtcIdOgg(ivvc0213.getDatiLivello().getIdLivello());
        //
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA
        //             TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //
        // COB_CODE: SET IDSV0003-SELECT              TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION     TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC
        //             TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL
        //             TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBSF220'                       TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBSF220");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM USING IDSV0003 DETT-TIT-CONT
        //           *
        //                ON EXCEPTION
        //                    SET IDSV0003-INVALID-OPER         TO TRUE
        //                END-CALL.
        try {
            ldbsf220 = Ldbsf220.getInstance();
            ldbsf220.run(idsv0003, ws.getDettTitCont());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'LVVS3390 - ERRORE CHIAMATA LDBSF220'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("LVVS3390 - ERRORE CHIAMATA LDBSF220");
            // COB_CODE: SET IDSV0003-INVALID-OPER         TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                   WHEN IDSV0003-SUCCESSFUL-SQL
            //           *--> PREPARA AREA TIT-CONT
            //                           THRU S1450-CALL-TIT-CONT-EX
            //                   WHEN IDSV0003-NOT-FOUND
            //                        SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //                   WHEN OTHER
            //                        END-STRING
            //           *
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://--> PREPARA AREA TIT-CONT
                    // COB_CODE: PERFORM S1410-PREP-TIT-CONT
                    //              THRU S1410-PREP-TIT-CONT-EX
                    s1410PrepTitCont();
                    //--> SE ESTRAE UN'OCCORRENZA IN DETT_TIT_CONT
                    //    ACCEDO ALL'IDBS STANDARD TIT-CONT
                    // COB_CODE: PERFORM S1450-CALL-TIT-CONT
                    //              THRU S1450-CALL-TIT-CONT-EX
                    s1450CallTitCont();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    break;

                default:// COB_CODE: SET    IDSV0003-INVALID-OPER         TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE   WK-PGM
                    //           TO     IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'LVVS3390 - ERRORE CHIAMATA LDBSF220 ;'
                    //                   IDSV0003-RETURN-CODE ';'
                    //                   IDSV0003-SQLCODE
                    //                   DELIMITED BY SIZE
                    //                   INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "LVVS3390 - ERRORE CHIAMATA LDBSF220 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    //
                    break;
            }
        }
    }

    /**Original name: S1410-PREP-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *    SETTAGGIO IDBS STANDARD TIT-CONT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1410PrepTitCont() {
        // COB_CODE: INITIALIZE                       TIT-CONT
        //                                            WK-CALL-PGM.
        initTitCont();
        ws.setWkCallPgm("");
        //
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-SELECT              TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-ID                  TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC
        //             TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL
        //             TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: MOVE DTC-ID-TIT-CONT           TO TIT-ID-TIT-CONT.
        ws.getTitCont().setTitIdTitCont(ws.getDettTitCont().getDtcIdTitCont());
        // COB_CODE: MOVE 'IDBSTIT0'                TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSTIT0");
    }

    /**Original name: S1450-CALL-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *    ROUTINE ACCESSO IDBS STANDARD TIT-CONT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1450CallTitCont() {
        Idbstit0 idbstit0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE:      CALL WK-CALL-PGM USING IDSV0003 TIT-CONT
        //           *
        //                ON EXCEPTION
        //                    SET IDSV0003-INVALID-OPER         TO TRUE
        //                END-CALL.
        try {
            idbstit0 = Idbstit0.getInstance();
            idbstit0.run(idsv0003, ws.getTitCont());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'LVVS3390 - ERRORE CHIAMATA IDBSTIT0'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("LVVS3390 - ERRORE CHIAMATA IDBSTIT0");
            // COB_CODE: SET IDSV0003-INVALID-OPER         TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                   WHEN IDSV0003-SUCCESSFUL-SQL
            //                        END-IF
            //                   WHEN IDSV0003-NOT-FOUND
            //                        SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //                   WHEN OTHER
            //                        END-STRING
            //           *
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF TIT-TP-STAT-TIT = 'IN'
                    //           OR TIT-TP-STAT-TIT = 'EM'
                    //              END-IF
                    //           ELSE
                    //              MOVE ZEROES            TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    if (Conditions.eq(ws.getTitCont().getTitTpStatTit(), "IN") || Conditions.eq(ws.getTitCont().getTitTpStatTit(), "EM")) {
                        // COB_CODE: IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUES OR LOW-VALUE
                        //                                                 OR SPACES
                        //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        //           ELSE
                        //             MOVE TIT-TOT-PRE-TOT TO IVVC0213-VAL-IMP-O
                        //           END-IF
                        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNullFormatted()) || Characters.EQ_LOW.test(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNullFormatted()) || Characters.EQ_SPACE.test(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNull())) {
                            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                            idsv0003.getReturnCode().setFieldNotValued();
                        }
                        else {
                            // COB_CODE: MOVE TIT-TOT-PRE-TOT TO IVVC0213-VAL-IMP-O
                            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getTitCont().getTitTotPreTot().getTitTotPreTot(), 18, 7));
                        }
                    }
                    else {
                        // COB_CODE: MOVE ZEROES            TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
                    }
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    break;

                default:// COB_CODE: SET    IDSV0003-INVALID-OPER         TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE   WK-PGM
                    //           TO     IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'LVVS3390 - ERRORE CHIAMATA IDBSTIT0 ;'
                    //                   IDSV0003-RETURN-CODE ';'
                    //                   IDSV0003-SQLCODE
                    //                   DELIMITED BY SIZE
                    //                   INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "LVVS3390 - ERRORE CHIAMATA IDBSTIT0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    //
                    break;
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        ws.getDtgaTabTranObj().fill(new W1tgaTabTran().initW1tgaTabTran());
    }

    public void initDettTitCont() {
        ws.getDettTitCont().setDtcIdDettTitCont(0);
        ws.getDettTitCont().setDtcIdTitCont(0);
        ws.getDettTitCont().setDtcIdOgg(0);
        ws.getDettTitCont().setDtcTpOgg("");
        ws.getDettTitCont().setDtcIdMoviCrz(0);
        ws.getDettTitCont().getDtcIdMoviChiu().setDtcIdMoviChiu(0);
        ws.getDettTitCont().setDtcDtIniEff(0);
        ws.getDettTitCont().setDtcDtEndEff(0);
        ws.getDettTitCont().setDtcCodCompAnia(0);
        ws.getDettTitCont().getDtcDtIniCop().setDtcDtIniCop(0);
        ws.getDettTitCont().getDtcDtEndCop().setDtcDtEndCop(0);
        ws.getDettTitCont().getDtcPreNet().setDtcPreNet(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrFraz().setDtcIntrFraz(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrMora().setDtcIntrMora(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrRetdt().setDtcIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrRiat().setDtcIntrRiat(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcDir().setDtcDir(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSpeMed().setDtcSpeMed(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcTax().setDtcTax(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprSan().setDtcSoprSan(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprSpo().setDtcSoprSpo(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprTec().setDtcSoprTec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprProf().setDtcSoprProf(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprAlt().setDtcSoprAlt(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPreTot().setDtcPreTot(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPrePpIas().setDtcPrePpIas(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPreSoloRsh().setDtcPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarAcq().setDtcCarAcq(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarGest().setDtcCarGest(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarInc().setDtcCarInc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvAcq1aa().setDtcProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvAcq2aa().setDtcProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvRicor().setDtcProvRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvInc().setDtcProvInc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvDaRec().setDtcProvDaRec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().setDtcCodDvs("");
        ws.getDettTitCont().getDtcFrqMovi().setDtcFrqMovi(0);
        ws.getDettTitCont().setDtcTpRgmFisc("");
        ws.getDettTitCont().setDtcCodTari("");
        ws.getDettTitCont().setDtcTpStatTit("");
        ws.getDettTitCont().getDtcImpAz().setDtcImpAz(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpAder().setDtcImpAder(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpTfr().setDtcImpTfr(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpVolo().setDtcImpVolo(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeAntic().setDtcManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeRicor().setDtcManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeRec().setDtcManfeeRec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcDtEsiTit().setDtcDtEsiTit(0);
        ws.getDettTitCont().getDtcSpeAge().setDtcSpeAge(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarIas().setDtcCarIas(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcTotIntrPrest().setDtcTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().setDtcDsRiga(0);
        ws.getDettTitCont().setDtcDsOperSql(Types.SPACE_CHAR);
        ws.getDettTitCont().setDtcDsVer(0);
        ws.getDettTitCont().setDtcDsTsIniCptz(0);
        ws.getDettTitCont().setDtcDsTsEndCptz(0);
        ws.getDettTitCont().setDtcDsUtente("");
        ws.getDettTitCont().setDtcDsStatoElab(Types.SPACE_CHAR);
        ws.getDettTitCont().getDtcImpTrasfe().setDtcImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpTfrStrc().setDtcImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(0);
        ws.getDettTitCont().getDtcNumGgRival().setDtcNumGgRival(0);
        ws.getDettTitCont().getDtcAcqExp().setDtcAcqExp(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcRemunAss().setDtcRemunAss(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCommisInter().setDtcCommisInter(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCnbtAntirac().setDtcCnbtAntirac(new AfDecimal(0, 15, 3));
    }

    public void initTitCont() {
        ws.getTitCont().setTitIdTitCont(0);
        ws.getTitCont().setTitIdOgg(0);
        ws.getTitCont().setTitTpOgg("");
        ws.getTitCont().setTitIbRich("");
        ws.getTitCont().setTitIdMoviCrz(0);
        ws.getTitCont().getTitIdMoviChiu().setTitIdMoviChiu(0);
        ws.getTitCont().setTitDtIniEff(0);
        ws.getTitCont().setTitDtEndEff(0);
        ws.getTitCont().setTitCodCompAnia(0);
        ws.getTitCont().setTitTpTit("");
        ws.getTitCont().getTitProgTit().setTitProgTit(0);
        ws.getTitCont().setTitTpPreTit("");
        ws.getTitCont().setTitTpStatTit("");
        ws.getTitCont().getTitDtIniCop().setTitDtIniCop(0);
        ws.getTitCont().getTitDtEndCop().setTitDtEndCop(0);
        ws.getTitCont().getTitImpPag().setTitImpPag(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitFlSoll(Types.SPACE_CHAR);
        ws.getTitCont().getTitFraz().setTitFraz(0);
        ws.getTitCont().getTitDtApplzMora().setTitDtApplzMora(0);
        ws.getTitCont().setTitFlMora(Types.SPACE_CHAR);
        ws.getTitCont().getTitIdRappRete().setTitIdRappRete(0);
        ws.getTitCont().getTitIdRappAna().setTitIdRappAna(0);
        ws.getTitCont().setTitCodDvs("");
        ws.getTitCont().getTitDtEmisTit().setTitDtEmisTit(0);
        ws.getTitCont().getTitDtEsiTit().setTitDtEsiTit(0);
        ws.getTitCont().getTitTotPreNet().setTitTotPreNet(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrFraz().setTitTotIntrFraz(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrMora().setTitTotIntrMora(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrRetdt().setTitTotIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrRiat().setTitTotIntrRiat(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotDir().setTitTotDir(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSpeMed().setTitTotSpeMed(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotTax().setTitTotTax(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprSan().setTitTotSoprSan(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprTec().setTitTotSoprTec(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprSpo().setTitTotSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprProf().setTitTotSoprProf(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprAlt().setTitTotSoprAlt(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPreTot().setTitTotPreTot(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPrePpIas().setTitTotPrePpIas(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarAcq().setTitTotCarAcq(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarGest().setTitTotCarGest(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarInc().setTitTotCarInc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPreSoloRsh().setTitTotPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvAcq1aa().setTitTotProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvAcq2aa().setTitTotProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvRicor().setTitTotProvRicor(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvInc().setTitTotProvInc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvDaRec().setTitTotProvDaRec(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpAz().setTitImpAz(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpAder().setTitImpAder(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpTfr().setTitImpTfr(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpVolo().setTitImpVolo(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeAntic().setTitTotManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeRicor().setTitTotManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeRec().setTitTotManfeeRec(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitTpMezPagAdd("");
        ws.getTitCont().setTitEstrCntCorrAdd("");
        ws.getTitCont().getTitDtVlt().setTitDtVlt(0);
        ws.getTitCont().setTitFlForzDtVlt(Types.SPACE_CHAR);
        ws.getTitCont().getTitDtCambioVlt().setTitDtCambioVlt(0);
        ws.getTitCont().getTitTotSpeAge().setTitTotSpeAge(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarIas().setTitTotCarIas(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitNumRatAccorpate().setTitNumRatAccorpate(0);
        ws.getTitCont().setTitDsRiga(0);
        ws.getTitCont().setTitDsOperSql(Types.SPACE_CHAR);
        ws.getTitCont().setTitDsVer(0);
        ws.getTitCont().setTitDsTsIniCptz(0);
        ws.getTitCont().setTitDsTsEndCptz(0);
        ws.getTitCont().setTitDsUtente("");
        ws.getTitCont().setTitDsStatoElab(Types.SPACE_CHAR);
        ws.getTitCont().setTitFlTitDaReinvst(Types.SPACE_CHAR);
        ws.getTitCont().getTitDtRichAddRid().setTitDtRichAddRid(0);
        ws.getTitCont().setTitTpEsiRid("");
        ws.getTitCont().setTitCodIban("");
        ws.getTitCont().getTitImpTrasfe().setTitImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpTfrStrc().setTitImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitDtCertFisc().setTitDtCertFisc(0);
        ws.getTitCont().getTitTpCausStor().setTitTpCausStor(0);
        ws.getTitCont().setTitTpCausDispStor("");
        ws.getTitCont().setTitTpTitMigraz("");
        ws.getTitCont().getTitTotAcqExp().setTitTotAcqExp(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotRemunAss().setTitTotRemunAss(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCommisInter().setTitTotCommisInter(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitTpCausRimb("");
        ws.getTitCont().getTitTotCnbtAntirac().setTitTotCnbtAntirac(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitFlIncAutogen(Types.SPACE_CHAR);
    }
}
