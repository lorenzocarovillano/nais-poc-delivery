package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0145Data;

/**Original name: LVVS0145<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0145
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0145 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0145Data ws = new Lvvs0145Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0145
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0145_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0145 getInstance() {
        return ((Lvvs0145)Programs.getInstance(Lvvs0145.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-LIQUID.
        initAreaIoLiquid();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS0145.cbl:line=116, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              PERFORM S1251-CALCOLA-TOT            THRU S1251-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1251-CALCOLA-TOT            THRU S1251-EX
            s1251CalcolaTot();
        }
    }

    /**Original name: S1251-CALCOLA-TOT<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 2
	 * ----------------------------------------------------------------*</pre>*/
    private void s1251CalcolaTot() {
        Ldbs2270 ldbs2270 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE AREA-LDBV2271.
        initAreaLdbv2271();
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE           TO LDBV2271-ID-OGG.
        ws.getAreaLdbv2271().setLdbv2271IdOgg(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE 'AD'                           TO LDBV2271-TP-OGG.
        ws.getAreaLdbv2271().setLdbv2271TpOgg("AD");
        // COB_CODE: MOVE 'CE'                           TO LDBV2271-TP-LIQ.
        ws.getAreaLdbv2271().setLdbv2271TpLiq("CE");
        //
        // COB_CODE: MOVE 'LDBS2270'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS2270");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 AREA-LDBV2271
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs2270 = Ldbs2270.getInstance();
            ldbs2270.run(idsv0003, ws.getAreaLdbv2271());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS5480 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS5480 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBV2271-IMP-LRD-LIQTO
            //             TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getAreaLdbv2271().getLdbv2271ImpLrdLiqto(), 18, 7));
        }
        else if (idsv0003.getSqlcode().getSqlcode() == 100 || idsv0003.getSqlcode().getSqlcode() == -305) {
            // COB_CODE: IF IDSV0003-SQLCODE = +100
            //           OR IDSV0003-SQLCODE = -305
            //               TO IDSV0003-DESCRIZ-ERR-DB2
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'LVVS0145 - TOTALE NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("LVVS0145 - TOTALE NON VALORIZZATO");
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS5480 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5480 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoLiquid() {
        ws.setDlquEleLiqMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0145Data.DLQU_TAB_LIQ_MAXOCCURS; idx0++) {
            ws.getDlquTabLiq(idx0).getLccvlqu1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDlquTabLiq(idx0).getLccvlqu1().setWlquIdPtf(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIdLiq(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIdOgg(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpOgg("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIdMoviCrz(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089IdMoviChiu().setWlquIdMoviChiu(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDtIniEff(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDtEndEff(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodCompAnia(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIbOgg("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpLiq("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDescCauEveSin("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodCauSin("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodSinCatstrf("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtMor().setWlquDtMor(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtDen().setWlquDtDen(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtPervDen().setWlquDtPervDen(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtRich().setWlquDtRich(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpSin("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpRisc("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TpMetRisc().setWlquTpMetRisc(((short)0));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtLiq().setWlquDtLiq(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquCodDvs("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpLrdLiqto().setWlquTotImpLrdLiqto(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpPrest().setWlquTotImpPrest(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpIntrPrest().setWlquTotImpIntrPrest(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpUti().setWlquTotImpUti(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRitTfr().setWlquTotImpRitTfr(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRitAcc().setWlquTotImpRitAcc(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRitVis().setWlquTotImpRitVis(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpbTfr().setWlquTotImpbTfr(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpbAcc().setWlquTotImpbAcc(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpbVis().setWlquTotImpbVis(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpRimb().setWlquTotImpRimb(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbImpstPrvr().setWlquImpbImpstPrvr(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstPrvr().setWlquImpstPrvr(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbImpst252().setWlquImpbImpst252(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089Impst252().setWlquImpst252(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpIs().setWlquTotImpIs(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpDirLiq().setWlquImpDirLiq(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotImpNetLiqto().setWlquTotImpNetLiqto(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089MontEnd2000().setWlquMontEnd2000(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089MontEnd2006().setWlquMontEnd2006(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRen().setWlquPcRen(new AfDecimal(0, 6, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpPnl().setWlquImpPnl(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIrpef().setWlquImpbIrpef(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstIrpef().setWlquImpstIrpef(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtVlt().setWlquDtVlt(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089DtEndIstr().setWlquDtEndIstr(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpRimb("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089SpeRcs().setWlquSpeRcs(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquIbLiq("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasOnerPrvnt().setWlquTotIasOnerPrvnt(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasMggSin().setWlquTotIasMggSin(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasRstDpst().setWlquTotIasRstDpst(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpOnerLiq().setWlquImpOnerLiq(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ComponTaxRimb().setWlquComponTaxRimb(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpMezPag("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpExcontr().setWlquImpExcontr(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpIntrRitPag().setWlquImpIntrRitPag(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089BnsNonGoduto().setWlquBnsNonGoduto(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089CnbtInpstfm().setWlquCnbtInpstfm(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstDaRimb().setWlquImpstDaRimb(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIs().setWlquImpbIs(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TaxSep().setWlquTaxSep(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbTaxSep().setWlquImpbTaxSep(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIntrSuPrest().setWlquImpbIntrSuPrest(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089AddizComun().setWlquAddizComun(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbAddizComun().setWlquImpbAddizComun(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089AddizRegion().setWlquAddizRegion(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbAddizRegion().setWlquImpbAddizRegion(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089MontDal2007().setWlquMontDal2007(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbCnbtInpstfm().setWlquImpbCnbtInpstfm(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpLrdDaRimb().setWlquImpLrdDaRimb(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpDirDaRimb().setWlquImpDirDaRimb(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089RisMat().setWlquRisMat(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089RisSpe().setWlquRisSpe(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsRiga(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsOperSql(Types.SPACE_CHAR);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsVer(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsTsIniCptz(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsTsEndCptz(0);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsUtente("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquDsStatoElab(Types.SPACE_CHAR);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089TotIasPnl().setWlquTotIasPnl(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquFlEveGarto(Types.SPACE_CHAR);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpRenK1().setWlquImpRenK1(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpRenK2().setWlquImpRenK2(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpRenK3().setWlquImpRenK3(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRenK1().setWlquPcRenK1(new AfDecimal(0, 6, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRenK2().setWlquPcRenK2(new AfDecimal(0, 6, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRenK3().setWlquPcRenK3(new AfDecimal(0, 6, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquTpCausAntic("");
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpLrdLiqtoRilt().setWlquImpLrdLiqtoRilt(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstApplRilt().setWlquImpstApplRilt(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcRiscParz().setWlquPcRiscParz(new AfDecimal(0, 12, 5));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloTotV().setWlquImpstBolloTotV(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloDettC().setWlquImpstBolloDettC(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloTotSw().setWlquImpstBolloTotSw(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstBolloTotAa().setWlquImpstBolloTotAa(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbVis1382011().setWlquImpbVis1382011(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstVis1382011().setWlquImpstVis1382011(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIs1382011().setWlquImpbIs1382011(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstSost1382011().setWlquImpstSost1382011(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcAbbTitStat().setWlquPcAbbTitStat(new AfDecimal(0, 6, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbBolloDettC().setWlquImpbBolloDettC(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().setWlquFlPreComp(Types.SPACE_CHAR);
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbVis662014().setWlquImpbVis662014(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstVis662014().setWlquImpstVis662014(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpbIs662014().setWlquImpbIs662014(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpstSost662014().setWlquImpstSost662014(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089PcAbbTs662014().setWlquPcAbbTs662014(new AfDecimal(0, 6, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089ImpLrdCalcCp().setWlquImpLrdCalcCp(new AfDecimal(0, 15, 3));
            ws.getDlquTabLiq(idx0).getLccvlqu1().getDati().getS089CosTunnelUscita().setWlquCosTunnelUscita(new AfDecimal(0, 15, 3));
        }
    }

    public void initAreaLdbv2271() {
        ws.getAreaLdbv2271().setLdbv2271IdOgg(0);
        ws.getAreaLdbv2271().setLdbv2271TpOgg("");
        ws.getAreaLdbv2271().setLdbv2271TpLiq("");
        ws.getAreaLdbv2271().setLdbv2271ImpLrdLiqto(new AfDecimal(0, 15, 3));
    }
}
