package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsi0011Area;
import it.accenture.jnais.copy.WpmoDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoLccs0029;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.Idsv0001LivelloDebug;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccs0320Data;
import it.accenture.jnais.ws.redefines.WpmoDtRicorPrec;
import it.accenture.jnais.ws.redefines.WpmoDurAa;
import it.accenture.jnais.ws.redefines.WpmoDurMm;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WcomAreaLccc0320;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WpmoAreaParamMovi;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;
import static java.lang.Math.abs;

/**Original name: LCCS0320<br>
 * <pre>  ============================================================= *
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 *   ============================================================= *
 * AUTHOR.             AISS.
 * DATE-WRITTEN.       MAGGIO 2008.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LCCS0320
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CALCOLO DATA RICORRENZA SUCCESSIVA
 * **------------------------------------------------------------***</pre>*/
public class Lccs0320 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0320Data ws = new Lccs0320Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WPMO-AREA-PMO
    private WpmoAreaParamMovi wpmoAreaPmo;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TGA
    private WtgaAreaTrancheLoas0800 wtgaAreaTga;
    //Original name: WCOM-AREA-LCCC0320
    private WcomAreaLccc0320 wcomAreaLccc0320;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0320_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WpmoAreaParamMovi wpmoAreaPmo, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLoas0800 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTga, WcomAreaLccc0320 wcomAreaLccc0320) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wpmoAreaPmo = wpmoAreaPmo;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTga = wtgaAreaTga;
        this.wcomAreaLccc0320 = wcomAreaLccc0320;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        //
        // COB_CODE: PERFORM S9000-OPERAZ-FINALI
        //              THRU EX-S9000.
        s9000OperazFinali();
        return 0;
    }

    public static Lccs0320 getInstance() {
        return ((Lccs0320)Programs.getInstance(Lccs0320.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE WPOL-TP-FRM-ASSVA
        //             TO WS-TP-FRM-ASSVA.
        ws.getWsTpFrmAssva().setWsTpFrmAssva(wpolAreaPolizza.getLccvpol1().getDati().getWpolTpFrmAssva());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE:      EVALUATE IDSV0001-TIPO-MOVIMENTO
        //           *
        //                   WHEN 6101
        //                   WHEN 6002
        //                   WHEN 6003
        //                   WHEN 6102
        //           *
        //                           THRU GESTINONE-PMO-EX
        //           *
        //                   WHEN 6006
        //           *
        //                          THRU GESTIONE-RIVAL-EX
        //           *
        //                   WHEN 6009
        //           *
        //                          THRU GESTIONE-SCADE-EX
        //           *
        //                   WHEN 6013
        //                   WHEN 6016
        //           *
        //                          THRU GESTIONE-BONUS-EX
        //           *
        //           *
        //                   WHEN OTHER
        //           *
        //                        CONTINUE
        //           *
        //                END-EVALUATE.
        switch (areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento()) {

            case 6101:
            case 6002:
            case 6003:
            case 6102://
                // COB_CODE: PERFORM GESTINONE-PMO
                //               THRU GESTINONE-PMO-EX
                gestinonePmo();
                //
                break;

            case 6006://
                // COB_CODE: PERFORM GESTIONE-RIVAL
                //              THRU GESTIONE-RIVAL-EX
                gestioneRival();
                //
                break;

            case 6009://
                // COB_CODE: PERFORM GESTIONE-SCADE
                //              THRU GESTIONE-SCADE-EX
                gestioneScade();
                //
                break;

            case 6013:
            case 6016://
                // COB_CODE: PERFORM GESTIONE-BONUS
                //              THRU GESTIONE-BONUS-EX
                gestioneBonus();
                //
                //
                break;

            default://
            // COB_CODE: CONTINUE
            //continue
            //
                break;
        }
    }

    /**Original name: GESTINONE-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALCOLO DATA RICORRENZA SUCCESSIVA (QUIETANZAMENTO)
	 * ----------------------------------------------------------------*</pre>*/
    private void gestinonePmo() {
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: IF GENER-TRAINC
        //               THRU S10100-6003-AGGIUNG-FRAZ-EX
        //           ELSE
        //               THRU S10100-6101-AGGIUNG-FRAZ-EX
        //           END-IF.
        if (ws.getWsMovimento().isGenerTrainc()) {
            // COB_CODE: PERFORM S10100-6003-AGGIUNG-FRAZ
            //              THRU S10100-6003-AGGIUNG-FRAZ-EX
            s101006003AggiungFraz();
        }
        else {
            // COB_CODE: PERFORM S10100-6101-AGGIUNG-FRAZ
            //               THRU S10100-6101-AGGIUNG-FRAZ-EX
            s101006101AggiungFraz();
        }
        //
        //    MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //                AND NOT GENER-TRAINC
        //           *
        //                      THRU S10200-6101-VALIDA-DATA-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && !ws.getWsMovimento().isGenerTrainc()) {
            //
            // COB_CODE: PERFORM S10200-6101-VALIDA-DATA
            //              THRU S10200-6101-VALIDA-DATA-EX
            s102006101ValidaData();
            //
        }
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getWkIndici().setPmo(((short)1));
        while (!(ws.getWkIndici().getPmo() > wpmoAreaPmo.getEleParamMovMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE:          IF NOT WPMO-ST-DEL(IX-TAB-PMO)
            //           *
            //                       END-IF
            //           *
            //                    ELSE
            //                       END-IF
            //                    END-IF
            if (!wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().isDel()) {
                //
                // COB_CODE: MOVE WPMO-TP-MOVI(IX-TAB-PMO)
                //             TO WS-MOVIMENTO
                ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi(), 5));
                //
                // COB_CODE:             IF GENER-TRANCH OR MOVIM-QUIETA OR
                //                          MOVIM-QUIETA-INT-PREST
                //                       OR GENER-TRAINC
                //           *
                //                             THRU S10300-6101-CICLO-PMO-GRZ-EX
                //           *
                //                       END-IF
                if (ws.getWsMovimento().isGenerTranch() || ws.getWsMovimento().isMovimQuieta() || ws.getWsMovimento().isMovimQuietaIntPrest() || ws.getWsMovimento().isGenerTrainc()) {
                    //
                    // COB_CODE: PERFORM S10300-6101-CICLO-PMO-GRZ
                    //              THRU S10300-6101-CICLO-PMO-GRZ-EX
                    s103006101CicloPmoGrz();
                    //
                }
                //
            }
            else {
                // COB_CODE: MOVE WPMO-TP-MOVI(IX-TAB-PMO)
                //             TO WS-MOVIMENTO
                ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi(), 5));
                // COB_CODE: IF GENER-TRAINC
                //                 THRU S10500-6101-VERIF-VAR-FRAZ-EX
                //           END-IF
                if (ws.getWsMovimento().isGenerTrainc()) {
                    // COB_CODE: PERFORM S10500-6101-VERIF-VAR-FRAZ
                    //              THRU S10500-6101-VERIF-VAR-FRAZ-EX
                    s105006101VerifVarFraz();
                }
            }
            //
            ws.getWkIndici().setPmo(Trunc.toShort(ws.getWkIndici().getPmo() + 1, 4));
        }
    }

    /**Original name: S10100-6101-AGGIUNG-FRAZ<br>
	 * <pre>----------------------------------------------------------------*
	 *      AGGIUNGO UN FRAZIONAMENTO ALLA RICORRENZA ATTUALE
	 * ----------------------------------------------------------------*
	 *  --> A2K-DELTA -> Delta Da Sommare
	 *  --> Mensile</pre>*/
    private void s101006101AggiungFraz() {
        // COB_CODE:      EVALUATE WPMO-FRQ-MOVI(1)
        //           * --> Mensile
        //                    WHEN 12
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Bimestrale
        //                    WHEN 6
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Trimestrale
        //                    WHEN 4
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Quadrimestrale
        //                    WHEN 3
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Semestrale
        //                    WHEN 2
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Annuale
        //                    WHEN 1
        //           *
        //                        TO A2K-DELTA
        //           *
        //                END-EVALUATE.
        switch (wpmoAreaPmo.getTabParamMov(1).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi()) {

            case 12://
                // COB_CODE: MOVE 1
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
                //
                // --> Bimestrale
                break;

            case 6://
                // COB_CODE: MOVE 2
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)2));
                //
                // --> Trimestrale
                break;

            case 4://
                // COB_CODE: MOVE 3
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)3));
                //
                // --> Quadrimestrale
                break;

            case 3://
                // COB_CODE: MOVE 4
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)4));
                //
                // --> Semestrale
                break;

            case 2://
                // COB_CODE: MOVE 6
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)6));
                //
                // --> Annuale
                break;

            case 1://
                // COB_CODE: MOVE 12
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)12));
                //
                break;

            default:break;
        }
        //
        // --> A2K-TDELTA -> Tipo Delta: Mesi(M) o Anni(A)
        //
        // COB_CODE: IF WCOM-ACCORPATE-SI
        //              COMPUTE A2K-DELTA = A2K-DELTA * WCOM-NUM-RATE-ACCORPATE
        //           END-IF.
        if (wcomAreaLccc0320.getFlRateAccorpate().isSi()) {
            // COB_CODE: COMPUTE A2K-DELTA = A2K-DELTA * WCOM-NUM-RATE-ACCORPATE
            ws.getIoA2kLccc0003().getInput().setA2kDelta(Trunc.toShort(abs(ws.getIoA2kLccc0003().getInput().getA2kDelta() * wcomAreaLccc0320.getNumRateAccorpate()), 3));
        }
        // COB_CODE: MOVE 'M'                     TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(1)
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamg(TruncAbs.toInt(wpmoAreaPmo.getTabParamMov(1).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc(), 8));
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX
        callLccs0003();
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //                TO WK-DT-CALCOLATA
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG
            //            TO WK-DT-CALCOLATA
            ws.setWkDtCalcolataFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: S10100-6003-AGGIUNG-FRAZ<br>
	 * <pre>---------------------------------------------------------------- *
	 *      AGGIUNGO UN FRAZIONAMENTO ALLA RICORRENZA ATTUALE
	 *      PER GETRA DA INCASSO
	 * ----------------------------------------------------------------*</pre>*/
    private void s101006003AggiungFraz() {
        // COB_CODE: MOVE ZERO            TO WK-FRQ-MOVI
        //                                   WK-DT-RICOR-SUCC
        ws.setWkFrqMovi(0);
        ws.setWkDtRicorSucc(0);
        // COB_CODE: SET PMO-TROV-NO      TO TRUE
        ws.getPmoTrov().setNo();
        // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //             UNTIL IX-TAB-PMO > WK-PMO-MAX-B
        //                OR PMO-TROV-SI
        //             END-IF
        //           END-PERFORM
        ws.getWkIndici().setPmo(((short)1));
        while (!(ws.getWkIndici().getPmo() > ws.getLccvpmoz().getMaxB() || ws.getPmoTrov().isSi())) {
            // COB_CODE: IF  WPMO-TP-MOVI(IX-TAB-PMO) = 6003
            //           AND NOT WPMO-ST-DEL(IX-TAB-PMO)
            //               SET PMO-TROV-SI      TO TRUE
            //           END-IF
            if (wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == 6003 && !wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().isDel()) {
                // COB_CODE: IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT =
                //              HIGH-VALUES
                //                TO WK-FRQ-MOVI
                //           END-IF
                if (!Characters.EQ_HIGH.test(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNullFormatted())) {
                    // COB_CODE: MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                    //             TO WK-FRQ-MOVI
                    ws.setWkFrqMovi(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi());
                }
                // COB_CODE: IF WPMO-DT-RICOR-SUCC-NULL(IX-TAB-PMO) NOT =
                //              HIGH-VALUES
                //                TO WK-DT-RICOR-SUCC
                //           END-IF
                if (!Characters.EQ_HIGH.test(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSuccNullFormatted())) {
                    // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                    //             TO WK-DT-RICOR-SUCC
                    ws.setWkDtRicorSucc(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
                }
                // COB_CODE: SET PMO-TROV-SI      TO TRUE
                ws.getPmoTrov().setSi();
            }
            ws.getWkIndici().setPmo(Trunc.toShort(ws.getWkIndici().getPmo() + 1, 4));
        }
        // COB_CODE: IF PMO-TROV-NO
        //              END-PERFORM
        //           END-IF
        if (ws.getPmoTrov().isNo()) {
            // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
            //             UNTIL IX-TAB-PMO > WK-PMO-MAX-B
            //                OR PMO-TROV-SI
            //             END-IF
            //           END-PERFORM
            ws.getWkIndici().setPmo(((short)1));
            while (!(ws.getWkIndici().getPmo() > ws.getLccvpmoz().getMaxB() || ws.getPmoTrov().isSi())) {
                // COB_CODE: IF  WPMO-TP-MOVI(IX-TAB-PMO) = 6003
                //           AND WPMO-ST-DEL(IX-TAB-PMO)
                //               SET PMO-TROV-SI      TO TRUE
                //           END-IF
                if (wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == 6003 && wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().isDel()) {
                    // COB_CODE: IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT =
                    //              HIGH-VALUES
                    //                TO WK-FRQ-MOVI
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNullFormatted())) {
                        // COB_CODE: MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                        //             TO WK-FRQ-MOVI
                        ws.setWkFrqMovi(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi());
                    }
                    // COB_CODE: IF WPMO-DT-RICOR-SUCC-NULL(IX-TAB-PMO) NOT =
                    //              HIGH-VALUES
                    //                TO WK-DT-RICOR-SUCC
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSuccNullFormatted())) {
                        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        //             TO WK-DT-RICOR-SUCC
                        ws.setWkDtRicorSucc(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
                    }
                    // COB_CODE: SET PMO-TROV-SI      TO TRUE
                    ws.getPmoTrov().setSi();
                }
                ws.getWkIndici().setPmo(Trunc.toShort(ws.getWkIndici().getPmo() + 1, 4));
            }
        }
        // --> A2K-DELTA -> Delta Da Sommare
        //
        // --> Mensile
        // COB_CODE:      EVALUATE WK-FRQ-MOVI
        //           * --> Mensile
        //                    WHEN 12
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Bimestrale
        //                    WHEN 6
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Trimestrale
        //                    WHEN 4
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Quadrimestrale
        //                    WHEN 3
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Semestrale
        //                    WHEN 2
        //           *
        //                        TO A2K-DELTA
        //           *
        //           * --> Annuale
        //                    WHEN 1
        //           *
        //                        TO A2K-DELTA
        //           *
        //                END-EVALUATE.
        switch (ws.getWkFrqMovi()) {

            case 12://
                // COB_CODE: MOVE 1
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
                //
                // --> Bimestrale
                break;

            case 6://
                // COB_CODE: MOVE 2
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)2));
                //
                // --> Trimestrale
                break;

            case 4://
                // COB_CODE: MOVE 3
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)3));
                //
                // --> Quadrimestrale
                break;

            case 3://
                // COB_CODE: MOVE 4
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)4));
                //
                // --> Semestrale
                break;

            case 2://
                // COB_CODE: MOVE 6
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)6));
                //
                // --> Annuale
                break;

            case 1://
                // COB_CODE: MOVE 12
                //             TO A2K-DELTA
                ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)12));
                //
                break;

            default:break;
        }
        //
        // --> A2K-TDELTA -> Tipo Delta: Mesi(M) o Anni(A)
        //
        // COB_CODE: IF WCOM-ACCORPATE-SI
        //              COMPUTE A2K-DELTA = A2K-DELTA * WCOM-NUM-RATE-ACCORPATE
        //           END-IF.
        if (wcomAreaLccc0320.getFlRateAccorpate().isSi()) {
            // COB_CODE: COMPUTE A2K-DELTA = A2K-DELTA * WCOM-NUM-RATE-ACCORPATE
            ws.getIoA2kLccc0003().getInput().setA2kDelta(Trunc.toShort(abs(ws.getIoA2kLccc0003().getInput().getA2kDelta() * wcomAreaLccc0320.getNumRateAccorpate()), 3));
        }
        // COB_CODE: MOVE 'M'                     TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE WK-DT-RICOR-SUCC
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamg(TruncAbs.toInt(ws.getWkDtRicorSucc(), 8));
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX
        callLccs0003();
        // COB_CODE: IF  IDSV0001-ESITO-OK
        //                TO WK-DT-CALCOLATA
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG
            //            TO WK-DT-CALCOLATA
            ws.setWkDtCalcolataFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: S10150-6101-CALCOLA-LIM<br>
	 * <pre>---------------------------------------------------------------- *
	 *      AGGIUNGO UN FRAZIONAMENTO ALLA RICORRENZA ATTUALE
	 * ----------------------------------------------------------------*
	 *  --> A2K-DELTA -> Delta Da Sommare</pre>*/
    private void s101506101CalcolaLim() {
        // COB_CODE: IF WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
        //            NOT =  HIGH-VALUE
        //             MOVE WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)   TO A2K-DELTA
        //           END-IF
        if (!Characters.EQ_HIGH.test(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().getWgrzNumAaPagPreNullFormatted())) {
            // COB_CODE: MOVE WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)   TO A2K-DELTA
            ws.getIoA2kLccc0003().getInput().setA2kDelta(TruncAbs.toShort(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().getWgrzNumAaPagPre(), 3));
        }
        //
        // COB_CODE: MOVE 'A'                     TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE WGRZ-DT-DECOR(IX-TAB-GRZ)
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamg(TruncAbs.toInt(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor(), 8));
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX
        callLccs0003();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                TO WK-DT-LIMITE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG
            //            TO WK-DT-LIMITE
            ws.setWkDtLimiteFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: CALL-LCCS0003<br>
	 * <pre>*
	 * ----------------------------------------------------------------*
	 *      CHIAMATA SERVIZIO CALCOLI SU DATE - LCCS0003 -
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: MOVE 'CALL-LCCS0003'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("CALL-LCCS0003");
        //
        //
        // COB_CODE:      CALL LCCS0003         USING IO-A2K-LCCC0003
        //                                            IN-RCODE
        //           *
        //                ON EXCEPTION
        //           *
        //                      THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lccs0320Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CALL SERVIZIO LCCS0003'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CALL SERVIZIO LCCS0003");
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
        // COB_CODE:      IF IN-RCODE  = ZEROES
        //                    CONTINUE
        //           *        MOVE A2K-OUAMG
        //           *         TO WK-DT-CALCOLATA
        //           *
        //                ELSE
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (Characters.EQ_ZERO.test(ws.getInRcodeFormatted())) {
        // COB_CODE: CONTINUE
        //continue
        //        MOVE A2K-OUAMG
        //         TO WK-DT-CALCOLATA
        //
        }
        else {
            //
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CALL LCCS0003'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE CALL LCCS0003");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: S10300-6101-CICLO-PMO-GRZ<br>
	 * <pre>----------------------------------------------------------------*
	 *      ALLINEAMENTO AREA PMO CON QUELLA DI GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s103006101CicloPmoGrz() {
        // COB_CODE:      PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //                  UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                    END-IF
        //           *
        //                END-PERFORM.
        ws.getWkIndici().setGrz(((short)1));
        while (!(ws.getWkIndici().getGrz() > wgrzAreaGaranzia.getEleGarMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE:          IF WGRZ-ID-GAR(IX-TAB-GRZ) =
            //                       WPMO-ID-OGG(IX-TAB-PMO)
            //           *
            //                      END-IF
            //           *
            //                    END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar() == wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoIdOgg()) {
                //
                // COB_CODE: INITIALIZE WK-DT-LIMITE
                ws.setWkDtLimiteFormatted("00000000");
                // COB_CODE: IF WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
                //                                       NOT = HIGH-VALUE
                //             THRU S10150-6101-CALCOLA-LIM-EX
                //           END-IF
                if (!Characters.EQ_HIGH.test(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzNumAaPagPre().getWgrzNumAaPagPreNullFormatted())) {
                    // COB_CODE: PERFORM S10150-6101-CALCOLA-LIM
                    //           THRU S10150-6101-CALCOLA-LIM-EX
                    s101506101CalcolaLim();
                }
                // COB_CODE: IF IDSV0001-ESITO-OK
                //               THRU S10400-6101-VERIFICA-DATA-EX
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S10400-6101-VERIFICA-DATA
                    //              THRU S10400-6101-VERIFICA-DATA-EX
                    s104006101VerificaData();
                }
                //
            }
            //
            ws.getWkIndici().setGrz(Trunc.toShort(ws.getWkIndici().getGrz() + 1, 4));
        }
    }

    /**Original name: S10200-6101-VALIDA-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALIDAZIONE DELLA DATA IN FUNZIONE DELLA DECORRENZA DI TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private void s102006101ValidaData() {
        // COB_CODE:      IF INDIVIDUALE
        //           *
        //                     TO WK-APPO-DT-NUM
        //           *
        //                ELSE
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (ws.getWsTpFrmAssva().isIndividuale()) {
            //
            // COB_CODE: MOVE WPOL-DT-DECOR
            //             TO WK-APPO-DT-NUM
            ws.setWkAppoDtNum(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor(), 8));
            //
        }
        else {
            //
            // COB_CODE:         IF WADE-DT-DECOR-NULL(1) NOT = HIGH-VALUES
            //           *
            //                        TO WK-APPO-DT-NUM
            //           *
            //                   ELSE
            //           *
            //                        TO WK-APPO-DT-NUM
            //           *
            //                   END-IF
            if (!Characters.EQ_HIGH.test(wadeAreaAdesione.getTabAde(1).getLccvade1().getDati().getWadeDtDecor().getWadeDtDecorNullFormatted())) {
                //
                // COB_CODE: MOVE WADE-DT-DECOR(1)
                //             TO WK-APPO-DT-NUM
                ws.setWkAppoDtNum(TruncAbs.toInt(wadeAreaAdesione.getTabAde(1).getLccvade1().getDati().getWadeDtDecor().getWadeDtDecor(), 8));
                //
            }
            else {
                //
                // COB_CODE: MOVE WPOL-DT-DECOR
                //             TO WK-APPO-DT-NUM
                ws.setWkAppoDtNum(TruncAbs.toInt(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor(), 8));
                //
            }
            //
        }
        //
        // COB_CODE: MOVE WK-APPO-DT-NUM
        //             TO WK-DT-DEC.
        ws.getWkDtDec().setWkDtDecFormatted(ws.getWkAppoDtNumFormatted());
        //
        // COB_CODE: MOVE WK-DT-CALCOLATA
        //             TO WK-APPO-DT-NUM.
        ws.setWkAppoDtNumFormatted(ws.getWkDtCalcolataFormatted());
        // COB_CODE: MOVE WK-APPO-DT-NUM
        //             TO WK-DT-CALC.
        ws.getWkDtCalc().setWkDtCalcFormatted(ws.getWkAppoDtNumFormatted());
        //
        // COB_CODE:      IF WK-DT-DEC-GG NOT = WK-DT-CALC-GG
        //           *
        //                      THRU S10210-6101-VALID-FIN-MESE-EX
        //           *
        //                END-IF.
        if (ws.getWkDtDec().getGg() != ws.getWkDtCalc().getGg()) {
            //
            // COB_CODE: MOVE WK-DT-DEC-GG
            //             TO WK-DT-CALC-GG
            ws.getWkDtCalc().setGgFormatted(ws.getWkDtDec().getGgFormatted());
            // COB_CODE: MOVE WK-DT-CALC
            //             TO S029-DT-CALC
            ws.getAreaIoLccs0029().setDtCalcFromBuffer(ws.getWkDtCalc().getWkDtCalcBytes());
            //
            // COB_CODE: PERFORM S10210-6101-VALID-FIN-MESE
            //              THRU S10210-6101-VALID-FIN-MESE-EX
            s102106101ValidFinMese();
            //
        }
    }

    /**Original name: S10210-6101-VALID-FIN-MESE<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALIDAZIONE FINE MESE
	 * ----------------------------------------------------------------*</pre>*/
    private void s102106101ValidFinMese() {
        Lccs0029 lccs0029 = null;
        // COB_CODE:      CALL LCCS0029         USING AREA-IDSV0001
        //                                            AREA-IO-LCCS0029
        //                ON EXCEPTION
        //           *
        //                         THRU EX-S0290
        //           *
        //                END-CALL.
        try {
            lccs0029 = Lccs0029.getInstance();
            lccs0029.run(areaIdsv0001, ws.getAreaIoLccs0029());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CHIAMATA LCCS0029'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE CHIAMATA LCCS0029");
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: IF WK-DT-CALC > S029-DT-CALC
            //                TO WK-DT-CALCOLATA
            //           ELSE
            //                TO WK-DT-CALCOLATA
            //           END-IF
            if (Conditions.gt(ws.getWkDtCalc().getWkDtCalcBytes(), MarshalByteExt.strToBuffer(ws.getAreaIoLccs0029().getDtCalcFormatted(), AreaIoLccs0029.Len.DT_CALC))) {
                // COB_CODE: MOVE S029-DT-CALC
                //             TO WK-DT-CALCOLATA
                ws.setWkDtCalcolataFormatted(ws.getAreaIoLccs0029().getDtCalcFormatted());
            }
            else {
                // COB_CODE: MOVE WK-DT-CALC
                //             TO WK-DT-CALCOLATA
                ws.setWkDtCalcolataFromBuffer(ws.getWkDtCalc().getWkDtCalcBytes());
            }
            //
        }
    }

    /**Original name: S10400-6101-VERIFICA-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *      VERIFICHE SULLA DATA CALCOLATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s104006101VerificaData() {
        // COB_CODE:      IF WGRZ-DT-SCAD-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
        //           *
        //           *       IF WK-DT-CALCOLATA > WGRZ-DT-SCAD(IX-TAB-GRZ)
        //           *       IF  (WK-DT-CALCOLATA >=  WGRZ-DT-SCAD(IX-TAB-GRZ)
        //           *         OR  ( WK-DT-CALCOLATA >= WK-DT-LIMITE
        //           *         AND WK-DT-LIMITE > 0))
        //                   END-IF
        //           *
        //                ELSE
        //                   END-IF
        //                END-IF.
        if (!Characters.EQ_HIGH.test(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtScad().getWgrzDtScadNullFormatted())) {
            //
            //       IF WK-DT-CALCOLATA > WGRZ-DT-SCAD(IX-TAB-GRZ)
            //       IF  (WK-DT-CALCOLATA >=  WGRZ-DT-SCAD(IX-TAB-GRZ)
            //         OR  ( WK-DT-CALCOLATA >= WK-DT-LIMITE
            //         AND WK-DT-LIMITE > 0))
            // COB_CODE:         IF  (WK-DT-CALCOLATA >  WGRZ-DT-SCAD(IX-TAB-GRZ)
            //                     OR  ( WK-DT-CALCOLATA > WK-DT-LIMITE
            //                     AND WK-DT-LIMITE > 0))
            //           *
            //           *          SET WPMO-ST-DEL(IX-TAB-PMO)
            //           *            TO TRUE
            //                        TO TRUE
            //           *
            //                   ELSE
            //           *
            //                         THRU S10500-6101-VERIF-VAR-FRAZ-EX
            //           *
            //                   END-IF
            if (ws.getWkDtCalcolata() > wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtScad().getWgrzDtScad() || ws.getWkDtCalcolata() > ws.getWkDtLimite() && ws.getWkDtLimite() > 0) {
                //
                //          SET WPMO-ST-DEL(IX-TAB-PMO)
                //            TO TRUE
                // COB_CODE: SET WPMO-ST-INV(IX-TAB-PMO)
                //             TO TRUE
                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setInv();
                //
            }
            else {
                //
                // COB_CODE: PERFORM S10500-6101-VERIF-VAR-FRAZ
                //              THRU S10500-6101-VERIF-VAR-FRAZ-EX
                s105006101VerifVarFraz();
                //
            }
            //
        }
        else if (ws.getWkDtCalcolata() > ws.getWkDtLimite() && ws.getWkDtLimite() > 0) {
            // COB_CODE:         IF   (WK-DT-CALCOLATA >  WK-DT-LIMITE
            //                     AND WK-DT-LIMITE > 0)
            //           *
            //           *          SET WPMO-ST-DEL(IX-TAB-PMO)
            //           *            TO TRUE
            //                        TO TRUE
            //                   ELSE
            //           *
            //                      THRU S10500-6101-VERIF-VAR-FRAZ-EX
            //           *
            //                   END-IF
            //
            //          SET WPMO-ST-DEL(IX-TAB-PMO)
            //            TO TRUE
            // COB_CODE: SET WPMO-ST-INV(IX-TAB-PMO)
            //             TO TRUE
            wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setInv();
        }
        else {
            //
            // COB_CODE: PERFORM S10500-6101-VERIF-VAR-FRAZ
            //              THRU S10500-6101-VERIF-VAR-FRAZ-EX
            s105006101VerifVarFraz();
            //
        }
    }

    /**Original name: S10500-6101-VERIF-VAR-FRAZ<br>
	 * <pre>----------------------------------------------------------------*
	 *      VERIFICA ESISTENZA PMO CON STESSA DATA CALCOLATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s105006101VerifVarFraz() {
        // COB_CODE: PERFORM S10600-6101-IMPOSTA-PMO
        //              THRU S10600-6101-IMPOSTA-PMO-EX.
        s106006101ImpostaPmo();
        //
        // COB_CODE: PERFORM S10700-6101-LEGGI-PMO
        //              THRU S10700-6101-LEGGI-PMO-EX.
        s107006101LeggiPmo();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF WK-PMO-TROV-SI
            //                    END-IF
            //                   ELSE
            //           *
            //                        TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
            //           *
            //                   END-IF
            if (ws.getWkPmoTrovato().isSi()) {
                // COB_CODE:          IF  WK-DT-CALCOLATA <= PMO-DT-RICOR-SUCC
                //           *
                //                      END-IF
                //           *
                //                    ELSE
                //           *
                //                        TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                //           *
                //                    END-IF
                if (ws.getWkDtCalcolata() <= ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc()) {
                    //
                    // COB_CODE:            IF WPMO-ID-PARAM-MOVI(IX-TAB-PMO) =
                    //                         PMO-ID-PARAM-MOVI
                    //           *
                    //                           TO TRUE
                    //           *
                    //                      ELSE
                    //           *
                    //           *                   SET WPMO-ST-DEL(IX-TAB-PMO)
                    //                           TO TRUE
                    //           *
                    //                      END-IF
                    if (wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoIdParamMovi() == ws.getParamMovi().getPmoIdParamMovi()) {
                        //
                        // COB_CODE: SET WPMO-ST-INV(IX-TAB-PMO)
                        //             TO TRUE
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setInv();
                        //
                    }
                    else {
                        //
                        //                   SET WPMO-ST-DEL(IX-TAB-PMO)
                        // COB_CODE:     SET WPMO-ST-INV(IX-TAB-PMO)
                        //           TO TRUE
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setInv();
                        //
                    }
                    //
                }
                else {
                    //
                    // COB_CODE: IF NOT WPMO-ST-DEL(IX-TAB-PMO)
                    //              TO TRUE
                    //           END-IF
                    if (!wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().isDel()) {
                        // COB_CODE: SET WPMO-ST-MOD(IX-TAB-PMO)
                        //             TO TRUE
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setMod();
                    }
                    //
                    // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                    //             TO WPMO-DT-RICOR-PREC(IX-TAB-PMO)
                    wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
                    //
                    // COB_CODE: MOVE WK-DT-CALCOLATA
                    //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                    wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getWkDtCalcolata());
                    //
                }
            }
            else {
                //
                // COB_CODE: SET WPMO-ST-ADD(IX-TAB-PMO)
                //             TO TRUE
                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setWcomStAdd();
                //
                // COB_CODE: MOVE HIGH-VALUES
                //             TO WPMO-DT-RICOR-PREC-NULL(IX-TAB-PMO)
                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpmoDtRicorPrec.Len.WPMO_DT_RICOR_PREC_NULL));
                //
                // COB_CODE: MOVE WK-DT-CALCOLATA
                //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getWkDtCalcolata());
                //
            }
            //
        }
    }

    /**Original name: S10600-6101-IMPOSTA-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *   VALORIZZAZIONI PER LETTURA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s106006101ImpostaPmo() {
        // COB_CODE: SET WK-PMO-TROV-NO
        //             TO TRUE.
        ws.getWkPmoTrovato().setNo();
        //
        // COB_CODE: INITIALIZE PARAM-MOVI.
        initParamMovi();
        //
        //      TO LDBV2681-ID-OGG.
        // COB_CODE:      MOVE WPMO-ID-OGG(IX-TAB-PMO)
        //           *      TO LDBV2681-ID-OGG.
        //                  TO LDBVG351-ID-OGG.
        ws.getLdbvg351().setIdOgg(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoIdOgg());
        //
        // COB_CODE: SET GARANZIA
        //             TO TRUE
        ws.getWsTpOgg().setGaranzia();
        //      TO LDBV2681-TP-OGG.
        // COB_CODE:      MOVE WS-TP-OGG
        //           *      TO LDBV2681-TP-OGG.
        //                  TO LDBVG351-TP-OGG
        ws.getLdbvg351().setTpOgg(ws.getWsTpOgg().getWsTpOgg());
        //
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //             TO LDBVG351-TP-MOVI.
        ws.getLdbvg351().setTpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        //      TO LDBV2681-TP-MOVI-01
        //         LDBV2681-TP-MOVI-02
        //         LDBV2681-TP-MOVI-03
        //         LDBV2681-TP-MOVI-04
        //         LDBV2681-TP-MOVI-05
        //         LDBV2681-TP-MOVI-06.
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE WK-DT-CALCOLATA
        //             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getWkDtCalcolata());
        //
        // COB_CODE: MOVE 999912314023595959
        //             TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912314023595959L);
        //
        // COB_CODE: MOVE ZEROES
        //             TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        //
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //
        //  --> Nome tabella fisica db
        //
        //    MOVE LDBS2680
        // COB_CODE: MOVE LDBSG350
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbsg350());
        //
        //  --> Dclgen tabella
        //
        //    MOVE LDBV2681
        // COB_CODE: MOVE LDBVG351
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbvg351().getLdbvg351Formatted());
        // COB_CODE: MOVE SPACES
        //              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-TRATT-X-EFFETTO
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattXEffetto();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10700-6101-LEGGI-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *   LETTURA DELLA TABELLA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s107006101LeggiPmo() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'S10700-6101-LEGGI-PMO'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("S10700-6101-LEGGI-PMO");
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore Dispatcher
        //           *
        //                      THRU EX-S0300
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                            CONTINUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore Lettura
            //           *
            //                               THRU EX-S0300
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                //  --> Chiave non trovata
                //
                // COB_CODE: CONTINUE
                //continue
                //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO PARAM-MOVI
                    ws.getParamMovi().setParamMoviFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //
                    // COB_CODE: SET WK-PMO-TROV-SI
                    //             TO TRUE
                    ws.getWkPmoTrovato().setSi();
                    //
                    break;

                default://
                    //  --> Errore Lettura
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
                    // COB_CODE: MOVE '005015'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING 'ERRORE LETTURA PARAM MOVI ' ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IEAI9901-PARAMETRI-ERR
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE LETTURA PARAM MOVI ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore Dispatcher
            //
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE WK-LABEL-ERR
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA PARAM MOVI'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE DISPATCHER LETTURA PARAM MOVI");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
            //
        }
    }

    /**Original name: GESTIONE-RIVAL<br>
	 * <pre>----------------------------------------------------------------*
	 *   LETTURA DELLA TABELLA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneRival() {
        // COB_CODE: MOVE 'GESTIONE-RIVAL'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("GESTIONE-RIVAL");
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
        //           *
        //                    END-PERFORM
        //           *
        //                END-PERFORM.
        ws.getWkIndici().setPmo(((short)1));
        while (!(ws.getWkIndici().getPmo() > wpmoAreaPmo.getEleParamMovMax())) {
            //
            // COB_CODE:          PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
            //                       UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
            //           *
            //                         END-IF
            //           *
            //                    END-PERFORM
            ws.getWkIndici().setGrz(((short)1));
            while (!(ws.getWkIndici().getGrz() > wgrzAreaGaranzia.getEleGarMax())) {
                //
                // COB_CODE:               IF WPMO-ID-OGG(IX-TAB-PMO) =
                //                            WGRZ-ID-GAR(IX-TAB-GRZ)
                //           *
                //                               THRU S6006-CALC-PROX-RIC-EX
                //           *
                //                         END-IF
                if (wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoIdOgg() == wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    //
                    // COB_CODE: PERFORM S6006-CALC-PROX-RIC
                    //              THRU S6006-CALC-PROX-RIC-EX
                    s6006CalcProxRic();
                    //
                }
                //
                ws.getWkIndici().setGrz(Trunc.toShort(ws.getWkIndici().getGrz() + 1, 4));
            }
            //
            ws.getWkIndici().setPmo(Trunc.toShort(ws.getWkIndici().getPmo() + 1, 4));
        }
    }

    /**Original name: S6006-CALC-PROX-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *             CALCOLA LA DATA DI PROSSIMA RICORRENZA              *
	 * ----------------------------------------------------------------*</pre>*/
    private void s6006CalcProxRic() {
        // COB_CODE: MOVE 'S6006-CALC-PROX-RIC'
        //             TO WK-LABEL-ERR.
        ws.setWkLabelErr("S6006-CALC-PROX-RIC");
        //
        // COB_CODE: SET WPMO-ST-MOD(IX-TAB-PMO)
        //             TO TRUE.
        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setMod();
        //
        // COB_CODE: MOVE WGRZ-ID-GAR(IX-TAB-GRZ)  TO   LDBV1131-ID-OGG
        ws.getAreaLdbv1131().setIdOgg(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzIdGar());
        // COB_CODE: MOVE 'GA'                      TO   LDBV1131-TP-OGG
        ws.getAreaLdbv1131().setTpOgg("GA");
        // COB_CODE: MOVE 'PERIODADEG'              TO   LDBV1131-COD-PARAM
        ws.getAreaLdbv1131().setCodParam("PERIODADEG");
        // COB_CODE: PERFORM S1820-LETTURA-POG      THRU EX-S1820
        s1820LetturaPog();
        //
        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
        //             TO WPMO-DT-RICOR-PREC(IX-TAB-PMO).
        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
        //
        // A SECONDA DELLA PRESENZA DEL PARAMETRO PERIODADEG BISOGNERu
        // AGGIUNGERE UN ANNO OPPURE UN PERIODO IN MESI ALLA DATA
        // DECORRENZA SUCCESSIVA
        //
        // COB_CODE: PERFORM S6006-AGGIUNGI-ANNO
        //              THRU S6006-AGGIUNGI-ANNO-EX.
        s6006AggiungiAnno();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: MOVE A2K-OUAMG
            //             TO WK-APPO-DT-RICOR-SUCC
            ws.setWkAppoDtRicorSuccFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
            //
            // --> Se la data di scadenza h minore della prossima ricorrenza
            // --> calcolata, allora la data di prossima ricorrenza h proprio
            // --> quella di scadenza
            //
            // COB_CODE:         IF WGRZ-DT-SCAD-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
            //           *
            //                      END-IF
            //           *
            //                   ELSE
            //           *
            //                        TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
            //           *
            //                   END-IF
            if (!Characters.EQ_HIGH.test(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtScad().getWgrzDtScadNullFormatted())) {
                //
                // COB_CODE:            IF WGRZ-DT-SCAD(IX-TAB-GRZ) <
                //                         WK-APPO-DT-RICOR-SUCC
                //           *
                //                         END-IF
                //           *
                //                      ELSE
                //           *
                //                           TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                //           *
                //                      END-IF
                if (wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtScad().getWgrzDtScad() < ws.getWkAppoDtRicorSucc()) {
                    //
                    // COB_CODE: PERFORM VERIFICA-STATO-POL
                    //              THRU VERIFICA-STATO-POL-EX
                    verificaStatoPol();
                    //
                    //                DIFFERITA
                    //             OR PROROGATA
                    // COB_CODE:               IF IDSV0001-ESITO-OK AND (
                    //           *                DIFFERITA
                    //           *             OR PROROGATA
                    //                            PROROGATA
                    //                         OR ATTESA-COMUNIC-PAG-REN
                    //                         OR ATTESA-LIQ-RENDITA
                    //                         OR SINISTRO-RENDITA-DA-EROGARE
                    //                         OR ATTESA-LIQ-REN-PRIMO-ASSIC
                    //                         OR ATTESA-COM-PAG-REN-PRIMO-ASS
                    //                         OR RENDITA-EROG-REVISIONARIO
                    //                         OR RENDITA-EROGAZIONE )
                    //                         AND STB-TP-STAT-BUS = 'VI'
                    //                              TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                    //           *
                    //                         ELSE
                    //                              TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                    //           *
                    //           *
                    //                         END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && (ws.getWsTpCaus().isProrogata() || ws.getWsTpCaus().isAttesaComunicPagRen() || ws.getWsTpCaus().isAttesaLiqRendita() || ws.getWsTpCaus().isSinistroRenditaDaErogare() || ws.getWsTpCaus().isAttesaLiqRenPrimoAssic() || ws.getWsTpCaus().isAttesaComPagRenPrimoAss() || ws.getWsTpCaus().isRenditaErogRevisionario() || ws.getWsTpCaus().isRenditaErogazione()) && Conditions.eq(ws.getStatOggBus().getStbTpStatBus(), "VI")) {
                        // COB_CODE: MOVE WK-APPO-DT-RICOR-SUCC
                        //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getWkAppoDtRicorSucc());
                        //
                    }
                    else {
                        // COB_CODE: MOVE WGRZ-DT-SCAD(IX-TAB-GRZ)
                        //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzDtScad().getWgrzDtScad());
                        //
                        //
                    }
                    //
                }
                else {
                    //
                    // COB_CODE: MOVE WK-APPO-DT-RICOR-SUCC
                    //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                    wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getWkAppoDtRicorSucc());
                    //
                }
                //
                // COB_CODE: IF WPMO-DT-RICOR-SUCC(IX-TAB-PMO) =
                //              WPMO-DT-RICOR-PREC(IX-TAB-PMO)
                //              SET WPMO-ST-DEL(IX-TAB-PMO) TO TRUE
                //           END-IF
                if (wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc() == wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec()) {
                    // COB_CODE: SET WPMO-ST-DEL(IX-TAB-PMO) TO TRUE
                    wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setWpmoStDel();
                }
                //
            }
            else {
                //
                // COB_CODE: MOVE WK-APPO-DT-RICOR-SUCC
                //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(ws.getWkAppoDtRicorSucc());
                //
            }
            //
        }
    }

    /**Original name: S6006-AGGIUNGI-ANNO<br>
	 * <pre>----------------------------------------------------------------*
	 *   RICORRENZA SUCCESSIVA LETTA IN INPUT INCREMENTATA DI UN ANNO  *
	 * ----------------------------------------------------------------*</pre>*/
    private void s6006AggiungiAnno() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE ZEROES
        //             TO WK-APPO-DT-RICOR-SUCC.
        ws.setWkAppoDtRicorSucc(0);
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamg(TruncAbs.toInt(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc(), 8));
        //
        // --> A2K-TDELTA -> Tipo Delta: Anni(A)
        //
        // COB_CODE: IF WK-NON-TROVATO-PERIODADEG
        //                TO A2K-TDELTA
        //           ELSE
        //                TO A2K-TDELTA
        //           END-IF
        if (!ws.isWkRicercaPeriodadeg()) {
            // COB_CODE: MOVE 'A'
            //             TO A2K-TDELTA
            ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        }
        else {
            // COB_CODE: MOVE 'M'
            //             TO A2K-TDELTA
            ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        }
        //
        // --> A2K-DELTA  -> Delta Da Sommare
        //
        // COB_CODE: IF WK-NON-TROVATO-PERIODADEG
        //                TO A2K-DELTA
        //           ELSE
        //              END-EVALUATE
        //           END-IF
        if (!ws.isWkRicercaPeriodadeg()) {
            // COB_CODE: MOVE 1
            //             TO A2K-DELTA
            ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        }
        else {
            // COB_CODE: IF WGRZ-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ) = HIGH-VALUE
            //              MOVE 1 TO WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
            //           END-IF
            if (Characters.EQ_HIGH.test(wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().getWgrzFrazDecrCptNullFormatted())) {
                // COB_CODE: MOVE 1 TO WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
                wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(1);
            }
            // COB_CODE: EVALUATE WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
            //           WHEN 1
            //                MOVE 12 TO A2K-TDELTA
            //           WHEN 2
            //                MOVE 6  TO A2K-TDELTA
            //           WHEN 3
            //                MOVE 4  TO A2K-TDELTA
            //           WHEN 4
            //                MOVE 3  TO A2K-TDELTA
            //           WHEN 6
            //                MOVE 2  TO A2K-TDELTA
            //           WHEN OTHER
            //               PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
            //           END-EVALUATE
            switch (wgrzAreaGaranzia.getTabGar(ws.getWkIndici().getGrz()).getLccvgrz1().getDati().getWgrzFrazDecrCpt().getWgrzFrazDecrCpt()) {

                case 1:// COB_CODE: MOVE 12 TO A2K-TDELTA
                    ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("12");
                    break;

                case 2:// COB_CODE: MOVE 6  TO A2K-TDELTA
                    ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("6");
                    break;

                case 3:// COB_CODE: MOVE 4  TO A2K-TDELTA
                    ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("4");
                    break;

                case 4:// COB_CODE: MOVE 3  TO A2K-TDELTA
                    ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("3");
                    break;

                case 6:// COB_CODE: MOVE 2  TO A2K-TDELTA
                    ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("2");
                    break;

                default:// COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S6006-AGGIUNGI-ANNO' TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S6006-AGGIUNGI-ANNO");
                    // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'FRAZIONAMENTO DECRESCENZA CAPITALE ERRATO' ';'
                    //                IDSO0011-RETURN-CODE ';'
                    //                IDSO0011-SQLCODE
                    //                DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "FRAZIONAMENTO DECRESCENZA CAPITALE ERRATO", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX.
        callLccs0003();
    }

    /**Original name: VERIFICA-STATO-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *    VERIFICA STATO POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void verificaStatoPol() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-AREA.
        initIdsi0011Area();
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-ID-OGGETTO       TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: MOVE 'PO'                     TO STB-TP-OGG
        ws.getStatOggBus().setStbTpOgg("PO");
        // COB_CODE: MOVE WPOL-ID-POLI             TO STB-ID-OGG
        ws.getStatOggBus().setStbIdOgg(wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE STAT-OGG-BUS             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatOggBus().getStatOggBusFormatted());
        // COB_CODE: MOVE 'STAT-OGG-BUS'           TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("STAT-OGG-BUS");
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                          MOVE STB-TP-CAUS TO WS-TP-CAUS
            //                       WHEN OTHER
            //           *
            //           *  --> Errore Lettura
            //           *
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-BUS
                    ws.getStatOggBus().setStatOggBusFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE STB-TP-CAUS TO WS-TP-CAUS
                    ws.getWsTpCaus().setWsTpCaus(ws.getStatOggBus().getStbTpCaus());
                    break;

                default://
                    //  --> Errore Lettura
                    //
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE WK-LABEL-ERR
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr(ws.getWkLabelErr());
                    // COB_CODE: MOVE '005015'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING 'ERRORE LETTURA STAT-OGG-BUS'';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE
                    //           INTO IEAI9901-PARAMETRI-ERR
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ERRORE LETTURA STAT-OGG-BUS';", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'VERIFICA-STATO-POL'    TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("VERIFICA-STATO-POL");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'STAT-OGG-BUS'        ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "STAT-OGG-BUS", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1820-LETTURA-POG<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA PARAMETRO OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1820LetturaPog() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IDSI0011-AREA.
        initIdsi0011Area();
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE PARAM-OGG                TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamOgg().getParamOggFormatted());
        // COB_CODE: MOVE 'LDBS1130'               TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS1130");
        // COB_CODE: MOVE  AREA-LDBV1131           TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAreaLdbv1131().getAreaLdbv1131Formatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-NOT-FOUND
            //                  SET WK-NON-TROVATO-PERIODADEG TO TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  MOVE IDSO0011-BUFFER-DATI TO PARAM-OGG
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: SET WK-NON-TROVATO-PERIODADEG TO TRUE
                    ws.setWkRicercaPeriodadeg(false);
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: SET WK-TROVATO-PERIODADEG  TO TRUE
                    ws.setWkRicercaPeriodadeg(true);
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO PARAM-OGG
                    ws.getParamOgg().setParamOggFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1820-LETTURA-POG'     TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1820-LETTURA-POG");
            // COB_CODE: MOVE '005016'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'PARAM-OGG  '        ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "PARAM-OGG  ", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: GESTIONE-SCADE<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE SPOSTAMENTO DATA PER OPZIONI A SCADENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneScade() {
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
        //           *
        //                  END-IF
        //                END-PERFORM.
        ws.getWkIndici().setPmo(((short)1));
        while (!(ws.getWkIndici().getPmo() > wpmoAreaPmo.getEleParamMovMax())) {
            //
            // COB_CODE: IF WPMO-TP-OGG(IX-TAB-PMO) = 'AD'
            //               END-IF
            //           END-IF
            if (Conditions.eq(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpOgg(), "AD")) {
                // COB_CODE: IF WPMO-TP-OPZ-NULL(IX-TAB-PMO) = HIGH-VALUES
                //              CONTINUE
                //           ELSE
                //                TO WK-TP-OPZIONI
                //           END-IF
                if (Characters.EQ_HIGH.test(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpOpzFormatted())) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WPMO-TP-OPZ(IX-TAB-PMO)
                    //             TO WK-STR-OPZIONE
                    ws.setWkStrOpzioneFormatted(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTpOpzFormatted());
                    // COB_CODE: MOVE WK-COD-OPZIONE
                    //             TO WK-TP-OPZIONI
                    ws.getWkTpOpzioni().setWkTpOpzioni(ws.getWkCodOpzione());
                }
                //
                // COB_CODE: IF WK-DIFFERIMENTO
                //           OR WK-PROROGA
                //               END-IF
                //            END-IF
                if (ws.getWkTpOpzioni().isDifferimento() || ws.getWkTpOpzioni().isProroga()) {
                    // COB_CODE: IF WADE-DT-SCAD-NULL(1) = HIGH-VALUES
                    //              CONTINUE
                    //           ELSE
                    //                         WPMO-DUR-MM-NULL(IX-TAB-PMO)
                    //            END-IF
                    if (Characters.EQ_HIGH.test(wadeAreaAdesione.getTabAde(1).getLccvade1().getDati().getWadeDtScad().getWadeDtScadNullFormatted())) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET WPMO-ST-MOD(IX-TAB-PMO)
                        //            TO TRUE
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setMod();
                        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        //             TO WPMO-DT-RICOR-PREC(IX-TAB-PMO)
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
                        // COB_CODE: MOVE WADE-DT-SCAD(1)
                        //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(wadeAreaAdesione.getTabAde(1).getLccvade1().getDati().getWadeDtScad().getWadeDtScad());
                        // COB_CODE: IF WK-PROROGA
                        //            END-IF
                        //           END-IF
                        if (ws.getWkTpOpzioni().isProroga()) {
                            // COB_CODE: IF WPMO-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO) =
                            //                                           HIGH-VALUES
                            //                TO WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO)
                            //           ELSE
                            //                    WPMO-DUR-AA(IX-TAB-PMO)
                            //           END-IF
                            if (Characters.EQ_HIGH.test(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().getWpmoTotAaGiaProrNullFormatted())) {
                                // COB_CODE: MOVE WPMO-DUR-AA(IX-TAB-PMO)
                                //             TO WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO)
                                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDurAa().getWpmoDurAa());
                            }
                            else {
                                // COB_CODE: COMPUTE WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO) =
                                //                   WPMO-TOT-AA-GIA-PROR(IX-TAB-PMO) +
                                //                   WPMO-DUR-AA(IX-TAB-PMO)
                                wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(Trunc.toInt(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoTotAaGiaPror().getWpmoTotAaGiaPror() + wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDurAa().getWpmoDurAa(), 5));
                            }
                        }
                        // COB_CODE: MOVE HIGH-VALUES TO
                        //                      WPMO-DUR-AA-NULL(IX-TAB-PMO)
                        //                      WPMO-TP-OPZ-NULL(IX-TAB-PMO)
                        //                      WPMO-DUR-MM-NULL(IX-TAB-PMO)
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpmoDurAa.Len.WPMO_DUR_AA_NULL));
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().setWpmoTpOpz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpmoDati.Len.WPMO_TP_OPZ));
                        wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WpmoDurMm.Len.WPMO_DUR_MM_NULL));
                    }
                }
            }
            ws.getWkIndici().setPmo(Trunc.toShort(ws.getWkIndici().getPmo() + 1, 4));
        }
    }

    /**Original name: GESTIONE-BONUS<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE SPOSTAMENTO DATA PER BONUS RICORRENTE E FEDELTA'
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneBonus() {
        // COB_CODE:      PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //                  UNTIL IX-TAB-PMO > WPMO-ELE-PMO-MAX
        //           *
        //                    TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
        //           *
        //                END-PERFORM.
        ws.getWkIndici().setPmo(((short)1));
        while (!(ws.getWkIndici().getPmo() > wpmoAreaPmo.getEleParamMovMax())) {
            //
            // COB_CODE: SET WPMO-ST-MOD(IX-TAB-PMO) TO TRUE
            wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getStatus().setMod();
            // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
            //             TO WPMO-DT-RICOR-PREC(IX-TAB-PMO)
            wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc());
            // COB_CODE: MOVE WCOM-DATA-RICOR-SUCC
            //             TO WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
            wpmoAreaPmo.getTabParamMov(ws.getWkIndici().getPmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(wcomAreaLccc0320.getDataRicorSucc());
            //
            ws.getWkIndici().setPmo(Trunc.toShort(ws.getWkIndici().getPmo() + 1, 4));
        }
    }

    /**Original name: S9000-OPERAZ-FINALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   F I N A L I              <-- *
	 *  ============================================================== *</pre>*/
    private void s9000OperazFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORI
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initIdsi0011Area() {
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted("00000");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(0);
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullatoFormatted("000000000");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setIdsi0011IdentitaChiamante("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setLivelloOperazione("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setOperazione("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FlagCodaTs().setIdsi0011FlagCodaTs(Types.SPACE_CHAR);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted("0");
    }
}
