package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Wb03Dati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaOut;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Llbs0240Data;
import it.accenture.jnais.ws.redefines.B03DtDecorAdes;
import it.accenture.jnais.ws.redefines.B03DtEffCambStat;
import it.accenture.jnais.ws.redefines.B03DtEffRidz;
import it.accenture.jnais.ws.redefines.B03DtEffStab;
import it.accenture.jnais.ws.redefines.B03DtEmisCambStat;
import it.accenture.jnais.ws.redefines.B03DtEmisRidz;
import it.accenture.jnais.ws.redefines.B03DtEmisTrch;
import it.accenture.jnais.ws.redefines.B03DtIncUltPre;
import it.accenture.jnais.ws.redefines.B03DtIniValTar;
import it.accenture.jnais.ws.redefines.B03DtNasc1oAssto;
import it.accenture.jnais.ws.redefines.B03DtQtzEmis;
import it.accenture.jnais.ws.redefines.B03DtScadIntmd;
import it.accenture.jnais.ws.redefines.B03DtScadPagPre;
import it.accenture.jnais.ws.redefines.B03DtScadTrch;
import it.accenture.jnais.ws.redefines.B03DtUltPrePag;
import it.accenture.jnais.ws.redefines.B03DtUltRival;

/**Original name: LLBS0240<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             IASS.
 * DATE-WRITTEN.       GIUGNO 2008.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LLBS0240
 *     TIPOLOGIA...... DRIVER EOC
 *     PROCESSO....... LEGGE E BILANCIO
 *     FUNZIONE....... ESTRAZIONE RISERVA MATEMATICA
 *     DESCRIZIONE....
 *     PAGINA WEB.....
 * **------------------------------------------------------------***</pre>*/
public class Llbs0240 extends BatchProgram {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Llbs0240Data ws = new Llbs0240Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: AREA-OUT
    private AreaOut areaOut;

    //==== METHODS ====
    /**Original name: PROGRAM_LLBS0240_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaOut areaOut) {
        this.areaIdsv0001 = areaIdsv0001;
        this.areaOut = areaOut;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI        THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM S1000-ELABORAZIONE            THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE            THRU EX-S1000
            s1000Elaborazione();
        }
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI          THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Llbs0240 getInstance() {
        return ((Llbs0240)Programs.getInstance(Llbs0240.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE IX-INDICI
        //                      BILA-TRCH-ESTR.
        initIxIndici();
        initBilaTrchEstr();
        // COB_CODE: MOVE 'BILA-TRCH-ESTR'             TO WK-TABELLA.
        ws.setWkTabella("BILA-TRCH-ESTR");
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU SCRIVI-BIL-ESTRATTI-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM SCRIVI-BIL-ESTRATTI
            //              THRU SCRIVI-BIL-ESTRATTI-EX
            scriviBilEstratti();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VAL-DCLGEN-B03<br>
	 * <pre>  ---------------------------------------------------------------
	 *      CONTIENE STATEMENTS PER LA FASE DI EOC
	 *   ---------------------------------------------------------------
	 * --> ESTRATTI RISERVA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVB035
	 *    ULTIMO AGG. 12 SET 2014
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenB03() {
        // COB_CODE: MOVE (SF)-ID-BILA-TRCH-ESTR
        //              TO B03-ID-BILA-TRCH-ESTR
        ws.getBilaTrchEstr().setB03IdBilaTrchEstr(areaOut.getLccvb031().getDati().getWb03IdBilaTrchEstr());
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO B03-COD-COMP-ANIA
        ws.getBilaTrchEstr().setB03CodCompAnia(areaOut.getLccvb031().getDati().getWb03CodCompAnia());
        // COB_CODE: MOVE (SF)-ID-RICH-ESTRAZ-MAS
        //              TO B03-ID-RICH-ESTRAZ-MAS
        ws.getBilaTrchEstr().setB03IdRichEstrazMas(areaOut.getLccvb031().getDati().getWb03IdRichEstrazMas());
        // COB_CODE: IF (SF)-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
        //              TO B03-ID-RICH-ESTRAZ-AGG-NULL
        //           ELSE
        //              TO B03-ID-RICH-ESTRAZ-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03IdRichEstrazAgg().getWb03IdRichEstrazAggNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-RICH-ESTRAZ-AGG-NULL
            //           TO B03-ID-RICH-ESTRAZ-AGG-NULL
            ws.getBilaTrchEstr().getB03IdRichEstrazAgg().setB03IdRichEstrazAggNull(areaOut.getLccvb031().getDati().getWb03IdRichEstrazAgg().getWb03IdRichEstrazAggNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-RICH-ESTRAZ-AGG
            //           TO B03-ID-RICH-ESTRAZ-AGG
            ws.getBilaTrchEstr().getB03IdRichEstrazAgg().setB03IdRichEstrazAgg(areaOut.getLccvb031().getDati().getWb03IdRichEstrazAgg().getWb03IdRichEstrazAgg());
        }
        // COB_CODE: IF (SF)-FL-SIMULAZIONE-NULL = HIGH-VALUES
        //              TO B03-FL-SIMULAZIONE-NULL
        //           ELSE
        //              TO B03-FL-SIMULAZIONE
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlSimulazione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-SIMULAZIONE-NULL
            //           TO B03-FL-SIMULAZIONE-NULL
            ws.getBilaTrchEstr().setB03FlSimulazione(areaOut.getLccvb031().getDati().getWb03FlSimulazione());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-SIMULAZIONE
            //           TO B03-FL-SIMULAZIONE
            ws.getBilaTrchEstr().setB03FlSimulazione(areaOut.getLccvb031().getDati().getWb03FlSimulazione());
        }
        // COB_CODE: MOVE (SF)-DT-RIS
        //              TO B03-DT-RIS
        ws.getBilaTrchEstr().setB03DtRis(areaOut.getLccvb031().getDati().getWb03DtRis());
        // COB_CODE: MOVE (SF)-DT-PRODUZIONE
        //              TO B03-DT-PRODUZIONE
        ws.getBilaTrchEstr().setB03DtProduzione(areaOut.getLccvb031().getDati().getWb03DtProduzione());
        // COB_CODE: MOVE (SF)-ID-POLI
        //              TO B03-ID-POLI
        ws.getBilaTrchEstr().setB03IdPoli(areaOut.getLccvb031().getDati().getWb03IdPoli());
        // COB_CODE: MOVE (SF)-ID-ADES
        //              TO B03-ID-ADES
        ws.getBilaTrchEstr().setB03IdAdes(areaOut.getLccvb031().getDati().getWb03IdAdes());
        // COB_CODE: MOVE (SF)-ID-GAR
        //              TO B03-ID-GAR
        ws.getBilaTrchEstr().setB03IdGar(areaOut.getLccvb031().getDati().getWb03IdGar());
        // COB_CODE: MOVE (SF)-ID-TRCH-DI-GAR
        //              TO B03-ID-TRCH-DI-GAR
        ws.getBilaTrchEstr().setB03IdTrchDiGar(areaOut.getLccvb031().getDati().getWb03IdTrchDiGar());
        // COB_CODE: MOVE (SF)-TP-FRM-ASSVA
        //              TO B03-TP-FRM-ASSVA
        ws.getBilaTrchEstr().setB03TpFrmAssva(areaOut.getLccvb031().getDati().getWb03TpFrmAssva());
        // COB_CODE: MOVE (SF)-TP-RAMO-BILA
        //              TO B03-TP-RAMO-BILA
        ws.getBilaTrchEstr().setB03TpRamoBila(areaOut.getLccvb031().getDati().getWb03TpRamoBila());
        // COB_CODE: MOVE (SF)-TP-CALC-RIS
        //              TO B03-TP-CALC-RIS
        ws.getBilaTrchEstr().setB03TpCalcRis(areaOut.getLccvb031().getDati().getWb03TpCalcRis());
        // COB_CODE: MOVE (SF)-COD-RAMO
        //              TO B03-COD-RAMO
        ws.getBilaTrchEstr().setB03CodRamo(areaOut.getLccvb031().getDati().getWb03CodRamo());
        // COB_CODE: MOVE (SF)-COD-TARI
        //              TO B03-COD-TARI
        ws.getBilaTrchEstr().setB03CodTari(areaOut.getLccvb031().getDati().getWb03CodTari());
        // COB_CODE: IF (SF)-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //              TO B03-DT-INI-VAL-TAR-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtIniValTar().getWb03DtIniValTarNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-INI-VAL-TAR-NULL
            //           TO B03-DT-INI-VAL-TAR-NULL
            ws.getBilaTrchEstr().getB03DtIniValTar().setB03DtIniValTarNull(areaOut.getLccvb031().getDati().getWb03DtIniValTar().getWb03DtIniValTarNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtIniValTar().getWb03DtIniValTar() == 0) {
            // COB_CODE: IF (SF)-DT-INI-VAL-TAR = ZERO
            //              TO B03-DT-INI-VAL-TAR-NULL
            //           ELSE
            //            TO B03-DT-INI-VAL-TAR
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-INI-VAL-TAR-NULL
            ws.getBilaTrchEstr().getB03DtIniValTar().setB03DtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtIniValTar.Len.B03_DT_INI_VAL_TAR_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-VAL-TAR
            //           TO B03-DT-INI-VAL-TAR
            ws.getBilaTrchEstr().getB03DtIniValTar().setB03DtIniValTar(areaOut.getLccvb031().getDati().getWb03DtIniValTar().getWb03DtIniValTar());
        }
        // COB_CODE: IF (SF)-COD-PROD-NULL = HIGH-VALUES
        //              TO B03-COD-PROD-NULL
        //           ELSE
        //              TO B03-COD-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodProdFormatted())) {
            // COB_CODE: MOVE (SF)-COD-PROD-NULL
            //           TO B03-COD-PROD-NULL
            ws.getBilaTrchEstr().setB03CodProd(areaOut.getLccvb031().getDati().getWb03CodProd());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-PROD
            //           TO B03-COD-PROD
            ws.getBilaTrchEstr().setB03CodProd(areaOut.getLccvb031().getDati().getWb03CodProd());
        }
        // COB_CODE: MOVE (SF)-DT-INI-VLDT-PROD
        //              TO B03-DT-INI-VLDT-PROD
        ws.getBilaTrchEstr().setB03DtIniVldtProd(areaOut.getLccvb031().getDati().getWb03DtIniVldtProd());
        // COB_CODE: IF (SF)-COD-TARI-ORGN-NULL = HIGH-VALUES
        //              TO B03-COD-TARI-ORGN-NULL
        //           ELSE
        //              TO B03-COD-TARI-ORGN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodTariOrgnFormatted())) {
            // COB_CODE: MOVE (SF)-COD-TARI-ORGN-NULL
            //           TO B03-COD-TARI-ORGN-NULL
            ws.getBilaTrchEstr().setB03CodTariOrgn(areaOut.getLccvb031().getDati().getWb03CodTariOrgn());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-TARI-ORGN
            //           TO B03-COD-TARI-ORGN
            ws.getBilaTrchEstr().setB03CodTariOrgn(areaOut.getLccvb031().getDati().getWb03CodTariOrgn());
        }
        // COB_CODE: IF (SF)-MIN-GARTO-T-NULL = HIGH-VALUES
        //              TO B03-MIN-GARTO-T-NULL
        //           ELSE
        //              TO B03-MIN-GARTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03MinGartoT().getWb03MinGartoTNullFormatted())) {
            // COB_CODE: MOVE (SF)-MIN-GARTO-T-NULL
            //           TO B03-MIN-GARTO-T-NULL
            ws.getBilaTrchEstr().getB03MinGartoT().setB03MinGartoTNull(areaOut.getLccvb031().getDati().getWb03MinGartoT().getWb03MinGartoTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MIN-GARTO-T
            //           TO B03-MIN-GARTO-T
            ws.getBilaTrchEstr().getB03MinGartoT().setB03MinGartoT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03MinGartoT().getWb03MinGartoT(), 15, 3));
        }
        // COB_CODE: IF (SF)-TP-TARI-NULL = HIGH-VALUES
        //              TO B03-TP-TARI-NULL
        //           ELSE
        //              TO B03-TP-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpTariFormatted())) {
            // COB_CODE: MOVE (SF)-TP-TARI-NULL
            //           TO B03-TP-TARI-NULL
            ws.getBilaTrchEstr().setB03TpTari(areaOut.getLccvb031().getDati().getWb03TpTari());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-TARI
            //           TO B03-TP-TARI
            ws.getBilaTrchEstr().setB03TpTari(areaOut.getLccvb031().getDati().getWb03TpTari());
        }
        // COB_CODE: IF (SF)-TP-PRE-NULL = HIGH-VALUES
        //              TO B03-TP-PRE-NULL
        //           ELSE
        //              TO B03-TP-PRE
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03TpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-TP-PRE-NULL
            //           TO B03-TP-PRE-NULL
            ws.getBilaTrchEstr().setB03TpPre(areaOut.getLccvb031().getDati().getWb03TpPre());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-PRE
            //           TO B03-TP-PRE
            ws.getBilaTrchEstr().setB03TpPre(areaOut.getLccvb031().getDati().getWb03TpPre());
        }
        // COB_CODE: IF (SF)-TP-ADEG-PRE-NULL = HIGH-VALUES
        //              TO B03-TP-ADEG-PRE-NULL
        //           ELSE
        //              TO B03-TP-ADEG-PRE
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03TpAdegPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-TP-ADEG-PRE-NULL
            //           TO B03-TP-ADEG-PRE-NULL
            ws.getBilaTrchEstr().setB03TpAdegPre(areaOut.getLccvb031().getDati().getWb03TpAdegPre());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-ADEG-PRE
            //           TO B03-TP-ADEG-PRE
            ws.getBilaTrchEstr().setB03TpAdegPre(areaOut.getLccvb031().getDati().getWb03TpAdegPre());
        }
        // COB_CODE: IF (SF)-TP-RIVAL-NULL = HIGH-VALUES
        //              TO B03-TP-RIVAL-NULL
        //           ELSE
        //              TO B03-TP-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpRivalFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RIVAL-NULL
            //           TO B03-TP-RIVAL-NULL
            ws.getBilaTrchEstr().setB03TpRival(areaOut.getLccvb031().getDati().getWb03TpRival());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RIVAL
            //           TO B03-TP-RIVAL
            ws.getBilaTrchEstr().setB03TpRival(areaOut.getLccvb031().getDati().getWb03TpRival());
        }
        // COB_CODE: IF (SF)-FL-DA-TRASF-NULL = HIGH-VALUES
        //              TO B03-FL-DA-TRASF-NULL
        //           ELSE
        //              TO B03-FL-DA-TRASF
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlDaTrasf(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-DA-TRASF-NULL
            //           TO B03-FL-DA-TRASF-NULL
            ws.getBilaTrchEstr().setB03FlDaTrasf(areaOut.getLccvb031().getDati().getWb03FlDaTrasf());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-DA-TRASF
            //           TO B03-FL-DA-TRASF
            ws.getBilaTrchEstr().setB03FlDaTrasf(areaOut.getLccvb031().getDati().getWb03FlDaTrasf());
        }
        // COB_CODE: IF (SF)-FL-CAR-CONT-NULL = HIGH-VALUES
        //              TO B03-FL-CAR-CONT-NULL
        //           ELSE
        //              TO B03-FL-CAR-CONT
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlCarCont(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-CAR-CONT-NULL
            //           TO B03-FL-CAR-CONT-NULL
            ws.getBilaTrchEstr().setB03FlCarCont(areaOut.getLccvb031().getDati().getWb03FlCarCont());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-CAR-CONT
            //           TO B03-FL-CAR-CONT
            ws.getBilaTrchEstr().setB03FlCarCont(areaOut.getLccvb031().getDati().getWb03FlCarCont());
        }
        // COB_CODE: IF (SF)-FL-PRE-DA-RIS-NULL = HIGH-VALUES
        //              TO B03-FL-PRE-DA-RIS-NULL
        //           ELSE
        //              TO B03-FL-PRE-DA-RIS
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlPreDaRis(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-PRE-DA-RIS-NULL
            //           TO B03-FL-PRE-DA-RIS-NULL
            ws.getBilaTrchEstr().setB03FlPreDaRis(areaOut.getLccvb031().getDati().getWb03FlPreDaRis());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-PRE-DA-RIS
            //           TO B03-FL-PRE-DA-RIS
            ws.getBilaTrchEstr().setB03FlPreDaRis(areaOut.getLccvb031().getDati().getWb03FlPreDaRis());
        }
        // COB_CODE: IF (SF)-FL-PRE-AGG-NULL = HIGH-VALUES
        //              TO B03-FL-PRE-AGG-NULL
        //           ELSE
        //              TO B03-FL-PRE-AGG
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlPreAgg(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-PRE-AGG-NULL
            //           TO B03-FL-PRE-AGG-NULL
            ws.getBilaTrchEstr().setB03FlPreAgg(areaOut.getLccvb031().getDati().getWb03FlPreAgg());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-PRE-AGG
            //           TO B03-FL-PRE-AGG
            ws.getBilaTrchEstr().setB03FlPreAgg(areaOut.getLccvb031().getDati().getWb03FlPreAgg());
        }
        // COB_CODE: IF (SF)-TP-TRCH-NULL = HIGH-VALUES
        //              TO B03-TP-TRCH-NULL
        //           ELSE
        //              TO B03-TP-TRCH
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpTrchFormatted())) {
            // COB_CODE: MOVE (SF)-TP-TRCH-NULL
            //           TO B03-TP-TRCH-NULL
            ws.getBilaTrchEstr().setB03TpTrch(areaOut.getLccvb031().getDati().getWb03TpTrch());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-TRCH
            //           TO B03-TP-TRCH
            ws.getBilaTrchEstr().setB03TpTrch(areaOut.getLccvb031().getDati().getWb03TpTrch());
        }
        // COB_CODE: IF (SF)-TP-TST-NULL = HIGH-VALUES
        //              TO B03-TP-TST-NULL
        //           ELSE
        //              TO B03-TP-TST
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpTstFormatted())) {
            // COB_CODE: MOVE (SF)-TP-TST-NULL
            //           TO B03-TP-TST-NULL
            ws.getBilaTrchEstr().setB03TpTst(areaOut.getLccvb031().getDati().getWb03TpTst());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-TST
            //           TO B03-TP-TST
            ws.getBilaTrchEstr().setB03TpTst(areaOut.getLccvb031().getDati().getWb03TpTst());
        }
        // COB_CODE: IF (SF)-COD-CONV-NULL = HIGH-VALUES
        //              TO B03-COD-CONV-NULL
        //           ELSE
        //              TO B03-COD-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodConvFormatted())) {
            // COB_CODE: MOVE (SF)-COD-CONV-NULL
            //           TO B03-COD-CONV-NULL
            ws.getBilaTrchEstr().setB03CodConv(areaOut.getLccvb031().getDati().getWb03CodConv());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-CONV
            //           TO B03-COD-CONV
            ws.getBilaTrchEstr().setB03CodConv(areaOut.getLccvb031().getDati().getWb03CodConv());
        }
        // COB_CODE: MOVE (SF)-DT-DECOR-POLI
        //              TO B03-DT-DECOR-POLI
        ws.getBilaTrchEstr().setB03DtDecorPoli(areaOut.getLccvb031().getDati().getWb03DtDecorPoli());
        // COB_CODE: IF (SF)-DT-DECOR-ADES-NULL = HIGH-VALUES
        //              TO B03-DT-DECOR-ADES-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtDecorAdes().getWb03DtDecorAdesNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-DECOR-ADES-NULL
            //           TO B03-DT-DECOR-ADES-NULL
            ws.getBilaTrchEstr().getB03DtDecorAdes().setB03DtDecorAdesNull(areaOut.getLccvb031().getDati().getWb03DtDecorAdes().getWb03DtDecorAdesNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtDecorAdes().getWb03DtDecorAdes() == 0) {
            // COB_CODE: IF (SF)-DT-DECOR-ADES = ZERO
            //              TO B03-DT-DECOR-ADES-NULL
            //           ELSE
            //            TO B03-DT-DECOR-ADES
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-DECOR-ADES-NULL
            ws.getBilaTrchEstr().getB03DtDecorAdes().setB03DtDecorAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtDecorAdes.Len.B03_DT_DECOR_ADES_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-DECOR-ADES
            //           TO B03-DT-DECOR-ADES
            ws.getBilaTrchEstr().getB03DtDecorAdes().setB03DtDecorAdes(areaOut.getLccvb031().getDati().getWb03DtDecorAdes().getWb03DtDecorAdes());
        }
        // COB_CODE: MOVE (SF)-DT-DECOR-TRCH
        //              TO B03-DT-DECOR-TRCH
        ws.getBilaTrchEstr().setB03DtDecorTrch(areaOut.getLccvb031().getDati().getWb03DtDecorTrch());
        // COB_CODE: MOVE (SF)-DT-EMIS-POLI
        //              TO B03-DT-EMIS-POLI
        ws.getBilaTrchEstr().setB03DtEmisPoli(areaOut.getLccvb031().getDati().getWb03DtEmisPoli());
        // COB_CODE: IF (SF)-DT-EMIS-TRCH-NULL = HIGH-VALUES
        //              TO B03-DT-EMIS-TRCH-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtEmisTrch().getWb03DtEmisTrchNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EMIS-TRCH-NULL
            //           TO B03-DT-EMIS-TRCH-NULL
            ws.getBilaTrchEstr().getB03DtEmisTrch().setB03DtEmisTrchNull(areaOut.getLccvb031().getDati().getWb03DtEmisTrch().getWb03DtEmisTrchNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtEmisTrch().getWb03DtEmisTrch() == 0) {
            // COB_CODE: IF (SF)-DT-EMIS-TRCH = ZERO
            //              TO B03-DT-EMIS-TRCH-NULL
            //           ELSE
            //            TO B03-DT-EMIS-TRCH
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-EMIS-TRCH-NULL
            ws.getBilaTrchEstr().getB03DtEmisTrch().setB03DtEmisTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisTrch.Len.B03_DT_EMIS_TRCH_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EMIS-TRCH
            //           TO B03-DT-EMIS-TRCH
            ws.getBilaTrchEstr().getB03DtEmisTrch().setB03DtEmisTrch(areaOut.getLccvb031().getDati().getWb03DtEmisTrch().getWb03DtEmisTrch());
        }
        // COB_CODE: IF (SF)-DT-SCAD-TRCH-NULL = HIGH-VALUES
        //              TO B03-DT-SCAD-TRCH-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtScadTrch().getWb03DtScadTrchNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-SCAD-TRCH-NULL
            //           TO B03-DT-SCAD-TRCH-NULL
            ws.getBilaTrchEstr().getB03DtScadTrch().setB03DtScadTrchNull(areaOut.getLccvb031().getDati().getWb03DtScadTrch().getWb03DtScadTrchNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtScadTrch().getWb03DtScadTrch() == 0) {
            // COB_CODE: IF (SF)-DT-SCAD-TRCH = ZERO
            //              TO B03-DT-SCAD-TRCH-NULL
            //           ELSE
            //            TO B03-DT-SCAD-TRCH
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-SCAD-TRCH-NULL
            ws.getBilaTrchEstr().getB03DtScadTrch().setB03DtScadTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadTrch.Len.B03_DT_SCAD_TRCH_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-SCAD-TRCH
            //           TO B03-DT-SCAD-TRCH
            ws.getBilaTrchEstr().getB03DtScadTrch().setB03DtScadTrch(areaOut.getLccvb031().getDati().getWb03DtScadTrch().getWb03DtScadTrch());
        }
        // COB_CODE: IF (SF)-DT-SCAD-INTMD-NULL = HIGH-VALUES
        //              TO B03-DT-SCAD-INTMD-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtScadIntmd().getWb03DtScadIntmdNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-SCAD-INTMD-NULL
            //           TO B03-DT-SCAD-INTMD-NULL
            ws.getBilaTrchEstr().getB03DtScadIntmd().setB03DtScadIntmdNull(areaOut.getLccvb031().getDati().getWb03DtScadIntmd().getWb03DtScadIntmdNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtScadIntmd().getWb03DtScadIntmd() == 0) {
            // COB_CODE: IF (SF)-DT-SCAD-INTMD = ZERO
            //              TO B03-DT-SCAD-INTMD-NULL
            //           ELSE
            //            TO B03-DT-SCAD-INTMD
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-SCAD-INTMD-NULL
            ws.getBilaTrchEstr().getB03DtScadIntmd().setB03DtScadIntmdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadIntmd.Len.B03_DT_SCAD_INTMD_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-SCAD-INTMD
            //           TO B03-DT-SCAD-INTMD
            ws.getBilaTrchEstr().getB03DtScadIntmd().setB03DtScadIntmd(areaOut.getLccvb031().getDati().getWb03DtScadIntmd().getWb03DtScadIntmd());
        }
        // COB_CODE: IF (SF)-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
        //              TO B03-DT-SCAD-PAG-PRE-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtScadPagPre().getWb03DtScadPagPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-SCAD-PAG-PRE-NULL
            //           TO B03-DT-SCAD-PAG-PRE-NULL
            ws.getBilaTrchEstr().getB03DtScadPagPre().setB03DtScadPagPreNull(areaOut.getLccvb031().getDati().getWb03DtScadPagPre().getWb03DtScadPagPreNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtScadPagPre().getWb03DtScadPagPre() == 0) {
            // COB_CODE: IF (SF)-DT-SCAD-PAG-PRE = ZERO
            //              TO B03-DT-SCAD-PAG-PRE-NULL
            //           ELSE
            //            TO B03-DT-SCAD-PAG-PRE
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-SCAD-PAG-PRE-NULL
            ws.getBilaTrchEstr().getB03DtScadPagPre().setB03DtScadPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtScadPagPre.Len.B03_DT_SCAD_PAG_PRE_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-SCAD-PAG-PRE
            //           TO B03-DT-SCAD-PAG-PRE
            ws.getBilaTrchEstr().getB03DtScadPagPre().setB03DtScadPagPre(areaOut.getLccvb031().getDati().getWb03DtScadPagPre().getWb03DtScadPagPre());
        }
        // COB_CODE: IF (SF)-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
        //              TO B03-DT-ULT-PRE-PAG-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtUltPrePag().getWb03DtUltPrePagNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-PRE-PAG-NULL
            //           TO B03-DT-ULT-PRE-PAG-NULL
            ws.getBilaTrchEstr().getB03DtUltPrePag().setB03DtUltPrePagNull(areaOut.getLccvb031().getDati().getWb03DtUltPrePag().getWb03DtUltPrePagNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtUltPrePag().getWb03DtUltPrePag() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-PRE-PAG = ZERO
            //              TO B03-DT-ULT-PRE-PAG-NULL
            //           ELSE
            //            TO B03-DT-ULT-PRE-PAG
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-ULT-PRE-PAG-NULL
            ws.getBilaTrchEstr().getB03DtUltPrePag().setB03DtUltPrePagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtUltPrePag.Len.B03_DT_ULT_PRE_PAG_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-PRE-PAG
            //           TO B03-DT-ULT-PRE-PAG
            ws.getBilaTrchEstr().getB03DtUltPrePag().setB03DtUltPrePag(areaOut.getLccvb031().getDati().getWb03DtUltPrePag().getWb03DtUltPrePag());
        }
        // COB_CODE: IF (SF)-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
        //              TO B03-DT-NASC-1O-ASSTO-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtNasc1oAssto().getWb03DtNasc1oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-NASC-1O-ASSTO-NULL
            //           TO B03-DT-NASC-1O-ASSTO-NULL
            ws.getBilaTrchEstr().getB03DtNasc1oAssto().setB03DtNasc1oAsstoNull(areaOut.getLccvb031().getDati().getWb03DtNasc1oAssto().getWb03DtNasc1oAsstoNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtNasc1oAssto().getWb03DtNasc1oAssto() == 0) {
            // COB_CODE: IF (SF)-DT-NASC-1O-ASSTO = ZERO
            //              TO B03-DT-NASC-1O-ASSTO-NULL
            //           ELSE
            //            TO B03-DT-NASC-1O-ASSTO
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-NASC-1O-ASSTO-NULL
            ws.getBilaTrchEstr().getB03DtNasc1oAssto().setB03DtNasc1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtNasc1oAssto.Len.B03_DT_NASC1O_ASSTO_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-NASC-1O-ASSTO
            //           TO B03-DT-NASC-1O-ASSTO
            ws.getBilaTrchEstr().getB03DtNasc1oAssto().setB03DtNasc1oAssto(areaOut.getLccvb031().getDati().getWb03DtNasc1oAssto().getWb03DtNasc1oAssto());
        }
        // COB_CODE: IF (SF)-SEX-1O-ASSTO-NULL = HIGH-VALUES
        //              TO B03-SEX-1O-ASSTO-NULL
        //           ELSE
        //              TO B03-SEX-1O-ASSTO
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03Sex1oAssto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-SEX-1O-ASSTO-NULL
            //           TO B03-SEX-1O-ASSTO-NULL
            ws.getBilaTrchEstr().setB03Sex1oAssto(areaOut.getLccvb031().getDati().getWb03Sex1oAssto());
        }
        else {
            // COB_CODE: MOVE (SF)-SEX-1O-ASSTO
            //           TO B03-SEX-1O-ASSTO
            ws.getBilaTrchEstr().setB03Sex1oAssto(areaOut.getLccvb031().getDati().getWb03Sex1oAssto());
        }
        // COB_CODE: IF (SF)-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //              TO B03-ETA-AA-1O-ASSTO-NULL
        //           ELSE
        //              TO B03-ETA-AA-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03EtaAa1oAssto().getWb03EtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-AA-1O-ASSTO-NULL
            //           TO B03-ETA-AA-1O-ASSTO-NULL
            ws.getBilaTrchEstr().getB03EtaAa1oAssto().setB03EtaAa1oAsstoNull(areaOut.getLccvb031().getDati().getWb03EtaAa1oAssto().getWb03EtaAa1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-AA-1O-ASSTO
            //           TO B03-ETA-AA-1O-ASSTO
            ws.getBilaTrchEstr().getB03EtaAa1oAssto().setB03EtaAa1oAssto(areaOut.getLccvb031().getDati().getWb03EtaAa1oAssto().getWb03EtaAa1oAssto());
        }
        // COB_CODE: IF (SF)-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //              TO B03-ETA-MM-1O-ASSTO-NULL
        //           ELSE
        //              TO B03-ETA-MM-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03EtaMm1oAssto().getWb03EtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-MM-1O-ASSTO-NULL
            //           TO B03-ETA-MM-1O-ASSTO-NULL
            ws.getBilaTrchEstr().getB03EtaMm1oAssto().setB03EtaMm1oAsstoNull(areaOut.getLccvb031().getDati().getWb03EtaMm1oAssto().getWb03EtaMm1oAsstoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-MM-1O-ASSTO
            //           TO B03-ETA-MM-1O-ASSTO
            ws.getBilaTrchEstr().getB03EtaMm1oAssto().setB03EtaMm1oAssto(areaOut.getLccvb031().getDati().getWb03EtaMm1oAssto().getWb03EtaMm1oAssto());
        }
        // COB_CODE: IF (SF)-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
        //              TO B03-ETA-RAGGN-DT-CALC-NULL
        //           ELSE
        //              TO B03-ETA-RAGGN-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03EtaRaggnDtCalc().getWb03EtaRaggnDtCalcNullFormatted())) {
            // COB_CODE: MOVE (SF)-ETA-RAGGN-DT-CALC-NULL
            //           TO B03-ETA-RAGGN-DT-CALC-NULL
            ws.getBilaTrchEstr().getB03EtaRaggnDtCalc().setB03EtaRaggnDtCalcNull(areaOut.getLccvb031().getDati().getWb03EtaRaggnDtCalc().getWb03EtaRaggnDtCalcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ETA-RAGGN-DT-CALC
            //           TO B03-ETA-RAGGN-DT-CALC
            ws.getBilaTrchEstr().getB03EtaRaggnDtCalc().setB03EtaRaggnDtCalc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03EtaRaggnDtCalc().getWb03EtaRaggnDtCalc(), 7, 3));
        }
        // COB_CODE: IF (SF)-DUR-AA-NULL = HIGH-VALUES
        //              TO B03-DUR-AA-NULL
        //           ELSE
        //              TO B03-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurAa().getWb03DurAaNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-AA-NULL
            //           TO B03-DUR-AA-NULL
            ws.getBilaTrchEstr().getB03DurAa().setB03DurAaNull(areaOut.getLccvb031().getDati().getWb03DurAa().getWb03DurAaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-AA
            //           TO B03-DUR-AA
            ws.getBilaTrchEstr().getB03DurAa().setB03DurAa(areaOut.getLccvb031().getDati().getWb03DurAa().getWb03DurAa());
        }
        // COB_CODE: IF (SF)-DUR-MM-NULL = HIGH-VALUES
        //              TO B03-DUR-MM-NULL
        //           ELSE
        //              TO B03-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurMm().getWb03DurMmNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-MM-NULL
            //           TO B03-DUR-MM-NULL
            ws.getBilaTrchEstr().getB03DurMm().setB03DurMmNull(areaOut.getLccvb031().getDati().getWb03DurMm().getWb03DurMmNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-MM
            //           TO B03-DUR-MM
            ws.getBilaTrchEstr().getB03DurMm().setB03DurMm(areaOut.getLccvb031().getDati().getWb03DurMm().getWb03DurMm());
        }
        // COB_CODE: IF (SF)-DUR-GG-NULL = HIGH-VALUES
        //              TO B03-DUR-GG-NULL
        //           ELSE
        //              TO B03-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurGg().getWb03DurGgNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-GG-NULL
            //           TO B03-DUR-GG-NULL
            ws.getBilaTrchEstr().getB03DurGg().setB03DurGgNull(areaOut.getLccvb031().getDati().getWb03DurGg().getWb03DurGgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-GG
            //           TO B03-DUR-GG
            ws.getBilaTrchEstr().getB03DurGg().setB03DurGg(areaOut.getLccvb031().getDati().getWb03DurGg().getWb03DurGg());
        }
        // COB_CODE: IF (SF)-DUR-1O-PER-AA-NULL = HIGH-VALUES
        //              TO B03-DUR-1O-PER-AA-NULL
        //           ELSE
        //              TO B03-DUR-1O-PER-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Dur1oPerAa().getWb03Dur1oPerAaNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-1O-PER-AA-NULL
            //           TO B03-DUR-1O-PER-AA-NULL
            ws.getBilaTrchEstr().getB03Dur1oPerAa().setB03Dur1oPerAaNull(areaOut.getLccvb031().getDati().getWb03Dur1oPerAa().getWb03Dur1oPerAaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-1O-PER-AA
            //           TO B03-DUR-1O-PER-AA
            ws.getBilaTrchEstr().getB03Dur1oPerAa().setB03Dur1oPerAa(areaOut.getLccvb031().getDati().getWb03Dur1oPerAa().getWb03Dur1oPerAa());
        }
        // COB_CODE: IF (SF)-DUR-1O-PER-MM-NULL = HIGH-VALUES
        //              TO B03-DUR-1O-PER-MM-NULL
        //           ELSE
        //              TO B03-DUR-1O-PER-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Dur1oPerMm().getWb03Dur1oPerMmNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-1O-PER-MM-NULL
            //           TO B03-DUR-1O-PER-MM-NULL
            ws.getBilaTrchEstr().getB03Dur1oPerMm().setB03Dur1oPerMmNull(areaOut.getLccvb031().getDati().getWb03Dur1oPerMm().getWb03Dur1oPerMmNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-1O-PER-MM
            //           TO B03-DUR-1O-PER-MM
            ws.getBilaTrchEstr().getB03Dur1oPerMm().setB03Dur1oPerMm(areaOut.getLccvb031().getDati().getWb03Dur1oPerMm().getWb03Dur1oPerMm());
        }
        // COB_CODE: IF (SF)-DUR-1O-PER-GG-NULL = HIGH-VALUES
        //              TO B03-DUR-1O-PER-GG-NULL
        //           ELSE
        //              TO B03-DUR-1O-PER-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Dur1oPerGg().getWb03Dur1oPerGgNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-1O-PER-GG-NULL
            //           TO B03-DUR-1O-PER-GG-NULL
            ws.getBilaTrchEstr().getB03Dur1oPerGg().setB03Dur1oPerGgNull(areaOut.getLccvb031().getDati().getWb03Dur1oPerGg().getWb03Dur1oPerGgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-1O-PER-GG
            //           TO B03-DUR-1O-PER-GG
            ws.getBilaTrchEstr().getB03Dur1oPerGg().setB03Dur1oPerGg(areaOut.getLccvb031().getDati().getWb03Dur1oPerGg().getWb03Dur1oPerGg());
        }
        // COB_CODE: IF (SF)-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
        //              TO B03-ANTIDUR-RICOR-PREC-NULL
        //           ELSE
        //              TO B03-ANTIDUR-RICOR-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AntidurRicorPrec().getWb03AntidurRicorPrecNullFormatted())) {
            // COB_CODE: MOVE (SF)-ANTIDUR-RICOR-PREC-NULL
            //           TO B03-ANTIDUR-RICOR-PREC-NULL
            ws.getBilaTrchEstr().getB03AntidurRicorPrec().setB03AntidurRicorPrecNull(areaOut.getLccvb031().getDati().getWb03AntidurRicorPrec().getWb03AntidurRicorPrecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ANTIDUR-RICOR-PREC
            //           TO B03-ANTIDUR-RICOR-PREC
            ws.getBilaTrchEstr().getB03AntidurRicorPrec().setB03AntidurRicorPrec(areaOut.getLccvb031().getDati().getWb03AntidurRicorPrec().getWb03AntidurRicorPrec());
        }
        // COB_CODE: IF (SF)-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
        //              TO B03-ANTIDUR-DT-CALC-NULL
        //           ELSE
        //              TO B03-ANTIDUR-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AntidurDtCalc().getWb03AntidurDtCalcNullFormatted())) {
            // COB_CODE: MOVE (SF)-ANTIDUR-DT-CALC-NULL
            //           TO B03-ANTIDUR-DT-CALC-NULL
            ws.getBilaTrchEstr().getB03AntidurDtCalc().setB03AntidurDtCalcNull(areaOut.getLccvb031().getDati().getWb03AntidurDtCalc().getWb03AntidurDtCalcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ANTIDUR-DT-CALC
            //           TO B03-ANTIDUR-DT-CALC
            ws.getBilaTrchEstr().getB03AntidurDtCalc().setB03AntidurDtCalc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03AntidurDtCalc().getWb03AntidurDtCalc(), 11, 7));
        }
        // COB_CODE: IF (SF)-DUR-RES-DT-CALC-NULL = HIGH-VALUES
        //              TO B03-DUR-RES-DT-CALC-NULL
        //           ELSE
        //              TO B03-DUR-RES-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurResDtCalc().getWb03DurResDtCalcNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-RES-DT-CALC-NULL
            //           TO B03-DUR-RES-DT-CALC-NULL
            ws.getBilaTrchEstr().getB03DurResDtCalc().setB03DurResDtCalcNull(areaOut.getLccvb031().getDati().getWb03DurResDtCalc().getWb03DurResDtCalcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-RES-DT-CALC
            //           TO B03-DUR-RES-DT-CALC
            ws.getBilaTrchEstr().getB03DurResDtCalc().setB03DurResDtCalc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03DurResDtCalc().getWb03DurResDtCalc(), 11, 7));
        }
        // COB_CODE: MOVE (SF)-TP-STAT-BUS-POLI
        //              TO B03-TP-STAT-BUS-POLI
        ws.getBilaTrchEstr().setB03TpStatBusPoli(areaOut.getLccvb031().getDati().getWb03TpStatBusPoli());
        // COB_CODE: MOVE (SF)-TP-CAUS-POLI
        //              TO B03-TP-CAUS-POLI
        ws.getBilaTrchEstr().setB03TpCausPoli(areaOut.getLccvb031().getDati().getWb03TpCausPoli());
        // COB_CODE: MOVE (SF)-TP-STAT-BUS-ADES
        //              TO B03-TP-STAT-BUS-ADES
        ws.getBilaTrchEstr().setB03TpStatBusAdes(areaOut.getLccvb031().getDati().getWb03TpStatBusAdes());
        // COB_CODE: MOVE (SF)-TP-CAUS-ADES
        //              TO B03-TP-CAUS-ADES
        ws.getBilaTrchEstr().setB03TpCausAdes(areaOut.getLccvb031().getDati().getWb03TpCausAdes());
        // COB_CODE: MOVE (SF)-TP-STAT-BUS-TRCH
        //              TO B03-TP-STAT-BUS-TRCH
        ws.getBilaTrchEstr().setB03TpStatBusTrch(areaOut.getLccvb031().getDati().getWb03TpStatBusTrch());
        // COB_CODE: MOVE (SF)-TP-CAUS-TRCH
        //              TO B03-TP-CAUS-TRCH
        ws.getBilaTrchEstr().setB03TpCausTrch(areaOut.getLccvb031().getDati().getWb03TpCausTrch());
        // COB_CODE: IF (SF)-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
        //              TO B03-DT-EFF-CAMB-STAT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtEffCambStat().getWb03DtEffCambStatNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EFF-CAMB-STAT-NULL
            //           TO B03-DT-EFF-CAMB-STAT-NULL
            ws.getBilaTrchEstr().getB03DtEffCambStat().setB03DtEffCambStatNull(areaOut.getLccvb031().getDati().getWb03DtEffCambStat().getWb03DtEffCambStatNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtEffCambStat().getWb03DtEffCambStat() == 0) {
            // COB_CODE: IF (SF)-DT-EFF-CAMB-STAT = ZERO
            //              TO B03-DT-EFF-CAMB-STAT-NULL
            //           ELSE
            //            TO B03-DT-EFF-CAMB-STAT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-EFF-CAMB-STAT-NULL
            ws.getBilaTrchEstr().getB03DtEffCambStat().setB03DtEffCambStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffCambStat.Len.B03_DT_EFF_CAMB_STAT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EFF-CAMB-STAT
            //           TO B03-DT-EFF-CAMB-STAT
            ws.getBilaTrchEstr().getB03DtEffCambStat().setB03DtEffCambStat(areaOut.getLccvb031().getDati().getWb03DtEffCambStat().getWb03DtEffCambStat());
        }
        // COB_CODE: IF (SF)-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
        //              TO B03-DT-EMIS-CAMB-STAT-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtEmisCambStat().getWb03DtEmisCambStatNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EMIS-CAMB-STAT-NULL
            //           TO B03-DT-EMIS-CAMB-STAT-NULL
            ws.getBilaTrchEstr().getB03DtEmisCambStat().setB03DtEmisCambStatNull(areaOut.getLccvb031().getDati().getWb03DtEmisCambStat().getWb03DtEmisCambStatNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtEmisCambStat().getWb03DtEmisCambStat() == 0) {
            // COB_CODE: IF (SF)-DT-EMIS-CAMB-STAT = ZERO
            //              TO B03-DT-EMIS-CAMB-STAT-NULL
            //           ELSE
            //            TO B03-DT-EMIS-CAMB-STAT
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-EMIS-CAMB-STAT-NULL
            ws.getBilaTrchEstr().getB03DtEmisCambStat().setB03DtEmisCambStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisCambStat.Len.B03_DT_EMIS_CAMB_STAT_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EMIS-CAMB-STAT
            //           TO B03-DT-EMIS-CAMB-STAT
            ws.getBilaTrchEstr().getB03DtEmisCambStat().setB03DtEmisCambStat(areaOut.getLccvb031().getDati().getWb03DtEmisCambStat().getWb03DtEmisCambStat());
        }
        // COB_CODE: IF (SF)-DT-EFF-STAB-NULL = HIGH-VALUES
        //              TO B03-DT-EFF-STAB-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtEffStab().getWb03DtEffStabNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EFF-STAB-NULL
            //           TO B03-DT-EFF-STAB-NULL
            ws.getBilaTrchEstr().getB03DtEffStab().setB03DtEffStabNull(areaOut.getLccvb031().getDati().getWb03DtEffStab().getWb03DtEffStabNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtEffStab().getWb03DtEffStab() == 0) {
            // COB_CODE: IF (SF)-DT-EFF-STAB = ZERO
            //              TO B03-DT-EFF-STAB-NULL
            //           ELSE
            //            TO B03-DT-EFF-STAB
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-EFF-STAB-NULL
            ws.getBilaTrchEstr().getB03DtEffStab().setB03DtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffStab.Len.B03_DT_EFF_STAB_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EFF-STAB
            //           TO B03-DT-EFF-STAB
            ws.getBilaTrchEstr().getB03DtEffStab().setB03DtEffStab(areaOut.getLccvb031().getDati().getWb03DtEffStab().getWb03DtEffStab());
        }
        // COB_CODE: IF (SF)-CPT-DT-STAB-NULL = HIGH-VALUES
        //              TO B03-CPT-DT-STAB-NULL
        //           ELSE
        //              TO B03-CPT-DT-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CptDtStab().getWb03CptDtStabNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-DT-STAB-NULL
            //           TO B03-CPT-DT-STAB-NULL
            ws.getBilaTrchEstr().getB03CptDtStab().setB03CptDtStabNull(areaOut.getLccvb031().getDati().getWb03CptDtStab().getWb03CptDtStabNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-DT-STAB
            //           TO B03-CPT-DT-STAB
            ws.getBilaTrchEstr().getB03CptDtStab().setB03CptDtStab(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CptDtStab().getWb03CptDtStab(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-EFF-RIDZ-NULL = HIGH-VALUES
        //              TO B03-DT-EFF-RIDZ-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtEffRidz().getWb03DtEffRidzNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EFF-RIDZ-NULL
            //           TO B03-DT-EFF-RIDZ-NULL
            ws.getBilaTrchEstr().getB03DtEffRidz().setB03DtEffRidzNull(areaOut.getLccvb031().getDati().getWb03DtEffRidz().getWb03DtEffRidzNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtEffRidz().getWb03DtEffRidz() == 0) {
            // COB_CODE: IF (SF)-DT-EFF-RIDZ = ZERO
            //              TO B03-DT-EFF-RIDZ-NULL
            //           ELSE
            //            TO B03-DT-EFF-RIDZ
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-EFF-RIDZ-NULL
            ws.getBilaTrchEstr().getB03DtEffRidz().setB03DtEffRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEffRidz.Len.B03_DT_EFF_RIDZ_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EFF-RIDZ
            //           TO B03-DT-EFF-RIDZ
            ws.getBilaTrchEstr().getB03DtEffRidz().setB03DtEffRidz(areaOut.getLccvb031().getDati().getWb03DtEffRidz().getWb03DtEffRidz());
        }
        // COB_CODE: IF (SF)-DT-EMIS-RIDZ-NULL = HIGH-VALUES
        //              TO B03-DT-EMIS-RIDZ-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtEmisRidz().getWb03DtEmisRidzNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-EMIS-RIDZ-NULL
            //           TO B03-DT-EMIS-RIDZ-NULL
            ws.getBilaTrchEstr().getB03DtEmisRidz().setB03DtEmisRidzNull(areaOut.getLccvb031().getDati().getWb03DtEmisRidz().getWb03DtEmisRidzNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtEmisRidz().getWb03DtEmisRidz() == 0) {
            // COB_CODE: IF (SF)-DT-EMIS-RIDZ = ZERO
            //              TO B03-DT-EMIS-RIDZ-NULL
            //           ELSE
            //            TO B03-DT-EMIS-RIDZ
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-EMIS-RIDZ-NULL
            ws.getBilaTrchEstr().getB03DtEmisRidz().setB03DtEmisRidzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtEmisRidz.Len.B03_DT_EMIS_RIDZ_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-EMIS-RIDZ
            //           TO B03-DT-EMIS-RIDZ
            ws.getBilaTrchEstr().getB03DtEmisRidz().setB03DtEmisRidz(areaOut.getLccvb031().getDati().getWb03DtEmisRidz().getWb03DtEmisRidz());
        }
        // COB_CODE: IF (SF)-CPT-DT-RIDZ-NULL = HIGH-VALUES
        //              TO B03-CPT-DT-RIDZ-NULL
        //           ELSE
        //              TO B03-CPT-DT-RIDZ
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CptDtRidz().getWb03CptDtRidzNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-DT-RIDZ-NULL
            //           TO B03-CPT-DT-RIDZ-NULL
            ws.getBilaTrchEstr().getB03CptDtRidz().setB03CptDtRidzNull(areaOut.getLccvb031().getDati().getWb03CptDtRidz().getWb03CptDtRidzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-DT-RIDZ
            //           TO B03-CPT-DT-RIDZ
            ws.getBilaTrchEstr().getB03CptDtRidz().setB03CptDtRidz(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CptDtRidz().getWb03CptDtRidz(), 15, 3));
        }
        // COB_CODE: IF (SF)-FRAZ-NULL = HIGH-VALUES
        //              TO B03-FRAZ-NULL
        //           ELSE
        //              TO B03-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Fraz().getWb03FrazNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRAZ-NULL
            //           TO B03-FRAZ-NULL
            ws.getBilaTrchEstr().getB03Fraz().setB03FrazNull(areaOut.getLccvb031().getDati().getWb03Fraz().getWb03FrazNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRAZ
            //           TO B03-FRAZ
            ws.getBilaTrchEstr().getB03Fraz().setB03Fraz(areaOut.getLccvb031().getDati().getWb03Fraz().getWb03Fraz());
        }
        // COB_CODE: IF (SF)-DUR-PAG-PRE-NULL = HIGH-VALUES
        //              TO B03-DUR-PAG-PRE-NULL
        //           ELSE
        //              TO B03-DUR-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurPagPre().getWb03DurPagPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-PAG-PRE-NULL
            //           TO B03-DUR-PAG-PRE-NULL
            ws.getBilaTrchEstr().getB03DurPagPre().setB03DurPagPreNull(areaOut.getLccvb031().getDati().getWb03DurPagPre().getWb03DurPagPreNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-PAG-PRE
            //           TO B03-DUR-PAG-PRE
            ws.getBilaTrchEstr().getB03DurPagPre().setB03DurPagPre(areaOut.getLccvb031().getDati().getWb03DurPagPre().getWb03DurPagPre());
        }
        // COB_CODE: IF (SF)-NUM-PRE-PATT-NULL = HIGH-VALUES
        //              TO B03-NUM-PRE-PATT-NULL
        //           ELSE
        //              TO B03-NUM-PRE-PATT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03NumPrePatt().getWb03NumPrePattNullFormatted())) {
            // COB_CODE: MOVE (SF)-NUM-PRE-PATT-NULL
            //           TO B03-NUM-PRE-PATT-NULL
            ws.getBilaTrchEstr().getB03NumPrePatt().setB03NumPrePattNull(areaOut.getLccvb031().getDati().getWb03NumPrePatt().getWb03NumPrePattNull());
        }
        else {
            // COB_CODE: MOVE (SF)-NUM-PRE-PATT
            //           TO B03-NUM-PRE-PATT
            ws.getBilaTrchEstr().getB03NumPrePatt().setB03NumPrePatt(areaOut.getLccvb031().getDati().getWb03NumPrePatt().getWb03NumPrePatt());
        }
        // COB_CODE: IF (SF)-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
        //              TO B03-FRAZ-INI-EROG-REN-NULL
        //           ELSE
        //              TO B03-FRAZ-INI-EROG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03FrazIniErogRen().getWb03FrazIniErogRenNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRAZ-INI-EROG-REN-NULL
            //           TO B03-FRAZ-INI-EROG-REN-NULL
            ws.getBilaTrchEstr().getB03FrazIniErogRen().setB03FrazIniErogRenNull(areaOut.getLccvb031().getDati().getWb03FrazIniErogRen().getWb03FrazIniErogRenNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRAZ-INI-EROG-REN
            //           TO B03-FRAZ-INI-EROG-REN
            ws.getBilaTrchEstr().getB03FrazIniErogRen().setB03FrazIniErogRen(areaOut.getLccvb031().getDati().getWb03FrazIniErogRen().getWb03FrazIniErogRen());
        }
        // COB_CODE: IF (SF)-AA-REN-CER-NULL = HIGH-VALUES
        //              TO B03-AA-REN-CER-NULL
        //           ELSE
        //              TO B03-AA-REN-CER
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AaRenCer().getWb03AaRenCerNullFormatted())) {
            // COB_CODE: MOVE (SF)-AA-REN-CER-NULL
            //           TO B03-AA-REN-CER-NULL
            ws.getBilaTrchEstr().getB03AaRenCer().setB03AaRenCerNull(areaOut.getLccvb031().getDati().getWb03AaRenCer().getWb03AaRenCerNull());
        }
        else {
            // COB_CODE: MOVE (SF)-AA-REN-CER
            //           TO B03-AA-REN-CER
            ws.getBilaTrchEstr().getB03AaRenCer().setB03AaRenCer(areaOut.getLccvb031().getDati().getWb03AaRenCer().getWb03AaRenCer());
        }
        // COB_CODE: IF (SF)-RAT-REN-NULL = HIGH-VALUES
        //              TO B03-RAT-REN-NULL
        //           ELSE
        //              TO B03-RAT-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RatRen().getWb03RatRenNullFormatted())) {
            // COB_CODE: MOVE (SF)-RAT-REN-NULL
            //           TO B03-RAT-REN-NULL
            ws.getBilaTrchEstr().getB03RatRen().setB03RatRenNull(areaOut.getLccvb031().getDati().getWb03RatRen().getWb03RatRenNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RAT-REN
            //           TO B03-RAT-REN
            ws.getBilaTrchEstr().getB03RatRen().setB03RatRen(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RatRen().getWb03RatRen(), 15, 3));
        }
        // COB_CODE: IF (SF)-COD-DIV-NULL = HIGH-VALUES
        //              TO B03-COD-DIV-NULL
        //           ELSE
        //              TO B03-COD-DIV
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodDivFormatted())) {
            // COB_CODE: MOVE (SF)-COD-DIV-NULL
            //           TO B03-COD-DIV-NULL
            ws.getBilaTrchEstr().setB03CodDiv(areaOut.getLccvb031().getDati().getWb03CodDiv());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-DIV
            //           TO B03-COD-DIV
            ws.getBilaTrchEstr().setB03CodDiv(areaOut.getLccvb031().getDati().getWb03CodDiv());
        }
        // COB_CODE: IF (SF)-RISCPAR-NULL = HIGH-VALUES
        //              TO B03-RISCPAR-NULL
        //           ELSE
        //              TO B03-RISCPAR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Riscpar().getWb03RiscparNullFormatted())) {
            // COB_CODE: MOVE (SF)-RISCPAR-NULL
            //           TO B03-RISCPAR-NULL
            ws.getBilaTrchEstr().getB03Riscpar().setB03RiscparNull(areaOut.getLccvb031().getDati().getWb03Riscpar().getWb03RiscparNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RISCPAR
            //           TO B03-RISCPAR
            ws.getBilaTrchEstr().getB03Riscpar().setB03Riscpar(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03Riscpar().getWb03Riscpar(), 15, 3));
        }
        // COB_CODE: IF (SF)-CUM-RISCPAR-NULL = HIGH-VALUES
        //              TO B03-CUM-RISCPAR-NULL
        //           ELSE
        //              TO B03-CUM-RISCPAR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CumRiscpar().getWb03CumRiscparNullFormatted())) {
            // COB_CODE: MOVE (SF)-CUM-RISCPAR-NULL
            //           TO B03-CUM-RISCPAR-NULL
            ws.getBilaTrchEstr().getB03CumRiscpar().setB03CumRiscparNull(areaOut.getLccvb031().getDati().getWb03CumRiscpar().getWb03CumRiscparNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CUM-RISCPAR
            //           TO B03-CUM-RISCPAR
            ws.getBilaTrchEstr().getB03CumRiscpar().setB03CumRiscpar(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CumRiscpar().getWb03CumRiscpar(), 15, 3));
        }
        // COB_CODE: IF (SF)-ULT-RM-NULL = HIGH-VALUES
        //              TO B03-ULT-RM-NULL
        //           ELSE
        //              TO B03-ULT-RM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03UltRm().getWb03UltRmNullFormatted())) {
            // COB_CODE: MOVE (SF)-ULT-RM-NULL
            //           TO B03-ULT-RM-NULL
            ws.getBilaTrchEstr().getB03UltRm().setB03UltRmNull(areaOut.getLccvb031().getDati().getWb03UltRm().getWb03UltRmNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ULT-RM
            //           TO B03-ULT-RM
            ws.getBilaTrchEstr().getB03UltRm().setB03UltRm(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03UltRm().getWb03UltRm(), 15, 3));
        }
        // COB_CODE: IF (SF)-TS-RENDTO-T-NULL = HIGH-VALUES
        //              TO B03-TS-RENDTO-T-NULL
        //           ELSE
        //              TO B03-TS-RENDTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsRendtoT().getWb03TsRendtoTNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-RENDTO-T-NULL
            //           TO B03-TS-RENDTO-T-NULL
            ws.getBilaTrchEstr().getB03TsRendtoT().setB03TsRendtoTNull(areaOut.getLccvb031().getDati().getWb03TsRendtoT().getWb03TsRendtoTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-RENDTO-T
            //           TO B03-TS-RENDTO-T
            ws.getBilaTrchEstr().getB03TsRendtoT().setB03TsRendtoT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsRendtoT().getWb03TsRendtoT(), 14, 9));
        }
        // COB_CODE: IF (SF)-ALQ-RETR-T-NULL = HIGH-VALUES
        //              TO B03-ALQ-RETR-T-NULL
        //           ELSE
        //              TO B03-ALQ-RETR-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AlqRetrT().getWb03AlqRetrTNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-RETR-T-NULL
            //           TO B03-ALQ-RETR-T-NULL
            ws.getBilaTrchEstr().getB03AlqRetrT().setB03AlqRetrTNull(areaOut.getLccvb031().getDati().getWb03AlqRetrT().getWb03AlqRetrTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-RETR-T
            //           TO B03-ALQ-RETR-T
            ws.getBilaTrchEstr().getB03AlqRetrT().setB03AlqRetrT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03AlqRetrT().getWb03AlqRetrT(), 6, 3));
        }
        // COB_CODE: IF (SF)-MIN-TRNUT-T-NULL = HIGH-VALUES
        //              TO B03-MIN-TRNUT-T-NULL
        //           ELSE
        //              TO B03-MIN-TRNUT-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03MinTrnutT().getWb03MinTrnutTNullFormatted())) {
            // COB_CODE: MOVE (SF)-MIN-TRNUT-T-NULL
            //           TO B03-MIN-TRNUT-T-NULL
            ws.getBilaTrchEstr().getB03MinTrnutT().setB03MinTrnutTNull(areaOut.getLccvb031().getDati().getWb03MinTrnutT().getWb03MinTrnutTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MIN-TRNUT-T
            //           TO B03-MIN-TRNUT-T
            ws.getBilaTrchEstr().getB03MinTrnutT().setB03MinTrnutT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03MinTrnutT().getWb03MinTrnutT(), 15, 3));
        }
        // COB_CODE: IF (SF)-TS-NET-T-NULL = HIGH-VALUES
        //              TO B03-TS-NET-T-NULL
        //           ELSE
        //              TO B03-TS-NET-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsNetT().getWb03TsNetTNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-NET-T-NULL
            //           TO B03-TS-NET-T-NULL
            ws.getBilaTrchEstr().getB03TsNetT().setB03TsNetTNull(areaOut.getLccvb031().getDati().getWb03TsNetT().getWb03TsNetTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-NET-T
            //           TO B03-TS-NET-T
            ws.getBilaTrchEstr().getB03TsNetT().setB03TsNetT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsNetT().getWb03TsNetT(), 14, 9));
        }
        // COB_CODE: IF (SF)-DT-ULT-RIVAL-NULL = HIGH-VALUES
        //              TO B03-DT-ULT-RIVAL-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtUltRival().getWb03DtUltRivalNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-ULT-RIVAL-NULL
            //           TO B03-DT-ULT-RIVAL-NULL
            ws.getBilaTrchEstr().getB03DtUltRival().setB03DtUltRivalNull(areaOut.getLccvb031().getDati().getWb03DtUltRival().getWb03DtUltRivalNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtUltRival().getWb03DtUltRival() == 0) {
            // COB_CODE: IF (SF)-DT-ULT-RIVAL = ZERO
            //              TO B03-DT-ULT-RIVAL-NULL
            //           ELSE
            //            TO B03-DT-ULT-RIVAL
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-ULT-RIVAL-NULL
            ws.getBilaTrchEstr().getB03DtUltRival().setB03DtUltRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtUltRival.Len.B03_DT_ULT_RIVAL_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-ULT-RIVAL
            //           TO B03-DT-ULT-RIVAL
            ws.getBilaTrchEstr().getB03DtUltRival().setB03DtUltRival(areaOut.getLccvb031().getDati().getWb03DtUltRival().getWb03DtUltRival());
        }
        // COB_CODE: IF (SF)-PRSTZ-INI-NULL = HIGH-VALUES
        //              TO B03-PRSTZ-INI-NULL
        //           ELSE
        //              TO B03-PRSTZ-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrstzIni().getWb03PrstzIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-INI-NULL
            //           TO B03-PRSTZ-INI-NULL
            ws.getBilaTrchEstr().getB03PrstzIni().setB03PrstzIniNull(areaOut.getLccvb031().getDati().getWb03PrstzIni().getWb03PrstzIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-INI
            //           TO B03-PRSTZ-INI
            ws.getBilaTrchEstr().getB03PrstzIni().setB03PrstzIni(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrstzIni().getWb03PrstzIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRSTZ-AGG-INI-NULL = HIGH-VALUES
        //              TO B03-PRSTZ-AGG-INI-NULL
        //           ELSE
        //              TO B03-PRSTZ-AGG-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrstzAggIni().getWb03PrstzAggIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-INI-NULL
            //           TO B03-PRSTZ-AGG-INI-NULL
            ws.getBilaTrchEstr().getB03PrstzAggIni().setB03PrstzAggIniNull(areaOut.getLccvb031().getDati().getWb03PrstzAggIni().getWb03PrstzAggIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-INI
            //           TO B03-PRSTZ-AGG-INI
            ws.getBilaTrchEstr().getB03PrstzAggIni().setB03PrstzAggIni(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrstzAggIni().getWb03PrstzAggIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
        //              TO B03-PRSTZ-AGG-ULT-NULL
        //           ELSE
        //              TO B03-PRSTZ-AGG-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrstzAggUlt().getWb03PrstzAggUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-ULT-NULL
            //           TO B03-PRSTZ-AGG-ULT-NULL
            ws.getBilaTrchEstr().getB03PrstzAggUlt().setB03PrstzAggUltNull(areaOut.getLccvb031().getDati().getWb03PrstzAggUlt().getWb03PrstzAggUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-AGG-ULT
            //           TO B03-PRSTZ-AGG-ULT
            ws.getBilaTrchEstr().getB03PrstzAggUlt().setB03PrstzAggUlt(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrstzAggUlt().getWb03PrstzAggUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-RAPPEL-NULL = HIGH-VALUES
        //              TO B03-RAPPEL-NULL
        //           ELSE
        //              TO B03-RAPPEL
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Rappel().getWb03RappelNullFormatted())) {
            // COB_CODE: MOVE (SF)-RAPPEL-NULL
            //           TO B03-RAPPEL-NULL
            ws.getBilaTrchEstr().getB03Rappel().setB03RappelNull(areaOut.getLccvb031().getDati().getWb03Rappel().getWb03RappelNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RAPPEL
            //           TO B03-RAPPEL
            ws.getBilaTrchEstr().getB03Rappel().setB03Rappel(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03Rappel().getWb03Rappel(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-PATTUITO-INI-NULL = HIGH-VALUES
        //              TO B03-PRE-PATTUITO-INI-NULL
        //           ELSE
        //              TO B03-PRE-PATTUITO-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrePattuitoIni().getWb03PrePattuitoIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PATTUITO-INI-NULL
            //           TO B03-PRE-PATTUITO-INI-NULL
            ws.getBilaTrchEstr().getB03PrePattuitoIni().setB03PrePattuitoIniNull(areaOut.getLccvb031().getDati().getWb03PrePattuitoIni().getWb03PrePattuitoIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PATTUITO-INI
            //           TO B03-PRE-PATTUITO-INI
            ws.getBilaTrchEstr().getB03PrePattuitoIni().setB03PrePattuitoIni(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrePattuitoIni().getWb03PrePattuitoIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-DOV-INI-NULL = HIGH-VALUES
        //              TO B03-PRE-DOV-INI-NULL
        //           ELSE
        //              TO B03-PRE-DOV-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreDovIni().getWb03PreDovIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-DOV-INI-NULL
            //           TO B03-PRE-DOV-INI-NULL
            ws.getBilaTrchEstr().getB03PreDovIni().setB03PreDovIniNull(areaOut.getLccvb031().getDati().getWb03PreDovIni().getWb03PreDovIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-DOV-INI
            //           TO B03-PRE-DOV-INI
            ws.getBilaTrchEstr().getB03PreDovIni().setB03PreDovIni(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreDovIni().getWb03PreDovIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
        //              TO B03-PRE-DOV-RIVTO-T-NULL
        //           ELSE
        //              TO B03-PRE-DOV-RIVTO-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreDovRivtoT().getWb03PreDovRivtoTNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-DOV-RIVTO-T-NULL
            //           TO B03-PRE-DOV-RIVTO-T-NULL
            ws.getBilaTrchEstr().getB03PreDovRivtoT().setB03PreDovRivtoTNull(areaOut.getLccvb031().getDati().getWb03PreDovRivtoT().getWb03PreDovRivtoTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-DOV-RIVTO-T
            //           TO B03-PRE-DOV-RIVTO-T
            ws.getBilaTrchEstr().getB03PreDovRivtoT().setB03PreDovRivtoT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreDovRivtoT().getWb03PreDovRivtoT(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
        //              TO B03-PRE-ANNUALIZ-RICOR-NULL
        //           ELSE
        //              TO B03-PRE-ANNUALIZ-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreAnnualizRicor().getWb03PreAnnualizRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-ANNUALIZ-RICOR-NULL
            //           TO B03-PRE-ANNUALIZ-RICOR-NULL
            ws.getBilaTrchEstr().getB03PreAnnualizRicor().setB03PreAnnualizRicorNull(areaOut.getLccvb031().getDati().getWb03PreAnnualizRicor().getWb03PreAnnualizRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-ANNUALIZ-RICOR
            //           TO B03-PRE-ANNUALIZ-RICOR
            ws.getBilaTrchEstr().getB03PreAnnualizRicor().setB03PreAnnualizRicor(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreAnnualizRicor().getWb03PreAnnualizRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-CONT-NULL = HIGH-VALUES
        //              TO B03-PRE-CONT-NULL
        //           ELSE
        //              TO B03-PRE-CONT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreCont().getWb03PreContNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-CONT-NULL
            //           TO B03-PRE-CONT-NULL
            ws.getBilaTrchEstr().getB03PreCont().setB03PreContNull(areaOut.getLccvb031().getDati().getWb03PreCont().getWb03PreContNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-CONT
            //           TO B03-PRE-CONT
            ws.getBilaTrchEstr().getB03PreCont().setB03PreCont(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreCont().getWb03PreCont(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-PP-INI-NULL = HIGH-VALUES
        //              TO B03-PRE-PP-INI-NULL
        //           ELSE
        //              TO B03-PRE-PP-INI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrePpIni().getWb03PrePpIniNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PP-INI-NULL
            //           TO B03-PRE-PP-INI-NULL
            ws.getBilaTrchEstr().getB03PrePpIni().setB03PrePpIniNull(areaOut.getLccvb031().getDati().getWb03PrePpIni().getWb03PrePpIniNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PP-INI
            //           TO B03-PRE-PP-INI
            ws.getBilaTrchEstr().getB03PrePpIni().setB03PrePpIni(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrePpIni().getWb03PrePpIni(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-PURA-T-NULL = HIGH-VALUES
        //              TO B03-RIS-PURA-T-NULL
        //           ELSE
        //              TO B03-RIS-PURA-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisPuraT().getWb03RisPuraTNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-PURA-T-NULL
            //           TO B03-RIS-PURA-T-NULL
            ws.getBilaTrchEstr().getB03RisPuraT().setB03RisPuraTNull(areaOut.getLccvb031().getDati().getWb03RisPuraT().getWb03RisPuraTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-PURA-T
            //           TO B03-RIS-PURA-T
            ws.getBilaTrchEstr().getB03RisPuraT().setB03RisPuraT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisPuraT().getWb03RisPuraT(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-ACQ-NULL = HIGH-VALUES
        //              TO B03-PROV-ACQ-NULL
        //           ELSE
        //              TO B03-PROV-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03ProvAcq().getWb03ProvAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-ACQ-NULL
            //           TO B03-PROV-ACQ-NULL
            ws.getBilaTrchEstr().getB03ProvAcq().setB03ProvAcqNull(areaOut.getLccvb031().getDati().getWb03ProvAcq().getWb03ProvAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-ACQ
            //           TO B03-PROV-ACQ
            ws.getBilaTrchEstr().getB03ProvAcq().setB03ProvAcq(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03ProvAcq().getWb03ProvAcq(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-ACQ-RICOR-NULL = HIGH-VALUES
        //              TO B03-PROV-ACQ-RICOR-NULL
        //           ELSE
        //              TO B03-PROV-ACQ-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03ProvAcqRicor().getWb03ProvAcqRicorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-ACQ-RICOR-NULL
            //           TO B03-PROV-ACQ-RICOR-NULL
            ws.getBilaTrchEstr().getB03ProvAcqRicor().setB03ProvAcqRicorNull(areaOut.getLccvb031().getDati().getWb03ProvAcqRicor().getWb03ProvAcqRicorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-ACQ-RICOR
            //           TO B03-PROV-ACQ-RICOR
            ws.getBilaTrchEstr().getB03ProvAcqRicor().setB03ProvAcqRicor(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03ProvAcqRicor().getWb03ProvAcqRicor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PROV-INC-NULL = HIGH-VALUES
        //              TO B03-PROV-INC-NULL
        //           ELSE
        //              TO B03-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03ProvInc().getWb03ProvIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-PROV-INC-NULL
            //           TO B03-PROV-INC-NULL
            ws.getBilaTrchEstr().getB03ProvInc().setB03ProvIncNull(areaOut.getLccvb031().getDati().getWb03ProvInc().getWb03ProvIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PROV-INC
            //           TO B03-PROV-INC
            ws.getBilaTrchEstr().getB03ProvInc().setB03ProvInc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03ProvInc().getWb03ProvInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
        //              TO B03-CAR-ACQ-NON-SCON-NULL
        //           ELSE
        //              TO B03-CAR-ACQ-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CarAcqNonScon().getWb03CarAcqNonSconNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-ACQ-NON-SCON-NULL
            //           TO B03-CAR-ACQ-NON-SCON-NULL
            ws.getBilaTrchEstr().getB03CarAcqNonScon().setB03CarAcqNonSconNull(areaOut.getLccvb031().getDati().getWb03CarAcqNonScon().getWb03CarAcqNonSconNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-ACQ-NON-SCON
            //           TO B03-CAR-ACQ-NON-SCON
            ws.getBilaTrchEstr().getB03CarAcqNonScon().setB03CarAcqNonScon(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CarAcqNonScon().getWb03CarAcqNonScon(), 15, 3));
        }
        // COB_CODE: IF (SF)-OVER-COMM-NULL = HIGH-VALUES
        //              TO B03-OVER-COMM-NULL
        //           ELSE
        //              TO B03-OVER-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03OverComm().getWb03OverCommNullFormatted())) {
            // COB_CODE: MOVE (SF)-OVER-COMM-NULL
            //           TO B03-OVER-COMM-NULL
            ws.getBilaTrchEstr().getB03OverComm().setB03OverCommNull(areaOut.getLccvb031().getDati().getWb03OverComm().getWb03OverCommNull());
        }
        else {
            // COB_CODE: MOVE (SF)-OVER-COMM
            //           TO B03-OVER-COMM
            ws.getBilaTrchEstr().getB03OverComm().setB03OverComm(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03OverComm().getWb03OverComm(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
        //              TO B03-CAR-ACQ-PRECONTATO-NULL
        //           ELSE
        //              TO B03-CAR-ACQ-PRECONTATO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CarAcqPrecontato().getWb03CarAcqPrecontatoNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-ACQ-PRECONTATO-NULL
            //           TO B03-CAR-ACQ-PRECONTATO-NULL
            ws.getBilaTrchEstr().getB03CarAcqPrecontato().setB03CarAcqPrecontatoNull(areaOut.getLccvb031().getDati().getWb03CarAcqPrecontato().getWb03CarAcqPrecontatoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-ACQ-PRECONTATO
            //           TO B03-CAR-ACQ-PRECONTATO
            ws.getBilaTrchEstr().getB03CarAcqPrecontato().setB03CarAcqPrecontato(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CarAcqPrecontato().getWb03CarAcqPrecontato(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-ACQ-T-NULL = HIGH-VALUES
        //              TO B03-RIS-ACQ-T-NULL
        //           ELSE
        //              TO B03-RIS-ACQ-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisAcqT().getWb03RisAcqTNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-ACQ-T-NULL
            //           TO B03-RIS-ACQ-T-NULL
            ws.getBilaTrchEstr().getB03RisAcqT().setB03RisAcqTNull(areaOut.getLccvb031().getDati().getWb03RisAcqT().getWb03RisAcqTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-ACQ-T
            //           TO B03-RIS-ACQ-T
            ws.getBilaTrchEstr().getB03RisAcqT().setB03RisAcqT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisAcqT().getWb03RisAcqT(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-ZIL-T-NULL = HIGH-VALUES
        //              TO B03-RIS-ZIL-T-NULL
        //           ELSE
        //              TO B03-RIS-ZIL-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisZilT().getWb03RisZilTNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-ZIL-T-NULL
            //           TO B03-RIS-ZIL-T-NULL
            ws.getBilaTrchEstr().getB03RisZilT().setB03RisZilTNull(areaOut.getLccvb031().getDati().getWb03RisZilT().getWb03RisZilTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-ZIL-T
            //           TO B03-RIS-ZIL-T
            ws.getBilaTrchEstr().getB03RisZilT().setB03RisZilT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisZilT().getWb03RisZilT(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
        //              TO B03-CAR-GEST-NON-SCON-NULL
        //           ELSE
        //              TO B03-CAR-GEST-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CarGestNonScon().getWb03CarGestNonSconNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-GEST-NON-SCON-NULL
            //           TO B03-CAR-GEST-NON-SCON-NULL
            ws.getBilaTrchEstr().getB03CarGestNonScon().setB03CarGestNonSconNull(areaOut.getLccvb031().getDati().getWb03CarGestNonScon().getWb03CarGestNonSconNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-GEST-NON-SCON
            //           TO B03-CAR-GEST-NON-SCON
            ws.getBilaTrchEstr().getB03CarGestNonScon().setB03CarGestNonScon(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CarGestNonScon().getWb03CarGestNonScon(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-GEST-NULL = HIGH-VALUES
        //              TO B03-CAR-GEST-NULL
        //           ELSE
        //              TO B03-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CarGest().getWb03CarGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-GEST-NULL
            //           TO B03-CAR-GEST-NULL
            ws.getBilaTrchEstr().getB03CarGest().setB03CarGestNull(areaOut.getLccvb031().getDati().getWb03CarGest().getWb03CarGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-GEST
            //           TO B03-CAR-GEST
            ws.getBilaTrchEstr().getB03CarGest().setB03CarGest(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CarGest().getWb03CarGest(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-SPE-T-NULL = HIGH-VALUES
        //              TO B03-RIS-SPE-T-NULL
        //           ELSE
        //              TO B03-RIS-SPE-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisSpeT().getWb03RisSpeTNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-SPE-T-NULL
            //           TO B03-RIS-SPE-T-NULL
            ws.getBilaTrchEstr().getB03RisSpeT().setB03RisSpeTNull(areaOut.getLccvb031().getDati().getWb03RisSpeT().getWb03RisSpeTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-SPE-T
            //           TO B03-RIS-SPE-T
            ws.getBilaTrchEstr().getB03RisSpeT().setB03RisSpeT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisSpeT().getWb03RisSpeT(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-INC-NON-SCON-NULL = HIGH-VALUES
        //              TO B03-CAR-INC-NON-SCON-NULL
        //           ELSE
        //              TO B03-CAR-INC-NON-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CarIncNonScon().getWb03CarIncNonSconNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-INC-NON-SCON-NULL
            //           TO B03-CAR-INC-NON-SCON-NULL
            ws.getBilaTrchEstr().getB03CarIncNonScon().setB03CarIncNonSconNull(areaOut.getLccvb031().getDati().getWb03CarIncNonScon().getWb03CarIncNonSconNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-INC-NON-SCON
            //           TO B03-CAR-INC-NON-SCON
            ws.getBilaTrchEstr().getB03CarIncNonScon().setB03CarIncNonScon(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CarIncNonScon().getWb03CarIncNonScon(), 15, 3));
        }
        // COB_CODE: IF (SF)-CAR-INC-NULL = HIGH-VALUES
        //              TO B03-CAR-INC-NULL
        //           ELSE
        //              TO B03-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CarInc().getWb03CarIncNullFormatted())) {
            // COB_CODE: MOVE (SF)-CAR-INC-NULL
            //           TO B03-CAR-INC-NULL
            ws.getBilaTrchEstr().getB03CarInc().setB03CarIncNull(areaOut.getLccvb031().getDati().getWb03CarInc().getWb03CarIncNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CAR-INC
            //           TO B03-CAR-INC
            ws.getBilaTrchEstr().getB03CarInc().setB03CarInc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CarInc().getWb03CarInc(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
        //              TO B03-RIS-RISTORNI-CAP-NULL
        //           ELSE
        //              TO B03-RIS-RISTORNI-CAP
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisRistorniCap().getWb03RisRistorniCapNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-RISTORNI-CAP-NULL
            //           TO B03-RIS-RISTORNI-CAP-NULL
            ws.getBilaTrchEstr().getB03RisRistorniCap().setB03RisRistorniCapNull(areaOut.getLccvb031().getDati().getWb03RisRistorniCap().getWb03RisRistorniCapNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-RISTORNI-CAP
            //           TO B03-RIS-RISTORNI-CAP
            ws.getBilaTrchEstr().getB03RisRistorniCap().setB03RisRistorniCap(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisRistorniCap().getWb03RisRistorniCap(), 15, 3));
        }
        // COB_CODE: IF (SF)-INTR-TECN-NULL = HIGH-VALUES
        //              TO B03-INTR-TECN-NULL
        //           ELSE
        //              TO B03-INTR-TECN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03IntrTecn().getWb03IntrTecnNullFormatted())) {
            // COB_CODE: MOVE (SF)-INTR-TECN-NULL
            //           TO B03-INTR-TECN-NULL
            ws.getBilaTrchEstr().getB03IntrTecn().setB03IntrTecnNull(areaOut.getLccvb031().getDati().getWb03IntrTecn().getWb03IntrTecnNull());
        }
        else {
            // COB_CODE: MOVE (SF)-INTR-TECN
            //           TO B03-INTR-TECN
            ws.getBilaTrchEstr().getB03IntrTecn().setB03IntrTecn(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03IntrTecn().getWb03IntrTecn(), 6, 3));
        }
        // COB_CODE: IF (SF)-CPT-RSH-MOR-NULL = HIGH-VALUES
        //              TO B03-CPT-RSH-MOR-NULL
        //           ELSE
        //              TO B03-CPT-RSH-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CptRshMor().getWb03CptRshMorNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-RSH-MOR-NULL
            //           TO B03-CPT-RSH-MOR-NULL
            ws.getBilaTrchEstr().getB03CptRshMor().setB03CptRshMorNull(areaOut.getLccvb031().getDati().getWb03CptRshMor().getWb03CptRshMorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-RSH-MOR
            //           TO B03-CPT-RSH-MOR
            ws.getBilaTrchEstr().getB03CptRshMor().setB03CptRshMor(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CptRshMor().getWb03CptRshMor(), 15, 3));
        }
        // COB_CODE: IF (SF)-C-SUBRSH-T-NULL = HIGH-VALUES
        //              TO B03-C-SUBRSH-T-NULL
        //           ELSE
        //              TO B03-C-SUBRSH-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CSubrshT().getWb03CSubrshTNullFormatted())) {
            // COB_CODE: MOVE (SF)-C-SUBRSH-T-NULL
            //           TO B03-C-SUBRSH-T-NULL
            ws.getBilaTrchEstr().getB03CSubrshT().setB03CSubrshTNull(areaOut.getLccvb031().getDati().getWb03CSubrshT().getWb03CSubrshTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-C-SUBRSH-T
            //           TO B03-C-SUBRSH-T
            ws.getBilaTrchEstr().getB03CSubrshT().setB03CSubrshT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CSubrshT().getWb03CSubrshT(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-RSH-T-NULL = HIGH-VALUES
        //              TO B03-PRE-RSH-T-NULL
        //           ELSE
        //              TO B03-PRE-RSH-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreRshT().getWb03PreRshTNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-RSH-T-NULL
            //           TO B03-PRE-RSH-T-NULL
            ws.getBilaTrchEstr().getB03PreRshT().setB03PreRshTNull(areaOut.getLccvb031().getDati().getWb03PreRshT().getWb03PreRshTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-RSH-T
            //           TO B03-PRE-RSH-T
            ws.getBilaTrchEstr().getB03PreRshT().setB03PreRshT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreRshT().getWb03PreRshT(), 15, 3));
        }
        // COB_CODE: IF (SF)-ALQ-MARG-RIS-NULL = HIGH-VALUES
        //              TO B03-ALQ-MARG-RIS-NULL
        //           ELSE
        //              TO B03-ALQ-MARG-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AlqMargRis().getWb03AlqMargRisNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-MARG-RIS-NULL
            //           TO B03-ALQ-MARG-RIS-NULL
            ws.getBilaTrchEstr().getB03AlqMargRis().setB03AlqMargRisNull(areaOut.getLccvb031().getDati().getWb03AlqMargRis().getWb03AlqMargRisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-MARG-RIS
            //           TO B03-ALQ-MARG-RIS
            ws.getBilaTrchEstr().getB03AlqMargRis().setB03AlqMargRis(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03AlqMargRis().getWb03AlqMargRis(), 6, 3));
        }
        // COB_CODE: IF (SF)-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
        //              TO B03-ALQ-MARG-C-SUBRSH-NULL
        //           ELSE
        //              TO B03-ALQ-MARG-C-SUBRSH
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AlqMargCSubrsh().getWb03AlqMargCSubrshNullFormatted())) {
            // COB_CODE: MOVE (SF)-ALQ-MARG-C-SUBRSH-NULL
            //           TO B03-ALQ-MARG-C-SUBRSH-NULL
            ws.getBilaTrchEstr().getB03AlqMargCSubrsh().setB03AlqMargCSubrshNull(areaOut.getLccvb031().getDati().getWb03AlqMargCSubrsh().getWb03AlqMargCSubrshNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ALQ-MARG-C-SUBRSH
            //           TO B03-ALQ-MARG-C-SUBRSH
            ws.getBilaTrchEstr().getB03AlqMargCSubrsh().setB03AlqMargCSubrsh(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03AlqMargCSubrsh().getWb03AlqMargCSubrsh(), 6, 3));
        }
        // COB_CODE: IF (SF)-TS-RENDTO-SPPR-NULL = HIGH-VALUES
        //              TO B03-TS-RENDTO-SPPR-NULL
        //           ELSE
        //              TO B03-TS-RENDTO-SPPR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsRendtoSppr().getWb03TsRendtoSpprNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-RENDTO-SPPR-NULL
            //           TO B03-TS-RENDTO-SPPR-NULL
            ws.getBilaTrchEstr().getB03TsRendtoSppr().setB03TsRendtoSpprNull(areaOut.getLccvb031().getDati().getWb03TsRendtoSppr().getWb03TsRendtoSpprNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-RENDTO-SPPR
            //           TO B03-TS-RENDTO-SPPR
            ws.getBilaTrchEstr().getB03TsRendtoSppr().setB03TsRendtoSppr(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsRendtoSppr().getWb03TsRendtoSppr(), 14, 9));
        }
        // COB_CODE: IF (SF)-TP-IAS-NULL = HIGH-VALUES
        //              TO B03-TP-IAS-NULL
        //           ELSE
        //              TO B03-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpIasFormatted())) {
            // COB_CODE: MOVE (SF)-TP-IAS-NULL
            //           TO B03-TP-IAS-NULL
            ws.getBilaTrchEstr().setB03TpIas(areaOut.getLccvb031().getDati().getWb03TpIas());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-IAS
            //           TO B03-TP-IAS
            ws.getBilaTrchEstr().setB03TpIas(areaOut.getLccvb031().getDati().getWb03TpIas());
        }
        // COB_CODE: IF (SF)-NS-QUO-NULL = HIGH-VALUES
        //              TO B03-NS-QUO-NULL
        //           ELSE
        //              TO B03-NS-QUO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03NsQuo().getWb03NsQuoNullFormatted())) {
            // COB_CODE: MOVE (SF)-NS-QUO-NULL
            //           TO B03-NS-QUO-NULL
            ws.getBilaTrchEstr().getB03NsQuo().setB03NsQuoNull(areaOut.getLccvb031().getDati().getWb03NsQuo().getWb03NsQuoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-NS-QUO
            //           TO B03-NS-QUO
            ws.getBilaTrchEstr().getB03NsQuo().setB03NsQuo(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03NsQuo().getWb03NsQuo(), 6, 3));
        }
        // COB_CODE: IF (SF)-TS-MEDIO-NULL = HIGH-VALUES
        //              TO B03-TS-MEDIO-NULL
        //           ELSE
        //              TO B03-TS-MEDIO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsMedio().getWb03TsMedioNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-MEDIO-NULL
            //           TO B03-TS-MEDIO-NULL
            ws.getBilaTrchEstr().getB03TsMedio().setB03TsMedioNull(areaOut.getLccvb031().getDati().getWb03TsMedio().getWb03TsMedioNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-MEDIO
            //           TO B03-TS-MEDIO
            ws.getBilaTrchEstr().getB03TsMedio().setB03TsMedio(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsMedio().getWb03TsMedio(), 14, 9));
        }
        // COB_CODE: IF (SF)-CPT-RIASTO-NULL = HIGH-VALUES
        //              TO B03-CPT-RIASTO-NULL
        //           ELSE
        //              TO B03-CPT-RIASTO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CptRiasto().getWb03CptRiastoNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-RIASTO-NULL
            //           TO B03-CPT-RIASTO-NULL
            ws.getBilaTrchEstr().getB03CptRiasto().setB03CptRiastoNull(areaOut.getLccvb031().getDati().getWb03CptRiasto().getWb03CptRiastoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-RIASTO
            //           TO B03-CPT-RIASTO
            ws.getBilaTrchEstr().getB03CptRiasto().setB03CptRiasto(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CptRiasto().getWb03CptRiasto(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-RIASTO-NULL = HIGH-VALUES
        //              TO B03-PRE-RIASTO-NULL
        //           ELSE
        //              TO B03-PRE-RIASTO
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreRiasto().getWb03PreRiastoNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-RIASTO-NULL
            //           TO B03-PRE-RIASTO-NULL
            ws.getBilaTrchEstr().getB03PreRiasto().setB03PreRiastoNull(areaOut.getLccvb031().getDati().getWb03PreRiasto().getWb03PreRiastoNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-RIASTO
            //           TO B03-PRE-RIASTO
            ws.getBilaTrchEstr().getB03PreRiasto().setB03PreRiasto(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreRiasto().getWb03PreRiasto(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-RIASTA-NULL = HIGH-VALUES
        //              TO B03-RIS-RIASTA-NULL
        //           ELSE
        //              TO B03-RIS-RIASTA
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisRiasta().getWb03RisRiastaNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-RIASTA-NULL
            //           TO B03-RIS-RIASTA-NULL
            ws.getBilaTrchEstr().getB03RisRiasta().setB03RisRiastaNull(areaOut.getLccvb031().getDati().getWb03RisRiasta().getWb03RisRiastaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-RIASTA
            //           TO B03-RIS-RIASTA
            ws.getBilaTrchEstr().getB03RisRiasta().setB03RisRiasta(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisRiasta().getWb03RisRiasta(), 15, 3));
        }
        // COB_CODE: IF (SF)-CPT-RIASTO-ECC-NULL = HIGH-VALUES
        //              TO B03-CPT-RIASTO-ECC-NULL
        //           ELSE
        //              TO B03-CPT-RIASTO-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CptRiastoEcc().getWb03CptRiastoEccNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-RIASTO-ECC-NULL
            //           TO B03-CPT-RIASTO-ECC-NULL
            ws.getBilaTrchEstr().getB03CptRiastoEcc().setB03CptRiastoEccNull(areaOut.getLccvb031().getDati().getWb03CptRiastoEcc().getWb03CptRiastoEccNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-RIASTO-ECC
            //           TO B03-CPT-RIASTO-ECC
            ws.getBilaTrchEstr().getB03CptRiastoEcc().setB03CptRiastoEcc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CptRiastoEcc().getWb03CptRiastoEcc(), 15, 3));
        }
        // COB_CODE: IF (SF)-PRE-RIASTO-ECC-NULL = HIGH-VALUES
        //              TO B03-PRE-RIASTO-ECC-NULL
        //           ELSE
        //              TO B03-PRE-RIASTO-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PreRiastoEcc().getWb03PreRiastoEccNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-RIASTO-ECC-NULL
            //           TO B03-PRE-RIASTO-ECC-NULL
            ws.getBilaTrchEstr().getB03PreRiastoEcc().setB03PreRiastoEccNull(areaOut.getLccvb031().getDati().getWb03PreRiastoEcc().getWb03PreRiastoEccNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-RIASTO-ECC
            //           TO B03-PRE-RIASTO-ECC
            ws.getBilaTrchEstr().getB03PreRiastoEcc().setB03PreRiastoEcc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PreRiastoEcc().getWb03PreRiastoEcc(), 15, 3));
        }
        // COB_CODE: IF (SF)-RIS-RIASTA-ECC-NULL = HIGH-VALUES
        //              TO B03-RIS-RIASTA-ECC-NULL
        //           ELSE
        //              TO B03-RIS-RIASTA-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisRiastaEcc().getWb03RisRiastaEccNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-RIASTA-ECC-NULL
            //           TO B03-RIS-RIASTA-ECC-NULL
            ws.getBilaTrchEstr().getB03RisRiastaEcc().setB03RisRiastaEccNull(areaOut.getLccvb031().getDati().getWb03RisRiastaEcc().getWb03RisRiastaEccNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-RIASTA-ECC
            //           TO B03-RIS-RIASTA-ECC
            ws.getBilaTrchEstr().getB03RisRiastaEcc().setB03RisRiastaEcc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisRiastaEcc().getWb03RisRiastaEcc(), 15, 3));
        }
        // COB_CODE: IF (SF)-COD-AGE-NULL = HIGH-VALUES
        //              TO B03-COD-AGE-NULL
        //           ELSE
        //              TO B03-COD-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodAge().getWb03CodAgeNullFormatted())) {
            // COB_CODE: MOVE (SF)-COD-AGE-NULL
            //           TO B03-COD-AGE-NULL
            ws.getBilaTrchEstr().getB03CodAge().setB03CodAgeNull(areaOut.getLccvb031().getDati().getWb03CodAge().getWb03CodAgeNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-AGE
            //           TO B03-COD-AGE
            ws.getBilaTrchEstr().getB03CodAge().setB03CodAge(areaOut.getLccvb031().getDati().getWb03CodAge().getWb03CodAge());
        }
        // COB_CODE: IF (SF)-COD-SUBAGE-NULL = HIGH-VALUES
        //              TO B03-COD-SUBAGE-NULL
        //           ELSE
        //              TO B03-COD-SUBAGE
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodSubage().getWb03CodSubageNullFormatted())) {
            // COB_CODE: MOVE (SF)-COD-SUBAGE-NULL
            //           TO B03-COD-SUBAGE-NULL
            ws.getBilaTrchEstr().getB03CodSubage().setB03CodSubageNull(areaOut.getLccvb031().getDati().getWb03CodSubage().getWb03CodSubageNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-SUBAGE
            //           TO B03-COD-SUBAGE
            ws.getBilaTrchEstr().getB03CodSubage().setB03CodSubage(areaOut.getLccvb031().getDati().getWb03CodSubage().getWb03CodSubage());
        }
        // COB_CODE: IF (SF)-COD-CAN-NULL = HIGH-VALUES
        //              TO B03-COD-CAN-NULL
        //           ELSE
        //              TO B03-COD-CAN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodCan().getWb03CodCanNullFormatted())) {
            // COB_CODE: MOVE (SF)-COD-CAN-NULL
            //           TO B03-COD-CAN-NULL
            ws.getBilaTrchEstr().getB03CodCan().setB03CodCanNull(areaOut.getLccvb031().getDati().getWb03CodCan().getWb03CodCanNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-CAN
            //           TO B03-COD-CAN
            ws.getBilaTrchEstr().getB03CodCan().setB03CodCan(areaOut.getLccvb031().getDati().getWb03CodCan().getWb03CodCan());
        }
        // COB_CODE: IF (SF)-IB-POLI-NULL = HIGH-VALUES
        //              TO B03-IB-POLI-NULL
        //           ELSE
        //              TO B03-IB-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03IbPoli(), Wb03Dati.Len.WB03_IB_POLI)) {
            // COB_CODE: MOVE (SF)-IB-POLI-NULL
            //           TO B03-IB-POLI-NULL
            ws.getBilaTrchEstr().setB03IbPoli(areaOut.getLccvb031().getDati().getWb03IbPoli());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-POLI
            //           TO B03-IB-POLI
            ws.getBilaTrchEstr().setB03IbPoli(areaOut.getLccvb031().getDati().getWb03IbPoli());
        }
        // COB_CODE: IF (SF)-IB-ADES-NULL = HIGH-VALUES
        //              TO B03-IB-ADES-NULL
        //           ELSE
        //              TO B03-IB-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03IbAdes(), Wb03Dati.Len.WB03_IB_ADES)) {
            // COB_CODE: MOVE (SF)-IB-ADES-NULL
            //           TO B03-IB-ADES-NULL
            ws.getBilaTrchEstr().setB03IbAdes(areaOut.getLccvb031().getDati().getWb03IbAdes());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-ADES
            //           TO B03-IB-ADES
            ws.getBilaTrchEstr().setB03IbAdes(areaOut.getLccvb031().getDati().getWb03IbAdes());
        }
        // COB_CODE: IF (SF)-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
        //              TO B03-IB-TRCH-DI-GAR-NULL
        //           ELSE
        //              TO B03-IB-TRCH-DI-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03IbTrchDiGar(), Wb03Dati.Len.WB03_IB_TRCH_DI_GAR)) {
            // COB_CODE: MOVE (SF)-IB-TRCH-DI-GAR-NULL
            //           TO B03-IB-TRCH-DI-GAR-NULL
            ws.getBilaTrchEstr().setB03IbTrchDiGar(areaOut.getLccvb031().getDati().getWb03IbTrchDiGar());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-TRCH-DI-GAR
            //           TO B03-IB-TRCH-DI-GAR
            ws.getBilaTrchEstr().setB03IbTrchDiGar(areaOut.getLccvb031().getDati().getWb03IbTrchDiGar());
        }
        // COB_CODE: IF (SF)-TP-PRSTZ-NULL = HIGH-VALUES
        //              TO B03-TP-PRSTZ-NULL
        //           ELSE
        //              TO B03-TP-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpPrstzFormatted())) {
            // COB_CODE: MOVE (SF)-TP-PRSTZ-NULL
            //           TO B03-TP-PRSTZ-NULL
            ws.getBilaTrchEstr().setB03TpPrstz(areaOut.getLccvb031().getDati().getWb03TpPrstz());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-PRSTZ
            //           TO B03-TP-PRSTZ
            ws.getBilaTrchEstr().setB03TpPrstz(areaOut.getLccvb031().getDati().getWb03TpPrstz());
        }
        // COB_CODE: IF (SF)-TP-TRASF-NULL = HIGH-VALUES
        //              TO B03-TP-TRASF-NULL
        //           ELSE
        //              TO B03-TP-TRASF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpTrasfFormatted())) {
            // COB_CODE: MOVE (SF)-TP-TRASF-NULL
            //           TO B03-TP-TRASF-NULL
            ws.getBilaTrchEstr().setB03TpTrasf(areaOut.getLccvb031().getDati().getWb03TpTrasf());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-TRASF
            //           TO B03-TP-TRASF
            ws.getBilaTrchEstr().setB03TpTrasf(areaOut.getLccvb031().getDati().getWb03TpTrasf());
        }
        // COB_CODE: IF (SF)-PP-INVRIO-TARI-NULL = HIGH-VALUES
        //              TO B03-PP-INVRIO-TARI-NULL
        //           ELSE
        //              TO B03-PP-INVRIO-TARI
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03PpInvrioTari(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-PP-INVRIO-TARI-NULL
            //           TO B03-PP-INVRIO-TARI-NULL
            ws.getBilaTrchEstr().setB03PpInvrioTari(areaOut.getLccvb031().getDati().getWb03PpInvrioTari());
        }
        else {
            // COB_CODE: MOVE (SF)-PP-INVRIO-TARI
            //           TO B03-PP-INVRIO-TARI
            ws.getBilaTrchEstr().setB03PpInvrioTari(areaOut.getLccvb031().getDati().getWb03PpInvrioTari());
        }
        // COB_CODE: IF (SF)-COEFF-OPZ-REN-NULL = HIGH-VALUES
        //              TO B03-COEFF-OPZ-REN-NULL
        //           ELSE
        //              TO B03-COEFF-OPZ-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CoeffOpzRen().getWb03CoeffOpzRenNullFormatted())) {
            // COB_CODE: MOVE (SF)-COEFF-OPZ-REN-NULL
            //           TO B03-COEFF-OPZ-REN-NULL
            ws.getBilaTrchEstr().getB03CoeffOpzRen().setB03CoeffOpzRenNull(areaOut.getLccvb031().getDati().getWb03CoeffOpzRen().getWb03CoeffOpzRenNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COEFF-OPZ-REN
            //           TO B03-COEFF-OPZ-REN
            ws.getBilaTrchEstr().getB03CoeffOpzRen().setB03CoeffOpzRen(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CoeffOpzRen().getWb03CoeffOpzRen(), 6, 3));
        }
        // COB_CODE: IF (SF)-COEFF-OPZ-CPT-NULL = HIGH-VALUES
        //              TO B03-COEFF-OPZ-CPT-NULL
        //           ELSE
        //              TO B03-COEFF-OPZ-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CoeffOpzCpt().getWb03CoeffOpzCptNullFormatted())) {
            // COB_CODE: MOVE (SF)-COEFF-OPZ-CPT-NULL
            //           TO B03-COEFF-OPZ-CPT-NULL
            ws.getBilaTrchEstr().getB03CoeffOpzCpt().setB03CoeffOpzCptNull(areaOut.getLccvb031().getDati().getWb03CoeffOpzCpt().getWb03CoeffOpzCptNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COEFF-OPZ-CPT
            //           TO B03-COEFF-OPZ-CPT
            ws.getBilaTrchEstr().getB03CoeffOpzCpt().setB03CoeffOpzCpt(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CoeffOpzCpt().getWb03CoeffOpzCpt(), 6, 3));
        }
        // COB_CODE: IF (SF)-DUR-PAG-REN-NULL = HIGH-VALUES
        //              TO B03-DUR-PAG-REN-NULL
        //           ELSE
        //              TO B03-DUR-PAG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurPagRen().getWb03DurPagRenNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-PAG-REN-NULL
            //           TO B03-DUR-PAG-REN-NULL
            ws.getBilaTrchEstr().getB03DurPagRen().setB03DurPagRenNull(areaOut.getLccvb031().getDati().getWb03DurPagRen().getWb03DurPagRenNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-PAG-REN
            //           TO B03-DUR-PAG-REN
            ws.getBilaTrchEstr().getB03DurPagRen().setB03DurPagRen(areaOut.getLccvb031().getDati().getWb03DurPagRen().getWb03DurPagRen());
        }
        // COB_CODE: IF (SF)-VLT-NULL = HIGH-VALUES
        //              TO B03-VLT-NULL
        //           ELSE
        //              TO B03-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03VltFormatted())) {
            // COB_CODE: MOVE (SF)-VLT-NULL
            //           TO B03-VLT-NULL
            ws.getBilaTrchEstr().setB03Vlt(areaOut.getLccvb031().getDati().getWb03Vlt());
        }
        else {
            // COB_CODE: MOVE (SF)-VLT
            //           TO B03-VLT
            ws.getBilaTrchEstr().setB03Vlt(areaOut.getLccvb031().getDati().getWb03Vlt());
        }
        // COB_CODE: IF (SF)-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
        //              TO B03-RIS-MAT-CHIU-PREC-NULL
        //           ELSE
        //              TO B03-RIS-MAT-CHIU-PREC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RisMatChiuPrec().getWb03RisMatChiuPrecNullFormatted())) {
            // COB_CODE: MOVE (SF)-RIS-MAT-CHIU-PREC-NULL
            //           TO B03-RIS-MAT-CHIU-PREC-NULL
            ws.getBilaTrchEstr().getB03RisMatChiuPrec().setB03RisMatChiuPrecNull(areaOut.getLccvb031().getDati().getWb03RisMatChiuPrec().getWb03RisMatChiuPrecNull());
        }
        else {
            // COB_CODE: MOVE (SF)-RIS-MAT-CHIU-PREC
            //           TO B03-RIS-MAT-CHIU-PREC
            ws.getBilaTrchEstr().getB03RisMatChiuPrec().setB03RisMatChiuPrec(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RisMatChiuPrec().getWb03RisMatChiuPrec(), 15, 3));
        }
        // COB_CODE: IF (SF)-COD-FND-NULL = HIGH-VALUES
        //              TO B03-COD-FND-NULL
        //           ELSE
        //              TO B03-COD-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodFndFormatted())) {
            // COB_CODE: MOVE (SF)-COD-FND-NULL
            //           TO B03-COD-FND-NULL
            ws.getBilaTrchEstr().setB03CodFnd(areaOut.getLccvb031().getDati().getWb03CodFnd());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-FND
            //           TO B03-COD-FND
            ws.getBilaTrchEstr().setB03CodFnd(areaOut.getLccvb031().getDati().getWb03CodFnd());
        }
        // COB_CODE: IF (SF)-PRSTZ-T-NULL = HIGH-VALUES
        //              TO B03-PRSTZ-T-NULL
        //           ELSE
        //              TO B03-PRSTZ-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrstzT().getWb03PrstzTNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRSTZ-T-NULL
            //           TO B03-PRSTZ-T-NULL
            ws.getBilaTrchEstr().getB03PrstzT().setB03PrstzTNull(areaOut.getLccvb031().getDati().getWb03PrstzT().getWb03PrstzTNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRSTZ-T
            //           TO B03-PRSTZ-T
            ws.getBilaTrchEstr().getB03PrstzT().setB03PrstzT(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrstzT().getWb03PrstzT(), 15, 3));
        }
        // COB_CODE: IF (SF)-TS-TARI-DOV-NULL = HIGH-VALUES
        //              TO B03-TS-TARI-DOV-NULL
        //           ELSE
        //              TO B03-TS-TARI-DOV
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsTariDov().getWb03TsTariDovNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-TARI-DOV-NULL
            //           TO B03-TS-TARI-DOV-NULL
            ws.getBilaTrchEstr().getB03TsTariDov().setB03TsTariDovNull(areaOut.getLccvb031().getDati().getWb03TsTariDov().getWb03TsTariDovNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-TARI-DOV
            //           TO B03-TS-TARI-DOV
            ws.getBilaTrchEstr().getB03TsTariDov().setB03TsTariDov(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsTariDov().getWb03TsTariDov(), 14, 9));
        }
        // COB_CODE: IF (SF)-TS-TARI-SCON-NULL = HIGH-VALUES
        //              TO B03-TS-TARI-SCON-NULL
        //           ELSE
        //              TO B03-TS-TARI-SCON
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsTariScon().getWb03TsTariSconNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-TARI-SCON-NULL
            //           TO B03-TS-TARI-SCON-NULL
            ws.getBilaTrchEstr().getB03TsTariScon().setB03TsTariSconNull(areaOut.getLccvb031().getDati().getWb03TsTariScon().getWb03TsTariSconNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-TARI-SCON
            //           TO B03-TS-TARI-SCON
            ws.getBilaTrchEstr().getB03TsTariScon().setB03TsTariScon(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsTariScon().getWb03TsTariScon(), 14, 9));
        }
        // COB_CODE: IF (SF)-TS-PP-NULL = HIGH-VALUES
        //              TO B03-TS-PP-NULL
        //           ELSE
        //              TO B03-TS-PP
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsPp().getWb03TsPpNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-PP-NULL
            //           TO B03-TS-PP-NULL
            ws.getBilaTrchEstr().getB03TsPp().setB03TsPpNull(areaOut.getLccvb031().getDati().getWb03TsPp().getWb03TsPpNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-PP
            //           TO B03-TS-PP
            ws.getBilaTrchEstr().getB03TsPp().setB03TsPp(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsPp().getWb03TsPp(), 14, 9));
        }
        // COB_CODE: IF (SF)-COEFF-RIS-1-T-NULL = HIGH-VALUES
        //              TO B03-COEFF-RIS-1-T-NULL
        //           ELSE
        //              TO B03-COEFF-RIS-1-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CoeffRis1T().getWb03CoeffRis1TNullFormatted())) {
            // COB_CODE: MOVE (SF)-COEFF-RIS-1-T-NULL
            //           TO B03-COEFF-RIS-1-T-NULL
            ws.getBilaTrchEstr().getB03CoeffRis1T().setB03CoeffRis1TNull(areaOut.getLccvb031().getDati().getWb03CoeffRis1T().getWb03CoeffRis1TNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COEFF-RIS-1-T
            //           TO B03-COEFF-RIS-1-T
            ws.getBilaTrchEstr().getB03CoeffRis1T().setB03CoeffRis1T(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CoeffRis1T().getWb03CoeffRis1T(), 14, 9));
        }
        // COB_CODE: IF (SF)-COEFF-RIS-2-T-NULL = HIGH-VALUES
        //              TO B03-COEFF-RIS-2-T-NULL
        //           ELSE
        //              TO B03-COEFF-RIS-2-T
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CoeffRis2T().getWb03CoeffRis2TNullFormatted())) {
            // COB_CODE: MOVE (SF)-COEFF-RIS-2-T-NULL
            //           TO B03-COEFF-RIS-2-T-NULL
            ws.getBilaTrchEstr().getB03CoeffRis2T().setB03CoeffRis2TNull(areaOut.getLccvb031().getDati().getWb03CoeffRis2T().getWb03CoeffRis2TNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COEFF-RIS-2-T
            //           TO B03-COEFF-RIS-2-T
            ws.getBilaTrchEstr().getB03CoeffRis2T().setB03CoeffRis2T(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CoeffRis2T().getWb03CoeffRis2T(), 14, 9));
        }
        // COB_CODE: IF (SF)-ABB-NULL = HIGH-VALUES
        //              TO B03-ABB-NULL
        //           ELSE
        //              TO B03-ABB
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Abb().getWb03AbbNullFormatted())) {
            // COB_CODE: MOVE (SF)-ABB-NULL
            //           TO B03-ABB-NULL
            ws.getBilaTrchEstr().getB03Abb().setB03AbbNull(areaOut.getLccvb031().getDati().getWb03Abb().getWb03AbbNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ABB
            //           TO B03-ABB
            ws.getBilaTrchEstr().getB03Abb().setB03Abb(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03Abb().getWb03Abb(), 15, 3));
        }
        // COB_CODE: IF (SF)-TP-COASS-NULL = HIGH-VALUES
        //              TO B03-TP-COASS-NULL
        //           ELSE
        //              TO B03-TP-COASS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpCoassFormatted())) {
            // COB_CODE: MOVE (SF)-TP-COASS-NULL
            //           TO B03-TP-COASS-NULL
            ws.getBilaTrchEstr().setB03TpCoass(areaOut.getLccvb031().getDati().getWb03TpCoass());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-COASS
            //           TO B03-TP-COASS
            ws.getBilaTrchEstr().setB03TpCoass(areaOut.getLccvb031().getDati().getWb03TpCoass());
        }
        // COB_CODE: IF (SF)-TRAT-RIASS-NULL = HIGH-VALUES
        //              TO B03-TRAT-RIASS-NULL
        //           ELSE
        //              TO B03-TRAT-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TratRiassFormatted())) {
            // COB_CODE: MOVE (SF)-TRAT-RIASS-NULL
            //           TO B03-TRAT-RIASS-NULL
            ws.getBilaTrchEstr().setB03TratRiass(areaOut.getLccvb031().getDati().getWb03TratRiass());
        }
        else {
            // COB_CODE: MOVE (SF)-TRAT-RIASS
            //           TO B03-TRAT-RIASS
            ws.getBilaTrchEstr().setB03TratRiass(areaOut.getLccvb031().getDati().getWb03TratRiass());
        }
        // COB_CODE: IF (SF)-TRAT-RIASS-ECC-NULL = HIGH-VALUES
        //              TO B03-TRAT-RIASS-ECC-NULL
        //           ELSE
        //              TO B03-TRAT-RIASS-ECC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TratRiassEccFormatted())) {
            // COB_CODE: MOVE (SF)-TRAT-RIASS-ECC-NULL
            //           TO B03-TRAT-RIASS-ECC-NULL
            ws.getBilaTrchEstr().setB03TratRiassEcc(areaOut.getLccvb031().getDati().getWb03TratRiassEcc());
        }
        else {
            // COB_CODE: MOVE (SF)-TRAT-RIASS-ECC
            //           TO B03-TRAT-RIASS-ECC
            ws.getBilaTrchEstr().setB03TratRiassEcc(areaOut.getLccvb031().getDati().getWb03TratRiassEcc());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO B03-DS-OPER-SQL
        ws.getBilaTrchEstr().setB03DsOperSql(areaOut.getLccvb031().getDati().getWb03DsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO B03-DS-VER
        //           ELSE
        //              TO B03-DS-VER
        //           END-IF
        if (!Functions.isNumber(areaOut.getLccvb031().getDati().getWb03DsVer())) {
            // COB_CODE: MOVE 0 TO B03-DS-VER
            ws.getBilaTrchEstr().setB03DsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO B03-DS-VER
            ws.getBilaTrchEstr().setB03DsVer(areaOut.getLccvb031().getDati().getWb03DsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO B03-DS-TS-CPTZ
        //           ELSE
        //              TO B03-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(areaOut.getLccvb031().getDati().getWb03DsTsCptz())) {
            // COB_CODE: MOVE 0 TO B03-DS-TS-CPTZ
            ws.getBilaTrchEstr().setB03DsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO B03-DS-TS-CPTZ
            ws.getBilaTrchEstr().setB03DsTsCptz(areaOut.getLccvb031().getDati().getWb03DsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO B03-DS-UTENTE
        ws.getBilaTrchEstr().setB03DsUtente(areaOut.getLccvb031().getDati().getWb03DsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO B03-DS-STATO-ELAB
        ws.getBilaTrchEstr().setB03DsStatoElab(areaOut.getLccvb031().getDati().getWb03DsStatoElab());
        // COB_CODE: IF (SF)-TP-RGM-FISC-NULL = HIGH-VALUES
        //              TO B03-TP-RGM-FISC-NULL
        //           ELSE
        //              TO B03-TP-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpRgmFiscFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RGM-FISC-NULL
            //           TO B03-TP-RGM-FISC-NULL
            ws.getBilaTrchEstr().setB03TpRgmFisc(areaOut.getLccvb031().getDati().getWb03TpRgmFisc());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RGM-FISC
            //           TO B03-TP-RGM-FISC
            ws.getBilaTrchEstr().setB03TpRgmFisc(areaOut.getLccvb031().getDati().getWb03TpRgmFisc());
        }
        // COB_CODE: IF (SF)-DUR-GAR-AA-NULL = HIGH-VALUES
        //              TO B03-DUR-GAR-AA-NULL
        //           ELSE
        //              TO B03-DUR-GAR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurGarAa().getWb03DurGarAaNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-GAR-AA-NULL
            //           TO B03-DUR-GAR-AA-NULL
            ws.getBilaTrchEstr().getB03DurGarAa().setB03DurGarAaNull(areaOut.getLccvb031().getDati().getWb03DurGarAa().getWb03DurGarAaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-GAR-AA
            //           TO B03-DUR-GAR-AA
            ws.getBilaTrchEstr().getB03DurGarAa().setB03DurGarAa(areaOut.getLccvb031().getDati().getWb03DurGarAa().getWb03DurGarAa());
        }
        // COB_CODE: IF (SF)-DUR-GAR-MM-NULL = HIGH-VALUES
        //              TO B03-DUR-GAR-MM-NULL
        //           ELSE
        //              TO B03-DUR-GAR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurGarMm().getWb03DurGarMmNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-GAR-MM-NULL
            //           TO B03-DUR-GAR-MM-NULL
            ws.getBilaTrchEstr().getB03DurGarMm().setB03DurGarMmNull(areaOut.getLccvb031().getDati().getWb03DurGarMm().getWb03DurGarMmNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-GAR-MM
            //           TO B03-DUR-GAR-MM
            ws.getBilaTrchEstr().getB03DurGarMm().setB03DurGarMm(areaOut.getLccvb031().getDati().getWb03DurGarMm().getWb03DurGarMm());
        }
        // COB_CODE: IF (SF)-DUR-GAR-GG-NULL = HIGH-VALUES
        //              TO B03-DUR-GAR-GG-NULL
        //           ELSE
        //              TO B03-DUR-GAR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DurGarGg().getWb03DurGarGgNullFormatted())) {
            // COB_CODE: MOVE (SF)-DUR-GAR-GG-NULL
            //           TO B03-DUR-GAR-GG-NULL
            ws.getBilaTrchEstr().getB03DurGarGg().setB03DurGarGgNull(areaOut.getLccvb031().getDati().getWb03DurGarGg().getWb03DurGarGgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DUR-GAR-GG
            //           TO B03-DUR-GAR-GG
            ws.getBilaTrchEstr().getB03DurGarGg().setB03DurGarGg(areaOut.getLccvb031().getDati().getWb03DurGarGg().getWb03DurGarGg());
        }
        // COB_CODE: IF (SF)-ANTIDUR-CALC-365-NULL = HIGH-VALUES
        //              TO B03-ANTIDUR-CALC-365-NULL
        //           ELSE
        //              TO B03-ANTIDUR-CALC-365
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AntidurCalc365().getWb03AntidurCalc365NullFormatted())) {
            // COB_CODE: MOVE (SF)-ANTIDUR-CALC-365-NULL
            //           TO B03-ANTIDUR-CALC-365-NULL
            ws.getBilaTrchEstr().getB03AntidurCalc365().setB03AntidurCalc365Null(areaOut.getLccvb031().getDati().getWb03AntidurCalc365().getWb03AntidurCalc365Null());
        }
        else {
            // COB_CODE: MOVE (SF)-ANTIDUR-CALC-365
            //           TO B03-ANTIDUR-CALC-365
            ws.getBilaTrchEstr().getB03AntidurCalc365().setB03AntidurCalc365(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03AntidurCalc365().getWb03AntidurCalc365(), 11, 7));
        }
        // COB_CODE: IF (SF)-COD-FISC-CNTR-NULL = HIGH-VALUES
        //              TO B03-COD-FISC-CNTR-NULL
        //           ELSE
        //              TO B03-COD-FISC-CNTR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodFiscCntrFormatted())) {
            // COB_CODE: MOVE (SF)-COD-FISC-CNTR-NULL
            //           TO B03-COD-FISC-CNTR-NULL
            ws.getBilaTrchEstr().setB03CodFiscCntr(areaOut.getLccvb031().getDati().getWb03CodFiscCntr());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-FISC-CNTR
            //           TO B03-COD-FISC-CNTR
            ws.getBilaTrchEstr().setB03CodFiscCntr(areaOut.getLccvb031().getDati().getWb03CodFiscCntr());
        }
        // COB_CODE: IF (SF)-COD-FISC-ASSTO1-NULL = HIGH-VALUES
        //              TO B03-COD-FISC-ASSTO1-NULL
        //           ELSE
        //              TO B03-COD-FISC-ASSTO1
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodFiscAssto1Formatted())) {
            // COB_CODE: MOVE (SF)-COD-FISC-ASSTO1-NULL
            //           TO B03-COD-FISC-ASSTO1-NULL
            ws.getBilaTrchEstr().setB03CodFiscAssto1(areaOut.getLccvb031().getDati().getWb03CodFiscAssto1());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-FISC-ASSTO1
            //           TO B03-COD-FISC-ASSTO1
            ws.getBilaTrchEstr().setB03CodFiscAssto1(areaOut.getLccvb031().getDati().getWb03CodFiscAssto1());
        }
        // COB_CODE: IF (SF)-COD-FISC-ASSTO2-NULL = HIGH-VALUES
        //              TO B03-COD-FISC-ASSTO2-NULL
        //           ELSE
        //              TO B03-COD-FISC-ASSTO2
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodFiscAssto2Formatted())) {
            // COB_CODE: MOVE (SF)-COD-FISC-ASSTO2-NULL
            //           TO B03-COD-FISC-ASSTO2-NULL
            ws.getBilaTrchEstr().setB03CodFiscAssto2(areaOut.getLccvb031().getDati().getWb03CodFiscAssto2());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-FISC-ASSTO2
            //           TO B03-COD-FISC-ASSTO2
            ws.getBilaTrchEstr().setB03CodFiscAssto2(areaOut.getLccvb031().getDati().getWb03CodFiscAssto2());
        }
        // COB_CODE: IF (SF)-COD-FISC-ASSTO3-NULL = HIGH-VALUES
        //              TO B03-COD-FISC-ASSTO3-NULL
        //           ELSE
        //              TO B03-COD-FISC-ASSTO3
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodFiscAssto3Formatted())) {
            // COB_CODE: MOVE (SF)-COD-FISC-ASSTO3-NULL
            //           TO B03-COD-FISC-ASSTO3-NULL
            ws.getBilaTrchEstr().setB03CodFiscAssto3(areaOut.getLccvb031().getDati().getWb03CodFiscAssto3());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-FISC-ASSTO3
            //           TO B03-COD-FISC-ASSTO3
            ws.getBilaTrchEstr().setB03CodFiscAssto3(areaOut.getLccvb031().getDati().getWb03CodFiscAssto3());
        }
        // COB_CODE: MOVE (SF)-CAUS-SCON
        //              TO B03-CAUS-SCON
        ws.getBilaTrchEstr().setB03CausScon(areaOut.getLccvb031().getDati().getWb03CausScon());
        // COB_CODE: MOVE (SF)-EMIT-TIT-OPZ
        //              TO B03-EMIT-TIT-OPZ
        ws.getBilaTrchEstr().setB03EmitTitOpz(areaOut.getLccvb031().getDati().getWb03EmitTitOpz());
        // COB_CODE: IF (SF)-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
        //              TO B03-QTZ-SP-Z-COUP-EMIS-NULL
        //           ELSE
        //              TO B03-QTZ-SP-Z-COUP-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzSpZCoupEmis().getWb03QtzSpZCoupEmisNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-COUP-EMIS-NULL
            //           TO B03-QTZ-SP-Z-COUP-EMIS-NULL
            ws.getBilaTrchEstr().getB03QtzSpZCoupEmis().setB03QtzSpZCoupEmisNull(areaOut.getLccvb031().getDati().getWb03QtzSpZCoupEmis().getWb03QtzSpZCoupEmisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-COUP-EMIS
            //           TO B03-QTZ-SP-Z-COUP-EMIS
            ws.getBilaTrchEstr().getB03QtzSpZCoupEmis().setB03QtzSpZCoupEmis(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzSpZCoupEmis().getWb03QtzSpZCoupEmis(), 12, 5));
        }
        // COB_CODE: IF (SF)-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
        //              TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
        //           ELSE
        //              TO B03-QTZ-SP-Z-OPZ-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzSpZOpzEmis().getWb03QtzSpZOpzEmisNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-OPZ-EMIS-NULL
            //           TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
            ws.getBilaTrchEstr().getB03QtzSpZOpzEmis().setB03QtzSpZOpzEmisNull(areaOut.getLccvb031().getDati().getWb03QtzSpZOpzEmis().getWb03QtzSpZOpzEmisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-OPZ-EMIS
            //           TO B03-QTZ-SP-Z-OPZ-EMIS
            ws.getBilaTrchEstr().getB03QtzSpZOpzEmis().setB03QtzSpZOpzEmis(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzSpZOpzEmis().getWb03QtzSpZOpzEmis(), 12, 5));
        }
        // COB_CODE: IF (SF)-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
        //              TO B03-QTZ-SP-Z-COUP-DT-C-NULL
        //           ELSE
        //              TO B03-QTZ-SP-Z-COUP-DT-C
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzSpZCoupDtC().getWb03QtzSpZCoupDtCNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-COUP-DT-C-NULL
            //           TO B03-QTZ-SP-Z-COUP-DT-C-NULL
            ws.getBilaTrchEstr().getB03QtzSpZCoupDtC().setB03QtzSpZCoupDtCNull(areaOut.getLccvb031().getDati().getWb03QtzSpZCoupDtC().getWb03QtzSpZCoupDtCNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-COUP-DT-C
            //           TO B03-QTZ-SP-Z-COUP-DT-C
            ws.getBilaTrchEstr().getB03QtzSpZCoupDtC().setB03QtzSpZCoupDtC(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzSpZCoupDtC().getWb03QtzSpZCoupDtC(), 12, 5));
        }
        // COB_CODE: IF (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
        //              TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
        //           ELSE
        //              TO B03-QTZ-SP-Z-OPZ-DT-CA
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzSpZOpzDtCa().getWb03QtzSpZOpzDtCaNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL
            //           TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
            ws.getBilaTrchEstr().getB03QtzSpZOpzDtCa().setB03QtzSpZOpzDtCaNull(areaOut.getLccvb031().getDati().getWb03QtzSpZOpzDtCa().getWb03QtzSpZOpzDtCaNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-SP-Z-OPZ-DT-CA
            //           TO B03-QTZ-SP-Z-OPZ-DT-CA
            ws.getBilaTrchEstr().getB03QtzSpZOpzDtCa().setB03QtzSpZOpzDtCa(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzSpZOpzDtCa().getWb03QtzSpZOpzDtCa(), 12, 5));
        }
        // COB_CODE: IF (SF)-QTZ-TOT-EMIS-NULL = HIGH-VALUES
        //              TO B03-QTZ-TOT-EMIS-NULL
        //           ELSE
        //              TO B03-QTZ-TOT-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzTotEmis().getWb03QtzTotEmisNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-TOT-EMIS-NULL
            //           TO B03-QTZ-TOT-EMIS-NULL
            ws.getBilaTrchEstr().getB03QtzTotEmis().setB03QtzTotEmisNull(areaOut.getLccvb031().getDati().getWb03QtzTotEmis().getWb03QtzTotEmisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-TOT-EMIS
            //           TO B03-QTZ-TOT-EMIS
            ws.getBilaTrchEstr().getB03QtzTotEmis().setB03QtzTotEmis(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzTotEmis().getWb03QtzTotEmis(), 12, 5));
        }
        // COB_CODE: IF (SF)-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
        //              TO B03-QTZ-TOT-DT-CALC-NULL
        //           ELSE
        //              TO B03-QTZ-TOT-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzTotDtCalc().getWb03QtzTotDtCalcNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-TOT-DT-CALC-NULL
            //           TO B03-QTZ-TOT-DT-CALC-NULL
            ws.getBilaTrchEstr().getB03QtzTotDtCalc().setB03QtzTotDtCalcNull(areaOut.getLccvb031().getDati().getWb03QtzTotDtCalc().getWb03QtzTotDtCalcNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-TOT-DT-CALC
            //           TO B03-QTZ-TOT-DT-CALC
            ws.getBilaTrchEstr().getB03QtzTotDtCalc().setB03QtzTotDtCalc(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzTotDtCalc().getWb03QtzTotDtCalc(), 12, 5));
        }
        // COB_CODE: IF (SF)-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
        //              TO B03-QTZ-TOT-DT-ULT-BIL-NULL
        //           ELSE
        //              TO B03-QTZ-TOT-DT-ULT-BIL
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03QtzTotDtUltBil().getWb03QtzTotDtUltBilNullFormatted())) {
            // COB_CODE: MOVE (SF)-QTZ-TOT-DT-ULT-BIL-NULL
            //           TO B03-QTZ-TOT-DT-ULT-BIL-NULL
            ws.getBilaTrchEstr().getB03QtzTotDtUltBil().setB03QtzTotDtUltBilNull(areaOut.getLccvb031().getDati().getWb03QtzTotDtUltBil().getWb03QtzTotDtUltBilNull());
        }
        else {
            // COB_CODE: MOVE (SF)-QTZ-TOT-DT-ULT-BIL
            //           TO B03-QTZ-TOT-DT-ULT-BIL
            ws.getBilaTrchEstr().getB03QtzTotDtUltBil().setB03QtzTotDtUltBil(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03QtzTotDtUltBil().getWb03QtzTotDtUltBil(), 12, 5));
        }
        // COB_CODE: IF (SF)-DT-QTZ-EMIS-NULL = HIGH-VALUES
        //              TO B03-DT-QTZ-EMIS-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtQtzEmis().getWb03DtQtzEmisNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-QTZ-EMIS-NULL
            //           TO B03-DT-QTZ-EMIS-NULL
            ws.getBilaTrchEstr().getB03DtQtzEmis().setB03DtQtzEmisNull(areaOut.getLccvb031().getDati().getWb03DtQtzEmis().getWb03DtQtzEmisNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtQtzEmis().getWb03DtQtzEmis() == 0) {
            // COB_CODE: IF (SF)-DT-QTZ-EMIS = ZERO
            //              TO B03-DT-QTZ-EMIS-NULL
            //           ELSE
            //            TO B03-DT-QTZ-EMIS
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-QTZ-EMIS-NULL
            ws.getBilaTrchEstr().getB03DtQtzEmis().setB03DtQtzEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtQtzEmis.Len.B03_DT_QTZ_EMIS_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-QTZ-EMIS
            //           TO B03-DT-QTZ-EMIS
            ws.getBilaTrchEstr().getB03DtQtzEmis().setB03DtQtzEmis(areaOut.getLccvb031().getDati().getWb03DtQtzEmis().getWb03DtQtzEmis());
        }
        // COB_CODE: IF (SF)-PC-CAR-GEST-NULL = HIGH-VALUES
        //              TO B03-PC-CAR-GEST-NULL
        //           ELSE
        //              TO B03-PC-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PcCarGest().getWb03PcCarGestNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-CAR-GEST-NULL
            //           TO B03-PC-CAR-GEST-NULL
            ws.getBilaTrchEstr().getB03PcCarGest().setB03PcCarGestNull(areaOut.getLccvb031().getDati().getWb03PcCarGest().getWb03PcCarGestNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-CAR-GEST
            //           TO B03-PC-CAR-GEST
            ws.getBilaTrchEstr().getB03PcCarGest().setB03PcCarGest(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PcCarGest().getWb03PcCarGest(), 6, 3));
        }
        // COB_CODE: IF (SF)-PC-CAR-ACQ-NULL = HIGH-VALUES
        //              TO B03-PC-CAR-ACQ-NULL
        //           ELSE
        //              TO B03-PC-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PcCarAcq().getWb03PcCarAcqNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-CAR-ACQ-NULL
            //           TO B03-PC-CAR-ACQ-NULL
            ws.getBilaTrchEstr().getB03PcCarAcq().setB03PcCarAcqNull(areaOut.getLccvb031().getDati().getWb03PcCarAcq().getWb03PcCarAcqNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-CAR-ACQ
            //           TO B03-PC-CAR-ACQ
            ws.getBilaTrchEstr().getB03PcCarAcq().setB03PcCarAcq(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PcCarAcq().getWb03PcCarAcq(), 6, 3));
        }
        // COB_CODE: IF (SF)-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
        //              TO B03-IMP-CAR-CASO-MOR-NULL
        //           ELSE
        //              TO B03-IMP-CAR-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03ImpCarCasoMor().getWb03ImpCarCasoMorNullFormatted())) {
            // COB_CODE: MOVE (SF)-IMP-CAR-CASO-MOR-NULL
            //           TO B03-IMP-CAR-CASO-MOR-NULL
            ws.getBilaTrchEstr().getB03ImpCarCasoMor().setB03ImpCarCasoMorNull(areaOut.getLccvb031().getDati().getWb03ImpCarCasoMor().getWb03ImpCarCasoMorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-IMP-CAR-CASO-MOR
            //           TO B03-IMP-CAR-CASO-MOR
            ws.getBilaTrchEstr().getB03ImpCarCasoMor().setB03ImpCarCasoMor(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03ImpCarCasoMor().getWb03ImpCarCasoMor(), 15, 3));
        }
        // COB_CODE: IF (SF)-PC-CAR-MOR-NULL = HIGH-VALUES
        //              TO B03-PC-CAR-MOR-NULL
        //           ELSE
        //              TO B03-PC-CAR-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PcCarMor().getWb03PcCarMorNullFormatted())) {
            // COB_CODE: MOVE (SF)-PC-CAR-MOR-NULL
            //           TO B03-PC-CAR-MOR-NULL
            ws.getBilaTrchEstr().getB03PcCarMor().setB03PcCarMorNull(areaOut.getLccvb031().getDati().getWb03PcCarMor().getWb03PcCarMorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PC-CAR-MOR
            //           TO B03-PC-CAR-MOR
            ws.getBilaTrchEstr().getB03PcCarMor().setB03PcCarMor(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PcCarMor().getWb03PcCarMor(), 6, 3));
        }
        // COB_CODE: IF (SF)-TP-VERS-NULL = HIGH-VALUES
        //              TO B03-TP-VERS-NULL
        //           ELSE
        //              TO B03-TP-VERS
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03TpVers(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-TP-VERS-NULL
            //           TO B03-TP-VERS-NULL
            ws.getBilaTrchEstr().setB03TpVers(areaOut.getLccvb031().getDati().getWb03TpVers());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-VERS
            //           TO B03-TP-VERS
            ws.getBilaTrchEstr().setB03TpVers(areaOut.getLccvb031().getDati().getWb03TpVers());
        }
        // COB_CODE: IF (SF)-FL-SWITCH-NULL = HIGH-VALUES
        //              TO B03-FL-SWITCH-NULL
        //           ELSE
        //              TO B03-FL-SWITCH
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlSwitch(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-SWITCH-NULL
            //           TO B03-FL-SWITCH-NULL
            ws.getBilaTrchEstr().setB03FlSwitch(areaOut.getLccvb031().getDati().getWb03FlSwitch());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-SWITCH
            //           TO B03-FL-SWITCH
            ws.getBilaTrchEstr().setB03FlSwitch(areaOut.getLccvb031().getDati().getWb03FlSwitch());
        }
        // COB_CODE: IF (SF)-FL-IAS-NULL = HIGH-VALUES
        //              TO B03-FL-IAS-NULL
        //           ELSE
        //              TO B03-FL-IAS
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03FlIas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-IAS-NULL
            //           TO B03-FL-IAS-NULL
            ws.getBilaTrchEstr().setB03FlIas(areaOut.getLccvb031().getDati().getWb03FlIas());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-IAS
            //           TO B03-FL-IAS
            ws.getBilaTrchEstr().setB03FlIas(areaOut.getLccvb031().getDati().getWb03FlIas());
        }
        // COB_CODE: IF (SF)-DIR-NULL = HIGH-VALUES
        //              TO B03-DIR-NULL
        //           ELSE
        //              TO B03-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Dir().getWb03DirNullFormatted())) {
            // COB_CODE: MOVE (SF)-DIR-NULL
            //           TO B03-DIR-NULL
            ws.getBilaTrchEstr().getB03Dir().setB03DirNull(areaOut.getLccvb031().getDati().getWb03Dir().getWb03DirNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DIR
            //           TO B03-DIR
            ws.getBilaTrchEstr().getB03Dir().setB03Dir(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03Dir().getWb03Dir(), 15, 3));
        }
        // COB_CODE: IF (SF)-TP-COP-CASO-MOR-NULL = HIGH-VALUES
        //              TO B03-TP-COP-CASO-MOR-NULL
        //           ELSE
        //              TO B03-TP-COP-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpCopCasoMorFormatted())) {
            // COB_CODE: MOVE (SF)-TP-COP-CASO-MOR-NULL
            //           TO B03-TP-COP-CASO-MOR-NULL
            ws.getBilaTrchEstr().setB03TpCopCasoMor(areaOut.getLccvb031().getDati().getWb03TpCopCasoMor());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-COP-CASO-MOR
            //           TO B03-TP-COP-CASO-MOR
            ws.getBilaTrchEstr().setB03TpCopCasoMor(areaOut.getLccvb031().getDati().getWb03TpCopCasoMor());
        }
        // COB_CODE: IF (SF)-MET-RISC-SPCL-NULL = HIGH-VALUES
        //              TO B03-MET-RISC-SPCL-NULL
        //           ELSE
        //              TO B03-MET-RISC-SPCL
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03MetRiscSpcl().getWb03MetRiscSpclNull(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-MET-RISC-SPCL-NULL
            //           TO B03-MET-RISC-SPCL-NULL
            ws.getBilaTrchEstr().getB03MetRiscSpcl().setB03MetRiscSpclNull(areaOut.getLccvb031().getDati().getWb03MetRiscSpcl().getWb03MetRiscSpclNull());
        }
        else {
            // COB_CODE: MOVE (SF)-MET-RISC-SPCL
            //           TO B03-MET-RISC-SPCL
            ws.getBilaTrchEstr().getB03MetRiscSpcl().setB03MetRiscSpcl(areaOut.getLccvb031().getDati().getWb03MetRiscSpcl().getWb03MetRiscSpcl());
        }
        // COB_CODE: IF (SF)-TP-STAT-INVST-NULL = HIGH-VALUES
        //              TO B03-TP-STAT-INVST-NULL
        //           ELSE
        //              TO B03-TP-STAT-INVST
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpStatInvstFormatted())) {
            // COB_CODE: MOVE (SF)-TP-STAT-INVST-NULL
            //           TO B03-TP-STAT-INVST-NULL
            ws.getBilaTrchEstr().setB03TpStatInvst(areaOut.getLccvb031().getDati().getWb03TpStatInvst());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-STAT-INVST
            //           TO B03-TP-STAT-INVST
            ws.getBilaTrchEstr().setB03TpStatInvst(areaOut.getLccvb031().getDati().getWb03TpStatInvst());
        }
        // COB_CODE: IF (SF)-COD-PRDT-NULL = HIGH-VALUES
        //              TO B03-COD-PRDT-NULL
        //           ELSE
        //              TO B03-COD-PRDT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CodPrdt().getWb03CodPrdtNullFormatted())) {
            // COB_CODE: MOVE (SF)-COD-PRDT-NULL
            //           TO B03-COD-PRDT-NULL
            ws.getBilaTrchEstr().getB03CodPrdt().setB03CodPrdtNull(areaOut.getLccvb031().getDati().getWb03CodPrdt().getWb03CodPrdtNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-PRDT
            //           TO B03-COD-PRDT
            ws.getBilaTrchEstr().getB03CodPrdt().setB03CodPrdt(areaOut.getLccvb031().getDati().getWb03CodPrdt().getWb03CodPrdt());
        }
        // COB_CODE: IF (SF)-STAT-ASSTO-1-NULL = HIGH-VALUES
        //              TO B03-STAT-ASSTO-1-NULL
        //           ELSE
        //              TO B03-STAT-ASSTO-1
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03StatAssto1(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-STAT-ASSTO-1-NULL
            //           TO B03-STAT-ASSTO-1-NULL
            ws.getBilaTrchEstr().setB03StatAssto1(areaOut.getLccvb031().getDati().getWb03StatAssto1());
        }
        else {
            // COB_CODE: MOVE (SF)-STAT-ASSTO-1
            //           TO B03-STAT-ASSTO-1
            ws.getBilaTrchEstr().setB03StatAssto1(areaOut.getLccvb031().getDati().getWb03StatAssto1());
        }
        // COB_CODE: IF (SF)-STAT-ASSTO-2-NULL = HIGH-VALUES
        //              TO B03-STAT-ASSTO-2-NULL
        //           ELSE
        //              TO B03-STAT-ASSTO-2
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03StatAssto2(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-STAT-ASSTO-2-NULL
            //           TO B03-STAT-ASSTO-2-NULL
            ws.getBilaTrchEstr().setB03StatAssto2(areaOut.getLccvb031().getDati().getWb03StatAssto2());
        }
        else {
            // COB_CODE: MOVE (SF)-STAT-ASSTO-2
            //           TO B03-STAT-ASSTO-2
            ws.getBilaTrchEstr().setB03StatAssto2(areaOut.getLccvb031().getDati().getWb03StatAssto2());
        }
        // COB_CODE: IF (SF)-STAT-ASSTO-3-NULL = HIGH-VALUES
        //              TO B03-STAT-ASSTO-3-NULL
        //           ELSE
        //              TO B03-STAT-ASSTO-3
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03StatAssto3(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-STAT-ASSTO-3-NULL
            //           TO B03-STAT-ASSTO-3-NULL
            ws.getBilaTrchEstr().setB03StatAssto3(areaOut.getLccvb031().getDati().getWb03StatAssto3());
        }
        else {
            // COB_CODE: MOVE (SF)-STAT-ASSTO-3
            //           TO B03-STAT-ASSTO-3
            ws.getBilaTrchEstr().setB03StatAssto3(areaOut.getLccvb031().getDati().getWb03StatAssto3());
        }
        // COB_CODE: IF (SF)-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
        //              TO B03-CPT-ASSTO-INI-MOR-NULL
        //           ELSE
        //              TO B03-CPT-ASSTO-INI-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CptAsstoIniMor().getWb03CptAsstoIniMorNullFormatted())) {
            // COB_CODE: MOVE (SF)-CPT-ASSTO-INI-MOR-NULL
            //           TO B03-CPT-ASSTO-INI-MOR-NULL
            ws.getBilaTrchEstr().getB03CptAsstoIniMor().setB03CptAsstoIniMorNull(areaOut.getLccvb031().getDati().getWb03CptAsstoIniMor().getWb03CptAsstoIniMorNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CPT-ASSTO-INI-MOR
            //           TO B03-CPT-ASSTO-INI-MOR
            ws.getBilaTrchEstr().getB03CptAsstoIniMor().setB03CptAsstoIniMor(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CptAsstoIniMor().getWb03CptAsstoIniMor(), 15, 3));
        }
        // COB_CODE: IF (SF)-TS-STAB-PRE-NULL = HIGH-VALUES
        //              TO B03-TS-STAB-PRE-NULL
        //           ELSE
        //              TO B03-TS-STAB-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TsStabPre().getWb03TsStabPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-STAB-PRE-NULL
            //           TO B03-TS-STAB-PRE-NULL
            ws.getBilaTrchEstr().getB03TsStabPre().setB03TsStabPreNull(areaOut.getLccvb031().getDati().getWb03TsStabPre().getWb03TsStabPreNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-STAB-PRE
            //           TO B03-TS-STAB-PRE
            ws.getBilaTrchEstr().getB03TsStabPre().setB03TsStabPre(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03TsStabPre().getWb03TsStabPre(), 14, 9));
        }
        // COB_CODE: IF (SF)-DIR-EMIS-NULL = HIGH-VALUES
        //              TO B03-DIR-EMIS-NULL
        //           ELSE
        //              TO B03-DIR-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DirEmis().getWb03DirEmisNullFormatted())) {
            // COB_CODE: MOVE (SF)-DIR-EMIS-NULL
            //           TO B03-DIR-EMIS-NULL
            ws.getBilaTrchEstr().getB03DirEmis().setB03DirEmisNull(areaOut.getLccvb031().getDati().getWb03DirEmis().getWb03DirEmisNull());
        }
        else {
            // COB_CODE: MOVE (SF)-DIR-EMIS
            //           TO B03-DIR-EMIS
            ws.getBilaTrchEstr().getB03DirEmis().setB03DirEmis(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03DirEmis().getWb03DirEmis(), 15, 3));
        }
        // COB_CODE: IF (SF)-DT-INC-ULT-PRE-NULL = HIGH-VALUES
        //              TO B03-DT-INC-ULT-PRE-NULL
        //           ELSE
        //             END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03DtIncUltPre().getWb03DtIncUltPreNullFormatted())) {
            // COB_CODE: MOVE (SF)-DT-INC-ULT-PRE-NULL
            //           TO B03-DT-INC-ULT-PRE-NULL
            ws.getBilaTrchEstr().getB03DtIncUltPre().setB03DtIncUltPreNull(areaOut.getLccvb031().getDati().getWb03DtIncUltPre().getWb03DtIncUltPreNull());
        }
        else if (areaOut.getLccvb031().getDati().getWb03DtIncUltPre().getWb03DtIncUltPre() == 0) {
            // COB_CODE: IF (SF)-DT-INC-ULT-PRE = ZERO
            //              TO B03-DT-INC-ULT-PRE-NULL
            //           ELSE
            //            TO B03-DT-INC-ULT-PRE
            //           END-IF
            // COB_CODE: MOVE HIGH-VALUES
            //           TO B03-DT-INC-ULT-PRE-NULL
            ws.getBilaTrchEstr().getB03DtIncUltPre().setB03DtIncUltPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, B03DtIncUltPre.Len.B03_DT_INC_ULT_PRE_NULL));
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INC-ULT-PRE
            //           TO B03-DT-INC-ULT-PRE
            ws.getBilaTrchEstr().getB03DtIncUltPre().setB03DtIncUltPre(areaOut.getLccvb031().getDati().getWb03DtIncUltPre().getWb03DtIncUltPre());
        }
        // COB_CODE: IF (SF)-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
        //              TO B03-STAT-TBGC-ASSTO-1-NULL
        //           ELSE
        //              TO B03-STAT-TBGC-ASSTO-1
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto1(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-STAT-TBGC-ASSTO-1-NULL
            //           TO B03-STAT-TBGC-ASSTO-1-NULL
            ws.getBilaTrchEstr().setB03StatTbgcAssto1(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto1());
        }
        else {
            // COB_CODE: MOVE (SF)-STAT-TBGC-ASSTO-1
            //           TO B03-STAT-TBGC-ASSTO-1
            ws.getBilaTrchEstr().setB03StatTbgcAssto1(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto1());
        }
        // COB_CODE: IF (SF)-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
        //              TO B03-STAT-TBGC-ASSTO-2-NULL
        //           ELSE
        //              TO B03-STAT-TBGC-ASSTO-2
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto2(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-STAT-TBGC-ASSTO-2-NULL
            //           TO B03-STAT-TBGC-ASSTO-2-NULL
            ws.getBilaTrchEstr().setB03StatTbgcAssto2(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto2());
        }
        else {
            // COB_CODE: MOVE (SF)-STAT-TBGC-ASSTO-2
            //           TO B03-STAT-TBGC-ASSTO-2
            ws.getBilaTrchEstr().setB03StatTbgcAssto2(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto2());
        }
        // COB_CODE: IF (SF)-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
        //              TO B03-STAT-TBGC-ASSTO-3-NULL
        //           ELSE
        //              TO B03-STAT-TBGC-ASSTO-3
        //           END-IF
        if (Conditions.eq(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto3(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-STAT-TBGC-ASSTO-3-NULL
            //           TO B03-STAT-TBGC-ASSTO-3-NULL
            ws.getBilaTrchEstr().setB03StatTbgcAssto3(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto3());
        }
        else {
            // COB_CODE: MOVE (SF)-STAT-TBGC-ASSTO-3
            //           TO B03-STAT-TBGC-ASSTO-3
            ws.getBilaTrchEstr().setB03StatTbgcAssto3(areaOut.getLccvb031().getDati().getWb03StatTbgcAssto3());
        }
        // COB_CODE: IF (SF)-FRAZ-DECR-CPT-NULL = HIGH-VALUES
        //              TO B03-FRAZ-DECR-CPT-NULL
        //           ELSE
        //              TO B03-FRAZ-DECR-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03FrazDecrCpt().getWb03FrazDecrCptNullFormatted())) {
            // COB_CODE: MOVE (SF)-FRAZ-DECR-CPT-NULL
            //           TO B03-FRAZ-DECR-CPT-NULL
            ws.getBilaTrchEstr().getB03FrazDecrCpt().setB03FrazDecrCptNull(areaOut.getLccvb031().getDati().getWb03FrazDecrCpt().getWb03FrazDecrCptNull());
        }
        else {
            // COB_CODE: MOVE (SF)-FRAZ-DECR-CPT
            //           TO B03-FRAZ-DECR-CPT
            ws.getBilaTrchEstr().getB03FrazDecrCpt().setB03FrazDecrCpt(areaOut.getLccvb031().getDati().getWb03FrazDecrCpt().getWb03FrazDecrCpt());
        }
        // COB_CODE: IF (SF)-PRE-PP-ULT-NULL = HIGH-VALUES
        //              TO B03-PRE-PP-ULT-NULL
        //           ELSE
        //              TO B03-PRE-PP-ULT
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03PrePpUlt().getWb03PrePpUltNullFormatted())) {
            // COB_CODE: MOVE (SF)-PRE-PP-ULT-NULL
            //           TO B03-PRE-PP-ULT-NULL
            ws.getBilaTrchEstr().getB03PrePpUlt().setB03PrePpUltNull(areaOut.getLccvb031().getDati().getWb03PrePpUlt().getWb03PrePpUltNull());
        }
        else {
            // COB_CODE: MOVE (SF)-PRE-PP-ULT
            //           TO B03-PRE-PP-ULT
            ws.getBilaTrchEstr().getB03PrePpUlt().setB03PrePpUlt(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03PrePpUlt().getWb03PrePpUlt(), 15, 3));
        }
        // COB_CODE: IF (SF)-ACQ-EXP-NULL = HIGH-VALUES
        //              TO B03-ACQ-EXP-NULL
        //           ELSE
        //              TO B03-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03AcqExp().getWb03AcqExpNullFormatted())) {
            // COB_CODE: MOVE (SF)-ACQ-EXP-NULL
            //           TO B03-ACQ-EXP-NULL
            ws.getBilaTrchEstr().getB03AcqExp().setB03AcqExpNull(areaOut.getLccvb031().getDati().getWb03AcqExp().getWb03AcqExpNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ACQ-EXP
            //           TO B03-ACQ-EXP
            ws.getBilaTrchEstr().getB03AcqExp().setB03AcqExp(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03AcqExp().getWb03AcqExp(), 15, 3));
        }
        // COB_CODE: IF (SF)-REMUN-ASS-NULL = HIGH-VALUES
        //              TO B03-REMUN-ASS-NULL
        //           ELSE
        //              TO B03-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03RemunAss().getWb03RemunAssNullFormatted())) {
            // COB_CODE: MOVE (SF)-REMUN-ASS-NULL
            //           TO B03-REMUN-ASS-NULL
            ws.getBilaTrchEstr().getB03RemunAss().setB03RemunAssNull(areaOut.getLccvb031().getDati().getWb03RemunAss().getWb03RemunAssNull());
        }
        else {
            // COB_CODE: MOVE (SF)-REMUN-ASS
            //           TO B03-REMUN-ASS
            ws.getBilaTrchEstr().getB03RemunAss().setB03RemunAss(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03RemunAss().getWb03RemunAss(), 15, 3));
        }
        // COB_CODE: IF (SF)-COMMIS-INTER-NULL = HIGH-VALUES
        //              TO B03-COMMIS-INTER-NULL
        //           ELSE
        //              TO B03-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03CommisInter().getWb03CommisInterNullFormatted())) {
            // COB_CODE: MOVE (SF)-COMMIS-INTER-NULL
            //           TO B03-COMMIS-INTER-NULL
            ws.getBilaTrchEstr().getB03CommisInter().setB03CommisInterNull(areaOut.getLccvb031().getDati().getWb03CommisInter().getWb03CommisInterNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COMMIS-INTER
            //           TO B03-COMMIS-INTER
            ws.getBilaTrchEstr().getB03CommisInter().setB03CommisInter(Trunc.toDecimal(areaOut.getLccvb031().getDati().getWb03CommisInter().getWb03CommisInter(), 15, 3));
        }
        // COB_CODE: IF (SF)-NUM-FINANZ-NULL = HIGH-VALUES
        //              TO B03-NUM-FINANZ-NULL
        //           ELSE
        //              TO B03-NUM-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03NumFinanz(), Wb03Dati.Len.WB03_NUM_FINANZ)) {
            // COB_CODE: MOVE (SF)-NUM-FINANZ-NULL
            //           TO B03-NUM-FINANZ-NULL
            ws.getBilaTrchEstr().setB03NumFinanz(areaOut.getLccvb031().getDati().getWb03NumFinanz());
        }
        else {
            // COB_CODE: MOVE (SF)-NUM-FINANZ
            //           TO B03-NUM-FINANZ
            ws.getBilaTrchEstr().setB03NumFinanz(areaOut.getLccvb031().getDati().getWb03NumFinanz());
        }
        // COB_CODE: IF (SF)-TP-ACC-COMM-NULL = HIGH-VALUES
        //              TO B03-TP-ACC-COMM-NULL
        //           ELSE
        //              TO B03-TP-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03TpAccCommFormatted())) {
            // COB_CODE: MOVE (SF)-TP-ACC-COMM-NULL
            //           TO B03-TP-ACC-COMM-NULL
            ws.getBilaTrchEstr().setB03TpAccComm(areaOut.getLccvb031().getDati().getWb03TpAccComm());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-ACC-COMM
            //           TO B03-TP-ACC-COMM
            ws.getBilaTrchEstr().setB03TpAccComm(areaOut.getLccvb031().getDati().getWb03TpAccComm());
        }
        // COB_CODE: IF (SF)-IB-ACC-COMM-NULL = HIGH-VALUES
        //              TO B03-IB-ACC-COMM-NULL
        //           ELSE
        //              TO B03-IB-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03IbAccComm(), Wb03Dati.Len.WB03_IB_ACC_COMM)) {
            // COB_CODE: MOVE (SF)-IB-ACC-COMM-NULL
            //           TO B03-IB-ACC-COMM-NULL
            ws.getBilaTrchEstr().setB03IbAccComm(areaOut.getLccvb031().getDati().getWb03IbAccComm());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-ACC-COMM
            //           TO B03-IB-ACC-COMM
            ws.getBilaTrchEstr().setB03IbAccComm(areaOut.getLccvb031().getDati().getWb03IbAccComm());
        }
        // COB_CODE: MOVE (SF)-RAMO-BILA
        //              TO B03-RAMO-BILA
        ws.getBilaTrchEstr().setB03RamoBila(areaOut.getLccvb031().getDati().getWb03RamoBila());
        // COB_CODE: IF (SF)-CARZ-NULL = HIGH-VALUES
        //              TO B03-CARZ-NULL
        //           ELSE
        //              TO B03-CARZ
        //           END-IF.
        if (Characters.EQ_HIGH.test(areaOut.getLccvb031().getDati().getWb03Carz().getWb03CarzNullFormatted())) {
            // COB_CODE: MOVE (SF)-CARZ-NULL
            //           TO B03-CARZ-NULL
            ws.getBilaTrchEstr().getB03Carz().setB03CarzNull(areaOut.getLccvb031().getDati().getWb03Carz().getWb03CarzNull());
        }
        else {
            // COB_CODE: MOVE (SF)-CARZ
            //           TO B03-CARZ
            ws.getBilaTrchEstr().getB03Carz().setB03Carz(areaOut.getLccvb031().getDati().getWb03Carz().getWb03Carz());
        }
    }

    /**Original name: SCRIVI-BIL-ESTRATTI<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVB036
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO BIL_ESTRATTI
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCVB035 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN VINCOLO PEGNO (LCCVB031)
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void scriviBilEstratti() {
        // COB_CODE: INITIALIZE BILA-TRCH-ESTR.
        initBilaTrchEstr();
        //--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
        // COB_CODE:      IF  NOT WB03-ST-INV
        //                AND NOT WB03-ST-CON
        //                AND WB03-ELE-B03-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                      THRU AGGIORNA-TABELLA-EX
        //                END-IF.
        if (!areaOut.getLccvb031().getStatus().isInv() && !areaOut.getLccvb031().getStatus().isWpmoStCon() && areaOut.getWb03EleB03Max() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WB03-ST-ADD
            //           *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
            //                            SET IDSI0011-INSERT TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WB03-ST-MOD
            //                            SET IDSI0011-UPDATE TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WB03-ST-DEL
            //                            SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //                   END-EVALUATE
            switch (areaOut.getLccvb031().getStatus().getStatus()) {

                case WpolStatus.ADD://-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    // COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                TO WB03-ID-BILA-TRCH-ESTR
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA
                        //             TO WB03-ID-BILA-TRCH-ESTR
                        areaOut.getLccvb031().getDati().setWb03IdBilaTrchEstr(ws.getAreaIoLccs0090().getSeqTabella());
                    }
                    //-->        TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET IDSI0011-INSERT TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WB03-ID-BILA-TRCH-ESTR
                    //             TO B03-ID-BILA-TRCH-ESTR
                    ws.getBilaTrchEstr().setB03IdBilaTrchEstr(areaOut.getLccvb031().getDati().getWb03IdBilaTrchEstr());
                    //-->        TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET IDSI0011-UPDATE TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WB03-ID-BILA-TRCH-ESTR
                    //             TO B03-ID-BILA-TRCH-ESTR
                    ws.getBilaTrchEstr().setB03IdBilaTrchEstr(areaOut.getLccvb031().getDati().getWb03IdBilaTrchEstr());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    break;

                default:break;
            }
            //-->    VALORIZZA DCLGEN ADESIONE
            // COB_CODE: PERFORM VAL-DCLGEN-B03
            //              THRU VAL-DCLGEN-B03-EX
            valDclgenB03();
            //-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
            // COB_CODE: PERFORM VALORIZZA-AREA-DSH-B03
            //              THRU VALORIZZA-AREA-DSH-B03-EX
            valorizzaAreaDshB03();
            //-->    CALL DISPATCHER PER AGGIORNAMENTO
            // COB_CODE: PERFORM AGGIORNA-TABELLA
            //              THRU AGGIORNA-TABELLA-EX
            aggiornaTabella();
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-B03<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void valorizzaAreaDshB03() {
        // COB_CODE: MOVE 'BILA-TRCH-ESTR'         TO WK-TABELLA.
        ws.setWkTabella("BILA-TRCH-ESTR");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE BILA-TRCH-ESTR           TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getBilaTrchEstr().getBilaTrchEstrFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY        TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO   TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre> ----------------------------------------------------------------
	 *   ROUTINES GESTIONE ERRORI
	 *  ----------------------------------------------------------------
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: ESTR-SEQUENCE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCP0002
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequence() {
        Lccs0090 lccs0090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA TO S090-NOME-TABELLA.
        ws.getAreaIoLccs0090().setNomeTabella(ws.getWkTabella());
        // COB_CODE: CALL LCCS0090 USING AREA-IO-LCCS0090
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getAreaIoLccs0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ESTRAZIONE SEQUENCE");
            // COB_CODE: MOVE 'ESTR-SEQUENCE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("ESTR-SEQUENCE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-EVALUATE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         EVALUATE S090-RETURN-CODE
            //                       WHEN '00'
            //                            CONTINUE
            //                       WHEN 'S1'
            //           *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
            //                             THRU EX-S0300
            //                       WHEN 'D3'
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getAreaIoLccs0090().getReturnCode().getReturnCode()) {

                case "00":// COB_CODE: CONTINUE
                //continue
                    break;

                case "S1"://-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005015'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case "D3"://-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005016'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
    }

    public void initIxIndici() {
        ws.setIxTabB03(((short)0));
        ws.setIxWcom(((short)0));
    }

    public void initBilaTrchEstr() {
        ws.getBilaTrchEstr().setB03IdBilaTrchEstr(0);
        ws.getBilaTrchEstr().setB03CodCompAnia(0);
        ws.getBilaTrchEstr().setB03IdRichEstrazMas(0);
        ws.getBilaTrchEstr().getB03IdRichEstrazAgg().setB03IdRichEstrazAgg(0);
        ws.getBilaTrchEstr().setB03FlSimulazione(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03DtRis(0);
        ws.getBilaTrchEstr().setB03DtProduzione(0);
        ws.getBilaTrchEstr().setB03IdPoli(0);
        ws.getBilaTrchEstr().setB03IdAdes(0);
        ws.getBilaTrchEstr().setB03IdGar(0);
        ws.getBilaTrchEstr().setB03IdTrchDiGar(0);
        ws.getBilaTrchEstr().setB03TpFrmAssva("");
        ws.getBilaTrchEstr().setB03TpRamoBila("");
        ws.getBilaTrchEstr().setB03TpCalcRis("");
        ws.getBilaTrchEstr().setB03CodRamo("");
        ws.getBilaTrchEstr().setB03CodTari("");
        ws.getBilaTrchEstr().getB03DtIniValTar().setB03DtIniValTar(0);
        ws.getBilaTrchEstr().setB03CodProd("");
        ws.getBilaTrchEstr().setB03DtIniVldtProd(0);
        ws.getBilaTrchEstr().setB03CodTariOrgn("");
        ws.getBilaTrchEstr().getB03MinGartoT().setB03MinGartoT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().setB03TpTari("");
        ws.getBilaTrchEstr().setB03TpPre(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03TpAdegPre(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03TpRival("");
        ws.getBilaTrchEstr().setB03FlDaTrasf(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03FlCarCont(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03FlPreDaRis(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03FlPreAgg(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03TpTrch("");
        ws.getBilaTrchEstr().setB03TpTst("");
        ws.getBilaTrchEstr().setB03CodConv("");
        ws.getBilaTrchEstr().setB03DtDecorPoli(0);
        ws.getBilaTrchEstr().getB03DtDecorAdes().setB03DtDecorAdes(0);
        ws.getBilaTrchEstr().setB03DtDecorTrch(0);
        ws.getBilaTrchEstr().setB03DtEmisPoli(0);
        ws.getBilaTrchEstr().getB03DtEmisTrch().setB03DtEmisTrch(0);
        ws.getBilaTrchEstr().getB03DtScadTrch().setB03DtScadTrch(0);
        ws.getBilaTrchEstr().getB03DtScadIntmd().setB03DtScadIntmd(0);
        ws.getBilaTrchEstr().getB03DtScadPagPre().setB03DtScadPagPre(0);
        ws.getBilaTrchEstr().getB03DtUltPrePag().setB03DtUltPrePag(0);
        ws.getBilaTrchEstr().getB03DtNasc1oAssto().setB03DtNasc1oAssto(0);
        ws.getBilaTrchEstr().setB03Sex1oAssto(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().getB03EtaAa1oAssto().setB03EtaAa1oAssto(0);
        ws.getBilaTrchEstr().getB03EtaMm1oAssto().setB03EtaMm1oAssto(0);
        ws.getBilaTrchEstr().getB03EtaRaggnDtCalc().setB03EtaRaggnDtCalc(new AfDecimal(0, 7, 3));
        ws.getBilaTrchEstr().getB03DurAa().setB03DurAa(0);
        ws.getBilaTrchEstr().getB03DurMm().setB03DurMm(0);
        ws.getBilaTrchEstr().getB03DurGg().setB03DurGg(0);
        ws.getBilaTrchEstr().getB03Dur1oPerAa().setB03Dur1oPerAa(0);
        ws.getBilaTrchEstr().getB03Dur1oPerMm().setB03Dur1oPerMm(0);
        ws.getBilaTrchEstr().getB03Dur1oPerGg().setB03Dur1oPerGg(0);
        ws.getBilaTrchEstr().getB03AntidurRicorPrec().setB03AntidurRicorPrec(0);
        ws.getBilaTrchEstr().getB03AntidurDtCalc().setB03AntidurDtCalc(new AfDecimal(0, 11, 7));
        ws.getBilaTrchEstr().getB03DurResDtCalc().setB03DurResDtCalc(new AfDecimal(0, 11, 7));
        ws.getBilaTrchEstr().setB03TpStatBusPoli("");
        ws.getBilaTrchEstr().setB03TpCausPoli("");
        ws.getBilaTrchEstr().setB03TpStatBusAdes("");
        ws.getBilaTrchEstr().setB03TpCausAdes("");
        ws.getBilaTrchEstr().setB03TpStatBusTrch("");
        ws.getBilaTrchEstr().setB03TpCausTrch("");
        ws.getBilaTrchEstr().getB03DtEffCambStat().setB03DtEffCambStat(0);
        ws.getBilaTrchEstr().getB03DtEmisCambStat().setB03DtEmisCambStat(0);
        ws.getBilaTrchEstr().getB03DtEffStab().setB03DtEffStab(0);
        ws.getBilaTrchEstr().getB03CptDtStab().setB03CptDtStab(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03DtEffRidz().setB03DtEffRidz(0);
        ws.getBilaTrchEstr().getB03DtEmisRidz().setB03DtEmisRidz(0);
        ws.getBilaTrchEstr().getB03CptDtRidz().setB03CptDtRidz(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03Fraz().setB03Fraz(0);
        ws.getBilaTrchEstr().getB03DurPagPre().setB03DurPagPre(0);
        ws.getBilaTrchEstr().getB03NumPrePatt().setB03NumPrePatt(0);
        ws.getBilaTrchEstr().getB03FrazIniErogRen().setB03FrazIniErogRen(0);
        ws.getBilaTrchEstr().getB03AaRenCer().setB03AaRenCer(0);
        ws.getBilaTrchEstr().getB03RatRen().setB03RatRen(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().setB03CodDiv("");
        ws.getBilaTrchEstr().getB03Riscpar().setB03Riscpar(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CumRiscpar().setB03CumRiscpar(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03UltRm().setB03UltRm(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03TsRendtoT().setB03TsRendtoT(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03AlqRetrT().setB03AlqRetrT(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03MinTrnutT().setB03MinTrnutT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03TsNetT().setB03TsNetT(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03DtUltRival().setB03DtUltRival(0);
        ws.getBilaTrchEstr().getB03PrstzIni().setB03PrstzIni(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PrstzAggIni().setB03PrstzAggIni(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PrstzAggUlt().setB03PrstzAggUlt(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03Rappel().setB03Rappel(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PrePattuitoIni().setB03PrePattuitoIni(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreDovIni().setB03PreDovIni(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreDovRivtoT().setB03PreDovRivtoT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreAnnualizRicor().setB03PreAnnualizRicor(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreCont().setB03PreCont(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PrePpIni().setB03PrePpIni(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisPuraT().setB03RisPuraT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03ProvAcq().setB03ProvAcq(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03ProvAcqRicor().setB03ProvAcqRicor(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03ProvInc().setB03ProvInc(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CarAcqNonScon().setB03CarAcqNonScon(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03OverComm().setB03OverComm(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CarAcqPrecontato().setB03CarAcqPrecontato(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisAcqT().setB03RisAcqT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisZilT().setB03RisZilT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CarGestNonScon().setB03CarGestNonScon(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CarGest().setB03CarGest(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisSpeT().setB03RisSpeT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CarIncNonScon().setB03CarIncNonScon(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CarInc().setB03CarInc(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisRistorniCap().setB03RisRistorniCap(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03IntrTecn().setB03IntrTecn(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03CptRshMor().setB03CptRshMor(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CSubrshT().setB03CSubrshT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreRshT().setB03PreRshT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03AlqMargRis().setB03AlqMargRis(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03AlqMargCSubrsh().setB03AlqMargCSubrsh(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03TsRendtoSppr().setB03TsRendtoSppr(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().setB03TpIas("");
        ws.getBilaTrchEstr().getB03NsQuo().setB03NsQuo(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03TsMedio().setB03TsMedio(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03CptRiasto().setB03CptRiasto(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreRiasto().setB03PreRiasto(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisRiasta().setB03RisRiasta(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CptRiastoEcc().setB03CptRiastoEcc(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PreRiastoEcc().setB03PreRiastoEcc(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RisRiastaEcc().setB03RisRiastaEcc(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CodAge().setB03CodAge(0);
        ws.getBilaTrchEstr().getB03CodSubage().setB03CodSubage(0);
        ws.getBilaTrchEstr().getB03CodCan().setB03CodCan(0);
        ws.getBilaTrchEstr().setB03IbPoli("");
        ws.getBilaTrchEstr().setB03IbAdes("");
        ws.getBilaTrchEstr().setB03IbTrchDiGar("");
        ws.getBilaTrchEstr().setB03TpPrstz("");
        ws.getBilaTrchEstr().setB03TpTrasf("");
        ws.getBilaTrchEstr().setB03PpInvrioTari(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().getB03CoeffOpzRen().setB03CoeffOpzRen(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03CoeffOpzCpt().setB03CoeffOpzCpt(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03DurPagRen().setB03DurPagRen(0);
        ws.getBilaTrchEstr().setB03Vlt("");
        ws.getBilaTrchEstr().getB03RisMatChiuPrec().setB03RisMatChiuPrec(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().setB03CodFnd("");
        ws.getBilaTrchEstr().getB03PrstzT().setB03PrstzT(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03TsTariDov().setB03TsTariDov(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03TsTariScon().setB03TsTariScon(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03TsPp().setB03TsPp(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03CoeffRis1T().setB03CoeffRis1T(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03CoeffRis2T().setB03CoeffRis2T(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03Abb().setB03Abb(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().setB03TpCoass("");
        ws.getBilaTrchEstr().setB03TratRiass("");
        ws.getBilaTrchEstr().setB03TratRiassEcc("");
        ws.getBilaTrchEstr().setB03DsOperSql(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03DsVer(0);
        ws.getBilaTrchEstr().setB03DsTsCptz(0);
        ws.getBilaTrchEstr().setB03DsUtente("");
        ws.getBilaTrchEstr().setB03DsStatoElab(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03TpRgmFisc("");
        ws.getBilaTrchEstr().getB03DurGarAa().setB03DurGarAa(0);
        ws.getBilaTrchEstr().getB03DurGarMm().setB03DurGarMm(0);
        ws.getBilaTrchEstr().getB03DurGarGg().setB03DurGarGg(0);
        ws.getBilaTrchEstr().getB03AntidurCalc365().setB03AntidurCalc365(new AfDecimal(0, 11, 7));
        ws.getBilaTrchEstr().setB03CodFiscCntr("");
        ws.getBilaTrchEstr().setB03CodFiscAssto1("");
        ws.getBilaTrchEstr().setB03CodFiscAssto2("");
        ws.getBilaTrchEstr().setB03CodFiscAssto3("");
        ws.getBilaTrchEstr().setB03CausSconLen(((short)0));
        ws.getBilaTrchEstr().setB03CausScon("");
        ws.getBilaTrchEstr().setB03EmitTitOpzLen(((short)0));
        ws.getBilaTrchEstr().setB03EmitTitOpz("");
        ws.getBilaTrchEstr().getB03QtzSpZCoupEmis().setB03QtzSpZCoupEmis(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03QtzSpZOpzEmis().setB03QtzSpZOpzEmis(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03QtzSpZCoupDtC().setB03QtzSpZCoupDtC(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03QtzSpZOpzDtCa().setB03QtzSpZOpzDtCa(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03QtzTotEmis().setB03QtzTotEmis(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03QtzTotDtCalc().setB03QtzTotDtCalc(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03QtzTotDtUltBil().setB03QtzTotDtUltBil(new AfDecimal(0, 12, 5));
        ws.getBilaTrchEstr().getB03DtQtzEmis().setB03DtQtzEmis(0);
        ws.getBilaTrchEstr().getB03PcCarGest().setB03PcCarGest(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03PcCarAcq().setB03PcCarAcq(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().getB03ImpCarCasoMor().setB03ImpCarCasoMor(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03PcCarMor().setB03PcCarMor(new AfDecimal(0, 6, 3));
        ws.getBilaTrchEstr().setB03TpVers(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03FlSwitch(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03FlIas(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().getB03Dir().setB03Dir(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().setB03TpCopCasoMor("");
        ws.getBilaTrchEstr().getB03MetRiscSpcl().setB03MetRiscSpcl(((short)0));
        ws.getBilaTrchEstr().setB03TpStatInvst("");
        ws.getBilaTrchEstr().getB03CodPrdt().setB03CodPrdt(0);
        ws.getBilaTrchEstr().setB03StatAssto1(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03StatAssto2(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03StatAssto3(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().getB03CptAsstoIniMor().setB03CptAsstoIniMor(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03TsStabPre().setB03TsStabPre(new AfDecimal(0, 14, 9));
        ws.getBilaTrchEstr().getB03DirEmis().setB03DirEmis(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03DtIncUltPre().setB03DtIncUltPre(0);
        ws.getBilaTrchEstr().setB03StatTbgcAssto1(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03StatTbgcAssto2(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().setB03StatTbgcAssto3(Types.SPACE_CHAR);
        ws.getBilaTrchEstr().getB03FrazDecrCpt().setB03FrazDecrCpt(0);
        ws.getBilaTrchEstr().getB03PrePpUlt().setB03PrePpUlt(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03AcqExp().setB03AcqExp(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03RemunAss().setB03RemunAss(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().getB03CommisInter().setB03CommisInter(new AfDecimal(0, 15, 3));
        ws.getBilaTrchEstr().setB03NumFinanz("");
        ws.getBilaTrchEstr().setB03TpAccComm("");
        ws.getBilaTrchEstr().setB03IbAccComm("");
        ws.getBilaTrchEstr().setB03RamoBila("");
        ws.getBilaTrchEstr().getB03Carz().setB03Carz(0);
    }
}
