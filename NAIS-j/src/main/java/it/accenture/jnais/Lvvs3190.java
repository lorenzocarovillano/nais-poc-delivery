package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3190Data;

/**Original name: LVVS3190<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       DICEMBRE 2014.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS3190
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE.......
 *   DESCRIZIONE.... VALORIZZAZIONE VARIABILE DATAPREAMM
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3190 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3190Data ws = new Lvvs3190Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3190
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3190_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3190 getInstance() {
        return ((Lvvs3190)Programs.getInstance(Lvvs3190.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        initIxIndici();
        // COB_CODE: MOVE SPACES                       TO IVVC0213-VAL-STR-O
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> RECUPERA PRIMA IMMAGINE DI POLIZZA
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                 THRU S1200-LEGGI-POLIZZA-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-LEGGI-POLIZZA
            //              THRU S1200-LEGGI-POLIZZA-EX
            s1200LeggiPolizza();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLIZZA
            ws.setDpolAreaPolizzaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-LEGGI-POLIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *  RICERCA PARAM MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200LeggiPolizza() {
        Ldbsf510 ldbsf510 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        //
        // COB_CODE: MOVE DPOL-ID-POLI              TO POL-ID-POLI
        ws.getPoli().setPolIdPoli(ws.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: MOVE POLI                      TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getPoli().getPoliFormatted());
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION   TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE 'LDBSF510'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBSF510");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 POLI
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbsf510 = Ldbsf510.getInstance();
            ldbsf510.run(idsv0003, ws.getPoli());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBSF510 ERRORE CHIAMATA - LDBSF510'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSF510 ERRORE CHIAMATA - LDBSF510");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSV0003-SUCCESSFUL-SQL
            //                    END-IF
            //               WHEN IDSV0003-NOT-FOUND
            //                    SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //               WHEN OTHER
            //                    END-STRING
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF POL-DT-PRESC-NULL NOT = HIGH-VALUE
                    //                                AND LOW-VALUE AND SPACES
                    //                TO IVVC0213-VAL-STR-O
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getPoli().getPolDtPresc().getPolDtPrescNullFormatted()) && !Characters.EQ_LOW.test(ws.getPoli().getPolDtPresc().getPolDtPrescNullFormatted()) && !Characters.EQ_SPACE.test(ws.getPoli().getPolDtPresc().getPolDtPrescNull())) {
                        // COB_CODE: MOVE POL-DT-PRESC
                        //             TO IVVC0213-VAL-STR-O
                        ivvc0213.getTabOutput().setValStrO(ws.getPoli().getPolDtPresc().getPolDtPrescFormatted());
                    }
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM          TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA LDBSF510 ;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LDBSF510 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'ERRORE LETTURA TABELLA LDBSF510 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO
            //                  IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LDBSF510 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }
}
