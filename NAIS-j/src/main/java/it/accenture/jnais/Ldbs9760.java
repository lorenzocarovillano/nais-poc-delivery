package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.LiqDao;
import it.accenture.jnais.commons.data.to.ILiq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs9760Data;
import it.accenture.jnais.ws.LiqIdbslqu0;
import it.accenture.jnais.ws.redefines.LquAddizComun;
import it.accenture.jnais.ws.redefines.LquAddizRegion;
import it.accenture.jnais.ws.redefines.LquBnsNonGoduto;
import it.accenture.jnais.ws.redefines.LquCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquComponTaxRimb;
import it.accenture.jnais.ws.redefines.LquCosTunnelUscita;
import it.accenture.jnais.ws.redefines.LquDtDen;
import it.accenture.jnais.ws.redefines.LquDtEndIstr;
import it.accenture.jnais.ws.redefines.LquDtLiq;
import it.accenture.jnais.ws.redefines.LquDtMor;
import it.accenture.jnais.ws.redefines.LquDtPervDen;
import it.accenture.jnais.ws.redefines.LquDtRich;
import it.accenture.jnais.ws.redefines.LquDtVlt;
import it.accenture.jnais.ws.redefines.LquIdMoviChiu;
import it.accenture.jnais.ws.redefines.LquImpbAddizComun;
import it.accenture.jnais.ws.redefines.LquImpbAddizRegion;
import it.accenture.jnais.ws.redefines.LquImpbBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpbCnbtInpstfm;
import it.accenture.jnais.ws.redefines.LquImpbImpst252;
import it.accenture.jnais.ws.redefines.LquImpbImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpbIntrSuPrest;
import it.accenture.jnais.ws.redefines.LquImpbIrpef;
import it.accenture.jnais.ws.redefines.LquImpbIs;
import it.accenture.jnais.ws.redefines.LquImpbIs1382011;
import it.accenture.jnais.ws.redefines.LquImpbIs662014;
import it.accenture.jnais.ws.redefines.LquImpbTaxSep;
import it.accenture.jnais.ws.redefines.LquImpbVis1382011;
import it.accenture.jnais.ws.redefines.LquImpbVis662014;
import it.accenture.jnais.ws.redefines.LquImpDirDaRimb;
import it.accenture.jnais.ws.redefines.LquImpDirLiq;
import it.accenture.jnais.ws.redefines.LquImpExcontr;
import it.accenture.jnais.ws.redefines.LquImpIntrRitPag;
import it.accenture.jnais.ws.redefines.LquImpLrdCalcCp;
import it.accenture.jnais.ws.redefines.LquImpLrdDaRimb;
import it.accenture.jnais.ws.redefines.LquImpLrdLiqtoRilt;
import it.accenture.jnais.ws.redefines.LquImpOnerLiq;
import it.accenture.jnais.ws.redefines.LquImpPnl;
import it.accenture.jnais.ws.redefines.LquImpRenK1;
import it.accenture.jnais.ws.redefines.LquImpRenK2;
import it.accenture.jnais.ws.redefines.LquImpRenK3;
import it.accenture.jnais.ws.redefines.LquImpst252;
import it.accenture.jnais.ws.redefines.LquImpstApplRilt;
import it.accenture.jnais.ws.redefines.LquImpstBolloDettC;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotAa;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotSw;
import it.accenture.jnais.ws.redefines.LquImpstBolloTotV;
import it.accenture.jnais.ws.redefines.LquImpstDaRimb;
import it.accenture.jnais.ws.redefines.LquImpstIrpef;
import it.accenture.jnais.ws.redefines.LquImpstPrvr;
import it.accenture.jnais.ws.redefines.LquImpstSost1382011;
import it.accenture.jnais.ws.redefines.LquImpstSost662014;
import it.accenture.jnais.ws.redefines.LquImpstVis1382011;
import it.accenture.jnais.ws.redefines.LquImpstVis662014;
import it.accenture.jnais.ws.redefines.LquMontDal2007;
import it.accenture.jnais.ws.redefines.LquMontEnd2000;
import it.accenture.jnais.ws.redefines.LquMontEnd2006;
import it.accenture.jnais.ws.redefines.LquPcAbbTitStat;
import it.accenture.jnais.ws.redefines.LquPcAbbTs662014;
import it.accenture.jnais.ws.redefines.LquPcRen;
import it.accenture.jnais.ws.redefines.LquPcRenK1;
import it.accenture.jnais.ws.redefines.LquPcRenK2;
import it.accenture.jnais.ws.redefines.LquPcRenK3;
import it.accenture.jnais.ws.redefines.LquPcRiscParz;
import it.accenture.jnais.ws.redefines.LquRisMat;
import it.accenture.jnais.ws.redefines.LquRisSpe;
import it.accenture.jnais.ws.redefines.LquSpeRcs;
import it.accenture.jnais.ws.redefines.LquTaxSep;
import it.accenture.jnais.ws.redefines.LquTotIasMggSin;
import it.accenture.jnais.ws.redefines.LquTotIasOnerPrvnt;
import it.accenture.jnais.ws.redefines.LquTotIasPnl;
import it.accenture.jnais.ws.redefines.LquTotIasRstDpst;
import it.accenture.jnais.ws.redefines.LquTotImpbAcc;
import it.accenture.jnais.ws.redefines.LquTotImpbTfr;
import it.accenture.jnais.ws.redefines.LquTotImpbVis;
import it.accenture.jnais.ws.redefines.LquTotImpIntrPrest;
import it.accenture.jnais.ws.redefines.LquTotImpIs;
import it.accenture.jnais.ws.redefines.LquTotImpLrdLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpNetLiqto;
import it.accenture.jnais.ws.redefines.LquTotImpPrest;
import it.accenture.jnais.ws.redefines.LquTotImpRimb;
import it.accenture.jnais.ws.redefines.LquTotImpRitAcc;
import it.accenture.jnais.ws.redefines.LquTotImpRitTfr;
import it.accenture.jnais.ws.redefines.LquTotImpRitVis;
import it.accenture.jnais.ws.redefines.LquTotImpUti;
import it.accenture.jnais.ws.redefines.LquTpMetRisc;

/**Original name: LDBS9760<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 GIU 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs9760 extends Program implements ILiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private LiqDao liqDao = new LiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs9760Data ws = new Ldbs9760Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LIQ
    private LiqIdbslqu0 liq;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS9760_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, LiqIdbslqu0 liq) {
        this.idsv0003 = idsv0003;
        this.liq = liq;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs9760 getInstance() {
        return ((Ldbs9760)Programs.getInstance(Ldbs9760.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS9760'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS9760");
        // COB_CODE: MOVE 'LIQ' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("LIQ");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE   ID_OGG = :LQU-ID-OGG
        //                    AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY DS_TS_INI_CPTZ  DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE  ID_OGG = :LQU-ID-OGG
        //                    AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY DS_TS_INI_CPTZ  DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        liqDao.selectRec12(liq.getLquIdOgg(), liq.getLquTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        liqDao.openCEff40(liq.getLquIdOgg(), liq.getLquTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        liqDao.closeCEff40();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCEff40(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //              FROM LIQ
        //              WHERE   ID_OGG = :LQU-ID-OGG
        //                        AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY DS_TS_INI_CPTZ  DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_LIQ
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,TP_LIQ
        //                    ,DESC_CAU_EVE_SIN
        //                    ,COD_CAU_SIN
        //                    ,COD_SIN_CATSTRF
        //                    ,DT_MOR
        //                    ,DT_DEN
        //                    ,DT_PERV_DEN
        //                    ,DT_RICH
        //                    ,TP_SIN
        //                    ,TP_RISC
        //                    ,TP_MET_RISC
        //                    ,DT_LIQ
        //                    ,COD_DVS
        //                    ,TOT_IMP_LRD_LIQTO
        //                    ,TOT_IMP_PREST
        //                    ,TOT_IMP_INTR_PREST
        //                    ,TOT_IMP_UTI
        //                    ,TOT_IMP_RIT_TFR
        //                    ,TOT_IMP_RIT_ACC
        //                    ,TOT_IMP_RIT_VIS
        //                    ,TOT_IMPB_TFR
        //                    ,TOT_IMPB_ACC
        //                    ,TOT_IMPB_VIS
        //                    ,TOT_IMP_RIMB
        //                    ,IMPB_IMPST_PRVR
        //                    ,IMPST_PRVR
        //                    ,IMPB_IMPST_252
        //                    ,IMPST_252
        //                    ,TOT_IMP_IS
        //                    ,IMP_DIR_LIQ
        //                    ,TOT_IMP_NET_LIQTO
        //                    ,MONT_END2000
        //                    ,MONT_END2006
        //                    ,PC_REN
        //                    ,IMP_PNL
        //                    ,IMPB_IRPEF
        //                    ,IMPST_IRPEF
        //                    ,DT_VLT
        //                    ,DT_END_ISTR
        //                    ,TP_RIMB
        //                    ,SPE_RCS
        //                    ,IB_LIQ
        //                    ,TOT_IAS_ONER_PRVNT
        //                    ,TOT_IAS_MGG_SIN
        //                    ,TOT_IAS_RST_DPST
        //                    ,IMP_ONER_LIQ
        //                    ,COMPON_TAX_RIMB
        //                    ,TP_MEZ_PAG
        //                    ,IMP_EXCONTR
        //                    ,IMP_INTR_RIT_PAG
        //                    ,BNS_NON_GODUTO
        //                    ,CNBT_INPSTFM
        //                    ,IMPST_DA_RIMB
        //                    ,IMPB_IS
        //                    ,TAX_SEP
        //                    ,IMPB_TAX_SEP
        //                    ,IMPB_INTR_SU_PREST
        //                    ,ADDIZ_COMUN
        //                    ,IMPB_ADDIZ_COMUN
        //                    ,ADDIZ_REGION
        //                    ,IMPB_ADDIZ_REGION
        //                    ,MONT_DAL2007
        //                    ,IMPB_CNBT_INPSTFM
        //                    ,IMP_LRD_DA_RIMB
        //                    ,IMP_DIR_DA_RIMB
        //                    ,RIS_MAT
        //                    ,RIS_SPE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TOT_IAS_PNL
        //                    ,FL_EVE_GARTO
        //                    ,IMP_REN_K1
        //                    ,IMP_REN_K2
        //                    ,IMP_REN_K3
        //                    ,PC_REN_K1
        //                    ,PC_REN_K2
        //                    ,PC_REN_K3
        //                    ,TP_CAUS_ANTIC
        //                    ,IMP_LRD_LIQTO_RILT
        //                    ,IMPST_APPL_RILT
        //                    ,PC_RISC_PARZ
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_BOLLO_DETT_C
        //                    ,IMPST_BOLLO_TOT_SW
        //                    ,IMPST_BOLLO_TOT_AA
        //                    ,IMPB_VIS_1382011
        //                    ,IMPST_VIS_1382011
        //                    ,IMPB_IS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,PC_ABB_TIT_STAT
        //                    ,IMPB_BOLLO_DETT_C
        //                    ,FL_PRE_COMP
        //                    ,IMPB_VIS_662014
        //                    ,IMPST_VIS_662014
        //                    ,IMPB_IS_662014
        //                    ,IMPST_SOST_662014
        //                    ,PC_ABB_TS_662014
        //                    ,IMP_LRD_CALC_CP
        //                    ,COS_TUNNEL_USCITA
        //             INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //             FROM LIQ
        //             WHERE  ID_OGG = :LQU-ID-OGG
        //                    AND TP_OGG = :LQU-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY DS_TS_INI_CPTZ  DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        liqDao.selectRec13(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        liqDao.openCCpz40(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        liqDao.closeCCpz40();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :LQU-ID-LIQ
        //               ,:LQU-ID-OGG
        //               ,:LQU-TP-OGG
        //               ,:LQU-ID-MOVI-CRZ
        //               ,:LQU-ID-MOVI-CHIU
        //                :IND-LQU-ID-MOVI-CHIU
        //               ,:LQU-DT-INI-EFF-DB
        //               ,:LQU-DT-END-EFF-DB
        //               ,:LQU-COD-COMP-ANIA
        //               ,:LQU-IB-OGG
        //                :IND-LQU-IB-OGG
        //               ,:LQU-TP-LIQ
        //               ,:LQU-DESC-CAU-EVE-SIN-VCHAR
        //                :IND-LQU-DESC-CAU-EVE-SIN
        //               ,:LQU-COD-CAU-SIN
        //                :IND-LQU-COD-CAU-SIN
        //               ,:LQU-COD-SIN-CATSTRF
        //                :IND-LQU-COD-SIN-CATSTRF
        //               ,:LQU-DT-MOR-DB
        //                :IND-LQU-DT-MOR
        //               ,:LQU-DT-DEN-DB
        //                :IND-LQU-DT-DEN
        //               ,:LQU-DT-PERV-DEN-DB
        //                :IND-LQU-DT-PERV-DEN
        //               ,:LQU-DT-RICH-DB
        //                :IND-LQU-DT-RICH
        //               ,:LQU-TP-SIN
        //                :IND-LQU-TP-SIN
        //               ,:LQU-TP-RISC
        //                :IND-LQU-TP-RISC
        //               ,:LQU-TP-MET-RISC
        //                :IND-LQU-TP-MET-RISC
        //               ,:LQU-DT-LIQ-DB
        //                :IND-LQU-DT-LIQ
        //               ,:LQU-COD-DVS
        //                :IND-LQU-COD-DVS
        //               ,:LQU-TOT-IMP-LRD-LIQTO
        //                :IND-LQU-TOT-IMP-LRD-LIQTO
        //               ,:LQU-TOT-IMP-PREST
        //                :IND-LQU-TOT-IMP-PREST
        //               ,:LQU-TOT-IMP-INTR-PREST
        //                :IND-LQU-TOT-IMP-INTR-PREST
        //               ,:LQU-TOT-IMP-UTI
        //                :IND-LQU-TOT-IMP-UTI
        //               ,:LQU-TOT-IMP-RIT-TFR
        //                :IND-LQU-TOT-IMP-RIT-TFR
        //               ,:LQU-TOT-IMP-RIT-ACC
        //                :IND-LQU-TOT-IMP-RIT-ACC
        //               ,:LQU-TOT-IMP-RIT-VIS
        //                :IND-LQU-TOT-IMP-RIT-VIS
        //               ,:LQU-TOT-IMPB-TFR
        //                :IND-LQU-TOT-IMPB-TFR
        //               ,:LQU-TOT-IMPB-ACC
        //                :IND-LQU-TOT-IMPB-ACC
        //               ,:LQU-TOT-IMPB-VIS
        //                :IND-LQU-TOT-IMPB-VIS
        //               ,:LQU-TOT-IMP-RIMB
        //                :IND-LQU-TOT-IMP-RIMB
        //               ,:LQU-IMPB-IMPST-PRVR
        //                :IND-LQU-IMPB-IMPST-PRVR
        //               ,:LQU-IMPST-PRVR
        //                :IND-LQU-IMPST-PRVR
        //               ,:LQU-IMPB-IMPST-252
        //                :IND-LQU-IMPB-IMPST-252
        //               ,:LQU-IMPST-252
        //                :IND-LQU-IMPST-252
        //               ,:LQU-TOT-IMP-IS
        //                :IND-LQU-TOT-IMP-IS
        //               ,:LQU-IMP-DIR-LIQ
        //                :IND-LQU-IMP-DIR-LIQ
        //               ,:LQU-TOT-IMP-NET-LIQTO
        //                :IND-LQU-TOT-IMP-NET-LIQTO
        //               ,:LQU-MONT-END2000
        //                :IND-LQU-MONT-END2000
        //               ,:LQU-MONT-END2006
        //                :IND-LQU-MONT-END2006
        //               ,:LQU-PC-REN
        //                :IND-LQU-PC-REN
        //               ,:LQU-IMP-PNL
        //                :IND-LQU-IMP-PNL
        //               ,:LQU-IMPB-IRPEF
        //                :IND-LQU-IMPB-IRPEF
        //               ,:LQU-IMPST-IRPEF
        //                :IND-LQU-IMPST-IRPEF
        //               ,:LQU-DT-VLT-DB
        //                :IND-LQU-DT-VLT
        //               ,:LQU-DT-END-ISTR-DB
        //                :IND-LQU-DT-END-ISTR
        //               ,:LQU-TP-RIMB
        //               ,:LQU-SPE-RCS
        //                :IND-LQU-SPE-RCS
        //               ,:LQU-IB-LIQ
        //                :IND-LQU-IB-LIQ
        //               ,:LQU-TOT-IAS-ONER-PRVNT
        //                :IND-LQU-TOT-IAS-ONER-PRVNT
        //               ,:LQU-TOT-IAS-MGG-SIN
        //                :IND-LQU-TOT-IAS-MGG-SIN
        //               ,:LQU-TOT-IAS-RST-DPST
        //                :IND-LQU-TOT-IAS-RST-DPST
        //               ,:LQU-IMP-ONER-LIQ
        //                :IND-LQU-IMP-ONER-LIQ
        //               ,:LQU-COMPON-TAX-RIMB
        //                :IND-LQU-COMPON-TAX-RIMB
        //               ,:LQU-TP-MEZ-PAG
        //                :IND-LQU-TP-MEZ-PAG
        //               ,:LQU-IMP-EXCONTR
        //                :IND-LQU-IMP-EXCONTR
        //               ,:LQU-IMP-INTR-RIT-PAG
        //                :IND-LQU-IMP-INTR-RIT-PAG
        //               ,:LQU-BNS-NON-GODUTO
        //                :IND-LQU-BNS-NON-GODUTO
        //               ,:LQU-CNBT-INPSTFM
        //                :IND-LQU-CNBT-INPSTFM
        //               ,:LQU-IMPST-DA-RIMB
        //                :IND-LQU-IMPST-DA-RIMB
        //               ,:LQU-IMPB-IS
        //                :IND-LQU-IMPB-IS
        //               ,:LQU-TAX-SEP
        //                :IND-LQU-TAX-SEP
        //               ,:LQU-IMPB-TAX-SEP
        //                :IND-LQU-IMPB-TAX-SEP
        //               ,:LQU-IMPB-INTR-SU-PREST
        //                :IND-LQU-IMPB-INTR-SU-PREST
        //               ,:LQU-ADDIZ-COMUN
        //                :IND-LQU-ADDIZ-COMUN
        //               ,:LQU-IMPB-ADDIZ-COMUN
        //                :IND-LQU-IMPB-ADDIZ-COMUN
        //               ,:LQU-ADDIZ-REGION
        //                :IND-LQU-ADDIZ-REGION
        //               ,:LQU-IMPB-ADDIZ-REGION
        //                :IND-LQU-IMPB-ADDIZ-REGION
        //               ,:LQU-MONT-DAL2007
        //                :IND-LQU-MONT-DAL2007
        //               ,:LQU-IMPB-CNBT-INPSTFM
        //                :IND-LQU-IMPB-CNBT-INPSTFM
        //               ,:LQU-IMP-LRD-DA-RIMB
        //                :IND-LQU-IMP-LRD-DA-RIMB
        //               ,:LQU-IMP-DIR-DA-RIMB
        //                :IND-LQU-IMP-DIR-DA-RIMB
        //               ,:LQU-RIS-MAT
        //                :IND-LQU-RIS-MAT
        //               ,:LQU-RIS-SPE
        //                :IND-LQU-RIS-SPE
        //               ,:LQU-DS-RIGA
        //               ,:LQU-DS-OPER-SQL
        //               ,:LQU-DS-VER
        //               ,:LQU-DS-TS-INI-CPTZ
        //               ,:LQU-DS-TS-END-CPTZ
        //               ,:LQU-DS-UTENTE
        //               ,:LQU-DS-STATO-ELAB
        //               ,:LQU-TOT-IAS-PNL
        //                :IND-LQU-TOT-IAS-PNL
        //               ,:LQU-FL-EVE-GARTO
        //                :IND-LQU-FL-EVE-GARTO
        //               ,:LQU-IMP-REN-K1
        //                :IND-LQU-IMP-REN-K1
        //               ,:LQU-IMP-REN-K2
        //                :IND-LQU-IMP-REN-K2
        //               ,:LQU-IMP-REN-K3
        //                :IND-LQU-IMP-REN-K3
        //               ,:LQU-PC-REN-K1
        //                :IND-LQU-PC-REN-K1
        //               ,:LQU-PC-REN-K2
        //                :IND-LQU-PC-REN-K2
        //               ,:LQU-PC-REN-K3
        //                :IND-LQU-PC-REN-K3
        //               ,:LQU-TP-CAUS-ANTIC
        //                :IND-LQU-TP-CAUS-ANTIC
        //               ,:LQU-IMP-LRD-LIQTO-RILT
        //                :IND-LQU-IMP-LRD-LIQTO-RILT
        //               ,:LQU-IMPST-APPL-RILT
        //                :IND-LQU-IMPST-APPL-RILT
        //               ,:LQU-PC-RISC-PARZ
        //                :IND-LQU-PC-RISC-PARZ
        //               ,:LQU-IMPST-BOLLO-TOT-V
        //                :IND-LQU-IMPST-BOLLO-TOT-V
        //               ,:LQU-IMPST-BOLLO-DETT-C
        //                :IND-LQU-IMPST-BOLLO-DETT-C
        //               ,:LQU-IMPST-BOLLO-TOT-SW
        //                :IND-LQU-IMPST-BOLLO-TOT-SW
        //               ,:LQU-IMPST-BOLLO-TOT-AA
        //                :IND-LQU-IMPST-BOLLO-TOT-AA
        //               ,:LQU-IMPB-VIS-1382011
        //                :IND-LQU-IMPB-VIS-1382011
        //               ,:LQU-IMPST-VIS-1382011
        //                :IND-LQU-IMPST-VIS-1382011
        //               ,:LQU-IMPB-IS-1382011
        //                :IND-LQU-IMPB-IS-1382011
        //               ,:LQU-IMPST-SOST-1382011
        //                :IND-LQU-IMPST-SOST-1382011
        //               ,:LQU-PC-ABB-TIT-STAT
        //                :IND-LQU-PC-ABB-TIT-STAT
        //               ,:LQU-IMPB-BOLLO-DETT-C
        //                :IND-LQU-IMPB-BOLLO-DETT-C
        //               ,:LQU-FL-PRE-COMP
        //                :IND-LQU-FL-PRE-COMP
        //               ,:LQU-IMPB-VIS-662014
        //                :IND-LQU-IMPB-VIS-662014
        //               ,:LQU-IMPST-VIS-662014
        //                :IND-LQU-IMPST-VIS-662014
        //               ,:LQU-IMPB-IS-662014
        //                :IND-LQU-IMPB-IS-662014
        //               ,:LQU-IMPST-SOST-662014
        //                :IND-LQU-IMPST-SOST-662014
        //               ,:LQU-PC-ABB-TS-662014
        //                :IND-LQU-PC-ABB-TS-662014
        //               ,:LQU-IMP-LRD-CALC-CP
        //                :IND-LQU-IMP-LRD-CALC-CP
        //               ,:LQU-COS-TUNNEL-USCITA
        //                :IND-LQU-COS-TUNNEL-USCITA
        //           END-EXEC.
        liqDao.fetchCCpz40(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-LQU-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ID-MOVI-CHIU-NULL
            liq.getLquIdMoviChiu().setLquIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquIdMoviChiu.Len.LQU_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-LQU-IB-OGG = -1
        //              MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
        //           END-IF
        if (ws.getIndLiq().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IB-OGG-NULL
            liq.setLquIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_IB_OGG));
        }
        // COB_CODE: IF IND-LQU-DESC-CAU-EVE-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
        //           END-IF
        if (ws.getIndLiq().getDescCauEveSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DESC-CAU-EVE-SIN
            liq.setLquDescCauEveSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_DESC_CAU_EVE_SIN));
        }
        // COB_CODE: IF IND-LQU-COD-CAU-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getCodCauSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-CAU-SIN-NULL
            liq.setLquCodCauSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_COD_CAU_SIN));
        }
        // COB_CODE: IF IND-LQU-COD-SIN-CATSTRF = -1
        //              MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
        //           END-IF
        if (ws.getIndLiq().getCodSinCatstrf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-SIN-CATSTRF-NULL
            liq.setLquCodSinCatstrf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_COD_SIN_CATSTRF));
        }
        // COB_CODE: IF IND-LQU-DT-MOR = -1
        //              MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
        //           END-IF
        if (ws.getIndLiq().getDtMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-MOR-NULL
            liq.getLquDtMor().setLquDtMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtMor.Len.LQU_DT_MOR_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-DEN = -1
        //              MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
        //           END-IF
        if (ws.getIndLiq().getDtDen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-DEN-NULL
            liq.getLquDtDen().setLquDtDenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtDen.Len.LQU_DT_DEN_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-PERV-DEN = -1
        //              MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
        //           END-IF
        if (ws.getIndLiq().getDtPervDen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-PERV-DEN-NULL
            liq.getLquDtPervDen().setLquDtPervDenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtPervDen.Len.LQU_DT_PERV_DEN_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-RICH = -1
        //              MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
        //           END-IF
        if (ws.getIndLiq().getDtRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-RICH-NULL
            liq.getLquDtRich().setLquDtRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtRich.Len.LQU_DT_RICH_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getTpSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-SIN-NULL
            liq.setLquTpSin(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_SIN));
        }
        // COB_CODE: IF IND-LQU-TP-RISC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-RISC-NULL
            liq.setLquTpRisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_RISC));
        }
        // COB_CODE: IF IND-LQU-TP-MET-RISC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpMetRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-MET-RISC-NULL
            liq.getLquTpMetRisc().setLquTpMetRiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTpMetRisc.Len.LQU_TP_MET_RISC_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getDtLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-LIQ-NULL
            liq.getLquDtLiq().setLquDtLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtLiq.Len.LQU_DT_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-COD-DVS = -1
        //              MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
        //           END-IF
        if (ws.getIndLiq().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COD-DVS-NULL
            liq.setLquCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_COD_DVS));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-LRD-LIQTO = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpLrdLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-LRD-LIQTO-NULL
            liq.getLquTotImpLrdLiqto().setLquTotImpLrdLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpLrdLiqto.Len.LQU_TOT_IMP_LRD_LIQTO_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-PREST-NULL
            liq.getLquTotImpPrest().setLquTotImpPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpPrest.Len.LQU_TOT_IMP_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-INTR-PREST-NULL
            liq.getLquTotImpIntrPrest().setLquTotImpIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpIntrPrest.Len.LQU_TOT_IMP_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-UTI = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-UTI-NULL
            liq.getLquTotImpUti().setLquTotImpUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpUti.Len.LQU_TOT_IMP_UTI_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-TFR = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-TFR-NULL
            liq.getLquTotImpRitTfr().setLquTotImpRitTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitTfr.Len.LQU_TOT_IMP_RIT_TFR_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-ACC-NULL
            liq.getLquTotImpRitAcc().setLquTotImpRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitAcc.Len.LQU_TOT_IMP_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIT-VIS-NULL
            liq.getLquTotImpRitVis().setLquTotImpRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRitVis.Len.LQU_TOT_IMP_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-TFR = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-TFR-NULL
            liq.getLquTotImpbTfr().setLquTotImpbTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbTfr.Len.LQU_TOT_IMPB_TFR_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-ACC = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-ACC-NULL
            liq.getLquTotImpbAcc().setLquTotImpbAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbAcc.Len.LQU_TOT_IMPB_ACC_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMPB-VIS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpbVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMPB-VIS-NULL
            liq.getLquTotImpbVis().setLquTotImpbVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpbVis.Len.LQU_TOT_IMPB_VIS_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-RIMB-NULL
            liq.getLquTotImpRimb().setLquTotImpRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpRimb.Len.LQU_TOT_IMP_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IMPST-PRVR-NULL
            liq.getLquImpbImpstPrvr().setLquImpbImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbImpstPrvr.Len.LQU_IMPB_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-PRVR = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstPrvr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-PRVR-NULL
            liq.getLquImpstPrvr().setLquImpstPrvrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstPrvr.Len.LQU_IMPST_PRVR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IMPST-252-NULL
            liq.getLquImpbImpst252().setLquImpbImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbImpst252.Len.LQU_IMPB_IMPST252_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-252 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
        //           END-IF
        if (ws.getIndLiq().getImpst252() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-252-NULL
            liq.getLquImpst252().setLquImpst252Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpst252.Len.LQU_IMPST252_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-IS = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-IS-NULL
            liq.getLquTotImpIs().setLquTotImpIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpIs.Len.LQU_TOT_IMP_IS_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-DIR-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getImpDirLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-DIR-LIQ-NULL
            liq.getLquImpDirLiq().setLquImpDirLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpDirLiq.Len.LQU_IMP_DIR_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IMP-NET-LIQTO = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
        //           END-IF
        if (ws.getIndLiq().getTotImpNetLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IMP-NET-LIQTO-NULL
            liq.getLquTotImpNetLiqto().setLquTotImpNetLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotImpNetLiqto.Len.LQU_TOT_IMP_NET_LIQTO_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-END2000 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
        //           END-IF
        if (ws.getIndLiq().getMontEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-END2000-NULL
            liq.getLquMontEnd2000().setLquMontEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontEnd2000.Len.LQU_MONT_END2000_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-END2006 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
        //           END-IF
        if (ws.getIndLiq().getMontEnd2006() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-END2006-NULL
            liq.getLquMontEnd2006().setLquMontEnd2006Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontEnd2006.Len.LQU_MONT_END2006_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-NULL
            liq.getLquPcRen().setLquPcRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRen.Len.LQU_PC_REN_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-PNL = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
        //           END-IF
        if (ws.getIndLiq().getImpPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-PNL-NULL
            liq.getLquImpPnl().setLquImpPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpPnl.Len.LQU_IMP_PNL_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IRPEF = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIrpef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IRPEF-NULL
            liq.getLquImpbIrpef().setLquImpbIrpefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIrpef.Len.LQU_IMPB_IRPEF_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-IRPEF = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstIrpef() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-IRPEF-NULL
            liq.getLquImpstIrpef().setLquImpstIrpefNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstIrpef.Len.LQU_IMPST_IRPEF_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-VLT = -1
        //              MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
        //           END-IF
        if (ws.getIndLiq().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-VLT-NULL
            liq.getLquDtVlt().setLquDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtVlt.Len.LQU_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-LQU-DT-END-ISTR = -1
        //              MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
        //           END-IF
        if (ws.getIndLiq().getDtEndIstr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-DT-END-ISTR-NULL
            liq.getLquDtEndIstr().setLquDtEndIstrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquDtEndIstr.Len.LQU_DT_END_ISTR_NULL));
        }
        // COB_CODE: IF IND-LQU-SPE-RCS = -1
        //              MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
        //           END-IF
        if (ws.getIndLiq().getSpeRcs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-SPE-RCS-NULL
            liq.getLquSpeRcs().setLquSpeRcsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquSpeRcs.Len.LQU_SPE_RCS_NULL));
        }
        // COB_CODE: IF IND-LQU-IB-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getIbLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IB-LIQ-NULL
            liq.setLquIbLiq(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_IB_LIQ));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-ONER-PRVNT = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasOnerPrvnt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-ONER-PRVNT-NULL
            liq.getLquTotIasOnerPrvnt().setLquTotIasOnerPrvntNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasOnerPrvnt.Len.LQU_TOT_IAS_ONER_PRVNT_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-MGG-SIN = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasMggSin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-MGG-SIN-NULL
            liq.getLquTotIasMggSin().setLquTotIasMggSinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasMggSin.Len.LQU_TOT_IAS_MGG_SIN_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-RST-DPST = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasRstDpst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-RST-DPST-NULL
            liq.getLquTotIasRstDpst().setLquTotIasRstDpstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasRstDpst.Len.LQU_TOT_IAS_RST_DPST_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-ONER-LIQ = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
        //           END-IF
        if (ws.getIndLiq().getImpOnerLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-ONER-LIQ-NULL
            liq.getLquImpOnerLiq().setLquImpOnerLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpOnerLiq.Len.LQU_IMP_ONER_LIQ_NULL));
        }
        // COB_CODE: IF IND-LQU-COMPON-TAX-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getComponTaxRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COMPON-TAX-RIMB-NULL
            liq.getLquComponTaxRimb().setLquComponTaxRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquComponTaxRimb.Len.LQU_COMPON_TAX_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-MEZ-PAG = -1
        //              MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
        //           END-IF
        if (ws.getIndLiq().getTpMezPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-MEZ-PAG-NULL
            liq.setLquTpMezPag(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_MEZ_PAG));
        }
        // COB_CODE: IF IND-LQU-IMP-EXCONTR = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
        //           END-IF
        if (ws.getIndLiq().getImpExcontr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-EXCONTR-NULL
            liq.getLquImpExcontr().setLquImpExcontrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpExcontr.Len.LQU_IMP_EXCONTR_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-INTR-RIT-PAG = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
        //           END-IF
        if (ws.getIndLiq().getImpIntrRitPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-INTR-RIT-PAG-NULL
            liq.getLquImpIntrRitPag().setLquImpIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpIntrRitPag.Len.LQU_IMP_INTR_RIT_PAG_NULL));
        }
        // COB_CODE: IF IND-LQU-BNS-NON-GODUTO = -1
        //              MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
        //           END-IF
        if (ws.getIndLiq().getBnsNonGoduto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-BNS-NON-GODUTO-NULL
            liq.getLquBnsNonGoduto().setLquBnsNonGodutoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquBnsNonGoduto.Len.LQU_BNS_NON_GODUTO_NULL));
        }
        // COB_CODE: IF IND-LQU-CNBT-INPSTFM = -1
        //              MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
        //           END-IF
        if (ws.getIndLiq().getCnbtInpstfm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-CNBT-INPSTFM-NULL
            liq.getLquCnbtInpstfm().setLquCnbtInpstfmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquCnbtInpstfm.Len.LQU_CNBT_INPSTFM_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-DA-RIMB-NULL
            liq.getLquImpstDaRimb().setLquImpstDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstDaRimb.Len.LQU_IMPST_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-NULL
            liq.getLquImpbIs().setLquImpbIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs.Len.LQU_IMPB_IS_NULL));
        }
        // COB_CODE: IF IND-LQU-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndLiq().getTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TAX-SEP-NULL
            liq.getLquTaxSep().setLquTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTaxSep.Len.LQU_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-TAX-SEP-NULL
            liq.getLquImpbTaxSep().setLquImpbTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbTaxSep.Len.LQU_IMPB_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-INTR-SU-PREST = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIntrSuPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-INTR-SU-PREST-NULL
            liq.getLquImpbIntrSuPrest().setLquImpbIntrSuPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIntrSuPrest.Len.LQU_IMPB_INTR_SU_PREST_NULL));
        }
        // COB_CODE: IF IND-LQU-ADDIZ-COMUN = -1
        //              MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
        //           END-IF
        if (ws.getIndLiq().getAddizComun() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ADDIZ-COMUN-NULL
            liq.getLquAddizComun().setLquAddizComunNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquAddizComun.Len.LQU_ADDIZ_COMUN_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-ADDIZ-COMUN = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbAddizComun() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-COMUN-NULL
            liq.getLquImpbAddizComun().setLquImpbAddizComunNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbAddizComun.Len.LQU_IMPB_ADDIZ_COMUN_NULL));
        }
        // COB_CODE: IF IND-LQU-ADDIZ-REGION = -1
        //              MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
        //           END-IF
        if (ws.getIndLiq().getAddizRegion() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-ADDIZ-REGION-NULL
            liq.getLquAddizRegion().setLquAddizRegionNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquAddizRegion.Len.LQU_ADDIZ_REGION_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-ADDIZ-REGION = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbAddizRegion() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-ADDIZ-REGION-NULL
            liq.getLquImpbAddizRegion().setLquImpbAddizRegionNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbAddizRegion.Len.LQU_IMPB_ADDIZ_REGION_NULL));
        }
        // COB_CODE: IF IND-LQU-MONT-DAL2007 = -1
        //              MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
        //           END-IF
        if (ws.getIndLiq().getMontDal2007() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-MONT-DAL2007-NULL
            liq.getLquMontDal2007().setLquMontDal2007Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquMontDal2007.Len.LQU_MONT_DAL2007_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-CNBT-INPSTFM = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbCnbtInpstfm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-CNBT-INPSTFM-NULL
            liq.getLquImpbCnbtInpstfm().setLquImpbCnbtInpstfmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbCnbtInpstfm.Len.LQU_IMPB_CNBT_INPSTFM_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-DA-RIMB-NULL
            liq.getLquImpLrdDaRimb().setLquImpLrdDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdDaRimb.Len.LQU_IMP_LRD_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-DIR-DA-RIMB = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
        //           END-IF
        if (ws.getIndLiq().getImpDirDaRimb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-DIR-DA-RIMB-NULL
            liq.getLquImpDirDaRimb().setLquImpDirDaRimbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpDirDaRimb.Len.LQU_IMP_DIR_DA_RIMB_NULL));
        }
        // COB_CODE: IF IND-LQU-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndLiq().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-RIS-MAT-NULL
            liq.getLquRisMat().setLquRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquRisMat.Len.LQU_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-LQU-RIS-SPE = -1
        //              MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
        //           END-IF
        if (ws.getIndLiq().getRisSpe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-RIS-SPE-NULL
            liq.getLquRisSpe().setLquRisSpeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquRisSpe.Len.LQU_RIS_SPE_NULL));
        }
        // COB_CODE: IF IND-LQU-TOT-IAS-PNL = -1
        //              MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
        //           END-IF
        if (ws.getIndLiq().getTotIasPnl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TOT-IAS-PNL-NULL
            liq.getLquTotIasPnl().setLquTotIasPnlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquTotIasPnl.Len.LQU_TOT_IAS_PNL_NULL));
        }
        // COB_CODE: IF IND-LQU-FL-EVE-GARTO = -1
        //              MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
        //           END-IF
        if (ws.getIndLiq().getFlEveGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-FL-EVE-GARTO-NULL
            liq.setLquFlEveGarto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K1 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K1-NULL
            liq.getLquImpRenK1().setLquImpRenK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK1.Len.LQU_IMP_REN_K1_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K2 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K2-NULL
            liq.getLquImpRenK2().setLquImpRenK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK2.Len.LQU_IMP_REN_K2_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-REN-K3 = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
        //           END-IF
        if (ws.getIndLiq().getImpRenK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-REN-K3-NULL
            liq.getLquImpRenK3().setLquImpRenK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpRenK3.Len.LQU_IMP_REN_K3_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K1 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K1-NULL
            liq.getLquPcRenK1().setLquPcRenK1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK1.Len.LQU_PC_REN_K1_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K2 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K2-NULL
            liq.getLquPcRenK2().setLquPcRenK2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK2.Len.LQU_PC_REN_K2_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-REN-K3 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRenK3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-REN-K3-NULL
            liq.getLquPcRenK3().setLquPcRenK3Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRenK3.Len.LQU_PC_REN_K3_NULL));
        }
        // COB_CODE: IF IND-LQU-TP-CAUS-ANTIC = -1
        //              MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
        //           END-IF
        if (ws.getIndLiq().getTpCausAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-TP-CAUS-ANTIC-NULL
            liq.setLquTpCausAntic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LiqIdbslqu0.Len.LQU_TP_CAUS_ANTIC));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-LIQTO-RILT = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdLiqtoRilt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-LIQTO-RILT-NULL
            liq.getLquImpLrdLiqtoRilt().setLquImpLrdLiqtoRiltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdLiqtoRilt.Len.LQU_IMP_LRD_LIQTO_RILT_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-APPL-RILT = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstApplRilt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-APPL-RILT-NULL
            liq.getLquImpstApplRilt().setLquImpstApplRiltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstApplRilt.Len.LQU_IMPST_APPL_RILT_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-RISC-PARZ = -1
        //              MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
        //           END-IF
        if (ws.getIndLiq().getPcRiscParz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-RISC-PARZ-NULL
            liq.getLquPcRiscParz().setLquPcRiscParzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcRiscParz.Len.LQU_PC_RISC_PARZ_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-V = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotV() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-V-NULL
            liq.getLquImpstBolloTotV().setLquImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotV.Len.LQU_IMPST_BOLLO_TOT_V_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-DETT-C-NULL
            liq.getLquImpstBolloDettC().setLquImpstBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloDettC.Len.LQU_IMPST_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-SW = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotSw() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-SW-NULL
            liq.getLquImpstBolloTotSw().setLquImpstBolloTotSwNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotSw.Len.LQU_IMPST_BOLLO_TOT_SW_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-BOLLO-TOT-AA = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstBolloTotAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-BOLLO-TOT-AA-NULL
            liq.getLquImpstBolloTotAa().setLquImpstBolloTotAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstBolloTotAa.Len.LQU_IMPST_BOLLO_TOT_AA_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-VIS-1382011-NULL
            liq.getLquImpbVis1382011().setLquImpbVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbVis1382011.Len.LQU_IMPB_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-VIS-1382011-NULL
            liq.getLquImpstVis1382011().setLquImpstVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstVis1382011.Len.LQU_IMPST_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-1382011-NULL
            liq.getLquImpbIs1382011().setLquImpbIs1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs1382011.Len.LQU_IMPB_IS1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-SOST-1382011 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstSost1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-SOST-1382011-NULL
            liq.getLquImpstSost1382011().setLquImpstSost1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstSost1382011.Len.LQU_IMPST_SOST1382011_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-ABB-TIT-STAT = -1
        //              MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
        //           END-IF
        if (ws.getIndLiq().getPcAbbTitStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-ABB-TIT-STAT-NULL
            liq.getLquPcAbbTitStat().setLquPcAbbTitStatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcAbbTitStat.Len.LQU_PC_ABB_TIT_STAT_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-BOLLO-DETT-C = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbBolloDettC() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-BOLLO-DETT-C-NULL
            liq.getLquImpbBolloDettC().setLquImpbBolloDettCNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbBolloDettC.Len.LQU_IMPB_BOLLO_DETT_C_NULL));
        }
        // COB_CODE: IF IND-LQU-FL-PRE-COMP = -1
        //              MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
        //           END-IF
        if (ws.getIndLiq().getFlPreComp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-FL-PRE-COMP-NULL
            liq.setLquFlPreComp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-LQU-IMPB-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-VIS-662014-NULL
            liq.getLquImpbVis662014().setLquImpbVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbVis662014.Len.LQU_IMPB_VIS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-VIS-662014-NULL
            liq.getLquImpstVis662014().setLquImpstVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstVis662014.Len.LQU_IMPST_VIS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPB-IS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpbIs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPB-IS-662014-NULL
            liq.getLquImpbIs662014().setLquImpbIs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpbIs662014.Len.LQU_IMPB_IS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMPST-SOST-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getImpstSost662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMPST-SOST-662014-NULL
            liq.getLquImpstSost662014().setLquImpstSost662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpstSost662014.Len.LQU_IMPST_SOST662014_NULL));
        }
        // COB_CODE: IF IND-LQU-PC-ABB-TS-662014 = -1
        //              MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
        //           END-IF
        if (ws.getIndLiq().getPcAbbTs662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-PC-ABB-TS-662014-NULL
            liq.getLquPcAbbTs662014().setLquPcAbbTs662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquPcAbbTs662014.Len.LQU_PC_ABB_TS662014_NULL));
        }
        // COB_CODE: IF IND-LQU-IMP-LRD-CALC-CP = -1
        //              MOVE HIGH-VALUES TO LQU-IMP-LRD-CALC-CP-NULL
        //           END-IF
        if (ws.getIndLiq().getImpLrdCalcCp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-IMP-LRD-CALC-CP-NULL
            liq.getLquImpLrdCalcCp().setLquImpLrdCalcCpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquImpLrdCalcCp.Len.LQU_IMP_LRD_CALC_CP_NULL));
        }
        // COB_CODE: IF IND-LQU-COS-TUNNEL-USCITA = -1
        //              MOVE HIGH-VALUES TO LQU-COS-TUNNEL-USCITA-NULL
        //           END-IF.
        if (ws.getIndLiq().getCosTunnelUscita() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO LQU-COS-TUNNEL-USCITA-NULL
            liq.getLquCosTunnelUscita().setLquCosTunnelUscitaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, LquCosTunnelUscita.Len.LQU_COS_TUNNEL_USCITA_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE LQU-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getLiqDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-INI-EFF
        liq.setLquDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE LQU-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getLiqDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-END-EFF
        liq.setLquDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-LQU-DT-MOR = 0
        //               MOVE WS-DATE-N      TO LQU-DT-MOR
        //           END-IF
        if (ws.getIndLiq().getDtMor() == 0) {
            // COB_CODE: MOVE LQU-DT-MOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-MOR
            liq.getLquDtMor().setLquDtMor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-DEN = 0
        //               MOVE WS-DATE-N      TO LQU-DT-DEN
        //           END-IF
        if (ws.getIndLiq().getDtDen() == 0) {
            // COB_CODE: MOVE LQU-DT-DEN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-DEN
            liq.getLquDtDen().setLquDtDen(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-PERV-DEN = 0
        //               MOVE WS-DATE-N      TO LQU-DT-PERV-DEN
        //           END-IF
        if (ws.getIndLiq().getDtPervDen() == 0) {
            // COB_CODE: MOVE LQU-DT-PERV-DEN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-PERV-DEN
            liq.getLquDtPervDen().setLquDtPervDen(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-RICH = 0
        //               MOVE WS-DATE-N      TO LQU-DT-RICH
        //           END-IF
        if (ws.getIndLiq().getDtRich() == 0) {
            // COB_CODE: MOVE LQU-DT-RICH-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-RICH
            liq.getLquDtRich().setLquDtRich(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-LIQ = 0
        //               MOVE WS-DATE-N      TO LQU-DT-LIQ
        //           END-IF
        if (ws.getIndLiq().getDtLiq() == 0) {
            // COB_CODE: MOVE LQU-DT-LIQ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getVldtProdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-LIQ
            liq.getLquDtLiq().setLquDtLiq(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-VLT = 0
        //               MOVE WS-DATE-N      TO LQU-DT-VLT
        //           END-IF
        if (ws.getIndLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE LQU-DT-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-VLT
            liq.getLquDtVlt().setLquDtVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-LQU-DT-END-ISTR = 0
        //               MOVE WS-DATE-N      TO LQU-DT-END-ISTR
        //           END-IF.
        if (ws.getIndLiq().getDtEndIstr() == 0) {
            // COB_CODE: MOVE LQU-DT-END-ISTR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getLiqDb().getUltAdegPrePrDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO LQU-DT-END-ISTR
            liq.getLquDtEndIstr().setLquDtEndIstr(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF LQU-DESC-CAU-EVE-SIN
        //                       TO LQU-DESC-CAU-EVE-SIN-LEN.
        liq.setLquDescCauEveSinLen(((short)LiqIdbslqu0.Len.LQU_DESC_CAU_EVE_SIN));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAddizComun() {
        return liq.getLquAddizComun().getLquAddizComun();
    }

    @Override
    public void setAddizComun(AfDecimal addizComun) {
        this.liq.getLquAddizComun().setLquAddizComun(addizComun.copy());
    }

    @Override
    public AfDecimal getAddizComunObj() {
        if (ws.getIndLiq().getAddizComun() >= 0) {
            return getAddizComun();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAddizComunObj(AfDecimal addizComunObj) {
        if (addizComunObj != null) {
            setAddizComun(new AfDecimal(addizComunObj, 15, 3));
            ws.getIndLiq().setAddizComun(((short)0));
        }
        else {
            ws.getIndLiq().setAddizComun(((short)-1));
        }
    }

    @Override
    public AfDecimal getAddizRegion() {
        return liq.getLquAddizRegion().getLquAddizRegion();
    }

    @Override
    public void setAddizRegion(AfDecimal addizRegion) {
        this.liq.getLquAddizRegion().setLquAddizRegion(addizRegion.copy());
    }

    @Override
    public AfDecimal getAddizRegionObj() {
        if (ws.getIndLiq().getAddizRegion() >= 0) {
            return getAddizRegion();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAddizRegionObj(AfDecimal addizRegionObj) {
        if (addizRegionObj != null) {
            setAddizRegion(new AfDecimal(addizRegionObj, 15, 3));
            ws.getIndLiq().setAddizRegion(((short)0));
        }
        else {
            ws.getIndLiq().setAddizRegion(((short)-1));
        }
    }

    @Override
    public AfDecimal getBnsNonGoduto() {
        return liq.getLquBnsNonGoduto().getLquBnsNonGoduto();
    }

    @Override
    public void setBnsNonGoduto(AfDecimal bnsNonGoduto) {
        this.liq.getLquBnsNonGoduto().setLquBnsNonGoduto(bnsNonGoduto.copy());
    }

    @Override
    public AfDecimal getBnsNonGodutoObj() {
        if (ws.getIndLiq().getBnsNonGoduto() >= 0) {
            return getBnsNonGoduto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setBnsNonGodutoObj(AfDecimal bnsNonGodutoObj) {
        if (bnsNonGodutoObj != null) {
            setBnsNonGoduto(new AfDecimal(bnsNonGodutoObj, 15, 3));
            ws.getIndLiq().setBnsNonGoduto(((short)0));
        }
        else {
            ws.getIndLiq().setBnsNonGoduto(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtInpstfm() {
        return liq.getLquCnbtInpstfm().getLquCnbtInpstfm();
    }

    @Override
    public void setCnbtInpstfm(AfDecimal cnbtInpstfm) {
        this.liq.getLquCnbtInpstfm().setLquCnbtInpstfm(cnbtInpstfm.copy());
    }

    @Override
    public AfDecimal getCnbtInpstfmObj() {
        if (ws.getIndLiq().getCnbtInpstfm() >= 0) {
            return getCnbtInpstfm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtInpstfmObj(AfDecimal cnbtInpstfmObj) {
        if (cnbtInpstfmObj != null) {
            setCnbtInpstfm(new AfDecimal(cnbtInpstfmObj, 15, 3));
            ws.getIndLiq().setCnbtInpstfm(((short)0));
        }
        else {
            ws.getIndLiq().setCnbtInpstfm(((short)-1));
        }
    }

    @Override
    public String getCodCauSin() {
        return liq.getLquCodCauSin();
    }

    @Override
    public void setCodCauSin(String codCauSin) {
        this.liq.setLquCodCauSin(codCauSin);
    }

    @Override
    public String getCodCauSinObj() {
        if (ws.getIndLiq().getCodCauSin() >= 0) {
            return getCodCauSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCauSinObj(String codCauSinObj) {
        if (codCauSinObj != null) {
            setCodCauSin(codCauSinObj);
            ws.getIndLiq().setCodCauSin(((short)0));
        }
        else {
            ws.getIndLiq().setCodCauSin(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return liq.getLquCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.liq.setLquCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return liq.getLquCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.liq.setLquCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndLiq().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndLiq().setCodDvs(((short)0));
        }
        else {
            ws.getIndLiq().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getCodSinCatstrf() {
        return liq.getLquCodSinCatstrf();
    }

    @Override
    public void setCodSinCatstrf(String codSinCatstrf) {
        this.liq.setLquCodSinCatstrf(codSinCatstrf);
    }

    @Override
    public String getCodSinCatstrfObj() {
        if (ws.getIndLiq().getCodSinCatstrf() >= 0) {
            return getCodSinCatstrf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSinCatstrfObj(String codSinCatstrfObj) {
        if (codSinCatstrfObj != null) {
            setCodSinCatstrf(codSinCatstrfObj);
            ws.getIndLiq().setCodSinCatstrf(((short)0));
        }
        else {
            ws.getIndLiq().setCodSinCatstrf(((short)-1));
        }
    }

    @Override
    public AfDecimal getComponTaxRimb() {
        return liq.getLquComponTaxRimb().getLquComponTaxRimb();
    }

    @Override
    public void setComponTaxRimb(AfDecimal componTaxRimb) {
        this.liq.getLquComponTaxRimb().setLquComponTaxRimb(componTaxRimb.copy());
    }

    @Override
    public AfDecimal getComponTaxRimbObj() {
        if (ws.getIndLiq().getComponTaxRimb() >= 0) {
            return getComponTaxRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setComponTaxRimbObj(AfDecimal componTaxRimbObj) {
        if (componTaxRimbObj != null) {
            setComponTaxRimb(new AfDecimal(componTaxRimbObj, 15, 3));
            ws.getIndLiq().setComponTaxRimb(((short)0));
        }
        else {
            ws.getIndLiq().setComponTaxRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getCosTunnelUscita() {
        return liq.getLquCosTunnelUscita().getLquCosTunnelUscita();
    }

    @Override
    public void setCosTunnelUscita(AfDecimal cosTunnelUscita) {
        this.liq.getLquCosTunnelUscita().setLquCosTunnelUscita(cosTunnelUscita.copy());
    }

    @Override
    public AfDecimal getCosTunnelUscitaObj() {
        if (ws.getIndLiq().getCosTunnelUscita() >= 0) {
            return getCosTunnelUscita();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosTunnelUscitaObj(AfDecimal cosTunnelUscitaObj) {
        if (cosTunnelUscitaObj != null) {
            setCosTunnelUscita(new AfDecimal(cosTunnelUscitaObj, 15, 3));
            ws.getIndLiq().setCosTunnelUscita(((short)0));
        }
        else {
            ws.getIndLiq().setCosTunnelUscita(((short)-1));
        }
    }

    @Override
    public String getDescCauEveSinVchar() {
        return liq.getLquDescCauEveSinVcharFormatted();
    }

    @Override
    public void setDescCauEveSinVchar(String descCauEveSinVchar) {
        this.liq.setLquDescCauEveSinVcharFormatted(descCauEveSinVchar);
    }

    @Override
    public String getDescCauEveSinVcharObj() {
        if (ws.getIndLiq().getDescCauEveSin() >= 0) {
            return getDescCauEveSinVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescCauEveSinVcharObj(String descCauEveSinVcharObj) {
        if (descCauEveSinVcharObj != null) {
            setDescCauEveSinVchar(descCauEveSinVcharObj);
            ws.getIndLiq().setDescCauEveSin(((short)0));
        }
        else {
            ws.getIndLiq().setDescCauEveSin(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return liq.getLquDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.liq.setLquDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return liq.getLquDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.liq.setLquDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return liq.getLquDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.liq.setLquDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return liq.getLquDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.liq.setLquDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return liq.getLquDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.liq.setLquDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return liq.getLquDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.liq.setLquDsVer(dsVer);
    }

    @Override
    public String getDtDenDb() {
        return ws.getLiqDb().getScadDb();
    }

    @Override
    public void setDtDenDb(String dtDenDb) {
        this.ws.getLiqDb().setScadDb(dtDenDb);
    }

    @Override
    public String getDtDenDbObj() {
        if (ws.getIndLiq().getDtDen() >= 0) {
            return getDtDenDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDenDbObj(String dtDenDbObj) {
        if (dtDenDbObj != null) {
            setDtDenDb(dtDenDbObj);
            ws.getIndLiq().setDtDen(((short)0));
        }
        else {
            ws.getIndLiq().setDtDen(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getLiqDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getLiqDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtEndIstrDb() {
        return ws.getLiqDb().getUltAdegPrePrDb();
    }

    @Override
    public void setDtEndIstrDb(String dtEndIstrDb) {
        this.ws.getLiqDb().setUltAdegPrePrDb(dtEndIstrDb);
    }

    @Override
    public String getDtEndIstrDbObj() {
        if (ws.getIndLiq().getDtEndIstr() >= 0) {
            return getDtEndIstrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndIstrDbObj(String dtEndIstrDbObj) {
        if (dtEndIstrDbObj != null) {
            setDtEndIstrDb(dtEndIstrDbObj);
            ws.getIndLiq().setDtEndIstr(((short)0));
        }
        else {
            ws.getIndLiq().setDtEndIstr(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getLiqDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getLiqDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtLiqDb() {
        return ws.getLiqDb().getVldtProdDb();
    }

    @Override
    public void setDtLiqDb(String dtLiqDb) {
        this.ws.getLiqDb().setVldtProdDb(dtLiqDb);
    }

    @Override
    public String getDtLiqDbObj() {
        if (ws.getIndLiq().getDtLiq() >= 0) {
            return getDtLiqDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtLiqDbObj(String dtLiqDbObj) {
        if (dtLiqDbObj != null) {
            setDtLiqDb(dtLiqDbObj);
            ws.getIndLiq().setDtLiq(((short)0));
        }
        else {
            ws.getIndLiq().setDtLiq(((short)-1));
        }
    }

    @Override
    public String getDtMorDb() {
        return ws.getLiqDb().getDecorDb();
    }

    @Override
    public void setDtMorDb(String dtMorDb) {
        this.ws.getLiqDb().setDecorDb(dtMorDb);
    }

    @Override
    public String getDtMorDbObj() {
        if (ws.getIndLiq().getDtMor() >= 0) {
            return getDtMorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtMorDbObj(String dtMorDbObj) {
        if (dtMorDbObj != null) {
            setDtMorDb(dtMorDbObj);
            ws.getIndLiq().setDtMor(((short)0));
        }
        else {
            ws.getIndLiq().setDtMor(((short)-1));
        }
    }

    @Override
    public String getDtPervDenDb() {
        return ws.getLiqDb().getEmisDb();
    }

    @Override
    public void setDtPervDenDb(String dtPervDenDb) {
        this.ws.getLiqDb().setEmisDb(dtPervDenDb);
    }

    @Override
    public String getDtPervDenDbObj() {
        if (ws.getIndLiq().getDtPervDen() >= 0) {
            return getDtPervDenDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPervDenDbObj(String dtPervDenDbObj) {
        if (dtPervDenDbObj != null) {
            setDtPervDenDb(dtPervDenDbObj);
            ws.getIndLiq().setDtPervDen(((short)0));
        }
        else {
            ws.getIndLiq().setDtPervDen(((short)-1));
        }
    }

    @Override
    public String getDtRichDb() {
        return ws.getLiqDb().getEffStabDb();
    }

    @Override
    public void setDtRichDb(String dtRichDb) {
        this.ws.getLiqDb().setEffStabDb(dtRichDb);
    }

    @Override
    public String getDtRichDbObj() {
        if (ws.getIndLiq().getDtRich() >= 0) {
            return getDtRichDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRichDbObj(String dtRichDbObj) {
        if (dtRichDbObj != null) {
            setDtRichDb(dtRichDbObj);
            ws.getIndLiq().setDtRich(((short)0));
        }
        else {
            ws.getIndLiq().setDtRich(((short)-1));
        }
    }

    @Override
    public String getDtVltDb() {
        return ws.getLiqDb().getIniValTarDb();
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        this.ws.getLiqDb().setIniValTarDb(dtVltDb);
    }

    @Override
    public String getDtVltDbObj() {
        if (ws.getIndLiq().getDtVlt() >= 0) {
            return getDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        if (dtVltDbObj != null) {
            setDtVltDb(dtVltDbObj);
            ws.getIndLiq().setDtVlt(((short)0));
        }
        else {
            ws.getIndLiq().setDtVlt(((short)-1));
        }
    }

    @Override
    public char getFlEveGarto() {
        return liq.getLquFlEveGarto();
    }

    @Override
    public void setFlEveGarto(char flEveGarto) {
        this.liq.setLquFlEveGarto(flEveGarto);
    }

    @Override
    public Character getFlEveGartoObj() {
        if (ws.getIndLiq().getFlEveGarto() >= 0) {
            return ((Character)getFlEveGarto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEveGartoObj(Character flEveGartoObj) {
        if (flEveGartoObj != null) {
            setFlEveGarto(((char)flEveGartoObj));
            ws.getIndLiq().setFlEveGarto(((short)0));
        }
        else {
            ws.getIndLiq().setFlEveGarto(((short)-1));
        }
    }

    @Override
    public char getFlPreComp() {
        return liq.getLquFlPreComp();
    }

    @Override
    public void setFlPreComp(char flPreComp) {
        this.liq.setLquFlPreComp(flPreComp);
    }

    @Override
    public Character getFlPreCompObj() {
        if (ws.getIndLiq().getFlPreComp() >= 0) {
            return ((Character)getFlPreComp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlPreCompObj(Character flPreCompObj) {
        if (flPreCompObj != null) {
            setFlPreComp(((char)flPreCompObj));
            ws.getIndLiq().setFlPreComp(((short)0));
        }
        else {
            ws.getIndLiq().setFlPreComp(((short)-1));
        }
    }

    @Override
    public String getIbLiq() {
        return liq.getLquIbLiq();
    }

    @Override
    public void setIbLiq(String ibLiq) {
        this.liq.setLquIbLiq(ibLiq);
    }

    @Override
    public String getIbLiqObj() {
        if (ws.getIndLiq().getIbLiq() >= 0) {
            return getIbLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbLiqObj(String ibLiqObj) {
        if (ibLiqObj != null) {
            setIbLiq(ibLiqObj);
            ws.getIndLiq().setIbLiq(((short)0));
        }
        else {
            ws.getIndLiq().setIbLiq(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return liq.getLquIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.liq.setLquIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndLiq().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndLiq().setIbOgg(((short)0));
        }
        else {
            ws.getIndLiq().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getIdLiq() {
        return liq.getLquIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.liq.setLquIdLiq(idLiq);
    }

    @Override
    public int getIdMoviChiu() {
        return liq.getLquIdMoviChiu().getLquIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.liq.getLquIdMoviChiu().setLquIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndLiq().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndLiq().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndLiq().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpDirDaRimb() {
        return liq.getLquImpDirDaRimb().getLquImpDirDaRimb();
    }

    @Override
    public void setImpDirDaRimb(AfDecimal impDirDaRimb) {
        this.liq.getLquImpDirDaRimb().setLquImpDirDaRimb(impDirDaRimb.copy());
    }

    @Override
    public AfDecimal getImpDirDaRimbObj() {
        if (ws.getIndLiq().getImpDirDaRimb() >= 0) {
            return getImpDirDaRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpDirDaRimbObj(AfDecimal impDirDaRimbObj) {
        if (impDirDaRimbObj != null) {
            setImpDirDaRimb(new AfDecimal(impDirDaRimbObj, 15, 3));
            ws.getIndLiq().setImpDirDaRimb(((short)0));
        }
        else {
            ws.getIndLiq().setImpDirDaRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpDirLiq() {
        return liq.getLquImpDirLiq().getLquImpDirLiq();
    }

    @Override
    public void setImpDirLiq(AfDecimal impDirLiq) {
        this.liq.getLquImpDirLiq().setLquImpDirLiq(impDirLiq.copy());
    }

    @Override
    public AfDecimal getImpDirLiqObj() {
        if (ws.getIndLiq().getImpDirLiq() >= 0) {
            return getImpDirLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpDirLiqObj(AfDecimal impDirLiqObj) {
        if (impDirLiqObj != null) {
            setImpDirLiq(new AfDecimal(impDirLiqObj, 15, 3));
            ws.getIndLiq().setImpDirLiq(((short)0));
        }
        else {
            ws.getIndLiq().setImpDirLiq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpExcontr() {
        return liq.getLquImpExcontr().getLquImpExcontr();
    }

    @Override
    public void setImpExcontr(AfDecimal impExcontr) {
        this.liq.getLquImpExcontr().setLquImpExcontr(impExcontr.copy());
    }

    @Override
    public AfDecimal getImpExcontrObj() {
        if (ws.getIndLiq().getImpExcontr() >= 0) {
            return getImpExcontr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpExcontrObj(AfDecimal impExcontrObj) {
        if (impExcontrObj != null) {
            setImpExcontr(new AfDecimal(impExcontrObj, 15, 3));
            ws.getIndLiq().setImpExcontr(((short)0));
        }
        else {
            ws.getIndLiq().setImpExcontr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPag() {
        return liq.getLquImpIntrRitPag().getLquImpIntrRitPag();
    }

    @Override
    public void setImpIntrRitPag(AfDecimal impIntrRitPag) {
        this.liq.getLquImpIntrRitPag().setLquImpIntrRitPag(impIntrRitPag.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagObj() {
        if (ws.getIndLiq().getImpIntrRitPag() >= 0) {
            return getImpIntrRitPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj) {
        if (impIntrRitPagObj != null) {
            setImpIntrRitPag(new AfDecimal(impIntrRitPagObj, 15, 3));
            ws.getIndLiq().setImpIntrRitPag(((short)0));
        }
        else {
            ws.getIndLiq().setImpIntrRitPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdCalcCp() {
        return liq.getLquImpLrdCalcCp().getLquImpLrdCalcCp();
    }

    @Override
    public void setImpLrdCalcCp(AfDecimal impLrdCalcCp) {
        this.liq.getLquImpLrdCalcCp().setLquImpLrdCalcCp(impLrdCalcCp.copy());
    }

    @Override
    public AfDecimal getImpLrdCalcCpObj() {
        if (ws.getIndLiq().getImpLrdCalcCp() >= 0) {
            return getImpLrdCalcCp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdCalcCpObj(AfDecimal impLrdCalcCpObj) {
        if (impLrdCalcCpObj != null) {
            setImpLrdCalcCp(new AfDecimal(impLrdCalcCpObj, 15, 3));
            ws.getIndLiq().setImpLrdCalcCp(((short)0));
        }
        else {
            ws.getIndLiq().setImpLrdCalcCp(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdDaRimb() {
        return liq.getLquImpLrdDaRimb().getLquImpLrdDaRimb();
    }

    @Override
    public void setImpLrdDaRimb(AfDecimal impLrdDaRimb) {
        this.liq.getLquImpLrdDaRimb().setLquImpLrdDaRimb(impLrdDaRimb.copy());
    }

    @Override
    public AfDecimal getImpLrdDaRimbObj() {
        if (ws.getIndLiq().getImpLrdDaRimb() >= 0) {
            return getImpLrdDaRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdDaRimbObj(AfDecimal impLrdDaRimbObj) {
        if (impLrdDaRimbObj != null) {
            setImpLrdDaRimb(new AfDecimal(impLrdDaRimbObj, 15, 3));
            ws.getIndLiq().setImpLrdDaRimb(((short)0));
        }
        else {
            ws.getIndLiq().setImpLrdDaRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdLiqtoRilt() {
        return liq.getLquImpLrdLiqtoRilt().getLquImpLrdLiqtoRilt();
    }

    @Override
    public void setImpLrdLiqtoRilt(AfDecimal impLrdLiqtoRilt) {
        this.liq.getLquImpLrdLiqtoRilt().setLquImpLrdLiqtoRilt(impLrdLiqtoRilt.copy());
    }

    @Override
    public AfDecimal getImpLrdLiqtoRiltObj() {
        if (ws.getIndLiq().getImpLrdLiqtoRilt() >= 0) {
            return getImpLrdLiqtoRilt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdLiqtoRiltObj(AfDecimal impLrdLiqtoRiltObj) {
        if (impLrdLiqtoRiltObj != null) {
            setImpLrdLiqtoRilt(new AfDecimal(impLrdLiqtoRiltObj, 15, 3));
            ws.getIndLiq().setImpLrdLiqtoRilt(((short)0));
        }
        else {
            ws.getIndLiq().setImpLrdLiqtoRilt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpOnerLiq() {
        return liq.getLquImpOnerLiq().getLquImpOnerLiq();
    }

    @Override
    public void setImpOnerLiq(AfDecimal impOnerLiq) {
        this.liq.getLquImpOnerLiq().setLquImpOnerLiq(impOnerLiq.copy());
    }

    @Override
    public AfDecimal getImpOnerLiqObj() {
        if (ws.getIndLiq().getImpOnerLiq() >= 0) {
            return getImpOnerLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpOnerLiqObj(AfDecimal impOnerLiqObj) {
        if (impOnerLiqObj != null) {
            setImpOnerLiq(new AfDecimal(impOnerLiqObj, 15, 3));
            ws.getIndLiq().setImpOnerLiq(((short)0));
        }
        else {
            ws.getIndLiq().setImpOnerLiq(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpPnl() {
        return liq.getLquImpPnl().getLquImpPnl();
    }

    @Override
    public void setImpPnl(AfDecimal impPnl) {
        this.liq.getLquImpPnl().setLquImpPnl(impPnl.copy());
    }

    @Override
    public AfDecimal getImpPnlObj() {
        if (ws.getIndLiq().getImpPnl() >= 0) {
            return getImpPnl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpPnlObj(AfDecimal impPnlObj) {
        if (impPnlObj != null) {
            setImpPnl(new AfDecimal(impPnlObj, 15, 3));
            ws.getIndLiq().setImpPnl(((short)0));
        }
        else {
            ws.getIndLiq().setImpPnl(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRenK1() {
        return liq.getLquImpRenK1().getLquImpRenK1();
    }

    @Override
    public void setImpRenK1(AfDecimal impRenK1) {
        this.liq.getLquImpRenK1().setLquImpRenK1(impRenK1.copy());
    }

    @Override
    public AfDecimal getImpRenK1Obj() {
        if (ws.getIndLiq().getImpRenK1() >= 0) {
            return getImpRenK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRenK1Obj(AfDecimal impRenK1Obj) {
        if (impRenK1Obj != null) {
            setImpRenK1(new AfDecimal(impRenK1Obj, 15, 3));
            ws.getIndLiq().setImpRenK1(((short)0));
        }
        else {
            ws.getIndLiq().setImpRenK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRenK2() {
        return liq.getLquImpRenK2().getLquImpRenK2();
    }

    @Override
    public void setImpRenK2(AfDecimal impRenK2) {
        this.liq.getLquImpRenK2().setLquImpRenK2(impRenK2.copy());
    }

    @Override
    public AfDecimal getImpRenK2Obj() {
        if (ws.getIndLiq().getImpRenK2() >= 0) {
            return getImpRenK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRenK2Obj(AfDecimal impRenK2Obj) {
        if (impRenK2Obj != null) {
            setImpRenK2(new AfDecimal(impRenK2Obj, 15, 3));
            ws.getIndLiq().setImpRenK2(((short)0));
        }
        else {
            ws.getIndLiq().setImpRenK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRenK3() {
        return liq.getLquImpRenK3().getLquImpRenK3();
    }

    @Override
    public void setImpRenK3(AfDecimal impRenK3) {
        this.liq.getLquImpRenK3().setLquImpRenK3(impRenK3.copy());
    }

    @Override
    public AfDecimal getImpRenK3Obj() {
        if (ws.getIndLiq().getImpRenK3() >= 0) {
            return getImpRenK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRenK3Obj(AfDecimal impRenK3Obj) {
        if (impRenK3Obj != null) {
            setImpRenK3(new AfDecimal(impRenK3Obj, 15, 3));
            ws.getIndLiq().setImpRenK3(((short)0));
        }
        else {
            ws.getIndLiq().setImpRenK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbAddizComun() {
        return liq.getLquImpbAddizComun().getLquImpbAddizComun();
    }

    @Override
    public void setImpbAddizComun(AfDecimal impbAddizComun) {
        this.liq.getLquImpbAddizComun().setLquImpbAddizComun(impbAddizComun.copy());
    }

    @Override
    public AfDecimal getImpbAddizComunObj() {
        if (ws.getIndLiq().getImpbAddizComun() >= 0) {
            return getImpbAddizComun();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbAddizComunObj(AfDecimal impbAddizComunObj) {
        if (impbAddizComunObj != null) {
            setImpbAddizComun(new AfDecimal(impbAddizComunObj, 15, 3));
            ws.getIndLiq().setImpbAddizComun(((short)0));
        }
        else {
            ws.getIndLiq().setImpbAddizComun(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbAddizRegion() {
        return liq.getLquImpbAddizRegion().getLquImpbAddizRegion();
    }

    @Override
    public void setImpbAddizRegion(AfDecimal impbAddizRegion) {
        this.liq.getLquImpbAddizRegion().setLquImpbAddizRegion(impbAddizRegion.copy());
    }

    @Override
    public AfDecimal getImpbAddizRegionObj() {
        if (ws.getIndLiq().getImpbAddizRegion() >= 0) {
            return getImpbAddizRegion();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbAddizRegionObj(AfDecimal impbAddizRegionObj) {
        if (impbAddizRegionObj != null) {
            setImpbAddizRegion(new AfDecimal(impbAddizRegionObj, 15, 3));
            ws.getIndLiq().setImpbAddizRegion(((short)0));
        }
        else {
            ws.getIndLiq().setImpbAddizRegion(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbBolloDettC() {
        return liq.getLquImpbBolloDettC().getLquImpbBolloDettC();
    }

    @Override
    public void setImpbBolloDettC(AfDecimal impbBolloDettC) {
        this.liq.getLquImpbBolloDettC().setLquImpbBolloDettC(impbBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpbBolloDettCObj() {
        if (ws.getIndLiq().getImpbBolloDettC() >= 0) {
            return getImpbBolloDettC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbBolloDettCObj(AfDecimal impbBolloDettCObj) {
        if (impbBolloDettCObj != null) {
            setImpbBolloDettC(new AfDecimal(impbBolloDettCObj, 15, 3));
            ws.getIndLiq().setImpbBolloDettC(((short)0));
        }
        else {
            ws.getIndLiq().setImpbBolloDettC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbCnbtInpstfm() {
        return liq.getLquImpbCnbtInpstfm().getLquImpbCnbtInpstfm();
    }

    @Override
    public void setImpbCnbtInpstfm(AfDecimal impbCnbtInpstfm) {
        this.liq.getLquImpbCnbtInpstfm().setLquImpbCnbtInpstfm(impbCnbtInpstfm.copy());
    }

    @Override
    public AfDecimal getImpbCnbtInpstfmObj() {
        if (ws.getIndLiq().getImpbCnbtInpstfm() >= 0) {
            return getImpbCnbtInpstfm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbCnbtInpstfmObj(AfDecimal impbCnbtInpstfmObj) {
        if (impbCnbtInpstfmObj != null) {
            setImpbCnbtInpstfm(new AfDecimal(impbCnbtInpstfmObj, 15, 3));
            ws.getIndLiq().setImpbCnbtInpstfm(((short)0));
        }
        else {
            ws.getIndLiq().setImpbCnbtInpstfm(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbImpst252() {
        return liq.getLquImpbImpst252().getLquImpbImpst252();
    }

    @Override
    public void setImpbImpst252(AfDecimal impbImpst252) {
        this.liq.getLquImpbImpst252().setLquImpbImpst252(impbImpst252.copy());
    }

    @Override
    public AfDecimal getImpbImpst252Obj() {
        if (ws.getIndLiq().getImpbImpst252() >= 0) {
            return getImpbImpst252();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbImpst252Obj(AfDecimal impbImpst252Obj) {
        if (impbImpst252Obj != null) {
            setImpbImpst252(new AfDecimal(impbImpst252Obj, 15, 3));
            ws.getIndLiq().setImpbImpst252(((short)0));
        }
        else {
            ws.getIndLiq().setImpbImpst252(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbImpstPrvr() {
        return liq.getLquImpbImpstPrvr().getLquImpbImpstPrvr();
    }

    @Override
    public void setImpbImpstPrvr(AfDecimal impbImpstPrvr) {
        this.liq.getLquImpbImpstPrvr().setLquImpbImpstPrvr(impbImpstPrvr.copy());
    }

    @Override
    public AfDecimal getImpbImpstPrvrObj() {
        if (ws.getIndLiq().getImpbImpstPrvr() >= 0) {
            return getImpbImpstPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbImpstPrvrObj(AfDecimal impbImpstPrvrObj) {
        if (impbImpstPrvrObj != null) {
            setImpbImpstPrvr(new AfDecimal(impbImpstPrvrObj, 15, 3));
            ws.getIndLiq().setImpbImpstPrvr(((short)0));
        }
        else {
            ws.getIndLiq().setImpbImpstPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIntrSuPrest() {
        return liq.getLquImpbIntrSuPrest().getLquImpbIntrSuPrest();
    }

    @Override
    public void setImpbIntrSuPrest(AfDecimal impbIntrSuPrest) {
        this.liq.getLquImpbIntrSuPrest().setLquImpbIntrSuPrest(impbIntrSuPrest.copy());
    }

    @Override
    public AfDecimal getImpbIntrSuPrestObj() {
        if (ws.getIndLiq().getImpbIntrSuPrest() >= 0) {
            return getImpbIntrSuPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIntrSuPrestObj(AfDecimal impbIntrSuPrestObj) {
        if (impbIntrSuPrestObj != null) {
            setImpbIntrSuPrest(new AfDecimal(impbIntrSuPrestObj, 15, 3));
            ws.getIndLiq().setImpbIntrSuPrest(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIntrSuPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIrpef() {
        return liq.getLquImpbIrpef().getLquImpbIrpef();
    }

    @Override
    public void setImpbIrpef(AfDecimal impbIrpef) {
        this.liq.getLquImpbIrpef().setLquImpbIrpef(impbIrpef.copy());
    }

    @Override
    public AfDecimal getImpbIrpefObj() {
        if (ws.getIndLiq().getImpbIrpef() >= 0) {
            return getImpbIrpef();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIrpefObj(AfDecimal impbIrpefObj) {
        if (impbIrpefObj != null) {
            setImpbIrpef(new AfDecimal(impbIrpefObj, 15, 3));
            ws.getIndLiq().setImpbIrpef(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIrpef(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs1382011() {
        return liq.getLquImpbIs1382011().getLquImpbIs1382011();
    }

    @Override
    public void setImpbIs1382011(AfDecimal impbIs1382011) {
        this.liq.getLquImpbIs1382011().setLquImpbIs1382011(impbIs1382011.copy());
    }

    @Override
    public AfDecimal getImpbIs1382011Obj() {
        if (ws.getIndLiq().getImpbIs1382011() >= 0) {
            return getImpbIs1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs1382011Obj(AfDecimal impbIs1382011Obj) {
        if (impbIs1382011Obj != null) {
            setImpbIs1382011(new AfDecimal(impbIs1382011Obj, 15, 3));
            ws.getIndLiq().setImpbIs1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIs1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs662014() {
        return liq.getLquImpbIs662014().getLquImpbIs662014();
    }

    @Override
    public void setImpbIs662014(AfDecimal impbIs662014) {
        this.liq.getLquImpbIs662014().setLquImpbIs662014(impbIs662014.copy());
    }

    @Override
    public AfDecimal getImpbIs662014Obj() {
        if (ws.getIndLiq().getImpbIs662014() >= 0) {
            return getImpbIs662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIs662014Obj(AfDecimal impbIs662014Obj) {
        if (impbIs662014Obj != null) {
            setImpbIs662014(new AfDecimal(impbIs662014Obj, 15, 3));
            ws.getIndLiq().setImpbIs662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIs662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbIs() {
        return liq.getLquImpbIs().getLquImpbIs();
    }

    @Override
    public void setImpbIs(AfDecimal impbIs) {
        this.liq.getLquImpbIs().setLquImpbIs(impbIs.copy());
    }

    @Override
    public AfDecimal getImpbIsObj() {
        if (ws.getIndLiq().getImpbIs() >= 0) {
            return getImpbIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsObj(AfDecimal impbIsObj) {
        if (impbIsObj != null) {
            setImpbIs(new AfDecimal(impbIsObj, 15, 3));
            ws.getIndLiq().setImpbIs(((short)0));
        }
        else {
            ws.getIndLiq().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbTaxSep() {
        return liq.getLquImpbTaxSep().getLquImpbTaxSep();
    }

    @Override
    public void setImpbTaxSep(AfDecimal impbTaxSep) {
        this.liq.getLquImpbTaxSep().setLquImpbTaxSep(impbTaxSep.copy());
    }

    @Override
    public AfDecimal getImpbTaxSepObj() {
        if (ws.getIndLiq().getImpbTaxSep() >= 0) {
            return getImpbTaxSep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbTaxSepObj(AfDecimal impbTaxSepObj) {
        if (impbTaxSepObj != null) {
            setImpbTaxSep(new AfDecimal(impbTaxSepObj, 15, 3));
            ws.getIndLiq().setImpbTaxSep(((short)0));
        }
        else {
            ws.getIndLiq().setImpbTaxSep(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis1382011() {
        return liq.getLquImpbVis1382011().getLquImpbVis1382011();
    }

    @Override
    public void setImpbVis1382011(AfDecimal impbVis1382011) {
        this.liq.getLquImpbVis1382011().setLquImpbVis1382011(impbVis1382011.copy());
    }

    @Override
    public AfDecimal getImpbVis1382011Obj() {
        if (ws.getIndLiq().getImpbVis1382011() >= 0) {
            return getImpbVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis1382011Obj(AfDecimal impbVis1382011Obj) {
        if (impbVis1382011Obj != null) {
            setImpbVis1382011(new AfDecimal(impbVis1382011Obj, 15, 3));
            ws.getIndLiq().setImpbVis1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpbVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVis662014() {
        return liq.getLquImpbVis662014().getLquImpbVis662014();
    }

    @Override
    public void setImpbVis662014(AfDecimal impbVis662014) {
        this.liq.getLquImpbVis662014().setLquImpbVis662014(impbVis662014.copy());
    }

    @Override
    public AfDecimal getImpbVis662014Obj() {
        if (ws.getIndLiq().getImpbVis662014() >= 0) {
            return getImpbVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVis662014Obj(AfDecimal impbVis662014Obj) {
        if (impbVis662014Obj != null) {
            setImpbVis662014(new AfDecimal(impbVis662014Obj, 15, 3));
            ws.getIndLiq().setImpbVis662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpbVis662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252() {
        return liq.getLquImpst252().getLquImpst252();
    }

    @Override
    public void setImpst252(AfDecimal impst252) {
        this.liq.getLquImpst252().setLquImpst252(impst252.copy());
    }

    @Override
    public AfDecimal getImpst252Obj() {
        if (ws.getIndLiq().getImpst252() >= 0) {
            return getImpst252();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252Obj(AfDecimal impst252Obj) {
        if (impst252Obj != null) {
            setImpst252(new AfDecimal(impst252Obj, 15, 3));
            ws.getIndLiq().setImpst252(((short)0));
        }
        else {
            ws.getIndLiq().setImpst252(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstApplRilt() {
        return liq.getLquImpstApplRilt().getLquImpstApplRilt();
    }

    @Override
    public void setImpstApplRilt(AfDecimal impstApplRilt) {
        this.liq.getLquImpstApplRilt().setLquImpstApplRilt(impstApplRilt.copy());
    }

    @Override
    public AfDecimal getImpstApplRiltObj() {
        if (ws.getIndLiq().getImpstApplRilt() >= 0) {
            return getImpstApplRilt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstApplRiltObj(AfDecimal impstApplRiltObj) {
        if (impstApplRiltObj != null) {
            setImpstApplRilt(new AfDecimal(impstApplRiltObj, 15, 3));
            ws.getIndLiq().setImpstApplRilt(((short)0));
        }
        else {
            ws.getIndLiq().setImpstApplRilt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloDettC() {
        return liq.getLquImpstBolloDettC().getLquImpstBolloDettC();
    }

    @Override
    public void setImpstBolloDettC(AfDecimal impstBolloDettC) {
        this.liq.getLquImpstBolloDettC().setLquImpstBolloDettC(impstBolloDettC.copy());
    }

    @Override
    public AfDecimal getImpstBolloDettCObj() {
        if (ws.getIndLiq().getImpstBolloDettC() >= 0) {
            return getImpstBolloDettC();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloDettCObj(AfDecimal impstBolloDettCObj) {
        if (impstBolloDettCObj != null) {
            setImpstBolloDettC(new AfDecimal(impstBolloDettCObj, 15, 3));
            ws.getIndLiq().setImpstBolloDettC(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloDettC(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotAa() {
        return liq.getLquImpstBolloTotAa().getLquImpstBolloTotAa();
    }

    @Override
    public void setImpstBolloTotAa(AfDecimal impstBolloTotAa) {
        this.liq.getLquImpstBolloTotAa().setLquImpstBolloTotAa(impstBolloTotAa.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotAaObj() {
        if (ws.getIndLiq().getImpstBolloTotAa() >= 0) {
            return getImpstBolloTotAa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotAaObj(AfDecimal impstBolloTotAaObj) {
        if (impstBolloTotAaObj != null) {
            setImpstBolloTotAa(new AfDecimal(impstBolloTotAaObj, 15, 3));
            ws.getIndLiq().setImpstBolloTotAa(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloTotAa(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotSw() {
        return liq.getLquImpstBolloTotSw().getLquImpstBolloTotSw();
    }

    @Override
    public void setImpstBolloTotSw(AfDecimal impstBolloTotSw) {
        this.liq.getLquImpstBolloTotSw().setLquImpstBolloTotSw(impstBolloTotSw.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotSwObj() {
        if (ws.getIndLiq().getImpstBolloTotSw() >= 0) {
            return getImpstBolloTotSw();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotSwObj(AfDecimal impstBolloTotSwObj) {
        if (impstBolloTotSwObj != null) {
            setImpstBolloTotSw(new AfDecimal(impstBolloTotSwObj, 15, 3));
            ws.getIndLiq().setImpstBolloTotSw(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloTotSw(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotV() {
        return liq.getLquImpstBolloTotV().getLquImpstBolloTotV();
    }

    @Override
    public void setImpstBolloTotV(AfDecimal impstBolloTotV) {
        this.liq.getLquImpstBolloTotV().setLquImpstBolloTotV(impstBolloTotV.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVObj() {
        if (ws.getIndLiq().getImpstBolloTotV() >= 0) {
            return getImpstBolloTotV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj) {
        if (impstBolloTotVObj != null) {
            setImpstBolloTotV(new AfDecimal(impstBolloTotVObj, 15, 3));
            ws.getIndLiq().setImpstBolloTotV(((short)0));
        }
        else {
            ws.getIndLiq().setImpstBolloTotV(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstDaRimb() {
        return liq.getLquImpstDaRimb().getLquImpstDaRimb();
    }

    @Override
    public void setImpstDaRimb(AfDecimal impstDaRimb) {
        this.liq.getLquImpstDaRimb().setLquImpstDaRimb(impstDaRimb.copy());
    }

    @Override
    public AfDecimal getImpstDaRimbObj() {
        if (ws.getIndLiq().getImpstDaRimb() >= 0) {
            return getImpstDaRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstDaRimbObj(AfDecimal impstDaRimbObj) {
        if (impstDaRimbObj != null) {
            setImpstDaRimb(new AfDecimal(impstDaRimbObj, 15, 3));
            ws.getIndLiq().setImpstDaRimb(((short)0));
        }
        else {
            ws.getIndLiq().setImpstDaRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstIrpef() {
        return liq.getLquImpstIrpef().getLquImpstIrpef();
    }

    @Override
    public void setImpstIrpef(AfDecimal impstIrpef) {
        this.liq.getLquImpstIrpef().setLquImpstIrpef(impstIrpef.copy());
    }

    @Override
    public AfDecimal getImpstIrpefObj() {
        if (ws.getIndLiq().getImpstIrpef() >= 0) {
            return getImpstIrpef();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstIrpefObj(AfDecimal impstIrpefObj) {
        if (impstIrpefObj != null) {
            setImpstIrpef(new AfDecimal(impstIrpefObj, 15, 3));
            ws.getIndLiq().setImpstIrpef(((short)0));
        }
        else {
            ws.getIndLiq().setImpstIrpef(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvr() {
        return liq.getLquImpstPrvr().getLquImpstPrvr();
    }

    @Override
    public void setImpstPrvr(AfDecimal impstPrvr) {
        this.liq.getLquImpstPrvr().setLquImpstPrvr(impstPrvr.copy());
    }

    @Override
    public AfDecimal getImpstPrvrObj() {
        if (ws.getIndLiq().getImpstPrvr() >= 0) {
            return getImpstPrvr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrObj(AfDecimal impstPrvrObj) {
        if (impstPrvrObj != null) {
            setImpstPrvr(new AfDecimal(impstPrvrObj, 15, 3));
            ws.getIndLiq().setImpstPrvr(((short)0));
        }
        else {
            ws.getIndLiq().setImpstPrvr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost1382011() {
        return liq.getLquImpstSost1382011().getLquImpstSost1382011();
    }

    @Override
    public void setImpstSost1382011(AfDecimal impstSost1382011) {
        this.liq.getLquImpstSost1382011().setLquImpstSost1382011(impstSost1382011.copy());
    }

    @Override
    public AfDecimal getImpstSost1382011Obj() {
        if (ws.getIndLiq().getImpstSost1382011() >= 0) {
            return getImpstSost1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj) {
        if (impstSost1382011Obj != null) {
            setImpstSost1382011(new AfDecimal(impstSost1382011Obj, 15, 3));
            ws.getIndLiq().setImpstSost1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpstSost1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost662014() {
        return liq.getLquImpstSost662014().getLquImpstSost662014();
    }

    @Override
    public void setImpstSost662014(AfDecimal impstSost662014) {
        this.liq.getLquImpstSost662014().setLquImpstSost662014(impstSost662014.copy());
    }

    @Override
    public AfDecimal getImpstSost662014Obj() {
        if (ws.getIndLiq().getImpstSost662014() >= 0) {
            return getImpstSost662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost662014Obj(AfDecimal impstSost662014Obj) {
        if (impstSost662014Obj != null) {
            setImpstSost662014(new AfDecimal(impstSost662014Obj, 15, 3));
            ws.getIndLiq().setImpstSost662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpstSost662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011() {
        return liq.getLquImpstVis1382011().getLquImpstVis1382011();
    }

    @Override
    public void setImpstVis1382011(AfDecimal impstVis1382011) {
        this.liq.getLquImpstVis1382011().setLquImpstVis1382011(impstVis1382011.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011Obj() {
        if (ws.getIndLiq().getImpstVis1382011() >= 0) {
            return getImpstVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj) {
        if (impstVis1382011Obj != null) {
            setImpstVis1382011(new AfDecimal(impstVis1382011Obj, 15, 3));
            ws.getIndLiq().setImpstVis1382011(((short)0));
        }
        else {
            ws.getIndLiq().setImpstVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014() {
        return liq.getLquImpstVis662014().getLquImpstVis662014();
    }

    @Override
    public void setImpstVis662014(AfDecimal impstVis662014) {
        this.liq.getLquImpstVis662014().setLquImpstVis662014(impstVis662014.copy());
    }

    @Override
    public AfDecimal getImpstVis662014Obj() {
        if (ws.getIndLiq().getImpstVis662014() >= 0) {
            return getImpstVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014Obj(AfDecimal impstVis662014Obj) {
        if (impstVis662014Obj != null) {
            setImpstVis662014(new AfDecimal(impstVis662014Obj, 15, 3));
            ws.getIndLiq().setImpstVis662014(((short)0));
        }
        else {
            ws.getIndLiq().setImpstVis662014(((short)-1));
        }
    }

    @Override
    public int getLdbv2271IdOgg() {
        throw new FieldNotMappedException("ldbv2271IdOgg");
    }

    @Override
    public void setLdbv2271IdOgg(int ldbv2271IdOgg) {
        throw new FieldNotMappedException("ldbv2271IdOgg");
    }

    @Override
    public AfDecimal getLdbv2271ImpLrdLiqto() {
        throw new FieldNotMappedException("ldbv2271ImpLrdLiqto");
    }

    @Override
    public void setLdbv2271ImpLrdLiqto(AfDecimal ldbv2271ImpLrdLiqto) {
        throw new FieldNotMappedException("ldbv2271ImpLrdLiqto");
    }

    @Override
    public AfDecimal getLdbv2271ImpLrdLiqtoObj() {
        return getLdbv2271ImpLrdLiqto();
    }

    @Override
    public void setLdbv2271ImpLrdLiqtoObj(AfDecimal ldbv2271ImpLrdLiqtoObj) {
        setLdbv2271ImpLrdLiqto(new AfDecimal(ldbv2271ImpLrdLiqtoObj, 15, 3));
    }

    @Override
    public String getLdbv2271TpLiq() {
        throw new FieldNotMappedException("ldbv2271TpLiq");
    }

    @Override
    public void setLdbv2271TpLiq(String ldbv2271TpLiq) {
        throw new FieldNotMappedException("ldbv2271TpLiq");
    }

    @Override
    public String getLdbv2271TpOgg() {
        throw new FieldNotMappedException("ldbv2271TpOgg");
    }

    @Override
    public void setLdbv2271TpOgg(String ldbv2271TpOgg) {
        throw new FieldNotMappedException("ldbv2271TpOgg");
    }

    @Override
    public long getLquDsRiga() {
        return liq.getLquDsRiga();
    }

    @Override
    public void setLquDsRiga(long lquDsRiga) {
        this.liq.setLquDsRiga(lquDsRiga);
    }

    @Override
    public int getLquIdMoviCrz() {
        return liq.getLquIdMoviCrz();
    }

    @Override
    public void setLquIdMoviCrz(int lquIdMoviCrz) {
        this.liq.setLquIdMoviCrz(lquIdMoviCrz);
    }

    @Override
    public int getLquIdOgg() {
        return liq.getLquIdOgg();
    }

    @Override
    public void setLquIdOgg(int lquIdOgg) {
        this.liq.setLquIdOgg(lquIdOgg);
    }

    @Override
    public String getLquTpOgg() {
        return liq.getLquTpOgg();
    }

    @Override
    public void setLquTpOgg(String lquTpOgg) {
        this.liq.setLquTpOgg(lquTpOgg);
    }

    @Override
    public AfDecimal getMontDal2007() {
        return liq.getLquMontDal2007().getLquMontDal2007();
    }

    @Override
    public void setMontDal2007(AfDecimal montDal2007) {
        this.liq.getLquMontDal2007().setLquMontDal2007(montDal2007.copy());
    }

    @Override
    public AfDecimal getMontDal2007Obj() {
        if (ws.getIndLiq().getMontDal2007() >= 0) {
            return getMontDal2007();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontDal2007Obj(AfDecimal montDal2007Obj) {
        if (montDal2007Obj != null) {
            setMontDal2007(new AfDecimal(montDal2007Obj, 15, 3));
            ws.getIndLiq().setMontDal2007(((short)0));
        }
        else {
            ws.getIndLiq().setMontDal2007(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2000() {
        return liq.getLquMontEnd2000().getLquMontEnd2000();
    }

    @Override
    public void setMontEnd2000(AfDecimal montEnd2000) {
        this.liq.getLquMontEnd2000().setLquMontEnd2000(montEnd2000.copy());
    }

    @Override
    public AfDecimal getMontEnd2000Obj() {
        if (ws.getIndLiq().getMontEnd2000() >= 0) {
            return getMontEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2000Obj(AfDecimal montEnd2000Obj) {
        if (montEnd2000Obj != null) {
            setMontEnd2000(new AfDecimal(montEnd2000Obj, 15, 3));
            ws.getIndLiq().setMontEnd2000(((short)0));
        }
        else {
            ws.getIndLiq().setMontEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getMontEnd2006() {
        return liq.getLquMontEnd2006().getLquMontEnd2006();
    }

    @Override
    public void setMontEnd2006(AfDecimal montEnd2006) {
        this.liq.getLquMontEnd2006().setLquMontEnd2006(montEnd2006.copy());
    }

    @Override
    public AfDecimal getMontEnd2006Obj() {
        if (ws.getIndLiq().getMontEnd2006() >= 0) {
            return getMontEnd2006();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMontEnd2006Obj(AfDecimal montEnd2006Obj) {
        if (montEnd2006Obj != null) {
            setMontEnd2006(new AfDecimal(montEnd2006Obj, 15, 3));
            ws.getIndLiq().setMontEnd2006(((short)0));
        }
        else {
            ws.getIndLiq().setMontEnd2006(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAbbTitStat() {
        return liq.getLquPcAbbTitStat().getLquPcAbbTitStat();
    }

    @Override
    public void setPcAbbTitStat(AfDecimal pcAbbTitStat) {
        this.liq.getLquPcAbbTitStat().setLquPcAbbTitStat(pcAbbTitStat.copy());
    }

    @Override
    public AfDecimal getPcAbbTitStatObj() {
        if (ws.getIndLiq().getPcAbbTitStat() >= 0) {
            return getPcAbbTitStat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAbbTitStatObj(AfDecimal pcAbbTitStatObj) {
        if (pcAbbTitStatObj != null) {
            setPcAbbTitStat(new AfDecimal(pcAbbTitStatObj, 6, 3));
            ws.getIndLiq().setPcAbbTitStat(((short)0));
        }
        else {
            ws.getIndLiq().setPcAbbTitStat(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAbbTs662014() {
        return liq.getLquPcAbbTs662014().getLquPcAbbTs662014();
    }

    @Override
    public void setPcAbbTs662014(AfDecimal pcAbbTs662014) {
        this.liq.getLquPcAbbTs662014().setLquPcAbbTs662014(pcAbbTs662014.copy());
    }

    @Override
    public AfDecimal getPcAbbTs662014Obj() {
        if (ws.getIndLiq().getPcAbbTs662014() >= 0) {
            return getPcAbbTs662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAbbTs662014Obj(AfDecimal pcAbbTs662014Obj) {
        if (pcAbbTs662014Obj != null) {
            setPcAbbTs662014(new AfDecimal(pcAbbTs662014Obj, 6, 3));
            ws.getIndLiq().setPcAbbTs662014(((short)0));
        }
        else {
            ws.getIndLiq().setPcAbbTs662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRen() {
        return liq.getLquPcRen().getLquPcRen();
    }

    @Override
    public void setPcRen(AfDecimal pcRen) {
        this.liq.getLquPcRen().setLquPcRen(pcRen.copy());
    }

    @Override
    public AfDecimal getPcRenK1() {
        return liq.getLquPcRenK1().getLquPcRenK1();
    }

    @Override
    public void setPcRenK1(AfDecimal pcRenK1) {
        this.liq.getLquPcRenK1().setLquPcRenK1(pcRenK1.copy());
    }

    @Override
    public AfDecimal getPcRenK1Obj() {
        if (ws.getIndLiq().getPcRenK1() >= 0) {
            return getPcRenK1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenK1Obj(AfDecimal pcRenK1Obj) {
        if (pcRenK1Obj != null) {
            setPcRenK1(new AfDecimal(pcRenK1Obj, 6, 3));
            ws.getIndLiq().setPcRenK1(((short)0));
        }
        else {
            ws.getIndLiq().setPcRenK1(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRenK2() {
        return liq.getLquPcRenK2().getLquPcRenK2();
    }

    @Override
    public void setPcRenK2(AfDecimal pcRenK2) {
        this.liq.getLquPcRenK2().setLquPcRenK2(pcRenK2.copy());
    }

    @Override
    public AfDecimal getPcRenK2Obj() {
        if (ws.getIndLiq().getPcRenK2() >= 0) {
            return getPcRenK2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenK2Obj(AfDecimal pcRenK2Obj) {
        if (pcRenK2Obj != null) {
            setPcRenK2(new AfDecimal(pcRenK2Obj, 6, 3));
            ws.getIndLiq().setPcRenK2(((short)0));
        }
        else {
            ws.getIndLiq().setPcRenK2(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRenK3() {
        return liq.getLquPcRenK3().getLquPcRenK3();
    }

    @Override
    public void setPcRenK3(AfDecimal pcRenK3) {
        this.liq.getLquPcRenK3().setLquPcRenK3(pcRenK3.copy());
    }

    @Override
    public AfDecimal getPcRenK3Obj() {
        if (ws.getIndLiq().getPcRenK3() >= 0) {
            return getPcRenK3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenK3Obj(AfDecimal pcRenK3Obj) {
        if (pcRenK3Obj != null) {
            setPcRenK3(new AfDecimal(pcRenK3Obj, 6, 3));
            ws.getIndLiq().setPcRenK3(((short)0));
        }
        else {
            ws.getIndLiq().setPcRenK3(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRenObj() {
        if (ws.getIndLiq().getPcRen() >= 0) {
            return getPcRen();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRenObj(AfDecimal pcRenObj) {
        if (pcRenObj != null) {
            setPcRen(new AfDecimal(pcRenObj, 6, 3));
            ws.getIndLiq().setPcRen(((short)0));
        }
        else {
            ws.getIndLiq().setPcRen(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRiscParz() {
        return liq.getLquPcRiscParz().getLquPcRiscParz();
    }

    @Override
    public void setPcRiscParz(AfDecimal pcRiscParz) {
        this.liq.getLquPcRiscParz().setLquPcRiscParz(pcRiscParz.copy());
    }

    @Override
    public AfDecimal getPcRiscParzObj() {
        if (ws.getIndLiq().getPcRiscParz() >= 0) {
            return getPcRiscParz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRiscParzObj(AfDecimal pcRiscParzObj) {
        if (pcRiscParzObj != null) {
            setPcRiscParz(new AfDecimal(pcRiscParzObj, 12, 5));
            ws.getIndLiq().setPcRiscParz(((short)0));
        }
        else {
            ws.getIndLiq().setPcRiscParz(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return liq.getLquRisMat().getLquRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.liq.getLquRisMat().setLquRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndLiq().getRisMat() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndLiq().setRisMat(((short)0));
        }
        else {
            ws.getIndLiq().setRisMat(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSpe() {
        return liq.getLquRisSpe().getLquRisSpe();
    }

    @Override
    public void setRisSpe(AfDecimal risSpe) {
        this.liq.getLquRisSpe().setLquRisSpe(risSpe.copy());
    }

    @Override
    public AfDecimal getRisSpeObj() {
        if (ws.getIndLiq().getRisSpe() >= 0) {
            return getRisSpe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSpeObj(AfDecimal risSpeObj) {
        if (risSpeObj != null) {
            setRisSpe(new AfDecimal(risSpeObj, 15, 3));
            ws.getIndLiq().setRisSpe(((short)0));
        }
        else {
            ws.getIndLiq().setRisSpe(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeRcs() {
        return liq.getLquSpeRcs().getLquSpeRcs();
    }

    @Override
    public void setSpeRcs(AfDecimal speRcs) {
        this.liq.getLquSpeRcs().setLquSpeRcs(speRcs.copy());
    }

    @Override
    public AfDecimal getSpeRcsObj() {
        if (ws.getIndLiq().getSpeRcs() >= 0) {
            return getSpeRcs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeRcsObj(AfDecimal speRcsObj) {
        if (speRcsObj != null) {
            setSpeRcs(new AfDecimal(speRcsObj, 15, 3));
            ws.getIndLiq().setSpeRcs(((short)0));
        }
        else {
            ws.getIndLiq().setSpeRcs(((short)-1));
        }
    }

    @Override
    public AfDecimal getTaxSep() {
        return liq.getLquTaxSep().getLquTaxSep();
    }

    @Override
    public void setTaxSep(AfDecimal taxSep) {
        this.liq.getLquTaxSep().setLquTaxSep(taxSep.copy());
    }

    @Override
    public AfDecimal getTaxSepObj() {
        if (ws.getIndLiq().getTaxSep() >= 0) {
            return getTaxSep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxSepObj(AfDecimal taxSepObj) {
        if (taxSepObj != null) {
            setTaxSep(new AfDecimal(taxSepObj, 15, 3));
            ws.getIndLiq().setTaxSep(((short)0));
        }
        else {
            ws.getIndLiq().setTaxSep(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasMggSin() {
        return liq.getLquTotIasMggSin().getLquTotIasMggSin();
    }

    @Override
    public void setTotIasMggSin(AfDecimal totIasMggSin) {
        this.liq.getLquTotIasMggSin().setLquTotIasMggSin(totIasMggSin.copy());
    }

    @Override
    public AfDecimal getTotIasMggSinObj() {
        if (ws.getIndLiq().getTotIasMggSin() >= 0) {
            return getTotIasMggSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasMggSinObj(AfDecimal totIasMggSinObj) {
        if (totIasMggSinObj != null) {
            setTotIasMggSin(new AfDecimal(totIasMggSinObj, 15, 3));
            ws.getIndLiq().setTotIasMggSin(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasMggSin(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasOnerPrvnt() {
        return liq.getLquTotIasOnerPrvnt().getLquTotIasOnerPrvnt();
    }

    @Override
    public void setTotIasOnerPrvnt(AfDecimal totIasOnerPrvnt) {
        this.liq.getLquTotIasOnerPrvnt().setLquTotIasOnerPrvnt(totIasOnerPrvnt.copy());
    }

    @Override
    public AfDecimal getTotIasOnerPrvntObj() {
        if (ws.getIndLiq().getTotIasOnerPrvnt() >= 0) {
            return getTotIasOnerPrvnt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasOnerPrvntObj(AfDecimal totIasOnerPrvntObj) {
        if (totIasOnerPrvntObj != null) {
            setTotIasOnerPrvnt(new AfDecimal(totIasOnerPrvntObj, 15, 3));
            ws.getIndLiq().setTotIasOnerPrvnt(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasOnerPrvnt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasPnl() {
        return liq.getLquTotIasPnl().getLquTotIasPnl();
    }

    @Override
    public void setTotIasPnl(AfDecimal totIasPnl) {
        this.liq.getLquTotIasPnl().setLquTotIasPnl(totIasPnl.copy());
    }

    @Override
    public AfDecimal getTotIasPnlObj() {
        if (ws.getIndLiq().getTotIasPnl() >= 0) {
            return getTotIasPnl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasPnlObj(AfDecimal totIasPnlObj) {
        if (totIasPnlObj != null) {
            setTotIasPnl(new AfDecimal(totIasPnlObj, 15, 3));
            ws.getIndLiq().setTotIasPnl(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasPnl(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIasRstDpst() {
        return liq.getLquTotIasRstDpst().getLquTotIasRstDpst();
    }

    @Override
    public void setTotIasRstDpst(AfDecimal totIasRstDpst) {
        this.liq.getLquTotIasRstDpst().setLquTotIasRstDpst(totIasRstDpst.copy());
    }

    @Override
    public AfDecimal getTotIasRstDpstObj() {
        if (ws.getIndLiq().getTotIasRstDpst() >= 0) {
            return getTotIasRstDpst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIasRstDpstObj(AfDecimal totIasRstDpstObj) {
        if (totIasRstDpstObj != null) {
            setTotIasRstDpst(new AfDecimal(totIasRstDpstObj, 15, 3));
            ws.getIndLiq().setTotIasRstDpst(((short)0));
        }
        else {
            ws.getIndLiq().setTotIasRstDpst(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpIntrPrest() {
        return liq.getLquTotImpIntrPrest().getLquTotImpIntrPrest();
    }

    @Override
    public void setTotImpIntrPrest(AfDecimal totImpIntrPrest) {
        this.liq.getLquTotImpIntrPrest().setLquTotImpIntrPrest(totImpIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotImpIntrPrestObj() {
        if (ws.getIndLiq().getTotImpIntrPrest() >= 0) {
            return getTotImpIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpIntrPrestObj(AfDecimal totImpIntrPrestObj) {
        if (totImpIntrPrestObj != null) {
            setTotImpIntrPrest(new AfDecimal(totImpIntrPrestObj, 15, 3));
            ws.getIndLiq().setTotImpIntrPrest(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpIs() {
        return liq.getLquTotImpIs().getLquTotImpIs();
    }

    @Override
    public void setTotImpIs(AfDecimal totImpIs) {
        this.liq.getLquTotImpIs().setLquTotImpIs(totImpIs.copy());
    }

    @Override
    public AfDecimal getTotImpIsObj() {
        if (ws.getIndLiq().getTotImpIs() >= 0) {
            return getTotImpIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpIsObj(AfDecimal totImpIsObj) {
        if (totImpIsObj != null) {
            setTotImpIs(new AfDecimal(totImpIsObj, 15, 3));
            ws.getIndLiq().setTotImpIs(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpLrdLiqto() {
        return liq.getLquTotImpLrdLiqto().getLquTotImpLrdLiqto();
    }

    @Override
    public void setTotImpLrdLiqto(AfDecimal totImpLrdLiqto) {
        this.liq.getLquTotImpLrdLiqto().setLquTotImpLrdLiqto(totImpLrdLiqto.copy());
    }

    @Override
    public AfDecimal getTotImpLrdLiqtoObj() {
        if (ws.getIndLiq().getTotImpLrdLiqto() >= 0) {
            return getTotImpLrdLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpLrdLiqtoObj(AfDecimal totImpLrdLiqtoObj) {
        if (totImpLrdLiqtoObj != null) {
            setTotImpLrdLiqto(new AfDecimal(totImpLrdLiqtoObj, 15, 3));
            ws.getIndLiq().setTotImpLrdLiqto(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpLrdLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpNetLiqto() {
        return liq.getLquTotImpNetLiqto().getLquTotImpNetLiqto();
    }

    @Override
    public void setTotImpNetLiqto(AfDecimal totImpNetLiqto) {
        this.liq.getLquTotImpNetLiqto().setLquTotImpNetLiqto(totImpNetLiqto.copy());
    }

    @Override
    public AfDecimal getTotImpNetLiqtoObj() {
        if (ws.getIndLiq().getTotImpNetLiqto() >= 0) {
            return getTotImpNetLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpNetLiqtoObj(AfDecimal totImpNetLiqtoObj) {
        if (totImpNetLiqtoObj != null) {
            setTotImpNetLiqto(new AfDecimal(totImpNetLiqtoObj, 15, 3));
            ws.getIndLiq().setTotImpNetLiqto(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpNetLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpPrest() {
        return liq.getLquTotImpPrest().getLquTotImpPrest();
    }

    @Override
    public void setTotImpPrest(AfDecimal totImpPrest) {
        this.liq.getLquTotImpPrest().setLquTotImpPrest(totImpPrest.copy());
    }

    @Override
    public AfDecimal getTotImpPrestObj() {
        if (ws.getIndLiq().getTotImpPrest() >= 0) {
            return getTotImpPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpPrestObj(AfDecimal totImpPrestObj) {
        if (totImpPrestObj != null) {
            setTotImpPrest(new AfDecimal(totImpPrestObj, 15, 3));
            ws.getIndLiq().setTotImpPrest(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRimb() {
        return liq.getLquTotImpRimb().getLquTotImpRimb();
    }

    @Override
    public void setTotImpRimb(AfDecimal totImpRimb) {
        this.liq.getLquTotImpRimb().setLquTotImpRimb(totImpRimb.copy());
    }

    @Override
    public AfDecimal getTotImpRimbObj() {
        if (ws.getIndLiq().getTotImpRimb() >= 0) {
            return getTotImpRimb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRimbObj(AfDecimal totImpRimbObj) {
        if (totImpRimbObj != null) {
            setTotImpRimb(new AfDecimal(totImpRimbObj, 15, 3));
            ws.getIndLiq().setTotImpRimb(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRimb(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRitAcc() {
        return liq.getLquTotImpRitAcc().getLquTotImpRitAcc();
    }

    @Override
    public void setTotImpRitAcc(AfDecimal totImpRitAcc) {
        this.liq.getLquTotImpRitAcc().setLquTotImpRitAcc(totImpRitAcc.copy());
    }

    @Override
    public AfDecimal getTotImpRitAccObj() {
        if (ws.getIndLiq().getTotImpRitAcc() >= 0) {
            return getTotImpRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRitAccObj(AfDecimal totImpRitAccObj) {
        if (totImpRitAccObj != null) {
            setTotImpRitAcc(new AfDecimal(totImpRitAccObj, 15, 3));
            ws.getIndLiq().setTotImpRitAcc(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRitTfr() {
        return liq.getLquTotImpRitTfr().getLquTotImpRitTfr();
    }

    @Override
    public void setTotImpRitTfr(AfDecimal totImpRitTfr) {
        this.liq.getLquTotImpRitTfr().setLquTotImpRitTfr(totImpRitTfr.copy());
    }

    @Override
    public AfDecimal getTotImpRitTfrObj() {
        if (ws.getIndLiq().getTotImpRitTfr() >= 0) {
            return getTotImpRitTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRitTfrObj(AfDecimal totImpRitTfrObj) {
        if (totImpRitTfrObj != null) {
            setTotImpRitTfr(new AfDecimal(totImpRitTfrObj, 15, 3));
            ws.getIndLiq().setTotImpRitTfr(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRitTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpRitVis() {
        return liq.getLquTotImpRitVis().getLquTotImpRitVis();
    }

    @Override
    public void setTotImpRitVis(AfDecimal totImpRitVis) {
        this.liq.getLquTotImpRitVis().setLquTotImpRitVis(totImpRitVis.copy());
    }

    @Override
    public AfDecimal getTotImpRitVisObj() {
        if (ws.getIndLiq().getTotImpRitVis() >= 0) {
            return getTotImpRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpRitVisObj(AfDecimal totImpRitVisObj) {
        if (totImpRitVisObj != null) {
            setTotImpRitVis(new AfDecimal(totImpRitVisObj, 15, 3));
            ws.getIndLiq().setTotImpRitVis(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpUti() {
        return liq.getLquTotImpUti().getLquTotImpUti();
    }

    @Override
    public void setTotImpUti(AfDecimal totImpUti) {
        this.liq.getLquTotImpUti().setLquTotImpUti(totImpUti.copy());
    }

    @Override
    public AfDecimal getTotImpUtiObj() {
        if (ws.getIndLiq().getTotImpUti() >= 0) {
            return getTotImpUti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpUtiObj(AfDecimal totImpUtiObj) {
        if (totImpUtiObj != null) {
            setTotImpUti(new AfDecimal(totImpUtiObj, 15, 3));
            ws.getIndLiq().setTotImpUti(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpUti(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpbAcc() {
        return liq.getLquTotImpbAcc().getLquTotImpbAcc();
    }

    @Override
    public void setTotImpbAcc(AfDecimal totImpbAcc) {
        this.liq.getLquTotImpbAcc().setLquTotImpbAcc(totImpbAcc.copy());
    }

    @Override
    public AfDecimal getTotImpbAccObj() {
        if (ws.getIndLiq().getTotImpbAcc() >= 0) {
            return getTotImpbAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpbAccObj(AfDecimal totImpbAccObj) {
        if (totImpbAccObj != null) {
            setTotImpbAcc(new AfDecimal(totImpbAccObj, 15, 3));
            ws.getIndLiq().setTotImpbAcc(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpbAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpbTfr() {
        return liq.getLquTotImpbTfr().getLquTotImpbTfr();
    }

    @Override
    public void setTotImpbTfr(AfDecimal totImpbTfr) {
        this.liq.getLquTotImpbTfr().setLquTotImpbTfr(totImpbTfr.copy());
    }

    @Override
    public AfDecimal getTotImpbTfrObj() {
        if (ws.getIndLiq().getTotImpbTfr() >= 0) {
            return getTotImpbTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpbTfrObj(AfDecimal totImpbTfrObj) {
        if (totImpbTfrObj != null) {
            setTotImpbTfr(new AfDecimal(totImpbTfrObj, 15, 3));
            ws.getIndLiq().setTotImpbTfr(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpbTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotImpbVis() {
        return liq.getLquTotImpbVis().getLquTotImpbVis();
    }

    @Override
    public void setTotImpbVis(AfDecimal totImpbVis) {
        this.liq.getLquTotImpbVis().setLquTotImpbVis(totImpbVis.copy());
    }

    @Override
    public AfDecimal getTotImpbVisObj() {
        if (ws.getIndLiq().getTotImpbVis() >= 0) {
            return getTotImpbVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotImpbVisObj(AfDecimal totImpbVisObj) {
        if (totImpbVisObj != null) {
            setTotImpbVis(new AfDecimal(totImpbVisObj, 15, 3));
            ws.getIndLiq().setTotImpbVis(((short)0));
        }
        else {
            ws.getIndLiq().setTotImpbVis(((short)-1));
        }
    }

    @Override
    public String getTpCausAntic() {
        return liq.getLquTpCausAntic();
    }

    @Override
    public void setTpCausAntic(String tpCausAntic) {
        this.liq.setLquTpCausAntic(tpCausAntic);
    }

    @Override
    public String getTpCausAnticObj() {
        if (ws.getIndLiq().getTpCausAntic() >= 0) {
            return getTpCausAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausAnticObj(String tpCausAnticObj) {
        if (tpCausAnticObj != null) {
            setTpCausAntic(tpCausAnticObj);
            ws.getIndLiq().setTpCausAntic(((short)0));
        }
        else {
            ws.getIndLiq().setTpCausAntic(((short)-1));
        }
    }

    @Override
    public String getTpLiq() {
        return liq.getLquTpLiq();
    }

    @Override
    public void setTpLiq(String tpLiq) {
        this.liq.setLquTpLiq(tpLiq);
    }

    @Override
    public short getTpMetRisc() {
        return liq.getLquTpMetRisc().getLquTpMetRisc();
    }

    @Override
    public void setTpMetRisc(short tpMetRisc) {
        this.liq.getLquTpMetRisc().setLquTpMetRisc(tpMetRisc);
    }

    @Override
    public Short getTpMetRiscObj() {
        if (ws.getIndLiq().getTpMetRisc() >= 0) {
            return ((Short)getTpMetRisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMetRiscObj(Short tpMetRiscObj) {
        if (tpMetRiscObj != null) {
            setTpMetRisc(((short)tpMetRiscObj));
            ws.getIndLiq().setTpMetRisc(((short)0));
        }
        else {
            ws.getIndLiq().setTpMetRisc(((short)-1));
        }
    }

    @Override
    public String getTpMezPag() {
        return liq.getLquTpMezPag();
    }

    @Override
    public void setTpMezPag(String tpMezPag) {
        this.liq.setLquTpMezPag(tpMezPag);
    }

    @Override
    public String getTpMezPagObj() {
        if (ws.getIndLiq().getTpMezPag() >= 0) {
            return getTpMezPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagObj(String tpMezPagObj) {
        if (tpMezPagObj != null) {
            setTpMezPag(tpMezPagObj);
            ws.getIndLiq().setTpMezPag(((short)0));
        }
        else {
            ws.getIndLiq().setTpMezPag(((short)-1));
        }
    }

    @Override
    public String getTpRimb() {
        return liq.getLquTpRimb();
    }

    @Override
    public void setTpRimb(String tpRimb) {
        this.liq.setLquTpRimb(tpRimb);
    }

    @Override
    public String getTpRisc() {
        return liq.getLquTpRisc();
    }

    @Override
    public void setTpRisc(String tpRisc) {
        this.liq.setLquTpRisc(tpRisc);
    }

    @Override
    public String getTpRiscObj() {
        if (ws.getIndLiq().getTpRisc() >= 0) {
            return getTpRisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRiscObj(String tpRiscObj) {
        if (tpRiscObj != null) {
            setTpRisc(tpRiscObj);
            ws.getIndLiq().setTpRisc(((short)0));
        }
        else {
            ws.getIndLiq().setTpRisc(((short)-1));
        }
    }

    @Override
    public String getTpSin() {
        return liq.getLquTpSin();
    }

    @Override
    public void setTpSin(String tpSin) {
        this.liq.setLquTpSin(tpSin);
    }

    @Override
    public String getTpSinObj() {
        if (ws.getIndLiq().getTpSin() >= 0) {
            return getTpSin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpSinObj(String tpSinObj) {
        if (tpSinObj != null) {
            setTpSin(tpSinObj);
            ws.getIndLiq().setTpSin(((short)0));
        }
        else {
            ws.getIndLiq().setTpSin(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
