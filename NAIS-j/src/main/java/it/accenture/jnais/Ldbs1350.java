package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.GarStatOggBusDao;
import it.accenture.jnais.commons.data.to.IGarStatOggBus;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Gar;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1350Data;
import it.accenture.jnais.ws.redefines.GrzAaPagPreUni;
import it.accenture.jnais.ws.redefines.GrzAaStab;
import it.accenture.jnais.ws.redefines.GrzDtDecor;
import it.accenture.jnais.ws.redefines.GrzDtEndCarz;
import it.accenture.jnais.ws.redefines.GrzDtIniValTar;
import it.accenture.jnais.ws.redefines.GrzDtPresc;
import it.accenture.jnais.ws.redefines.GrzDtScad;
import it.accenture.jnais.ws.redefines.GrzDtVarzTpIas;
import it.accenture.jnais.ws.redefines.GrzDurAa;
import it.accenture.jnais.ws.redefines.GrzDurGg;
import it.accenture.jnais.ws.redefines.GrzDurMm;
import it.accenture.jnais.ws.redefines.GrzEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAScad;
import it.accenture.jnais.ws.redefines.GrzEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.GrzFrazDecrCpt;
import it.accenture.jnais.ws.redefines.GrzFrazIniErogRen;
import it.accenture.jnais.ws.redefines.GrzId1oAssto;
import it.accenture.jnais.ws.redefines.GrzId2oAssto;
import it.accenture.jnais.ws.redefines.GrzId3oAssto;
import it.accenture.jnais.ws.redefines.GrzIdAdes;
import it.accenture.jnais.ws.redefines.GrzIdMoviChiu;
import it.accenture.jnais.ws.redefines.GrzMm1oRat;
import it.accenture.jnais.ws.redefines.GrzMmPagPreUni;
import it.accenture.jnais.ws.redefines.GrzNumAaPagPre;
import it.accenture.jnais.ws.redefines.GrzPc1oRat;
import it.accenture.jnais.ws.redefines.GrzPcOpz;
import it.accenture.jnais.ws.redefines.GrzPcRevrsb;
import it.accenture.jnais.ws.redefines.GrzPcRipPre;
import it.accenture.jnais.ws.redefines.GrzTpGar;
import it.accenture.jnais.ws.redefines.GrzTpInvst;
import it.accenture.jnais.ws.redefines.GrzTsStabLimitata;

/**Original name: LDBS1350<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  .
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         :
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1350 extends Program implements IGarStatOggBus {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private GarStatOggBusDao garStatOggBusDao = new GarStatOggBusDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1350Data ws = new Ldbs1350Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: GAR
    private Gar gar;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1350_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Gar gar) {
        this.idsv0003 = idsv0003;
        this.gar = gar;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //               END-EVALUATE
            //           ELSE
            //            END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE ALSO TRUE
                //              WHEN STATO ALSO CAUSALE
                //                   PERFORM A400-ELABORA-EFF       THRU A400-EX
                //              WHEN NOT-STATO ALSO CAUSALE
                //                   PERFORM C400-ELABORA-EFF-NS    THRU C400-EX
                //              WHEN STATO ALSO NOT-CAUSALE
                //                   PERFORM E400-ELABORA-EFF-NC    THRU E400-EX
                //              WHEN NOT-STATO ALSO NOT-CAUSALE
                //                   PERFORM G400-ELABORA-EFF-NS-NC THRU G400-EX
                //            END-EVALUATE
                if (ws.getFlagStato().isStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: PERFORM A400-ELABORA-EFF       THRU A400-EX
                    a400ElaboraEff();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: PERFORM C400-ELABORA-EFF-NS    THRU C400-EX
                    c400ElaboraEffNs();
                }
                else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM E400-ELABORA-EFF-NC    THRU E400-EX
                    e400ElaboraEffNc();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM G400-ELABORA-EFF-NS-NC THRU G400-EX
                    g400ElaboraEffNsNc();
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE:  IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //            ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-IF
                // COB_CODE: EVALUATE TRUE ALSO TRUE
                //              WHEN STATO ALSO CAUSALE
                //                   PERFORM B400-ELABORA-CPZ       THRU B400-EX
                //              WHEN NOT-STATO ALSO CAUSALE
                //                   PERFORM D400-ELABORA-CPZ-NS    THRU D400-EX
                //              WHEN STATO ALSO NOT-CAUSALE
                //                   PERFORM F400-ELABORA-CPZ-NC    THRU F400-EX
                //              WHEN NOT-STATO ALSO NOT-CAUSALE
                //                   PERFORM H400-ELABORA-CPZ-NS-NC THRU H400-EX
                //            END-EVALUATE
                if (ws.getFlagStato().isStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: PERFORM B400-ELABORA-CPZ       THRU B400-EX
                    b400ElaboraCpz();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: PERFORM D400-ELABORA-CPZ-NS    THRU D400-EX
                    d400ElaboraCpzNs();
                }
                else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM F400-ELABORA-CPZ-NC    THRU F400-EX
                    f400ElaboraCpzNc();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM H400-ELABORA-CPZ-NS-NC THRU H400-EX
                    h400ElaboraCpzNsNc();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1350 getInstance() {
        return ((Ldbs1350)Programs.getInstance(Ldbs1350.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1350'               TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1350");
        // COB_CODE: MOVE 'GAR-STB'                TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("GAR-STB");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE 000000000                TO WK-ID-ADES-DA
        //                                            WK-ID-GAR-DA
        ws.setWkIdAdesDa(0);
        ws.setWkIdGarDa(0);
        // COB_CODE: MOVE 999999999                TO WK-ID-ADES-A
        //                                            WK-ID-GAR-A
        ws.setWkIdAdesA(999999999);
        ws.setWkIdGarA(999999999);
        // COB_CODE: SET STATO                     TO TRUE
        ws.getFlagStato().setStato();
        // COB_CODE: SET CAUSALE                   TO TRUE
        ws.getFlagCausale().setCausale();
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV1351
        ws.getLdbv1351().setLdbv1351Formatted(idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM V010-VERIFICA-WHERE-COND THRU V010-EX
        v010VerificaWhereCond();
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: V010-VERIFICA-WHERE-COND<br>*/
    private void v010VerificaWhereCond() {
        // COB_CODE: IF GRZ-ID-ADES IS NUMERIC AND
        //              GRZ-ID-ADES NOT = ZERO
        //                                       WK-ID-ADES-A
        //           END-IF.
        if (Functions.isNumber(gar.getGrzIdAdes().getGrzIdAdes()) && gar.getGrzIdAdes().getGrzIdAdes() != 0) {
            // COB_CODE: MOVE GRZ-ID-ADES      TO WK-ID-ADES-DA
            //                                    WK-ID-ADES-A
            ws.setWkIdAdesDa(gar.getGrzIdAdes().getGrzIdAdes());
            ws.setWkIdAdesA(gar.getGrzIdAdes().getGrzIdAdes());
        }
        // COB_CODE: IF GRZ-ID-GAR IS NUMERIC AND
        //              GRZ-ID-GAR NOT = ZERO
        //                                       WK-ID-GAR-A
        //           END-IF.
        if (Functions.isNumber(gar.getGrzIdGar()) && gar.getGrzIdGar() != 0) {
            // COB_CODE: MOVE GRZ-ID-GAR       TO WK-ID-GAR-DA
            //                                    WK-ID-GAR-A
            ws.setWkIdGarDa(gar.getGrzIdGar());
            ws.setWkIdGarA(gar.getGrzIdGar());
        }
        // COB_CODE: PERFORM V020-VERIF-OPERATORI-LOGICI THRU V020-EX.
        v020VerifOperatoriLogici();
    }

    /**Original name: V020-VERIF-OPERATORI-LOGICI<br>*/
    private void v020VerifOperatoriLogici() {
        // COB_CODE: IF LDBV1351-OPER-LOG-STAT-BUS = NEGAZIONE
        //              SET NOT-STATO               TO TRUE
        //           END-IF
        if (Conditions.eq(ws.getLdbv1351().getLdbv1351OperLogStatBus(), ws.getNegazione())) {
            // COB_CODE: SET NOT-STATO               TO TRUE
            ws.getFlagStato().setNotStato();
        }
        // COB_CODE: IF LDBV1351-OPER-LOG-CAUS = NEGAZIONE
        //              SET NOT-CAUSALE             TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getLdbv1351().getLdbv1351OperLogCaus(), ws.getNegazione())) {
            // COB_CODE: SET NOT-CAUSALE             TO TRUE
            ws.getFlagCausale().setNotCausale();
        }
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A400-ELABORA-EFF<br>*/
    private void a400ElaboraEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM A450-WHERE-CONDITION-EFF THRU A450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-EFF     THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-EFF    THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-EFF     THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-EFF      THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM A450-WHERE-CONDITION-EFF THRU A450-EX
            a450WhereConditionEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-EFF     THRU A460-EX
            a460OpenCursorEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-EFF    THRU A470-EX
            a470CloseCursorEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-EFF     THRU A480-EX
            a480FetchFirstEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-EFF      THRU A490-EX
            a490FetchNextEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-CPZ<br>*/
    private void b400ElaboraCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM B450-WHERE-CONDITION-CPZ THRU B450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-CPZ     THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-CPZ    THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-CPZ     THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-CPZ      THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM B450-WHERE-CONDITION-CPZ THRU B450-EX
            b450WhereConditionCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-CPZ     THRU B460-EX
            b460OpenCursorCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-CPZ    THRU B470-EX
            b470CloseCursorCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-CPZ     THRU B480-EX
            b480FetchFirstCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-CPZ      THRU B490-EX
            b490FetchNextCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C400-ELABORA-EFF-NS<br>*/
    private void c400ElaboraEffNs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM C450-WHERE-CONDITION-EFF-NS THRU C450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C460-OPEN-CURSOR-EFF-NS     THRU C460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C470-CLOSE-CURSOR-EFF-NS    THRU C470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C480-FETCH-FIRST-EFF-NS     THRU C480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C490-FETCH-NEXT-EFF-NS      THRU C490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM C450-WHERE-CONDITION-EFF-NS THRU C450-EX
            c450WhereConditionEffNs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C460-OPEN-CURSOR-EFF-NS     THRU C460-EX
            c460OpenCursorEffNs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C470-CLOSE-CURSOR-EFF-NS    THRU C470-EX
            c470CloseCursorEffNs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C480-FETCH-FIRST-EFF-NS     THRU C480-EX
            c480FetchFirstEffNs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C490-FETCH-NEXT-EFF-NS      THRU C490-EX
            c490FetchNextEffNs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: D400-ELABORA-CPZ-NS<br>*/
    private void d400ElaboraCpzNs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM D450-WHERE-CONDITION-CPZ-NS THRU D450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM D460-OPEN-CURSOR-CPZ-NS     THRU D460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM D470-CLOSE-CURSOR-CPZ-NS    THRU D470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM D480-FETCH-FIRST-CPZ-NS     THRU D480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM D490-FETCH-NEXT-CPZ-NS      THRU D490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM D450-WHERE-CONDITION-CPZ-NS THRU D450-EX
            d450WhereConditionCpzNs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM D460-OPEN-CURSOR-CPZ-NS     THRU D460-EX
            d460OpenCursorCpzNs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM D470-CLOSE-CURSOR-CPZ-NS    THRU D470-EX
            d470CloseCursorCpzNs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM D480-FETCH-FIRST-CPZ-NS     THRU D480-EX
            d480FetchFirstCpzNs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM D490-FETCH-NEXT-CPZ-NS      THRU D490-EX
            d490FetchNextCpzNs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: E400-ELABORA-EFF-NC<br>*/
    private void e400ElaboraEffNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM E450-WHERE-CONDITION-EFF-NC THRU E450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM E460-OPEN-CURSOR-EFF-NC     THRU E460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM E470-CLOSE-CURSOR-EFF-NC    THRU E470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM E480-FETCH-FIRST-EFF-NC     THRU E480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM E490-FETCH-NEXT-EFF-NC      THRU E490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM E450-WHERE-CONDITION-EFF-NC THRU E450-EX
            e450WhereConditionEffNc();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM E460-OPEN-CURSOR-EFF-NC     THRU E460-EX
            e460OpenCursorEffNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM E470-CLOSE-CURSOR-EFF-NC    THRU E470-EX
            e470CloseCursorEffNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM E480-FETCH-FIRST-EFF-NC     THRU E480-EX
            e480FetchFirstEffNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM E490-FETCH-NEXT-EFF-NC      THRU E490-EX
            e490FetchNextEffNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: F400-ELABORA-CPZ-NC<br>*/
    private void f400ElaboraCpzNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM F450-WHERE-CONDITION-CPZ-NC THRU F450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM F460-OPEN-CURSOR-CPZ-NC     THRU F460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM F470-CLOSE-CURSOR-CPZ-NC    THRU F470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM F480-FETCH-FIRST-CPZ-NC     THRU F480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM F490-FETCH-NEXT-CPZ-NC      THRU F490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM F450-WHERE-CONDITION-CPZ-NC THRU F450-EX
            f450WhereConditionCpzNc();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM F460-OPEN-CURSOR-CPZ-NC     THRU F460-EX
            f460OpenCursorCpzNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM F470-CLOSE-CURSOR-CPZ-NC    THRU F470-EX
            f470CloseCursorCpzNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM F480-FETCH-FIRST-CPZ-NC     THRU F480-EX
            f480FetchFirstCpzNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM F490-FETCH-NEXT-CPZ-NC      THRU F490-EX
            f490FetchNextCpzNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: G400-ELABORA-EFF-NS-NC<br>*/
    private void g400ElaboraEffNsNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM G450-WHERE-CONDITION-EFF-NS-NC THRU G450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM G460-OPEN-CURSOR-EFF-NS-NC     THRU G460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM G470-CLOSE-CURSOR-EFF-NS-NC    THRU G470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM G480-FETCH-FIRST-EFF-NS-NC     THRU G480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM G490-FETCH-NEXT-EFF-NS-NC      THRU G490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM G450-WHERE-CONDITION-EFF-NS-NC THRU G450-EX
            g450WhereConditionEffNsNc();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM G460-OPEN-CURSOR-EFF-NS-NC     THRU G460-EX
            g460OpenCursorEffNsNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM G470-CLOSE-CURSOR-EFF-NS-NC    THRU G470-EX
            g470CloseCursorEffNsNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM G480-FETCH-FIRST-EFF-NS-NC     THRU G480-EX
            g480FetchFirstEffNsNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM G490-FETCH-NEXT-EFF-NS-NC      THRU G490-EX
            g490FetchNextEffNsNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: H400-ELABORA-CPZ-NS-NC<br>*/
    private void h400ElaboraCpzNsNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM H450-WHERE-CONDITION-CPZ-NS-NC THRU H450-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM H460-OPEN-CURSOR-CPZ-NS-NC     THRU H460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC    THRU H470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM H480-FETCH-FIRST-CPZ-NS-NC     THRU H480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM H490-FETCH-NEXT-CPZ-NS-NC      THRU H490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER        TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003WhereCondition()) {
            // COB_CODE: PERFORM H450-WHERE-CONDITION-CPZ-NS-NC THRU H450-EX
            h450WhereConditionCpzNsNc();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM H460-OPEN-CURSOR-CPZ-NS-NC     THRU H460-EX
            h460OpenCursorCpzNsNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC    THRU H470-EX
            h470CloseCursorCpzNsNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM H480-FETCH-FIRST-CPZ-NS-NC     THRU H480-EX
            h480FetchFirstCpzNsNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM H490-FETCH-NEXT-CPZ-NS-NC      THRU H490-EX
            h490FetchNextCpzNsNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A405-DECLARE-CURSOR-EFF<br>
	 * <pre>----
	 * ----  gestione Effetto STATO / CAUSALE
	 * ----</pre>*/
    private void a405DeclareCursorEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-GRZ CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI       = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: A450-WHERE-CONDITION-EFF<br>*/
    private void a450WhereConditionEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-EFF<br>*/
    private void a460OpenCursorEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-EFF THRU A405-EX.
        a405DeclareCursorEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-GRZ
        //           END-EXEC.
        garStatOggBusDao.openCEffGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-EFF<br>*/
    private void a470CloseCursorEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-GRZ
        //           END-EXEC.
        garStatOggBusDao.closeCEffGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-EFF<br>*/
    private void a480FetchFirstEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-EFF    THRU A460-EX.
        a460OpenCursorEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-EFF THRU A490-EX
            a490FetchNextEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-EFF<br>*/
    private void a490FetchNextEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCEffGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX
            a470CloseCursorEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B405-DECLARE-CURSOR-CPZ<br>
	 * <pre>----
	 * ----  gestione Competenza STATO / CAUSALE
	 * ----</pre>*/
    private void b405DeclareCursorCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-GRZ CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: B450-WHERE-CONDITION-CPZ<br>*/
    private void b450WhereConditionCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //                ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-CPZ<br>*/
    private void b460OpenCursorCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-CPZ THRU B405-EX.
        b405DeclareCursorCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-GRZ
        //           END-EXEC.
        garStatOggBusDao.openCCpzGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-CPZ<br>*/
    private void b470CloseCursorCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-GRZ
        //           END-EXEC.
        garStatOggBusDao.closeCCpzGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-CPZ<br>*/
    private void b480FetchFirstCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-CPZ    THRU B460-EX.
        b460OpenCursorCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-CPZ THRU B490-EX
            b490FetchNextCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-CPZ<br>*/
    private void b490FetchNextCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCCpzGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX
            b470CloseCursorCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C405-DECLARE-CURSOR-EFF-NS<br>
	 * <pre>----
	 * ----  gestione Effetto NOT STATO / CAUS
	 * ----</pre>*/
    private void c405DeclareCursorEffNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-GRZ-NS CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI       = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: C450-WHERE-CONDITION-EFF-NS<br>*/
    private void c450WhereConditionEffNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: C460-OPEN-CURSOR-EFF-NS<br>*/
    private void c460OpenCursorEffNs() {
        // COB_CODE: PERFORM C405-DECLARE-CURSOR-EFF-NS THRU C405-EX.
        c405DeclareCursorEffNs();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-GRZ-NS
        //           END-EXEC.
        garStatOggBusDao.openCEffGrzNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C470-CLOSE-CURSOR-EFF-NS<br>*/
    private void c470CloseCursorEffNs() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-GRZ-NS
        //           END-EXEC.
        garStatOggBusDao.closeCEffGrzNs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C480-FETCH-FIRST-EFF-NS<br>*/
    private void c480FetchFirstEffNs() {
        // COB_CODE: PERFORM C460-OPEN-CURSOR-EFF-NS    THRU C460-EX.
        c460OpenCursorEffNs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C490-FETCH-NEXT-EFF-NS THRU C490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C490-FETCH-NEXT-EFF-NS THRU C490-EX
            c490FetchNextEffNs();
        }
    }

    /**Original name: C490-FETCH-NEXT-EFF-NS<br>*/
    private void c490FetchNextEffNs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-GRZ-NS
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCEffGrzNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX
            c470CloseCursorEffNs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: D405-DECLARE-CURSOR-CPZ-NS<br>
	 * <pre>----
	 * ----  gestione Competenza NOT STATO / CAUSALE
	 * ----</pre>*/
    private void d405DeclareCursorCpzNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-GRZ-NS CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: D450-WHERE-CONDITION-CPZ-NS<br>*/
    private void d450WhereConditionCpzNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: D460-OPEN-CURSOR-CPZ-NS<br>*/
    private void d460OpenCursorCpzNs() {
        // COB_CODE: PERFORM D405-DECLARE-CURSOR-CPZ-NS THRU D405-EX.
        d405DeclareCursorCpzNs();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-GRZ-NS
        //           END-EXEC.
        garStatOggBusDao.openCCpzGrzNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: D470-CLOSE-CURSOR-CPZ-NS<br>*/
    private void d470CloseCursorCpzNs() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-GRZ-NS
        //           END-EXEC.
        garStatOggBusDao.closeCCpzGrzNs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: D480-FETCH-FIRST-CPZ-NS<br>*/
    private void d480FetchFirstCpzNs() {
        // COB_CODE: PERFORM D460-OPEN-CURSOR-CPZ-NS    THRU D460-EX.
        d460OpenCursorCpzNs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM D490-FETCH-NEXT-CPZ-NS THRU D490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM D490-FETCH-NEXT-CPZ-NS THRU D490-EX
            d490FetchNextCpzNs();
        }
    }

    /**Original name: D490-FETCH-NEXT-CPZ-NS<br>*/
    private void d490FetchNextCpzNs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-GRZ-NS
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCCpzGrzNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX
            d470CloseCursorCpzNs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: E405-DECLARE-CURSOR-EFF-NC<br>
	 * <pre>----
	 * ----  gestione Effetto STATO / CAUS NOT
	 * ----</pre>*/
    private void e405DeclareCursorEffNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-GRZ-NC CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI       = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: E450-WHERE-CONDITION-EFF-NC<br>*/
    private void e450WhereConditionEffNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: E460-OPEN-CURSOR-EFF-NC<br>*/
    private void e460OpenCursorEffNc() {
        // COB_CODE: PERFORM E405-DECLARE-CURSOR-EFF-NC THRU E405-EX.
        e405DeclareCursorEffNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-GRZ-NC
        //           END-EXEC.
        garStatOggBusDao.openCEffGrzNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: E470-CLOSE-CURSOR-EFF-NC<br>*/
    private void e470CloseCursorEffNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-GRZ-NC
        //           END-EXEC.
        garStatOggBusDao.closeCEffGrzNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: E480-FETCH-FIRST-EFF-NC<br>*/
    private void e480FetchFirstEffNc() {
        // COB_CODE: PERFORM E460-OPEN-CURSOR-EFF-NC    THRU E460-EX.
        e460OpenCursorEffNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM E490-FETCH-NEXT-EFF-NC THRU E490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM E490-FETCH-NEXT-EFF-NC THRU E490-EX
            e490FetchNextEffNc();
        }
    }

    /**Original name: E490-FETCH-NEXT-EFF-NC<br>*/
    private void e490FetchNextEffNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-GRZ-NC
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCEffGrzNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX
            e470CloseCursorEffNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F405-DECLARE-CURSOR-CPZ-NC<br>
	 * <pre>----
	 * ----  gestione Competenza STATO / NOT CAUSALE
	 * ----</pre>*/
    private void f405DeclareCursorCpzNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-GRZ-NC CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: F450-WHERE-CONDITION-CPZ-NC<br>*/
    private void f450WhereConditionCpzNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: F460-OPEN-CURSOR-CPZ-NC<br>*/
    private void f460OpenCursorCpzNc() {
        // COB_CODE: PERFORM F405-DECLARE-CURSOR-CPZ-NC THRU F405-EX.
        f405DeclareCursorCpzNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-GRZ-NC
        //           END-EXEC.
        garStatOggBusDao.openCCpzGrzNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: F470-CLOSE-CURSOR-CPZ-NC<br>*/
    private void f470CloseCursorCpzNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-GRZ-NC
        //           END-EXEC.
        garStatOggBusDao.closeCCpzGrzNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: F480-FETCH-FIRST-CPZ-NC<br>*/
    private void f480FetchFirstCpzNc() {
        // COB_CODE: PERFORM F460-OPEN-CURSOR-CPZ-NC    THRU F460-EX.
        f460OpenCursorCpzNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM F490-FETCH-NEXT-CPZ-NC THRU F490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM F490-FETCH-NEXT-CPZ-NC THRU F490-EX
            f490FetchNextCpzNc();
        }
    }

    /**Original name: F490-FETCH-NEXT-CPZ-NC<br>*/
    private void f490FetchNextCpzNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-GRZ-NC
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCCpzGrzNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX
            f470CloseCursorCpzNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: G405-DECLARE-CURSOR-EFF-NS-NC<br>
	 * <pre>----
	 * ----  gestione Effetto NOT STATO / CAUS NOT
	 * ----</pre>*/
    private void g405DeclareCursorEffNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-GRZ-NS-NC CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI       = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: G450-WHERE-CONDITION-EFF-NS-NC<br>*/
    private void g450WhereConditionEffNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec6(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: G460-OPEN-CURSOR-EFF-NS-NC<br>*/
    private void g460OpenCursorEffNsNc() {
        // COB_CODE: PERFORM G405-DECLARE-CURSOR-EFF-NS-NC THRU G405-EX.
        g405DeclareCursorEffNsNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-GRZ-NS-NC
        //           END-EXEC.
        garStatOggBusDao.openCEffGrzNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: G470-CLOSE-CURSOR-EFF-NS-NC<br>*/
    private void g470CloseCursorEffNsNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-GRZ-NS-NC
        //           END-EXEC.
        garStatOggBusDao.closeCEffGrzNsNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: G480-FETCH-FIRST-EFF-NS-NC<br>*/
    private void g480FetchFirstEffNsNc() {
        // COB_CODE: PERFORM G460-OPEN-CURSOR-EFF-NS-NC    THRU G460-EX.
        g460OpenCursorEffNsNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM G490-FETCH-NEXT-EFF-NS-NC THRU G490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM G490-FETCH-NEXT-EFF-NS-NC THRU G490-EX
            g490FetchNextEffNsNc();
        }
    }

    /**Original name: G490-FETCH-NEXT-EFF-NS-NC<br>*/
    private void g490FetchNextEffNsNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-GRZ-NS-NC
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCEffGrzNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX
            g470CloseCursorEffNsNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: H405-DECLARE-CURSOR-CPZ-NS-NC<br>
	 * <pre>----
	 * ----  gestione Competenza NOT STATO / NOT CAUSALE
	 * ----</pre>*/
    private void h405DeclareCursorCpzNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-GRZ-NS-NC CURSOR FOR
        //              SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :GRZ-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: H450-WHERE-CONDITION-CPZ-NS-NC<br>*/
    private void h450WhereConditionCpzNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.IB_OGG
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.COD_SEZ
        //                ,A.COD_TARI
        //                ,A.RAMO_BILA
        //                ,A.DT_INI_VAL_TAR
        //                ,A.ID_1O_ASSTO
        //                ,A.ID_2O_ASSTO
        //                ,A.ID_3O_ASSTO
        //                ,A.TP_GAR
        //                ,A.TP_RSH
        //                ,A.TP_INVST
        //                ,A.MOD_PAG_GARCOL
        //                ,A.TP_PER_PRE
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.TP_EMIS_PUR
        //                ,A.ETA_A_SCAD
        //                ,A.TP_CALC_PRE_PRSTZ
        //                ,A.TP_PRE
        //                ,A.TP_DUR
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.NUM_AA_PAG_PRE
        //                ,A.AA_PAG_PRE_UNI
        //                ,A.MM_PAG_PRE_UNI
        //                ,A.FRAZ_INI_EROG_REN
        //                ,A.MM_1O_RAT
        //                ,A.PC_1O_RAT
        //                ,A.TP_PRSTZ_ASSTA
        //                ,A.DT_END_CARZ
        //                ,A.PC_RIP_PRE
        //                ,A.COD_FND
        //                ,A.AA_REN_CER
        //                ,A.PC_REVRSB
        //                ,A.TP_PC_RIP
        //                ,A.PC_OPZ
        //                ,A.TP_IAS
        //                ,A.TP_STAB
        //                ,A.TP_ADEG_PRE
        //                ,A.DT_VARZ_TP_IAS
        //                ,A.FRAZ_DECR_CPT
        //                ,A.COD_TRAT_RIASS
        //                ,A.TP_DT_EMIS_RIASS
        //                ,A.TP_CESS_RIASS
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.AA_STAB
        //                ,A.TS_STAB_LIMITATA
        //                ,A.DT_PRESC
        //                ,A.RSH_INVST
        //                ,A.TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //              FROM GAR A, STAT_OGG_BUS B
        //              WHERE     A.COD_TARI = :GRZ-COD-TARI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'GA'
        //                    AND B.ID_OGG      = A.ID_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1351-TP-STAT-BUS-01,
        //                                         :LDBV1351-TP-STAT-BUS-02,
        //                                         :LDBV1351-TP-STAT-BUS-03,
        //                                         :LDBV1351-TP-STAT-BUS-04,
        //                                         :LDBV1351-TP-STAT-BUS-05,
        //                                         :LDBV1351-TP-STAT-BUS-06,
        //                                         :LDBV1351-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1351-TP-CAUS-01,
        //                                         :LDBV1351-TP-CAUS-02,
        //                                         :LDBV1351-TP-CAUS-03,
        //                                         :LDBV1351-TP-CAUS-04,
        //                                         :LDBV1351-TP-CAUS-05,
        //                                         :LDBV1351-TP-CAUS-06,
        //                                         :LDBV1351-TP-CAUS-07
        //                                         )
        //           END-EXEC.
        garStatOggBusDao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: H460-OPEN-CURSOR-CPZ-NS-NC<br>*/
    private void h460OpenCursorCpzNsNc() {
        // COB_CODE: PERFORM H405-DECLARE-CURSOR-CPZ-NS-NC THRU H405-EX.
        h405DeclareCursorCpzNsNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-GRZ-NS-NC
        //           END-EXEC.
        garStatOggBusDao.openCCpzGrzNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: H470-CLOSE-CURSOR-CPZ-NS-NC<br>*/
    private void h470CloseCursorCpzNsNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-GRZ-NS-NC
        //           END-EXEC.
        garStatOggBusDao.closeCCpzGrzNsNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: H480-FETCH-FIRST-CPZ-NS-NC<br>*/
    private void h480FetchFirstCpzNsNc() {
        // COB_CODE: PERFORM H460-OPEN-CURSOR-CPZ-NS-NC    THRU H460-EX.
        h460OpenCursorCpzNsNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM H490-FETCH-NEXT-CPZ-NS-NC THRU H490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM H490-FETCH-NEXT-CPZ-NS-NC THRU H490-EX
            h490FetchNextCpzNsNc();
        }
    }

    /**Original name: H490-FETCH-NEXT-CPZ-NS-NC<br>*/
    private void h490FetchNextCpzNsNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-GRZ-NS-NC
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garStatOggBusDao.fetchCCpzGrzNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX
            h470CloseCursorCpzNsNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-GRZ-ID-ADES = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-ADES-NULL
        //           END-IF
        if (ws.getIndGar().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-ADES-NULL
            gar.getGrzIdAdes().setGrzIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzIdAdes.Len.GRZ_ID_ADES_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndGar().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-MOVI-CHIU-NULL
            gar.getGrzIdMoviChiu().setGrzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-GRZ-IB-OGG = -1
        //              MOVE HIGH-VALUES TO GRZ-IB-OGG-NULL
        //           END-IF
        if (ws.getIndGar().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-IB-OGG-NULL
            gar.setGrzIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_IB_OGG));
        }
        // COB_CODE: IF IND-GRZ-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndGar().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-DECOR-NULL
            gar.getGrzDtDecor().setGrzDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtDecor.Len.GRZ_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-GRZ-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndGar().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-SCAD-NULL
            gar.getGrzDtScad().setGrzDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtScad.Len.GRZ_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-GRZ-COD-SEZ = -1
        //              MOVE HIGH-VALUES TO GRZ-COD-SEZ-NULL
        //           END-IF
        if (ws.getIndGar().getCodSez() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-COD-SEZ-NULL
            gar.setGrzCodSez(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_COD_SEZ));
        }
        // COB_CODE: IF IND-GRZ-RAMO-BILA = -1
        //              MOVE HIGH-VALUES TO GRZ-RAMO-BILA-NULL
        //           END-IF
        if (ws.getIndGar().getRamoBila() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-RAMO-BILA-NULL
            gar.setGrzRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_RAMO_BILA));
        }
        // COB_CODE: IF IND-GRZ-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndGar().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-INI-VAL-TAR-NULL
            gar.getGrzDtIniValTar().setGrzDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtIniValTar.Len.GRZ_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getId1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-1O-ASSTO-NULL
            gar.getGrzId1oAssto().setGrzId1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzId1oAssto.Len.GRZ_ID1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getId2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-2O-ASSTO-NULL
            gar.getGrzId2oAssto().setGrzId2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzId2oAssto.Len.GRZ_ID2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getId3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-3O-ASSTO-NULL
            gar.getGrzId3oAssto().setGrzId3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzId3oAssto.Len.GRZ_ID3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-GAR = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-GAR-NULL
        //           END-IF
        if (ws.getIndGar().getTpGar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-GAR-NULL
            gar.getGrzTpGar().setGrzTpGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzTpGar.Len.GRZ_TP_GAR_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-RSH = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-RSH-NULL
        //           END-IF
        if (ws.getIndGar().getTpRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-RSH-NULL
            gar.setGrzTpRsh(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_RSH));
        }
        // COB_CODE: IF IND-GRZ-TP-INVST = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-INVST-NULL
        //           END-IF
        if (ws.getIndGar().getTpInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-INVST-NULL
            gar.getGrzTpInvst().setGrzTpInvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzTpInvst.Len.GRZ_TP_INVST_NULL));
        }
        // COB_CODE: IF IND-GRZ-MOD-PAG-GARCOL = -1
        //              MOVE HIGH-VALUES TO GRZ-MOD-PAG-GARCOL-NULL
        //           END-IF
        if (ws.getIndGar().getModPagGarcol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-MOD-PAG-GARCOL-NULL
            gar.setGrzModPagGarcol(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_MOD_PAG_GARCOL));
        }
        // COB_CODE: IF IND-GRZ-TP-PER-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PER-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getTpPerPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PER-PRE-NULL
            gar.setGrzTpPerPre(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_PER_PRE));
        }
        // COB_CODE: IF IND-GRZ-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-AA-1O-ASSTO-NULL
            gar.getGrzEtaAa1oAssto().setGrzEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAa1oAssto.Len.GRZ_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-MM-1O-ASSTO-NULL
            gar.getGrzEtaMm1oAssto().setGrzEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaMm1oAssto.Len.GRZ_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-AA-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-AA-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAa2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-AA-2O-ASSTO-NULL
            gar.getGrzEtaAa2oAssto().setGrzEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAa2oAssto.Len.GRZ_ETA_AA2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-MM-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-MM-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaMm2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-MM-2O-ASSTO-NULL
            gar.getGrzEtaMm2oAssto().setGrzEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaMm2oAssto.Len.GRZ_ETA_MM2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-AA-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-AA-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAa3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-AA-3O-ASSTO-NULL
            gar.getGrzEtaAa3oAssto().setGrzEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAa3oAssto.Len.GRZ_ETA_AA3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-MM-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-MM-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaMm3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-MM-3O-ASSTO-NULL
            gar.getGrzEtaMm3oAssto().setGrzEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaMm3oAssto.Len.GRZ_ETA_MM3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-EMIS-PUR = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-EMIS-PUR-NULL
        //           END-IF
        if (ws.getIndGar().getTpEmisPur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-EMIS-PUR-NULL
            gar.setGrzTpEmisPur(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-GRZ-ETA-A-SCAD = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-A-SCAD-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-A-SCAD-NULL
            gar.getGrzEtaAScad().setGrzEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAScad.Len.GRZ_ETA_A_SCAD_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-CALC-PRE-PRSTZ = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-CALC-PRE-PRSTZ-NULL
        //           END-IF
        if (ws.getIndGar().getTpCalcPrePrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-CALC-PRE-PRSTZ-NULL
            gar.setGrzTpCalcPrePrstz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_CALC_PRE_PRSTZ));
        }
        // COB_CODE: IF IND-GRZ-TP-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getTpPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PRE-NULL
            gar.setGrzTpPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-GRZ-TP-DUR = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-DUR-NULL
        //           END-IF
        if (ws.getIndGar().getTpDur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-DUR-NULL
            gar.setGrzTpDur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_DUR));
        }
        // COB_CODE: IF IND-GRZ-DUR-AA = -1
        //              MOVE HIGH-VALUES TO GRZ-DUR-AA-NULL
        //           END-IF
        if (ws.getIndGar().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DUR-AA-NULL
            gar.getGrzDurAa().setGrzDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDurAa.Len.GRZ_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-GRZ-DUR-MM = -1
        //              MOVE HIGH-VALUES TO GRZ-DUR-MM-NULL
        //           END-IF
        if (ws.getIndGar().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DUR-MM-NULL
            gar.getGrzDurMm().setGrzDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDurMm.Len.GRZ_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-GRZ-DUR-GG = -1
        //              MOVE HIGH-VALUES TO GRZ-DUR-GG-NULL
        //           END-IF
        if (ws.getIndGar().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DUR-GG-NULL
            gar.getGrzDurGg().setGrzDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDurGg.Len.GRZ_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-GRZ-NUM-AA-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-NUM-AA-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getNumAaPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-NUM-AA-PAG-PRE-NULL
            gar.getGrzNumAaPagPre().setGrzNumAaPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzNumAaPagPre.Len.GRZ_NUM_AA_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-GRZ-AA-PAG-PRE-UNI = -1
        //              MOVE HIGH-VALUES TO GRZ-AA-PAG-PRE-UNI-NULL
        //           END-IF
        if (ws.getIndGar().getAaPagPreUni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-AA-PAG-PRE-UNI-NULL
            gar.getGrzAaPagPreUni().setGrzAaPagPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzAaPagPreUni.Len.GRZ_AA_PAG_PRE_UNI_NULL));
        }
        // COB_CODE: IF IND-GRZ-MM-PAG-PRE-UNI = -1
        //              MOVE HIGH-VALUES TO GRZ-MM-PAG-PRE-UNI-NULL
        //           END-IF
        if (ws.getIndGar().getMmPagPreUni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-MM-PAG-PRE-UNI-NULL
            gar.getGrzMmPagPreUni().setGrzMmPagPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzMmPagPreUni.Len.GRZ_MM_PAG_PRE_UNI_NULL));
        }
        // COB_CODE: IF IND-GRZ-FRAZ-INI-EROG-REN = -1
        //              MOVE HIGH-VALUES TO GRZ-FRAZ-INI-EROG-REN-NULL
        //           END-IF
        if (ws.getIndGar().getFrazIniErogRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-FRAZ-INI-EROG-REN-NULL
            gar.getGrzFrazIniErogRen().setGrzFrazIniErogRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzFrazIniErogRen.Len.GRZ_FRAZ_INI_EROG_REN_NULL));
        }
        // COB_CODE: IF IND-GRZ-MM-1O-RAT = -1
        //              MOVE HIGH-VALUES TO GRZ-MM-1O-RAT-NULL
        //           END-IF
        if (ws.getIndGar().getMm1oRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-MM-1O-RAT-NULL
            gar.getGrzMm1oRat().setGrzMm1oRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzMm1oRat.Len.GRZ_MM1O_RAT_NULL));
        }
        // COB_CODE: IF IND-GRZ-PC-1O-RAT = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-1O-RAT-NULL
        //           END-IF
        if (ws.getIndGar().getPc1oRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-1O-RAT-NULL
            gar.getGrzPc1oRat().setGrzPc1oRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPc1oRat.Len.GRZ_PC1O_RAT_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-PRSTZ-ASSTA = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PRSTZ-ASSTA-NULL
        //           END-IF
        if (ws.getIndGar().getTpPrstzAssta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PRSTZ-ASSTA-NULL
            gar.setGrzTpPrstzAssta(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_PRSTZ_ASSTA));
        }
        // COB_CODE: IF IND-GRZ-DT-END-CARZ = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-END-CARZ-NULL
        //           END-IF
        if (ws.getIndGar().getDtEndCarz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-END-CARZ-NULL
            gar.getGrzDtEndCarz().setGrzDtEndCarzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtEndCarz.Len.GRZ_DT_END_CARZ_NULL));
        }
        // COB_CODE: IF IND-GRZ-PC-RIP-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-RIP-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getPcRipPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-RIP-PRE-NULL
            gar.getGrzPcRipPre().setGrzPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPcRipPre.Len.GRZ_PC_RIP_PRE_NULL));
        }
        // COB_CODE: IF IND-GRZ-COD-FND = -1
        //              MOVE HIGH-VALUES TO GRZ-COD-FND-NULL
        //           END-IF
        if (ws.getIndGar().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-COD-FND-NULL
            gar.setGrzCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_COD_FND));
        }
        // COB_CODE: IF IND-GRZ-AA-REN-CER = -1
        //              MOVE HIGH-VALUES TO GRZ-AA-REN-CER-NULL
        //           END-IF
        if (ws.getIndGar().getAaRenCer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-AA-REN-CER-NULL
            gar.setGrzAaRenCer(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_AA_REN_CER));
        }
        // COB_CODE: IF IND-GRZ-PC-REVRSB = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-REVRSB-NULL
        //           END-IF
        if (ws.getIndGar().getPcRevrsb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-REVRSB-NULL
            gar.getGrzPcRevrsb().setGrzPcRevrsbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPcRevrsb.Len.GRZ_PC_REVRSB_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-PC-RIP = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PC-RIP-NULL
        //           END-IF
        if (ws.getIndGar().getTpPcRip() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PC-RIP-NULL
            gar.setGrzTpPcRip(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_PC_RIP));
        }
        // COB_CODE: IF IND-GRZ-PC-OPZ = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-OPZ-NULL
        //           END-IF
        if (ws.getIndGar().getPcOpz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-OPZ-NULL
            gar.getGrzPcOpz().setGrzPcOpzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPcOpz.Len.GRZ_PC_OPZ_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndGar().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-IAS-NULL
            gar.setGrzTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_IAS));
        }
        // COB_CODE: IF IND-GRZ-TP-STAB = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-STAB-NULL
        //           END-IF
        if (ws.getIndGar().getTpStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-STAB-NULL
            gar.setGrzTpStab(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_STAB));
        }
        // COB_CODE: IF IND-GRZ-TP-ADEG-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-ADEG-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getTpAdegPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-ADEG-PRE-NULL
            gar.setGrzTpAdegPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-GRZ-DT-VARZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-VARZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndGar().getDtVarzTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-VARZ-TP-IAS-NULL
            gar.getGrzDtVarzTpIas().setGrzDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtVarzTpIas.Len.GRZ_DT_VARZ_TP_IAS_NULL));
        }
        // COB_CODE: IF IND-GRZ-FRAZ-DECR-CPT = -1
        //              MOVE HIGH-VALUES TO GRZ-FRAZ-DECR-CPT-NULL
        //           END-IF
        if (ws.getIndGar().getFrazDecrCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-FRAZ-DECR-CPT-NULL
            gar.getGrzFrazDecrCpt().setGrzFrazDecrCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzFrazDecrCpt.Len.GRZ_FRAZ_DECR_CPT_NULL));
        }
        // COB_CODE: IF IND-GRZ-COD-TRAT-RIASS = -1
        //              MOVE HIGH-VALUES TO GRZ-COD-TRAT-RIASS-NULL
        //           END-IF
        if (ws.getIndGar().getCodTratRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-COD-TRAT-RIASS-NULL
            gar.setGrzCodTratRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_COD_TRAT_RIASS));
        }
        // COB_CODE: IF IND-GRZ-TP-DT-EMIS-RIASS = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-DT-EMIS-RIASS-NULL
        //           END-IF
        if (ws.getIndGar().getTpDtEmisRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-DT-EMIS-RIASS-NULL
            gar.setGrzTpDtEmisRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_DT_EMIS_RIASS));
        }
        // COB_CODE: IF IND-GRZ-TP-CESS-RIASS = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-CESS-RIASS-NULL
        //           END-IF
        if (ws.getIndGar().getTpCessRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-CESS-RIASS-NULL
            gar.setGrzTpCessRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_CESS_RIASS));
        }
        // COB_CODE: IF IND-GRZ-AA-STAB = -1
        //              MOVE HIGH-VALUES TO GRZ-AA-STAB-NULL
        //           END-IF
        if (ws.getIndGar().getAaStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-AA-STAB-NULL
            gar.getGrzAaStab().setGrzAaStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzAaStab.Len.GRZ_AA_STAB_NULL));
        }
        // COB_CODE: IF IND-GRZ-TS-STAB-LIMITATA = -1
        //              MOVE HIGH-VALUES TO GRZ-TS-STAB-LIMITATA-NULL
        //           END-IF
        if (ws.getIndGar().getTsStabLimitata() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TS-STAB-LIMITATA-NULL
            gar.getGrzTsStabLimitata().setGrzTsStabLimitataNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzTsStabLimitata.Len.GRZ_TS_STAB_LIMITATA_NULL));
        }
        // COB_CODE: IF IND-GRZ-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-PRESC-NULL
        //           END-IF.
        if (ws.getIndGar().getDtPresc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-PRESC-NULL
            gar.getGrzDtPresc().setGrzDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtPresc.Len.GRZ_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-GRZ-RSH-INVST = -1
        //              MOVE HIGH-VALUES TO GRZ-RSH-INVST-NULL
        //           END-IF.
        if (ws.getIndGar().getRshInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-RSH-INVST-NULL
            gar.setGrzRshInvst(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE GRZ-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getGarDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-INI-EFF
        gar.setGrzDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE GRZ-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getGarDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-END-EFF
        gar.setGrzDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-GRZ-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-DECOR
        //           END-IF
        if (ws.getIndGar().getDtDecor() == 0) {
            // COB_CODE: MOVE GRZ-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-DECOR
            gar.getGrzDtDecor().setGrzDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-SCAD
        //           END-IF
        if (ws.getIndGar().getDtScad() == 0) {
            // COB_CODE: MOVE GRZ-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-SCAD
            gar.getGrzDtScad().setGrzDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndGar().getDtIniValTar() == 0) {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-INI-VAL-TAR
            gar.getGrzDtIniValTar().setGrzDtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-END-CARZ = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-END-CARZ
        //           END-IF
        if (ws.getIndGar().getDtEndCarz() == 0) {
            // COB_CODE: MOVE GRZ-DT-END-CARZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getEndCarzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-END-CARZ
            gar.getGrzDtEndCarz().setGrzDtEndCarz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-VARZ-TP-IAS
        //           END-IF
        if (ws.getIndGar().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-VARZ-TP-IAS
            gar.getGrzDtVarzTpIas().setGrzDtVarzTpIas(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-PRESC
        //           END-IF.
        if (ws.getIndGar().getDtPresc() == 0) {
            // COB_CODE: MOVE GRZ-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-PRESC
            gar.getGrzDtPresc().setGrzDtPresc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaPagPreUni() {
        return gar.getGrzAaPagPreUni().getGrzAaPagPreUni();
    }

    @Override
    public void setAaPagPreUni(int aaPagPreUni) {
        this.gar.getGrzAaPagPreUni().setGrzAaPagPreUni(aaPagPreUni);
    }

    @Override
    public Integer getAaPagPreUniObj() {
        if (ws.getIndGar().getAaPagPreUni() >= 0) {
            return ((Integer)getAaPagPreUni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaPagPreUniObj(Integer aaPagPreUniObj) {
        if (aaPagPreUniObj != null) {
            setAaPagPreUni(((int)aaPagPreUniObj));
            ws.getIndGar().setAaPagPreUni(((short)0));
        }
        else {
            ws.getIndGar().setAaPagPreUni(((short)-1));
        }
    }

    @Override
    public String getAaRenCer() {
        return gar.getGrzAaRenCer();
    }

    @Override
    public void setAaRenCer(String aaRenCer) {
        this.gar.setGrzAaRenCer(aaRenCer);
    }

    @Override
    public String getAaRenCerObj() {
        if (ws.getIndGar().getAaRenCer() >= 0) {
            return getAaRenCer();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaRenCerObj(String aaRenCerObj) {
        if (aaRenCerObj != null) {
            setAaRenCer(aaRenCerObj);
            ws.getIndGar().setAaRenCer(((short)0));
        }
        else {
            ws.getIndGar().setAaRenCer(((short)-1));
        }
    }

    @Override
    public int getAaStab() {
        return gar.getGrzAaStab().getGrzAaStab();
    }

    @Override
    public void setAaStab(int aaStab) {
        this.gar.getGrzAaStab().setGrzAaStab(aaStab);
    }

    @Override
    public Integer getAaStabObj() {
        if (ws.getIndGar().getAaStab() >= 0) {
            return ((Integer)getAaStab());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaStabObj(Integer aaStabObj) {
        if (aaStabObj != null) {
            setAaStab(((int)aaStabObj));
            ws.getIndGar().setAaStab(((short)0));
        }
        else {
            ws.getIndGar().setAaStab(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return gar.getGrzCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.gar.setGrzCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return gar.getGrzCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.gar.setGrzCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndGar().getCodFnd() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndGar().setCodFnd(((short)0));
        }
        else {
            ws.getIndGar().setCodFnd(((short)-1));
        }
    }

    @Override
    public String getCodSez() {
        return gar.getGrzCodSez();
    }

    @Override
    public void setCodSez(String codSez) {
        this.gar.setGrzCodSez(codSez);
    }

    @Override
    public String getCodSezObj() {
        if (ws.getIndGar().getCodSez() >= 0) {
            return getCodSez();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSezObj(String codSezObj) {
        if (codSezObj != null) {
            setCodSez(codSezObj);
            ws.getIndGar().setCodSez(((short)0));
        }
        else {
            ws.getIndGar().setCodSez(((short)-1));
        }
    }

    @Override
    public String getCodTratRiass() {
        return gar.getGrzCodTratRiass();
    }

    @Override
    public void setCodTratRiass(String codTratRiass) {
        this.gar.setGrzCodTratRiass(codTratRiass);
    }

    @Override
    public String getCodTratRiassObj() {
        if (ws.getIndGar().getCodTratRiass() >= 0) {
            return getCodTratRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTratRiassObj(String codTratRiassObj) {
        if (codTratRiassObj != null) {
            setCodTratRiass(codTratRiassObj);
            ws.getIndGar().setCodTratRiass(((short)0));
        }
        else {
            ws.getIndGar().setCodTratRiass(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return gar.getGrzDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.gar.setGrzDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return gar.getGrzDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.gar.setGrzDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return gar.getGrzDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.gar.setGrzDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return gar.getGrzDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.gar.setGrzDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return gar.getGrzDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.gar.setGrzDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return gar.getGrzDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.gar.setGrzDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return gar.getGrzDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.gar.setGrzDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getGarDb().getDecorDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getGarDb().setDecorDb(dtDecorDb);
    }

    @Override
    public String getDtDecorDbObj() {
        if (ws.getIndGar().getDtDecor() >= 0) {
            return getDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        if (dtDecorDbObj != null) {
            setDtDecorDb(dtDecorDbObj);
            ws.getIndGar().setDtDecor(((short)0));
        }
        else {
            ws.getIndGar().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getDtEndCarzDb() {
        return ws.getGarDb().getEndCarzDb();
    }

    @Override
    public void setDtEndCarzDb(String dtEndCarzDb) {
        this.ws.getGarDb().setEndCarzDb(dtEndCarzDb);
    }

    @Override
    public String getDtEndCarzDbObj() {
        if (ws.getIndGar().getDtEndCarz() >= 0) {
            return getDtEndCarzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCarzDbObj(String dtEndCarzDbObj) {
        if (dtEndCarzDbObj != null) {
            setDtEndCarzDb(dtEndCarzDbObj);
            ws.getIndGar().setDtEndCarz(((short)0));
        }
        else {
            ws.getIndGar().setDtEndCarz(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getGarDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getGarDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getGarDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getGarDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniValTarDb() {
        return ws.getGarDb().getIniValTarDb();
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        this.ws.getGarDb().setIniValTarDb(dtIniValTarDb);
    }

    @Override
    public String getDtIniValTarDbObj() {
        if (ws.getIndGar().getDtIniValTar() >= 0) {
            return getDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        if (dtIniValTarDbObj != null) {
            setDtIniValTarDb(dtIniValTarDbObj);
            ws.getIndGar().setDtIniValTar(((short)0));
        }
        else {
            ws.getIndGar().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getDtPrescDb() {
        return ws.getGarDb().getPrescDb();
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        this.ws.getGarDb().setPrescDb(dtPrescDb);
    }

    @Override
    public String getDtPrescDbObj() {
        if (ws.getIndGar().getDtPresc() >= 0) {
            return getDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        if (dtPrescDbObj != null) {
            setDtPrescDb(dtPrescDbObj);
            ws.getIndGar().setDtPresc(((short)0));
        }
        else {
            ws.getIndGar().setDtPresc(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return ws.getGarDb().getScadDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getGarDb().setScadDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndGar().getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndGar().setDtScad(((short)0));
        }
        else {
            ws.getIndGar().setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtVarzTpIasDb() {
        return ws.getGarDb().getVarzTpIasDb();
    }

    @Override
    public void setDtVarzTpIasDb(String dtVarzTpIasDb) {
        this.ws.getGarDb().setVarzTpIasDb(dtVarzTpIasDb);
    }

    @Override
    public String getDtVarzTpIasDbObj() {
        if (ws.getIndGar().getDtVarzTpIas() >= 0) {
            return getDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj) {
        if (dtVarzTpIasDbObj != null) {
            setDtVarzTpIasDb(dtVarzTpIasDbObj);
            ws.getIndGar().setDtVarzTpIas(((short)0));
        }
        else {
            ws.getIndGar().setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return gar.getGrzDurAa().getGrzDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.gar.getGrzDurAa().setGrzDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndGar().getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndGar().setDurAa(((short)0));
        }
        else {
            ws.getIndGar().setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return gar.getGrzDurGg().getGrzDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.gar.getGrzDurGg().setGrzDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndGar().getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndGar().setDurGg(((short)0));
        }
        else {
            ws.getIndGar().setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return gar.getGrzDurMm().getGrzDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.gar.getGrzDurMm().setGrzDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndGar().getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndGar().setDurMm(((short)0));
        }
        else {
            ws.getIndGar().setDurMm(((short)-1));
        }
    }

    @Override
    public int getEtaAScad() {
        return gar.getGrzEtaAScad().getGrzEtaAScad();
    }

    @Override
    public void setEtaAScad(int etaAScad) {
        this.gar.getGrzEtaAScad().setGrzEtaAScad(etaAScad);
    }

    @Override
    public Integer getEtaAScadObj() {
        if (ws.getIndGar().getEtaAScad() >= 0) {
            return ((Integer)getEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAScadObj(Integer etaAScadObj) {
        if (etaAScadObj != null) {
            setEtaAScad(((int)etaAScadObj));
            ws.getIndGar().setEtaAScad(((short)0));
        }
        else {
            ws.getIndGar().setEtaAScad(((short)-1));
        }
    }

    @Override
    public short getEtaAa1oAssto() {
        return gar.getGrzEtaAa1oAssto().getGrzEtaAa1oAssto();
    }

    @Override
    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.gar.getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(etaAa1oAssto);
    }

    @Override
    public Short getEtaAa1oAsstoObj() {
        if (ws.getIndGar().getEtaAa1oAssto() >= 0) {
            return ((Short)getEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj) {
        if (etaAa1oAsstoObj != null) {
            setEtaAa1oAssto(((short)etaAa1oAsstoObj));
            ws.getIndGar().setEtaAa1oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa2oAssto() {
        return gar.getGrzEtaAa2oAssto().getGrzEtaAa2oAssto();
    }

    @Override
    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.gar.getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(etaAa2oAssto);
    }

    @Override
    public Short getEtaAa2oAsstoObj() {
        if (ws.getIndGar().getEtaAa2oAssto() >= 0) {
            return ((Short)getEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj) {
        if (etaAa2oAsstoObj != null) {
            setEtaAa2oAssto(((short)etaAa2oAsstoObj));
            ws.getIndGar().setEtaAa2oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa3oAssto() {
        return gar.getGrzEtaAa3oAssto().getGrzEtaAa3oAssto();
    }

    @Override
    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.gar.getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(etaAa3oAssto);
    }

    @Override
    public Short getEtaAa3oAsstoObj() {
        if (ws.getIndGar().getEtaAa3oAssto() >= 0) {
            return ((Short)getEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj) {
        if (etaAa3oAsstoObj != null) {
            setEtaAa3oAssto(((short)etaAa3oAsstoObj));
            ws.getIndGar().setEtaAa3oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm1oAssto() {
        return gar.getGrzEtaMm1oAssto().getGrzEtaMm1oAssto();
    }

    @Override
    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.gar.getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(etaMm1oAssto);
    }

    @Override
    public Short getEtaMm1oAsstoObj() {
        if (ws.getIndGar().getEtaMm1oAssto() >= 0) {
            return ((Short)getEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj) {
        if (etaMm1oAsstoObj != null) {
            setEtaMm1oAssto(((short)etaMm1oAsstoObj));
            ws.getIndGar().setEtaMm1oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm2oAssto() {
        return gar.getGrzEtaMm2oAssto().getGrzEtaMm2oAssto();
    }

    @Override
    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.gar.getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(etaMm2oAssto);
    }

    @Override
    public Short getEtaMm2oAsstoObj() {
        if (ws.getIndGar().getEtaMm2oAssto() >= 0) {
            return ((Short)getEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj) {
        if (etaMm2oAsstoObj != null) {
            setEtaMm2oAssto(((short)etaMm2oAsstoObj));
            ws.getIndGar().setEtaMm2oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm3oAssto() {
        return gar.getGrzEtaMm3oAssto().getGrzEtaMm3oAssto();
    }

    @Override
    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.gar.getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(etaMm3oAssto);
    }

    @Override
    public Short getEtaMm3oAsstoObj() {
        if (ws.getIndGar().getEtaMm3oAssto() >= 0) {
            return ((Short)getEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj) {
        if (etaMm3oAsstoObj != null) {
            setEtaMm3oAssto(((short)etaMm3oAsstoObj));
            ws.getIndGar().setEtaMm3oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public int getFrazDecrCpt() {
        return gar.getGrzFrazDecrCpt().getGrzFrazDecrCpt();
    }

    @Override
    public void setFrazDecrCpt(int frazDecrCpt) {
        this.gar.getGrzFrazDecrCpt().setGrzFrazDecrCpt(frazDecrCpt);
    }

    @Override
    public Integer getFrazDecrCptObj() {
        if (ws.getIndGar().getFrazDecrCpt() >= 0) {
            return ((Integer)getFrazDecrCpt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazDecrCptObj(Integer frazDecrCptObj) {
        if (frazDecrCptObj != null) {
            setFrazDecrCpt(((int)frazDecrCptObj));
            ws.getIndGar().setFrazDecrCpt(((short)0));
        }
        else {
            ws.getIndGar().setFrazDecrCpt(((short)-1));
        }
    }

    @Override
    public int getFrazIniErogRen() {
        return gar.getGrzFrazIniErogRen().getGrzFrazIniErogRen();
    }

    @Override
    public void setFrazIniErogRen(int frazIniErogRen) {
        this.gar.getGrzFrazIniErogRen().setGrzFrazIniErogRen(frazIniErogRen);
    }

    @Override
    public Integer getFrazIniErogRenObj() {
        if (ws.getIndGar().getFrazIniErogRen() >= 0) {
            return ((Integer)getFrazIniErogRen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazIniErogRenObj(Integer frazIniErogRenObj) {
        if (frazIniErogRenObj != null) {
            setFrazIniErogRen(((int)frazIniErogRenObj));
            ws.getIndGar().setFrazIniErogRen(((short)0));
        }
        else {
            ws.getIndGar().setFrazIniErogRen(((short)-1));
        }
    }

    @Override
    public String getGrzCodTari() {
        return gar.getGrzCodTari();
    }

    @Override
    public void setGrzCodTari(String grzCodTari) {
        this.gar.setGrzCodTari(grzCodTari);
    }

    @Override
    public int getGrzIdPoli() {
        return gar.getGrzIdPoli();
    }

    @Override
    public void setGrzIdPoli(int grzIdPoli) {
        this.gar.setGrzIdPoli(grzIdPoli);
    }

    @Override
    public String getIbOgg() {
        return gar.getGrzIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.gar.setGrzIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndGar().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndGar().setIbOgg(((short)0));
        }
        else {
            ws.getIndGar().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getId1oAssto() {
        return gar.getGrzId1oAssto().getGrzId1oAssto();
    }

    @Override
    public void setId1oAssto(int id1oAssto) {
        this.gar.getGrzId1oAssto().setGrzId1oAssto(id1oAssto);
    }

    @Override
    public Integer getId1oAsstoObj() {
        if (ws.getIndGar().getId1oAssto() >= 0) {
            return ((Integer)getId1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId1oAsstoObj(Integer id1oAsstoObj) {
        if (id1oAsstoObj != null) {
            setId1oAssto(((int)id1oAsstoObj));
            ws.getIndGar().setId1oAssto(((short)0));
        }
        else {
            ws.getIndGar().setId1oAssto(((short)-1));
        }
    }

    @Override
    public int getId2oAssto() {
        return gar.getGrzId2oAssto().getGrzId2oAssto();
    }

    @Override
    public void setId2oAssto(int id2oAssto) {
        this.gar.getGrzId2oAssto().setGrzId2oAssto(id2oAssto);
    }

    @Override
    public Integer getId2oAsstoObj() {
        if (ws.getIndGar().getId2oAssto() >= 0) {
            return ((Integer)getId2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId2oAsstoObj(Integer id2oAsstoObj) {
        if (id2oAsstoObj != null) {
            setId2oAssto(((int)id2oAsstoObj));
            ws.getIndGar().setId2oAssto(((short)0));
        }
        else {
            ws.getIndGar().setId2oAssto(((short)-1));
        }
    }

    @Override
    public int getId3oAssto() {
        return gar.getGrzId3oAssto().getGrzId3oAssto();
    }

    @Override
    public void setId3oAssto(int id3oAssto) {
        this.gar.getGrzId3oAssto().setGrzId3oAssto(id3oAssto);
    }

    @Override
    public Integer getId3oAsstoObj() {
        if (ws.getIndGar().getId3oAssto() >= 0) {
            return ((Integer)getId3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId3oAsstoObj(Integer id3oAsstoObj) {
        if (id3oAsstoObj != null) {
            setId3oAssto(((int)id3oAsstoObj));
            ws.getIndGar().setId3oAssto(((short)0));
        }
        else {
            ws.getIndGar().setId3oAssto(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return gar.getGrzIdAdes().getGrzIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.gar.getGrzIdAdes().setGrzIdAdes(idAdes);
    }

    @Override
    public Integer getIdAdesObj() {
        if (ws.getIndGar().getIdAdes() >= 0) {
            return ((Integer)getIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAdesObj(Integer idAdesObj) {
        if (idAdesObj != null) {
            setIdAdes(((int)idAdesObj));
            ws.getIndGar().setIdAdes(((short)0));
        }
        else {
            ws.getIndGar().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getIdGar() {
        return gar.getGrzIdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.gar.setGrzIdGar(idGar);
    }

    @Override
    public int getIdMoviChiu() {
        return gar.getGrzIdMoviChiu().getGrzIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.gar.getGrzIdMoviChiu().setGrzIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndGar().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndGar().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndGar().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return gar.getGrzIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.gar.setGrzIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getLdbv1351TpCaus01() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus01();
    }

    @Override
    public void setLdbv1351TpCaus01(String ldbv1351TpCaus01) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus01(ldbv1351TpCaus01);
    }

    @Override
    public String getLdbv1351TpCaus02() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus02();
    }

    @Override
    public void setLdbv1351TpCaus02(String ldbv1351TpCaus02) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus02(ldbv1351TpCaus02);
    }

    @Override
    public String getLdbv1351TpCaus03() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus03();
    }

    @Override
    public void setLdbv1351TpCaus03(String ldbv1351TpCaus03) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus03(ldbv1351TpCaus03);
    }

    @Override
    public String getLdbv1351TpCaus04() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus04();
    }

    @Override
    public void setLdbv1351TpCaus04(String ldbv1351TpCaus04) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus04(ldbv1351TpCaus04);
    }

    @Override
    public String getLdbv1351TpCaus05() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus05();
    }

    @Override
    public void setLdbv1351TpCaus05(String ldbv1351TpCaus05) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus05(ldbv1351TpCaus05);
    }

    @Override
    public String getLdbv1351TpCaus06() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus06();
    }

    @Override
    public void setLdbv1351TpCaus06(String ldbv1351TpCaus06) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus06(ldbv1351TpCaus06);
    }

    @Override
    public String getLdbv1351TpCaus07() {
        return ws.getLdbv1351().getLdbv1351TpCausTab().getCaus07();
    }

    @Override
    public void setLdbv1351TpCaus07(String ldbv1351TpCaus07) {
        this.ws.getLdbv1351().getLdbv1351TpCausTab().setCaus07(ldbv1351TpCaus07);
    }

    @Override
    public String getLdbv1351TpStatBus01() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus01();
    }

    @Override
    public void setLdbv1351TpStatBus01(String ldbv1351TpStatBus01) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus01(ldbv1351TpStatBus01);
    }

    @Override
    public String getLdbv1351TpStatBus02() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus02();
    }

    @Override
    public void setLdbv1351TpStatBus02(String ldbv1351TpStatBus02) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus02(ldbv1351TpStatBus02);
    }

    @Override
    public String getLdbv1351TpStatBus03() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus03();
    }

    @Override
    public void setLdbv1351TpStatBus03(String ldbv1351TpStatBus03) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus03(ldbv1351TpStatBus03);
    }

    @Override
    public String getLdbv1351TpStatBus04() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus04();
    }

    @Override
    public void setLdbv1351TpStatBus04(String ldbv1351TpStatBus04) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus04(ldbv1351TpStatBus04);
    }

    @Override
    public String getLdbv1351TpStatBus05() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus05();
    }

    @Override
    public void setLdbv1351TpStatBus05(String ldbv1351TpStatBus05) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus05(ldbv1351TpStatBus05);
    }

    @Override
    public String getLdbv1351TpStatBus06() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus06();
    }

    @Override
    public void setLdbv1351TpStatBus06(String ldbv1351TpStatBus06) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus06(ldbv1351TpStatBus06);
    }

    @Override
    public String getLdbv1351TpStatBus07() {
        return ws.getLdbv1351().getLdbv1351StatBusTab().getTpStatBus07();
    }

    @Override
    public void setLdbv1351TpStatBus07(String ldbv1351TpStatBus07) {
        this.ws.getLdbv1351().getLdbv1351StatBusTab().setTpStatBus07(ldbv1351TpStatBus07);
    }

    @Override
    public int getLdbv1421IdPoli() {
        throw new FieldNotMappedException("ldbv1421IdPoli");
    }

    @Override
    public void setLdbv1421IdPoli(int ldbv1421IdPoli) {
        throw new FieldNotMappedException("ldbv1421IdPoli");
    }

    @Override
    public String getLdbv1421RamoBila1() {
        throw new FieldNotMappedException("ldbv1421RamoBila1");
    }

    @Override
    public void setLdbv1421RamoBila1(String ldbv1421RamoBila1) {
        throw new FieldNotMappedException("ldbv1421RamoBila1");
    }

    @Override
    public String getLdbv1421RamoBila2() {
        throw new FieldNotMappedException("ldbv1421RamoBila2");
    }

    @Override
    public void setLdbv1421RamoBila2(String ldbv1421RamoBila2) {
        throw new FieldNotMappedException("ldbv1421RamoBila2");
    }

    @Override
    public String getLdbv1421RamoBila3() {
        throw new FieldNotMappedException("ldbv1421RamoBila3");
    }

    @Override
    public void setLdbv1421RamoBila3(String ldbv1421RamoBila3) {
        throw new FieldNotMappedException("ldbv1421RamoBila3");
    }

    @Override
    public String getLdbv1421RamoBila4() {
        throw new FieldNotMappedException("ldbv1421RamoBila4");
    }

    @Override
    public void setLdbv1421RamoBila4(String ldbv1421RamoBila4) {
        throw new FieldNotMappedException("ldbv1421RamoBila4");
    }

    @Override
    public String getLdbv1421RamoBila5() {
        throw new FieldNotMappedException("ldbv1421RamoBila5");
    }

    @Override
    public void setLdbv1421RamoBila5(String ldbv1421RamoBila5) {
        throw new FieldNotMappedException("ldbv1421RamoBila5");
    }

    @Override
    public int getLdbv3401IdAdes() {
        throw new FieldNotMappedException("ldbv3401IdAdes");
    }

    @Override
    public void setLdbv3401IdAdes(int ldbv3401IdAdes) {
        throw new FieldNotMappedException("ldbv3401IdAdes");
    }

    @Override
    public int getLdbv3401IdPoli() {
        throw new FieldNotMappedException("ldbv3401IdPoli");
    }

    @Override
    public void setLdbv3401IdPoli(int ldbv3401IdPoli) {
        throw new FieldNotMappedException("ldbv3401IdPoli");
    }

    @Override
    public String getLdbv3401Ramo1() {
        throw new FieldNotMappedException("ldbv3401Ramo1");
    }

    @Override
    public void setLdbv3401Ramo1(String ldbv3401Ramo1) {
        throw new FieldNotMappedException("ldbv3401Ramo1");
    }

    @Override
    public String getLdbv3401Ramo2() {
        throw new FieldNotMappedException("ldbv3401Ramo2");
    }

    @Override
    public void setLdbv3401Ramo2(String ldbv3401Ramo2) {
        throw new FieldNotMappedException("ldbv3401Ramo2");
    }

    @Override
    public String getLdbv3401Ramo3() {
        throw new FieldNotMappedException("ldbv3401Ramo3");
    }

    @Override
    public void setLdbv3401Ramo3(String ldbv3401Ramo3) {
        throw new FieldNotMappedException("ldbv3401Ramo3");
    }

    @Override
    public String getLdbv3401Ramo4() {
        throw new FieldNotMappedException("ldbv3401Ramo4");
    }

    @Override
    public void setLdbv3401Ramo4(String ldbv3401Ramo4) {
        throw new FieldNotMappedException("ldbv3401Ramo4");
    }

    @Override
    public String getLdbv3401Ramo5() {
        throw new FieldNotMappedException("ldbv3401Ramo5");
    }

    @Override
    public void setLdbv3401Ramo5(String ldbv3401Ramo5) {
        throw new FieldNotMappedException("ldbv3401Ramo5");
    }

    @Override
    public String getLdbv3401Ramo6() {
        throw new FieldNotMappedException("ldbv3401Ramo6");
    }

    @Override
    public void setLdbv3401Ramo6(String ldbv3401Ramo6) {
        throw new FieldNotMappedException("ldbv3401Ramo6");
    }

    @Override
    public String getLdbv3401TpOgg() {
        throw new FieldNotMappedException("ldbv3401TpOgg");
    }

    @Override
    public void setLdbv3401TpOgg(String ldbv3401TpOgg) {
        throw new FieldNotMappedException("ldbv3401TpOgg");
    }

    @Override
    public String getLdbv3401TpStatBus1() {
        throw new FieldNotMappedException("ldbv3401TpStatBus1");
    }

    @Override
    public void setLdbv3401TpStatBus1(String ldbv3401TpStatBus1) {
        throw new FieldNotMappedException("ldbv3401TpStatBus1");
    }

    @Override
    public String getLdbv3401TpStatBus2() {
        throw new FieldNotMappedException("ldbv3401TpStatBus2");
    }

    @Override
    public void setLdbv3401TpStatBus2(String ldbv3401TpStatBus2) {
        throw new FieldNotMappedException("ldbv3401TpStatBus2");
    }

    @Override
    public int getLdbv3411IdAdes() {
        throw new FieldNotMappedException("ldbv3411IdAdes");
    }

    @Override
    public void setLdbv3411IdAdes(int ldbv3411IdAdes) {
        throw new FieldNotMappedException("ldbv3411IdAdes");
    }

    @Override
    public int getLdbv3411IdPoli() {
        throw new FieldNotMappedException("ldbv3411IdPoli");
    }

    @Override
    public void setLdbv3411IdPoli(int ldbv3411IdPoli) {
        throw new FieldNotMappedException("ldbv3411IdPoli");
    }

    @Override
    public String getLdbv3411TpOgg() {
        throw new FieldNotMappedException("ldbv3411TpOgg");
    }

    @Override
    public void setLdbv3411TpOgg(String ldbv3411TpOgg) {
        throw new FieldNotMappedException("ldbv3411TpOgg");
    }

    @Override
    public String getLdbv3411TpStatBus1() {
        throw new FieldNotMappedException("ldbv3411TpStatBus1");
    }

    @Override
    public void setLdbv3411TpStatBus1(String ldbv3411TpStatBus1) {
        throw new FieldNotMappedException("ldbv3411TpStatBus1");
    }

    @Override
    public String getLdbv3411TpStatBus2() {
        throw new FieldNotMappedException("ldbv3411TpStatBus2");
    }

    @Override
    public void setLdbv3411TpStatBus2(String ldbv3411TpStatBus2) {
        throw new FieldNotMappedException("ldbv3411TpStatBus2");
    }

    @Override
    public int getLdbv9091IdAdes() {
        throw new FieldNotMappedException("ldbv9091IdAdes");
    }

    @Override
    public void setLdbv9091IdAdes(int ldbv9091IdAdes) {
        throw new FieldNotMappedException("ldbv9091IdAdes");
    }

    @Override
    public int getLdbv9091IdPoli() {
        throw new FieldNotMappedException("ldbv9091IdPoli");
    }

    @Override
    public void setLdbv9091IdPoli(int ldbv9091IdPoli) {
        throw new FieldNotMappedException("ldbv9091IdPoli");
    }

    @Override
    public String getLdbv9091Ramo1() {
        throw new FieldNotMappedException("ldbv9091Ramo1");
    }

    @Override
    public void setLdbv9091Ramo1(String ldbv9091Ramo1) {
        throw new FieldNotMappedException("ldbv9091Ramo1");
    }

    @Override
    public String getLdbv9091Ramo2() {
        throw new FieldNotMappedException("ldbv9091Ramo2");
    }

    @Override
    public void setLdbv9091Ramo2(String ldbv9091Ramo2) {
        throw new FieldNotMappedException("ldbv9091Ramo2");
    }

    @Override
    public String getLdbv9091Ramo3() {
        throw new FieldNotMappedException("ldbv9091Ramo3");
    }

    @Override
    public void setLdbv9091Ramo3(String ldbv9091Ramo3) {
        throw new FieldNotMappedException("ldbv9091Ramo3");
    }

    @Override
    public String getLdbv9091Ramo4() {
        throw new FieldNotMappedException("ldbv9091Ramo4");
    }

    @Override
    public void setLdbv9091Ramo4(String ldbv9091Ramo4) {
        throw new FieldNotMappedException("ldbv9091Ramo4");
    }

    @Override
    public String getLdbv9091Ramo5() {
        throw new FieldNotMappedException("ldbv9091Ramo5");
    }

    @Override
    public void setLdbv9091Ramo5(String ldbv9091Ramo5) {
        throw new FieldNotMappedException("ldbv9091Ramo5");
    }

    @Override
    public String getLdbv9091Ramo6() {
        throw new FieldNotMappedException("ldbv9091Ramo6");
    }

    @Override
    public void setLdbv9091Ramo6(String ldbv9091Ramo6) {
        throw new FieldNotMappedException("ldbv9091Ramo6");
    }

    @Override
    public short getLdbv9091TpInvst1() {
        throw new FieldNotMappedException("ldbv9091TpInvst1");
    }

    @Override
    public void setLdbv9091TpInvst1(short ldbv9091TpInvst1) {
        throw new FieldNotMappedException("ldbv9091TpInvst1");
    }

    @Override
    public short getLdbv9091TpInvst2() {
        throw new FieldNotMappedException("ldbv9091TpInvst2");
    }

    @Override
    public void setLdbv9091TpInvst2(short ldbv9091TpInvst2) {
        throw new FieldNotMappedException("ldbv9091TpInvst2");
    }

    @Override
    public short getLdbv9091TpInvst3() {
        throw new FieldNotMappedException("ldbv9091TpInvst3");
    }

    @Override
    public void setLdbv9091TpInvst3(short ldbv9091TpInvst3) {
        throw new FieldNotMappedException("ldbv9091TpInvst3");
    }

    @Override
    public short getLdbv9091TpInvst4() {
        throw new FieldNotMappedException("ldbv9091TpInvst4");
    }

    @Override
    public void setLdbv9091TpInvst4(short ldbv9091TpInvst4) {
        throw new FieldNotMappedException("ldbv9091TpInvst4");
    }

    @Override
    public short getLdbv9091TpInvst5() {
        throw new FieldNotMappedException("ldbv9091TpInvst5");
    }

    @Override
    public void setLdbv9091TpInvst5(short ldbv9091TpInvst5) {
        throw new FieldNotMappedException("ldbv9091TpInvst5");
    }

    @Override
    public short getLdbv9091TpInvst6() {
        throw new FieldNotMappedException("ldbv9091TpInvst6");
    }

    @Override
    public void setLdbv9091TpInvst6(short ldbv9091TpInvst6) {
        throw new FieldNotMappedException("ldbv9091TpInvst6");
    }

    @Override
    public String getLdbv9091TpOgg() {
        throw new FieldNotMappedException("ldbv9091TpOgg");
    }

    @Override
    public void setLdbv9091TpOgg(String ldbv9091TpOgg) {
        throw new FieldNotMappedException("ldbv9091TpOgg");
    }

    @Override
    public String getLdbv9091TpStatBus1() {
        throw new FieldNotMappedException("ldbv9091TpStatBus1");
    }

    @Override
    public void setLdbv9091TpStatBus1(String ldbv9091TpStatBus1) {
        throw new FieldNotMappedException("ldbv9091TpStatBus1");
    }

    @Override
    public String getLdbv9091TpStatBus2() {
        throw new FieldNotMappedException("ldbv9091TpStatBus2");
    }

    @Override
    public void setLdbv9091TpStatBus2(String ldbv9091TpStatBus2) {
        throw new FieldNotMappedException("ldbv9091TpStatBus2");
    }

    @Override
    public short getMm1oRat() {
        return gar.getGrzMm1oRat().getGrzMm1oRat();
    }

    @Override
    public void setMm1oRat(short mm1oRat) {
        this.gar.getGrzMm1oRat().setGrzMm1oRat(mm1oRat);
    }

    @Override
    public Short getMm1oRatObj() {
        if (ws.getIndGar().getMm1oRat() >= 0) {
            return ((Short)getMm1oRat());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMm1oRatObj(Short mm1oRatObj) {
        if (mm1oRatObj != null) {
            setMm1oRat(((short)mm1oRatObj));
            ws.getIndGar().setMm1oRat(((short)0));
        }
        else {
            ws.getIndGar().setMm1oRat(((short)-1));
        }
    }

    @Override
    public int getMmPagPreUni() {
        return gar.getGrzMmPagPreUni().getGrzMmPagPreUni();
    }

    @Override
    public void setMmPagPreUni(int mmPagPreUni) {
        this.gar.getGrzMmPagPreUni().setGrzMmPagPreUni(mmPagPreUni);
    }

    @Override
    public Integer getMmPagPreUniObj() {
        if (ws.getIndGar().getMmPagPreUni() >= 0) {
            return ((Integer)getMmPagPreUni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmPagPreUniObj(Integer mmPagPreUniObj) {
        if (mmPagPreUniObj != null) {
            setMmPagPreUni(((int)mmPagPreUniObj));
            ws.getIndGar().setMmPagPreUni(((short)0));
        }
        else {
            ws.getIndGar().setMmPagPreUni(((short)-1));
        }
    }

    @Override
    public String getModPagGarcol() {
        return gar.getGrzModPagGarcol();
    }

    @Override
    public void setModPagGarcol(String modPagGarcol) {
        this.gar.setGrzModPagGarcol(modPagGarcol);
    }

    @Override
    public String getModPagGarcolObj() {
        if (ws.getIndGar().getModPagGarcol() >= 0) {
            return getModPagGarcol();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModPagGarcolObj(String modPagGarcolObj) {
        if (modPagGarcolObj != null) {
            setModPagGarcol(modPagGarcolObj);
            ws.getIndGar().setModPagGarcol(((short)0));
        }
        else {
            ws.getIndGar().setModPagGarcol(((short)-1));
        }
    }

    @Override
    public int getNumAaPagPre() {
        return gar.getGrzNumAaPagPre().getGrzNumAaPagPre();
    }

    @Override
    public void setNumAaPagPre(int numAaPagPre) {
        this.gar.getGrzNumAaPagPre().setGrzNumAaPagPre(numAaPagPre);
    }

    @Override
    public Integer getNumAaPagPreObj() {
        if (ws.getIndGar().getNumAaPagPre() >= 0) {
            return ((Integer)getNumAaPagPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumAaPagPreObj(Integer numAaPagPreObj) {
        if (numAaPagPreObj != null) {
            setNumAaPagPre(((int)numAaPagPreObj));
            ws.getIndGar().setNumAaPagPre(((short)0));
        }
        else {
            ws.getIndGar().setNumAaPagPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getPc1oRat() {
        return gar.getGrzPc1oRat().getGrzPc1oRat();
    }

    @Override
    public void setPc1oRat(AfDecimal pc1oRat) {
        this.gar.getGrzPc1oRat().setGrzPc1oRat(pc1oRat.copy());
    }

    @Override
    public AfDecimal getPc1oRatObj() {
        if (ws.getIndGar().getPc1oRat() >= 0) {
            return getPc1oRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPc1oRatObj(AfDecimal pc1oRatObj) {
        if (pc1oRatObj != null) {
            setPc1oRat(new AfDecimal(pc1oRatObj, 6, 3));
            ws.getIndGar().setPc1oRat(((short)0));
        }
        else {
            ws.getIndGar().setPc1oRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcOpz() {
        return gar.getGrzPcOpz().getGrzPcOpz();
    }

    @Override
    public void setPcOpz(AfDecimal pcOpz) {
        this.gar.getGrzPcOpz().setGrzPcOpz(pcOpz.copy());
    }

    @Override
    public AfDecimal getPcOpzObj() {
        if (ws.getIndGar().getPcOpz() >= 0) {
            return getPcOpz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcOpzObj(AfDecimal pcOpzObj) {
        if (pcOpzObj != null) {
            setPcOpz(new AfDecimal(pcOpzObj, 6, 3));
            ws.getIndGar().setPcOpz(((short)0));
        }
        else {
            ws.getIndGar().setPcOpz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRevrsb() {
        return gar.getGrzPcRevrsb().getGrzPcRevrsb();
    }

    @Override
    public void setPcRevrsb(AfDecimal pcRevrsb) {
        this.gar.getGrzPcRevrsb().setGrzPcRevrsb(pcRevrsb.copy());
    }

    @Override
    public AfDecimal getPcRevrsbObj() {
        if (ws.getIndGar().getPcRevrsb() >= 0) {
            return getPcRevrsb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRevrsbObj(AfDecimal pcRevrsbObj) {
        if (pcRevrsbObj != null) {
            setPcRevrsb(new AfDecimal(pcRevrsbObj, 6, 3));
            ws.getIndGar().setPcRevrsb(((short)0));
        }
        else {
            ws.getIndGar().setPcRevrsb(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPre() {
        return gar.getGrzPcRipPre().getGrzPcRipPre();
    }

    @Override
    public void setPcRipPre(AfDecimal pcRipPre) {
        this.gar.getGrzPcRipPre().setGrzPcRipPre(pcRipPre.copy());
    }

    @Override
    public AfDecimal getPcRipPreObj() {
        if (ws.getIndGar().getPcRipPre() >= 0) {
            return getPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPreObj(AfDecimal pcRipPreObj) {
        if (pcRipPreObj != null) {
            setPcRipPre(new AfDecimal(pcRipPreObj, 6, 3));
            ws.getIndGar().setPcRipPre(((short)0));
        }
        else {
            ws.getIndGar().setPcRipPre(((short)-1));
        }
    }

    @Override
    public String getRamoBila() {
        return gar.getGrzRamoBila();
    }

    @Override
    public void setRamoBila(String ramoBila) {
        this.gar.setGrzRamoBila(ramoBila);
    }

    @Override
    public String getRamoBilaObj() {
        if (ws.getIndGar().getRamoBila() >= 0) {
            return getRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        if (ramoBilaObj != null) {
            setRamoBila(ramoBilaObj);
            ws.getIndGar().setRamoBila(((short)0));
        }
        else {
            ws.getIndGar().setRamoBila(((short)-1));
        }
    }

    @Override
    public char getRshInvst() {
        return gar.getGrzRshInvst();
    }

    @Override
    public void setRshInvst(char rshInvst) {
        this.gar.setGrzRshInvst(rshInvst);
    }

    @Override
    public Character getRshInvstObj() {
        if (ws.getIndGar().getRshInvst() >= 0) {
            return ((Character)getRshInvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRshInvstObj(Character rshInvstObj) {
        if (rshInvstObj != null) {
            setRshInvst(((char)rshInvstObj));
            ws.getIndGar().setRshInvst(((short)0));
        }
        else {
            ws.getIndGar().setRshInvst(((short)-1));
        }
    }

    @Override
    public int getStbCodCompAnia() {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public char getStbDsOperSql() {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public long getStbDsRiga() {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public char getStbDsStatoElab() {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public long getStbDsTsEndCptz() {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public long getStbDsTsIniCptz() {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public String getStbDsUtente() {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public int getStbDsVer() {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public String getStbDtEndEffDb() {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public String getStbDtIniEffDb() {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public int getStbIdMoviChiu() {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        return ((Integer)getStbIdMoviChiu());
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        setStbIdMoviChiu(((int)stbIdMoviChiuObj));
    }

    @Override
    public int getStbIdMoviCrz() {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public int getStbIdOgg() {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public int getStbIdStatOggBus() {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public String getStbTpCaus() {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public String getStbTpOgg() {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public String getStbTpStatBus() {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public char getTpAdegPre() {
        return gar.getGrzTpAdegPre();
    }

    @Override
    public void setTpAdegPre(char tpAdegPre) {
        this.gar.setGrzTpAdegPre(tpAdegPre);
    }

    @Override
    public Character getTpAdegPreObj() {
        if (ws.getIndGar().getTpAdegPre() >= 0) {
            return ((Character)getTpAdegPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegPreObj(Character tpAdegPreObj) {
        if (tpAdegPreObj != null) {
            setTpAdegPre(((char)tpAdegPreObj));
            ws.getIndGar().setTpAdegPre(((short)0));
        }
        else {
            ws.getIndGar().setTpAdegPre(((short)-1));
        }
    }

    @Override
    public String getTpCalcPrePrstz() {
        return gar.getGrzTpCalcPrePrstz();
    }

    @Override
    public void setTpCalcPrePrstz(String tpCalcPrePrstz) {
        this.gar.setGrzTpCalcPrePrstz(tpCalcPrePrstz);
    }

    @Override
    public String getTpCalcPrePrstzObj() {
        if (ws.getIndGar().getTpCalcPrePrstz() >= 0) {
            return getTpCalcPrePrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcPrePrstzObj(String tpCalcPrePrstzObj) {
        if (tpCalcPrePrstzObj != null) {
            setTpCalcPrePrstz(tpCalcPrePrstzObj);
            ws.getIndGar().setTpCalcPrePrstz(((short)0));
        }
        else {
            ws.getIndGar().setTpCalcPrePrstz(((short)-1));
        }
    }

    @Override
    public String getTpCessRiass() {
        return gar.getGrzTpCessRiass();
    }

    @Override
    public void setTpCessRiass(String tpCessRiass) {
        this.gar.setGrzTpCessRiass(tpCessRiass);
    }

    @Override
    public String getTpCessRiassObj() {
        if (ws.getIndGar().getTpCessRiass() >= 0) {
            return getTpCessRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCessRiassObj(String tpCessRiassObj) {
        if (tpCessRiassObj != null) {
            setTpCessRiass(tpCessRiassObj);
            ws.getIndGar().setTpCessRiass(((short)0));
        }
        else {
            ws.getIndGar().setTpCessRiass(((short)-1));
        }
    }

    @Override
    public String getTpDtEmisRiass() {
        return gar.getGrzTpDtEmisRiass();
    }

    @Override
    public void setTpDtEmisRiass(String tpDtEmisRiass) {
        this.gar.setGrzTpDtEmisRiass(tpDtEmisRiass);
    }

    @Override
    public String getTpDtEmisRiassObj() {
        if (ws.getIndGar().getTpDtEmisRiass() >= 0) {
            return getTpDtEmisRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDtEmisRiassObj(String tpDtEmisRiassObj) {
        if (tpDtEmisRiassObj != null) {
            setTpDtEmisRiass(tpDtEmisRiassObj);
            ws.getIndGar().setTpDtEmisRiass(((short)0));
        }
        else {
            ws.getIndGar().setTpDtEmisRiass(((short)-1));
        }
    }

    @Override
    public String getTpDur() {
        return gar.getGrzTpDur();
    }

    @Override
    public void setTpDur(String tpDur) {
        this.gar.setGrzTpDur(tpDur);
    }

    @Override
    public String getTpDurObj() {
        if (ws.getIndGar().getTpDur() >= 0) {
            return getTpDur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDurObj(String tpDurObj) {
        if (tpDurObj != null) {
            setTpDur(tpDurObj);
            ws.getIndGar().setTpDur(((short)0));
        }
        else {
            ws.getIndGar().setTpDur(((short)-1));
        }
    }

    @Override
    public char getTpEmisPur() {
        return gar.getGrzTpEmisPur();
    }

    @Override
    public void setTpEmisPur(char tpEmisPur) {
        this.gar.setGrzTpEmisPur(tpEmisPur);
    }

    @Override
    public Character getTpEmisPurObj() {
        if (ws.getIndGar().getTpEmisPur() >= 0) {
            return ((Character)getTpEmisPur());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpEmisPurObj(Character tpEmisPurObj) {
        if (tpEmisPurObj != null) {
            setTpEmisPur(((char)tpEmisPurObj));
            ws.getIndGar().setTpEmisPur(((short)0));
        }
        else {
            ws.getIndGar().setTpEmisPur(((short)-1));
        }
    }

    @Override
    public short getTpGar() {
        return gar.getGrzTpGar().getGrzTpGar();
    }

    @Override
    public void setTpGar(short tpGar) {
        this.gar.getGrzTpGar().setGrzTpGar(tpGar);
    }

    @Override
    public Short getTpGarObj() {
        if (ws.getIndGar().getTpGar() >= 0) {
            return ((Short)getTpGar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpGarObj(Short tpGarObj) {
        if (tpGarObj != null) {
            setTpGar(((short)tpGarObj));
            ws.getIndGar().setTpGar(((short)0));
        }
        else {
            ws.getIndGar().setTpGar(((short)-1));
        }
    }

    @Override
    public String getTpIas() {
        return gar.getGrzTpIas();
    }

    @Override
    public void setTpIas(String tpIas) {
        this.gar.setGrzTpIas(tpIas);
    }

    @Override
    public String getTpIasObj() {
        if (ws.getIndGar().getTpIas() >= 0) {
            return getTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        if (tpIasObj != null) {
            setTpIas(tpIasObj);
            ws.getIndGar().setTpIas(((short)0));
        }
        else {
            ws.getIndGar().setTpIas(((short)-1));
        }
    }

    @Override
    public short getTpInvst() {
        return gar.getGrzTpInvst().getGrzTpInvst();
    }

    @Override
    public void setTpInvst(short tpInvst) {
        this.gar.getGrzTpInvst().setGrzTpInvst(tpInvst);
    }

    @Override
    public Short getTpInvstObj() {
        if (ws.getIndGar().getTpInvst() >= 0) {
            return ((Short)getTpInvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpInvstObj(Short tpInvstObj) {
        if (tpInvstObj != null) {
            setTpInvst(((short)tpInvstObj));
            ws.getIndGar().setTpInvst(((short)0));
        }
        else {
            ws.getIndGar().setTpInvst(((short)-1));
        }
    }

    @Override
    public String getTpPcRip() {
        return gar.getGrzTpPcRip();
    }

    @Override
    public void setTpPcRip(String tpPcRip) {
        this.gar.setGrzTpPcRip(tpPcRip);
    }

    @Override
    public String getTpPcRipObj() {
        if (ws.getIndGar().getTpPcRip() >= 0) {
            return getTpPcRip();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPcRipObj(String tpPcRipObj) {
        if (tpPcRipObj != null) {
            setTpPcRip(tpPcRipObj);
            ws.getIndGar().setTpPcRip(((short)0));
        }
        else {
            ws.getIndGar().setTpPcRip(((short)-1));
        }
    }

    @Override
    public String getTpPerPre() {
        return gar.getGrzTpPerPre();
    }

    @Override
    public void setTpPerPre(String tpPerPre) {
        this.gar.setGrzTpPerPre(tpPerPre);
    }

    @Override
    public String getTpPerPreObj() {
        if (ws.getIndGar().getTpPerPre() >= 0) {
            return getTpPerPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPerPreObj(String tpPerPreObj) {
        if (tpPerPreObj != null) {
            setTpPerPre(tpPerPreObj);
            ws.getIndGar().setTpPerPre(((short)0));
        }
        else {
            ws.getIndGar().setTpPerPre(((short)-1));
        }
    }

    @Override
    public char getTpPre() {
        return gar.getGrzTpPre();
    }

    @Override
    public void setTpPre(char tpPre) {
        this.gar.setGrzTpPre(tpPre);
    }

    @Override
    public Character getTpPreObj() {
        if (ws.getIndGar().getTpPre() >= 0) {
            return ((Character)getTpPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        if (tpPreObj != null) {
            setTpPre(((char)tpPreObj));
            ws.getIndGar().setTpPre(((short)0));
        }
        else {
            ws.getIndGar().setTpPre(((short)-1));
        }
    }

    @Override
    public String getTpPrstzAssta() {
        return gar.getGrzTpPrstzAssta();
    }

    @Override
    public void setTpPrstzAssta(String tpPrstzAssta) {
        this.gar.setGrzTpPrstzAssta(tpPrstzAssta);
    }

    @Override
    public String getTpPrstzAsstaObj() {
        if (ws.getIndGar().getTpPrstzAssta() >= 0) {
            return getTpPrstzAssta();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrstzAsstaObj(String tpPrstzAsstaObj) {
        if (tpPrstzAsstaObj != null) {
            setTpPrstzAssta(tpPrstzAsstaObj);
            ws.getIndGar().setTpPrstzAssta(((short)0));
        }
        else {
            ws.getIndGar().setTpPrstzAssta(((short)-1));
        }
    }

    @Override
    public String getTpRamoBila() {
        return gar.getGrzTpRamoBila();
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        this.gar.setGrzTpRamoBila(tpRamoBila);
    }

    @Override
    public String getTpRsh() {
        return gar.getGrzTpRsh();
    }

    @Override
    public void setTpRsh(String tpRsh) {
        this.gar.setGrzTpRsh(tpRsh);
    }

    @Override
    public String getTpRshObj() {
        if (ws.getIndGar().getTpRsh() >= 0) {
            return getTpRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRshObj(String tpRshObj) {
        if (tpRshObj != null) {
            setTpRsh(tpRshObj);
            ws.getIndGar().setTpRsh(((short)0));
        }
        else {
            ws.getIndGar().setTpRsh(((short)-1));
        }
    }

    @Override
    public String getTpStab() {
        return gar.getGrzTpStab();
    }

    @Override
    public void setTpStab(String tpStab) {
        this.gar.setGrzTpStab(tpStab);
    }

    @Override
    public String getTpStabObj() {
        if (ws.getIndGar().getTpStab() >= 0) {
            return getTpStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStabObj(String tpStabObj) {
        if (tpStabObj != null) {
            setTpStab(tpStabObj);
            ws.getIndGar().setTpStab(((short)0));
        }
        else {
            ws.getIndGar().setTpStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsStabLimitata() {
        return gar.getGrzTsStabLimitata().getGrzTsStabLimitata();
    }

    @Override
    public void setTsStabLimitata(AfDecimal tsStabLimitata) {
        this.gar.getGrzTsStabLimitata().setGrzTsStabLimitata(tsStabLimitata.copy());
    }

    @Override
    public AfDecimal getTsStabLimitataObj() {
        if (ws.getIndGar().getTsStabLimitata() >= 0) {
            return getTsStabLimitata();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsStabLimitataObj(AfDecimal tsStabLimitataObj) {
        if (tsStabLimitataObj != null) {
            setTsStabLimitata(new AfDecimal(tsStabLimitataObj, 14, 9));
            ws.getIndGar().setTsStabLimitata(((short)0));
        }
        else {
            ws.getIndGar().setTsStabLimitata(((short)-1));
        }
    }

    @Override
    public int getWkIdAdesA() {
        return ws.getWkIdAdesA();
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        this.ws.setWkIdAdesA(wkIdAdesA);
    }

    @Override
    public int getWkIdAdesDa() {
        return ws.getWkIdAdesDa();
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        this.ws.setWkIdAdesDa(wkIdAdesDa);
    }

    @Override
    public int getWkIdGarA() {
        return ws.getWkIdGarA();
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        this.ws.setWkIdGarA(wkIdGarA);
    }

    @Override
    public int getWkIdGarDa() {
        return ws.getWkIdGarDa();
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        this.ws.setWkIdGarDa(wkIdGarDa);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
