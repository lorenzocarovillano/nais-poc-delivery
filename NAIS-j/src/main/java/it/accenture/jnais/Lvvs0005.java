package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0005Data;

/**Original name: LVVS0005<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0005
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CALCOLO VARIABILE SESSO ASS SX,SY,SZ
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0005 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0005Data ws = new Lvvs0005Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0005
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0005_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0005 getInstance() {
        return ((Lvvs0005)Programs.getInstance(Lvvs0005.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI.
        initIxIndici();
        // COB_CODE: INITIALIZE                        AREA-APPOGGIO.
        initAreaAppoggio();
        // COB_CODE: INITIALIZE                        IVVC0213-TAB-OUTPUT.
        initTabOutput();
        //
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //               VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF IVVC0213-TP-LIVELLO = 'G'
        //              PERFORM S2027-RAN-GAR          THRU S2027-EX
        //           ELSE
        //              PERFORM S3027-RAN-POL          THRU S3027-EX
        //           END-IF.
        if (ivvc0213.getDatiLivello().getTpLivello() == 'G') {
            // COB_CODE: PERFORM S2027-RAN-GAR          THRU S2027-EX
            s2027RanGar();
        }
        else {
            // COB_CODE: PERFORM S3027-RAN-POL          THRU S3027-EX
            s3027RanPol();
        }
    }

    /**Original name: S2027-RAN-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORA  RAPP ANA GAR
	 * ----------------------------------------------------------------*</pre>*/
    private void s2027RanGar() {
        // COB_CODE: IF WRAN-SEX(IVVC0213-IX-TABB) = 'M'
        //              MOVE 0      TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              MOVE 1      TO IVVC0213-VAL-IMP-O
        //           END-IF.
        if (ws.getWranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranSex() == 'M') {
            // COB_CODE: MOVE 0      TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        }
        else {
            // COB_CODE: MOVE 1      TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
        }
    }

    /**Original name: S3027-RAN-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORA  RAPP ANA GAR
	 * ----------------------------------------------------------------*</pre>*/
    private void s3027RanPol() {
        // COB_CODE: SET WK-LETTO-NO                TO TRUE.
        ws.getWkFindLetto().setNo();
        //
        // COB_CODE: PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
        //             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
        //                OR WK-LETTO-SI
        //                END-IF
        //           END-PERFORM.
        ws.setIxTabRan(((short)1));
        while (!(ws.getIxTabRan() > ws.getWranEleRappAnagMax() || ws.getWkFindLetto().isSi())) {
            // COB_CODE: IF WRAN-TP-OGG(IX-TAB-RAN) = 'PO'
            //             END-IF
            //           END-IF
            if (Conditions.eq(ws.getWranTabRappAnag(ws.getIxTabRan()).getLccvran1().getDati().getWranTpOgg(), "PO")) {
                // COB_CODE: IF IVVC0213-ID-POL-GAR
                //            = WRAN-ID-OGG(IX-TAB-RAN)
                //              END-IF
                //           END-IF
                if (ivvc0213.getIdPolGar() == ws.getWranTabRappAnag(ws.getIxTabRan()).getLccvran1().getDati().getWranIdOgg()) {
                    // COB_CODE: SET WK-LETTO-SI              TO TRUE
                    ws.getWkFindLetto().setSi();
                    // COB_CODE: IF WRAN-SEX(IX-TAB-RAN) = 'M'
                    //              MOVE 0      TO IVVC0213-VAL-IMP-O
                    //           ELSE
                    //              MOVE 1      TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    if (ws.getWranTabRappAnag(ws.getIxTabRan()).getLccvran1().getDati().getWranSex() == 'M') {
                        // COB_CODE: MOVE 0      TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
                    }
                    else {
                        // COB_CODE: MOVE 1      TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
                    }
                }
            }
            ws.setIxTabRan(Trunc.toShort(ws.getIxTabRan() + 1, 4));
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO WRAN-AREA-RAPP-ANAG
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO WRAN-AREA-RAPP-ANAG
            ws.setWranAreaRappAnagFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabRan(((short)0));
    }

    public void initAreaAppoggio() {
        ws.setWranEleRappAnagMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0005Data.WRAN_TAB_RAPP_ANAG_MAXOCCURS; idx0++) {
            ws.getWranTabRappAnag(idx0).getLccvran1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().setIdPtf(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIdRappAna(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().getWranIdRappAnaCollg().setWranIdRappAnaCollg(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIdOgg(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpOgg("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIdMoviCrz(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().getWranIdMoviChiu().setWranIdMoviChiu(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDtIniEff(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDtEndEff(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodCompAnia(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodSogg("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpRappAna("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpPers(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranSex(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().getWranDtNasc().setWranDtNasc(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlEstas(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir1("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir2("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir3("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir1("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir2("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir3("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAccr("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAdd("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrDocto("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().getWranPcNelRapp().setWranPcNelRapp(new AfDecimal(0, 6, 3));
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAdd("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAccr("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodMatr("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpAdegz("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlTstRsh(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodAz("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndPrinc("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeliberaCda().setWranDtDeliberaCda(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDlgAlRisc(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranLegaleRapprPrinc(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpLegaleRappr("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpIndPrinc("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpStatRid("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntRid("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntRid("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntTratt("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntTratt("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCfIntRid("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincDipCntr(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincIntCntr(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeces().setWranDtDeces(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlFumatore(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsRiga(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsOperSql(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsVer(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsIniCptz(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsEndCptz(0);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsUtente("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranDsStatoElab(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranFlLavDip(Types.SPACE_CHAR);
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpVarzPagat("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCodRid("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranTpCausRid("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranIndMassaCorp("");
            ws.getWranTabRappAnag(idx0).getLccvran1().getDati().setWranCatRshProf("");
        }
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }
}
