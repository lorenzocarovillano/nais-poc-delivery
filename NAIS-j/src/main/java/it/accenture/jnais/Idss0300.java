package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.AddressOf;
import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TemporaryDataDao;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.copy.TemporaryData;
import it.accenture.jnais.ws.enums.Idsv0301Operazione;
import it.accenture.jnais.ws.Idss0300Data;
import it.accenture.jnais.ws.Idsv0301;
import it.accenture.jnais.ws.ptr.Idsv0302;
import javax.inject.Inject;
import static java.lang.Math.abs;

/**Original name: IDSS0300<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  10 NOV 2009.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idss0300 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    @Inject
    private IPointerManager pointerManager;
    private TemporaryDataDao temporaryDataDao = new TemporaryDataDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idss0300Data ws = new Idss0300Data();
    //Original name: IDSV0301
    private Idsv0301 idsv0301;
    //Original name: IDSV0302
    private Idsv0302 idsv0302 = new Idsv0302(null);

    //==== METHODS ====
    /**Original name: PROGRAM_IDSS0300_FIRST_SENTENCES<br>*/
    public long execute(Idsv0301 idsv0301, Idsv0302 idsv0302) {
        this.idsv0301 = idsv0301;
        this.idsv0302.assignBc(idsv0302);
        registerArgListeners();
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0301-ESITO-OK
        //              END-IF
        //           END-IF.
        if (this.idsv0301.getEsito().isOk()) {
            // COB_CODE: IF IDSV0301-DECLARE
            //              PERFORM D000-CREA-SESSION-TABLE  THRU D000-EX
            //           ELSE
            //              PERFORM B000-ELABORA             THRU B000-EX
            //           END-IF
            if (this.idsv0301.getOperazione().isDeclare()) {
                // COB_CODE: PERFORM D000-CREA-SESSION-TABLE  THRU D000-EX
                d000CreaSessionTable();
            }
            else {
                // COB_CODE: PERFORM B000-ELABORA             THRU B000-EX
                b000Elabora();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        deleteArgListeners();
        return 0;
    }

    public static Idss0300 getInstance() {
        return ((Idss0300)Programs.getInstance(Idss0300.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDSS0300'             TO IDSV0301-COD-SERVIZIO-BE.
        idsv0301.setCodServizioBe("IDSS0300");
        // COB_CODE: SET IDSV0301-ESITO-OK       TO TRUE.
        idsv0301.getEsito().setOk();
        // COB_CODE: SET IDSV0301-SUCCESSFUL-SQL TO TRUE.
        idsv0301.getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET TEMPORARY-DATA-TROVATI-NO TO TRUE.
        ws.getTemporaryDataTrovati().setNo();
        // COB_CODE: MOVE SPACES                 TO IDSV0301-DESC-ERRORE-ESTESA.
        idsv0301.setDescErroreEstesa("");
        // COB_CODE: PERFORM A010-CNTL-INPUT     THRU A010-EX.
        a010CntlInput();
    }

    /**Original name: A010-CNTL-INPUT<br>
	 * <pre>****************************************************************</pre>*/
    private void a010CntlInput() {
        // COB_CODE: IF IDSV0301-DECLARE
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0301.getOperazione().isDeclare()) {
            // COB_CODE: IF NOT IDSV0301-SESSION-TEMP-TABLE
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            //           END-IF
            if (!idsv0301.getTemporaryTable().isSessionTempTable()) {
                // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
                idsv0301.getEsito().setKo();
                // COB_CODE: MOVE 'DECLARE NON ESEGUIBILE'
                //                                TO IDSV0301-DESC-ERRORE-ESTESA
                idsv0301.setDescErroreEstesa("DECLARE NON ESEGUIBILE");
            }
        }
        else {
            // COB_CODE: SET ADDRESS OF IDSV0302 TO IDSV0301-ADDRESS
            AddressOf.get(pointerManager, idsv0302, idsv0301.getAddress());
            // COB_CODE: IF IDSV0301-WRITE
            //              END-IF
            //           END-IF
            if (idsv0301.getOperazione().isWrite()) {
                // COB_CODE: IF IDSV0301-BUFFER-DATA-LEN NOT NUMERIC OR
                //              IDSV0301-BUFFER-DATA-LEN   = ZEROES
                //                                 TO IDSV0301-DESC-ERRORE-ESTESA
                //           ELSE
                //                 END-IF
                //           END-IF
                if (!Functions.isNumber(idsv0301.getBufferDataLen0()) || idsv0301.getBufferDataLen0() == 0) {
                    // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                    idsv0301.getEsito().setKo();
                    // COB_CODE: MOVE 'BUFFER-DATA-LEN NON VALIDA'
                    //                              TO IDSV0301-DESC-ERRORE-ESTESA
                    idsv0301.setDescErroreEstesa("BUFFER-DATA-LEN NON VALIDA");
                }
                else if (idsv0301.getFlContiguous().isNo() && idsv0301.getBufferDataLen0() > TemporaryData.Len.BUFFER_DATA) {
                    // COB_CODE: IF IDSV0301-CONTIGUOUS-NO   AND
                    //              IDSV0301-BUFFER-DATA-LEN >
                    //              LENGTH OF D08-BUFFER-DATA
                    //                              TO IDSV0301-DESC-ERRORE-ESTESA
                    //              END-IF
                    // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                    idsv0301.getEsito().setKo();
                    // COB_CODE: MOVE 'BUFFER-DATA-LEN NON VALIDA'
                    //                        TO IDSV0301-DESC-ERRORE-ESTESA
                    idsv0301.setDescErroreEstesa("BUFFER-DATA-LEN NON VALIDA");
                }
            }
            // COB_CODE: IF IDSV0301-COD-COMP-ANIA NOT NUMERIC OR
            //              IDSV0301-COD-COMP-ANIA   = ZEROES
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            //           END-IF
            if (!Functions.isNumber(idsv0301.getCodCompAnia()) || idsv0301.getCodCompAnia() == 0) {
                // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
                idsv0301.getEsito().setKo();
                // COB_CODE: MOVE 'COD. COMPAGNIA NON VALIDO'
                //                                TO IDSV0301-DESC-ERRORE-ESTESA
                idsv0301.setDescErroreEstesa("COD. COMPAGNIA NON VALIDO");
            }
            // COB_CODE: IF IDSV0301-ESITO-OK
            //              END-IF
            //           END-IF
            if (idsv0301.getEsito().isOk()) {
                // COB_CODE: IF IDSV0301-ID-TEMPORARY-DATA NOT NUMERIC
                //                                   TO IDSV0301-DESC-ERRORE-ESTESA
                //           END-IF
                if (!Functions.isNumber(idsv0301.getIdTemporaryData())) {
                    // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
                    idsv0301.getEsito().setKo();
                    // COB_CODE: MOVE 'ID TEMPORARY DATA NON VALIDO'
                    //                                TO IDSV0301-DESC-ERRORE-ESTESA
                    idsv0301.setDescErroreEstesa("ID TEMPORARY DATA NON VALIDO");
                }
            }
            // COB_CODE: IF IDSV0301-ESITO-OK
            //              END-IF
            //           END-IF
            if (idsv0301.getEsito().isOk()) {
                // COB_CODE: IF IDSV0301-ALIAS-STR-DATO = HIGH-VALUE OR
                //                                        LOW-VALUE  OR
                //                                        SPACES
                //                                   TO IDSV0301-DESC-ERRORE-ESTESA
                //           END-IF
                if (Characters.EQ_HIGH.test(idsv0301.getAliasStrDato(), Idsv0301.Len.ALIAS_STR_DATO) || Characters.EQ_LOW.test(idsv0301.getAliasStrDato(), Idsv0301.Len.ALIAS_STR_DATO) || Characters.EQ_SPACE.test(idsv0301.getAliasStrDato())) {
                    // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
                    idsv0301.getEsito().setKo();
                    // COB_CODE: MOVE 'ALIAS-STR-DATO NON VALIDO'
                    //                                TO IDSV0301-DESC-ERRORE-ESTESA
                    idsv0301.setDescErroreEstesa("ALIAS-STR-DATO NON VALIDO");
                }
            }
            // COB_CODE: IF IDSV0301-ESITO-OK
            //              END-IF
            //           END-IF
            if (idsv0301.getEsito().isOk()) {
                // COB_CODE: IF IDSV0301-ID-SESSION = HIGH-VALUE OR
                //                                    LOW-VALUE  OR
                //                                    SPACES
                //                                   TO IDSV0301-DESC-ERRORE-ESTESA
                //           END-IF
                if (Characters.EQ_HIGH.test(idsv0301.getIdSessionFormatted()) || Characters.EQ_LOW.test(idsv0301.getIdSessionFormatted()) || Characters.EQ_SPACE.test(idsv0301.getIdSession())) {
                    // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
                    idsv0301.getEsito().setKo();
                    // COB_CODE: MOVE 'SESSIONE NON VALIDA'
                    //                                TO IDSV0301-DESC-ERRORE-ESTESA
                    idsv0301.setDescErroreEstesa("SESSIONE NON VALIDA");
                }
            }
        }
    }

    /**Original name: B000-ELABORA<br>
	 * <pre>****************************************************************</pre>*/
    private void b000Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0301-WRITE
        //                   PERFORM B200-TRATTA-WRITE     THRU B200-EX
        //              WHEN IDSV0301-READ
        //                   PERFORM B300-TRATTA-READ      THRU B300-EX
        //              WHEN IDSV0301-READ-NEXT
        //                   PERFORM B400-TRATTA-READ-NEXT THRU B400-EX
        //              WHEN OTHER
        //                                        TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-EVALUATE.
        switch (idsv0301.getOperazione().getOperazione()) {

            case Idsv0301Operazione.WRITE:// COB_CODE: PERFORM B200-TRATTA-WRITE     THRU B200-EX
                b200TrattaWrite();
                break;

            case Idsv0301Operazione.READ:// COB_CODE: PERFORM B300-TRATTA-READ      THRU B300-EX
                b300TrattaRead();
                break;

            case Idsv0301Operazione.READ_NEXT:// COB_CODE: PERFORM B400-TRATTA-READ-NEXT THRU B400-EX
                b400TrattaReadNext();
                break;

            default:// COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
                idsv0301.getEsito().setKo();
                // COB_CODE: MOVE 'OPERAZIONE NON VALIDA'
                //                                   TO IDSV0301-DESC-ERRORE-ESTESA
                idsv0301.setDescErroreEstesa("OPERAZIONE NON VALIDA");
                break;
        }
    }

    /**Original name: B200-TRATTA-WRITE<br>
	 * <pre>****************************************************************</pre>*/
    private void b200TrattaWrite() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0301-CONTIGUOUS-SI
        //              WHEN IDSV0301-CONTIGUOUS-NO
        //                                         THRU B250-EX
        //              WHEN OTHER
        //                                        TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-EVALUATE.
        if (idsv0301.getFlContiguous().isSi() || idsv0301.getFlContiguous().isNo()) {
            // COB_CODE: MOVE IDSV0301-BUFFER-DATA-LEN
            //                                    TO WK-LEN-TRANS-DATA
            ws.setWkLenTransData(idsv0301.getBufferDataLen0());
            // COB_CODE: MOVE LENGTH OF D08-BUFFER-DATA
            //                                    TO WK-DATA-PACKAGE-MAX
            ws.setWkDataPackageMax(TemporaryData.Len.BUFFER_DATA);
            // COB_CODE: PERFORM CALCOLA-PACKAGES THRU CALCOLA-PACKAGES-EX
            calcolaPackages();
            // COB_CODE: PERFORM B250-SCRIVI-TEMPORARY-TABLE
            //                                    THRU B250-EX
            b250ScriviTemporaryTable();
        }
        else {
            // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'FLAG CONTIGUOUS NON VALIDO'
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("FLAG CONTIGUOUS NON VALIDO");
        }
    }

    /**Original name: B300-TRATTA-READ<br>
	 * <pre>****************************************************************</pre>*/
    private void b300TrattaRead() {
        // COB_CODE: PERFORM E100-DECLARE-CURSOR-PACK       THRU E100-EX
        e100DeclareCursorPack();
        // COB_CODE: IF IDSV0301-ESITO-OK
        //              END-IF
        //           END-IF.
        if (idsv0301.getEsito().isOk()) {
            // COB_CODE: PERFORM E200-OPEN-CURSOR-PACK       THRU E200-EX
            e200OpenCursorPack();
            // COB_CODE: IF IDSV0301-ESITO-OK
            //              END-IF
            //           END-IF
            if (idsv0301.getEsito().isOk()) {
                // COB_CODE: PERFORM E300-FETCH-CURSOR-PACK   THRU E300-EX
                e300FetchCursorPack();
                // COB_CODE: IF IDSV0301-ESITO-OK
                //              END-IF
                //           END-IF
                if (idsv0301.getEsito().isOk()) {
                    // COB_CODE: IF IDSV0301-SUCCESSFUL-SQL
                    //              END-IF
                    //           END-IF
                    if (idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
                        // COB_CODE: MOVE 1                     TO WK-START-POSITION
                        ws.setWkStartPosition(1);
                        // COB_CODE: PERFORM E600-VALORIZZA-OUT THRU E600-EX
                        e600ValorizzaOut();
                        // COB_CODE: IF IDSV0301-CONTIGUOUS-SI
                        //                      UNTIL NOT IDSV0301-SUCCESSFUL-SQL
                        //           END-IF
                        if (idsv0301.getFlContiguous().isSi()) {
                            // COB_CODE: PERFORM B400-TRATTA-READ-NEXT
                            //                                   THRU B400-EX
                            //                   UNTIL NOT IDSV0301-SUCCESSFUL-SQL
                            while (idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
                                b400TrattaReadNext();
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: B250-SCRIVI-TEMPORARY-TABLE<br>
	 * <pre>****************************************************************</pre>*/
    private void b250ScriviTemporaryTable() {
        // COB_CODE: PERFORM V100-VERIFICA-TEMP-TABLE   THRU V100-EX
        v100VerificaTempTable();
        // COB_CODE: MOVE 1               TO WK-START-POSITION
        ws.setWkStartPosition(1);
        // COB_CODE: PERFORM VARYING WK-IND-FRAME FROM 1 BY 1
        //                   UNTIL WK-IND-FRAME > WK-TOT-NUM-FRAMES
        //                   OR    IDSV0301-ESITO-KO
        //                   PERFORM B255-ELAB-CONT    THRU B255-EX
        //           END-PERFORM.
        ws.setWkIndFrame(((short)1));
        while (!(ws.getWkIndFrame() > ws.getWkTotNumFrames() || idsv0301.getEsito().isKo())) {
            // COB_CODE: IF WK-IND-FRAME = WK-TOT-NUM-FRAMES AND
            //              WK-LEN-PACKAGES-NO-STD NOT =  ZEROES
            //                              TO WK-DATA-PACKAGE-MAX
            //           END-IF
            if (ws.getWkIndFrame() == ws.getWkTotNumFrames() && ws.getWkLenPackagesNoStd() != 0) {
                // COB_CODE: MOVE WK-LEN-PACKAGES-NO-STD
                //                           TO WK-DATA-PACKAGE-MAX
                ws.setWkDataPackageMax(ws.getWkLenPackagesNoStd());
            }
            // COB_CODE: PERFORM B255-ELAB-CONT    THRU B255-EX
            b255ElabCont();
            ws.setWkIndFrame(Trunc.toShort(ws.getWkIndFrame() + 1, 4));
        }
    }

    /**Original name: B255-ELAB-CONT<br>
	 * <pre>****************************************************************</pre>*/
    private void b255ElabCont() {
        // COB_CODE: PERFORM B260-VAL-INSERT-CONT      THRU B260-EX
        b260ValInsertCont();
        // COB_CODE: PERFORM A220-INSERT               THRU A220-EX.
        a220Insert();
    }

    /**Original name: B260-VAL-INSERT-CONT<br>
	 * <pre>****************************************************************</pre>*/
    private void b260ValInsertCont() {
        // COB_CODE: INITIALIZE TEMPORARY-DATA.
        initTemporaryData();
        // COB_CODE: MOVE IDSV0301-COD-COMP-ANIA      TO D08-COD-COMP-ANIA
        ws.getTemporaryData().setCodCompAnia(idsv0301.getCodCompAnia());
        // COB_CODE: MOVE IDSV0301-ID-TEMPORARY-DATA  TO D08-ID-TEMPORARY-DATA
        ws.getTemporaryData().setIdTemporaryData(idsv0301.getIdTemporaryData());
        // COB_CODE: MOVE IDSV0301-ALIAS-STR-DATO     TO D08-ALIAS-STR-DATO
        ws.getTemporaryData().setAliasStrDato(idsv0301.getAliasStrDato());
        // COB_CODE: IF TEMPORARY-DATA-TROVATI-NO
        //              MOVE WK-IND-FRAME             TO D08-NUM-FRAME
        //           ELSE
        //              MOVE NUM-FRAME-APPEND         TO D08-NUM-FRAME
        //           END-IF
        if (ws.getTemporaryDataTrovati().isNo()) {
            // COB_CODE: MOVE WK-IND-FRAME             TO D08-NUM-FRAME
            ws.getTemporaryData().setNumFrame(ws.getWkIndFrame());
        }
        else {
            // COB_CODE: MOVE NUM-FRAME-APPEND         TO D08-NUM-FRAME
            ws.getTemporaryData().setNumFrame(ws.getNumFrameAppend());
        }
        // COB_CODE: MOVE IDSV0301-ID-SESSION         TO D08-ID-SESSION
        ws.getTemporaryData().setIdSession(idsv0301.getIdSession());
        // COB_CODE: MOVE IDSV0301-FL-CONTIGUOUS      TO D08-FL-CONTIGUOUS-DATA
        ws.getTemporaryData().setFlContiguousData(idsv0301.getFlContiguous().getFlContiguous());
        // COB_CODE: MOVE IDSV0301-TOTAL-RECURRENCE   TO D08-TOTAL-RECURRENCE
        ws.getTemporaryData().setTotalRecurrence(idsv0301.getTotalRecurrence());
        // COB_CODE: MOVE IDSV0301-PARTIAL-RECURRENCE TO D08-PARTIAL-RECURRENCE
        ws.getTemporaryData().setPartialRecurrence(idsv0301.getPartialRecurrence());
        // COB_CODE: MOVE IDSV0301-ACTUAL-RECURRENCE  TO D08-ACTUAL-RECURRENCE
        ws.getTemporaryData().setActualRecurrence(idsv0301.getActualRecurrence());
        // COB_CODE: MOVE 'I'                         TO D08-DS-OPER-SQL
        ws.getTemporaryData().setDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                           TO D08-DS-VER
        ws.getTemporaryData().setDsVer(0);
        // COB_CODE: MOVE 'USER'                      TO D08-DS-UTENTE
        ws.getTemporaryData().setDsUtente("USER");
        // COB_CODE: MOVE '1'                         TO D08-DS-STATO-ELAB
        ws.getTemporaryData().setDsStatoElabFormatted("1");
        // COB_CODE: MOVE 0                           TO D08-DS-TS-CPTZ
        ws.getTemporaryData().setDsTsCptz(0);
        // COB_CODE: MOVE WK-DATA-PACKAGE-MAX         TO D08-BUFFER-DATA-LEN
        ws.getTemporaryData().setBufferDataLen(((short)(ws.getWkDataPackageMax())));
        // COB_CODE: MOVE SPACES                      TO D08-BUFFER-DATA.
        ws.getTemporaryData().setBufferData("");
        // COB_CODE: MOVE IDSV0302-BUFFER-DATA
        //                (WK-START-POSITION : WK-DATA-PACKAGE-MAX )
        //                                            TO D08-BUFFER-DATA.
        ws.getTemporaryData().setBufferData(idsv0302.getBufferDataFormatted().substring((ws.getWkStartPosition()) - 1, ws.getWkStartPosition() + (((int)(ws.getWkDataPackageMax()))) - 1));
        // COB_CODE: ADD  WK-DATA-PACKAGE-MAX         TO WK-START-POSITION.
        ws.setWkStartPosition(Trunc.toInt(ws.getWkDataPackageMax() + ws.getWkStartPosition(), 9));
    }

    /**Original name: B400-TRATTA-READ-NEXT<br>
	 * <pre>****************************************************************</pre>*/
    private void b400TrattaReadNext() {
        // COB_CODE: PERFORM E300-FETCH-CURSOR-PACK      THRU E300-EX.
        e300FetchCursorPack();
        // COB_CODE: IF IDSV0301-ESITO-OK
        //              END-IF
        //           END-IF.
        if (idsv0301.getEsito().isOk()) {
            // COB_CODE: IF IDSV0301-SUCCESSFUL-SQL
            //              PERFORM E600-VALORIZZA-OUT    THRU E600-EX
            //           END-IF
            if (idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
                // COB_CODE: PERFORM E600-VALORIZZA-OUT    THRU E600-EX
                e600ValorizzaOut();
            }
        }
    }

    /**Original name: E100-DECLARE-CURSOR-PACK<br>
	 * <pre>****************************************************************</pre>*/
    private void e100DeclareCursorPack() {
        // COB_CODE: IF IDSV0301-SESSION-TEMP-TABLE
        //              PERFORM E101-DECL-CUR-PACK-SESSION-TAB THRU E101-EX
        //           ELSE
        //              PERFORM E103-DECL-CUR-PACK-STATIC-TAB  THRU E103-EX
        //           END-IF.
        if (idsv0301.getTemporaryTable().isSessionTempTable()) {
            // COB_CODE: PERFORM E101-DECL-CUR-PACK-SESSION-TAB THRU E101-EX
            e101DeclCurPackSessionTab();
        }
        else {
            // COB_CODE: PERFORM E103-DECL-CUR-PACK-STATIC-TAB  THRU E103-EX
            e103DeclCurPackStaticTab();
        }
    }

    /**Original name: E101-DECL-CUR-PACK-SESSION-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e101DeclCurPackSessionTab() {
    // COB_CODE: EXEC SQL
    //             DECLARE CUR-PACKAGE-SESSIO CURSOR FOR
    //             SELECT
    //                ID_TEMPORARY_DATA
    //                ,ALIAS_STR_DATO
    //                ,NUM_FRAME
    //                ,COD_COMP_ANIA
    //                ,ID_SESSION
    //                ,FL_CONTIGUOUS_DATA
    //                ,TOTAL_RECURRENCE
    //                ,PARTIAL_RECURRENCE
    //                ,ACTUAL_RECURRENCE
    //                ,DS_OPER_SQL
    //                ,DS_VER
    //                ,DS_TS_CPTZ
    //                ,DS_UTENTE
    //                ,DS_STATO_ELAB
    //                ,TYPE_RECORD
    //                ,BUFFER_DATA
    //             FROM SESSION.TEMPORARY_DATA
    //             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
    //                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
    //                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
    //                   ORDER BY COD_COMP_ANIA
    //                           ,ID_TEMPORARY_DATA
    //                           ,ALIAS_STR_DATO
    //                           ,NUM_FRAME
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: E103-DECL-CUR-PACK-STATIC-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e103DeclCurPackStaticTab() {
    // COB_CODE: EXEC SQL
    //             DECLARE CUR-PACKAGE-STATIC CURSOR FOR
    //             SELECT
    //                ID_TEMPORARY_DATA
    //                ,ALIAS_STR_DATO
    //                ,NUM_FRAME
    //                ,COD_COMP_ANIA
    //                ,ID_SESSION
    //                ,FL_CONTIGUOUS_DATA
    //                ,TOTAL_RECURRENCE
    //                ,PARTIAL_RECURRENCE
    //                ,ACTUAL_RECURRENCE
    //                ,DS_OPER_SQL
    //                ,DS_VER
    //                ,DS_TS_CPTZ
    //                ,DS_UTENTE
    //                ,DS_STATO_ELAB
    //                ,TYPE_RECORD
    //                ,BUFFER_DATA
    //             FROM TEMPORARY_DATA
    //             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
    //                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
    //                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
    //                   ORDER BY COD_COMP_ANIA
    //                           ,ID_TEMPORARY_DATA
    //                           ,ALIAS_STR_DATO
    //                           ,NUM_FRAME
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: E200-OPEN-CURSOR-PACK<br>
	 * <pre>****************************************************************</pre>*/
    private void e200OpenCursorPack() {
        // COB_CODE: IF IDSV0301-SESSION-TEMP-TABLE
        //              PERFORM E201-DECL-CUR-PACK-SESSION-TAB THRU E201-EX
        //           ELSE
        //              PERFORM E203-DECL-CUR-PACK-STATIC-TAB  THRU E203-EX
        //           END-IF.
        if (idsv0301.getTemporaryTable().isSessionTempTable()) {
            // COB_CODE: PERFORM E201-DECL-CUR-PACK-SESSION-TAB THRU E201-EX
            e201DeclCurPackSessionTab();
        }
        else {
            // COB_CODE: PERFORM E203-DECL-CUR-PACK-STATIC-TAB  THRU E203-EX
            e203DeclCurPackStaticTab();
        }
    }

    /**Original name: E201-DECL-CUR-PACK-SESSION-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e201DeclCurPackSessionTab() {
        // COB_CODE: EXEC SQL
        //                OPEN CUR-PACKAGE-SESSIO
        //           END-EXEC.
        temporaryDataDao.openCurPackageSessio(idsv0301.getCodCompAnia(), idsv0301.getIdTemporaryData(), idsv0301.getAliasStrDato());
        // COB_CODE: MOVE SQLCODE          TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                    TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'ERRORE OPEN CURSOR STATIC TEMPORARY TABLE'
            //                                 TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE OPEN CURSOR STATIC TEMPORARY TABLE");
        }
    }

    /**Original name: E203-DECL-CUR-PACK-STATIC-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e203DeclCurPackStaticTab() {
        // COB_CODE: EXEC SQL
        //                OPEN CUR-PACKAGE-STATIC
        //           END-EXEC.
        temporaryDataDao.openCurPackageSessio(idsv0301.getCodCompAnia(), idsv0301.getIdTemporaryData(), idsv0301.getAliasStrDato());
        // COB_CODE: MOVE SQLCODE          TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                    TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'ERRORE OPEN CURSOR SESSION TEMPORARY TABLE'
            //                                 TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE OPEN CURSOR SESSION TEMPORARY TABLE");
        }
    }

    /**Original name: E300-FETCH-CURSOR-PACK<br>
	 * <pre>****************************************************************</pre>*/
    private void e300FetchCursorPack() {
        // COB_CODE: IF IDSV0301-SESSION-TEMP-TABLE
        //              PERFORM E301-FTCH-CUR-PACK-SESSION-TAB THRU E301-EX
        //           ELSE
        //              PERFORM E303-FTCH-CUR-PACK-STATIC-TAB  THRU E303-EX
        //           END-IF.
        if (idsv0301.getTemporaryTable().isSessionTempTable()) {
            // COB_CODE: PERFORM E301-FTCH-CUR-PACK-SESSION-TAB THRU E301-EX
            e301FtchCurPackSessionTab();
        }
        else {
            // COB_CODE: PERFORM E303-FTCH-CUR-PACK-STATIC-TAB  THRU E303-EX
            e303FtchCurPackStaticTab();
        }
    }

    /**Original name: E301-FTCH-CUR-PACK-SESSION-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e301FtchCurPackSessionTab() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-PACKAGE-SESSIO
        //             INTO
        //                :D08-ID-TEMPORARY-DATA
        //               ,:D08-ALIAS-STR-DATO
        //               ,:D08-NUM-FRAME
        //               ,:D08-COD-COMP-ANIA
        //               ,:D08-ID-SESSION
        //               ,:D08-FL-CONTIGUOUS-DATA
        //                :IND-D08-FL-CONTIGUOUS-DATA
        //               ,:D08-TOTAL-RECURRENCE
        //                :IND-D08-TOTAL-RECURRENCE
        //               ,:D08-PARTIAL-RECURRENCE
        //                :IND-D08-PARTIAL-RECURRENCE
        //               ,:D08-ACTUAL-RECURRENCE
        //                :IND-D08-ACTUAL-RECURRENCE
        //               ,:D08-DS-OPER-SQL
        //               ,:D08-DS-VER
        //               ,:D08-DS-TS-CPTZ
        //               ,:D08-DS-UTENTE
        //               ,:D08-DS-STATO-ELAB
        //               ,:D08-TYPE-RECORD
        //                :IND-D08-TYPE-RECORD
        //               ,:D08-BUFFER-DATA-VCHAR
        //                :IND-D08-BUFFER-DATA
        //           END-EXEC.
        temporaryDataDao.fetchCurPackageSessio(ws);
        // COB_CODE: MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: IF IDSV0301-NOT-FOUND
            //              END-IF
            //           ELSE
            //                                TO IDSV0301-DESC-ERRORE-ESTESA
            //           END-IF
            if (idsv0301.getSqlcodeSigned().isNotFound()) {
                // COB_CODE: PERFORM E400-CLOSE-CURSOR-PACK THRU E400-EX
                e400CloseCursorPack();
                // COB_CODE: IF IDSV0301-ESITO-OK
                //              SET IDSV0301-NOT-FOUND      TO TRUE
                //           END-IF
                if (idsv0301.getEsito().isOk()) {
                    // COB_CODE: IF IDSV0301-READ
                    //                          TO IDSV0301-DESC-ERRORE-ESTESA
                    //           END-IF
                    if (idsv0301.getOperazione().isRead()) {
                        // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                        idsv0301.getEsito().setKo();
                        // COB_CODE: MOVE 'DATI NON TROVATI SU TAB. TEMPORANEA'
                        //                       TO IDSV0301-DESC-ERRORE-ESTESA
                        idsv0301.setDescErroreEstesa("DATI NON TROVATI SU TAB. TEMPORANEA");
                    }
                    // COB_CODE: SET IDSV0301-NOT-FOUND      TO TRUE
                    idsv0301.getSqlcodeSigned().setNotFound();
                }
            }
            else {
                // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                idsv0301.getEsito().setKo();
                // COB_CODE: MOVE 'ERRORE FETCH CURSOR'
                //                             TO IDSV0301-DESC-ERRORE-ESTESA
                idsv0301.setDescErroreEstesa("ERRORE FETCH CURSOR");
            }
        }
    }

    /**Original name: E303-FTCH-CUR-PACK-STATIC-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e303FtchCurPackStaticTab() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-PACKAGE-STATIC
        //             INTO
        //                :D08-ID-TEMPORARY-DATA
        //               ,:D08-ALIAS-STR-DATO
        //               ,:D08-NUM-FRAME
        //               ,:D08-COD-COMP-ANIA
        //               ,:D08-ID-SESSION
        //               ,:D08-FL-CONTIGUOUS-DATA
        //                :IND-D08-FL-CONTIGUOUS-DATA
        //               ,:D08-TOTAL-RECURRENCE
        //                :IND-D08-TOTAL-RECURRENCE
        //               ,:D08-PARTIAL-RECURRENCE
        //                :IND-D08-PARTIAL-RECURRENCE
        //               ,:D08-ACTUAL-RECURRENCE
        //                :IND-D08-ACTUAL-RECURRENCE
        //               ,:D08-DS-OPER-SQL
        //               ,:D08-DS-VER
        //               ,:D08-DS-TS-CPTZ
        //               ,:D08-DS-UTENTE
        //               ,:D08-DS-STATO-ELAB
        //               ,:D08-TYPE-RECORD
        //                :IND-D08-TYPE-RECORD
        //               ,:D08-BUFFER-DATA-VCHAR
        //                :IND-D08-BUFFER-DATA
        //           END-EXEC.
        temporaryDataDao.fetchCurPackageSessio(ws);
        // COB_CODE: MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //              END-IF
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: IF IDSV0301-NOT-FOUND
            //              END-IF
            //           ELSE
            //                                TO IDSV0301-DESC-ERRORE-ESTESA
            //           END-IF
            if (idsv0301.getSqlcodeSigned().isNotFound()) {
                // COB_CODE: PERFORM E400-CLOSE-CURSOR-PACK THRU E400-EX
                e400CloseCursorPack();
                // COB_CODE: IF IDSV0301-ESITO-OK
                //              SET IDSV0301-NOT-FOUND      TO TRUE
                //           END-IF
                if (idsv0301.getEsito().isOk()) {
                    // COB_CODE: IF IDSV0301-READ
                    //                          TO IDSV0301-DESC-ERRORE-ESTESA
                    //           END-IF
                    if (idsv0301.getOperazione().isRead()) {
                        // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                        idsv0301.getEsito().setKo();
                        // COB_CODE: MOVE 'DATI NON TROVATI SU TAB. TEMPORANEA'
                        //                       TO IDSV0301-DESC-ERRORE-ESTESA
                        idsv0301.setDescErroreEstesa("DATI NON TROVATI SU TAB. TEMPORANEA");
                    }
                    // COB_CODE: SET IDSV0301-NOT-FOUND      TO TRUE
                    idsv0301.getSqlcodeSigned().setNotFound();
                }
            }
            else {
                // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                idsv0301.getEsito().setKo();
                // COB_CODE: MOVE 'ERRORE FETCH CURSOR'
                //                             TO IDSV0301-DESC-ERRORE-ESTESA
                idsv0301.setDescErroreEstesa("ERRORE FETCH CURSOR");
            }
        }
    }

    /**Original name: E400-CLOSE-CURSOR-PACK<br>
	 * <pre>****************************************************************</pre>*/
    private void e400CloseCursorPack() {
        // COB_CODE: IF IDSV0301-SESSION-TEMP-TABLE
        //              PERFORM E401-CLS-CUR-PACK-SESSION-TAB THRU E401-EX
        //           ELSE
        //              PERFORM E403-CLS-CUR-PACK-STATIC-TAB  THRU E403-EX
        //           END-IF.
        if (idsv0301.getTemporaryTable().isSessionTempTable()) {
            // COB_CODE: PERFORM E401-CLS-CUR-PACK-SESSION-TAB THRU E401-EX
            e401ClsCurPackSessionTab();
        }
        else {
            // COB_CODE: PERFORM E403-CLS-CUR-PACK-STATIC-TAB  THRU E403-EX
            e403ClsCurPackStaticTab();
        }
    }

    /**Original name: E401-CLS-CUR-PACK-SESSION-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e401ClsCurPackSessionTab() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-PACKAGE-SESSIO
        //           END-EXEC.
        temporaryDataDao.closeCurPackageSessio();
        // COB_CODE: MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                 TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'ERRORE CLOSE CURSOR SESSION TEMPORARY TABLE'
            //                              TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE CLOSE CURSOR SESSION TEMPORARY TABLE");
        }
    }

    /**Original name: E403-CLS-CUR-PACK-STATIC-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void e403ClsCurPackStaticTab() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-PACKAGE-STATIC
        //           END-EXEC.
        temporaryDataDao.closeCurPackageSessio();
        // COB_CODE: MOVE SQLCODE            TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                 TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'ERRORE CLOSE CURSOR STATIC TEMPORARY TABLE'
            //                              TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE CLOSE CURSOR STATIC TEMPORARY TABLE");
        }
    }

    /**Original name: E600-VALORIZZA-OUT<br>
	 * <pre>****************************************************************</pre>*/
    private void e600ValorizzaOut() {
        // COB_CODE: SET IDSV0301-READ-NEXT      TO TRUE.
        idsv0301.getOperazione().setReadNext();
        // COB_CODE: MOVE D08-ID-SESSION         TO IDSV0301-ID-SESSION
        idsv0301.setIdSession(ws.getTemporaryData().getIdSession());
        // COB_CODE: MOVE D08-FL-CONTIGUOUS-DATA TO IDSV0301-FL-CONTIGUOUS
        idsv0301.getFlContiguous().setFlContiguous(ws.getTemporaryData().getFlContiguousData());
        // COB_CODE: MOVE D08-TOTAL-RECURRENCE   TO IDSV0301-TOTAL-RECURRENCE
        idsv0301.setTotalRecurrence(ws.getTemporaryData().getTotalRecurrence());
        // COB_CODE: MOVE D08-PARTIAL-RECURRENCE TO IDSV0301-PARTIAL-RECURRENCE
        idsv0301.setPartialRecurrence(ws.getTemporaryData().getPartialRecurrence());
        // COB_CODE: MOVE D08-ACTUAL-RECURRENCE  TO IDSV0301-ACTUAL-RECURRENCE
        idsv0301.setActualRecurrence(ws.getTemporaryData().getActualRecurrence());
        // COB_CODE: IF IDSV0301-CONTIGUOUS-NO
        //              MOVE D08-BUFFER-DATA     TO IDSV0302-BUFFER-DATA
        //           ELSE
        //              ADD  D08-BUFFER-DATA-LEN TO WK-START-POSITION
        //           END-IF.
        if (idsv0301.getFlContiguous().isNo()) {
            // COB_CODE: MOVE D08-BUFFER-DATA     TO IDSV0302-BUFFER-DATA
            idsv0302.setBufferDataFormatted(ws.getTemporaryData().getBufferDataFormatted());
        }
        else {
            // COB_CODE: MOVE D08-BUFFER-DATA (1 : D08-BUFFER-DATA-LEN)
            //                                    TO IDSV0302-BUFFER-DATA
            //                (WK-START-POSITION : D08-BUFFER-DATA-LEN)
            idsv0302.setBufferDataSubstring(ws.getTemporaryData().getBufferDataFormatted().substring((1) - 1, ws.getTemporaryData().getBufferDataLen()), ws.getWkStartPosition(), ws.getTemporaryData().getBufferDataLen());
            // COB_CODE: ADD  D08-BUFFER-DATA-LEN TO WK-START-POSITION
            ws.setWkStartPosition(Trunc.toInt(abs(ws.getTemporaryData().getBufferDataLen() + ws.getWkStartPosition()), 9));
        }
    }

    /**Original name: A210-SELECT<br>
	 * <pre>****************************************************************</pre>*/
    private void a210Select() {
        // COB_CODE: IF IDSV0301-SESSION-TEMP-TABLE
        //              PERFORM A211-SELECT-SESSION-TAB THRU A211-EX
        //           ELSE
        //              PERFORM A213-SELECT-STATIC-TAB  THRU A213-EX
        //           END-IF.
        if (idsv0301.getTemporaryTable().isSessionTempTable()) {
            // COB_CODE: PERFORM A211-SELECT-SESSION-TAB THRU A211-EX
            a211SelectSessionTab();
        }
        else {
            // COB_CODE: PERFORM A213-SELECT-STATIC-TAB  THRU A213-EX
            a213SelectStaticTab();
        }
    }

    /**Original name: A211-SELECT-SESSION-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void a211SelectSessionTab() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 VALUE (MAX(NUM_FRAME), 0)
        //             INTO
        //                :D08-NUM-FRAME
        //             FROM SESSION.TEMPORARY_DATA
        //             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
        //                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
        //                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
        //           END-EXEC.
        ws.getTemporaryData().setNumFrame(temporaryDataDao.selectRec(idsv0301.getCodCompAnia(), idsv0301.getIdTemporaryData(), idsv0301.getAliasStrDato(), ws.getTemporaryData().getNumFrame()));
        // COB_CODE: MOVE SQLCODE             TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0301-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //                                    TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: IF D08-NUM-FRAME > ZEROES
            //              SET TEMPORARY-DATA-TROVATI-SI TO TRUE
            //           END-IF
            if (ws.getTemporaryData().getNumFrame() > 0) {
                // COB_CODE: SET TEMPORARY-DATA-TROVATI-SI TO TRUE
                ws.getTemporaryDataTrovati().setSi();
            }
        }
        else {
            // COB_CODE: SET IDSV0301-ESITO-KO            TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'ERRORE SELECT SESSION TEMPORARY TABLE'
            //                                 TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE SELECT SESSION TEMPORARY TABLE");
        }
    }

    /**Original name: A213-SELECT-STATIC-TAB<br>
	 * <pre>****************************************************************</pre>*/
    private void a213SelectStaticTab() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 VALUE (MAX(NUM_FRAME), 0)
        //             INTO
        //                :D08-NUM-FRAME
        //             FROM TEMPORARY_DATA
        //             WHERE     COD_COMP_ANIA     = :IDSV0301-COD-COMP-ANIA
        //                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
        //                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
        //           END-EXEC.
        ws.getTemporaryData().setNumFrame(temporaryDataDao.selectRec(idsv0301.getCodCompAnia(), idsv0301.getIdTemporaryData(), idsv0301.getAliasStrDato(), ws.getTemporaryData().getNumFrame()));
        // COB_CODE: MOVE SQLCODE       TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0301-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //                                    TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: IF D08-NUM-FRAME > ZEROES
            //              SET TEMPORARY-DATA-TROVATI-SI TO TRUE
            //           END-IF
            if (ws.getTemporaryData().getNumFrame() > 0) {
                // COB_CODE: SET TEMPORARY-DATA-TROVATI-SI TO TRUE
                ws.getTemporaryDataTrovati().setSi();
            }
        }
        else {
            // COB_CODE: SET IDSV0301-ESITO-KO            TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE 'ERRORE SELECT STATIC TEMPORARY TABLE'
            //                                 TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE SELECT STATIC TEMPORARY TABLE");
        }
    }

    /**Original name: A220-INSERT<br>
	 * <pre>****************************************************************</pre>*/
    private void a220Insert() {
        // COB_CODE: IF IDSV0301-SESSION-TEMP-TABLE
        //              PERFORM A221-INSERT-SESSION-TABLE      THRU A221-EX
        //           ELSE
        //              PERFORM A223-INSERT-STATIC-TABLE       THRU A223-EX
        //           END-IF.
        if (idsv0301.getTemporaryTable().isSessionTempTable()) {
            // COB_CODE: PERFORM A221-INSERT-SESSION-TABLE      THRU A221-EX
            a221InsertSessionTable();
        }
        else {
            // COB_CODE: PERFORM A223-INSERT-STATIC-TABLE       THRU A223-EX
            a223InsertStaticTable();
        }
    }

    /**Original name: A221-INSERT-SESSION-TABLE<br>
	 * <pre>****************************************************************</pre>*/
    private void a221InsertSessionTable() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL THRU Z200-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSS0300.cbl:line=765, because the code is unreachable.
        // COB_CODE: EXEC SQL
        //              INSERT
        //              INTO SESSION.TEMPORARY_DATA
        //                  (
        //                ID_TEMPORARY_DATA
        //                ,ALIAS_STR_DATO
        //                ,NUM_FRAME
        //                ,COD_COMP_ANIA
        //                ,ID_SESSION
        //                ,FL_CONTIGUOUS_DATA
        //                ,TOTAL_RECURRENCE
        //                ,PARTIAL_RECURRENCE
        //                ,ACTUAL_RECURRENCE
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TYPE_RECORD
        //                ,BUFFER_DATA
        //                  )
        //              VALUES
        //                  (
        //                :D08-ID-TEMPORARY-DATA
        //               ,:D08-ALIAS-STR-DATO
        //               ,:D08-NUM-FRAME
        //               ,:D08-COD-COMP-ANIA
        //               ,:D08-ID-SESSION
        //               ,:D08-FL-CONTIGUOUS-DATA
        //                :IND-D08-FL-CONTIGUOUS-DATA
        //               ,:D08-TOTAL-RECURRENCE
        //                :IND-D08-TOTAL-RECURRENCE
        //               ,:D08-PARTIAL-RECURRENCE
        //                :IND-D08-PARTIAL-RECURRENCE
        //               ,:D08-ACTUAL-RECURRENCE
        //                :IND-D08-ACTUAL-RECURRENCE
        //               ,:D08-DS-OPER-SQL
        //               ,:D08-DS-VER
        //               ,:D08-DS-TS-CPTZ
        //               ,:D08-DS-UTENTE
        //               ,:D08-DS-STATO-ELAB
        //               ,:D08-TYPE-RECORD
        //                :IND-D08-TYPE-RECORD
        //               ,:D08-BUFFER-DATA-VCHAR
        //                :IND-D08-BUFFER-DATA
        //                  )
        //           END-EXEC
        temporaryDataDao.insertRec(ws);
        // COB_CODE: MOVE SQLCODE  TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                      TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE SQLCODE          TO IDSV0301-SQLCODE
            idsv0301.setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE 'ERRORE INSERT TEMPORARY_DATA'
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE INSERT TEMPORARY_DATA");
        }
    }

    /**Original name: A223-INSERT-STATIC-TABLE<br>
	 * <pre>****************************************************************</pre>*/
    private void a223InsertStaticTable() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSS0300.cbl:line=831, because the code is unreachable.
        // COB_CODE: EXEC SQL
        //              INSERT
        //              INTO TEMPORARY_DATA
        //                  (
        //                ID_TEMPORARY_DATA
        //                ,ALIAS_STR_DATO
        //                ,NUM_FRAME
        //                ,COD_COMP_ANIA
        //                ,ID_SESSION
        //                ,FL_CONTIGUOUS_DATA
        //                ,TOTAL_RECURRENCE
        //                ,PARTIAL_RECURRENCE
        //                ,ACTUAL_RECURRENCE
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,TYPE_RECORD
        //                ,BUFFER_DATA
        //                  )
        //              VALUES
        //                  (
        //                :D08-ID-TEMPORARY-DATA
        //               ,:D08-ALIAS-STR-DATO
        //               ,:D08-NUM-FRAME
        //               ,:D08-COD-COMP-ANIA
        //               ,:D08-ID-SESSION
        //               ,:D08-FL-CONTIGUOUS-DATA
        //                :IND-D08-FL-CONTIGUOUS-DATA
        //               ,:D08-TOTAL-RECURRENCE
        //                :IND-D08-TOTAL-RECURRENCE
        //               ,:D08-PARTIAL-RECURRENCE
        //                :IND-D08-PARTIAL-RECURRENCE
        //               ,:D08-ACTUAL-RECURRENCE
        //                :IND-D08-ACTUAL-RECURRENCE
        //               ,:D08-DS-OPER-SQL
        //               ,:D08-DS-VER
        //               ,:D08-DS-TS-CPTZ
        //               ,:D08-DS-UTENTE
        //               ,:D08-DS-STATO-ELAB
        //               ,:D08-TYPE-RECORD
        //                :IND-D08-TYPE-RECORD
        //               ,:D08-BUFFER-DATA-VCHAR
        //                :IND-D08-BUFFER-DATA
        //                  )
        //           END-EXEC
        temporaryDataDao.insertRec(ws);
        // COB_CODE: MOVE SQLCODE  TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                      TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE SQLCODE          TO IDSV0301-SQLCODE
            idsv0301.setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE 'ERRORE INSERT STATIC TEMPORARY_DATA'
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE INSERT STATIC TEMPORARY_DATA");
        }
    }

    /**Original name: A240-DELETE<br>
	 * <pre>****************************************************************</pre>*/
    private void a240Delete() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSS0300.cbl:line=897, because the code is unreachable.
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM TEMPORARY_DATA
        //                WHERE COD_COMP_ANIA      = :IDSV0301-COD-COMP-ANIA
        //                   AND ID_TEMPORARY_DATA = :IDSV0301-ID-TEMPORARY-DATA
        //                   AND ALIAS_STR_DATO    = :IDSV0301-ALIAS-STR-DATO
        //           END-EXEC.
        temporaryDataDao.deleteRec(idsv0301.getCodCompAnia(), idsv0301.getIdTemporaryData(), idsv0301.getAliasStrDato());
        // COB_CODE: MOVE SQLCODE               TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                      TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO   TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE SQLCODE            TO IDSV0301-SQLCODE
            idsv0301.setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE 'ERRORE DELETE TEMPORARY_DATA'
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE DELETE TEMPORARY_DATA");
        }
    }

    /**Original name: D000-CREA-SESSION-TABLE<br>
	 * <pre>****************************************************************</pre>*/
    private void d000CreaSessionTable() {
        // COB_CODE: EXEC SQL DECLARE GLOBAL TEMPORARY
        //                TABLE SESSION.TEMPORARY_DATA
        //                  (
        //                   ID_TEMPORARY_DATA   DECIMAL(10, 0) NOT NULL,
        //                   ALIAS_STR_DATO      CHAR(30) NOT NULL,
        //                   NUM_FRAME           DECIMAL(5, 0) NOT NULL,
        //                   COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
        //                   ID_SESSION          CHAR(20) NOT NULL,
        //                   FL_CONTIGUOUS_DATA  CHAR(1),
        //                   TOTAL_RECURRENCE    DECIMAL(5, 0),
        //                   PARTIAL_RECURRENCE  DECIMAL(5, 0),
        //                   ACTUAL_RECURRENCE   DECIMAL(5, 0),
        //                   DS_OPER_SQL         CHAR(1) NOT NULL,
        //                   DS_VER              DECIMAL(9, 0) NOT NULL,
        //                   DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
        //                   DS_UTENTE           CHAR(20) NOT NULL,
        //                   DS_STATO_ELAB       CHAR(1) NOT NULL,
        //                   TYPE_RECORD         CHAR(3),
        //                   BUFFER_DATA         VARCHAR(32000)
        //                  )
        //           END-EXEC.
        //SQL statement db2sql:DeclareGlobalTempTable doesn't need a translation
        // COB_CODE: MOVE SQLCODE       TO IDSV0301-SQLCODE-SIGNED
        idsv0301.getSqlcodeSigned().setSqlcodeSigned(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0301-SUCCESSFUL-SQL
        //                                      TO IDSV0301-DESC-ERRORE-ESTESA
        //           END-IF.
        if (!idsv0301.getSqlcodeSigned().isSuccessfulSql()) {
            // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
            idsv0301.getEsito().setKo();
            // COB_CODE: MOVE SQLCODE          TO IDSV0301-SQLCODE
            idsv0301.setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE 'ERRORE CREAZIONE SESSION TABLE'
            //                                   TO IDSV0301-DESC-ERRORE-ESTESA
            idsv0301.setDescErroreEstesa("ERRORE CREAZIONE SESSION TABLE");
        }
    }

    /**Original name: V100-VERIFICA-TEMP-TABLE<br>
	 * <pre>****************************************************************</pre>*/
    private void v100VerificaTempTable() {
        // COB_CODE: PERFORM A210-SELECT                    THRU A210-EX
        a210Select();
        // COB_CODE: IF IDSV0301-ESITO-OK
        //              END-IF
        //           END-IF.
        if (idsv0301.getEsito().isOk()) {
            // COB_CODE: IF TEMPORARY-DATA-TROVATI-SI
            //              END-IF
            //           END-IF
            if (ws.getTemporaryDataTrovati().isSi()) {
                // COB_CODE: IF IDSV0301-DELETE-ACTION
                //              SET TEMPORARY-DATA-TROVATI-NO TO TRUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (idsv0301.getActionType().isDeleteAction()) {
                    // COB_CODE: PERFORM A240-DELETE           THRU A240-EX
                    a240Delete();
                    // COB_CODE: SET IDSV0301-APPEND-ACTION    TO TRUE
                    idsv0301.getActionType().setAppendAction();
                    // COB_CODE: SET TEMPORARY-DATA-TROVATI-NO TO TRUE
                    ws.getTemporaryDataTrovati().setNo();
                }
                else if (idsv0301.getFlContiguous().isSi()) {
                    // COB_CODE: IF IDSV0301-CONTIGUOUS-SI
                    //                                 TO IDSV0301-DESC-ERRORE-ESTESA
                    //           ELSE
                    //              ADD 1              TO NUM-FRAME-APPEND
                    //           END-IF
                    // COB_CODE: SET IDSV0301-ESITO-KO TO TRUE
                    idsv0301.getEsito().setKo();
                    // COB_CODE: MOVE SQLCODE       TO IDSV0301-SQLCODE
                    idsv0301.setSqlcode(sqlca.getSqlcode());
                    // COB_CODE: MOVE 'DATI TEMPORANEI GIA'' PRESENTI X ALIAS'
                    //                              TO IDSV0301-DESC-ERRORE-ESTESA
                    idsv0301.setDescErroreEstesa("DATI TEMPORANEI GIA' PRESENTI X ALIAS");
                }
                else {
                    // COB_CODE: MOVE D08-NUM-FRAME TO NUM-FRAME-APPEND
                    ws.setNumFrameAppend(Trunc.toShort(ws.getTemporaryData().getNumFrame(), 4));
                    // COB_CODE: ADD 1              TO NUM-FRAME-APPEND
                    ws.setNumFrameAppend(Trunc.toShort(1 + ws.getNumFrameAppend(), 4));
                }
            }
        }
    }

    /**Original name: CALCOLA-PACKAGES<br>
	 * <pre>*************************************************************</pre>*/
    private void calcolaPackages() {
        // COB_CODE: PERFORM INIT-CAMPI-PACKAGE THRU INIT-CAMPI-PACKAGE-EX
        initCampiPackage();
        // COB_CODE: DIVIDE    WK-DATA-PACKAGE-MAX
        //           INTO      WK-LEN-TRANS-DATA
        //           GIVING    WK-TOT-NUM-FRAMES
        //           REMAINDER WK-REMAINDER-PACKAGE
        ws.setWkTotNumFrames(Trunc.toShort(ws.getWkLenTransData() / ws.getWkDataPackageMax(), 4));
        ws.setWkRemainderPackage(Trunc.toInt(ws.getWkLenTransData() % ws.getWkDataPackageMax(), 9));
        // COB_CODE: IF WK-REMAINDER-PACKAGE > ZEROES
        //              ADD 1               TO WK-TOT-NUM-FRAMES
        //           END-IF.
        if (ws.getWkRemainderPackage() > 0) {
            // COB_CODE: MOVE WK-REMAINDER-PACKAGE
            //                               TO WK-LEN-PACKAGES-NO-STD
            ws.setWkLenPackagesNoStd(TruncAbs.toLong(ws.getWkRemainderPackage(), 9));
            // COB_CODE: ADD 1               TO WK-TOT-NUM-FRAMES
            ws.setWkTotNumFrames(Trunc.toShort(1 + ws.getWkTotNumFrames(), 4));
        }
    }

    /**Original name: INIT-CAMPI-PACKAGE<br>
	 * <pre>*************************************************************</pre>*/
    private void initCampiPackage() {
        // COB_CODE: MOVE ZEROES TO  WK-LEN-PACKAGES-NO-STD
        //                           WK-TOT-NUM-FRAMES
        //                           WK-REMAINDER-PACKAGE.
        ws.setWkLenPackagesNoStd(0);
        ws.setWkTotNumFrames(((short)0));
        ws.setWkRemainderPackage(0);
    }

    public void initTemporaryData() {
        ws.getTemporaryData().setIdTemporaryData(0);
        ws.getTemporaryData().setAliasStrDato("");
        ws.getTemporaryData().setNumFrame(0);
        ws.getTemporaryData().setCodCompAnia(0);
        ws.getTemporaryData().setIdSession("");
        ws.getTemporaryData().setFlContiguousData(Types.SPACE_CHAR);
        ws.getTemporaryData().setTotalRecurrence(0);
        ws.getTemporaryData().setPartialRecurrence(0);
        ws.getTemporaryData().setActualRecurrence(0);
        ws.getTemporaryData().setDsOperSql(Types.SPACE_CHAR);
        ws.getTemporaryData().setDsVer(0);
        ws.getTemporaryData().setDsTsCptz(0);
        ws.getTemporaryData().setDsUtente("");
        ws.getTemporaryData().setDsStatoElab(Types.SPACE_CHAR);
        ws.getTemporaryData().setTypeRecord("");
        ws.getTemporaryData().setBufferDataLen(((short)0));
        ws.getTemporaryData().setBufferData("");
    }

    public void deleteArgListeners() {
        idsv0301.getBufferDataLen().deleteListener(idsv0302.getIdsv0302EleListener());
    }

    public void registerArgListeners() {
        idsv0301.getBufferDataLen().addListener(idsv0302.getIdsv0302EleListener());
        idsv0301.getBufferDataLen().notifyListeners();
    }
}
