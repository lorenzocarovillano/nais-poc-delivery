package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs2760Data;
import it.accenture.jnais.ws.WkVariabiliLvvs2760;

/**Original name: LVVS2760<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2010.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS2760
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... MODULO DI CALCOLO FLPOLUNIC
 * **------------------------------------------------------------***</pre>*/
public class Lvvs2760 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs2760Data ws = new Lvvs2760Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS2760
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS2760_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000.
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs2760 getInstance() {
        return ((Lvvs2760)Programs.getInstance(Lvvs2760.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-VARIABILI.
        initIxIndici();
        initTabOutput();
        initWkVariabili();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE 0 TO NUM-POLI.
        ws.getWkVariabili().setNumPoli(0);
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: PERFORM S1100-LETTURA-CONTRAENTE
        //              THRU S1100-LETTURA-CONTRAENTE-EX.
        s1100LetturaContraente();
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO DRAN-AREA-RAPP-ANA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DRAN-AREA-RAPP-ANA
            ws.setDranAreaRappAnaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1101-INIZIA-CONTRAENTE<br>
	 * <pre>----------------------------------------------------------------*
	 *     INIZIALIZZA LETTURA TABELLA CONTRAENTE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1101IniziaContraente() {
        // COB_CODE: INITIALIZE RAPP-ANA
        initRappAna();
        // COB_CODE: MOVE DRAN-COD-SOGG(1)
        //             TO RAN-COD-SOGG
        ws.getRappAna().setRanCodSogg(ws.getDranTabRappAnag(1).getLccvran1().getDati().getWranCodSogg());
        //--> TIPO CONTRAENTE
        // COB_CODE: MOVE 'CO'              TO RAN-TP-RAPP-ANA
        ws.getRappAna().setRanTpRappAna("CO");
        // COB_CODE: MOVE 'PO'              TO RAN-TP-OGG
        ws.getRappAna().setRanTpOgg("PO");
        // COB_CODE: MOVE RAPP-ANA          TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getRappAna().getRappAnaFormatted());
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
    }

    /**Original name: S1100-LETTURA-CONTRAENTE<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI TABELLA CONTRAENTE
	 * ----------------------------------------------------------------*
	 * --> TIPO OPERAZIONE</pre>*/
    private void s1100LetturaContraente() {
        Ldbs5910 ldbs5910 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET FINE-RAN-NO                TO TRUE.
        ws.getWkVariabili().getFlagFineRan().setNo();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: PERFORM UNTIL FINE-RAN-SI
        //              END-IF
        //           END-PERFORM.
        while (!ws.getWkVariabili().getFlagFineRan().isSi()) {
            // COB_CODE: PERFORM S1101-INIZIA-CONTRAENTE
            //              THRU S1101-INIZIA-CONTRAENTE-EX
            s1101IniziaContraente();
            // COB_CODE: CALL LDBS5910 USING IDSV0003 RAPP-ANA
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER TO TRUE
            //           END-CALL
            try {
                ldbs5910 = Ldbs5910.getInstance();
                ldbs5910.run(idsv0003, ws.getRappAna());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS5910
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs5910());
                // COB_CODE: MOVE 'ERRORE CALL LDBS5910 FETCH'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS5910 FETCH");
                // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               SET FINE-RAN-SI                TO TRUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *-->   SE AL SOGGETTO SONO ASSOCIATE PIU' POLIZZE IL FLAG E' PARI
                //           *-->   SU CUI E' ABILITATO IL CALCOLO DI IMPOSTA DI BOLLO
                //                               END-IF
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               SET FINE-RAN-SI                TO TRUE
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                        // COB_CODE: SET FINE-RAN-SI                TO TRUE
                        ws.getWkVariabili().getFlagFineRan().setSi();
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                        //-->   SE AL SOGGETTO SONO ASSOCIATE PIU' POLIZZE IL FLAG E' PARI AD 1
                        //-->   SU CUI E' ABILITATO IL CALCOLO DI IMPOSTA DI BOLLO
                        // COB_CODE: PERFORM LETTURA-POLIZZA
                        //              THRU LETTURA-POLIZZA-EX
                        letturaPolizza();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                        idsv0003.getOperazione().setFetchNext();
                        // COB_CODE: PERFORM S1101-INIZIA-CONTRAENTE
                        //              THRU S1101-INIZIA-CONTRAENTE-EX
                        s1101IniziaContraente();
                        // COB_CODE: MOVE '1'
                        //             TO IVVC0213-VAL-STR-O
                        ivvc0213.getTabOutput().setValStrO("1");
                        // COB_CODE: MOVE 1
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(1, 18, 7));
                        // COB_CODE: IF NUM-POLI > 1
                        //              SET FINE-RAN-SI     TO TRUE
                        //           END-IF
                        if (ws.getWkVariabili().getNumPoli() > 1) {
                            // COB_CODE: MOVE '0'
                            //             TO IVVC0213-VAL-STR-O
                            ivvc0213.getTabOutput().setValStrO("0");
                            // COB_CODE: MOVE 0
                            //             TO IVVC0213-VAL-IMP-O
                            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
                            // COB_CODE: PERFORM S1130-CHIUSURA-CURS
                            //              THRU S1130-CHIUSURA-CURS-EX
                            s1130ChiusuraCurs();
                            // COB_CODE: SET FINE-RAN-SI     TO TRUE
                            ws.getWkVariabili().getFlagFineRan().setSi();
                        }
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA RAPP-ANA ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA RAPP-ANA ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        // COB_CODE: SET FINE-RAN-SI                TO TRUE
                        ws.getWkVariabili().getFlagFineRan().setSi();
                        break;
                }
            }
        }
    }

    /**Original name: LETTURA-POLIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA DELLA POLIZZA ASSOCIATA AL CONTRAENTE
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaPolizza() {
        Idbspol0 idbspol0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        // COB_CODE: MOVE RAN-ID-OGG       TO POL-ID-POLI.
        ws.getPoli().setPolIdPoli(ws.getRappAna().getRanIdOgg());
        // COB_CODE: SET POLI-NO TO TRUE
        ws.getWkVariabili().getFlagPoli().setNo();
        // COB_CODE: MOVE 'IDBSPOL0'                 TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSPOL0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID                 TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        //
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: CALL WK-CALL-PGM USING IDSV0003 POLI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbspol0 = Idbspol0.getInstance();
            idbspol0.run(idsv0003, ws.getPoli());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSPOL0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF POL-ID-POLI = IVVC0213-ID-POLIZZA
            //              SET POLI-SI TO TRUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getPoli().getPolIdPoli() == ivvc0213.getIdPolizza()) {
                // COB_CODE: SET POLI-SI TO TRUE
                ws.getWkVariabili().getFlagPoli().setSi();
            }
            else if (Conditions.eq(ws.getPoli().getPolTpRgmFisc(), "01")) {
                // COB_CODE: IF POL-TP-RGM-FISC = '01'
                //              END-IF
                //           END-IF
                // COB_CODE: MOVE POL-TP-POLI
                //             TO ISPV0000-TP-POLI
                ws.getIspv0000().getTpPoli().setTpPoli(ws.getPoli().getPolTpPoli());
                // COB_CODE: IF ISPV0000-IND-FIP-PIP
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIspv0000().getTpPoli().isIspv0000IndFipPip()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: PERFORM LETTURA-STB
                    //              THRU LETTURA-STB-EX
                    letturaStb();
                    // COB_CODE: IF IN-VIGORE
                    //                 THRU S1110-LETTURA-GARANZIA-EX
                    //           END-IF
                    if (ws.getWsTpStatBus().isInVigore()) {
                        // COB_CODE: PERFORM S1110-LETTURA-GARANZIA
                        //              THRU S1110-LETTURA-GARANZIA-EX
                        s1110LetturaGaranzia();
                    }
                }
            }
            // COB_CODE: IF POLI-SI
            //              COMPUTE NUM-POLI = NUM-POLI + 1
            //           END-IF
            if (ws.getWkVariabili().getFlagPoli().isSi()) {
                // COB_CODE: COMPUTE NUM-POLI = NUM-POLI + 1
                ws.getWkVariabili().setNumPoli(Trunc.toInt(ws.getWkVariabili().getNumPoli() + 1, 9));
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA IDBSPOL0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSPOL0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: LETTURA-STB<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA STATO OGGETTO BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaStb() {
        Idbsstb0 idbsstb0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE STAT-OGG-BUS.
        initStatOggBus();
        // COB_CODE: SET POLIZZA                   TO TRUE
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE POL-ID-POLI              TO STB-ID-OGG.
        ws.getStatOggBus().setStbIdOgg(ws.getPoli().getPolIdPoli());
        // COB_CODE: MOVE WS-TP-OGG                TO STB-TP-OGG.
        ws.getStatOggBus().setStbTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: MOVE 'IDBSSTB0'               TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSSTB0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT              TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID-OGGETTO          TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdOggetto();
        //
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: CALL WK-CALL-PGM USING IDSV0003 STAT-OGG-BUS
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbsstb0 = Idbsstb0.getInstance();
            idbsstb0.run(idsv0003, ws.getStatOggBus());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-IDBSSTB0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSSTB0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-SQL
        //           *       MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-BUS
        //                   MOVE STB-TP-STAT-BUS      TO WS-TP-STAT-BUS
        //                ELSE
        //                   END-IF
        //                END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            //       MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-BUS
            // COB_CODE: MOVE STB-TP-STAT-BUS      TO WS-TP-STAT-BUS
            ws.getWsTpStatBus().setWsTpStatBus(ws.getStatOggBus().getStbTpStatBus());
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA IDBSSTB0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSSTB0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1110-LETTURA-GARANZIA<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI TABELLA GARANZIA
	 * ----------------------------------------------------------------*
	 * --> TIPO OPERAZIONE</pre>*/
    private void s1110LetturaGaranzia() {
        Ldbs1420 ldbs1420 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //                      OR IDSV0003-CLOSE-CURSOR
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql() || idsv0003.getOperazione().isCloseCursor())) {
            // COB_CODE: INITIALIZE GAR
            initGar();
            // COB_CODE: MOVE POL-ID-POLI
            //             TO LDBV1421-ID-POLI
            ws.getLdbv1421().setIdPoli(ws.getPoli().getPolIdPoli());
            // COB_CODE: MOVE 5
            //             TO LDBV1421-RAMO-BILA-1
            ws.getLdbv1421().setRamoBila1("5");
            // COB_CODE: MOVE 3
            //             TO LDBV1421-RAMO-BILA-2
            ws.getLdbv1421().setRamoBila2("3");
            // COB_CODE: MOVE LDBV1421     TO IDSV0003-BUFFER-WHERE-COND
            idsv0003.setBufferWhereCond(ws.getLdbv1421().getLdbv1421Formatted());
            // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: CALL LDBS1420 USING IDSV0003 GAR
            //           ON EXCEPTION
            //              SET IDSV0003-INVALID-OPER TO TRUE
            //           END-CALL
            try {
                ldbs1420 = Ldbs1420.getInstance();
                ldbs1420.run(idsv0003, ws.getGar());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS1420
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs1420());
                // COB_CODE: MOVE 'ERRORE CALL LDBS1420 FETCH'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS1420 FETCH");
                // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-NOT-FOUND
                //           *-->    NESSUN DATO IN TABELLA
                //                               CONTINUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                //           *-->   SE LA POLIZZA HA ALMENO UNA GARANZIA DI RAMO 5 O 3
                //           *-->   E' AMMESSO IL CALCOLO DEL BOLLO
                //                                  THRU S1135-CHIUSURA-CURS-GAR-EX
                //                          WHEN OTHER
                //           *--->   ERRORE DI ACCESSO AL DB
                //                               END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                    // COB_CODE: CONTINUE
                    //continue
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                        //-->   SE LA POLIZZA HA ALMENO UNA GARANZIA DI RAMO 5 O 3         AD 1
                        //-->   E' AMMESSO IL CALCOLO DEL BOLLO                            AD 1
                        // COB_CODE: SET POLI-SI TO TRUE
                        ws.getWkVariabili().getFlagPoli().setSi();
                        // COB_CODE: PERFORM S1135-CHIUSURA-CURS-GAR
                        //              THRU S1135-CHIUSURA-CURS-GAR-EX
                        s1135ChiusuraCursGar();
                        break;

                    default://--->   ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA GARANZIA ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA GARANZIA ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
        }
    }

    /**Original name: S1135-CHIUSURA-CURS-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIUSURA CURSORE GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1135ChiusuraCursGar() {
        Ldbs1420 ldbs1420 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS1420 USING IDSV0003 GAR
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs1420 = Ldbs1420.getInstance();
            ldbs1420.run(idsv0003, ws.getGar());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS1420
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs1420());
            // COB_CODE: MOVE 'ERRORE CALL LDBS1420 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS1420 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS1420'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS1420");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S1130-CHIUSURA-CURS<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIUSURA CURSORE BENEFICIARIO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1130ChiusuraCurs() {
        Ldbs5910 ldbs5910 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: CALL LDBS5910 USING IDSV0003 RAPP-ANA
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            ldbs5910 = Ldbs5910.getInstance();
            ldbs5910.run(idsv0003, ws.getRappAna());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS5910
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs5910());
            // COB_CODE: MOVE 'ERRORE CALL LDBS5910 CLOSE'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS5910 CLOSE");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ERRORE CLOSE CURSORE LDBS5910'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CLOSE CURSORE LDBS5910");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkVariabili() {
        ws.getWkVariabili().setNumPoliFormatted("000000000");
        ws.getWkVariabili().getFlagPoli().setFlagPoli(Types.SPACE_CHAR);
        ws.getWkVariabili().getFlagFineRan().setFlagFineRan(Types.SPACE_CHAR);
        ws.getWkVariabili().getFlagFineGar().setFlagFineGar(Types.SPACE_CHAR);
    }

    public void initRappAna() {
        ws.getRappAna().setRanIdRappAna(0);
        ws.getRappAna().getRanIdRappAnaCollg().setRanIdRappAnaCollg(0);
        ws.getRappAna().setRanIdOgg(0);
        ws.getRappAna().setRanTpOgg("");
        ws.getRappAna().setRanIdMoviCrz(0);
        ws.getRappAna().getRanIdMoviChiu().setRanIdMoviChiu(0);
        ws.getRappAna().setRanDtIniEff(0);
        ws.getRappAna().setRanDtEndEff(0);
        ws.getRappAna().setRanCodCompAnia(0);
        ws.getRappAna().setRanCodSogg("");
        ws.getRappAna().setRanTpRappAna("");
        ws.getRappAna().setRanTpPers(Types.SPACE_CHAR);
        ws.getRappAna().setRanSex(Types.SPACE_CHAR);
        ws.getRappAna().getRanDtNasc().setRanDtNasc(0);
        ws.getRappAna().setRanFlEstas(Types.SPACE_CHAR);
        ws.getRappAna().setRanIndir1("");
        ws.getRappAna().setRanIndir2("");
        ws.getRappAna().setRanIndir3("");
        ws.getRappAna().setRanTpUtlzIndir1("");
        ws.getRappAna().setRanTpUtlzIndir2("");
        ws.getRappAna().setRanTpUtlzIndir3("");
        ws.getRappAna().setRanEstrCntCorrAccr("");
        ws.getRappAna().setRanEstrCntCorrAdd("");
        ws.getRappAna().setRanEstrDocto("");
        ws.getRappAna().getRanPcNelRapp().setRanPcNelRapp(new AfDecimal(0, 6, 3));
        ws.getRappAna().setRanTpMezPagAdd("");
        ws.getRappAna().setRanTpMezPagAccr("");
        ws.getRappAna().setRanCodMatr("");
        ws.getRappAna().setRanTpAdegz("");
        ws.getRappAna().setRanFlTstRsh(Types.SPACE_CHAR);
        ws.getRappAna().setRanCodAz("");
        ws.getRappAna().setRanIndPrinc("");
        ws.getRappAna().getRanDtDeliberaCda().setRanDtDeliberaCda(0);
        ws.getRappAna().setRanDlgAlRisc(Types.SPACE_CHAR);
        ws.getRappAna().setRanLegaleRapprPrinc(Types.SPACE_CHAR);
        ws.getRappAna().setRanTpLegaleRappr("");
        ws.getRappAna().setRanTpIndPrinc("");
        ws.getRappAna().setRanTpStatRid("");
        ws.getRappAna().setRanNomeIntRidLen(((short)0));
        ws.getRappAna().setRanNomeIntRid("");
        ws.getRappAna().setRanCognIntRidLen(((short)0));
        ws.getRappAna().setRanCognIntRid("");
        ws.getRappAna().setRanCognIntTrattLen(((short)0));
        ws.getRappAna().setRanCognIntTratt("");
        ws.getRappAna().setRanNomeIntTrattLen(((short)0));
        ws.getRappAna().setRanNomeIntTratt("");
        ws.getRappAna().setRanCfIntRid("");
        ws.getRappAna().setRanFlCoincDipCntr(Types.SPACE_CHAR);
        ws.getRappAna().setRanFlCoincIntCntr(Types.SPACE_CHAR);
        ws.getRappAna().getRanDtDeces().setRanDtDeces(0);
        ws.getRappAna().setRanFlFumatore(Types.SPACE_CHAR);
        ws.getRappAna().setRanDsRiga(0);
        ws.getRappAna().setRanDsOperSql(Types.SPACE_CHAR);
        ws.getRappAna().setRanDsVer(0);
        ws.getRappAna().setRanDsTsIniCptz(0);
        ws.getRappAna().setRanDsTsEndCptz(0);
        ws.getRappAna().setRanDsUtente("");
        ws.getRappAna().setRanDsStatoElab(Types.SPACE_CHAR);
        ws.getRappAna().setRanFlLavDip(Types.SPACE_CHAR);
        ws.getRappAna().setRanTpVarzPagat("");
        ws.getRappAna().setRanCodRid("");
        ws.getRappAna().setRanTpCausRid("");
        ws.getRappAna().setRanIndMassaCorp("");
        ws.getRappAna().setRanCatRshProf("");
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initStatOggBus() {
        ws.getStatOggBus().setStbIdStatOggBus(0);
        ws.getStatOggBus().setStbIdOgg(0);
        ws.getStatOggBus().setStbTpOgg("");
        ws.getStatOggBus().setStbIdMoviCrz(0);
        ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(0);
        ws.getStatOggBus().setStbDtIniEff(0);
        ws.getStatOggBus().setStbDtEndEff(0);
        ws.getStatOggBus().setStbCodCompAnia(0);
        ws.getStatOggBus().setStbTpStatBus("");
        ws.getStatOggBus().setStbTpCaus("");
        ws.getStatOggBus().setStbDsRiga(0);
        ws.getStatOggBus().setStbDsOperSql(Types.SPACE_CHAR);
        ws.getStatOggBus().setStbDsVer(0);
        ws.getStatOggBus().setStbDsTsIniCptz(0);
        ws.getStatOggBus().setStbDsTsEndCptz(0);
        ws.getStatOggBus().setStbDsUtente("");
        ws.getStatOggBus().setStbDsStatoElab(Types.SPACE_CHAR);
    }

    public void initGar() {
        ws.getGar().setGrzIdGar(0);
        ws.getGar().getGrzIdAdes().setGrzIdAdes(0);
        ws.getGar().setGrzIdPoli(0);
        ws.getGar().setGrzIdMoviCrz(0);
        ws.getGar().getGrzIdMoviChiu().setGrzIdMoviChiu(0);
        ws.getGar().setGrzDtIniEff(0);
        ws.getGar().setGrzDtEndEff(0);
        ws.getGar().setGrzCodCompAnia(0);
        ws.getGar().setGrzIbOgg("");
        ws.getGar().getGrzDtDecor().setGrzDtDecor(0);
        ws.getGar().getGrzDtScad().setGrzDtScad(0);
        ws.getGar().setGrzCodSez("");
        ws.getGar().setGrzCodTari("");
        ws.getGar().setGrzRamoBila("");
        ws.getGar().getGrzDtIniValTar().setGrzDtIniValTar(0);
        ws.getGar().getGrzId1oAssto().setGrzId1oAssto(0);
        ws.getGar().getGrzId2oAssto().setGrzId2oAssto(0);
        ws.getGar().getGrzId3oAssto().setGrzId3oAssto(0);
        ws.getGar().getGrzTpGar().setGrzTpGar(((short)0));
        ws.getGar().setGrzTpRsh("");
        ws.getGar().getGrzTpInvst().setGrzTpInvst(((short)0));
        ws.getGar().setGrzModPagGarcol("");
        ws.getGar().setGrzTpPerPre("");
        ws.getGar().getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(((short)0));
        ws.getGar().getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(((short)0));
        ws.getGar().getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(((short)0));
        ws.getGar().getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(((short)0));
        ws.getGar().getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(((short)0));
        ws.getGar().getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(((short)0));
        ws.getGar().setGrzTpEmisPur(Types.SPACE_CHAR);
        ws.getGar().getGrzEtaAScad().setGrzEtaAScad(0);
        ws.getGar().setGrzTpCalcPrePrstz("");
        ws.getGar().setGrzTpPre(Types.SPACE_CHAR);
        ws.getGar().setGrzTpDur("");
        ws.getGar().getGrzDurAa().setGrzDurAa(0);
        ws.getGar().getGrzDurMm().setGrzDurMm(0);
        ws.getGar().getGrzDurGg().setGrzDurGg(0);
        ws.getGar().getGrzNumAaPagPre().setGrzNumAaPagPre(0);
        ws.getGar().getGrzAaPagPreUni().setGrzAaPagPreUni(0);
        ws.getGar().getGrzMmPagPreUni().setGrzMmPagPreUni(0);
        ws.getGar().getGrzFrazIniErogRen().setGrzFrazIniErogRen(0);
        ws.getGar().getGrzMm1oRat().setGrzMm1oRat(((short)0));
        ws.getGar().getGrzPc1oRat().setGrzPc1oRat(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPrstzAssta("");
        ws.getGar().getGrzDtEndCarz().setGrzDtEndCarz(0);
        ws.getGar().getGrzPcRipPre().setGrzPcRipPre(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzCodFnd("");
        ws.getGar().setGrzAaRenCer("");
        ws.getGar().getGrzPcRevrsb().setGrzPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpPcRip("");
        ws.getGar().getGrzPcOpz().setGrzPcOpz(new AfDecimal(0, 6, 3));
        ws.getGar().setGrzTpIas("");
        ws.getGar().setGrzTpStab("");
        ws.getGar().setGrzTpAdegPre(Types.SPACE_CHAR);
        ws.getGar().getGrzDtVarzTpIas().setGrzDtVarzTpIas(0);
        ws.getGar().getGrzFrazDecrCpt().setGrzFrazDecrCpt(0);
        ws.getGar().setGrzCodTratRiass("");
        ws.getGar().setGrzTpDtEmisRiass("");
        ws.getGar().setGrzTpCessRiass("");
        ws.getGar().setGrzDsRiga(0);
        ws.getGar().setGrzDsOperSql(Types.SPACE_CHAR);
        ws.getGar().setGrzDsVer(0);
        ws.getGar().setGrzDsTsIniCptz(0);
        ws.getGar().setGrzDsTsEndCptz(0);
        ws.getGar().setGrzDsUtente("");
        ws.getGar().setGrzDsStatoElab(Types.SPACE_CHAR);
        ws.getGar().getGrzAaStab().setGrzAaStab(0);
        ws.getGar().getGrzTsStabLimitata().setGrzTsStabLimitata(new AfDecimal(0, 14, 9));
        ws.getGar().getGrzDtPresc().setGrzDtPresc(0);
        ws.getGar().setGrzRshInvst(Types.SPACE_CHAR);
        ws.getGar().setGrzTpRamoBila("");
    }
}
