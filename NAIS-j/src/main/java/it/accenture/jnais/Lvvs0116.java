package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0116Data;

/**Original name: LVVS0116<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0116 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0116Data ws = new Lvvs0116Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0116_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0116 getInstance() {
        return ((Lvvs0116)Programs.getInstance(Lvvs0116.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE VAL-AST
        //                      AREA-IO-TGA
        //                      AREA-IO-GAR
        //                      AREA-IO-L19.
        initValAst();
        initAreaIoTga();
        initAreaIoGar();
        initAreaIoL19();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
            //
            // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
            //           AND IDSV0003-SUCCESSFUL-SQL
            //               END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: IF DGRZ-TP-INVST(IX-GUIDA-GRZ) = 7 OR 8
                //              END-IF
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getDgrzTabGar(ws.getIxIndici().getGuidaGrz()).getLccvgrz1().getDati().getWgrzTpInvst().getWgrzTpInvst() == 7 || ws.getDgrzTabGar(ws.getIxIndici().getGuidaGrz()).getLccvgrz1().getDati().getWgrzTpInvst().getWgrzTpInvst() == 8) {
                    // COB_CODE: IF DL19-ELE-FND-MAX EQUAL ZERO
                    //              END-IF
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (ws.getDl19EleFndMax() == 0) {
                        // COB_CODE: IF DTGA-PRSTZ-ULT(IVVC0213-IX-TABB) IS NUMERIC
                        //                TO IVVC0213-VAL-IMP-O
                        //           ELSE
                        //                TO IVVC0213-VAL-IMP-O
                        //           END-IF
                        if (Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt())) {
                            // COB_CODE: MOVE DTGA-PRSTZ-ULT(IVVC0213-IX-TABB)
                            //             TO IVVC0213-VAL-IMP-O
                            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt(), 18, 7));
                        }
                        else {
                            // COB_CODE: MOVE ZEROES
                            //             TO IVVC0213-VAL-IMP-O
                            ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
                        }
                    }
                    else {
                        // COB_CODE: IF LIQUI-RISPAR-POLIND
                        //           OR LIQUI-RPP-REDDITO-PROGR
                        //           OR LIQUI-RISPAR-ADE
                        //           OR LIQUI-RPP-TAKE-PROFIT
                        //                 THRU S1230-EX
                        //           ELSE
                        //                TO WK-ID-MOVI-FINRIO
                        //           END-IF
                        if (ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppTakeProfit()) {
                            // COB_CODE: PERFORM S1230-RECUP-ID-COMUN
                            //              THRU S1230-EX
                            s1230RecupIdComun();
                        }
                        else {
                            // COB_CODE: MOVE ZEROES
                            //             TO WK-ID-MOVI-FINRIO
                            ws.setWkIdMoviFinrio(0);
                        }
                        // COB_CODE: PERFORM S1250-CALCOLA-QUOTE     THRU S1250-EX
                        s1250CalcolaQuote();
                        //                  PERFORM S1260-SOTTRAI-ANNULLO   THRU S1260-EX
                        // COB_CODE: IF IVVC0213-VAL-IMP-O < ZEROES
                        //                      IVVC0213-VAL-IMP-O * (-1)
                        //           END-IF
                        if (ivvc0213.getTabOutput().getValImpO().compareTo(0) < 0) {
                            // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O =
                            //                   IVVC0213-VAL-IMP-O * (-1)
                            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ivvc0213.getTabOutput().getValImpO().multiply(-1), 18, 7));
                        }
                    }
                }
                else if (Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt())) {
                    // COB_CODE: IF DTGA-PRSTZ-ULT(IVVC0213-IX-TABB) IS NUMERIC
                    //                TO IVVC0213-VAL-IMP-O
                    //           ELSE
                    //                TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    // COB_CODE: MOVE DTGA-PRSTZ-ULT(IVVC0213-IX-TABB)
                    //             TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaPrstzUlt().getWtgaPrstzUlt(), 18, 7));
                }
                else {
                    // COB_CODE: MOVE ZEROES
                    //             TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
                }
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GRA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GRA
            ws.setDgrzAreaGraFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        //
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-QOTAZ-FON
        //                TO DL19-AREA-QUOTA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasQotazFon())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DL19-AREA-QUOTA
            ws.setDl19AreaQuotaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: SET WK-ID-COD-NON-TROVATO              TO TRUE.
        ws.getWkIdCodLivFlag().setNonTrovato();
        //
        // COB_CODE:      IF DGRZ-ELE-GAR-MAX GREATER ZERO
        //           *
        //                   END-IF
        //           *
        //                ELSE
        //                     TO IDSV0003-DESCRIZ-ERR-DB2
        //                END-IF.
        if (ws.getDgrzEleGarMax() > 0) {
            //
            // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
            //             UNTIL IX-TAB-GRZ > DGRZ-ELE-GAR-MAX
            //                OR WK-ID-COD-TROVATO
            //                END-IF
            //           END-PERFORM
            ws.getIxIndici().setTabGrz(((short)1));
            while (!(ws.getIxIndici().getTabGrz() > ws.getDgrzEleGarMax() || ws.getWkIdCodLivFlag().isTrovato())) {
                // COB_CODE: IF DTGA-ID-GAR(IVVC0213-IX-TABB) =
                //              DGRZ-ID-GAR(IX-TAB-GRZ)
                //              MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                //           END-IF
                if (ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdGar() == ws.getDgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    // COB_CODE: SET WK-ID-COD-TROVATO     TO TRUE
                    ws.getWkIdCodLivFlag().setTrovato();
                    // COB_CODE: MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                    ws.getIxIndici().setGuidaGrz(ws.getIxIndici().getTabGrz());
                }
                ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
            }
            //
            // COB_CODE: IF WK-ID-COD-NON-TROVATO
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getWkIdCodLivFlag().isNonTrovato()) {
                // COB_CODE: SET  IDSV0003-INVALID-OPER               TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'GARANZIA NON VALORIZZATA PER CALCOLO QUOTE'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("GARANZIA NON VALORIZZATA PER CALCOLO QUOTE");
            }
            //
        }
        else {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'GARANZIA NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("GARANZIA NON VALORIZZATA");
        }
    }

    /**Original name: S1230-RECUP-ID-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *   RECUPERO L'ID DEL MOVIMENTO DI COMUNICAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1230RecupIdComun() {
        Ldbs1530 ldbs1530 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                      MOVI.
        initMovi();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: IF LIQUI-RISPAR-POLIND
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRisparPolind()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET COMUN-RISPAR-IND         TO TRUE
            ws.getWsMovimento().setComunRisparInd();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: IF LIQUI-RPP-REDDITO-PROGR
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRppRedditoProgr()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET RPP-REDDITO-PROGRAMMATO  TO TRUE
            ws.getWsMovimento().setRppRedditoProgrammato();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: IF LIQUI-RISPAR-ADE
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRisparAde()) {
            // COB_CODE: MOVE IVVC0213-ID-ADESIONE    TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdAdesione());
            // COB_CODE: MOVE 'AD'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("AD");
            // COB_CODE: SET COMUN-RISPAR-ADE         TO TRUE
            ws.getWsMovimento().setComunRisparAde();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: IF LIQUI-RPP-TAKE-PROFIT
        //              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
        //           END-IF.
        if (ws.getWsMovimento().isLiquiRppTakeProfit()) {
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                    TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET RPP-TAKE-PROFIT          TO TRUE
            ws.getWsMovimento().setRppTakeProfit();
            // COB_CODE: MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(ws.getWsMovimento().getWsMovimento());
        }
        // COB_CODE: MOVE MOVI                       TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getMovi().getMoviFormatted());
        // COB_CODE: CALL LDBS1530  USING  IDSV0003 MOVI
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs1530 = Ldbs1530.getInstance();
            ldbs1530.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS1530
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs1530());
            // COB_CODE: MOVE 'ERRORE CALL LDBS1530'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS1530");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN IDSV0003-SUCCESSFUL-SQL
        //           *-->       OPERAZIONE ESEGUITA CON SUCCESSO
        //                         END-IF
        //                    WHEN IDSV0003-NOT-FOUND
        //           *-->     CHIAVE NON TROVATA
        //                           TO WK-ID-MOVI-FINRIO
        //                    WHEN OTHER
        //           *-->     ERRORE DB
        //                       END-STRING
        //                END-EVALUATE.
        switch (idsv0003.getSqlcode().getSqlcode()) {

            case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CON SUCCESSO
                // COB_CODE: PERFORM RECUP-STAT-MOVI-COMUN
                //              THRU RECUP-STAT-MOVI-COMUN-EX
                recupStatMoviComun();
                // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
                //           AND IDSV0003-SUCCESSFUL-SQL
                //                  THRU S1235-EX
                //           END-IF
                if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: PERFORM S1235-RECUP-MOVI-FINRIO
                    //              THRU S1235-EX
                    s1235RecupMoviFinrio();
                }
                break;

            case Idsv0003Sqlcode.NOT_FOUND://-->     CHIAVE NON TROVATA
                // COB_CODE: MOVE ZEROES
                //             TO WK-ID-MOVI-FINRIO
                ws.setWkIdMoviFinrio(0);
                break;

            default://-->     ERRORE DB
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                //                 IDSV0003-RETURN-CODE ';'
                //                 IDSV0003-SQLCODE
                //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LDBS1530 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                break;
        }
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
    }

    /**Original name: RECUP-STAT-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Valorizzazione dell'area di pagina
	 * ----------------------------------------------------------------*</pre>*/
    private void recupStatMoviComun() {
        Idbsstw0 idbsstw0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE STAT-OGG-WF.
        initStatOggWf();
        //--> DATA EFFETTO
        //--> SALVO LE DATE DI CONTESTO PER RIPRISTINARLE DOPO L'ACCESSO
        // COB_CODE: INITIALIZE WK-APPO-DATE
        initWkAppoDate();
        // COB_CODE: IF  IDSV0003-DATA-INIZIO-EFFETTO IS NUMERIC
        //           AND IDSV0003-DATA-INIZIO-EFFETTO > ZEROES
        //                 TO WK-APPO-DATA-INIZIO-EFFETTO
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataInizioEffetto()) && idsv0003.getDataInizioEffetto() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
            //             TO WK-APPO-DATA-INIZIO-EFFETTO
            ws.getWkAppoDate().setInizioEffetto(idsv0003.getDataInizioEffetto());
        }
        // COB_CODE: IF  IDSV0003-DATA-FINE-EFFETTO IS NUMERIC
        //           AND IDSV0003-DATA-FINE-EFFETTO > ZEROES
        //                 TO WK-APPO-DATA-FINE-EFFETTO
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO
            //             TO WK-APPO-DATA-FINE-EFFETTO
            ws.getWkAppoDate().setFineEffetto(idsv0003.getDataFineEffetto());
        }
        // COB_CODE: IF  IDSV0003-DATA-COMPETENZA IS NUMERIC
        //           AND IDSV0003-DATA-COMPETENZA > ZEROES
        //                 TO WK-APPO-DATA-COMPETENZA
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataCompetenza()) && idsv0003.getDataCompetenza() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
            //             TO WK-APPO-DATA-COMPETENZA
            ws.getWkAppoDate().setCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-ID-OGGETTO         TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        //--> VALORRIZZA DCLGEN TABELLA
        // COB_CODE: MOVE MOV-ID-MOVI              TO STW-ID-OGG.
        ws.getStatOggWf().setStwIdOgg(ws.getMovi().getMovIdMovi());
        // COB_CODE: MOVE 'MO'                     TO STW-TP-OGG.
        ws.getStatOggWf().setStwTpOgg("MO");
        // COB_CODE: CALL IDBSSTW0  USING  IDSV0003 STAT-OGG-WF
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            idbsstw0 = Idbsstw0.getInstance();
            idbsstw0.run(idsv0003, ws.getStatOggWf());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE IDBSSTW0
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsstw0());
            // COB_CODE: MOVE 'ERRORE CALL LDBS1530'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS1530");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //           *-->    GESTIRE ERRORE DB
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   END-STRING
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //-->    GESTIRE ERRORE DB
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          END-IF
            //                       WHEN IDSV0003-NOT-FOUND
            //           *-->        CHIAVE NON TROVATA
            //                             END-STRING
            //                       WHEN OTHER
            //           *-->        ERRORE DI ACCESSO AL DB
            //                          END-STRING
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: IF STW-STAT-OGG-WF = 'CO'
                    //                TO WK-DT-CPTZ-COMUN
                    //           ELSE
                    //              END-STRING
                    //           END-IF
                    if (Conditions.eq(ws.getStatOggWf().getStwStatOggWf(), "CO")) {
                        // COB_CODE: MOVE STW-DT-INI-EFF
                        //             TO WK-DT-EFF-COMUN
                        ws.setWkDtEffComun(ws.getStatOggWf().getStwDtIniEff());
                        // COB_CODE: MOVE STW-DS-TS-INI-CPTZ
                        //             TO WK-DT-CPTZ-COMUN
                        ws.setWkDtCptzComun(ws.getStatOggWf().getStwDsTsIniCptz());
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE IDBSSTW0
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsstw0());
                        // COB_CODE: STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                        //                   IDSV0003-RETURN-CODE ';'
                        //                   IDSV0003-SQLCODE
                        //            DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA IDBSSTW0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    }
                    break;

                case Idsv0003Sqlcode.NOT_FOUND://-->        CHIAVE NON TROVATA
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE IDBSSTW0
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsstw0());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                    //                   IDSV0003-RETURN-CODE ';'
                    //                   IDSV0003-SQLCODE
                    //            DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA IDBSSTW0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;

                default://-->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE IDBSSTW0
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsstw0());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                    //                   IDSV0003-RETURN-CODE ';'
                    //                   IDSV0003-SQLCODE
                    //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA IDBSSTW0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE IDBSSTW0
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbsstw0());
            // COB_CODE: STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
            //                   IDSV0003-RETURN-CODE ';'
            //                   IDSV0003-SQLCODE
            //                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA IDBSSTW0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
        //--> RIPRISTINO LE DATE DI CONTESTO
        // COB_CODE: IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
        //                TO IDSV0003-DATA-INIZIO-EFFETTO
        //           END-IF.
        if (ws.getWkAppoDate().getInizioEffetto() > 0) {
            // COB_CODE: MOVE WK-APPO-DATA-INIZIO-EFFETTO
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkAppoDate().getInizioEffetto());
        }
        // COB_CODE: IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
        //                TO IDSV0003-DATA-FINE-EFFETTO
        //           END-IF.
        if (ws.getWkAppoDate().getFineEffetto() > 0) {
            // COB_CODE: MOVE WK-APPO-DATA-FINE-EFFETTO
            //             TO IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataFineEffetto(ws.getWkAppoDate().getFineEffetto());
        }
        // COB_CODE: IF WK-APPO-DATA-COMPETENZA > ZEROES
        //                TO IDSV0003-DATA-COMPETENZA
        //           END-IF.
        if (ws.getWkAppoDate().getCompetenza() > 0) {
            // COB_CODE: MOVE WK-APPO-DATA-COMPETENZA
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkAppoDate().getCompetenza());
        }
    }

    /**Original name: S1235-RECUP-MOVI-FINRIO<br>
	 * <pre>----------------------------------------------------------------*
	 *   RECUPERO L'ID DELLA MOVIMENTO FINANZIARIO DEL RISC. PARZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1235RecupMoviFinrio() {
        Ldbs5950 ldbs5950 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                      MOVI-FINRIO.
        initMoviFinrio();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: MOVE MOV-ID-MOVI                TO MFZ-ID-MOVI-CRZ
        ws.getMoviFinrio().setMfzIdMoviCrz(ws.getMovi().getMovIdMovi());
        //--> SALVO LE DATE DI CONTESTO PER RIPRISTINARLE DOPO L'ACCESSO
        // COB_CODE: INITIALIZE WK-APPO-DATE
        initWkAppoDate();
        // COB_CODE: IF  IDSV0003-DATA-INIZIO-EFFETTO IS NUMERIC
        //           AND IDSV0003-DATA-INIZIO-EFFETTO > ZEROES
        //                 TO WK-APPO-DATA-INIZIO-EFFETTO
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataInizioEffetto()) && idsv0003.getDataInizioEffetto() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
            //             TO WK-APPO-DATA-INIZIO-EFFETTO
            ws.getWkAppoDate().setInizioEffetto(idsv0003.getDataInizioEffetto());
        }
        // COB_CODE: IF  IDSV0003-DATA-FINE-EFFETTO IS NUMERIC
        //           AND IDSV0003-DATA-FINE-EFFETTO > ZEROES
        //                 TO WK-APPO-DATA-FINE-EFFETTO
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO
            //             TO WK-APPO-DATA-FINE-EFFETTO
            ws.getWkAppoDate().setFineEffetto(idsv0003.getDataFineEffetto());
        }
        // COB_CODE: IF  IDSV0003-DATA-COMPETENZA IS NUMERIC
        //           AND IDSV0003-DATA-COMPETENZA > ZEROES
        //                 TO WK-APPO-DATA-COMPETENZA
        //           END-IF
        if (Functions.isNumber(idsv0003.getDataCompetenza()) && idsv0003.getDataCompetenza() > 0) {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA
            //             TO WK-APPO-DATA-COMPETENZA
            ws.getWkAppoDate().setCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE: MOVE WK-DT-EFF-COMUN          TO IDSV0003-DATA-INIZIO-EFFETTO
        //                                            IDSV0003-DATA-FINE-EFFETTO
        idsv0003.setDataInizioEffetto(ws.getWkDtEffComun());
        idsv0003.setDataFineEffetto(ws.getWkDtEffComun());
        // COB_CODE: MOVE WK-DT-CPTZ-COMUN         TO IDSV0003-DATA-COMPETENZA
        idsv0003.setDataCompetenza(ws.getWkDtCptzComun());
        // COB_CODE: MOVE MOVI-FINRIO                TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getMoviFinrio().getMoviFinrioFormatted());
        // COB_CODE: CALL LDBS5950  USING  IDSV0003 MOVI-FINRIO
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs5950 = Ldbs5950.getInstance();
            ldbs5950.run(idsv0003, ws.getMoviFinrio());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS5950
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs5950());
            // COB_CODE: MOVE 'ERRORE CALL LDBS5950'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS5950");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN IDSV0003-SUCCESSFUL-SQL
        //           *-->       OPERAZIONE ESEGUITA CON SUCCESSO
        //                           TO WK-ID-MOVI-FINRIO
        //                    WHEN IDSV0003-NOT-FOUND
        //           *-->     CHIAVE NON TROVATA
        //                           TO WK-ID-MOVI-FINRIO
        //                    WHEN OTHER
        //           *-->     ERRORE DB
        //                       END-STRING
        //                END-EVALUATE.
        switch (idsv0003.getSqlcode().getSqlcode()) {

            case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CON SUCCESSO
                // COB_CODE: MOVE MFZ-ID-MOVI-FINRIO
                //             TO WK-ID-MOVI-FINRIO
                ws.setWkIdMoviFinrio(ws.getMoviFinrio().getMfzIdMoviFinrio());
                break;

            case Idsv0003Sqlcode.NOT_FOUND://-->     CHIAVE NON TROVATA
                // COB_CODE: MOVE ZEROES
                //             TO WK-ID-MOVI-FINRIO
                ws.setWkIdMoviFinrio(0);
                break;

            default://-->     ERRORE DB
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                //                 IDSV0003-RETURN-CODE ';'
                //                 IDSV0003-SQLCODE
                //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA LDBS1530 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                break;
        }
        //--> RIPRISTINO LE DATE DI CONTESTO
        // COB_CODE: IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
        //                TO IDSV0003-DATA-INIZIO-EFFETTO
        //           END-IF.
        if (ws.getWkAppoDate().getInizioEffetto() > 0) {
            // COB_CODE: MOVE WK-APPO-DATA-INIZIO-EFFETTO
            //             TO IDSV0003-DATA-INIZIO-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkAppoDate().getInizioEffetto());
        }
        // COB_CODE: IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
        //                TO IDSV0003-DATA-FINE-EFFETTO
        //           END-IF.
        if (ws.getWkAppoDate().getFineEffetto() > 0) {
            // COB_CODE: MOVE WK-APPO-DATA-FINE-EFFETTO
            //             TO IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataFineEffetto(ws.getWkAppoDate().getFineEffetto());
        }
        // COB_CODE: IF WK-APPO-DATA-COMPETENZA > ZEROES
        //                TO IDSV0003-DATA-COMPETENZA
        //           END-IF.
        if (ws.getWkAppoDate().getCompetenza() > 0) {
            // COB_CODE: MOVE WK-APPO-DATA-COMPETENZA
            //             TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkAppoDate().getCompetenza());
        }
    }

    /**Original name: S1250-CALCOLA-QUOTE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaQuote() {
        Ldbs4910 ldbs4910 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-FETCH-FIRST            TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET WK-NUM-QUO-OK                   TO TRUE.
        ws.getWkNumQuo().setOk();
        //
        // COB_CODE: INITIALIZE LDBV4911
        initLdbv4911();
        // COB_CODE: MOVE IVVC0213-ID-TRANCHE   TO LDBV4911-ID-TRCH-DI-GAR
        ws.getLdbv4911().setIdTrchDiGar(ivvc0213.getIdTranche());
        // COB_CODE: MOVE 'LQ'                  TO LDBV4911-TP-VAL-AST-1
        ws.getLdbv4911().setTpValAst1("LQ");
        // COB_CODE: MOVE 'AL'                  TO LDBV4911-TP-VAL-AST-2
        ws.getLdbv4911().setTpValAst2("AL");
        // COB_CODE: MOVE LDBV4911              TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbv4911().getLdbv4911Formatted());
        // COB_CODE:      PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                           OR WK-NUM-QUO-KO
        //           *
        //                   END-EVALUATE
        //           *
        //                END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql() || ws.getWkNumQuo().isKo())) {
            //
            // COB_CODE: CALL LDBS4910  USING  IDSV0003 VAL-AST
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs4910 = Ldbs4910.getInstance();
                ldbs4910.run(idsv0003, ws.getValAst());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS4910
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
                // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSV0003-NOT-FOUND
            //           *-->          CHIAVE NON TROVATA
            //                         END-IF
            //                      WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         SET IDSV0003-FETCH-NEXT TO TRUE
            //           *
            //                      WHEN OTHER
            //                         END-STRING
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.NOT_FOUND://-->          CHIAVE NON TROVATA
                    // COB_CODE: IF IDSV0003-FETCH-FIRST
                    //                TO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-IF
                    if (idsv0003.getOperazione().isFetchFirst()) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'LETTURA VALORE ASSETT - SQLCODE = 100 '
                        //             TO IDSV0003-DESCRIZ-ERR-DB2
                        idsv0003.getCampiEsito().setDescrizErrDb2("LETTURA VALORE ASSETT - SQLCODE = 100 ");
                    }
                    break;

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM S1251-CALCOLA-QUOTE
                    //              THRU S1251-EX
                    s1251CalcolaQuote();
                    // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                    //
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                    //                 IDSV0003-RETURN-CODE ';'
                    //                 IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA VALORE ASSET ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
            //
        }
        //
        // COB_CODE:      IF WK-NUM-QUO-KO
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (ws.getWkNumQuo().isKo()) {
            //
            // COB_CODE: PERFORM S1252-CHIUDE-CURVAS         THRU S1252-EX
            s1252ChiudeCurvas();
            //
            // COB_CODE: IF DTGA-PRSTZ-INI(IVVC0213-IX-TABB) IS NUMERIC
            //                TO IVVC0213-VAL-IMP-O
            //           ELSE
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIni())) {
                // COB_CODE: MOVE DTGA-PRSTZ-INI(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIni(), 18, 7));
            }
            else {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'QUOTE NON VALORIZZATE'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("QUOTE NON VALORIZZATE");
            }
            //
        }
        // COB_CODE: PERFORM S1253-ARROTONDA-IMP     THRU S1253-EX.
        s1253ArrotondaImp();
    }

    /**Original name: S1252-CHIUDE-CURVAS<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIUSURA CURSORE VALORE ASSET VALORE ASSETT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1252ChiudeCurvas() {
        Ldbs4910 ldbs4910 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        //
        // COB_CODE: CALL LDBS4910 USING  IDSV0003 VAL-AST
        //             ON EXCEPTION
        //                 SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            ldbs4910 = Ldbs4910.getInstance();
            ldbs4910.run(idsv0003, ws.getValAst());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS4910
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
            // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBS4910
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
            // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S1251-CALCOLA-QUOTE<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA VALORIZZAZIONE VALORE ASSETT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1251CalcolaQuote() {
        // COB_CODE: SET WK-ID-COD-NON-TROVATO           TO TRUE.
        ws.getWkIdCodLivFlag().setNonTrovato();
        //
        // COB_CODE: PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
        //             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
        //                OR WK-ID-COD-TROVATO
        //                OR WK-NUM-QUO-KO
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabL19(((short)1));
        while (!(ws.getIxIndici().getTabL19() > ws.getDl19EleFndMax() || ws.getWkIdCodLivFlag().isTrovato() || ws.getWkNumQuo().isKo())) {
            // COB_CODE: IF VAS-COD-FND = DL19-COD-FND(IX-TAB-L19)
            //              END-IF
            //           END-IF
            if (Conditions.eq(ws.getValAst().getVasCodFnd(), ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19CodFnd())) {
                // COB_CODE: SET WK-ID-COD-TROVATO       TO TRUE
                ws.getWkIdCodLivFlag().setTrovato();
                //
                // COB_CODE:              IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC
                //           *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                //                           END-IF
                //           *
                //                        ELSE
                //           *
                //                            SET IDSV0003-INVALID-OPER  TO TRUE
                //                        END-IF
                if (Functions.isNumber(ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().getWl19ValQuo())) {
                    //--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                    // COB_CODE: IF VAS-NUM-QUO IS NUMERIC
                    //              END-IF
                    //           ELSE
                    //              SET WK-NUM-QUO-KO      TO TRUE
                    //           END-IF
                    if (Functions.isNumber(ws.getValAst().getVasNumQuo().getVasNumQuo())) {
                        // COB_CODE: IF VAS-ID-MOVI-FINRIO = WK-ID-MOVI-FINRIO
                        //              CONTINUE
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        if (ws.getValAst().getVasIdMoviFinrio() == ws.getWkIdMoviFinrio()) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: MOVE ZERO          TO WK-VAL-FONDO
                            ws.setWkValFondo(new AfDecimal(0, 15, 3));
                            // COB_CODE: COMPUTE WK-VAL-FONDO = (VAS-NUM-QUO *
                            //                             DL19-VAL-QUO(IX-TAB-L19))
                            ws.setWkValFondo(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo().multiply(ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().getWl19ValQuo()), 15, 3));
                            // COB_CODE: MOVE VAS-TP-VAL-AST TO WK-TP-VAL-AST
                            ws.getWkTpValAst().setWkTpValAst(ws.getValAst().getVasTpValAst());
                            // COB_CODE: IF VALORE-POSITIVO
                            //           OR ANNULLO-NEGATIVO
                            //                 = (IVVC0213-VAL-IMP-O + WK-VAL-FONDO)
                            //           END-IF
                            if (ws.getWkTpValAst().isValorePositivo() || ws.getWkTpValAst().isAnnulloNegativo()) {
                                // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O
                                //              = (IVVC0213-VAL-IMP-O + WK-VAL-FONDO)
                                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ivvc0213.getTabOutput().getValImpO().add(ws.getWkValFondo()), 18, 7));
                            }
                            // COB_CODE: IF VALORE-NEGATIVO
                            //           OR ANNULLO-POSITIVO
                            //                 = (IVVC0213-VAL-IMP-O - WK-VAL-FONDO)
                            //           END-IF
                            if (ws.getWkTpValAst().isValoreNegativo() || ws.getWkTpValAst().isAnnulloPositivo()) {
                                // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O
                                //              = (IVVC0213-VAL-IMP-O - WK-VAL-FONDO)
                                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ivvc0213.getTabOutput().getValImpO().subtract(ws.getWkValFondo()), 18, 7));
                            }
                        }
                    }
                    else {
                        // COB_CODE: SET WK-NUM-QUO-KO      TO TRUE
                        ws.getWkNumQuo().setKo();
                    }
                    //
                }
                else {
                    //
                    // COB_CODE: MOVE LDBS4910
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
                    // COB_CODE: MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("NUM-QUO / VAL-QUO NON NUMERICI");
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
            ws.getIxIndici().setTabL19(Trunc.toShort(ws.getIxIndici().getTabL19() + 1, 4));
        }
    }

    /**Original name: S1253-ARROTONDA-IMP<br>
	 * <pre>----------------------------------------------------------------*
	 *    ARROTONDAMENTO IMPORTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1253ArrotondaImp() {
        // COB_CODE: MOVE IVVC0213-VAL-IMP-O              TO WK-AREA-ARRO.
        ws.setWkAreaArroFormatted(ivvc0213.getTabOutput().getIvvc0213ValImpOFormatted());
        // COB_CODE: MOVE IVVC0213-VAL-IMP-O              TO WK-APPO-2DEC.
        ws.setWkAppo2dec(Trunc.toDecimal(ivvc0213.getTabOutput().getValImpO(), 13, 2));
        // COB_CODE: MOVE WK-IMP-ARRO(14)                 TO WK-APP-ARRO-1.
        ws.setWkAppArro1Formatted(String.valueOf(ws.getWkTabArro(14).getWkImpArro()));
        // COB_CODE: IF WK-APP-ARRO-1 NOT LESS 5
        //              END-IF
        //           END-IF.
        if (ws.getWkAppArro1() >= 5) {
            // COB_CODE: IF WK-APPO-2DEC < ZERO
            //              SUBTRACT WK-ADD-CENT           FROM WK-APPO-2DEC
            //           ELSE
            //              ADD WK-ADD-CENT                TO WK-APPO-2DEC
            //           END-IF
            if (ws.getWkAppo2dec().compareTo(0) < 0) {
                // COB_CODE: SUBTRACT WK-ADD-CENT           FROM WK-APPO-2DEC
                ws.setWkAppo2dec(Trunc.toDecimal(ws.getWkAppo2dec().subtract(ws.getWkAddCent()), 13, 2));
            }
            else {
                // COB_CODE: ADD WK-ADD-CENT                TO WK-APPO-2DEC
                ws.setWkAppo2dec(Trunc.toDecimal(ws.getWkAddCent().add(ws.getWkAppo2dec()), 13, 2));
            }
        }
        //
        // COB_CODE: MOVE WK-APPO-2DEC                    TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWkAppo2dec(), 18, 7));
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabL19(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabVas(((short)0));
        ws.getIxIndici().setGuidaGrz(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initValAst() {
        ws.getValAst().setVasIdValAst(0);
        ws.getValAst().getVasIdTrchDiGar().setVasIdTrchDiGar(0);
        ws.getValAst().setVasIdMoviFinrio(0);
        ws.getValAst().setVasIdMoviCrz(0);
        ws.getValAst().getVasIdMoviChiu().setVasIdMoviChiu(0);
        ws.getValAst().setVasDtIniEff(0);
        ws.getValAst().setVasDtEndEff(0);
        ws.getValAst().setVasCodCompAnia(0);
        ws.getValAst().setVasCodFnd("");
        ws.getValAst().getVasNumQuo().setVasNumQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasValQuo().setVasValQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzz().setVasDtValzz(0);
        ws.getValAst().setVasTpValAst("");
        ws.getValAst().getVasIdRichInvstFnd().setVasIdRichInvstFnd(0);
        ws.getValAst().getVasIdRichDisFnd().setVasIdRichDisFnd(0);
        ws.getValAst().setVasDsRiga(0);
        ws.getValAst().setVasDsOperSql(Types.SPACE_CHAR);
        ws.getValAst().setVasDsVer(0);
        ws.getValAst().setVasDsTsIniCptz(0);
        ws.getValAst().setVasDsTsEndCptz(0);
        ws.getValAst().setVasDsUtente("");
        ws.getValAst().setVasDsStatoElab(Types.SPACE_CHAR);
        ws.getValAst().getVasPreMovto().setVasPreMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasImpMovto().setVasImpMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPcInvDis().setVasPcInvDis(new AfDecimal(0, 14, 9));
        ws.getValAst().getVasNumQuoLorde().setVasNumQuoLorde(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzzCalc().setVasDtValzzCalc(0);
        ws.getValAst().getVasMinusValenza().setVasMinusValenza(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPlusValenza().setVasPlusValenza(new AfDecimal(0, 15, 3));
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0116Data.DTGA_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getDtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
    }

    public void initAreaIoGar() {
        ws.setDgrzEleGarMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0116Data.DGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getDgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initAreaIoL19() {
        ws.setDl19EleFndMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0116Data.DL19_TAB_FND_MAXOCCURS; idx0++) {
            ws.getDl19TabFnd(idx0).getLccvl191().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19CodCompAnia(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19CodFnd("");
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DtQtz(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuo(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuoManfee().setDl19ValQuoManfee(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsOperSql(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsVer(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsTsCptz(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsUtente("");
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsStatoElab(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19TpFnd(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19DtRilevazioneNav().setDl19DtRilevazioneNav(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuoAcq().setDl19ValQuoAcq(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19FlNoNav(Types.SPACE_CHAR);
        }
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initStatOggWf() {
        ws.getStatOggWf().setStwIdStatOggWf(0);
        ws.getStatOggWf().setStwIdOgg(0);
        ws.getStatOggWf().setStwTpOgg("");
        ws.getStatOggWf().setStwIdMoviCrz(0);
        ws.getStatOggWf().getStwIdMoviChiu().setStwIdMoviChiu(0);
        ws.getStatOggWf().setStwDtIniEff(0);
        ws.getStatOggWf().setStwDtEndEff(0);
        ws.getStatOggWf().setStwCodCompAnia(0);
        ws.getStatOggWf().setStwCodPrcs("");
        ws.getStatOggWf().setStwCodAttvt("");
        ws.getStatOggWf().setStwStatOggWf("");
        ws.getStatOggWf().setStwFlStatEnd(Types.SPACE_CHAR);
        ws.getStatOggWf().setStwDsRiga(0);
        ws.getStatOggWf().setStwDsOperSql(Types.SPACE_CHAR);
        ws.getStatOggWf().setStwDsVer(0);
        ws.getStatOggWf().setStwDsTsIniCptz(0);
        ws.getStatOggWf().setStwDsTsEndCptz(0);
        ws.getStatOggWf().setStwDsUtente("");
        ws.getStatOggWf().setStwDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWkAppoDate() {
        ws.getWkAppoDate().setInizioEffetto(0);
        ws.getWkAppoDate().setFineEffetto(0);
        ws.getWkAppoDate().setCompetenza(0);
    }

    public void initMoviFinrio() {
        ws.getMoviFinrio().setMfzIdMoviFinrio(0);
        ws.getMoviFinrio().getMfzIdAdes().setMfzIdAdes(0);
        ws.getMoviFinrio().getMfzIdLiq().setMfzIdLiq(0);
        ws.getMoviFinrio().getMfzIdTitCont().setMfzIdTitCont(0);
        ws.getMoviFinrio().setMfzIdMoviCrz(0);
        ws.getMoviFinrio().getMfzIdMoviChiu().setMfzIdMoviChiu(0);
        ws.getMoviFinrio().setMfzDtIniEff(0);
        ws.getMoviFinrio().setMfzDtEndEff(0);
        ws.getMoviFinrio().setMfzCodCompAnia(0);
        ws.getMoviFinrio().setMfzTpMoviFinrio("");
        ws.getMoviFinrio().getMfzDtEffMoviFinrio().setMfzDtEffMoviFinrio(0);
        ws.getMoviFinrio().getMfzDtElab().setMfzDtElab(0);
        ws.getMoviFinrio().getMfzDtRichMovi().setMfzDtRichMovi(0);
        ws.getMoviFinrio().getMfzCosOprz().setMfzCosOprz(new AfDecimal(0, 15, 3));
        ws.getMoviFinrio().setMfzStatMovi("");
        ws.getMoviFinrio().setMfzDsRiga(0);
        ws.getMoviFinrio().setMfzDsOperSql(Types.SPACE_CHAR);
        ws.getMoviFinrio().setMfzDsVer(0);
        ws.getMoviFinrio().setMfzDsTsIniCptz(0);
        ws.getMoviFinrio().setMfzDsTsEndCptz(0);
        ws.getMoviFinrio().setMfzDsUtente("");
        ws.getMoviFinrio().setMfzDsStatoElab(Types.SPACE_CHAR);
        ws.getMoviFinrio().setMfzTpCausRettifica("");
    }

    public void initLdbv4911() {
        ws.getLdbv4911().setTpValAst1("");
        ws.getLdbv4911().setTpValAst2("");
        ws.getLdbv4911().setTpValAst3("");
        ws.getLdbv4911().setTpValAst4("");
        ws.getLdbv4911().setTpValAst5("");
        ws.getLdbv4911().setTpValAst6("");
        ws.getLdbv4911().setTpValAst7("");
        ws.getLdbv4911().setTpValAst8("");
        ws.getLdbv4911().setTpValAst9("");
        ws.getLdbv4911().setTpValAst10("");
        ws.getLdbv4911().setIdTrchDiGar(0);
    }
}
