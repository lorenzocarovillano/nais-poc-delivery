package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOugbmba;
import it.accenture.jnais.copy.A2kOurid;
import it.accenture.jnais.copy.A2kOusta;
import it.accenture.jnais.copy.A2kOutput;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ldbv4021;
import it.accenture.jnais.ws.Lvvs0097Data;
import it.accenture.jnais.ws.occurs.W1tgaTabTran;

/**Original name: LVVS0097<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0097
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0097 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0097Data ws = new Lvvs0097Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0097_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lvvs0097 getInstance() {
        return ((Lvvs0097)Programs.getInstance(Lvvs0097.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV4021.
        initLdbv4021();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-TGA.
        initAreaIoTga();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE  IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
            // COB_CODE: PERFORM S1250-VERIFICA-DATE         THRU S1250-EX
            s1250VerificaDate();
            // COB_CODE: IF  CALC-IMPOSTA-SOSTIT
            //               END-IF
            //           ELSE
            //               PERFORM S1255-CALCOLA-IMPORTO   THRU S1255-EX
            //           END-IF
            if (ws.getWsMovimento().isCalcImpostaSostit()) {
                // COB_CODE: IF IN-RCODE  = '00'
                //              PERFORM S1256-CALCOLA-IMPORTO   THRU S1256-EX
                //           END-IF
                if (ws.getInRcodeFormatted().equals("00")) {
                    // COB_CODE: PERFORM S1256-CALCOLA-IMPORTO   THRU S1256-EX
                    s1256CalcolaImporto();
                }
            }
            else {
                // COB_CODE: PERFORM S1255-CALCOLA-IMPORTO   THRU S1255-EX
                s1255CalcolaImporto();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-VERIFICA-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATAULTADEGP
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250VerificaDate() {
        // COB_CODE: INITIALIZE PARAM-MOVI
        //                      LDBV1471.
        initParamMovi();
        initLdbv1471();
        //
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE             TO PMO-ID-OGG.
        ws.getParamMovi().setPmoIdOgg(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE 'AD'                             TO PMO-TP-OGG.
        ws.getParamMovi().setPmoTpOgg("AD");
        // COB_CODE: MOVE 6010                             TO LDBV1471-TP-MOVI-01.
        ws.getLdbv1471().setMovi01(6010);
        // COB_CODE: SET IDSV0003-SELECT                   TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION          TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE LDBV1471                 TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbv1471().getLdbv1471Formatted());
        // COB_CODE: PERFORM S1251-CALL-LDBS1470           THRU S1251-EX.
        s1251CallLdbs1470();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              MOVE IVVC0213-DATA-EFFETTO(1:4)    TO AAAA-SUP
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUE
            //              MOVE IVVC0213-DATA-EFFETTO(1:4) TO AAAA-SUP
            //           ELSE
            //              END-IF
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted())) {
                // COB_CODE: MOVE IVVC0213-DATA-EFFETTO(1:4) TO AAAA-INF
                ws.getDataInferiore().setAaaaInfFormatted(ivvc0213.getDataEffettoFormatted().substring((1) - 1, 4));
                // COB_CODE: MOVE IVVC0213-DATA-EFFETTO(1:4) TO AAAA-SUP
                ws.getDataSuperiore().setAaaaSupFormatted(ivvc0213.getDataEffettoFormatted().substring((1) - 1, 4));
            }
            else if (ws.getWsMovimento().isCalcImpostaSostit()) {
                // COB_CODE: IF CALC-IMPOSTA-SOSTIT
                //                 THRU S1257-EX
                //           ELSE
                //             MOVE PMO-DT-RICOR-PREC        TO DATA-SUPERIORE-N
                //           END-IF
                // COB_CODE: PERFORM S1257-CALC-DT-INFERIORE
                //              THRU S1257-EX
                s1257CalcDtInferiore();
            }
            else {
                // COB_CODE: MOVE PMO-DT-INI-EFF           TO DATA-INFERIORE-N
                ws.getDataInferiore().setDataInferioreN(TruncAbs.toInt(ws.getParamMovi().getPmoDtIniEff(), 8));
                // COB_CODE: MOVE PMO-DT-RICOR-PREC        TO DATA-SUPERIORE-N
                ws.getDataSuperiore().setDataSuperioreN(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec(), 8));
            }
        }
        else {
            // COB_CODE: MOVE IVVC0213-DATA-EFFETTO(1:4)    TO AAAA-INF
            ws.getDataInferiore().setAaaaInfFormatted(ivvc0213.getDataEffettoFormatted().substring((1) - 1, 4));
            // COB_CODE: MOVE IVVC0213-DATA-EFFETTO(1:4)    TO AAAA-SUP
            ws.getDataSuperiore().setAaaaSupFormatted(ivvc0213.getDataEffettoFormatted().substring((1) - 1, 4));
        }
    }

    /**Original name: S1251-CALL-LDBS1470<br>
	 * <pre>----------------------------------------------------------------*
	 *   LETTURA PMO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1251CallLdbs1470() {
        Ldbs1470 ldbs1470 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT               TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION      TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE 'LDBS1470'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS1470");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs1470 = Ldbs1470.getInstance();
            ldbs1470.run(idsv0003, ws.getParamMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1470 ERRORE CHIAMATA - LDBS1470'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1470 ERRORE CHIAMATA - LDBS1470");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF NOT IDSV0003-SUCCESSFUL-SQL
        //                   END-IF
        //           *
        //                END-IF.
        if (!idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              CONTINUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == -811) {
                // COB_CODE: IF IDSV0003-SQLCODE = -811
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              END-STRING
                //           END-IF
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS1470 ;'
                //                   IDSV0003-RETURN-CODE ';'
                //                   IDSV0003-SQLCODE
                //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1470 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
            //
        }
    }

    /**Original name: S1255-CALCOLA-IMPORTO<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1255CalcolaImporto() {
        Ldbs4020 ldbs4020 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBV4021.
        initLdbv4021();
        //
        // COB_CODE: SET IDSV0003-SELECT               TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION      TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE DTGA-ID-TRCH-DI-GAR(IVVC0213-IX-TABB)
        //                                               TO LDBV4021-ID-TGA.
        ws.getLdbv4021().setIdTga(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: MOVE DATA-INFERIORE-N               TO LDBV4021-DATA-INIZIO.
        ws.getLdbv4021().setDataInizioFormatted(ws.getDataInferiore().getDataInferioreNFormatted());
        // COB_CODE: MOVE DATA-SUPERIORE-N               TO LDBV4021-DATA-FINE.
        ws.getLdbv4021().setDataFineFormatted(ws.getDataSuperiore().getDataSuperioreNFormatted());
        //
        // COB_CODE: MOVE 'LDBS4020'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS4020");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV4021
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs4020 = Ldbs4020.getInstance();
            ldbs4020.run(idsv0003, ws.getLdbv4021());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS4020 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS4020 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-SQL
        //                   MOVE LDBV4021-PRE-LRD      TO IVVC0213-VAL-IMP-O
        //                ELSE
        //                   END-STRING
        //           *
        //                END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBV4021-PRE-LRD      TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbv4021().getPreLrd(), 18, 7));
        }
        else {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS4020 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS4020 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            //
        }
    }

    /**Original name: S1256-CALCOLA-IMPORTO<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA PREPAGANNO IN CASO DI CALCOLO IMPOSTA SOSTITUTIVA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1256CalcolaImporto() {
        Ldbse090 ldbse090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBVE091.
        initLdbve091();
        //
        // COB_CODE: SET IDSV0003-SELECT               TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION      TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE DTGA-ID-TRCH-DI-GAR(IVVC0213-IX-TABB)
        //                                               TO LDBVE091-ID-TGA.
        ws.getLdbve091().setIdTga(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: MOVE DATA-INFERIORE-N               TO LDBVE091-DATA-INIZIO.
        ws.getLdbve091().setDataInizioFormatted(ws.getDataInferiore().getDataInferioreNFormatted());
        // COB_CODE: MOVE DATA-SUPERIORE-N               TO LDBVE091-DATA-FINE.
        ws.getLdbve091().setDataFineFormatted(ws.getDataSuperiore().getDataSuperioreNFormatted());
        //
        // COB_CODE: MOVE 'LDBSE090'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBSE090");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBVE091
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbse090 = Ldbse090.getInstance();
            ldbse090.run(idsv0003, ws.getLdbve091());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS4020 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS4020 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-SQL
        //                   MOVE LDBVE091-PRE-LRD      TO IVVC0213-VAL-IMP-O
        //                ELSE
        //                   END-STRING
        //           *
        //                END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBVE091-PRE-LRD      TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLdbve091().getPreLrd(), 18, 7));
        }
        else {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBSE090 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSE090 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            //
        }
    }

    /**Original name: S1257-CALC-DT-INFERIORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALCOLA IL VALORE DELLA DATA INIZIO VERIFICA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1257CalcDtInferiore() {
        // COB_CODE: INITIALIZE           IO-A2K-LCCC0003
        //                                IN-RCODE
        initIoA2kLccc0003();
        ws.setInRcodeFormatted("00");
        //    sottrarre DELTA alla data di input
        // COB_CODE: MOVE '02'                        TO A2K-FUNZ
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //    FORMATO AAAAMMGG
        // COB_CODE: MOVE '03'                        TO A2K-INFO
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //    NUMERO GIORNI
        // COB_CODE: MOVE 1                           TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        //    GIORNI
        // COB_CODE: MOVE SPACE                       TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        //    GIORNI FISSI
        // COB_CODE: MOVE '0'                         TO A2K-FISLAV
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //    INIZIO CONTEGGIO DA STESSO GIORNO
        // COB_CODE: MOVE 0                           TO A2K-INICON
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //    DATA INPUT : DATA RICORRENZA PRECEDENTE
        // COB_CODE: MOVE PMO-DT-RICOR-PREC           TO DATA-INFERIORE-N
        ws.getDataInferiore().setDataInferioreN(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec(), 8));
        // COB_CODE: MOVE DATA-INFERIORE              TO A2K-INDATA
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(ws.getDataInferiore().getDataInferioreFormatted());
        // COB_CODE: PERFORM CALL-ROUTINE-DATE
        //              THRU CALL-ROUTINE-DATE-EX
        callRoutineDate();
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //           *       DATA OUTPUT (CALCOLATA)
        //                   MOVE PMO-DT-RICOR-SUCC          TO DATA-SUPERIORE-N
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //       DATA OUTPUT (CALCOLATA)
            // COB_CODE: MOVE A2K-OUAMG                  TO DATA-INFERIORE-N
            ws.getDataInferiore().setDataInferioreNFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
            // COB_CODE: MOVE PMO-DT-RICOR-SUCC          TO DATA-SUPERIORE-N
            ws.getDataSuperiore().setDataSuperioreN(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc(), 8));
        }
    }

    /**Original name: CALL-ROUTINE-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CHIAMATA AL SERVIZIO CALCOLA DATA
	 * ----------------------------------------------------------------*</pre>*/
    private void callRoutineDate() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: CALL LCCS0003      USING IO-A2K-LCCC0003
        //                                    IN-RCODE
        //           ON EXCEPTION
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lvvs0097Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'LVVS0144 - ERRORE CALL LCCS0003'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("LVVS0144 - ERRORE CALL LCCS0003");
        }
        // COB_CODE: IF IN-RCODE  = '00'
        //              CONTINUE
        //           ELSE
        //                INTO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'LVVS0144 - ERRORE CALL LCCS0003 - RC: '
            //                  IN-RCODE
            //                  DELIMITED BY SIZE
            //             INTO IDSV0003-DESCRIZ-ERR-DB2
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "LVVS0144 - ERRORE CALL LCCS0003 - RC: ", ws.getInRcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabTga(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv4021() {
        ws.getLdbv4021().setIdTga(0);
        ws.getLdbv4021().setPreLrd(new AfDecimal(0, 15, 3));
        ws.getLdbv4021().setDataInizioFormatted("00000000");
        ws.getLdbv4021().setDataFineFormatted("00000000");
        ws.getLdbv4021().setDataInizioDb("");
        ws.getLdbv4021().setDataFineDb("");
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        ws.getDtgaTabTranObj().fill(new W1tgaTabTran().initW1tgaTabTran());
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initLdbv1471() {
        ws.getLdbv1471().setMovi01(0);
        ws.getLdbv1471().setMovi02(0);
        ws.getLdbv1471().setMovi03(0);
        ws.getLdbv1471().setMovi04(0);
        ws.getLdbv1471().setMovi05(0);
        ws.getLdbv1471().setMovi06(0);
        ws.getLdbv1471().setMovi07(0);
        ws.getLdbv1471().setMovi08(0);
        ws.getLdbv1471().setMovi09(0);
        ws.getLdbv1471().setMovi10(0);
    }

    public void initLdbve091() {
        ws.getLdbve091().setIdTga(0);
        ws.getLdbve091().setPreLrd(new AfDecimal(0, 15, 3));
        ws.getLdbve091().setDataInizioFormatted("00000000");
        ws.getLdbve091().setDataFineFormatted("00000000");
        ws.getLdbve091().setDataInizioDb("");
        ws.getLdbve091().setDataFineDb("");
    }

    public void initIoA2kLccc0003() {
        ws.getIoA2kLccc0003().getInput().setA2kFunz("");
        ws.getIoA2kLccc0003().getInput().setA2kInfo("");
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted("000");
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kFislav(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kInicon(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata("");
        ws.getIoA2kLccc0003().getOutput().getA2kOugmaX().setA2kOugmaFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOugg02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus102(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOumm02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus202(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuss02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuaa02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().setA2kOuamgFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().setA2kOuamgp(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuamg0p(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog9(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprogc(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuproco(0);
        ws.getIoA2kLccc0003().getOutput().getA2kOujulX().setA2kOujulFormatted("0000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOugg04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOumm04("");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuss04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuaa04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOugg05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOumm05("");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOuaa05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg06("");
        ws.getIoA2kLccc0003().getOutput().setA2kOung06Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOutg06(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg07Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugl07X().setA2kOufa07Formatted("000");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg08Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg09(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg10(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().setRcode("");
    }
}
