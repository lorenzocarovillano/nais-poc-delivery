package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcElabStateDao;
import it.accenture.jnais.commons.data.to.IBtcElabState;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcElabState;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IABS0030<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE TABELLA BTC_ELAB_STATE
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0030 extends Program implements IBtcElabState {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcElabStateDao btcElabStateDao = new BtcElabStateDao(dbAccessStatus);
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IND-BES-FLAG-TO-EXECUTE
    private short indBesFlagToExecute = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: BTC-ELAB-STATE
    private BtcElabState btcElabState;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0030_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, BtcElabState btcElabState) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.btcElabState = btcElabState;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
        a300Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0030 getInstance() {
        return ((Iabs0030)Programs.getInstance(Iabs0030.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>****</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0030'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0030");
        // COB_CODE: MOVE 'BTC_ELAB_STATE'        TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_ELAB_STATE");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT                 THRU A310-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A330-UPDATE                 THRU A330-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT                 THRU A310-EX
            a310Select();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR            THRU A360-EX
            a360OpenCursor();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR           THRU A370-EX
            a370CloseCursor();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A330-UPDATE                 THRU A330-EX
            a330Update();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursor() {
    // COB_CODE: EXEC SQL
    //                DECLARE CUR-BES CURSOR WITH HOLD FOR
    //              SELECT
    //                 COD_ELAB_STATE
    //                ,DES
    //                ,FLAG_TO_EXECUTE
    //              FROM BTC_ELAB_STATE
    //              WHERE FLAG_TO_EXECUTE = 'Y'
    //              ORDER BY COD_ELAB_STATE
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310Select() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 COD_ELAB_STATE
        //                ,DES
        //                ,FLAG_TO_EXECUTE
        //             INTO
        //                  :BES-COD-ELAB-STATE
        //                 ,:BES-DES
        //                 ,:BES-FLAG-TO-EXECUTE
        //                  :IND-BES-FLAG-TO-EXECUTE
        //             FROM BTC_ELAB_STATE
        //             WHERE  COD_ELAB_STATE = :IABV0002-STATE-CURRENT
        //           END-EXEC.
        btcElabStateDao.selectByIabv0002StateCurrent(iabv0002.getIabv0002StateCurrent(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
    }

    /**Original name: A330-UPDATE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330Update() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_ELAB_STATE SET
        //                  COD_ELAB_STATE      = :IABV0002-STATE-CURRENT
        //                  ,DES                 = :BES-DES
        //                  ,FLAG_TO_EXECUTE     = :BES-FLAG-TO-EXECUTE
        //                                         :IND-BES-FLAG-TO-EXECUTE
        //                 WHERE
        //                   COD_ELAB_STATE IN (
        //                                         :IABV0002-STATE-01,
        //                                         :IABV0002-STATE-02,
        //                                         :IABV0002-STATE-03,
        //                                         :IABV0002-STATE-04,
        //                                         :IABV0002-STATE-05,
        //                                         :IABV0002-STATE-06,
        //                                         :IABV0002-STATE-07,
        //                                         :IABV0002-STATE-08,
        //                                         :IABV0002-STATE-09,
        //                                         :IABV0002-STATE-10
        //                                     )
        //           END-EXEC.
        btcElabStateDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursor() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR THRU A305-EX.
        a305DeclareCursor();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-BES
        //           END-EXEC.
        btcElabStateDao.openCurBes();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-BES
        //           END-EXEC.
        btcElabStateDao.closeCurBes();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirst() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR    THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT THRU A390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT THRU A390-EX
            a390FetchNext();
        }
    }

    /**Original name: A390-FETCH-NEXT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BES
        //           INTO
        //                  :BES-COD-ELAB-STATE
        //                 ,:BES-DES
        //                 ,:BES-FLAG-TO-EXECUTE
        //                  :IND-BES-FLAG-TO-EXECUTE
        //           END-EXEC.
        btcElabStateDao.fetchCurBes(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BES-FLAG-TO-EXECUTE = -1
        //              MOVE HIGH-VALUES TO BES-FLAG-TO-EXECUTE-NULL
        //           END-IF.
        if (indBesFlagToExecute == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BES-FLAG-TO-EXECUTE-NULL
            btcElabState.setFlagToExecute(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BES-FLAG-TO-EXECUTE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BES-FLAG-TO-EXECUTE
        //           ELSE
        //              MOVE 0  TO IND-BES-FLAG-TO-EXECUTE
        //           END-IF.
        if (Conditions.eq(btcElabState.getFlagToExecute(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BES-FLAG-TO-EXECUTE
            indBesFlagToExecute = ((short)-1);
        }
        else {
            // COB_CODE: MOVE 0  TO IND-BES-FLAG-TO-EXECUTE
            indBesFlagToExecute = ((short)0);
        }
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndBesFlagToExecute(short indBesFlagToExecute) {
        this.indBesFlagToExecute = indBesFlagToExecute;
    }

    public short getIndBesFlagToExecute() {
        return this.indBesFlagToExecute;
    }

    @Override
    public char getCodElabState() {
        return btcElabState.getCodElabState();
    }

    @Override
    public void setCodElabState(char codElabState) {
        this.btcElabState.setCodElabState(codElabState);
    }

    @Override
    public String getDes() {
        return btcElabState.getDes();
    }

    @Override
    public void setDes(String des) {
        this.btcElabState.setDes(des);
    }

    @Override
    public char getFlagToExecute() {
        return btcElabState.getFlagToExecute();
    }

    @Override
    public void setFlagToExecute(char flagToExecute) {
        this.btcElabState.setFlagToExecute(flagToExecute);
    }

    @Override
    public Character getFlagToExecuteObj() {
        if (getIndBesFlagToExecute() >= 0) {
            return ((Character)getFlagToExecute());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagToExecuteObj(Character flagToExecuteObj) {
        if (flagToExecuteObj != null) {
            setFlagToExecute(((char)flagToExecuteObj));
            setIndBesFlagToExecute(((short)0));
        }
        else {
            setIndBesFlagToExecute(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        return iabv0002.getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        this.iabv0002.getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabv0002.getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        this.iabv0002.getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabv0002.getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        this.iabv0002.getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabv0002.getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        this.iabv0002.getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabv0002.getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        this.iabv0002.getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabv0002.getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        this.iabv0002.getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabv0002.getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        this.iabv0002.getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabv0002.getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        this.iabv0002.getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabv0002.getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        this.iabv0002.getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabv0002.getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        this.iabv0002.getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public char getIabv0002StateCurrent() {
        return iabv0002.getIabv0002StateCurrent();
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        this.iabv0002.setIabv0002StateCurrent(iabv0002StateCurrent);
    }
}
